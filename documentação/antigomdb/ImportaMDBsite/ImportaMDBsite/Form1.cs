﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImportaMDBsite
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //private System.Text.StringBuilder SB;
        private int LinhasT;

        private void GravaLog(string men)
        {
            string Men = string.Format("{0:dd/MM/yyyy HH:mm:ss} - {1}\r\n",DateTime.Now,men);
            //SB.Append(Men);
            if (LinhasT++ > 100)
            {
                LinhasT = 0;
                textBox1.Text = "";
            }
            textBox1.Text += Men;            
        }

        private int EMP = 1;
        private DataSet1 DS;

        private void Preparacao()
        {
            Cancelar = false;
            EMP = checkBox1.Checked ? 1 : 3;
            //SB = new StringBuilder();
            GravaLog("Inicio");
            DS = new DataSet1();
            GravaLog("Carrega BAL");
            DataSet1TableAdapters.BALTableAdapter TABAL = new DataSet1TableAdapters.BALTableAdapter();
            if (!checkBox1.Checked)
                TABAL.Connection.ConnectionString = TABAL.Connection.ConnectionString.Replace("Catalog=Neon;", "Catalog=NeonSA;");
            int Linhas = TABAL.Fill(DS.BAL);
            GravaLog(string.Format("Linhas Balancetes:{0:n0}", Linhas));
            Application.DoEvents();
        }

        private void Termino()
        {
            GravaLog("FIM");
            //System.IO.File.WriteAllText("log.log", SB.ToString());
            labelControl1.Text = "Terminado";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Preparacao();

            //Linhas para importar
            DataSet1TableAdapters.totaisTableAdapter TA = new DataSet1TableAdapters.totaisTableAdapter();
            if (!checkBox1.Checked)
                TA.Connection.ConnectionString = TA.Connection.ConnectionString.Replace("bal1", "bal2");
            GravaLog("Carrega Totais");
            int LinhasImportar = TA.Fill(DS.totais);
            GravaLog(string.Format("Linhas Origem:{0:n0}", LinhasImportar));
            Application.DoEvents();
            
            //Destino
            DataSet1TableAdapters.BalanceteTotaisTableAdapter TAT = new DataSet1TableAdapters.BalanceteTotaisTableAdapter();
            int Linhas = TAT.Fill(DS.BalanceteTotais);
            GravaLog(string.Format("Linhas Pre-Importadas:{0:n0}", Linhas));
            Application.DoEvents();

            //Importacao
            DateTime Prox = DateTime.Now.AddSeconds(5);
            int Linha = 0;
            string CodconAnt = "";            
            foreach (DataSet1.totaisRow rowO in DS.totais)
            {
                Linha++;
                if (Cancelar)
                {
                    GravaLog("Cancelado");
                    break;
                }
                if (rowO.codcon != CodconAnt)
                {
                    CodconAnt = rowO.codcon;
                    textBox1.Text = "";
                    LinhasT = 0;
                    GravaLog(string.Format("Condomino: {0}",CodconAnt));
                    Application.DoEvents();
                }                
                string mes = rowO.competencia.Substring(0,2);
                string ano = rowO.competencia.Substring(2,4);
                int competencia = int.Parse(ano)*100+int.Parse(mes);
                DataSet1.BALRow rowBAL = DS.BAL.FindByCONCodigoBALCompet(rowO.codcon, competencia);
                if (rowBAL != null)
                {                    
                    if (DS.BalanceteTotais.FindByBALEMP(rowBAL.BAL,EMP) != null)
                        GravaLog(string.Format("ok {0} {1}", rowO.codcon, competencia));
                    else
                    {
                        GravaLog(string.Format("NOVA {0} {1}", rowO.codcon, competencia));
                        DataSet1.BalanceteTotaisRow rowNova = DS.BalanceteTotais.NewBalanceteTotaisRow();
                        rowNova.BAL = rowBAL.BAL;
                        rowNova.EMP = EMP;
                        rowNova.acc = rowO.acc;
                        rowNova.s113 = rowO.s113;
                        rowNova.s1acp = rowO.s1acp;
                        rowNova.s1af = rowO.s1af;
                        rowNova.s1cc = rowO.s1cc;
                        rowNova.s1fr = rowO.s1fr;
                        rowNova.s1po = rowO.s1po;
                        rowNova.s1psf = rowO.s1psf;
                        rowNova.totdesp = rowO.totdesp;
                        rowNova.totrec = rowO.totrec;
                        DS.BalanceteTotais.AddBalanceteTotaisRow(rowNova);
                        TAT.Update(rowNova);
                    }
                }
                else
                    GravaLog(string.Format("X {0} {1}", rowO.codcon, competencia));
                if (DateTime.Now > Prox)
                {
                    labelControl1.Text = string.Format("Totais: {0:n0}/{1:n0} {2:n0} %", Linha,LinhasImportar,100*Linha/LinhasImportar);
                    Application.DoEvents();
                    Prox = DateTime.Now.AddSeconds(5);
                }
            }

            Termino();            
        }

        private bool Cancelar = false;

        private void button2_Click(object sender, EventArgs e)
        {
            Cancelar = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int Pular = 0;
            int.TryParse(textBox2.Text, out Pular);
            Preparacao();

            //Linhas para importar
            DataSet1TableAdapters.det1TableAdapter TA = new DataSet1TableAdapters.det1TableAdapter();
            if (!checkBox1.Checked)
                TA.Connection.ConnectionString = TA.Connection.ConnectionString.Replace("bal1", "bal2");
            GravaLog("Carrega Det1");
            int LinhasImportar = TA.Fill(DS.det1);
            GravaLog(string.Format("Linhas Origem:{0:n0}", LinhasImportar));
            Application.DoEvents();

            //Destino
            DataSet1TableAdapters.BalanceteDetTableAdapter TAD = new DataSet1TableAdapters.BalanceteDetTableAdapter();
            //int Linhas = TAD.Fill(DS.BalanceteDet);
            //GravaLog(string.Format("Linhas Pre-Importadas:{0:n0}", Linhas));
            Application.DoEvents();

            //Importacao
            DateTime Prox = DateTime.Now.AddSeconds(5);
            int Linha = 0;
            string CodconAnt = "";
            foreach (DataSet1.det1Row rowO in DS.det1)
            {
                Linha++;                                
                if (Cancelar)
                {
                    GravaLog("Cancelado");
                    break;
                };
                if (Pular > 0)
                    Pular--;
                else
                {
                    if (rowO.IsquadroNull())
                    {
                        rowO.quadro = 0;
                        //GravaLog(string.Format("              Sem Quadro {0} {1} {2} {3}", rowO.Codcon, rowO.controle, EMP, rowO.titulo));
                        //continue;
                    }
                    if (rowO.Codcon != CodconAnt)
                    {
                        CodconAnt = rowO.Codcon;
                        textBox1.Text = "";
                        LinhasT = 0;
                        GravaLog(string.Format("Condomino: {0}", CodconAnt));
                        Application.DoEvents();
                    }
                    string mes = rowO.competencia.Substring(0, 2);
                    string ano = rowO.competencia.Substring(2, 4);
                    int competencia = int.Parse(ano) * 100 + int.Parse(mes);
                    DataSet1.BALRow rowBAL = DS.BAL.FindByCONCodigoBALCompet(rowO.Codcon, competencia);
                    if (rowBAL != null)
                    {
                        //if (DS.BalanceteDet.FindByControleEMP(rowO.controle,EMP) != null)
                        if (TAD.GetDataBy(rowO.controle, EMP).Rows.Count > 0)
                            GravaLog(string.Format("ok {0} {1} - {2} {3}", rowO.Codcon, competencia, rowO.controle, EMP));
                        else
                        {
                            GravaLog(string.Format("NOVA {0} {1} - {2} {3}", rowO.Codcon, competencia, rowO.controle, EMP));
                            DataSet1.BalanceteDetRow rowNova = DS.BalanceteDet.NewBalanceteDetRow();
                            rowNova.BAL = rowBAL.BAL;
                            rowNova.EMP = EMP;
                            rowNova.Controle = rowO.controle;
                            rowNova.negrito = rowO.negrito;
                            rowNova.quadro = (int)rowO.quadro;
                            rowNova.titulo = rowO.titulo;
                            rowNova.valor = rowO.valor;
                            DS.BalanceteDet.AddBalanceteDetRow(rowNova);
                            TAD.Update(rowNova);
                        }
                    }
                    else
                        GravaLog(string.Format("X {0} {1}", rowO.Codcon, competencia));
                }
                if (DateTime.Now > Prox)
                {
                    labelControl1.Text = string.Format("Totais: {0:n0}/{1:n0} {2:n0} % - {3:n0}", Linha, LinhasImportar, 100 * Linha / LinhasImportar,Pular);
                    Application.DoEvents();
                    Prox = DateTime.Now.AddSeconds(5);
                }
            }

            Termino();
        }
    }
}
