using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TesteProxyVirtual
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string StatusTeste() 
        {
            return "**Status Teste**";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //cVirtualProxy1.StatusAplicativo = new proxyvirweb.cVirtualProxy.delStatusAplicativo(StatusTeste);
            cVirtualProxy1.StatusAplicativo = StatusTeste;
            cVirtualProxy1.Ativar(null);            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {           
            double inter = cVirtualProxy1.IntervaloAtual() / 60000;
            memoEdit1.Text = inter.ToString() + "\r\n" + memoEdit1.Text;
            cVirtualProxy1.ProcessaChamada(); 
        }

        private void cVirtualProxy1_TrocaModulo(object sender, EventArgs e)
        {
            MessageBox.Show("Troca M�dulo");
        }
    }
}