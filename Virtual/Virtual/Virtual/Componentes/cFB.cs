using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Virtual.Componentes
{
    public partial class cFB : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cFB()
        {
            InitializeComponent();
            foreach (System.ServiceProcess.ServiceControllerStatus Sts in Enum.GetValues(typeof(System.ServiceProcess.ServiceControllerStatus)))
            {
                DevExpress.XtraEditors.Controls.RadioGroupItem It = new DevExpress.XtraEditors.Controls.RadioGroupItem(Sts, Sts.ToString());                
                radioGroup1.Properties.Items.Add(It);                
            }
            Tela();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Tela();
        }

        private void Tela() {
            FBserviceController.Refresh();
            Bloc = true;
            radioGroup1.EditValue = FBserviceController.Status;
            Bloc = false;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            
 
        }

        private bool Bloc = false; 


        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Bloc)
                return;


            System.ServiceProcess.ServiceControllerStatus Sts = (System.ServiceProcess.ServiceControllerStatus)radioGroup1.EditValue;
            FBserviceController.Refresh();
            
            switch (Sts)
            {                                        
                    case System.ServiceProcess.ServiceControllerStatus.Running:
                        if (FBserviceController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                            FBserviceController.Start();
                        else if (FBserviceController.Status == System.ServiceProcess.ServiceControllerStatus.Paused)
                            FBserviceController.Continue();
                        break;
                                        
                        
                    case System.ServiceProcess.ServiceControllerStatus.Stopped:
                        if (FBserviceController.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                            FBserviceController.Stop();
                        break;

            };
            Bloc = true;
            radioGroup1.EditValue = null;
            Bloc = false;
            timer1.Enabled = true;
            

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            Tela();
        }
    }
}

