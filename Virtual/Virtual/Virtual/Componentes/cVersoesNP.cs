using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Virtual.Componentes
{
    /// <summary>
    /// Controle provis�rio de vers�es
    /// </summary>
    public partial class cVersoesNP : Virtual.Componentes.cNotePad
    {
        /// <summary>
        /// Nome do arquivo base
        /// </summary>
        protected override string ArquivoBlocoNotas
        {
            get
            {
                return PathTXT + @"versoes.txt";
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cVersoesNP()
        {
            InitializeComponent();
        }
    }
}

