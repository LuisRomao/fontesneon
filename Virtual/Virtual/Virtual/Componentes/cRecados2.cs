using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Virtual.Componentes
{
    /// <summary>
    /// Editor de recados 2
    /// </summary>
    public partial class cRecados2 : Virtual.Componentes.cNotePad
    {
        /// <summary>
        /// Cosntrutor
        /// </summary>
        public cRecados2()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Arquivo base
        /// </summary>
        protected override string ArquivoBlocoNotas
        {
            get
            {
                return PathTXT + @"Rec.vir";
            }
        }
    }
}

