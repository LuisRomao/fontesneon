﻿using System;
using System.IO;
using System.Windows.Forms;
using CompontesBasicos;
using System.Text;

namespace Virtual.Componentes
{
    /// <summary>
    /// 
    /// </summary>
    public partial class EditorASCII : ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public EditorASCII()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int offset = (int)spinoffset.Value;
            int count = (int)spinMaximo.Value;
            memoEdit1.Text = memoEdit2.Text = "";
            OpenFileDialog op = new OpenFileDialog();
            byte[] buf;
            if (op.ShowDialog() == DialogResult.OK)
            {
                using (FileStream fs = new FileStream(op.FileName, FileMode.Open))
                {
                    if (count <= 0)
                        count = (int)fs.Length;
                    buf = new byte[count];
                    fs.Position = (offset >= 0) ? offset : 0;
                    int lidos = fs.Read(buf, 0, count);
                    System.Text.StringBuilder SB = new StringBuilder();
                    System.Text.StringBuilder SB2 = new StringBuilder();
                    SB.AppendFormat("Arquivo: {0}\r\n", op.FileName);
                    SB2.AppendFormat("Arquivo: {0}\r\n", op.FileName);
                    int linha = 1;
                    int coluna = 1;
                    int MaxColuna = 0;
                    for (int i = 1; i <= lidos; i++)
                    {
                        char car = (char)buf[i - 1];
                        SB.AppendFormat("{0}", car);
                        SB2.AppendFormat("{0:00000} {1:0000}:{2:000} <{3}> <{4:X2}>\r\n", i, linha, coluna++, (char.IsLetterOrDigit(car) || (car == ' ')) ? car.ToString() : "?", (byte)car);
                        if (car == '\n')
                        {
                            linha++;
                            MaxColuna = Math.Max(MaxColuna, coluna);
                            coluna = 1;
                            SB2.AppendLine("==============================");
                        }
                    }
                    if (chGabarito.Checked)
                    {
                        for (int k = 1; k <= MaxColuna; k++)
                            if (k % 10 == 0)
                                SB.Insert(k-1,(k % 100) / 10);
                            else
                                SB.Insert(k-1, ".");
                        SB.Insert(MaxColuna, "\r\n");
                    }
                    memoEdit1.Text = SB.ToString();
                    memoEdit2.Text = SB2.ToString();
                    fs.Close();
                }
            }
        }
    }
}
