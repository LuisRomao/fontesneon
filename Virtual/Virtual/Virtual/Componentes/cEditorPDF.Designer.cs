﻿namespace Virtual.Componentes
{
    partial class cEditorPDF
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.components = new System.ComponentModel.Container();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.chBranco = new DevExpress.XtraEditors.CheckEdit();
            this.chSobrepor = new DevExpress.XtraEditors.CheckEdit();
            this.textEditTMP = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.chPintar = new DevExpress.XtraEditors.CheckEdit();
            this.chCalibracao = new DevExpress.XtraEditors.CheckEdit();
            this.SBGerar = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.pdfViewer1 = new DevExpress.XtraPdfViewer.PdfViewer();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.pdfViewer2 = new DevExpress.XtraPdfViewer.PdfViewer();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.zoomTrackBarControl1 = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.Informações = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            this.memoEdit3 = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.trocaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colModelo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFonte = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTamanhoFonte = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCentralizado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomeCampo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAjusteFino = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditDEL = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditGera = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.splitterControl3 = new DevExpress.XtraEditors.SplitterControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gabaritoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNomeCampo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConteudo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDelataLinha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFonte1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTamanhoFonte1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCentralizado1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chBranco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSobrepor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTMP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chPintar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCalibracao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).BeginInit();
            this.Informações.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trocaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditDEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditGera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gabaritoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.chBranco);
            this.panelControl1.Controls.Add(this.chSobrepor);
            this.panelControl1.Controls.Add(this.textEditTMP);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.chPintar);
            this.panelControl1.Controls.Add(this.chCalibracao);
            this.panelControl1.Controls.Add(this.SBGerar);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.textEdit1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1568, 111);
            this.panelControl1.TabIndex = 1;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Enabled = false;
            this.simpleButton5.Location = new System.Drawing.Point(665, 12);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(190, 23);
            this.simpleButton5.TabIndex = 11;
            this.simpleButton5.Text = "Gerar (Gabarito)";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // chBranco
            // 
            this.chBranco.Location = new System.Drawing.Point(559, 31);
            this.chBranco.Name = "chBranco";
            this.chBranco.Properties.Caption = "Branco";
            this.chBranco.Size = new System.Drawing.Size(75, 19);
            this.chBranco.TabIndex = 10;
            // 
            // chSobrepor
            // 
            this.chSobrepor.Location = new System.Drawing.Point(559, 6);
            this.chSobrepor.Name = "chSobrepor";
            this.chSobrepor.Properties.Caption = "Sobrepor";
            this.chSobrepor.Size = new System.Drawing.Size(75, 19);
            this.chSobrepor.TabIndex = 9;
            // 
            // textEditTMP
            // 
            this.textEditTMP.EditValue = "C:\\lixo\\testePDF.pdf";
            this.textEditTMP.Location = new System.Drawing.Point(102, 79);
            this.textEditTMP.Name = "textEditTMP";
            this.textEditTMP.Size = new System.Drawing.Size(495, 20);
            this.textEditTMP.TabIndex = 8;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 82);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 13);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Arquivo TMP";
            // 
            // chPintar
            // 
            this.chPintar.Location = new System.Drawing.Point(443, 28);
            this.chPintar.Name = "chPintar";
            this.chPintar.Properties.Caption = "Pintar de verde";
            this.chPintar.Size = new System.Drawing.Size(111, 19);
            this.chPintar.TabIndex = 6;
            // 
            // chCalibracao
            // 
            this.chCalibracao.Location = new System.Drawing.Point(443, 6);
            this.chCalibracao.Name = "chCalibracao";
            this.chCalibracao.Properties.Caption = "Calibração";
            this.chCalibracao.Size = new System.Drawing.Size(75, 19);
            this.chCalibracao.TabIndex = 4;
            // 
            // SBGerar
            // 
            this.SBGerar.Enabled = false;
            this.SBGerar.Location = new System.Drawing.Point(232, 12);
            this.SBGerar.Name = "SBGerar";
            this.SBGerar.Size = new System.Drawing.Size(190, 23);
            this.SBGerar.TabIndex = 3;
            this.SBGerar.Text = "Gerar (Modelo)";
            this.SBGerar.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 56);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(74, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Arquivo Modelo";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(102, 53);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(495, 20);
            this.textEdit1.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 12);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(190, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Selecionar Modelo";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 111);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1568, 490);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.Informações,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.pdfViewer1);
            this.xtraTabPage1.Controls.Add(this.splitterControl1);
            this.xtraTabPage1.Controls.Add(this.pdfViewer2);
            this.xtraTabPage1.Controls.Add(this.panelControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1530, 668);
            this.xtraTabPage1.Text = "PDF";
            // 
            // pdfViewer1
            // 
            this.pdfViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pdfViewer1.Location = new System.Drawing.Point(0, 0);
            this.pdfViewer1.Name = "pdfViewer1";
            this.pdfViewer1.Size = new System.Drawing.Size(681, 635);
            this.pdfViewer1.TabIndex = 1;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterControl1.Location = new System.Drawing.Point(681, 0);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 635);
            this.splitterControl1.TabIndex = 3;
            this.splitterControl1.TabStop = false;
            // 
            // pdfViewer2
            // 
            this.pdfViewer2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdfViewer2.Location = new System.Drawing.Point(686, 0);
            this.pdfViewer2.Name = "pdfViewer2";
            this.pdfViewer2.Size = new System.Drawing.Size(844, 635);
            this.pdfViewer2.TabIndex = 2;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.zoomTrackBarControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 635);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1530, 33);
            this.panelControl2.TabIndex = 0;
            // 
            // zoomTrackBarControl1
            // 
            this.zoomTrackBarControl1.EditValue = null;
            this.zoomTrackBarControl1.Location = new System.Drawing.Point(686, 10);
            this.zoomTrackBarControl1.Name = "zoomTrackBarControl1";
            this.zoomTrackBarControl1.Size = new System.Drawing.Size(844, 23);
            this.zoomTrackBarControl1.TabIndex = 0;
            this.zoomTrackBarControl1.ValueChanged += new System.EventHandler(this.zoomTrackBarControl1_ValueChanged);
            // 
            // Informações
            // 
            this.Informações.Controls.Add(this.memoEdit1);
            this.Informações.Controls.Add(this.splitContainerControl1);
            this.Informações.Controls.Add(this.splitterControl2);
            this.Informações.Controls.Add(this.memoEdit3);
            this.Informações.Controls.Add(this.panelControl3);
            this.Informações.Name = "Informações";
            this.Informações.Size = new System.Drawing.Size(1505, 351);
            this.Informações.Text = "Informações";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(0, 44);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(656, 307);
            this.memoEdit1.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Location = new System.Drawing.Point(313, 343);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(8, 8);
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitterControl2
            // 
            this.splitterControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterControl2.Location = new System.Drawing.Point(656, 44);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(5, 307);
            this.splitterControl2.TabIndex = 4;
            this.splitterControl2.TabStop = false;
            // 
            // memoEdit3
            // 
            this.memoEdit3.Dock = System.Windows.Forms.DockStyle.Right;
            this.memoEdit3.Location = new System.Drawing.Point(661, 44);
            this.memoEdit3.Name = "memoEdit3";
            this.memoEdit3.Size = new System.Drawing.Size(844, 307);
            this.memoEdit3.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton4);
            this.panelControl3.Controls.Add(this.simpleButton3);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1505, 44);
            this.panelControl3.TabIndex = 2;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(93, 15);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(89, 23);
            this.simpleButton4.TabIndex = 4;
            this.simpleButton4.Text = "Texto";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(12, 15);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 3;
            this.simpleButton3.Text = "Fontes";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl1);
            this.xtraTabPage2.Controls.Add(this.splitterControl3);
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Controls.Add(this.panelControl4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1562, 462);
            this.xtraTabPage2.Text = "Mapeamento XML";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.trocaBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 44);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEditDEL,
            this.repositoryItemButtonEditGera});
            this.gridControl1.Size = new System.Drawing.Size(1031, 418);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // trocaBindingSource
            // 
            this.trocaBindingSource.DataMember = "Troca";
            this.trocaBindingSource.DataSource = typeof(VirPDFModelo.dMapeamento);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colModelo,
            this.colFonte,
            this.colTamanhoFonte,
            this.colCentralizado,
            this.colValor,
            this.colNomeCampo,
            this.colAjusteFino,
            this.gridColumn1,
            this.gridColumn2});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colModelo
            // 
            this.colModelo.FieldName = "Modelo";
            this.colModelo.Name = "colModelo";
            this.colModelo.Visible = true;
            this.colModelo.VisibleIndex = 1;
            this.colModelo.Width = 118;
            // 
            // colFonte
            // 
            this.colFonte.FieldName = "Fonte";
            this.colFonte.Name = "colFonte";
            this.colFonte.Visible = true;
            this.colFonte.VisibleIndex = 2;
            this.colFonte.Width = 99;
            // 
            // colTamanhoFonte
            // 
            this.colTamanhoFonte.FieldName = "TamanhoFonte";
            this.colTamanhoFonte.Name = "colTamanhoFonte";
            this.colTamanhoFonte.Visible = true;
            this.colTamanhoFonte.VisibleIndex = 3;
            this.colTamanhoFonte.Width = 109;
            // 
            // colCentralizado
            // 
            this.colCentralizado.FieldName = "Centralizado";
            this.colCentralizado.Name = "colCentralizado";
            this.colCentralizado.Visible = true;
            this.colCentralizado.VisibleIndex = 4;
            this.colCentralizado.Width = 95;
            // 
            // colValor
            // 
            this.colValor.FieldName = "Valor";
            this.colValor.Name = "colValor";
            this.colValor.Visible = true;
            this.colValor.VisibleIndex = 6;
            this.colValor.Width = 168;
            // 
            // colNomeCampo
            // 
            this.colNomeCampo.Caption = "Campo";
            this.colNomeCampo.FieldName = "NomeCampo";
            this.colNomeCampo.Name = "colNomeCampo";
            this.colNomeCampo.Visible = true;
            this.colNomeCampo.VisibleIndex = 0;
            this.colNomeCampo.Width = 130;
            // 
            // colAjusteFino
            // 
            this.colAjusteFino.FieldName = "AjusteFino";
            this.colAjusteFino.Name = "colAjusteFino";
            this.colAjusteFino.Visible = true;
            this.colAjusteFino.VisibleIndex = 5;
            this.colAjusteFino.Width = 138;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEditDEL;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 30;
            // 
            // repositoryItemButtonEditDEL
            // 
            this.repositoryItemButtonEditDEL.AutoHeight = false;
            this.repositoryItemButtonEditDEL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonEditDEL.Name = "repositoryItemButtonEditDEL";
            this.repositoryItemButtonEditDEL.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditDEL.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditDEL_ButtonClick);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEditGera;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 8;
            this.gridColumn2.Width = 30;
            // 
            // repositoryItemButtonEditGera
            // 
            this.repositoryItemButtonEditGera.AutoHeight = false;
            this.repositoryItemButtonEditGera.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right)});
            this.repositoryItemButtonEditGera.Name = "repositoryItemButtonEditGera";
            this.repositoryItemButtonEditGera.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditGera.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditGera_ButtonClick);
            // 
            // splitterControl3
            // 
            this.splitterControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterControl3.Location = new System.Drawing.Point(1031, 44);
            this.splitterControl3.Name = "splitterControl3";
            this.splitterControl3.Size = new System.Drawing.Size(5, 418);
            this.splitterControl3.TabIndex = 3;
            this.splitterControl3.TabStop = false;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.gabaritoBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.gridControl2.Location = new System.Drawing.Point(1036, 44);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(526, 418);
            this.gridControl2.TabIndex = 2;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gabaritoBindingSource
            // 
            this.gabaritoBindingSource.DataMember = "Gabarito";
            this.gabaritoBindingSource.DataSource = typeof(VirPDFModelo.dGabarito);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNomeCampo1,
            this.colX,
            this.colY,
            this.colConteudo,
            this.colDelataLinha,
            this.colFonte1,
            this.colTamanhoFonte1,
            this.colCentralizado1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colNomeCampo1
            // 
            this.colNomeCampo1.FieldName = "NomeCampo";
            this.colNomeCampo1.Name = "colNomeCampo1";
            this.colNomeCampo1.Visible = true;
            this.colNomeCampo1.VisibleIndex = 0;
            this.colNomeCampo1.Width = 103;
            // 
            // colX
            // 
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.Visible = true;
            this.colX.VisibleIndex = 1;
            // 
            // colY
            // 
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.Visible = true;
            this.colY.VisibleIndex = 2;
            // 
            // colConteudo
            // 
            this.colConteudo.FieldName = "Conteudo";
            this.colConteudo.Name = "colConteudo";
            this.colConteudo.Visible = true;
            this.colConteudo.VisibleIndex = 3;
            this.colConteudo.Width = 86;
            // 
            // colDelataLinha
            // 
            this.colDelataLinha.FieldName = "DelataLinha";
            this.colDelataLinha.Name = "colDelataLinha";
            this.colDelataLinha.Visible = true;
            this.colDelataLinha.VisibleIndex = 4;
            this.colDelataLinha.Width = 98;
            // 
            // colFonte1
            // 
            this.colFonte1.FieldName = "Fonte";
            this.colFonte1.Name = "colFonte1";
            this.colFonte1.Visible = true;
            this.colFonte1.VisibleIndex = 5;
            this.colFonte1.Width = 93;
            // 
            // colTamanhoFonte1
            // 
            this.colTamanhoFonte1.FieldName = "TamanhoFonte";
            this.colTamanhoFonte1.Name = "colTamanhoFonte1";
            this.colTamanhoFonte1.Visible = true;
            this.colTamanhoFonte1.VisibleIndex = 6;
            this.colTamanhoFonte1.Width = 111;
            // 
            // colCentralizado1
            // 
            this.colCentralizado1.FieldName = "Centralizado";
            this.colCentralizado1.Name = "colCentralizado1";
            this.colCentralizado1.Visible = true;
            this.colCentralizado1.VisibleIndex = 7;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.simpleButton2);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1562, 44);
            this.panelControl4.TabIndex = 1;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(5, 15);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 4;
            this.simpleButton2.Text = "Salvar";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click_1);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "PDF|*.pdf";
            this.openFileDialog1.InitialDirectory = "C:\\FontesVS\\documentação\\NFS-e\\Modelos";
            // 
            // cEditorPDF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cEditorPDF";
            this.Size = new System.Drawing.Size(1568, 601);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chBranco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSobrepor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTMP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chPintar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCalibracao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).EndInit();
            this.Informações.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trocaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditDEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditGera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gabaritoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton SBGerar;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraPdfViewer.PdfViewer pdfViewer1;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraPdfViewer.PdfViewer pdfViewer2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.ZoomTrackBarControl zoomTrackBarControl1;
        private DevExpress.XtraTab.XtraTabPage Informações;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraEditors.MemoEdit memoEdit3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraEditors.CheckEdit chPintar;
        private DevExpress.XtraEditors.CheckEdit chCalibracao;
        private DevExpress.XtraEditors.TextEdit textEditTMP;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.BindingSource trocaBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colModelo;
        private DevExpress.XtraGrid.Columns.GridColumn colFonte;
        private DevExpress.XtraGrid.Columns.GridColumn colTamanhoFonte;
        private DevExpress.XtraGrid.Columns.GridColumn colCentralizado;
        private DevExpress.XtraGrid.Columns.GridColumn colValor;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeCampo;
        private DevExpress.XtraGrid.Columns.GridColumn colAjusteFino;
        private DevExpress.XtraEditors.CheckEdit chSobrepor;
        private DevExpress.XtraEditors.CheckEdit chBranco;
        private DevExpress.XtraEditors.SplitterControl splitterControl3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource gabaritoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeCampo1;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colConteudo;
        private DevExpress.XtraGrid.Columns.GridColumn colDelataLinha;
        private DevExpress.XtraGrid.Columns.GridColumn colFonte1;
        private DevExpress.XtraGrid.Columns.GridColumn colTamanhoFonte1;
        private DevExpress.XtraGrid.Columns.GridColumn colCentralizado1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditDEL;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditGera;
    }
}
