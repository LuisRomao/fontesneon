﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VirPDFModelo;
using System.IO;

namespace Virtual.Componentes
{
    /// <summary>
    /// Gerado dos modelos de PDF
    /// </summary>
    public partial class cEditorPDF : CompontesBasicos.ComponenteBase
    {
        private dMapeamento DMapeamento;
        private dGabarito DGabarito;
        private string NomeArquivoMapeamento;
        private string NomeArquivoMapeamentoConf;

        /// <summary>
        /// Construtor
        /// </summary>
        public cEditorPDF()
        {
            InitializeComponent();
            Gerador1 = new Gerador();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textEdit1.Text = openFileDialog1.FileName;
                pdfViewer1.DocumentFilePath = openFileDialog1.FileName;
                SBGerar.Enabled = true;
                simpleButton5.Enabled = true;
                NomeArquivoMapeamento = Path.ChangeExtension(textEdit1.Text,"xml");
                NomeArquivoMapeamentoConf = NomeArquivoMapeamento.Replace(".xml", "conf.xml");
                DMapeamento = new dMapeamento();
                DGabarito = new dGabarito();
                if (File.Exists(NomeArquivoMapeamento))
                    try
                    {
                        DGabarito.ReadXml(NomeArquivoMapeamento);                        
                    }
                    catch
                    {
                        MessageBox.Show("Arquivo XML inválido ignorado","ERRO",MessageBoxButtons.OK);
                    }
                if (File.Exists(NomeArquivoMapeamentoConf))
                    try
                    {                        
                        DMapeamento.ReadXml(NomeArquivoMapeamentoConf);
                    }
                    catch
                    {
                        MessageBox.Show("Arquivo XML inválido ignorado", "ERRO", MessageBoxButtons.OK);
                    }
                trocaBindingSource.DataSource = DMapeamento;
                gabaritoBindingSource.DataSource = DGabarito;
            }
        }

        private Gerador Gerador1;

        private void simpleButton2_Click(object sender, EventArgs e)
        {            
            Gerador1.Calibracao = chCalibracao.Checked;
            Gerador1.PintarDeVerde = chPintar.Checked;
            Gerador1.Sobrepor = chSobrepor.Checked;
            Gerador1.Branco = chBranco.Checked;
            pdfViewer2.CloseDocument();
            Gerador1.GeraPDF(textEdit1.Text,textEditTMP.Text,DMapeamento);
            pdfViewer2.DocumentFilePath = textEditTMP.Text;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = Gerador1.ListaFontes(textEdit1.Text);
            memoEdit3.Text = Gerador1.ListaFontes(textEditTMP.Text);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = Gerador1.ConteudoPDF(textEdit1.Text).Replace("\n", "\r\n");
            memoEdit3.Text = Gerador1.ConteudoPDF(textEditTMP.Text).Replace("\n", "\r\n");
        }

        private void zoomTrackBarControl1_ValueChanged(object sender, EventArgs e)
        {
            pdfViewer1.ZoomFactor = zoomTrackBarControl1.Value * 100;
            pdfViewer2.ZoomFactor = zoomTrackBarControl1.Value * 100;
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            DMapeamento.WriteXml(NomeArquivoMapeamento);
            DGabarito.WriteXml(NomeArquivoMapeamento);
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            Gerador1.Calibracao = chCalibracao.Checked;
            Gerador1.PintarDeVerde = chPintar.Checked;
            Gerador1.Sobrepor = chSobrepor.Checked;
            Gerador1.Branco = chBranco.Checked;
            pdfViewer2.CloseDocument();
            Gerador1.GeraPDF(textEdit1.Text, textEditTMP.Text, DGabarito);
            pdfViewer2.DocumentFilePath = textEditTMP.Text;
        }

        private void repositoryItemButtonEditGera_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dMapeamento.TrocaRow row = (dMapeamento.TrocaRow)gridView1.GetFocusedDataRow();
            if (row != null)
            {
                List<iTextSharp.text.Rectangle> Ocorrencias = Gerador1.BuscaOcorrencias(textEdit1.Text, row.Modelo);
                foreach (iTextSharp.text.Rectangle rect in Ocorrencias)
                {
                    if (float.IsNaN(rect.Left) || float.IsNaN(rect.Bottom))
                        continue;
                    dGabarito.GabaritoRow Nova = DGabarito.Gabarito.NewGabaritoRow();
                    string NomeCampoNovo = row.NomeCampo;
                    for (int X = 1; DGabarito.Gabarito.FindByNomeCampo(NomeCampoNovo) != null; X++)
                        NomeCampoNovo = string.Format("{0}{1}",row.NomeCampo,X);
                    Nova.NomeCampo = NomeCampoNovo;
                    Nova.Fonte = row.Fonte;
                    Nova.DelataLinha = (decimal)rect.Height;
                    Nova.Conteudo = row.Modelo;
                    Nova.Centralizado = row.Centralizado;
                    Nova.TamanhoFonte = row.TamanhoFonte;
                    Nova.X = (decimal)(row.Centralizado ? (rect.Left + rect.Right) / 2 : rect.Left);
                    Nova.Y = (decimal)rect.Bottom + row.AjusteFino;
                    DGabarito.Gabarito.AddGabaritoRow(Nova);
                }
            }
        }

        private void repositoryItemButtonEditDEL_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dMapeamento.TrocaRow row = (dMapeamento.TrocaRow)gridView1.GetFocusedDataRow();
            if (row != null)
                row.Delete();
        }
    }
}
