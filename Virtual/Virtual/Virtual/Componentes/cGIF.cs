﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;



namespace Virtual.Componentes
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cGIF : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cGIF()
        {
            InitializeComponent();
        }

        private Image _gifImage;

        private ColorPalette cp;

        private bool showTrans;

        private int CurrentEntry;

        private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {

            if (cp == null)

                return;



            for (float y = 0; y < 16; y++)

                for (float x = 0; x < 16; x++)
                {

                    Color c = Color.Black;

                    if (((16 * y) + x) < cp.Entries.Length)

                        c = cp.Entries[(int)((16 * y) + x)];

                    SolidBrush sb = new SolidBrush(Color.FromArgb(255, c));

                    float w = ((float)this.panel1.Width) / 16;

                    float h = ((float)this.panel1.Height) / 16;

                    e.Graphics.FillRectangle(sb, w * x, h * y, w, h);

                    if (c.A != 255)
                    {

                        if (showTrans)

                            e.Graphics.DrawRectangle(Pens.Black, w * x, h * y, w - 1, h - 1);

                        else

                            e.Graphics.DrawRectangle(Pens.White, w * x, h * y, w - 1, h - 1);

                    }



                    sb.Dispose();

                }

        }

        private void panel1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {

            int y = (int)(((float)e.Y) / (((float)this.panel1.Width) / 16f));

            int x = (int)(((float)e.X) / (((float)this.panel1.Height) / 16f));

            CurrentEntry = (int)((16 * y) + x);

            if (cp != null)
            {

                if (CurrentEntry >= cp.Entries.Length)

                    CurrentEntry = cp.Entries.Length - 1;

                //Little bit of diagnostic for the palette chooser below 

                //System.Diagnostics.Trace.WriteLine(string.Format("{0},{1}, adjusted={4},{5} entry={2} Colour={3}",e.X,e.Y,CurrentEntry,cp.Entries[CurrentEntry].ToString(),x,y)); 

            }

        }

        private void panel1_Click(object sender, System.EventArgs e)
        {

            //Creates a new GIF image with a modified colour palette 

            if (cp != null)
            {

                //Create a new 8 bit per pixel image 

                Bitmap bm = new Bitmap(_gifImage.Width, _gifImage.Height, PixelFormat.Format8bppIndexed);

                //get it's palette 

                ColorPalette ncp = bm.Palette;



                //copy all the entries from the old palette removing any transparency 

                int n = 0;

                foreach (Color c in cp.Entries)

                    ncp.Entries[n++] = Color.FromArgb(255, c);



                //Set the newly selected transparency 

                ncp.Entries[CurrentEntry] = Color.FromArgb(0, cp.Entries[CurrentEntry]);

                //re-insert the palette 

                bm.Palette = ncp;



                //now to copy the actual bitmap data 

                //lock the source and destination bits 

                BitmapData src = ((Bitmap)_gifImage).LockBits(new Rectangle(0, 0, _gifImage.Width, _gifImage.Height), ImageLockMode.ReadOnly, _gifImage.PixelFormat);

                BitmapData dst = bm.LockBits(new Rectangle(0, 0, bm.Width, bm.Height), ImageLockMode.WriteOnly, bm.PixelFormat);



                //uses pointers so we need unsafe code. 

                //the project is also compiled with /unsafe 

#if (DEBUG)
#else           

                unsafe
                {

                    //steps through each pixel 

                    for (int y = 0; y < _gifImage.Height; y++)

                        for (int x = 0; x < _gifImage.Width; x++)
                        {

                            //transferring the bytes 

                            ((byte*)dst.Scan0.ToPointer())[(dst.Stride * y) + x] = ((byte*)src.Scan0.ToPointer())[(src.Stride * y) + x];

                        }

                }
#endif


                //all done, unlock the bitmaps 

                ((Bitmap)_gifImage).UnlockBits(src);

                bm.UnlockBits(dst);



                //clear out the picturebox 

                this.pictureBox1.Image = null;

                _gifImage.Dispose();

                //set the new image in place 

                _gifImage = bm;

                cp = _gifImage.Palette;

                this.pictureBox1.Image = _gifImage;

            }

        }

        private string nomeArq;

        private void button1_Click(object sender, System.EventArgs e)
        {

            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Filter = "GIF files|*.GIF";

            if (dlg.ShowDialog() == DialogResult.OK)
            {

                _gifImage = Image.FromFile(dlg.FileName);

                nomeArq = dlg.FileName;

                this.pictureBox1.Image = _gifImage;

                cp = _gifImage.Palette;

                this.panel1.Invalidate();

            }

        }

        private void button2_Click(object sender, System.EventArgs e)
        {

            SaveFileDialog dlg = new SaveFileDialog();

            dlg.Filter = "GIF files|*.gif";

            dlg.DefaultExt = ".gif";

            dlg.AddExtension = true;

            dlg.FileName = nomeArq;

            if (dlg.ShowDialog() == DialogResult.OK)
            {

                _gifImage.Save(dlg.FileName, ImageFormat.Gif);

            }

        }
        
        private void timer1_Tick(object sender, System.EventArgs e)
        {

            showTrans ^= true;

            Graphics g = this.panel1.CreateGraphics();

            //I do this rather than invalidate the panel because 

            //the panel draws its background ans so flickers horribly. 

            PaintEventArgs pe = new PaintEventArgs(g, new Rectangle(0, 0, this.panel1.Width, this.panel1.Height));

            this.panel1_Paint(this, pe);

            g.Dispose();

        }
        
        private string PastaSaida;

        private bool Parar = false;

        private void button3_Click(object sender, EventArgs e)
        {
            Parar = false;
            OpenFileDialog Origem = new OpenFileDialog();
            if (Origem.ShowDialog() == DialogResult.OK)
            {
                int NovoTamanho = 500;
                if (!VirInput.Input.Execute("Novo Tamanho", out NovoTamanho))
                    return;
                string Pasta = System.IO.Path.GetDirectoryName(Origem.FileName);
                PastaSaida = string.Format("{0}\\JPG",Pasta);
                int i = 1;
                while(Directory.Exists(PastaSaida))
                    PastaSaida = string.Format("{0}\\JPG{1}",Pasta,i++);
                Directory.CreateDirectory(PastaSaida);
                foreach (string arquivo in Directory.GetFiles(Pasta))
                {
                    if (Path.GetExtension(arquivo).ToUpper() != ".JPG")
                        continue;
                    string arquivoNovo = string.Format("{0}\\{1}",PastaSaida,Path.GetFileName(arquivo));
                    ResizeImageToFile(arquivo, arquivoNovo, NovoTamanho, null, false);
                    Application.DoEvents();
                    if (Parar)
                        return;
                }
                    
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <param name="Destino"></param>
        /// <param name="W"></param>
        /// <param name="H"></param>
        /// <param name="ampliar"></param>
        /// <param name="Diminuir"></param>
        /// <param name="preserveAspectRatio"></param>
        /// <returns></returns>
        public static bool ResizeImageToFile(string Arquivo, string Destino, int? W, int? H, bool ampliar = true, bool Diminuir = true, bool preserveAspectRatio = true)
        {
            try
            {
                using (System.Drawing.Image original = System.Drawing.Image.FromFile(Arquivo))
                {
                    System.Drawing.Image resized = ResizeImage(original, W, H, ampliar, Diminuir, preserveAspectRatio);
                    MemoryStream memStream = new MemoryStream();
                    resized.Save(Destino, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <param name="W"></param>
        /// <param name="H"></param>
        /// <param name="ampliar"></param>
        /// <param name="Diminuir"></param>
        /// <param name="preserveAspectRatio"></param>
        /// <returns></returns>
        public static MemoryStream ResizeImageStream(string Arquivo, int? W, int? H, bool ampliar = true, bool Diminuir = true, bool preserveAspectRatio = true)
        {
            using (System.Drawing.Image original = System.Drawing.Image.FromFile(Arquivo))
            {
                System.Drawing.Image resized = ResizeImage(original, W, H, ampliar, Diminuir, preserveAspectRatio);
                MemoryStream memStream = new MemoryStream();
                resized.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                return memStream;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="W"></param>
        /// <param name="H"></param>
        /// <param name="ampliar"></param>
        /// <param name="Diminuir"></param>
        /// <param name="preserveAspectRatio"></param>
        /// <returns></returns>
        public static System.Drawing.Image ResizeImage(System.Drawing.Image image, int? W, int? H, bool ampliar = true, bool Diminuir = true, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)W.GetValueOrDefault(1000000) / (float)originalWidth;
                float percentHeight = (float)H.GetValueOrDefault(1000000) / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
                if ((newWidth < originalWidth) && !Diminuir)
                    return image;
                if ((newWidth > originalWidth) && !ampliar)
                    return image;
            }
            else
            {
                newWidth = W.Value;
                newHeight = H.Value;
            }
            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Parar = true;
        }

    }
}
