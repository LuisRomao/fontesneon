using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Virtual.Componentes
{
    /// <summary>
    /// Componente de tradu��o
    /// </summary>
    public partial class cTradutor : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public cTradutor()
        {
            InitializeComponent();
        }

        /*
        private dllCheques.dImpNotaDebTableAdapters.EscritorioTableAdapter escritorioTableAdapter;
        
        /// <summary>
        /// TableAdapter para tabela Escritorio
        /// </summary>
        public dllCheques.dImpNotaDebTableAdapters.EscritorioTableAdapter EscritorioTableAdapter { 
            get{
                if (escritorioTableAdapter == null) {
                    escritorioTableAdapter = new dllCheques.dImpNotaDebTableAdapters.EscritorioTableAdapter();
                    escritorioTableAdapter.TrocarStringDeConexao();                    
                }
                return escritorioTableAdapter;
            }
        }

        */

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            memoEdit2.Text = "";
            foreach (string linha in memoEdit1.Lines)
            {
                if (memoEdit2.Text != "")
                    memoEdit2.Text += @"\r\n" + "\" + \r\n";                
                memoEdit2.Text += "\"" + linha;
            };
            memoEdit2.Text += ";\";";
            Clipboard.SetDataObject(memoEdit2.Text);
        }

        private void cTradutor_Enter(object sender, EventArgs e)
        {
            IDataObject iData = Clipboard.GetDataObject();
            if (iData.GetDataPresent(DataFormats.Text))
                memoEdit2.Text = memoEdit1.Text = (String)iData.GetData(DataFormats.Text);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = "";
            foreach (string linha in memoEdit2.Lines)
            {
                if (memoEdit1.Text != "")
                    memoEdit1.Text += "\r\n";
                string linha1 = linha;
                int PosAs = linha1.LastIndexOf("\"");                
                if(PosAs >= 0)
                    linha1 = linha.Remove(PosAs);
                linha1 = linha1.Substring(linha.IndexOf("\"")+1).Trim();
                while(linha1.IndexOf("  ") >= 0)
                    linha1 = linha1.Replace("  ", " ");
                linha1 = linha1.Replace(@"\r\n", "");
                if(linha1 != "")
                    memoEdit1.Text += linha1;
            };            
            Clipboard.SetDataObject(memoEdit1.Text);
        }

        private string Entrada;

        private bool PegaEntrada() {
            IDataObject iData = Clipboard.GetDataObject();
            if (!iData.GetDataPresent(DataFormats.Text))
                return false;
            Entrada = (String)iData.GetData(DataFormats.Text);
            return Entrada != "";
        }

        private bool PegaEntradaImput(string Pergunta)
        {
            string sujestao = "";
            IDataObject iData = Clipboard.GetDataObject();
            if (iData.GetDataPresent(DataFormats.Text))
                sujestao = (String)iData.GetData(DataFormats.Text);
            if (VirInput.Input.Execute(Pergunta, ref sujestao, false, 0))
            {
                Entrada = sujestao;
                return Entrada != "";
            }
            else
                return false;
        }
        

        private void TradutoDatasetEstatico()
        {
            if (!PegaEntrada())
                return;
            string modedo =
"        private static $entrada$ _$entrada$St;\r\n" +
"\r\n" +
"        /// <summary>\r\n" +
"        /// dataset est�tico:$entrada$\r\n" +
"        /// </summary>\r\n" +
"        public static $entrada$ $entrada$St {\r\n" +
"            get {\r\n" +
"                if (_$entrada$St == null)\r\n" +
"                    _$entrada$St = new $entrada$();\r\n" +
"                return _$entrada$St;\r\n" +
"            }\r\n" +
"        }";
            string saida = modedo.Replace("$entrada$", Entrada);
            Clipboard.SetDataObject(saida);
            memoEdit2.Text = saida;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            TradutoDatasetEstatico();
        }

        private void GeraCodigoClasse()
        {
            if (!PegaEntradaImput("Nome da classe"))
                return;
            string NomeClasse = Entrada;
            string NomeDataset = string.Format("d{0}",Entrada);
            if (!VirInput.Input.Execute("nome dataset", ref NomeDataset, false, 0))                
                return;
            string NomeID = NomeClasse.Substring(0, 3);
            if (!VirInput.Input.Execute("Campo ID", ref NomeID, false, 0))
                return;
            string NomeDatasettraco = string.Format("_{0}{1}",NomeDataset.Substring(0,1).ToLower(),NomeDataset.Substring(1));
            string NomeDatasetmini = string.Format("{0}{1}", NomeDataset.Substring(0, 1).ToLower(), NomeDataset.Substring(1));
            string NomeDatasetmai = string.Format("{0}{1}", NomeDataset.Substring(0, 1).ToUpper(), NomeDataset.Substring(1));
            string NomeDatasetPuro = string.Format("{0}", NomeDataset.Substring(1));
            string NomeTabela = "";
            int k = 0;
            for (int i = 0; i < NomeDatasetPuro.Length; i++)
                if ((k < 3) && (NomeID.Substring(k, 1) == NomeDatasetPuro.Substring(i, 1).ToUpper()))
                {
                    NomeTabela += NomeDatasetPuro.Substring(i, 1).ToUpper();
                    k++;
                }
                else
                    NomeTabela += NomeDatasetPuro.Substring(i, 1).ToLower();
            StringBuilder modedo = new StringBuilder();
            modedo.AppendFormat("    /// <summary>\r\n");
            modedo.AppendFormat("    /// \r\n");
            modedo.AppendFormat("    /// </summary>\r\n");
            modedo.AppendFormat("    public class {0} : IEMPTProc\r\n",NomeClasse);
            modedo.AppendFormat("    {{\r\n");
            modedo.AppendFormat("            private EMPTProc _EMPTProc1;\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("            /// <summary>\r\n");
            modedo.AppendFormat("            /// Tipo de EMP: local ou filial\r\n");
            modedo.AppendFormat("            /// </summary>\r\n");
            modedo.AppendFormat("            public EMPTProc EMPTProc1\r\n");
            modedo.AppendFormat("            {{\r\n");
            modedo.AppendFormat("                get\r\n");
            modedo.AppendFormat("                {{\r\n");
            modedo.AppendFormat("                    if (_EMPTProc1 == null)\r\n");
            modedo.AppendFormat("                    {{\r\n");
            modedo.AppendFormat("                        if (TableAdapter.Servidor_De_Processos)\r\n");
            modedo.AppendFormat("                            throw new Exception(\"uso de construtor sem EMPTProc em Servidor de Processos\");\r\n");
            modedo.AppendFormat("                        else\r\n");
            modedo.AppendFormat("                            _EMPTProc1 = new EMPTProc(EMPTipo.Local);\r\n");
            modedo.AppendFormat("                    }}\r\n");
            modedo.AppendFormat("                    return _EMPTProc1;\r\n");
            modedo.AppendFormat("                }}\r\n");
            modedo.AppendFormat("                set => _EMPTProc1 = value;\r\n");
            modedo.AppendFormat("            }}\r\n");
            modedo.AppendFormat("\r\n");                         
            modedo.AppendFormat("        private {0} {1};\r\n",NomeDatasetmini,NomeDatasettraco);
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// \r\n");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        public {0} {1}\r\n",NomeDatasetmini,NomeDatasetmai);
            modedo.AppendFormat("        {{\r\n");
            modedo.AppendFormat("            get\r\n");
            modedo.AppendFormat("            {{\r\n");
            modedo.AppendFormat("                if ({0} == null)\r\n", NomeDatasettraco);
            modedo.AppendFormat("                {{\r\n");
            modedo.AppendFormat("                    if (linhaMae == null)\r\n");
            modedo.AppendFormat("                        {0} = new {1}(EMPTProc1);\r\n", NomeDatasettraco,NomeDataset);
            modedo.AppendFormat("                    else\r\n");
            modedo.AppendFormat("                        {0} = ({1})linhaMae.Table.DataSet;\r\n", NomeDatasettraco,NomeDataset);
            modedo.AppendFormat("                }}\r\n");
            modedo.AppendFormat("                return {0};\r\n", NomeDatasettraco);
            modedo.AppendFormat("            }}\r\n");
            modedo.AppendFormat("        }}\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// Linha m�e\r\n");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        protected {0}.{1}Row linhaMae;\r\n",NomeDatasetmini, NomeTabela);
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// Linha m�e\r\n");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        public {0}.{1}Row LinhaMae\r\n", NomeDatasetmini, NomeTabela);
            modedo.AppendFormat("        {{\r\n");
            modedo.AppendFormat("            get {{ return linhaMae; }}\r\n");
            modedo.AppendFormat("        }}\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("        #region Construtores\r\n");
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// Construtor\r\n");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        /// <param name=\"_EMPTProc\"></param>\r\n");
            modedo.AppendFormat("        public {0}(EMPTProc _EMPTProc = null)\r\n",NomeClasse);
            modedo.AppendFormat("        {{\r\n");
            modedo.AppendFormat("            if (_EMPTProc != null)\r\n");
            modedo.AppendFormat("                EMPTProc1 = _EMPTProc;\r\n");
            modedo.AppendFormat("        }}\r\n");
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// Construtor para uma nota j� existente. verifique depois se \"encotrada\" est� como true\r\n");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        /// <param name=\"NOA\"></param>\r\n");
            modedo.AppendFormat("        /// <param name=\"_EMPTProc1\"></param>\r\n");
            modedo.AppendFormat("        public {0}(int {1}, EMPTProc _EMPTProc1 = null)\r\n",NomeClasse,NomeID);
            modedo.AppendFormat("            : this(_EMPTProc1)\r\n");
            modedo.AppendFormat("        {{\r\n");
            modedo.AppendFormat("            EMPTProc1.EmbarcaEmTrans({0}.{1}TableAdapter);\r\n",NomeDatasetmai, NomeTabela);
            modedo.AppendFormat("            if ({0}.{1}TableAdapter.FillBy{2}({0}.{1}, {2}) > 0)\r\n",NomeDatasetmai, NomeTabela, NomeID);
            modedo.AppendFormat("                linhaMae = {0}.{1}[0];\r\n", NomeDatasetmai, NomeTabela);
            modedo.AppendFormat("        }}\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// Construtor\r\n");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        /// <param name=\"_LinhaMae\"></param>\r\n");
            modedo.AppendFormat("        /// <param name=\"_EMPTProc1\"></param>\r\n");
            modedo.AppendFormat("        public {0}({1}.{2}Row _LinhaMae, EMPTProc _EMPTProc1)\r\n",NomeClasse, NomeDatasetmini, NomeTabela);
            modedo.AppendFormat("            : this(_EMPTProc1)\r\n");
            modedo.AppendFormat("        {{\r\n");
            modedo.AppendFormat("            linhaMae = _LinhaMae;\r\n");
            modedo.AppendFormat("        }}\r\n");
            modedo.AppendFormat("        #endregion\r\n");            
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// ID\r\n");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        public int {0} {{ get => LinhaMae.{0}; }}\r\n", NomeID);            
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("    }}\r\n");

            Clipboard.SetDataObject(modedo.ToString());
            memoEdit2.Text = modedo.ToString();
            MessageBox.Show("Cole substituindo a classe toda");
        }

        private void GeraCodigoParaDS()
        {
            if (!PegaEntradaImput("Nome do dataset"))
                return;
            StringBuilder modedo = new StringBuilder();
            modedo.AppendFormat("partial class {0} : IEMPTProc\r\n",Entrada);
            modedo.AppendFormat("    {{\r\n");
            modedo.AppendFormat("        private EMPTProc _EMPTProc1;\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// Tipo de EMP: local ou filial\r\n");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        public EMPTProc EMPTProc1\r\n");
            modedo.AppendFormat("        {{\r\n");
            modedo.AppendFormat("            get\r\n");
            modedo.AppendFormat("            {{\r\n");
            modedo.AppendFormat("                if (_EMPTProc1 == null)\r\n");
            modedo.AppendFormat("                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);\r\n");
            modedo.AppendFormat("                return _EMPTProc1;\r\n");
            modedo.AppendFormat("            }}\r\n");
            modedo.AppendFormat("            set\r\n");
            modedo.AppendFormat("            {{\r\n");
            modedo.AppendFormat("                _EMPTProc1 = value;\r\n");
            modedo.AppendFormat("            }}\r\n");
            modedo.AppendFormat("        }}\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("            /// <summary>\r\n");
            modedo.AppendFormat("            /// Construtor com EMPTipo\r\n");
            modedo.AppendFormat("            /// </summary>\r\n");
            modedo.AppendFormat("            /// <param name=\"_EMPTProc1\"></param>\r\n");
            modedo.AppendFormat("            public {0}(EMPTProc _EMPTProc1)\r\n",Entrada);
            modedo.AppendFormat("            : this()\r\n");
            modedo.AppendFormat("            {{\r\n");
            modedo.AppendFormat("                if (_EMPTProc1 == null)\r\n");
            modedo.AppendFormat("                {{\r\n");
            modedo.AppendFormat("                    if (VirMSSQL.TableAdapter.Servidor_De_Processos)\r\n");
            modedo.AppendFormat("                        throw new Exception(\"uso de construtor sem EMPTProc em Servidor de Processos\");\r\n");
            modedo.AppendFormat("                    EMPTProc1 = new EMPTProc(EMPTipo.Local);\r\n");
            modedo.AppendFormat("                }}\r\n");
            modedo.AppendFormat("                else\r\n");
            modedo.AppendFormat("                    EMPTProc1 = _EMPTProc1;\r\n");
            modedo.AppendFormat("            }}\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("            #region Adapters\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("            #endregion\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("    }}\r\n");
            Clipboard.SetDataObject(modedo.ToString());
            memoEdit2.Text = modedo.ToString();
            MessageBox.Show("Cole substituindo a classe toda");
        }

        private void TradutorTableAdapterPrincipal(bool Filial = false) {
            if (!PegaEntradaImput("Nome do table adapter"))
                return;                        
            string NomePublico;            
            string NomePrivado;
            string NomeTabela;
            NomePublico = NomePrivado = Entrada.Substring(Entrada.IndexOf('.') + 1);
            if (NomePublico == "")
                return;
            NomePublico = char.ToUpper(NomePublico[0]) + NomePublico.Substring(1);
            NomePrivado = char.ToLower(NomePublico[0]) + NomePublico.Substring(1);
            NomeTabela = NomePublico.Substring(0, NomePublico.IndexOf("TableAdapter"));            
            StringBuilder modedo = new StringBuilder();
            modedo.AppendFormat("        private {0} {1}{2};\r\n", Entrada, NomePrivado,Filial?"F":"");
            modedo.AppendFormat("        /// <summary>\r\n");
            modedo.AppendFormat("        /// TableAdapter padr�o para tabela: {0}{1}\r\n", NomeTabela, Filial ? " (Filial)" : "");
            modedo.AppendFormat("        /// </summary>\r\n");
            modedo.AppendFormat("        public {0} {1}{2} {{\r\n", Entrada, NomePublico, Filial ? "F" : "");
            modedo.AppendFormat("            get {{\r\n");
            modedo.AppendFormat("                if ({0}{1} == null) {{\r\n", NomePrivado,Filial?"F":"");
            modedo.AppendFormat("                    {0} = new {1}();\r\n", NomePrivado,Entrada);
            modedo.AppendFormat("                    {0}{1}.TrocarStringDeConexao({2});\r\n", NomePrivado, Filial ? "F" : "", Filial ? "VirDB.Bancovirtual.TiposDeBanco.Filial" : "EMPTProc1.TBanco");
            modedo.AppendFormat("                }};\r\n");
            modedo.AppendFormat("                return {0}{1};\r\n", NomePrivado, Filial ? "F" : "");
            modedo.AppendFormat("            }}\r\n");
            modedo.AppendFormat("        }}");
            if (Filial)
            {
                modedo.AppendFormat("\r\n");
                modedo.AppendFormat("        /// <summary>\r\n");
                modedo.AppendFormat("        /// TableAdapter padr�o para tabela: {0} (Local/Filial)\r\n", NomeTabela);
                modedo.AppendFormat("        /// </summary>\r\n");
                modedo.AppendFormat("        public {0} {1}X(Framework.Enumeracoes.EMPTipo EMPTipo)\r\n",Entrada,NomePublico);
                modedo.AppendFormat("        {{\r\n");
                modedo.AppendFormat("            return EMPTipo == Framework.Enumeracoes.EMPTipo.Local ? {0} : {0}F;\r\n",NomePublico);
                modedo.AppendFormat("        }}\r\n");
            }     
            Clipboard.SetDataObject(modedo.ToString());
            memoEdit2.Text = modedo.ToString();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            TradutorTableAdapterPrincipal();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            TradutorTabelaBase();
        }

        private void TradutorTabelaBase()
        {
            if (!PegaEntrada())
                return;
            string modedo =
"        private static $entrada$ _$entrada$St;\r\n" +
"\r\n" +
"        /// <summary>\r\n" +
"        /// dataset est�tico:$entrada$\r\n" +
"        /// </summary>\r\n" +
"        public static $entrada$ $entrada$St\r\n" +
"        {\r\n" +
"            get\r\n" +
"            {\r\n" +
"                if (_$entrada$St == null)\r\n" +
"                {\r\n" +
"                    _$entrada$St = new $entrada$();\r\n" +
"                    _$entrada$St.$pub$TableAdapter.Fill(_$entrada$St.$pub$);\r\n" +
"                }\r\n" +
"                return _$entrada$St;\r\n" +
"            }\r\n" +
"        }\r\n" +
"\r\n" +
"        private $entrada$TableAdapters.$pub$TableAdapter $pri$TableAdapter;\r\n" +
"        /// <summary>\r\n" +
"        /// TableAdapter padr�o para tabela: $pub$\r\n" +
"        /// </summary>\r\n" +
"        public $entrada$TableAdapters.$pub$TableAdapter $pub$TableAdapter\r\n" +
"        {\r\n" +
"            get\r\n" +
"            {\r\n" +
"                if ($pri$TableAdapter == null)\r\n" +
"                {\r\n" +
"                    $pri$TableAdapter = new $entrada$TableAdapters.$pub$TableAdapter();\r\n" +
"                    $pri$TableAdapter.TrocarStringDeConexao();\r\n" +
"                };\r\n" +
"                return $pri$TableAdapter;\r\n" +
"            }\r\n" +
"        }";
            string NomePublico = Entrada.Substring(1);
            string NomePrivado;
            //string NomeTabela;
            //NomePublico = NomePrivado = Entrada.Substring(Entrada.IndexOf('.') + 1);
            if (NomePublico == "")
                return;
            NomePublico = char.ToUpper(NomePublico[0]) + NomePublico.Substring(1);
            NomePrivado = char.ToLower(NomePublico[0]) + NomePublico.Substring(1);
            //NomeTabela = NomePublico.Substring(0, NomePublico.IndexOf("TableAdapter"));
            string saida = modedo.Replace("$entrada$", Entrada);
            saida = saida.Replace("$pri$", NomePrivado);
            saida = saida.Replace("$pub$", NomePublico);
            //saida = saida.Replace("$Tabela$", NomeTabela);
            Clipboard.SetDataObject(saida);
            memoEdit2.Text = saida;
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            TradutorClasseInteligente();
        }

        private void TradutorClasseInteligente()
        {
            if (!PegaEntrada())
                return;
            string modedo =
"    /// <summary>\r\n" +
"    /// Classe de inteligencia para $entrada$\r\n" +
"    /// </summary>\r\n" +
"    public class $entrada$\r\n" +
"    {\r\n" +
"        private static $entrada$ _$entrada$St;\r\n" +
"\r\n" +
"        /// <summary>\r\n" +
"        /// Instancia est�tica de $entrada$\r\n" +
"        /// </summary>\r\n" +
"        public static $entrada$ $entrada$St {\r\n" +
"            get {\r\n" +
"                if (_$entrada$St == null)\r\n" +
"                    _$entrada$St = new $entrada$();\r\n" +
"                return _$entrada$St;\r\n" +
"            }\r\n" +
"        }\r\n" +
"    }";                  
            string saida = modedo.Replace("$entrada$", Entrada);            
            Clipboard.SetDataObject(saida);
            memoEdit2.Text = saida;
        }

        private bool GeraCodigoDataRowView()
        {
            if (!PegaEntrada())
                return false;
            
            string NomeTabela = Entrada;
            string Camposstr = "";
            if (Entrada.Contains(" "))
            {
                Camposstr = Entrada.Substring(Entrada.IndexOf(' ') + 1);
                NomeTabela = Entrada.Substring(0, Entrada.IndexOf(' '));
            }
            else
            {
                MessageBox.Show("Colocar na �rea de tranfer�ncia:<Tabela int XXXX, string YYYY, string ZZZZ>");
            }
            if ((Camposstr == "") && !VirInput.Input.Execute("Campos conforme exemplo <int XXXX, string YYYY, string ZZZZ>", ref Camposstr))
                return false;
            string[] Campos = Camposstr.Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);
            string camp1 = "";
            string camp2 = "";
            for (int i = 0; i < Campos.Length; i++)
                if (i % 2 == 1)
                {
                    camp1 += string.Format("_{0}", Campos[i].Replace(",", ""));
                    camp2 += string.Format("{0}{1}",(camp2 == "" ? "" : ",") , Campos[i].Replace(",", ""));
                }
            
            
            StringBuilder modedo = new StringBuilder();
            modedo.AppendFormat("        private DataView dV_{0}{1};\r\n", NomeTabela, camp1);
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("        private DataView DV_{0}{1}\r\n", NomeTabela, camp1);
            modedo.AppendFormat("        {{\r\n");
            modedo.AppendFormat("            get\r\n");
            modedo.AppendFormat("            {{\r\n");
            modedo.AppendFormat("                if (dV_{0}{1} == null)\r\n", NomeTabela, camp1);
            modedo.AppendFormat("                {{\r\n");
            modedo.AppendFormat("                    dV_{0}{1} = new DataView({0});\r\n", NomeTabela, camp1);
            modedo.AppendFormat("                    dV_{0}{1}.Sort = \"{2}\";\r\n", NomeTabela, camp1, camp2);
            modedo.AppendFormat("                }};\r\n");
            modedo.AppendFormat("                return dV_{0}{1};\r\n", NomeTabela, camp1);
            modedo.AppendFormat("            }}\r\n");
            modedo.AppendFormat("        }}\r\n");
            modedo.AppendFormat("\r\n");
            modedo.AppendFormat("        internal {0}Row Busca_row{0}({1})\r\n", NomeTabela, Camposstr);
            modedo.AppendFormat("        {{\r\n");
            modedo.AppendFormat("            int ind = DV_{0}{1}.Find(new object[] {{ {2} }});\r\n", NomeTabela, camp1, camp2);
            modedo.AppendFormat("            return (ind < 0) ? null : ({0}Row)(DV_{0}{1}[ind].Row);\r\n", NomeTabela, camp1);
            modedo.AppendFormat("        }}\r\n");            

            Clipboard.SetDataObject(modedo.ToString());
            memoEdit2.Text = modedo.ToString();
            return true;
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            GeraCodigoDataRowView();
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            string Campo = "";
            if (VirInput.Input.Execute("Campo", ref Campo))
            {
                string modedo =
"/// <summary>\r\n" +
"    /// Enumera��o para campo \r\n" +
"    /// </summary>\r\n" +
"    public enum $entrada$\r\n" +
"    {\r\n" +
"        XXX = 0,\r\n" +
"        YYY = 1\r\n" +
"    }\r\n" +
"\r\n" +
"    /// <summary>\r\n" +
"    /// virEnum para campo \r\n" +
"    /// </summary>\r\n" +
"    public class virEnum$entrada$:dllVirEnum.VirEnum\r\n" +
"    {\r\n" +
"        /// <summary>\r\n" +
"        /// Construtor padrao\r\n" +
"        /// </summary>\r\n" +
"        /// <param name=\"Tipo\">Tipo</param>\r\n" +
"        public virEnum$entrada$(Type Tipo):\r\n" +
"            base(Tipo)\r\n" +
"        { \r\n" +
"        }\r\n" +
"\r\n" +
"        private static virEnum$entrada$ _virEnum$entrada$St;\r\n" +
"\r\n" +
"        /// <summary>\r\n" +
"        /// VirEnum est�tico para campo $entrada$\r\n" +
"        /// </summary>\r\n" +
"        public static virEnum$entrada$ virEnum$entrada$St\r\n" +
"        {\r\n" +
"            get \r\n" +
"            {\r\n" +
"                if (_virEnum$entrada$St == null)\r\n" +
"                {\r\n" +
"                    _virEnum$entrada$St = new virEnum$entrada$(typeof($entrada$));\r\n" +
"                    _virEnum$entrada$St.GravaNomes($entrada$.XXX, \"xxx\");\r\n" +
"                    _virEnum$entrada$St.GravaCor($entrada$.XXX, System.Drawing.Color.White);\r\n" +
"\r\n" +
"                }\r\n" +
"                return _virEnum$entrada$St;\r\n" +
"            }\r\n" +
"        }\r\n" +
"    }";
                string saida = modedo.Replace("$entrada$", Campo);
                Clipboard.SetDataObject(saida);
                memoEdit2.Text = saida;
            }
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            TradutorPropriedade();
        }

        private void TradutorPropriedadeUnidade(string EntradaUnidade)
        {
            string modedo1 =
"        private %Tipo% _%Nome%;\r\n" +
"\r\n" +
"        public %Tipo% %Nome%\r\n" +
"        {\r\n" +
"            get { return _%Nome%; }\r\n" +
"            set { _%Nome% = value; }\r\n" +
"        }"+
"\r\n\r\n";
            string modedo2 =
"        RegistraFortementeTipado(\"%Nome%\", , );" +
"\r\n";
            string[] Partes = EntradaUnidade.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (Partes.Length != 2)
                return ;
            string saida = modedo1.Replace("%Tipo%", Partes[0]);
            saida = saida.Replace("%Nome%", Partes[1]);
            Trapro1 += saida;
            Trapro2 += modedo2.Replace("%Nome%", Partes[1]);
        }

        private string Trapro1 = "";
        private string Trapro2 = "";

        private void TradutorPropriedade()
        {
            if (!PegaEntrada())
                return;
            Trapro1 = Trapro2 = "";
            string[] Partes = Entrada.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string Parte in Partes)
            {
                string ParteLimpa = Parte.Replace(";", "");                
                TradutorPropriedadeUnidade(ParteLimpa);
            }

            string saida = Trapro1 + Trapro2;
            Clipboard.SetDataObject(saida);
            memoEdit2.Text = saida;
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            TradutorVariaveis();
        }

        private void TradutorVariaveis()
        {
            if (!PegaEntrada())
                return;
            Trapro1 = Trapro2 = "";
            string[] Partes = Entrada.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string Parte in Partes)
            {
                string ParteLimpa = Parte.Replace(";", "");
                TradutorVariaveisUnidade(ParteLimpa);
            }

            string saida =
"        #region Variaveis Tipadas\r\n" +
Trapro1 +
"        #endregion\r\n\r\n" +
"        protected override void RegistraMapa()\r\n" +
"        {\r\n" +
"            base.RegistraMapa();\r\n" +
Trapro2 +
"        }";
            Clipboard.SetDataObject(saida);
            memoEdit2.Text = saida;
        }

        private void TradutorVariaveisUnidade(string EntradaUnidade)
        {
            string modedo1 =
"        public %Tipo% %Nome%;\r\n";
            string modedo2 =
"        RegistraFortementeTipado(\"%Nome%\", , );" +
"\r\n";
            string[] Partes = EntradaUnidade.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (Partes.Length != 2)
                return;
            string saida = modedo1.Replace("%Tipo%", Partes[0]);
            saida = saida.Replace("%Nome%", Partes[1]);
            Trapro1 += saida;
            Trapro2 += modedo2.Replace("%Nome%", Partes[1]);
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            CompactarBanco();                           
        }

        private void CompactarBanco()
        {
            /*
            try
            {

                string str1 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=#ARQUIVO#";
                string str2 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=#ARQUIVO#;Jet OLEDB:Engine Type=5";
                //string str2 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=#ARQUIVO#";
                System.Windows.Forms.OpenFileDialog OpenF = new OpenFileDialog();
                //System.Windows.Forms.SaveFileDialog SaveF = new SaveFileDialog();
                if (OpenF.ShowDialog() == DialogResult.OK)
                {
                    string nomeReserva = OpenF.FileName + "RESERVA";
                    System.IO.File.Move(OpenF.FileName, nomeReserva);
                    //SaveF.FileName = OpenF.FileName;
                    //if (SaveF.ShowDialog() == DialogResult.OK)
                    //{
                        str1 = str1.Replace("#ARQUIVO#", nomeReserva);
                        str2 = str2.Replace("#ARQUIVO#", OpenF.FileName);
                        JRO.JetEngine jet = new JRO.JetEngine();

                        jet.CompactDatabase(str1, str2);
                        MessageBox.Show("ok");

                    //}
                }

                

            }

            catch (Exception e)
            {

                memoEdit2.Text = string.Format("Erro:{0}\r\n{1}",e.Message,e.StackTrace);

            }
            */
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            memoEdit2.Text = "";
            foreach (string linha in memoEdit1.Lines)
            {
                string linha1 = linha.Replace("\\", "\\\\");
                linha1 = linha1.Replace("\"","\\\"");
                if (memoEdit2.Text != "")
                    memoEdit2.Text += @"\r\n" + "\" + \r\n";
                memoEdit2.Text += "\"" + linha1;
            };
            memoEdit2.Text += "\"";
            Clipboard.SetDataObject(memoEdit2.Text);
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            TradutorTableAdapterPrincipal(true);
        }

        private void simpleButton14_Click(object sender, EventArgs e)
        {
            GeraCodigoParaDS();
        }

        private void simpleButton15_Click(object sender, EventArgs e)
        {
            GeraCodigoClasse();
        }
    }
}

