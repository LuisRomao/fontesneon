using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using dllVirEnum;
using TiposVirtual;
using VirCrip;
using Virtual.Central40;

namespace Virtual.Componentes
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cCentral : CompontesBasicos.ComponenteBase
    {
        #region Usado por outra thread CUIDADO

        private bool Cancelar;
        private Clientes ClientePublicacao; 
        #endregion

        private const string STR_ReferenciaNeon = @"C:\sistema neon";
        private const string STR_PublicacaoNeon = @"C:\FontesVS\Geral Releases\Publicar";
        private const string STR_ReferenciaKarina = @"C:\sistema Karina";
        private const string STR_PublicacaoKarina = @"c:\FontesVS\Karina\Unificado\InstaladorKarina\Release";
        private DevExpress.XtraEditors.ProgressBarControl[] Gaus;

        /// <summary>
        /// 
        /// </summary>
        public cCentral()
        {
            InitializeComponent();
            VirEnumClientes.CarregaEditorDaGrid(colCliente);
            VirEnumClientes.CarregaEditorDaGrid(colAutor);
            VirEnumTarefas.CarregaEditorDaGrid(colTar);
            VirEnumClientes.CarregaEditorDaGrid(ComboCliente);
            VirEnumTarefas.CarregaEditorDaGrid(ComboTarefa);
            Gaus = new DevExpress.XtraEditors.ProgressBarControl[] { progressBarControl1, progressBarControl2 };
        }

        private VirEnum virEnumClientes;

        /// <summary>
        /// 
        /// </summary>
        public VirEnum VirEnumClientes
        {
            get
            {
                if (virEnumClientes == null)
                {                    
                    virEnumClientes = new VirEnum(typeof(Clientes));
                    virEnumClientes.GravaNomes(Clientes.Karina, "Karina");
                    virEnumClientes.GravaNomes(Clientes.NeonSA, "Neon SA");
                    virEnumClientes.GravaNomes(Clientes.NeonSBC, "Neon SBC");
                    virEnumClientes.GravaNomes(Clientes.VirtualLap, "Virtual");
                    virEnumClientes.GravaNomes(Clientes.VirtualTeste, "Teste");
                }
                return virEnumClientes;
            }
        }

        private VirEnum virEnumTarefas;

        /// <summary>
        /// 
        /// </summary>
        public VirEnum VirEnumTarefas
        {
            get
            {
                if (virEnumTarefas == null)
                {
                    virEnumTarefas = new VirEnum(typeof(Tarefas));
                    virEnumTarefas.GravaNomes(Tarefas.NovaVersao, "Nova Vers�o");
                    virEnumTarefas.GravaNomes(Tarefas.SQL, "Comando SQL");
                    virEnumTarefas.GravaNomes(Tarefas.SQLRetorno, "Select SQL");
                    virEnumTarefas.GravaNomes(Tarefas.TempoBusca, "Ajusta Tempo");
                }
                return virEnumTarefas;
            }
        }

        private Central _Central;
        
        private Central Central
        {
            get 
            {
                if (_Central == null)
                {
                    _Central = new Central();
                    //_Central.Url = "https://ssl504.websiteseguro.com/virweb/Central/Central.asmx";
                    _Central.Url = "http://virweb.com.br.windflower.arvixe.com/central/central.asmx";
                    if (chProxy.Checked)
                    {
                        //_Central.Proxy = new System.Net.WebProxy("10.135.1.20", 8080);
                        //_Central.Proxy.Credentials = new System.Net.NetworkCredential("publicacoes", "neon01");
                        _Central.Proxy = new System.Net.WebProxy("10.135.1.20", 8080);
                        _Central.Proxy.Credentials = new System.Net.NetworkCredential("publicacoes", "neon01");
                    }
                }
                return _Central;
            }
        }

        private System.Data.DataSet DS;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Tarefa Tarefa = Central.Ping(Clientes.VirtualLap);
            if (Tarefa == null)
                MessageBox.Show("Sem tarefa");
            else
            {
                MessageBox.Show(Tarefa.Tar.ToString());
                if (Tarefa.Tar == Tarefas.Retorno)
                {
                    Central.RemoverTarefa(Tarefa.ID);
                    memoEdit1.Text += string.Format("RETORNO:\r\n{0}\r\n", Tarefa.Parametro.Replace("\n", "\r\n"));
                }
            }
            DS = Central.DSRetorno();
            if (DS != null) {
                gridControl1.DataSource = DS;
                gridView3.Columns.Clear();
                gridControl1.DataMember = DS.Tables[0].TableName;
                gridView3.PopulateColumns();
            }
            
        }

        private void Salvar_Click(object sender, EventArgs e)
        {
            if (DS != null)
            {
                SaveFileDialog Dia = new SaveFileDialog();
                if (Dia.ShowDialog() == DialogResult.OK)
                {
                    DS.WriteXml(Dia.FileName + ".xml");
                    gridView3.ExportToXls(Dia.FileName + ".xls");
                }
            }
            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dRegistros dRegistros = Central.UltimosRegistros();
            if (dRegistros != null)
                dRegistrosBindingSource.DataSource = dRegistros;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if ((ComboCliente.EditValue != null) && (ComboTarefa.EditValue != null))
            {
                Tarefas Tar = (Tarefas)ComboTarefa.EditValue;
                switch (Tar)
                {
                    case Tarefas.SQL:                       
                    case Tarefas.SQLRetorno:
                    case Tarefas.SQL2:
                    case Tarefas.SQLRetorno2:
                        Central.AgendaTarefa(Clientes.VirtualLap, (Clientes)ComboCliente.EditValue, Tar, null, VirCripWS.Crip(memoEdit2.Text));
                        break;
                    case Tarefas.TempoBusca:
                        if (spinEdit1.Value != 0)
                            Central.AgendaTarefa(Clientes.VirtualLap, (Clientes)ComboCliente.EditValue, Tar, spinEdit1.Value.ToString("n0"), null);
                        break;
                    case Tarefas.NovaVersao:                        
                        break;
                    case Tarefas.AbortarTroca:
                    case Tarefas.Status:
                    case Tarefas.TrocarModulo:
                        Central.AgendaTarefa(Clientes.VirtualLap, (Clientes)ComboCliente.EditValue, Tar, null, null);
                        break;
                    case Tarefas.Retorno:
                        Central.AgendaTarefa(Clientes.VirtualLap, (Clientes)ComboCliente.EditValue, Tar,memoEdit2.Text.Trim(), null);
                        break;
                    default:
                        break;
                }
                
            }
        }

        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit Editor = (DevExpress.XtraEditors.TextEdit)sender;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK) 
                Editor.EditValue = folderBrowserDialog1.SelectedPath;
            
                
        }

        private int ivir;
        private string DiretorioTMP = @"c:\TMP\";
        private string NomeBase;

        private void Publicar()
        {                                  
            if ((Origem.EditValue != null) && (Referencia.EditValue != null) && (ComboCliente.EditValue != null))
            {
                Cancelar = false;
                ClientePublicacao = (Clientes)ComboCliente.EditValue;
                NomeBase = DateTime.Now.ToString("yyyyMMdd_HH_mm_ss");
                memoEdit1.Text = NomeBase;
                if (!Directory.Exists(String.Format("{0}{1}\\", DiretorioTMP, NomeBase)))
                    Directory.CreateDirectory(String.Format("{0}{1}\\", DiretorioTMP, NomeBase));
                else
                {
                    string[] ArquivosDEL = Directory.GetFiles(String.Format("{0}{1}\\", DiretorioTMP, NomeBase));
                    Array.ForEach(ArquivosDEL, File.Delete);
                }
                using (dVersao Cabecalho = new dVersao())
                {
                    Cabecalho.Versao = NomeBase;
                    if (GravaArquivos("", Cabecalho))
                    {
                        Cabecalho.WriteXml(String.Format("{0}{1}\\{2}.xml", DiretorioTMP, NomeBase , NomeBase));
                        Thread ThEnvioFTP = new Thread(EnvioFTPth);
                        BotCancelar.Enabled = true;
                        BotPublicar.Enabled = false;                        
                        ThEnvioFTP.Start(NomeBase);
                    }
                };
            };
        }
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Publicar();                      
        }
        
        void MostraLog(string Log) {
            if (!memoEdit1.InvokeRequired)
                if (Log == "")
                    memoEdit1.Text = "";
                else
                    if (Log == "_ok_")
                        memoEdit1.Text = String.Format("ok      {0}", memoEdit1.Text);
                    else
                        memoEdit1.Text = String.Format("{0}\r\n{1}", Log, memoEdit1.Text);
            else
            {
                //MostraLogDelegate _MostraLogDelegate = MostraLog;
                //this.Invoke(_MostraLogDelegate, Log);
                Invoke(new MostraLogDelegate(MostraLog), Log);
            }
        }

        delegate void AjGauDelegate(int i, int valor);

        private void AtivaGau(int i,int Maximo) 
        {
            if (!Gaus[i].InvokeRequired)
            {
                Gaus[i].Visible = (Maximo != 0);
                Gaus[i].Properties.Maximum = Maximo;
            }
            else
                Invoke(new AjGauDelegate(AtivaGau), i, Maximo);
        }

        private void AtuaGau(int i,int posicao) 
        {
            if (!Gaus[i].InvokeRequired)
            {
                Gaus[i].Position = posicao;
            }
            else
                Invoke(new AjGauDelegate(AtuaGau), i, posicao);
        }

        delegate void MostraLogDelegate(string Log);

        delegate void FimFTPDelegate();

        private void FimFTP() 
        {
            if (!BotPublicar.InvokeRequired)
            {
                BotPublicar.Enabled = true;
                BotCancelar.Enabled = false;
                Gaus[0].Visible = Gaus[1].Visible = false;
            }
            else
                Invoke(new FimFTPDelegate(FimFTP));
        }

        private void EnvioFTPth(object oNomeBase)
        {
            string NomeBase = (string)oNomeBase;
            bool Abortado = false;
            WebProxy PRX = new WebProxy(new Uri("ftp://10.135.1.20"));
            //WebProxy PRX = new WebProxy();
            //WebProxy PRX = (WebProxy)WebProxy.GetDefaultProxy();
            PRX.UseDefaultCredentials = true;
            
            
            //PRX.BypassProxyOnLocal = false;
            NetworkCredential Credenciais = new NetworkCredential("virweb", "venussaulo1000");            
            string[] arquivoEnvs = Directory.GetFiles(String.Format("{0}{1}\\", DiretorioTMP, NomeBase));
            AtivaGau(0, arquivoEnvs.Length);
            int Conta = 1;
            int Tentativas = 0;
            foreach (string arquivoEnv in arquivoEnvs)
            {
                Tentativas = 10;
                while ((!Abortado) && (Tentativas > 0))
                {


                    AtuaGau(0, Conta++);
                    FtpWebResponse _FTPResponse = null;
                    try
                    {
                        //Uri _URIx = new Uri(String.Format("ftp://ftp.virweb.com.br/Nova/{0}", Path.GetFileName(arquivoEnv)));
                        //Uri _URIx = new Uri(String.Format("ftp://200.234.196.121/Nova/{0}", Path.GetFileName(arquivoEnv)));
                        Uri _URIx = new Uri(String.Format("ftp://ftp.windflower.arvixe.com/{0}", Path.GetFileName(arquivoEnv)));
                        //Uri _URIx = new Uri();
                        //Uri _URIx = new Uri(String.Format("/Nova/{0}", Path.GetFileName(arquivoEnv)));
                        //_URIx.HostNameType = UriHostNameType.IPv4;
                        //_URIx.Host = "208.70.188.17";
                        
                        FtpWebRequest _FTPRequest = ((FtpWebRequest)(WebRequest.Create(_URIx)));
                        //_FTPRequest.UseDefaultCredentials = false;
                        _FTPRequest.Credentials = Credenciais;
                        //_FTPRequest.Proxy = PRX;
                        //_FTPRequest.Proxy = null;
                        _FTPRequest.UseBinary = true;
                        _FTPRequest.KeepAlive = true;
                        //_FTPRequest.UsePassive = false;
                        _FTPRequest.UsePassive = true;
                        
                        _FTPRequest.Method = WebRequestMethods.Ftp.UploadFile;



                        byte[] fileContents = File.ReadAllBytes(arquivoEnv);

                        Int64 TamanhoEsperado = _FTPRequest.ContentLength = fileContents.Length;
                        AtivaGau(1, (int)TamanhoEsperado);
                        if (TamanhoEsperado < 1024)
                            MostraLog(String.Format("Arquivo {0} {1:n0} bytes", arquivoEnv, TamanhoEsperado));
                        else
                            if (TamanhoEsperado < (1024 * 1024))
                                MostraLog(String.Format("Arquivo {0} {1:n0} K", arquivoEnv, TamanhoEsperado / 1024));
                            else
                                MostraLog(String.Format("Arquivo {0} {1:n1} M", arquivoEnv, TamanhoEsperado / (1024 * 1024)));
                        Stream requestStream = _FTPRequest.GetRequestStream();
                        int nEnvidado = 0;

                        while (nEnvidado < fileContents.Length)
                        {
                            int enviar = (int)(TamanhoEsperado - nEnvidado);
                            if (enviar > 102400)
                                enviar = 102400;
                            requestStream.Write(fileContents, nEnvidado, enviar);
                            nEnvidado += enviar;

                            AtuaGau(1, nEnvidado);

                            //if (Cancelar)
                            if (Cancelar)
                            {
                                Abortado = true;                               
                                MostraLog("Cancelado");                                
                                FimFTP();
                                return;
                            }
                        }
                        //requestStream.Write(fileContents, 0, fileContents.Length);
                        requestStream.Close();




                        _FTPResponse = ((FtpWebResponse)(_FTPRequest.GetResponse()));
                        _FTPResponse.Close();
                        MostraLog("_ok_");
                        Tentativas = 0;
                    }
                    catch (Exception Erro)
                    {
                        if (_FTPResponse != null)
                            _FTPResponse.Close();                        
                        MostraLog(String.Format("ERRO          X FTP:{0}{1}{2}", arquivoEnv, Environment.NewLine, Erro.Message));
                        Tentativas--;
                        if (Tentativas == 0)
                            Abortado = true;
                        DateTime Espera = DateTime.Now.AddSeconds(10);
                        while (DateTime.Now < Espera) ;
                        MostraLog(String.Format("Tentativas: {0:n0}", 10 - Tentativas));
                    }
                }
            };
            if (Abortado)
                MostraLog("Abortado");
            else
            {
                MostraLog("FTP conclu�do");
                Central.AgendaTarefa(Clientes.VirtualLap, ClientePublicacao, Tarefas.NovaVersao, NomeBase,null);
            }            
            FimFTP();
        }
       
        private bool GravaArquivos(string prefixo, dVersao Cabecalho)
        {
            string[] Arquivos = Directory.GetFiles(Origem.EditValue + prefixo);
            foreach (string arquivo in Arquivos)
            {                
                string nomepuro = Path.GetFileName(arquivo);
                string ArquivoRef = Referencia.EditValue + prefixo + "\\" + nomepuro;
                if(File.Exists(ArquivoRef)){
                    FileVersionInfo VerN = FileVersionInfo.GetVersionInfo(arquivo);
                    if (VerN.FileVersion == null)
                    {
                        if (File.GetLastWriteTime(arquivo) == File.GetLastWriteTime(ArquivoRef))
                        {
                            memoEdit1.Text = String.Format("          X data({0}):{1}ok\r\n{2}", File.GetLastWriteTime(arquivo), arquivo, memoEdit1.Text);
                            Application.DoEvents();
                            continue;
                        }
                    }
                    else
                    {
                        FileVersionInfo VerV = FileVersionInfo.GetVersionInfo(ArquivoRef);
                        if (VerN.FileVersion == VerV.FileVersion)
                        {
                            memoEdit1.Text = String.Format("          X Vers�o({0}):{1} - {2}ok\r\n{3}", VerN.FileVersion, arquivo, VerV.FileVersion, memoEdit1.Text);
                            Application.DoEvents();
                            continue;
                        }
                    }                                       
                };
                ivir++;
                string NomeVirtual = String.Format("{0}_{1}.vir", NomeBase, ivir);
                MostraLog(String.Format("{0} -> {1}", arquivo, NomeVirtual));
                VirZIP.zip.Execute(arquivo, String.Format("{0}{1}\\{2}", DiretorioTMP, NomeBase, NomeVirtual), System.IO.Compression.CompressionMode.Compress);
                dVersao.PacoteAtualizacaoRow NovaLinha = Cabecalho.PacoteAtualizacao.NewPacoteAtualizacaoRow();
                NovaLinha.Diretorio = prefixo;
                NovaLinha.NomeOrig = nomepuro;
                NovaLinha.NomeVir = NomeVirtual;
                NovaLinha.Tamanho = new FileInfo(String.Format("{0}{1}\\{2}", DiretorioTMP, NomeBase, NomeVirtual)).Length;
                Cabecalho.PacoteAtualizacao.AddPacoteAtualizacaoRow(NovaLinha);                
                MostraLog("_ok_");
                Application.DoEvents();
            };
            string[] Diretorios = Directory.GetDirectories(Origem.EditValue + prefixo);
            foreach (string novodiretorio in Diretorios)
            {
                if (!GravaArquivos(novodiretorio.Replace(Origem.EditValue.ToString(), ""), Cabecalho))
                    return false;
            }
            return true;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            Origem.EditValue = STR_PublicacaoNeon;
            Referencia.EditValue = STR_ReferenciaNeon;
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            Cancelar = true;
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            Origem.EditValue = STR_PublicacaoKarina;
            Referencia.EditValue = STR_ReferenciaKarina;
        }

        private void simpleButton6_Click_1(object sender, EventArgs e)
        {
            Central.RemoverTarefa((int)spinEdit1.Value);            
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            int de = (int)spinDE.Value;
            int ate = (int)spinATE.Value;
            for (int i = de; i <= ate; i++)
                Central.RemoverTarefa(i);
            MessageBox.Show("ok");
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = "";
        }

        private void gridControl3_DoubleClick(object sender, EventArgs e)
        {
            dRegistros.TarefasRow rowClicada = (dRegistros.TarefasRow)gridView5.GetFocusedDataRow();
            int id = rowClicada.ID;
            int de = (int)spinDE.Value;
            int ate = (int)spinATE.Value;
            if (id > ate)
                spinATE.EditValue = id;
            if (id < ate)
                spinDE.EditValue = id;
        }

        private void chProxy_CheckedChanged(object sender, EventArgs e)
        {
            if (chProxy.Checked)
            {
                if(ProxyPort.Value != 0)
                    Central.Proxy = new System.Net.WebProxy(ProxyIP.Text, (int)ProxyPort.Value);
                else
                    Central.Proxy = new System.Net.WebProxy(ProxyIP.Text, true);
                if (ProxyIPUsu.Text != "")
                    Central.Proxy.Credentials = new System.Net.NetworkCredential(ProxyIPUsu.Text, ProxyPass.Text);
            }
            else
            {
                Central.Proxy = null;
            }

        }

    }
}

