using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

namespace Virtual.Componentes
{
    public partial class cNotePad : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Caminho para os aquivos
        /// </summary>
        protected string PathTXT = @"C:\FontesVS\txt\";

        /// <summary>
        /// Arquivo base
        /// </summary>
        protected virtual string ArquivoBlocoNotas{
            get { return PathTXT + @"BlocoDeNotas.txt"; }
        }

        private void Carrega() {
            if (File.Exists(ArquivoBlocoNotas))
                memoEdit1.Text = File.ReadAllText(ArquivoBlocoNotas);
        }

        /// <summary>
        /// Construtor principal
        /// </summary>
        public cNotePad()
        {
            InitializeComponent();                        
        }


        private byte[] aChave;

        private byte[] Vector = new byte[8] { 32, 43, 218, 16, 53, 69, 118, 24 };

        

        private void SalvaCrip(){
            //prepara();


            FileStream arqSaida = new FileStream(ArquivoBlocoNotas + "X", FileMode.OpenOrCreate, FileAccess.Write);
            arqSaida.SetLength(0);
           
            
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            CryptoStream crStream = new CryptoStream(arqSaida, des.CreateEncryptor(aChave, Vector), CryptoStreamMode.Write);

            UnicodeEncoding Enc = new UnicodeEncoding();
            byte[] Gravar = Enc.GetBytes(memoEdit1.Text); 
            
            crStream.Write(Gravar, 0, Gravar.Length);
           
            crStream.Close();        

        }

        private void Decifrar(){
            FileStream arqEntrada = null;
            try
            {
                byte[] storage = new byte[4096];
                int tamanhoPacote;
                arqEntrada = new FileStream(ArquivoBlocoNotas + "X", FileMode.Open, FileAccess.Read);
                MemoryStream arqSaida = new MemoryStream();
                memoEdit1.Text = "";

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                CryptoStream crStream = new CryptoStream(arqSaida, des.CreateDecryptor(aChave, Vector), CryptoStreamMode.Write);

                while (true)
                {
                    tamanhoPacote = arqEntrada.Read(storage, 0, 4096);
                    if (tamanhoPacote == 0)
                        break;
                    crStream.Write(storage, 0, tamanhoPacote);
                };
                
                crStream.Close();
                UnicodeEncoding Enc = new UnicodeEncoding();
                byte[] Lido = arqSaida.ToArray();
                memoEdit1.Text = Enc.GetString(Lido);
            }
            catch (Exception e)
            {
                memoEdit1.Text = "ERRO";
                memoEdit1.Text += "\r\n" + e.Message;
                memoEdit1.Text += "\r\n" + e.Message.GetType().ToString();
            }
            finally {
                if(arqEntrada != null)
                     arqEntrada.Close();
            }

        }



        private void cNotePad_OnClose(object sender, EventArgs e)
        {
            if (!Travado)
            {
                if (!Directory.Exists(Path.GetDirectoryName(ArquivoBlocoNotas)))
                    Directory.CreateDirectory(Path.GetDirectoryName(ArquivoBlocoNotas));
                string nomepuro = Path.GetDirectoryName(ArquivoBlocoNotas) + @"\" + Path.GetFileNameWithoutExtension(ArquivoBlocoNotas);
                if (File.Exists(nomepuro + "5.txt"))
                    File.Delete(nomepuro + "5.txt");
                for (int i = 4; i > 0; i--)
                    if (File.Exists(nomepuro + i.ToString() + ".txt"))
                        File.Move(nomepuro + i.ToString() + ".txt", nomepuro + ((int)(i + 1)).ToString() + ".txt");
                if (File.Exists(ArquivoBlocoNotas + "X"))
                    File.Move(ArquivoBlocoNotas + "X", nomepuro + "1.txt");
                SalvaCrip();
            };
        }

        

        

        bool Travado = true;

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (textEdit1.Text.Length == 13)
            {
                byte[] Manobra;
                Travado = false;
                ASCIIEncoding AscEncod = new ASCIIEncoding();
                Manobra =  AscEncod.GetBytes(textEdit1.Text);
                SHA1CryptoServiceProvider hashSha = new SHA1CryptoServiceProvider(); 
                Manobra = hashSha.ComputeHash(Manobra);
                aChave = new byte[8];
                for(int i = 0;i<7;i++)
                    aChave[i] = Manobra[i];

                textEdit1.Text = "";
                if(System.IO.File.Exists(ArquivoBlocoNotas + "X"))
                    Decifrar();
            }
        }

        private void memoEdit1_Leave(object sender, EventArgs e)
        {
            if (!Travado)
                SalvaCrip();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int pos = memoEdit1.Text.IndexOf(textEdit2.Text, memoEdit1.SelectionStart+1);
            if(pos == -1)
                pos = memoEdit1.Text.IndexOf(textEdit2.Text, 0);
            if (pos > -1)
            {
                memoEdit1.Focus();
                memoEdit1.Select(pos, textEdit2.Text.Length);
                memoEdit1.ScrollToCaret();
            }
        }

        private void textEdit2_Enter(object sender, EventArgs e)
        {
            textEdit2.Text = memoEdit1.SelectedText;            
        }

       

        
        

    }
}

