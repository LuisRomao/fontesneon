using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Virtual.Componentes
{
    /// <summary>
    /// Editor de Recados
    /// </summary>
    public partial class cRecados : Virtual.Componentes.cNotePad
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cRecados()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Arquivo Base
        /// </summary>
        protected override string ArquivoBlocoNotas
        {
            get
            {
                return PathTXT + @"Recados.txt";
            }
        }
    }
}

