﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Virtual.Componentes
{
    /// <summary>
    /// Classe para tratar enumerações 
    /// </summary>
    public partial class cEnumeracoes : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Trata as enumerações
        /// </summary>
        public cEnumeracoes()
        {
            InitializeComponent();
        }

        private void Lista(string NomeAssembly)
        {
            Assembly AssemblyEnumeracoes = Assembly.LoadFrom(NomeAssembly);
            Type[] Tipos = AssemblyEnumeracoes.GetTypes();
            foreach (Type T in Tipos)
            {
                if (T.IsEnum)
                {
                    //memoEdit1.Text += string.Format("{0}\t{1} = {2}\r\n", NomeAssembly, T, T.FullName);



                    foreach (object o in Enum.GetValues(T))
                        dEnimeracoes.Enumeracoes.AddEnumeracoesRow(NomeAssembly, T.Name, o.ToString(), (int)o);
                        //memoEdit1.Text += string.Format("\t-> {0} \t= {1}\r\n", o, (int)o);


                }                
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            dEnimeracoes.Enumeracoes.Clear();
            Lista("VirEnumeracoes.dll");
            Lista("VirEnumeracoesNeon.dll");
        }
    }
}
