namespace Virtual.Componentes
{
    partial class cCentral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.dRegistrosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCliente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colUltimaVisita = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAutor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colExecutante = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colParametro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.ComboCliente = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.ComboTarefa = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.BotPublicar = new DevExpress.XtraEditors.SimpleButton();
            this.Origem = new DevExpress.XtraEditors.ButtonEdit();
            this.Referencia = new DevExpress.XtraEditors.ButtonEdit();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ProxyPort = new DevExpress.XtraEditors.SpinEdit();
            this.ProxyPass = new DevExpress.XtraEditors.TextEdit();
            this.ProxyIPUsu = new DevExpress.XtraEditors.TextEdit();
            this.ProxyIP = new DevExpress.XtraEditors.TextEdit();
            this.chProxy = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.spinATE = new DevExpress.XtraEditors.SpinEdit();
            this.spinDE = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.Salvar = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.BotCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.progressBarControl2 = new DevExpress.XtraEditors.ProgressBarControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRegistrosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboCliente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboTarefa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Origem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProxyPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProxyPass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProxyIPUsu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProxyIP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chProxy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinATE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinDE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(757, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(101, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Ping";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(5, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(184, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Verifica Ativos";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView3;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1530, 194);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridControl1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "Ativos";
            this.gridControl2.DataSource = this.dRegistrosBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(5, 34);
            this.gridControl2.MainView = this.gridView4;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1});
            this.gridControl2.Size = new System.Drawing.Size(302, 169);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // dRegistrosBindingSource
            // 
            this.dRegistrosBindingSource.DataSource = typeof(Virtual.Central40.dRegistros);
            this.dRegistrosBindingSource.Position = 0;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCliente,
            this.colUltimaVisita});
            this.gridView4.GridControl = this.gridControl2;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.Editable = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // colCliente
            // 
            this.colCliente.Caption = "Cliente";
            this.colCliente.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCliente.FieldName = "Cliente";
            this.colCliente.Name = "colCliente";
            this.colCliente.Visible = true;
            this.colCliente.VisibleIndex = 0;
            this.colCliente.Width = 121;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colUltimaVisita
            // 
            this.colUltimaVisita.Caption = "UltimaVisita";
            this.colUltimaVisita.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.colUltimaVisita.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colUltimaVisita.FieldName = "UltimaVisita";
            this.colUltimaVisita.Name = "colUltimaVisita";
            this.colUltimaVisita.Visible = true;
            this.colUltimaVisita.VisibleIndex = 1;
            this.colUltimaVisita.Width = 145;
            // 
            // gridControl3
            // 
            this.gridControl3.DataMember = "Tarefas";
            this.gridControl3.DataSource = this.dRegistrosBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(313, 34);
            this.gridControl3.MainView = this.gridView5;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox2,
            this.repositoryItemImageComboBox3});
            this.gridControl3.Size = new System.Drawing.Size(545, 169);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            this.gridControl3.DoubleClick += new System.EventHandler(this.gridControl3_DoubleClick);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAutor,
            this.colExecutante,
            this.colTar,
            this.colParametro,
            this.colID});
            this.gridView5.GridControl = this.gridControl3;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.Editable = false;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // colAutor
            // 
            this.colAutor.Caption = "Autor";
            this.colAutor.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colAutor.FieldName = "Autor";
            this.colAutor.Name = "colAutor";
            this.colAutor.Visible = true;
            this.colAutor.VisibleIndex = 1;
            this.colAutor.Width = 83;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colExecutante
            // 
            this.colExecutante.Caption = "Executante";
            this.colExecutante.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colExecutante.FieldName = "Executante";
            this.colExecutante.Name = "colExecutante";
            this.colExecutante.Visible = true;
            this.colExecutante.VisibleIndex = 2;
            this.colExecutante.Width = 91;
            // 
            // colTar
            // 
            this.colTar.Caption = "Tar";
            this.colTar.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colTar.FieldName = "Tar";
            this.colTar.Name = "colTar";
            this.colTar.Visible = true;
            this.colTar.VisibleIndex = 3;
            this.colTar.Width = 80;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colParametro
            // 
            this.colParametro.Caption = "Parametro";
            this.colParametro.FieldName = "Parametro";
            this.colParametro.Name = "colParametro";
            this.colParametro.Visible = true;
            this.colParametro.VisibleIndex = 4;
            this.colParametro.Width = 162;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Visible = true;
            this.colID.VisibleIndex = 0;
            this.colID.Width = 42;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(195, 5);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(184, 23);
            this.simpleButton3.TabIndex = 5;
            this.simpleButton3.Text = "Registrar Tarefa";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(597, 5);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Size = new System.Drawing.Size(100, 20);
            this.spinEdit1.TabIndex = 6;
            // 
            // ComboCliente
            // 
            this.ComboCliente.Location = new System.Drawing.Point(385, 5);
            this.ComboCliente.Name = "ComboCliente";
            this.ComboCliente.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboCliente.Size = new System.Drawing.Size(100, 20);
            this.ComboCliente.TabIndex = 9;
            // 
            // ComboTarefa
            // 
            this.ComboTarefa.Location = new System.Drawing.Point(491, 5);
            this.ComboTarefa.Name = "ComboTarefa";
            this.ComboTarefa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboTarefa.Size = new System.Drawing.Size(100, 20);
            this.ComboTarefa.TabIndex = 10;
            // 
            // BotPublicar
            // 
            this.BotPublicar.Location = new System.Drawing.Point(5, 209);
            this.BotPublicar.Name = "BotPublicar";
            this.BotPublicar.Size = new System.Drawing.Size(184, 23);
            this.BotPublicar.TabIndex = 11;
            this.BotPublicar.Text = "Publicar";
            this.BotPublicar.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // Origem
            // 
            this.Origem.Location = new System.Drawing.Point(195, 208);
            this.Origem.Name = "Origem";
            this.Origem.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Origem.Size = new System.Drawing.Size(356, 20);
            this.Origem.TabIndex = 12;
            this.Origem.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(195, 234);
            this.Referencia.Name = "Referencia";
            this.Referencia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Referencia.Size = new System.Drawing.Size(356, 20);
            this.Referencia.TabIndex = 13;
            this.Referencia.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog1.SelectedPath = "c:\\fontesvs\\";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.ProxyPort);
            this.panelControl1.Controls.Add(this.ProxyPass);
            this.panelControl1.Controls.Add(this.ProxyIPUsu);
            this.panelControl1.Controls.Add(this.ProxyIP);
            this.panelControl1.Controls.Add(this.chProxy);
            this.panelControl1.Controls.Add(this.simpleButton8);
            this.panelControl1.Controls.Add(this.spinATE);
            this.panelControl1.Controls.Add(this.spinDE);
            this.panelControl1.Controls.Add(this.simpleButton7);
            this.panelControl1.Controls.Add(this.Salvar);
            this.panelControl1.Controls.Add(this.simpleButton6);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.BotCancelar);
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.Referencia);
            this.panelControl1.Controls.Add(this.Origem);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.ComboCliente);
            this.panelControl1.Controls.Add(this.BotPublicar);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.gridControl3);
            this.panelControl1.Controls.Add(this.ComboTarefa);
            this.panelControl1.Controls.Add(this.gridControl2);
            this.panelControl1.Controls.Add(this.spinEdit1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1511, 268);
            this.panelControl1.TabIndex = 14;
            // 
            // ProxyPort
            // 
            this.ProxyPort.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ProxyPort.Location = new System.Drawing.Point(1090, 6);
            this.ProxyPort.Name = "ProxyPort";
            this.ProxyPort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ProxyPort.Size = new System.Drawing.Size(49, 20);
            this.ProxyPort.TabIndex = 28;
            // 
            // ProxyPass
            // 
            this.ProxyPass.EditValue = "neon01";
            this.ProxyPass.Location = new System.Drawing.Point(984, 58);
            this.ProxyPass.Name = "ProxyPass";
            this.ProxyPass.Properties.PasswordChar = '*';
            this.ProxyPass.Size = new System.Drawing.Size(100, 20);
            this.ProxyPass.TabIndex = 26;
            // 
            // ProxyIPUsu
            // 
            this.ProxyIPUsu.EditValue = "publicacoes";
            this.ProxyIPUsu.Location = new System.Drawing.Point(984, 32);
            this.ProxyIPUsu.Name = "ProxyIPUsu";
            this.ProxyIPUsu.Size = new System.Drawing.Size(100, 20);
            this.ProxyIPUsu.TabIndex = 25;
            // 
            // ProxyIP
            // 
            this.ProxyIP.EditValue = "192.168.0.1";
            this.ProxyIP.Location = new System.Drawing.Point(984, 6);
            this.ProxyIP.Name = "ProxyIP";
            this.ProxyIP.Size = new System.Drawing.Size(100, 20);
            this.ProxyIP.TabIndex = 24;
            // 
            // chProxy
            // 
            this.chProxy.Location = new System.Drawing.Point(878, 8);
            this.chProxy.Name = "chProxy";
            this.chProxy.Properties.Caption = "Proxy";
            this.chProxy.Size = new System.Drawing.Size(75, 19);
            this.chProxy.TabIndex = 23;
            this.chProxy.CheckedChanged += new System.EventHandler(this.chProxy_CheckedChanged);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(864, 68);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(101, 23);
            this.simpleButton8.TabIndex = 22;
            this.simpleButton8.Text = "Limpa";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // spinATE
            // 
            this.spinATE.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinATE.Location = new System.Drawing.Point(864, 183);
            this.spinATE.Name = "spinATE";
            this.spinATE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinATE.Size = new System.Drawing.Size(100, 20);
            this.spinATE.TabIndex = 21;
            // 
            // spinDE
            // 
            this.spinDE.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinDE.Location = new System.Drawing.Point(864, 155);
            this.spinDE.Name = "spinDE";
            this.spinDE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinDE.Size = new System.Drawing.Size(100, 20);
            this.spinDE.TabIndex = 20;
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(864, 126);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(101, 23);
            this.simpleButton7.TabIndex = 19;
            this.simpleButton7.Text = "Remover de ate";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // Salvar
            // 
            this.Salvar.Location = new System.Drawing.Point(864, 237);
            this.Salvar.Name = "Salvar";
            this.Salvar.Size = new System.Drawing.Size(101, 23);
            this.Salvar.TabIndex = 18;
            this.Salvar.Text = "Salvar";
            this.Salvar.Click += new System.EventHandler(this.Salvar_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(864, 97);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(101, 23);
            this.simpleButton6.TabIndex = 17;
            this.simpleButton6.Text = "Remover";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click_1);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(674, 239);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(184, 23);
            this.simpleButton4.TabIndex = 16;
            this.simpleButton4.Text = "Sistema Karina";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click_1);
            // 
            // BotCancelar
            // 
            this.BotCancelar.Enabled = false;
            this.BotCancelar.Location = new System.Drawing.Point(5, 237);
            this.BotCancelar.Name = "BotCancelar";
            this.BotCancelar.Size = new System.Drawing.Size(184, 23);
            this.BotCancelar.TabIndex = 15;
            this.BotCancelar.Text = "Cancelar";
            this.BotCancelar.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(674, 209);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(184, 23);
            this.simpleButton5.TabIndex = 14;
            this.simpleButton5.Text = "Sistema Neon";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 268);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1511, 539);
            this.xtraTabControl1.TabIndex = 15;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.memoEdit1);
            this.xtraTabPage1.Controls.Add(this.progressBarControl2);
            this.xtraTabPage1.Controls.Add(this.progressBarControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1505, 511);
            this.xtraTabPage1.Text = "Retorno";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(0, 0);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(1505, 475);
            this.memoEdit1.TabIndex = 0;
            // 
            // progressBarControl2
            // 
            this.progressBarControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBarControl2.EditValue = "20";
            this.progressBarControl2.Location = new System.Drawing.Point(0, 475);
            this.progressBarControl2.Name = "progressBarControl2";
            this.progressBarControl2.Properties.EndColor = System.Drawing.Color.Red;
            this.progressBarControl2.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.progressBarControl2.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressBarControl2.Properties.ShowTitle = true;
            this.progressBarControl2.Properties.StartColor = System.Drawing.Color.Red;
            this.progressBarControl2.Size = new System.Drawing.Size(1505, 18);
            this.progressBarControl2.TabIndex = 2;
            this.progressBarControl2.Visible = false;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBarControl1.Location = new System.Drawing.Point(0, 493);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.progressBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Size = new System.Drawing.Size(1505, 18);
            this.progressBarControl1.TabIndex = 1;
            this.progressBarControl1.Visible = false;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1530, 194);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.memoEdit2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1530, 194);
            this.xtraTabPage3.Text = "Comando";
            // 
            // memoEdit2
            // 
            this.memoEdit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit2.Location = new System.Drawing.Point(0, 0);
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Size = new System.Drawing.Size(1530, 194);
            this.memoEdit2.TabIndex = 0;
            // 
            // cCentral
            // 
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCentral";
            this.Size = new System.Drawing.Size(1511, 807);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRegistrosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboCliente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboTarefa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Origem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProxyPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProxyPass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProxyIPUsu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProxyIP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chProxy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinATE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinDE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colCliente;
        private DevExpress.XtraGrid.Columns.GridColumn colUltimaVisita;
        private System.Windows.Forms.BindingSource dRegistrosBindingSource;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colAutor;
        private DevExpress.XtraGrid.Columns.GridColumn colExecutante;
        private DevExpress.XtraGrid.Columns.GridColumn colTar;
        private DevExpress.XtraGrid.Columns.GridColumn colParametro;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboCliente;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboTarefa;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraEditors.SimpleButton BotPublicar;
        private DevExpress.XtraEditors.ButtonEdit Origem;
        private DevExpress.XtraEditors.ButtonEdit Referencia;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton BotCancelar;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.MemoEdit memoEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton Salvar;
        private DevExpress.XtraEditors.SpinEdit spinATE;
        private DevExpress.XtraEditors.SpinEdit spinDE;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.CheckEdit chProxy;
        private DevExpress.XtraEditors.TextEdit ProxyPass;
        private DevExpress.XtraEditors.TextEdit ProxyIPUsu;
        private DevExpress.XtraEditors.TextEdit ProxyIP;
        private DevExpress.XtraEditors.SpinEdit ProxyPort;
    }
}
