using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Virtual
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt = new UsuarioLogadoVirtual();
            Application.Run(new PrincipalVirtual());
        }
    }
}