using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Virtual
{
    /// <summary>
    /// Tela principal
    /// </summary>
    public partial class PrincipalVirtual : CompontesBasicos.FormPrincipalBase
    {
        /// <summary>
        /// Construtor principal
        /// </summary>
        public PrincipalVirtual()
        {
            InitializeComponent();
#if (!DEBUG)
            CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao = true;      
#endif
        }

              
    }
}

