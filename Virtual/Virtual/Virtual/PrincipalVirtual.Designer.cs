namespace Virtual
{
    partial class PrincipalVirtual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrincipalVirtual));
            CompontesBasicos.ModuleInfo moduleInfo1 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo2 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo3 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo4 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo5 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo6 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo7 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo8 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo9 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo10 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo11 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo12 = new CompontesBasicos.ModuleInfo();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.Editores = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.DockPanel_FContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            this.DockPanel_F.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureEdit_F
            // 
            this.PictureEdit_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.PictureEdit_F.EditValue = global::Virtual.Properties.Resources.logo;
            // 
            // NavBarControl_F
            // 
            this.NavBarControl_F.ActiveGroup = this.Editores;
            this.NavBarControl_F.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1,
            this.Editores,
            this.navBarGroup2});
            this.NavBarControl_F.OptionsNavPane.ExpandedWidth = 194;
            this.NavBarControl_F.Size = new System.Drawing.Size(194, 430);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            this.DefaultLookAndFeel_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.DefaultLookAndFeel_F.LookAndFeel.UseWindowsXPTheme = true;
            // 
            // DefaultToolTipController_F
            // 
            // 
            // 
            // 
            this.DefaultToolTipController_F.DefaultController.AutoPopDelay = 10000;
            this.DefaultToolTipController_F.DefaultController.Rounded = true;
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // DockPanel_FContainer
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.DockPanel_FContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.DockPanel_FContainer.Location = new System.Drawing.Point(3, 20);
            this.DockPanel_FContainer.Size = new System.Drawing.Size(194, 511);
            // 
            // DockPanel_F
            // 
            this.DockPanel_F.Appearance.BackColor = System.Drawing.Color.White;
            this.DockPanel_F.Appearance.Options.UseBackColor = true;
            this.DockPanel_F.Options.AllowDockFill = false;
            this.DockPanel_F.Options.ShowCloseButton = false;
            this.DockPanel_F.Options.ShowMaximizeButton = false;
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "Dia";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // Editores
            // 
            this.Editores.Caption = "Editores";
            this.Editores.Expanded = true;
            this.Editores.Name = "Editores";
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Publica��o";
            this.navBarGroup2.Expanded = true;
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // PrincipalVirtual
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(782, 556);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            moduleInfo1.Grupo = 0;
            moduleInfo1.ImagemG = null;
            moduleInfo1.ImagemP = null;
            moduleInfo1.IniciarAberto = true;
            moduleInfo1.ModuleType = null;
            moduleInfo1.ModuleTypestr = "Virtual.Componentes.cNotePad";
            moduleInfo1.Name = "Anota��es";
            moduleInfo2.Grupo = 0;
            moduleInfo2.ImagemG = null;
            moduleInfo2.ImagemP = null;
            moduleInfo2.ModuleType = null;
            moduleInfo2.ModuleTypestr = "Virtual.Componentes.cTradutor";
            moduleInfo2.Name = "Tradutor";
            moduleInfo3.Grupo = 1;
            moduleInfo3.ImagemG = null;
            moduleInfo3.ImagemP = null;
            moduleInfo3.ModuleType = null;
            moduleInfo3.ModuleTypestr = "Virtual.Componentes.cVersoesNP";
            moduleInfo3.Name = "Vers�es";
            moduleInfo4.Grupo = 1;
            moduleInfo4.ImagemG = null;
            moduleInfo4.ImagemP = null;
            moduleInfo4.ModuleType = null;
            moduleInfo4.ModuleTypestr = "Virtual.Componentes.cRecados";
            moduleInfo4.Name = "Recados";
            moduleInfo5.Grupo = 1;
            moduleInfo5.ImagemG = null;
            moduleInfo5.ImagemP = null;
            moduleInfo5.ModuleType = null;
            moduleInfo5.ModuleTypestr = "Virtual.Componentes.cRecados2";
            moduleInfo5.Name = "Recados 2";
            moduleInfo6.Grupo = 2;
            moduleInfo6.ImagemG = null;
            moduleInfo6.ImagemP = null;
            moduleInfo6.ModuleType = null;
            moduleInfo6.ModuleTypestr = "Virtual.Componentes.cModulos";
            moduleInfo6.Name = "M�dulos";
            moduleInfo7.Grupo = 0;
            moduleInfo7.ImagemG = null;
            moduleInfo7.ImagemP = null;
            moduleInfo7.ModuleType = null;
            moduleInfo7.ModuleTypestr = "Virtual.Componentes.cFB";
            moduleInfo7.Name = "FireBird";
            moduleInfo8.Grupo = 2;
            moduleInfo8.ImagemG = null;
            moduleInfo8.ImagemP = null;
            moduleInfo8.ModuleType = null;
            moduleInfo8.ModuleTypestr = "Virtual.Componentes.cCentral";
            moduleInfo8.Name = "Central";
            moduleInfo9.Grupo = 0;
            moduleInfo9.ImagemG = null;
            moduleInfo9.ImagemP = null;
            moduleInfo9.ModuleType = null;
            moduleInfo9.ModuleTypestr = "Virtual.Componentes.cGIF";
            moduleInfo9.Name = "GIF";
            moduleInfo10.Grupo = 0;
            moduleInfo10.ImagemG = null;
            moduleInfo10.ImagemP = null;
            moduleInfo10.ModuleType = null;
            moduleInfo10.ModuleTypestr = "Virtual.Componentes.EditorASCII";
            moduleInfo10.Name = "ASCII";
            moduleInfo11.Grupo = 2;
            moduleInfo11.ImagemG = null;
            moduleInfo11.ImagemP = null;
            moduleInfo11.ModuleType = null;
            moduleInfo11.ModuleTypestr = "Virtual.Componentes.cEnumeracoes";
            moduleInfo11.Name = "Enumera��es";
            moduleInfo12.Grupo = 0;
            moduleInfo12.ImagemG = null;
            moduleInfo12.ImagemP = null;
            moduleInfo12.ModuleType = null;
            moduleInfo12.ModuleTypestr = "Virtual.Componentes.cEditorPDF";
            moduleInfo12.Name = "Editor PDF";
            this.ModuleInfoCollection.AddRange(new CompontesBasicos.ModuleInfo[] {
            moduleInfo1,
            moduleInfo2,
            moduleInfo3,
            moduleInfo4,
            moduleInfo5,
            moduleInfo6,
            moduleInfo7,
            moduleInfo8,
            moduleInfo9,
            moduleInfo10,
            moduleInfo11,
            moduleInfo12});
            this.Name = "PrincipalVirtual";
            this.Text = "Virtual - Gest�o - Novo";
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.DockPanel_FContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            this.DockPanel_F.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarGroup Editores;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
    }
}
