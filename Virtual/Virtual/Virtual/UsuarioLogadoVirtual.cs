using System;
using System.Collections.Generic;
using System.Text;

namespace Virtual
{
    /// <summary>
    /// Classe de login Virtual
    /// </summary>
    public class UsuarioLogadoVirtual : CompontesBasicos.Bancovirtual.UsuarioLogado
    {
        /// <summary>
        /// Busca USUNome
        /// </summary>
        /// <param name="USU"></param>
        /// <returns></returns>
        public override string BuscaUSUNome(object USU)
        {
            return "Lu�s Henrique";
        }

        /// <summary>
        /// C�digo USU
        /// </summary>
        public override int USU
        {
            get { return 1; }
        }

        /// <summary>
        /// Nome do Usu�rio
        /// </summary>
        public override string USUNome
        {
            get { return "Lu�s Henrique"; }
        }

        /// <summary>
        /// Verifica a funcionalidade
        /// </summary>
        /// <param name="Funcionalidade"></param>
        /// <returns></returns>
        public override int VerificaFuncionalidade(string Funcionalidade)
        {
            return 1000;
        }

        /// <summary>
        /// 
        /// </summary>
        public override System.Net.NetworkCredential EmailNetCredenciais
        {
            get { return null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string EmailRemetente
        {
            get { return "luis@virweb.com.br"; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override System.Net.WebProxy Proxy
        {
            get { return null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string ServidorSMTP
        {
            get { return "smtp.virweb.com.br"; }
        }
    }
}
