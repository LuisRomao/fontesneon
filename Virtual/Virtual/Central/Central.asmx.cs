using System;
using System.Data;
using System.Web.Services;
using System.ComponentModel;


namespace Central
{
    /// <summary>
    /// Summary description for Central
    /// </summary>
    [WebService(Namespace = "http://www.virweb.com.br/central/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class Central : WebService
    {
        //static ArrayList Tarefas = new ArrayList();
        //static int IDgeral;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Cliente"></param>
        /// <returns></returns>
        [WebMethod]        
        public Tarefa Ping(Enumeracoes.Clientes Cliente)
        {
            dRegistros.AtivosRow rowAtivos = dRegistros.dRegistrosSt.Ativos.FindByCliente((int)Cliente);
            if (rowAtivos == null)
            {
                rowAtivos = dRegistros.dRegistrosSt.Ativos.NewAtivosRow();
                rowAtivos.Cliente = (int)Cliente;
                dRegistros.dRegistrosSt.Ativos.AddAtivosRow(rowAtivos);
            };
            rowAtivos.UltimaVisita = DateTime.Now;
            foreach (dRegistros.TarefasRow rowTarefa in dRegistros.dRegistrosSt.Tarefas)
                if (rowTarefa.Executante == (int)Cliente)
                    return new Tarefa(rowTarefa);
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public dRegistros UltimosRegistros() {
            return dRegistros.dRegistrosSt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Autor"></param>
        /// <param name="Executante"></param>
        /// <param name="Tarefa"></param>
        /// <param name="Parametro"></param>
        /// <param name="ParametroB"></param>
        /// <returns></returns>
        [WebMethod]
        public int AgendaTarefa(Enumeracoes.Clientes Autor, Enumeracoes.Clientes Executante, Enumeracoes.Tarefas Tarefa, string Parametro, byte[] ParametroB) {
            dRegistros.TarefasRow NovaTarefa = dRegistros.dRegistrosSt.Tarefas.NewTarefasRow();
            NovaTarefa.Autor = (int)Autor;
            NovaTarefa.Executante = (int)Executante;
            NovaTarefa.Parametro = Parametro;
            NovaTarefa.Tar = (int)Tarefa;
            NovaTarefa.ParByte = ParametroB;
            dRegistros.dRegistrosSt.Tarefas.AddTarefasRow(NovaTarefa);
            return NovaTarefa.ID;                                                
        }
       

        static private DataSet RetornoDS;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_RetornoDS"></param>
        [WebMethod]
        public void DSGuarda(DataSet _RetornoDS)
        {
            RetornoDS = _RetornoDS;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        [WebMethod]
        public void RemoverTarefa(int ID)
        {
            dRegistros.TarefasRow rowRemover = dRegistros.dRegistrosSt.Tarefas.FindByID(ID);
            if(rowRemover != null){
                rowRemover.Delete();
                dRegistros.dRegistrosSt.Tarefas.AcceptChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public DataSet DSRetorno() {
            DataSet Ret = RetornoDS;
            RetornoDS = null;
            return Ret;
        }
    }
}
