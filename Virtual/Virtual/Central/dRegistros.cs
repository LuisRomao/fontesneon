﻿namespace Central
{


    partial class dRegistros
    {
        private static dRegistros _dRegistrosSt;

        /// <summary>
        /// dataset estático:dRegistros
        /// </summary>
        public static dRegistros dRegistrosSt
        {
            get
            {
                if (_dRegistrosSt == null)
                    _dRegistrosSt = new dRegistros();
                return _dRegistrosSt;
            }
        }
    }
}
