using System;

namespace Central
{
    /// <summary>
    /// Enumerações
    /// </summary>
    public class Enumeracoes
    {
        /// <summary>
        /// Clientes
        /// </summary>
        public enum Clientes
        {
            /// <summary>
            /// Cliente teste
            /// </summary>            
            VirtualTeste,
            /// <summary>
            /// Clintes Virtual
            /// </summary>
            VirtualLap,
            /// <summary>
            /// SBC
            /// </summary>
            NeonSBC,
            /// <summary>
            /// SA
            /// </summary>
            NeonSA,
            /// <summary>
            /// Karina
            /// </summary>
            Karina
        }

        /// <summary>
        /// 
        /// </summary>
        public enum Tarefas : int
        {
            /// <summary>
            /// 
            /// </summary>
            SQL,
            /// <summary>
            /// 
            /// </summary>
            SQLRetorno,
            /// <summary>
            /// 
            /// </summary>
            TempoBusca,
            /// <summary>
            /// 
            /// </summary>
            NovaVersao,
            /// <summary>
            /// 
            /// </summary>
            Status,
            /// <summary>
            /// 
            /// </summary>
            AbortarTroca,
            /// <summary>
            /// 
            /// </summary>
            TrocarModulo,
            /// <summary>
            /// 
            /// </summary>
            Retorno,
            /// <summary>
            /// 
            /// </summary>
            SQL2,
            /// <summary>
            /// 
            /// </summary>
            SQLRetorno2,
        }
        
    }

    /// <summary>
    /// 
    /// </summary>
    public class Tarefa 
    {
        /// <summary>
        /// 
        /// </summary>
        public Enumeracoes.Tarefas Tar;
        /// <summary>
        /// 
        /// </summary>
        public string Parametro;
        /// <summary>
        /// 
        /// </summary>
        public byte[] ParametroB;
        /// <summary>
        /// 
        /// </summary>
        public int ID;
        /// <summary>
        /// 
        /// </summary>
        public Enumeracoes.Clientes Autor;
        /// <summary>
        /// 
        /// </summary>
        public Enumeracoes.Clientes Executante;

        /// <summary>
        /// 
        /// </summary>
        public Tarefa() { }

        /// <summary>
        /// 
        /// </summary>
        public Tarefa(dRegistros.TarefasRow rowTarefa) 
        {
            Tar = (Enumeracoes.Tarefas)rowTarefa.Tar;
            if (!rowTarefa.IsParametroNull())
                Parametro = rowTarefa.Parametro;
            ID = rowTarefa.ID;
            if(!rowTarefa.IsParByteNull())
                ParametroB = rowTarefa.ParByte;
            Autor = (Enumeracoes.Clientes)rowTarefa.Autor;
            Executante = (Enumeracoes.Clientes)rowTarefa.Executante;
        }
    }
}
