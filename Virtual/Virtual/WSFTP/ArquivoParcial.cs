using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;


namespace WSFTP
{
    internal class ArquivoParcial
    {
        public FileStream fs;        
        public int nPacote;
        bool EnviarZipado;
        bool EstocarZipado;
        private string NomeTotalArquivoRec;

        public BinaryReader BR;
        public long Saldo;

        public ArquivoParcial(string NomeTotalArquivo, bool _EnviarZipado, bool _EstocarZipado)
        {
            EnviarZipado = _EnviarZipado;
            EstocarZipado = _EstocarZipado;
            NomeTotalArquivoRec = NomeTotalArquivo;
            nPacote = 1;
        }

        public bool Fecha()
        {
            fs.Close();
            if (!EnviarZipado)
            {
                if (File.Exists(NomeTotalArquivoRec))
                    File.Delete(NomeTotalArquivoRec);
                File.Move(NomeTotalArquivoRec + ".$$$", NomeTotalArquivoRec);
            }
            else
                if (!EstocarZipado)
                {
                    VirZIP.zip.Execute(NomeTotalArquivoRec, NomeTotalArquivoRec + ".vi$", System.IO.Compression.CompressionMode.Decompress);
                    File.Delete(NomeTotalArquivoRec + ".vi$");
                }
                else
                {
                    if (File.Exists(NomeTotalArquivoRec + ".vir"))
                        File.Delete(NomeTotalArquivoRec + ".vir");
                    File.Move(NomeTotalArquivoRec + ".vi$", NomeTotalArquivoRec + ".vir");
                }
            return true;
        }
    }
}
