using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Configuration;
using System.IO;

namespace WSFTP
{
    /// <summary>
    /// Summary description for WSFTP
    /// </summary>
    [WebService(Namespace = "http://virweb.com.br/WSFTP")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]



    public class WSFTP : System.Web.Services.WebService
    {

        [WebMethod]
        public bool HelloWorld(byte[] massa)
        {
            return massa.Length > 1000;
        }

        private const int TamanhoMaxPacote = 0xFFFF;

        private static string Caminho
        {
            get
            {
                return ConfigurationManager.AppSettings["Dados"];
            }
        }
       
        private static SortedList _Arqs;
        private static SortedList Arqs
        {
            get { return _Arqs == null ? _Arqs = new SortedList() : _Arqs; }
        }

        private int numerador=0;

        [WebMethod]
        public string GetAbre(string FileName,out long Tamanho,out bool zipado)
        {
            zipado = true;
            string NomeTotalArquivo = string.Format("{0}{1}.vir", Caminho, FileName);
            if (!File.Exists(NomeTotalArquivo))
            {
                NomeTotalArquivo = string.Format("{0}{1}", Caminho, FileName);
                zipado = false;
                if (!File.Exists(NomeTotalArquivo))
                {
                    Tamanho = 0;
                    return "Arquivo n�o encontrado";
                }
            };
            ArquivoParcial ArqP = new ArquivoParcial(NomeTotalArquivo, zipado, zipado);
            string Codigostr = string.Format("{0:yyyyMMddHHmmss}_{1}", DateTime.Now, numerador++);
            Arqs.Add(Codigostr, ArqP);                                  
            ArqP.fs = new FileStream(NomeTotalArquivo, FileMode.Open);
            ArqP.BR = new BinaryReader(ArqP.fs);
            ArqP.Saldo = Tamanho = ArqP.fs.Length;
            return Codigostr;
        }

        [WebMethod]
        public byte[] GetPacote(string Codigostr, int Pacote)
        {
            ArquivoParcial ArqP = (ArquivoParcial)Arqs[Codigostr];
            if (ArqP == null)
                return null;
            if (ArqP.nPacote != Pacote)
                return null;
            ArqP.nPacote++;            
            int TamanhoPacote = ArqP.Saldo > TamanhoMaxPacote ? TamanhoMaxPacote : (int)ArqP.Saldo;
            byte[] dados = ArqP.BR.ReadBytes(TamanhoPacote);
            ArqP.Saldo -= TamanhoPacote;
            return dados;
        }

        [WebMethod]
        public bool GetFecha(string Codigostr)
        {
            ArquivoParcial ArqP = (ArquivoParcial)Arqs[Codigostr];
            if (ArqP == null)
                return false;
            ArqP.fs.Close();
            Arqs.RemoveAt(Arqs.IndexOfKey(Codigostr));
            return (ArqP.Saldo == 0);
        }

        [WebMethod]
        public string PutAbre(string FileName,long Tamanho,bool EnviarZipado,bool EstocarZipado)
        {                       
            string NomeTotalArquivo = string.Format("{0}{1}",Caminho,FileName);
            ArquivoParcial ArqP = new ArquivoParcial(NomeTotalArquivo, EnviarZipado, EstocarZipado);
            string Codigostr = string.Format("{0:yyyyMMddHHmmss}_{1}", DateTime.Now, numerador++);
            Arqs.Add(Codigostr, ArqP);
            if(EnviarZipado)
                NomeTotalArquivo = NomeTotalArquivo + ".vi$";
            else            
                NomeTotalArquivo = NomeTotalArquivo + ".$$$";
            string Pasta = System.IO.Path.GetDirectoryName(NomeTotalArquivo);
            if (!Directory.Exists(Pasta))
                Directory.CreateDirectory(Pasta);
            ArqP.fs = new FileStream(NomeTotalArquivo, FileMode.Create);
            
            return Codigostr;
        }

        [WebMethod]
        public int PutPacote(string Codigostr, int Pacote,byte[] dados)
        {
            ArquivoParcial ArqP = (ArquivoParcial)Arqs[Codigostr];
            if (ArqP == null)
                return -1;
            if (ArqP.nPacote != Pacote)
                return -1;
            ArqP.nPacote++;
            using (MemoryStream ms = new MemoryStream(dados))
                ms.WriteTo(ArqP.fs);
            return ArqP.nPacote;
        }

        [WebMethod]
        public bool PutFecha(string Codigostr)
        {
            ArquivoParcial ArqP = (ArquivoParcial)Arqs[Codigostr];
            if (ArqP == null)
                return false;
            bool retorno = ArqP.Fecha();
            Arqs.RemoveAt(Arqs.IndexOfKey(Codigostr));
            return retorno;
        }
    }
}
