﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18051
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WSFTPdll.WcfFTP {
    using System.Runtime.Serialization;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Tipoenvio", Namespace="http://schemas.datacontract.org/2004/07/WcfFTP35")]
    public enum Tipoenvio : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        envio_com_compressao = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        envio_sem_compressao = 0,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TipoArmazenagem", Namespace="http://schemas.datacontract.org/2004/07/WcfFTP35")]
    public enum TipoArmazenagem : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Armazenagem_com_compressao = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Armazenagem_sem_compressao = 0,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://virweb.com.br/WcfFTP", ConfigurationName="WcfFTP.IServiceFTP")]
    public interface IServiceFTP {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://virweb.com.br/WcfFTP/IServiceFTP/Eco", ReplyAction="http://virweb.com.br/WcfFTP/IServiceFTP/EcoResponse")]
        string Eco(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://virweb.com.br/WcfFTP/IServiceFTP/TestePacote", ReplyAction="http://virweb.com.br/WcfFTP/IServiceFTP/TestePacoteResponse")]
        long TestePacote(byte[] massa);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://virweb.com.br/WcfFTP/IServiceFTP/PutAbre", ReplyAction="http://virweb.com.br/WcfFTP/IServiceFTP/PutAbreResponse")]
        string PutAbre(string FileName, long Tamanho, WSFTPdll.WcfFTP.Tipoenvio EnviarZipado, WSFTPdll.WcfFTP.TipoArmazenagem EstocarZipado);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://virweb.com.br/WcfFTP/IServiceFTP/PutPacote", ReplyAction="http://virweb.com.br/WcfFTP/IServiceFTP/PutPacoteResponse")]
        int PutPacote(string Codigostr, int Pacote, byte[] dados);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://virweb.com.br/WcfFTP/IServiceFTP/PutFecha", ReplyAction="http://virweb.com.br/WcfFTP/IServiceFTP/PutFechaResponse")]
        bool PutFecha(string Codigostr);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://virweb.com.br/WcfFTP/IServiceFTP/GetAbre", ReplyAction="http://virweb.com.br/WcfFTP/IServiceFTP/GetAbreResponse")]
        string GetAbre(out long Tamanho, out bool zipado, string FileName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://virweb.com.br/WcfFTP/IServiceFTP/GetPacote", ReplyAction="http://virweb.com.br/WcfFTP/IServiceFTP/GetPacoteResponse")]
        byte[] GetPacote(string Codigostr, int Pacote);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://virweb.com.br/WcfFTP/IServiceFTP/GetFecha", ReplyAction="http://virweb.com.br/WcfFTP/IServiceFTP/GetFechaResponse")]
        bool GetFecha(string Codigostr);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceFTPChannel : WSFTPdll.WcfFTP.IServiceFTP, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceFTPClient : System.ServiceModel.ClientBase<WSFTPdll.WcfFTP.IServiceFTP>, WSFTPdll.WcfFTP.IServiceFTP {
        
        public ServiceFTPClient() {
        }
        
        public ServiceFTPClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceFTPClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceFTPClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceFTPClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string Eco(int value) {
            return base.Channel.Eco(value);
        }
        
        public long TestePacote(byte[] massa) {
            return base.Channel.TestePacote(massa);
        }
        
        public string PutAbre(string FileName, long Tamanho, WSFTPdll.WcfFTP.Tipoenvio EnviarZipado, WSFTPdll.WcfFTP.TipoArmazenagem EstocarZipado) {
            return base.Channel.PutAbre(FileName, Tamanho, EnviarZipado, EstocarZipado);
        }
        
        public int PutPacote(string Codigostr, int Pacote, byte[] dados) {
            return base.Channel.PutPacote(Codigostr, Pacote, dados);
        }
        
        public bool PutFecha(string Codigostr) {
            return base.Channel.PutFecha(Codigostr);
        }
        
        public string GetAbre(out long Tamanho, out bool zipado, string FileName) {
            return base.Channel.GetAbre(out Tamanho, out zipado, FileName);
        }
        
        public byte[] GetPacote(string Codigostr, int Pacote) {
            return base.Channel.GetPacote(Codigostr, Pacote);
        }
        
        public bool GetFecha(string Codigostr) {
            return base.Channel.GetFecha(Codigostr);
        }
    }
}
