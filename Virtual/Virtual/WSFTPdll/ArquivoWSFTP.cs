using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace WSFTPdll
{
    

    public class ArquivoWSFTP
    {

        private class itAgenda
        {
            public string FileName;
            public Stream File; 
            public string NomeRemoto; 
            public TipoArmazenagem EstocarZipado;
            public Tipoenvio EnviarZipado;

            public itAgenda(string FileName, Stream File, string NomeRemoto, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado)
            {
                this.FileName = FileName;
                this.File = File;
                this.NomeRemoto = NomeRemoto;
                this.EnviarZipado = EnviarZipado;
                this.EstocarZipado = EstocarZipado;
            }
        }

        private List<itAgenda> Agenda;

        private static WSHttpBinding ConfigwsHttpBinding()
        {
            // ----- Programmatic definition of the SomeService Binding -----
            WSHttpBinding wsHttpBinding = new WSHttpBinding();
            wsHttpBinding.Name = "WSHttpBinding_IServiceFTP";
            wsHttpBinding.CloseTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.OpenTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            wsHttpBinding.SendTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.BypassProxyOnLocal = false;
            wsHttpBinding.TransactionFlow = false;
            wsHttpBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            wsHttpBinding.MaxBufferPoolSize = 2147483647;
            wsHttpBinding.MaxReceivedMessageSize = 2147483647;
            wsHttpBinding.MessageEncoding = WSMessageEncoding.Text;
            wsHttpBinding.TextEncoding = Encoding.UTF8;
            wsHttpBinding.UseDefaultWebProxy = true;
            wsHttpBinding.AllowCookies = false;
            wsHttpBinding.ReaderQuotas.MaxDepth = 32;
            wsHttpBinding.ReaderQuotas.MaxArrayLength = 2147483647;
            wsHttpBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
            wsHttpBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
            wsHttpBinding.ReaderQuotas.MaxNameTableCharCount = 16384;
            wsHttpBinding.ReliableSession.Ordered = true;
            wsHttpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromMinutes(10);
            wsHttpBinding.ReliableSession.Enabled = false;
            wsHttpBinding.Security.Mode = SecurityMode.None;            
            // ----------- End Programmatic definition of the SomeServiceServiceBinding --------------
            return wsHttpBinding;

        }


        private bool Assincrono = false;

        private WcfFTP.ServiceFTPClient Wcf;

        private Thread ThRecFTP;

        /// <summary>
        /// Cria um novo mecanismo de envio/rec de arquivos
        /// </summary>
        /// <param name="URL">URL do svc (completo com o .svc)</param>
        public ArquivoWSFTP(string URL,bool Sincrono = true)
        {
            Assincrono = !Sincrono;
            Wcf = new WcfFTP.ServiceFTPClient(ConfigwsHttpBinding(), new EndpointAddress(URL));            
            //WS.Proxy
            Agenda = new List<itAgenda>();
            if (!Sincrono)
            {
                parar = false;
                ThRecFTP = new Thread(LoopAssicrono);
                PutAtivo = true;
                ThRecFTP.Start();
            }

        }

        

        private const int TamanhoMaxPacote = 0xFFFF;

        public bool parar;

        private void LoopAssicrono()
        {
            while (!parar)
            {
                if (Agenda.Count > 0)
                {
                    ThRecFTP.Priority = ThreadPriority.Normal;
                    itAgenda DaVez = Agenda[0];
                    Agenda.Remove(DaVez);
                    if (DaVez.FileName != null)
                    {
                        if (DaVez.NomeRemoto == null)
                            DaVez.NomeRemoto = Path.GetFileName(DaVez.FileName);
                        using (FileStream fs = new FileStream(DaVez.FileName, FileMode.Open))
                        {
                            PutEfetivo(fs, DaVez.NomeRemoto, DaVez.EnviarZipado, DaVez.EstocarZipado);
                        }
                    }
                    else
                        PutEfetivo(DaVez.File, DaVez.NomeRemoto, DaVez.EnviarZipado, DaVez.EstocarZipado);
                }
                else
                {
                    Thread.Sleep(1000);
                    ThRecFTP.Priority = ThreadPriority.Lowest;
                }

            }
        }

        #region PUT

        public bool Put(string FileName, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado = TipoArmazenagem.Armazenagem_sem_compressao)
        {
            return Put(FileName, null, EnviarZipado, EstocarZipado);
        }

        /// <summary>
        /// Envia Arquivo
        /// </summary>
        /// <param name="FileName">Nome do arquivo</param>
        /// <param name="NomeRemoto">Nome que ser� dado ao arquivo remoto</param>
        /// <param name="EnviarZipado">Envia zipado</param>
        /// <param name="EstocarZipado">Guardar zipado</param>
        /// <returns></returns>
        public bool Put(string FileName, string NomeRemoto = null, Tipoenvio EnviarZipado = Tipoenvio.envio_com_compressao, TipoArmazenagem EstocarZipado = TipoArmazenagem.Armazenagem_sem_compressao)
        {
            if (Assincrono)
            {
                Agenda.Add(new itAgenda(FileName, null, NomeRemoto, EnviarZipado, EstocarZipado));
                return true;
            }
            else
            {
                bool retorno = false;
                if (NomeRemoto == null)
                    NomeRemoto = Path.GetFileName(FileName);
                using (FileStream fs = new FileStream(FileName, FileMode.Open))
                {
                    retorno = Put(fs, NomeRemoto, EnviarZipado, EstocarZipado);
                }
                return retorno;
            }
        }

        public enum Tipoenvio { envio_com_compressao = 1,envio_sem_compressao=0}

        public enum TipoArmazenagem { Armazenagem_com_compressao = 1,Armazenagem_sem_compressao=0}

        public bool Put (Stream File, string NomeRemoto, Tipoenvio EnviarZipado = Tipoenvio.envio_com_compressao, TipoArmazenagem EstocarZipado = TipoArmazenagem.Armazenagem_sem_compressao)
        {
            if (Assincrono)
            {
                Agenda.Add(new itAgenda(null,File,NomeRemoto,EnviarZipado,EstocarZipado));
                return true;
            }
            else
                return PutEfetivo(File, NomeRemoto, EnviarZipado, EstocarZipado);
        }

        /// <summary>
        /// Envia Arquivo
        /// </summary>
        /// <param name="File">Stream com o arquivo</param>
        /// <param name="NomeArquivo">Nome que ser� dado ao arquivo remoto</param>
        /// <param name="EnviarZipado">Envia zipado</param>
        /// <param name="EstocarZipado">Guardar zipado</param>
        /// <returns></returns>
        private bool PutEfetivo(Stream File, string NomeRemoto, Tipoenvio EnviarZipado,TipoArmazenagem EstocarZipado)
        {

            if (EnviarZipado == Tipoenvio.envio_com_compressao)
            {
                MemoryStream MSZ = new MemoryStream();
                VirZIP.zip.Execute(File, MSZ, System.IO.Compression.CompressionMode.Compress);
                //MSZ.Position = 0;
                if (MSZ.Length < File.Length)
                {
                    st = MSZ;
                    File.Close();                    
                }
                else
                {
                    EnviarZipado = Tipoenvio.envio_sem_compressao;
                    st = File;
                }
                //st.Position = 0;
            }
            else
                st = File;
            st.Position = 0;

            if (EnviarZipado == Tipoenvio.envio_sem_compressao)
                EstocarZipado = TipoArmazenagem.Armazenagem_sem_compressao;
            
            Saldo = TotalPut = st.Length;

            bool Retorno = false;
            Codigostr = null;
            try
            {

                Codigostr = Wcf.PutAbre(NomeRemoto, Saldo, (WcfFTP.Tipoenvio)EnviarZipado, (WcfFTP.TipoArmazenagem)EstocarZipado);
                byte[] pacote;

                using (BinaryReader BR = new BinaryReader(st))
                {
                    int i = 1;
                    while (Saldo > 0)
                    {
                        int TamanhoPacote = Saldo > TamanhoMaxPacote ? TamanhoMaxPacote : (int)Saldo;
                        Saldo -= TamanhoPacote;
                        pacote = BR.ReadBytes(TamanhoPacote);
                        Wcf.PutPacote(Codigostr, i, pacote);
                        i++;
                    }
                }
                
                Retorno = true;
            }
            finally
            {
                if (Codigostr != null)
                    Wcf.PutFecha(Codigostr);
            }
            return Retorno;                       
        }
               

        #endregion

        #region GET
        
               
        /// <summary>
        /// Recebe um arquivo
        /// </summary>
        /// <param name="FileName">Nome do arquivo</param>
        /// <param name="NomeRemoto">Nome do arquivo remoto</param>        
        /// <returns></returns>
        public bool Get(string FileName, string NomeRemoto = null)
        {
            bool retorno = false;
            if (NomeRemoto == null)
                NomeRemoto = Path.GetFileName(FileName);
            if (pathTMP == null)
                pathTMP = Path.GetDirectoryName(FileName);
            Stream fs = new FileStream(FileName, FileMode.Create);
            retorno = Get(ref fs, NomeRemoto);
            fs.Close();
            return retorno;
        }

        public string pathTMP = null;
        private int numerador = 0;
        
        /// <summary>
        /// Recebe um arquivo
        /// </summary>
        /// <param name="File">Stream com o arquivo - pode ser null</param>
        /// <param name="NomeRemoto">Nome do arquivo remoto</param>       
        /// <returns></returns>
        public bool Get(ref Stream File, string NomeRemoto)
        {
            string ArquivoTMP = null;
            Codigostr = Wcf.GetAbre(out TotalPut, out zipado, NomeRemoto);
            if (TotalPut == 0)
            {
                UltimoErro = Codigostr;
                return false;
            }
            Saldo = TotalPut;

            if (File == null)
                st = new MemoryStream();
            else
                st = File;

            if (zipado)
            {
                stRetorno = st;
                if (TotalPut < 100000000)
                    st = new MemoryStream();
                else
                {                    
                    ArquivoTMP = string.Format("{0}\\{1:yyyyMMddHHmmss}_{2}.$$$",pathTMP, DateTime.Now, numerador++);
                    st = new FileStream(ArquivoTMP, FileMode.Create);
                }
            }          

            //if (Assincrono)
            //{
            //    Thread ThRecFTP = new Thread(Getth);
            //    PutAtivo = true;
            //    ThRecFTP.Start();                
            //    return true;
            //}
            //else
            //{
            try
            {
                ResultadoPut = false;
                int i = 1;
                while (Saldo > 0)
                {
                    byte[] pacote = Wcf.GetPacote(Codigostr, i++);
                    if (pacote == null)
                        break;
                    Saldo -= pacote.Length;
                    using (MemoryStream ms = new MemoryStream(pacote))
                        ms.WriteTo(st);
                };
                if (Saldo == 0)
                {
                    ResultadoPut = Wcf.GetFecha(Codigostr);
                    if (zipado)
                    {
                        st.Position = 0;
                        VirZIP.zip.Execute(stRetorno, st, System.IO.Compression.CompressionMode.Decompress);
                        st.Close();                        
                        st = null;
                        //st = null;
                    }
                    else
                    {
                        //st.Close();
                    }
                }
            }
            finally
            {
                PutAtivo = false;
                st = null;
                stRetorno = null;
                if((ArquivoTMP != null) && System.IO.File.Exists(ArquivoTMP))
                    System.IO.File.Delete(ArquivoTMP);
            }
            return ResultadoPut;                        
        }
        
        

        

        #endregion

        private Stream st;
        private Stream stRetorno;
        private bool ResultadoPut;
        public long TotalPut;
        public long Saldo;
        private string Codigostr;
        public bool PutAtivo;
        public string UltimoErro = "";
        bool zipado;
    }
}
