using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Net;
using System.Threading;
using System.Timers;
using proxyvirweb.Centra35;
using TiposVirtual;
using VirCrip;
using VirDB;

namespace proxyvirweb
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cVirtualProxy : Component
    {
        private string repositorio = @"c:\Repositorio";

        /// <summary>
        /// 
        /// </summary>
        public VirtualTableAdapter VirtualTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public VirtualTableAdapter VirtualTableAdapter2;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Reposit�rio")]
        public string Repositorio
        {
            get
            {
                return repositorio;
            }
            set
            {
                repositorio = value;
            }
        }
        private string tMP = @"c:\tmp\";


        private bool _ModoPassivo = true;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Modo Passivo")]
        public bool ModoPassivo
        {
            get
            {
                return _ModoPassivo;
            }
            set
            {
                if (_ModoPassivo == value)
                    return;
                _ModoPassivo = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Diretorio TMP")]
        public string TMP
        {
            get
            {
                return tMP;
            }
            set
            {
                tMP = value;
            }
        }
        private Clientes _Cliente;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double IntervaloAtual()
        {
            return TimerProxy.Interval;
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Cliente")]
        public Clientes Cliente {
            get { return _Cliente; }
            set { _Cliente = value; }
        }

        private System.Timers.Timer TimerProxy;

        private Central _Central;

        private WebProxy _Proxy;

        /// <summary>
        /// 
        /// </summary>
        public WebProxy Proxy
        {
            set { _Proxy = value; }
        }                

        private Centra35.Central Central
        {
            get
            {
                if (_Central == null)
                {
                    _Central = new Central();
                    _Central.Url = "http://virweb.com.br.windflower.arvixe.com/central/central.asmx";
                    if (_Proxy != null)
                        _Central.Proxy = _Proxy;
                    //_Central.Url = 
                    //_Central.Proxy = new System.Net.WebProxy("10.135.1.20", 8080);
                    //_Central.Proxy.Credentials = new System.Net.NetworkCredential("publicacoes", "neon01");
                }
                return _Central;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_VirtualTableAdapter"></param>
        /// <param name="_VirtualTableAdapter2"></param>
        public void Ativar(VirtualTableAdapter _VirtualTableAdapter, VirtualTableAdapter _VirtualTableAdapter2)
        {
            VirtualTableAdapter = _VirtualTableAdapter;
            VirtualTableAdapter2 = _VirtualTableAdapter2;
            ProcessaChamada();
            TimerProxy.Enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_VirtualTableAdapter"></param>
        public void Ativar(VirtualTableAdapter _VirtualTableAdapter)
        {
            VirtualTableAdapter = _VirtualTableAdapter;
            ProcessaChamada();
            TimerProxy.Enabled = true;
        }

        private void BaixaFTP(string NomeBase, string arquivoRec)
        {                       
            WebProxy PRX;
            if (_Proxy == null)
                PRX = new WebProxy();
            else
                PRX = _Proxy;
            PRX.UseDefaultCredentials = true;
            NetworkCredential Credenciais = new NetworkCredential("virweb", "venussaulo1000");                        
            Tentativas = 10;
            byte[] buffer = new byte[1024];
            int bytesRead;            
            while ((!AbortarTroca) && (Tentativas > 0))
            {                
                FtpWebResponse _FTPResponse = null;
                try
                {
                    Uri _URIx = new Uri(String.Format("ftp://ftp.windflower.arvixe.com/{0}", Path.GetFileName(arquivoRec)));
                    //Uri _URIx = new Uri(String.Format("ftp://ftp.virweb.com.br/Nova/{0}"   , Path.GetFileName(arquivoRec)));
                    FtpWebRequest _FTPRequest = ((FtpWebRequest)(WebRequest.Create(_URIx)));
                    _FTPRequest.Credentials = Credenciais;
                    _FTPRequest.Proxy = PRX;
                    _FTPRequest.UseBinary = true;
                    _FTPRequest.KeepAlive = true;
                    _FTPRequest.UsePassive = ModoPassivo;
                    _FTPRequest.Method = WebRequestMethods.Ftp.GetFileSize;
                    _FTPResponse = ((FtpWebResponse)(_FTPRequest.GetResponse()));
                    TamanhoEsperado = _FTPResponse.ContentLength;                    
                    _FTPRequest = ((FtpWebRequest)(WebRequest.Create(_URIx)));
                    _FTPRequest.Credentials = Credenciais;
                    _FTPRequest.Proxy = PRX;
                    _FTPRequest.UseBinary = true;
                    _FTPRequest.KeepAlive = true;
                    _FTPRequest.UsePassive = ModoPassivo;
                    _FTPRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                    _FTPResponse = ((FtpWebResponse)(_FTPRequest.GetResponse()));

                    Stream _ResponseStream = _FTPResponse.GetResponseStream();
                    using (FileStream _StreamWriter = new FileStream(String.Format(@"{0}{1}\{2}", TMP, NomeBase, arquivoRec), FileMode.Create, FileAccess.Write))
                    {
                        Lido = 0;
                        bytesRead = _ResponseStream.Read(buffer, 0, 1024);
                        while (bytesRead != 0)
                        {
                            Lido += bytesRead;
                            if (AbortarTroca)
                                break;
                            _StreamWriter.Write(buffer, 0, bytesRead);
                            if (TamanhoEsperado > Lido)
                                bytesRead = _ResponseStream.Read(buffer, 0, 1024);
                            else
                                bytesRead = 0;
                        }
                        ;
                        _StreamWriter.Flush();
                        _StreamWriter.Close();
                    }
                    _ResponseStream.Close();

                    Tentativas = 0;
                }
                catch (Exception e)
                {
                    HistoricoProxy = string.Format("Erro FTP:\r\nClasse{0}\r\nMensagem{1}\r\n**********\r\n{2}\r\n**********\r\n",
                             e.GetType(),
                             e.Message,
                             e.StackTrace);
                    Tentativas--;
                    if (Tentativas == 0)
                        AbortarTroca = true;
                    DateTime Espera = DateTime.Now.AddSeconds(10);
                    while (DateTime.Now < Espera) ;                    
                }
                finally
                {
                    if (_FTPResponse != null)
                        _FTPResponse.Close();
                    TamanhoEsperado = Lido = 0;
                }
            };
            
            //if (Abortado) { }
            //MostraLog("Abortado");
            //else
            //{
                //MostraLog("FTP conclu�do");
                //Central.AgendaTarefa(Virtual.br.com.locaweb.ssl504.Clientes.VirtualLap, ClientePublicacao, Virtual.br.com.locaweb.ssl504.Tarefas.NovaVersao, NomeBase);
            //}
            //FimFTP();
        }

        int ArquivosFTP;
        int RecebidosFTP;
        Int64 TamanhoEsperado;
        Int64 Lido;
        int Tentativas;
        bool AbortarTroca;
        string UltimoErro = "";
        DateTime UltimaChamada;
        string[] _HistoricoProxy = new string[10];
 
        string HistoricoProxy
        {
            get 
            {
                string Retorno = "";
                Array.ForEach(_HistoricoProxy, delegate(string linha) { Retorno += (linha == null ? "" : linha + Environment.NewLine); });
                return Retorno;
            }
            set 
            {
                for (int i = 0; i < 9; i++)
                    _HistoricoProxy[i] = _HistoricoProxy[i + 1];
                _HistoricoProxy[9] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string StatusFTP() 
        {
            if (ArquivosFTP == 0)
                return "Inativo";
            else
            {
                int Divisor;
                string Unidade;
                if (TamanhoEsperado > (1024 * 1024)) 
                {
                    Divisor = 1024 * 1024;
                    Unidade = "M";
                }
                else
                    if (TamanhoEsperado > 1024)
                    {
                        Divisor = 1024;
                        Unidade = "K";
                    }
                    else
                    {
                        Divisor = 1;
                        Unidade = "bytes";
                    }
                return string.Format("Recebendo {0}/{1} arquivos - {2:n2} {4}/{3:n2} {4} - Erros:{5}",                    
                    RecebidosFTP,
                    ArquivosFTP,
                    Lido/Divisor,
                    TamanhoEsperado/Divisor,
                    Unidade,
                    10 - Tentativas);
            }
        }

        private void RecebeFTPth(object oParametro)
        {
            object[] oParametros = (object[])oParametro;
            string NomeBase = (string)oParametros[0];
            Tarefa Tarefa = (Tarefa)oParametros[1];
            int ID = Tarefa.ID;
            if (!Directory.Exists(TMP + NomeBase))
                Directory.CreateDirectory(TMP + NomeBase);
            BaixaFTP(NomeBase, String.Format("{0}.xml", NomeBase));
            if (AbortarTroca)
                return;
            using (dVersao dVersao = new dVersao())
            {
                dVersao.ReadXml(String.Format("{0}{1}\\{1}.xml", TMP, NomeBase));
                ArquivosFTP = dVersao.PacoteAtualizacao.Count;
                RecebidosFTP = 0;
                foreach (dVersao.PacoteAtualizacaoRow row in dVersao.PacoteAtualizacao) 
                {
                    if (AbortarTroca)
                        break;
                    BaixaFTP(NomeBase, row.NomeVir);
                    RecebidosFTP++;
                };
                ArquivosFTP = RecebidosFTP = 0;
                if (AbortarTroca)
                    return;
                if (!Directory.Exists(Repositorio))
                    Directory.CreateDirectory(Repositorio);
                if (!Directory.Exists(String.Format("{0}\\{1}\\Novos", Repositorio, NomeBase)))
                    Directory.CreateDirectory(String.Format("{0}\\{1}\\Novos", Repositorio, NomeBase));
                if (!Directory.Exists(String.Format("{0}\\{1}\\Antigos", Repositorio, NomeBase)))
                    Directory.CreateDirectory(String.Format("{0}\\{1}\\Antigos", Repositorio, NomeBase));
                foreach (dVersao.PacoteAtualizacaoRow row in dVersao.PacoteAtualizacao)
                {
                    string nZipado = String.Format("{0}{1}\\{2}", TMP, NomeBase, row.NomeVir);
                    string nUnZipado = String.Format("{0}\\{1}\\Novos{2}\\{3}", Repositorio, NomeBase,row.Diretorio,row.NomeOrig);
                    if (!Directory.Exists(String.Format("{0}\\{1}\\Novos{2}", Repositorio, NomeBase, row.Diretorio)))
                        Directory.CreateDirectory(String.Format("{0}\\{1}\\Novos{2}", Repositorio, NomeBase, row.Diretorio));
                    if (!Directory.Exists(String.Format("{0}\\{1}\\Antigos{2}", Repositorio, NomeBase, row.Diretorio)))
                        Directory.CreateDirectory(String.Format("{0}\\{1}\\Antigos{2}", Repositorio, NomeBase, row.Diretorio));
                    if (!Directory.Exists(String.Format("{0}{1}", Repositorio, row.Diretorio)))
                        Directory.CreateDirectory(String.Format("{0}{1}", Repositorio, row.Diretorio));
                    VirZIP.zip.Execute(nUnZipado, nZipado, System.IO.Compression.CompressionMode.Decompress);
                };
                foreach (dVersao.PacoteAtualizacaoRow row in dVersao.PacoteAtualizacao)
                {                    
                    string Nova    = String.Format("{0}\\{1}\\Novos{2}\\{3}", Repositorio, NomeBase, row.Diretorio, row.NomeOrig);
                    string Ativa   = String.Format("{0}{1}\\{2}", Repositorio, row.Diretorio, row.NomeOrig);
                    string Reserva = String.Format("{0}\\{1}\\Antigos{2}\\{3}", Repositorio, NomeBase, row.Diretorio, row.NomeOrig);
                    if (File.Exists(Ativa))
                        if (!File.Exists(Reserva))
                            File.Move(Ativa, Reserva);
                        else
                            File.Delete(Ativa);
                    File.Copy(Nova, Ativa);
                };
            };
            Central.RemoverTarefa(ID);
            if (IDsBloc.Contains(Tarefa.ID))
                IDsBloc.Remove(Tarefa.ID);
        }

        private System.Collections.ArrayList IDsBloc = new System.Collections.ArrayList();

        private DataSet BuscaSQL(string comando, int N)
        {
            VirtualTableAdapter VirtualTableAdapterX = (N == 1) ? VirtualTableAdapter : VirtualTableAdapter2;
            if (VirtualTableAdapterX == null)
                throw new Exception("Vari�vel VirtualTableAdapter n�o definida");
            DataSet NDS = new DataSet();
            DataTable NTabela = VirtualTableAdapterX.BuscaSQLTabela(comando);
            if (NTabela != null)
            {
                NDS.Tables.Add(NTabela);
                return NDS;
            }
            return null;
        }

        private bool ExecutaSQL(string comando, int N)
        {
            VirtualTableAdapter VirtualTableAdapterX = (N == 1) ? VirtualTableAdapter : VirtualTableAdapter2;
            if (VirtualTableAdapterX == null)
                throw new Exception("Vari�vel VirtualTableAdapter n�o definida");
            return (VirtualTableAdapterX.ExecutarSQLNonQuery(comando) != 0);
        }
        
        /// <summary>
        /// Processa a chamada
        /// </summary>
        public void ProcessaChamada()
        {
            try
            {
                Tarefa Tarefa = Central.Ping(_Cliente);
                if (Tarefa != null)
                    switch (Tarefa.Tar)
                    {
                        case Tarefas.NovaVersao:
                            if (!IDsBloc.Contains(Tarefa.ID))
                            {
                                AbortarTroca = false;
                                IDsBloc.Add(Tarefa.ID);
                                Thread ThRecebeFTP = new Thread(RecebeFTPth);
                                ThRecebeFTP.Start(new object[] { Tarefa.Parametro, Tarefa });
                                HistoricoProxy = string.Format("{0} - Vers�o", DateTime.Now);
                                Central.AgendaTarefa(Cliente, Tarefa.Autor, Tarefas.Retorno, string.Format("\r\n{0} Inicio FTP ", DateTime.Now), null);
                            }
                            else
                                Central.AgendaTarefa(Cliente, Tarefa.Autor, Tarefas.Retorno, string.Format("\r\n{0} FTP: {1}", DateTime.Now, StatusFTP()), null);
                            break;
                        case Tarefas.SQL:
                            Central.AgendaTarefa(Cliente, Tarefa.Autor, Tarefas.Retorno, ExecutaSQL(VirCripWS.Uncrip(Tarefa.ParametroB), 1).ToString(), null);
                            HistoricoProxy = string.Format("{0} - SQL", DateTime.Now);
                            Central.RemoverTarefa(Tarefa.ID);
                            break;
                        case Tarefas.SQLRetorno:
                            DataSet Retornar = BuscaSQL(VirCripWS.Uncrip(Tarefa.ParametroB), 1);
                            if (Retornar != null)
                            {
                                Central.DSGuarda(Retornar);
                                Central.RemoverTarefa(Tarefa.ID);
                            };
                            HistoricoProxy = string.Format("{0} - SQL com retorno", DateTime.Now);
                            break;
                        case Tarefas.SQL2:
                            Central.AgendaTarefa(Cliente, Tarefa.Autor, Tarefas.Retorno, ExecutaSQL(VirCripWS.Uncrip(Tarefa.ParametroB), 2).ToString(), null);
                            HistoricoProxy = string.Format("{0} - SQL", DateTime.Now);
                            Central.RemoverTarefa(Tarefa.ID);
                            break;
                        case Tarefas.SQLRetorno2:
                            DataSet Retornar2 = BuscaSQL(VirCripWS.Uncrip(Tarefa.ParametroB), 2);
                            if (Retornar2 != null)
                            {
                                Central.DSGuarda(Retornar2);
                                Central.RemoverTarefa(Tarefa.ID);
                            };
                            HistoricoProxy = string.Format("{0} - SQL com retorno", DateTime.Now);
                            break;
                        case Tarefas.TempoBusca:
                            double NovoTempo;
                            if (double.TryParse(Tarefa.Parametro, out NovoTempo))
                                TimerProxy.Interval = NovoTempo * 60000;
                            Central.RemoverTarefa(Tarefa.ID);
                            HistoricoProxy = string.Format("{0} - Aj Timer {1}", DateTime.Now, NovoTempo);
                            break;
                        case Tarefas.AbortarTroca:
                            AbortarTroca = true;
                            UltimoErro += string.Format("{0:dd/MM/yyyy HH:mm:ss} Abortado FTP", DateTime.Now);
                            HistoricoProxy = string.Format("{0} - Aborta Troca", DateTime.Now);
                            IDsBloc.Clear();
                            break;
                        case Tarefas.Retorno:
                            Central.RemoverTarefa(Tarefa.ID);
                            if (TarefaAplicativo != null)
                                TarefaAplicativo(Tarefa.Parametro);
                            HistoricoProxy = string.Format("{0} - Retorno", DateTime.Now);
                            break;
                        case Tarefas.Status:
                            Central.AgendaTarefa(Cliente, Tarefa.Autor, Tarefas.Retorno, StatusProxy(), null);
                            Central.RemoverTarefa(Tarefa.ID);
                            HistoricoProxy = string.Format("{0} - Status", DateTime.Now);
                            break;
                        case Tarefas.TrocarModulo:
                            Central.RemoverTarefa(Tarefa.ID);
                            if (TrocaModulo != null)
                                TrocaModulo(this, new EventArgs());
                            HistoricoProxy = string.Format("{0} - Troca M�dulo", DateTime.Now);
                            break;
                        default:
                            HistoricoProxy = string.Format("{0} - default", DateTime.Now);
                            break;
                    }
                else
                    HistoricoProxy = string.Format("{0} - Sem tarefa", DateTime.Now);
            }
            catch (WebException e)
            {
                HistoricoProxy = string.Format("{0} - Erro de acesso:{1}", DateTime.Now, e.Message);
            }
            catch (Exception e)
            {
                HistoricoProxy = string.Format("Erro n�o tratado:\r\nClasse{0}\r\nMensagem{1}\r\n**********\r\n{2}\r\n**********\r\n",
                    e.GetType(),
                    e.Message,
                    e.StackTrace);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string StatusProxy() 
        {
            return string.Format("Status FTP:{0}\r\nUltimo erro:\r\n{1}\r\nStatus Aplicativo:\r\n{2}\r\nHistorico\r\n{3}\r\nProxima Chamada:{4:dd/MM/yyyy HH:mm:ss}",
                            StatusFTP(),
                            UltimoErro,
                            StatusAplicativo == null ? "-" : StatusAplicativo(),
                            HistoricoProxy,
                            UltimaChamada.AddMilliseconds(TimerProxy.Interval));
        }

        void Chamada(object sender, ElapsedEventArgs e)
        {
            UltimaChamada = DateTime.Now;           
            ProcessaChamada();            
        }
        #region Callback e Eventos

        /// <summary>
        /// delegate para callback 
        /// </summary>
        /// <returns></returns>
        public delegate string delStatusAplicativo();

        /// <summary>
        /// Callback (registrar um m�dodo a ser chamado
        /// </summary>
        public delStatusAplicativo StatusAplicativo;


        /// <summary>
        /// delegate para callback
        /// </summary>
        /// <param name="Parametro"></param>
        /// <returns></returns>
        public delegate void delTarefaAplicativo(string Parametro);

        /// <summary>
        /// Callback (registrar um m�dodo a ser chamado
        /// </summary>
        public delTarefaAplicativo TarefaAplicativo;

        /// <summary>
        /// Evento para troca do m�dulo
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento para troca do m�dulo")]
        public event EventHandler TrocaModulo; 
        #endregion

        /// <summary>
        /// Envia retorno para central
        /// </summary>
        /// <param name="Retornar"></param>
        /// <returns></returns>
        public int RetornarCentral(string Retornar) 
        {
            return Central.AgendaTarefa(Cliente,Clientes.VirtualLap , Tarefas.Retorno, Retornar, null);
        }

        /*
        public cVirtualProxy()
        {
            InitializeComponent();
            TimerProxy = new Timer(30000);
            TimerProxy.Elapsed += new ElapsedEventHandler(Chamada);
        }*/

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="container"></param>
        public cVirtualProxy(IContainer container)
        {
            container.Add(this);
            Inicializacao(60000 * 15);
        }

        private void Inicializacao(int TempoInicial)
        {
            InitializeComponent();
            TimerProxy = new System.Timers.Timer(TempoInicial);
            TimerProxy.Elapsed += Chamada;
            UltimaChamada = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public cVirtualProxy()
        {
            Inicializacao(60000 * 15);
        }
         
    }

   
}
