using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace WSTeste
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /*
        public static ContractDescription ConfigContractDescription()
        {
            // ----- Programmatic definition of the Service ContractDescription Binding -----
            ContractDescription Contract = ContractDescription.GetContract(typeof(WCFTPteste.IServiceFTP), typeof(WCFTPteste.ServiceFTPClient));
            return Contract;
        }*/

        public static WSHttpBinding ConfigBinding35()        
        {
            // ----- Programmatic definition of the SomeService Binding -----
            WSHttpBinding wsHttpBinding = new WSHttpBinding();
            wsHttpBinding.Name = "WSHttpBinding_IServiceFTP";
            wsHttpBinding.CloseTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.OpenTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            wsHttpBinding.SendTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.BypassProxyOnLocal = false;
            wsHttpBinding.TransactionFlow = false;
            wsHttpBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            wsHttpBinding.MaxBufferPoolSize = 2147483647;
            wsHttpBinding.MaxReceivedMessageSize = 2147483647;
            wsHttpBinding.MessageEncoding = WSMessageEncoding.Text;
            wsHttpBinding.TextEncoding = Encoding.UTF8;
            wsHttpBinding.UseDefaultWebProxy = true;

            wsHttpBinding.AllowCookies = false;

            wsHttpBinding.ReaderQuotas.MaxDepth = 32;
            wsHttpBinding.ReaderQuotas.MaxArrayLength = 2147483647;
            wsHttpBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
            wsHttpBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
            wsHttpBinding.ReaderQuotas.MaxNameTableCharCount = 16384;

            wsHttpBinding.ReliableSession.Ordered = true;
            wsHttpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromMinutes(10);
            wsHttpBinding.ReliableSession.Enabled = false;
            
            //wsHttpBinding.Security.Mode = SecurityMode.Message;
            //wsHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            //wsHttpBinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
            //wsHttpBinding.Security.Transport.Realm = "";

            wsHttpBinding.Security.Mode = SecurityMode.None;
            //wsHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            //wsHttpBinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
            //wsHttpBinding.Security.Transport.Realm = "";

            
            
            //wsHttpBinding.UseDefaultWebProxy


            //wsHttpBinding.Security.Message.NegotiateServiceCredential = true;
            //wsHttpBinding.Security.Message.ClientCredentialType = MessageCredentialType.UserName;
            //wsHttpBinding.Security.Message.AlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Basic256;
            // ----------- End Programmatic definition of the SomeServiceServiceBinding --------------

            return wsHttpBinding;

        }

        //public static WSHttpBinding ConfigBinding()
        public static BasicHttpBinding ConfigBinding()

        {
            // ----- Programmatic definition of the SomeService Binding -----
            //var wsHttpBinding = new WSHttpBinding();
            BasicHttpBinding wsHttpBinding = new BasicHttpBinding();

            wsHttpBinding.Name = "BasicHttpBinding_IServiceFTP1";
            wsHttpBinding.CloseTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.OpenTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            wsHttpBinding.SendTimeout = TimeSpan.FromMinutes(1);
            wsHttpBinding.BypassProxyOnLocal = false;           
            //wsHttpBinding.TransactionFlow = false;
            wsHttpBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            wsHttpBinding.MaxBufferPoolSize = 524288;
            wsHttpBinding.MaxReceivedMessageSize = 65536;
            wsHttpBinding.MessageEncoding = WSMessageEncoding.Text;
            wsHttpBinding.TextEncoding = Encoding.UTF8;
            wsHttpBinding.TransferMode = TransferMode.Buffered;
            wsHttpBinding.UseDefaultWebProxy = true;
            
            wsHttpBinding.AllowCookies = false;

            wsHttpBinding.ReaderQuotas.MaxDepth = 32;
            wsHttpBinding.ReaderQuotas.MaxArrayLength = 16384;
            wsHttpBinding.ReaderQuotas.MaxStringContentLength = 8192;
            wsHttpBinding.ReaderQuotas.MaxBytesPerRead = 4096;
            wsHttpBinding.ReaderQuotas.MaxNameTableCharCount = 16384;

            //wsHttpBinding.ReliableSession.Ordered = true;
            //wsHttpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromMinutes(10);
            //wsHttpBinding.ReliableSession.Enabled = false;
            /*
            wsHttpBinding.Security.Mode = SecurityMode.Message;
            wsHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            wsHttpBinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
            wsHttpBinding.Security.Transport.Realm = "";
            */
            wsHttpBinding.Security.Mode = BasicHttpSecurityMode.None;
            wsHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            wsHttpBinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
            wsHttpBinding.Security.Transport.Realm = "";

            //wsHttpBinding.UseDefaultWebProxy
            

            //wsHttpBinding.Security.Message.NegotiateServiceCredential = true;
            wsHttpBinding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            //wsHttpBinding.Security.Message.AlgorithmSuite = BasicHttpMessageSecurity
            // ----------- End Programmatic definition of the SomeServiceServiceBinding --------------

            return wsHttpBinding;

        }


        //Forma 1
        /*
        private WCFTPteste.IServiceFTP CreateChannel()
        {
            //WSHttpBinding wsHttpBinding = new WSHttpBinding();
            BasicHttpBinding wsHttpBinding = new BasicHttpBinding();

            wsHttpBinding.Name = "dynamicBinding";

            //wsHttpBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            wsHttpBinding.Security.Mode = BasicHttpSecurityMode.None;

            wsHttpBinding.TextEncoding = Encoding.UTF8;

            wsHttpBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;

            wsHttpBinding.AllowCookies = false;

            wsHttpBinding.MessageEncoding = WSMessageEncoding.Text;

            wsHttpBinding.UseDefaultWebProxy = true;

            wsHttpBinding.OpenTimeout = new TimeSpan(0, 1, 0);

            wsHttpBinding.ReceiveTimeout = new TimeSpan(0, 10, 0);

            wsHttpBinding.MaxBufferPoolSize = 524288;

            wsHttpBinding.MaxReceivedMessageSize = 65536;

            EndpointAddress endpointAddress = new EndpointAddress("http://localhost:51551/ServiceFTP.svc");



            ChannelFactory<WCFTPteste.IServiceFTP> channelFactory = new ChannelFactory<WCFTPteste.IServiceFTP>(

                wsHttpBinding, endpointAddress);

            return (channelFactory.CreateChannel());

        }
        //Forma1 memoEdit1.Text += CreateChannel().GetData(10); 
         */
        




        private void simpleButton1_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = "";
            string strURL35 = "";
            WcfTeste35.ServiceFTPClient Wcf35 = null;
            //WebRequest.DefaultWebProxy = 
            
            

            //WCFTPteste.ServiceFTPClient WCFx = new WCFTPteste.ServiceFTPClient("BasicHttpBinding_IServiceFTP", "http://localhost:51551/ServiceFTP.svc");
            //memoEdit1.Text += WCFx.GetData(10);
            //return;

            //ChannelFactory<WCFTPteste.IServiceFTP> factory = new ChannelFactory<WCFTPteste.IServiceFTP>(ConfigBinding(), new EndpointAddress("http://localhost:51551/ServiceFTP.svc"));
            //WCFTPteste.IServiceFTP channel = factory.CreateChannel();
            //memoEdit1.Text += channel.GetData(10);

            //return;
            //WebRequest.DefaultWebProxy = new WebProxy(proxyAddress)
            //{
            //    Credentials = new NetworkCredential(userName, password),
            //};


            //EndpointIdentity DNSIdentity = EndpointIdentity.CreateDnsIdentity("localhost");

            //Framework 3.5
            if (checkBoxVirWeb.Checked)
            {
                try
                {
                    ServiceReference1.ServiceFTPClient WCFClassico_site = new ServiceReference1.ServiceFTPClient();
                    
                    memoEdit1.Text += string.Format("Teste Classico 3.5 site\r\n");
                    memoEdit1.Text += string.Format("      Retorno:{0}\r\n", WCFClassico_site.Eco(35));
                    memoEdit1.Text += string.Format("      Retorno pacote:{0}\r\n\r\n", WCFClassico_site.TestePacote(new byte[] { 1, 2, 3 }));
                    WCFClassico_site.Close();
                }
                catch (Exception e1)
                {
                    memoEdit1.Text += string.Format("Teste Classico site FALHA\r\nRetorno:{0}\r\n\r\n", e1.Message);
                }
            }
            else
            {
                try
                {
                    WcfTeste35.ServiceFTPClient WCFClassico = new WcfTeste35.ServiceFTPClient();
                    memoEdit1.Text += string.Format("Teste Classico 3.5\r\n");
                    memoEdit1.Text += string.Format("      Retorno:{0}\r\n", WCFClassico.Eco(35));
                    memoEdit1.Text += string.Format("      Retorno pacote:{0}\r\n\r\n", WCFClassico.TestePacote(new byte[] { 1, 2, 3 }));
                    WCFClassico.Close();
                }
                catch (Exception e1)
                {
                    memoEdit1.Text += string.Format("Teste Classico FALHA\r\nRetorno:{0}\r\n\r\n", e1.Message);
                }
            }
            try
            {
                strURL35 = checkBoxVirWeb.Checked ? "http://www.virweb.com.br/central/ServiceFTP.svc" : "http://localhost:50242/ServiceFTP.svc";
                //string strURL35 = checkBox1.Checked ? "https://ssl504.websiteseguro.com/virweb/central35/ServiceFTP.svc" : "http://localhost:50242/ServiceFTP.svc";            
                Wcf35 = new WcfTeste35.ServiceFTPClient(ConfigBinding35(), new EndpointAddress(strURL35));
                memoEdit1.Text += string.Format("Teste 3.5\r\n");
                memoEdit1.Text += string.Format("      Retorno:{0}\r\n", Wcf35.Eco(35));
                memoEdit1.Text += string.Format("      Retorno pacote:{0}\r\n\r\n", Wcf35.TestePacote(new byte[] { 1, 2, 3,4 }));
                Wcf35.Close();
            }
            catch (Exception e1)
            {
                memoEdit1.Text += string.Format("Teste 3.5 FALHA\r\nRetorno:{0}\r\n\r\n", e1.Message);
            }
            
            
            //Framework 4.0            
            try
            {
                string strURL40 = checkBoxVirWeb.Checked ? "http://www.virweb.com.br/central40/ServiceFTP.svc" : "http://localhost:51048/ServiceFTP.svc";
                WcfTeste40.ServiceFTPClient Wcf40 = new WcfTeste40.ServiceFTPClient(ConfigBinding(), new EndpointAddress(strURL40));
                memoEdit1.Text += string.Format("Teste 4.0\r\n");
                memoEdit1.Text += string.Format("      Retorno:{0}\r\n\r\n", Wcf40.GetData(40));
                Wcf40.Close();
            }
            catch (Exception e1)
            {
                memoEdit1.Text += string.Format("Teste 4.0 FALHA\r\nRetorno:{0}\r\n", e1.Message);
                for(Exception ex = e1.InnerException;ex != null;ex = ex.InnerException)
                    memoEdit1.Text += string.Format("Teste 4.0 FALHA\r\nRetorno:{0}\r\n", e1.Message);
                memoEdit1.Text += string.Format("\r\n");
            }






            
            
            Wcf35 = new WcfTeste35.ServiceFTPClient(ConfigBinding35(), new EndpointAddress(strURL35));

            WSFTP.WSFTP WS = new WSTeste.WSFTP.WSFTP();
            if (checkBoxVirWeb.Checked)
                WS.Url = @"http:\\www.virweb.com.br/testeaspnet/WSFTP.asmx";

            long TamanhoBase = 1024 * 1024;

            TamanhoBase = 0xFFFF;

            try
            {
                for (long Tamanho = TamanhoBase; Tamanho < (TamanhoBase * 10); Tamanho += (TamanhoBase / 2))
                {
                    byte[] massa = new byte[Tamanho];
                    DateTime Inicio = DateTime.Now;
                    long res1 = Wcf35.TestePacote(massa);
                    DateTime Repique = DateTime.Now;
                    long res2 = Wcf35.TestePacote(massa);
                    DateTime Fim = DateTime.Now;
                    TimeSpan TimeSpan1 = Repique - Inicio;
                    TimeSpan TimeSpan2 = Fim - Repique;
                    memoEdit1.Text += string.Format("Massa1: {0:d12} {1,5:f3} s - {2,5:f3} s - {3} - {4}\r\n", Tamanho, TimeSpan1.TotalSeconds, TimeSpan2.TotalSeconds, res1, res2);
                    Application.DoEvents();

                    Inicio = DateTime.Now;
                    WS.HelloWorld(massa);
                    Repique = DateTime.Now;
                    WS.HelloWorld(massa);
                    Fim = DateTime.Now;
                    TimeSpan1 = Repique - Inicio;
                    TimeSpan2 = Fim - Repique;
                    memoEdit1.Text += string.Format("Massa2: {0:d12} {1,5:f3} s - {2,5:f3} s - {3} - {4}\r\n", Tamanho, TimeSpan1.TotalSeconds, TimeSpan2.TotalSeconds, res1, res2);

                    
                }
            }
            catch (Exception e2)
            {
                //memoEdit1.Text += "Erro: " + e2.Message;
                int Nivel = 1;
                for (Exception ex = e2; ex != null; ex = ex.InnerException)
                    memoEdit1.Text += string.Format("err {0:00} - {1}\r\n", Nivel++, e2.Message);
            }
            finally
            {
                Wcf35.Close();
            }
        }

        private DateTime InicioAs;
        private long TamanhoAs;
        bool esperando;

        private void Retorno(object Sender, WSTeste.WSFTP.HelloWorldCompletedEventArgs e)
        {
            DateTime Fim = DateTime.Now;
            TimeSpan TimeSpan1 = Fim - InicioAs;
            memoEdit1.Text += string.Format("Massa: {0:d12} {1,5:f1} - {2}\r\n", TamanhoAs, TimeSpan1.TotalSeconds,!e.Cancelled);
            Application.DoEvents();
            esperando = false;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = "";
            string strURL35 = "";
            strURL35 = checkBoxVirWeb.Checked ? "http://www.virweb.com.br/central/ServiceFTP.svc" : "http://localhost:50242/ServiceFTP.svc";
            WcfTeste35.ServiceFTPClient Wcf35 = null;
            
            WSFTP.WSFTP WS = new WSTeste.WSFTP.WSFTP();
            WS.HelloWorldCompleted += new WSTeste.WSFTP.HelloWorldCompletedEventHandler(Retorno);
            spinEdit1.Value = 0;
            int TamanhoBase = 0xFFFF;
            try
            {
                for (TamanhoAs = TamanhoBase; spinEdit1.Value < 10; TamanhoAs += (TamanhoBase / 2))
                {
                    spinEdit1.Value = spinEdit1.Value + 1;
                    byte[] massa = new byte[TamanhoAs];
                    InicioAs = DateTime.Now;
                    WS.HelloWorldAsync(massa);
                    esperando = true;
                    while (esperando)
                        Application.DoEvents();                    
                }
            }
            catch (Exception ex)
            {
                memoEdit1.Text += "Erro: " + ex.Message;
            }

            memoEdit1.Text += "\r\n\r\n----\r\n\r\n";
            Wcf35 = new WcfTeste35.ServiceFTPClient(ConfigBinding35(), new EndpointAddress(strURL35));
            


            WS.HelloWorldCompleted += new WSTeste.WSFTP.HelloWorldCompletedEventHandler(Retorno);
            spinEdit1.Value = 0;
            
            try
            {
                for (TamanhoAs = TamanhoBase; spinEdit1.Value < 10; TamanhoAs += (TamanhoBase / 2))
                {
                    spinEdit1.Value = spinEdit1.Value + 1;
                    byte[] massa = new byte[TamanhoAs];
                    InicioAs = DateTime.Now;
                    WS.HelloWorldAsync(massa);
                    esperando = true;
                    while (esperando)
                        Application.DoEvents();
                }
            }
            catch (Exception ex)
            {
                memoEdit1.Text += "Erro: " + ex.Message;
            }

        }

        private WSFTPdll.ArquivoWSFTP Monitorado1 = null;
        private WSFTPdll.ArquivoWSFTP Monitorado2 = null;

        private DateTime Inicio;

        WSFTPdll.ArquivoWSFTP Emissor = null;

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if(Emissor == null)
                 Emissor = new WSFTPdll.ArquivoWSFTP(checkBoxVirWeb.Checked ? "http://www.virweb.com.br/central/ServiceFTP.svc" : "http://localhost:50242/ServiceFTP.svc", checkBox1.Checked);                        
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Inicio = DateTime.Now;

                foreach (string FileName in openFileDialog1.FileNames)
                    Emissor.Put(FileName,
                                checkBoxEnv.Checked ? WSFTPdll.ArquivoWSFTP.Tipoenvio.envio_com_compressao : WSFTPdll.ArquivoWSFTP.Tipoenvio.envio_sem_compressao,
                                checkBoxEst.Checked ? WSFTPdll.ArquivoWSFTP.TipoArmazenagem.Armazenagem_com_compressao : WSFTPdll.ArquivoWSFTP.TipoArmazenagem.Armazenagem_sem_compressao);

                if (!checkBox1.Checked)
                {
                    progressBarControl1.Properties.Maximum = (int)Emissor.TotalPut;
                    progressBarControl1.Position = 0;
                    progressBarControl1.Visible = true;
                    Monitorado1 = Emissor;
                    timer1.Enabled = true;
                }
                else
                {
                    TimeSpan deltaTempo = DateTime.Now - Inicio;
                    memoEdit1.Text += string.Format("{0}\r\nTempo: {1:n1} segundos\r\n", openFileDialog1.FileName, deltaTempo.TotalSeconds);
                }
            }    
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (Monitorado1 == null)
            {                
                progressBarControl1.Visible = false;                
            }
            else
            {
                if (!Monitorado1.PutAtivo)
                {
                    progressBarControl1.Visible = false;
                    Monitorado1 = null;
                    TimeSpan deltaTempo = DateTime.Now - Inicio;
                    memoEdit1.Text += string.Format("{0}\r\nTempo: {1:n1} segundos\r\n", openFileDialog1.FileName, deltaTempo.TotalSeconds);
                }
                else
                {
                    progressBarControl1.Position = (int)(Monitorado1.TotalPut - Monitorado1.Saldo);
                    timer1.Enabled = true;
                }
            }
            if (Monitorado2 == null)
            {
                progressBarControl2.Visible = false;
            }
            else
            {
                if (!Monitorado2.PutAtivo)
                {
                    progressBarControl2.Visible = false;
                    Monitorado2 = null;
                }
                else
                {
                    progressBarControl2.Position = (int)(Monitorado2.TotalPut - Monitorado2.Saldo);
                    timer1.Enabled = true;
                }
            }
        }

        

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = openFileDialog1.FileName;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                WSFTPdll.ArquivoWSFTP Emissor = new WSFTPdll.ArquivoWSFTP(checkBoxVirWeb.Checked ? "http://www.virweb.com.br/central/ServiceFTP.svc" : "http://localhost:50242/ServiceFTP.svc", checkBox1.Checked);                                        
                string nome = saveFileDialog1.FileName;
                DateTime Inicio = DateTime.Now;
                //Emissor.pathTMP = @"c:\lixo\retorno";
                Emissor.Get(nome,System.IO.Path.GetFileName(nome));
                TimeSpan deltaTempo = DateTime.Now - Inicio;
                //if (!checkBox1.Checked)
                //{
                //    progressBarControl1.Properties.Maximum = (int)Emissor.TotalPut;
                //    progressBarControl1.Position = 0;
                //    progressBarControl1.Visible = true;
                //    Monitorado1 = Emissor;
                //    timer1.Enabled = true;
                //}
                memoEdit1.Text += string.Format("{0}\r\nTempo: {1:n1} segundos\r\n", nome, deltaTempo.TotalSeconds);
            }
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Emissor != null)
                Emissor.parar = true;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (Emissor != null)
                Emissor.parar = true;
            Emissor = null;
        }
    }
}