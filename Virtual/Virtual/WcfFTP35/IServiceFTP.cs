﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfFTP35
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace = "http://virweb.com.br/WcfFTP")]
    public interface IServiceFTP
    {

        [OperationContract]
        string Eco(int value);

        [OperationContract]
        long TestePacote(byte[] massa);

        #region PUT
        [OperationContract]
        string PutAbre(string FileName, long Tamanho, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado);

        [OperationContract]
        int PutPacote(string Codigostr, int Pacote, byte[] dados);

        [OperationContract]
        bool PutFecha(string Codigostr); 
        #endregion

        #region GET

        [OperationContract]
        string GetAbre(out long Tamanho, out bool zipado, string FileName);

        [OperationContract]
        byte[] GetPacote(string Codigostr, int Pacote);

        [OperationContract]
        bool GetFecha(string Codigostr);
        #endregion

        //[OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
    }

    [DataContract]
    public enum Tipoenvio
    {
        [EnumMember]
        envio_com_compressao = 1,
        [EnumMember]
        envio_sem_compressao = 0
    }

    [DataContract]
    public enum TipoArmazenagem 
    {
        [EnumMember]
        Armazenagem_com_compressao = 1,
        [EnumMember]
        Armazenagem_sem_compressao = 0
    }

    
    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    /*
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    */
}
