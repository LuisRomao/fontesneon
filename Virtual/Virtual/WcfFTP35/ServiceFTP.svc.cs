﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.IO;

namespace WcfFTP35
{
    
    public class ServiceFTP : IServiceFTP
    {
        private const int TamanhoMaxPacote = 0xFFFF;

        private int numerador = 0;

        private static string Caminho
        {
            get
            {
                return ConfigurationManager.AppSettings["Dados"];
            }
        }

        private static SortedList<string,ArquivoParcial> _Arqs;
        private static SortedList<string, ArquivoParcial> Arqs
        {
            get { return _Arqs == null ? _Arqs = new System.Collections.Generic.SortedList<string,ArquivoParcial>() : _Arqs; }
        }

        #region Testes
        public string Eco(int value)
        {
            return string.Format("Eco: {0}", value);
        }

        public long TestePacote(byte[] massa)
        {
            return massa.Length;
        }
        
        #endregion

        #region PUT
        public string PutAbre(string FileName, long Tamanho, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado)
        {
            string NomeTotalArquivo = string.Format("{0}{1}", Caminho, FileName);
            string Codigostr = string.Format("{0:yyyyMMddHHmmss}_{1}", DateTime.Now, numerador++);
            ArquivoParcial ArqP = new ArquivoParcial(Codigostr, NomeTotalArquivo, EnviarZipado, EstocarZipado);            
            Arqs.Add(Codigostr, ArqP);
            //if (EnviarZipado == Tipoenvio.envio_com_compressao)
            //    NomeTotalArquivo = NomeTotalArquivo + ".vi$";
            //else
            //    NomeTotalArquivo = NomeTotalArquivo + ".$$$";
            string Pasta = System.IO.Path.GetDirectoryName(NomeTotalArquivo);
            if (!Directory.Exists(Pasta))
                Directory.CreateDirectory(Pasta);
            //ArqP.fs = new FileStream(NomeTotalArquivo, FileMode.Create);
            ArqP.fs = new FileStream(ArqP.NomeTMP, FileMode.Create);
            return Codigostr;
        }

        public int PutPacote(string Codigostr, int Pacote, byte[] dados)
        {
            ArquivoParcial ArqP = Arqs[Codigostr];
            if (ArqP == null)
                return -1;
            if (ArqP.nPacote != Pacote)
                return -1;
            ArqP.nPacote++;
            using (MemoryStream ms = new MemoryStream(dados))
                ms.WriteTo(ArqP.fs);
            return ArqP.nPacote;
        }

        public bool PutFecha(string Codigostr)
        {
            ArquivoParcial ArqP = Arqs[Codigostr];
            if (ArqP == null)
                return false;
            bool retorno = ArqP.Fecha();
            Arqs.RemoveAt(Arqs.IndexOfKey(Codigostr));
            return retorno;
        }
        
        #endregion

        #region GET
        public string GetAbre(out long Tamanho, out bool zipado, string FileName)
        {
            zipado = true;
            string NomeTotalArquivo = string.Format("{0}{1}.vir", Caminho, FileName);
            if (!File.Exists(NomeTotalArquivo))
            {
                NomeTotalArquivo = string.Format("{0}{1}", Caminho, FileName);
                zipado = false;
                if (!File.Exists(NomeTotalArquivo))
                {
                    Tamanho = 0;
                    return "Arquivo não encontrado";
                }
            };
            string Codigostr = string.Format("{0:yyyyMMddHHmmss}_{1}", DateTime.Now, numerador++);
            ArquivoParcial ArqP = new ArquivoParcial(Codigostr, 
                                                     NomeTotalArquivo, 
                                                     zipado ? Tipoenvio.envio_com_compressao:Tipoenvio.envio_sem_compressao, 
                                                     zipado ? TipoArmazenagem.Armazenagem_com_compressao: TipoArmazenagem.Armazenagem_sem_compressao);            
            Arqs.Add(Codigostr, ArqP);
            ArqP.fs = new FileStream(NomeTotalArquivo, FileMode.Open,FileAccess.Read,FileShare.Read);
            ArqP.BR = new BinaryReader(ArqP.fs);
            ArqP.Saldo = Tamanho = ArqP.fs.Length;
            return Codigostr;
        }

        public byte[] GetPacote(string Codigostr, int Pacote)
        {
            ArquivoParcial ArqP = (ArquivoParcial)Arqs[Codigostr];
            if (ArqP == null)
                return null;
            if (ArqP.nPacote != Pacote)
                return null;
            ArqP.nPacote++;
            int TamanhoPacote = ArqP.Saldo > TamanhoMaxPacote ? TamanhoMaxPacote : (int)ArqP.Saldo;
            byte[] dados = ArqP.BR.ReadBytes(TamanhoPacote);
            ArqP.Saldo -= TamanhoPacote;
            return dados;
        }
        
        public bool GetFecha(string Codigostr)
        {
            ArquivoParcial ArqP = (ArquivoParcial)Arqs[Codigostr];
            if (ArqP == null)
                return false;
            ArqP.fs.Close();
            Arqs.RemoveAt(Arqs.IndexOfKey(Codigostr));
            return (ArqP.Saldo == 0);
        }

        #endregion

        /*
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }*/
    }
}
