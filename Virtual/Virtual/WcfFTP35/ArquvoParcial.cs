﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;


namespace WcfFTP35
{
    internal class ArquivoParcial
    {
        internal FileStream fs;
        internal int nPacote;
        private Tipoenvio EnviarZipado;
        private TipoArmazenagem EstocarZipado;
        private string NomeTotalArquivoRec;
        internal string NomeTMP;

        internal BinaryReader BR;
        internal long Saldo;

        public ArquivoParcial(string Codigo, string NomeTotalArquivo, Tipoenvio _EnviarZipado, TipoArmazenagem _EstocarZipado)
        {
            EnviarZipado = _EnviarZipado;
            EstocarZipado = _EstocarZipado;
            NomeTotalArquivoRec = NomeTotalArquivo;
            NomeTMP = string.Format("{0}/{1}.$$$", Path.GetDirectoryName(NomeTotalArquivo), Codigo); 
            nPacote = 1;
        }

        public bool Fecha()
        {
            fs.Close();
            if (EnviarZipado == Tipoenvio.envio_sem_compressao)
            {
                if (File.Exists(NomeTotalArquivoRec))
                    File.Delete(NomeTotalArquivoRec);
                File.Move(NomeTMP, NomeTotalArquivoRec);
                if (File.Exists(NomeTotalArquivoRec + ".vir"))
                    try
                    {
                        File.Delete(NomeTotalArquivoRec + ".vir");
                    }
                    catch { }
            }
            else
                if (EstocarZipado == TipoArmazenagem.Armazenagem_sem_compressao)
                {
                    VirZIP.zip.Execute(NomeTotalArquivoRec, NomeTMP, System.IO.Compression.CompressionMode.Decompress);
                    File.Delete(NomeTMP);
                    if (File.Exists(NomeTotalArquivoRec + ".vir"))
                        try
                        {
                            File.Delete(NomeTotalArquivoRec + ".vir");
                        }
                        catch { }
                }
                else
                {
                    if (File.Exists(NomeTotalArquivoRec + ".vir"))
                        File.Delete(NomeTotalArquivoRec + ".vir");
                    File.Move(NomeTMP, NomeTotalArquivoRec + ".vir");
                    if (File.Exists(NomeTotalArquivoRec))
                        try
                        {
                            File.Delete(NomeTotalArquivoRec);
                        }
                        catch { }
                }
            return true;
        }
    }
}
