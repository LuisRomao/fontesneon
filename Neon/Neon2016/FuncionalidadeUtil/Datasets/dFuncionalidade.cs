﻿namespace FuncionalidadeUtil.Datasets
{
    partial class dFuncionalidade
    {
        public int EMP;

        public dFuncionalidade(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dFuncionalidadeTableAdapters.CondominosTableAdapter taCondominosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Condominos
        /// </summary>
        public dFuncionalidadeTableAdapters.CondominosTableAdapter CondominosTableAdapter
        {
            get
            {
                if (taCondominosTableAdapter == null)
                {
                    taCondominosTableAdapter = new dFuncionalidadeTableAdapters.CondominosTableAdapter();
                    if (EMP == 3)
                        taCondominosTableAdapter.Connection.ConnectionString = taCondominosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCondominosTableAdapter;
            }
        }

        private dFuncionalidadeTableAdapters.CORPODIRETIVOTableAdapter taCORPODIRETIVOTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CORPODIRETIVO
        /// </summary>
        public dFuncionalidadeTableAdapters.CORPODIRETIVOTableAdapter CORPODIRETIVOTableAdapter
        {
            get
            {
                if (taCORPODIRETIVOTableAdapter == null)
                {
                    taCORPODIRETIVOTableAdapter = new dFuncionalidadeTableAdapters.CORPODIRETIVOTableAdapter();
                    if (EMP == 3)
                        taCORPODIRETIVOTableAdapter.Connection.ConnectionString = taCORPODIRETIVOTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCORPODIRETIVOTableAdapter;
            }
        }

        private dFuncionalidadeTableAdapters.FaleConoscoDestinatariosTableAdapter taFaleConoscoDestinatariosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FaleConoscoDestinatarios
        /// </summary>
        public dFuncionalidadeTableAdapters.FaleConoscoDestinatariosTableAdapter FaleConoscoDestinatariosTableAdapter
        {
            get
            {
                if (taFaleConoscoDestinatariosTableAdapter == null)
                {
                    taFaleConoscoDestinatariosTableAdapter = new dFuncionalidadeTableAdapters.FaleConoscoDestinatariosTableAdapter();
                    if (EMP == 3)
                        taFaleConoscoDestinatariosTableAdapter.Connection.ConnectionString = taFaleConoscoDestinatariosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taFaleConoscoDestinatariosTableAdapter;
            }
        }

        private dFuncionalidadeTableAdapters.ST_SMTpTableAdapter taST_SMTpTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_SMTp
        /// </summary>
        public dFuncionalidadeTableAdapters.ST_SMTpTableAdapter ST_SMTpTableAdapter
        {
            get
            {
                if (taST_SMTpTableAdapter == null)
                {
                    taST_SMTpTableAdapter = new dFuncionalidadeTableAdapters.ST_SMTpTableAdapter();
                    if (EMP == 3)
                        taST_SMTpTableAdapter.Connection.ConnectionString = taST_SMTpTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_SMTpTableAdapter;
            }
        }

        private dFuncionalidadeTableAdapters.ST_EMailEnvioTableAdapter taST_EMailEnvioTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_EMailEnvio
        /// </summary>
        public dFuncionalidadeTableAdapters.ST_EMailEnvioTableAdapter ST_EMailEnvioTableAdapter
        {
            get
            {
                if (taST_EMailEnvioTableAdapter == null)
                {
                    taST_EMailEnvioTableAdapter = new dFuncionalidadeTableAdapters.ST_EMailEnvioTableAdapter();
                    if (EMP == 3)
                        taST_EMailEnvioTableAdapter.Connection.ConnectionString = taST_EMailEnvioTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_EMailEnvioTableAdapter;
            }
        }

        private dFuncionalidadeTableAdapters.ST_AViSosTableAdapter taST_AViSosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_AViSosTableAdapter
        /// </summary>
        public dFuncionalidadeTableAdapters.ST_AViSosTableAdapter ST_AViSosTableAdapter
        {
            get
            {
                if (taST_AViSosTableAdapter == null)
                {
                    taST_AViSosTableAdapter = new dFuncionalidadeTableAdapters.ST_AViSosTableAdapter();
                    if (EMP == 3)
                        taST_AViSosTableAdapter.Connection.ConnectionString = taST_AViSosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_AViSosTableAdapter;
            }
        }
    }
}
