﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalUtil;

namespace Cadastro
{
    public partial class Projeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void cmdCadastroUsuarioAdm_Click(object sender, EventArgs e)
        {
            if (Login("rodino", "062129")) Response.Redirect("~/Pages_Portal/CadastroUsuario.aspx"); //Admin
        }

        protected void cmdCadastroUsuario_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/CadastroUsuario.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/CadastroUsuario.aspx"); //Sindico - ALGRAN (SA)  
        }

        protected void cmdCadastroEmail_Click(object sender, EventArgs e)
        {
            if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/CadastroEmail.aspx"); //Sindico - 000999 (SB)
            //if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/CadastroEmail.aspx"); //Sindico - ALGRAN (SA)  
            //if (Login("1nair20", "2580")) Response.Redirect("~/Pages_Portal/CadastroEmail.aspx"); //Box
        }

        protected void cmdCadastroCondominio_Click(object sender, EventArgs e)
        {
            if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/CadastroCondominio.aspx"); //Proprietario - 000999 (SB)
            //if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/CadastroCondominio.aspx"); //Sindico - ALGRAN (SA)  
        }

        protected void cmdCadastroUnidade_Click(object sender, EventArgs e)
        {
            //if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/CadastroUnidade.aspx"); //Proprietario - 000999
            //if (Login("1marcos123", "6008")) Response.Redirect("~/Pages_Portal/CadastroUnidade.aspx"); //Proprietario - 000999
            //if (Login("1lucian215", "2886")) Response.Redirect("~/Pages_Portal/CadastroUnidade.aspx"); //Proprietario - 000566 - Com Seguro Conteudo
            //if (Login("1vitor58", "2996")) Response.Redirect("~/Pages_Portal/CadastroUnidade.aspx"); //Proprietario - 000566 - Sem Seguro Conteudo
            //if (Login("1nair20", "2580")) Response.Redirect("~/Pages_Portal/CadastroUnidade.aspx"); //Box
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/CadastroUnidade.aspx"); //Sindico - ALGRAN (SA)  
            //if (Login("1JOSE696", "9515")) Response.Redirect("~/Pages_Portal/CadastroUnidade.aspx"); //Proprietario - 000244 (SB)
            //if (Login("1PAULO469", "7842")) Response.Redirect("~/Pages_Portal/CadastroUnidade.aspx"); //Proprietario - 000244 (SB)
        }

        protected void cmdCadastroCorpoDiretivo_Click(object sender, EventArgs e)
        {
            //if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/CadastroCorpoDiretivo.aspx"); //Proprietario - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/CadastroCorpoDiretivo.aspx"); //Sindico - ALGRAN (SA)  
        }

        protected void cmdCadastroDocumentacao_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=0"); //Sindico - 000429 (SB)
            //if (Login("1andre39", "5542")) Response.Redirect("~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=0"); //Sindico - 000429 (SB)
            //if (Login("1andre39", "5542")) Response.Redirect("~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=5"); //Sindico - 000429 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=4"); //Sindico - ALGRAN (SA)  
            //if (Login("3VAGNER4", "3954")) Response.Redirect("~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=5"); //Sindico - AMSTE. (SA)  
            //if (Login("1NATALI2", "6791")) Response.Redirect("~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=1"); //Proprietario - ACAE.. (SB) - html
            //if (Login("1NATALI2", "6791")) Response.Redirect("~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=2"); //Proprietario - ACAE.. (SB) - docx
            //if (Login("1LUIZA16", "4493")) Response.Redirect("~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=4"); //Proprietario - 000541 (SB) - jpg
        }

        protected void cmdCadastroCondominioConfiguracao_Click(object sender, EventArgs e)
        {
            //if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/CadastroCondominioConfiguracao.aspx"); //Proprietario - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/CadastroCondominioConfiguracao.aspx"); //Sindico - ALGRAN (SA)  
            //if (Login("2PAULO64", "4964")) Response.Redirect("~/Pages_Portal/CadastroCondominioConfiguracao.aspx"); //Proprietario - GRENOB (SA)  
            //if (Login("1MERC28", "7089")) Response.Redirect("~/Pages_Portal/CadastroCondominioConfiguracao.aspx"); //Proprietario - PNOBIL (SB)  
        }

        protected void cmdCadastroAviso_Click(object sender, EventArgs e)
        {
            //if (Login("rodino", "062129")) Response.Redirect("~/Pages_Portal/CadastroAviso.aspx"); //Adm
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/CadastroAviso.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/CadastroAviso.aspx"); //Sindico - ALGRAN (SA)  
        }

        protected bool Login(string strUsuario, string strSenha)
        {
            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(strUsuario, strSenha);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = strUsuario;
                Session["Pass"] = strSenha;

                //Autenticou
                return true;
            }

            //Não autenticou
            return false;
        }
    }
}