﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using CadastroUtil.Datasets;

namespace Cadastro.Pages_Portal
{
    public partial class CadastroEmail : System.Web.UI.Page
    {
        private dCadastro dCad = new dCadastro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Sessao para grids
            Session["CON"] = Cred.CON;

            //Inicia dataset
            dCad = new dCadastro(Cred.EMP);

            //Inicia datasources
            if (Cred.EMP == 3)
            {
                dsFaleConosco.ConnectionString = dsFaleConosco.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsControleMudanca.ConnectionString = dsControleMudanca.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsAutorizacaoEntradaPrestador.ConnectionString = dsAutorizacaoEntradaPrestador.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsSolicitacaoReparo.ConnectionString = dsSolicitacaoReparo.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsCaixaSugestoes.ConnectionString = dsCaixaSugestoes.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsReserva.ConnectionString = dsReserva.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsReservaListaConvidados.ConnectionString = dsReservaListaConvidados.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
            }

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Pega funcionalidades do condominio
            dCadastro.ST_CondominioFUncionalidadeRow rowFunCon = null;
            dCad.ST_CondominioFUncionalidadeTableAdapter.Fill(dCad.ST_CondominioFUncionalidade, Cred.CON);
            if (dCad.ST_CondominioFUncionalidade.Count != 0)
                rowFunCon = (dCadastro.ST_CondominioFUncionalidadeRow)dCad.ST_CondominioFUncionalidade.Rows[0];

            //Preenche campos
            chkFaleConoscoCD.Checked = (rowFunCon == null) ? false : rowFunCon.CFUFaleConoscoCD;
            chkFaleConosco.Checked = (rowFunCon == null) ? false : rowFunCon.CFUFaleConosco;
            panFaleConosco.Visible = (rowFunCon == null) ? false : true;
            panControleMudanca.Visible = (rowFunCon == null) ? false : rowFunCon.CFUControleMudanca;
            panAutorizacaoEntradaPrestador.Visible = (rowFunCon == null) ? false : rowFunCon.CFUAutorizacaoEntradaPrestador;
            panSolicitacaoReparo.Visible = (rowFunCon == null) ? false : rowFunCon.CFUSolicitacaoReparo;
            panCaixaSugestoes.Visible = (rowFunCon == null) ? false : rowFunCon.CFUCaixaSugestoes;
            panReserva.Visible = panReservaListaConvidados.Visible = (rowFunCon == null) ? false : rowFunCon.CFUReserva;

            //Carrega e-mails corpo diretivo
            dCad.CORPODIRETIVOTableAdapter.Fill(dCad.CORPODIRETIVO, Cred.CON);
            grdCorpoDiretivo.DataSource = dCad.CORPODIRETIVO;
            grdCorpoDiretivo.DataBind();
        }

        protected void cmdSalvar_Click(object sender, EventArgs e)
        {
            //Pega funcionalidades do condominio
            dCad.ST_CondominioFUncionalidadeTableAdapter.Fill(dCad.ST_CondominioFUncionalidade, Cred.CON);
            dCadastro.ST_CondominioFUncionalidadeRow rowFunCon = (dCadastro.ST_CondominioFUncionalidadeRow)dCad.ST_CondominioFUncionalidade.Rows[0];

            //Salva campos
            rowFunCon.CFUFaleConoscoCD = chkFaleConoscoCD.Checked;
            rowFunCon.CFUFaleConosco = chkFaleConosco.Checked;
            dCad.ST_CondominioFUncionalidadeTableAdapter.Update(dCad.ST_CondominioFUncionalidade);
            dCad.ST_CondominioFUncionalidade.AcceptChanges();

            //Exibe mensagem
            lblMsg.Text = "Dados salvos com sucesso!";
            panMsg.Visible = true;
        }

        protected void chkFaleConosco_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsg.Text = "";
            panMsg.Visible = false;
        }
    }
}