﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Configuration;
using DevExpress.Web;
using PortalUtil;
using CadastroUtil.Datasets;

namespace Cadastro.Pages_Portal
{
    public partial class CadastroUnidade : System.Web.UI.Page
    {
        private dCadastro dCad = new dCadastro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        private SortedList lstAlteracoes = new SortedList();

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dCad = new dCadastro(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Seleciona Condomino
            if (dCad.CondominoTableAdapter.Fill(dCad.Condomino, Cred.rowUsuario.APT, Cred.rowUsuario.PES) > 0)
            {
                //Pega dados da pessoa do apto
                dCadastro.CondominoRow rowPesApt = dCad.Condomino[0];
                txtEmail.Text = rowPesApt.Email;
                txtEndereco.Text = rowPesApt.Endereco;
                txtBairro.Text = rowPesApt.Bairro;
                txtCidade.Text = rowPesApt.Cidade;
                txtEstado.Text = rowPesApt.Estado;
                txtCEP.Text = rowPesApt.CEP;
                txtTelefone1.Text = rowPesApt.Telefone1;
                txtTelefone2.Text = rowPesApt.Telefone2;
                txtTelefone3.Text = rowPesApt.Telefone3;

                //Preenche correio e email
                int intAPTFormaPagto = rowPesApt.IsAPTFormaPagtoNull() ? 4 : rowPesApt.APTFormaPagto;
                switch (intAPTFormaPagto)
                {
                    //Com Correio / Sem E-mail
                    case 2:
                        chkCorreio.Checked = true;
                        rblEmailBoleto.Value = 0;
                        break;
                    //Sem Correio / Sem E-mail
                    case 4:
                        chkCorreio.Checked = false;
                        rblEmailBoleto.Value = 0;
                        break;
                    //Com Correio / Com Aviso
                    case 12:
                        chkCorreio.Checked = true;
                        rblEmailBoleto.Value = 1;
                        break;
                    //Sem Correio / Com Aviso
                    case 14:
                        chkCorreio.Checked = false;
                        rblEmailBoleto.Value = 1;
                        break;
                    //Com Correio / E-mail
                    case 22:
                        chkCorreio.Checked = true;
                        rblEmailBoleto.Value = 2;
                        break;
                    //Sem Correio / E-mail
                    case 24:
                        chkCorreio.Checked = false;
                        rblEmailBoleto.Value = 2;
                        break;
                    //Default - Sem Correio / Sem E-mail
                    default:
                        chkCorreio.Checked = false;
                        rblEmailBoleto.Value = 0;
                        break;
                }

                //Preenche debito automatico
                dCad.CONDOMINIOSTableAdapter.Fill(dCad.CONDOMINIOS, Cred.CON);
                dCadastro.CONDOMINIOSRow rowCON = dCad.CONDOMINIOS[0];
                floCadastro.FindItemOrGroupByName("loiDebitoAutomatico").Visible = Convert.ToBoolean(rowCON.CONDebitoAutomatico);
                if (Convert.ToBoolean(rowCON.CONDebitoAutomatico))
                    lblDebitoAutomatico.Text = Convert.ToBoolean(rowPesApt.APTDebitoAutomatico) ? "Boleto cadastrado em débito automático" : "Não cadastrado";

                //Preenche seguro conteudo
                panSeguroConteudo.Visible = panSeguroConteudoBr.Visible = false;
                if (!rowPesApt.IsCON_PSCNull())
                {
                    panSeguroConteudo.Visible = panSeguroConteudoBr.Visible = true;
                    chkSeguroConteudo.Checked = rowPesApt.APTSeguro;
                }

                //Preenche usuario
                txtUsuario.Text = rowPesApt.Usuario;

                //Pega funcionalidades do condominio
                dCadastro.ST_CondominioFUncionalidadeRow rowFunCon = null;
                dCad.ST_CondominioFUncionalidadeTableAdapter.Fill(dCad.ST_CondominioFUncionalidade, Cred.CON);
                if (dCad.ST_CondominioFUncionalidade.Count != 0)
                    rowFunCon = (dCadastro.ST_CondominioFUncionalidadeRow)dCad.ST_CondominioFUncionalidade.Rows[0];

                //Verifica se habilita campos de classificados (ativo ou nao para o condominio)
                panClassificados.Visible = panClassificadosBr.Visible = (rowFunCon == null) ? false : rowFunCon.CFUClassificado;
                if (panClassificados.Visible)
                {
                    //Carrega o campo de ramos de atividade do formulario
                    dCad.RamoAtiVidadeTableAdapter.Fill(dCad.RamoAtiVidade);
                    lstRamoAtividade.DataSource = dCad.RamoAtiVidade;
                    lstRamoAtividade.DataBind();

                    //Preenche dados classificados
                    txtSite.Text = rowPesApt.Site;
                    txtVideo.Text = rowPesApt.Video;
                    txtDescricao.Text = rowPesApt.Descricao;

                    //Para cada ramo de atividade do cliente, seleciona a descricao na lista
                    dCad.PESxRAVTableAdapter.Fill(dCad.PESxRAV, Cred.rowUsuario.PES);
                    foreach (DataRow row in dCad.PESxRAV.Rows)
                    {
                        for (int i = 0; i < (lstRamoAtividade.Items.Count); i++)
                            if ((int)row["RAV"] == (int)lstRamoAtividade.Items[i].Value)
                                lstRamoAtividade.Items[i].Selected = true;
                    }
                    for (int i = 0; i < (lstRamoAtividade.Items.Count); i++)
                        if (lstRamoAtividade.Items[i].Selected)
                            lblRavOriginal.Text = lblRavOriginal.Text + "[" + lstRamoAtividade.Items[i].Value.ToString() + "]";
                }
            }
        }

        protected void rblEmailBoleto_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se clicou na opcao de envio de boleto por e-mail e seta codigo
            lblCodigo.Text = "";
            if ((int)rblEmailBoleto.SelectedItem.Value == 2)
            {
                Random randNum = new Random();
                lblCodigo.Text = randNum.Next(999999).ToString("000000");
            }
        }

        protected string EnviaEmailConfirmacao(string Destino, string strCodigo)
        {
            try
            {
                //Cria e-mail
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    email.To.Add(Destino);

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Confirmação de E-mail";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "       <p><b>Data:</b> {0}</p>\r\n" +
                                               "       <p><b>Código de Confirmação:</b> {1}\r\n</p>\r\n" +
                                               "       <p><i>Caro Condômino,</i></p>\r\n" +
                                               "       <p><i>Com muita satisfação, vimos por meio desta informar que recebemos sua solicitação para o envio dos Boletos Mensais de Condomínio <b>APENAS POR E-MAIL, com o acesso ao site <u>www.neonimoveis.com.br</u></b>, portanto, a partir da confirmação deste e-mail o senhor(a) passará a recebê-los exclusivamente de forma eletrônica, sendo que fazendo-o, ajudará a preservar o meio ambiente, e ainda, estará contando com os principais benefícios deste serviço, quais sejam, agilidade, praticidade, conforto e economia.</i></p>\r\n" +
                                               "       <p><i>Informamos que os boletos ficarão como efetivamente já ocorre, mensalmente disponíveis no site da Neon Imóveis para consulta ou pagamento, resguardando-se os prazos administrativos e bancários, além do envio via e-mail mediante a presente solicitação.</i></p>\r\n" +
                                               "       <p><i>Resta por este intermédio caracterizado como de inteira responsabilidade do solicitante, o acompanhamento das datas de vencimento, e principalmente de manutenção do e-mail devidamente atualizado e configurado para recepção do mesmo evitando qualquer tipo de transtorno, portanto, solicitamos que a caixa de e-mail seja assim configurada implicando no não envio à lixeira, ou mesmo ao chamado “spam”.</i></p>\r\n" +
                                               "       <p><i>Ao confirmar a presente solicitação para que haja a ativação do serviço retro, Vossa Senhoria está concordando e assumindo a responsabilidade legal para com o controle do presente envio e consequente pagamento conforme estipulado na legislação Civil, Lei n.º 10.406/2002, artigos 1336 Inciso I e  1347 e 1348 VII.</i></p>\r\n" +
                                               "       <p><i>Agradecemos portanto a confiança em nós depositada!</i></p>\r\n" +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, strCodigo);

                    //Envia email
                    SmtpClient1.Send(email);

                    //Sucesso no envio
                    return "E-mail de confirmação enviado com sucesso!";
                }
            }
            catch (Exception)
            {
                //Erro no envio
                return "Não foi possível enviar o e-mail de confirmação para envio de boleto por e-mail ! \r\nPor favor, tente novamente.";
            }
        }

        protected void cmdReenviar_Click(object sender, EventArgs e)
        {
            //Limpa campo de msg
            lblMsgConfirmacaoEmail.Text = "";

            //Envia novamente o e-mail de confirmacao e exibe retorno do envio
            lblMsgConfirmacaoEmail.Text = EnviaEmailConfirmacao(txtEmail.Text, lblCodigo.Text);
        }

        protected void cmdConfirmar_Click(object sender, EventArgs e)
        {
            //Limpa campo de msg
            lblMsgConfirmacaoEmail.Text = "";

            //Verifica se codigo nao confirma
            if (txtCodigo.Text != lblCodigo.Text)
            {
                lblMsgConfirmacaoEmail.Text = "Código de confirmação de e-mail não confere. Verifique o código digitado. Caso não tenha recebido o e-mail com o código de confirmação, clique em 'Reenviar' e verifique sua caixa de 'spam' ou sua lixeira.";
                return;
            }

            //Salva
            Salvar();

            //Volta pans
            panConfirmacaoEmail.Visible = false;
            panFormulario.Visible = true;
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Seta a opcao default
            rblEmailBoleto.Value = 1;
            txtCodigo.Text = lblCodigo.Text = "";

            //Salva
            Salvar();

            //Volta pans
            panConfirmacaoEmail.Visible = false;
            panFormulario.Visible = true;
        }

        protected void cmdSalvar_Click(object sender, EventArgs e)
        {
            //Limpa campo de msg
            lblMsgConfirmacaoEmail.Text = "";
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Verifica se senhas diferentes
            if (txtNovaSenha.Text != txtRepetirNovaSenha.Text)
            { 
                lblMsg.Text = "Nova senha não confirmada, as senhas devem ser idênticas";
                panMsg.Visible = true;
                return;
            }

            try
            {
                //Verifica confirmacao de email
                if (lblCodigo.Text != "")
                {
                    //Verifica se informou o e-mail
                    if (txtEmail.Text == "")
                    {
                        lblMsg.Text = "Para envio de boleto por e-mail é obrigatório informar um endereço de e-mail válido no campo e-mail";
                        panMsg.Visible = true;
                        return;
                    }

                    //Envia o e-mail de confirmacao
                    string strMsgEnvio = EnviaEmailConfirmacao(txtEmail.Text, lblCodigo.Text);
                    if (!strMsgEnvio.Contains("sucesso"))
                    {
                        lblMsg.Text = strMsgEnvio;
                        panMsg.Visible = true;
                        return;
                    }

                    //Abre tela de confirmacao
                    panConfirmacaoEmail.Visible = true;
                    panFormulario.Visible = false;
                }
                else
                    //Salva
                    Salvar();
            }
            catch (Exception ex)
            {
                lblMsg.Text =  String.Format("Erro ao salvar as alterações: <br/>{0}", ex.Message);
                panMsg.Visible = true;
            }
        }

        protected void Salvar()
        {
            //Seleciona Condomino
            dCad.CondominoTableAdapter.Fill(dCad.Condomino, Cred.rowUsuario.APT, Cred.rowUsuario.PES);
            dCadastro.CondominoRow rowPesApt = dCad.Condomino[0];

            //Verifica usuario e senha alterados
            if ((txtNovaSenha.Text.Trim() != "" && txtNovaSenha.Text != rowPesApt.Senha) || (txtUsuario.Text.Trim() != "" && txtUsuario.Text != rowPesApt.Usuario))
            {
                //Checa usuario e senha repetidos
                object oID = null;

                //Base apartamentos em neon e neonsa
                CadastroUtil.Datasets.dCadastroTableAdapters.CondominoTableAdapter taCon = new CadastroUtil.Datasets.dCadastroTableAdapters.CondominoTableAdapter();
                oID = taCon.VerificaUsuarioSenha(txtUsuario.Text, txtNovaSenha.Text.Trim() != "" ? txtNovaSenha.Text : rowPesApt.Senha);
                if ((oID != null) && ((int)oID != rowPesApt.PES))
                {
                    lblMsg.Text = "Este usuário e senha já estão cadastrados e não podem ser utilizados";
                    panMsg.Visible = true;
                    return;
                }
                taCon.Connection.ConnectionString = taCon.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                oID = taCon.VerificaUsuarioSenha(txtUsuario.Text, txtNovaSenha.Text.Trim() != "" ? txtNovaSenha.Text : rowPesApt.Senha);
                if ((oID != null) && ((int)oID != rowPesApt.PES))
                {
                    lblMsg.Text = "Este usuário e senha já estão cadastrados e não podem ser utilizados";
                    panMsg.Visible = true;
                    return;
                }

                //Base usuarios
                oID = dCad.USuaRiosTableAdapter.VerificaUsuarioSenha(txtUsuario.Text, txtNovaSenha.Text.Trim() != "" ? txtNovaSenha.Text : rowPesApt.Senha);
                if (oID != null)
                {
                    lblMsg.Text = "Este usuário e senha já estão cadastrados e não podem ser utilizados";
                    panMsg.Visible = true;
                    return;
                }
            };

            //Pega dados da pessoa do apto
            dCad.PESSOASTableAdapter.Fill(dCad.PESSOAS, rowPesApt.PES);
            dCadastro.PESSOASRow rowPes = dCad.PESSOAS[0];
            if (txtEmail.Text != rowPesApt.Email) { rowPes.PESEmail = txtEmail.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - E-mail", txtEmail.Text); }
            if (txtEndereco.Text != rowPesApt.Endereco) { rowPes.PESEndereco = txtEndereco.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Endereço", txtEndereco.Text); }
            if (txtBairro.Text != rowPesApt.Bairro) { rowPes.PESBairro = txtBairro.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Bairro", txtBairro.Text); }
            if (txtCEP.Text != rowPesApt.CEP) { rowPes.PESCep = txtCEP.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - CEP", txtCEP.Text); }
            if (txtCidade.Text != rowPesApt.Cidade)
            {
                if (txtCidade.Text != rowPesApt.Cidade) lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Cidade/Estado", txtCidade.Text + "/" + txtEstado.Text);
                object objCID = dCad.CIDADESTableAdapter.BuscaCidade(txtCidade.Text);
                int CID;
                if (objCID == null)
                {
                    dCadastro.CIDADESRow rowCid = dCad.CIDADES.NewCIDADESRow();
                    rowCid.CIDDataInclusao = DateTime.Now;
                    if (txtCidade.Text.Trim() != "")
                        rowCid.CIDNome = txtCidade.Text.Trim();
                    if (txtEstado.Text.Trim() != "")
                        rowCid.CIDUf = txtEstado.Text.Trim();
                    dCad.CIDADES.AddCIDADESRow(rowCid);
                    dCad.CIDADESTableAdapter.Update(rowCid);
                    CID = rowCid.CID;
                }
                else
                    CID = (int)objCID;
                rowPes.PES_CID = CID;
            }
            if (txtTelefone1.Text != rowPesApt.Telefone1) { rowPes.PESFone1 = txtTelefone1.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Telefone 1", txtTelefone1.Text); }
            if (txtTelefone2.Text != rowPesApt.Telefone2) { rowPes.PESFone2 = txtTelefone2.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Telefone 2", txtTelefone2.Text); }
            if (txtTelefone3.Text != rowPesApt.Telefone3) { rowPes.PESFone3 = txtTelefone3.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Telefone 3", txtTelefone3.Text); }
            rowPes.PESSolicitante = "INTERNET";
            rowPes.PESDataAtualizacao = DateTime.Now;
            dCad.PESSOASTableAdapter.Update(rowPes);

            //Salva correio, email, seguro conteudo (se necessario), usuario e senha
            dCad.APARTAMENTOSTableAdapter.Fill(dCad.APARTAMENTOS, rowPesApt.APT);
            dCadastro.APARTAMENTOSRow rowApt = dCad.APARTAMENTOS[0];
            int intAPTFormaPagtoNova = 4;
            if (chkCorreio.Checked && (int)rblEmailBoleto.SelectedItem.Value == 0) intAPTFormaPagtoNova = 2;
            else if (!chkCorreio.Checked && (int)rblEmailBoleto.SelectedItem.Value == 0) intAPTFormaPagtoNova = 4;
            else if (chkCorreio.Checked && (int)rblEmailBoleto.SelectedItem.Value == 1) intAPTFormaPagtoNova = 12;
            else if (!chkCorreio.Checked && (int)rblEmailBoleto.SelectedItem.Value == 1) intAPTFormaPagtoNova = 14;
            else if (chkCorreio.Checked && (int)rblEmailBoleto.SelectedItem.Value == 2) intAPTFormaPagtoNova = 22;
            else if (!chkCorreio.Checked && (int)rblEmailBoleto.SelectedItem.Value == 2) intAPTFormaPagtoNova = 24;
            if (intAPTFormaPagtoNova != rowPesApt.APTFormaPagto)
            {
                if (rowPesApt.Proprietario == 1)
                    rowApt.APTFormaPagtoProprietario_FPG = intAPTFormaPagtoNova;
                else
                    rowApt.APTFormaPagtoInquilino_FPG = intAPTFormaPagtoNova;
                lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Correio", chkCorreio.Checked ? "QUERO receber correspondência por correio" : "NÃO quero receber correspondência por correio");
                lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - E-mail Boletos", rblEmailBoleto.SelectedItem.Text);
            }
            if (panSeguroConteudo.Visible && (chkSeguroConteudo.Checked != rowPesApt.APTSeguro))
            {
                rowApt.APTSeguro = chkSeguroConteudo.Checked;
                lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Seguro Conteúdo", chkSeguroConteudo.Checked ? "Adesão ao programa seguro conteúdo" : "Exclusão do programa seguro conteúdo");
            }
            if (txtUsuario.Text.Trim() != "" && txtUsuario.Text != rowPesApt.Usuario)
            {
                if (rowPesApt.Proprietario == 1)
                    rowApt.APTUsuarioInternetProprietario = txtUsuario.Text;
                else
                    rowApt.APTUsuarioInternetInquilino = txtUsuario.Text;
                lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Usuário", txtUsuario.Text);
            }
            if (txtNovaSenha.Text.Trim() != "" && txtNovaSenha.Text != rowPesApt.Senha)
            {
                if (rowPesApt.Proprietario == 1)
                    rowApt.APTSenhaInternetProprietario = txtNovaSenha.Text;
                else
                    rowApt.APTSenhaInternetInquilino = txtNovaSenha.Text;
                lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Senha", txtNovaSenha.Text);
            }
            dCad.APARTAMENTOSTableAdapter.Update(rowApt);

            //Salva classificados
            if (panClassificados.Visible)
            {
                //Dados classificados
                if (txtSite.Text != rowPesApt.Site || txtVideo.Text != rowPesApt.Video || txtDescricao.Text != rowPesApt.Descricao)
                {
                    dCad.ST_CLAssificadosTableAdapter.Fill(dCad.ST_CLAssificados, rowPesApt.PES);
                    dCadastro.ST_CLAssificadosRow rowCla;
                    if (dCad.ST_CLAssificados.Count > 0)
                        rowCla = dCad.ST_CLAssificados[0];
                    else
                    {
                        rowCla = dCad.ST_CLAssificados.NewST_CLAssificadosRow();
                        rowCla.CLA_PES = rowPesApt.PES;
                        dCad.ST_CLAssificados.AddST_CLAssificadosRow(rowCla);
                    }
                    if (txtSite.Text != rowPesApt.Site) { rowCla.CLASite = txtSite.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Site", txtSite.Text); }
                    if (txtVideo.Text != rowPesApt.Video) { rowCla.CLAVideo = txtVideo.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Vídeo", txtVideo.Text); }
                    if (txtDescricao.Text != rowPesApt.Descricao) { rowCla.CLADescricao = txtDescricao.Text; lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Descrição", txtDescricao.Text); }
                    dCad.ST_CLAssificadosTableAdapter.Update(rowCla);
                }

                //Lista ramos de atividade
                string strRavAlterado = "";
                string strRavAlteradoDesc = "";
                for (int i = 0; i < (lstRamoAtividade.Items.Count); i++)
                    if (lstRamoAtividade.Items[i].Selected)
                    {
                        strRavAlterado = strRavAlterado + "[" + lstRamoAtividade.Items[i].Value.ToString() + "]";
                        strRavAlteradoDesc = strRavAlteradoDesc + lstRamoAtividade.Items[i].Text + ", ";
                    }

                //Compara com a selecao oirginal
                if (lblRavOriginal.Text != strRavAlterado)
                {
                    //Apaga originais
                    dCad.PESxRAVTableAdapter.DeletePESxRAV(rowPesApt.PES);

                    //Insere novos
                    for (int i = 0; i < (lstRamoAtividade.Items.Count); i++)
                        if (lstRamoAtividade.Items[i].Selected)
                            dCad.PESxRAVTableAdapter.InsertPESxRAV(rowPesApt.PES, (int)lstRamoAtividade.Items[i].Value);
                    if (strRavAlteradoDesc != "") { strRavAlteradoDesc = strRavAlteradoDesc.Substring(0, (strRavAlteradoDesc.Length - 2)); }
                    lstAlteracoes.Add((lstAlteracoes.Count + 1).ToString("00") + " - Ramos de Atividade", strRavAlteradoDesc);

                    //Atualiza ramos de atividade original
                    lblRavOriginal.Text = strRavAlterado;
                }
            }

            //Gera o email de atualizacao se tem email de destino
            if (!rowPesApt.IsEmailNull())
                if (rowPesApt.Email != "")
                    EnviaEmailAlteracao(rowPesApt.Email, GeraCodigoEmail());

            //Sucesso
            lblMsg.Text = "Alterações salvas com sucesso!";
            panMsg.Visible = true;
        }

        private string GeraCodigoEmail()
        {
            string retorno = "";
            if (lstAlteracoes == null)
                retorno = "Nenhum campo foi alterado";
            else
                foreach (DictionaryEntry DE in lstAlteracoes)
                {
                    string Titulo = DE.Key.ToString();
                    string Conteudo = DE.Value.ToString();
                    retorno += string.Format("{0} - {1}<br/>",
                        Server.HtmlEncode(Titulo), Server.HtmlEncode(Conteudo));
                }
            return retorno;
        }

        private void EnviaEmailAlteracao(string Destino, string strAlteracoes)
        {
            try
            {
                //Cria e-mail
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    email.To.Add(Destino);

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Alteração de cadastro";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>ALTERAÇÃO DE CADASTRO</b></p>\r\n" +
                                               "      <p><b>Data da Alteração:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               ((Cred.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : "      <p><b>Bloco:</b> {2}</p>\r\n") +
                                               "      <p><b>Unidade:</b> {3}</p>\r\n" +
                                               "      \r\n" +
                                               "      <p><b>Alterações:</b><br/>{4}</p>\r\n" +
                                               "   </body>\r\n" +
                                               "</html>", 
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo),
                                               Server.HtmlEncode(Cred.rowUsuario.BLOCodigo), Server.HtmlEncode(Cred.rowUsuario.APTNumero), strAlteracoes);

                    //Envia email
                    SmtpClient1.Send(email);
                }
            }
            catch (Exception)
            { }
        }        
    }
}