﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using CadastroUtil.Datasets;

namespace Cadastro.Pages_Portal
{
    public partial class CadastroUsuario : System.Web.UI.Page
    {
        private dCadastro dCad = new dCadastro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "12px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dCad = new dCadastro(Cred.EMP);

            //Inicia tela
            if (!IsPostBack)
                IniciaDados();

            //Carrega tela
            Carregar();
        }

        private void IniciaDados()
        {
            //Carrega campo site
            cmbSite.DataBind();

            //Seta dados especificos para nao adm
            if (Cred.TUsuario != Credencial.TipoUsuario.Administradora)
            {
                //Tipos de usuario nao Admin
                dsTipoUsuarioSiteCombo.FilterExpression = "TUS NOT IN (1, 7)";

                //Oculta colunas fixas
                (grdUsuario.Columns["colSite"] as GridViewDataComboBoxColumn).Visible = false;
            }

            //Carrega campo tipo de usuario
            cmbTipoUsuario.DataBind();
        }

        private void Carregar()
        {
            //Monta combos do grid de usuarios e seta combos de usuario nao adm
            if (Cred.TUsuario != Credencial.TipoUsuario.Administradora)
            {
                //Desabilita site e condominio
                cmbSite.Enabled = cmbCondominio.Enabled = false;

                //Tipos de usuario nao Admin
                dsTipoUsuarioSite.FilterExpression = "TUS NOT IN (1, 7)";
                dsTipoUsuarioSite.DataBind();
            }
            (grdUsuario.Columns["colTipoUsuario"] as GridViewDataComboBoxColumn).PropertiesComboBox.DataSource = dsTipoUsuarioSite;
            (grdUsuario.Columns["colSite"] as GridViewDataComboBoxColumn).PropertiesComboBox.DataSource = dsEmpresa;

            //Carrega os usuarios
            if (Cred.TUsuario == Credencial.TipoUsuario.Administradora)
                dCad.USuaRiosTableAdapter.Fill(dCad.USuaRios);
            else
                dCad.USuaRiosTableAdapter.FillByEMPCON(dCad.USuaRios, Cred.EMP, Cred.CON);
            grdUsuario.DataSource = dCad.USuaRios;
            grdUsuario.DataBind();
        }

        private void CarregarCondominio()
        {
            //Seta EMP
            dCad.EMP = Convert.ToInt32(cmbSite.Value);

            //Carrega o campo de condominios
            dCad.EnforceConstraints = false;
            dCad.CONDOMINIOSTableAdapter.FillCombo(dCad.CONDOMINIOS, Convert.ToInt32(cmbSite.Value));
            cmbCondominio.DataSource = dCad.CONDOMINIOS;
            cmbCondominio.DataBind();
            cmbCondominio.Text = "";
        }

        private void CarregarBloco()
        {
            //Seta EMP
            dCad.EMP = Convert.ToInt32(cmbSite.Value);

            //Carrega o campo de blocos
            dCad.EnforceConstraints = false;
            dCad.BLOCOSTableAdapter.FillCombo(dCad.BLOCOS, Convert.ToInt32(cmbCondominio.Value));
            cmbBloco.DataSource = dCad.BLOCOS;
            cmbBloco.DataBind();
            cmbBloco.Text = "";
        }

        private void CarregarApartamento()
        {
            //Seta EMP
            dCad.EMP = Convert.ToInt32(cmbSite.Value);

            //Carrega o campo de apartamentos
            dCad.EnforceConstraints = false;
            dCad.APARTAMENTOSTableAdapter.FillCombo(dCad.APARTAMENTOS, Convert.ToInt32(cmbBloco.Value));
            cmbApartamento.DataSource = dCad.APARTAMENTOS;
            cmbApartamento.DataBind();
            cmbApartamento.Text = "";
        }

        protected void cmbTipoUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica quais campos exibe dependendo do tipo de usuario
            VerificaExibicaoCampos((int)cmbTipoUsuario.Value);

            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbTipoUsuario.Value) == 0)
                cmbTipoUsuario.SelectedIndex = -1;

            //Posiciona no mesmo campo
            this.smnCadastroUsuario.SetFocus(cmbTipoUsuario);
        }

        protected void VerificaExibicaoCampos(int intTipoUsuario)
        {
            //Verifica quais campos exibe dependendo do tipo de usuario
            if (Cred.TUsuario == Credencial.TipoUsuario.Administradora && intTipoUsuario == 0 || intTipoUsuario == (int)Credencial.TipoUsuario.Administradora)
            {
                floCampos.FindItemOrGroupByName("loiSite").Visible = false;
                floCampos.FindItemOrGroupByName("loiAdvogado").Visible = false;
                floCampos.FindItemOrGroupByName("loiCondominio").Visible = false;
                floCampos.FindItemOrGroupByName("loiBloco").Visible = false;
                floCampos.FindItemOrGroupByName("loiUnidade").Visible = false;
            }
            else if (intTipoUsuario == (int)Credencial.TipoUsuario.AdvogadoAdministradora)
            {
                floCampos.FindItemOrGroupByName("loiSite").Visible = false;
                floCampos.FindItemOrGroupByName("loiAdvogado").Visible = true;
                floCampos.FindItemOrGroupByName("loiCondominio").Visible = false;
                floCampos.FindItemOrGroupByName("loiBloco").Visible = false;
                floCampos.FindItemOrGroupByName("loiUnidade").Visible = false;
            }
            else if (Cred.TUsuario == Credencial.TipoUsuario.Administradora)
            {
                floCampos.FindItemOrGroupByName("loiSite").Visible = true;
                floCampos.FindItemOrGroupByName("loiAdvogado").Visible = false;
                floCampos.FindItemOrGroupByName("loiCondominio").Visible = true;
                floCampos.FindItemOrGroupByName("loiBloco").Visible = true;
                floCampos.FindItemOrGroupByName("loiUnidade").Visible = true;
            }
            else
            {
                floCampos.FindItemOrGroupByName("loiSite").Visible = true;
                floCampos.FindItemOrGroupByName("loiAdvogado").Visible = false;
                floCampos.FindItemOrGroupByName("loiCondominio").Visible = true;
                floCampos.FindItemOrGroupByName("loiBloco").Visible = true;
                floCampos.FindItemOrGroupByName("loiUnidade").Visible = true;
            }
        }

        protected void cmbAdvogadoAdministradora_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbAdvogadoAdministradora.Value) == 0)
                cmbAdvogadoAdministradora.SelectedIndex = -1;

            //Posiciona no mesmo campo
            this.smnCadastroUsuario.SetFocus(cmbAdvogadoAdministradora);
        }

        protected void cmbSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se nao trata evento
            if (!cmbSite.Enabled || cmbSite.Value == null || (int)cmbSite.Value == (int)Session["EMPValue"])
                return;

            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbSite.Value) == 0)
                cmbSite.SelectedIndex = -1;

            //Carrega condominio, bloco e apartamento
            CarregarCondominio();
            CarregarBloco();
            CarregarApartamento();

            //Posiciona no mesmo campo salvando o dado
            Session["EMPValue"] = cmbSite.Value;
            this.smnCadastroUsuario.SetFocus(cmbSite);
        }

        protected void cmbCondominio_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se nao trata evento
            if (!cmbCondominio.Enabled || cmbCondominio.Value == null || (int)cmbCondominio.Value == (int)Session["CONValue"])
                return;

            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbCondominio.Value) == 0)
                cmbCondominio.SelectedIndex = -1;

            //Carrega bloco e apartamento
            CarregarBloco();
            CarregarApartamento();

            //Posiciona no mesmo campo salvando o dado
            Session["CONValue"] = cmbCondominio.Value;
            this.smnCadastroUsuario.SetFocus(cmbCondominio);
        }

        protected void cmbBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se nao trata evento
            if (cmbBloco.Value == null || (int)cmbBloco.Value == (int)Session["BLOValue"])
                return;

            //Carrega apartamento
            CarregarApartamento();

            //Posiciona no mesmo campo salvando o dado
            Session["BLOValue"] = cmbBloco.Value;
            this.smnCadastroUsuario.SetFocus(cmbBloco);
        }

        private void LimpaCampos()
        {
            //Limpa os campos
            cmbTipoUsuario.SelectedIndex = -1;
            cmbAdvogadoAdministradora.SelectedIndex = -1;
            cmbSite.SelectedIndex = -1;
            cmbCondominio.SelectedIndex = -1;
            cmbBloco.SelectedIndex = -1;
            cmbApartamento.SelectedIndex = -1;
            txtNome.Text = "";
            txtUsuario.Text = "";
            txtSenha.Text = "";
            txtNovaSenha.Text = "";
            txtEmail.Text = "";

            //Seta campos
            VerificaExibicaoCampos(0);

            //Limpa variaveis de sessao (controle de postback)
            Session["EMPValue"] = Session["CONValue"] = Session["BLOValue"] = -1;

            //Posiciona no tipo de usuario
            this.smnCadastroUsuario.SetFocus(cmbTipoUsuario);
        }

        protected void cmdAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Altera o cadastro do usuario
            ASPxButton cmdAlterar = (ASPxButton)sender;
            int ID = int.Parse(cmdAlterar.CommandArgument);

            //Procura pelo registro selecionado para alterar
            dCad.USuaRiosTableAdapter.Fill(dCad.USuaRios);
            dCadastro.USuaRiosRow rowUsuario = dCad.USuaRios.FindByUSR(ID);

            //Preenche tela
            lblID.Text = rowUsuario.USR.ToString();
            lblSenha.Text = rowUsuario.USRSenha;
            cmbTipoUsuario.Value = rowUsuario.USR_TUS;
            if ((int)cmbTipoUsuario.Value == (int)Credencial.TipoUsuario.AdvogadoAdministradora)
                cmbAdvogadoAdministradora.Value = Convert.ToInt32(rowUsuario.USRCodigo);
            cmbSite.Value = Session["EMPValue"] = rowUsuario.USR_EMP;
            CarregarCondominio();
            cmbCondominio.Value = Session["CONValue"] = rowUsuario.USR_CON;
            CarregarBloco();
            cmbBloco.Value = Session["BLOValue"] = rowUsuario.USR_BLO;
            CarregarApartamento();
            cmbApartamento.Value = rowUsuario.USR_APT;
            txtNome.Text = rowUsuario.USRNome;
            txtUsuario.Text = rowUsuario.USRUsuario;
            txtEmail.Text = rowUsuario.USREmail;

            //Exibe campo de nova senha
            floCampos.FindItemOrGroupByName("loiSenha").Visible = false;
            floCampos.FindItemOrGroupByName("loiNovaSenha").Visible = true;

            //Verifica quais campos exibe dependendo do tipo de usuario
            VerificaExibicaoCampos((int)cmbTipoUsuario.Value);

            //Seta campos               
            floCampos.Visible = true;
            grdUsuario.Visible = false;
            cmdCancelar.Visible = true;
            cmdInserirAlterar.Text = "ALTERAR";

            //Posiciona no mesmo campo
            this.smnCadastroUsuario.SetFocus(cmbTipoUsuario);
        }

        protected void cmdRemover_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Remove o cadastro do usuario da tabela
            ASPxButton cmdRemover = (ASPxButton)sender;
            int ID = int.Parse(cmdRemover.CommandArgument);

            //Apaga o registro selecionado para excluir
            dCad.USuaRiosTableAdapter.Delete(ID);

            //Recarrega a tela
            Carregar();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Volta campos
            floCampos.Visible = false;
            grdUsuario.Visible = true;
            cmdCancelar.Visible = false;
            cmdInserirAlterar.Text = "INSERIR NOVO USUÁRIO";
        }

        protected void cmdInserirAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Verifica se apenas exibe campos para inserir
            if (cmdInserirAlterar.Text == "INSERIR NOVO USUÁRIO")
            {
                //Limpa os campos da tela
                LimpaCampos();

                //Seta site e condominio se nao for adm
                if (Cred.TUsuario != Credencial.TipoUsuario.Administradora)
                {
                    cmbSite.Value = Session["EMPValue"] = Cred.EMP;
                    CarregarCondominio();
                    cmbCondominio.Value = Session["CONValue"] = Cred.CON;
                    CarregarBloco();
                    Session["BLOValue"] = -1;
                    CarregarApartamento();
                }

                //Exibe campo de senha
                floCampos.FindItemOrGroupByName("loiSenha").Visible = true;
                floCampos.FindItemOrGroupByName("loiNovaSenha").Visible = false;

                //Seta campos               
                floCampos.Visible = true;
                grdUsuario.Visible = false;
                cmdCancelar.Visible = true;
                cmdInserirAlterar.Text = "INSERIR";
                return;
            }

            //Verifica se existe usuario e senha ja cadastrado
            object oID = null;
            //Base apartamentos em neon e neonsa
            CadastroUtil.Datasets.dCadastroTableAdapters.CondominoTableAdapter taCon = new CadastroUtil.Datasets.dCadastroTableAdapters.CondominoTableAdapter();
            oID = taCon.VerificaUsuarioSenha(txtUsuario.Text, txtSenha.Text);
            if (oID != null)
            {
                lblMsg.Text = "Este usuário e senha já estão cadastrados e não podem ser utilizados";
                panMsg.Visible = true;
                return;
            }
            taCon.Connection.ConnectionString = taCon.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
            oID = taCon.VerificaUsuarioSenha(txtUsuario.Text, txtSenha.Text);
            if (oID != null)
            {
                lblMsg.Text = "Este usuário e senha já estão cadastrados e não podem ser utilizados";
                panMsg.Visible = true;
                return;
            }
            //Base usuarios
            oID = dCad.USuaRiosTableAdapter.VerificaUsuarioSenha(txtUsuario.Text, txtSenha.Text);
            if ((oID != null) && ((int)oID != Convert.ToInt32(lblID.Text == "" ? "0" : lblID.Text)))
            {
                lblMsg.Text = "Este usuário e senha já estão cadastrados e não podem ser utilizados";
                panMsg.Visible = true;
                return;
            }

            //Insere ou Altera
            try
            {
                //Abre uma conexao
                dCad.USuaRiosTableAdapter.Connection.Open();
                if (cmdInserirAlterar.Text == "INSERIR")
                {
                    //Insere o novo registro
                    dCad.USuaRiosTableAdapter.Insert(
                        (int)cmbTipoUsuario.SelectedItem.Value,
                        txtNome.Text.ToUpper(),
                        txtUsuario.Text.ToUpper(),
                        txtSenha.Text.ToUpper(),
                        txtEmail.Text.ToLower(),
                        cmbSite.SelectedIndex <= 0 ? 0 : (int)cmbSite.SelectedItem.Value,
                        cmbCondominio.SelectedIndex <= 0 ? 0 : (int)cmbCondominio.SelectedItem.Value,
                        (int)cmbTipoUsuario.SelectedItem.Value == (int)Credencial.TipoUsuario.AdvogadoAdministradora ? cmbAdvogadoAdministradora.SelectedItem.Value.ToString() : cmbCondominio.SelectedIndex <= 0 ? "" : cmbCondominio.SelectedItem.Text.Substring(0, 6).ToUpper(),
                        cmbBloco.SelectedIndex <= 0 ? 0 : (int)cmbBloco.SelectedItem.Value,
                        cmbBloco.SelectedIndex <= 0 ? "" : cmbBloco.SelectedItem.Text.Substring(0, 2).ToUpper(),
                        cmbApartamento.SelectedIndex <= 0 ? 0 : (int)cmbApartamento.SelectedItem.Value,
                        cmbApartamento.SelectedIndex <= 0 ? "" : cmbApartamento.SelectedItem.Text.ToUpper());

                    //Sucesso
                    lblMsg.Text = "Usuário inserido com sucesso !";
                    panMsg.Visible = true;
                }
                else
                {
                    //Altera o registro
                    dCad.USuaRiosTableAdapter.Update(
                        (int)cmbTipoUsuario.SelectedItem.Value,
                        txtNome.Text.ToUpper(),
                        txtUsuario.Text.ToUpper(),
                        txtNovaSenha.Text == "" ? lblSenha.Text : txtNovaSenha.Text.ToUpper(),
                        txtEmail.Text.ToLower(),
                        cmbSite.SelectedIndex <= 0 ? 0 : (int)cmbSite.SelectedItem.Value,
                        cmbCondominio.SelectedIndex <= 0 ? 0 : (int)cmbCondominio.SelectedItem.Value,
                        (int)cmbTipoUsuario.SelectedItem.Value == (int)Credencial.TipoUsuario.AdvogadoAdministradora ? cmbAdvogadoAdministradora.SelectedItem.Value.ToString() : cmbCondominio.SelectedIndex <= 0 ? "" : cmbCondominio.SelectedItem.Text.Substring(0, 6).ToUpper(),
                        cmbBloco.SelectedIndex <= 0 ? 0 : (int)cmbBloco.SelectedItem.Value,
                        cmbBloco.SelectedIndex <= 0 ? "" : cmbBloco.SelectedItem.Text.Substring(0, 2).ToUpper(),
                        cmbApartamento.SelectedIndex <= 0 ? 0 : (int)cmbApartamento.SelectedItem.Value,
                        cmbApartamento.SelectedIndex <= 0 ? "" : cmbApartamento.SelectedItem.Text.ToUpper(),
                        Convert.ToInt32(lblID.Text));

                    //Sucesso
                    lblMsg.Text = "Usuário alterado com sucesso !";
                    panMsg.Visible = true;
                }
            }
            catch (Exception)
            {
                //Erro
                lblMsg.Text = "Ocorreu um erro no cadastro do usuário, tente novamente.";
                panMsg.Visible = true;
                return;
            }
            finally
            {
                //Fecha a conexao
                dCad.USuaRiosTableAdapter.Connection.Close();
            }

            //Recarrega a tela
            Carregar();

            //Volta campos
            floCampos.Visible = false;
            grdUsuario.Visible = true;
            cmdCancelar.Visible = false;
            cmdInserirAlterar.Text = "INSERIR NOVO USUÁRIO";
        }
    }
}