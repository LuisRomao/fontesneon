﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using PortalUtil;

namespace Cadastro.Pages_Portal
{
    public partial class Arquivo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Pega dados do arquivo a ser aberto
            string strArquivo = Request.QueryString["Arquivo"].ToString();
            
            //Abre o arquivo
            if (strArquivo != null)
            {
                if (Request.QueryString["Tipo"] == null)
                {
                    //Arquivo em pasta fora do dominio
                    Response.ContentType = "application/pdf";
                    Response.WriteFile(ConfigurationManager.AppSettings["PublicacoesArquivos"] + strArquivo + (strArquivo.Contains(".pdf") ? "" : ".pdf"));
                    Response.End();
                }
                else
                {
                    Tipos.TipoDocumento typTipo = (Tipos.TipoDocumento)short.Parse(Request.QueryString["Tipo"]);
                    if (typTipo == Tipos.TipoDocumento.RegrasReserva)
                    {
                        //Arquivo pdf em pasta fora do dominio
                        Response.ContentType = "application/pdf";
                        Response.WriteFile(ConfigurationManager.AppSettings["PublicacoesArquivos"] + strArquivo);
                        Response.End();
                    }
                    else if (typTipo == Tipos.TipoDocumento.Balancete)
                    {
                        //Arquivo em pasta no dominio
                        Response.Redirect(ConfigurationManager.AppSettings["PublicacoesBalancetes"] + strArquivo);
                    }
                    else if (typTipo == Tipos.TipoDocumento.ContratoAtivo)
                    {
                        //Arquivo em pasta no dominio
                        Response.Redirect(ConfigurationManager.AppSettings["PublicacoesContratos"] + strArquivo);
                    }
                    else
                    {
                        //Arquivo em pasta no dominio
                        Response.Redirect(ConfigurationManager.AppSettings["PublicacoesDocumentos"] + strArquivo);
                    }
                }
            }
        }
    }
}