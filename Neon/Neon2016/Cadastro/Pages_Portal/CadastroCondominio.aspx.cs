﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using CadastroUtil.Datasets;

namespace Cadastro.Pages_Portal
{
    public partial class CadastroCondominio : System.Web.UI.Page
    {
        private dCadastro dCad = new dCadastro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dCad = new dCadastro(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega os dados do condominio
            dCad.CONDOMINIOSTableAdapter.Fill(dCad.CONDOMINIOS, Cred.CON);
            grdCondominio.DataSource = dCad.CONDOMINIOS;
            grdCondominio.DataBind();

            //Exibe ou esconde campos
            if (dCad.CONDOMINIOS.Count > 0)
            {
                bool booObservacoes = false;
                (grdCondominio.Rows["colDadosBancarios"] as VerticalGridCategoryRow).Visible = Cred.TUsuario == Credencial.TipoUsuario.AdvogadoAdministradora ? true : false;
                if (Cred.TUsuario == Credencial.TipoUsuario.AdvogadoAdministradora && !dCad.CONDOMINIOS[0].IsCONObservacaoNull() && dCad.CONDOMINIOS[0].CONObservacao != "")
                    (grdCondominio.Rows["colObservacao"] as VerticalGridDataRow).Visible = booObservacoes = true;
                else
                    (grdCondominio.Rows["colObservacao"] as VerticalGridDataRow).Visible = booObservacoes = false;
                if (!dCad.CONDOMINIOS[0].IsCONObservacaoInternetNull() && dCad.CONDOMINIOS[0].CONObservacaoInternet != "")
                    (grdCondominio.Rows["colObservacaoInternet"] as VerticalGridDataRow).Visible = booObservacoes = true;
                else
                    (grdCondominio.Rows["colObservacaoInternet"] as VerticalGridDataRow).Visible = false;
                (grdCondominio.Rows["colObservacoes"] as VerticalGridCategoryRow).Visible = booObservacoes;
            }
        }
    }
}