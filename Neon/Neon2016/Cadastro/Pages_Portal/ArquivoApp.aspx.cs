﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Configuration;
using PortalUtil;

namespace Cadastro.Pages_Portal
{
    public partial class ArquivoApp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Pega dados do arquivo a ser aberto
            string strArquivo = Request.QueryString["ID"].ToString();

            //Descriptografa nome do arquivo
            strArquivo = strArquivo.Replace("N", "0").Replace("e", "1").Replace("O", "2").Replace("c", "3").Replace("I", "4");
            strArquivo = strArquivo.Replace("m", "5").Replace("D", "6").Replace("s", "7").Replace("R", "8").Replace("b", "9");
            strArquivo = strArquivo.Replace("F", "/").Replace("g", "_");

            //Abre o arquivo
            if (strArquivo != null)
            {
                if (Request.QueryString["Tipo"] == null)
                {
                    //Arquivo em pasta fora do dominio
                    Response.ContentType = "application/pdf";
                    Response.WriteFile(ConfigurationManager.AppSettings["PublicacoesArquivos"] + strArquivo + ".pdf");
                    Response.End();
                }
                else
                {
                    Tipos.TipoDocumento typTipo = (Tipos.TipoDocumento)short.Parse(Request.QueryString["Tipo"]);
                    if (typTipo == Tipos.TipoDocumento.Balancete)
                    {
                        //Arquivo em pasta no dominio
                        Response.Redirect(ConfigurationManager.AppSettings["PublicacoesBalancetes"] + strArquivo);
                    }
                    else if (typTipo == Tipos.TipoDocumento.ContratoAtivo)
                    {
                        //Arquivo em pasta no dominio
                        Response.Redirect(ConfigurationManager.AppSettings["PublicacoesContratos"] + strArquivo);
                    }
                    else
                    {
                        //Arquivo em pasta no dominio
                        Response.Redirect(ConfigurationManager.AppSettings["PublicacoesDocumentos"] + strArquivo);
                    }
                }
            }

        }
    }
}