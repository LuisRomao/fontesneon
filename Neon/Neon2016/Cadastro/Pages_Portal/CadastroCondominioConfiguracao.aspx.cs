﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using DevExpress.Web;
using PortalUtil;
using CadastroUtil.Datasets;

namespace Cadastro.Pages_Portal
{
    public partial class CadastroCondominioConfiguracao : System.Web.UI.Page
    {
        private dCadastro dCad = new dCadastro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Sessao para grids
            Session["CON"] = Cred.CON;
            Session["EMP"] = Cred.EMP;

            //Inicia dataset
            dCad = new dCadastro(Cred.EMP);

            //Inicia datasources
            if (Cred.EMP == 3)
            {
                dsFaleConosco.ConnectionString = dsFaleConosco.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsControleMudanca.ConnectionString = dsControleMudanca.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsAutorizacaoEntradaPrestador.ConnectionString = dsAutorizacaoEntradaPrestador.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsSolicitacaoReparo.ConnectionString = dsSolicitacaoReparo.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsCaixaSugestoes.ConnectionString = dsCaixaSugestoes.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsReserva.ConnectionString = dsReserva.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                dsReservaListaConvidados.ConnectionString = dsReservaListaConvidados.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
            }

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }


        private void Carregar()
        {
            //Pasta
            string pasta = string.Format("{0}_{1}", Cred.EMP, Cred.rowUsuario.CONCodigo.Replace(".", "_"));
            lblPasta.Text = string.Format("{0}{1}", ConfigurationManager.AppSettings["PathCondominios"].Replace("http://www.neonimoveis.com.br", "web"), pasta);
            pasta = string.Format("{0}{1}_{2}", ConfigurationManager.AppSettings["PathCondominios"], Cred.EMP, Cred.rowUsuario.CONCodigo.Replace(".", "_"));
            if (!System.IO.Directory.Exists(pasta))
                lblPasta.Text += " - CADASTRAR";

            //Link
            int CONCRI = Cred.CON;
            CONCRI = 100 * CONCRI + (CONCRI + 3) % 17;
            lblLink.Text = string.Format("http://www.neonimoveis.com.br/Neon2016/Pages_Portal/Site.aspx?EMP={0}&CON={1}", Cred.EMP, CONCRI);

            //Pega SMTP do condominio
            dCadastro.ST_SMTpRow rowSMTP = null;
            dCad.ST_SMTpTableAdapter.Fill(dCad.ST_SMTp, Cred.CON);
            if (dCad.ST_SMTp.Count != 0)
                rowSMTP = (dCadastro.ST_SMTpRow)dCad.ST_SMTp.Rows[0];

            //Preenche campos smtp
            txtDominio.Text = (rowSMTP == null) ? "" : rowSMTP.SMTpDominio;
            txtHost.Text = (rowSMTP == null) ? "" : rowSMTP.SMTpHost;
            txtUser.Text = (rowSMTP == null) ? "" : rowSMTP.SMTpUser;
            txtPass.Text = (rowSMTP == null) ? "" : rowSMTP.SMTpPass;
            txtPort.Text = (rowSMTP == null) ? "" : (rowSMTP.IsSMTpPortNull() ? "" : (rowSMTP.SMTpPort == 0 ? "" : rowSMTP.SMTpPort.ToString()));
            txtEmailRemetente.Text = (rowSMTP == null) ? "" : rowSMTP.SMTpRemetente;
            txtEmailRemetenteCircular.Text = (rowSMTP == null) ? "" : rowSMTP.SMTpRemetenteCircular;

            //Pega funcionalidades do condominio
            dCadastro.ST_CondominioFUncionalidadeRow rowFunCon = null;
            dCad.ST_CondominioFUncionalidadeTableAdapter.Fill(dCad.ST_CondominioFUncionalidade, Cred.CON);
            if (dCad.ST_CondominioFUncionalidade.Count != 0)
                rowFunCon = (dCadastro.ST_CondominioFUncionalidadeRow)dCad.ST_CondominioFUncionalidade.Rows[0];

            //Preenche campos
            chkAvisoCorrespondencia.Checked = (rowFunCon == null) ? false : rowFunCon.CFUAvisoCorrespondencia;
            chkCadastroEmail.Checked = (rowFunCon == null) ? false : rowFunCon.CFUCadastroEmail;
            chkCadastroUsuario.Checked = (rowFunCon == null) ? false : rowFunCon.CFUCadastroUsuario;
            chkCirculares.Checked = (rowFunCon == null) ? false : rowFunCon.CFUCirculares;
            chkClassificado.Checked = (rowFunCon == null) ? false : rowFunCon.CFUClassificado;
            chkFaleConoscoCD.Checked = (rowFunCon == null) ? false : rowFunCon.CFUFaleConoscoCD;
            chkFaleConosco.Checked = (rowFunCon == null) ? false : rowFunCon.CFUFaleConosco;
            chkControleMudanca.Checked = (rowFunCon == null) ? false : rowFunCon.CFUControleMudanca;
            chkAutorizacaoEntradaPrestador.Checked = (rowFunCon == null) ? false : rowFunCon.CFUAutorizacaoEntradaPrestador;
            chkSolicitacaoReparo.Checked = (rowFunCon == null) ? false : rowFunCon.CFUSolicitacaoReparo;
            chkCaixaSugestoes.Checked = (rowFunCon == null) ? false : rowFunCon.CFUCaixaSugestoes;
            chkReserva.Checked = (rowFunCon == null) ? false : rowFunCon.CFUReserva;
            chkReservaImplantacao.Checked = (rowFunCon == null) ? false : rowFunCon.CFUReservaImplantacao;
            txtCorBase.Text = (rowFunCon == null) ? "" : rowFunCon.CFUCorBase;

            //Carrega e-mails corpo diretivo
            dCad.CORPODIRETIVOTableAdapter.Fill(dCad.CORPODIRETIVO, Cred.CON);
            grdCorpoDiretivo.DataSource = dCad.CORPODIRETIVO;
            grdCorpoDiretivo.DataBind();
        }

        protected void cmdSalvarSMTP_Click(object sender, EventArgs e)
        {
            //Pega SMTP do condominio
            dCadastro.ST_SMTpRow rowSMTP = dCad.ST_SMTp.NewST_SMTpRow();
            dCad.ST_SMTpTableAdapter.Fill(dCad.ST_SMTp, Cred.CON);
            if (dCad.ST_SMTp.Count != 0)
                rowSMTP = (dCadastro.ST_SMTpRow)dCad.ST_SMTp.Rows[0];

            //Seta campos smtp
            rowSMTP.SMTpDominio = txtDominio.Text.ToUpper();
            rowSMTP.SMTpHost = txtHost.Text.ToLower();
            rowSMTP.SMTpUser = txtUser.Text;
            rowSMTP.SMTpPass = txtPass.Text;
            rowSMTP.SMTpPort = txtPort.Text == "" ? 0 : Convert.ToInt32(txtPort.Text);
            rowSMTP.SMTpRemetente = txtEmailRemetente.Text.ToLower();
            rowSMTP.SMTpRemetenteCircular = txtEmailRemetenteCircular.Text.ToLower();

            //Adiciona ou Atualiza
            if (dCad.ST_SMTp.Count == 0)
            { 
                rowSMTP.SMT_CON = Cred.CON;
                dCad.ST_SMTp.AddST_SMTpRow(rowSMTP);
            }
            dCad.ST_SMTpTableAdapter.Update(dCad.ST_SMTp);
            dCad.ST_SMTp.AcceptChanges();

            //Exibe mensagem
            lblMsgSMTP.Text = "Dados salvos com sucesso!";
            panMsgSMTP.Visible = true;
        }

        protected void txtSMTP_TextChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgSMTP.Text = "";
            panMsgSMTP.Visible = false;
        }

        protected void txtSMTP_ValueChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgSMTP.Text = "";
            panMsgSMTP.Visible = false;
        }

        protected void SalvaFuncionalidade(Tipos.TipoFuncionalidade typFuncionalidade, ASPxLabel lblMsg, Panel panMsg)
        {
            //Pega funcionalidades do condominio
            dCadastro.ST_CondominioFUncionalidadeRow rowFunCon = dCad.ST_CondominioFUncionalidade.NewST_CondominioFUncionalidadeRow();
            dCad.ST_CondominioFUncionalidadeTableAdapter.Fill(dCad.ST_CondominioFUncionalidade, Cred.CON);
            if (dCad.ST_CondominioFUncionalidade.Count != 0)
                rowFunCon = (dCadastro.ST_CondominioFUncionalidadeRow)dCad.ST_CondominioFUncionalidade.Rows[0];

            //Salva campos de acordo com a funcionalidade
            switch (typFuncionalidade)
            {
                case Tipos.TipoFuncionalidade.FaleConosco:
                    rowFunCon.CFUFaleConoscoCD = chkFaleConoscoCD.Checked;
                    rowFunCon.CFUFaleConosco = chkFaleConosco.Checked;
                    break;
                case Tipos.TipoFuncionalidade.ControleMudanca:
                    rowFunCon.CFUControleMudanca = chkControleMudanca.Checked;
                    break;
                case Tipos.TipoFuncionalidade.AutorizacaoEntradaPrestador:
                    rowFunCon.CFUAutorizacaoEntradaPrestador = chkAutorizacaoEntradaPrestador.Checked;
                    break;
                case Tipos.TipoFuncionalidade.SolicitacaoReparo:
                    rowFunCon.CFUSolicitacaoReparo = chkSolicitacaoReparo.Checked;
                    break;
                case Tipos.TipoFuncionalidade.CaixaSugestoes:
                    rowFunCon.CFUCaixaSugestoes = chkCaixaSugestoes.Checked;
                    break;
                case Tipos.TipoFuncionalidade.Reserva:
                    rowFunCon.CFUReserva = chkReserva.Checked;
                    rowFunCon.CFUReservaImplantacao = chkReservaImplantacao.Checked;
                    break;
                default:
                    rowFunCon.CFUAvisoCorrespondencia = chkAvisoCorrespondencia.Checked;
                    rowFunCon.CFUCadastroEmail = chkCadastroEmail.Checked;
                    rowFunCon.CFUCadastroUsuario = chkCadastroUsuario.Checked;
                    rowFunCon.CFUCirculares = chkCirculares.Checked;
                    rowFunCon.CFUClassificado = chkClassificado.Checked;
                    rowFunCon.CFUCorBase = txtCorBase.Text;
                    break;
            }

            if (dCad.ST_CondominioFUncionalidade.Count == 0)
            { 
                rowFunCon.CFU_CON = Cred.CON;
                dCad.ST_CondominioFUncionalidade.AddST_CondominioFUncionalidadeRow(rowFunCon);
            }
            dCad.ST_CondominioFUncionalidadeTableAdapter.Update(dCad.ST_CondominioFUncionalidade);
            dCad.ST_CondominioFUncionalidade.AcceptChanges();

            //Exibe mensagem
            lblMsg.Text = "Dados salvos com sucesso!";
            panMsg.Visible = true;
        }

        protected void cmdSalvarFuncionalidades_Click(object sender, EventArgs e)
        {
            //Salva todas as funcionalidades (sem configuração de e-mails como AvisoCorrespondencia)
            SalvaFuncionalidade(Tipos.TipoFuncionalidade.AvisoCorrespondencia, lblMsgFuncionalidades, panMsgFuncionalidades);
        }

        protected void chkFuncionalidades_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgFuncionalidades.Text = "";
            panMsgFuncionalidades.Visible = false;
        }

        protected void txtFuncionalidade_TextChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgFuncionalidades.Text = "";
            panMsgFuncionalidades.Visible = false;
        }

        protected void cmdSalvarFaleConosco_Click(object sender, EventArgs e)
        {
            //Salva a funcionalidade
            SalvaFuncionalidade(Tipos.TipoFuncionalidade.FaleConosco, lblMsgFaleConosco, panMsgFaleConosco);
        }

        protected void chkFaleConosco_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgFaleConosco.Text = "";
            panMsgFaleConosco.Visible = false;
        }

        protected void cmdSalvarControleMudanca_Click(object sender, EventArgs e)
        {
            //Salva a funcionalidade
            SalvaFuncionalidade(Tipos.TipoFuncionalidade.ControleMudanca, lblMsgControleMudanca, panMsgControleMudanca);
        }

        protected void chkControleMudanca_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgControleMudanca.Text = "";
            panMsgControleMudanca.Visible = false;
        }

        protected void cmdSalvarAutorizacaoEntradaPrestador_Click(object sender, EventArgs e)
        {
            //Salva a funcionalidade
            SalvaFuncionalidade(Tipos.TipoFuncionalidade.AutorizacaoEntradaPrestador, lblMsgAutorizacaoEntradaPrestador, panMsgAutorizacaoEntradaPrestador);
        }

        protected void chkAutorizacaoEntradaPrestador_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgAutorizacaoEntradaPrestador.Text = "";
            panMsgAutorizacaoEntradaPrestador.Visible = false;
        }

        protected void cmdSalvarSolicitacaoReparo_Click(object sender, EventArgs e)
        {
            //Salva a funcionalidade
            SalvaFuncionalidade(Tipos.TipoFuncionalidade.SolicitacaoReparo, lblMsgSolicitacaoReparo, panMsgSolicitacaoReparo);
        }

        protected void chkSolicitacaoReparo_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgSolicitacaoReparo.Text = "";
            panMsgSolicitacaoReparo.Visible = false;
        }

        protected void cmdSalvarCaixaSugestoes_Click(object sender, EventArgs e)
        {
            //Salva a funcionalidade
            SalvaFuncionalidade(Tipos.TipoFuncionalidade.CaixaSugestoes, lblMsgCaixaSugestoes, panMsgCaixaSugestoes);
        }

        protected void chkCaixaSugestoes_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgCaixaSugestoes.Text = "";
            panMsgCaixaSugestoes.Visible = false;
        }

        protected void cmdSalvarReserva_Click(object sender, EventArgs e)
        {
            //Salva a funcionalidade
            SalvaFuncionalidade(Tipos.TipoFuncionalidade.Reserva, lblMsgReserva, panMsgReserva);
        }

        protected void chkReserva_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgReserva.Text = "";
            panMsgReserva.Visible = false;
        }

        protected void chkReservaImplantacao_CheckedChanged(object sender, EventArgs e)
        {
            //Esconde mensagem
            lblMsgReserva.Text = "";
            panMsgReserva.Visible = false;
        }
    }
}