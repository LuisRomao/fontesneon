﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using CadastroUtil.Datasets;

namespace Cadastro.Pages_Portal
{
    public partial class CadastroAviso : System.Web.UI.Page
    {
        private dCadastro dCad = new dCadastro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "12px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dCad = new dCadastro(Cred.EMP);

            //Inicia tela
            if (!IsPostBack)
                IniciaDados();

            //Carrega tela
            if (grdAviso.Visible)
                Carregar();
        }

        private void IniciaDados()
        {
            //Carrega site
            rblSite.DataBind();
            rblSite.Items.RemoveAt(0);

            //Verifica se ADM ou nao e seta campos
            if (Cred.TUsuario == Credencial.TipoUsuario.Administradora)
            {
                floSite.Visible = true;
                floComandos.Visible = grdAviso.Visible = false;
            }
            else
            {
                floSite.Visible = false;
                rblSite.Value = Cred.EMP;
                cmbCondominio.Enabled = false;
                (grdAviso.Columns["colCondominio"] as GridViewDataTextColumn).Visible = false;
            }
        }

        protected void rblSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Carrega os condominios do site selecionado
            Carregar();
        }

        private void Carregar()
        {
            //Seta EMP
            dCad.EMP = Convert.ToInt32(rblSite.SelectedItem.Value);

            //Exibe comandos e tela
            floComandos.Visible = grdAviso.Visible = true;

            //Carrega condominio, bloco e apartamento
            CarregarCondominio();
            CarregarBloco();
            CarregarApartamento();

            //Carrega os avisos
            if (Cred.TUsuario == Credencial.TipoUsuario.Administradora)
                dCad.ST_AViSosTableAdapter.FillGrid(dCad.ST_AViSos);
            else
                dCad.ST_AViSosTableAdapter.FillGridByCON(dCad.ST_AViSos, Cred.CON);
            grdAviso.DataSource = dCad.ST_AViSos;
            grdAviso.DataBind();
        }

        private void CarregarCondominio()
        {
            //Seta EMP
            dCad.EMP = Convert.ToInt32(rblSite.SelectedItem.Value);

            //Carrega o campo de condominios
            dCad.EnforceConstraints = false;
            dCad.CONDOMINIOSTableAdapter.FillComboTodos(dCad.CONDOMINIOS, Convert.ToInt32(rblSite.SelectedItem.Value));
            cmbCondominio.DataSource = dCad.CONDOMINIOS;
            cmbCondominio.DataBind();
            cmbCondominio.Text = "";

            //Remove TODOS temporariamente (ate a criacao de usuario ADM MASTER)
            cmbCondominio.Items.Remove(cmbCondominio.Items.FindByText("*  TODOS OS CONDOMÍNIOS"));
        }

        private void CarregarBloco()
        {
            //Seta EMP
            dCad.EMP = Convert.ToInt32(rblSite.SelectedItem.Value);

            //Carrega o campo de blocos
            dCad.EnforceConstraints = false;
            dCad.BLOCOSTableAdapter.FillComboTodos(dCad.BLOCOS, Convert.ToInt32(cmbCondominio.Value));
            cmbBloco.DataSource = dCad.BLOCOS;
            cmbBloco.DataBind();
            cmbBloco.Text = "* TODOS OS BLOCOS";
        }

        private void CarregarApartamento()
        {
            //Seta EMP
            dCad.EMP = Convert.ToInt32(rblSite.SelectedItem.Value);

            //Carrega o campo de apartamentos
            dCad.EnforceConstraints = false;
            dCad.APARTAMENTOSTableAdapter.FillCombo(dCad.APARTAMENTOS, Convert.ToInt32(cmbBloco.Value));
            cmbApartamento.DataSource = dCad.APARTAMENTOS;
            cmbApartamento.DataBind();
            cmbApartamento.Text = "";
            if (Convert.ToInt32(cmbBloco.Value) > 0)
            {
                dCad.APARTAMENTOSTableAdapter.FillList(dCad.APARTAMENTOS, Convert.ToInt32(cmbBloco.Value));
                lstApartamentos.DataSource = dCad.APARTAMENTOS;
                lstApartamentos.DataBind();
                lstApartamentos.UnselectAll();
            }
            chkTodosApartamentos.Checked = true;
            lstApartamentos.Visible = false;
        }

        protected void cmbCondominio_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se nao trata evento
            if (Session["CONValue"] != null && (int)cmbCondominio.Value == (int)Session["CONValue"])
                return;

            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbCondominio.Value) == -1)
                cmbCondominio.SelectedIndex = -1;

            //Carrega bloco e apartamento
            CarregarBloco();
            CarregarApartamento();

            //Verifica campos a serem exibidos
            floCampos.FindItemOrGroupByName("loiBloco").Visible = Convert.ToInt32(cmbCondominio.Value) > 0 ? true : false;
            floCampos.FindItemOrGroupByName("loiUnidade").Visible = false;
            floCampos.FindItemOrGroupByName("loiUnidadeList").Visible = false;

            //Posiciona no mesmo campo salvando o dado
            Session["CONValue"] = cmbCondominio.Value;
            this.smnCadastroAviso.SetFocus(cmbCondominio);
        }

        protected void cmbBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se nao trata evento
            if (Session["BLOValue"] != null && (int)cmbBloco.Value == (int)Session["BLOValue"])
                return;

            //Carrega apartamento
            CarregarApartamento();

            //Verifica campos a serem exibidos
            floCampos.FindItemOrGroupByName("loiUnidadeList").Visible = Convert.ToInt32(cmbBloco.Value) > 0 ? true : false;

            //Posiciona no mesmo campo salvando o dado
            Session["BLOValue"] = cmbBloco.Value;
            this.smnCadastroAviso.SetFocus(cmbBloco);
        }

        protected void chkTodosApartamentos_CheckedChanged(object sender, EventArgs e)
        {
            //Verifica se exibe ou esconde lista de apartamentos
            lstApartamentos.Visible = !(bool)chkTodosApartamentos.Checked;            
            if ((bool)chkTodosApartamentos.Checked)
                lstApartamentos.UnselectAll();

            //Posiciona no mesmo campo
            this.smnCadastroAviso.SetFocus(chkTodosApartamentos);
        }

        private void LimpaCampos()
        {
            //Limpa os campos
            cmbCondominio.SelectedIndex = -1;
            cmbBloco.SelectedIndex = -1;
            cmbApartamento.SelectedIndex = -1;
            chkTodosApartamentos.Checked = false;
            lstApartamentos.UnselectAll();
            txtTitulo.Text = "";
            txtMensagem.Text = "";
            dteDataInicial.Text = "";
            dteDataFinal.Text = "";

            //Limpa variaveis de sessao (controle de postback)
            Session["CONValue"] = Session["BLOValue"] = null;

            //Posiciona no condominio (ADM) ou bloco
            if (Cred.TUsuario == Credencial.TipoUsuario.Administradora)
                this.smnCadastroAviso.SetFocus(cmbCondominio);
            else
                this.smnCadastroAviso.SetFocus(cmbBloco);
        }

        protected void cmdAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Altera o cadastro do usuario
            ASPxButton cmdAlterar = (ASPxButton)sender;
            int ID = int.Parse(cmdAlterar.CommandArgument);

            //Procura pelo registro selecionado para alterar
            dCad.ST_AViSosTableAdapter.Fill(dCad.ST_AViSos);
            dCadastro.ST_AViSosRow rowAviso = dCad.ST_AViSos.FindByAVS(ID);

            //Preenche tela
            lblID.Text = rowAviso.AVS.ToString();
            CarregarCondominio();
            cmbCondominio.Value = Session["CONValue"] = rowAviso.AVS_CON;
            CarregarBloco();
            cmbBloco.Value = Session["BLOValue"] = rowAviso.AVS_BLO;
            CarregarApartamento();
            cmbApartamento.Value = rowAviso.AVS_APT;
            txtTitulo.Text = rowAviso.AVSTitulo;
            txtMensagem.Text = rowAviso.AVSMensagem;
            if (!rowAviso.IsAVSDataInicialNull()) dteDataInicial.Text = rowAviso.AVSDataInicial.ToShortDateString();
            if (!rowAviso.IsAVSDataFinalNull()) dteDataFinal.Text = rowAviso.AVSDataFinal.ToShortDateString();

            //Seta visibilidade dos campos de selecao
            floCampos.FindItemOrGroupByName("loiBloco").Visible = true;
            if (rowAviso.AVS_BLO == 0)
            {
                chkTodosApartamentos.Checked = true;
                floCampos.FindItemOrGroupByName("loiUnidade").Visible = false;
                floCampos.FindItemOrGroupByName("loiUnidadeList").Visible = false;
            }
            else if (rowAviso.AVS_BLO > 0 && rowAviso.AVS_APT == 0)
            {
                chkTodosApartamentos.Checked = true;
                floCampos.FindItemOrGroupByName("loiUnidade").Visible = false;
                floCampos.FindItemOrGroupByName("loiUnidadeList").Visible = true;
            }
            else
            {
                chkTodosApartamentos.Checked = false;
                floCampos.FindItemOrGroupByName("loiUnidade").Visible = true;
                floCampos.FindItemOrGroupByName("loiUnidadeList").Visible = false;
            }

            //Seta campos               
            floCampos.Visible = true;
            cmbCondominio.Enabled = cmbBloco.Enabled = cmbApartamento.Enabled = chkTodosApartamentos.Enabled = false;
            rblSite.Visible = grdAviso.Visible = false;
            cmdCancelar.Visible = true;
            cmdInserirAlterar.Text = "ALTERAR";

            //Posiciona no condominio (ADM) ou bloco
            if (Cred.TUsuario == Credencial.TipoUsuario.Administradora)
                this.smnCadastroAviso.SetFocus(cmbCondominio);
            else
                this.smnCadastroAviso.SetFocus(cmbBloco);
        }

        protected void cmdRemover_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Remove o cadastro do usuario da tabela
            ASPxButton cmdRemover = (ASPxButton)sender;
            int ID = int.Parse(cmdRemover.CommandArgument);

            //Apaga o registro selecionado para excluir
            dCad.ST_AViSosTableAdapter.Delete(ID);

            //Recarrega a tela
            Carregar();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Recarrega grid
            Carregar();

            //Volta campos
            floCampos.Visible = false;
            rblSite.Visible = grdAviso.Visible = true;
            cmdCancelar.Visible = false;
            cmdInserirAlterar.Text = "INSERIR NOVO AVISO";
        }

        protected void cmdInserirAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Verifica se apenas exibe campos para inserir
            if (cmdInserirAlterar.Text == "INSERIR NOVO AVISO")
            {
                //Limpa os campos da tela
                LimpaCampos();

                //Seta condominio se nao for adm
                if (Cred.TUsuario != Credencial.TipoUsuario.Administradora)
                {
                    CarregarCondominio();
                    cmbCondominio.Value = Session["CONValue"] = Cred.CON;
                    CarregarBloco();
                    Session["BLOValue"] = null;
                    CarregarApartamento();
                }

                //Seta campos               
                floCampos.Visible = true;
                cmbCondominio.Enabled = Cred.TUsuario == Credencial.TipoUsuario.Administradora ? true : false;
                cmbBloco.Enabled = cmbApartamento.Enabled = chkTodosApartamentos.Enabled = true;
                rblSite.Visible = grdAviso.Visible = false;
                cmdCancelar.Visible = true;
                cmdInserirAlterar.Text = "INSERIR";

                //Verifica campos a serem exibidos
                floCampos.FindItemOrGroupByName("loiBloco").Visible = Convert.ToInt32(cmbCondominio.Value) > 0 ? true : false;
                floCampos.FindItemOrGroupByName("loiUnidade").Visible = false;
                floCampos.FindItemOrGroupByName("loiUnidadeList").Visible = false;

                return;
            }

            //Verifica consistencias
            if (floCampos.FindItemOrGroupByName("loiUnidadeList").Visible && lstApartamentos.Visible && lstApartamentos.SelectedItems.Count == 0)
            {
                lblMsg.Text = "Selecione pelo menos uma unidade !";
                panMsg.Visible = true;
                return;
            }

            //Insere ou Altera
            try
            {
                //Abre uma conexao
                dCad.EMP = Convert.ToInt32(rblSite.SelectedItem.Value);
                dCad.ST_AViSosTableAdapter.Connection.Open();
                if (cmdInserirAlterar.Text == "INSERIR")
                {
                    //Insere o novo registro
                    if (floCampos.FindItemOrGroupByName("loiUnidadeList").Visible && cmbBloco.SelectedIndex > 0 && !chkTodosApartamentos.Checked)
                    {
                        foreach (ListEditItem item in lstApartamentos.Items)
                        {
                            if (!item.Selected) continue;

                            dCad.ST_AViSosTableAdapter.Insert(
                                (int)cmbCondominio.SelectedItem.Value,
                                cmbBloco.SelectedIndex <= 0 ? 0 : (int)cmbBloco.SelectedItem.Value,
                                (int)item.Value,
                                txtTitulo.Text,
                                txtMensagem.Text,
                                dteDataInicial.Text == "" ? DateTime.Now.Date : Convert.ToDateTime(dteDataInicial.Text),
                                dteDataFinal.Text == "" ? (DateTime?)null : Convert.ToDateTime(dteDataFinal.Text),
                                DateTime.Now,
                                String.Format("{0}{1}", Cred.rowUsuario.Nome, (!Cred.rowUsuario.IsAPTNull() && Cred.rowUsuario.APT != 0) ? " (APT " + Cred.rowUsuario.APT + ")" : !Cred.rowUsuario.IsUSRNull() ? " (USR " + Cred.rowUsuario.USR + ")" : ""));
                        }
                    }
                    else
                    {
                        dCad.ST_AViSosTableAdapter.Insert(
                            (int)cmbCondominio.SelectedItem.Value,
                            cmbBloco.SelectedIndex <= 0 ? 0 : (int)cmbBloco.SelectedItem.Value,
                            (cmbApartamento.SelectedIndex <= 0 || chkTodosApartamentos.Checked) ? 0 : (int)cmbApartamento.SelectedItem.Value,
                            txtTitulo.Text,
                            txtMensagem.Text,
                            dteDataInicial.Text == "" ? DateTime.Now.Date : Convert.ToDateTime(dteDataInicial.Text),
                            dteDataFinal.Text == "" ? (DateTime?)null : Convert.ToDateTime(dteDataFinal.Text),
                            DateTime.Now,
                            String.Format("{0}{1}", Cred.rowUsuario.Nome, (!Cred.rowUsuario.IsAPTNull() && Cred.rowUsuario.APT != 0) ? " (APT " + Cred.rowUsuario.APT + ")" : !Cred.rowUsuario.IsUSRNull() ? " (USR " + Cred.rowUsuario.USR + ")" : ""));
                    }

                    //Sucesso
                    lblMsg.Text = "Aviso inserido com sucesso !";
                    panMsg.Visible = true;
                }
                else
                {
                    //Altera o registro
                    dCad.ST_AViSosTableAdapter.Update(
                        (int)cmbCondominio.SelectedItem.Value,
                        cmbBloco.SelectedIndex <= 0 ? 0 : (int)cmbBloco.SelectedItem.Value,
                        cmbApartamento.SelectedIndex <= 0 ? 0 : (int)cmbApartamento.SelectedItem.Value,
                        txtTitulo.Text,
                        txtMensagem.Text,
                        dteDataInicial.Text == "" ? DateTime.Now.Date : Convert.ToDateTime(dteDataInicial.Text),
                        dteDataFinal.Text == "" ? (DateTime?)null : Convert.ToDateTime(dteDataFinal.Text),
                        String.Format("{0}{1}", Cred.rowUsuario.Nome, (!Cred.rowUsuario.IsAPTNull() && Cred.rowUsuario.APT != 0) ? " (APT " + Cred.rowUsuario.APT + ")" : !Cred.rowUsuario.IsUSRNull() ? " (USR " + Cred.rowUsuario.USR + ")" : ""),
                        Convert.ToInt32(lblID.Text));

                    //Sucesso
                    lblMsg.Text = "Aviso alterado com sucesso !";
                    panMsg.Visible = true;
                }
            }
            catch (Exception)
            {
                //Erro
                lblMsg.Text = "Ocorreu um erro no cadastro do aviso, tente novamente.";
                panMsg.Visible = true;
                return;
            }
            finally
            {
                //Fecha a conexao
                dCad.ST_AViSosTableAdapter.Connection.Close();
            }

            //Recarrega a tela
            Carregar();

            //Volta campos
            floCampos.Visible = false;
            rblSite.Visible = grdAviso.Visible = true;
            cmdCancelar.Visible = false;
            cmdInserirAlterar.Text = "INSERIR NOVO AVISO";
        }
    }
}