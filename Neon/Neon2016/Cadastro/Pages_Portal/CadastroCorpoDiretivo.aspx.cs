﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using CadastroUtil.Datasets;

namespace Cadastro.Pages_Portal
{
    public partial class CadastroCorpoDiretivo : System.Web.UI.Page
    {
        private dCadastro dCad = new dCadastro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dCad = new dCadastro(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega os dados do corpo diretivo
            dCad.CORPODIRETIVOTableAdapter.Fill(dCad.CORPODIRETIVO, Cred.CON);
            grdCorpoDiretivo.DataSource = dCad.CORPODIRETIVO;
            grdCorpoDiretivo.DataBind();
        }
    }
}