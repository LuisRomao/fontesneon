﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using CadastroUtil.Datasets;

namespace Cadastro.Pages_Portal
{
    public partial class CadastroDocumentacao : System.Web.UI.Page
    {
        private dCadastro dCad = new dCadastro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dCad = new dCadastro(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Pega tipo de documento
            Tipos.TipoDocumento typTipo = (Tipos.TipoDocumento)short.Parse(Request.QueryString["Tipo"]);

            //Carrega os documentos
            if (typTipo == Tipos.TipoDocumento.ContratoAtivo)
            {
                //Contratos Ativos
                dCad.DOCumentacaoTableAdapter.Fill(dCad.DOCumentacao, Cred.EMP, Cred.CON);
                grdDocumento.DataSource = dCad.DOCumentacao;
            }
            else
            {
                //Documentacao
                dCad.PUBlicacaoTableAdapter.Fill(dCad.PUBlicacao, Cred.CON, (int)typTipo);
                grdDocumento.DataSource = dCad.PUBlicacao;
            }
            grdDocumento.DataBind();

            //Indica tipo de documento
            switch (typTipo)
            {
                case Tipos.TipoDocumento.Ata:
                    lblTituloPagina.Text = "Documentação - Atas";
                    break;
                case Tipos.TipoDocumento.Convocacao:
                    lblTituloPagina.Text = "Documentação - Convocações";
                    break;
                case Tipos.TipoDocumento.RegulamentoInterno:
                    lblTituloPagina.Text = "Documentação - Regulamento Interno";
                    break;
                case Tipos.TipoDocumento.Convencao:
                    lblTituloPagina.Text = "Documentação - Convenção";
                    break;
                case Tipos.TipoDocumento.Outros:
                    lblTituloPagina.Text = "Documentação - Outros";
                    break;
                case Tipos.TipoDocumento.ContratoAtivo:
                    lblTituloPagina.Text = "Contratos Ativos";
                    break;
            }

            //Exibe ou esconde colunas
            (grdDocumento.Columns["colData"] as GridViewDataTextColumn).Visible = (typTipo == Tipos.TipoDocumento.ContratoAtivo) ? false : true;
            (grdDocumento.Columns["colHorario"] as GridViewDataTextColumn).Visible = (typTipo == Tipos.TipoDocumento.ContratoAtivo) ? false : true;
        }

        protected void grdDocumento_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "Arquivo")
            {
                Button btn = grdDocumento.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "cmdAbrir") as Button;
                btn.OnClientClick = String.Format("javascript: window.open('Arquivo.aspx?Arquivo={0}&Tipo={1}', '{2}'); return false;", Convert.ToString(e.CellValue), btn.CommandArgument, !Request.Browser.IsMobileDevice ? "_self" : "_blank");
            }
        }
    }
}