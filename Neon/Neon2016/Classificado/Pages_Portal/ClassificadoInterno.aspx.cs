﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using DevExpress.Web.ASPxPivotGrid;
using PortalUtil;
using Classificado.Datasets;

namespace Classificado.Pages_Portal
{
    public partial class ClassificadoInterno : System.Web.UI.Page
    {
        private dClassificado dCla = new dClassificado();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "12px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dCla = new dClassificado(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega o campo de ramos de atividade do formulario
            dCla.RamoAtiVidadeTableAdapter.FillCombo(dCla.RamoAtiVidade);
            cmbRamoAtividade.DataSource = dCla.RamoAtiVidade;
            cmbRamoAtividade.DataBind();

            //Carrega todos os classificados (campos de busca vazios)
            Buscar();
        }

        protected void hplLink_DataBinding(object sender, EventArgs e)
        {
            //Seta link de site e video dentro do grid
            ASPxHyperLink link = (ASPxHyperLink)sender;
            link.Text = ((DevExpress.Web.ASPxPivotGrid.PivotGridFieldValueTemplateContainer)link.Parent).Text;
            link.NavigateUrl = link.Text;
        }

        protected void cmdBuscar_Click(object sender, EventArgs e)
        {
            //Busca os classificados de acordo com os campos de busca
            Buscar();
        }

        private void Buscar()
        {
            //Carrega tela com todos os condominos com classificados ativo
            dCla.CondominosClassificadosTableAdapter.FillcomClassificadosAtivo(dCla.CondominosClassificados, Cred.CON);

            //Aplica filtros - todos ou de acordo com os campos de busca
            DataView dClaBusca = dCla.CondominosClassificados.DefaultView;
            dClaBusca.RowFilter = "(Nome LIKE '%" + txtNome.Text + "%')";
            if (cmbRamoAtividade.SelectedItem != null)
                if ((int)cmbRamoAtividade.SelectedItem.Value > 0)
                    dClaBusca.RowFilter += " AND (RAV = " + cmbRamoAtividade.SelectedItem.Value + ")";

            //Ordena e exibe
            dClaBusca.Sort = "Nome";
            grdClassificados.DataSource = dClaBusca;
        }

        protected void grdClassificados_CustomFieldValueCells(object sender, DevExpress.Web.ASPxPivotGrid.PivotCustomFieldValueCellsEventArgs e)
        {
            //Esconde coluna 'Grand Total'
            for (int i = e.GetCellCount(true) - 1; i >= 0; i--)
            {
                FieldValueCell cell = e.GetCell(true, i);
                if (cell.DisplayText == "Grand Total")
                    e.Remove(cell);
            }

            //Esconde coluna 'Grand Total'
            for (int i = e.GetCellCount(false) - 1; i >= 0; i--)
            {
                FieldValueCell cell = e.GetCell(false, i);
                if (cell.DisplayText == "Grand Total")
                    e.Remove(cell);
            }
        }
    }
}