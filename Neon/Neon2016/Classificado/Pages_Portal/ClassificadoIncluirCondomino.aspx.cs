﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.ASPxPivotGrid;
using PortalUtil;
using Classificado.Datasets;

namespace Classificado.Pages_Portal
{
    public partial class ClassificadoIncluirCondomino : System.Web.UI.Page
    {
        private dClassificado dCla = new dClassificado();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dCla = new dClassificado(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega tela com todos os condominos com ramo de atividade
            dCla.CondominosClassificadosTableAdapter.FillcomRamoAtividade(dCla.CondominosClassificados, Cred.CON);
            grdClassificados.DataSource = dCla.CondominosClassificados;
            grdClassificados.DataBind();
        }

        protected void cmdClassificados_DataBinding(object sender, EventArgs e)
        {
            //Nomeia botao de classificados para exclusao caso ja esteja ativo
            Button btn = (Button)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)btn.NamingContainer;
            if ((int)container.Grid.GetRowValues(container.VisibleIndex, new string[] { "ClassificadosAtivo" }) > 0)
            {
                btn.Text = "Excluir";
                btn.BackColor = System.Drawing.ColorTranslator.FromHtml("#76a449");
            }
        }

        protected void cmdClassificados_Click(object sender, EventArgs e)
        {
            //Ativa ou desativa classificados para o cliente
            Button cmdClassificados = (Button)sender;
            int PES = int.Parse(cmdClassificados.CommandName);
            int APT = int.Parse(cmdClassificados.CommandArgument);

            if (cmdClassificados.Text == "Incluir")
            {
                //Habilita classificados, incluindo o registro na tabela 
                dCla.CondominosClassificadosTableAdapter.ClassificadosIncluir(PES, APT);
                cmdClassificados.Text = "Excluir";
                cmdClassificados.BackColor = System.Drawing.ColorTranslator.FromHtml("#76a449");
            }
            else
            {
                //Desabilita classificados, excluindo o registro selecionado na tabela 
                dCla.CondominosClassificadosTableAdapter.ClassificadosExcluir(PES, APT);
                cmdClassificados.Text = "Incluir";
                cmdClassificados.BackColor = System.Drawing.ColorTranslator.FromHtml("#e46a75");
            }
        }
    }
}