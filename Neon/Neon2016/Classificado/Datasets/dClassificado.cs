﻿namespace Classificado.Datasets
{
    partial class dClassificado
    {
        public int EMP;

        public dClassificado(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dClassificadoTableAdapters.CondominosClassificadosTableAdapter taCondominosClassificadosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CondominosClassificados
        /// </summary>
        public dClassificadoTableAdapters.CondominosClassificadosTableAdapter CondominosClassificadosTableAdapter
        {
            get
            {
                if (taCondominosClassificadosTableAdapter == null)
                {
                    taCondominosClassificadosTableAdapter = new dClassificadoTableAdapters.CondominosClassificadosTableAdapter();
                    if (EMP == 3)
                        taCondominosClassificadosTableAdapter.Connection.ConnectionString = taCondominosClassificadosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCondominosClassificadosTableAdapter;
            }
        }

        private dClassificadoTableAdapters.RamoAtiVidadeTableAdapter taRamoAtiVidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RamoAtiVidade
        /// </summary>
        public dClassificadoTableAdapters.RamoAtiVidadeTableAdapter RamoAtiVidadeTableAdapter
        {
            get
            {
                if (taRamoAtiVidadeTableAdapter == null)
                {
                    taRamoAtiVidadeTableAdapter = new dClassificadoTableAdapters.RamoAtiVidadeTableAdapter();
                    if (EMP == 3)
                        taRamoAtiVidadeTableAdapter.Connection.ConnectionString = taRamoAtiVidadeTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taRamoAtiVidadeTableAdapter;
            }
        }
    }
}
