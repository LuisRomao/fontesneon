﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalUtil;

namespace Classificado
{
    public partial class Projeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void cmdClassificadoInternoSBC_Click(object sender, EventArgs e)
        {
            if (Login("rodino", "062129")) Response.Redirect("~/Pages_Portal/ClassificadoInterno.aspx"); //Proprietario - 000999 (SB)
        }

        protected void cmdClassificadoInternoSA_Click(object sender, EventArgs e)
        {
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/ClassificadoInterno.aspx"); //Sindico - ALGRAN (SA)   
        }

        protected void cmdClassificadoBuscarCondominoSBC_Click(object sender, EventArgs e)
        {
            if (Login("rodino", "062129")) Response.Redirect("~/Pages_Portal/ClassificadoBuscarCondomino.aspx"); //Proprietario - 000999 (SB)
        }

        protected void cmdClassificadoBuscarCondominoSA_Click(object sender, EventArgs e)
        {
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/ClassificadoBuscarCondomino.aspx"); //Sindico - ALGRAN (SA)   
        }

        protected void cmdClassificadoIncluirCondominoSBC_Click(object sender, EventArgs e)
        {
            if (Login("rodino", "062129")) Response.Redirect("~/Pages_Portal/ClassificadoIncluirCondomino.aspx"); //Proprietario - 000999 (SB)
        }

        protected void cmdClassificadoIncluirCondominoSA_Click(object sender, EventArgs e)
        {
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/ClassificadoIncluirCondomino.aspx"); //Sindico - ALGRAN (SA)   
        }

        protected bool Login(string strUsuario, string strSenha)
        {
            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(strUsuario, strSenha);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = strUsuario;
                Session["Pass"] = strSenha;

                //Autenticou
                return true;
            }
            
            //Não autenticou
            return false;
        }
    }
}