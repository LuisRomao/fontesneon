﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalUtil;

namespace Financeiro
{
    public partial class Projeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void cmdBalancete_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/Balancete.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/Balancete.aspx"); //Sindico - ALGRAN (SA)
            //if (Login("1edgar12", "2761")) Response.Redirect("~/Pages_Portal/Balancete.aspx"); //Sindico - SHOPP. (SB)
            //if (Login("flavia", "anderson")) Response.Redirect("~/Pages_Portal/Balancete.aspx"); //Sindico - AUDREY (SB)
        }

        protected void cmdFrancesinha_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/Francesinha.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/Francesinha.aspx"); //Sindico - ALGRAN (SA)
            //if (Login("1edgar12", "2761")) Response.Redirect("~/Pages_Portal/Francesinha.aspx"); //Sindico - SHOPP. (SB)
            //if (Login("flavia", "anderson")) Response.Redirect("~/Pages_Portal/Francesinha.aspx"); //Sindico - AUDREY (SB)
        }

        protected void cmdExtratoBancario_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/ExtratoBancario.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/ExtratoBancario.aspx"); //Sindico - ALGRAN (SA) - Sem conta plus
            //if (Login("1edgar12", "2761")) Response.Redirect("~/Pages_Portal/ExtratoBancario.aspx"); //Sindico - SHOPP. (SB)
            //if (Login("flavia", "anderson")) Response.Redirect("~/Pages_Portal/ExtratoBancario.aspx"); //Sindico - AUDREY (SB) - Com conta plus
        }

        protected bool Login(string strUsuario, string strSenha)
        {
            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(strUsuario, strSenha);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = strUsuario;
                Session["Pass"] = strSenha;

                //Autenticou
                return true;
            }
            
            //Não autenticou
            return false;
        }
    }
}