﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using DevExpress.Web;
using PortalUtil;
using FinanceiroUtil.Datasets;

namespace Financeiro.Pages_Portal
{
    public partial class Balancete : System.Web.UI.Page
    {
        private dFinanceiro dFin = new dFinanceiro();

        enum Destino { arquivo, link };
        private int CompMes;
        private int CompAno;

        private string NomeArq(int modelo, int competencia)
        {
            return string.Format(@"{0}{1}{2}{3}{4}.asp",
                                 Cred.EMP,
                                 Cred.rowUsuario.CONCodigo.Replace(".", "_").Replace("/", "_").Replace("\\", "_").Replace(" ", "_"),
                                 modelo,
                                 competencia % 100,
                                 competencia / 100);
        }

        private string NomeArqH12()
        {
            return string.Format(@"{0}_{1}_h12.pdf",
                                 Cred.EMP,
                                 Cred.CON);
        }

        private string NomeArqBAL()
        {
            string NomeArquivo = string.Format(@"{2:0000}{3:00}\{0}_{1}_B{2:0000}{3:00}.pdf",
                                               Cred.EMP,
                                               Cred.CON,
                                               CompAno,
                                               CompMes);
            if (!System.IO.File.Exists(ConfigurationManager.AppSettings["PublicacoesArquivos"] + NomeArquivo))
                NomeArquivo = string.Format(@"{0}_{1}_B{2:0000}{3:00}.pdf",
                                            Cred.EMP,
                                            Cred.CON,
                                            CompAno,
                                            CompMes);
            return NomeArquivo;
        }

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia datasets
            dFin = new dFinanceiro(Cred.EMP);

            if (!IsPostBack)
            {
                //Salva dataset na sessao (para usar no postback dos botoes)
                Session["dFin"] = dFin;

                //Carrega tela
                Carregar();
            }
            else
            {
                //Pega dataset na sessao (para usar no postback dos botoes)
                dFin = (dFinanceiro)Session["dFin"];
            }
        }

        private void Carregar()
        {
            //Carrega os balancetes
            if (dFin.CarregaTotais(Cred.CON) > 0)
            {
                //Preenche o combo
                cmbMes.DataSource = dFin.BALancetes;
                cmbMes.DataBind();

                //Seta no primeiro e exibe a tela
                cmbMes.SelectedIndex = 0;
                cmbMes_SelectedIndexChanged(null, null);
            }
        }

        protected void cmbMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Exibe a tela com dados do mes selecionado
            dFinanceiro.BALancetesRow rowBAL = dFin.BALancetes.FindByBALCompet(Convert.ToInt32(cmbMes.SelectedItem.Value));

            //Seta competencia
            CompAno = rowBAL.BALCompet / 100;
            CompMes = rowBAL.BALCompet % 100;

            //Seta links dos documentos
            panModelo1.Visible = lnkModelo1.Visible = System.IO.File.Exists(ConfigurationManager.AppSettings["PublicacoesArquivosBalancetes"] + NomeArq(1, rowBAL.BALCompet));
            if (lnkModelo1.Visible) lnkModelo1.OnClientClick = String.Format("javascript: window.open('Arquivo.aspx?Arquivo={0}&Tipo={1}', '{2}'); return false;", NomeArq(1, rowBAL.BALCompet), (short)Tipos.TipoDocumento.Balancete, !Request.Browser.IsMobileDevice ? "_self" : "_blank");
            panModelo2.Visible = lnkModelo2.Visible = System.IO.File.Exists(ConfigurationManager.AppSettings["PublicacoesArquivosBalancetes"] + NomeArq(2, rowBAL.BALCompet));
            if (lnkModelo2.Visible) lnkModelo2.OnClientClick = String.Format("javascript: window.open('Arquivo.aspx?Arquivo={0}&Tipo={1}', '{2}'); return false;", NomeArq(2, rowBAL.BALCompet), (short)Tipos.TipoDocumento.Balancete, !Request.Browser.IsMobileDevice ? "_self" : "_blank");
            panModelo3.Visible = lnkModelo3.Visible = System.IO.File.Exists(ConfigurationManager.AppSettings["PublicacoesArquivosBalancetes"] + NomeArq(3, rowBAL.BALCompet));
            if (lnkModelo3.Visible) lnkModelo3.OnClientClick = String.Format("javascript: window.open('Arquivo.aspx?Arquivo={0}&Tipo={1}', '{2}'); return false;", NomeArq(3, rowBAL.BALCompet), (short)Tipos.TipoDocumento.Balancete, !Request.Browser.IsMobileDevice ? "_self" : "_blank");
            panModelo4.Visible = lnkModelo4.Visible = System.IO.File.Exists(ConfigurationManager.AppSettings["PublicacoesArquivosBalancetes"] + NomeArq(4, rowBAL.BALCompet));
            if (lnkModelo4.Visible) lnkModelo4.OnClientClick = String.Format("javascript: window.open('Arquivo.aspx?Arquivo={0}&Tipo={1}', '{2}'); return false;", NomeArq(4, rowBAL.BALCompet), (short)Tipos.TipoDocumento.Balancete, !Request.Browser.IsMobileDevice ? "_self" : "_blank");
            panAcumulado.Visible = lnkAcumulado.Visible = System.IO.File.Exists(ConfigurationManager.AppSettings["PublicacoesArquivos"] + NomeArqH12());
            if (lnkAcumulado.Visible) lnkAcumulado.OnClientClick = String.Format("javascript: window.open('Arquivo.aspx?Arquivo={0}', '{1}'); return false;", NomeArqH12().Replace(@"\", @"\\"), !Request.Browser.IsMobileDevice ? "_self" : "_blank");
            panBalancete.Visible = lnkBalancete.Visible = System.IO.File.Exists(ConfigurationManager.AppSettings["PublicacoesArquivos"] + NomeArqBAL());
            if (lnkBalancete.Visible) lnkBalancete.OnClientClick = String.Format("javascript: window.open('Arquivo.aspx?Arquivo={0}', '{1}'); return false;", NomeArqBAL().Replace(@"\", @"\\"), !Request.Browser.IsMobileDevice ? "_self" : "_blank");

            //Seta link da vizualizacao grafica
            dFin.CONDOMINIOSTableAdapter.Fill(dFin.CONDOMINIOS, Cred.CON);
            panVisualizacaoGrafica.Visible = lnkVisualizacaoGrafica.Visible = !dFin.CONDOMINIOS[0].IsCONLinkBalanceteGraficoInternetNull();
            if (lnkVisualizacaoGrafica.Visible) lnkVisualizacaoGrafica.OnClientClick = String.Format("javascript: window.open('{0}', '{1}'); return false;", dFin.CONDOMINIOS[0].CONLinkBalanceteGraficoInternet, !Request.Browser.IsMobileDevice ? "_self" : "_blank");

            //Posiciona no combo novamente
            this.smnBalancete.SetFocus(cmbMes);
        }
    }
}