﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using FinanceiroUtil.Datasets;

namespace Financeiro.Pages_Portal
{
    public partial class ExtratoBancario : System.Web.UI.Page
    {
        private dFinanceiro dFin = new dFinanceiro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        private System.Collections.Generic.SortedList<int, int> ListaPlus
        {
            get
            {
                System.Collections.Generic.SortedList<int, int> Lista = (System.Collections.Generic.SortedList<int, int>)Session["ListaPlus"];
                if (Lista == null)
                {
                    Lista = new System.Collections.Generic.SortedList<int, int>();
                    Session["ListaPlus"] = Lista;
                }
                return Lista;
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dFin = new dFinanceiro(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega as contas
            int intTotalContas = dFin.ContaCorrenTeTableAdapter.FillByAtivas(dFin.ContaCorrenTe, Cred.CON);
            rblConta.DataSource = dFin.ContaCorrenTe;
            rblConta.DataBind();

            //Verifica se nao encontrou conta
            if (intTotalContas == 0)
            {
                floSemConta.Visible = true;
                floConta.Visible = false;
                return;
            }

            //Seta contas do tipo Plus (3)
            ListaPlus.Clear();
            foreach (dFinanceiro.ContaCorrenTeRow CCTrow in dFin.ContaCorrenTe)
                if (!CCTrow.IsCCTTipoNull() && (CCTrow.CCTTipo == 3))
                    ListaPlus.Add(CCTrow.CCT, CCTrow.CCT_BCO);

            //Seta selecao de conta
            if (intTotalContas == 1)
            {
                rblConta.SelectedIndex = 0;
                PopulaDatas();
            }
            
        }

        protected void rblConta_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Esconde o grid
            grdExtratoBancario.Visible = false;

            //Popula as datas
            PopulaDatas();
        }

        private void PopulaDatas()
        {
            //Carrega as datas
            dFin.SaldoContaCorrenteTableAdapter.FillByDatas(dFin.SaldoContaCorrente, Convert.ToInt32(rblConta.SelectedItem.Value), DateTime.Now.AddDays(-90)).ToString("yyyy-MM-dd");
            cmbDataInicio.Items.Clear();
            cmbDataInicio.DataSource = dFin.SaldoContaCorrente;
            cmbDataInicio.DataBind();
            cmbDataFim.Items.Clear();
            cmbDataFim.DataSource = dFin.SaldoContaCorrente;
            cmbDataFim.DataBind();

            //Seta datas inciais
            if (cmbDataFim.Items.Count > 0)
            {
                cmbDataFim.SelectedIndex = cmbDataFim.Items.Count - 1;
                cmbDataInicio.SelectedIndex = (cmbDataInicio.Items.Count > 5) ? cmbDataInicio.Items.Count - 5 : 0;
            }

            //Atualiza campos
            cmbDataInicio.Validate();
            cmbDataFim.Validate();
        }

        protected void cmbData_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Esconde o grid
            grdExtratoBancario.Visible = false;
        }

        protected void chkTipoExtrato_CheckedChanged(object sender, EventArgs e)
        {
            //Verifica se executa o grid incluindo a informacao
            if (grdExtratoBancario.Visible)
                cmdExibir_Click(null, null);
        }

        protected void cmdExibir_Click(object sender, EventArgs e)
        {
            //Exibe extrato de acordo com campos
            ExibeExtrato(Convert.ToInt32(rblConta.SelectedItem.Value), Convert.ToInt32(cmbDataInicio.SelectedItem.Value), Convert.ToInt32(cmbDataFim.SelectedItem.Value));
        }

        private void ExibeExtrato(int intCCT, int intSCCInicio, int intSCCFim)
        {
            //Carrega dados do extrato de acordo 
            (grdExtratoBancario.Columns["colDetalhes"] as GridViewDataTextColumn).Visible = false;
            dFin.ContaCorrenteDetalheTableAdapter.FillByExtrato(dFin.ContaCorrenteDetalhe, intCCT, intSCCInicio, intSCCFim, ListaPlus.ContainsKey(intCCT) ? "1" : "0");

            //Carrega dados tratados do extrato no grid
            grdExtratoBancario.DataSource = dFin.ContaCorrenteDetalhe;
            grdExtratoBancario.DataBind();

            //Expande todas as linhas
            grdExtratoBancario.ExpandAll();

            //Exibe detalhes se necessario
            if (chkCheque.Checked || chkBoleto.Checked)
                (grdExtratoBancario.Columns["colDetalhes"] as GridViewDataTextColumn).Visible = true;

            //Exibe o grid
            grdExtratoBancario.Visible = true;
        }

        protected void grdExtratoBancario_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            //Pega a columa de grupo para desabilitar ordenacao ao clicar sobre essa coluna
            ASPxGridView grid = sender as ASPxGridView;
            e.Properties["cpGroupedColumns"] = grid.GetGroupedColumns().OfType<GridViewDataColumn>().Select(c => c.FieldName).ToList();
        }

        protected void grdExtratoBancario_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            //Seta saldo final (texto e cor)
            if (e.RowType == GridViewRowType.GroupFooter)
            {
                ASPxGridView grid = (ASPxGridView)sender;
                int index = grid.Columns["CCDDescricao"].VisibleIndex;
                e.Row.Cells[index].Text = "SALDO";
                e.Row.Cells[index].HorizontalAlign = HorizontalAlign.Right;
                index = grid.Columns["CCDValor"].VisibleIndex;
                e.Row.Cells[index].ForeColor = System.Drawing.ColorTranslator.FromHtml("#001626");
            }
        }

        protected void grdExtratoBancario_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Caption.Contains("Detalhes"))
            {
                //Carrega os cheques se necessario e exibe apenas se encontrado
                ASPxGridView gridCHE = grdExtratoBancario.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "grdCheques") as ASPxGridView;
                if (chkCheque.Checked)
                {
                    dFin.ExtratoChequesTableAdapter.Fill(dFin.ExtratoCheques, Convert.ToInt32(e.CellValue), ListaPlus.ContainsKey(Convert.ToInt32(rblConta.SelectedItem.Value)) ? "1" : "0");
                    if (dFin.ExtratoCheques.Count > 0)
                    {
                        gridCHE.DataSource = dFin.ExtratoCheques;
                        gridCHE.DataBind();
                        gridCHE.Visible = true;
                    }
                    gridCHE.Visible = dFin.ExtratoCheques.Count > 0 ? true : false;

                }
                else
                    gridCHE.Visible = false;

                //Carrega os boletos se necessario e exibe apenas se encontrado
                ASPxGridView gridBOL = grdExtratoBancario.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "grdBoletos") as ASPxGridView;
                if (chkBoleto.Checked)
                {
                    dFin.ExtratoBoletosTableAdapter.Fill(dFin.ExtratoBoletos, Convert.ToInt32(e.CellValue));
                    if (dFin.ExtratoBoletos.Count > 0)
                    {
                        gridBOL.DataSource = dFin.ExtratoBoletos;
                        gridBOL.DataBind();
                        gridBOL.Visible = true;
                    }
                    gridBOL.Visible = dFin.ExtratoBoletos.Count > 0 ? true : false;
                }
                else
                    gridBOL.Visible = false;
            }
        }

        protected void cmdFrancesinha_Click(object sender, EventArgs e)
        {
            //Abre boletos do condomino selecionado
            Button cmdFrancesinha = (Button)sender;
            int SCC = int.Parse(cmdFrancesinha.CommandArgument);
            Response.Redirect("~/Pages_Portal/Francesinha.aspx?SCC=" + SCC);
        }
    }
}