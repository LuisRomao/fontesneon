﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using FinanceiroUtil.Datasets;

namespace Financeiro.Pages_Portal
{
    public partial class Francesinha : System.Web.UI.Page
    {
        private dFinanceiro dFin = new dFinanceiro();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dFin = new dFinanceiro(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega as contas
            int intTotalContas = dFin.ContaCorrenTeTableAdapter.FillByFrancesinha(dFin.ContaCorrenTe, Cred.CON, DateTime.Now.AddDays(-90));
            rblConta.DataSource = dFin.ContaCorrenTe;
            rblConta.DataBind();

            //Verifica se nao encontrou conta
            if (intTotalContas == 0)
            { 
                floSemConta.Visible = true;
                floConta.Visible = false;
                return;
            }

            //Verifica se recebeu saldo de conta corrente como parametro
            int intSelCCT = -1;
            int SCCrow_CCT = -1;
            string SCCrow_SCCData = "";
            if ((Request.Params["SCC"] != null))
            {
                //Pega dados do saldo
                dFin.SaldoContaCorrenteTableAdapter.FillBySCC(dFin.SaldoContaCorrente, int.Parse(Request.Params["SCC"]));
                SCCrow_CCT = ((dFinanceiro.SaldoContaCorrenteRow)dFin.SaldoContaCorrente.Rows[0]).SCC_CCT;
                SCCrow_SCCData = ((dFinanceiro.SaldoContaCorrenteRow)dFin.SaldoContaCorrente.Rows[0]).SCCData.ToString("dd/MM/yyyy");

                //Procura pela conta corrente do saldo selecionado
                int i = 0;
                foreach (dFinanceiro.ContaCorrenTeRow CCTrow in dFin.ContaCorrenTe)
                { 
                    if (SCCrow_CCT == CCTrow.CCT)
                        intSelCCT = i;
                    i++;
                }
            }

            //Seta selecao de conta
            if (intTotalContas == 1)
            {
                rblConta.SelectedIndex = 0;
                PopulaDatas();
            }
            else if (intSelCCT != -1)
            {
                rblConta.SelectedIndex = intSelCCT;
                PopulaDatas();
            }

            //Verifica se saldo recebido
            if (SCCrow_CCT != -1)
            {
                //Seta datas do saldo recebido
                for (int k = 0; k < cmbDataInicio.Items.Count; k++)
                    if (cmbDataInicio.Items[k].Value.ToString() == SCCrow_SCCData)
                    {
                        cmbDataInicio.SelectedIndex = k;
                        cmbDataFim.SelectedIndex = k;
                        break;
                    }

                //Exibe extrato do saldo recebido
                ExibeExtrato(Convert.ToInt32(SCCrow_CCT), Convert.ToDateTime(SCCrow_SCCData), Convert.ToDateTime(SCCrow_SCCData));
            }
        }

        protected void rblConta_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Esconde o grid
            grdFrancesinha.Visible = false;

            //Popula as datas
            PopulaDatas();
        }

        private void PopulaDatas()
        {
            //Carrega as datas
            dFin.SaldoContaCorrenteTableAdapter.FillByDatas(dFin.SaldoContaCorrente, Convert.ToInt32(rblConta.SelectedItem.Value), DateTime.Now.AddDays(-60)).ToString("yyyy-MM-dd");
            cmbDataInicio.Items.Clear();
            cmbDataInicio.DataSource = dFin.SaldoContaCorrente;
            cmbDataInicio.DataBind();
            cmbDataFim.Items.Clear();
            cmbDataFim.DataSource = dFin.SaldoContaCorrente;
            cmbDataFim.DataBind();

            //Seta datas inciais
            if (cmbDataFim.Items.Count > 0)
            {
                cmbDataFim.SelectedIndex = cmbDataFim.Items.Count - 1;
                cmbDataInicio.SelectedIndex = (cmbDataInicio.Items.Count > 5) ? cmbDataInicio.Items.Count - 5 : 0;
            }

            //Atualiza campos
            cmbDataInicio.Validate();
            cmbDataFim.Validate();
        }

        protected void cmbData_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Esconde o grid
            grdFrancesinha.Visible = false;
        }

        protected void cmdExibir_Click(object sender, EventArgs e)
        {
            //Exibe extrato de acordo com campos
            ExibeExtrato(Convert.ToInt32(rblConta.SelectedItem.Value), Convert.ToDateTime(cmbDataInicio.SelectedItem.Value), Convert.ToDateTime(cmbDataFim.SelectedItem.Value));
        }

        private void ExibeExtrato(int intCCT, DateTime datDataInicio, DateTime datDataFim)
        {
            //Carrega a francesinha
            dFin.FrancesinhaTableAdapter.Fill(dFin.Francesinha, intCCT, datDataInicio, datDataFim);
            grdFrancesinha.DataSource = dFin.Francesinha;
            grdFrancesinha.DataBind();

            //Expande todas as linhas
            grdFrancesinha.ExpandAll();

            //Exibe o grid
            grdFrancesinha.Visible = true;
        }

        protected void grdFrancesinha_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            //Pega a columa de grupo para desabilitar ordenacao ao clicar sobre essa coluna
            ASPxGridView grid = sender as ASPxGridView;
            e.Properties["cpGroupedColumns"] = grid.GetGroupedColumns().OfType<GridViewDataColumn>().Select(c => c.FieldName).ToList();
        }
    }
}