﻿namespace BoletoUtil.Datasets
{
    partial class dBoleto
    {
        public int EMP;

        public dBoleto(int EMP) : this()
        {
            this.EMP = EMP;
        }

        #region TableAdapters
        private dBoletoTableAdapters.INPCTableAdapter iNPCTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: INPC
        /// </summary>
        public dBoletoTableAdapters.INPCTableAdapter INPCTableAdapter
        {
            get
            {
                if (iNPCTableAdapter == null)
                {
                    iNPCTableAdapter = new dBoletoTableAdapters.INPCTableAdapter();
                    if (EMP == 3)
                        iNPCTableAdapter.Connection.ConnectionString = iNPCTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return iNPCTableAdapter;
            }
        }

        private dBoletoTableAdapters.AParTamentoTableAdapter aParTamentoTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: AParTamento
        /// </summary>
        public dBoletoTableAdapters.AParTamentoTableAdapter AParTamentoTableAdapter
        {
            get
            {
                if (aParTamentoTableAdapter == null)
                {
                    aParTamentoTableAdapter = new dBoletoTableAdapters.AParTamentoTableAdapter();
                    if (EMP == 3)
                        aParTamentoTableAdapter.Connection.ConnectionString = aParTamentoTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return aParTamentoTableAdapter;
            }
        }

        private dBoletoTableAdapters.ACOrdosTableAdapter aCOrdosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: ACOrdos
        /// </summary>
        public dBoletoTableAdapters.ACOrdosTableAdapter ACOrdosTableAdapter
        {
            get
            {
                if (aCOrdosTableAdapter == null)
                {
                    aCOrdosTableAdapter = new dBoletoTableAdapters.ACOrdosTableAdapter();
                    if (EMP == 3)
                        aCOrdosTableAdapter.Connection.ConnectionString = aCOrdosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return aCOrdosTableAdapter;
            }
        }

        private dBoletoTableAdapters.OriginalTableAdapter originalTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Original
        /// </summary>
        public dBoletoTableAdapters.OriginalTableAdapter OriginalTableAdapter
        {
            get
            {
                if (originalTableAdapter == null)
                {
                    originalTableAdapter = new dBoletoTableAdapters.OriginalTableAdapter();
                    if (EMP == 3)
                        originalTableAdapter.Connection.ConnectionString = originalTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return originalTableAdapter;
            }
        }

        private dBoletoTableAdapters.NovosTableAdapter novosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Novos
        /// </summary>
        public dBoletoTableAdapters.NovosTableAdapter NovosTableAdapter
        {
            get
            {
                if (novosTableAdapter == null)
                {
                    novosTableAdapter = new dBoletoTableAdapters.NovosTableAdapter();
                    if (EMP == 3)
                        novosTableAdapter.Connection.ConnectionString = novosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return novosTableAdapter;
            }
        }

        private dBoletoTableAdapters.HonorariosTableAdapter taHonorariosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Honorarios
        /// </summary>
        public dBoletoTableAdapters.HonorariosTableAdapter HonorariosTableAdapter
        {
            get
            {
                if (taHonorariosTableAdapter == null)
                {
                    taHonorariosTableAdapter = new dBoletoTableAdapters.HonorariosTableAdapter();
                    if (EMP == 3)
                        taHonorariosTableAdapter.Connection.ConnectionString = taHonorariosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taHonorariosTableAdapter;
            }
        }

        private dBoletoTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dBoletoTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dBoletoTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP == 3)
                        cONDOMINIOSTableAdapter.Connection.ConnectionString = cONDOMINIOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dBoletoTableAdapters.BoletosTableAdapter boletosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Boletos
        /// </summary>
        public dBoletoTableAdapters.BoletosTableAdapter BoletosTableAdapter
        {
            get
            {
                if (boletosTableAdapter == null)
                {
                    boletosTableAdapter = new dBoletoTableAdapters.BoletosTableAdapter();
                    if (EMP == 3)
                        boletosTableAdapter.Connection.ConnectionString = boletosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return boletosTableAdapter;
            }
        }

        private dBoletoTableAdapters.DadosBancariosTableAdapter dadosBancariosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: DadosBancarios
        /// </summary>
        public dBoletoTableAdapters.DadosBancariosTableAdapter DadosBancariosTableAdapter
        {
            get
            {
                if (dadosBancariosTableAdapter == null)
                {
                    dadosBancariosTableAdapter = new dBoletoTableAdapters.DadosBancariosTableAdapter();
                    if (EMP == 3)
                        dadosBancariosTableAdapter.Connection.ConnectionString = dadosBancariosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return dadosBancariosTableAdapter;
            }
        }

        private dBoletoTableAdapters.QueriesTableAdapter queriesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Queries
        /// </summary>
        public dBoletoTableAdapters.QueriesTableAdapter QueriesTableAdapter
        {
            get
            {
                if (queriesTableAdapter == null)
                {
                    queriesTableAdapter = new dBoletoTableAdapters.QueriesTableAdapter();
                };
                return queriesTableAdapter;
            }
        }

        #endregion

        public void CorrecaoProvisorios(string CODCON, string BLOCodigo, string APTNumero, short EMP, int APT, bool Proprietario)
        {
            dAcordoRet dAcordoRet1 = new dAcordoRet();
            dAcordoRet1.RetAcordoTableAdapter.FillByAPT(dAcordoRet1.RetAcordo, CODCON, APTNumero, BLOCodigo, EMP);
            dAcordoRet1.RetNovosBTableAdapter.FillByAPT(dAcordoRet1.RetNovosB, CODCON, APTNumero, BLOCodigo, EMP);
            dAcordoRet1.RetOriginaisTableAdapter.FillByAPT(dAcordoRet1.RetOriginais, CODCON, APTNumero, BLOCodigo, EMP);
            foreach (dAcordoRet.RetAcordoRow rowA in dAcordoRet1.RetAcordo)
            {
                dBoleto.ACOrdosRow rowNova = ACOrdos.NewACOrdosRow();
                rowNova.ACO_APT = APT;
                rowNova.ACOJuridico = rowA.Juridico;
                rowNova.ACOProprietario = rowA.DESTINATARIO == "P";
                ACOrdos.AddACOrdosRow(rowNova);
                int prest = 1;
                foreach (dAcordoRet.RetNovosBRow rowN in rowA.GetRetNovosBRows())
                {
                    IncluirBoletoAProvisorio(rowN, Proprietario);
                    dBoleto.NovosRow rowNovo = Novos.NewNovosRow();
                    rowNovo.ACO = rowNova.ACO;
                    rowNovo.ACO_APT = APT;
                    rowNovo.dtvencto = rowN.vencto;
                    rowNovo.prest = prest++;
                    rowNovo.valprev = rowN.valor;
                    rowNovo.HValor = rowN.Honorarios;
                    Novos.AddNovosRow(rowNovo);
                }
                foreach (dAcordoRet.RetOriginaisRow rowO in rowA.GetRetOriginaisRows())
                {
                    dBoleto.BoletosRow rowBOL = Boletos.FindByNumero(rowO.numero);
                    if (rowBOL != null)
                    {
                        dBoleto.OriginalRow rowOrig = Original.NewOriginalRow();
                        rowOrig.ACO = rowNova.ACO;
                        rowOrig.ACO_APT = APT;
                        rowOrig.ACOStatus = 1;
                        rowOrig.competencia = string.Format("{0:00}/{1:0000}", rowBOL.BOLCompetenciaMes, rowBOL.BOLCompetenciaAno);
                        rowOrig.valor = rowBOL.valor;
                        rowOrig.valororiginal = rowBOL.valor;
                        rowOrig.vencto = rowBOL.dtvencto;
                        rowOrig.BOLCompetenciaMes = rowBOL.BOLCompetenciaMes;
                        rowOrig.BOLCompetenciaAno = rowBOL.BOLCompetenciaAno;
                        Original.AddOriginalRow(rowOrig);
                        rowBOL.Delete();
                    }
                }
            }
            Boletos.AcceptChanges();
        }

        private void IncluirBoletoAProvisorio(dAcordoRet.RetNovosBRow rowNovos, bool Proprietario)
        {
            dBoleto.BoletosRow rowBolNovo = Boletos.NewBoletosRow();
            rowBolNovo.competencia = "0101";
            rowBolNovo.dtvencto = rowNovos.vencto;
            rowBolNovo.Multa = 2;
            rowBolNovo.Numero = rowNovos.nn;
            rowBolNovo.Resp = Proprietario ? "P" : "I";
            rowBolNovo.Tipo = "M";
            rowBolNovo.tipoboleto = "A";
            rowBolNovo.valor = rowNovos.valor;
            rowBolNovo.BOLCompetenciaMes = (short)rowNovos.vencto.Month;
            rowBolNovo.BOLCompetenciaAno = (short)rowNovos.vencto.Year;
            rowBolNovo.competencia = string.Format("{0:00}/{1:0000}", rowBolNovo.BOLCompetenciaMes, rowBolNovo.BOLCompetenciaAno);
            rowBolNovo.Resp = (rowBolNovo.IsBOLProprietarioNull() || rowBolNovo.BOLProprietario) ? "P" : "I";
            rowBolNovo.pago = !rowBolNovo.IsBOLPagamentoNull();
            rowBolNovo.BLOCodigo = "";
            Boletos.AddBoletosRow(rowBolNovo);
            rowBolNovo.AcceptChanges();
        }
    }
}
