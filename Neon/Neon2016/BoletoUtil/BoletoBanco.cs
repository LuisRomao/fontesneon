﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoletoUtil
{
    public class TVia
    {
        public string TextoInterno;
        public string CodigoBarras;
        public string NomeVia;
        public TVia(string _TextoInterno, string _CodigoBarras, string _NomeVia)
        {
            TextoInterno = _TextoInterno;
            CodigoBarras = _CodigoBarras;
            NomeVia = _NomeVia;
        }
    }

    public enum TipoMulta { diaria, unica }

    public class TDadosBanco
    {
        public TipoMulta TMulta
        { get { return rowBanco.CONTipoMulta.ToUpper() == "M" ? TipoMulta.unica : TipoMulta.diaria; } }
        public decimal Multa
        { get { return rowBanco.CONValorMulta; } }
        public string Numbanco
        { get { return rowBanco.CON_BCO.ToString("000"); } }
        public int BCO
        { get { return rowBanco.CON_BCO; } }
        public string cc
        { get { return rowBanco.CONConta.PadLeft(6, '0'); } }
        public string dgcc
        { get { return rowBanco.CONDigitoConta; } }
        public string agencia
        { get { return rowBanco.CONAgencia; } }
        public string dgagencia
        { get { return rowBanco.CONDigitoAgencia; } }
        public string CNR
        { get { return rowBanco.IsCONCodigoCNRNull() ? "" : rowBanco.CONCodigoCNR.PadLeft(7, '0'); ; } }
        public bool BoletoComRegistro
        { get { return rowBanco.CONBoletoComRegistro; } }
        public string Local
        { get { return local; } }
        public string EspecieDOC
        { get { return especieDOC; } }
        public string Carteira
        { get { return carteira; } }
        public string FonteNN
        { get { return fonteNN; } }

        public int CarteiraBanco
        {
            get
            {
                if (CNR == "0000016")
                    return 16;
                switch (BCO)
                {
                    case 341:
                        return (ComRegistro ? 109 : 175);
                    default:
                        return (ComRegistro ? 9 : 6);
                }
            }
        }

        private bool ComRegistro
        {
            get
            {
                switch (BCO)
                {
                    case 237:
                        if (!(CNR == "" || CNR == "0000000" || CNR == "0000016"))
                            return BoletoComRegistro;
                        else
                            return false;
                    default:
                        return BoletoComRegistro;
                }
            }
        }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        public string Nome;
        public string Numero;
        public string CodigoCedente;
        private string local;
        private string especieDOC;
        private string carteira;
        private string fonteNN;

        private Datasets.dBoleto.DadosBancariosRow rowBanco;

        public TDadosBanco(Datasets.dBoleto.DadosBancariosRow _rowBanco)
        {
            rowBanco = _rowBanco;
            switch (rowBanco.CON_BCO)
            {
                case 237:
                    Nome = "<b><font face=\"Arial\" size=\"2\">BRADESCO S/A</font><br></b>" +
                            "<font face=\"Arial, Helvetica, sans-serif\" size=\"1\">C.N.P.J 60.748.948</font>";
                    Numero = "237-2";
                    local = System.Web.HttpUtility.HtmlEncode("ATÉ O VENCIMENTO, EM	QUALQUER AG. DA REDE BANCÁRIA");
                    CodigoCedente = string.Format("{0}-{1}/{2}-{3}",
                        rowBanco.CONAgencia,
                        rowBanco.CONDigitoAgencia,
                        rowBanco.CONConta,
                        rowBanco.CONDigitoConta);
                    especieDOC = "";
                    carteira = CarteiraBanco.ToString(); //(CNR == "" || CNR == "0000000") ? "6" : (CNR == "0000016" ? "16" : "9");
                    fonteNN = "N1";
                    break;
                case 341:
                    Nome = "<b><font face=\"Arial\" size=\"2\">Banco Ita&uacute; S.A.</font></b>";
                    Numero = "341-7";
                    local = "Até o vencimento pague preferencialmente no Itaú, após o vencimento pague somente no Itaú";
                    CodigoCedente = string.Format("{0}-{1}/{2}-{3}",
                        rowBanco.CONAgencia,
                        rowBanco.CONDigitoAgencia,
                        rowBanco.CONConta,
                        rowBanco.CONDigitoConta);
                    especieDOC = "";
                    carteira = CarteiraBanco.ToString(); //"6";
                    fonteNN = "N1";
                    break;
                case 409:
                    Nome = "<b><font face=\"Arial\" size=\"2\">UNIBANCO</font></b>";
                    Numero = "409-0";
                    local = "Até o vencimento, pagável em qualquer banco. Após o vencimento, em qualquer agência do Unibanco mediante consulta no sistema VC";
                    CodigoCedente = string.Format("{0}-{1}/{2}-{3}",
                        rowBanco.CONAgencia,
                        rowBanco.CONDigitoAgencia,
                        rowBanco.CONConta,
                        rowBanco.CONDigitoConta);
                    especieDOC = "REC";
                    carteira = CarteiraBanco.ToString(); //"6";
                    fonteNN = "N1";
                    break;
                case 104://124
                    Nome = "<img src=\"../Content/Images/logo-caixa-bol.gif\" width=\"100\" height=\"27\" alt = \"CAIXA\">";
                    Numero = "104-0";
                    local = "PREFERENCIALMENTE NAS CASAS LOTÉRICAS E AGÊNCIAS DA CAIXA";
                    CodigoCedente = string.Format("{0}/{1}-{2}",
                        rowBanco.CONAgencia,
                        rowBanco.CONCodigoCNR,
                        BoletosCalc.BoletoCalc.Modulo11_2_9(rowBanco.CONCodigoCNR, true));
                    especieDOC = "RC";
                    carteira = (CarteiraBanco == 6 ? "SR" : CarteiraBanco.ToString()); //"SR";
                    fonteNN = "N0";
                    break;
            }
        }
    }
}