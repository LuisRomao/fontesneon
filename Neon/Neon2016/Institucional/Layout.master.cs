using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Layout : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Carrega Header (menu recursivo)
        Page.Header.DataBind();

        //Seta botao default para o enter no botao de login (exceto MeuCondominio.aspx)
        if (!Page.Request.FilePath.Contains("MeuCondominio"))
            Page.Form.DefaultButton = cmdLogin.UniqueID;

        //Seta descricao dentro dos campos de login
        txtUsuario.Attributes["placeholder"] = "Usu�rio";
        txtSenha.Attributes["placeholder"] = "Senha";
    }

    protected void cmdLogin_Click(object sender, EventArgs e)
    {
        //Seta variaveis de sessao
        Session["User"] = txtUsuario.Text;
        Session["Pass"] = txtSenha.Text;

        //Vai para o login
        Response.Redirect("~/Account/Login.aspx");
    }
}