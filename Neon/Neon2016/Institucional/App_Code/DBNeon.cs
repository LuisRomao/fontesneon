﻿using System.Data.SqlClient;
using System.Data;

public class DBNeon
{
    public SqlDataReader CondominiosAtivos(string connStr)
    {
        //Cria conexao
        SqlConnection myConnection = new SqlConnection(connStr);

        //Abre conexao
        myConnection.Open();

        //Seta Query
        string strQuery = string.Format("SELECT * FROM CONDOMINIOS WHERE CONStatus =  1 AND CON_LAT <> 0 AND CON_LNG <> 0");
        SqlCommand myCommand = new SqlCommand(strQuery, myConnection);

        //Executa
        SqlDataReader drReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        myCommand.Dispose();

        //Retorna o datareader
        return drReader;
    }
}
