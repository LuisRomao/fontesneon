﻿using System.Data.SqlClient;
using System.Data;

public class DBSite
{
    public SqlDataReader Conteudo(string connStr, int intCNT)
    {
        //Cria conexao
        SqlConnection myConnection = new SqlConnection(connStr);

        //Abre conexao
        myConnection.Open();

        //Seta Query
        string strQuery = string.Format("SELECT * FROM CoNTeudo WHERE CNT = {0}", intCNT);
        SqlCommand myCommand = new SqlCommand(strQuery, myConnection);

        //Executa
        SqlDataReader drReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        myCommand.Dispose();

        //Retorna o datareader
        return drReader;
    }
}
