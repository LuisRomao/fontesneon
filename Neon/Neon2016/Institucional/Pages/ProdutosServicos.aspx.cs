﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using DevExpress.Web;

public partial class Pages_ProdutosServicos : System.Web.UI.Page
{
    //Conteúdo: Produtos e Serviços - CNT = 1
    private int intCNT = 1;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Seta o tema da pagina
        ASPxWebControl.GlobalTheme = "Moderno";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Preenche textos e imagens (substituir por banco de dados se necessario)
        litPrincipal.Text = "Os bens situados no interior do seu imóvel não estão segurados em caso de incêndio. Isto ocorre porque o seguro do condomínio cobre apenas " +
                            "a estrutura física do seu prédio. Em caso de incêndio, o patrimônio que você conquistou pode desaparecer num piscar de olhos, especialmente " +
                            "se não estiver com a cobertura de seguro. É claro que dentre suas prioridades estão sua tranquilidade e conforto. Pensando nisso, a Marítima " +
                            "Seguros desenvolveu um produto para atender as necessidades dos clientes da NEON – o Seguro *Conteúdo contra Incêndio, Raio e Explosão, com " +
                            "coberturas exclusivas e com uma ampla rede de serviços oferecidos pela Assistência 24hs. Garanta a segurança de seu patrimônio com um investimento " +
                            "na medida exata de suas necessidades!";

        litMensagemRodape.Text = "*Esse material não constitui um contrato de seguro, nem uma completa descrição das condições, garantias, exclusões ou outros termos de " +
                                 "cobertura. Para mais detalhes, por favor consulte as condições gerais do produto. Processo SUSEP Nº. 15414.005203/2005-11 (marítima residencial) " +
                                 "CNPJ: 61383493/0001-80 o registro deste plano na SUSEP não implica por parte da autarquia incentivo ou recomendação a sua comercialização.";
    }

    protected void cmdEnviar_Click(object sender, EventArgs e)
    {
        //Limpa mensagem
        lblMsg.Text = "";
        panMsg.Visible = false;

        try
        {
            //Pega dados configurados para o conteudo
            string strEmailTo = "";
            string strEmailBcc = "";
            DBSite DBSite = new DBSite();
            SqlDataReader drConteudo = DBSite.Conteudo(ConfigurationManager.ConnectionStrings["SiteConnectionString"].ToString(), intCNT);
            if (drConteudo.HasRows)
            {
                drConteudo.Read();
                if (drConteudo["CNTEmailTo"] != DBNull.Value) strEmailTo = drConteudo["CNTEmailTo"].ToString();
                if (drConteudo["CNTEmailBcc"] != DBNull.Value) strEmailBcc = drConteudo["CNTEmailBcc"].ToString();
            }
            drConteudo.Close();

            //Monta e envia e-mail
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();
            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //From
                email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");

                //To
                email.To.Add(strEmailTo.Replace(";", ","));

                //Bcc
                if (strEmailBcc != "") email.Bcc.Add(strEmailBcc.Replace(";", ","));

                //Corpo do email
                email.Subject = "Solicitação de Contato - Adesão Seguro Conteúdo";
                email.IsBodyHtml = true;
                email.Body = string.Format("<html>\r\n" +
                                           "   <head></head>\r\n" +
                                           "   <body style='font: 11pt Calibri'>\r\n" +
                                           "      <p style='font-size: 14pt'><b>SOLICITAÇÃO DE CONTATO - ADESÃO SEGURO CONTEÚDO</b></p>\r\n" +
                                           "      <p><b>Data:</b> {0}</p>\r\n" +
                                           "      <p><b>Nome:</b> {1}</p>\r\n" +
                                           "      <p><b>E-mail:</b> {2}</p>\r\n" +
                                           "      <p><b>Telefone:</b> {3}</p>\r\n" +
                                           "      <p><b>Mensagem:</b><br/>{4}</p>\r\n" +
                                           "   </body>\r\n" +
                                           "</html>",
                                           DateTime.Now, Server.HtmlEncode(txtNome.Text), Server.HtmlEncode(txtEmail.Text), Server.HtmlEncode(txtTelefone.Text), Server.HtmlEncode(txtMensagem.Text).Replace("\n", "<br/>"));

                //Envia email
                SmtpClient1.Send(email);

                //Sucesso no envio
                txtNome.Text = txtEmail.Text = txtTelefone.Text = txtMensagem.Text = "";
                lblMsg.Text = "Solicitação de contato enviada com sucesso! \r\nRetornaremos o seu contato o mais breve possível.";
                panMsg.Visible = true;
            }
        }
        catch (Exception)
        {
            //Erro no envio
            lblMsg.Text = "Não foi possível enviar sua solicitação de contato! \r\nPor favor, tente novamente.";
            panMsg.Visible = true;
        }

        //Posiciona no campo
        cmdEnviar.Focus();
    }
}