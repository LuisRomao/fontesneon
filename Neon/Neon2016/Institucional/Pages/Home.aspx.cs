using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

public partial class Pages_Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Abre popup se no periodo indicado
        if (DateTime.Now.Date <= Convert.ToDateTime("31/01/2018"))
            ASPxPopupControl.ShowOnPageLoad = true;
        else
            ASPxPopupControl.ShowOnPageLoad = false;

        //Seta banners se no periodo indicado
        ASPxImageSlider1.Items.Add(new ImageSliderItem(@"~/Content/Images/Sliders/banner5.jpg", "", "~/Pages/ProdutosServicos.aspx", "<h3 class='slideTitle'>Seguro conte�do</h3><p>Proteja seu patrim�nio e relaxe! <a tabindex='-1' href='javascript:void({0});'>Clique aqui e saiba mais �</a></p>"));
        if (DateTime.Now.Date <= Convert.ToDateTime("31/01/2018"))
            ASPxImageSlider1.Items.Add(new ImageSliderItem(@"~/Content/Images/Sliders/banner6.jpg", "", "http://www.mb7.com.br/neon/pdf/mesa_redonda_email.pdf", "<h3 class='slideTitle'>Mesa Redonda</h3><p>Venha tirar suas d�vidas. Garanta o seu lugar. Fa�a j� a sua inscri��o, <a tabindex='-1' href='javascript:void({0});'>clicando aqui �</a></p>"));
        ASPxImageSlider1.Items.Add(new ImageSliderItem(@"~/Content/Images/Sliders/banner1.jpg", "", "~/Pages/Administracao.aspx", "<h3 class='slideTitle'>Neon Im�veis</h3><p>Seja bem vindo � maior administradora do ABC. <a tabindex='-1' href='javascript:void({0});'>Clique aqui �</a></p>"));
        ASPxImageSlider1.Items.Add(new ImageSliderItem(@"~/Content/Images/Sliders/banner2.jpg", "", "~/Pages/LinhaTempo.aspx", "<h3 class='slideTitle'>Trajet�ria</h3><p>Conhe�a nossa trajet�ria. <a tabindex='-1' href='javascript:void({0});'>Clique aqui �</a></p>"));
        ASPxImageSlider1.Items.Add(new ImageSliderItem(@"~/Content/Images/Sliders/banner3.jpg", "", "~/Pages/Confianca.aspx", "<h3 class='slideTitle'>30 anos</h3><p>A Neon faz 30 anos, veja as palavras da fundadora. <a tabindex='-1' href='javascript:void({0});'>Clique aqui �</a></p>"));

        //Seta foco no campos de usuario
        ((TextBox)Master.FindControl("TopPanel$txtUsuario")).Focus();
    }
}
