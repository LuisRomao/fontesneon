﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using DevExpress.Web;

public partial class Pages_Proposta : System.Web.UI.Page
{
    //Conteúdo: Proposta - CNT = 4
    private int intCNT = 4;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Seta o tema da pagina
        ASPxWebControl.GlobalTheme = "Moderno";
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void cmdEnviar_Click(object sender, EventArgs e)
    {
        //Limpa mensagem
        lblMsg.Text = "";
        panMsg.Visible = false;

        try
        {
            //Pega dados configurados para o conteudo
            string strEmailTo = "";
            string strEmailBcc = "";
            DBSite DBSite = new DBSite();
            SqlDataReader drConteudo = DBSite.Conteudo(ConfigurationManager.ConnectionStrings["SiteConnectionString"].ToString(), intCNT);
            if (drConteudo.HasRows)
            {
                drConteudo.Read();
                if (drConteudo["CNTEmailTo"] != DBNull.Value) strEmailTo = drConteudo["CNTEmailTo"].ToString();
                if (drConteudo["CNTEmailBcc"] != DBNull.Value) strEmailBcc = drConteudo["CNTEmailBcc"].ToString();
            }
            drConteudo.Close();

            //Monta e envia e-mail
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();
            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //From
                email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");

                //To
                email.To.Add(strEmailTo.Replace(";", ","));

                //Bcc
                if (strEmailBcc != "") email.Bcc.Add(strEmailBcc.Replace(";", ","));

                //Corpo do email
                email.Subject = "Solicitação de Proposta";
                email.IsBodyHtml = true;
                email.Body = string.Format("<html>\r\n" +
                                           "   <head></head>\r\n" +
                                           "   <body style='font: 11pt Calibri'>\r\n" +
                                           "      <p style='font-size: 14pt'><b>SOLICITAÇÃO DE PROPOSTA</b></p>\r\n" +
                                           "      <p><b>Data:</b> {0}</p>\r\n" +
                                           "      <p><b>Nome:</b> {1}</p>\r\n" +
                                           "      <p><b>E-mail:</b> {2}</p>\r\n" +
                                           "      <p><b>Telefone:</b> {3}</p>\r\n" +
                                           "      <p><b>Condomínio:</b> {4}</p>\r\n" +
                                           "      <p><b>Endereço:</b> {5}</p>\r\n" +
                                           "      <p><b>Bairro:</b> {6}</p>\r\n" +
                                           "      <p><b>Cidade:</b> {7}</p>\r\n" +
                                           "      <p><b>CEP:</b> {8}</p>\r\n" +
                                           "      <p><b>Unidades:</b> {9}</p>\r\n" +
                                           "      <p><b>Torres:</b> {10}</p>\r\n" +
                                           "      <p><b>Já possui Administradora? </b> {11}</p>\r\n" +
                                           "      <p><b>Qtd. Funcionários Contratados:</b> {12}</p>\r\n" +
                                           "      <p><b>Qtd. Funcionários Terceirizados:</b> {13}</p>\r\n" +
                                           "      <p><b>Seu Cargo:</b> {14}</p>\r\n" +
                                           "      <p><b>Observação:</b><br/>{15}\r\n" +
                                           "   </body>\r\n" +
                                           "</html>",
                                           DateTime.Now, Server.HtmlEncode(txtNome.Text), Server.HtmlEncode(txtEmail.Text), Server.HtmlEncode(txtTelefone.Text),
                                           Server.HtmlEncode(txtCondominio.Text), Server.HtmlEncode(txtEndereco.Text),
                                           Server.HtmlEncode(txtBairro.Text), Server.HtmlEncode(txtCidade.Text), Server.HtmlEncode(txtCEP.Text),
                                           Server.HtmlEncode(txtUnidades.Text), Server.HtmlEncode(txtTorres.Text), Server.HtmlEncode(rblAdministradora.Value.ToString() == "0" ? "Não" : "Sim"),
                                           Server.HtmlEncode(txtQtdFunContratados.Text), Server.HtmlEncode(txtQtdFunTerceirizados.Text), Server.HtmlEncode(txtCargo.Text),
                                           Server.HtmlEncode(txtObservacao.Text).Replace("\n", "<br/>"));

                //Envia email
                SmtpClient1.Send(email);

                //Sucesso no envio
                txtNome.Text = txtEmail.Text = txtTelefone.Text = txtCondominio.Text = txtEndereco.Text = 
                txtBairro.Text = txtCidade.Text = txtCEP.Text = txtUnidades.Text = txtTorres.Text = 
                txtQtdFunContratados.Text = txtQtdFunTerceirizados.Text = txtCargo.Text = txtObservacao.Text = "";
                rblAdministradora.Value = "";
                lblMsg.Text = "Solicitação de proposta enviada com sucesso! \r\nRetornaremos o seu contato o mais breve possível.";
                panMsg.Visible = true;
            }
        }
        catch (Exception)
        {
            //Erro no envio
            lblMsg.Text = "Não foi possível enviar sua solicitação de contato! \r\nPor favor, tente novamente.";
            panMsg.Visible = true;
        }

        //Posiciona no campo
        cmdEnviar.Focus();
    }
}