﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Diferenciais : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Preenche textos e imagens (substituir por banco de dados se necessario)
        imgDiferencial1.ImageUrl = "~/Content/Images/votacao-eletronica.jpg";
        litDiferencial1Titulo.Text = "Votação Eletrônica";
        litDiferencia1.Text = "• Votação pela fração ideal; <br/>" +
                              "• Possibilidade de conferência de cédulas; <br/>" +
                              "• Leitura por código de barras; <br/>" +
                              "• Confiabilidade nas informações.";

        imgDiferencial2.ImageUrl = "~/Content/Images/pagamento-eletronico.jpg";
        litDiferencial2Titulo.Text = "Pagamento Eletrônico";
        litDiferencia2.Text = "• Substituição a emissão de cheques por um procedimento eletrônico, moderno e totalmente seguro; <br/>" +
                              "• Ganho de produtividade; <br/>" +
                              "• Redução dos prazos nas operações; <br/>" +
                              "• Redução das tarifas bancárias.";

        imgDiferencial3.ImageUrl = "~/Content/Images/site-condominio.jpg";
        litDiferencial3Titulo.Text = "Site Exclusivo do Condomínio";
        litDiferencia3.Text = "• Informações exclusivas do condomínio como plantas, melhorias, ramais, etc; <br/>" +
                              "• Reserva de áreas comuns; <br/>" +
                              "• Controle de mudança; <br/>" +
                              "• Autorização de entrada de prestadores de serviço; <br/>" +
                              "• Dentre outras ferramentas importantes no dia a dia do condômino.";
    }
}