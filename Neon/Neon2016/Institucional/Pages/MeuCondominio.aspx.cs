﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

public partial class Pages_MeuCondominio : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Seta o tema da pagina
        ASPxWebControl.GlobalTheme = "Moderno";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Seta botao default para o enter no botao de login
        Page.Form.DefaultButton = cmdLogin.UniqueID;

        //Seta foco no botao
        cmdLogin.Focus();
    }

    protected void cmdLogin_Click(object sender, EventArgs e)
    {
        //Seta variaveis de sessao
        Session["User"] = txtUsuario.Text;
        Session["Pass"] = txtSenha.Text;
        
        //Vai para o login
        Response.Redirect("~/Account/Login.aspx");
    }
}