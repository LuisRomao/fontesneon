﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using DevExpress.Web;

public partial class Pages_Imobiliarias : System.Web.UI.Page
{
    //Conteúdo: Imobiliárias - CNT = 3
    private int intCNT = 3;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Seta o tema da pagina
        ASPxWebControl.GlobalTheme = "Moderno";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Preenche textos e imagens (substituir por banco de dados se necessario)
        litPrincipal.Text = "Atuamos em sintonia com as imobiliárias no sentido de oferecer praticidade e agilidade na entrada, administração e saída de inquilinos nos " +
                            "condomínios que administramos." +
                            "<br/><br/>" +
                            "Fornecemos uma senha exclusiva que dá acesso as principais informações para facilitar o trabalho da imobiliária e a vida dos condôminos." +
                            "<br/><br/>" +
                            "Entre com contato e veja os diferenciais que podemos oferecer." +
                            "<br/>";
    }

    protected void cmdEnviar_Click(object sender, EventArgs e)
    {
        //Limpa mensagem
        lblMsg.Text = "";
        panMsg.Visible = false;

        try
        {
            //Pega dados configurados para o conteudo
            string strEmailTo = "";
            string strEmailBcc = "";
            DBSite DBSite = new DBSite();
            SqlDataReader drConteudo = DBSite.Conteudo(ConfigurationManager.ConnectionStrings["SiteConnectionString"].ToString(), intCNT);
            if (drConteudo.HasRows)
            {
                drConteudo.Read();
                if (drConteudo["CNTEmailTo"] != DBNull.Value) strEmailTo = drConteudo["CNTEmailTo"].ToString();
                if (drConteudo["CNTEmailBcc"] != DBNull.Value) strEmailBcc = drConteudo["CNTEmailBcc"].ToString();
            }
            drConteudo.Close();

            //Monta e envia e-mail
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();
            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //From
                email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");

                //To
                email.To.Add(strEmailTo.Replace(";", ","));

                //Bcc
                if (strEmailBcc != "") email.Bcc.Add(strEmailBcc.Replace(";", ","));

                //Corpo do email
                email.Subject = "Solicitação de Contato - Imobiliárias";
                email.IsBodyHtml = true;
                email.Body = string.Format("<html>\r\n" +
                                           "   <head></head>\r\n" +
                                           "   <body style='font: 11pt Calibri'>\r\n" +
                                           "      <p style='font-size: 14pt'><b>SOLICITAÇÃO DE CONTATO - IMOBILIÁRIAS</b></p>\r\n" +
                                           "      <p><b>Data:</b> {0}</p>\r\n" +
                                           "      <p><b>Nome:</b> {1}</p>\r\n" +
                                           "      <p><b>E-mail:</b> {2}</p>\r\n" +
                                           "      <p><b>Telefone:</b> {3}</p>\r\n" +
                                           "      <p><b>Mensagem:</b><br/>{4}</p>\r\n" +
                                           "   </body>\r\n" +
                                           "</html>",
                                           DateTime.Now, Server.HtmlEncode(txtNome.Text), Server.HtmlEncode(txtEmail.Text), Server.HtmlEncode(txtTelefone.Text), Server.HtmlEncode(txtMensagem.Text).Replace("\n", "<br/>"));

                //Envia email
                SmtpClient1.Send(email);

                //Sucesso no envio
                txtNome.Text = txtEmail.Text = txtTelefone.Text = txtMensagem.Text = "";
                lblMsg.Text = "Solicitação de contato enviada com sucesso! \r\nRetornaremos o seu contato o mais breve possível.";
                panMsg.Visible = true;
            }
        }
        catch (Exception)
        {
            //Erro no envio
            lblMsg.Text = "Não foi possível enviar sua solicitação de contato! \r\nPor favor, tente novamente.";
            panMsg.Visible = true;
        }

        //Posiciona no campo
        cmdEnviar.Focus();
    }
}