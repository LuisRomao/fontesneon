﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Filosofia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Preenche textos e imagens (substituir por banco de dados se necessario)
        litPrincipal.Text = "A NEON IMÓVEIS é uma empresa de Assessoria e Administração de Imóveis e Condomínios. Desde 1987 no mercado, acompanhou a verticalização da cidade, " +
                        "onde o Condomínio tornou-se um verdadeiro complexo econômico/jurídico/social, no qual centenas de pessoas convivem diariamente, tornando-se indispensável " +
                        "um acompanhamento especializado na rotina dos seus habitantes. Para tanto mantemos uma completa estrutura, no sentido de colaborar com o SR(A). SÍNDICO(A), " +
                        "colocando à sua disposição toda a nossa experiência nas áreas de administração, segurança, recursos humanos, manutenção, convivência interna e principalmente " +
                        "no respeito à todos que participam desta comunidade." +
                        "<br/><br/>" +
                        "Sempre numa posição de vanguarda na área de prestação de serviços, a NEON Administradora possui um sistema integrado e inovador, oferecendo condições" +
                        " para atendimentos personalizados. O nosso objetivo é o aperfeiçoamento constante, só possível numa empresa que questiona permanentemente seus próprios métodos.";

        imgRazao1.ImageUrl = "~/Content/Images/razao-transparencia.png";
        litRazao1Titulo.Text = "Transparência".ToUpper();
        litRazao1.Text = "A tomada de decisão quanto à escolha de uma administradora para gerenciar a conta de um condomínio é de extrema importância, não só pela responsabilidade que " +
                         "se exige das partes, como também, por tratar-se de um relacionamento de longa duração. Administramos todos os condomínio com CONTAS CORRENTES INDIVIDUALIZADAS, " +
                         "o que significa uma total TRANSPARÊNCIA no demonstrativo das receitas e despesas, transformando o rateio condominial no melhor custo / benefício, de forma a " +
                         "devolver aos condôminos a qualidade de serviços compatíveis aos valores arrecadados.";

        imgRazao2.ImageUrl = "~/Content/Images/razao-agilidade.png";
        litRazao2Titulo.Text = "Agilidade".ToUpper();
        litRazao2.Text = "Respondemos com rapidez e agilidade as mais diversas demandas do constante crescimento e profissionalização do mercado de condomínios. Para isso, investimos " +
                         "constantemente não só no treinamento de nossos profissionais, como também na aquisição de tecnologia de ponta, objetivando encontrar soluções com rapidez e " +
                         "eficácia para os mais diversos problemas que surgem do dia-a-dia.";

        imgRazao3.ImageUrl = "~/Content/Images/razao-tecnologia.png";
        litRazao3Titulo.Text = "Tecnologia".ToUpper();
        litRazao3.Text = "Investimos em um sistema próprio e exclusivo, disponibilizando aos síndicos dados claros e precisos, fornecendo subsídios para tomada de importantes decisões. " +
                         "Afinada com as rápidas transformações, nosso sistema está em constante aperfeiçoamento, dando um exemplo de conduta a tudo que envolve o interesse de nossos " +
                         "clientes, primando pela ética, respeito e idoneidade.";

        imgRazao4.ImageUrl = "~/Content/Images/razao-confianca.png";
        litRazao4Titulo.Text = "Confiança".ToUpper();
        litRazao4.Text = "Com o objetivo de ter nosso nome consolidado no mercado, trabalhamos incessantemente em busca da perfeição. Para tanto não medimos esforços no aprimoramento de " +
                         "nossos profissionais, que se empenham em propor soluções rápidas e eficientes. Construímos nosso nome passo a passo, sempre em destaque como uma empresa sólida " +
                         "e responsável. Tornamo-nos uma das mais notáveis companhias de prestação de serviços do ABC, o que se comprova não apenas pela constante expansão, como também " +
                         "pela fidelização de nossos clientes. Temos muito orgulho de nossa história, porque hoje, como poucas, temos uma história de sucesso para contar.";

        litVisao.Text = "Valorizamos nossos funcionários através da integração, formação e utilização das mais avançadas tecnologias que visam superar as expectativas de todos os " +
                        "nossos clientes e colaboradores. Buscamos o crescimento baseado em confiança, respeito e fidelização.";

        litMissao.Text = "Administração transparente voltada a preservação e valorização do patrimônio, promovendo a qualidade de vida, segurança e uma convivência harmônica e de " +
                         "bem estar nos Condomínios que administramos.";

        litValores.Text = "<ul>" +
                         "<li><strong> Ética Total </strong><br>" +
                         "Postura nos relacionamentos profissionais e interpessoais.</li>" +
                         "<li><strong> Respeito </strong><br>" +
                         "Valorização dos ser humano e respeito pelo patrimônio do cliente.</li>" +
                         "<li><strong> Transparência </strong><br>" +
                         "Conduta que transmite confiança.</li>" +
                         "<li><strong> Responsabilidade </strong><br>" +
                         "Zelo na gestão dos recursos.</li>" +
                         "<li><strong> Inovação </strong><br>" +
                         "Valorização da criatividade aliada a avanços tecnológicos.</li>" +
                         "<li><strong> Estratégia </strong><br>" +
                         "Clareza e foco nos objetivos.</li>" +
                         "</ul>";
    }
}