﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Administracao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Preenche textos e imagens (substituir por banco de dados se necessario)
        litItem1.Text = "1 – Administração Geral".ToUpper();
        litSubItens1.Text = "1.1 – Emissão de conformidade com a Convenção do Condomínio, das convocações para Assembléias Gerais Ordinárias e Extraordinárias, com discriminação dos itens constantes das respectivas ordens do dia; <br/>" +
                            "1.2 – Elaboração das atas das assembléias, seus competentes registros e oportuno envio de cópias à todos os senhores condôminos; <br/>" +
                            "1.3 – Execução das determinações do Síndico ou das Assembléias tais como: expedição de circulares, cartas e notificações, coletas de propostas e orçamentos para serviços, obras, reparos e consertos bem como supervisão nos serviços de rotina; <br/>" +
                            "1.4 – Expedir ordens e instruções transmitidas pelo Síndico de modo a impor disciplina à boa conservação e asseio das partes comuns do edifício.";

        litItem2.Text = "2 – Administração de Recursos Financeiros".ToUpper();
        litSubItens2.Text = "2.1 – Visando o melhor controle, transparência e segurança para os recursos angariados pelo Condomínio, os Edifícios administrados pela Neon Imóveis possuem Contas Bancárias Próprias, onde além da possibilidade de consultas a saldos, os recursos poderão ser aplicados gerando rendimentos para o Condomínio; <br/>" +
                            "2.2 – Com base nas despesas do período anterior, enviamos a Previsão Orçamentária, com o objetivo de mostrar a real posição econômica do Condomínio, possibilitando a emissão de valores adequados para as despesas previstas, ordinárias ou extraordinárias. A serem submetidas as Assembléias de Condôminos ou Síndico; <br/>" +
                            "2.3 – Rateio de Cobrança das despesas condominiais aprovadas em Assembléias, mediante o envio antecipado de Avisos/Recibos aos condôminos, para pagamento em qualquer banco participante do sistema de compensação, até a data de vencimento; <br/>" +
                            "2.4 – Prestação mensal de contas, mediante o envio a todos os condôminos, do demonstrativo das receitas e despesas. Posição dos condôminos inadimplentes e encaminhamento ao Síndico e membros do Conselho Consultivo/Fiscal, de todos os documentos comprovatórios dos recebimentos e dos pagamentos efetuados; <br/>" +
                            "2.5 – Efetuamos o pagamento pontual de todas as contas e obrigações condominiais, representados por documentos, devidamente autorizados pelo Síndico, bem como os pagamentos de rotina, tais como: conta de luz, taxas de água e esgoto, impostos e tributos, salários, encargos sociais e despesas previamente autorizadas; <br/>" +
                            "2.6 – Fazemos a emissão de cheque e Controle Bancário, de forma totalmente informatizada, com acompanhamento dos saldos diários através de Sistema On-line com os Bancos, captando informações necessárias para atualização diária; <br/>" +
                            "2.7 – O demonstrativo das Receitas/Despesas, são totalmente conciliados com o extrato bancário, em conta independente do condomínio, de forma que o saldo existente no Balancete seja o mesmo apresentado na Conta Corrente Bancária, Conta Poupança e Aplicações do Condomínio; <br/>" +
                            "2.8 – Elaboração de Previsão Orçamentária: <br/>" +
                            "NOS DEMONSTRATIVOS FINANCEIROS, SERÃO APRESENTADOS OS VALORES DOS SALDOS EXISTENTES EM: CONTA CORRENTE, FUNDO DE RESERVA, FUNDO DE OBRAS, PROVISÃO PARA 13º SALÁRIO, FÉRIAS, ETC.";

        litItem3.Text = "3 – Administração de Pessoal".ToUpper();
        litSubItens3.Text = "Para a conservação, manutenção e guarda do Edifício, são necessárias diversas contratações. Pensando nisso e com o intuito de proporcionar maior comodidade ao condomínio, temos nosso setor de Recursos Humanos à sua inteira disposição: <br/>" +
                            "3.1 – O Departamento de Pessoal encaminha ao Corpo Diretivo do Condomínio, candidatos previamente selecionados, para entrevistas e admissões; <br/>" +
                            "3.2 – Efetuamos o pagamento dos salários dos empregados do Condomínio (o qual será feito até o quinto dia útil de cada mês subsequente ao trabalhado), pagamento de horas extras, prêmios, adicionais extraordinários, férias, etc. sempre aprovadas pelo Síndico e em cumprimento à legislação trabalhista e a convenção coletiva de trabalho da categoria; <br/>" +
                            "3.3 – Os funcionários receberão seus vencimentos através de cartão eletrônico. Desse forma poderão sacar seus salários em qualquer Banco 24 horas e no número de vezes que for mais adequado às suas necessidades; <br/>" +
                            "3.4 – Cumprimos todas as exigências da legislação social trabalhista, recolhendo sempre a tempo, os encargos devidos ao INSS, PIS e outros, mantendo sempre em dia os livros e documentos exigidos por lei; <br/>" +
                            "3.5 – Calculamos e acompanhamos todas as rescisões; <br/>" +
                            "3.6 – Acompanhamos a Legislação pertinente.";

        litItem4.Text = "4 – Manutenção e Conservação".ToUpper();
        litSubItens4.Text = "Além dos empregados com vínculo empregatício, são necessários também a prestação de serviços autônomos, tais como: eletricistas, pedreiros, encanadores, etc. A Neon Imóveis se propõe a: <br/>" +
                            "4.1 – Efetuar a captação e encaminhamento de orçamentos de serviços, quando solicitados; <br/>" +
                            "4.2 – Comprar Bens e Materiais solicitados previamente; <br/>" +
                            "4.3 – No caso de obras contratadas pelo Edifício junto à terceiros, a Neon efetuará as arrecadações necessárias e efetuará o pagamento nas datas determinadas; <br/>" +
                            "4.4 – Encaminhar prestadores de serviços autônomos, ou seja, eletricistas, pedreiros, encanadores, etc. Desde que solicitado pelo Sindico; <br/>" +
                            "4.5 – Realizar semestralmente uma verificação dos equipamentos preventivos de incêndio; <br/>" +
                            "4.6 – Informar ao Condomínio o período necessário para a execução de limpeza de Caixas D’água.";

        litItem5.Text = "5 – Departamento Jurídico".ToUpper();
        litSubItens5.Text = "5.1 – Atendemos gratuitamente o condomínio, em consultas e na orientação de natureza jurídica, bem como efetuamos o acompanhamento de contratos firmados com empresas de prestação de serviços; <br/>" +
                            "5.2 – Efetuamos a cobrança dos inadimplentes através de cobranças judiciais e extrajudiciais, quando necessárias; <br/>" +
                            "5.3 – Efetuamos a defesa e o acompanhamento nas audiências referentes à reclamações trabalhistas, com honorários já previstos contratualmente; <br/>" +
                            "5.4 – O acompanhamento dos processos são totalmente informatizados, possibilitando ao Síndico informações atualizadas e providências tomadas com a antecedência necessária, através de relatórios enviados periodicamente.";

        litItem6.Text = "6 – Malotes".ToUpper();
        litSubItens6.Text = "6.1 – Visitamos o Condomínio duas vezes por semana para entrega/retirada de correspondência, com malotes lacrados, possibilitando assim maior conforto e segurança. <br/>";

        litItem7.Text = "7 – Deveres".ToUpper();
        litSubItens7.Text = "7.1 – Obrigamo-nos a pagar pontualmente (respondendo por acréscimos e multas a que der causa) todas as contas e obrigações condominiais, representadas por documentos devidamente autorizados pelo Síndico, e recebido por nós em tempo hábil, desde que o Condomínio possua saldo para tal.";
    }
}