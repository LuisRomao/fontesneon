﻿namespace CadastroUtil.Datasets
{
    partial class dCadastro
    {
        public int EMP;

        public dCadastro(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dCadastroTableAdapters.USuaRiosTableAdapter taUSuaRiosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: USuaRios
        /// </summary>
        public dCadastroTableAdapters.USuaRiosTableAdapter USuaRiosTableAdapter
        {
            get
            {
                if (taUSuaRiosTableAdapter == null)
                {
                    taUSuaRiosTableAdapter = new dCadastroTableAdapters.USuaRiosTableAdapter();
                };
                return taUSuaRiosTableAdapter;
            }
        }

        private dCadastroTableAdapters.TipoUsuarioSiteTableAdapter taTipoUsuarioSiteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TipoUsuarioSite
        /// </summary>
        public dCadastroTableAdapters.TipoUsuarioSiteTableAdapter TipoUsuarioSiteTableAdapter
        {
            get
            {
                if (taTipoUsuarioSiteTableAdapter == null)
                {
                    taTipoUsuarioSiteTableAdapter = new dCadastroTableAdapters.TipoUsuarioSiteTableAdapter();
                };
                return taTipoUsuarioSiteTableAdapter;
            }
        }

        private dCadastroTableAdapters.CONDOMINIOSTableAdapter taCONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dCadastroTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (taCONDOMINIOSTableAdapter == null)
                {
                    taCONDOMINIOSTableAdapter = new dCadastroTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP == 3)
                        taCONDOMINIOSTableAdapter.Connection.ConnectionString = taCONDOMINIOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCONDOMINIOSTableAdapter;
            }
        }

        private dCadastroTableAdapters.ST_CondominioFUncionalidadeTableAdapter taST_CondominioFUncionalidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_CondominioFUncionalidade
        /// </summary>
        public dCadastroTableAdapters.ST_CondominioFUncionalidadeTableAdapter ST_CondominioFUncionalidadeTableAdapter
        {
            get
            {
                if (taST_CondominioFUncionalidadeTableAdapter == null)
                {
                    taST_CondominioFUncionalidadeTableAdapter = new dCadastroTableAdapters.ST_CondominioFUncionalidadeTableAdapter();
                    if (EMP == 3)
                        taST_CondominioFUncionalidadeTableAdapter.Connection.ConnectionString = taST_CondominioFUncionalidadeTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_CondominioFUncionalidadeTableAdapter;
            }
        }

        private dCadastroTableAdapters.ST_SMTpTableAdapter taST_SMTpTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_SMTp
        /// </summary>
        public dCadastroTableAdapters.ST_SMTpTableAdapter ST_SMTpTableAdapter
        {
            get
            {
                if (taST_SMTpTableAdapter == null)
                {
                    taST_SMTpTableAdapter = new dCadastroTableAdapters.ST_SMTpTableAdapter();
                    if (EMP == 3)
                        taST_SMTpTableAdapter.Connection.ConnectionString = taST_SMTpTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_SMTpTableAdapter;
            }
        }

        private dCadastroTableAdapters.BLOCOSTableAdapter taBLOCOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BLOCOS
        /// </summary>
        public dCadastroTableAdapters.BLOCOSTableAdapter BLOCOSTableAdapter
        {
            get
            {
                if (taBLOCOSTableAdapter == null)
                {
                    taBLOCOSTableAdapter = new dCadastroTableAdapters.BLOCOSTableAdapter();
                    if (EMP == 3)
                        taBLOCOSTableAdapter.Connection.ConnectionString = taBLOCOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taBLOCOSTableAdapter;
            }
        }

        private dCadastroTableAdapters.APARTAMENTOSTableAdapter taAPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dCadastroTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (taAPARTAMENTOSTableAdapter == null)
                {
                    taAPARTAMENTOSTableAdapter = new dCadastroTableAdapters.APARTAMENTOSTableAdapter();
                    if (EMP == 3)
                        taAPARTAMENTOSTableAdapter.Connection.ConnectionString = taAPARTAMENTOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taAPARTAMENTOSTableAdapter;
            }
        }

        private dCadastroTableAdapters.CondominoTableAdapter taCondominoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Condomino
        /// </summary>
        public dCadastroTableAdapters.CondominoTableAdapter CondominoTableAdapter
        {
            get
            {
                if (taCondominoTableAdapter == null)
                {
                    taCondominoTableAdapter = new dCadastroTableAdapters.CondominoTableAdapter();
                    if (EMP == 3)
                        taCondominoTableAdapter.Connection.ConnectionString = taCondominoTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCondominoTableAdapter;
            }
        }

        private dCadastroTableAdapters.PESSOASTableAdapter taPESSOASTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESSOAS
        /// </summary>
        public dCadastroTableAdapters.PESSOASTableAdapter PESSOASTableAdapter
        {
            get
            {
                if (taPESSOASTableAdapter == null)
                {
                    taPESSOASTableAdapter = new dCadastroTableAdapters.PESSOASTableAdapter();
                    if (EMP == 3) taPESSOASTableAdapter.Connection.ConnectionString = taPESSOASTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taPESSOASTableAdapter;
            }
        }

        private dCadastroTableAdapters.PESxRAVTableAdapter taPESxRAVTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESxRAV
        /// </summary>
        public dCadastroTableAdapters.PESxRAVTableAdapter PESxRAVTableAdapter
        {
            get
            {
                if (taPESxRAVTableAdapter == null)
                {
                    taPESxRAVTableAdapter = new dCadastroTableAdapters.PESxRAVTableAdapter();
                    if (EMP == 3) taPESxRAVTableAdapter.Connection.ConnectionString = taPESxRAVTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taPESxRAVTableAdapter;
            }
        }

        private dCadastroTableAdapters.RamoAtiVidadeTableAdapter taRamoAtiVidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RamoAtiVidade
        /// </summary>
        public dCadastroTableAdapters.RamoAtiVidadeTableAdapter RamoAtiVidadeTableAdapter
        {
            get
            {
                if (taRamoAtiVidadeTableAdapter == null)
                {
                    taRamoAtiVidadeTableAdapter = new dCadastroTableAdapters.RamoAtiVidadeTableAdapter();
                    if (EMP == 3)
                        taRamoAtiVidadeTableAdapter.Connection.ConnectionString = taRamoAtiVidadeTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taRamoAtiVidadeTableAdapter;
            }
        }

        private dCadastroTableAdapters.ST_CLAssificadosTableAdapter taST_CLAssificadosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_CLAssificadosTableAdapter
        /// </summary>
        public dCadastroTableAdapters.ST_CLAssificadosTableAdapter ST_CLAssificadosTableAdapter
        {
            get
            {
                if (taST_CLAssificadosTableAdapter == null)
                {
                    taST_CLAssificadosTableAdapter = new dCadastroTableAdapters.ST_CLAssificadosTableAdapter();
                    if (EMP == 3) taST_CLAssificadosTableAdapter.Connection.ConnectionString = taST_CLAssificadosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_CLAssificadosTableAdapter;
            }
        }

        private dCadastroTableAdapters.CIDADESTableAdapter taCIDADESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CIDADES
        /// </summary>
        public dCadastroTableAdapters.CIDADESTableAdapter CIDADESTableAdapter
        {
            get
            {
                if (taCIDADESTableAdapter == null)
                {
                    taCIDADESTableAdapter = new dCadastroTableAdapters.CIDADESTableAdapter();
                    if (EMP == 3) taCIDADESTableAdapter.Connection.ConnectionString = taCIDADESTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCIDADESTableAdapter;
            }
        }

        private dCadastroTableAdapters.CORPODIRETIVOTableAdapter taCORPODIRETIVOTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CORPODIRETIVO
        /// </summary>
        public dCadastroTableAdapters.CORPODIRETIVOTableAdapter CORPODIRETIVOTableAdapter
        {
            get
            {
                if (taCORPODIRETIVOTableAdapter == null)
                {
                    taCORPODIRETIVOTableAdapter = new dCadastroTableAdapters.CORPODIRETIVOTableAdapter();
                    if (EMP == 3)
                        taCORPODIRETIVOTableAdapter.Connection.ConnectionString = taCORPODIRETIVOTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCORPODIRETIVOTableAdapter;
            }
        }

        private dCadastroTableAdapters.DOCumentacaoTableAdapter taDOCumentacaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DOCumentacao
        /// </summary>
        public dCadastroTableAdapters.DOCumentacaoTableAdapter DOCumentacaoTableAdapter
        {
            get
            {
                if (taDOCumentacaoTableAdapter == null)
                {
                    taDOCumentacaoTableAdapter = new dCadastroTableAdapters.DOCumentacaoTableAdapter();
                    if (EMP == 3)
                        taDOCumentacaoTableAdapter.Connection.ConnectionString = taDOCumentacaoTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taDOCumentacaoTableAdapter;
            }
        }

        private dCadastroTableAdapters.PUBlicacaoTableAdapter taPUBlicacaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PUBlicacao
        /// </summary>
        public dCadastroTableAdapters.PUBlicacaoTableAdapter PUBlicacaoTableAdapter
        {
            get
            {
                if (taPUBlicacaoTableAdapter == null)
                {
                    taPUBlicacaoTableAdapter = new dCadastroTableAdapters.PUBlicacaoTableAdapter();
                    if (EMP == 3)
                        taPUBlicacaoTableAdapter.Connection.ConnectionString = taPUBlicacaoTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taPUBlicacaoTableAdapter;
            }
        }


        private dCadastroTableAdapters.ST_AViSosTableAdapter taST_AViSosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_AViSos
        /// </summary>
        public dCadastroTableAdapters.ST_AViSosTableAdapter ST_AViSosTableAdapter
        {
            get
            {
                if (taST_AViSosTableAdapter == null)
                {
                    taST_AViSosTableAdapter = new dCadastroTableAdapters.ST_AViSosTableAdapter();
                    if (EMP == 3)
                        taST_AViSosTableAdapter.Connection.ConnectionString = taST_AViSosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_AViSosTableAdapter;
            }
        }
    }
}
