﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using DevExpress.Web;
using PortalUtil;
using BoletoUtil.Datasets;

namespace Boleto.Pages_Portal
{
    public partial class BoletoAcordo : System.Web.UI.Page
    {
        private dBoleto dBol = new dBoleto();

        private Credencial Cred
        {
            get
            {
                if (Session["CredACO"] == null)
                {
                    //Desloga e vai para a pagina de login
                    FormsAuthentication.SignOut();
                    Response.Redirect("~/Account/SessaoExpirada.aspx");
                }
                return (Credencial)Session["CredACO"];
            }
        }
        
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia datasets
            dBol = new dBoleto(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega todos os dados
            dBol.ACOrdosTableAdapter.Fill(dBol.ACOrdos, Cred.rowUsuario.APT);
            dBol.OriginalTableAdapter.Fill(dBol.Original, Cred.rowUsuario.APT);
            dBol.NovosTableAdapter.Fill(dBol.Novos, Cred.rowUsuario.APT);
            dBol.CorrecaoProvisorios(Cred.rowUsuario.CONCodigo, Cred.rowUsuario.BLOCodigo, Cred.rowUsuario.APTNumero, (short)(Cred.EMP == 1 ? 1 : 2), Cred.rowUsuario.APT, Cred.rowUsuario.Proprietario);

            //Atualiza honorarios nos boletos novos
            foreach (dBoleto.NovosRow rowNovo in dBol.Novos)
            {
                DataRow drHon = null;
                dBol.HonorariosTableAdapter.FillByBOD_BOL(dBol.Honorarios, rowNovo.BOL);
                if (dBol.Honorarios.Count > 0) drHon = dBol.Honorarios.Rows[0];
                if (drHon == null)
                {
                    dBol.HonorariosTableAdapter.FillByBOL(dBol.Honorarios, rowNovo.BOL);
                    if (dBol.Honorarios.Count > 0) drHon = dBol.Honorarios.Rows[0];
                }
                if (drHon == null)
                {
                    rowNovo.HValor = 0;
                    rowNovo.HPag = false;
                }
                else
                {
                    if (drHon["BODValor"] == DBNull.Value)
                        rowNovo.HValor = 0;
                    else
                        rowNovo.HValor = (decimal)drHon["BODValor"];
                    rowNovo.HPag = (drHon["CHEEmissao"] != DBNull.Value);
                }
            }

            //Carrega acordo (boletos originais e boletos novos)
            grdBoletosOriginais.DataSource = dBol.Original;
            grdBoletosOriginais.DataBind();
            grdBoletosNovos.DataSource = dBol.Novos;
            grdBoletosNovos.DataBind();
        }

        protected void grdBoletosNovos_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.Caption == "Parcela")
                e.DisplayText = (e.VisibleIndex + 1).ToString();
        }
    }
}