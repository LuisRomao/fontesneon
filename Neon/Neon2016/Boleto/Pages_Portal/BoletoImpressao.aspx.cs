﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;
using PortalUtil;
using BoletoUtil;
using BoletoUtil.Datasets;

namespace Boleto.Pages_Portal
{
    public partial class BoletoImpressao : System.Web.UI.Page
    {
        public TDadosBanco DadosBanco;
        public string LinhaDigitavel;
        public string[] TextoInterno;
        public string NomenclaturaCedente = "Cedente";
        public string NomenclaturaSacado = "Sacado";
        public string CedenteNome = "";
        public string CedenteEndereco = "";

        private dBoleto dBol = new dBoleto();
        private dBoleto.BoletosRow rowBoleto;
        public VirBoleto CurBoleto;

        public Credencial Cred
        {
            get
            {
                if (Session["CredBOL"] == null)
                {
                    //Desloga e vai para a pagina de login
                    FormsAuthentication.SignOut();
                    Response.Redirect("~/Account/SessaoExpirada.aspx");
                }
                return (Credencial)Session["CredBOL"];
            }
        }

        public string Bloco
        {
            get
            {
                if (Cred.rowUsuario.BLOCodigo != "SB")
                    return String.Format("bloco:{0}&nbsp;", Cred.rowUsuario.BLOCodigo);
                else
                    return "";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Pega o boleto recebido por sessao
            rowBoleto = (dBoleto.BoletosRow)Session["rowBOL"];

            //Inicia datasets
            dBol = new dBoleto(Cred.EMP);

            if (!IsPostBack)
            {
                //Monta boleto
                AjustaDadosBanco();
                AjustaDadosBoleto();
                AjustaDadosInternos();
                AjustaDadosCamposBoleto();
                ArrayList Lista = new ArrayList();
                Lista.Add(new TVia(TextoInterno[0], "", "Recibo do sacado"));
                Lista.Add(new TVia(TextoInterno[1], CurBoleto.CodigoBarras, Server.HtmlEncode("Ficha de Compensação")));
                Repeater1.DataSource = Lista;
                Repeater1.DataBind();
            }
        }

        private void AjustaDadosBanco()
        {
            dBol.DadosBancariosTableAdapter.Fill(dBol.DadosBancarios, Cred.rowUsuario.CON);
            dBoleto.DadosBancariosRow rowBanco = dBol.DadosBancarios[0];
            DadosBanco = new TDadosBanco(rowBanco);
        }

        private void AjustaDadosBoleto()
        {
            CurBoleto = new VirBoleto(rowBoleto, DadosBanco);
            if (Session["BoletoPara"] == null)
                CurBoleto.Calcula();
            else
                CurBoleto.Calcula((DateTime)Session["BoletoPara"], false);
            LinhaDigitavel = CurBoleto.LinhaDigitavel();
        }

        private void AjustaDadosInternos()
        {
            TextoInterno = new string[2];
            if (CurBoleto.data > CurBoleto.Vencto)
                TextoInterno[0] = string.Format("<div class=\"B1\">Data de vencimento original: {0:dd/MM/yyyy}<br/></div>", CurBoleto.Vencto);
            else
                TextoInterno[0] = "";
            if (rowBoleto.competencia != "0101")
                TextoInterno[0] += string.Format("<div class=\"B1\">{0}: {1}/{2}</div>",
                    Server.HtmlEncode("Competência"),
                    rowBoleto.competencia.Substring(0, 2),
                    rowBoleto.competencia.Substring(2));
            TextoInterno[1] = "<div class=\"B1\">&nbsp;Instru&ccedil;&otilde;es:</div><div class=\"N1\">"; //<font size="1">-',
            if (Cred.rowUsuario.EMP == 1 && Cred.rowUsuario.CON == 325)
            {
                //Temporario
                TextoInterno[1] += "- Conforme delibera&ccedil;&atilde;o de A.G.E. de 16/06/2003:<br/>";
                TextoInterno[1] += "- O Banco n&atilde;o est&aacute; autorizado a receber ap&oacute;s 10 dias da data de vencimento.<br/>";
                TextoInterno[1] += "- A&ccedil;&atilde;o de cobran&ccedil;a do d&eacute;bito ser&aacute; ajuizada no vig&eacute;simo dia do m&ecirc;s correspondente ao vencimento.<br/>";
                TextoInterno[1] += "- N&atilde;o conceder abatimento.<br/>";
                TextoInterno[1] += "- Quita&ccedil;&atilde;o v&aacute;lida apenas ap&oacute;s compensa&ccedil;&atilde;o do cheque.<br/>";
                TextoInterno[1] += string.Format("- Ap&oacute;s o vencimento cobrar multa de {0:n}%.<br/>", DadosBanco.Multa);
                TextoInterno[1] += "- Boleto sujeito a protesto conforme lei n&ordm; 13.160 de 21 de julho de 2008.<br/>";
            }
            else
            { 
                if (CurBoleto.data == CurBoleto.Vencto)
                    TextoInterno[1] += "- Banco n&atilde;o est&aacute; autorizado a receber ap&oacute;s 30 dias da data do vencimento.<br/>";
                else
                    TextoInterno[1] += "- Banco n&atilde;o est&aacute; autorizado a receber ap&oacute;s o vencimento.<br/>";
                TextoInterno[1] += "- N&atilde;o conceder abatimento.<br/>";
                TextoInterno[1] += "- Quita&ccedil;&atilde;o v&aacute;lida apenas ap&oacute;s compensa&ccedil;&atilde;o do cheque.<br/>";
                if (CurBoleto.data == CurBoleto.Vencto)
                {
                    if (DadosBanco.Multa > 0)
                        if (DadosBanco.TMulta == TipoMulta.unica)
                            TextoInterno[1] += string.Format("- Ap&oacute;s o vencimento cobrar multa de {0:n}%.<br/>", DadosBanco.Multa);
                        else
                            TextoInterno[1] += string.Format("- Ap&oacute;s o vencimento cobrar juros di&aacute;rios de {0}%.<br/>", DadosBanco.Multa);
                };
            }
            TextoInterno[1] += "</div>";
        }

        private void AjustaDadosCamposBoleto()
        {
            //Verifica conflito de dados de condominio (mudanca de usuario, expira sessao)
            if (Cred.CON != Cred.rowUsuario.CON)
            {
                //Desloga e vai para a pagina de login
                FormsAuthentication.SignOut();
                Response.Redirect("~/Account/SessaoExpirada.aspx");
            }

            //Seta o condominio
            dBoleto.CONDOMINIOSRow rowCON;
            dBol.CONDOMINIOSTableAdapter.Fill(dBol.CONDOMINIOS, Cred.rowUsuario.CON);
            rowCON = dBol.CONDOMINIOS[0];

            //Cedente/Beneficiário com endereço
            CedenteNome = rowCON.CONNome.Trim() + (!rowCON.IsCONCnpjNull() ? " - CNPJ: " + rowCON.CONCnpj : " - CNPJ: 55.055.651/0001-70");
            CedenteEndereco = rowCON.CONEndereco.Trim() + " - " + rowCON.CONBairro.Trim() + (!(rowCON.IsCONUfNull()) ? " " + rowCON.CONUf : "");

            //Nomenclaturas
            if (DadosBanco.BCO == 341)
            {
                NomenclaturaCedente = "Beneficiário";
                NomenclaturaSacado = "Pagador";
            }
        }
    }
}