﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using DevExpress.Web;
using PortalUtil;
using BoletoUtil.Datasets;

namespace Boleto.Pages_Portal
{
    public partial class BoletoAcordoNovo : System.Web.UI.Page
    {
        private dBoleto dBol = new dBoleto();

        private decimal porhonorario;
        public decimal TotHonorarios;
        public decimal TotGeral;
        public decimal TotSubTotal;

        private double indiceinicial = 1;
        private double indicefinal = 1;
        private double inpcmesinicial = 1;
        private double inpcmesfinal = 1;

        private List<int> Selecionados;
        private List<Parcela> Valores;

        private enum Statusparcela
        {
            Paga, Aberto, AbertoTolerancia3, AtrasadoTolerancia30, Atrasado
        }

        private Credencial Cred
        {
            get
            {
                if (Session["CredACO"] == null)
                {
                    //Desloga e vai para a pagina de login
                    FormsAuthentication.SignOut();
                    Response.Redirect("~/Account/SessaoExpirada.aspx");
                }
                return (Credencial)Session["CredACO"];
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia datasets
            dBol = new dBoleto(Cred.EMP);

            if (!IsPostBack)
            {
                //Inicia dados
                IniciaDados();

                //Carrega tela
                Carregar(true);
            }
        }

        private void IniciaDados()
        {
            //Seta porcentagem honorarios e data inicio
            porhonorario = 20;
            DataIni.Text = DateTime.Today.AddMonths(1).ToString("dd/MM/yyyy");
        }

        private void Carregar(bool ComTotais)
        {
            dBol.ACOrdosTableAdapter.Fill(dBol.ACOrdos, Cred.rowUsuario.APT);
            RBDestinatario.Items[0].Enabled = true;
            int? PESInquilino = (int?)dBol.QueriesTableAdapter.PESInquilino(Cred.rowUsuario.APT);
            RBDestinatario.Items[1].Enabled = PESInquilino.HasValue;
            foreach (dBoleto.ACOrdosRow rowACO in dBol.ACOrdos)
            {
                ListItem item = rowACO.ACOProprietario ? RBDestinatario.Items[0] : RBDestinatario.Items[1];
                if (!item.Text.Contains("acordo"))
                    item.Text += " - Já possui acordo";
                item.Enabled = false;
            };

            dBol.BoletosTableAdapter.FillByCriaAcordo(dBol.Boletos, Cred.rowUsuario.APT);

            decimal Totvalor = 0;
            decimal TotvalorCorr = 0;
            decimal TotValMutal = 0;
            decimal TotValJuros = 0;
            if (ComTotais)
            {
                TotSubTotal = 0;
                TotHonorarios = 0;
                TotGeral = 0;
            }
            else
                TotGeral = TotSubTotal + TotHonorarios;


            foreach (dBoleto.BoletosRow row in dBol.Boletos)
            {
                Calcula(row, DateTime.Today);
                row.competencia = string.Format("{0:00}{1:0000}", row.BOLCompetenciaMes, row.BOLCompetenciaAno);
                row.Resp = (row.IsBOLProprietarioNull() || row.BOLProprietario) ? "P" : "I";
                row.pago = !row.IsBOLPagamentoNull();
                row.SubTotal = row.ValorCorrigido + row.ValorMulta + row.ValorJuros;
                row.TotalH = row.SubTotal + row.Honorarios;

                if (!IsPostBack || ((Selecionados != null) && (Selecionados.Contains(row.Numero))))
                {
                    Totvalor += row.valor;
                    TotvalorCorr += row.ValorCorrigido;
                    TotValMutal += row.ValorMulta;
                    TotValJuros += row.ValorJuros;
                    if (ComTotais)
                    {
                        TotSubTotal += row.SubTotal;
                        TotHonorarios += row.Honorarios;
                        TotGeral += row.TotalH;
                    };
                };

            }

            GridView1.DataSource = dBol.Boletos;
            GridView1.Columns[3].FooterText = Totvalor.ToString("n2");
            ViewState["ValorOriginal"] = Totvalor;
            GridView1.Columns[4].FooterText = TotvalorCorr.ToString("n2");
            GridView1.Columns[5].FooterText = TotValMutal.ToString("n2");
            GridView1.Columns[6].FooterText = TotValJuros.ToString("n2");
            GridView1.DataBind();
        }

        private Statusparcela CalculaStatus(dBoleto.BoletosRow row, DateTime DataRef)
        {
            if (row.dtvencto >= DataRef)
                return Statusparcela.Aberto;
            if (row.dtvencto >= (DataRef.AddDays(-3)))
                return Statusparcela.AbertoTolerancia3;
            if (row.dtvencto >= (DataRef.AddDays(-30)))
                return Statusparcela.AtrasadoTolerancia30;
            return Statusparcela.Atrasado;
        }

        private void buscaindices(DateTime dataf, DateTime datai)
        {
            dBol.INPCTableAdapter.Fill(dBol.INPC);
            dBoleto.INPCRow inpcI = dBol.INPC.FindByINPanoINPmes(datai.Year, datai.Month);
            dBoleto.INPCRow inpcf = dBol.INPC.FindByINPanoINPmes(dataf.Year, dataf.Month);
            if (inpcf == null)
                inpcf = dBol.INPC[0];
            inpcmesfinal = inpcf.INPInpc;
            indicefinal = inpcf.INPIndice;
            inpcmesinicial = inpcI.INPInpc;
            indiceinicial = inpcI.INPIndice;
        }

        private void Calcula(dBoleto.BoletosRow row, DateTime data)
        {
            DateTime vencto = row.dtvencto;
            TimeSpan diascorridos = (data - vencto);
            Statusparcela Status = CalculaStatus(row, DateTime.Today);
            decimal valorcorrigidomanobra;
            valorcorrigidomanobra = row.valor;
            if (Status == Statusparcela.Atrasado)
            {
                int diasinicio = vencto.Day;
                int diasfinal = 30 - data.Day;
                buscaindices(DateTime.Today, row.dtvencto);
                valorcorrigidomanobra *= (decimal)((indicefinal / indiceinicial) * inpcmesfinal);
                //Correção dos dias iniciais
                valorcorrigidomanobra /= (decimal)Math.Pow(inpcmesinicial, (diasinicio / 30D));
                //Correção dos dias finais
                valorcorrigidomanobra /= (decimal)Math.Pow(inpcmesfinal, (diasfinal / 30D));

            };

            decimal multamanobra = 0;
            decimal jurosmanobra = 0;
            decimal TxMulta = row.Multa / 100M;
            if ((Status == Statusparcela.AbertoTolerancia3) || (Status == Statusparcela.AtrasadoTolerancia30) || (Status == Statusparcela.Atrasado))
            {
                if (row.Tipo == "M")
                    multamanobra = valorcorrigidomanobra * TxMulta;
                else
                    if (diascorridos.Days > 30)
                    multamanobra = valorcorrigidomanobra * 30 * TxMulta;
                else
                    multamanobra = valorcorrigidomanobra * diascorridos.Days * TxMulta;

                if (diascorridos.Days > 30)
                    jurosmanobra = (valorcorrigidomanobra + multamanobra) * (decimal)(Math.Pow(1.01, (diascorridos.Days / 30D)) - 1);
                else
                    jurosmanobra = 0;
            };

            multamanobra = Math.Round(multamanobra, 2, MidpointRounding.AwayFromZero);
            jurosmanobra = Math.Round(jurosmanobra, 2, MidpointRounding.AwayFromZero);

            decimal valorfinal = Math.Round(valorcorrigidomanobra, 2, MidpointRounding.AwayFromZero) + multamanobra + jurosmanobra;
            decimal valorcorrigido = valorfinal - multamanobra - jurosmanobra;
            decimal honor = Math.Round(valorfinal * porhonorario / 100M, 2, MidpointRounding.AwayFromZero);

            row.ValorCorrigido = valorcorrigido;
            row.ValorMulta = multamanobra;
            row.ValorJuros = jurosmanobra;
            row.Honorarios = honor;
        }

        private void RecuperaCheck()
        {
            Selecionados = new List<int>();
            Valores = new List<Parcela>();
            if (GridView1.Rows.Count > 0)
                foreach (GridViewRow Linhas in GridView1.Rows)
                {
                    CheckBox Somar = (CheckBox)BuscaPorID(Linhas, "CheckBoxIncluir");
                    Label ValorCorrigido = (Label)BuscaPorID(Linhas, "LabelValorCorrigido");
                    Label ValorOriginal = (Label)BuscaPorID(Linhas, "LabelValorOriginal");
                    Label Vencimento = (Label)BuscaPorID(Linhas, "LabelVencimento");
                    Label Competencia = (Label)BuscaPorID(Linhas, "LabelCompetencia");
                    Label Numero = (Label)BuscaPorID(Linhas, "Labelnn");
                    Label NumeroID = (Label)BuscaPorID(Linhas, "IDorig");

                    if (Somar.Checked)
                    {
                        Selecionados.Add(int.Parse(Numero.Text));
                        Valores.Add(new Parcela(Numero.Text, ValorCorrigido.Text, ValorOriginal.Text, Vencimento.Text, Competencia.Text, NumeroID.Text));
                    }
                }
            //decimal.TryParse(((System.Web.UI.WebControls.TextBox)(GridView1.FooterRow.Controls[8].Controls[1])).Text, out TotHonorarios);               
        }

        private void RemarcaCheck()
        {
            if (GridView1.Rows.Count > 0)
                foreach (GridViewRow Linhas in GridView1.Rows)
                {
                    CheckBox Somar = (CheckBox)BuscaPorID(Linhas, "CheckBoxIncluir");
                    Label numero = (Label)BuscaPorID(Linhas, "Labelnn");
                    int nn = int.Parse(numero.Text);
                    Somar.Checked = Selecionados.Contains(nn);
                }
        }

        private Control BuscaPorID(Control Raiz, string ID)
        {
            Control Resposta;
            foreach (Control Cx in Raiz.Controls)
            {
                if (Cx.ID == ID)
                    return Cx;
                else
                {
                    Resposta = BuscaPorID(Cx, ID);
                    if (Resposta != null)
                        return Resposta;
                }
            }
            return null;
        }

        protected void cmdRecalcular_Click(object sender, EventArgs e)
        {
            decimal.TryParse(Honorarios.Text, out porhonorario);
            RecuperaCheck();
            Carregar(true);
            RemarcaCheck();
            cmdCalcularParcelas.Visible = true;
        }

        protected void cmdCalcularParcelas_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.TextBox XTextBox;
            XTextBox = (System.Web.UI.WebControls.TextBox)BuscaPorID(GridView1.FooterRow, "TextTotalH");
            decimal.TryParse(XTextBox.Text, out TotHonorarios);
            XTextBox.Text = TotHonorarios.ToString("n2");
            XTextBox = (System.Web.UI.WebControls.TextBox)BuscaPorID(GridView1.FooterRow, "TextBoxTotalGeral");
            decimal.TryParse(XTextBox.Text, out TotGeral);
            XTextBox.Text = TotGeral.ToString("n2");
            TotSubTotal = TotGeral - TotHonorarios;
            Label LSub = (Label)BuscaPorID(GridView1.FooterRow, "LabelFooSub");
            LSub.Text = TotSubTotal.ToString("n2");

            int NPar;
            int NParh;
            DateTime DataPar;

            Panel1.Visible = false;

            if (int.TryParse(NParcelas.Text, out NPar) && DateTime.TryParse(DataIni.Text, out DataPar) && (int.TryParse(NparH.Text, out NParh)))
            {
                if (NParh > NPar)
                    return;
                decimal ValorParcela = Math.Round(TotGeral / NPar, 2, MidpointRounding.AwayFromZero);
                decimal ValorParcelaH = Math.Round(TotHonorarios / NParh, 2, MidpointRounding.AwayFromZero);
                DataView TabelaNovas = new DataView();
                DataTable TabelaN = TabelaNovas.Table = new DataTable("Novos");
                TabelaN.Columns.Add("Data", typeof(DateTime));
                TabelaN.Columns.Add("ValorParcelas", typeof(decimal));
                TabelaN.Columns.Add("ValorHonorarios", typeof(decimal));
                TabelaN.Columns.Add("ValorTotal", typeof(decimal));
                for (int i = 1; i <= NPar; i++)
                {
                    if (i == NPar)
                        ValorParcela = TotGeral - (NPar - 1) * ValorParcela;
                    if (i == NParh)
                        ValorParcelaH = TotHonorarios - (NParh - 1) * ValorParcelaH;
                    if (i > NParh)
                        ValorParcelaH = 0;
                    TabelaN.Rows.Add(DataPar.AddMonths(i - 1), ValorParcela - ValorParcelaH, ValorParcelaH, ValorParcela);
                    //DataPar = DataPar.AddMonths(1);
                }
                Panel1.Visible = true;
                GridView2.Visible = true;
                GridView2.DataSource = TabelaN;
                GridView2.DataBind();
                Panel2.Visible = false;
            }
        }

        protected void cmdValidar_Click(object sender, EventArgs e)
        {
            DataView TabelaNovas = new DataView();
            DataTable TabelaN = TabelaNovas.Table = new DataTable("Novos");
            TabelaN.Columns.Add("Data", typeof(DateTime));
            TabelaN.Columns.Add("ValorParcelas", typeof(decimal));
            TabelaN.Columns.Add("ValorHonorarios", typeof(decimal));
            TabelaN.Columns.Add("ValorTotal", typeof(decimal));

            bool ok = true;
            DateTime Data;
            DateTime DataAnt;
            decimal Valor;
            decimal Honorario;
            TextBox XTextBox;
            DataAnt = DateTime.MinValue;

            TotHonorarios = 0;
            TotGeral = 0;
            TotSubTotal = 0;
            LabelAviso.Visible = false;

            decimal ValorOriginal = (decimal)ViewState["ValorOriginal"];

            foreach (GridViewRow Linhas in GridView2.Rows)
            {
                XTextBox = (TextBox)BuscaPorID(Linhas, "TextDataParcela");
                if (!DateTime.TryParse(XTextBox.Text, out Data))
                {
                    ok = false;
                    XTextBox.CssClass = "Aviso";
                }
                else
                {
                    if (DataAnt == DateTime.MinValue)
                        DataAnt = Data;
                    else
                    {
                        TimeSpan intervalo = (Data - DataAnt);
                        if (intervalo.Days < 0)
                        {
                            ok = false;
                            XTextBox.CssClass = "Aviso";
                            LabelAviso.Visible = true;
                            LabelAviso.Text = "Ordem das datas errada";
                        }
                        else
                        {
                            XTextBox.CssClass = "";
                            DataAnt = Data;
                        }
                        if (intervalo.Days > 60)
                        {
                            XTextBox.CssClass = "Aviso";
                            LabelAviso.Visible = true;
                            LabelAviso.Text = "Período maior de que 60 dias";
                        }
                    };
                }
                XTextBox = (TextBox)BuscaPorID(Linhas, "TextValorParcela");
                if (!decimal.TryParse(XTextBox.Text, out Valor))
                {
                    ok = false;
                    XTextBox.CssClass = "Aviso";
                }
                else
                    XTextBox.CssClass = "";
                TotGeral += Valor;
                XTextBox = (TextBox)BuscaPorID(Linhas, "TextValorHonorario");
                if (!decimal.TryParse(XTextBox.Text, out Honorario))
                {
                    ok = false;
                    XTextBox.CssClass = "Aviso";
                }
                else
                {
                    if (Honorario > Valor)
                    {
                        ok = false;
                        XTextBox.CssClass = "Aviso";
                        LabelAviso.Visible = true;
                        LabelAviso.Text = "Valor de honorários maior do que a parcela";
                    }
                    else
                        XTextBox.CssClass = "";
                }
                TotHonorarios += Honorario;
                TotSubTotal = TotGeral - TotHonorarios;
                TabelaN.Rows.Add(Data, Valor - Honorario, Honorario, Valor);
            }

            if (TotSubTotal < ValorOriginal)
            {
                ok = false;
                LabelAviso.Visible = true;
                LabelAviso.Text = string.Format("Valor das parcelas ({0:n2}) menor do que o valor original ({1:n2})", TotSubTotal, ValorOriginal);
            }

            if (ok)
            {
                Panel1.Visible = false;
                Panel2.Visible = true;
                GridView3.Columns[1].FooterText = TotSubTotal.ToString("n2");
                GridView3.Columns[2].FooterText = TotHonorarios.ToString("n2");
                GridView3.Columns[3].FooterText = TotGeral.ToString("n2");
                GridView3.DataSource = TabelaN;
                GridView3.DataBind();                
            }
        }

        protected void cmdCorrigir_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
        }

        protected void cmdGerarAcordo_Click(object sender, EventArgs e)
        {
            RecuperaCheck();
            using (dAcordoRet dRetorno = new dAcordoRet())
            {
                VirMSSQL.TableAdapter TATrans = new VirMSSQL.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.SQL, true);
                try
                {
                    TATrans.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN1"].ConnectionString;
                    TATrans.AbreTrasacaoLocal("ProcessoAntigo", dRetorno.RetAcordoTableAdapter, dRetorno.RetOriginaisTableAdapter, dRetorno.RetNovosBTableAdapter, dRetorno.BOLETOSTableAdapter);
                    // Criar acordo na tabela ret
                    dAcordoRet.RetAcordoRow rowAcordo = dRetorno.RetAcordo.NewRetAcordoRow();
                    rowAcordo.apartamento = Cred.rowUsuario.APTNumero;
                    rowAcordo.bloco = Cred.rowUsuario.BLOCodigo;
                    rowAcordo.codcon = Cred.rowUsuario.CONCodigo;
                    rowAcordo.data = DateTime.Now;
                    rowAcordo.DESTINATARIO = RBDestinatario.Text;
                    rowAcordo.Efetuado = false;
                    rowAcordo.filial = Cred.EMP == 3 ? (short)2 : (short)Cred.EMP;
                    rowAcordo.Judicial = (RBTipo.Text == "1");
                    rowAcordo.Juridico = true;
                    rowAcordo.FRN = ((Credencial)Session["Cred"]).rowUsuario.USR;
                    rowAcordo.MenJuridico = txtMenJuridico.Text;
                    dRetorno.RetAcordo.AddRetAcordoRow(rowAcordo);
                    dRetorno.RetAcordoTableAdapter.Update(rowAcordo);
                    rowAcordo.AcceptChanges();
                    foreach (Parcela P in Valores)
                    {
                        dAcordoRet.RetOriginaisRow rowOrig = dRetorno.RetOriginais.NewRetOriginaisRow();
                        rowOrig.RetAcordoRow = rowAcordo;
                        rowOrig.numero = P.numero;
                        rowOrig.total = P.ValorTotal;
                        dRetorno.RetOriginais.AddRetOriginaisRow(rowOrig);
                        dRetorno.RetOriginaisTableAdapter.Update(rowOrig);
                        rowOrig.AcceptChanges();
                    }
                    //Criando Novos                  
                    foreach (GridViewRow Linhas in GridView2.Rows)
                    {
                        TextBox XTextBox = (TextBox)BuscaPorID(Linhas, "TextDataParcela");
                        DateTime Data = DateTime.Parse(XTextBox.Text);
                        XTextBox = (TextBox)BuscaPorID(Linhas, "TextValorParcela");
                        decimal Valor = decimal.Parse(XTextBox.Text);
                        XTextBox = (TextBox)BuscaPorID(Linhas, "TextValorHonorario");
                        decimal Honorarios = decimal.Parse(XTextBox.Text);
                        dAcordoRet.BOLETOSRow rowBoletos = dRetorno.BOLETOS.NewBOLETOSRow();
                        rowBoletos.apartamento = Cred.rowUsuario.APTNumero;
                        rowBoletos.bloco = Cred.rowUsuario.BLOCodigo;
                        rowBoletos.CODCON = Cred.rowUsuario.CONCodigo;
                        rowBoletos.vencto = Data;
                        dRetorno.BOLETOS.AddBOLETOSRow(rowBoletos);
                        dRetorno.BOLETOSTableAdapter.Update(rowBoletos);
                        rowBoletos.AcceptChanges();
                        dAcordoRet.RetNovosBRow rowNovos = dRetorno.RetNovosB.NewRetNovosBRow();
                        rowNovos.RetAcordoRow = rowAcordo;
                        rowNovos.Honorarios = Honorarios;
                        rowNovos.valor = Valor;
                        rowNovos.vencto = Data;
                        rowNovos.nn = rowBoletos.NN;
                        dRetorno.RetNovosB.AddRetNovosBRow(rowNovos);
                        dRetorno.RetNovosBTableAdapter.Update(rowNovos);
                        rowNovos.AcceptChanges();
                    };
                    TATrans.Commit();
                    Panel1.Visible = false;
                    Panel2.Visible = false;
                    cmdRecalcular.Visible = false;
                    cmdCalcularParcelas.Visible = false;
                }
                catch (Exception ex)
                {
                    TATrans.Vircatch(ex);
                    throw ex;
                }
            }

            //Volta para tela de boletos
            Response.Redirect(string.Format("~/Pages_Portal/Boleto.aspx?CredAPT={0}", Cred.rowUsuario.APT));
        }
    }

    class Parcela : Object
    {
        public int numero;
        public int NumeroID;
        public decimal ValorOriginal;
        public decimal ValorTotal;
        public DateTime Vencto;
        public string Competencia;

        public Parcela(string numero, string Total, string Original, string Vencto, string Competencia, string NumeroID)
        {
            this.numero = int.Parse(numero);
            this.ValorTotal = decimal.Parse(Total);
            this.ValorOriginal = decimal.Parse(Original);
            this.Vencto = DateTime.Parse(Vencto);
            this.Competencia = Competencia;
            this.NumeroID = int.Parse(NumeroID);
        }
        public override string ToString()
        {
            return numero.ToString();
        }
    }
}