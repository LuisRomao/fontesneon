﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using DevExpress.Web;
using System.Collections;
using PortalUtil;

namespace Boleto.Pages_Portal
{
    public partial class BoletoSelecaoData : System.Web.UI.Page
    {
        private SortedList Alternativas;

        private Credencial Cred
        {
            get
            {
                if (Session["CredBOL"] == null)
                {
                    //Desloga e vai para a pagina de login
                    FormsAuthentication.SignOut();
                    Response.Redirect("~/Account/SessaoExpirada.aspx");
                }
                return (Credencial)Session["CredBOL"];
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Pega datas para selecao enviadas por sessao
            Alternativas = (SortedList)Session["BoletoDatas"];

            if (!IsPostBack)
            {
                //Exibe datas para selecao
                foreach (DictionaryEntry DE in Alternativas)
                    rblBoletoDatas.Items.Add(string.Format("Até {0:dd/MM/yyyy} - Valor = R$ {1:n2}", DE.Key, DE.Value));
            }
        }

        protected void rblSelecaoData_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime DataSel = (DateTime)Alternativas.GetKey(rblBoletoDatas.SelectedIndex);
            Session["BoletoPara"] = DataSel;
            Response.Redirect("~/Pages_Portal/BoletoImpressao.aspx");
        }
    }
}