﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using PortalUtil;
using BoletoUtil.Datasets;

namespace Boleto.Pages_Portal
{
    public partial class BoletoDebitoAutomaticoImpressao : System.Web.UI.Page
    {
        private dBoleto dBol = new dBoleto();
        private dBoleto.CONDOMINIOSRow rowCON;

        private Credencial Cred
        {
            get
            {
                if (Session["CredDEB"] == null)
                {
                    //Desloga e vai para a pagina de login
                    FormsAuthentication.SignOut();
                    Response.Redirect("~/Account/SessaoExpirada.aspx");
                }
                return (Credencial)Session["CredDEB"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia datasets
            dBol = new dBoleto(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Pega o condominio           
            dBol.CONDOMINIOSTableAdapter.Fill(dBol.CONDOMINIOS, Cred.CON);
            rowCON = dBol.CONDOMINIOS[0];

            //Preenche campos da conta
            txtNumeroBanco.Text = "<b>Número do Banco: </b>237 - BRADESCO";
            txtNumeroAgencia.Text = "<b>Número da Agência: </b>" + Session["Agencia"].ToString().PadLeft(4, '0');
            txtDigitoAgencia.Text = "<b>Dígito da Agência: </b>" + Session["AgenciaDG"].ToString();
            txtNumeroConta.Text = "<b>Número da Conta: </b>" + Session["Conta"].ToString();
            txtDigitoConta.Text = "<b>Dígito da Conta: </b>" + Session["ContaDG"].ToString();

            //Preenche campos da declaracao
            txtTitularConta.Text = "<b>Titular da Conta: </b>" + Cred.rowUsuario.Nome + " - " + (Cred.rowUsuario.CpfCnpj.Replace(".", "").Replace("-", "").Length == 11 ? "CPF " : "CNPJ ") + Cred.rowUsuario.CpfCnpj;
            txtAssNomeTitular.Text = Cred.rowUsuario.Nome;
            txtAssCPFTitular.Text = (Cred.rowUsuario.CpfCnpj.Replace(".", "").Replace("-", "").Length == 11 ? "CPF " : "CNPJ ") + Cred.rowUsuario.CpfCnpj;
            txtNomeCondominio.Text = "<b>Nome do Condomínio: </b>" + Cred.rowUsuario.CONNome;
            txtEnderecoCondominio.Text = "<b>Endereço: </b>" + rowCON.CONEndereco.Trim() + " - " + rowCON.CONBairro.Trim();
            txtUnidade.Text = "<b>Unidade: </b>" + Cred.rowUsuario.APTNumero + (Cred.rowUsuario.BLOCodigo == "SB" ? "" : " - Bloco " + Cred.rowUsuario.BLOCodigo);
            txtPropInq.Text = "<b>Proprietário ou Inquilino: </b>" + (Cred.rowUsuario.Proprietario ? "Proprietário" : "Inquilino");

            //Preenche local e data
            lblLocalData.Text = Session["LocalData"].ToString();
        }
    }
}