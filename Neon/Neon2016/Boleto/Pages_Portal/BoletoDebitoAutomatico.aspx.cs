﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;
using System.Configuration;
using System.Web.Security;
using DevExpress.Web;
using PortalUtil;
using BoletoUtil.Datasets;
using CompontesBasicosProc; 

namespace Boleto.Pages_Portal
{
    public partial class BoletoDebitoAutomatico : System.Web.UI.Page
    {
        private dBoleto dBol = new dBoleto();
        private dBoleto.CONDOMINIOSRow rowCON;

        private string[] arrMeses = new string[13] { "", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };

        private Credencial Cred
        {
            get
            {
                if (Session["CredDEB"] == null)
                {
                    //Desloga e vai para a pagina de login
                    FormsAuthentication.SignOut();
                    Response.Redirect("~/Account/SessaoExpirada.aspx");
                }
                return (Credencial)Session["CredDEB"];
            }
        }

        private string AnexoMS = "AnexoADA";
        private string NomeAnexo = "AnexoADA_0";

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia datasets
            dBol = new dBoleto(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Pega o condominio           
            dBol.CONDOMINIOSTableAdapter.Fill(dBol.CONDOMINIOS, Cred.CON);
            rowCON = dBol.CONDOMINIOS[0];

            //Preenche campos da declaracao
            txtNomeTitular.Text = Cred.rowUsuario.Nome;
            txtCPFTitular.Text = (Cred.rowUsuario.CpfCnpj.Replace(".","").Replace("-", "").Length == 11 ? "CPF " : "CNPJ ") + Cred.rowUsuario.CpfCnpj;
            txtNomeCondominio.Text = Cred.rowUsuario.CONNome;
            txtEnderecoCondominio.Text = rowCON.CONEndereco.Trim() + " - " + rowCON.CONBairro.Trim();
            txtUnidade.Text = Cred.rowUsuario.APTNumero + (Cred.rowUsuario.BLOCodigo == "SB" ? "" : " - Bloco " + Cred.rowUsuario.BLOCodigo);
            txtPropInq.Text = Cred.rowUsuario.Proprietario ? "Proprietário" : "Inquilino";

            //Preenche local e data (se ja existe variavel de sessao indica retorno de impressao e pega os valores ja existentes)
            if (Session["LocalData"] == null)
            {
                //Seta novo local e data
                DateTime datData = DateTime.Now;
                lblLocalData.Text = String.Format("{0}, {1} de {2} de {3}", (Cred.EMP == 1 ? "São Bernardo do Campo" : "Santo André"), datData.Day, arrMeses[datData.Month], datData.Year);
            }
            else
            {
                //Preenche campos da conta ja digitados
                txtNumeroAgencia.Text = Session["Agencia"].ToString().PadLeft(4, '0');
                txtDigitoAgencia.Text = Session["AgenciaDG"].ToString();
                txtNumeroConta.Text = Session["Conta"].ToString();
                txtDigitoConta.Text = Session["ContaDG"].ToString();

                //Preenche local e data ja setada
                lblLocalData.Text = Session["LocalData"].ToString();
            }

            //Limpa variaval sessao para controle de retorno de impressao
            Session["LocalData"] = null;
        }

        protected void cmdImprimir_Click(object sender, EventArgs e)
        {
            //Limpa campos de msg
            lblMsgImp.Text = "";
            panMsgImp.Visible = false;

            //Verifica consistencia de agencia e conta
            if (txtNumeroAgencia.Text.Trim() == "")
            { 
                lblMsgImp.Text = "Informe o número da agência";
                panMsgImp.Visible = true;
                return;
            }
            if (Convert.ToInt32(txtNumeroAgencia.Text) == 0)
            {
                lblMsgImp.Text = "O número da agência não pode ser 0. Informe um número válido.";
                panMsgImp.Visible = true;
                return;
            }
            if (txtNumeroConta.Text.Trim() == "")
            {
                lblMsgImp.Text = "Informe o número da conta";
                panMsgImp.Visible = true;
                return;
            }
            if (Convert.ToInt32(txtNumeroConta.Text) == 0)
            {
                lblMsgImp.Text = "O número da conta não pode ser 0. Informe um número válido";
                panMsgImp.Visible = true;
                return;
            }
            if (txtDigitoConta.Text.Trim() == "")
            {
                lblMsgImp.Text = "Informe o dígito da conta";
                panMsgImp.Visible = true;
                return;
            }

            //Valida agencia e conta (via componente)
            ContaCorrente CC = new ContaCorrente();
            CC.Tipo = ContaCorrente.TipoConta.CC; 
            CC.BCO = Convert.ToInt32(txtNumeroBanco.Text.Substring(0, 3));
            if (Convert.ToInt32(txtNumeroBanco.Text.Substring(0, 3)) == 237 && txtDigitoAgencia.Text == "")
            { 
                CC.Agencia = Convert.ToInt32(txtNumeroAgencia.Text.Substring(0, txtNumeroAgencia.Text.Length - 1));
                CC.AgenciaDg = Convert.ToInt32(txtNumeroAgencia.Text.Substring(txtNumeroAgencia.Text.Length - 1, 1));
            }
            else
            {
                CC.Agencia = Convert.ToInt32(txtNumeroAgencia.Text);
                CC.AgenciaDg = txtDigitoAgencia.Text == "" ? 0 : Convert.ToInt32(txtDigitoAgencia.Text);
            }
            CC.NumeroConta = Convert.ToInt32(txtNumeroConta.Text); 
            CC.NumeroDg = txtDigitoConta.Text;
            if (!CC.validaAgencia() || !CC.validaConta())
            {
                lblMsgImp.Text = "Número de Agência ou Conta inválido. Por favor, confirme todos os números digitados.";
                panMsgImp.Visible = true;
                return;
            }

            //Seta Cred do DEB
            Session["CredDEB"] = Cred;

            //Imprime a declaracao
            Session["Agencia"] = txtNumeroAgencia.Text;
            Session["AgenciaDG"] = txtDigitoAgencia.Text;
            Session["Conta"] = txtNumeroConta.Text;
            Session["ContaDG"] = txtDigitoConta.Text;
            Session["LocalData"] = lblLocalData.Text;
            Response.Redirect("~/Pages_Portal/BoletoDebitoAutomaticoImpressao.aspx");
        }

        protected void uplAnexo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            //Inicia upload de anexo
            ASPxUploadControl upload = sender as ASPxUploadControl;
            Session[AnexoMS] = null;

            //Pega o anexos selecionado e salva na sessao
            UploadedFile arquivo = upload.UploadedFiles[0];
            if (arquivo.FileName != "")
            {
                Session[NomeAnexo] = arquivo.FileName;
                Session[AnexoMS] = new MemoryStream(arquivo.FileBytes);
            }
        }

        protected void cbpEmail_Callback(object sender, CallbackEventArgsBase e)
        {
            //Envia o email
            lblMsg.Text = Enviar();

            //Limpa anexo
            Session[AnexoMS] = null;
        }

        protected void cbpEmail_CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpMsg"] = lblMsg.Text;
        }

        protected string Enviar()
        {
            //Limpa campos de msg
            lblMsgImp.Text = "";
            panMsgImp.Visible = false;

            //Verifica se indicou o arquivo
            if (Session[AnexoMS] == null)
                return "Selecione o arquivo com a autorização para débito automático assinada, para o envio por e-mail.";

            try
            {
                //Cria SMTP
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    email.To.Add(ConfigurationManager.AppSettings["EnvioEmailAutorizacaoDebitoAutomatico" + Cred.EMP.ToString()].Replace(";", ","));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Autorização para Débito Automático em Conta Corrente";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>AUTORIZAÇÃO PARA DÉBITO AUTOMÁTICO EM CONTA CORRENTE</b></p>\r\n" +
                                               "      <p><b>Data:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               ((Cred.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : "      <p><b>Bloco:</b> {2}</p>\r\n") +
                                               "      <p><b>Unidade:</b> {3}</p>\r\n" +
                                               "      <p><b>Nome:</b> {4}</p>\r\n" +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo),
                                               Server.HtmlEncode(Cred.rowUsuario.BLOCodigo), Server.HtmlEncode(Cred.rowUsuario.APTNumero), Server.HtmlEncode(Cred.rowUsuario.Nome));

                    //Inclui Anexos
                    MemoryStream ms = (MemoryStream)Session[AnexoMS];
                    email.Attachments.Add(new System.Net.Mail.Attachment(ms, Session[NomeAnexo].ToString()));

                    //Envia email
                    SmtpClient1.Send(email);
                    Thread.Sleep(1000);

                    //Sucesso no envio
                    return "Sua autorização para Débito Automático em Conta Corrente foi enviada para a Neon com sucesso!";
                }
            }
            catch (Exception)
            {
                //Erro no envio
                return "Ocorreu um erro no envio da sua autorização para Débito Automático em Conta Corrente. Por favor, tente novamente.";
            }
        }
    }
}