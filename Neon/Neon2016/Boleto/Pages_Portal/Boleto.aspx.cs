﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using DevExpress.Web;
using PortalUtil;
using BoletoUtil;
using BoletoUtil.Datasets;

namespace Boleto.Pages_Portal
{
    public partial class Boleto : System.Web.UI.Page
    {
        private dBoleto dBol = new dBoleto();

        private dBoleto.CONDOMINIOSRow rowCON;
        private dBoleto.AParTamentoRow rowAPT;
        private VirBoleto CurBoleto;
        DataRow rowBOLAPT = null;

        private decimal decTotalVencido;
        private decimal decTotalVencidoCorrigido;
        private decimal decTotalMulta;
        private decimal decTotalJuros;
        private decimal decTotalFinal;
        private decimal decTotalAguardo;
        private decimal decTotalMultaAguardo;
        private decimal decTotalFinalAguardo;
        private decimal decTotalAVencer;
        private int intBoletosVencidosPagaveis;

        private bool booPermiteAcordo;
        public bool PermiteAcordo
        {
            get
            {
                if (!booPermiteAcordo)
                    return false;
                if (intBoletosVencidosPagaveis > 1)
                    return true;
                if ((intBoletosVencidosPagaveis == 1) && (rowCON.CON_BCO == 341 || rowCON.CON_BCO == 104) && (rowCON.CONBoletoComRegistro))
                    return true;
                else
                    return false;
            }
        }

        private string strNadaConsta = "";
        private bool booNoJuridico;
        private bool booOcultarBoletosJuridico = false;

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }
        private Credencial CredAPT;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Pega Cred do APT (caso tenha sido direcinado da lista de condominos pega o setado na sessao)
            CredAPT = (Request.QueryString["CredAPT"] == null) ? Cred : (Credencial)Session["Cred" + Request.QueryString["CredAPT"]];

            //Inicia datasets
            dBol = new dBoleto(CredAPT.EMP);

            if (!IsPostBack)
            {
                //Inicia dados
                IniciaDados();

                //Carrega tela
                Carregar();
            }
            else
                //Pega dataset na sessao (para usar no postback dos botoes)
                dBol = (dBoleto)Session["dBol"];
        }

        private void IniciaDados()
        {
            //Seta o condominio
            dBol.CONDOMINIOSTableAdapter.Fill(dBol.CONDOMINIOS, CredAPT.CON);
            rowCON = dBol.CONDOMINIOS[0];

            //Seta dados do APT
            dBol.ACOrdosTableAdapter.Fill(dBol.ACOrdos, CredAPT.rowUsuario.APT);
            dBol.OriginalTableAdapter.Fill(dBol.Original, CredAPT.rowUsuario.APT);
            dBol.NovosTableAdapter.Fill(dBol.Novos, CredAPT.rowUsuario.APT);
            dBol.AParTamentoTableAdapter.Fill(dBol.AParTamento, CredAPT.rowUsuario.APT);
            rowAPT = dBol.AParTamento[0];
            booNoJuridico = (rowAPT.APTStatusCobranca == -1);
            if (booNoJuridico) strNadaConsta = "Unidade no departamento jurídico";

            //Seta labels e status
            lblCondomino.Text = string.Format("{0} - {1}{2} - {3}",
                                CredAPT.rowUsuario.Nome, CredAPT.rowUsuario.CONNome,
                                (CredAPT.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : string.Format(" - {0}", CredAPT.rowUsuario.BLOCodigo),
                                CredAPT.rowUsuario.APTNumero);
            floStatus.FindItemOrGroupByName("loiAcordoBr").Visible = floStatus.FindItemOrGroupByName("loiAcordo").Visible = false;
            floStatus.FindItemOrGroupByName("loiJuridicoBr").Visible = floStatus.FindItemOrGroupByName("loiJuridico").Visible = booNoJuridico;

            //Seta comandos (do usuario logado)
            if (Cred.TUsuario == Credencial.TipoUsuario.AdvogadoAdministradora)
            {
                floComandos.FindItemOrGroupByName("loiVerificarAcordo").Visible = floComandos.FindItemOrGroupByName("loiGerarAcordo").Visible = true;
                floComandos.FindItemOrGroupByName("loiDeclaracaoNadaConsta").Visible = floComandos.FindItemOrGroupByName("loiSolicitacaoDebitoAutomatico").Visible = false;
            }
            else
            {
                floComandos.FindItemOrGroupByName("loiVerificarAcordo").Visible = floComandos.FindItemOrGroupByName("loiGerarAcordo").Visible = false;
                floComandos.FindItemOrGroupByName("loiDeclaracaoNadaConsta").Visible = true;
                floComandos.FindItemOrGroupByName("loiSolicitacaoDebitoAutomatico").Visible = Convert.ToBoolean(rowCON.CONDebitoAutomatico);
            }

            //Salva dataset na sessao (para usar no postback dos botoes)
            Session["dBol"] = dBol;
        }

        private void Carregar()
        {
            //Inicia campos
            DateTime datCalculo = DateTime.Today;
            lblDataCalculo.Text = datCalculo.ToString("dd/MM/yyyy");

            //Verifica se deve ocultar boletos do juridico
            if ((Cred.TUsuario != Credencial.TipoUsuario.AdvogadoAdministradora) && (CredAPT.rowUsuario.CONCodigo.ToUpper() == "GRENOB") && (CredAPT.rowUsuario.APTNumero == "0061")) //Exibe apenas os boletos do último mês para o Apt 61 do GRENOB (já estava assim no site antigo)
            { 
                booOcultarBoletosJuridico = true;
                lblMsgCobrancaJuridico.Text = lblMsgCobrancaJuridico.Text + "\r\n * Listados apenas os boletos em aberto ou vencidos a menos de 30 dias. Para lista completa contatar o jurídico.";
            }
            else
                booOcultarBoletosJuridico = false;

            //Carrega os boletos
            if (!booOcultarBoletosJuridico)
                dBol.BoletosTableAdapter.Fill(dBol.Boletos, DateTime.Today.AddMonths(-6), CredAPT.rowUsuario.APT);
            else
                dBol.BoletosTableAdapter.FillByPagaveis(dBol.Boletos, DateTime.Today.AddMonths(-6), CredAPT.rowUsuario.APT, datCalculo.AddMonths(-1));

            //Faz correcao de provisorios
            CorrecaoProvisorios();

            //Carrega acordo (boletos e parcelas) caso exista
            booPermiteAcordo = !booNoJuridico;
            if (!rowCON.IsCONPermiteAcordoInternetNull())
                if (!rowCON.CONPermiteAcordoInternet)
                    booPermiteAcordo = false;
            if (dBol.ACOrdos.Count != 0)
            {
                booPermiteAcordo = false;
                strNadaConsta = "Unidade com parcelamento";
                floStatus.FindItemOrGroupByName("loiAcordoBr").Visible = floStatus.FindItemOrGroupByName("loiAcordo").Visible = true;
                grdAcordoBoletos.DataSource = dBol.Original;
                grdAcordoBoletos.DataBind();
                grdAcordoParcelas.DataSource = dBol.Novos;
                grdAcordoParcelas.DataBind();
            }

            //Inicia boleto
            CurBoleto = new VirBoleto(dBol, true);
            if ((CredAPT.CON == 732) && (CredAPT.EMP == 1))
                CurBoleto.jurosCompostos = false;

            //Calcula valores, totais e insere na tabela de boletos do apt
            dBol.BoletosAPT.Clear();
            decTotalVencido = decTotalVencidoCorrigido = decTotalMulta = decTotalJuros = decTotalFinal = 0;
            decTotalAVencer = decTotalAguardo = decTotalMultaAguardo = decTotalFinalAguardo = intBoletosVencidosPagaveis = 0;
            foreach (dBoleto.BoletosRow rowBOL in dBol.Boletos)
            {
                //Verifica pagamento e competencia
                rowBOL.pago = !rowBOL.IsBOLPagamentoNull();
                rowBOL.competencia = string.Format("{0:00}{1:0000}", rowBOL.BOLCompetenciaMes, rowBOL.BOLCompetenciaAno);

                //Verifica protesto
                bool booProtesto = false;
                if (rowBOL.BOLProtestado)
                {
                    booProtesto = true;
                    booPermiteAcordo = false;
                    strNadaConsta = "Unidade com boleto protestado";
                }

                //Pega o boleto e calcula valores
                CurBoleto.CarregaDados(rowBOL);
                CurBoleto.Calcula(datCalculo, false);

                //Verifica situacao (e cor da situacao) do boleto 
                string strSituacao = "";
                string strSituacaoCor = "Black";
                switch (CurBoleto.stparcela)
                {
                    case Statusparcela.Stparok:
                        strSituacao = "Pago";
                        strSituacaoCor = "#000000";
                        break;
                    case Statusparcela.Stparaberto:
                        strSituacao = "A Vencer";
                        strSituacaoCor = "#000000";
                        break;
                    case Statusparcela.Stparabertotolerancia:
                        strSituacao = "Aguardo";
                        strSituacaoCor = "#000000";
                        break;
                    case Statusparcela.Stparatrasadodentro:
                    case Statusparcela.Stparatrasadofora:
                        strSituacao = "Vencido";
                        strSituacaoCor = "#ff0000";
                        break;
                    default:
                        break;
                };

                //Verifica impressao e boletos vencidos pagaveis
                bool booImprimir = false;
                if (rowBOL.pago || booProtesto)
                    booImprimir = false;
                else
                {
                    if (booNoJuridico && (CurBoleto.stparcela == Statusparcela.Stparatrasadofora))
                        booImprimir = false;
                    else
                    {
                        if (!TravaRegistrado30(rowBOL))
                        {
                            booImprimir = true;
                            if (CurBoleto.stparcela != Statusparcela.Stparaberto)
                                intBoletosVencidosPagaveis++;
                        }
                        else
                        {
                            booImprimir = false;
                            intBoletosVencidosPagaveis++;
                        }
                    }
                }

                //Adiciona na lista
                rowBOLAPT = dBol.BoletosAPT.NewRow();
                rowBOLAPT["BOL"] = rowBOL.BOL;
                rowBOLAPT["BLOCodigo"] = rowBOL.BLOCodigo;
                rowBOLAPT["APTNumero"] = rowBOL.APTNumero;
                rowBOLAPT["BOLTipoUsuario"] = rowBOL.BOLTipoUsuario;
                rowBOLAPT["BOLVencto"] = rowBOL.dtvencto.ToString("dd/MM/yyyy");
                rowBOLAPT["BOLOriginal"] = CurBoleto.valororiginal;
                rowBOLAPT["BOLCorrigido"] = CurBoleto.ValorCorrigidostr == "" ? 0 : Convert.ToDecimal(CurBoleto.ValorCorrigidostr);
                rowBOLAPT["BOLMulta"] = CurBoleto.multa;
                rowBOLAPT["BOLJuros"] = CurBoleto.juros;
                rowBOLAPT["BOLTotal"] = CurBoleto.valorfinal;
                rowBOLAPT["BOLSituacao"] = strSituacao;
                rowBOLAPT["BOLSituacaoCor"] = strSituacaoCor;
                rowBOLAPT["BOLTipo"] = rowBOL.tipoboleto;
                rowBOLAPT["BOLImprimir"] = booImprimir;
                dBol.BoletosAPT.Rows.Add(rowBOLAPT);

                //Soma nos totais dependendo do status da parcela
                switch (CurBoleto.stparcela)
                {
                    default:
                    case Statusparcela.Stparok:
                        break;
                    case Statusparcela.Stparaberto:
                        decTotalAVencer += CurBoleto.valororiginal;
                        break;
                    case Statusparcela.Stparabertotolerancia:
                        strNadaConsta = "Existem boletos em aberto.";
                        decTotalAguardo += CurBoleto.valororiginal;
                        decTotalMultaAguardo += rowBOL.ValorMulta;
                        decTotalFinalAguardo += rowBOL.ValorFinal;
                        break;
                    case Statusparcela.Stparatrasadodentro:
                        strNadaConsta = "Existem boletos em aberto.";
                        decTotalVencido += CurBoleto.valororiginal;
                        decTotalVencidoCorrigido += rowBOL.ValorCorrigido;
                        decTotalMulta += rowBOL.ValorMulta;
                        decTotalFinal += rowBOL.ValorFinal;
                        break;
                    case Statusparcela.Stparatrasadofora:
                        strNadaConsta = "Existem boletos em aberto.";
                        decTotalVencido += CurBoleto.valororiginal;
                        decTotalVencidoCorrigido += rowBOL.ValorCorrigido;
                        decTotalMulta += rowBOL.ValorMulta;
                        decTotalFinal += rowBOL.ValorFinal;
                        break;
                }
            }

            //Indica totais se existir
            if (decTotalVencido > 0 || decTotalAguardo > 0 || decTotalAVencer > 0)
            {
                rowBOLAPT = dBol.BoletosAPT.NewRow();
                rowBOLAPT["BOLVencto"] = "Total";
                dBol.BoletosAPT.Rows.Add(rowBOLAPT);
            }
            
            //Insere Total Vencido (se maior que zero)
            if (decTotalVencido > 0)
            {
                rowBOLAPT = dBol.BoletosAPT.NewRow();
                rowBOLAPT["BOLVencto"] = "Vencido";
                rowBOLAPT["BOLOriginal"] = decTotalVencido;
                rowBOLAPT["BOLCorrigido"] = decTotalVencidoCorrigido;
                rowBOLAPT["BOLMulta"] = decTotalMulta;
                rowBOLAPT["BOLJuros"] = decTotalJuros;
                rowBOLAPT["BOLTotal"] = decTotalFinal;
                rowBOLAPT["BOLParcelar"] = PermiteAcordo;
                dBol.BoletosAPT.Rows.Add(rowBOLAPT);
            }

            //Insere Total Aguardo (se maior que zero) = tolerancia
            if (decTotalAguardo > 0)
            {
                rowBOLAPT = dBol.BoletosAPT.NewRow();
                rowBOLAPT["BOLVencto"] = "Aguardo";
                rowBOLAPT["BOLOriginal"] = decTotalAguardo;
                rowBOLAPT["BOLMulta"] = decTotalMultaAguardo;
                rowBOLAPT["BOLTotal"] = decTotalFinalAguardo;
                dBol.BoletosAPT.Rows.Add(rowBOLAPT);
            }

            //Insere Total A Vencer (se maior que zero)
            if (decTotalAVencer > 0)
            {
                rowBOLAPT = dBol.BoletosAPT.NewRow();
                rowBOLAPT["BOLVencto"] = "A Vencer";
                rowBOLAPT["BOLOriginal"] = decTotalAVencer;
                rowBOLAPT["BOLTotal"] = decTotalAVencer;
                dBol.BoletosAPT.Rows.Add(rowBOLAPT);
            }

            //Exibe na grid
            grdBoletos.DataSource = dBol.BoletosAPT;
            grdBoletos.DataBind();

            //Verifica se tem acordo pra visualizar
            if (floComandos.FindItemOrGroupByName("loiVerificarAcordo").Visible)
                cmdVerificarAcordo.Enabled = floStatus.FindItemOrGroupByName("loiAcordo").Visible;

            //Verifica se esta em dia
            if (decTotalVencido == 0 && decTotalAguardo == 0 && decTotalAVencer == 0)
                cmdGerarAcordo.Enabled = false;

            //Verifica se habilita declaracao de nada costa
            cmdDeclaracaoNadaConsta.Enabled = (strNadaConsta == "");

            //Verifica se solicitou declaracao de nada consta
            if (Request.QueryString["DNC"] != null)
                if (cmdDeclaracaoNadaConsta.Enabled)
                    cmdDeclaracaoNadaConsta_Click(null, null);
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", "alert(\"Não é possível solicitar a Declaração Nada Consta pois existem restrições para esta unidade!\");", true);
        }

        private bool TravaRegistrado30(dBoleto.BoletosRow rowBOL)
        {
            return (rowCON.CON_BCO == 341 || rowCON.CON_BCO == 104) && (rowCON.CONBoletoComRegistro) && (rowBOL.dtvencto.AddMonths(1) < DateTime.Today);
        }

        private void CorrecaoProvisorios()
        {
            dAcordoRet dAcordoRet1 = new dAcordoRet();
            short EMPAntigo = CredAPT.EMP == 1 ? (short)1 : (short)2;
            if (dAcordoRet1.RetAcordoTableAdapter.FillByAPT(dAcordoRet1.RetAcordo, CredAPT.rowUsuario.CONCodigo, CredAPT.rowUsuario.APTNumero, CredAPT.rowUsuario.BLOCodigo, EMPAntigo) > 0)
                floComandos.Enabled = false;
            dAcordoRet1.RetNovosBTableAdapter.FillByAPT(dAcordoRet1.RetNovosB, CredAPT.rowUsuario.CONCodigo, CredAPT.rowUsuario.APTNumero, CredAPT.rowUsuario.BLOCodigo, EMPAntigo);
            dAcordoRet1.RetOriginaisTableAdapter.FillByAPT(dAcordoRet1.RetOriginais, CredAPT.rowUsuario.CONCodigo, CredAPT.rowUsuario.APTNumero, CredAPT.rowUsuario.BLOCodigo, EMPAntigo);
            foreach (dAcordoRet.RetAcordoRow rowA in dAcordoRet1.RetAcordo)
            {
                dBoleto.ACOrdosRow rowNova = dBol.ACOrdos.NewACOrdosRow();
                rowNova.ACO_APT = CredAPT.rowUsuario.APT;
                rowNova.ACOJuridico = rowA.Juridico;
                rowNova.ACOProprietario = rowA.DESTINATARIO == "P";
                dBol.ACOrdos.AddACOrdosRow(rowNova);
                int prest = 1;
                foreach (dAcordoRet.RetNovosBRow rowN in rowA.GetRetNovosBRows())
                {
                    IncluirBoletoAProvisorio(rowN);
                    dBoleto.NovosRow rowNovo = dBol.Novos.NewNovosRow();
                    rowNovo.ACO = rowNova.ACO;
                    rowNovo.ACO_APT = CredAPT.rowUsuario.APT;
                    rowNovo.dtvencto = rowN.vencto;
                    rowNovo.prest = prest++;
                    rowNovo.valprev = rowN.valor;
                    rowNovo.HValor = rowN.Honorarios;
                    rowNovo.BOLPago = rowNovo.IsvalpagoNull() ? "Não" : "Sim";
                    dBol.Novos.AddNovosRow(rowNovo);
                }
                foreach (dAcordoRet.RetOriginaisRow rowO in rowA.GetRetOriginaisRows())
                {
                    dBoleto.BoletosRow rowBOL = dBol.Boletos.FindByNumero(rowO.numero);
                    if (rowBOL != null)
                    {
                        dBoleto.OriginalRow rowOrig = dBol.Original.NewOriginalRow();
                        rowOrig.ACO = rowNova.ACO;
                        rowOrig.ACO_APT = CredAPT.rowUsuario.APT;
                        rowOrig.ACOStatus = 1;
                        rowOrig.competencia = string.Format("{0:00}{1:0000}", rowBOL.BOLCompetenciaMes, rowBOL.BOLCompetenciaAno);
                        rowOrig.valor = rowBOL.valor;
                        rowOrig.valororiginal = rowBOL.valor;
                        rowOrig.vencto = rowBOL.dtvencto;
                        rowOrig.BOLCompetenciaMes = rowBOL.BOLCompetenciaMes;
                        rowOrig.BOLCompetenciaAno = rowBOL.BOLCompetenciaAno;
                        rowOrig.BOLTipoCRAI = "";
                        rowOrig.BOLCompetencia = rowBOL.BOLCompetenciaMes.ToString("00") + "/" + rowBOL.BOLCompetenciaAno;
                        dBol.Original.AddOriginalRow(rowOrig);
                        rowBOL.Delete();
                    }
                }
            }
            dBol.Boletos.AcceptChanges();
        }

        private void IncluirBoletoAProvisorio(dAcordoRet.RetNovosBRow rowNovos)
        {
            dBoleto.BoletosRow rowBolNovo = dBol.Boletos.NewBoletosRow();
            rowBolNovo.competencia = "0101";
            rowBolNovo.dtvencto = rowNovos.vencto;
            rowBolNovo.Multa = 2;
            rowBolNovo.Numero = rowNovos.nn;
            rowBolNovo.Resp = CredAPT.rowUsuario.Proprietario ? "P" : "I";
            rowBolNovo.Tipo = "M";
            rowBolNovo.tipoboleto = "A";
            rowBolNovo.valor = rowNovos.valor;
            rowBolNovo.BOLCompetenciaMes = (short)rowNovos.vencto.Month;
            rowBolNovo.BOLCompetenciaAno = (short)rowNovos.vencto.Year;
            rowBolNovo.competencia = string.Format("{0:00}{1:0000}", rowBolNovo.BOLCompetenciaMes, rowBolNovo.BOLCompetenciaAno);
            rowBolNovo.Resp = (rowBolNovo.IsBOLProprietarioNull() || rowBolNovo.BOLProprietario) ? "P" : "I";
            rowBolNovo.pago = !rowBolNovo.IsBOLPagamentoNull();
            rowBolNovo.BLOCodigo = "";
            rowBolNovo.APTNumero = "";
            rowBolNovo.BOLProtestado = false;
            rowBolNovo.BOL = rowNovos.nn;
            rowBolNovo.BOLTipoUsuario = (rowBolNovo.IsBOLProprietarioNull() || rowBolNovo.BOLProprietario) ? "Proprietário" : "Inquilino";
            dBol.Boletos.AddBoletosRow(rowBolNovo);
            rowBolNovo.AcceptChanges();
        }

        protected void cmdAcordo_Click(object sender, EventArgs e)
        {
            //Exibe acordo
            panAcordo.Visible = true;
            panBoletos.Visible = false;
            lblTituloPagina.Text = "Boleto - Acordo";
        }

        protected void cmdVoltar_Click(object sender, EventArgs e)
        {
            //Esconde acordo
            panAcordo.Visible = false;
            panBoletos.Visible = true;
            lblTituloPagina.Text = "Boleto";
        }

        protected void cmdImprimir_Click(object sender, ImageClickEventArgs e)
        {
            //Pega o numero do boleto
            ImageButton image = (ImageButton)sender;
            int intBOL = int.Parse(image.CommandArgument.ToString());

            //Seta Cred do BOL
            Session["CredBOL"] = CredAPT;

            //Seta o boleto para impressao
            dBoleto.BoletosRow rowBOL = dBol.Boletos.FindByNumero(intBOL);
            Session["rowBOL"] = rowBOL;
            if (rowBOL.dtvencto > DateTime.Today.AddDays(5))
            {
                //Exibe boleto
                Session["BoletoPara"] = null;
                Response.Redirect("~/Pages_Portal/BoletoImpressao.aspx");
            }
            else
            {
                //Verifica se tem opcoes de data de pagamento
                CurBoleto = new VirBoleto(dBol, true);
                if ((CredAPT.CON == 732) && (CredAPT.EMP == 1))
                    CurBoleto.jurosCompostos = false;
                CurBoleto.CarregaDados(rowBOL);
                SortedList Alternativas = new SortedList();
                decimal ValorAlter = 0;
                for (DateTime dataAlter = DateTime.Today.AddDays(7); dataAlter >= DateTime.Today; dataAlter = dataAlter.AddDays(-1))
                {
                    if ((dataAlter.DayOfWeek == DayOfWeek.Saturday) || (dataAlter.DayOfWeek == DayOfWeek.Sunday))
                        continue;
                    decimal NovoValorAlter = CurBoleto.Calcula(dataAlter, false);
                    if (NovoValorAlter != ValorAlter)
                    {
                        ValorAlter = NovoValorAlter;
                        Alternativas.Add(dataAlter, ValorAlter);
                    };
                }

                //Exibe boleto ou datas de pagto para selecao
                if (Alternativas.Count == 1)
                {
                    Session["BoletoPara"] = (DateTime)Alternativas.GetKey(0);
                    Response.Redirect("~/Pages_Portal/BoletoImpressao.aspx");
                }
                else
                {
                    Session["BoletoDatas"] = Alternativas;
                    Response.Redirect("~/Pages_Portal/BoletoSelecaoData.aspx");
                }
            }
        }

        protected void cmdParcelar_Click(object sender, EventArgs e)
        {
            //Separa apenas os boletos nao pagos
            ArrayList Apagar = new ArrayList();
            foreach (dBoleto.BoletosRow rowBOL in dBol.Boletos)
                if (rowBOL.pago)
                    Apagar.Add(rowBOL);
            foreach (dBoleto.BoletosRow rowBOL in Apagar)
                rowBOL.Delete();
            dBol.Boletos.AcceptChanges();

            //Limpa variavel de sessao indicando acordo feito
            Session["AcordoFeito"] = null;

            //Vai para a tela de parcelamento
            if (Request.QueryString["CredAPT"] == null)
                Response.Redirect("~/Pages_Portal/BoletoParcelar.aspx");
            else
                Response.Redirect(string.Format("~/Pages_Portal/BoletoParcelar.aspx?CredAPT={0}", Request.QueryString["CredAPT"]));
        }

        protected void cmdDeclaracaoNadaConsta_Click(object sender, EventArgs e)
        {
            try
            {
                //Cria SMTP
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    email.To.Add(ConfigurationManager.AppSettings["EnvioEmailDeclaracaoNadaConsta" + Cred.EMP.ToString()].Replace(";", ","));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Solicitação de Declaração Nada Consta";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>SOLICITAÇÃO DE DECLARAÇÃO NADA CONSTA</b></p>\r\n" +
                                               "      <p><b>Data:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               ((Cred.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : "      <p><b>Bloco:</b> {2}</p>\r\n") +
                                               "      <p><b>Unidade:</b> {3}</p>\r\n" +
                                               "      <p><b>Nome:</b> {4}</p>\r\n" +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo),
                                               Server.HtmlEncode(Cred.rowUsuario.BLOCodigo), Server.HtmlEncode(Cred.rowUsuario.APTNumero), Server.HtmlEncode(Cred.rowUsuario.Nome));

                    //Envia email
                    SmtpClient1.Send(email);

                    //Enviou
                    ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", "alert(\"Sua solicitação de Declaração Nada Consta foi enviada para a Neon com sucesso! O documento será enviado pelo malote.\");", true);
                }
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", "alert(\"Ocorreu um erro na solicitação de Declaração Nada Consta. Por favor, tente novamente.\");", true);
            }
        }

        protected void cmdSolicitacaoDebitoAutomatico_Click(object sender, EventArgs e)
        {
            //Seta Cred do DEB
            Session["CredDEB"] = CredAPT;

            //Vai para tela de solicitacao de debito automatico
            Response.Redirect("~/Pages_Portal/BoletoDebitoAutomatico.aspx");
        }

        protected void cmdVerificarAcordo_Click(object sender, EventArgs e)
        {
            //Seta Cred do ACO
            Session["CredACO"] = CredAPT;
            
            //Vai para tela de visualizacao de acordo
            Response.Redirect("~/Pages_Portal/BoletoAcordo.aspx");
        }

        protected void cmdGerarAcordo_Click(object sender, EventArgs e)
        {
            //Seta Cred do ACO
            Session["CredACO"] = CredAPT;

            //vai para tela de novo de acordo
            Response.Redirect("~/Pages_Portal/BoletoAcordoNovo.aspx");
        }

        protected void grdBoletos_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.GetValue("BOLVencto").ToString().ToUpper() == "AGUARDO" || e.GetValue("BOLVencto").ToString().ToUpper() == "A VENCER") 
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(248, 248, 248);
                e.Row.Font.Bold = true;
            }
            else if (e.GetValue("BOLVencto").ToString().ToUpper() == "VENCIDO")
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(248, 248, 248);
                e.Row.ForeColor = System.Drawing.Color.Red;
                e.Row.Font.Bold = true;
            }
            else if (e.GetValue("BOLVencto").ToString().ToUpper() == "TOTAL")
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(245, 245, 245);
                e.Row.Font.Bold = true;
            }
        }

        public static string FormataSaida(decimal decValor, string strZero)
        {
            return (decValor == 0) ? strZero : string.Format("{0:n2}", decValor);
        }

        protected void grdBoletos_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.Caption == "Original" || e.Column.Caption == "Total")
                e.DisplayText = e.Value == DBNull.Value ? "" : FormataSaida(Convert.ToDecimal(e.Value), "0,00");
            else if (e.Column.Caption == "Corrigido" || e.Column.Caption == "Multa" || e.Column.Caption == "Juros")
                e.DisplayText = e.Value == DBNull.Value ? "" : FormataSaida(Convert.ToDecimal(e.Value), "");
        }

        protected void grdAcordoParcelas_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.Caption == "N°")
                e.DisplayText = (e.VisibleIndex + 1).ToString();
        }
    }
}