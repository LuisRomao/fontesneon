﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Collections;
using DevExpress.Web;
using PortalUtil;
using BoletoUtil;
using BoletoUtil.Datasets;

namespace Boleto.Pages_Portal
{
    public partial class BoletoParcelar : System.Web.UI.Page
    {
        private dBoleto dBol = new dBoleto();

        private VirBoleto CurBoleto;
        DataRow rowBOLAPT = null;

        private decimal decTotalVencido;
        private decimal decTotalVencidoCorrigido;
        private decimal decTotalMulta;
        private decimal decTotalJuros;
        private decimal decTotalFinal;

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }
        private Credencial CredAPT;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Verifica se ja tem acordo feito e retorna para boletos
            if (Session["AcordoFeito"] != null)
            {
                if (Request.QueryString["CredAPT"] == null)
                    Response.Redirect("~/Pages_Portal/Boleto.aspx");
                else
                    Response.Redirect(string.Format("~/Pages_Portal/Boleto.aspx?CredAPT={0}", Request.QueryString["CredAPT"]));
            }

            //Pega Cred do APT (caso tenha sido direcinado da lista de condominos pega o setado na sessao)
            CredAPT = (Request.QueryString["CredAPT"] == null) ? Cred : (Credencial)Session["Cred" + Request.QueryString["CredAPT"]];

            //Pega os boleto recebidos por sessao
            dBol = (dBoleto)Session["dBol"];

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Inicia boleto
            CurBoleto = new VirBoleto(dBol, true);
            if ((CredAPT.CON == 732) && (CredAPT.EMP == 1))
                CurBoleto.jurosCompostos = false;

            //Calcula valores, totais e insere na tabela de boletos do apt
            dBol.BoletosAPT.Clear();
            decTotalVencido = decTotalVencidoCorrigido = decTotalMulta = decTotalJuros = decTotalFinal = 0;
            int intParcelas = 0;
            foreach (dBoleto.BoletosRow rowBOL in dBol.Boletos)
            {
                //Pega o boleto e calcula valores
                CurBoleto.CarregaDados(rowBOL);
                CurBoleto.Calcula(DateTime.Now, true);

                //Seta boleto parcelar incial (caso nao seja postback, pois nesse caso pode ter mudado a selecao)
                if (!IsPostBack)
                    if (CurBoleto.stparcela == Statusparcela.Stparaberto)
                        rowBOL.Parcelar = false;
                    else
                        rowBOL.Parcelar = true;

                //Adiciona na lista
                rowBOLAPT = dBol.BoletosAPT.NewRow();
                rowBOLAPT["BOL"] = rowBOL.BOL;
                rowBOLAPT["BLOCodigo"] = rowBOL.BLOCodigo;
                rowBOLAPT["APTNumero"] = rowBOL.APTNumero;
                rowBOLAPT["BOLTipoUsuario"] = rowBOL.BOLTipoUsuario;
                rowBOLAPT["BOLVencto"] = rowBOL.dtvencto.ToString("dd/MM/yyyy");
                rowBOLAPT["BOLOriginal"] = CurBoleto.valororiginal;
                rowBOLAPT["BOLCorrigido"] = CurBoleto.ValorCorrigidostr == "" ? 0 : Convert.ToDecimal(CurBoleto.ValorCorrigidostr);
                rowBOLAPT["BOLMulta"] = CurBoleto.multa;
                rowBOLAPT["BOLJuros"] = CurBoleto.juros;
                rowBOLAPT["BOLTotal"] = CurBoleto.valorfinal;
                rowBOLAPT["BOLNossoNumero"] = CurBoleto.NossoNumero.ToString();
                rowBOLAPT["BOLParcelar"] = rowBOL.Parcelar;
                rowBOLAPT["BOLSelecao"] = true;
                dBol.BoletosAPT.Rows.Add(rowBOLAPT);

                //Soma no total se parcelar
                if (Convert.ToBoolean(rowBOLAPT["BOLParcelar"]))
                {
                    decTotalVencido += CurBoleto.valororiginal;
                    decTotalVencidoCorrigido += rowBOL.ValorCorrigido;
                    decTotalMulta += rowBOL.ValorMulta;
                    decTotalJuros += rowBOL.ValorJuros;
                    decTotalFinal += rowBOL.ValorFinal;
                    intParcelas++;
                }
            }

            //Insere Total
            rowBOLAPT = dBol.BoletosAPT.NewRow();
            rowBOLAPT["BOLVencto"] = "Total";
            rowBOLAPT["BOLOriginal"] = decTotalVencido;
            rowBOLAPT["BOLCorrigido"] = decTotalVencidoCorrigido;
            rowBOLAPT["BOLMulta"] = decTotalMulta;
            rowBOLAPT["BOLJuros"] = decTotalJuros;
            rowBOLAPT["BOLTotal"] = decTotalFinal;
            dBol.BoletosAPT.Rows.Add(rowBOLAPT);

            //Seta parcelamento
            SetaParcelamento(intParcelas, decTotalFinal);

            //Carrega boletos para acordo
            grdBoletosParaParcelar.DataSource = dBol.BoletosAPT;
            grdBoletosParaParcelar.DataBind();
        }

        private void SetaParcelamento(int intParcelas, decimal decTotal)
        {
            //Seta valores e datas
            int intVezes = 0;
            if (intParcelas > 0)
            {
                intVezes = Math.Min(3, intParcelas);
                decimal ValParcel = decTotal / intVezes;
                while ((ValParcel < 50) && (intVezes > 1))
                    ValParcel = decTotal / --intVezes;
                VALOR_1_1.Text = decTotal.ToString("n2");
                VALOR_2_1.Text = Math.Round(decTotal / 2, 2, MidpointRounding.AwayFromZero).ToString("n2");
                VALOR_2_2.Text = (decTotal - Math.Round(decTotal / 2, 2, MidpointRounding.AwayFromZero)).ToString("n2");
                VALOR_3_1.Text = VALOR_3_2.Text = Math.Round(decTotal / 3, 2, MidpointRounding.AwayFromZero).ToString("n2");
                VALOR_3_3.Text = (decTotal - 2 * Math.Round(decTotal / 3, 2, MidpointRounding.AwayFromZero)).ToString("n2");
                DateTime d1 = DateTime.Today.AddDays(7);
                if (d1.DayOfWeek == DayOfWeek.Saturday)
                    d1 = d1.AddDays(-1);
                if (d1.DayOfWeek == DayOfWeek.Sunday)
                    d1 = d1.AddDays(-2);
                DATA_1_1.Text = DATA_2_1.Text = DATA_3_1.Text = d1.ToString("dd/MM/yyyy");
                d1 = DateTime.Today.AddDays(7).AddMonths(1);
                if (d1.DayOfWeek == DayOfWeek.Saturday)
                    d1 = d1.AddDays(-1);
                if (d1.DayOfWeek == DayOfWeek.Sunday)
                    d1 = d1.AddDays(-2);
                DATA_2_2.Text = DATA_3_2.Text = d1.ToString("dd/MM/yyyy");
                d1 = DateTime.Today.AddDays(7).AddMonths(2);
                if (d1.DayOfWeek == DayOfWeek.Saturday)
                    d1 = d1.AddDays(-1);
                if (d1.DayOfWeek == DayOfWeek.Sunday)
                    d1 = d1.AddDays(-2);
                DATA_3_3.Text = d1.ToString("dd/MM/yyyy");
            };

            //Exibe parcelcamentos disponiveis
            panParcela1.Visible = (intVezes > 0);
            panParcela2.Visible = (intVezes > 1);
            panParcela3.Visible = (intVezes > 2);

            //Seta selecao de parcelamento e botao 
            if (intVezes == 1)
            {
                rdbParcela1.Checked = true;
                cmdEfetuarParcelamento.Enabled = true;
            }
            else
            {
                rdbParcela1.Checked = false;
                rdbParcela2.Checked = false;
                rdbParcela3.Checked = false;
                cmdEfetuarParcelamento.Enabled = false;
            }
        }

        protected void chkIncluirParcelamento_CheckedChanged(object sender, EventArgs e)
        {
            //Pega o boleto 
            ASPxCheckBox checkbox = (ASPxCheckBox)sender;
            int BOL = int.Parse(checkbox.ToolTip);
            dBoleto.BoletosRow rowBOL = dBol.Boletos.FindByNumero(BOL);
            rowBOL.Parcelar = checkbox.Checked;

            //Carrega tela
            Carregar();
        }

        protected void rdbParcela_CheckedChanged(object sender, EventArgs e)
        {
            //Habilita botao para efetuar parcelamento
            if (rdbParcela1.Checked || rdbParcela2.Checked || rdbParcela2.Checked)
                cmdEfetuarParcelamento.Enabled = true;
            else
                cmdEfetuarParcelamento.Enabled = false;
        }

        private void ProcessoAntigo()
        {
            //Pega boletos para parcelar
            ArrayList BoletosOrigem = new ArrayList();
            foreach (dBoleto.BoletosRow rowBoletos in dBol.Boletos)
                if (rowBoletos.Parcelar)
                    BoletosOrigem.Add(rowBoletos);

            //Insere novos boletos de acordo com o parcelamento selecionado
            SortedList BoletosNovos = new SortedList();
            if (rdbParcela1.Checked)
                BoletosNovos.Add(DateTime.Parse(DATA_1_1.Text), decimal.Parse(VALOR_1_1.Text));
            else if (rdbParcela2.Checked)
            {
                BoletosNovos.Add(DateTime.Parse(DATA_2_1.Text), decimal.Parse(VALOR_2_1.Text));
                BoletosNovos.Add(DateTime.Parse(DATA_2_2.Text), decimal.Parse(VALOR_2_2.Text));
            }
            else if (rdbParcela3.Checked)
            {
                BoletosNovos.Add(DateTime.Parse(DATA_3_1.Text), decimal.Parse(VALOR_3_1.Text));
                BoletosNovos.Add(DateTime.Parse(DATA_3_2.Text), decimal.Parse(VALOR_3_2.Text));
                BoletosNovos.Add(DateTime.Parse(DATA_3_3.Text), decimal.Parse(VALOR_3_3.Text));
            };

            //Seta acordo
            using (dAcordoRet dRetorno = new dAcordoRet())
            {
                VirMSSQL.TableAdapter TATrans = new VirMSSQL.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.SQL, true);
                try
                {
                    TATrans.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN1"].ConnectionString;
                    TATrans.AbreTrasacaoLocal("ProcessoAntigo", dRetorno.RetAcordoTableAdapter, dRetorno.RetOriginaisTableAdapter, dRetorno.RetNovosBTableAdapter, dRetorno.BOLETOSTableAdapter);
                    // Criar acordo na tabela ret
                    dAcordoRet.RetAcordoRow rowAcordo = dRetorno.RetAcordo.NewRetAcordoRow();
                    rowAcordo.apartamento = CredAPT.rowUsuario.APTNumero;
                    rowAcordo.bloco = CredAPT.rowUsuario.BLOCodigo;
                    rowAcordo.codcon = CredAPT.rowUsuario.CONCodigo;
                    rowAcordo.data = DateTime.Now;
                    rowAcordo.DESTINATARIO = CredAPT.rowUsuario.Proprietario ? "P" : "I";
                    rowAcordo.Efetuado = false;
                    rowAcordo.filial = CredAPT.EMP == 3 ? (short)2 : (short)CredAPT.EMP;
                    rowAcordo.Judicial = false;
                    rowAcordo.Juridico = false;
                    dRetorno.RetAcordo.AddRetAcordoRow(rowAcordo);
                    dRetorno.RetAcordoTableAdapter.Update(rowAcordo);
                    rowAcordo.AcceptChanges();
                    CredAPT.rowUsuario.Acordo = -rowAcordo.id;
                    foreach (dBoleto.BoletosRow rowBoletos in BoletosOrigem)
                    {
                        dAcordoRet.RetOriginaisRow rowOrig = dRetorno.RetOriginais.NewRetOriginaisRow();
                        rowOrig.RetAcordoRow = rowAcordo;
                        rowOrig.numero = (int)rowBoletos.Numero;
                        rowOrig.total = rowBoletos.ValorFinal;
                        dRetorno.RetOriginais.AddRetOriginaisRow(rowOrig);
                        dRetorno.RetOriginaisTableAdapter.Update(rowOrig);
                        rowOrig.AcceptChanges();
                        rowBoletos.Delete();
                        dBol.Boletos.AcceptChanges();
                    }
                    //Criando Novos
                    //short i = 1;
                    foreach (DictionaryEntry DE in BoletosNovos)
                    {
                        dAcordoRet.BOLETOSRow rowBoletos = dRetorno.BOLETOS.NewBOLETOSRow();
                        rowBoletos.apartamento = CredAPT.rowUsuario.APTNumero;
                        rowBoletos.bloco = CredAPT.rowUsuario.BLOCodigo;
                        rowBoletos.CODCON = CredAPT.rowUsuario.CONCodigo;
                        rowBoletos.vencto = (DateTime)DE.Key;
                        dRetorno.BOLETOS.AddBOLETOSRow(rowBoletos);
                        dRetorno.BOLETOSTableAdapter.Update(rowBoletos);
                        rowBoletos.AcceptChanges();
                        dAcordoRet.RetNovosBRow rowNovos = dRetorno.RetNovosB.NewRetNovosBRow();
                        rowNovos.RetAcordoRow = rowAcordo;
                        rowNovos.Honorarios = 0;
                        rowNovos.valor = (decimal)DE.Value;
                        rowNovos.vencto = (DateTime)DE.Key;
                        rowNovos.nn = rowBoletos.NN;
                        dRetorno.RetNovosB.AddRetNovosBRow(rowNovos);
                        dRetorno.RetNovosBTableAdapter.Update(rowNovos);
                        rowNovos.AcceptChanges();
                    }
                    TATrans.Commit();
                }
                catch (Exception e)
                {
                    TATrans.Vircatch(e);
                    throw e;
                }
            }
        }

        protected void cmdEfetuarParcelamento_Click(object sender, EventArgs e)
        {
            //Efetua parcelamento (processo antigo com neonimoveis_1)
            ProcessoAntigo();

            //Seta acordo feito
            Session["AcordoFeito"] = 1;

            //Volta para tela de boletos
            if (Request.QueryString["CredAPT"] == null)
                Response.Redirect("~/Pages_Portal/Boleto.aspx");
            else
                Response.Redirect(string.Format("~/Pages_Portal/Boleto.aspx?CredAPT={0}", Request.QueryString["CredAPT"]));
        }

        protected void grdBoletosParaParcelar_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.GetValue("BOLVencto").ToString().ToUpper() == "TOTAL")
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(245, 245, 245);
                e.Row.Font.Bold = true;
            }
        }

        public static string FormataSaida(decimal decValor, string strZero)
        {
            return (decValor == 0) ? strZero : string.Format("{0:n2}", decValor);
        }

        protected void grdBoletosParaCancelar_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.Caption == "Original" || e.Column.Caption == "Total")
                e.DisplayText = e.Value == DBNull.Value ? "" : FormataSaida(Convert.ToDecimal(e.Value), "0,00");
            else if (e.Column.Caption == "Corrigido" || e.Column.Caption == "Multa" || e.Column.Caption == "Juros")
                e.DisplayText = e.Value == DBNull.Value ? "" : FormataSaida(Convert.ToDecimal(e.Value), "");
        }
    }
}