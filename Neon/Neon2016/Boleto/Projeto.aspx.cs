﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalUtil;

namespace Boleto
{
    public partial class Projeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void cmdBoleto_Click(object sender, EventArgs e)
        {
            //if (Login("1ALESSA109", "2270")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - 000244 (SB) - Inadimplente com Acordo (sem parcela paga)
            //if (Login("1SHIRLE20", "1047")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - 000244 (SB) - Inadimplente com Acordo (com parcela paga)
            //if (Login("1FABIO175", "5826")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - 000244 (SB) - Inadimplente com Parcelar
            //if (Login("1eduard185", "1714")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - 000244 (SB) - Inadimplente com Parcelar
            //if (Login("1JOSE696", "9515")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - 000244 (SB) - Inadimplente sem Acordo
            //if (Login("1fabio305", "7497")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Inquilino - 000244 (SB) - Em dia
            //if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Sindico - ALGRAN (SA) - Em dia
            //if (Login("3JOSE138", "8550")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - ALGRAN (SA) - Inadimplente com Acordo (sem parcela paga)
            //if (Login("3daniel48", "1991")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - ALGRAN (SA) - Adimplente
            //if (Login("3PAULO78", "8217")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - ALGRAN (SA) - Inadimplente com Parcelar
            //if (Login("1adalbe34", "4354")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Itau
            //if (Login("1jose706", "6132")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Itau
            //if (Login("2PAULO64", "4964")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - GRENOB (SA)
            //if (Login("2MARIA7", "2854")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); //Proprietario - ACROPO (SA)
            //if (Login("1DIOGO18", "8550")) Response.Redirect("~/Pages_Portal/Boleto.aspx");
            //if (Login("1sérgio13", "0224")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); // Proprietario - CPARK. (SB) - Boleto E (nao exibir)
            //if (Login("1rejane7", "7719")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); // Proprietario - CURUMU (SB) - Inadimplente sem Acordo
            //if (Login("1paulo120", "2122")) Response.Redirect("~/Pages_Portal/Boleto.aspx"); // 000563 (SB) - Boletos vencidos e opcoes de data de vencimento
            //if (Login("1raimun43", "4173")) Response.Redirect("~/Pages_Portal/Boleto.aspx");
            //if (Login("1FABIO19", "5011")) Response.Redirect("~/Pages_Portal/Boleto.aspx");
            //if (Login("2fernan4", "6907")) Response.Redirect("~/Pages_Portal/Boleto.aspx");
            //if (Login("3quele1", "3903")) Response.Redirect("~/Pages_Portal/Boleto.aspx");
            //if (Login("3cristi57", "6995")) Response.Redirect("~/Pages_Portal/Boleto.aspx");
            //if (Login("1walter27", "6236")) Response.Redirect("~/Pages_Portal/Boleto.aspx");
            if (Login("1patric113", "3381")) Response.Redirect("~/Pages_Portal/Boleto.aspx");

            //Seta proprietario CredAPT e loga como adv adm
            //if (Login("1JOSE696", "9515"))
            //if (Login("3sergio77", "1550"))
            //if (Login("1CLAUDI90", "2042"))
            //if (Login("1FABIO19", "5011"))
            //if (Login("1PERY", "6587"))
            //{
            //    Credencial CredAPT = (Credencial)Session["Cred"];
            //    Session["Cred" + CredAPT.rowUsuario.APT] = CredAPT;
            //    if (Login("3Renata", "5437")) Response.Redirect("~/Pages_Portal/Boleto.aspx?CredAPT=" + CredAPT.rowUsuario.APT); //ADV ADM
            //    //if (Login("rodino", "062129")) Response.Redirect("~/Pages_Portal/Boleto.aspx?CredAPT=" + CredAPT.rowUsuario.APT); //ADM
            //}
        }

        protected bool Login(string strUsuario, string strSenha)
        {
            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(strUsuario, strSenha);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = strUsuario;
                Session["Pass"] = strSenha;

                //Autenticou
                return true;
            }
            
            //Não autenticou
            return false;
        }
    }
}