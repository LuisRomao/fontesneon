﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Web.Security;
using PortalUtil.Datasets;

namespace PortalUtil
{
    public class Credencial
    {
        public int EMP;
        public int CON;
        public string CONCorBase;
        public TipoAut TAut;
        public TipoUsuario TUsuario;
        public dCredencial.UsuarioRow rowUsuario;

        public enum TipoEMP
        {
            Neon = 1,
            NeonSA = 3,
        }

        public enum TipoAut
        {
            Neon,
            NeonSA,
            Site,
        }

        public enum TipoUsuario
        {
            Administradora = 1,
            Sindico = 2,
            Proprietario = 3,
            Inquilino = 4,
            ProprietarioSindico = 5,
            InquilinoSindico = 6,
            AdvogadoAdministradora = 7,
            Advogado = 8,
            Fornecedor = 9,
            Imobiliaria = 10,
            Subsindico = 11,
            GerentePredial = 12,
            Zelador = 13,
            Portaria = 14,
        }

        public ArrayList TiposSindico
        {
            get
            {
                return new ArrayList(new TipoUsuario[] { TipoUsuario.Sindico,
                                                         TipoUsuario.ProprietarioSindico,
                                                         TipoUsuario.InquilinoSindico });
            }
        }

        public static Credencial CredST(System.Web.UI.Page Chamador, bool obrigatorio)
        {
            //Verifica se ainda tem chamador
            Credencial ret = (Credencial)Chamador.Session["Cred"];
            if ((ret == null) && obrigatorio)
            {
                //Desloga e vai para a pagina de login
                FormsAuthentication.SignOut();
                Chamador.Response.Redirect("~/Account/SessaoExpirada.aspx");
            }

            //Retorna 
            return ret;
        }

        public Credencial(TipoAut _Aut, dCredencial.UsuarioRow _rowUsuario, int _EMP, int _CON)
        {
            EMP = _EMP;
            CON = _CON;
            TAut = _Aut;
            rowUsuario = _rowUsuario;
            TiraDBNull(rowUsuario);

            if (rowUsuario.TipoUsuario == 1)
                TUsuario = rowUsuario.Proprietario ? TipoUsuario.ProprietarioSindico : TipoUsuario.InquilinoSindico;
            else
                TUsuario = rowUsuario.Proprietario ? TipoUsuario.Proprietario : TipoUsuario.Inquilino;
            CONCorBase = "";
        }

        public Credencial(TipoAut _Aut, TipoUsuario _TUsuario, dCredencial.UsuarioRow _rowUsuario)
        {
            if (_Aut != Credencial.TipoAut.Site) EMP = (_Aut == Credencial.TipoAut.Neon) ? (int)Credencial.TipoEMP.Neon : (int)Credencial.TipoEMP.NeonSA;
            TAut = _Aut;
            TUsuario = _TUsuario;
            rowUsuario = _rowUsuario;
            TiraDBNull(rowUsuario);
            CONCorBase = "";
        }

        private void TiraDBNull(DataRow row)
        {
            foreach (DataColumn DC in row.Table.Columns)
                if (row[DC] == System.DBNull.Value)
                {
                    if (DC.DataType == typeof(string))
                        row[DC] = "";
                    else if (DC.DataType == typeof(int))
                        row[DC] = 0;
                }
        }
    }
}
