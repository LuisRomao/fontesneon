﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalUtil.Datasets;

namespace PortalUtil
{
    public class Autenticacao
    {
        public Autenticacao()
        {
        }

        public Credencial Autentica(string strUsuario, string strSenha)
        {
            dCredencial dCred = new dCredencial();
            Credencial.TipoAut TipoAut;

            //Autenticacao usuario condominos
            Datasets.dCredencialTableAdapters.AutenticaTableAdapter taAUT = new Datasets.dCredencialTableAdapters.AutenticaTableAdapter();
            dCredencial.AutenticaRow rowAutentica = null;
            try
            {
                //Tenta autenticar pelo SQL - Neon
                TipoAut = Credencial.TipoAut.Neon;
                if (taAUT.FillProp(dCred.Autentica, strUsuario, strSenha, (int)Credencial.TipoEMP.Neon) > 0)
                    rowAutentica = dCred.Autentica[0];
                else if (taAUT.FillInq(dCred.Autentica, strUsuario, strSenha, (int)Credencial.TipoEMP.Neon) > 0)
                    rowAutentica = dCred.Autentica[0];
                else
                {
                    //Tenta autenticar pelo SQL - Neon SA
                    TipoAut = Credencial.TipoAut.NeonSA;
                    taAUT.Connection.ConnectionString = taAUT.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                    if (taAUT.FillProp(dCred.Autentica, strUsuario, strSenha, (int)Credencial.TipoEMP.NeonSA) > 0)
                        rowAutentica = dCred.Autentica[0];
                    else if (taAUT.FillInq(dCred.Autentica, strUsuario, strSenha, (int)Credencial.TipoEMP.NeonSA) > 0)
                        rowAutentica = dCred.Autentica[0];
                }

                //Verifica se autenticou
                if (rowAutentica != null)
                {
                    dCredencial.UsuarioRow rowUsuario = dCred.Usuario.NewUsuarioRow();
                    rowUsuario.EMP = rowAutentica.EMP;
                    rowUsuario.CON = rowAutentica.CON;
                    rowUsuario.APT = rowAutentica.APT;
                    rowUsuario.PES = rowAutentica.PES;
                    rowUsuario.Nome = rowAutentica.Nome;
                    rowUsuario.CONCodigo = rowAutentica.CONCodigo;
                    rowUsuario.CONNome = rowAutentica.CONNome;
                    rowUsuario.BLOCodigo = rowAutentica.BLOCodigo;
                    rowUsuario.APTNumero = rowAutentica.APTNumero;
                    rowUsuario.Email = rowAutentica.Email;
                    rowUsuario.CpfCnpj = rowAutentica.CpfCnpj;
                    rowUsuario.TipoUsuario = rowAutentica.TipoUsuario;
                    rowUsuario.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                    return new Credencial(TipoAut, rowUsuario, rowAutentica.EMP, rowAutentica.CON);
                }
            }
            catch
            {
            };

            //Autenticacao usuario fornecedor
            int FRN;
            if (int.TryParse(strUsuario, out FRN))
            {
                Datasets.dCredencialTableAdapters.FORNECEDORESTableAdapter taFRN = new Datasets.dCredencialTableAdapters.FORNECEDORESTableAdapter();
                dCredencial.FORNECEDORESRow rowFRN = null;
                try
                {
                    //Tenta autenticar pelo SQL - Neon
                    TipoAut = Credencial.TipoAut.Neon;
                    if (taFRN.Fill(dCred.FORNECEDORES, FRN, strSenha) == 1)
                        rowFRN = dCred.FORNECEDORES[0];
                    else
                    {
                        //Tenta autenticar pelo SQL - Neon SA
                        TipoAut = Credencial.TipoAut.NeonSA;
                        taFRN.Connection.ConnectionString = taFRN.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                        if (taFRN.Fill(dCred.FORNECEDORES, FRN, strSenha) == 1)
                            rowFRN = dCred.FORNECEDORES[0];
                    }

                    //Verifica se autenticou
                    if (rowFRN != null)
                    {
                        dCredencial.UsuarioRow rowUsuario = dCred.Usuario.NewUsuarioRow();
                        rowUsuario.APT = 0;
                        rowUsuario.PES = 0;
                        rowUsuario.FRN = rowFRN.FRN;
                        rowUsuario.Nome = rowFRN.FRNNome;
                        rowUsuario.TipoUsuario = rowFRN.FRNTipo == "IMO" ? (int)Credencial.TipoUsuario.Imobiliaria : (int)Credencial.TipoUsuario.Fornecedor;
                        return new Credencial(TipoAut, (Credencial.TipoUsuario)rowUsuario.TipoUsuario, rowUsuario);
                    }
                }
                catch
                {
                }
            }

            //Autenticacao usuario site
            Datasets.dCredencialTableAdapters.USuaRiosTableAdapter taUSR = new Datasets.dCredencialTableAdapters.USuaRiosTableAdapter();
            dCredencial.USuaRiosRow rowUSR = null;
            try
            {
                TipoAut = Credencial.TipoAut.Site;
                if (taUSR.Fill(dCred.USuaRios, strUsuario, strSenha) > 0)
                {
                    rowUSR = dCred.USuaRios[0];
                    dCredencial.UsuarioRow rowUsuario = dCred.Usuario.NewUsuarioRow();
                    rowUsuario.APT = 0;
                    rowUsuario.PES = 0;
                    rowUsuario.USR = rowUSR.USR;
                    rowUsuario.Nome = rowUSR.USRNome;
                    rowUsuario.Email = rowUSR.USREmail;
                    rowUsuario.TipoUsuario = rowUSR.USR_TUS;
                    switch (rowUSR.USR_TUS)
                    {
                        case (int)Credencial.TipoUsuario.Administradora:
                            rowUsuario.CONNome = "Administradora";
                            break;
                        case (int)Credencial.TipoUsuario.AdvogadoAdministradora:
                            rowUsuario.USR = Convert.ToInt32(rowUSR.USRCodigo);
                            rowUsuario.CONNome = "Advogado(a)";
                            break;
                        case (int)Credencial.TipoUsuario.Advogado:
                        case (int)Credencial.TipoUsuario.Sindico:
                        case (int)Credencial.TipoUsuario.Subsindico:
                        case (int)Credencial.TipoUsuario.GerentePredial:
                        case (int)Credencial.TipoUsuario.Zelador:
                        case (int)Credencial.TipoUsuario.Portaria:
                            rowUsuario.APT = rowUSR.USR_APT;
                            rowUsuario.CONCodigo = rowUSR.USRCodigo;
                            rowUsuario.BLOCodigo = rowUSR.USR_BLOCodigo;
                            rowUsuario.APTNumero = rowUSR.USR_APTNumero;
                            break;
                        default:
                            return null;
                    }
                    Credencial Cred = new Credencial(TipoAut, (Credencial.TipoUsuario)rowUsuario.TipoUsuario, rowUsuario);
                    Cred.EMP = rowUSR.USR_EMP;
                    Cred.CON = rowUSR.USR_CON;
                    if (Cred.EMP != 0)
                    {
                        dCred.EMP = Cred.EMP;
                        dCred.CONDOMINIOSTableAdapter.Fill(dCred.CONDOMINIOS, Cred.EMP);
                        dCredencial.CONDOMINIOSRow rowCON = dCred.CONDOMINIOS.FindByCON(Cred.CON);
                        if ((rowCON.CONStatus == 1 || rowCON.CONStatus == 2) && rowCON.CONExportaInternet)
                        {
                            rowUsuario.EMP = rowCON.CON_EMP;
                            rowUsuario.CON = rowCON.CON;
                            rowUsuario.CONCodigo = rowCON.CONCodigo;
                            rowUsuario.CONNome = rowCON.CONNome;
                        }
                        else
                            return null;
                    }
                    return Cred;
                }
            }
            catch
            {
            }
            return null;
        }
    }
}
