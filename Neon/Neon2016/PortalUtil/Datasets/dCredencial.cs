﻿namespace PortalUtil.Datasets
{
    public partial class dCredencial
    {
        public int EMP;

        public dCredencial(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dCredencialTableAdapters.AutenticaTableAdapter taAutenticaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Autentica
        /// </summary>
        public dCredencialTableAdapters.AutenticaTableAdapter AutenticaTableAdapter
        {
            get
            {
                if (taAutenticaTableAdapter == null)
                {
                    taAutenticaTableAdapter = new dCredencialTableAdapters.AutenticaTableAdapter();
                };
                return taAutenticaTableAdapter;
            }
        }

        private dCredencialTableAdapters.FORNECEDORESTableAdapter taFORNECEDORESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FORNECEDORES
        /// </summary>
        public dCredencialTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (taFORNECEDORESTableAdapter == null)
                {
                    taFORNECEDORESTableAdapter = new dCredencialTableAdapters.FORNECEDORESTableAdapter();
                };
                return taFORNECEDORESTableAdapter;
            }
        }

        private dCredencialTableAdapters.CONDOMINIOSTableAdapter taCONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dCredencialTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (taCONDOMINIOSTableAdapter == null)
                {
                    taCONDOMINIOSTableAdapter = new dCredencialTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP == 3)
                        taCONDOMINIOSTableAdapter.Connection.ConnectionString = taCONDOMINIOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCONDOMINIOSTableAdapter;
            }
        }
    }
}
