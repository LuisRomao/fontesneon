﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalUtil
{
    public class Tipos
    {
        public enum TipoDocumento
        {
            Ata = 0,
            Convocacao = 1,
            RegulamentoInterno = 2,
            Convencao = 3,
            Outros = 4,
            ContratoAtivo = 5,
            Balancete = 6,
            RegrasReserva = 7,
        }

        public enum TipoEmailEnvio
        {
            FaleConosco = 0,
            ControleMudanca = 1,
            AutorizacaoEntradaPrestador = 2,
            SolicitacaoReparo = 3,
            CaixaSugestoes = 4,
            Reserva = 5,
            ReservaListaConvidados = 6,
        }

        public enum TipoSolicitacaoCheque
        {
            Talao = 0,
            Cheque = 1,
        }

        public enum TipoFuncionalidade
        {
            FaleConosco = 0,
            FaleConoscoCD = 1,
            ControleMudanca = 2,
            AutorizacaoEntradaPrestador = 3,
            SolicitacaoReparo = 4,
            CaixaSugestoes = 5,
            Classificado = 6,
            Reserva = 7,
            AvisoCorrespondencia = 8,
            Circulares = 9,
            CadastroEmail = 10,
            CadastroUsuario = 11,
        }
    }
}