﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalUtil;

namespace Funcionalidade
{
    public partial class Projeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void cmdCaixaSugestoes_Click(object sender, EventArgs e)
        {
            if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/CaixaSugestoes.aspx"); //Proprietario - 000999 (SB)
            //if (Login("anderson", "aqui09la")) Response.Redirect("~/Pages_Portal/CaixaSugestoes.aspx"); //Sindico - MANACA (SA)  
        }

        protected void cmdSolicitacaoReparo_Click(object sender, EventArgs e)
        {
            if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/SolicitacaoReparo.aspx"); //Proprietario - 000999 (SB)
            //if (Login("anderson", "aqui09la")) Response.Redirect("~/Pages_Portal/SolicitacaoReparo.aspx"); //Sindico - MANACA (SA)  
        }

        protected void cmdFaleConosco_Click(object sender, EventArgs e)
        {
            //if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/FaleConosco.aspx"); //Proprietario - 000999 (SB)
            if (Login("1MERC28", "7089")) Response.Redirect("~/Pages_Portal/FaleConosco.aspx"); //Proprietario - PNOBIL (SB)  
            //if (Login("1NAIR12", "9131")) Response.Redirect("~/Pages_Portal/FaleConosco.aspx"); //Proprietario - 000205 (SB)  
            //if (Login("anderson", "aqui09la")) Response.Redirect("~/Pages_Portal/FaleConosco.aspx"); //Sindico - MANACA (SA)  
        }

        protected void cmdSolicitacaoCheque_Click(object sender, EventArgs e)
        {
            if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/SolicitacaoCheque.aspx"); //Sindico - 000999 (SB)
            //if (Login("anderson", "aqui09la")) Response.Redirect("~/Pages_Portal/FaleConosco.aspx"); //Sindico - MANACA (SA)  
        }

        protected void cmdAvisoCorrespondencia_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/AvisoCorrespondencia.aspx"); //Sindico - 000999 (SB)
            //if (Login("anderson", "aqui09la")) Response.Redirect("~/Pages_Portal/AvisoCorrespondencia.aspx"); //Sindico - MANACA (SA)  
            if (Login("1leandr17", "3459")) Response.Redirect("~/Pages_Portal/AvisoCorrespondencia.aspx"); //Sindico - CURUMI (SB)
        }

        protected void cmdCircular_Click(object sender, EventArgs e)
        {
            if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/Circular.aspx"); //Sindico - 000999 (SB)
            //if (Login("anderson", "aqui09la")) Response.Redirect("~/Pages_Portal/Circular.aspx"); //Sindico - MANACA (SA)  
            //if (Login("1luiz282", "9985")) Response.Redirect("~/Pages_Portal/Circular.aspx"); //Sindico - M.IMPE (SB)   
            //if (Login("1leandr17", "3459")) Response.Redirect("~/Pages_Portal/Circular.aspx"); //Sindico - CURUMI (SB)
            //if (Login("1marcel318", "1924")) Response.Redirect("~/Pages_Portal/Circular.aspx"); //Sindico - ARCADI (SB)
        }

        protected void cmdControleMudanca_Click(object sender, EventArgs e)
        {
            if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/ControleMudanca.aspx"); //Sindico - 000999 (SB)
            //if (Login("3JOSE138", "8550")) Response.Redirect("~/Pages_Portal/ControleMudanca.aspx"); //Proprietario - ALGRAN (SA)
        }

        protected void cmdAutorizacaoEntradaPrestador_Click(object sender, EventArgs e)
        {
            if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/AutorizacaoEntradaPrestador.aspx"); //Sindico - 000999 (SB)
            //if (Login("anderson", "aqui09la")) Response.Redirect("~/Pages_Portal/AutorizacaoEntradaPrestador.aspx"); //Sindico - MANACA (SA)  
        }

        protected void cmdAviso_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/Aviso.aspx"); //Sindico - 000999 (SB)
            if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/Aviso.aspx"); //Proprietario - 000999 (SB)
            //if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/Aviso.aspx"); //Sindico - ALGRAN (SA)
            //if (Login("3marcos98", "7992")) Response.Redirect("~/Pages_Portal/Aviso.aspx"); //Proprietario - ALGRAN (SA)
            //if (Login("1leandr17", "3459")) Response.Redirect("~/Pages_Portal/Aviso.aspx"); //Sindico - CURUMI (SB)
        }

        protected bool Login(string strUsuario, string strSenha)
        {
            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(strUsuario, strSenha);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = strUsuario;
                Session["Pass"] = strSenha;

                //Autenticou
                return true;
            }
            
            //Não autenticou
            return false;
        }
    }
}