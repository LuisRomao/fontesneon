﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;
using System.Configuration;
using DevExpress.Web;
using PortalUtil;
using FuncionalidadeUtil.Datasets;

namespace Funcionalidade.Pages_Portal
{
    public partial class SolicitacaoReparo : System.Web.UI.Page
    {
        private dFuncionalidade dFun = new dFuncionalidade();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        private int nAnexos;
        private string NomeAnexoLista = "AnexosSR";
        private string NomeAnexo(int n)
        { return string.Format("AnexoSR_{0}", n); }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dFun = new dFuncionalidade(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //E-mail de retorno
            if (!Cred.rowUsuario.IsEmailNull())
                txtEmail.Text = Cred.rowUsuario.Email;

            //Limpa campos
            txtMensagem.Text = "";

            //Limpa anexos
            Session[NomeAnexoLista] = null;
        }

        protected void uplAnexo_FilesUploadComplete(object sender, FilesUploadCompleteEventArgs e)
        {
            //Inicia upload de anexos
            ASPxUploadControl upload = sender as ASPxUploadControl;
            Session[NomeAnexoLista] = null;

            //Pega os anexos selecionados e salva na sessao
            Dictionary<int, string> lstAnexos = new Dictionary<int, string>();
            if (upload.UploadedFiles != null && upload.UploadedFiles.Length > 0)
            {
                for (int i = 0; i < upload.UploadedFiles.Length; i++)
                {
                    UploadedFile arquivo = upload.UploadedFiles[i];
                    if (arquivo.FileName != "")
                    {
                        lstAnexos.Add(++nAnexos, arquivo.FileName);
                        Session[NomeAnexo(nAnexos)] = new MemoryStream(arquivo.FileBytes);
                    }
                }
                Session[NomeAnexoLista] = lstAnexos;
            }
        }

        protected void cbpEmail_Callback(object sender, CallbackEventArgsBase e)
        {
            //Envia o email
            lblMsg.Text = Enviar();

            //Limpa lista de anexos
            Session[NomeAnexoLista] = null;
        }

        protected void cbpEmail_CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpMsg"] = lblMsg.Text;
        }

        protected string Enviar()
        {
            try
            {
                //Cria SMTP
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];
                string strMsgResponsabilidade = "    <p style='font-style: italic;'>Esta é uma mensagem enviada através do site da Neon, o texto acima é de responsabilidade exclusiva do autor.</p>";

                //Verifica SMTP de dominio proprio e seta dados de envio especificos
                if (dFun.ST_SMTpTableAdapter.Fill(dFun.ST_SMTp, Cred.CON) > 0)
                {
                    SmtpClient1.Host = dFun.ST_SMTp[0].SMTpHost;
                    SmtpClient1.Credentials = new System.Net.NetworkCredential(dFun.ST_SMTp[0].SMTpUser, dFun.ST_SMTp[0].SMTpPass);
                    SmtpClient1.Port = Convert.ToInt32(dFun.ST_SMTp[0].SMTpPort);
                    if (!dFun.ST_SMTp[0].IsSMTpRemetenteNull()) strRemetente = dFun.ST_SMTp[0].SMTpRemetente;
                    strMsgResponsabilidade = "    <p></p>";
                }

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    dFun.ST_EMailEnvioTableAdapter.Fill(dFun.ST_EMailEnvio, Cred.CON, (int)Tipos.TipoEmailEnvio.SolicitacaoReparo);
                    foreach (dFuncionalidade.ST_EMailEnvioRow row in dFun.ST_EMailEnvio.Rows)
                        email.To.Add(row.EMEEmail.ToString().Replace(";", ","));

                    //Replay
                    email.ReplyToList.Add(new System.Net.Mail.MailAddress(txtEmail.Text.Replace(";", ",")));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Solicitação de Reparo na Área Comum";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>SOLICITAÇÃO DE REPARO NA ÁREA COMUM</b></p>\r\n" +
                                               "      <p><b>Data:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               ((Cred.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : "      <p><b>Bloco:</b> {2}</p>\r\n") +
                                               "      <p><b>Unidade:</b> {3}</p>\r\n" +
                                               "      <p><b>Nome:</b> {4}</p>\r\n" +
                                               "      <p><b>E-mail:</b> {5}</p>\r\n" +
                                               "      <p><b>Mensagem:</b><br/>{6}</p>\r\n" +
                                               strMsgResponsabilidade +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo), 
                                               Server.HtmlEncode(Cred.rowUsuario.BLOCodigo), Server.HtmlEncode(Cred.rowUsuario.APTNumero),
                                               Server.HtmlEncode(Cred.rowUsuario.Nome), Server.HtmlEncode(txtEmail.Text), Server.HtmlEncode(txtMensagem.Text).Replace("\n", "<br/>"));

                    //Inclui Anexos
                    Dictionary<int, string> lstAnexos = Session[NomeAnexoLista] as Dictionary<int, string>;
                    if (lstAnexos != null)
                        foreach (KeyValuePair<int, string> anexo in lstAnexos)
                        {
                            MemoryStream ms = (MemoryStream)Session[NomeAnexo(anexo.Key)];
                            email.Attachments.Add(new System.Net.Mail.Attachment(ms, anexo.Value));
                        }

                    //Envia email
                    SmtpClient1.Send(email);
                    Thread.Sleep(1000);

                    //Sucesso no envio
                    return "Solicitação enviada com sucesso!";
                }
            }
            catch (Exception)
            {
                //Erro no envio
                return "Não foi possível enviar sua solicitação! \r\nPor favor, tente novamente.";
            }
        }
    }
}