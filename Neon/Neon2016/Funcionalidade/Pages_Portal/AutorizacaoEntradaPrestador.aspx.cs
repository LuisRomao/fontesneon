﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Configuration;
using DevExpress.Web;
using PortalUtil;
using FuncionalidadeUtil.Datasets;

namespace Funcionalidade.Pages_Portal
{
    public partial class AutorizacaoEntradaPrestador : System.Web.UI.Page
    {
        private dFuncionalidade dFun = new dFuncionalidade();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dFun = new dFuncionalidade(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Preenche campos
            if (!Cred.rowUsuario.IsEmailNull())
                txtEmail.Text = Cred.rowUsuario.Email;
            if (!Cred.rowUsuario.IsNomeNull())
                txtNomeCondomino.Text = Cred.rowUsuario.Nome;

            //Limita campo de data (a partir de hoje)
            dteDataInicio.MinDate = DateTime.Now.Date;
            dteDataTermino.MinDate = DateTime.Now.Date;
        }
        
        protected void cbpEmail_Callback(object sender, CallbackEventArgsBase e)
        {
            //Envia o email
            lblMsg.Text = Enviar();
        }

        protected void cbpEmail_CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpMsg"] = lblMsg.Text;
        }

        protected string Enviar()
        {
            try
            {
                //Cria SMTP
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Verifica SMTP de dominio proprio e seta dados de envio especificos
                if (dFun.ST_SMTpTableAdapter.Fill(dFun.ST_SMTp, Cred.CON) > 0)
                {
                    SmtpClient1.Host = dFun.ST_SMTp[0].SMTpHost;
                    SmtpClient1.Credentials = new System.Net.NetworkCredential(dFun.ST_SMTp[0].SMTpUser, dFun.ST_SMTp[0].SMTpPass);
                    SmtpClient1.Port = Convert.ToInt32(dFun.ST_SMTp[0].SMTpPort);
                    if (!dFun.ST_SMTp[0].IsSMTpRemetenteNull()) strRemetente = dFun.ST_SMTp[0].SMTpRemetente;
                }

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    dFun.ST_EMailEnvioTableAdapter.Fill(dFun.ST_EMailEnvio, Cred.CON, (int)Tipos.TipoEmailEnvio.AutorizacaoEntradaPrestador);
                    foreach (dFuncionalidade.ST_EMailEnvioRow row in dFun.ST_EMailEnvio.Rows)
                        email.To.Add(row.EMEEmail.ToString().Replace(";", ","));

                    //Replay
                    email.ReplyToList.Add(new System.Net.Mail.MailAddress(txtEmail.Text.Replace(";", ",")));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Solicitação de Autorização para Entrada de Prestadores de Serviço";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>SOLICITAÇÃO DE AUTORIZAÇÃO PARA ENTRADA DE PRESTADORES DE SERVIÇO</b></p>\r\n" +
                                               "      <p><b>Data de Envio:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               ((Cred.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : "      <p><b>Bloco:</b> {2}</p>\r\n") +
                                               "      <p><b>Unidade:</b> {3}</p>\r\n" +
                                               "      \r\n" +
                                               "      <p><b>Nome do condômino:</b> {4}</p>\r\n" +
                                               "      <p><b>E-mail:</b> {5}</p>\r\n" +
                                               "      <p><b>Telefone para contato:</b> {6}</p>\r\n" +
                                               "      <p><b>Serviço prestado:</b> {7}</p>\r\n" +
                                               "      <p><b>Data (início):</b> {8}</p>\r\n" +
                                               (dteDataTermino.Text == "" ? "" : "      <p><b>Data (término):</b> {9}</p>\r\n") +
                                               (tmeHorarioEntrada.Text == "" ? "      <p><b>Horário:</b> livre<br/><br/>" : (tmeHorarioSaida.Text == "" ? "      <p><b>Horário:</b> a partir de {10}<br/><br/>" : "      <p><b>Horário:</b> {10} às {11}<br/><br/>")),
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo),
                                               Server.HtmlEncode(Cred.rowUsuario.BLOCodigo), Server.HtmlEncode(Cred.rowUsuario.APTNumero),
                                               Server.HtmlEncode(txtNomeCondomino.Text), Server.HtmlEncode(txtEmail.Text),Server.HtmlEncode(txtTelefone.Text), Server.HtmlEncode(txtServico.Text),
                                               Server.HtmlEncode(dteDataInicio.Text), Server.HtmlEncode(dteDataTermino.Text), Server.HtmlEncode(tmeHorarioEntrada.Text), Server.HtmlEncode(tmeHorarioSaida.Text));
                    email.Body += string.Format("      <br/></p>\r\n");
                    email.Body += string.Format("      <p><b>PRESTADORES DE SERVIÇO</b></p>\r\n");
                    email.Body += string.Format("      <p><b>Nome:</b> {0} <b>- RG:</b> {1} <b>- Empresa:</b> {2}<br/>", txtNome1.Text, txtRG1.Text, txtEmpresa1.Text);
                    if (txtNome2.Text != "") { email.Body += string.Format("      <b>Nome:</b> {0} <b>- RG:</b> {1} <b>- Empresa:</b> {2}<br/>", txtNome2.Text, txtRG2.Text, txtEmpresa2.Text); }
                    if (txtNome3.Text != "") { email.Body += string.Format("      <b>Nome:</b> {0} <b>- RG:</b> {1} <b>- Empresa:</b> {2}<br/>", txtNome3.Text, txtRG3.Text, txtEmpresa3.Text); }
                    if (txtNome4.Text != "") { email.Body += string.Format("      <b>Nome:</b> {0} <b>- RG:</b> {1} <b>- Empresa:</b> {2}<br/>", txtNome4.Text, txtRG4.Text, txtEmpresa4.Text); }
                    if (txtNome5.Text != "") { email.Body += string.Format("      <b>Nome:</b> {0} <b>- RG:</b> {1} <b>- Empresa:</b> {2}</p>", txtNome5.Text, txtRG5.Text, txtEmpresa5.Text); }
                    email.Body += string.Format("   </body>\r\n");
                    email.Body += string.Format("</html>");

                    //Envia email
                    SmtpClient1.Send(email);
                    Thread.Sleep(1000);

                    //Reinicia tela
                    txtEmail.Text = "";
                    if (!Cred.rowUsuario.IsEmailNull())
                        txtEmail.Text = Cred.rowUsuario.Email;
                    txtNomeCondomino.Text = "";
                    if (!Cred.rowUsuario.IsNomeNull())
                        txtNomeCondomino.Text = Cred.rowUsuario.Nome;
                    txtTelefone.Text = "";
                    txtServico.Text = "";
                    dteDataInicio.Text = "";
                    dteDataTermino.Text = "";
                    tmeHorarioEntrada.Text = "";
                    tmeHorarioSaida.Text = "";
                    txtNome1.Text = "";
                    txtRG1.Text = "";
                    txtEmpresa1.Text = "";
                    txtNome2.Text = "";
                    txtRG2.Text = "";
                    txtEmpresa2.Text = "";
                    txtNome3.Text = "";
                    txtRG3.Text = "";
                    txtEmpresa3.Text = "";
                    txtNome4.Text = "";
                    txtRG4.Text = "";
                    txtEmpresa4.Text = "";
                    txtNome5.Text = "";
                    txtRG5.Text = "";
                    txtEmpresa5.Text = "";

                    //Sucesso no envio
                    return "Solicitação enviada com sucesso!";
                }
            }
            catch (Exception)
            {
                //Erro no envio
                return "Não foi possível enviar sua solicitação! \r\nPor favor, tente novamente.";
            }
        }
    }
}