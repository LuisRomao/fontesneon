﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using PortalUtil.Datasets;
using FuncionalidadeUtil.Datasets;

namespace Funcionalidade.Pages_Portal
{
    public partial class Aviso : System.Web.UI.Page
    {
        private dFuncionalidade dFun = new dFuncionalidade();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "12px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dFun = new dFuncionalidade(Cred.EMP);

            //Carrega tela
            Carregar();
        }

        private void Carregar()
        {
            //Carrega os avisos ativos da unidade
            dFun.ST_AViSosTableAdapter.FillAtivos(dFun.ST_AViSos, Cred.CON, Cred.rowUsuario.APT, DateTime.Now.ToShortDateString());
            grdAvisos.DataSource = dFun.ST_AViSos;
            grdAvisos.DataBind();
        }
    }
}