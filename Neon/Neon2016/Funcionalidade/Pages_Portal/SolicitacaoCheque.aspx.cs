﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;
using System.Configuration;
using DevExpress.Web;
using PortalUtil;

namespace Funcionalidade.Pages_Portal
{
    public partial class SolicitacaoCheque : System.Web.UI.Page
    {
        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rblTipoSolicitacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Exibe ou enconde campo de mensagem (apenas para cheque especifico)
            FormLayout.FindItemOrGroupByName("laiMensagem").Visible = ((int)rblTipoSolicitacao.SelectedItem.Value == (int)Tipos.TipoSolicitacaoCheque.Cheque) ? true : false;
        }

        protected void cbpEmail_Callback(object sender, CallbackEventArgsBase e)
        {
            //Envia o email
            lblMsg.Text = Enviar();
        }

        protected void cbpEmail_CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpMsg"] = lblMsg.Text;
        }

        protected string Enviar()
        {
            try
            {
                //Cria SMTP
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    email.To.Add(ConfigurationManager.AppSettings["EnvioEmailSolicitacaoCheque" + Cred.EMP.ToString()].Replace(";", ","));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    if ((int)rblTipoSolicitacao.SelectedItem.Value == (int)Tipos.TipoSolicitacaoCheque.Talao)
                        email.Subject = "Solicitação de Talão de Cheques";
                    else
                        email.Subject = "Solicitação de Cheque";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>{6}</b></p>\r\n" +
                                               "      <p><b>Data:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               ((Cred.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : "      <p><b>Bloco:</b> {2}</p>\r\n") +
                                               "      <p><b>Unidade:</b> {3}</p>\r\n" +
                                               "      <p><b>Nome:</b> {4}</p>\r\n" +
                                               ((int)rblTipoSolicitacao.SelectedItem.Value == (int)Tipos.TipoSolicitacaoCheque.Cheque ? "      <p><b>Dados do Cheque:</b><br/>{5}</p>\r\n" : "") +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo), 
                                               Server.HtmlEncode(Cred.rowUsuario.BLOCodigo), Server.HtmlEncode(Cred.rowUsuario.APTNumero),
                                               Server.HtmlEncode(Cred.rowUsuario.Nome), Server.HtmlEncode(txtMensagem.Text).Replace("\n", "<br/>"), 
                                               ((int)rblTipoSolicitacao.SelectedItem.Value == (int)Tipos.TipoSolicitacaoCheque.Talao ? "SOLICITAÇÃO DE TALÃO DE CHEQUES" : "SOLICITAÇÃO DE CHEQUE"));

                    //Envia email
                    SmtpClient1.Send(email);
                    Thread.Sleep(1000);

                    //Sucesso no envio
                    return "Solicitação enviada com sucesso!";
                }
            }
            catch (Exception ex)
            {
                //Erro no envio
                return "Não foi possível enviar sua solicitação! \r\nPor favor, tente novamente. \r\n(Erro: " + ex.Message + ")";
            }
        }
    }
}