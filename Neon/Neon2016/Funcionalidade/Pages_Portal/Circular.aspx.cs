﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;
using System.Configuration;
using System.Text.RegularExpressions;
using DevExpress.Web;
using PortalUtil;
using FuncionalidadeUtil.Datasets;

namespace Portal
{
    public partial class Circular : System.Web.UI.Page
    {
        private dFuncionalidade dFun = new dFuncionalidade();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        private int nAnexos;
        private string NomeAnexoLista = "AnexosCI";
        private string NomeAnexo(int n)
        { return string.Format("AnexoCI_{0}", n); }

        Regex rgEmail = new Regex(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dFun = new dFuncionalidade(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Limpa campos
            txtMensagem.Text = "";

            //Limpa anexos
            Session[NomeAnexoLista] = null;
        }

        protected void uplAnexo_FilesUploadComplete(object sender, FilesUploadCompleteEventArgs e)
        {
            //Inicia upload de anexos
            ASPxUploadControl upload = sender as ASPxUploadControl;
            Session[NomeAnexoLista] = null;

            //Pega os anexos selecionados e salva na sessao
            Dictionary<int, string> lstAnexos = new Dictionary<int, string>();
            if (upload.UploadedFiles != null && upload.UploadedFiles.Length > 0)
            {
                for (int i = 0; i < upload.UploadedFiles.Length; i++)
                {
                    UploadedFile arquivo = upload.UploadedFiles[i];
                    if (arquivo.FileName != "")
                    {
                        lstAnexos.Add(++nAnexos, arquivo.FileName);
                        Session[NomeAnexo(nAnexos)] = new MemoryStream(arquivo.FileBytes);
                    }
                }
                Session[NomeAnexoLista] = lstAnexos;
            }
        }

        protected void cbpEmail_Callback(object sender, CallbackEventArgsBase e)
        {
            //Envia o email
            string strRelatorio = "";
            lblMsg.Text = Enviar(ref strRelatorio);
            txtResultado.Html = strRelatorio;

            //Limpa lista de anexos
            Session[NomeAnexoLista] = null;
        }

        protected void cbpEmail_CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpMsg"] = lblMsg.Text;
            e.Properties["cpResultado"] = txtResultado.Html;
        }

        protected string Enviar(ref string strRelatorio)
        {
            try
            {
                //Cria SMTP
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];
                string strMsgResponsabilidade = "    <p style='font-style: italic;'>Esta é uma mensagem enviada através do site da Neon, o texto acima é de responsabilidade exclusiva do autor.</p>";

                //Verifica SMTP de dominio proprio e seta dados de envio especificos (apenas para condominios com menos de 100 unidades + corpo diretivo e com email de circular configurado)
                if ((dFun.CondominosTableAdapter.Fill(dFun.Condominos, Cred.CON) + dFun.CORPODIRETIVOTableAdapter.Fill(dFun.CORPODIRETIVO, Cred.CON) < 100) 
                    && (dFun.ST_SMTpTableAdapter.Fill(dFun.ST_SMTp, Cred.CON) > 0))
                {
                    if (!dFun.ST_SMTp[0].IsSMTpRemetenteCircularNull())
                    {
                        SmtpClient1.Host = dFun.ST_SMTp[0].SMTpHost;
                        SmtpClient1.Credentials = new System.Net.NetworkCredential(dFun.ST_SMTp[0].SMTpUser, dFun.ST_SMTp[0].SMTpPass);
                        SmtpClient1.Port = Convert.ToInt32(dFun.ST_SMTp[0].SMTpPort);
                        strRemetente = dFun.ST_SMTp[0].SMTpRemetenteCircular;
                        strMsgResponsabilidade = "    <p></p>";
                    }
                }

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Circular";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>CIRCULAR</b></p>\r\n" +
                                               "      <p><b>Data:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               "      <p><b>Mensagem:</b><br/>{2}</p>\r\n" +
                                               strMsgResponsabilidade +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo), 
                                               Server.HtmlEncode(txtMensagem.Text).Replace("\n", "<br/>"));

                    //Inclui Anexos
                    Dictionary<int, string> lstAnexos = Session[NomeAnexoLista] as Dictionary<int, string>;
                    if (lstAnexos != null)
                        foreach (KeyValuePair<int, string> anexo in lstAnexos)
                        {
                            MemoryStream ms = (MemoryStream)Session[NomeAnexo(anexo.Key)];
                            email.Attachments.Add(new System.Net.Mail.Attachment(ms, anexo.Value));
                        }

                    //To - Verifica destinatarios e manda um por um
                    if (cklDestinatario.SelectedValues.Contains("CDIR"))
                    {
                        //Envia para os destinatarios do Corpo Diretivo
                        strRelatorio += String.Format("<b>* CORPO DIRETIVO:</b><br>Total de envios = {0}<br><br>", dFun.CORPODIRETIVO.Rows.Count);
                        int i = 0;
                        foreach (dFuncionalidade.CORPODIRETIVORow row in dFun.CORPODIRETIVO.Rows)
                        {
                            strRelatorio += String.Format("<b>{0} - {1} =</b> ", ++i, row["Nome"].ToString());
                            if (row["Email"].ToString() != "")
                            {
                                //Verifica e-mail valido
                                if (rgEmail.IsMatch(row["Email"].ToString().Trim()))
                                {
                                    //Adiciona apenas este e-mail
                                    email.To.Clear();
                                    email.To.Add(row["Email"].ToString().Replace(";", ","));

                                    //Envia o e-mail
                                    SmtpClient1.Send(email);
                                    Thread.Sleep(200);

                                    //Com email definido
                                    strRelatorio += String.Format("{0}<br>", row["Email"].ToString().ToLower());
                                }
                                else
                                {
                                    //E-mail invalido
                                    strRelatorio += String.Format("<span style='color: red'>Endereço de e-mail inválido - {0}</span><br>", row["Email"].ToString());
                                }
                            }
                            else
                                //Sem email definido
                                strRelatorio += String.Format("<span style='color: red'>Sem endereço de e-mail definido</span><br>");
                        }
                    }
                    if (cklDestinatario.SelectedValues.Contains("COND"))
                    {
                        //Envia para os destinatarios Condominos
                        if (strRelatorio != "") strRelatorio += "<br>";
                        strRelatorio += String.Format("<b>* CONDÔMINOS:</b><br>Total de envios = {0}<br><br>", dFun.Condominos.Rows.Count);
                        int i = 0;
                        foreach (dFuncionalidade.CondominosRow row in dFun.Condominos.Rows)
                        {
                            strRelatorio += String.Format("<b>{0} - {1} =</b> ", ++i, row["Nome"].ToString());
                            if (row["Email"].ToString() != "")
                            {
                                //Verifica e-mail valido
                                if (rgEmail.IsMatch(row["Email"].ToString().Trim()))
                                {
                                    //Adiciona apenas este e-mail
                                    email.To.Clear();
                                    email.To.Add(row["Email"].ToString().Replace(";", ","));

                                    //Envia o e-mail
                                    SmtpClient1.Send(email);
                                    Thread.Sleep(200);

                                    //Com email definido
                                    strRelatorio += String.Format("{0}<br>", row["Email"].ToString().ToLower());
                                }
                                else
                                {
                                    //E-mail invalido
                                    strRelatorio += String.Format("<span style='color: red'>Endereço de e-mail inválido - {0}</span><br>", row["Email"].ToString());
                                }
                            }
                            else
                                //Sem email definido
                                strRelatorio += String.Format("<span style='color: red'>Sem endereço de e-mail definido</span><br>");

                            //Controla envio
                            if (i % 50 == 0)
                                Thread.Sleep(6000);
                        }
                    }
                    if (strRelatorio != "") strRelatorio += "<br>";

                    //Sucesso log no relatorio
                    strRelatorio += String.Format("<span style='color: blue'><b>RESULTADO:</b> Envio da circular finalizado com sucesso.</span><br><br>");

                    //Sucesso no envio
                    return "O envio da circular foi finalizado! \r\n\r\nConfira abaixo o resultado do envio.";
                }
            }
            catch (Exception ex)
            {
                //Erro log no relatorio
                strRelatorio += "<span style='color: red'>Ocorreu um erro ... </span><br><br>";
                strRelatorio += String.Format("<span style='color: red'><b>RESULTADO:</b> Erro no envio da circular.</span><br><br>", ex.Message, ex.HResult);
                if (ex.HResult == -2146233088)
                    strRelatorio += String.Format("<span style='color: red'><b>Erro -</b> O limite máximo de envio de e-mails por hora, permitido pelo servidor de hospedagem, foi excedido. (Número: {0}).<br><br></span>", ex.HResult);
                else
                    strRelatorio += String.Format("<span style='color: red'><b>Erro -</b> {0} (Número: {1}).<br><br></span>", ex.Message, ex.HResult);

                //Erro no envio
                return "Ocorreu um erro no envio da circular! \r\n\r\nConfira abaixo o resultado do envio.";
            }
        }
    }
}