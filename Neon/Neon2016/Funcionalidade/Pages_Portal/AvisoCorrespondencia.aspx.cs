﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;
using System.Configuration;
using System.Text.RegularExpressions;
using DevExpress.Web;
using PortalUtil;
using FuncionalidadeUtil.Datasets;

namespace Portal
{
    public partial class AvisoCorrespondencia : System.Web.UI.Page
    {
        private dFuncionalidade dFun = new dFuncionalidade();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        Regex rgEmail = new Regex(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dFun = new dFuncionalidade(Cred.EMP);

            //if (!IsPostBack)
            //{
                //Carrega tela
                Carregar();
            //}
        }

        private void Carregar()
        {
            //Carrega os destinatarios
            dFun.CondominosTableAdapter.Fill(dFun.Condominos, Cred.CON);
            grdCondominos.DataSource = dFun.Condominos;
            grdCondominos.FilterExpression = "Email <> ''";
            grdCondominos.DataBind();

            //Limpa campos
            txtMensagem.Text = "";
        }

        protected void cbpEmail_Callback(object sender, CallbackEventArgsBase e)
        {
            //Envia o email
            lblMsg.Text = Enviar();
        }

        protected void cbpEmail_CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpMsg"] = lblMsg.Text;
        }

        protected string Enviar()
        {
            try
            {
                //Cria SMTP
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Verifica SMTP de dominio proprio e seta dados de envio especificos
                if (dFun.ST_SMTpTableAdapter.Fill(dFun.ST_SMTp, Cred.CON) > 0)
                {
                    SmtpClient1.Host = dFun.ST_SMTp[0].SMTpHost;
                    SmtpClient1.Credentials = new System.Net.NetworkCredential(dFun.ST_SMTp[0].SMTpUser, dFun.ST_SMTp[0].SMTpPass);
                    SmtpClient1.Port = Convert.ToInt32(dFun.ST_SMTp[0].SMTpPort);
                    if (!dFun.ST_SMTp[0].IsSMTpRemetenteNull()) strRemetente = dFun.ST_SMTp[0].SMTpRemetente;
                }

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Aviso de Correspondência";
                    email.IsBodyHtml = true;
                    string strMsg = string.Format("<html>\r\n" +
                                                  "   <head></head>\r\n" +
                                                  "   <body style='font: 11pt Calibri'>\r\n" +
                                                  "      <p style='font-size: 14pt'><b>AVISO DE CORRESPONDÊNCIA</b></p>\r\n" +
                                                  "      <p><b>Data:</b> {0}</p>\r\n" +
                                                  "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                                  "      [DESTINATARIO]\r\n" +
                                                  "      <p><b>FAVOR RETIRAR CORRESPONDÊNCIA NA PORTARIA</b></p>\r\n" +
                                                  (txtMensagem.Text.Trim() != "" ? "      <p><b>Mensagem:</b><br/>{2}</p>\r\n" : "") +
                                                  "   </body>\r\n" +
                                                  "</html>",
                                                  DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo), 
                                                  Server.HtmlEncode(txtMensagem.Text).Replace("\n", "<br/>"));

                    //To - Verifica destinatario e manda um por um
                    List<object> selectedRows = grdCondominos.GetSelectedFieldValues(new string[] { "Email", "Bloco", "Apartamento" });
                    foreach (object[] row in selectedRows)
                    {
                        //Verifica e-mail valido
                        if (rgEmail.IsMatch(row[0].ToString().Trim()))
                        {
                            //Adiciona apenas este e-mail
                            email.To.Clear();
                            email.To.Add(row[0].ToString().Replace(";", ","));

                            //Seta corpo da mensagem
                            email.Body = strMsg.Replace("[DESTINATARIO]",
                                                        ((row[1].ToString().ToUpper() == "SB") ? "" : "<p><b>Bloco:</b> " + row[1].ToString() + "</p>\r\n") +
                                                        "<p><b>Unidade:</b> " + row[2].ToString() + "</p>\r\n");

                            //Envia o e-mail
                            SmtpClient1.Send(email);
                            Thread.Sleep(200);
                        }
                    }

                    //Sucesso no envio
                    return "Aviso enviado com sucesso!";
                }
            }
            catch (Exception)
            {
                //Erro no envio
                return "Não foi possível enviar o aviso! \r\nPor favor, tente novamente.";
            }
        }
    }
}