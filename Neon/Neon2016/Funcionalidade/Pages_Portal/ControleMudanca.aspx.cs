﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;
using System.Configuration;
using DevExpress.Web;
using PortalUtil;
using FuncionalidadeUtil.Datasets;

namespace Funcionalidade.Pages_Portal
{
    public partial class ControleMudanca : System.Web.UI.Page
    {
        private dFuncionalidade dFun = new dFuncionalidade();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dFun = new dFuncionalidade(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Preenche campos
            if (!Cred.rowUsuario.IsEmailNull())
                txtEmail.Text = Cred.rowUsuario.Email;
            if (!Cred.rowUsuario.IsNomeNull())
                txtNomeCondomino.Text = Cred.rowUsuario.Nome;

            //Limita campo de data (a partir de hoje)
            dteData.MinDate = DateTime.Now.Date;

            //Seta termos
            lnkTermos.NavigateUrl= ConfigurationManager.AppSettings["PathCondominios"] + string.Format("{0}_{1}/RegrasMudanca.pdf", Cred.EMP, Cred.rowUsuario.CONCodigo.Replace(".", "_"));

            //Seta mensagem para novo condômino
            lblCondicao2.Text = String.Format("2 - Se for um novo condômino, por favor entrar em contato com a Neon Administradora pelo telefone \n{0}, para alteração cadastral e emissão de autorização para entrada no condomínio", Cred.EMP == 1 ? "(11) 3376-7900" : "(11) 4990-1394");
        }

        protected void chkTermos_CheckedChanged(object sender, EventArgs e)
        {
            //Habilita ou nao o botao de enviar
            cmdEnviar.Enabled = chkTermos.Checked;
        }

        protected void cbpEmail_Callback(object sender, CallbackEventArgsBase e)
        {
            //Envia o email
            lblMsg.Text = Enviar();
        }

        protected void cbpEmail_CustomJSProperties(object sender, CustomJSPropertiesEventArgs e)
        {
            e.Properties["cpMsg"] = lblMsg.Text;
        }

        protected string Enviar()
        {
            try
            {
                //Cria SMTP
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Verifica SMTP de dominio proprio e seta dados de envio especificos
                if (dFun.ST_SMTpTableAdapter.Fill(dFun.ST_SMTp, Cred.CON) > 0)
                {
                    SmtpClient1.Host = dFun.ST_SMTp[0].SMTpHost;
                    SmtpClient1.Credentials = new System.Net.NetworkCredential(dFun.ST_SMTp[0].SMTpUser, dFun.ST_SMTp[0].SMTpPass);
                    SmtpClient1.Port = Convert.ToInt32(dFun.ST_SMTp[0].SMTpPort);
                    if (!dFun.ST_SMTp[0].IsSMTpRemetenteNull()) strRemetente = dFun.ST_SMTp[0].SMTpRemetente;
                }

                //Monta e envia e-mail
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    dFun.ST_EMailEnvioTableAdapter.Fill(dFun.ST_EMailEnvio, Cred.CON, (int)Tipos.TipoEmailEnvio.ControleMudanca);
                    foreach (dFuncionalidade.ST_EMailEnvioRow row in dFun.ST_EMailEnvio.Rows)
                        email.To.Add(row.EMEEmail.ToString().Replace(";", ","));

                    //Replay
                    email.ReplyToList.Add(new System.Net.Mail.MailAddress(txtEmail.Text.Replace(";", ",")));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Comunicação de Mudança";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>COMUNICAÇÃO DE MUDANÇA</b></p>\r\n" +
                                               "      <p><b>Data de Envio:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               ((Cred.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : "      <p><b>Bloco:</b> {2}</p>\r\n") +
                                               "      <p><b>Unidade:</b> {3}</p>\r\n" +
                                               "      \r\n" +
                                               "      <p><b>Nome do condômino:</b> {4}</p>\r\n" +
                                               "      <p><b>Responsável da mudança:</b> {5}</p>\r\n" +
                                               "      <p><b>E-mail:</b> {6}</p>\r\n" +
                                               "      <p><b>Telefone 1:</b> {7}</p>\r\n" +
                                               "      <p><b>Telefone 2:</b> {8}</p>\r\n" +
                                               "      <p><b>Celular:</b> {9}</p>\r\n" +
                                               "      <p><b>Data da mudança:</b> {10}</p>\r\n" +
                                               "      <p><b>Horário:</b> {11}</p>\r\n" +
                                               "      <p><b>Placa do caminhão:</b> {12}</p>\r\n" +
                                               "      <p><b>Empresa contratada:</b> {13}</p>\r\n" +
                                               "      <p><b>Uso de elevador:</b> {14}</p>\r\n" +
                                               "      <p><b>Uso de escada:</b> {15}</p>\r\n" +
                                               "      <p><b>Içamento:</b> {16}</p>\r\n" +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo), 
                                               Server.HtmlEncode(Cred.rowUsuario.BLOCodigo), Server.HtmlEncode(Cred.rowUsuario.APTNumero),
                                               Server.HtmlEncode(txtNomeCondomino.Text), Server.HtmlEncode(txtResponsavel.Text),
                                               Server.HtmlEncode(txtEmail.Text), Server.HtmlEncode(txtTelefone1.Text), Server.HtmlEncode(txtTelefone2.Text), Server.HtmlEncode(txtCelular.Text),
                                               Server.HtmlEncode(dteData.Text), Server.HtmlEncode(tmeHorario.Text), Server.HtmlEncode(txtPlaca.Text), Server.HtmlEncode(txtEmpresa.Text),
                                               chkElevador.Checked ? "SIM" : "NÃO", chkEscada.Checked ? "SIM" : "NÃO", chkIcamento.Checked ? "SIM" : "NÃO");
                    
                    //Envia email
                    SmtpClient1.Send(email);
                    Thread.Sleep(1000);

                    //Reinicia tela
                    txtEmail.Text = "";
                    if (!Cred.rowUsuario.IsEmailNull())
                        txtEmail.Text = Cred.rowUsuario.Email;
                    txtNomeCondomino.Text = "";
                    if (!Cred.rowUsuario.IsNomeNull())
                        txtNomeCondomino.Text = Cred.rowUsuario.Nome;
                    txtResponsavel.Text = "";
                    dteData.Text = "";
                    tmeHorario.Text = "";
                    txtPlaca.Text = "";
                    txtTelefone1.Text = "";
                    txtTelefone2.Text = "";
                    txtCelular.Text = "";
                    txtEmpresa.Text = "";
                    chkElevador.Checked = chkEscada.Checked = chkIcamento.Checked = false;
                    cmdEnviar.Enabled = chkTermos.Checked = false;

                    //Sucesso no envio
                    return "Solicitação enviada com sucesso!";
                }
            }
            catch (Exception)
            {
                //Erro no envio
                return "Não foi possível enviar sua solicitação! \r\nPor favor, tente novamente.";
            }
        }
    }
}