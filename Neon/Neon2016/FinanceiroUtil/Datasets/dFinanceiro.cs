﻿namespace FinanceiroUtil.Datasets
{
    public partial class dFinanceiro
    {
        public int EMP;

        public dFinanceiro(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dFinanceiroTableAdapters.ContaCorrenTeTableAdapter taContaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dFinanceiroTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (taContaCorrenTeTableAdapter == null)
                {
                    taContaCorrenTeTableAdapter = new dFinanceiroTableAdapters.ContaCorrenTeTableAdapter();
                    if (EMP == 3)
                        taContaCorrenTeTableAdapter.Connection.ConnectionString = taContaCorrenTeTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taContaCorrenTeTableAdapter;
            }
        }

        private dFinanceiroTableAdapters.SaldoContaCorrenteTableAdapter taSaldoContaCorrenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoContaCorrente
        /// </summary>
        public dFinanceiroTableAdapters.SaldoContaCorrenteTableAdapter SaldoContaCorrenteTableAdapter
        {
            get
            {
                if (taSaldoContaCorrenteTableAdapter == null)
                {
                    taSaldoContaCorrenteTableAdapter = new dFinanceiroTableAdapters.SaldoContaCorrenteTableAdapter();
                    if (EMP == 3)
                        taSaldoContaCorrenteTableAdapter.Connection.ConnectionString = taSaldoContaCorrenteTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taSaldoContaCorrenteTableAdapter;
            }
        }

        private dFinanceiroTableAdapters.ContaCorrenteDetalheTableAdapter taContaCorrenteDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteDetalhe
        /// </summary>
        public dFinanceiroTableAdapters.ContaCorrenteDetalheTableAdapter ContaCorrenteDetalheTableAdapter
        {
            get
            {
                if (taContaCorrenteDetalheTableAdapter == null)
                {
                    taContaCorrenteDetalheTableAdapter = new dFinanceiroTableAdapters.ContaCorrenteDetalheTableAdapter();
                    if (EMP == 3)
                        taContaCorrenteDetalheTableAdapter.Connection.ConnectionString = taContaCorrenteDetalheTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taContaCorrenteDetalheTableAdapter;
            }
        }

        private dFinanceiroTableAdapters.ExtratoChequesTableAdapter taExtratoChequesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ExtratoCheques
        /// </summary>
        public dFinanceiroTableAdapters.ExtratoChequesTableAdapter ExtratoChequesTableAdapter
        {
            get
            {
                if (taExtratoChequesTableAdapter == null)
                {
                    taExtratoChequesTableAdapter = new dFinanceiroTableAdapters.ExtratoChequesTableAdapter();
                    if (EMP == 3)
                        taExtratoChequesTableAdapter.Connection.ConnectionString = taExtratoChequesTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taExtratoChequesTableAdapter;
            }
        }

        private dFinanceiroTableAdapters.ExtratoBoletosTableAdapter taExtratoBoletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ExtratoBoletos
        /// </summary>
        public dFinanceiroTableAdapters.ExtratoBoletosTableAdapter ExtratoBoletosTableAdapter
        {
            get
            {
                if (taExtratoBoletosTableAdapter == null)
                {
                    taExtratoBoletosTableAdapter = new dFinanceiroTableAdapters.ExtratoBoletosTableAdapter();
                    if (EMP == 3)
                        taExtratoBoletosTableAdapter.Connection.ConnectionString = taExtratoBoletosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taExtratoBoletosTableAdapter;
            }
        }

        private dFinanceiroTableAdapters.FrancesinhaTableAdapter taFrancesinhaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Francesinha
        /// </summary>
        public dFinanceiroTableAdapters.FrancesinhaTableAdapter FrancesinhaTableAdapter
        {
            get
            {
                if (taFrancesinhaTableAdapter == null)
                {
                    taFrancesinhaTableAdapter = new dFinanceiroTableAdapters.FrancesinhaTableAdapter();
                    if (EMP == 3)
                        taFrancesinhaTableAdapter.Connection.ConnectionString = taFrancesinhaTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taFrancesinhaTableAdapter;
            }
        }

        private dFinanceiroTableAdapters.BALancetesTableAdapter taBALancetesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BALancetes
        /// </summary>
        public dFinanceiroTableAdapters.BALancetesTableAdapter BALancetesTableAdapter
        {
            get
            {
                if (taBALancetesTableAdapter == null)
                {
                    taBALancetesTableAdapter = new dFinanceiroTableAdapters.BALancetesTableAdapter();
                    if (EMP == 3)
                        taBALancetesTableAdapter.Connection.ConnectionString = taBALancetesTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taBALancetesTableAdapter;
            }
        }

        private dFinanceiroTableAdapters.CONDOMINIOSTableAdapter taCONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dFinanceiroTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (taCONDOMINIOSTableAdapter == null)
                {
                    taCONDOMINIOSTableAdapter = new dFinanceiroTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP == 3)
                        taCONDOMINIOSTableAdapter.Connection.ConnectionString = taCONDOMINIOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCONDOMINIOSTableAdapter;
            }
        }

        /// <summary>
        /// Carrega a tabela balancetes
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public int CarregaTotais(int CON)
        {
            int retorno = BALancetesTableAdapter.Fill(BALancetes, CON);
            PopulaCompetenciaSTR();
            return retorno;
        }

        private void PopulaCompetenciaSTR()
        {
            foreach (BALancetesRow BALrow in BALancetes)
            {
                int mes = BALrow.BALCompet % 100;
                int ano = BALrow.BALCompet / 100; ;
                System.DateTime data = new System.DateTime(ano, mes, 1);
                BALrow.competenciaSTR = string.Format("{0:MMMM - yyyy} ({0:MM/yyyy})", data);
            }
        }
    }
}
