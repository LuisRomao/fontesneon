﻿namespace Portal.Datasets
{
    partial class dPortal
    {
        public int EMP;

        public dPortal(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dPortalTableAdapters.CONDOMINIOSTableAdapter taCONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dPortalTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (taCONDOMINIOSTableAdapter == null)
                {
                    taCONDOMINIOSTableAdapter = new dPortalTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP == 3)
                        taCONDOMINIOSTableAdapter.Connection.ConnectionString = taCONDOMINIOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCONDOMINIOSTableAdapter;
            }
        }

        private dPortalTableAdapters.MENuTableAdapter taMENuTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: MENu
        /// </summary>
        public dPortalTableAdapters.MENuTableAdapter MENuTableAdapter
        {
            get
            {
                if (taMENuTableAdapter == null)
                {
                    taMENuTableAdapter = new dPortalTableAdapters.MENuTableAdapter();
                };
                return taMENuTableAdapter;
            }
        }
    }
}
