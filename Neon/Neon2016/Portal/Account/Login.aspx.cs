using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using DevExpress.Web;
using PortalUtil;

namespace Portal.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta o tema da pagina
            ASPxWebControl.GlobalTheme = "Moderno";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Verifica se tem usuario e senha na sessao
                if (Session["User"] != null || Session["Pass"] != null)
                {
                    //Seta na tela
                    txtUsuario.Text = Session["User"].ToString();
                    txtSenha.Text = Session["Pass"].ToString();

                    //Tenta login
                    if (txtUsuario.Text != "" && txtSenha.Text != "")
                        cmdLogin_Click(null, null);
                }
            }

            //Seta botao default para o enter no botao de login
            this.Form.DefaultButton = this.cmdLogin.UniqueID;
        }

        protected void cmdLogin_Click(object sender, EventArgs e)
        {
            //Limpa mensagem
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(txtUsuario.Text, txtSenha.Text);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = txtUsuario.Text;
                Session["Pass"] = txtSenha.Text;

                //Verifica destino (pagina inicial do portal ou pagina especifica)
                FormsAuthentication.SetAuthCookie(txtUsuario.Text, false);
                if (string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                    Response.Redirect("~/Pages_Portal/Portal.aspx");
                else
                    FormsAuthentication.RedirectFromLoginPage(txtUsuario.Text, false);
            }
            else
            {
                //Erro na autenticacao
                lblMsg.Text = "Usu�rio / Senha n�o cadastrados.";
                panMsg.Visible = true;
            }
        }
    }
}