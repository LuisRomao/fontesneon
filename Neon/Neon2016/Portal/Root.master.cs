using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using DevExpress.Web;
using System.Web.Security;
using PortalUtil;
using Portal.Datasets;

namespace Portal
{
    public partial class RootMaster : System.Web.UI.MasterPage
    {
        private dPortal dPor = new dPortal();

        private Credencial Cred
        {
            get
            {
                return (Credencial)Session["Cred"];
            }
        }

        private ArrayList UsuariosCondominio
        {
            get
            {
                return new ArrayList(new Credencial.TipoUsuario[] { Credencial.TipoUsuario.Sindico,
                                                                    Credencial.TipoUsuario.Subsindico,
                                                                    Credencial.TipoUsuario.ProprietarioSindico,
                                                                    Credencial.TipoUsuario.InquilinoSindico,
                                                                    Credencial.TipoUsuario.Proprietario,
                                                                    Credencial.TipoUsuario.Inquilino,
                                                                    Credencial.TipoUsuario.GerentePredial,
                                                                    Credencial.TipoUsuario.Portaria,
                                                                    Credencial.TipoUsuario.Zelador});
            }
        }

        string strPasta = "";
        string strPaginaInicial = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Retira a autenticacao se nao tem credencial de usuario
            if (HttpContext.Current.User.Identity.IsAuthenticated && Cred == null)
            {
                //Desloga e vai para a pagina de login
                FormsAuthentication.SignOut();
                Response.Redirect("~/Account/Login.aspx");
            }

            //Preenche dados do usuario logado no header
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                //Verifica se esta redirecionando com o EMP / CON
                dPortal.CONDOMINIOSRow rowCON = null;
                if (Cred.EMP != 0 && Cred.CON != 0)
                {
                    //Seta dados do condominio
                    dPor.EMP = Cred.EMP;
                    dPor.CONDOMINIOSTableAdapter.Fill(dPor.CONDOMINIOS, Cred.CON, Cred.EMP);
                    rowCON = dPor.CONDOMINIOS.FindByCON(Cred.CON);

                    //Seta dados do condominio (caso tenha condominio selecionado)
                    if (rowCON != null)
                    {
                        strPasta = string.Format("{0}_{1}", Cred.EMP, rowCON.CONCodigo.Replace(".", "_"));
                        if (!rowCON.IsSMTpDominioNull())
                        {
                            lblCONNome.Text = rowCON.CONNome.ToUpper();
                            imgLogo.ImageUrl = ConfigurationManager.AppSettings["PathCondominios"] + String.Format("{0}/images/logo.png", strPasta);
                            panCopyrightCondominio.Visible = true;
                            panCopyrightNeon.Visible = false;
                            strPaginaInicial = String.Format("~/Pages_Portal/Condominios/{0}/{1}", strPasta, "inicial1.htm");
                        }
                        else
                            strPaginaInicial = String.Format("~/Pages_Portal/Aviso.aspx");
                        ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase = rowCON.CFUCorBase;
                        ASPxRoundPanel panWelcomeBox = (ASPxRoundPanel)HeadLoginView.FindControl("panWelcomeBox");
                        panWelcomeBox.BackColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
                    }
                }

                //Preenche dados do usuario logado no header
                PreencheUsuarioLogado();

                //Monta Menu
                MontaMenu(rowCON);
            }

            //Seta menu e tela de conteudo
            if (Request.Browser.IsMobileDevice)
            {
                ASPxRoundPanel panWelcomeBox = (ASPxRoundPanel)HeadLoginView.FindControl("panWelcomeBox");
                if (Request.UserAgent.ToLower().Contains("ipad") || Request.UserAgent.ToLower().Contains("iphone"))
                    ContentSplitter.FullscreenMode = false;
                else
                    ContentSplitter.FullscreenMode = true;
                nvbSite.Font.Size = nvbCondominio.Font.Size = nvbSindico.Font.Size = nvbFornecedor.Font.Size = nvbAdmin.Font.Size = 8;
            }
            else
                ContentSplitter.FullscreenMode = true;

            //Exibe pagina inicial de condominio proprio, apenas se pagina inicial existir, se for usuario de condominio e se nao for redirecionamento com pagina setada em PAGE
            if (!IsPostBack && strPaginaInicial != "" && UsuariosCondominio.Contains(Cred.TUsuario) && Request.QueryString["Page"] == null)
                ContentSplitter.GetPaneByName("MainFrame").ContentUrl = strPaginaInicial;
        }

        protected void PreencheUsuarioLogado()
        {
            //Pega campos do LoginView
            ASPxRoundPanel panWelcomeBox = (ASPxRoundPanel)HeadLoginView.FindControl("panWelcomeBox");
            Label lblNome = (Label)panWelcomeBox.FindControl("lblNome");
            Label lblCondominio = (Label)panWelcomeBox.FindControl("lblCondominio");
            Label lblBloco = (Label)panWelcomeBox.FindControl("lblBloco");
            Label lblApartamento = (Label)panWelcomeBox.FindControl("lblApartamento");
            Label lblTipoUsuario = (Label)panWelcomeBox.FindControl("lblTipoUsuario");

            //Seta dados do usuario logado
            lblNome.Text = Cred.rowUsuario.Nome;
            lblCondominio.Text = Cred.rowUsuario.CONNome;
            if (Cred.rowUsuario.BLOCodigo != "")
                lblBloco.Text = (Cred.rowUsuario.BLOCodigo.ToUpper() == "SB") ? "" : "| Bloco: " + Cred.rowUsuario.BLOCodigo;
            if (Cred.rowUsuario.APTNumero != "")
                lblApartamento.Text = "| Apto: " + Cred.rowUsuario.APTNumero;

            //Seta tipo do usuario logado
            if (Cred.TUsuario == Credencial.TipoUsuario.Administradora)
                lblTipoUsuario.Text = "Admin";
            else if (Cred.TiposSindico.Contains(Cred.TUsuario))
                lblTipoUsuario.Text = "S�ndico";
            else if (Cred.TUsuario == Credencial.TipoUsuario.Proprietario)
                lblTipoUsuario.Text = "Propriet�rio";
            else if (Cred.TUsuario == Credencial.TipoUsuario.Inquilino)
                lblTipoUsuario.Text = "Inquilino";
            else if (Cred.TUsuario == Credencial.TipoUsuario.Fornecedor)
                lblTipoUsuario.Text = "Fornecedor";
            else if (Cred.TUsuario == Credencial.TipoUsuario.Imobiliaria)
                lblTipoUsuario.Text = "Imobili�ria";
            else if (Cred.TUsuario == Credencial.TipoUsuario.AdvogadoAdministradora)
                lblTipoUsuario.Text = "Advogado Administradora";
            else if (Cred.TUsuario == Credencial.TipoUsuario.Advogado)
                lblTipoUsuario.Text = "Advogado";
            else if (Cred.TUsuario == Credencial.TipoUsuario.Subsindico)
                lblTipoUsuario.Text = "Subs�ndico";
            else if (Cred.TUsuario == Credencial.TipoUsuario.GerentePredial)
                lblTipoUsuario.Text = "Gerente Predial";
            else if (Cred.TUsuario == Credencial.TipoUsuario.Zelador)
                lblTipoUsuario.Text = "Zelador";
            else if (Cred.TUsuario == Credencial.TipoUsuario.Portaria)
                lblTipoUsuario.Text = "Portaria";
            else
                lblTipoUsuario.Text = "";
            lblTipoUsuario.Text = ((lblCondominio.Text == "" && lblBloco.Text == "" && lblApartamento.Text == "") ? "" : (lblTipoUsuario.Text == "" ? "" : "| ")) + lblTipoUsuario.Text;
        }

        protected void MontaMenu(dPortal.CONDOMINIOSRow rowCON)
        {
            //Monta menus de acordo com o perfil
            if (Cred != null)
            {
                //Verifica se tem condominio definido
                if (rowCON != null)
                {
                    //Menu Site
                    MontaMenuSite(rowCON);
                    for (int i = 0; i < nvbSite.Groups.Count; i++)
                        nvbSite.Groups[i].Expanded = false;
                    if (nvbSite.Groups.Count > 0) panSite.Visible = panSiteBr.Visible = true;

                    //Menu Condominos
                    MontaMenuCondominio(Cred.TUsuario, rowCON);
                    for (int i = 0; i < nvbCondominio.Groups.Count; i++)
                        nvbCondominio.Groups[i].Expanded = false;
                    if (nvbCondominio.Groups.Count > 0) panCondominio.Visible = panCondominioBr.Visible = true;

                    //Menu Sindico
                    MontaMenuSindico(Cred.TUsuario, rowCON);
                    for (int i = 0; i < nvbSindico.Groups.Count; i++)
                        nvbSindico.Groups[i].Expanded = false;
                    if (nvbSindico.Groups.Count > 0) panSindico.Visible = panSindicoBr.Visible = true;
                }

                //Verifica se tem condominio definido ou e imobiliaria
                if (rowCON != null || Cred.TUsuario == Credencial.TipoUsuario.Imobiliaria)
                {
                    //Menu Fornecedor
                    MontaMenuFornecedor(Cred.TUsuario, rowCON);
                    for (int i = 0; i < nvbFornecedor.Groups.Count; i++)
                        nvbFornecedor.Groups[i].Expanded = false;
                    if (nvbFornecedor.Groups.Count > 0) panFornecedor.Visible = panFornecedorBr.Visible = true;
                }

                //Menu Admin
                MontaMenuAdmin(Cred.TUsuario, rowCON);
                for (int i = 0; i < nvbAdmin.Groups.Count; i++)
                    nvbAdmin.Groups[i].Expanded = false;
                if (nvbAdmin.Groups.Count > 0) panAdmin.Visible = panAdminBr.Visible = true;
            }
        }

        private string URLMenuSite(string strLink, string strPasta)
        {
            if (strLink.Contains("http:"))
                return strLink;
            else
            {
                return string.Format("~/Pages_Portal/Condominios/{0}/{1}", strPasta, strLink);
            }
        }

        private void MontaMenuSite(dPortal.CONDOMINIOSRow rowCON)
        {
            //Pega os menus adicionais Neon
            int i = -1;
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.Proprietario:
                case Credencial.TipoUsuario.Inquilino:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                    //Pega os menus Site
                    dPor.MENuTableAdapter.Fill(dPor.MENu, Cred.CON, Cred.EMP);
                    foreach (dPortal.MENuRow row in dPor.MENu.Rows)
                    {
                        //Exibe opcoes menu Neon
                        if (row.MENNeon)
                        {
                            i++;
                            nvbSite.Groups.Add(row.MENTitulo);
                            nvbSite.Groups[i].NavigateUrl = URLMenuSite(row.MENURL, strPasta);
                            nvbSite.Groups[i].HeaderImage.Url = "~/Content/Images/ico-site.png";
                        }
                    }
                    break;
            }
        }

        private void MontaMenuCondominio(Credencial.TipoUsuario typTipoUsuario, dPortal.CONDOMINIOSRow rowCON)
        {
            //Exibe opcoes menu de acordo com perfil
            int i = -1;
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.Proprietario:
                case Credencial.TipoUsuario.Inquilino:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                case Credencial.TipoUsuario.AdvogadoAdministradora:
                    i++;
                    nvbCondominio.Groups.Add("Cadastro");
                    nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-house.png";
                    nvbCondominio.Groups[i].Items.Add("Condom�nio");
                    nvbCondominio.Groups[i].Items[0].NavigateUrl = "~/Pages_Portal/CadastroCondominio.aspx";
                    if (Cred.TUsuario != Credencial.TipoUsuario.AdvogadoAdministradora && Cred.rowUsuario.APT != 0 && Cred.rowUsuario.PES != 0)
                    {
                        nvbCondominio.Groups[i].Items.Add("Unidade");
                        nvbCondominio.Groups[i].Items[1].NavigateUrl = "~/Pages_Portal/CadastroUnidade.aspx";
                    }
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.Proprietario:
                case Credencial.TipoUsuario.Inquilino:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                    i++;
                    nvbCondominio.Groups.Add("Corpo Diretivo");
                    nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-mans.png";
                    nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/CadastroCorpoDiretivo.aspx";
                    if (Cred.rowUsuario.APT != 0  && Cred.rowUsuario.APT != 36282) //Foi ocultada a op��o de boletos para o Apt 41 do ALCANT a pedido da IRIS
                    {
                        i++;
                        nvbCondominio.Groups.Add("Boletos Segunda Via Acordos");
                        nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-barcode.png";
                        nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/Boleto.aspx";
                    }
                    if (!rowCON.IsCFUFaleConoscoCDNull() && rowCON.CFUFaleConoscoCD || (!rowCON.IsCFUFaleConoscoNull() && rowCON.CFUFaleConosco && rowCON.EMEFaleConosco > 0))
                    {
                        i++;
                        nvbCondominio.Groups.Add("Fale Conosco");
                        nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-speech.png";
                        nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/FaleConosco.aspx";
                    }
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.Proprietario:
                case Credencial.TipoUsuario.Inquilino:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                    i++;
                    nvbCondominio.Groups.Add("Balancete");
                    nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-scales.png";
                    nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/Balancete.aspx";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.Proprietario:
                case Credencial.TipoUsuario.Inquilino:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                case Credencial.TipoUsuario.AdvogadoAdministradora:
                case Credencial.TipoUsuario.Advogado:
                    i++;
                    nvbCondominio.Groups.Add("Documenta��o");
                    nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-folder.png";
                    nvbCondominio.Groups[i].Items.Add("Atas");
                    nvbCondominio.Groups[i].Items[0].NavigateUrl = "~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=0";
                    nvbCondominio.Groups[i].Items.Add("Convoca��o");
                    nvbCondominio.Groups[i].Items[1].NavigateUrl = "~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=1";
                    nvbCondominio.Groups[i].Items.Add("Conven��o");
                    nvbCondominio.Groups[i].Items[2].NavigateUrl = "~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=3";
                    nvbCondominio.Groups[i].Items.Add("Regulamento");
                    nvbCondominio.Groups[i].Items[3].NavigateUrl = "~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=2";
                    nvbCondominio.Groups[i].Items.Add("Outros");
                    nvbCondominio.Groups[i].Items[4].NavigateUrl = "~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=4";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.Proprietario:
                case Credencial.TipoUsuario.Inquilino:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                    i++;
                    nvbCondominio.Groups.Add("Mural de Avisos");
                    nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-notice.png";
                    nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/Aviso.aspx";
                    if (!rowCON.IsCFUControleMudancaNull() && rowCON.CFUControleMudanca)
                    {
                        i++;
                        nvbCondominio.Groups.Add("Controle de Mudan�a");
                        nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-delivery.png";
                        nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/ControleMudanca.aspx";
                    }
                    if (!rowCON.IsCFUAutorizacaoEntradaPrestadorNull() && rowCON.CFUAutorizacaoEntradaPrestador)
                    {
                        i++;
                        nvbCondominio.Groups.Add("Autoriza��o Entrada Prestadores de Servi�o");
                        nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-briefcase.png";
                        nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/AutorizacaoEntradaPrestador.aspx";
                    }
                    if (!rowCON.IsCFUSolicitacaoReparoNull() && rowCON.CFUSolicitacaoReparo)
                    {
                        i++;
                        nvbCondominio.Groups.Add("Solicita��o de Reparo na �rea Comum");
                        nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-fix.png";
                        nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/SolicitacaoReparo.aspx";
                    }
                    if (!rowCON.IsCFUCaixaSugestoesNull() && rowCON.CFUCaixaSugestoes)
                    {
                        i++;
                        nvbCondominio.Groups.Add("Caixa de Sugest�es");
                        nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-lightbulb.png";
                        nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/CaixaSugestoes.aspx";
                    }
                    if (!rowCON.IsCFUClassificadoNull() && rowCON.CFUClassificado)
                    {
                        i++;
                        nvbCondominio.Groups.Add("Classificados Internos");
                        nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-shopping.png";
                        nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/ClassificadoInterno.aspx";
                    }
                    if (!rowCON.IsCFUReservaNull() && rowCON.CFUReserva)
                        if (!rowCON.CFUReservaImplantacao)
                        { 
                            i++;
                            nvbCondominio.Groups.Add("Reserva �rea Comum");
                            nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-check.png";
                            nvbCondominio.Groups[i].NavigateUrl = "~/Pages_Portal/Reserva.aspx";
                        }

                    //Pega os menus adicionais Condominio
                    dPor.MENuTableAdapter.Fill(dPor.MENu, Cred.CON, Cred.EMP);
                    foreach (dPortal.MENuRow row in dPor.MENu.Rows)
                    {
                        //Exibe opcoes menu Condominio
                        if (row.MENCondomino)
                        {
                            i++;
                            nvbCondominio.Groups.Add(row.MENTitulo);
                            nvbCondominio.Groups[i].NavigateUrl = URLMenuSite(row.MENURL, strPasta);
                            nvbCondominio.Groups[i].HeaderImage.Url = "~/Content/Images/ico-site.png";
                        }
                    }
                    break;
            }
        }

        private void MontaMenuSindico(Credencial.TipoUsuario typTipoUsuario, dPortal.CONDOMINIOSRow rowCON)
        {
            //Exibe opcoes menu de acordo com perfil
            int i = -1;
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.AdvogadoAdministradora:
                    i++;
                    nvbSindico.Groups.Add("Extrato Banc�rio");
                    nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-calculator.png";
                    nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/ExtratoBancario.aspx";
                    i++;
                    nvbSindico.Groups.Add("Francesinha");
                    nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-list.png";
                    nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/Francesinha.aspx";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                case Credencial.TipoUsuario.Portaria:
                case Credencial.TipoUsuario.AdvogadoAdministradora:
                case Credencial.TipoUsuario.Advogado:
                    i++;
                    nvbSindico.Groups.Add("Cond�minos");
                    nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-house.png";
                    nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/Condominos.aspx";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.AdvogadoAdministradora:
                case Credencial.TipoUsuario.Advogado:
                    i++;
                    nvbSindico.Groups.Add("Acordos");
                    nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-deal.png";
                    nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/Acordos.aspx";
                    i++;
                    nvbSindico.Groups.Add("Inadimplentes");
                    nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-blacklist.png";
                    nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/Inadimplentes.aspx";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                    i++;
                    nvbSindico.Groups.Add("Cheques");
                    nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-paper.png";
                    nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/SolicitacaoCheque.aspx";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                    if (!rowCON.IsCFUClassificadoNull() && rowCON.CFUClassificado)
                    {
                        i++;
                        nvbSindico.Groups.Add("Classificados");
                        nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-shopping.png";
                        nvbSindico.Groups[i].Items.Add("Incluir Cond�mino");
                        nvbSindico.Groups[i].Items[0].NavigateUrl = "~/Pages_Portal/ClassificadoIncluirCondomino.aspx";
                        nvbSindico.Groups[i].Items.Add("Buscar Cond�mino");
                        nvbSindico.Groups[i].Items[1].NavigateUrl = "~/Pages_Portal/ClassificadoBuscarCondomino.aspx";
                    }
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.AdvogadoAdministradora:
                case Credencial.TipoUsuario.Advogado:
                    i++;
                    nvbSindico.Groups.Add("Contratos Ativos");
                    nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-folder.png";
                    nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/CadastroDocumentacao.aspx?Tipo=5";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                case Credencial.TipoUsuario.Portaria:
                    if (!rowCON.IsCFUAvisoCorrespondenciaNull() && rowCON.CFUAvisoCorrespondencia)
                    {
                        i++;
                        nvbSindico.Groups.Add("Aviso de Correspond�ncia");
                        nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-mail.png";
                        nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/AvisoCorrespondencia.aspx";
                    }
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                    if (!rowCON.IsCFUCircularesNull() && rowCON.CFUCirculares)
                    {
                        i++;
                        nvbSindico.Groups.Add("Circulares");
                        nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-mailopened.png";
                        nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/Circular.aspx";
                    }
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                    if (!rowCON.IsCFUCadastroEmailNull() && rowCON.CFUCadastroEmail)
                    {
                        i++;
                        nvbSindico.Groups.Add("Cadastro de E-mail");
                        nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-datamail.png";
                        nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/CadastroEmail.aspx";
                    }
                    if (!rowCON.IsCFUCadastroUsuarioNull() && rowCON.CFUCadastroUsuario)
                    {
                        i++;
                        nvbSindico.Groups.Add("Cadastro de Usu�rio");
                        nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-datauser.png";
                        nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/CadastroUsuario.aspx";
                    }
                    i++;
                    nvbSindico.Groups.Add("Cadastro de Aviso");
                    nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-notice.png";
                    nvbSindico.Groups[i].NavigateUrl = "~/Pages_Portal/CadastroAviso.aspx";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Sindico:
                case Credencial.TipoUsuario.ProprietarioSindico:
                case Credencial.TipoUsuario.InquilinoSindico:
                case Credencial.TipoUsuario.Subsindico:
                case Credencial.TipoUsuario.GerentePredial:
                case Credencial.TipoUsuario.Zelador:
                    if (!rowCON.IsCFUReservaNull() && rowCON.CFUReserva)
                    {
                        i++;
                        nvbSindico.Groups.Add("Reserva �rea Comum");
                        nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-check.png";
                        nvbSindico.Groups[i].Items.Add("Reservas");
                        nvbSindico.Groups[i].Items[0].NavigateUrl = "~/Pages_Portal/Reserva.aspx";
                        nvbSindico.Groups[i].Items.Add("Bloqueio Data");
                        nvbSindico.Groups[i].Items[1].NavigateUrl = "~/Pages_Portal/ReservaBloqueioData.aspx";
                        nvbSindico.Groups[i].Items.Add("Bloqueio Unidade");
                        nvbSindico.Groups[i].Items[2].NavigateUrl = "~/Pages_Portal/ReservaBloqueioUnidade.aspx";
                        nvbSindico.Groups[i].Items.Add("Hist�rico Reservas");
                        nvbSindico.Groups[i].Items[3].NavigateUrl = "~/Pages_Portal/ReservaHistorico.aspx";
                    }
                    //Pega os menus adicionais Sindico
                    dPor.MENuTableAdapter.Fill(dPor.MENu, Cred.CON, Cred.EMP);
                    foreach (dPortal.MENuRow row in dPor.MENu.Rows)
                    {
                        //Exibe opcoes menu Sindico
                        if (row.MENSindico)
                        {
                            i++;
                            nvbSindico.Groups.Add(row.MENTitulo);
                            nvbSindico.Groups[i].NavigateUrl = URLMenuSite(row.MENURL, strPasta);
                            nvbSindico.Groups[i].HeaderImage.Url = "~/Content/Images/ico-site.png";
                        }
                    }
                    break;
            }
        }

        private void MontaMenuFornecedor(Credencial.TipoUsuario typTipoUsuario, dPortal.CONDOMINIOSRow rowCON)
        {
            //Exibe opcoes menu de acordo com perfil
            int i = -1;
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.Imobiliaria:
                    i++;
                    nvbFornecedor.Groups.Add("Loca��o");
                    nvbFornecedor.Groups[i].HeaderImage.Url = "~/Content/Images/ico-house.png";
                    nvbFornecedor.Groups[i].NavigateUrl = "~/Pages_Portal/Locacao.aspx";
                    break;
            }
        }

        private void MontaMenuAdmin(Credencial.TipoUsuario typTipoUsuario, dPortal.CONDOMINIOSRow rowCON)
        {
            //Exibe opcoes menu de acordo com perfil
            int i = -1;
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                case Credencial.TipoUsuario.AdvogadoAdministradora:
                    i++;
                    nvbAdmin.Groups.Add("Condom�nios");
                    nvbAdmin.Groups[i].HeaderImage.Url = "~/Content/Images/ico-house.png";
                    nvbAdmin.Groups[i].NavigateUrl = "~/Pages_Portal/Condominios.aspx";
                    break;
            }
            switch (Cred.TUsuario)
            {
                case Credencial.TipoUsuario.Administradora:
                    i++;
                    nvbAdmin.Groups.Add("Cadastro de Usu�rio");
                    nvbAdmin.Groups[i].HeaderImage.Url = "~/Content/Images/ico-datauser.png";
                    nvbAdmin.Groups[i].NavigateUrl = "~/Pages_Portal/CadastroUsuario.aspx";
                    i++;
                    nvbAdmin.Groups.Add("Cadastro de Aviso");
                    nvbAdmin.Groups[i].HeaderImage.Url = "~/Content/Images/ico-notice.png";
                    nvbAdmin.Groups[i].NavigateUrl = "~/Pages_Portal/CadastroAviso.aspx";
                    i++;
                    nvbAdmin.Groups.Add("Hist�rico Reservas");
                    nvbAdmin.Groups[i].HeaderImage.Url = "~/Content/Images/ico-check.png";
                    nvbAdmin.Groups[i].NavigateUrl = "~/Pages_Portal/ReservaHistorico.aspx";
                    break;
            }
        }

        protected void cmdLogout_Click(object sender, EventArgs e)
        {
            //Retira a autenticacao
            FormsAuthentication.SignOut();

            //Limpa variaveis de sessao
            Session["Cred"] = "";
            Session["User"] = "";
            Session["Pass"] = "";

            //Vai para a pagina home do institucional
            Response.Redirect("~/Pages/Home.aspx");
        }
    }
}