using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalUtil;
using PortalUtil.Datasets;

namespace Portal.Pages_Portal
{
    public partial class Portal : System.Web.UI.Page
    {
        private dCredencial dCre = new dCredencial();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Verifica se redireciona para pagina especifica do portal (dentro do MainFrame)
                if (Request.QueryString["Page"] != null)
                {
                    //Verifica se esta redirecionando com o EMP / CON
                    if (Request.QueryString["EMP"] != null && Request.QueryString["CON"] != null)
                    {
                        //Seta EMP e CON
                        Cred.EMP = Convert.ToInt32(Request.QueryString["EMP"]);
                        Cred.CON = Convert.ToInt32(Request.QueryString["CON"]);

                        //Seta tipo de autenticacao
                        Cred.TAut = (Cred.EMP == 1) ? Credencial.TipoAut.Neon : Credencial.TipoAut.NeonSA;

                        //Seta dados do condominio escolhido nas credenciais
                        dCre.EMP = Cred.EMP;
                        dCre.CONDOMINIOSTableAdapter.Fill(dCre.CONDOMINIOS, Cred.EMP);
                        dCredencial.CONDOMINIOSRow rowCON = dCre.CONDOMINIOS.FindByCON(Cred.CON);
                        Cred.rowUsuario.CON = rowCON.CON;
                        Cred.rowUsuario.CONCodigo = rowCON.CONCodigo;
                        Cred.rowUsuario.CONNome = rowCON.CONNome;
                    }

                    //Redireciona para a pagina
                    DevExpress.Web.ASPxSplitter ContentSplitter = (DevExpress.Web.ASPxSplitter)Master.FindControl("ContentSplitter");
                    ContentSplitter.GetPaneByName("MainFrame").ContentUrl = String.Format("~/Pages_Portal/{0}", Request.QueryString["Page"]);
                }
            }
        }
    }
}