﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Security;
using DevExpress.Web;
using PortalUtil;
using Portal.Datasets;

namespace Portal
{
    public partial class RootSite : System.Web.UI.MasterPage
    {
        private dPortal dPor = new dPortal();

        private string strPasta = "";
        private string strPaginaInicial = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Verifica se esta redirecionando com o EMP / CON
            if (Request.QueryString["EMP"] != null && Request.QueryString["CON"] != null)
            {
                //Seta EMP e CON
                int EMP = Convert.ToInt32(Request.QueryString["EMP"]);
                int CON = Convert.ToInt32(Request.QueryString["CON"]);

                //Seta CON real, se nao confere vai para o site da Neon
                int CONReal = CON / 100;
                if (CON != (100 * CONReal + (CONReal + 3) % 17))
                    Response.Redirect("~/Pages/Home.aspx");
                CON = CONReal;

                //Seta dados do condominio
                dPor.EMP = EMP;
                dPor.CONDOMINIOSTableAdapter.Fill(dPor.CONDOMINIOS, CON, EMP);
                dPortal.CONDOMINIOSRow rowCON = dPor.CONDOMINIOS.FindByCON(CON);

                //Seta dados de condominio proprio caso encontrado
                if (rowCON != null)
                {
                    strPasta = string.Format("{0}_{1}", EMP, rowCON.CONCodigo.Replace(".", "_"));
                    lblCONNome.Text = rowCON.CONNome.ToUpper();
                    imgLogo.ImageUrl = ConfigurationManager.AppSettings["PathCondominios"] + String.Format("{0}/images/logo.png", strPasta);
                    if (!rowCON.IsCFUCorBaseNull() &&  rowCON.CFUCorBase != "")
                    {
                        ASPxWebControl.GlobalThemeBaseColor = rowCON.CFUCorBase;
                        lblAcesse.ForeColor = lblCondominio.ForeColor = System.Drawing.ColorTranslator.FromHtml(rowCON.CFUCorBase);
                    }
                }
                else
                    Response.Redirect("~/Pages/Home.aspx");

                //Monta Menu
                MontaMenu(rowCON);
            }
            else
                Response.Redirect("~/Pages/Home.aspx");

            //Seta menu e tela de conteudo
            if (Request.Browser.IsMobileDevice)
            {
                if (Request.UserAgent.ToLower().Contains("ipad") || Request.UserAgent.ToLower().Contains("iphone"))
                    ContentSplitter.FullscreenMode = false;
                else
                    ContentSplitter.FullscreenMode = true;
                nvbSite.Font.Size = 8;
            }
            else
                ContentSplitter.FullscreenMode = true;

            //Exibe a pagina inicial
            ContentSplitter.GetPaneByName("MainFrame").ContentUrl = strPaginaInicial;
        }
        
        protected void MontaMenu(dPortal.CONDOMINIOSRow rowCON)
        {
            //Menu Site
            MontaMenuSite(rowCON);
            for (int i = 0; i < nvbSite.Groups.Count; i++)
                nvbSite.Groups[i].Expanded = false;
            if (nvbSite.Groups.Count > 0) panSite.Visible = panSiteBr.Visible = true;
        }

        private string URLMenuSite(string strLink, string strPasta)
        {
            if (strLink.Contains("http:"))
                return strLink;
            else
            {
                return string.Format("~/Pages_Portal/Condominios/{0}/{1}", strPasta, strLink);
            }
        }

        private void MontaMenuSite(dPortal.CONDOMINIOSRow rowCON)
        {
            //Pega os menus adicionais Neon
            int i = -1;
            dPor.MENuTableAdapter.Fill(dPor.MENu, rowCON.CON, rowCON.EMP);
            foreach (dPortal.MENuRow row in dPor.MENu.Rows)
            {
                //Exibe opcoes menu Neon
                if (row.MENNeon)
                {
                    i++;
                    nvbSite.Groups.Add(row.MENTitulo);
                    nvbSite.Groups[i].NavigateUrl = URLMenuSite(row.MENURL, strPasta);
                    nvbSite.Groups[i].HeaderImage.Url = "~/Content/Images/ico-site.png";
                    if (strPaginaInicial == "") strPaginaInicial = nvbSite.Groups[i].NavigateUrl;
                }
            }
        }

        protected void cmdLogin_Click(object sender, EventArgs e)
        {
            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(txtUsuario.Text, txtSenha.Text);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = txtUsuario.Text;
                Session["Pass"] = txtSenha.Text;

                //Verifica destino (pagina inicial do portal ou pagina especifica)
                FormsAuthentication.SetAuthCookie(txtUsuario.Text, false);
                if (string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                    Response.Redirect("~/Pages_Portal/Portal.aspx");
                else
                    FormsAuthentication.RedirectFromLoginPage(txtUsuario.Text, false);
            }
            else
            {
                //Erro na autenticacao
            }
        }
    }
}