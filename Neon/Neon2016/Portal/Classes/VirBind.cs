﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using DevExpress.Web;

namespace Portal.Classes
{
    public class VirBind
    {
        public string FormatadorPadraodecimal;
        private SortedList CamposXObj;

        public VirBind(params WebControl[] Controles)
        {
            CamposXObj = new SortedList();
            foreach (WebControl Controle in Controles)
            {
                CamposXObj.Add(Controle.ID.Substring(1).ToUpper(), Controle);
            }
        }

        public void Popula(DataRow row)
        {
            foreach (DataColumn DC in row.Table.Columns)
                if (CamposXObj.ContainsKey(DC.ColumnName.ToUpper()))
                {
                    WebControl Controle = (WebControl)CamposXObj[DC.ColumnName.ToUpper()];
                    if (Controle is Label)
                    {
                        Label CLable = (Label)Controle;
                        if ((FormatadorPadraodecimal != null) && (row[DC]) is decimal)
                            CLable.Text = ((decimal)row[DC]).ToString(FormatadorPadraodecimal);
                        else
                            CLable.Text = row[DC].ToString();
                    }
                    if (Controle is TextBox)
                    {
                        TextBox CTextBox = (TextBox)Controle;
                        if ((FormatadorPadraodecimal != null) && (row[DC]) is decimal)
                            CTextBox.Text = ((decimal)row[DC]).ToString(FormatadorPadraodecimal);
                        else
                            CTextBox.Text = row[DC].ToString();
                        CTextBox.MaxLength = DC.MaxLength;
                    }
                    if (Controle is CheckBox)
                    {
                        CheckBox CCheckBox = (CheckBox)Controle;
                        CCheckBox.Checked = (bool)row[DC];
                    }
                }
        }

        public void Recupera(DataRow row, out SortedList Alteracoes)
        {
            Alteracoes = new SortedList();
            foreach (DataColumn DC in row.Table.Columns)
                if (CamposXObj.ContainsKey(DC.ColumnName.ToUpper()))
                {
                    WebControl Controle = (WebControl)CamposXObj[DC.ColumnName.ToUpper()];
                    if (Controle is Label)
                    {
                        Label CLable = (Label)Controle;
                        if (row[DC].ToString() != CLable.Text)
                        {
                            row[DC] = CLable.Text;
                            Alteracoes.Add(DC.ColumnName.ToUpper(), CLable.Text);
                        }
                    }
                    if (Controle is TextBox)
                    {
                        TextBox CTextBox = (TextBox)Controle;
                        if (CTextBox.Text != row[DC].ToString())
                        {
                            row[DC] = CTextBox.Text;
                            Alteracoes.Add(DC.ColumnName.ToUpper(), CTextBox.Text);
                        }

                    }
                    if (Controle is CheckBox)
                    {
                        CheckBox CCheckBox = (CheckBox)Controle;
                        if ((row[DC] == System.DBNull.Value) || (CCheckBox.Checked != (bool)row[DC]))
                        {
                            row[DC] = CCheckBox.Checked;
                            Alteracoes.Add(DC.ColumnName.ToUpper(), CCheckBox.Checked);
                        }
                    }
                }
        }
    }
}