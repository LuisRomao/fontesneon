﻿namespace Reserva.Datasets
{
    partial class dReservas
    {
        public int EMP;

        public dReservas(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dReservasTableAdapters.ReservaEQuipamentoTableAdapter reservaEQuipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaEQuiPamento
        /// </summary>
        public dReservasTableAdapters.ReservaEQuipamentoTableAdapter ReservaEQuipamentoTableAdapter
        {
            get
            {
                if (reservaEQuipamentoTableAdapter == null)
                {
                    reservaEQuipamentoTableAdapter = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
                };
                return reservaEQuipamentoTableAdapter;
            }
        }

        private dReservasTableAdapters.ReservaBloqueioUnidadeTableAdapter reservaBloqueioUnidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaBloqueioUnidadeTableAdapter
        /// </summary>
        public dReservasTableAdapters.ReservaBloqueioUnidadeTableAdapter ReservaBloqueioUnidadeTableAdapter
        {
            get
            {
                if (reservaBloqueioUnidadeTableAdapter == null)
                {
                    reservaBloqueioUnidadeTableAdapter = new dReservasTableAdapters.ReservaBloqueioUnidadeTableAdapter();
                };
                return reservaBloqueioUnidadeTableAdapter;
            }
        }


        private dReservasTableAdapters.ReservaBloqueioDataTableAdapter reservaBloqueioDataTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaBloqueioDataTableAdapter
        /// </summary>
        public dReservasTableAdapters.ReservaBloqueioDataTableAdapter ReservaBloqueioDataTableAdapter
        {
            get
            {
                if (reservaBloqueioDataTableAdapter == null)
                {
                    reservaBloqueioDataTableAdapter = new dReservasTableAdapters.ReservaBloqueioDataTableAdapter();
                };
                return reservaBloqueioDataTableAdapter;
            }
        }
    }
}
