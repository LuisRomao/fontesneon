﻿namespace Reserva.Datasets
{
    partial class dReserva
    {
        public int EMP;

        public dReserva(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dReservaTableAdapters.CONDOMINIOSTableAdapter taCONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dReservaTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (taCONDOMINIOSTableAdapter == null)
                {
                    taCONDOMINIOSTableAdapter = new dReservaTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP == 3)
                        taCONDOMINIOSTableAdapter.Connection.ConnectionString = taCONDOMINIOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCONDOMINIOSTableAdapter;
            }
        }

        private dReservaTableAdapters.BLOCOSTableAdapter taBLOCOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BLOCOS
        /// </summary>
        public dReservaTableAdapters.BLOCOSTableAdapter BLOCOSTableAdapter
        {
            get
            {
                if (taBLOCOSTableAdapter == null)
                {
                    taBLOCOSTableAdapter = new dReservaTableAdapters.BLOCOSTableAdapter();
                    if (EMP == 3)
                        taBLOCOSTableAdapter.Connection.ConnectionString = taBLOCOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taBLOCOSTableAdapter;
            }
        }

        private dReservaTableAdapters.APARTAMENTOSTableAdapter taAPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dReservaTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (taAPARTAMENTOSTableAdapter == null)
                {
                    taAPARTAMENTOSTableAdapter = new dReservaTableAdapters.APARTAMENTOSTableAdapter();
                    if (EMP == 3)
                        taAPARTAMENTOSTableAdapter.Connection.ConnectionString = taAPARTAMENTOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taAPARTAMENTOSTableAdapter;
            }
        }

        private dReservaTableAdapters.EQuiPamentosTableAdapter taEQuiPamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EQuiPamentos
        /// </summary>
        public dReservaTableAdapters.EQuiPamentosTableAdapter EQuiPamentosTableAdapter
        {
            get
            {
                if (taEQuiPamentosTableAdapter == null)
                {
                    taEQuiPamentosTableAdapter = new dReservaTableAdapters.EQuiPamentosTableAdapter();
                    if (EMP == 3)
                        taEQuiPamentosTableAdapter.Connection.ConnectionString = taEQuiPamentosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taEQuiPamentosTableAdapter;
            }
        }

        private dReservaTableAdapters.EQuiPamentoTableAdapter taEQuiPamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EQuiPamento
        /// </summary>
        public dReservaTableAdapters.EQuiPamentoTableAdapter EQuiPamentoTableAdapter
        {
            get
            {
                if (taEQuiPamentoTableAdapter == null)
                {
                    taEQuiPamentoTableAdapter = new dReservaTableAdapters.EQuiPamentoTableAdapter();
                    if (EMP == 3)
                        taEQuiPamentoTableAdapter.Connection.ConnectionString = taEQuiPamentoTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taEQuiPamentoTableAdapter;
            }
        }


        private dReservaTableAdapters.ST_ReservaEquipamentoHistoricoTableAdapter taST_ReservaEquipamentoHistoricoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_ReservaEquipamentoHistorico
        /// </summary>
        public dReservaTableAdapters.ST_ReservaEquipamentoHistoricoTableAdapter ST_ReservaEquipamentoHistoricoTableAdapter
        {
            get
            {
                if (taST_ReservaEquipamentoHistoricoTableAdapter == null)
                {
                    taST_ReservaEquipamentoHistoricoTableAdapter = new dReservaTableAdapters.ST_ReservaEquipamentoHistoricoTableAdapter();
                    if (EMP == 3)
                        taST_ReservaEquipamentoHistoricoTableAdapter.Connection.ConnectionString = taST_ReservaEquipamentoHistoricoTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_ReservaEquipamentoHistoricoTableAdapter;
            }
        }

        private dReservaTableAdapters.ST_SMTpTableAdapter taST_SMTpTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_SMTp
        /// </summary>
        public dReservaTableAdapters.ST_SMTpTableAdapter ST_SMTpTableAdapter
        {
            get
            {
                if (taST_SMTpTableAdapter == null)
                {
                    taST_SMTpTableAdapter = new dReservaTableAdapters.ST_SMTpTableAdapter();
                    if (EMP == 3)
                        taST_SMTpTableAdapter.Connection.ConnectionString = taST_SMTpTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_SMTpTableAdapter;
            }
        }

        private dReservaTableAdapters.ST_EMailEnvioTableAdapter taST_EMailEnvioTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_EMailEnvio
        /// </summary>
        public dReservaTableAdapters.ST_EMailEnvioTableAdapter ST_EMailEnvioTableAdapter
        {
            get
            {
                if (taST_EMailEnvioTableAdapter == null)
                {
                    taST_EMailEnvioTableAdapter = new dReservaTableAdapters.ST_EMailEnvioTableAdapter();
                    if (EMP == 3)
                        taST_EMailEnvioTableAdapter.Connection.ConnectionString = taST_EMailEnvioTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taST_EMailEnvioTableAdapter;
            }
        }

        private dReservaTableAdapters.BOLetosTableAdapter taBOLetosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLetos
        /// </summary>
        public dReservaTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (taBOLetosTableAdapter == null)
                {
                    taBOLetosTableAdapter = new dReservaTableAdapters.BOLetosTableAdapter();
                    if (EMP == 3)
                        taBOLetosTableAdapter.Connection.ConnectionString = taBOLetosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taBOLetosTableAdapter;
            }
        }

        private dReservaTableAdapters.ACOrdosTableAdapter taACOrdosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ACOrdos
        /// </summary>
        public dReservaTableAdapters.ACOrdosTableAdapter ACOrdosTableAdapter
        {
            get
            {
                if (taACOrdosTableAdapter == null)
                {
                    taACOrdosTableAdapter = new dReservaTableAdapters.ACOrdosTableAdapter();
                    if (EMP == 3)
                        taACOrdosTableAdapter.Connection.ConnectionString = taACOrdosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taACOrdosTableAdapter;
            }
        }
    }
}
