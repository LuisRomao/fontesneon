﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Reservas.NeonOnLine
{
    public partial class ReservasImpressao : System.Web.UI.Page
    {
        private string[] arrMeses = new string[13] { "", "JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO" };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            //Carrega Impressao 
            CarregaImpressao();
            }
        }

        private void CarregaImpressao()
        {
            //Preenche o mes e ano
            lblMesAno.Text = arrMeses[Convert.ToInt16(Request.QueryString["M"])] + " / " + Request.QueryString["A"];

            //Verifica parametros
            DateTime datInicio;
            if (Convert.ToInt16(Request.QueryString["M"]) == DateTime.Now.Month)
            { datInicio = Convert.ToDateTime(DateTime.Now.Date.ToString()); }
            else
            { datInicio = new DateTime(Convert.ToInt16(Request.QueryString["A"]), Convert.ToInt16(Request.QueryString["M"]), 1); }
            DateTime datFim;
            datFim = Convert.ToDateTime((new DateTime(Convert.ToInt16(Request.QueryString["A"]), Convert.ToInt16(Request.QueryString["M"]), DateTime.DaysInMonth(Convert.ToInt16(Request.QueryString["A"]), Convert.ToInt16(Request.QueryString["M"])))).ToString().Replace("00:00:00", "23:59:59"));

            //Inicia parametros da impressao
            dsReservasImpressao.SelectParameters.Clear();
            dsReservasImpressao.SelectParameters.Add("EMP", Convert.ToString(Request.QueryString["EMP"]));
            dsReservasImpressao.SelectParameters.Add("EQP", Convert.ToString(Request.QueryString["E"]));
            dsReservasImpressao.SelectParameters.Add("Inicio", Convert.ToString(datInicio));
            dsReservasImpressao.SelectParameters.Add("Fim", Convert.ToString(datFim));
            dsReservasImpressao.DataBind();
        }
    }
}