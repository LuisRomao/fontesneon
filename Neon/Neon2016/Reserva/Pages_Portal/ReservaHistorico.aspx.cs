﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using PortalUtil;
using Reserva.Datasets;

namespace Reserva.Pages_Portal
{
    public partial class ReservaHistorico : System.Web.UI.Page
    {
        private dReserva dRes = new dReserva();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "12px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dRes = new dReserva(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega campo site
                cmbSite.DataBind();

                //Seta visibilidade de campos de acordo com usuario
                if (Cred.TUsuario == Credencial.TipoUsuario.Administradora)
                {
                    cmbSite.SelectedIndex = -1;
                    cmbSite.Enabled = cmbCondominio.Enabled = true;
                }
                else
                {
                    cmbSite.Value = Cred.EMP;
                    cmbSite.Enabled = cmbCondominio.Enabled = false;
                }

                //Carrega tela (nivel 1)
                Carregar(1);
            }
        }

        private void Carregar(int intNivel)
        {
            //Seta EMP
            dRes.EMP = Convert.ToInt32(cmbSite.Value);

            //Carrega o campo de condominios
            if (intNivel == 1)
            {
                dRes.CONDOMINIOSTableAdapter.FillCombo(dRes.CONDOMINIOS, Convert.ToInt32(cmbSite.Value));
                cmbCondominio.DataSource = dRes.CONDOMINIOS;
                cmbCondominio.DataBind();
                cmbCondominio.Text = "";
                if (Cred.TUsuario != Credencial.TipoUsuario.Administradora)
                    cmbCondominio.Value = Cred.CON;
            }

            //Carrega o campo de blocos
            if (intNivel <= 2)
            {
                dRes.BLOCOSTableAdapter.FillCombo(dRes.BLOCOS, Convert.ToInt32(cmbCondominio.Value));
                cmbBloco.DataSource = dRes.BLOCOS;
                cmbBloco.DataBind();
                cmbBloco.Text = "";
            }

            //Carrega o campo de apartamentos
            if (intNivel <= 3)
            {
                dRes.APARTAMENTOSTableAdapter.FillCombo(dRes.APARTAMENTOS, Convert.ToInt32(cmbBloco.Value));
                cmbApartamento.DataSource = dRes.APARTAMENTOS;
                cmbApartamento.DataBind();
                cmbApartamento.Text = "";
            }

            //Carrega o campo de equipamentos
            if (intNivel <= 2)
            {
                dRes.EQuiPamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Convert.ToInt32(cmbCondominio.Value));
                cmbEquipamento.DataSource = dRes.EQuiPamentos;
                cmbEquipamento.DataBind();
                cmbEquipamento.Text = "";
            }
        }

        protected void cmbSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbSite.Value) == 0)
                cmbSite.SelectedIndex = -1;               

            //Carrega tela (nivel 1)
            Carregar(1);
        }

        protected void cmbCondominio_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbCondominio.Value) == 0)
                cmbCondominio.SelectedIndex = -1;

            //Carrega tela (nivel 2)
            Carregar(2);
        }

        protected void cmbBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Carrega tela (nivel 3)
            Carregar(3);
        }

        protected void cmdBuscar_Click(object sender, EventArgs e)
        {
            //Busca os classificados de acordo com os campos de busca
            Buscar();
        }

        private void Buscar()
        {
            //Pega os dados
            int intBLO = ((cmbBloco.SelectedIndex == 0) ? 0 : Convert.ToInt32(cmbBloco.Value));
            int intAPT = ((cmbApartamento.SelectedIndex == 0) ? 0 : Convert.ToInt32(cmbApartamento.Value));
            int intEQP = ((cmbEquipamento.SelectedIndex == 0) ? 0 : Convert.ToInt32(cmbEquipamento.Value));
            string strDataInicial = dtePeriodoDataInicial.Text == "" ? "01/01/1800" : dtePeriodoDataInicial.Text;
            string strDataFinal = dtePeriodoDataFinal.Text == "" ? "01/01/9000" : dtePeriodoDataFinal.Text;

            //Seta EMP
            dRes.EMP = Convert.ToInt32(cmbSite.Value);

            //Carrega tela com os historicos
            dRes.ST_ReservaEquipamentoHistoricoTableAdapter.FillBusca(dRes.ST_ReservaEquipamentoHistorico, Convert.ToInt32(cmbCondominio.Value), Convert.ToInt32(intBLO), Convert.ToInt32(intAPT), Convert.ToInt32(intEQP), strDataInicial, strDataFinal);
            grdHistoricoReserva.DataSource = dRes.ST_ReservaEquipamentoHistorico;
            grdHistoricoReserva.DataBind();

            //Exibe Grid
            grdHistoricoReserva.Visible = true;
        }
    }
}