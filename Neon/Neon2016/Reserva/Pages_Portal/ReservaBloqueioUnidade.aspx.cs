﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using Reserva.Datasets;

namespace Reserva.Pages_Portal
{
    public partial class ReservaBloqueioUnidade : System.Web.UI.Page
    {
        private dReservas dResOld = new dReservas(); //dataset antigo a ser ajustado para o novo dReserva
        private dReserva dRes = new dReserva();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dResOld = new dReservas(Cred.EMP); //dataset antigo a ser ajustado para o novo dReserva
            dRes = new dReserva(Cred.EMP);

            if (!IsPostBack)
            {
                //Inicia dados
                IniciaDados();

                //Carrega tela
                Carregar();
            }
        }

        private void IniciaDados()
        {
            //Carrega o campo de equipamentos
            dRes.EQuiPamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Cred.CON);
            cmbEquipamento.DataSource = dRes.EQuiPamentos;
            cmbEquipamento.DataBind();
            cmbEquipamento.Text = "";

            //Carrega o campo de blocos
            dRes.BLOCOSTableAdapter.FillCombo(dRes.BLOCOS, Cred.CON);
            cmbBloco.DataSource = dRes.BLOCOS;
            cmbBloco.DataBind();
            cmbBloco.Text = "";
        }

        private void Carregar()
        {
            //Carrega os bloqueios de unidade
            dResOld.ReservaBloqueioUnidadeTableAdapter.FillByEMP_CON(dResOld.ReservaBloqueioUnidade, Cred.EMP, Cred.CON);
            grdReservaBloqueioUnidade.DataSource = dResOld.ReservaBloqueioUnidade;
            grdReservaBloqueioUnidade.DataBind();

            //Posiciona no mesmo campo
            this.smnReservaBloqueioUnidade.SetFocus(cmbEquipamento);
        }

        protected void cmbEquipamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbEquipamento.Value) == 0)
                cmbEquipamento.SelectedIndex = -1;

            //Posiciona no mesmo campo
            this.smnReservaBloqueioUnidade.SetFocus(cmbEquipamento);
        }

        protected void cmbBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Carrega o campo de apartamentos
            dRes.APARTAMENTOSTableAdapter.FillCombo(dRes.APARTAMENTOS, Convert.ToInt32(cmbBloco.Value));
            cmbApartamento.DataSource = dRes.APARTAMENTOS;
            cmbApartamento.DataBind();
            cmbApartamento.Text = "";

            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbBloco.Value) == 0)
                cmbBloco.SelectedIndex = -1;

            //Posiciona no mesmo campo
            this.smnReservaBloqueioUnidade.SetFocus(cmbBloco);
        }

        private void LimpaCampos()
        {
            //Limpa os campos
            cmbEquipamento.SelectedIndex = -1;
            cmbBloco.SelectedIndex = -1;
            cmbApartamento.SelectedIndex = -1;
            dteDataInicial.Text = "";
            dteDataFinal.Text = "";
            txtObservacao.Text = "";

            //Posiciona no mesmo campo
            this.smnReservaBloqueioUnidade.SetFocus(cmbEquipamento);
        }

        protected void cmdAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Altera o cadastro do bloqueio
            Button cmdAlterar = (Button)sender;
            int ID = int.Parse(cmdAlterar.CommandArgument);

            //Procura pelo registro selecionado para alterar
            dResOld.ReservaBloqueioUnidadeTableAdapter.Fill(dResOld.ReservaBloqueioUnidade);
            dReservas.ReservaBloqueioUnidadeRow rowBloqueio = dResOld.ReservaBloqueioUnidade.FindByRBU(ID);

            //Preenche tela
            lblID.Text = rowBloqueio.RBU.ToString();
            cmbEquipamento.Value = rowBloqueio.RBU_EQP;
            cmbBloco.Value = rowBloqueio.RBU_BLO;
            dRes.APARTAMENTOSTableAdapter.FillCombo(dRes.APARTAMENTOS, Convert.ToInt32(cmbBloco.Value));
            cmbApartamento.DataSource = dRes.APARTAMENTOS;
            cmbApartamento.DataBind();
            if (!rowBloqueio.IsRBU_APTNull()) cmbApartamento.Value = rowBloqueio.RBU_APT;
            if (!rowBloqueio.IsRBU_DataInicialNull()) dteDataInicial.Text = rowBloqueio.RBU_DataInicial.ToShortDateString();
            if (!rowBloqueio.IsRBU_DataFinalNull()) dteDataFinal.Text = rowBloqueio.RBU_DataFinal.ToShortDateString();
            if (!rowBloqueio.IsRBU_ObservacaoNull()) txtObservacao.Text = rowBloqueio.RBU_Observacao;

            //Seta campos               
            floCampos.Visible = true;
            grdReservaBloqueioUnidade.Visible = false;
            cmdCancelar.Visible = true;
            cmdInserirAlterar.Text = "ALTERAR";

            //Posiciona no mesmo campo
            this.smnReservaBloqueioUnidade.SetFocus(cmbEquipamento);
        }

        protected void cmdRemover_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Remove o cadastro do usuario da tabela
            Button cmdRemover = (Button)sender;
            int ID = int.Parse(cmdRemover.CommandArgument);

            //Apaga o registro selecionado para excluir
            dResOld.ReservaBloqueioUnidadeTableAdapter.Delete(ID);

            //Recarrega a tela
            Carregar();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Volta campos
            floCampos.Visible = false;
            grdReservaBloqueioUnidade.Visible = true;
            cmdCancelar.Visible = false;
            cmdInserirAlterar.Text = "INSERIR NOVO BLOQUEIO";
        }

        protected void cmdInserirAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Verifica se apenas exibe campos para inserir
            if (cmdInserirAlterar.Text == "INSERIR NOVO BLOQUEIO")
            {
                //Limpa os campos da tela
                LimpaCampos();

                //Seta campos               
                floCampos.Visible = true;
                grdReservaBloqueioUnidade.Visible = false;
                cmdCancelar.Visible = true;
                cmdInserirAlterar.Text = "INSERIR";
                return;
            }

            //Faz a consistência dos campos
            if ((dteDataInicial.Text == "" && dteDataFinal.Text != "") || (dteDataInicial.Text != "" && dteDataFinal.Text == ""))
            {
                lblMsg.Text = "Período inválido. Indique a data inicial e a data final do bloqueio para um período específico ou deixe as duas datas em branco para bloqueio com prazo indefinido.";
                panMsg.Visible = true;
                return;
            }

            //Verifica se existe reserva para a unidade do equipamento selecionado dentro do periodo indicado (apenas avisa apos cadastro do bloqueio)
            string strMsgAdicional = "";
            if ((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaReservaUnidade(Cred.EMP, (int)cmbEquipamento.SelectedItem.Value, (int)cmbBloco.SelectedItem.Value, (int)cmbApartamento.SelectedItem.Value <= 0 ? -1 : (int)cmbApartamento.SelectedItem.Value, dteDataInicial.Text, dteDataFinal.Text))
                strMsgAdicional = "\r\n\r\nATENÇÃO: Já existe reserva efetuada dentro do bloqueio cadastrado e a reserva será mantida.\r\nNovas tentativas de reserva dentro do bloqueio cadastrado não serão permitidas a partir deste momento.";

            //Insere ou Altera
            try
            {
                //Abre uma conexao
                dResOld.ReservaBloqueioUnidadeTableAdapter.Connection.Open();
                if (cmdInserirAlterar.Text == "INSERIR")
                {
                    //Insere o novo registro
                    dResOld.ReservaBloqueioUnidadeTableAdapter.Insert(
                        Cred.EMP,
                        (int)cmbEquipamento.SelectedItem.Value,
                        (int)cmbBloco.SelectedItem.Value,
                        (int)cmbApartamento.SelectedItem.Value <= 0 ? (int?)null : (int)cmbApartamento.SelectedItem.Value,
                        dteDataInicial.Text == "" ? (DateTime?)null : Convert.ToDateTime(dteDataInicial.Text),
                        dteDataFinal.Text == "" ? (DateTime?)null : Convert.ToDateTime(dteDataFinal.Text),
                        txtObservacao.Text);

                    //Sucesso
                    lblMsg.Text = "Bloqueio inserido com sucesso !" + strMsgAdicional;
                    panMsg.Visible = true;
                }
                else
                {
                    //Altera o registro
                    dResOld.ReservaBloqueioUnidadeTableAdapter.Update(
                        Cred.EMP,
                        (int)cmbEquipamento.SelectedItem.Value,
                        (int)cmbBloco.SelectedItem.Value,
                        (int)cmbApartamento.SelectedItem.Value <= 0 ? (int?)null : (int)cmbApartamento.SelectedItem.Value,
                        dteDataInicial.Text == "" ? (DateTime?)null : Convert.ToDateTime(dteDataInicial.Text),
                        dteDataFinal.Text == "" ? (DateTime?)null : Convert.ToDateTime(dteDataFinal.Text),
                        txtObservacao.Text,
                        Convert.ToInt32(lblID.Text));

                    //Sucesso
                    lblMsg.Text = "Bloqueio alterado com sucesso !" + strMsgAdicional;
                    panMsg.Visible = true;
                }
            }
            catch (Exception)
            {
                //Erro
                lblMsg.Text = "Ocorreu um erro no cadastro do bloqueio, tente novamente.";
                panMsg.Visible = true;
                return;
            }
            finally
            {
                //Fecha a conexao
                dResOld.ReservaBloqueioUnidadeTableAdapter.Connection.Close();
            }

            //Recarrega a tela
            Carregar();

            //Volta campos
            floCampos.Visible = false;
            grdReservaBloqueioUnidade.Visible = true;
            cmdCancelar.Visible = false;
            cmdInserirAlterar.Text = "INSERIR NOVO BLOQUEIO";
        }
    }
}