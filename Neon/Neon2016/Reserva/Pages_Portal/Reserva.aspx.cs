﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using DevExpress.Web;
using PortalUtil;
using Reserva.Datasets;

namespace Reserva.Pages_Portal
{
    public partial class Reserva : System.Web.UI.Page
    {
        private dReservas dResOld = new dReservas(); //dataset antigo a ser ajustado para o novo dReserva
        private dReserva dRes = new dReserva();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        private string[] arrMeses = new string[13] { "", "JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO" };
        private ArrayList UsuariosSindicos
        {
            get { return new ArrayList(new Credencial.TipoUsuario[] { Credencial.TipoUsuario.Administradora,
                                                                      Credencial.TipoUsuario.Sindico,
                                                                      Credencial.TipoUsuario.ProprietarioSindico,
                                                                      Credencial.TipoUsuario.InquilinoSindico });
            }
        }
        private ArrayList UsuariosTipoSindico
        {
            get { return new ArrayList(new Credencial.TipoUsuario[] { Credencial.TipoUsuario.Administradora,
                                                                      Credencial.TipoUsuario.Sindico,
                                                                      Credencial.TipoUsuario.ProprietarioSindico,
                                                                      Credencial.TipoUsuario.InquilinoSindico,
                                                                      Credencial.TipoUsuario.Subsindico,
                                                                      Credencial.TipoUsuario.GerentePredial,
                                                                      Credencial.TipoUsuario.Zelador }); }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dResOld = new dReservas(Cred.EMP); //dataset antigo a ser ajustado para o novo dReserva
            dRes = new dReserva(Cred.EMP);

            if (!IsPostBack)
            {
                //Inicia dados
                IniciaDados();

                //Carrega tela reservas 
                CarregaTelaReservas();

                //Posiciona no combo de escolha de equipamentos / espacos
                this.smnReserva.SetFocus(cmbEquipamentos);
            }
        }

        private void IniciaDados()
        {
            //Carrega o campo de equipamentos / espacos
            dRes.EQuiPamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Cred.CON);
            cmbEquipamentos.DataSource = dRes.EQuiPamentos;
            cmbEquipamentos.DataBind();

            //Carrega o campo de blocos
            dRes.BLOCOSTableAdapter.FillCombo(dRes.BLOCOS, Cred.CON);
            DataView dvBlocos = dRes.BLOCOS.DefaultView;
            dvBlocos.Sort = "BLONome";
            cmbBloco.DataSource = dvBlocos.ToTable();
            cmbBloco.DataBind();

            //Seta valores iniciais para validacao
            rfvBloco.InitialValue = "0";
            rfvApartamento.InitialValue = "0";

            //Inicia Mes e Ano
            lblMes.Text = DateTime.Now.Month.ToString();
            lblAno.Text = DateTime.Now.Year.ToString();

            //Desabilita botao anterior e habilita proximo
            cmdAnterior.Enabled = false;
            cmdProximo.Enabled = true;

            //Inicia pans
            panTelaReservas.Visible = true;
            panHorarios.Visible = true;
            panCampos.Visible = false;
            panAlertaSindico.Visible = false;
            panConfirmacao.Visible = false;
            panReserva.Visible = false;
            panBloqueioObservacao.Visible = false;
            if (UsuariosTipoSindico.Contains(Cred.TUsuario))
            {
                panComandos.Visible = false;
                panComandosSindico.Visible = panReservasImpressao.Visible = true;
            }
            else
            {
                panComandos.Visible = true;
                panComandosSindico.Visible = panReservasImpressao.Visible = false;
            }

            //Verifica dados de impressao
            CarregaDadosImpressao();

            //Limpa o equipamento
            cmbEquipamentos.SelectedValue = "0";
        }

        private void CarregaTelaReservas()
        {
            //Carrega calendario 
            CarregaCalendario();

            //Carrega reservas do apartamento 
            CarregaReservasApartamento();

            //Posiciona no combo de escolha de equipamentos / espacos
            this.smnReserva.SetFocus(cmbEquipamentos);
        }

        private void CarregaCalendario()
        {
            //Inicia parametros do calendario
            dsCalendarioReservas.SelectParameters.Clear();
            dsCalendarioReservas.SelectParameters.Add("EMP", Cred.EMP.ToString());
            dsCalendarioReservas.SelectParameters.Add("EQP", cmbEquipamentos.SelectedValue);
            dsCalendarioReservas.SelectParameters.Add("Mes", lblMes.Text);
            dsCalendarioReservas.SelectParameters.Add("Ano", lblAno.Text);
            if (UsuariosTipoSindico.Contains(Cred.TUsuario))
            { dsCalendarioReservas.SelectParameters.Add("TIPO", "1"); }
            else
            { dsCalendarioReservas.SelectParameters.Add("TIPO", ((int)Cred.TUsuario).ToString()); }
            dsCalendarioReservas.DataBind();
        }

        private void CarregaListaConvidados(String strREQ, String strAPT, String strEquipamento, String strData)
        {
            //Pega dados do APT
            string strUnidade = "Não identificada";
            dRes.APARTAMENTOSTableAdapter.Fill(dRes.APARTAMENTOS, Convert.ToInt32(strAPT));
            if (dRes.APARTAMENTOS.Count != 0)
            {
                dReserva.APARTAMENTOSRow rowAPT = (dReserva.APARTAMENTOSRow)dRes.APARTAMENTOS.Rows[0];
                strUnidade = rowAPT.BLONome + " - " + rowAPT.APTNumero;
            }                

            //Pega dados do EQP
            int intMaxConvidados = Convert.ToInt32(dResOld.ReservaEQuipamentoTableAdapter.VerificaMaxConvidados(Convert.ToInt32(strREQ), Cred.EMP));

            //Indica os dados da reserva da lista de convidados
            lblListaConvidadosREQ.Text = strREQ;
            lblListaConvidadosAPT.Text = strAPT;
            lblListaConvidadosUnidade.Text = strUnidade;
            lblListaConvidadosEquipamento.Text = strEquipamento;
            lblListaConvidadosData.Text = strData;
            lblListaConvidadosNumMax.Text = string.Format("{0} Convidados", intMaxConvidados.ToString());

            //Inicia parametros da lista de convidados
            dsListaConvidados.SelectParameters.Clear();
            dsListaConvidados.SelectParameters.Add("EMP", Cred.EMP.ToString());
            dsListaConvidados.SelectParameters.Add("REQ", strREQ);
            dsListaConvidados.SelectParameters.Add("APT", strAPT);
            dsListaConvidados.DataBind();

            //Limpa campos
            txtNomeConvidado.Text = "";

            //Seta no primeiro campo (limpo)
            this.smnReserva.SetFocus(txtNomeConvidado);
        }

        private void CarregaReservasApartamento()
        {
            //Verifica o APT
            if (Cred.rowUsuario.APT == 0)
            {
                dtReservasApartamento.Visible = false;
                return;
            }
            dtReservasApartamento.Visible = true;

            //Carrega as reservas do apartamento
            dsReservasApartamento.SelectParameters.Clear();
            dsReservasApartamento.SelectParameters.Add("EMP", Cred.EMP.ToString());
            dsReservasApartamento.SelectParameters.Add("APT", Cred.rowUsuario.APT.ToString());
            dsReservasApartamento.DataBind();
        }

        private void CarregaHorarios()
        {
            //Inicia parametros da grade de horarios
            dsCalendarioReservasHorarios.SelectParameters.Clear();
            dsCalendarioReservasHorarios.SelectParameters.Add("EMP", Cred.EMP.ToString());
            dsCalendarioReservasHorarios.SelectParameters.Add("EQP", cmbEquipamentos.SelectedValue);
            dsCalendarioReservasHorarios.SelectParameters.Add("Data", lblHorariosData.Text);
            dsCalendarioReservasHorarios.DataBind();

            //Verifica se trata ou nao bloqueio (se for sindico)
            if (UsuariosTipoSindico.Contains(Cred.TUsuario))
            {
                //Verifica se tem bloqueio para a data
                lblBloqueio.Text = "";
                if ((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaBloqueio(Convert.ToDateTime(lblHorariosData.Text), Convert.ToInt32(cmbEquipamentos.SelectedValue), Cred.EMP))
                    lblBloqueio.Text = (string)dResOld.ReservaEQuipamentoTableAdapter.VerificaBloqueioTipo(Convert.ToDateTime(lblHorariosData.Text), Convert.ToInt32(cmbEquipamentos.SelectedValue), Cred.EMP);
            }
        }

        private void CarregaRegras()
        {
            //Limpa campo de Regra selecionada
            lblRegraSelecionada.Text = "";
            lblEquipamentosSelecionados.Text = "";

            //Seta APT final (caso selecao por combo)
            if (cmbBloco.Visible)
                lblAPT.Text = cmbApartamento.SelectedValue;

            //Verifica se mostra lista de regras
            panRegras.Visible = ((lblAPT.Text != "0") && ((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaAplicaRegras(Convert.ToInt32(lblEQP.Text), Cred.EMP)));

            //Inicia parametros da lista de regras
            dsRegras.SelectParameters.Clear();
            dsRegras.SelectParameters.Add("EMP", Cred.EMP.ToString());
            dsRegras.SelectParameters.Add("EQP", lblEQP.Text);
            dsRegras.SelectParameters.Add("Apt", lblAPT.Text);
            dsRegras.SelectParameters.Add("Data", lblData.Text.Substring(0, 10));
            dsRegras.SelectParameters.Add("Tipo", cmdConfirmar.Text != "Cancelar Reserva" ? "R" : "C");
            dsRegras.DataBind();
        }

        private void CarregaDadosImpressao()
        {
            //Carrega dados for sindico (botao visivel)
            if (panReservasImpressao.Visible)
            {
                //Verifica se imprime todos os equipamentos ou apenas o selecionado
                string strEQP = "";
                if (cmbEquipamentos.SelectedIndex == 0)
                {
                    foreach (ListItem item in cmbEquipamentos.Items)
                    {
                        if (strEQP != "") { strEQP = strEQP + ","; }
                        strEQP = strEQP + "[" + item.Value.ToString() + "]";
                    }
                }
                else
                { strEQP = "[" + cmbEquipamentos.SelectedValue.ToString() + "]"; }
                strEQP = strEQP.Replace("[0],", "");

                //Seta dados para impressao
                cmdReservasImpressao.Enabled = true;
                cmdReservasImpressao.OnClientClick = "javascript:window.open('ReservasImpressao.aspx?EMP=" + Cred.EMP + "&E=" + strEQP + "&M=" + lblMes.Text + "&A=" + lblAno.Text + "','','width=875,height=500,scrollbars=1,toolbar=0,location=0,top=100');";
            }
        }

        protected void dtCalendarioReservas_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            //Verifica se e header ou item
            if (e.Item.ItemType == ListItemType.Header)
            {
                //Preenche o mes e ano
                Label lblMesAno = (Label)e.Item.FindControl("lblMesAno");
                lblMesAno.Text = arrMeses[Convert.ToInt16(lblMes.Text)] + " / " + lblAno.Text;
            }
        }

        protected void cmbEquipamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Carrega tela reservas 
            CarregaTelaReservas();

            //Verifica botoes anterior e proximo
            if (Convert.ToInt16(lblMes.Text) == DateTime.Now.Month) { cmdAnterior.Enabled = false; }
            else { cmdAnterior.Enabled = true; }
            cmdProximo.Enabled = true;

            //Verifica dados de impressao
            CarregaDadosImpressao();

            //Volta o foco no campo
            this.smnReserva.SetFocus(cmbEquipamentos);
        }

        protected void lblDiaSemana_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Pega o dia clicado e o id associado a ele
            Button lblDiaSemana = (Button)sender;

            //Preenche a data
            lblHorariosData.Text = lblDiaSemana.Text.PadLeft(2, '0') + "/" + lblMes.Text.PadLeft(2, '0') + "/" + lblAno.Text;

            //Preenche os horarios
            CarregaHorarios();

            //Desabilida tela de reservas
            panTelaReservas.Visible = false;

            //Mostra o popup da reserva exibindo os horarios
            panReserva.Visible = true;
            panHorarios.Visible = true;
            panCampos.Visible = false;
            panAlertaSindico.Visible = false;
            panConfirmacao.Visible = false;
            panBloqueioObservacao.Visible = false;
        }

        protected void lblVisualizarReserva_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Pega a reserva clicada e o id associado a ela
            Button lblReserva = (Button)sender;

            //Inicia bloco e apartamento
            txtBloco.Text = "";
            txtApartamento.Text = "";

            //Pega dados do APT
            dRes.APARTAMENTOSTableAdapter.Fill(dRes.APARTAMENTOS, Convert.ToInt32(lblReserva.CommandArgument));
            if (dRes.APARTAMENTOS.Count != 0)
            {
                dReserva.APARTAMENTOSRow rowAPT = (dReserva.APARTAMENTOSRow)dRes.APARTAMENTOS.Rows[0];
                txtApartamento.Text = rowAPT.APTNumero;
                txtBloco.Text = rowAPT.BLONome;
            }

            //Preenche os detalhes da reserva
            PreencheTelaReserva(lblReserva.ToolTip.Substring(0, 10), lblReserva.ToolTip.Substring(13, 18), lblReserva.ToolTip.Substring(34), lblReserva.CommandName, lblReserva.CommandArgument, txtBloco.Text, txtApartamento.Text);

            //Desabilida tela de reservas
            panTelaReservas.Visible = false;

            //Mostra o popup da reserva exibindo os campos
            panReserva.Visible = true;
            panHorarios.Visible = false;
            panCampos.Visible = true;
            panAlertaSindico.Visible = false;
            panConfirmacao.Visible = false;
            panBloqueioObservacao.Visible = false;
        }

        protected void lblVisualizarLista_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroIncluir.Text = "";
            panErroIncluir.Visible = false;

            //Pega a reserva clicada e o id associado a ela
            Button lblReserva = (Button)sender;

            //Carrega a lista de convidados
            CarregaListaConvidados(lblReserva.CommandName, lblReserva.CommandArgument, lblReserva.ToolTip.Substring(34), lblReserva.ToolTip.Substring(0, 10));

            //Deixa tela de reservas invisivel 
            panTelaReservas.Visible = false;

            //Mostra tela com lista de convidados
            panListaConvidados.Visible = true;
        }

        protected void lblIncluirConvidado_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroIncluir.Text = "";
            panErroIncluir.Visible = false;

            //Verifica se digitou nome convidado
            if (txtNomeConvidado.Text == "")
            {
                lblErroIncluir.Text = "Informe o nome do convidado a ser incluído na lista.";
                panErroIncluir.Visible = true;
                this.smnReserva.SetFocus(txtNomeConvidado);
                return;
            }

            //Verifica lista de convidados esgotada
            if ((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaListaConvidadosEsgotada(Cred.EMP, Convert.ToInt32(lblListaConvidadosREQ.Text), Convert.ToInt32(lblListaConvidadosAPT.Text)))
            {
                lblErroIncluir.Text = "Você já incluiu nesta lista de convidados a quantidade máxima permitida para este espaço.";
                panErroIncluir.Visible = true;
                return;
            }

            //Grava
            try
            {
                //Insere convidado na lista
                dResOld.ReservaEQuipamentoTableAdapter.ListaConvidadosIncluir(Convert.ToInt32(lblListaConvidadosREQ.Text), Convert.ToInt32(lblListaConvidadosAPT.Text), txtNomeConvidado.Text, Cred.EMP);

                //Atualiza lista
                CarregaListaConvidados(lblListaConvidadosREQ.Text, lblListaConvidadosAPT.Text, lblListaConvidadosEquipamento.Text, lblListaConvidadosData.Text);
            }
            catch (Exception)
            {
                //Erro
                lblErroIncluir.Text = "Erro ao incluir o convidado na lista. Por favor, tente novamente.";
                panErroIncluir.Visible = true;
            }
        }

        protected void lblRemoverConvidado_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroIncluir.Text = "";
            panErroIncluir.Visible = false;

            //Pega o convidado clicado e o id associado a ele
            Button lblConvidado = (Button)sender;
            int intRLC = Convert.ToInt32(lblConvidado.CommandName);

            //Remove
            try
            {
                //Remove convidado na lista
                dResOld.ReservaEQuipamentoTableAdapter.ListaConvidadosRemover(intRLC);

                //Atualiza lista
                CarregaListaConvidados(lblListaConvidadosREQ.Text, lblListaConvidadosAPT.Text, lblListaConvidadosEquipamento.Text, lblListaConvidadosData.Text);
            }
            catch (Exception)
            {
                //Erro
                lblErroIncluir.Text = "Erro ao remover o convidado da lista. Por favor, tente novamente.";
                panErroIncluir.Visible = true;
            }
        }

        protected void cmdEnviarLista_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroIncluir.Text = "";
            panErroIncluir.Visible = false;

            try
            {
                //Cria e-mail
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Verifica SMTP de dominio proprio e seta dados de envio especificos
                if (dRes.ST_SMTpTableAdapter.Fill(dRes.ST_SMTp, Cred.CON) > 0)
                {
                    SmtpClient1.Host = dRes.ST_SMTp[0].SMTpHost;
                    SmtpClient1.Credentials = new System.Net.NetworkCredential(dRes.ST_SMTp[0].SMTpUser, dRes.ST_SMTp[0].SMTpPass);
                    SmtpClient1.Port = Convert.ToInt32(dRes.ST_SMTp[0].SMTpPort);
                    if (!dRes.ST_SMTp[0].IsSMTpRemetenteNull()) strRemetente = dRes.ST_SMTp[0].SMTpRemetente;
                }

                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //To
                    dRes.ST_EMailEnvioTableAdapter.Fill(dRes.ST_EMailEnvio, Cred.CON, (int)Tipos.TipoEmailEnvio.ReservaListaConvidados);
                    foreach (dReserva.ST_EMailEnvioRow row in dRes.ST_EMailEnvio.Rows)
                        email.To.Add(row.EMEEmail.ToString().Replace(";", ","));

                    //Bcc
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));

                    //Verifica se nao tem destino e nao envia
                    if (email.To.Count == 0)
                    {
                        //Erro
                        lblErroIncluir.Text = "Nenhum e-mail de destino para envio da lista foi encontrado. Por favor, entre em contato com o síndico do condomínio e informe o ocorrido.";
                        panErroIncluir.Visible = true;
                        return;
                    }

                    //Inicia corpo do email
                    email.Subject = "Lista de Convidados (" + Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + lblListaConvidadosUnidade.Text + " - " + lblListaConvidadosEquipamento.Text + " - " + lblListaConvidadosData.Text + ")";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>LISTA DE CONVIDADOS</b></p>\r\n" +
                                               "      <p><b>Condomínio:</b> {0}</p>\r\n" +
                                               "      <p><b>Unidade:</b> {1}</p>\r\n" +
                                               "      <p><b>Espaço:</b> {2}<br>\r\n" +
                                               "      <b>Data:</b> {3}</p>\r\n" +
                                               "      <p><b>NOMES</b></p>\r\n" +
                                               "      <p>",
                                               Server.HtmlEncode(Cred.rowUsuario.CONNome), 
                                               lblListaConvidadosUnidade.Text, lblListaConvidadosEquipamento.Text, lblListaConvidadosData.Text);

                    //Inclui nomes no corpo do email
                    foreach (DataListItem convidado in dtListaConvidados.Items)
                        email.Body += ((Label)convidado.FindControl("lblNome")).Text + "<br>\r\n";

                    //Fecha corpo do email
                    email.Body += string.Format("   </p>\r\n" +
                                                "   </body>\r\n" +
                                                "</html>");

                    //Cria a lista em excel e anexa se criou com sucesso
                    try
                    {
                        //Cria o excel
                        StringWriter writer = new StringWriter();
                        HtmlTextWriter html = new HtmlTextWriter(writer);
                        html.WriteLine("LISTA DE CONVIDADOS");
                        html.WriteLine("");
                        html.WriteLine("CONDOMÍNIO: " + Server.HtmlEncode(Cred.rowUsuario.CONNome));
                        html.WriteLine("UNIDADE: " + lblListaConvidadosUnidade.Text);
                        html.WriteLine("");
                        html.WriteLine("ESPAÇO: " + lblListaConvidadosEquipamento.Text);
                        html.WriteLine("DATA: " + lblListaConvidadosData.Text);
                        html.WriteLine("");
                        html.WriteLine("NOMES");
                        html.WriteLine("");
                        foreach (DataListItem convidado in dtListaConvidados.Items)
                            html.WriteLine(((Label)convidado.FindControl("lblNome")).Text);
                        System.Text.Encoding Enc = System.Text.Encoding.Default;
                        byte[] mBArray = Enc.GetBytes(writer.ToString());
                        System.IO.MemoryStream stream = new System.IO.MemoryStream(mBArray, false);

                        //Anexa o excel
                        System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(stream, "ListaConvidados_" + Server.HtmlEncode(Cred.rowUsuario.CONNome) + "_" + lblListaConvidadosUnidade.Text + "_" + lblListaConvidadosEquipamento.Text + "_" + lblListaConvidadosData.Text.Replace("/", "-") + ".xls", "application/vnd.ms-excel");
                        email.Attachments.Add(attach);
                    }
                    catch (Exception)
                    { }

                    //Envia email
                    SmtpClient1.Send(email);

                    //Enviou
                    ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", "alert(\"Lista de Convidados enviada com sucesso!!\");", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", "alert(\"Ocorreu um erro no envio da Lista de Convidados. Por favor, tente novamente. (erro: " + ex.Message + ")\");", true);
            }
        }

        protected void cmdFecharLista_Click(object sender, EventArgs e)
        {
            //Volta pans
            panTelaReservas.Visible = true;
            panListaConvidados.Visible = false;
        }

        protected void cmdVoltar_Click(object sender, EventArgs e)
        {
            //Volta pans
            panTelaReservas.Visible = true;
            panReserva.Visible = false;
            panBloqueioObservacao.Visible = false;
            this.smnReserva.SetFocus(cmbEquipamentos);
        }

        protected void lblHorario_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Pega o horario clicado e o id associado a ele
            Button lblHorario = (Button)sender;

            //Inicia bloco e apartamento
            txtBloco.Text = "";
            txtApartamento.Text = "";

            //Pega dados do APT
            if (lblHorario.CommandArgument != "0")
            {
                dRes.APARTAMENTOSTableAdapter.Fill(dRes.APARTAMENTOS, Convert.ToInt32(lblHorario.CommandArgument));
                if (dRes.APARTAMENTOS.Count != 0)
                {
                    dReserva.APARTAMENTOSRow rowAPT = (dReserva.APARTAMENTOSRow)dRes.APARTAMENTOS.Rows[0];
                    txtApartamento.Text = rowAPT.APTNumero;
                    txtBloco.Text = rowAPT.BLONome;
                }
            }

            //Preenche os detalhes da reserva
            PreencheTelaReserva(lblHorariosData.Text, lblHorario.ToolTip, cmbEquipamentos.SelectedItem.Text, lblHorario.CommandName, lblHorario.CommandArgument, txtBloco.Text, txtApartamento.Text);

            //Esconde horarios e mostra os campos
            panHorarios.Visible = false;
            panCampos.Visible = true;
            panAlertaSindico.Visible = false;
            panConfirmacao.Visible = false;
        }

        private void SetaStatusUnidade()
        {
            //Verifica se nao setou a unidade
            string strBloco = txtBloco.Text != "" ? txtBloco.Text : (cmbBloco.SelectedItem.Text != "" ? cmbBloco.SelectedItem.Text : "");
            string strApartamento = txtApartamento.Text != "" ? txtApartamento.Text : (cmbApartamento.SelectedItem.Text != "" ? cmbApartamento.SelectedItem.Text : "");
            if (strApartamento == "")
                return;

            //Exibe o campo apenas para sindico e admin
            if (UsuariosSindicos.Contains(Cred.TUsuario))
                panStatus.Visible = true;

            //Seta adimplente
            lblStatus.Text = "Adimplente";
            lblStatus.ForeColor = System.Drawing.Color.FromName("#08225a");

            //Verifica Inadimplencia
            if ((int)dRes.BOLetosTableAdapter.VerificaInadimplencia(Convert.ToInt32(lblAPT.Text)) > 0)
            {
                lblStatus.Text = "Inadimplente";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return;
            }

            //Verifica Acordo
            if ((int)dRes.ACOrdosTableAdapter.VerificaAcordo(Convert.ToInt32(lblAPT.Text)) > 0)
            {
                lblStatus.Text = "Inadimplente (Acordo)";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return;
            }
        }

        protected void PreencheTelaReserva(String strData, String strHorario, String strEquipamento, String strREQ, String strAPT, String strAPTBloco, String strAPTNumero)
        {
            //Seta o id da reserva a ser alterada e id do apartamento
            lblREQ.Text = strREQ;
            lblAPT.Text = strAPT;

            //Seta dados da reserva
            DateTime datDataReserva = Convert.ToDateTime(dResOld.ReservaEQuipamentoTableAdapter.VerificaDataReserva(Convert.ToInt32(lblREQ.Text), Cred.EMP));
            lblDataHoraReservaEfetuada.Text = dResOld.ReservaEQuipamentoTableAdapter.VerificaDataHoraReservaEfetuada(Convert.ToInt32(lblREQ.Text), Cred.EMP).ToString();
            if (lblDataHoraReservaEfetuada.Text == "") lblDataHoraReservaEfetuada.Text = DateTime.Now.ToString();
            lblEQP.Text = ((int)dResOld.ReservaEQuipamentoTableAdapter.VerificaEQP(Convert.ToInt32(lblREQ.Text), Cred.EMP)).ToString();
            lblEquipamento.Text = strEquipamento;
            lblData.Text = strData + " - " + strHorario;
            chkTermos.Checked = false;

            //Preenche combo de blocos e apartamentos
            cmbBloco.SelectedValue = cmbBloco.Items.FindByText(strAPTBloco).Value;
            dRes.APARTAMENTOSTableAdapter.FillCombo(dRes.APARTAMENTOS, Convert.ToInt32(cmbBloco.SelectedValue));
            cmbApartamento.DataSource = dRes.APARTAMENTOS;
            cmbApartamento.DataBind();
            cmbApartamento.SelectedValue = cmbApartamento.Items.FindByText(strAPTNumero).Value;

            //Preenche valor
            lblValor.Text = Convert.ToString(dResOld.ReservaEQuipamentoTableAdapter.VerificaValor(Convert.ToInt32(strREQ), Cred.EMP)).Replace(".", ",").Replace(",00%", "%");

            //Preenche informacoes da lista de convidados
            panListaConvidadosMsg.Visible = false;
            int intMaxConvidados = Convert.ToInt32(dResOld.ReservaEQuipamentoTableAdapter.VerificaMaxConvidados(Convert.ToInt32(strREQ), Cred.EMP));
            if (intMaxConvidados > 0)
            {
                panListaConvidadosMsg.Visible = true;
                lblListaConvidadosMax.Text = string.Format("Máximo de {0} Convidados", intMaxConvidados.ToString());
            }

            //Preenche prazo minimo da reserva
            panPrazoMinimoMsg.Visible = false;
            DateTime datPrazoMinimo = Convert.ToDateTime(dRes.EQuiPamentoTableAdapter.VerificaPrazoMinimo(datDataReserva, Convert.ToInt32(lblEQP.Text)));
            if (datPrazoMinimo != datDataReserva)
            {
                panPrazoMinimoMsg.Visible = true;
                lblPrazoMinimo.Text = "Reserva permitida até " + datPrazoMinimo.ToString().Replace(" ", " às ").Replace(":00.000", "");
                if (datPrazoMinimo < DateTime.Now) { lblPrazoMinimo.ForeColor = System.Drawing.Color.Red; }
                else { lblPrazoMinimo.ForeColor = System.Drawing.Color.FromName("#08225a"); }
            }

            //Preenche prazo de cancelamento
            DateTime datPrazoCancelamento = Convert.ToDateTime(dRes.EQuiPamentoTableAdapter.VerificaPrazoCancelamento(Convert.ToInt32(lblEQP.Text), datDataReserva.ToString(), lblDataHoraReservaEfetuada.Text));
            if (datPrazoCancelamento > datDataReserva)
            {
                datPrazoCancelamento = Convert.ToDateTime(lblDataHoraReservaEfetuada.Text).AddSeconds(-10);
                lblPrazoCancelamento.Text = "O cancelamento desta reserva não será permitido (reserva fora do prazo de cancelamento)";
            }
            else
                lblPrazoCancelamento.Text = "Cancelamento permitido até " + datPrazoCancelamento.ToString().Replace(" ", " às ").Replace(":00.000", "");
            if (datPrazoCancelamento > datDataReserva) { lblPrazoCancelamento.ForeColor = System.Drawing.Color.Red; }
            else if (datPrazoCancelamento < DateTime.Now) { lblPrazoCancelamento.ForeColor = System.Drawing.Color.Red; }
            else { lblPrazoCancelamento.ForeColor = System.Drawing.Color.FromName("#08225a"); }

            //Seta o arquivo de regras
            string strArquivo = string.Format(@"RegrasReserva_EQP_{0}_{1}.pdf", lblEQP.Text, Cred.EMP);
            cmdTermos.OnClientClick = String.Format("javascript: window.open('Arquivo.aspx?Arquivo={0}&Tipo={1}', '{2}'); return false;", strArquivo, (short)Tipos.TipoDocumento.RegrasReserva, "_blank");

            //Verifica a condicao da reserva
            panUnidade.Visible = true;
            panTermos.Visible = true;
            txtBloco.Visible = txtApartamento.Visible = true;
            cmbBloco.Visible = cmbApartamento.Visible = false;
            rfvBloco.Enabled = rfvApartamento.Enabled = false;
            cmdConfirmar.Enabled = false;
            this.smnReserva.SetFocus(cmdConfirmar);
            if (UsuariosTipoSindico.Contains(Cred.TUsuario))
            {
                //Sindico
                if (strAPT == "0")
                {
                    lblReservaMsg.Text = "A reserva será feita para a unidade indicada abaixo.";
                    cmdConfirmar.Text = "Reservar";
                    txtBloco.Visible = txtApartamento.Visible = false;
                    cmbBloco.Visible = cmbApartamento.Visible = true;
                    rfvBloco.Enabled = rfvApartamento.Enabled = true;
                    this.smnReserva.SetFocus(cmbBloco);
                }
                else
                {
                    lblReservaMsg.Text = "A reserva está efetuada para a unidade indicada abaixo.";
                    panTermos.Visible = false;
                    cmdConfirmar.Text = "Cancelar Reserva";
                    cmdConfirmar.Enabled = true;
                }
            }
            else
            {
                //Condomino
                if (strAPT == "0")
                {
                    lblReservaMsg.Text = "A reserva será feita para a sua unidade.";
                    txtBloco.Text = dRes.APARTAMENTOSTableAdapter.VerificaBLONome(Cred.CON, Cred.rowUsuario.BLOCodigo, Cred.rowUsuario.APTNumero).ToString();
                    txtApartamento.Text = Cred.rowUsuario.APTNumero;
                    lblAPT.Text = Cred.rowUsuario.APT.ToString();
                    cmdConfirmar.Text = "Reservar";
                }
                else if (strAPT == Cred.rowUsuario.APT.ToString()) 
                {
                    lblReservaMsg.Text = "A reserva está efetuada para a sua unidade.";
                    txtBloco.Text = dRes.APARTAMENTOSTableAdapter.VerificaBLONome(Cred.CON, Cred.rowUsuario.BLOCodigo, Cred.rowUsuario.APTNumero).ToString();
                    txtApartamento.Text = Cred.rowUsuario.APTNumero;
                    panTermos.Visible = false;
                    cmdConfirmar.Text = "Cancelar Reserva";
                    if (lblPrazoCancelamento.ForeColor == System.Drawing.Color.Red) { cmdConfirmar.Enabled = false; }
                    else { cmdConfirmar.Enabled = true; }
                }
                else
                {
                    lblReservaMsg.Text = "Área comum já reservada na data escolhida.";
                    panUnidade.Visible = false;
                    panTermos.Visible = false;
                    cmdConfirmar.Text = "Reservar";
                    cmdConfirmar.Enabled = false;
                    this.smnReserva.SetFocus(cmdCancelar);
                }
            }

            //Verifica se reserva ou cancela
            lblRegras.Text = cmdConfirmar.Text != "Cancelar Reserva" ? "RESERVAS PERMITIDAS" : "CANCELAMENTOS PERMITIDOS";

            //Carrega as regras (para reserva ou cancelamento)
            CarregaRegras();

            //Verifica status unidade (apenas sindico)
            panStatus.Visible = false;
            SetaStatusUnidade();
        }

        protected void cmbBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Carrega o campo de apartamentos
            dRes.APARTAMENTOSTableAdapter.FillCombo(dRes.APARTAMENTOS, Convert.ToInt32(cmbBloco.SelectedValue));
            cmbApartamento.DataSource = dRes.APARTAMENTOS;
            cmbApartamento.DataBind();
            cmbApartamento.SelectedIndex = 0;

            //Limpa as regras (para reserva ou cancelamento)
            CarregaRegras();

            //Verifica status unidade (apenas sindico)
            panStatus.Visible = false;
            SetaStatusUnidade();

            //Volta o foco no campo
            this.smnReserva.SetFocus(cmbBloco);
        }

        protected void cmbApartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Carrega as regras (para reserva ou cancelamento)
            CarregaRegras();

            //Verifica status unidade (apenas sindico)
            panStatus.Visible = false;
            SetaStatusUnidade();

            //Volta o foco no campo
            this.smnReserva.SetFocus(cmbApartamento);
        }

        protected void rdbRegra_CheckedChanged(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Pega a regra clicada
            RadioButton rdbRegra = (RadioButton)sender;

            //Procura pelo item da opcao selecionada para selecionar apenas ele mesmo
            foreach (DataListItem Item in dtRegras.Items)
            {
                //Pega a regra da busca e verifica se e a selecionada
                RadioButton rdbRegraBusca = (RadioButton)Item.FindControl("rdbRegra");
                if (rdbRegraBusca.Equals(rdbRegra))
                {
                    //Pega os Equipamentos da Regra selecionada
                    Label lblEquipamentosRegra = (Label)Item.FindControl("lblEquipamentosRegra");
                    lblRegraSelecionada.Text = lblEquipamentosRegra.Text;
                    lblEquipamentosSelecionados.Text = rdbRegraBusca.Text;
                }
                else
                {
                    //Seta o campo como nao selecionado
                    rdbRegraBusca.Checked = false;
                }
            }

            //Volta o foco no campo
            this.smnReserva.SetFocus(rdbRegra);
        }

        protected void chkTermos_CheckedChanged(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Habilita ou nao o botao de enviar
            cmdConfirmar.Enabled = chkTermos.Checked;
            if (cmdConfirmar.Enabled) { this.smnReserva.SetFocus(cmdConfirmar); }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Volta pans
            panTelaReservas.Visible = true;
            panReserva.Visible = false;
            panBloqueioObservacao.Visible = false;
        }

        protected void cmdConfirmar_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Limpa restricoes para aprovacao do sindico
            string strMsg = "";
            lblReservaAlertaSindicoRestricoes.Text = "";

            //Seta bloco, apartamento e APT final
            if (cmbBloco.Visible)
            {
                txtBloco.Text = cmbBloco.SelectedItem.Text;
                txtApartamento.Text = cmbApartamento.SelectedItem.Text;
                lblAPT.Text = cmbApartamento.SelectedValue;
            }

            //Verifica se esta cancelando
            if (cmdConfirmar.Text == "Cancelar Reserva")
            {
                //Verifica se deve selecionar regra para cancelamento
                if (panRegras.Visible && lblRegraSelecionada.Text == "")
                {
                    lblErroGravar.Text = "Selecione o(s) espaço(s) que deseja cancelar.";
                    panErroGravar.Visible = true;
                    return;
                }

                //Verifica se segue regras para cancelamento em conjunto
                if (!((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaRegrasCancelamento(Cred.EMP, Cred.CON, Convert.ToInt32(lblAPT.Text), lblData.Text.Substring(0, 10), Convert.ToInt32(lblEQP.Text), lblRegraSelecionada.Text)))
                {
                    strMsg = "Este cancelamento não pode ser confirmado porque as áreas comuns ainda reservadas após o cancelamento não obedecem as regras determinadas para reserva em conjunto.";
                    if (UsuariosSindicos.Contains(Cred.TUsuario))
                        lblReservaAlertaSindicoRestricoes.Text += "<br/><br/> * " + strMsg;
                    else
                    {
                        lblErroGravar.Text = strMsg;
                        panErroGravar.Visible = true;
                        return;
                    }
                }
            }
            else
            {
                //Verifica se deve selecionar regra para reserva
                if (panRegras.Visible && lblRegraSelecionada.Text == "")
                {
                    lblErroGravar.Text = "Selecione o(s) espaço(s) que deseja reservar.";
                    panErroGravar.Visible = true;
                    return;
                }

                //Verifica regra de espaco cumulativo (OBS: se um espaço aplica regra então obrigatoriamente deve ser espaço cumulativo - EQPCumultaivo = true)
                if ((int)dResOld.ReservaEQuipamentoTableAdapter.VerificaCumulativo(Convert.ToInt32(lblAPT.Text), Cred.EMP, lblData.Text.Substring(0, 10), Convert.ToInt32(lblEQP.Text)) > 1)
                {
                    strMsg = "Esta reserva não pode ser confirmada porque outro espaço comum já foi reservado para a mesma unidade na data escolhida (espaços não cumulativos).";
                    if (UsuariosSindicos.Contains(Cred.TUsuario))
                        lblReservaAlertaSindicoRestricoes.Text += "<br/><br/> * " + strMsg;
                    else
                    {
                        lblErroGravar.Text = strMsg;
                        panErroGravar.Visible = true;
                        return;
                    }
                }

                //Verifica se segue regras para reserva em conjunto
                if (!((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaRegras(Cred.EMP, Convert.ToInt32(lblAPT.Text), lblData.Text.Substring(0, 10), Convert.ToInt32(lblEQP.Text), lblRegraSelecionada.Text)))
                {
                    strMsg = "Esta reserva não pode ser confirmada porque existem outras áreas comuns já reservadas que junto com esta reserva não obedecem as regras determinadas para reserva em conjunto (regras de reserva em conjunto).";
                    if (UsuariosSindicos.Contains(Cred.TUsuario))
                        lblReservaAlertaSindicoRestricoes.Text += "<br/><br/> * " + strMsg;
                    else
                    {
                        lblErroGravar.Text = strMsg;
                        panErroGravar.Visible = true;
                        return;
                    }
                }

                //Verifica inadimplencia ou acordo (reserva permitida para todos os usuarios SINDICOS, sem alerta)
                if (!UsuariosSindicos.Contains(Cred.TUsuario))
                {
                    if (!((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaReservaInad(Convert.ToInt32(lblEQP.Text), Cred.EMP)))
                    {
                        if (lblStatus.ForeColor == System.Drawing.Color.Red)
                        {
                            lblErroGravar.Text = "Por favor, checar se não há nenhuma pendência financeira para a sua unidade. Se estiver em dia e o problema persistir, entre em contato com a Administradora.";
                            panErroGravar.Visible = true;
                            return;
                        }
                    }
                }
            }

            //Verifica regras e equipamentos selecionado para as reservas ou cancelamentos a serem feitos (um espaco sem regra ou espacos de regra selecionada)
            string strRegraSelecionada;
            string strEquipamentosSelecionados;
            if (!panRegras.Visible)
            {
                strRegraSelecionada = "REQ[" + lblREQ.Text + "]";
                strEquipamentosSelecionados = lblEquipamento.Text;
            }
            else
            {
                strRegraSelecionada = "REQ[" + lblREQ.Text + "]" + "EQP" + lblRegraSelecionada.Text.Replace("[" + lblEQP.Text + "]", "");
                strEquipamentosSelecionados = lblEquipamentosSelecionados.Text;
            }

            //Verifica se esta cancelando          
            if (cmdConfirmar.Text == "Cancelar Reserva")
            {
                //Verifica prazo de cancelamento para os equipamentos selecionados (cancelamento permitido para todos os usuarios TIPO SINDICO, sem alerta)
                if (!UsuariosTipoSindico.Contains(Cred.TUsuario))
                    if (!((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaReservaCancelamentoValida(Cred.EMP, strRegraSelecionada, lblData.Text.Substring(0, 10))))
                    {
                        if (!panRegras.Visible)
                            lblErroGravar.Text = "Este cancelamento não pode ser efetivado porque o espaço está fora do prazo de cancelamento permitido pelo condomínio.";
                        else
                            lblErroGravar.Text = "Este cancelamento não pode ser efetivado porque um dos espaços selecionados está fora do prazo de cancelamento permitido pelo condomínio.";
                        panErroGravar.Visible = true;
                        return;
                    }
            }
            else
            {
                //Verifica prazo mínino de reserva para os equipamentos selecionados
                if (!((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaReservaPrazoMinimoValida(Cred.EMP, strRegraSelecionada, lblData.Text.Substring(0, 10))))
                {
                    if (!panRegras.Visible)
                        strMsg = "Esta reserva não pode ser confirmada porque o espaço está fora do prazo mínimo de reserva permitido pelo condomínio.";
                    else
                        strMsg = "Esta reserva não pode ser confirmada porque um dos espaços selecionados está fora do prazo mínimo de reserva permitido pelo condomínio.";
                    if (UsuariosSindicos.Contains(Cred.TUsuario))
                        lblReservaAlertaSindicoRestricoes.Text += "<br/><br/> * " + strMsg;
                    else
                    {
                        lblErroGravar.Text = strMsg;
                        panErroGravar.Visible = true;
                        return;
                    }
                }

                //Verifica reserva unitaria individual para os equipamentos selecionados
                if (!((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaReservaUnitariaIndividualValida(Cred.EMP, Convert.ToInt32(lblAPT.Text), strRegraSelecionada, lblData.Text.Substring(0, 10))))
                {
                    if (!panRegras.Visible)
                        strMsg = "Esta reserva não pode ser confirmada porque o espaço já está reservado para a mesma unidade em uma outra data, não permitindo que seja reservado novamente enquanto a outra reserva não for finalizada ou cancelada (reserva unitária individual).";
                    else
                        strMsg = "Esta reserva não pode ser confirmada porque um dos espaços selecionados já está reservado para a mesma unidade em uma outra data, não permitindo que seja reservado novamente enquanto a outra reserva não for finalizada ou cancelada (reserva unitária individual).";
                    if (UsuariosSindicos.Contains(Cred.TUsuario))
                        lblReservaAlertaSindicoRestricoes.Text += "<br/><br/> * " + strMsg;
                    else
                    {
                        lblErroGravar.Text = strMsg;
                        panErroGravar.Visible = true;
                        return;
                    }
                }

                //Verifica reserva unitaria para os equipamentos selecionados
                if (!((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaReservaUnitariaValida(Cred.EMP, Convert.ToInt32(lblAPT.Text), strRegraSelecionada, lblData.Text.Substring(0, 10))))
                {
                    if (!panRegras.Visible)
                        strMsg = "Esta reserva não pode ser confirmada porque já existe reserva para a mesma unidade em uma outra data, conflitando com este espaço e não permitindo que ele seja reservado enquanto a outra reserva não for finalizada ou cancelada (reserva unitária).";
                    else
                        strMsg = "Esta reserva não pode ser confirmada porque já existe reserva para a mesma unidade em uma outra data, conflitando com um dos espaços selecionados e não permitindo que este espaço seja reservado enquanto a outra reserva não for finalizada ou cancelada (reserva unitária).";
                    if (UsuariosSindicos.Contains(Cred.TUsuario))
                        lblReservaAlertaSindicoRestricoes.Text += "<br/><br/> * " + strMsg;
                    else
                    {
                        lblErroGravar.Text = strMsg;
                        panErroGravar.Visible = true;
                        return;
                    }
                }

                //Verifica se existe bloqueio de unidade (ou bloco) para os equipamentos selecionados
                if ((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaBloqueioUnidade(Cred.EMP, Convert.ToInt32(lblAPT.Text), strRegraSelecionada, lblData.Text.Substring(0, 10)))
                {
                    if (!panRegras.Visible)
                        strMsg = "Esta reserva não pode ser confirmada porque o espaço está bloqueado para esta unidade e/ou bloco, na data escolhida.";
                    else
                        strMsg = "Esta reserva não pode ser confirmada porque um dos espaços selecionados está bloqueado para esta unidade e/ou bloco, na data escolhida.";
                    if (UsuariosSindicos.Contains(Cred.TUsuario))
                        lblReservaAlertaSindicoRestricoes.Text += "<br/><br/> * " + strMsg;
                    else
                    {
                        lblErroGravar.Text = strMsg;
                        panErroGravar.Visible = true;
                        return;
                    }
                }
            }

            //Verifica se precisa confirmacao do sindico
            if (UsuariosSindicos.Contains(Cred.TUsuario) && lblReservaAlertaSindicoRestricoes.Text != "")
            {
                lblReservaAlertaSindicoConfirmacao.Text = (cmdConfirmar.Text != "Cancelar Reserva") ? "Deseja confirmar a reserva?" : "Deseja confirmar o cancelamento da reserva?";
                lblReservaAlertaSindicoRegraSelecionada.Text = strRegraSelecionada;
                panAlertaSindico.Visible = true;
                panCampos.Visible = false;
            }
            else
            {
                //Grava
                GravaReserva(strRegraSelecionada);
            }
        }

        protected void cmdAlertaCancelar_Click(object sender, EventArgs e)
        {
            //Volta para reserva
            panAlertaSindico.Visible = false;
            panCampos.Visible = true;
        }

        protected void cmdAlertaConfirmar_Click(object sender, EventArgs e)
        {
            //Volta para reserva
            panAlertaSindico.Visible = false;
            panCampos.Visible = true;

            //Grava
            GravaReserva(lblReservaAlertaSindicoRegraSelecionada.Text);
        }

        protected void GravaReserva(string strRegraSelecionada)
        {
            //Grava
            try
            {
                //Registra a reserva
                if (!((bool)dResOld.ReservaEQuipamentoTableAdapter.SetaReserva(Cred.EMP, Convert.ToInt32(lblAPT.Text), cmdConfirmar.Text != "Cancelar Reserva" ? "R" : "C", strRegraSelecionada, lblData.Text.Substring(0, 10), string.Format("Usuário {0}/{1} (Nome: {2} - Apto: {3} - Bloco: {4})", UsuariosTipoSindico.Contains(Cred.TUsuario) ? "S" : "P", ((int)Cred.TUsuario).ToString(), Cred.rowUsuario.Nome, Cred.rowUsuario.APTNumero, Cred.rowUsuario.BLOCodigo), cmdConfirmar.Text != "Cancelar Reserva" ? lblDataHoraReservaEfetuada.Text : "")))
                {
                    if (!panRegras.Visible)
                        lblErroGravar.Text = "Esta reserva não pode ser confirmada porque o espaço pode ter sido reservado por outra unidade neste instante.";
                    else
                        lblErroGravar.Text = "Esta reserva não pode ser confirmada porque alguns dos espaços pode ter sido reservado por outra unidade neste instante. As reservas disponíveis foram atualizadas na lista acima.";
                    panErroGravar.Visible = true;
                    CarregaRegras();
                    return;
                }

                //Monta detalhes do e-mail
                string strDetalhes = string.Format("{0} - Espaço: {1} - Data: {2}", cmdConfirmar.Text == "Reservar" ? "Reserva" : "Cancelamento de Reserva", lblEquipamento.Text, lblData.Text);
                string strDetalhes2 = "";
                string strDetalhes3 = "";
                if (panRegras.Visible)
                    strDetalhes2 = string.Format("{0} em conjunto permitida - Selecionada: {1}", cmdConfirmar.Text == "Reservar" ? "Reserva" : "Cancelamento de Reserva", lblEquipamentosSelecionados.Text);
                if (cmdConfirmar.Text == "Reservar")
                    if (Convert.ToInt32(dResOld.ReservaEQuipamentoTableAdapter.VerificaMaxConvidados(Convert.ToInt32(lblREQ.Text), Cred.EMP)) > 0)
                        strDetalhes3 = "* Preencha a lista de convidados em 'Minhas Reservas'";
                    else if ((panRegras.Visible) && (strRegraSelecionada != "REQ[" + lblREQ.Text + "]EQP"))
                        strDetalhes3 = "* Caso algum dos espaços reservados possua lista de convidados, preencha cada lista em 'Minhas Reservas'";

                //Verifica destino do e-mail
                string strEmail = "";
                if (!Cred.rowUsuario.IsEmailNull()) { strEmail = Cred.rowUsuario.Email; }
                if (UsuariosTipoSindico.Contains(Cred.TUsuario))
                {
                    object strEmailApto = dRes.APARTAMENTOSTableAdapter.VerificaEmail(Convert.ToInt32(lblAPT.Text));
                    if (strEmailApto != null) strEmail = strEmailApto.ToString();
                }

                //Manda e-mail              
                EnviaEmail(Cred.rowUsuario.CONNome, strEmail, txtBloco.Text + " - " + txtApartamento.Text, strDetalhes, strDetalhes2, strDetalhes3);

                //Mostra mensagem de confirmacao reserva processando
                panConfirmacao.Visible = true;
                panAlertaSindico.Visible = false;
                panCampos.Visible = false;
            }
            catch (Exception)
            {
                //Erro
                lblErroGravar.Text = "Erro ao confirmar a reserva. Por favor, tente novamente.";
                panErroGravar.Visible = true;
            }
        }

        protected void cmdOK_Click(object sender, EventArgs e)
        {
            //Volta pans
            panTelaReservas.Visible = true;
            panReserva.Visible = false;
            panBloqueioObservacao.Visible = false;

            //Carrega tela reservas 
            CarregaTelaReservas();
        }

        private void EnviaEmail(string Condominio, string Destino, string Unidade, string Detalhes, string Detalhes2, string Detalhes3)
        {
            try
            {
                //Cria e-mail
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Seta dados de envio padrao
                string strRemetente = ConfigurationManager.AppSettings["EnvioEmailRemetente" + Cred.EMP.ToString()];

                //Verifica SMTP de dominio proprio e seta dados de envio especificos
                if (dRes.ST_SMTpTableAdapter.Fill(dRes.ST_SMTp, Cred.CON) > 0)
                {
                    SmtpClient1.Host = dRes.ST_SMTp[0].SMTpHost;
                    SmtpClient1.Credentials = new System.Net.NetworkCredential(dRes.ST_SMTp[0].SMTpUser, dRes.ST_SMTp[0].SMTpPass);
                    SmtpClient1.Port = Convert.ToInt32(dRes.ST_SMTp[0].SMTpPort);
                    if (!dRes.ST_SMTp[0].IsSMTpRemetenteNull()) strRemetente = dRes.ST_SMTp[0].SMTpRemetente;
                }

                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress(strRemetente.Replace(";", ","));

                    //Bcc
                    string Bcc = "neon@vcommerce.com.br";
                    if (ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != null && ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()] != "")
                        email.Bcc.Add(ConfigurationManager.AppSettings["EnvioEmailCopia" + Cred.EMP.ToString()].Replace(";", ","));
                    if (Bcc != "")
                        email.Bcc.Add(Bcc);
                    dRes.ST_EMailEnvioTableAdapter.Fill(dRes.ST_EMailEnvio, Cred.CON, (int)Tipos.TipoEmailEnvio.Reserva);
                    foreach (dReserva.ST_EMailEnvioRow row in dRes.ST_EMailEnvio.Rows)
                        email.Bcc.Add(row.EMEEmail.ToString().Replace(";", ","));

                    //To
                    if (Destino != "")
                        email.To.Add(Destino);
                    else
                        foreach (dReserva.ST_EMailEnvioRow row in dRes.ST_EMailEnvio.Rows)
                            email.To.Add(row.EMEEmail.ToString().Replace(";", ","));

                    //Verifica se nao tem destino e nao envia
                    if (email.To.Count == 0) return;

                    //Corpo do email
                    email.Subject = "Reserva de Espaço Comum";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>RESERVA DE ESPAÇO COMUM</b></p>\r\n" +
                                               "      <p><b>Data:</b> {0}</p>\r\n" +
                                               "      <p><b>Condomínio:</b> {1}</p>\r\n" +
                                               "      <p><b>Unidade:</b> {2}</p>\r\n" +
                                               "      <p><b>Detalhes:</b></p>\r\n" +
                                               "      <p>{3}</p>\r\n" +
                                               "      <p>{4}</p>\r\n" +
                                               "      <p>{5}</p>\r\n" +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, Server.HtmlEncode(Cred.rowUsuario.CONNome) + " - " + Server.HtmlEncode(Cred.rowUsuario.CONCodigo), 
                                               Server.HtmlEncode(Unidade), Server.HtmlEncode(Detalhes), Server.HtmlEncode(Detalhes2), Server.HtmlEncode(Detalhes3));

                    //Envia email
                    SmtpClient1.Send(email);
                }
            }
            catch (Exception)
            {
            }
        }

        protected void cmdAnterior_Click(object sender, EventArgs e)
        {
            //Seta mes anterior
            if (lblMes.Text == "1")
            {
                lblMes.Text = "12";
                lblAno.Text = Convert.ToString(Convert.ToInt16(lblAno.Text) - 1);
            }
            else
            { lblMes.Text = Convert.ToString(Convert.ToInt16(lblMes.Text) - 1); }

            //Verifica se desabilita anterior
            if (Convert.ToInt16(lblMes.Text) == DateTime.Now.Month) { cmdAnterior.Enabled = false; }

            //Verifica dados de impressao
            CarregaDadosImpressao();

            //Carrega calendario 
            CarregaCalendario();
        }

        protected void cmdProximo_Click(object sender, EventArgs e)
        {
            //Seta mes posterior
            if (lblMes.Text == "12")
            {
                lblMes.Text = "1";
                lblAno.Text = Convert.ToString(Convert.ToInt16(lblAno.Text) + 1);
            }
            else
            { lblMes.Text = Convert.ToString(Convert.ToInt16(lblMes.Text) + 1); }

            //Verifica se desabilita anterior
            if (Convert.ToInt16(lblMes.Text) > DateTime.Now.Month) { cmdAnterior.Enabled = true; }

            //Verifica dados de impressao
            CarregaDadosImpressao();

            //Carrega calendario 
            CarregaCalendario();
        }
    }
}