﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using Reserva.Datasets;

namespace Reserva.Pages_Portal
{
    public partial class ReservaBloqueioData : System.Web.UI.Page
    {
        private dReservas dResOld = new dReservas(); //dataset antigo a ser ajustado para o novo dReserva
        private dReserva dRes = new dReserva();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dResOld = new dReservas(Cred.EMP); //dataset antigo a ser ajustado para o novo dReserva
            dRes = new dReserva(Cred.EMP);

            if (!IsPostBack)
            {
                //Inicia dados
                IniciaDados();

                //Carrega tela
                Carregar();
            }
        }

        private void IniciaDados()
        {
            //Carrega o campo de equipamentos
            dRes.EQuiPamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Cred.CON);
            cmbEquipamento.DataSource = dRes.EQuiPamentos;
            cmbEquipamento.DataBind();
            cmbEquipamento.Text = "";
        }

        private void Carregar()
        {
            //Carrega os bloqueios de data
            dResOld.ReservaBloqueioDataTableAdapter.FillByEMP_CON(dResOld.ReservaBloqueioData, Cred.EMP, Cred.CON);
            grdReservaBloqueioData.DataSource = dResOld.ReservaBloqueioData;
            grdReservaBloqueioData.DataBind();

            //Posiciona no mesmo campo
            this.smnReservaBloqueioData.SetFocus(cmbEquipamento);
        }

        protected void cmbEquipamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Verifica se apagou campo obrigatorio
            if (Convert.ToInt32(cmbEquipamento.Value) == 0)
                cmbEquipamento.SelectedIndex = -1;

            //Posiciona no mesmo campo
            this.smnReservaBloqueioData.SetFocus(cmbEquipamento);
        }

        private void LimpaCampos()
        {
            //Limpa os campos
            cmbEquipamento.SelectedIndex = -1;
            txtDia.Text = "";
            txtMes.Text = "";
            txtAno.Text = "";
            txtObservacao.Text = "";

            //Posiciona no mesmo campo
            this.smnReservaBloqueioData.SetFocus(cmbEquipamento);
        }

        protected void cmdAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Altera o cadastro do bloqueio
            Button cmdAlterar = (Button)sender;
            int ID = int.Parse(cmdAlterar.CommandArgument);

            //Procura pelo registro selecionado para alterar
            dResOld.ReservaBloqueioDataTableAdapter.Fill(dResOld.ReservaBloqueioData);
            dReservas.ReservaBloqueioDataRow rowBloqueio = dResOld.ReservaBloqueioData.FindByRBD(ID);

            //Preenche tela
            lblID.Text = rowBloqueio.RBD.ToString();
            cmbEquipamento.Value = rowBloqueio.RBD_EQP;
            txtDia.Text = rowBloqueio.RBD_Dia;
            txtMes.Text = rowBloqueio.RBD_Mes;
            if (!rowBloqueio.IsRBD_AnoNull()) txtAno.Text = rowBloqueio.RBD_Ano;
            if (!rowBloqueio.IsRBD_ObservacaoNull()) txtObservacao.Text = rowBloqueio.RBD_Observacao;

            //Seta campos               
            floCampos.Visible = true;
            grdReservaBloqueioData.Visible = false;
            cmdCancelar.Visible = true;
            cmdInserirAlterar.Text = "ALTERAR";

            //Posiciona no mesmo campo
            this.smnReservaBloqueioData.SetFocus(cmbEquipamento);
        }

        protected void cmdRemover_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Remove o cadastro do usuario da tabela
            Button cmdRemover = (Button)sender;
            int ID = int.Parse(cmdRemover.CommandArgument);

            //Apaga o registro selecionado para excluir
            dResOld.ReservaBloqueioDataTableAdapter.Delete(ID);

            //Recarrega a tela
            Carregar();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Volta campos
            floCampos.Visible = false;
            grdReservaBloqueioData.Visible = true;
            cmdCancelar.Visible = false;
            cmdInserirAlterar.Text = "INSERIR NOVO BLOQUEIO";
        }

        protected void cmdInserirAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            lblMsg.Text = "";
            panMsg.Visible = false;

            //Verifica se apenas exibe campos para inserir
            if (cmdInserirAlterar.Text == "INSERIR NOVO BLOQUEIO")
            {
                //Limpa os campos da tela
                LimpaCampos();

                //Seta campos               
                floCampos.Visible = true;
                grdReservaBloqueioData.Visible = false;
                cmdCancelar.Visible = true;
                cmdInserirAlterar.Text = "INSERIR";
                return;
            }

            //Faz a consistência dos campos
            if (txtDia.Text == "" || (txtMes.Text == ""))
            {
                lblMsg.Text = "Data inválida. Indique pelo menos os campos referentes ao dia e mês da data de bloqueio.";
                panMsg.Visible = true;
                return;
            }
            DateTime datDataBloqueio;
            string strDataBloqueio = txtDia.Text.PadLeft(2, '0') + "/" + txtMes.Text.PadLeft(2, '0') + "/" + (txtAno.Text == "" ? "1996" : txtAno.Text.PadLeft(4, '0'));
            if (!DateTime.TryParse(strDataBloqueio, out datDataBloqueio))
            {
                lblMsg.Text = "Data inválida. Verifique se os campos indicados formam uma data válida.";
                panMsg.Visible = true;
                return;
            }

            //Verifica se existe reserva na data para o equipamento selecionado (não permite cadastrar bloqueio)
            if ((bool)dResOld.ReservaEQuipamentoTableAdapter.VerificaReservaData(Cred.EMP, (int)cmbEquipamento.SelectedItem.Value, int.Parse(txtDia.Text), int.Parse(txtMes.Text), txtAno.Text == "" ? 0 : int.Parse(txtAno.Text)))
            {
                lblMsg.Text = "ATENÇÃO: Este bloqueio não pode ser confirmado porque o espaço já possui reserva na data escolhida.";
                panMsg.Visible = true;
                return;
            }

            //Insere ou Altera
            try
            {
                //Abre uma conexao
                dResOld.ReservaBloqueioDataTableAdapter.Connection.Open();
                if (cmdInserirAlterar.Text == "INSERIR")
                {
                    //Insere o novo registro
                    dResOld.ReservaBloqueioDataTableAdapter.Insert(
                        Cred.EMP,
                        (int)cmbEquipamento.SelectedItem.Value,
                        txtDia.Text.PadLeft(2, '0'),
                        txtMes.Text.PadLeft(2, '0'),
                        txtAno.Text == "" ? null : txtAno.Text.PadLeft(4, '0'),
                        txtObservacao.Text);

                    //Sucesso
                    lblMsg.Text = "Bloqueio inserido com sucesso !";
                    panMsg.Visible = true;
                }
                else
                {
                    //Altera o registro
                    dResOld.ReservaBloqueioDataTableAdapter.Update(
                        Cred.EMP,
                        (int)cmbEquipamento.SelectedItem.Value,
                        txtDia.Text.PadLeft(2, '0'),
                        txtMes.Text.PadLeft(2, '0'),
                        txtAno.Text == "" ? null : txtAno.Text.PadLeft(4, '0'),
                        txtObservacao.Text,
                        Convert.ToInt32(lblID.Text));

                    //Sucesso
                    lblMsg.Text = "Bloqueio alterado com sucesso !";
                    panMsg.Visible = true;
                }
            }
            catch (Exception)
            {
                //Erro
                lblMsg.Text = "Ocorreu um erro no cadastro do bloqueio, tente novamente.";
                panMsg.Visible = true;
                return;
            }
            finally
            {
                //Fecha a conexao
                dResOld.ReservaBloqueioDataTableAdapter.Connection.Close();
            }

            //Recarrega a tela
            Carregar();

            //Volta campos
            floCampos.Visible = false;
            grdReservaBloqueioData.Visible = true;
            cmdCancelar.Visible = false;
            cmdInserirAlterar.Text = "INSERIR NOVO BLOQUEIO";
        }
    }
}