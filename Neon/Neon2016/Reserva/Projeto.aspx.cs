﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalUtil;

namespace Reserva
{
    public partial class Projeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void cmdReservaSindico_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/Reserva.aspx"); //Sindico - 000999 (SB)
            //if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/Reserva.aspx"); //Sindico - ALGRAN (SA)
            if (Login("1robert68", "sindico197")) Response.Redirect("~/Pages_Portal/Reserva.aspx"); //Sindico - ALGRAN (SA)
        }

        protected void cmdReservaCondomino_Click(object sender, EventArgs e)
        {
            //if (Login("rod", "062129")) Response.Redirect("~/Pages_Portal/Reserva.aspx"); //Proprietario - 000999 (SB)
            //if (Login("rodsa", "062129")) Response.Redirect("~/Pages_Portal/Reserva.aspx"); //Proprietario - MANACA (SA)
            //if (Login("3rosana18", "4773")) Response.Redirect("~/Pages_Portal/Reserva.aspx"); //Proprietario - ALGRAN (SA)
            if (Login("1onofre", "cebola40")) Response.Redirect("~/Pages_Portal/Reserva.aspx"); //Proprietario - ITALIA (SB) 
        }

        protected void cmdReservaGerentePredial_Click(object sender, EventArgs e)
        {
            if (Login("gp000999", "gp")) Response.Redirect("~/Pages_Portal/Reserva.aspx"); //Gerente Predial - 000999 (SB)
        }

        protected void cmdReservaBloqueioData_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/ReservaBloqueioData.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/ReservaBloqueioData.aspx"); //Sindico - ALGRAN (SA)
            //if (Login("1monise1", "9191")) Response.Redirect("~/Pages_Portal/ReservaBloqueioData.aspx"); //Sindico - ANIMMA (SB)
        }

        protected void cmdReservaBloqueioUnidade_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/ReservaBloqueioUnidade.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/ReservaBloqueioUnidade.aspx"); //Sindico - ALGRAN (SA)
        }

        protected void cmdReservaHistoricoAdm_Click(object sender, EventArgs e)
        {
            if (Login("rodino", "062129")) Response.Redirect("~/Pages_Portal/ReservaHistorico.aspx"); //Admin
        }

        protected void cmdReservaHistorico_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/ReservaHistorico.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/ReservaHistorico.aspx"); //Sindico - ALGRAN (SA)  
        }

        protected bool Login(string strUsuario, string strSenha)
        {
            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(strUsuario, strSenha);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = strUsuario;
                Session["Pass"] = strSenha;

                //Autenticou
                return true;
            }
            
            //Não autenticou
            return false;
        }
    }
}