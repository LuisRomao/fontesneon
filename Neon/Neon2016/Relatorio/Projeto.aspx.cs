﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalUtil;

namespace Relatorio
{
    public partial class Projeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void cmdCondominiosADM_Click(object sender, EventArgs e)
        {
            if (Login("rodino", "062129")) Response.Redirect("~/Pages_Portal/Condominios.aspx"); //Admin
        }

        protected void cmdCondominiosADV_Click(object sender, EventArgs e)
        {
            if (Login("3blanca", "xneonx")) Response.Redirect("~/Pages_Portal/Condominios.aspx"); //Adv Admin
        }

        protected void cmdCondominos_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/Condominos.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/Condominos.aspx"); //Sindico - ALGRAN (SA)
            //if (Login("por000999", "por")) Response.Redirect("~/Pages_Portal/Condominos.aspx"); //Portaria - 000999 (SB)
            //if (Login("gp000999", "gp")) Response.Redirect("~/Pages_Portal/Condominos.aspx"); //Gerente Predial - 000999 (SB)
        }

        protected void cmdAcordos_Click(object sender, EventArgs e)
        {
            if (Login("2alexan26", "9233")) Response.Redirect("~/Pages_Portal/Acordos.aspx"); //Sindico - 000545 (SB)
            //if (Login("1luiz10", "0152")) Response.Redirect("~/Pages_Portal/Acordos.aspx"); //Sindico - 000545 (SB)
            //if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/Acordos.aspx"); //Sindico - ALGRAN (SA)
        }

        protected void cmdInadimplentes_Click(object sender, EventArgs e)
        {
            //if (Login("1marli50", "3631")) Response.Redirect("~/Pages_Portal/Inadimplentes.aspx"); //Sindico - 000999 (SB)
            if (Login("3sergio77", "1550")) Response.Redirect("~/Pages_Portal/Inadimplentes.aspx"); //Sindico - ALGRAN (SA)
            //if (Login("3elenil1", "2374")) Response.Redirect("~/Pages_Portal/Inadimplentes.aspx"); //Sindico - PFRANC (SA)
        }

        protected void cmdLocacao_Click(object sender, EventArgs e)
        {
            //if (Login("41", "sev")) Response.Redirect("~/Pages_Portal/Locacao.aspx"); //Casari Imoveis (SB)
            if (Login("64", "co1_sa")) Response.Redirect("~/Pages_Portal/Locacao.aspx"); //Colonia Imoveis (SA)
        }

        protected bool Login(string strUsuario, string strSenha)
        {
            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(strUsuario, strSenha);
            if (Cred != null)
            {
                //Seta variaveis de sessao
                Session["Cred"] = Cred;
                Session["User"] = strUsuario;
                Session["Pass"] = strSenha;

                //Autenticou
                return true;
            }
            
            //Não autenticou
            return false;
        }
    }
}