﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using DevExpress.Web.ASPxPivotGrid;
using PortalUtil;
using BoletoUtil;
using BoletoUtil.Datasets;
using Relatorio.Datasets;

namespace Relatorio.Pages_Portal
{
    public partial class Inadimplentes : System.Web.UI.Page
    {
        private dRelatorio dRel = new dRelatorio();
        private dBoleto dBol = new dBoleto();

        private VirBoleto CurBoleto;
        DataRow rowINA = null;

        private decimal decOriginal;
        private decimal decCorrigido;
        private decimal decMulta;
        private decimal decJuros;
        private decimal decSubtotal;
        private decimal decHonorarios;
        private decimal decTotal;
        private decimal decFinalOriginal;
        private decimal decFinalCorrigido;
        private decimal decFinalMulta;
        private decimal decFinalJuros;
        private decimal decFinalSubtotal;
        private decimal decFinalHonorarios;
        private decimal decFinalTotal;

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "12px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif"; 

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia datasets
            dRel = new dRelatorio(Cred.EMP);
            dBol = new dBoleto(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Inicia campos
            DateTime datCalculo = DateTime.Today;
            lblDataCalculo.Text = datCalculo.ToString("dd/MM/yyyy");
            lblDataCorte.Text = datCalculo.AddDays(-4).ToString("dd/MM/yyyy");

            //Seta o condominio
            dBol.CONDOMINIOSTableAdapter.Fill(dBol.CONDOMINIOS, Cred.CON);

            //Carrega os boletos inadimplentes
            dBol.BoletosTableAdapter.FillByInadimplencia(dBol.Boletos, Cred.CON, datCalculo.AddDays(-3));

            //Inicia boleto
            CurBoleto = new VirBoleto(dBol, true);
            if ((Cred.CON == 732) && (Cred.EMP == 1))
                CurBoleto.jurosCompostos = false;

            //Calcula valores, totais e insere na tabela de inadimplentes
            dRel.Inadimplentes.Clear();
            string strGrupo = "";
            decOriginal = decCorrigido = decMulta = decJuros = decSubtotal = decHonorarios = decTotal = 0;
            decFinalOriginal = decFinalCorrigido = decFinalMulta = decFinalJuros = decFinalSubtotal = decFinalHonorarios = decFinalTotal = 0;
            foreach (dBoleto.BoletosRow rowBOL in dBol.Boletos)
            {
                //Verifica se mudou de bloco / apto
                if (strGrupo != "" && strGrupo != String.Format("{0}.{1}.{2}", rowBOL.BLO, rowBOL.APT, rowBOL.BOLProprietario ? "P" : "I"))
                {
                    //Insere total se tem mais de um boleto
                    InsereTotal(0);

                    //Reinicia valores totais do bloco / apto
                    decOriginal = decCorrigido = decMulta = decJuros = decSubtotal = decHonorarios = decTotal = 0;
                }

                //Pega chave de grupo
                strGrupo = String.Format("{0}.{1}.{2}", rowBOL.BLO, rowBOL.APT, rowBOL.BOLProprietario ? "P" : "I");

                //Pega o boleto e calcula valores
                CurBoleto.CarregaDados(rowBOL);
                CurBoleto.Calcula(datCalculo, false);

                //Adiciona na lista
                rowINA = dRel.Inadimplentes.NewRow();
                rowINA["BOL"] = rowBOL.BOL;
                rowINA["BLOCodigo"] = rowBOL.BLOCodigo;
                rowINA["APTNumero"] = rowBOL.APTNumero;
                rowINA["BOLTipoUsuario"] = rowBOL.BOLTipoUsuario;
                rowINA["BOLVencto"] = rowBOL.dtvencto.ToString("dd/MM/yyyy");
                rowINA["BOLOriginal"] = CurBoleto.valororiginal;
                rowINA["BOLCorrigido"] = CurBoleto.ValorCorrigido;
                rowINA["BOLMulta"] = CurBoleto.multa;
                rowINA["BOLJuros"] = CurBoleto.juros;
                rowINA["BOLSubtotal"] = CurBoleto.valorfinal;
                rowINA["BOLHonorarios"] = CurBoleto.honor;
                rowINA["BOLTotal"] = CurBoleto.totcomhonor;
                dRel.Inadimplentes.Rows.Add(rowINA);

                //Soma nos totais
                decOriginal += CurBoleto.valororiginal;
                decFinalOriginal += CurBoleto.valororiginal;
                decCorrigido += CurBoleto.ValorCorrigido;
                decFinalCorrigido += CurBoleto.ValorCorrigido;
                decMulta += CurBoleto.multa;
                decFinalMulta += CurBoleto.multa;
                decJuros += CurBoleto.juros;
                decFinalJuros += CurBoleto.juros;
                decSubtotal += CurBoleto.valorfinal;
                decFinalSubtotal += CurBoleto.valorfinal;
                decHonorarios += CurBoleto.honor;
                decFinalHonorarios += CurBoleto.honor;
                decTotal += CurBoleto.totcomhonor;
                decFinalTotal += CurBoleto.totcomhonor;
            }

            //Insere total do ultimo se tem mais de um boleto
            InsereTotal(0);

            //Insere total geral
            InsereTotal(1);

            //Carrega os inadimplentes
            grdInadimplentes.DataSource = dRel.Inadimplentes;
            grdInadimplentes.DataBind();
        }
        
        private void InsereTotal(int intNivel)
        {
            //Insere total
            rowINA = dRel.Inadimplentes.NewRow();
            if (intNivel == 0)
                rowINA["BOLVencto"] = "Total";
            else
                rowINA["BLOCodigo"] = "Total Geral";
            rowINA["BOLOriginal"] = intNivel == 0 ? decOriginal : decFinalOriginal;
            rowINA["BOLCorrigido"] = intNivel == 0 ? decCorrigido : decFinalCorrigido;
            rowINA["BOLMulta"] = intNivel == 0 ? decMulta : decFinalMulta;
            rowINA["BOLJuros"] = intNivel == 0 ? decJuros : decFinalJuros;
            rowINA["BOLSubtotal"] = intNivel == 0 ? decSubtotal : decFinalSubtotal;
            rowINA["BOLHonorarios"] = intNivel == 0 ? decHonorarios : decFinalHonorarios;
            rowINA["BOLTotal"] = intNivel == 0 ? decTotal : decFinalTotal;
            dRel.Inadimplentes.Rows.Add(rowINA);

            //Insere linha em branco
            if (intNivel == 0)
            {
                rowINA = dRel.Inadimplentes.NewRow();
                dRel.Inadimplentes.Rows.Add(rowINA);
            }
        }

        protected void grdInadimplentes_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.GetValue("BOLVencto").ToString().ToUpper() == "TOTAL")
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(248, 248, 248);
                e.Row.Font.Bold = true;
            }
            else if (e.GetValue("BLOCodigo").ToString().ToUpper() == "TOTAL GERAL")
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(245, 245, 245);
                e.Row.Font.Bold = true;
            }
        }


        protected void grdInadimplentes_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.CellValue.ToString().ToUpper() == "TOTAL")
            {
                e.Cell.Font.Size = 9;
                e.Cell.Font.Bold = true;
            }
        }

        public static string FormataSaida(decimal decValor, string strZero)
        {
            return (decValor == 0) ? strZero : string.Format("{0:n2}", decValor);
        }

        protected void grdInadimplentes_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.Caption == "Original" || e.Column.Caption == "Corrigido" || e.Column.Caption == "Subtotal" || e.Column.Caption == "Total")
                e.DisplayText = e.Value == DBNull.Value ? "" : FormataSaida(Convert.ToDecimal(e.Value), "0,00");
            else if (e.Column.Caption == "Multa" || e.Column.Caption == "Juros" || e.Column.Caption == "Honorários")
                e.DisplayText = e.Value == DBNull.Value ? "" : FormataSaida(Convert.ToDecimal(e.Value), "");
        }
    }
}