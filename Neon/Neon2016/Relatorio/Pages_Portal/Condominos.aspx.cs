﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using PortalUtil.Datasets;
using Relatorio.Datasets;

namespace Boleto.Pages_Portal
{
    public partial class Condominos : System.Web.UI.Page
    {
        private dRelatorio dRel = new dRelatorio();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "12px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dRel = new dRelatorio(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega os condominos
            dRel.CondominosTableAdapter.Fill(dRel.Condominos, Cred.CON);
            grdCondominos.DataSource = dRel.Condominos;
            grdCondominos.DataBind();

            //Exibe ou nao coluna de boletos do condomino
            grdCondominos.Columns["colBoletos"].Visible = (Cred.TUsuario != Credencial.TipoUsuario.Zelador && Cred.TUsuario != Credencial.TipoUsuario.Portaria) ? true : false;
        }

        protected void cmdBoletos_Click(object sender, EventArgs e)
        {
            //Abre boletos do condomino selecionado
            Button cmdBoleto = (Button)sender;
            string Proprietario = Convert.ToString(cmdBoleto.CommandName);
            int APT = int.Parse(cmdBoleto.CommandArgument);

            //Pega credenciais do condomino selecionado e redireciona para os seus boletos
            dCredencial dCred = new dCredencial();
            Credencial.TipoAut TipoAut;
            PortalUtil.Datasets.dCredencialTableAdapters.AutenticaTableAdapter taAUT = new PortalUtil.Datasets.dCredencialTableAdapters.AutenticaTableAdapter();
            if (Cred.EMP == 3)
            {
                taAUT.Connection.ConnectionString = taAUT.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                TipoAut = Credencial.TipoAut.NeonSA;
            }
            else
                TipoAut = Credencial.TipoAut.Neon;
            if (taAUT.FillByAPTPropInq(dCred.Autentica, APT, Proprietario) > 0)
            {
                dCredencial.AutenticaRow rowAutentica = dCred.Autentica[0];
                dCredencial.UsuarioRow rowUsuario = dCred.Usuario.NewUsuarioRow();
                rowUsuario.EMP = rowAutentica.EMP;
                rowUsuario.CON = rowAutentica.CON;
                rowUsuario.APT = rowAutentica.APT;
                rowUsuario.PES = rowAutentica.PES;
                rowUsuario.Nome = rowAutentica.Nome;
                rowUsuario.CONCodigo = rowAutentica.CONCodigo;
                rowUsuario.CONNome = rowAutentica.CONNome;
                rowUsuario.BLOCodigo = rowAutentica.BLOCodigo;
                rowUsuario.APTNumero = rowAutentica.APTNumero;
                rowUsuario.Email = rowAutentica.Email;
                rowUsuario.CpfCnpj = rowAutentica.CpfCnpj;
                rowUsuario.TipoUsuario = rowAutentica.TipoUsuario;
                rowUsuario.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                Credencial CredAPT = new Credencial(TipoAut, rowUsuario, Cred.EMP, Cred.CON);
                Session["Cred" + APT] = CredAPT;
                Response.Redirect("~/Pages_Portal/Boleto.aspx?CredAPT=" + APT);
            };
        }
    }
}