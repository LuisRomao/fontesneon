﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DevExpress.Web;
using PortalUtil;
using Relatorio.Datasets;

namespace Relatorio.Pages_Portal
{
    public partial class LocacaoBoletos : System.Web.UI.Page
    {
        private dRelatorio dRel = new dRelatorio();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dRel = new dRelatorio(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega os boletos da unidade
            int intAPT = Request.QueryString["APT"] == null ? 0 : Convert.ToInt32(Request.QueryString["APT"]);
            dRel.LocacaoBoletosTableAdapter.Fill(dRel.LocacaoBoletos, intAPT, Cred.rowUsuario.FRN);
            grdLocacaoBoletos.DataSource = dRel.LocacaoBoletos;
            grdLocacaoBoletos.DataBind();
        }
    }
}