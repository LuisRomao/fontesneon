﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using Relatorio.Datasets;

namespace Relatorio.Pages_Portal
{
    public partial class Acordos : System.Web.UI.Page
    {
        private dRelatorio dRel = new dRelatorio();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "12px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif"; 

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dRel = new dRelatorio(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega os acordos
            dRel.ACOrdosTableAdapter.Fill(dRel.ACOrdos, Cred.CON);
            grdAcordos.DataSource = dRel.ACOrdos;
            grdAcordos.DataBind();
        }

        protected void grdAcordos_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Caption.Contains("Boletos"))
            {
                //Carrega os boletos do apt
                ASPxGridView grid = grdAcordos.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "grdBoletos") as ASPxGridView;
                dRel.ACOxBOLTableAdapter.FillByOriginal(dRel.ACOxBOL, Cred.CON, Convert.ToInt32(e.CellValue));
                grid.DataSource = dRel.ACOxBOL;
                grid.DataBind();
            }
            else if (e.DataColumn.Caption.Contains("Parcelas"))
            {
                //Carrega as parcelas do apt
                ASPxGridView grid = grdAcordos.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "grdParcelas") as ASPxGridView;
                dRel.ACOxBOLTableAdapter.FillByNovo(dRel.ACOxBOL, Cred.CON, Convert.ToInt32(e.CellValue));
                grid.DataSource = dRel.ACOxBOL;
                grid.DataBind();
            }
        }
        
        protected void grdParcelas_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.Caption == "N°")
                e.DisplayText = (e.VisibleIndex + 1).ToString();
        }
    }
}