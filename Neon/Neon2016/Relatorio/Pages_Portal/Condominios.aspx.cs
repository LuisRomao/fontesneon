﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using PortalUtil;
using Relatorio.Datasets;

namespace Relatorio.Pages_Portal
{
    public partial class Condominios : System.Web.UI.Page
    {
        private dRelatorio dRel = new dRelatorio();

        private Credencial Cred
        {
            get
            {
                return Credencial.CredST(this, true);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Seta a cor base do tema da pagina
            if (Cred.CONCorBase != "")
            {
                ASPxWebControl.GlobalThemeBaseColor = Cred.CONCorBase;
                lblTituloPagina.ForeColor = System.Drawing.ColorTranslator.FromHtml(Cred.CONCorBase);
            }

            //Seta font
            //ASPxWebControl.GlobalThemeFont = "13px 'Roboto Regular', Helvetica, Tahoma, Geneva, sans-serif";

            //Seta padding de tela
            if (Request.Browser.IsMobileDevice)
                panForm.ContentPaddings.PaddingLeft = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia dataset
            dRel = new dRelatorio(Cred.EMP);

            if (!IsPostBack)
            {
                //Carrega site
                rblSite.DataBind();
            }
        }

        protected void rblSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Carrega os condominios do site selecionado
            Carregar();
        }

        private void Carregar()
        {
            //Seta EMP
            dRel.EMP = Convert.ToInt32(rblSite.SelectedItem.Value);

            //Carrega os condominios (somente ativos)
            dRel.CONDOMINIOSTableAdapter.FillAtivos(dRel.CONDOMINIOS, Convert.ToInt32(rblSite.SelectedItem.Value));
            grdCondominios.DataSource = dRel.CONDOMINIOS;
            grdCondominios.DataBind();

            //Exibe ou nao coluna para configurar condominio
            grdCondominios.Columns["colConfigurar"].Visible = (Cred.TUsuario == Credencial.TipoUsuario.Administradora) ? true : false;

            //Exibe grid
            grdCondominios.Visible = true;
        }

        protected void grdCondominios_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Caption == "Abrir")
            {
                Button btn = grdCondominios.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "cmdAbrir") as Button;
                btn.OnClientClick = String.Format("javascript: window.open('Portal.aspx?Page={0}&EMP={1}&CON={2}', '_parent'); return false;", "CadastroCondominio.aspx", Convert.ToInt32(rblSite.SelectedItem.Value), Convert.ToString(e.CellValue));
            }
            else if (e.DataColumn.Caption == "Configurar")
            {
                Button btn = grdCondominios.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "cmdConfigurar") as Button;
                btn.OnClientClick = String.Format("javascript: window.open('Portal.aspx?Page={0}&EMP={1}&CON={2}', '_parent'); return false;", "CadastroCondominioConfiguracao.aspx", Convert.ToInt32(rblSite.SelectedItem.Value), Convert.ToString(e.CellValue));
            }
        }
    }
}