﻿namespace Relatorio.Datasets
{
    partial class dRelatorio
    {
        public int EMP;

        public dRelatorio(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dRelatorioTableAdapters.CONDOMINIOSTableAdapter taCONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dRelatorioTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (taCONDOMINIOSTableAdapter == null)
                {
                    taCONDOMINIOSTableAdapter = new dRelatorioTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP == 3)
                        taCONDOMINIOSTableAdapter.Connection.ConnectionString = taCONDOMINIOSTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCONDOMINIOSTableAdapter;
            }
        }

        private dRelatorioTableAdapters.CondominosTableAdapter taCondominosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Condominos
        /// </summary>
        public dRelatorioTableAdapters.CondominosTableAdapter CondominosTableAdapter
        {
            get
            {
                if (taCondominosTableAdapter == null)
                {
                    taCondominosTableAdapter = new dRelatorioTableAdapters.CondominosTableAdapter();
                    if (EMP == 3)
                        taCondominosTableAdapter.Connection.ConnectionString = taCondominosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taCondominosTableAdapter;
            }
        }

        private dRelatorioTableAdapters.ACOrdosTableAdapter taACOrdosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ACOrdos
        /// </summary>
        public dRelatorioTableAdapters.ACOrdosTableAdapter ACOrdosTableAdapter
        {
            get
            {
                if (taACOrdosTableAdapter == null)
                {
                    taACOrdosTableAdapter = new dRelatorioTableAdapters.ACOrdosTableAdapter();
                    if (EMP == 3)
                        taACOrdosTableAdapter.Connection.ConnectionString = taACOrdosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taACOrdosTableAdapter;
            }
        }

        private dRelatorioTableAdapters.ACOxBOLTableAdapter taACOxBOLTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ACOxBOL
        /// </summary>
        public dRelatorioTableAdapters.ACOxBOLTableAdapter ACOxBOLTableAdapter
        {
            get
            {
                if (taACOxBOLTableAdapter == null)
                {
                    taACOxBOLTableAdapter = new dRelatorioTableAdapters.ACOxBOLTableAdapter();
                    if (EMP == 3)
                        taACOxBOLTableAdapter.Connection.ConnectionString = taACOxBOLTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                }
                return taACOxBOLTableAdapter;
            }
        }

        private dRelatorioTableAdapters.LocacaoTableAdapter taLocacaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Locacao
        /// </summary>
        public dRelatorioTableAdapters.LocacaoTableAdapter LocacaoTableAdapter
        {
            get
            {
                if (taLocacaoTableAdapter == null)
                {
                    taLocacaoTableAdapter = new dRelatorioTableAdapters.LocacaoTableAdapter();
                    if (EMP == 3)
                        taLocacaoTableAdapter.Connection.ConnectionString = taLocacaoTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taLocacaoTableAdapter;
            }
        }

        private dRelatorioTableAdapters.LocacaoBoletosTableAdapter taLocacaoBoletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: LocacaoBoletos
        /// </summary>
        public dRelatorioTableAdapters.LocacaoBoletosTableAdapter LocacaoBoletosTableAdapter
        {
            get
            {
                if (taLocacaoBoletosTableAdapter == null)
                {
                    taLocacaoBoletosTableAdapter = new dRelatorioTableAdapters.LocacaoBoletosTableAdapter();
                    if (EMP == 3)
                        taLocacaoBoletosTableAdapter.Connection.ConnectionString = taLocacaoBoletosTableAdapter.Connection.ConnectionString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                };
                return taLocacaoBoletosTableAdapter;
            }
        }
    }
}
