﻿namespace TesteBoletos
{
    partial class cTestaTMP_Para_SCC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dCargaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTMPSCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPValorI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMP_ARQ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMP_BCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPCNPJ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPAplicacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPContaDg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPAgenciaDg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPRetroativo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCargaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "TMP_SCC";
            this.gridControl1.DataSource = this.dCargaBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "FK_TMP_CCD_TMP_SCC";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 100);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1492, 707);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // dCargaBindingSource
            // 
            this.dCargaBindingSource.DataSource = typeof(dllbanco.CarregaExtrato.dCarga);
            this.dCargaBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTMPSCC,
            this.colTMPData,
            this.colTMPValorI,
            this.colTMP_ARQ,
            this.colTMP_BCO,
            this.colTMPAgencia,
            this.colTMPConta,
            this.colTMPCNPJ,
            this.colTMPAplicacao,
            this.colTMPContaDg,
            this.colTMPAgenciaDg,
            this.colTMPRetroativo});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTMPConta, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTMPAplicacao, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTMPData, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colTMPSCC
            // 
            this.colTMPSCC.FieldName = "TMPSCC";
            this.colTMPSCC.Name = "colTMPSCC";
            this.colTMPSCC.Visible = true;
            this.colTMPSCC.VisibleIndex = 2;
            // 
            // colTMPData
            // 
            this.colTMPData.FieldName = "TMPData";
            this.colTMPData.Name = "colTMPData";
            this.colTMPData.Visible = true;
            this.colTMPData.VisibleIndex = 0;
            // 
            // colTMPValorI
            // 
            this.colTMPValorI.FieldName = "TMPValorI";
            this.colTMPValorI.Name = "colTMPValorI";
            this.colTMPValorI.Visible = true;
            this.colTMPValorI.VisibleIndex = 3;
            // 
            // colTMP_ARQ
            // 
            this.colTMP_ARQ.FieldName = "TMP_ARQ";
            this.colTMP_ARQ.Name = "colTMP_ARQ";
            this.colTMP_ARQ.Visible = true;
            this.colTMP_ARQ.VisibleIndex = 4;
            // 
            // colTMP_BCO
            // 
            this.colTMP_BCO.FieldName = "TMP_BCO";
            this.colTMP_BCO.Name = "colTMP_BCO";
            this.colTMP_BCO.Visible = true;
            this.colTMP_BCO.VisibleIndex = 5;
            // 
            // colTMPAgencia
            // 
            this.colTMPAgencia.FieldName = "TMPAgencia";
            this.colTMPAgencia.Name = "colTMPAgencia";
            this.colTMPAgencia.Visible = true;
            this.colTMPAgencia.VisibleIndex = 6;
            // 
            // colTMPConta
            // 
            this.colTMPConta.FieldName = "TMPConta";
            this.colTMPConta.Name = "colTMPConta";
            this.colTMPConta.Visible = true;
            this.colTMPConta.VisibleIndex = 7;
            // 
            // colTMPCNPJ
            // 
            this.colTMPCNPJ.FieldName = "TMPCNPJ";
            this.colTMPCNPJ.Name = "colTMPCNPJ";
            this.colTMPCNPJ.Visible = true;
            this.colTMPCNPJ.VisibleIndex = 8;
            // 
            // colTMPAplicacao
            // 
            this.colTMPAplicacao.FieldName = "TMPAplicacao";
            this.colTMPAplicacao.Name = "colTMPAplicacao";
            this.colTMPAplicacao.Visible = true;
            this.colTMPAplicacao.VisibleIndex = 9;
            // 
            // colTMPContaDg
            // 
            this.colTMPContaDg.FieldName = "TMPContaDg";
            this.colTMPContaDg.Name = "colTMPContaDg";
            this.colTMPContaDg.Visible = true;
            this.colTMPContaDg.VisibleIndex = 10;
            // 
            // colTMPAgenciaDg
            // 
            this.colTMPAgenciaDg.FieldName = "TMPAgenciaDg";
            this.colTMPAgenciaDg.Name = "colTMPAgenciaDg";
            this.colTMPAgenciaDg.Visible = true;
            this.colTMPAgenciaDg.VisibleIndex = 11;
            // 
            // colTMPRetroativo
            // 
            this.colTMPRetroativo.FieldName = "TMPRetroativo";
            this.colTMPRetroativo.Name = "colTMPRetroativo";
            this.colTMPRetroativo.Visible = true;
            this.colTMPRetroativo.VisibleIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.memoEdit1);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1492, 100);
            this.panelControl1.TabIndex = 0;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(615, 20);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(895, 65);
            this.memoEdit1.TabIndex = 5;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(407, 20);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(171, 48);
            this.simpleButton3.TabIndex = 4;
            this.simpleButton3.Text = "abrir extrato";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(204, 20);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(168, 48);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "TMP->SCC";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(18, 20);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(168, 48);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Carregar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cTestaTMP_Para_SCC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cTestaTMP_Para_SCC";
            this.Size = new System.Drawing.Size(1492, 807);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCargaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.BindingSource dCargaBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPSCC;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPData;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPValorI;
        private DevExpress.XtraGrid.Columns.GridColumn colTMP_ARQ;
        private DevExpress.XtraGrid.Columns.GridColumn colTMP_BCO;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPConta;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPCNPJ;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPAplicacao;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPContaDg;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPAgenciaDg;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPRetroativo;
    }
}
