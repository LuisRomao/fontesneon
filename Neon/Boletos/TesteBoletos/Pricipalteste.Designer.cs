namespace TesteBoletos
{
    partial class Pricipalteste
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            CompontesBasicos.ModuleInfo moduleInfo1 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo2 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo3 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo4 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo5 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo6 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo7 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo8 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo9 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo10 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo11 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo12 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo13 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo14 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo15 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo16 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo17 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo18 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo19 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo20 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo21 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo22 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo23 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo24 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo25 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo26 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo27 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo28 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo29 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo30 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo31 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo32 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo33 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo34 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo35 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo36 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo37 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo38 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo39 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo40 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo41 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo42 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo43 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo44 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo45 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo46 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo47 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo48 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo49 = new CompontesBasicos.ModuleInfo();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem6 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem3 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem12 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem13 = new DevExpress.XtraNavBar.NavBarItem();
            this.TesteDatas = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup3 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup4 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem7 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup5 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarImpressos = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem8 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem9 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem5 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup6 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem11 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem10 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup7 = new DevExpress.XtraNavBar.NavBarGroup();
            this.a = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem4 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup8 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup9 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup10 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup11 = new DevExpress.XtraNavBar.NavBarGroup();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.DockPanel_FContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            this.DockPanel_F.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureEdit_F
            // 
            this.PictureEdit_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.PictureEdit_F.Margin = new System.Windows.Forms.Padding(2);
            // 
            // NavBarControl_F
            // 
            this.NavBarControl_F.ActiveGroup = this.navBarGroup7;
            this.NavBarControl_F.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1,
            this.TesteDatas,
            this.navBarGroup2,
            this.navBarGroup3,
            this.navBarGroup4,
            this.navBarGroup5,
            this.navBarImpressos,
            this.navBarGroup6,
            this.navBarGroup7,
            this.a,
            this.navBarGroup8,
            this.navBarGroup9,
            this.navBarGroup10,
            this.navBarGroup11});
            this.NavBarControl_F.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem1,
            this.navBarItem2,
            this.navBarItem3,
            this.navBarItem6,
            this.navBarItem7,
            this.navBarItem8,
            this.navBarItem9,
            this.navBarItem10,
            this.navBarItem11,
            this.navBarItem4,
            this.navBarItem5,
            this.navBarItem12,
            this.navBarItem13});
            this.NavBarControl_F.Margin = new System.Windows.Forms.Padding(2);
            this.NavBarControl_F.OptionsNavPane.ExpandedWidth = 194;
            this.NavBarControl_F.Padding = new System.Windows.Forms.Padding(2);
            this.NavBarControl_F.Size = new System.Drawing.Size(194, 430);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            this.DefaultLookAndFeel_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.DefaultLookAndFeel_F.LookAndFeel.UseWindowsXPTheme = true;
            // 
            // DefaultToolTipController_F
            // 
            // 
            // 
            // 
            this.DefaultToolTipController_F.DefaultController.AutoPopDelay = 10000;
            this.DefaultToolTipController_F.DefaultController.Rounded = true;
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // DockPanel_FContainer
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.DockPanel_FContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.DockPanel_FContainer.Location = new System.Drawing.Point(3, 20);
            this.DockPanel_FContainer.Margin = new System.Windows.Forms.Padding(2);
            this.DockPanel_FContainer.Size = new System.Drawing.Size(194, 511);
            // 
            // DockPanel_F
            // 
            this.DockPanel_F.Appearance.BackColor = System.Drawing.Color.White;
            this.DockPanel_F.Appearance.Options.UseBackColor = true;
            this.DockPanel_F.Options.AllowDockFill = false;
            this.DockPanel_F.Options.ShowCloseButton = false;
            this.DockPanel_F.Options.ShowMaximizeButton = false;
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "navBarGroup1";
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem6),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem3),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem2),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem1),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem12),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem13)});
            this.navBarGroup1.Name = "navBarGroup1";
            this.navBarGroup1.ItemChanged += new System.EventHandler(this.navBarGroup1_ItemChanged);
            // 
            // navBarItem6
            // 
            this.navBarItem6.Caption = "Amarrar boletos caixa";
            this.navBarItem6.Name = "navBarItem6";
            this.navBarItem6.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem6_LinkClicked);
            // 
            // navBarItem3
            // 
            this.navBarItem3.Caption = "Alarmes D�bito";
            this.navBarItem3.Name = "navBarItem3";
            this.navBarItem3.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem3_LinkClicked);
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "Editar";
            this.navBarItem2.Name = "navBarItem2";
            this.navBarItem2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem2_LinkClicked);
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "Relatorio Colunas";
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // navBarItem12
            // 
            this.navBarItem12.Caption = "Teste Impresso FGTS";
            this.navBarItem12.Name = "navBarItem12";
            this.navBarItem12.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem12_LinkClicked_1);
            // 
            // navBarItem13
            // 
            this.navBarItem13.Caption = "Erro da Caixa";
            this.navBarItem13.Name = "navBarItem13";
            this.navBarItem13.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem13_LinkClicked);
            // 
            // TesteDatas
            // 
            this.TesteDatas.Caption = "BOLETOS";
            this.TesteDatas.Expanded = true;
            this.TesteDatas.Name = "TesteDatas";
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Agenda Datas";
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // navBarGroup3
            // 
            this.navBarGroup3.Caption = "Condom�nio";
            this.navBarGroup3.Name = "navBarGroup3";
            // 
            // navBarGroup4
            // 
            this.navBarGroup4.Caption = "Teste Rateio";
            this.navBarGroup4.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem7)});
            this.navBarGroup4.Name = "navBarGroup4";
            // 
            // navBarItem7
            // 
            this.navBarItem7.Caption = "Simula Rateio";
            this.navBarItem7.Name = "navBarItem7";
            this.navBarItem7.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem7_LinkClicked);
            // 
            // navBarGroup5
            // 
            this.navBarGroup5.Caption = "Retorno";
            this.navBarGroup5.Name = "navBarGroup5";
            // 
            // navBarImpressos
            // 
            this.navBarImpressos.Caption = "Impressos";
            this.navBarImpressos.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem8),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem9),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem5)});
            this.navBarImpressos.Name = "navBarImpressos";
            // 
            // navBarItem8
            // 
            this.navBarItem8.Caption = "Carta com logo";
            this.navBarItem8.Name = "navBarItem8";
            this.navBarItem8.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem8_LinkClicked);
            // 
            // navBarItem9
            // 
            this.navBarItem9.Caption = "Gera RTF";
            this.navBarItem9.Name = "navBarItem9";
            this.navBarItem9.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem9_LinkClicked);
            // 
            // navBarItem5
            // 
            this.navBarItem5.Caption = "Inad Por unidade";
            this.navBarItem5.Name = "navBarItem5";
            this.navBarItem5.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem5_LinkClicked_1);
            // 
            // navBarGroup6
            // 
            this.navBarGroup6.Caption = "Extrato";
            this.navBarGroup6.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem11),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem10)});
            this.navBarGroup6.Name = "navBarGroup6";
            // 
            // navBarItem11
            // 
            this.navBarItem11.Caption = "ExtratoManual";
            this.navBarItem11.Name = "navBarItem11";
            // 
            // navBarItem10
            // 
            this.navBarItem10.Caption = "Extrato";
            this.navBarItem10.Name = "navBarItem10";
            this.navBarItem10.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem10_LinkClicked);
            // 
            // navBarGroup7
            // 
            this.navBarGroup7.Caption = "seguro conteudo";
            this.navBarGroup7.Expanded = true;
            this.navBarGroup7.Name = "navBarGroup7";
            // 
            // a
            // 
            this.a.Caption = "Modelos Balancete";
            this.a.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem4)});
            this.a.Name = "a";
            // 
            // navBarItem4
            // 
            this.navBarItem4.Caption = "Teste Boleto";
            this.navBarItem4.Name = "navBarItem4";
            this.navBarItem4.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem4_LinkClicked_1);
            // 
            // navBarGroup8
            // 
            this.navBarGroup8.Caption = "Assembleia";
            this.navBarGroup8.Name = "navBarGroup8";
            // 
            // navBarGroup9
            // 
            this.navBarGroup9.Caption = "Concilia��o";
            this.navBarGroup9.Name = "navBarGroup9";
            // 
            // navBarGroup10
            // 
            this.navBarGroup10.Caption = "Francesinha";
            this.navBarGroup10.Name = "navBarGroup10";
            // 
            // navBarGroup11
            // 
            this.navBarGroup11.Caption = "Cobran�a";
            this.navBarGroup11.Name = "navBarGroup11";
            // 
            // Pricipalteste
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(782, 556);
            this.Margin = new System.Windows.Forms.Padding(5);
            moduleInfo1.Grupo = 3;
            moduleInfo1.ImagemG = null;
            moduleInfo1.ImagemP = null;
            moduleInfo1.ModuleType = null;
            moduleInfo1.ModuleTypestr = "Cadastros.Condominios.cCondominiosGrade";
            moduleInfo1.Name = "CondominosGrade";
            moduleInfo2.Grupo = 1;
            moduleInfo2.ImagemG = null;
            moduleInfo2.ImagemP = null;
            moduleInfo2.ModuleType = null;
            moduleInfo2.ModuleTypestr = "Boletos.Boleto.BoletosGrade";
            moduleInfo2.Name = "Boletos 1";
            moduleInfo3.Grupo = 1;
            moduleInfo3.ImagemG = null;
            moduleInfo3.ImagemP = null;
            moduleInfo3.ModuleType = null;
            moduleInfo3.ModuleTypestr = "Boletos.Eventual.cEventual";
            moduleInfo3.Name = "Eventual";
            moduleInfo4.Grupo = 1;
            moduleInfo4.ImagemG = null;
            moduleInfo4.ImagemP = null;
            moduleInfo4.ModuleType = null;
            moduleInfo4.ModuleTypestr = "Boletos.Eventual.cGradeEventual";
            moduleInfo4.Name = "Grade Eventual";
            moduleInfo5.Grupo = 1;
            moduleInfo5.ImagemG = null;
            moduleInfo5.ImagemP = null;
            moduleInfo5.ModuleType = null;
            moduleInfo5.ModuleTypestr = "Boletos.geracao.cGrade";
            moduleInfo5.Name = "Gerenciador";
            moduleInfo6.Grupo = 1;
            moduleInfo6.ImagemG = null;
            moduleInfo6.ImagemP = null;
            moduleInfo6.ModuleType = null;
            moduleInfo6.ModuleTypestr = "Boletos.Rateios.cGradeRateios";
            moduleInfo6.Name = "Rateios";
            moduleInfo7.Grupo = 1;
            moduleInfo7.ImagemG = null;
            moduleInfo7.ImagemP = null;
            moduleInfo7.ModuleType = null;
            moduleInfo7.ModuleTypestr = "Boletos.Boleto.Gas.cGradeGas";
            moduleInfo7.Name = "gas/agua";
            moduleInfo8.Grupo = 0;
            moduleInfo8.ImagemG = null;
            moduleInfo8.ImagemP = null;
            moduleInfo8.ModuleType = null;
            moduleInfo8.ModuleTypestr = "Boletos.Protesto.cProtesto";
            moduleInfo8.Name = "Protesto";
            moduleInfo9.Grupo = 0;
            moduleInfo9.ImagemG = null;
            moduleInfo9.ImagemP = null;
            moduleInfo9.ModuleType = null;
            moduleInfo9.ModuleTypestr = "Cobranca.Juridico.cRelatorio";
            moduleInfo9.Name = "honorarios";
            moduleInfo10.Grupo = 0;
            moduleInfo10.ImagemG = null;
            moduleInfo10.ImagemP = null;
            moduleInfo10.ModuleType = null;
            moduleInfo10.ModuleTypestr = "Cadastros.Pessoas.PesquisaPessoas";
            moduleInfo10.Name = "Inq/prop";
            moduleInfo11.Grupo = 0;
            moduleInfo11.ImagemG = null;
            moduleInfo11.ImagemP = null;
            moduleInfo11.ModuleType = null;
            moduleInfo11.ModuleTypestr = "dllbanco.cConciliacao";
            moduleInfo11.Name = "Conciliar";
            moduleInfo12.Grupo = 0;
            moduleInfo12.ImagemG = null;
            moduleInfo12.ImagemP = null;
            moduleInfo12.ModuleType = null;
            moduleInfo12.ModuleTypestr = "Boletos.AgendaBoletos.cAgendaBaseBol";
            moduleInfo12.Name = "Datas";
            moduleInfo13.Grupo = 0;
            moduleInfo13.ImagemG = null;
            moduleInfo13.ImagemP = null;
            moduleInfo13.ModuleType = null;
            moduleInfo13.ModuleTypestr = "Calendario.Agenda.cAgenda";
            moduleInfo13.Name = "agnda base";
            moduleInfo14.Grupo = 12;
            moduleInfo14.ImagemG = null;
            moduleInfo14.ImagemP = null;
            moduleInfo14.ModuleType = null;
            moduleInfo14.ModuleTypestr = "dllbanco.cFrancesinha";
            moduleInfo14.Name = "Francesinha";
            moduleInfo15.Grupo = 2;
            moduleInfo15.ImagemG = null;
            moduleInfo15.ImagemP = null;
            moduleInfo15.ModuleType = null;
            moduleInfo15.ModuleTypestr = "Calendario.Agenda.cTiposAgendamento";
            moduleInfo15.Name = "Tipos";
            moduleInfo16.Grupo = 2;
            moduleInfo16.ImagemG = null;
            moduleInfo16.ImagemP = null;
            moduleInfo16.ModuleType = null;
            moduleInfo16.ModuleTypestr = "Calendario.Agenda.cAgendaGrade";
            moduleInfo16.Name = "Datas";
            moduleInfo17.Grupo = 2;
            moduleInfo17.ImagemG = null;
            moduleInfo17.ImagemP = null;
            moduleInfo17.ModuleType = null;
            moduleInfo17.ModuleTypestr = "Calendario.Agenda.cAgenda";
            moduleInfo17.Name = "Calendario";
            moduleInfo18.Grupo = 3;
            moduleInfo18.ImagemG = null;
            moduleInfo18.ImagemP = null;
            moduleInfo18.ModuleType = null;
            moduleInfo18.ModuleTypestr = "Cadastros.Sindicos.cSindico";
            moduleInfo18.Name = "sindicos";
            moduleInfo19.Grupo = 2;
            moduleInfo19.ImagemG = null;
            moduleInfo19.ImagemP = null;
            moduleInfo19.ModuleType = null;
            moduleInfo19.ModuleTypestr = "Calendario.StatusGeral.cStatusGeral";
            moduleInfo19.Name = "Status";
            moduleInfo20.Grupo = 5;
            moduleInfo20.ImagemG = null;
            moduleInfo20.ImagemP = null;
            moduleInfo20.ModuleType = null;
            moduleInfo20.ModuleTypestr = "dllbanco.cArquivoRetorno";
            moduleInfo20.Name = "Retorno";
            moduleInfo21.Grupo = 0;
            moduleInfo21.ImagemG = null;
            moduleInfo21.ImagemP = null;
            moduleInfo21.ModuleType = null;
            moduleInfo21.ModuleTypestr = "Cadastros.PlanoDeContas.cPlanoDeContas";
            moduleInfo21.Name = "PLA";
            moduleInfo22.Grupo = 0;
            moduleInfo22.ImagemG = null;
            moduleInfo22.ImagemP = null;
            moduleInfo22.ModuleType = null;
            moduleInfo22.ModuleTypestr = "Cadastros.PlanoDeContas.cSubPlano";
            moduleInfo22.Name = "SubPLA";
            moduleInfo23.Grupo = 0;
            moduleInfo23.ImagemG = null;
            moduleInfo23.ImagemP = null;
            moduleInfo23.ModuleType = null;
            moduleInfo23.ModuleTypestr = "Cadastros.Fornecedores.Servicos.cFornecedoresServicos";
            moduleInfo23.Name = "Fornecedores";
            moduleInfo24.Grupo = 0;
            moduleInfo24.ImagemG = null;
            moduleInfo24.ImagemP = null;
            moduleInfo24.ModuleType = null;
            moduleInfo24.ModuleTypestr = "Cadastros.Fornecedores.Imobiliarias.cImobiliariasGrade";
            moduleInfo24.Name = "Imobiliarias";
            moduleInfo25.Grupo = 6;
            moduleInfo25.ImagemG = null;
            moduleInfo25.ImagemP = null;
            moduleInfo25.ModuleType = null;
            moduleInfo25.ModuleTypestr = "maladireta.EditorRTF.cGradeImpressos";
            moduleInfo25.Name = "Edita RTF";
            moduleInfo26.Grupo = 2;
            moduleInfo26.ImagemG = global::TesteBoletos.Properties.Resources.coruja;
            moduleInfo26.ImagemP = null;
            moduleInfo26.ModuleType = null;
            moduleInfo26.ModuleTypestr = "dllCheques.cChequesEmitir";
            moduleInfo26.Name = "Emitir Cheques";
            moduleInfo27.Grupo = 7;
            moduleInfo27.ImagemG = global::TesteBoletos.Properties.Resources.coruja;
            moduleInfo27.ImagemP = global::TesteBoletos.Properties.Resources.coruja;
            moduleInfo27.ModuleType = null;
            moduleInfo27.ModuleTypestr = "dllCaixa.cSaldosGeral";
            moduleInfo27.Name = "Saldos";
            moduleInfo28.Grupo = 0;
            moduleInfo28.ImagemG = global::TesteBoletos.Properties.Resources.coruja;
            moduleInfo28.ImagemP = null;
            moduleInfo28.ModuleType = null;
            moduleInfo28.ModuleTypestr = "Balancete.cGradeBalancetes";
            moduleInfo28.Name = "Balancete";
            moduleInfo29.Grupo = 0;
            moduleInfo29.ImagemG = null;
            moduleInfo29.ImagemP = null;
            moduleInfo29.ModuleType = null;
            moduleInfo29.ModuleTypestr = "Cadastros.Feriados.cFeriados";
            moduleInfo29.Name = "Feriados";
            moduleInfo30.Grupo = 6;
            moduleInfo30.ImagemG = null;
            moduleInfo30.ImagemP = null;
            moduleInfo30.ModuleType = null;
            moduleInfo30.ModuleTypestr = "dllCaixa.cLancamentosFuturos";
            moduleInfo30.Name = "Previs�o";
            moduleInfo31.Grupo = 3;
            moduleInfo31.ImagemG = null;
            moduleInfo31.ImagemP = null;
            moduleInfo31.ModuleType = null;
            moduleInfo31.ModuleTypestr = "Cadastros.AgendaGeral.cAGendaGeral";
            moduleInfo31.Name = "Agenda Geral";
            moduleInfo32.Grupo = 0;
            moduleInfo32.ImagemG = null;
            moduleInfo32.ImagemP = null;
            moduleInfo32.ModuleType = null;
            moduleInfo32.ModuleTypestr = "Cadastros.Responsaveis.cResponsaveis";
            moduleInfo32.Name = "Responsaveis";
            moduleInfo33.Grupo = 0;
            moduleInfo33.ImagemG = null;
            moduleInfo33.ImagemP = null;
            moduleInfo33.ModuleType = null;
            moduleInfo33.ModuleTypestr = "Cadastros.Responsaveis.cAvisosTipos";
            moduleInfo33.Name = "Tipos de aviso";
            moduleInfo34.Grupo = 0;
            moduleInfo34.ImagemG = null;
            moduleInfo34.ImagemP = null;
            moduleInfo34.ModuleType = null;
            moduleInfo34.ModuleTypestr = "TesteBoletos.cImportaData";
            moduleInfo34.Name = "Importa Data";
            moduleInfo35.Grupo = 10;
            moduleInfo35.ImagemG = null;
            moduleInfo35.ImagemP = null;
            moduleInfo35.ModuleType = null;
            moduleInfo35.ModuleTypestr = "dllAssembleia.cVotacoes";
            moduleInfo35.Name = "Votacao";
            moduleInfo36.Grupo = 0;
            moduleInfo36.ImagemG = null;
            moduleInfo36.ImagemP = null;
            moduleInfo36.ModuleType = null;
            moduleInfo36.ModuleTypestr = "Framework.Alarmes.cMeusAlarmes";
            moduleInfo36.Name = "meus alarmes";
            moduleInfo37.Grupo = 6;
            moduleInfo37.ImagemG = null;
            moduleInfo37.ImagemP = null;
            moduleInfo37.ModuleType = null;
            moduleInfo37.ModuleTypestr = "maladireta.cMalaDireta";
            moduleInfo37.Name = "Mala direta";
            moduleInfo38.Grupo = 8;
            moduleInfo38.ImagemG = null;
            moduleInfo38.ImagemP = null;
            moduleInfo38.ModuleType = null;
            moduleInfo38.ModuleTypestr = "Cadastros.seguroconteudo.cPlanosSeguroConteudo";
            moduleInfo38.Name = "Planos Seguro";
            moduleInfo39.Grupo = 8;
            moduleInfo39.ImagemG = null;
            moduleInfo39.ImagemP = null;
            moduleInfo39.ModuleType = null;
            moduleInfo39.ModuleTypestr = "Boletos.SeguroConteudo.cSeguros";
            moduleInfo39.Name = "Gera Arquivo Seguro";
            moduleInfo40.Grupo = 0;
            moduleInfo40.ImagemG = null;
            moduleInfo40.ImagemP = null;
            moduleInfo40.ModuleType = null;
            moduleInfo40.ModuleTypestr = "Cadastros.RamoAtividade.cRamoAtividadeGrade";
            moduleInfo40.Name = "Ramo de atividades";
            moduleInfo41.Grupo = 9;
            moduleInfo41.ImagemG = null;
            moduleInfo41.ImagemP = null;
            moduleInfo41.ModuleType = null;
            moduleInfo41.ModuleTypestr = "Cadastros.Balancetes.cModelos";
            moduleInfo41.Name = "Modelos";
            moduleInfo42.Grupo = 0;
            moduleInfo42.ImagemG = null;
            moduleInfo42.ImagemP = null;
            moduleInfo42.ModuleType = null;
            moduleInfo42.ModuleTypestr = "Cadastros.Bancos.cBancosGrade";
            moduleInfo42.Name = "Bancos";
            moduleInfo43.Grupo = 11;
            moduleInfo43.ImagemG = null;
            moduleInfo43.ImagemP = null;
            moduleInfo43.ModuleType = null;
            moduleInfo43.ModuleTypestr = "Cadastros.cConfiguraEstacao";
            moduleInfo43.Name = "Configura Esta��o";
            moduleInfo44.Grupo = 7;
            moduleInfo44.ImagemG = null;
            moduleInfo44.ImagemP = null;
            moduleInfo44.ModuleType = null;
            moduleInfo44.ModuleTypestr = "TesteBoletos.cTestaTMP_Para_SCC";
            moduleInfo44.Name = "TMP -> SCC";
            moduleInfo45.Grupo = 7;
            moduleInfo45.ImagemG = null;
            moduleInfo45.ImagemP = null;
            moduleInfo45.ModuleType = null;
            moduleInfo45.ModuleTypestr = "dllCaixa.cLoteExtratos";
            moduleInfo45.Name = "Lote de Extratos";
            moduleInfo46.Grupo = 11;
            moduleInfo46.ImagemG = null;
            moduleInfo46.ImagemP = null;
            moduleInfo46.ModuleType = null;
            moduleInfo46.ModuleTypestr = "TesteBoletos.cTesteConciliacao";
            moduleInfo46.Name = "Teste Concilia��o";
            moduleInfo47.Grupo = 0;
            moduleInfo47.ImagemG = null;
            moduleInfo47.ImagemP = null;
            moduleInfo47.ModuleType = null;
            moduleInfo47.ModuleTypestr = "TesteBoletos.cTestaTribElet";
            moduleInfo47.Name = "Tributo Eletr�nico";
            moduleInfo48.Grupo = 12;
            moduleInfo48.ImagemG = null;
            moduleInfo48.ImagemP = null;
            moduleInfo48.ModuleType = null;
            moduleInfo48.ModuleTypestr = "dllbanco.cFrancesinhaCorrecao";
            moduleInfo48.Name = "Corre��o Francesinha";
            moduleInfo49.Grupo = 13;
            moduleInfo49.ImagemG = null;
            moduleInfo49.ImagemP = null;
            moduleInfo49.ModuleType = null;
            moduleInfo49.ModuleTypestr = "Cobranca.cCobrancaV";
            moduleInfo49.Name = "Cobran�a";
            this.ModuleInfoCollection.AddRange(new CompontesBasicos.ModuleInfo[] {
            moduleInfo1,
            moduleInfo2,
            moduleInfo3,
            moduleInfo4,
            moduleInfo5,
            moduleInfo6,
            moduleInfo7,
            moduleInfo8,
            moduleInfo9,
            moduleInfo10,
            moduleInfo11,
            moduleInfo12,
            moduleInfo13,
            moduleInfo14,
            moduleInfo15,
            moduleInfo16,
            moduleInfo17,
            moduleInfo18,
            moduleInfo19,
            moduleInfo20,
            moduleInfo21,
            moduleInfo22,
            moduleInfo23,
            moduleInfo24,
            moduleInfo25,
            moduleInfo26,
            moduleInfo27,
            moduleInfo28,
            moduleInfo29,
            moduleInfo30,
            moduleInfo31,
            moduleInfo32,
            moduleInfo33,
            moduleInfo34,
            moduleInfo35,
            moduleInfo36,
            moduleInfo37,
            moduleInfo38,
            moduleInfo39,
            moduleInfo40,
            moduleInfo41,
            moduleInfo42,
            moduleInfo43,
            moduleInfo44,
            moduleInfo45,
            moduleInfo46,
            moduleInfo47,
            moduleInfo48,
            moduleInfo49});
            this.Name = "Pricipalteste";
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.DockPanel_FContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            this.DockPanel_F.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem3;
        private DevExpress.XtraNavBar.NavBarItem navBarItem6;
        private DevExpress.XtraNavBar.NavBarGroup TesteDatas;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup3;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup4;
        private DevExpress.XtraNavBar.NavBarItem navBarItem7;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup5;
        private DevExpress.XtraNavBar.NavBarGroup navBarImpressos;
        private DevExpress.XtraNavBar.NavBarItem navBarItem8;
        private DevExpress.XtraNavBar.NavBarItem navBarItem9;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup6;
        private DevExpress.XtraNavBar.NavBarItem navBarItem11;
        private DevExpress.XtraNavBar.NavBarItem navBarItem10;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup7;
        private DevExpress.XtraNavBar.NavBarGroup a;
        private DevExpress.XtraNavBar.NavBarItem navBarItem4;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup8;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup9;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup10;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup11;
        private DevExpress.XtraNavBar.NavBarItem navBarItem5;
        private DevExpress.XtraNavBar.NavBarItem navBarItem12;
        private DevExpress.XtraNavBar.NavBarItem navBarItem13;
    }
}
