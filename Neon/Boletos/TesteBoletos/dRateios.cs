﻿namespace Boletos.Rateios {


    partial class dRateios
    {
        public dRateios.ListaFracaoRow ReferenciaMaxima;// A princicio a referencia máxima é igual à LinhaMaior mas poderemos ter uma diferênça em função da não cosideração das linhas com fator (como ocorre na referenciaminima)
        dRateios.ListaFracaoRow LinhaMaior;  //Esta é a linha de maior peso devendo recer o ajuste fino
        public dRateios.ListaFracaoRow ReferenciaMinima;
        public void CalculaFracaoCorrigida(bool usarFracao) {
            decimal Total = 0;
            
            decimal Maior = 0;
            decimal Menor = 100000000;
            if(!usarFracao)
                foreach (dRateios.ListaFracaoRow ListaFracaoRow in this.ListaFracao) {
                    ListaFracaoRow.APTFracaoIdeal = 1;
                };
            foreach (dRateios.ListaFracaoRow ListaFracaoRow in this.ListaFracao) {
                
                decimal Corrigido = ListaFracaoRow.APTFracaoIdeal * ListaFracaoRow.FatorFracao;
                Total += Corrigido;
                if (Corrigido > Maior) {
                    Maior = Corrigido;
                    LinhaMaior = ListaFracaoRow;
                    ReferenciaMaxima = ListaFracaoRow;
                };
                if ((Corrigido < Menor) && (ListaFracaoRow.FatorFracao == 1) && (Corrigido > 0))
                {
                    Menor = Corrigido;
                    ReferenciaMinima = ListaFracaoRow;                    
                };
            };
            if (Total == 0) {
                Total = 0;
                foreach (dRateios.ListaFracaoRow ListaFracaoRow in this.ListaFracao)
                {
                    ListaFracaoRow.APTFracaoIdeal = 1;
                    decimal Corrigido = ListaFracaoRow.APTFracaoIdeal * ListaFracaoRow.FatorFracao;
                    Total += Corrigido;
                    if (Corrigido > Maior)
                    {
                        Maior = Corrigido;
                        LinhaMaior = ListaFracaoRow;
                        ReferenciaMaxima = ListaFracaoRow;
                    };
                    if ((Corrigido < Menor) && (ListaFracaoRow.FatorFracao == 1) && (Corrigido > 0))
                    {
                        Menor = Corrigido;
                        ReferenciaMinima = ListaFracaoRow;
                    };
                };
            }
            
            if (Total != 1) //ATENCAO o Total passa a ser o fator de correçao para levar a soma das Fracoes para 100%
                {
                    decimal NovoTotalCorrigido = 0;
                    foreach (dRateios.ListaFracaoRow ListaFracaoRow in this.ListaFracao)
                    {
                        decimal Corrigido = ListaFracaoRow.APTFracaoIdeal * ListaFracaoRow.FatorFracao;
                        ListaFracaoRow.FracaoCorrigida = Corrigido / Total;
                        NovoTotalCorrigido += ListaFracaoRow.FracaoCorrigida;
                    };
                    if (LinhaMaior != null)
                        LinhaMaior.FracaoCorrigida += (1-NovoTotalCorrigido);
                };
            
        }
    }
}
