﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace TesteBoletos
{
    public partial class cImportaData : CompontesBasicos.ComponenteBase
    {
        public cImportaData()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dllbanco.Extrato.dExtrato dExtrato1 = new dllbanco.Extrato.dExtrato();
            //dllbanco.Extrato.dExtrato.SaldoContaCorrenteRow row = dExtrato1.DataAnterior_SCC(984, 237, dateEdit1.DateTime);
            //memoEdit1.Text += string.Format("SCC {0} Data {1}\r\n",row.SCC,row.SCCData);

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog Dialogo = new OpenFileDialog();
            Dialogo.FileName = "c:\\lixo\\retornoxml";
            if (Dialogo.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    dImporta dsImp = new dImporta();
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("",dsImp.ContaCorrenteDetalheTableAdapter, dsImp.SaldoContaCorrenteTableAdapter);
                    DataSet dsImportado = new DataSet();
                    dsImportado.ReadXml(Dialogo.FileName);
                    DataTable tabela = dsImportado.Tables[0];
                    int CCT = int.Parse((string)tabela.Rows[0]["SCC_CCT"]);
                    DateTime DATA = DateTime.Parse((string)tabela.Rows[0]["SCCData"]);
                    dsImp.ContaCorrenteDetalheTableAdapter.DeleteQuery(DATA, CCT);
                    dsImp.SaldoContaCorrenteTableAdapter.DeleteQuery(DATA, CCT);

                    dImporta.SaldoContaCorrenteRow novaSCC = dsImp.SaldoContaCorrente.NewSaldoContaCorrenteRow();
                    novaSCC.SCC_ARQ = 1;
                    novaSCC.SCC_CCT = CCT; 
                    novaSCC.SCCAberto = bool.Parse((string)tabela.Rows[0]["SCCAberto"]);
                    novaSCC.SCCData = DATA; 
                    novaSCC.SCCDataA = DateTime.Parse((string)tabela.Rows[0]["SCCDataA"]);
                    novaSCC.SCCValorF = decimal.Parse((string)tabela.Rows[0]["SCCValorF"])/10000;
                    novaSCC.SCCValorI = decimal.Parse((string)tabela.Rows[0]["SCCValorI"])/10000;
                    dsImp.SaldoContaCorrente.AddSaldoContaCorrenteRow(novaSCC);
                    dsImp.SaldoContaCorrenteTableAdapter.Update(novaSCC);
                    novaSCC.AcceptChanges();
                    foreach (DataRow DR in tabela.Rows)
                    {
                        dImporta.ContaCorrenteDetalheRow CCDrow = dsImp.ContaCorrenteDetalhe.NewContaCorrenteDetalheRow();
                        CCDrow.CCD_SCC = novaSCC.SCC;
                        CCDrow.CCDCategoria = int.Parse((string)DR["CCDCategoria"]);
                        CCDrow.CCDCodHistorico = int.Parse((string)DR["CCDCodHistorico"]);
                        CCDrow.CCDConsolidado = int.Parse((string)DR["CCDConsolidado"]);
                        CCDrow.CCDDescComplemento = (string)DR["CCDDescComplemento"];
                        CCDrow.CCDDescricao = (string)DR["CCDDescricao"];
                        CCDrow.CCDDocumento = int.Parse((string)DR["CCDDocumento"]);
                        CCDrow.CCDTipoLancamento = int.Parse((string)DR["CCDTipoLancamento"]);
                        CCDrow.CCDValor = decimal.Parse((string)DR["CCDValor"])/10000;
                        CCDrow.CCDValidado = true;
                        dsImp.ContaCorrenteDetalhe.AddContaCorrenteDetalheRow(CCDrow);
                        dsImp.ContaCorrenteDetalheTableAdapter.Update(CCDrow);
                        CCDrow.AcceptChanges();
                    }
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                }
                catch (Exception ex) 
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                }
            }
        }
    }
}
