﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dllbanco;


namespace TesteBoletos
{
    public partial class cTesteConciliacao : CompontesBasicos.ComponenteBase
    {
        public cTesteConciliacao()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int total = Conciliacao.ConciliacaoSt.CarregaPendentes();
            Conciliacao.ConciliacaoSt.Consolidar((int)spinEdit1.Value, true);
            memoEdit1.Text = string.Format("Pendentes: {5}\r\n Baixados: {0:n0}\r\n Antigos: {1:n0}\r\n Inativos {2:n0}\r\n Aguardo: {3:0}\r\n Cheques: {4}\r\n Descrição: {6}\r\n",
                                                    Conciliacao.ConciliacaoSt.baixados,
                                                    Conciliacao.ConciliacaoSt.antigos,
                                                    Conciliacao.ConciliacaoSt.inativo,
                                                    Conciliacao.ConciliacaoSt.aguardo,
                                                    Conciliacao.ConciliacaoSt.chequesNao,
                                                    total,
                                                    Conciliacao.ConciliacaoSt.descricaonaoencontrada) + Conciliacao.ConciliacaoSt.Retorno;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            PagamentoEletronico PagamentoEletronico1 = new PagamentoEletronico();
            memoEdit1.Text = string.Format("Processamento APL:\r\n{0}",
                                            PagamentoEletronico1.ProcessarDados());
        }
    }
}
