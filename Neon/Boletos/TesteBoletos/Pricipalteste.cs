using System;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using dllCNAB;

namespace TesteBoletos
{
    public partial class Pricipalteste : CompontesBasicos.FormPrincipalBase
    {
        public Pricipalteste()
        {
            InitializeComponent();
        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            
            //int CON = 298;
            int CON = 233;

            DateTime DI = new DateTime(2012,12, 1);
            DateTime DF = new DateTime(2012,12, 31);
            //VirInput.Input.Execute("DI", ref DI);
            //VirInput.Input.Execute("DF", ref DF);
            
            Boletos.Boleto.Impresso.ImpBoletosColunas Impresso = new Boletos.Boleto.Impresso.ImpBoletosColunas(CON, DI, DF, Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Recebimento, true);
            
            Impresso.CreateDocument();
            Impresso.ShowPreviewDialog();
            /*
            Impresso = new Boletos.Boleto.Impresso.ImpBoletosColunas(CON, DI, DF, Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Emissao, true);
            Impresso.CreateDocument();
            Impresso.ShowPreviewDialog();

            Impresso = new Boletos.Boleto.Impresso.ImpBoletosColunas(CON, DI, DF, Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.DetalhamentoCredito, true);

            Boletos.Boleto.Impresso.dBoletosColunas.BoletosRow novarow = Impresso.dBoletosColunas1.Boletos.NewBoletosRow();
            novarow.Agrupamento = "agrupamento";
            novarow.APTNumero = "NuAP";
            novarow.BLOCodigo = "TE";
            novarow.BOL = 100;
            novarow.BOL_APT = 1;
            novarow.BOLProprietario = true;
            novarow.BOLPagamento = DateTime.Today;
            novarow.BOLValorPago = 110;
            novarow.BOLValorPrevisto = 100;
            novarow.BOLVencto = DateTime.Today;           
            Impresso.dBoletosColunas1.Boletos.AddBoletosRow(novarow);

            Boletos.Boleto.Impresso.dBoletosColunas.BoletosColunasRow novarowDet = Impresso.dBoletosColunas1.BoletosColunas.NewBoletosColunasRow();
            novarowDet.BOD_PLA = "101001";
            novarowDet.BODMensagem = "men1";
            novarowDet.BODValor = 100;
            novarowDet.BOL = 100;
            //novarowDet.
            Impresso.dBoletosColunas1.BoletosColunas.AddBoletosColunasRow(novarowDet);

            novarowDet = Impresso.dBoletosColunas1.BoletosColunas.NewBoletosColunasRow();
            novarowDet.BOD_PLA = "104000";
            novarowDet.BODMensagem = "men2";
            novarowDet.BODValor = 100;
            novarowDet.BOL = 100;
            //novarowDet.
            Impresso.dBoletosColunas1.BoletosColunas.AddBoletosColunasRow(novarowDet);

            //Impresso.dBoletosColunas1.Boletos.AddBoletosRow("A", "1234", true, 100, 110, DateTime.Today, DateTime.Today, "grupo 1", 0, "P", "BLOAPT", 0);
            Impresso.CreateDocument();
            Impresso.ShowPreviewDialog();    
            */
            
            for (int i = 6; i < 25; i++)
            //for (int i = 7; i < 8; i++)
            {
                Impresso = new Boletos.Boleto.Impresso.ImpBoletosColunas(CON, DI, DF, Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Recebimento,false, true,i);
                
                Impresso.testacolunas = i;
                Impresso.CreateDocument();
                Impresso.ShowPreviewDialog();    
            }
            
            return;
            
            
        }

        private void navBarItem2_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Boletos.Eventual.cEventual Novo = new Boletos.Eventual.cEventual();
            Novo.Fill();
            //Novo.lookupBlocosAptos_F1.CON_sel = 548;
            //Novo.lookupBlocosAptos_F1.BLO_Sel = 1025;
            //Novo.lookupBlocosAptos_F1.APT_Sel = 31875;
            //Novo.lookupBlocosAptos_F1.Enabled = false;
            //Novo.dEventual.ItensBODnovos.AddItensBODnovosRow("mensagen", "102000", true, 200);
            //Novo.dEventual.ItensBODnovos.AddItensBODnovosRow("mensagen", "103000", true, 201);
            //Novo.cCompetData1.cCompet1.Comp = new Framework.objetosNeon.Competencia(9, 2008);

            if (Boletos.Eventual.cEventual.ShowModuloSTA(CompontesBasicos.EstadosDosComponentes.PopUp, Novo) == DialogResult.OK)
                MessageBox.Show("ok");
        }

        private void navBarItem3_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {

            dllbanco.Conciliacao.ConciliacaoSt.VerificarDebitoAutomaticoPendente();
        }

        private void navBarGroup1_ItemChanged(object sender, EventArgs e)
        {
            
        }

        private void navBarItem4_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Boletos.Boleto.cDividir novo = new Boletos.Boleto.cDividir();
            novo.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);            
        }

        private void navBarItem5_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            /*
            int BOL;
            if(VirInput.Input.Execute("Boleto",out BOL))
            {
                Boletos.Boleto.Boleto B = new Boletos.Boleto.Boleto(BOL);
                decimal V1 = B.rowPrincipal.BOLValorPrevisto;
                VirInput.Input.Execute("Valor", ref V1);
                decimal V2 = V1 - B.rowPrincipal.BOLValorPrevisto;
                VirInput.Input.Execute("Valor", ref V2);
                if (B.Baixa(DateTime.Today, V1, V2, -1, -1))
                    MessageBox.Show("ok");
                else
                    MessageBox.Show(B.UltimoErro);
            }*/
        }

        private void navBarItem6_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            string relatorio = dllbanco.Conciliacao.ConciliacaoSt.AmarraBoletosCaixa();
            Clipboard.SetText(relatorio);
        }

        private void navBarItem7_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Boletos.Rateios.FracaoManual FM = new Boletos.Rateios.FracaoManual();            
            FM.DRateio = new Boletos.Rateios.dRateios();
            Boletos.Rateios.dRateios.ListaFracaoDataTable TabLista = Boletos.Rateios.dRateios.ListaFracaoTableAdapter.GetData(233);
            FM.Tabela = TabLista;
            FM.listaFracaoDataTableBindingSource.DataSource = TabLista;
            FM.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }

        private void navBarItem8_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            dllImpresso.ImpRTFLogoSimples Impresso = new dllImpresso.ImpRTFLogoSimples("Titulo do imp", 233, 77);                        
            System.Collections.SortedList SL = new System.Collections.SortedList();
            SL.Add("A", "-Eu sou o A-");
            SL.Add("B", "-Eu sou o B-");
            Impresso.CarregarImpresso(1, SL);
            Impresso.CreateDocument();
            Impresso.ShowPreviewDialog();
            System.Data.DataTable DT = new System.Data.DataTable();
            DT.Columns.Add("A");
            DT.Columns.Add("B");
            DT.Rows.Add("A1", "B1");
            DT.Rows.Add("A2", "B2");
            DT.Rows.Add("A3", "B3");
            Impresso.CarregarImpresso(1, DT.Rows[0]);
            //Impresso.CarregarImpresso(1, DT);
            Impresso.CreateDocument();
            Impresso.ShowPreviewDialog();

        }

        private void Retorno(CompontesBasicos.ComponenteBase Sender, DialogResult Modo, params object[] dados)
        {
            if (Modo == System.Windows.Forms.DialogResult.OK)
                MessageBox.Show(dados[0].ToString());
        }

        private void navBarItem9_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            maladireta.EditorRTF.cGeraRTF cgerador = new maladireta.EditorRTF.cGeraRTF(Retorno,"teste");
   
            cgerador.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);

            //if (cgerador.RTFRetonro != "")
              //  MessageBox.Show(cgerador.RTFRetonro);

            //cgerador = new maladireta.EditorRTF.cGeraRTF(Retorno, "teste");
            cgerador = new maladireta.EditorRTF.cGeraRTF(null, "teste");
            cgerador.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);

            //if (cgerador.RTFRetonro != "")
              //  MessageBox.Show(cgerador.RTFRetonro);
        }

        private void navBarItem10_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            dllbanco.Extrato.cExtrato cExtrato1 = new dllbanco.Extrato.cExtrato(new DateTime(2012, 06, 1), new DateTime(2012, 08, 1), 1257, false);
            cExtrato1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
        }

        

        private void navBarItem12_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            MessageBox.Show("Teste");
            DateTime DataI=new DateTime(2013,1,1);
            DateTime DataF = new DateTime(2013, 1, 31);
            System.Data.DataSet DS = new System.Data.DataSet("DS");
            System.Data.DataTable DT = DS.Tables.Add("DT");
            DT.Columns.Add("PROPRIETARIOXXX", typeof(string));
            DT.Rows.Add("P1");
            DT.Rows.Add("P2");
            DT.Rows.Add("P3");
            DT.Rows.Add("P4");
            ArquivosTXT.cTabelaTXT geraArquivo = new ArquivosTXT.cTabelaTXT(DT, ArquivosTXT.ModelosTXT.delimitado, ArquivosTXT.TipoMapeamento.manual);
            geraArquivo.Separador = new char[] { ';' };
            geraArquivo.GravaSeparadorFinal = false;
            geraArquivo.DefaultTamanhoVariavel = true;
            geraArquivo.GravarCamposNaPrimeiraLinha = true;
            geraArquivo.Formatodata = ArquivosTXT.FormatoData.dd_MM_YYYY;

            geraArquivo.EquivalenciaDeNomes.Add("PROPRIETARIOXXX", "mod");

            geraArquivo.RegistraMapa("CODIGO IDENTIFICADOR", 1, 0);
            geraArquivo.RegistraMapa("PROPRIETARIOXXX", 2, 0);
            geraArquivo.RegistraMapa("LOCATARIO", 3, 0);
            geraArquivo.RegistraMapa("PRODUTO", 4, 0,ArquivosTXT.TipoMapa.detalhe,"R");
            geraArquivo.RegistraMapa("TIPO LOGRADOURO", 5, 0, ArquivosTXT.TipoMapa.detalhe, "Rua");
            geraArquivo.RegistraMapa("ENDERECO RISCO", 6, 0);
            geraArquivo.RegistraMapa("NUMERO ENDERECO", 7, 0);
            geraArquivo.RegistraMapa("ECONOMIA", 8, 0, ArquivosTXT.TipoMapa.detalhe, 0);
            geraArquivo.RegistraMapa("APT", 9, 0);
            geraArquivo.RegistraMapa("CEP", 10, 0);
            geraArquivo.RegistraMapa("CIDADE", 11, 0);
            geraArquivo.RegistraMapa("UF", 12, 0, ArquivosTXT.TipoMapa.detalhe, "SP");
            geraArquivo.RegistraMapa("VIGENCIA INICIAL", 13, 0, ArquivosTXT.TipoMapa.detalhe, DataI);
            geraArquivo.RegistraMapa("VIGENCIA FINAL", 14, 0, ArquivosTXT.TipoMapa.detalhe, DataF);
            geraArquivo.RegistraMapa("E-MAIL CORRETORA", 15, 0, ArquivosTXT.TipoMapa.detalhe, "landia@sicura.com.br");
            geraArquivo.RegistraMapa("IMPORTANCIA SEGURADA", 16,0);
            //geraArquivo.RegistraMapa("", 5, 0, ArquivosTXT.TipoMapa.detalhe, "");
            geraArquivo.RegistraMapa("IMOBILIÁRIA", 18, 0, ArquivosTXT.TipoMapa.detalhe, "Neon Imóveis");
            

            //geraArquivo.IniciaGravacao(@"c:\lixo\testeseguro.txt");
            geraArquivo.Salva(@"c:\lixo\testeseguro.txt");
            MessageBox.Show("Teste terminado");
        }

        private void navBarItem4_LinkClicked_1(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Balancete.GrBalancete.balancete balancete1 = new Balancete.GrBalancete.balancete(15834);

            Framework.objetosNeon.AgrupadorBalancete.solicitaAgrupamento AgrupamentoUsar = new Framework.objetosNeon.AgrupadorBalancete.solicitaAgrupamento(Framework.objetosNeon.AgrupadorBalancete.TipoClassificacaoDesp.Grupos);
            balancete1.CarregarDados(true);
            balancete1.CarregarDadosBalancete(AgrupamentoUsar, false);
            balancete1.simulaBoleto();

            AgrupamentoUsar = new Framework.objetosNeon.AgrupadorBalancete.solicitaAgrupamento(Framework.objetosNeon.AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA,1);
            balancete1.CarregarDadosBalancete(AgrupamentoUsar, false);
            balancete1.TotDebitosGrupo.Add("Teste", new System.Collections.Generic.Dictionary<string, AbstratosNeon.ABS_balancete.TotDebito>());

            balancete1.TotDebitosGrupo["Teste"].Add("A", new AbstratosNeon.ABS_balancete.TotDebito());            
            balancete1.TotDebitosGrupo["Teste"]["A"].Total = 100;
            balancete1.TotDebitosGrupo["Teste"].Add("B1234567890", new AbstratosNeon.ABS_balancete.TotDebito());
            balancete1.TotDebitosGrupo["Teste"]["B1234567890"].Total = 100;

            balancete1.TotDebitosGrupo["Teste"].Add("C", new AbstratosNeon.ABS_balancete.TotDebito());
            balancete1.TotDebitosGrupo["Teste"]["C"].Total = 100;
            balancete1.TotDebitosGrupo["Teste"]["C"].Desc_Valor.Add("C1", 1);
            balancete1.TotDebitosGrupo["Teste"]["C"].Desc_Valor.Add("C2", 2);
            //balancete1.TotDebitosGrupo["Teste"]["C"].Desc_Valor.Add("C3", 3);

            balancete1.TotDebitosGrupo.Add("Teste D", new System.Collections.Generic.Dictionary<string, AbstratosNeon.ABS_balancete.TotDebito>());

            balancete1.TotDebitosGrupo["Teste D"].Add("D", new AbstratosNeon.ABS_balancete.TotDebito());
            balancete1.TotDebitosGrupo["Teste D"]["D"].Desc_Valor.Add("D1", 1);
            balancete1.TotDebitosGrupo["Teste D"]["D"].Desc_Valor.Add("D2", 2);
            //balancete1.TotDebitosGrupo["Teste D"]["D"].Desc_Valor.Add("D3", 3);
            balancete1.TotDebitosGrupo["Teste D"]["D"].Total = 100;

            balancete1.simulaBoleto();
            for (int i = 1; i <= 10; i++)
            {
                balancete1.TotDebitosGrupo["Teste"]["A"].Desc_Valor.Add(i.ToString(), i);
                balancete1.simulaBoleto();
            }
            for (int i = 1; i <= 15; i++)            
            {
                balancete1.TotDebitosGrupo["Teste"]["B1234567890"].Desc_Valor.Add(i.ToString(), i);
                balancete1.simulaBoleto();
            }
            balancete1.simulaBoleto();
        }

        private void navBarItem5_LinkClicked_1(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Boletos.Boleto.Impresso.ImpInadUnidades Impresso = new Boletos.Boleto.Impresso.ImpInadUnidades(AbstratosNeon.ABS_Condominio.GetCondominio(845), new DateTime(2017, 10, 1));
            Impresso.ShowPreviewDialog();
        }

        private void navBarItem12_LinkClicked_1(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            dllbanco.TributoEletronico Trib = new dllbanco.TributoEletronico();
        }

        private void navBarItem13_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            string retornossemdoc =
"SELECT dbo.ContaCorrenteDetalhe.CCD, dbo.ARQuivos.ARQNomeArquivo, dbo.ContaCorrenteDetalhe.CCDValor  \r\n" +
"FROM            dbo.ContaCorrenteDetalhe INNER JOIN\r\n" +
"                         dbo.SaldoContaCorrente ON dbo.ContaCorrenteDetalhe.CCD_SCC = dbo.SaldoContaCorrente.SCC INNER JOIN\r\n" +
"                         dbo.ContaCorrenTe ON dbo.SaldoContaCorrente.SCC_CCT = dbo.ContaCorrenTe.CCT INNER JOIN\r\n" +
"                         dbo.ARQuivos ON dbo.ContaCorrenteDetalhe.CCD_ARQ = dbo.ARQuivos.ARQ \r\n" +
"WHERE        (dbo.ContaCorrenTe.CCT_BCO = 104) AND (dbo.ContaCorrenteDetalhe.CCDDescricao = 'CHEQ COMP') AND (dbo.ContaCorrenteDetalhe.CCDDocumento = 0)\r\n";
            string Correcao = "update ContaCorrenteDetalhe set CCDDocumento = @P1 where CCD = @P2 and CCDDocumento = 0";
            System.Data.DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(retornossemdoc);
            foreach (System.Data.DataRow DR in DT.Rows)
            {
                int CCD = (int)DR["CCD"];
                decimal CCDValor = (decimal)DR["CCDValor"];
                string Arquivo = (string)DR["ARQNomeArquivo"];
                string Arquivocompleto = string.Format("C:\\lixo\\retorno\\{0}", Arquivo);
                if (System.IO.File.Exists(Arquivocompleto))
                {
                    dllCNAB.CNAB240 CNAB = new dllCNAB.CNAB240();
                    CNAB.Carrega(Arquivocompleto);
                    bool Feito = false;
                    foreach (regHeaderLote_1_E_04 lote in CNAB.LotesExtrato)
                    {
                        foreach (regDetalhe_3_E lan in lote.Lancamentos)
                            if ((lan.ValorLancamento == -CCDValor) && (lan.Historico.Trim() == "CHEQ COMP") && (lan.DebitoCredito == "D"))
                            {
                                int Doc = int.Parse(lan.Documento.Trim());
                                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Correcao, Doc, CCD);
                                Feito = true;
                                Console.WriteLine("ok");
                                break;
                            }
                        if (Feito)
                            break;
                    }

                }
                else
                    Console.WriteLine(Arquivo);
            }
        }
    }
}

