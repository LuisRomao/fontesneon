﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace TesteBoletos
{
    public partial class cTestaTMP_Para_SCC : ComponenteBase
    {
        public cTestaTMP_Para_SCC()
        {
            InitializeComponent();
        }

        private dllbanco.CarregaExtrato.carga carga;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            carga = new dllbanco.CarregaExtrato.carga();
            carga.CarregaTMP();
            dCargaBindingSource.DataSource = carga.dCarga;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            carga.AbreLog();
            int[] Sels = gridView1.GetSelectedRows();
            dllbanco.CarregaExtrato.dCarga.TMP_SCCRow[] rowTMPs = new dllbanco.CarregaExtrato.dCarga.TMP_SCCRow[Sels.Length];
            for(int i = 0;i<Sels.Length;i++)
                rowTMPs[i] = (dllbanco.CarregaExtrato.dCarga.TMP_SCCRow)gridView1.GetDataRow(Sels[i]);
            memoEdit1.Text = string.Format("Total: {0}", Sels.Length);
            int k = 1;
            foreach (dllbanco.CarregaExtrato.dCarga.TMP_SCCRow rowTMP in rowTMPs)
            {
                int SCC = rowTMP.TMPSCC;
                memoEdit1.Text = string.Format("TMP SCC {0} {1}/{2}\r\n", SCC, k++, Sels.Length) + memoEdit1.Text;
                if (!carga.UnidadeCarga(rowTMP))                
                    memoEdit1.Text = string.Format("ERRO: TMP SCC {0} {1}/{2}\r\n", SCC, k, Sels.Length) + memoEdit1.Text;
                
                Application.DoEvents();
            }
            carga.GravaLog();
            MessageBox.Show("ok");
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            dllbanco.CarregaExtrato.dCarga.TMP_SCCRow rowTMP = (dllbanco.CarregaExtrato.dCarga.TMP_SCCRow)gridView1.GetFocusedDataRow();
            if (rowTMP == null)
                return;
            int? CCT = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CCT from contacorrente where cctconta = @P1 and CCTAplicacao = @P2", rowTMP.TMPConta, rowTMP.TMPAplicacao);

            if (CCT.HasValue)
            {
                dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(new DateTime(2015, 7, 1), new DateTime(2015, 10, 1), CCT.Value, false);
                Extrato.somenteleitura = false;
                Extrato.LancamentosFuturos(false);
                Extrato.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            }
        }
    }
}
