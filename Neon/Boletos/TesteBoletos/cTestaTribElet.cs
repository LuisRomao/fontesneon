﻿using System;
using dllbanco;

namespace TesteBoletos
{
    public partial class cTestaTribElet : CompontesBasicos.ComponenteBase
    {
        public cTestaTribElet()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            TributoEletronico TributoEletronico1 = new TributoEletronico();
            TributoEletronico1.ProcessarDados();
        }
    }
}
