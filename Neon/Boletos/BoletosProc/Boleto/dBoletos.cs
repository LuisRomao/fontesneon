﻿/*
MR - 25/02/2016 11:00           - Inclusão do campo de boleto com registro (CONBoletoComRegistro) no DadosCONDOMINIOSBOLTableAdapter
*/

using System;
using System.Collections.Generic;
using FrameworkProc;

namespace BoletosProc.Boleto
{

    /// <summary>
    /// dBoletos
    /// </summary>
    partial class dBoletos : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dBoletos(EMPTProc _EMPTProc1)
        : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }


        /// <summary>
        /// 
        /// </summary>
        public SortedList<int, AbstratosNeon.ABS_Apartamento> Apartamentos;

        private SortedList<int, AbstratosNeon.ABS_Condominio> _Condominios;

        private SortedList<int, BoletoProc> _Boletos;

        /// <summary>
        /// 
        /// </summary>
        public SortedList<int, BoletoProc> Boletos => _Boletos ?? (_Boletos = new SortedList<int, BoletoProc>());

        /// <summary>
        /// Conjunto de Condominios
        /// </summary>
        public System.Collections.Generic.SortedList<int, AbstratosNeon.ABS_Condominio> Condominios
        {
            get { return _Condominios ?? (_Condominios = new System.Collections.Generic.SortedList<int, AbstratosNeon.ABS_Condominio>()); }
        }

        /// <summary>
        /// Carrega apartamentos no dataset para agilizar
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="Forcar">Forca a recarga</param>
        /// <param name="Completar"></param>
        /// <returns></returns>
        public int CarregaAPTs(int CON, bool Forcar = false, bool Completar = false)
        {
            if ((!Forcar) && (Apartamentos != null) && (Completar))
            {
                SortedList<int, AbstratosNeon.ABS_Apartamento> NovosApartamentos = AbstratosNeon.ABS_Apartamento.ABSGetApartamentos(CON);
                foreach (int NovoAPT in NovosApartamentos.Keys)
                    if (!Apartamentos.ContainsKey(NovoAPT))
                        Apartamentos.Add(NovoAPT, NovosApartamentos[NovoAPT]);
            }
            else
            {
                if ((Apartamentos == null) || Forcar)
                    Apartamentos = AbstratosNeon.ABS_Apartamento.ABSGetApartamentos(CON);
            }
            return Apartamentos.Count;
        }

        /// <summary>
        /// Carrega todos os apartamentos ativos
        /// </summary>
        /// <param name="Forcar"></param>
        /// <param name="Completar"></param>
        /// <returns></returns>
        public int CarregaAPTs(bool Forcar = false, bool Completar = false)
        {
            if ((!Forcar) && (Apartamentos != null) && (Completar))
            {
                SortedList<int, AbstratosNeon.ABS_Apartamento> NovosApartamentos = AbstratosNeon.ABS_Apartamento.ABSGetApartamentosAtivos();
                foreach (int NovoAPT in NovosApartamentos.Keys)
                    if (!Apartamentos.ContainsKey(NovoAPT))
                        Apartamentos.Add(NovoAPT, NovosApartamentos[NovoAPT]);
            }
            else
            {
                if ((Apartamentos == null) || Forcar)
                    Apartamentos = AbstratosNeon.ABS_Apartamento.ABSGetApartamentosAtivos();
            }
            return Apartamentos.Count;
        }

        /// <summary>
        /// Carrega todos os apartamentos ativos
        /// </summary>
        /// <param name="Forcar"></param>
        /// <param name="Completar"></param>
        /// <returns></returns>
        public int CarregaAPTSeguro(bool? interno)
        {
            Apartamentos = AbstratosNeon.ABS_Apartamento.GetApartamentosSeguro(interno);
            return Apartamentos.Count;
        }

        private static dBoletos _dBoletosSt;

        /// <summary>
        /// dataset estático:dBoletos
        /// </summary>
        public static dBoletos dBoletosSt
        {
            get
            {
                if (_dBoletosSt == null)
                    _dBoletosSt = new dBoletos();
                return _dBoletosSt;
            }
        }

        private dBoletosTableAdapters.ExtraPendenteTableAdapter extraPendenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ExtraPendente
        /// </summary>
        public dBoletosTableAdapters.ExtraPendenteTableAdapter ExtraPendenteTableAdapter
        {
            get
            {
                if (extraPendenteTableAdapter == null)
                {
                    extraPendenteTableAdapter = new dBoletosTableAdapters.ExtraPendenteTableAdapter();
                    extraPendenteTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return extraPendenteTableAdapter;
            }
        }

        private dBoletosTableAdapters.BODxBODTableAdapter bODxBODTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BODxBOD
        /// </summary>
        public dBoletosTableAdapters.BODxBODTableAdapter BODxBODTableAdapter
        {
            get
            {
                if (bODxBODTableAdapter == null)
                {
                    bODxBODTableAdapter = new dBoletosTableAdapters.BODxBODTableAdapter();
                    bODxBODTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return bODxBODTableAdapter;
            }
        }

        private dBoletosTableAdapters.BODxPAGTableAdapter bODxPAGTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BODxPAG
        /// </summary>
        public dBoletosTableAdapters.BODxPAGTableAdapter BODxPAGTableAdapter
        {
            get
            {
                if (bODxPAGTableAdapter == null)
                {
                    bODxPAGTableAdapter = new dBoletosTableAdapters.BODxPAGTableAdapter();
                    bODxPAGTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return bODxPAGTableAdapter;
            }
        }

        private dBoletosTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dBoletosTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dBoletosTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return conTasLogicasTableAdapter;
            }
        }

        private dBoletosTableAdapters.CTLxCCTTableAdapter cTLxCCTTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CTLxCCT
        /// </summary>
        public dBoletosTableAdapters.CTLxCCTTableAdapter CTLxCCTTableAdapter
        {
            get
            {
                if (cTLxCCTTableAdapter == null)
                {
                    cTLxCCTTableAdapter = new dBoletosTableAdapters.CTLxCCTTableAdapter();
                    cTLxCCTTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cTLxCCTTableAdapter;
            }
        }

        private dBoletosTableAdapters.DescontoAPartamentoTableAdapter descontoAPartamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DescontoAPartamento
        /// </summary>
        public dBoletosTableAdapters.DescontoAPartamentoTableAdapter DescontoAPartamentoTableAdapter
        {
            get
            {
                if (descontoAPartamentoTableAdapter == null)
                {
                    descontoAPartamentoTableAdapter = new dBoletosTableAdapters.DescontoAPartamentoTableAdapter();
                    descontoAPartamentoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return descontoAPartamentoTableAdapter;
            }
        }

        private dBoletosTableAdapters.BoletoDEscontoTableAdapter boletoDEscontoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BoletoDEsconto
        /// </summary>
        public dBoletosTableAdapters.BoletoDEscontoTableAdapter BoletoDEscontoTableAdapter
        {
            get
            {
                if (boletoDEscontoTableAdapter == null)
                {
                    boletoDEscontoTableAdapter = new dBoletosTableAdapters.BoletoDEscontoTableAdapter();
                    boletoDEscontoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return boletoDEscontoTableAdapter;
            }
        }

        private dBoletosTableAdapters.SuperBOletoTableAdapter superBOletoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SuperBOleto
        /// </summary>
        public dBoletosTableAdapters.SuperBOletoTableAdapter SuperBOletoTableAdapter
        {
            get
            {
                if (superBOletoTableAdapter == null)
                {
                    superBOletoTableAdapter = new dBoletosTableAdapters.SuperBOletoTableAdapter();
                    superBOletoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return superBOletoTableAdapter;
            }
        }

        private dBoletosTableAdapters.SuperBoletoDetalheTableAdapter superBoletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SuperBoletoDetalhe
        /// </summary>
        public dBoletosTableAdapters.SuperBoletoDetalheTableAdapter SuperBoletoDetalheTableAdapter
        {
            get
            {
                if (superBoletoDetalheTableAdapter == null)
                {
                    superBoletoDetalheTableAdapter = new dBoletosTableAdapters.SuperBoletoDetalheTableAdapter();
                    superBoletoDetalheTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return superBoletoDetalheTableAdapter;
            }
        }



        private static dBoletosTableAdapters.BOLetosTableAdapter stBOLetosTableAdapter;
        private static dBoletosTableAdapters.BOletoDetalheTableAdapter stBOletoDetalheTableAdapter;



        private dBoletosTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        //private dBoletosTableAdapters.BoletoAccessTableAdapter boletoAccessTableAdapter;
        private dBoletosTableAdapters.UsuarioSenhaTableAdapter usuarioSenhaTableAdapter;

        /// <summary>
        /// ??
        /// </summary>
        public dBoletosTableAdapters.UsuarioSenhaTableAdapter UsuarioSenhaTableAdapter
        {
            get
            {
                if (usuarioSenhaTableAdapter == null)
                {
                    usuarioSenhaTableAdapter = new BoletosProc.Boleto.dBoletosTableAdapters.UsuarioSenhaTableAdapter();
                    usuarioSenhaTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return usuarioSenhaTableAdapter;
            }
        }

        /*
        public dBoletosTableAdapters.BoletoAccessTableAdapter BoletoAccessTableAdapter
        {
            get
            {
                if (boletoAccessTableAdapter == null) {
                    boletoAccessTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.BoletoAccessTableAdapter();
                    boletoAccessTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return boletoAccessTableAdapter;
            }
        }*/

        /// <summary>
        /// CONDOMINIOSTableAdapter
        /// </summary>
        public dBoletosTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new BoletosProc.Boleto.dBoletosTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        /// <summary>
        /// STBOLetosTableAdapter
        /// </summary>
        public static dBoletosTableAdapters.BOLetosTableAdapter STBOLetosTableAdapter
        {
            get
            {
                if (stBOLetosTableAdapter == null)
                {
                    stBOLetosTableAdapter = new dBoletosTableAdapters.BOLetosTableAdapter();
                    stBOLetosTableAdapter.TrocarStringDeConexao(BoletoProc.TipoPadrao);
                };
                return stBOLetosTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static dBoletosTableAdapters.BOletoDetalheTableAdapter STBOletoDetalheTableAdapter
        {
            get
            {
                if (stBOletoDetalheTableAdapter == null)
                {
                    stBOletoDetalheTableAdapter = new BoletosProc.Boleto.dBoletosTableAdapters.BOletoDetalheTableAdapter();
                    stBOletoDetalheTableAdapter.TrocarStringDeConexao(BoletosProc.Boleto.BoletoProc.TipoPadrao);
                };
                return stBOletoDetalheTableAdapter;
            }
        }

        private dBoletosTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;
        private dBoletosTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        private dBoletosTableAdapters.DadosAPTTableAdapter dadosAPTTableAdapter;
        private dBoletosTableAdapters.DadosCONDOMINIOSBOLTableAdapter dadosCONDOMINIOSBOLTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public dBoletosTableAdapters.DadosCONDOMINIOSBOLTableAdapter DadosCONDOMINIOSBOLTableAdapter
        {
            get
            {
                if (dadosCONDOMINIOSBOLTableAdapter == null)
                {
                    dadosCONDOMINIOSBOLTableAdapter = new BoletosProc.Boleto.dBoletosTableAdapters.DadosCONDOMINIOSBOLTableAdapter();
                    dadosCONDOMINIOSBOLTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                }
                return dadosCONDOMINIOSBOLTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dBoletosTableAdapters.DadosAPTTableAdapter DadosAPTTableAdapter
        {
            get
            {
                if (dadosAPTTableAdapter == null)
                {
                    dadosAPTTableAdapter = new BoletosProc.Boleto.dBoletosTableAdapters.DadosAPTTableAdapter();
                    dadosAPTTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                }
                return dadosAPTTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dBoletosTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (bOLetosTableAdapter == null)
                {
                    bOLetosTableAdapter = new dBoletosTableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao(BoletoProc.TipoPadrao);
                };
                return bOLetosTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dBoletosTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dBoletosTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao(BoletoProc.TipoPadrao);
                };
                return bOletoDetalheTableAdapter;
            }
        }

        /*
        /// <summary>
        /// Calculas os campos complementares
        /// </summary>
        /// <param name="Data">Data de cálculo</param>
        /// <returns>Indica se algum boleto foi alterado durante o processo de cálculo dos dados complemetares</returns>
        public bool CamposComplementares(System.DateTime Data) {
            return CamposComplementares(Data, true);
        }*/

        /// <summary>
        /// Calculas os campos complementares
        /// </summary>
        /// <param name="Data">Data de cálculo</param>
        /// <param name="ocultarZeros">Oculta os zeros colocando null</param>        
        /// <returns>Indica se algum boleto foi alterado durante o processo de cálculo dos dados complemetares</returns>
        //public bool CamposComplementares(System.DateTime Data, bool ocultarZeros = true, CompontesBasicos.Espera.cEspera ESP = null)
        public bool CamposComplementares(System.DateTime Data, bool ocultarZeros = true)
        {
            Boletos.Clear();

            bool CamposComplementares_Recalcular = false;
            //if (ESP != null)
            //    ESP.AtivaGauge(BOLetos.Count);
            System.DateTime AjGauge = System.DateTime.Now.AddSeconds(3);
            int i = 0;
            foreach (dBoletos.BOLetosRow rowBOL in BOLetos)
            {
                i++;
                BoletoProc Bol;
                if (Boletos.ContainsKey(rowBOL.BOL))
                    Bol = Boletos[rowBOL.BOL];
                else
                {
                    Bol = new BoletoProc(rowBOL);
                    Boletos.Add(rowBOL.BOL, Bol);
                }
                if (Bol.RemoveSeguro(BoletoProc.TipoRemoveSeguro.remove_SeguroVencido))
                    CamposComplementares_Recalcular = true;
                if (Bol.Apartamento != null)
                {
                    rowBOL.BLOCodigo = Bol.Apartamento.BLOCodigo;
                    rowBOL.APTNumero = Bol.Apartamento.APTNumero;
                }
                if (rowBOL.IsBOLProprietarioNull())
                    rowBOL.Destinatario = "";
                else
                    rowBOL.Destinatario = rowBOL.BOLProprietario ? "Prop" : "Inq.";
                if ((rowBOL.IsBOLPagamentoNull()) || (rowBOL.BOLPagamento > Data))
                {
                    Bol.Valorpara(Data);
                    rowBOL.ValorCorrigido = Bol.valorcorrigido;
                    if (Bol.multa > 0)
                        rowBOL.Multa = Bol.multa;
                    else
                        rowBOL.SetMultaNull();
                    if (Bol.juros > 0)
                        rowBOL.Juros = Bol.juros;
                    else
                        rowBOL.SetJurosNull();
                    rowBOL.SubTotal = Bol.valorfinal;
                    rowBOL.Honorarios = Bol.honor;
                    rowBOL.Total = Bol.valorfinal + Bol.honor;
                }
                else
                {
                    if (!ocultarZeros)
                    {
                        rowBOL.Total = rowBOL.SubTotal = Bol.valorfinal;
                    }
                }
                rowBOL.AcceptChanges();
            }
            return CamposComplementares_Recalcular;
        }

        /// <summary>
        /// Inclui os boletos de acodos feitos apos a data de corte
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="CON"></param>
        public void RecuperaOsBolDoAcordo(System.DateTime Data, int CON)
        {
            string comandoRec = "SELECT     BOLetos.BOL\r\n" +
                                "FROM         ACOrdos INNER JOIN\r\n" +
                                "ACOxBOL ON ACOrdos.ACO = ACOxBOL.ACO INNER JOIN\r\n" +
                                "BOLetos ON ACOxBOL.BOL = BOLetos.BOL INNER JOIN\r\n" +
                                "APARTAMENTOS ON ACOrdos.ACO_APT = APARTAMENTOS.APT INNER JOIN\r\n" +
                                "BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO\r\n" +
                                "WHERE   (ACOStatus in (1,2)) and  (ACOrdos.ACODATAI > @P1) AND (BOLetos.BOLVencto <= @P2) AND (BLOCOS.BLO_CON = @P3)";
            System.Data.DataTable BolRec = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comandoRec, Data.AddDays(1).AddSeconds(-1), Data, CON);
            if (BolRec.Rows.Count > 0)
                try
                {
                    BOLetosTableAdapter.ClearBeforeFill = false;
                    foreach (System.Data.DataRow rowB in BolRec.Rows)
                        if (this.BOLetos.FindByBOL((int)rowB[0]) == null)
                            BOLetosTableAdapter.FillByBOL(this.BOLetos, (int)rowB[0]);
                }
                finally
                {
                    BOLetosTableAdapter.ClearBeforeFill = true;
                };

        }

        #region Velocidade
        private System.Collections.Generic.SortedList<int, string> BOLMensagemImps;

        private int CON;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_CON"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public string BOLMensagemImp(int _CON, Framework.objetosNeon.Competencia comp)
        {
            if (CON != _CON)
            {
                CON = _CON;
                BOLMensagemImps = null;
            }
            if (BOLMensagemImps == null)
                BOLMensagemImps = new System.Collections.Generic.SortedList<int, string>();
            if (!BOLMensagemImps.ContainsKey(comp.CompetenciaBind))
            {
                string PBOMensagemBalancete = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_string("SELECT PBOMensagemBalancete FROM PrevisaoBOletos WHERE (PBO_CON = @P1) AND (PBOCompetencia = @P2)", CON, comp.CompetenciaBind);
                BOLMensagemImps.Add(comp.CompetenciaBind, PBOMensagemBalancete);
            }
            return BOLMensagemImps[comp.CompetenciaBind];
        }
        #endregion


    }
}




