﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abstratos;
using AbstratosNeon;
using FrameworkProc;

namespace BoletosProc.Boleto
{
    /// <summary>
    /// Super Boleto para processos
    /// </summary>
    public class SuperBoletoProc : ABS_BoletoNeon, IEMPTProc, IDisposable
    {
        /// <summary>
        /// Boletos contidos no super
        /// </summary>
        public List<BoletoProc> BoletosFilhos
        {
            get
            {
                if (boletosFilhos == null)
                {
                    boletosFilhos = new List<BoletoProc>();
                    if ((DSuperBoleto.SuperBoletoDetalhe.Count == 0) && (SBO != null))
                    {
                        EMPTProc1.EmbarcaEmTrans(DSuperBoleto.SuperBoletoDetalheTableAdapter);
                        DSuperBoleto.SuperBoletoDetalheTableAdapter.FillBySBO(DSuperBoleto.SuperBoletoDetalhe, SBO.Value);
                    }
                    foreach (dSuperBoleto.SuperBoletoDetalheRow rowSBD in rowSBO.GetSuperBoletoDetalheRows())
                    {
                        boletosFilhos.Add((BoletoProc)ABS_BoletoNeon.GetBoleto(rowSBD.SBD_BOL));
                    }
                }
                return boletosFilhos;
            }            
        }
        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }


        private EMPTProc _EMPTProc1;
        private dSuperBoleto _DSuperBoleto;
        private int? _SBO;

        /// <summary>
        /// Boletos contidos no super
        /// </summary>
        private List<BoletoProc> boletosFilhos;

        /// <summary>
        /// SBO
        /// </summary>
        public int? SBO
        {
            get { return _SBO; }
            set
            {
                _SBO = value;
            }
        }

        /// <summary>
        /// Dataset interno
        /// </summary>
        public dSuperBoleto DSuperBoleto
        {
            get 
            {
                if (_DSuperBoleto == null)
                    _DSuperBoleto = new dSuperBoleto();
                return _DSuperBoleto; 
            }
            set
            {
                _DSuperBoleto = value;
            }
        }
         
        /// <summary>
        /// Linha mãe do SuperBoletoProc
        /// </summary>
        protected dSuperBoleto.SuperBOletoRow rowSBO;

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (_DSuperBoleto != null)
            {
                _DSuperBoleto.Dispose();
                _DSuperBoleto = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SBO"></param>
        /// <param name="EMPTProc"></param>
        public SuperBoletoProc(int SBO, EMPTProc EMPTProc = null)
        {
            _EMPTProc1 = EMPTProc;
            _SBO = SBO;
            CarregaDadosDoSuper(SBO);            
        }

        /// <summary>
        /// Initializes a new instance of the SuperBoletoProc class.
        /// </summary>
        public SuperBoletoProc(dSuperBoleto.SuperBoletoDetalheRow rowSBD, EMPTProc EMPTProc = null)
        {
            _EMPTProc1 = EMPTProc;
            _DSuperBoleto = (dSuperBoleto)rowSBD.Table.DataSet;
            _SBO = rowSBD.SBD_SBO;            
            this.rowSBO = rowSBD.SuperBOletoRow;
        }

        private bool CarregaDadosDoSuper(int _SBO)
        {
            EMPTProc1.EmbarcaEmTrans(DSuperBoleto.SuperBOletoTableAdapter);
            if (DSuperBoleto.SuperBOletoTableAdapter.FillBySBO(DSuperBoleto.SuperBOleto, _SBO) == 1)
            {
                rowSBO = DSuperBoleto.SuperBOleto[0];
                DSuperBoleto.SuperBoletoDetalhe.Clear();
                Encontrado = true;
            }
            return Encontrado;
        }

        /// <summary>
        /// Código do condominio
        /// </summary>
        public override int CON
        {
            get
            {
                return BoletosFilhos[0].CON;               
            }
        }        

        /// <summary>
        /// Apartamento do boleto
        /// </summary>        
        public override int? APT 
        { 
            get 
            {                 
                return null; 
            } 
        }

        /// <summary>
        /// Proprietário ou inquilino
        /// </summary>
        public bool Proprietario
        {
            get { return BoletosFilhos[0].rowPrincipal.BOLProprietario; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override ABS_Condominio Condominio
        {
            get
            {
                if (SBO.HasValue && BoletosFilhos.Count > 0)
                    return BoletosFilhos[0].Condominio;
                return null;
            }
        }

        /// <summary>
        /// Retorna o apartamento do primeiro boleto. Usado para acessar o cadastro do proprietario que geralmente é o mesmo para todos.
        /// </summary>
        public override ABS_Apartamento Apartamento
        {
            get
            {
                if (SBO.HasValue && BoletosFilhos.Count > 0)
                    return BoletosFilhos[0].Apartamento;
                return null;
            }
            set
            {
                base.Apartamento = value;
            }
        }

        /// <summary>
        /// Grava a Juatificatica em cada boleto
        /// </summary>
        /// <param name="Obs"></param>
        /// <param name="EmailJuridico"></param>
        public override void GravaJustificativa(string Obs, string EmailJuridico)
        {
            if (SBO.HasValue && BoletosFilhos.Count > 0)
            {
                Obs = string.Format("SuperBoleto {0}\r\n-----\r\n{1}\r\n-----", SBO.Value, Obs);
                foreach(BoletoProc BOL in BoletosFilhos)
                    BOL.GravaJustificativa(Obs,EmailJuridico);
            }
            
        }

        /// <summary>
        /// Conta Corrente dos boletos
        /// </summary>
        public CompontesBasicosProc.ContaCorrente Conta
        {
            get
            {
                if (SBO.HasValue && BoletosFilhos.Count > 0)
                    return BoletosFilhos[0].Conta;
                return null;
            }
            set 
            {               
                throw new NotImplementedException("set para conta em super-boleto");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override ABS_ContaCorrente ABSConta
        {
            get
            {
                if (SBO.HasValue && BoletosFilhos.Count > 0)
                    return BoletosFilhos[0].Conta;
                return null;
            }
            set
            {                
                throw new NotImplementedException("set para conta em super-boleto");
            }
        }

        /// <summary>
        /// Incluir o custo da emissão de segunda via
        /// </summary>        
        public override bool IncluicustoSegundaVia(bool QuestionarUsuario)
        {
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public override ABS_Competencia ABSCompetencia => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        public override decimal ValorOriginal => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        public override DateTime VencimentoOriginal => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        public override decimal ValorCorrigido => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        public override decimal ValorFinal => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        public override decimal Valorjuros => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        public override decimal Valormulta => throw new NotImplementedException();
    }
}
