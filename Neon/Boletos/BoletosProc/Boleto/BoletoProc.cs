﻿using AbstratosNeon;
using Framework;
using Framework.objetosNeon;
using FrameworkProc;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VirEnumeracoesNeon;
using Abstratos;
using VirMSSQL;

namespace BoletosProc.Boleto
{
    /// <summary>
    /// Objeto Boleto para processos
    /// </summary>
    public class BoletoProc : ABS_BoletoNeon, IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                {
                    if (TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                }
                return _EMPTProc1;
            }
            set => _EMPTProc1 = value;
        }

        /// <summary>
        /// Competência
        /// </summary>
        public override ABS_Competencia ABSCompetencia => Competencia;

        /// <summary>
        /// Valor Original
        /// </summary>
        public override decimal ValorOriginal => rowPrincipal.BOLValorPrevisto;

        /// <summary>
        /// Vencimento original
        /// </summary>
        public override DateTime VencimentoOriginal => rowPrincipal.BOLVencto;

        /// <summary>
        /// Valor dos juros
        /// </summary>
        public override decimal Valorjuros => juros;

        /// <summary>
        /// Valor da multa
        /// </summary>
        public override decimal Valormulta => multa;

        /// <summary>
        /// Valor corrigido
        /// </summary>
        public override decimal ValorCorrigido => valorcorrigido;

        /// <summary>
        /// Valor corrigido mais multa e juros
        /// </summary>
        public override decimal ValorFinal => rowPrincipal.SubTotal;

        /// <summary>
        /// Tipo
        /// </summary>
        public string TipoCRAI => rowPrincipal.BOLTipoCRAI;

        /// <summary>
        /// Competência do boleto
        /// </summary>
        public Competencia Competencia;

        /// <summary>
        /// Valor corrigido
        /// </summary>
        public decimal valorcorrigido;
        /// <summary>
        /// Multa
        /// </summary>
        public decimal multa = 0;
        /// <summary>
        /// Juros
        /// </summary>
        public decimal juros = 0;
        /// <summary>
        /// Honorários
        /// </summary>
        public decimal honor = 0;
        /// <summary>
        /// Desconto
        /// </summary>
        public decimal desconto = 0;
        /// <summary>
        /// Honorários
        /// </summary>
        public decimal porhonorario = 0.2M;
        /// <summary>
        /// Valor Final
        /// </summary>
        public decimal valorfinal = 0;

        /// <summary>
        /// Registra o ultimo erro ocorrido
        /// </summary>
        public string UltimoErro;

        /// <summary>
        /// Linha da tabela de boletos
        /// </summary>
        public dBoletos.BOLetosRow rowPrincipal;

        protected dBoletos.BOLetosRow LinhaMae 
        {
            get => rowPrincipal;
            set
            {
                rowPrincipal = value;
                dBoletos = (dBoletos)value.Table.DataSet;
            }
        }

        /// <summary>
        /// Indica se é um Super Boleto (agrupamento)
        /// </summary>
        public dBoletos.SuperBOletoRow rowSBO = null;

        /// <summary>
        /// Inicia campos
        /// </summary>
        protected void IniciaCampos()
        {
            Competencia = new Competencia(rowPrincipal.BOLCompetenciaMes, rowPrincipal.BOLCompetenciaAno, CON);
            valorcorrigido = valorfinal = rowPrincipal.BOLValorPrevisto;
        }

        /// <summary>
        /// BODrowsOriginal
        /// </summary>
        public dBoletos.BOletoDetalheRow[] BODrowsOriginal;

        private SortedList<int, string> _DadosLinhasOriginais;

        /// <summary>
        /// DadosLinhasOriginais
        /// </summary>
        public SortedList<int, string> DadosLinhasOriginais
        {
            get
            {
                if (_DadosLinhasOriginais == null)
                    _DadosLinhasOriginais = new SortedList<int, string>();
                return _DadosLinhasOriginais;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void GuardaBODsOriginal()
        {
            _DadosLinhasOriginais = null;
            BODrowsOriginal = rowPrincipal.GetBOletoDetalheRows();
            foreach (dBoletos.BOletoDetalheRow BODrow in BODrowsOriginal)
                DadosLinhasOriginais.Add(BODrow.GetHashCode(), String.Format("{0}: {1:n2}", BODrow.BODMensagem, BODrow.BODValor));
        }

        /// <summary>
        /// Construtor Padrão
        /// </summary>
        public BoletoProc(EMPTProc _EMPTProc = null)
        {            
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
        }

        /// <summary>
        /// Construtor para uma nota já existente. verifique depois se "encotrada" está como true
        /// </summary>
        /// <param name="NOA"></param>
        /// <param name="_EMPTProc1"></param>
        public BoletoProc(int Bol, EMPTProc _EMPTProc1 = null)
            : this(_EMPTProc1)
        {
            EMPTProc1.EmbarcaEmTrans(dBoletos.BOLetosTableAdapter);
            if (dBoletos.BOLetosTableAdapter.FillByBOL(dBoletos.BOLetos, Bol) > 0)
                LinhaMae = dBoletos.BOLetos[0];
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rowPrincipal"></param>
        public BoletoProc(dBoletos.BOLetosRow rowPrincipal, EMPTProc _EMPTProc1 = null)
            : this(_EMPTProc1)
        {            
            Encontrado = true;
            LinhaMae = rowPrincipal;            
            GuardaBODsOriginal();
            IniciaCampos();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PLA"></param>
        /// <returns></returns>
        protected Boolean Contemlinha(string PLA)
        {
            foreach (dBoletos.BOletoDetalheRow RowBOD in rowPrincipal.GetBOletoDetalheRows())
                if (RowBOD.BOD_PLA == PLA)
                    return true;
            return false;
        }

        /// <summary>
        /// Verifica se o boleto contém seguro conteúdo
        /// </summary>
        /// <returns></returns>
        public bool ComSeguro()
        {
            return Contemlinha(PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoC));
        }

        private decimal ValorSemSeguro()
        {
            decimal Valor = 0;
            foreach (dBoletos.BOletoDetalheRow rowemTeste in rowPrincipal.GetBOletoDetalheRows())
                if (rowemTeste.BOD_PLA != PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoC))
                    Valor += rowemTeste.BODValor;
            return Valor;
        }

        /// <summary>
        /// Data de cálculo
        /// </summary>
        public DateTime DataDeCalculo;

        /// <summary>
        /// Status de pagamento
        /// </summary>
        public Statusparcela Status;

        /// <summary>
        /// Data de vencimento efetiva (primeiro dia útil)
        /// </summary>
        public DateTime VencimentoCorrigido
        {
            get
            {
                return rowPrincipal.BOLVencto.SomaDiasUteis(0);
            }
        }

        private void CalculaStatus(DateTime DataRef)
        {            
            if (VencimentoCorrigido >= DataRef)
                Status = Statusparcela.Aberto;
            else if (rowPrincipal.BOLVencto >= (DataRef.AddDays(-3)))
                Status = Statusparcela.AbertoTolerancia3;
            else
            {
                DateTime DataLimeiteTol30 = rowPrincipal.BOLVencto.AddMonths(1).SomaDiasUteis(0);
                //DateTime DataLimeiteTol30 = AjustaParaDiaUtil(rowPrincipal.BOLVencto.AddMonths(1),true);
                if (DataLimeiteTol30 >= DataRef)
                    Status = Statusparcela.AtrasadoTolerancia30;
                else Status = Statusparcela.Atrasado;
            }
        }

        private float indiceinicial = 1;
        private float indicefinal = 1;
        private float inpcmesinicial = 1;
        private float inpcmesfinal = 1;

        private void buscaindices(DateTime dataf, DateTime datai)
        {
            FrameworkProc.datasets.dINPC.dINPCSt.buscaIndices(ref inpcmesinicial, ref indiceinicial, new Competencia(datai.Month, datai.Year));
            FrameworkProc.datasets.dINPC.dINPCSt.buscaIndices(ref inpcmesfinal, ref indicefinal, new Competencia(dataf.Month, dataf.Year));
        }

        /// <summary>
        /// TaxaJurosMes
        /// </summary>
        public double TaxaJurosMes { get { return (double)(1.0M + Condominio.CONValorJuros / 100); } }

        /// <summary>
        /// Calcula o valor para uma determinada data
        /// </summary>
        /// <param name="data">data de calculo</param>
        /// <param name="RemoveSeguro">Desconta o valor do seguro se houver</param>
        /// <returns>ok</returns>
        public bool Valorpara(DateTime data, bool RemoveSeguro = false)
        {
            //Referencia OneNote: CalculoBoletos
            if (!Encontrado)
                return false;
            else
            {
                DataDeCalculo = data;
                TimeSpan diascorridos = (data - rowPrincipal.BOLVencto);
                CalculaStatus(data);
                if (RemoveSeguro && ComSeguro())
                    valorcorrigido = ValorSemSeguro();
                else
                    valorcorrigido = rowPrincipal.BOLValorPrevisto;

                switch (Status)
                {
                    case Statusparcela.Paga:
                        break;
                    case Statusparcela.Aberto:
                        DateTime dataRef = data.SomaDiasUteis(0, Sentido.Traz);
                        object oDesconto = dBoletos.BoletoDEscontoTableAdapter.DescontoNaData(rowPrincipal.BOL, dataRef);
                        if (oDesconto != null)
                        {
                            desconto = (decimal)oDesconto;
                            valorcorrigido -= desconto;
                        }
                        break;
                    case Statusparcela.AbertoTolerancia3:
                        break;
                    case Statusparcela.AtrasadoTolerancia30:
                        break;
                    case Statusparcela.Atrasado:
                        int diasinicio = rowPrincipal.BOLVencto.Day;
                        int diasfinal = 30 - data.Day;
                        buscaindices(data, rowPrincipal.BOLVencto);
                        valorcorrigido *= (decimal)((indicefinal / indiceinicial) * inpcmesfinal);
                        //Correção dos dias iniciais
                        valorcorrigido /= (decimal)Math.Pow(inpcmesinicial, (diasinicio / 30D));
                        //Correção dos dias finais
                        valorcorrigido /= (decimal)Math.Pow(inpcmesfinal, (diasfinal / 30D));
                        break;
                    default:
                        break;
                }


                decimal TxMulta = rowPrincipal.BOLMulta / 100M;
                if ((Status == Statusparcela.AbertoTolerancia3) || (Status == Statusparcela.AtrasadoTolerancia30) || (Status == Statusparcela.Atrasado))
                {
                    //bool jurosCompostos = true;
                    //bool corbaraJurosNoPrimeiroMes = false;
                    //if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONJuroscompostos from condominios where CON = @P1", out jurosCompostos, rowComplementar.CON))
                    //    jurosCompostos = true;
                    //if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONJurosPrimeiroMes from condominios where CON = @P1", out corbaraJurosNoPrimeiroMes, rowComplementar.CON))
                    //    corbaraJurosNoPrimeiroMes = false;
                    if (rowPrincipal.BOLTipoMulta == "M")
                        multa = valorcorrigido * TxMulta;
                    else
                        if (diascorridos.Days > 30)
                            multa = valorcorrigido * 30 * TxMulta;
                        else
                            multa = valorcorrigido * diascorridos.Days * TxMulta;

                    if ((Status == Statusparcela.Atrasado) || Condominio.CONJurosPrimeiroMes)
                    {
                        if (Condominio.CONJurosCompostos)
                            juros = (valorcorrigido + multa) * (decimal)(Math.Pow(TaxaJurosMes, (diascorridos.Days / 30D)) - 1);
                        else
                            juros = (valorcorrigido + multa) * (decimal)((TaxaJurosMes - 1) * (diascorridos.Days / 30D));
                    }
                    else
                        juros = 0;
                };

                multa = Math.Round(multa, 2, MidpointRounding.AwayFromZero);
                juros = Math.Round(juros, 2, MidpointRounding.AwayFromZero);

                valorfinal = Math.Round(valorcorrigido, 2, MidpointRounding.AwayFromZero) + multa + juros;
                valorcorrigido = valorfinal - multa - juros;
                honor = Math.Round(valorfinal * porhonorario, 2, MidpointRounding.AwayFromZero);
                return true;
            }
        }

        /// <summary>
        /// Tipos de seguro
        /// </summary>
        public enum TipoRemoveSeguro
        {
            /// <summary>
            /// Remove o seguro
            /// </summary>
            soremove,
            // <summary>
            // Remove o seguro e descadastra
            // </summary>
            //remove_descadastra, 
            /// <summary>
            /// Remove o seguro se o seguro estiver vencido
            /// </summary>
            remove_SeguroVencido,
            /// <summary>
            /// Remove o seguro porque o arquivo ja foi gerado
            /// </summary>
            remove_ArquivoGerado,
        }

        /// <summary>
        /// Remove os seguros não pagos
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns>Verdadeiro se o seguro foi removido</returns>
        public bool RemoveSeguro(TipoRemoveSeguro Tipo)
        {
            if (!ComSeguro() || rowPrincipal.BOLCancelado || !rowPrincipal.IsBOLPagamentoNull())
                return false;
            if (Tipo == TipoRemoveSeguro.remove_SeguroVencido)
            {
                // se o seruro ainda for válido não deve ser removido
                DateTime DataValidadeSeguro = new DateTime(rowPrincipal.BOLVencto.Year, rowPrincipal.BOLVencto.Month, 1).AddMonths(1).AddDays(-1).SomaDiasUteis(2, Sentido.Frente);
                //DataValidadeSeguro = Framework.datasets.dFERiados.DiasUteis(DataValidadeSeguro, 2, Framework.datasets.dFERiados.Sentido.avanca, Framework.datasets.dFERiados.Sabados.naoUteis, Framework.datasets.dFERiados.Feriados.naoUteis);                
                if (DateTime.Today < DataValidadeSeguro)
                    return false;
            }
            if (rowPrincipal.BOLTipoCRAI == "S")
            {
                rowPrincipal.BOLCancelado = true;
                GravaJustificativa(string.Format("Em aberto no momento da geração do arquivo: {0:dd/MM/yyyy HH:mm:ss}", DateTime.Now), "");
                return false;
            }
            else
            {
                dBoletos DataSet = (dBoletos)rowPrincipal.Table.DataSet;
                dBoletos.BOletoDetalheRow rowBOD = null;
                foreach (dBoletos.BOletoDetalheRow rowemTeste in rowPrincipal.GetBOletoDetalheRows())
                    if (rowemTeste.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoC))
                    {
                        rowBOD = rowemTeste;
                        break;
                    }
                if (rowBOD != null)
                {
                    rowPrincipal.BOLValorPrevisto -= rowBOD.BODValor;
                    if (rowBOD.IsBODPrevistoNull() || rowBOD.BODPrevisto)
                        if (!rowPrincipal.IsBOLValorOriginalNull())
                            rowPrincipal.BOLValorOriginal -= rowBOD.BODValor;
                    rowBOD.Delete();
                    if (rowPrincipal.BOLValorPrevisto == 0)
                    {
                        rowPrincipal.BOLCancelado = true;
                        //Tipo = TipoRemoveSeguro.remove_descadastra;
                        //string ComandoApaga = "delete from [boleto - pagamentos] where [Número Boleto] = @P1;";
                        //VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL);
                        //ComandoApaga = "delete from boleto where [Número Boleto] = @P1;";
                        //VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL);
                    }
                    rowPrincipal.BOLExportar = true;
                }

                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 2278",
                                                                          dBoletos.BOLetosTableAdapter,
                                                                          dBoletos.BOletoDetalheTableAdapter);
                    dBoletos.BOletoDetalheTableAdapter.Update(DataSet.BOletoDetalhe);
                    //exporta na própria thread
                    //ReExportaTh();
                    GuardaBODsOriginal();
                    string Justificativa;
                    if (Tipo == TipoRemoveSeguro.soremove)
                        Justificativa = "Seguro conteúdo cancelado, pagamentos após vencimento do seguro";
                    //else if (Tipo == TipoRemoveSeguro.remove_descadastra)
                    //    Justificativa = string.Format("Seguro conteúdo cancelado, valor descontado no pagamento");
                    else
                        Justificativa = "Seguro conteúdo cancelado, boleto em aberto no vencimento do seguro";

                    GravaJustificativa(Justificativa, "");
                    //if (Tipo == TipoRemoveSeguro.remove_descadastra)
                    //    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update APARTAMENTOS set APTSeguro = 0 where APT = @P1", rowPrincipal.BOL_APT);
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();

                    return true;
                }

                catch (Exception e)
                {
                    UltimoErro = e.Message;
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                    throw new Exception("Erro: " + e.Message, e);
                }
            }

        }

        /// <summary>
        /// Define o Banco de dados a ser utilizado (local/internet etc)
        /// </summary>
        public static VirDB.Bancovirtual.TiposDeBanco TipoPadrao = VirDB.Bancovirtual.TiposDeBanco.SQL;

        /// <summary>
        /// Database usado
        /// </summary>
        protected dBoletos _dBoletos;

        /// <summary>
        /// ds interno
        /// </summary>
        public dBoletos dBoletos
        {
            get
            {
                if (_dBoletos == null)
                    _dBoletos = new dBoletos(EMPTProc1);
                return _dBoletos;
            }
            set
            {
                _dBoletos = value;
            }
        }

        /// <summary>
        /// Condomínio
        /// </summary>
        public override int CON => Condominio.CON;
        
        /// <summary>
        /// APT
        /// </summary>
        public override int? APT
        {
            get 
            {
                if((rowPrincipal != null) && (!rowPrincipal.IsBOL_APTNull()))
                    return rowPrincipal.BOL_APT;
                if (Apartamento != null)
                    return Apartamento.APT;                                    
                else
                    return null;
            }
        }

        /// <summary>
        /// Apartamento
        /// </summary>
        public override ABS_Apartamento Apartamento
        {
            get
            {
                if ((_Apartamento == null) && (!rowPrincipal.IsBOL_APTNull()))
                {
                    if ((dBoletos.Apartamentos != null) && (dBoletos.Apartamentos.ContainsKey(rowPrincipal.BOL_APT)))
                        _Apartamento = dBoletos.Apartamentos[rowPrincipal.BOL_APT];
                    else
                    {
                        _Apartamento = ABS_Apartamento.GetApartamento(rowPrincipal.BOL_APT);
                        if (dBoletos.Apartamentos == null)
                            dBoletos.Apartamentos = new SortedList<int, ABS_Apartamento>();
                        dBoletos.Apartamentos.Add(rowPrincipal.BOL_APT, _Apartamento);
                    }
                }
                return _Apartamento;                                
            }
            set
            {
                //é usado no caso do super
                _Apartamento = value;
            }
        }

        /// <summary>
        /// Forenecedor
        /// </summary>
        public override ABS_Fornecedor Fornecedor
        {
            get
            {
                if ((_Fornecedor == null) && (!rowPrincipal.IsBOL_FRNNull()))
                    _Fornecedor = ABS_Fornecedor.GetFornecedor(rowPrincipal.BOL_FRN);
                return _Fornecedor;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override ABS_Condominio Condominio
        {
            get 
            {                
                if (_Condominio == null)
                {                                                                               
                    if ((Apartamento != null) && (Apartamento.Condominio.CON == rowPrincipal.BOL_CON))
                        _Condominio = Apartamento.Condominio;
                    else
                    {                        
                        if (!dBoletos.Condominios.ContainsKey(rowPrincipal.BOL_CON))
                            dBoletos.Condominios.Add(rowPrincipal.BOL_CON,ABS_Condominio.GetCondominio(rowPrincipal.BOL_CON));
                        _Condominio = dBoletos.Condominios[rowPrincipal.BOL_CON];                        
                    }
                }                
                return _Condominio; 
            }
            set
            {
                //é usado no caso do super
                _Condominio = value;
            }
        }

        /// <summary>
        /// Conta Corrente do boleto
        /// </summary>
        protected CompontesBasicosProc.ContaCorrente _Conta;

        /// <summary>
        /// Conta Corrente do boleto
        /// </summary>
        public CompontesBasicosProc.ContaCorrente Conta
        {
            get
            {
                if (_Conta == null)
                    return (CompontesBasicosProc.ContaCorrente)Condominio.ABSConta;
                else 
                    return _Conta;
            }
            set
            {
                _Conta = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override ABS_ContaCorrente ABSConta
        {
            get
            {
                if (_Conta == null)
                    return Condominio.ABSConta;
                else
                    return _Conta;
            }
            set
            {
                _Conta = (CompontesBasicosProc.ContaCorrente)value;
            }
        }

        /// <summary>
        /// Salva os boleto granvando a justificativa
        /// </summary>
        /// <param name="Obs"></param>
        /// <param name="EmailJuridico">Email para enviar aviso de alteração</param>
        public override void GravaJustificativa(string Obs, string EmailJuridico)
        {
            GravaJustificativa(Obs, null, null, EmailJuridico);
        }

        /// <summary>
        /// Grava Justificativa para alteração do boleto
        /// </summary>
        /// <param name="Justificativa">justificativa</param>
        /// <param name="ValorAcordado">Valor acertado</param>
        /// <param name="DataLimite">Data Acordada</param>
        /// <param name="EmailJuridico">Email para notificação ao jurídico</param>
        public bool GravaJustificativa(string Justificativa, decimal? ValorAcordado = null, DateTime? DataLimite = null, string EmailJuridico = "")
        {
            //Super não grava
            if (rowSBO != null)
                return false;
            string Historico = (rowPrincipal.IsBOLJustificativaNull() ? "" : rowPrincipal.BOLJustificativa + "\r\n\r\n");
            Justificativa = String.Format("{0} - {1}", DateTime.Now, Justificativa);
            rowPrincipal.BOLJustificativa = Historico + Justificativa;
            if (ValorAcordado.HasValue)
            {
                rowPrincipal.BOLValorAcordado = ValorAcordado.Value;
                rowPrincipal.BOLDataLimiteJust = DataLimite.Value;
                //AlteracaoNoRegistroBoleto(); Ao reativar lembrar que nao pode ser chamado aqui porque pode estar dentro de uma transação                      
            };
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos boleto GravaJustificativa - 473", dBoletos.BOLetosTableAdapter);
                dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                rowPrincipal.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }
            if (EmailJuridico != "")
            {
                string Conteudo = string.Format("Condomínio: {0}{1}", Apartamento.Condominio.Nome, Environment.NewLine);
                Conteudo += string.Format("Bloco: {0}{1}", Apartamento.BLOCodigo, Environment.NewLine);
                Conteudo += string.Format("Número: {0}\r\n\r\n", Apartamento.APTNumero);
                Conteudo += string.Format("Número do Boleto: {0}{1}", rowPrincipal.BOL, Environment.NewLine);
                Conteudo += string.Format("Competência: {0}{1}", Competencia, Environment.NewLine);
                Conteudo += string.Format("Vencimeto: {0:dd/MM/yyyy}{1}", rowPrincipal.BOLVencto, Environment.NewLine);
                Conteudo += string.Format("Valor original: {0:n2}{1}", rowPrincipal.BOLValorPrevisto, Environment.NewLine);
                Conteudo += "Tipo: " + rowPrincipal.BOLTipoCRAI;
                Conteudo += "\r\n\r\n\r\n**** Evento:\r\n";
                Conteudo += Justificativa;
                if (Historico != "")
                    Conteudo += "\r\n\r\n\r\n**** Histórico:\r\n" + Historico;
                VirEmail.EmailDireto EmailDireto1 = new VirEmail.EmailDireto();
                EmailDireto1.Enviar(EmailJuridico, Conteudo, "Aviso de evento em boleto");
                //VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(EmailJuridico, Conteudo, "Aviso de evento em boleto");
            }

            return true;
        }

        /// <summary>
        /// Incluir o custo da emissão de segunda via
        /// </summary>
        public override bool IncluicustoSegundaVia(bool QuestionarUsuario)
        {            
            if ((Condominio.CustoSegudaVia > 0) && (!Contemlinha("120002")))
            {
                if (QuestionarUsuario)
                {
                    if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de MessaBox em Servidor de Processos BoletoProc");
                    if (MessageBox.Show("Confirma a cobrança da segunda via ?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                        return false;
                }
                IncluiBOD("120002", "Segunda via " + DateTime.Now, Condominio.CustoSegudaVia, false);
                return true;
            };
            return false;
        }

        /// <summary>
        /// Incluir um BOD em um boleto existente
        /// </summary>
        /// <param name="PLA"></param>
        /// <param name="Mensagem"></param>
        /// <param name="Valor"></param>
        /// <param name="Previsto">se o valor era previsto</param>
        /// <param name="BODAcordo_BOL"></param>
        /// <returns></returns>
        public dBoletos.BOletoDetalheRow IncluiBOD(string PLA, string Mensagem, decimal Valor, bool Previsto, int? BODAcordo_BOL = null)
        {
            return IncluiBOD(PLA, Mensagem, Valor, true, Previsto, null, null, BODAcordo_BOL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PLA"></param>
        /// <param name="Mensagem"></param>
        /// <param name="Valor"></param>
        /// <param name="GravarJustificativa"></param>
        /// <param name="Previsto"></param>
        /// <param name="PAG"></param>
        /// <param name="CTL"></param>
        /// <param name="BODAcordo_BOL"></param>
        /// <returns></returns>
        protected dBoletos.BOletoDetalheRow IncluiBOD(string PLA, string Mensagem, decimal Valor, bool GravarJustificativa, bool Previsto, int? PAG, int? CTL, int? BODAcordo_BOL = null)
        {
            dBoletos DataSet = (dBoletos)rowPrincipal.Table.DataSet;

            //evitar erro se os bods não estiverem carregados
            if ((rowPrincipal.BOLValorPrevisto != 0) && (rowPrincipal.GetBOletoDetalheRows().Length == 0))
            {
                dBoletos.BOletoDetalheTableAdapter.EmbarcaEmTransST();
                dBoletos.BOletoDetalheTableAdapter.FillBOL(DataSet.BOletoDetalhe, rowPrincipal.BOL);
            }

            dBoletos.BOletoDetalheRow rowBOD = DataSet.BOletoDetalhe.NewBOletoDetalheRow();
            rowBOD.BOD_CON = rowPrincipal.BOL_CON;
            if (!rowPrincipal.IsBOL_APTNull())
            {
                rowBOD.BOD_APT = rowPrincipal.BOL_APT;
                rowBOD.BODProprietario = rowPrincipal.BOLProprietario;
            }
            rowBOD.BOD_BOL = rowPrincipal.BOL;
            rowBOD.BOD_PLA = PLA;
            rowBOD.BODComCondominio = (rowPrincipal.BOLTipoCRAI == "C");
            rowBOD.BODCompetencia = rowPrincipal.BOLCompetenciaAno * 100 + rowPrincipal.BOLCompetenciaMes;
            rowBOD.BODPrevisto = Previsto;
            rowBOD.BODData = (rowPrincipal.BOLVencto);
            rowBOD.BODMensagem = Mensagem;
            rowBOD.BODValor = Valor;
            if (CTL.HasValue)
                rowBOD.BOD_CTL = CTL.Value;
            if (BODAcordo_BOL.HasValue)
                rowBOD.BODAcordo_BOL = BODAcordo_BOL.Value;
            DataSet.BOletoDetalhe.AddBOletoDetalheRow(rowBOD);

            if (rowPrincipal.IsBOLPagamentoNull())
            {
                rowPrincipal.BOLValorPrevisto = 0;
                rowPrincipal.BOLValorOriginal = 0;
                foreach (dBoletos.BOletoDetalheRow rowBODdet in rowPrincipal.GetBOletoDetalheRows())
                {
                    if (rowBODdet.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD))
                        continue;
                    rowPrincipal.BOLValorPrevisto += rowBODdet.BODValor;
                    if ((rowBODdet.IsBODPrevistoNull()) || (rowBODdet.BODPrevisto))
                        rowPrincipal.BOLValorOriginal += rowBODdet.BODValor;
                }
            }
            rowPrincipal.BOLExportar = true;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 837", dBoletos.BOletoDetalheTableAdapter);
                dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                if (GravarJustificativa)
                {
                    if (PLA == "120002")
                        GravaJustificativa(string.Format("Incluído valor de segunda via ({0})", EMPTProc1.USUNome), "");
                    else
                        if (rowPrincipal.IsBOLPagamentoNull())
                            GravaJustificativa(String.Format("Incluído item no boleto {0}-{1} ({2})", PLA, Mensagem, EMPTProc1.USUNome), "");
                }
                else
                    SalvarAlteracoesSemRegistrar();
                //SalvarAlteracoes("");

                rowBOD.AcceptChanges();
                if (PAG.HasValue)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("INSERT INTO BODxPAG (BOD, PAG) VALUES (@P1, @P2)", rowBOD.BOD, PAG.Value);
                }
                //exporta na própria thread
                if (rowPrincipal.IsBOLPagamentoNull())
                {
                    //ReExportaTh();
                    GuardaBODsOriginal();
                }
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro: " + e.Message, e);
            }
            return rowBOD;
        }

        /// <summary>
        /// Salva as laterações sem registrar nada. Usar somente na criação de um boleto onde são feitas alterações em um try que contenha a criação e a alteração.
        /// </summary>
        /// <returns></returns>
        public void SalvarAlteracoesSemRegistrar()
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos boleto.cs - 817", dBoletos.BOLetosTableAdapter, dBoletos.BOletoDetalheTableAdapter);
                if (!dBoletos.BOLetosTableAdapter.STTransAtiva())
                    throw new Exception("Uso incorreto da função");
                dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                rowPrincipal.AcceptChanges();
                foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                {
                    dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                    rowBOD.AcceptChanges();
                }
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                throw e;
            }
        }
        

        /// <summary>
        /// Carteira do boleto no banco do condomínio
        /// </summary>
        public int CarteiraBanco
        {
            get
            {
                if (Conta.CNR.HasValue)
                    if (Conta.CNR.Value == 16)
                        return 16;
                switch (Conta.BCO)
                {
                    case 341:
                        return 109;                      
                    case 237:
                        return 9;
                    case 104:
                        return 9; //para a Caixa nao existe este conceito de carteira então adotamos 9 e 6 para simplificar (o certo seria boolean)
                    case 33:
                        return 101;
                    default:
                        throw new NotImplementedException(string.Format("CarteiraBanco não implementada para BCO = {0}",Conta.BCO));
                }                
            }
        }

        

    }
}
