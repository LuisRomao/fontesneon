﻿namespace BoletosProc.Boleto {
    
    
    public partial class dSuperBoleto 
    {
        private dSuperBoletoTableAdapters.SuperBOletoTableAdapter superBOletoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SuperBOleto
        /// </summary>
        public dSuperBoletoTableAdapters.SuperBOletoTableAdapter SuperBOletoTableAdapter
        {
            get
            {
                if (superBOletoTableAdapter == null)
                {
                    superBOletoTableAdapter = new dSuperBoletoTableAdapters.SuperBOletoTableAdapter();
                    superBOletoTableAdapter.TrocarStringDeConexao();
                };
                return superBOletoTableAdapter;
            }
        }

        private dSuperBoletoTableAdapters.SuperBoletoDetalheTableAdapter superBoletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SuperBoletoDetalhe
        /// </summary>
        public dSuperBoletoTableAdapters.SuperBoletoDetalheTableAdapter SuperBoletoDetalheTableAdapter
        {
            get
            {
                if (superBoletoDetalheTableAdapter == null)
                {
                    superBoletoDetalheTableAdapter = new dSuperBoletoTableAdapters.SuperBoletoDetalheTableAdapter();
                    superBoletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return superBoletoDetalheTableAdapter;
            }
        }
    }
}
