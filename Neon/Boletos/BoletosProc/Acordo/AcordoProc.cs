﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FrameworkProc;

namespace BoletosProc.Acordo
{
    /// <summary>
    /// Classe representante do acordo Proc
    /// </summary>
    public class AcordoProc : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Indica se o acordo já foi gerado ou se esta somente na ram
        /// </summary>
        public bool Gerado;

        /// <summary>
        /// ACO
        /// </summary>
        public int ACO;

        /// <summary>
        /// APT
        /// </summary>
        public int APT;

        /// <summary>
        /// Se é do proprietário ou do inquilino
        /// </summary>
        public bool Proprietario;

        /// <summary>
        /// Se é judicial
        /// </summary>
        public bool Judicial;

        /// <summary>
        /// Se o acordo é do jurídico
        /// </summary>
        public bool Juridico;

        private bool EmTeste = true;        

        private int _FRN;

        /// <summary>
        /// Escritorio de advocacia
        /// </summary>
        public int FRN
        {
            get
            {
                return _FRN;
            }
            set
            {
                _FRN = value;
                _FRNEmail = null;
            }
        }

        private string _FRNEmail;

        /// <summary>
        /// e-mail do ADV
        /// </summary>
        protected string FRNEmail
        {
            get
            {
                if (_FRNEmail == null)
                {
                    if (!EmTeste)
                        VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select FRNEmail from fornecedores where FRN = @P1", out _FRNEmail, FRN);
                    else
                        _FRNEmail = "luis@virweb.com.br";
                }
                return _FRNEmail;
            }
        }

        private BoletosProc.Acordo.dAcordo _dAcordo;

        /// <summary>
        /// DataSet interno gerado altomaticamente
        /// </summary>
        public dAcordo dAcordo
        {
            get
            {
                if (_dAcordo == null)
                    _dAcordo = new dAcordo();
                return _dAcordo;
            }
        }

        /// <summary>
        /// Construtor Base
        /// </summary>
        public AcordoProc(bool _EmTeste, EMPTProc _EMPTProc = null)
        {
            EmTeste = _EmTeste;
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
            else
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
        }

        /// <summary>
        /// Construtor para novos acordos
        /// </summary>
        /// <param name="_EmTeste"></param>
        /// <param name="_APT">APT</param> 
        /// <param name="JaCalculado">indica se o acordo já está calculado e nao necessita das informações de atualização dos bolteos (juros, multa etc)</param>
        /// <param name="ComA">Com boletos de acordo</param>
        /// <param name="_EMPTProc"></param>
        public AcordoProc(bool _EmTeste, int _APT, bool JaCalculado, bool ComA, EMPTProc _EMPTProc = null)
            : this(_EmTeste, _EMPTProc)
        {
            this.APT = _APT;
            Gerado = (dAcordo.ACOrdosTableAdapter.Fill(dAcordo.ACOrdos,APT) > 0);
            //Nao vamos re-abrir acordos e sim gerar um segundo
            Gerado = false;

            this.Proprietario = true;
            this.Judicial = this.Juridico = false;            
            CarregaDados(JaCalculado, ComA);
        }

        private void CarregaDados(bool JaCalculado, bool ComA)
        {
            if (Gerado)
            {
                dAcordo.OriginaisTableAdapter.Fill(dAcordo.Originais, APT);
                dAcordo.NovosTableAdapter.Fill(dAcordo.Novos, APT);
            }
            else
            {
                if (ComA)
                    dAcordo.BOLetosTableAdapter.FillByComA(dAcordo.BOLetos, APT);
                else
                    dAcordo.BOLetosTableAdapter.Fill(dAcordo.BOLetos, APT);
                if (!JaCalculado)
                    AtualizaValores(DateTime.Today);
            };
        }

        /// <summary>
        /// Calcula os encargos dos boletos - só implementado no Acordo e não no AcordoProc
        /// </summary>
        /// <param name="Data">Date de cálculo</param>
        protected virtual void AtualizaValores(DateTime Data)
        {
            throw new NotImplementedException("O objeto AcordoProc só trabalha com boletos pré calculados");
        }

        /// <summary>
        /// Gera efetivamente o acordo
        /// </summary>        
        /// <returns></returns>
        public bool Gerar()
        {     
        /*           
            //Boleto.BoletosGrade.RegistraAlteracao(APT);
            string CorpoEmail = "";
            string CorpoEmailJ = "";
            try
            {                
                EMPTProc1.AbreTrasacaoSQL("Boletos Acordo - 449");
                //if (BolsRemoverSeguro != null)
                //    foreach (int nBOLSeg in BolsRemoverSeguro)
                //        BoletosOrig[nBOLSeg].RemoveSeguro(Boleto.Boleto.TipoRemoveSeguro.soremove);
                //CorpoEmail = string.Format("Condomínio: {0}\r\n", dadosAPT.CONNome);
                //CorpoEmail += string.Format("Bloco: {0}\r\n", dadosAPT.BLOCodigo);
                //CorpoEmail += string.Format("Apartamento: {0}\r\n\r\n", dadosAPT.APTNumero);
                //Cria acordo MSSQL
                string ComandoCriaAcordo = "INSERT INTO ACOrdos (ACO_APT, ACOStatus, ACODATAI, ACOProprietario, ACOI_USU, ACOAntigo,ACOJuridico,ACOJudicial,ACOADV_FRN) VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9)";
                object oFRN;
                if ((Juridico) && (FRN != 0) && (FRN != -1))
                    oFRN = FRN;
                else
                    oFRN = DBNull.Value;                
                ACO = VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc(ComandoCriaAcordo, APT, (int)StatusACO.Ativo, DateTime.Now, Proprietario, EMPTProc1.USU , 0, Juridico, Judicial, oFRN);
                if (ACO == -1)
                    throw new Exception("Erro na gravação do acordo");

                //Originais
                //string comandoIncluirACOBOL = "INSERT INTO ACOxBOL  (ACO, BOL, ACOBOLOriginal) VALUES (@P1,@P2,@P3)";
                CorpoEmail += "Boletos originais:\r\n\r\n";
                CorpoEmail += "Número\tVencimento\tValor\r\n";                
                foreach (dAcordo.BOLetosRow rowBoletos in dAcordo.BOLetos)
                {
                    if (rowBoletos.Incluir)
                    {
                        BoletosOrig[rowBoletos.BOL].IncluirEmAcordo(ACO, true);
                        CorpoEmail += string.Format("{0}\t{1:dd/MM/yyyy}\t{2:n2}\r\n", rowBoletos.BOL, rowBoletos.BOLVencto, rowBoletos.BOLValorPrevisto);
                    };
                };
                
                //NOVOS
                int prestacao = 1;
                CorpoEmailJ = CorpoEmail;
                CorpoEmail += "\r\n\r\nNovos Boletos\r\n\r\nNúmero\tVencimento\tValor\tLinha digitável\r\n";
                CorpoEmailJ += "\r\n\r\nNovos Boletos\r\n\r\nNúmero\tVencimento\tValor\tHonorários\tLinha digitável\r\n";
                NovosBoletosGerados = new System.Collections.SortedList();
                ZerarPegaParte();
                Performance.PerformanceST.Registra("Honorarios");
                //conta parcecelas com honorarios
                int nHonorarios = 0;
                for (int k = 0; k < dAcordo.GeraNovos.Count; k++)
                {
                    if (dAcordo.GeraNovos[k].Honorarios > 0)
                        nHonorarios++;
                };

                Performance.PerformanceST.Registra("Parte for");
                for (int k = 0; k < dAcordo.GeraNovos.Count; k++)
                {
                    Performance.PerformanceST.Registra("Parte for");
                    dAcordo.GeraNovosRow rowGeraNovo = dAcordo.GeraNovos[k];
                    Performance.PerformanceST.Registra("Competencia", true);
                    Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(rowGeraNovo.Data.Month, rowGeraNovo.Data.Year);
                    Performance.PerformanceST.StackPop();
                    //GERACAO DO BOLETO                                                                                 
                    System.Collections.ArrayList BODs = new System.Collections.ArrayList();
                    decimal Saldo = rowGeraNovo.Valor;
                    //honorario
                    if (!rowGeraNovo.IsHonorariosNull() && (rowGeraNovo.Honorarios > 0))
                    {
                        Boleto.dBoletos.BOletoDetalheRow novaBODHrow = Boleto.Boleto.GerarBOD(dadosAPT.CON,
                                                                                              APT, Proprietario,
                                                                                              "119000",
                                                                                              false,
                                                                                              Comp,
                                                                                              rowGeraNovo.Data,
                                                                                              string.Format("Honorários: unidade {0}{1} Parc {2}/{3}", dadosAPT.BLOCodigo == "SB" ? "" : dadosAPT.BLOCodigo + "-", dadosAPT.APTNumero, prestacao, nHonorarios),
                                                                                              rowGeraNovo.Honorarios,
                                                                                              false);
                        if (novaBODHrow == null)
                            throw new Exception("Falha na gravação do acordo!!!!");
                        BODs.Add(novaBODHrow.BOD);
                        Saldo -= rowGeraNovo.Honorarios;
                    }
                    while (Saldo > 0)
                    {
                        Performance.PerformanceST.Registra("PegaParteDaOrigem");
                        retParte BIncuir = PegaParteDaOrigem(Saldo, k);
                        Saldo -= BIncuir.ValorEfetivo;
                        Performance.PerformanceST.Registra("GeraBod");
                        Boleto.dBoletos.BOletoDetalheRow novaBODrow = Boleto.Boleto.GerarBOD(dadosAPT.CON, APT, BIncuir.Proprietario, "111000", false, Comp, rowGeraNovo.Data, BIncuir.Descricao, BIncuir.ValorEfetivo, false, null, BIncuir.BOL);
                        if (novaBODrow == null)
                            throw new Exception("Falha na gravação do acordo!!!!");
                        BODs.Add(novaBODrow.BOD);
                    };
                    //Cria Boleto
                    int NovoBOL = 0;
                    if (!rowGeraNovo.IsBOLNull())
                        NovoBOL = rowGeraNovo.BOL;
                    bool ProibirRegistro = (dAcordo.GeraNovos.Count > 3);
                    Performance.PerformanceST.Registra("new Boleto", true);
                    Boletos.Boleto.Boleto NovoBoleto = new Boletos.Boleto.Boleto(Proprietario, Apartamento, "Acordo", Comp, rowGeraNovo.Data, "A", NovoBOL, Boletos.Boleto.StatusBoleto.Acordo, ProibirRegistro, (int[])BODs.ToArray(typeof(int)));
                    Performance.PerformanceST.StackPop();
                    Performance.PerformanceST.Registra("corpo Email");
                    CorpoEmail += NovoBoleto.rowPrincipal.BOL.ToString() + "\t" + NovoBoleto.rowPrincipal.BOLVencto.ToString("dd/MM/yyyy") + "\t" + NovoBoleto.rowPrincipal.BOLValorPrevisto.ToString("n2") + "\t" + NovoBoleto.LinhaDigitavel + "\r\n";
                    CorpoEmailJ += NovoBoleto.rowPrincipal.BOL.ToString() + "\t" + NovoBoleto.rowPrincipal.BOLVencto.ToString("dd/MM/yyyy") + "\t" + NovoBoleto.rowPrincipal.BOLValorPrevisto.ToString("n2") + "\t" + rowGeraNovo.Honorarios.ToString("n2") + "\t" + NovoBoleto.LinhaDigitavel + "\r\n";

                    if (NovoBoleto.Encontrado == false)
                        throw new Exception("Erro ao criar boleto:\r\n" + NovoBoleto.UltimoErro);
                    NovosBoletosGerados.Add(NovoBoleto.rowPrincipal.BOL, NovoBoleto);
                    Performance.PerformanceST.Registra("incluir em acordo");
                    NovoBoleto.IncluirEmAcordo(ACO, false);
                    //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoIncluirACOBOL, ACO, NovoBoleto.rowPrincipal.BOL, false);
                    prestacao++;
                };
                Performance.PerformanceST.Registra("Comit");
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                throw new Exception("Erro no acordo: " + e.Message, e);

            };
            int CON = -1;
            Performance.PerformanceST.Registra("Impresso 1");
            dllImpresso.ImpBoletoAcordo Impresso = new dllImpresso.ImpBoletoAcordo();
            if (NovosBoletosGerados != null)
                foreach (System.Collections.DictionaryEntry DE in NovosBoletosGerados)
                {
                    Boletos.Boleto.Boleto NovoBoleto = (Boletos.Boleto.Boleto)DE.Value;
                    if (CON == -1)
                        CON = NovoBoleto.CON;
                    NovoBoleto.CarregaLinha(Impresso, true, false);
                };
            if ((Botao == dllImpresso.Botoes.Botao.imprimir_frente)
                ||
                (Botao == dllImpresso.Botoes.Botao.PDF_frente)
                ||
                (Botao == dllImpresso.Botoes.Botao.email))
                Impresso.ComRemetente = false;

            Performance.PerformanceST.Registra("Impresso 2");
            switch (Botao)
            {
                case dllImpresso.Botoes.Botao.email:
                    Impresso.CreateDocument();
                    if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Acordo " + ACO.ToString(), Impresso, CorpoEmail, "Acordo " + ACO.ToString()))
                        System.Windows.Forms.MessageBox.Show("E.mail enviado");
                    else
                    {
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                            System.Windows.Forms.MessageBox.Show("Falha no envio");
                        else
                            System.Windows.Forms.MessageBox.Show("Cancelado o envio");
                    };
                    break;
                case dllImpresso.Botoes.Botao.imprimir_frente:
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.botao:
                    Impresso.Apontamento(CON, 71);
                    Impresso.CreateDocument();
                    Impresso.Print();
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                case dllImpresso.Botoes.Botao.PDF_frente:
                    Impresso.CreateDocument();
                    System.Windows.Forms.SaveFileDialog saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
                    saveFileDialog1.DefaultExt = "pdf";
                    if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        Impresso.ExportOptions.Pdf.Compressed = true;
                        Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                        Impresso.ExportToPdf(saveFileDialog1.FileName);
                    }
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    Impresso.CreateDocument();
                    Impresso.ShowPreviewDialog();
                    break;
            };
            if ((Juridico) && (FRNEmail != null) && (FRNEmail != "") && (CorpoEmailJ != ""))
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(FRNEmail, "Boletos Acordo " + ACO.ToString(), Impresso, CorpoEmailJ, "Acordo Gerado");

            NovosBoletosGerados = null;
            Performance.PerformanceST.Relatorio(true, true);*/
            return true;
        }
        
    }

    /// <summary>
    /// Status de acordo
    /// </summary>
    public enum StatusACO
    {
        /// <summary>
        /// Não Gerado
        /// </summary>
        NaoGerado = -1,
        /// <summary>
        /// Acordo em andamento
        /// </summary>
        Ativo = 1,
        /// <summary>
        /// Acordo Terminado
        /// </summary>
        Terminado = 2,
        /// <summary>
        /// Acordo cancelado
        /// </summary>
        Cancelado = 3,
        /// <summary>
        /// Divisao de boleto
        /// </summary>
        Divide = 10
    }
}
