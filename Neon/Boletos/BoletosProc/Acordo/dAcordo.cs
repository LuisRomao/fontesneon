﻿using FrameworkProc;
using System;

namespace BoletosProc.Acordo {


    partial class dAcordo : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construitor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dAcordo(EMPTProc _EMPTProc1)
            : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            EMPTProc1 = _EMPTProc1;
        }

        private static dAcordo dAcordoST;

        /// <summary>
        /// 
        /// </summary>
        public static dAcordo DAcordoST {
            get {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de propriedade estática em Servidor de Processos");
                if(dAcordoST == null)
                    dAcordoST = new dAcordo();
                return dAcordoST;
            }
        }

        #region Table Adapters
        private BoletosProc.Acordo.dAcordoTableAdapters.ACOrdosTableAdapter _ACOrdosTableAdapter;
        /// <summary>
        /// 
        /// </summary>
        public BoletosProc.Acordo.dAcordoTableAdapters.ACOrdosTableAdapter ACOrdosTableAdapter
        {
            get
            {
                if (_ACOrdosTableAdapter == null)
                {
                    _ACOrdosTableAdapter = new BoletosProc.Acordo.dAcordoTableAdapters.ACOrdosTableAdapter();
                    _ACOrdosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return _ACOrdosTableAdapter;
            }
        }

        private BoletosProc.Acordo.dAcordoTableAdapters.BOLetosTableAdapter _BOLetosTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public BoletosProc.Acordo.dAcordoTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (_BOLetosTableAdapter == null)
                {
                    _BOLetosTableAdapter = new BoletosProc.Acordo.dAcordoTableAdapters.BOLetosTableAdapter();
                    _BOLetosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return _BOLetosTableAdapter;
            }
        }

        private BoletosProc.Acordo.dAcordoTableAdapters.NovosTableAdapter _NovosTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public BoletosProc.Acordo.dAcordoTableAdapters.NovosTableAdapter NovosTableAdapter
        {
            get
            {
                if (_NovosTableAdapter == null)
                {
                    _NovosTableAdapter = new BoletosProc.Acordo.dAcordoTableAdapters.NovosTableAdapter();
                    _NovosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return _NovosTableAdapter;
            }
        }

        private BoletosProc.Acordo.dAcordoTableAdapters.OriginaisTableAdapter _OriginaisTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public BoletosProc.Acordo.dAcordoTableAdapters.OriginaisTableAdapter OriginaisTableAdapter
        {
            get
            {
                if (_OriginaisTableAdapter == null)
                {
                    _OriginaisTableAdapter = new BoletosProc.Acordo.dAcordoTableAdapters.OriginaisTableAdapter();
                    _OriginaisTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return _OriginaisTableAdapter;
            }
        }

        private dAcordoTableAdapters.ACOxBOLTableAdapter aCOxBOLTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: ACOxBOL
        /// </summary>
        public dAcordoTableAdapters.ACOxBOLTableAdapter ACOxBOLTableAdapter
        {
            get
            {
                if (aCOxBOLTableAdapter == null)
                {
                    aCOxBOLTableAdapter = new dAcordoTableAdapters.ACOxBOLTableAdapter();
                    aCOxBOLTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return aCOxBOLTableAdapter;
            }
        }

        private dAcordoTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalhe
        /// </summary>
        public dAcordoTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dAcordoTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return bOletoDetalheTableAdapter;
            }
        }
        
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="TotalAcordado"></param>
        /// <param name="Forcar"></param>
        /// <returns></returns>
        public bool DistribuiEncargos(decimal TotalAcordado,bool Forcar)
        {
            decimal TotalOriginal = 0;
            decimal TotalEncargosOriginal = 0;
            BOLetosRow Maior = null;
            foreach (BOLetosRow rowB in BOLetos)
            {
                if (rowB.Incluir)
                {
                    TotalOriginal += rowB.BOLValorPrevisto;
                    rowB.incEncargos = rowB.SubTotal - rowB.BOLValorPrevisto;
                    TotalEncargosOriginal += rowB.incEncargos;
                    if ((Maior == null) || (Maior.incEncargos < rowB.incEncargos))
                        Maior = rowB;
                }
            };
            if (TotalOriginal == 0)
                return false;
            decimal EncargoAcordado = TotalAcordado - TotalOriginal;
            decimal FatorDesconto;
            decimal Erro;
            if ((EncargoAcordado < 0) || (TotalEncargosOriginal == 0))
            {
                FatorDesconto = TotalAcordado / TotalOriginal;
                Erro = TotalAcordado;
                foreach (BOLetosRow rowB in BOLetos)
                {
                    if (rowB.Incluir)
                    {                        
                        rowB.incTotal = System.Math.Round(rowB.incValOriginal * FatorDesconto, 2);
                        rowB.incEncargos = rowB.incTotal - rowB.incValOriginal;
                        Erro -= rowB.incTotal;
                    };

                };
                Maior.incEncargos += Erro;
                Maior.incTotal += Erro;
                return (EncargoAcordado >= 0);
            }            
            FatorDesconto = EncargoAcordado/TotalEncargosOriginal;
            Erro = EncargoAcordado;
            foreach (BOLetosRow rowB in BOLetos)
            {
                if (rowB.Incluir)
                {
                    rowB.incEncargos = System.Math.Round((rowB.SubTotal - rowB.BOLValorPrevisto) * FatorDesconto, 2);
                    rowB.incTotal = rowB.incValOriginal + rowB.incEncargos;
                    Erro -= rowB.incEncargos;
                };
                
            };
            Maior.incEncargos += Erro;
            Maior.incTotal += Erro;
            return true;
        }
    }
}
