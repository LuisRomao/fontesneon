﻿namespace dllbanco
{
    partial class cFrancesinhaCorrecao
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cFrancesinhaCorrecao));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chNConciliados = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dFrancesinhaCorrecaoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dFrancesinhaCorrecao = new dllbanco.dFrancesinhaCorrecao();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colARQ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARQBanco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARQIData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARQNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARL_BOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARL_CCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARLNossoNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARLValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARLStatusN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARL_Data = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARL_BOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARL_CONAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARL_CONConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBotao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSTATUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colARL_CCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLTipoCRAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BotaoDel = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chNConciliados.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrancesinhaCorrecaoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrancesinhaCorrecao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotaoDel)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chNConciliados);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.dateEdit1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1536, 67);
            this.panelControl1.TabIndex = 0;
            // 
            // chNConciliados
            // 
            this.chNConciliados.EditValue = true;
            this.chNConciliados.Location = new System.Drawing.Point(18, 43);
            this.chNConciliados.Name = "chNConciliados";
            this.chNConciliados.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chNConciliados.Properties.Appearance.Options.UseFont = true;
            this.chNConciliados.Properties.Caption = "Não conciliados";
            this.chNConciliados.Size = new System.Drawing.Size(150, 22);
            this.chNConciliados.TabIndex = 4;
            this.chNConciliados.Click += new System.EventHandler(this.checkEdit1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(442, 12);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(135, 41);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "Cadastrar";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(269, 12);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(136, 41);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "Calcular";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(122, 9);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(141, 24);
            this.dateEdit1.TabIndex = 1;
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(18, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(98, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Data Mínima:";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "FranAberta";
            this.gridControl1.DataSource = this.dFrancesinhaCorrecaoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 67);
            this.gridControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.BotaoDel,
            this.repositoryItemImageComboBox1});
            this.gridControl1.Size = new System.Drawing.Size(1536, 732);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dFrancesinhaCorrecaoBindingSource
            // 
            this.dFrancesinhaCorrecaoBindingSource.DataSource = this.dFrancesinhaCorrecao;
            this.dFrancesinhaCorrecaoBindingSource.Position = 0;
            // 
            // dFrancesinhaCorrecao
            // 
            this.dFrancesinhaCorrecao.DataSetName = "dFrancesinhaCorrecao";
            this.dFrancesinhaCorrecao.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.gridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colARQ,
            this.colARQBanco,
            this.colARQIData,
            this.colARQNome,
            this.colARL,
            this.colARL_BOL,
            this.colARL_CCT,
            this.colARLNossoNumero,
            this.colARLValor,
            this.colARLStatusN,
            this.colBOL,
            this.colBOLVencto,
            this.colBOLPagamento,
            this.colARL_Data,
            this.colARL_BOLPagamento,
            this.colARL_CONAgencia,
            this.colARL_CONConta,
            this.colCONCodigo,
            this.colCCTTitulo,
            this.colBotao,
            this.colSTATUS,
            this.colARL_CCD,
            this.colBOLTipoCRAI});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ARLValor", this.colARLValor, "{0:n2}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsView.AllowCellMerge = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PaintStyleName = "Flat";
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colARQIData, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colARQNome, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colARLNossoNumero, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colARQ
            // 
            this.colARQ.FieldName = "ARQ";
            this.colARQ.Name = "colARQ";
            this.colARQ.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARQ.OptionsColumn.ReadOnly = true;
            // 
            // colARQBanco
            // 
            this.colARQBanco.Caption = "Banco";
            this.colARQBanco.FieldName = "ARQBanco";
            this.colARQBanco.Name = "colARQBanco";
            this.colARQBanco.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARQBanco.OptionsColumn.ReadOnly = true;
            this.colARQBanco.Visible = true;
            this.colARQBanco.VisibleIndex = 1;
            this.colARQBanco.Width = 50;
            // 
            // colARQIData
            // 
            this.colARQIData.Caption = "Digitação";
            this.colARQIData.FieldName = "ARQIData";
            this.colARQIData.Name = "colARQIData";
            this.colARQIData.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARQIData.OptionsColumn.ReadOnly = true;
            this.colARQIData.Visible = true;
            this.colARQIData.VisibleIndex = 1;
            this.colARQIData.Width = 83;
            // 
            // colARQNome
            // 
            this.colARQNome.Caption = "Identificação";
            this.colARQNome.FieldName = "ARQNome";
            this.colARQNome.Name = "colARQNome";
            this.colARQNome.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARQNome.OptionsColumn.ReadOnly = true;
            this.colARQNome.Visible = true;
            this.colARQNome.VisibleIndex = 1;
            // 
            // colARL
            // 
            this.colARL.FieldName = "ARL";
            this.colARL.Name = "colARL";
            this.colARL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARL.OptionsColumn.ReadOnly = true;
            // 
            // colARL_BOL
            // 
            this.colARL_BOL.FieldName = "ARL_BOL";
            this.colARL_BOL.Name = "colARL_BOL";
            this.colARL_BOL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARL_BOL.OptionsColumn.ReadOnly = true;
            // 
            // colARL_CCT
            // 
            this.colARL_CCT.FieldName = "ARL_CCT";
            this.colARL_CCT.Name = "colARL_CCT";
            this.colARL_CCT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARL_CCT.OptionsColumn.ReadOnly = true;
            // 
            // colARLNossoNumero
            // 
            this.colARLNossoNumero.Caption = "Nossso Número";
            this.colARLNossoNumero.FieldName = "ARLNossoNumero";
            this.colARLNossoNumero.Name = "colARLNossoNumero";
            this.colARLNossoNumero.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colARLNossoNumero.OptionsColumn.ReadOnly = true;
            this.colARLNossoNumero.Visible = true;
            this.colARLNossoNumero.VisibleIndex = 4;
            this.colARLNossoNumero.Width = 120;
            // 
            // colARLValor
            // 
            this.colARLValor.Caption = "Valor Pago";
            this.colARLValor.DisplayFormat.FormatString = "n2";
            this.colARLValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLValor.FieldName = "ARLValor";
            this.colARLValor.Name = "colARLValor";
            this.colARLValor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARLValor.OptionsColumn.ReadOnly = true;
            this.colARLValor.Visible = true;
            this.colARLValor.VisibleIndex = 7;
            this.colARLValor.Width = 130;
            // 
            // colARLStatusN
            // 
            this.colARLStatusN.FieldName = "ARLStatusN";
            this.colARLStatusN.Name = "colARLStatusN";
            this.colARLStatusN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARLStatusN.OptionsColumn.ReadOnly = true;
            // 
            // colBOL
            // 
            this.colBOL.Caption = "Boleto Baixado";
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBOL.OptionsColumn.ReadOnly = true;
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 8;
            this.colBOL.Width = 121;
            // 
            // colBOLVencto
            // 
            this.colBOLVencto.Caption = "Vencimento";
            this.colBOLVencto.FieldName = "BOLVencto";
            this.colBOLVencto.Name = "colBOLVencto";
            this.colBOLVencto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBOLVencto.OptionsColumn.ReadOnly = true;
            // 
            // colBOLPagamento
            // 
            this.colBOLPagamento.Caption = "Pagamento";
            this.colBOLPagamento.FieldName = "BOLPagamento";
            this.colBOLPagamento.Name = "colBOLPagamento";
            this.colBOLPagamento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBOLPagamento.OptionsColumn.ReadOnly = true;
            // 
            // colARL_Data
            // 
            this.colARL_Data.Caption = "Pagamento";
            this.colARL_Data.FieldName = "ARL_Data";
            this.colARL_Data.Name = "colARL_Data";
            this.colARL_Data.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARL_Data.OptionsColumn.ReadOnly = true;
            this.colARL_Data.Visible = true;
            this.colARL_Data.VisibleIndex = 5;
            // 
            // colARL_BOLPagamento
            // 
            this.colARL_BOLPagamento.Caption = "Crédito";
            this.colARL_BOLPagamento.FieldName = "ARL_BOLPagamento";
            this.colARL_BOLPagamento.Name = "colARL_BOLPagamento";
            this.colARL_BOLPagamento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARL_BOLPagamento.Visible = true;
            this.colARL_BOLPagamento.VisibleIndex = 6;
            // 
            // colARL_CONAgencia
            // 
            this.colARL_CONAgencia.Caption = "Agência";
            this.colARL_CONAgencia.FieldName = "ARL_CONAgencia";
            this.colARL_CONAgencia.Name = "colARL_CONAgencia";
            this.colARL_CONAgencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARL_CONAgencia.OptionsColumn.ReadOnly = true;
            this.colARL_CONAgencia.Visible = true;
            this.colARL_CONAgencia.VisibleIndex = 2;
            this.colARL_CONAgencia.Width = 50;
            // 
            // colARL_CONConta
            // 
            this.colARL_CONConta.Caption = "Conta";
            this.colARL_CONConta.FieldName = "ARL_CONConta";
            this.colARL_CONConta.Name = "colARL_CONConta";
            this.colARL_CONConta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARL_CONConta.OptionsColumn.ReadOnly = true;
            this.colARL_CONConta.Visible = true;
            this.colARL_CONConta.VisibleIndex = 3;
            this.colARL_CONConta.Width = 72;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "Condomínio";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 9;
            this.colCONCodigo.Width = 81;
            // 
            // colCCTTitulo
            // 
            this.colCCTTitulo.Caption = "Conta";
            this.colCCTTitulo.FieldName = "CCTTitulo";
            this.colCCTTitulo.Name = "colCCTTitulo";
            this.colCCTTitulo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colCCTTitulo.OptionsColumn.ReadOnly = true;
            this.colCCTTitulo.Visible = true;
            this.colCCTTitulo.VisibleIndex = 10;
            this.colCCTTitulo.Width = 154;
            // 
            // colBotao
            // 
            this.colBotao.Caption = "gridColumn1";
            this.colBotao.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBotao.Name = "colBotao";
            this.colBotao.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBotao.OptionsColumn.ShowCaption = false;
            this.colBotao.Visible = true;
            this.colBotao.VisibleIndex = 11;
            this.colBotao.Width = 30;
            // 
            // colSTATUS
            // 
            this.colSTATUS.Caption = "Status";
            this.colSTATUS.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colSTATUS.FieldName = "STATUS";
            this.colSTATUS.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSTATUS.Name = "colSTATUS";
            this.colSTATUS.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colSTATUS.Visible = true;
            this.colSTATUS.VisibleIndex = 0;
            this.colSTATUS.Width = 111;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colARL_CCD
            // 
            this.colARL_CCD.FieldName = "ARL_CCD";
            this.colARL_CCD.Name = "colARL_CCD";
            this.colARL_CCD.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colBOLTipoCRAI
            // 
            this.colBOLTipoCRAI.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI.Name = "colBOLTipoCRAI";
            this.colBOLTipoCRAI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            // 
            // BotaoDel
            // 
            this.BotaoDel.AutoHeight = false;
            this.BotaoDel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.BotaoDel.Name = "BotaoDel";
            this.BotaoDel.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotaoDel.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotaoDel_ButtonClick);
            // 
            // cFrancesinhaCorrecao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cFrancesinhaCorrecao";
            this.Size = new System.Drawing.Size(1536, 799);
            this.Load += new System.EventHandler(this.cFrancesinhaCorrecao_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chNConciliados.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrancesinhaCorrecaoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrancesinhaCorrecao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotaoDel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.BindingSource dFrancesinhaCorrecaoBindingSource;
        private dFrancesinhaCorrecao dFrancesinhaCorrecao;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colARQ;
        private DevExpress.XtraGrid.Columns.GridColumn colARQBanco;
        private DevExpress.XtraGrid.Columns.GridColumn colARQIData;
        private DevExpress.XtraGrid.Columns.GridColumn colARQNome;
        private DevExpress.XtraGrid.Columns.GridColumn colARL;
        private DevExpress.XtraGrid.Columns.GridColumn colARL_BOL;
        private DevExpress.XtraGrid.Columns.GridColumn colARL_CCT;
        private DevExpress.XtraGrid.Columns.GridColumn colARLNossoNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colARLValor;
        private DevExpress.XtraGrid.Columns.GridColumn colARLStatusN;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colARL_Data;
        private DevExpress.XtraGrid.Columns.GridColumn colARL_BOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colARL_CONAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colARL_CONConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colBotao;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotaoDel;
        private DevExpress.XtraGrid.Columns.GridColumn colSTATUS;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.CheckEdit chNConciliados;
        private DevExpress.XtraGrid.Columns.GridColumn colARL_CCD;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI;
    }
}
