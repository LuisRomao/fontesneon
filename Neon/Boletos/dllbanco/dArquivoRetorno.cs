﻿namespace dllbanco {
    
    
    public partial class dArquivoRetorno 
    {
        private dArquivoRetornoTableAdapters.ARQuivosTableAdapter aRQuivosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ARQuivos
        /// </summary>
        public dArquivoRetornoTableAdapters.ARQuivosTableAdapter ARQuivosTableAdapter
        {
            get
            {
                if (aRQuivosTableAdapter == null)
                {
                    aRQuivosTableAdapter = new dArquivoRetornoTableAdapters.ARQuivosTableAdapter();
                    aRQuivosTableAdapter.TrocarStringDeConexao();
                };
                return aRQuivosTableAdapter;
            }
        }
    }
}
