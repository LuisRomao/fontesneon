/*
MR - 12/11/2014 10:00 -           - Processa retorno de pagamento de tributos eletronico (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 27/03/2015 09:30             - Incluido o caminho do servidor de retorno para SA (Altera��es indicadas por *** MRC - INICIO (27/03/2015 09:30) ***)
MR - 21/03/2016 20:00 -           - Tratamento Arquivo de Cobran�a Itau (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
*/

using System;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using dllImpostoNeon;
using VirEnumeracoes;
using dllImpostoNeonProc;
using EDIBoletos;
using DevExpress.XtraEditors;

namespace dllbanco
{   
    /// <summary>
    /// 
    /// </summary>
    public class IdentificaArquivo
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_SaidaTela">Saida de tela. Pode ser null</param>
        /// <param name="_Gau">Gauge</param>
        public IdentificaArquivo(MemoEdit _SaidaTela, ProgressBarControl _Gau)
        {
            SaidaTela = _SaidaTela;
            Gau = _Gau;
        }

        private string _PathTMP = "";

        private ProgressBarControl Gau;

        private static string emailFilial{
            get{
                #if (DEBUG)
                return "luis@virweb.com.br";
                #endif
                return "eduardo@neonimoveis.com.br";
            }
        }

        private string PathTMP
        {
            get
            {
                if (_PathTMP == "")
                {
                    _PathTMP = String.Format(@"{0}\TMP\", Application.StartupPath);
                    if (!Directory.Exists(_PathTMP))
                        Directory.CreateDirectory(_PathTMP);
                };
                return _PathTMP;
            }
        }
            
        private static IdentificaArquivo _IdentificaArquivoST;

        /// <summary>
        /// Instancia est�tica
        /// </summary>
        public static IdentificaArquivo IdentificaArquivoST {
            get {
                if (_IdentificaArquivoST == null)
                    _IdentificaArquivoST = new IdentificaArquivo(null,null);
                return _IdentificaArquivoST;
            }
        }

        private static dllbanco.Francesinha _Fr;

        /// <summary>
        /// Memo para gravar retornos
        /// </summary>
        public DevExpress.XtraEditors.MemoEdit SaidaTela;
        /// <summary>
        /// Log dos retornos; Se RetornoNoLog estivar ativado
        /// </summary>
        public string Log;
        /// <summary>
        /// Faz com que o retorno seja em um log e n�o na tela.
        /// </summary>
        public bool RetornoNoLog;
        
        private static dllbanco.Francesinha Fr
        {
            get
            {
                if (_Fr == null)
                    _Fr = new Francesinha();
                return _Fr;
            }
        }

        //*** MRC - INICIO - PAG-FOR (2) ***    
        private static dllbanco.PagamentoEletronico _Pe;

        private static dllbanco.PagamentoEletronico Pe
        {
            get
            {
                if (_Pe == null)
                    _Pe = new PagamentoEletronico();
                return _Pe;
            }
        }
        //*** MRC - FIM - PAG-FOR (2) ***

        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***   
        private static dllbanco.TributoEletronico _Te;

        private static dllbanco.TributoEletronico Te
        {
            get
            {
                if (_Te == null)
                    _Te = new TributoEletronico();
                return _Te;
            }
        }
        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) *** 

        private static dllbanco.Conciliacao Cn
        {
            get
            {
                return Conciliacao.ConciliacaoSt;
            }
        }

        private OpenFileDialog _OpenF;

        private OpenFileDialog OpenF
        {
            get
            {
                if (_OpenF == null)
                {
                    _OpenF = new OpenFileDialog();
                    _OpenF.Multiselect = true;
                    _OpenF.Title = "Arquivos retorno";
                }
                return _OpenF;
            }
        }

        private void ImprimeResposta(string Mem) {
            if(Mem == "")
                Mem = "--------------------------------------------------------\r\n";
            else
                Mem = string.Format("{0} {1}{2}",
                                    DateTime.Now.ToString(),
                                    Mem,
                                    (Mem.EndsWith("\r\n")) ? "" : "\r\n");
            if (RetornoNoLog)
                Log += Mem;
            else
            {
                if (SaidaTela == null)
                    MessageBox.Show(Mem);
                else
                    SaidaTela.Text += Mem;
            }
        }

        /// <summary>
        /// Diretorio de armazenamento dos arquivos retorno
        /// </summary>
        public string ServidorRetorno {
            get {
#if (DEBUG)
                VirEmailNeon.EmailDiretoNeon.EmalST.EmTeste();
                return @"C:\lixo\CAIXA001\";
                
#endif
                if (Framework.DSCentral.EMP == 1)
                    return @"\\NEON02\Retornos\";
                //*** MRC - INICIO (27/03/2015 09:30) ***
                else if (Framework.DSCentral.EMP == 3)
                    return @"\\NEON03\NewsccSA\Retorno\";
                //*** MRC - TERMINO (27/03/2015 09:30) ***
                else
                    return @"c:\retorno\";
            }
        }

        System.Collections.ArrayList ArquivosEnviar;

        /// <summary>
        /// Carrega todos os arquivos na data
        /// </summary>
        /// <param name="data">data</param>
        public void ArquivosData(DateTime data) {
            ArquivosEnviar = new System.Collections.ArrayList();
            //LimparDiretorio(TMPdirEnviar);
            ArquivosData(ServidorRetorno + @"Bradepag",data,data == DateTime.Today);
            ArquivosData(ServidorRetorno + @"Itauplus", data, data == DateTime.Today);
            ArquivosData(ServidorRetorno + @"UNIBANCO", data, false);
            ArquivosData(ServidorRetorno + @"CAIXA", data, false);
            if(Framework.DSCentral.EMP == 1)
              if (ArquivosEnviar.Count > 0)
               {
                string NomeArquivoZip = data.ToString("ddMMyyyy") + ".zip";
                Zipar(NomeArquivoZip);
                if (VirEmailNeon.EmailDiretoNeon.EmalST.EnviarArquivos(emailFilial, "Arquivos retorno do banco", "Arquivos retorno do banco", PathTMP + NomeArquivoZip))
                    ImprimeResposta("E.mail enviado");
                else
                    ImprimeResposta("Falha no envio do e.mail:"+VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro);
               };
            ImprimeResposta("\r\n");
            ImprimeResposta("\r\nTERMINADO\r\n");
            //dllCNAB.CNAB240 x;
            //x.Relatorio()
        }

        /*
        private void LimparDiretorio(string TMPdir) {
            MessageBox.Show("dirtorio:"+TMPdir);
            if (System.IO.Directory.Exists(TMPdir))
                foreach (string nome in System.IO.Directory.GetFiles(TMPdir))
                    System.IO.File.Delete(nome);
            else
            {
                MessageBox.Show("criar:" + TMPdir);
                System.IO.Directory.CreateDirectory(TMPdir);
            }
        }

         */
         /*
        public void TESTE() {
            ArquivosEnviar = new System.Collections.ArrayList();
            string Path = ServidorRetorno + @"Bradepag";           
                string[] Arquivos = System.IO.Directory.GetFiles(Path, "*.*", SearchOption.TopDirectoryOnly);
                foreach (string Candidato in Arquivos)
                {
                    System.IO.FileInfo InfoCandidato = new FileInfo(Candidato);
                    if (InfoCandidato.CreationTime.Date == DateTime.Today)
                    {
                                                   
                                ArquivosEnviar.Add(Candidato);
                        
                    }
                    
                };

            
            _PathTMP = "c:\\";
            Zipar("TESTE.zip");
        }*/

        private void Zipar(string ArquivoZIP) {
            try
            {
                //ICSharpCode.SharpZipLib.Zip.FastZip Fzip = new FastZip();
                //Directory.CreateDirectory("C:\\lixo\\Teste");
                //foreach (string file in ArquivosEnviar)
                //    File.Copy(file, "C:\\lixo\\Teste\\" + Path.GetFileName(file));
                //Fzip.CreateZip("C:\\Teste1.zip", "C:\\lixo\\Teste", false, "*.*");
                using (ZipOutputStream s = new ZipOutputStream(File.Create(PathTMP + ArquivoZIP)))
                {                    
                    byte[] buffer = new byte[4096];
                    foreach (string file in ArquivosEnviar)
                    {
                        ZipEntry entry = new ZipEntry(System.IO.Path.GetFileName(file));
                        entry.DateTime = DateTime.Now;
                        s.PutNextEntry(entry);
                        using (FileStream fs = File.OpenRead(file))
                        {
                            int sourceBytes;
                            do
                            {
                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                s.Write(buffer, 0, sourceBytes);
                            } while (sourceBytes > 0);
                        }
                    }                    
                    s.Finish();
                    s.Close();
                }
            }
            catch 
            {
                ImprimeResposta("Erro ao abrir arquivo ZIP:" + ArquivoZIP);
            }                        
        }
        

        private string peganomeTMP() {
            int i = 0;
            
            while (true)
            {
                
                string nome = PathTMP + DateTime.Now.ToString("mmssffff") + i.ToString()+".$$$";
                if (!File.Exists(nome))
                    return nome;
                i++;
            }
        }

        private void unzipar(string ArquivoZIP,string Path,DateTime data) {
            int size = 2048;
            byte[] buf = new byte[2048];
            bool OK = true;
            using (ZipInputStream s = new ZipInputStream(File.OpenRead(ArquivoZIP)))
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null){                                                                           
                    string Tipo="";
                    string NomeTemporario = peganomeTMP();
                    
                    using (FileStream streamWriter = File.Create(NomeTemporario))
                    {
                        while (true)
                        {
                                                                    
                                size = s.Read(buf, 0, buf.Length);
                                if (size > 0)
                                {
                                    if (Tipo == "")
                                    {
                                        Char[] bTipo = new Char[11];
                                        for (int i = 0; i < 11; i++)
                                        {
                                            Char Novo = (Char)buf[i + 11];
                                            if(Char.IsLetterOrDigit(Novo) || Char.IsWhiteSpace(Novo))
                                                Tipo += Novo;
                                        };
                                        Tipo = Tipo.Trim();
                                        for (int i = 0; i < 6; i++)
                                        {
                                            bTipo[i] = (Char)buf[i + 94];
                                            
                                        };
                                        string dia = "";
                                        string mes = "";
                                        string ano = "";
                                        dia += bTipo[0];
                                        dia += bTipo[1];
                                        mes += bTipo[2];
                                        mes += bTipo[3];
                                        ano += bTipo[4];
                                        ano += bTipo[5];
                                        
                                        try
                                        {
                                            int Idia = int.Parse(dia);
                                            int Imes = int.Parse(mes);
                                            int Iano = int.Parse(ano);
                                            data = new DateTime(Iano, Imes, Idia);
                                        }
                                        catch { }
                                        if (Tipo == "")
                                            Tipo = "NaoIdentificado";
                                    }
                                    streamWriter.Write(buf, 0, size);
                                }
                                else
                                    break;                                
                            }
                        };
                        string NomeDefinitivo = string.Format("{0}\\{1:ddMMyyyy}{2}.txt", Path, data, Tipo);
                        int Contador = 1;
                        while (File.Exists(NomeDefinitivo))
                            NomeDefinitivo = string.Format("{0}\\{1:ddMMyyyy}{2}{3}.txt", Path, data, Tipo,Contador++);
                        File.Move(NomeTemporario, NomeDefinitivo);
                        try
                        {                        
                            ProcessaArquivo(NomeDefinitivo);
                        }
                        catch
                        {
                            OK = false;
                        };
                    
                }
            };
            if (OK)
                System.IO.File.Delete(ArquivoZIP);
            return; 
        }


        private void ArquivosData(string Path,DateTime data,bool Enviar)
        {                                   
            if (System.IO.Directory.Exists(Path))
            {
                string[] Arquivos = System.IO.Directory.GetFiles(Path, "*.*", SearchOption.TopDirectoryOnly);
                foreach (string Candidato in Arquivos)
                {
                    System.IO.FileInfo InfoCandidato = new FileInfo(Candidato);
                    if (InfoCandidato.CreationTime.Date == data)
                    {
                        if (InfoCandidato.Extension.ToUpper() == ".ZIP")                                                   
                            unzipar(Candidato, Path, data);                        
                        else
                        {
                            ProcessaArquivo(Candidato);
                            if (Enviar)                            
                                ArquivosEnviar.Add(Candidato);                                
                        }
                        Application.DoEvents();
                    }
                    if ((data == DateTime.Today) && (InfoCandidato.CreationTime.Date < data.AddMonths(-3)))
                        System.IO.File.Delete(Candidato);
                };
                
            }
            else
                ImprimeResposta("Diret�rio n�o encontrado:"+Path);
        }

        /// <summary>
        /// Abre di�logo para selecionar arquivo
        /// </summary>
        public void prompt()
        {
            if (OpenF.ShowDialog() == DialogResult.OK)
            {               
                foreach (string Arquivo in OpenF.FileNames)
                {
                    ProcessaArquivo(Arquivo);
                    ImprimeResposta("");
                    Application.DoEvents();
                }
            }
            ImprimeResposta("Terminado");
        }

        
        /// <summary>
        /// Tipos de arquivo
        /// </summary>
        public enum TipoArquivo {
            /// <summary>
            /// FEBRABAN CNAB240
            /// </summary>
            FEBRABANExtrato,
            /// <summary>
            /// 
            /// </summary>
            FEBRABANCobranca,
            /// <summary>
            /// COBRANCA
            /// </summary>
            COBRANCA,
            /// <summary>
            /// EXTRATO BRADESCO
            /// </summary>
            EXTRATO,
            /// <summary>
            /// EXTRATO ITAU
            /// </summary>
            EXTRATOItau,
            /// <summary>
            /// GPS
            /// </summary>
            GPS,
            /// <summary>
            /// DARF
            /// </summary>
            DARF,
            /// <summary>
            /// INVALIDO
            /// </summary>
            INVALIDO,
            /// <summary>
            /// JACARREGADO
            /// </summary>
            JACARREGADO,
            /// <summary>
            /// COBRANCA BRADESCO
            /// </summary>
            COBRANCABradesco,
            //*** MRC - INICIO - PAG-FOR (2) ***    
            /// <summary>
            /// PAGAMENTO ELETR�NICO (Bradesco)
            /// </summary>
            PAGAMENTOELETRONICOBradesco,
            //*** MRC - FIM - PAG-FOR (2) ***
            //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***    
            /// <summary>
            /// TRIBUTO ELETR�NICO (Bradesco)
            /// </summary>
            TRIBUTOELETRONICOBradesco,
            //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
            //*** MRC - INICIO (21/03/2016 20:00) ***
            /// <summary>
            /// COBRANCA ITAU
            /// </summary>
            COBRANCAItau,
            //*** MRC - TERMINO (21/03/2016 20:00) ***
        }

        /// <summary>
        /// Identifica o arquivo
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <param name="cnab"></param>
        /// <param name="BoletoRetornoBra"></param>
        /// <param name="BoletoRetornoItau"></param>
        /// <param name="ChequeRetornoBra"></param>
        /// <param name="TributoRetornoBra"></param>
        /// <param name="Erro"></param>
        /// <param name="ChecarCarga"></param>
        /// <returns></returns>
        public TipoArquivo IdentificaTipo(string Arquivo,
                                          out dllCNAB.CNAB240 cnab,
                                          out BoletoRetornoBra BoletoRetornoBra,
                                          out BoletoRetornoItau BoletoRetornoItau,
                                          out ChequeRetornoBra ChequeRetornoBra,
                                          out TributoRetornoBra TributoRetornoBra,
                                          out string Erro,
                                          bool ChecarCarga = true)
        {
            cnab = null;
            BoletoRetornoBra = null;
            BoletoRetornoItau = null;
            ChequeRetornoBra = null;
            TributoRetornoBra = null;
            Erro = "";
            TipoArquivo TipoArq = TipoArquivo.INVALIDO;
            using (StreamReader Entrada = new StreamReader(Arquivo, Encoding.GetEncoding(1252)))
            {
                string Amostra = Entrada.ReadLine();
                if (Amostra == null)
                {
                    Erro = String.Format("Arquivo '{0}' com comprimento zero", Arquivo);
                    return TipoArquivo.INVALIDO;
                }
                if (ChecarCarga)
                {
                    //Legado - antes de 26/05/2015 o cabe�alho dos arquivos era gravado com os espa�os
                    object oData = dFrancesinha.dFrancesinhaSTA.ARQuivosTableAdapter.BuscaARQ(Amostra);
                    if (oData == null)
                        oData = dFrancesinha.dFrancesinhaSTA.ARQuivosTableAdapter.BuscaARQ(Amostra.Replace(" ", ""));
                    if (oData != null)
                    {
                        Erro = String.Format("Arquivo '{0}' j� processado em {1}", Arquivo, (DateTime)oData);
                        return TipoArquivo.JACARREGADO;
                    }
                }
                if (Amostra.Length == 240)
                {
                    cnab = new dllCNAB.CNAB240();
                    if (cnab.Carrega(Arquivo))
                    {
                        if (cnab.LotesCobranca.Count != 0)
                            TipoArq = TipoArquivo.FEBRABANCobranca;
                        else
                            TipoArq = TipoArquivo.FEBRABANExtrato;
                    }
                    else
                    {
                        Erro = cnab.UltimoErro.Message;
                        TipoArq = TipoArquivo.INVALIDO;
                    }
                }
                else if ((Amostra.Length == 400) && (Amostra.Substring(0, 19) == "02RETORNO01COBRANCA") && (Amostra.Substring(76, 3) == "237"))
                {
                    BoletoRetornoBra = new BoletoRetornoBra();
                    if (BoletoRetornoBra.Carrega(Arquivo))
                        TipoArq = TipoArquivo.COBRANCABradesco;
                    else
                    {
                        Erro = BoletoRetornoBra.UltimoErro.Message;
                        TipoArq = TipoArquivo.INVALIDO;
                    }
                }
                else if ((Amostra.Length == 400) && (Amostra.Substring(0, 19) == "02RETORNO01COBRANCA") && (Amostra.Substring(76, 3) == "341"))
                {
                    BoletoRetornoItau = new BoletoRetornoItau();
                    if (BoletoRetornoItau.Carrega(Arquivo))
                        TipoArq = TipoArquivo.COBRANCAItau;
                    else
                    {
                        Erro = BoletoRetornoItau.UltimoErro.Message;
                        TipoArq = TipoArquivo.INVALIDO;
                    }
                }
                else if ((Amostra.Length == 400) && (Amostra.Substring(0, 19) == "02RETORNO01COBRANCA"))
                    TipoArq = TipoArquivo.COBRANCA;
                else if ((Amostra.Length == 500) && (System.IO.Path.GetFileName(Arquivo).Substring(0, 2) == "PG"))
                {
                    ChequeRetornoBra = new ChequeRetornoBra();
                    if (ChequeRetornoBra.Carrega(Arquivo))
                    {
                        TipoArq = TipoArquivo.PAGAMENTOELETRONICOBradesco;
                    }
                    else
                    {
                        Erro = ChequeRetornoBra.UltimoErro.Message;
                        TipoArq = TipoArquivo.INVALIDO;
                    }
                }
                else if ((Amostra.Length == 1000) && (System.IO.Path.GetFileName(Arquivo).Substring(0, 2) == "PT"))
                {
                    TributoRetornoBra = new TributoRetornoBra();
                    if (TributoRetornoBra.Carrega(Arquivo))
                    {
                        TipoArq = TipoArquivo.TRIBUTOELETRONICOBradesco;
                    }
                    else
                    {
                        Erro = TributoRetornoBra.UltimoErro.Message;
                        TipoArq = TipoArquivo.INVALIDO;
                    }
                }
                else if ((Amostra.Length == 200) && (Amostra.Substring(11, 11) == "EXTRATO C/C"))
                {
                    if (Amostra.Substring(76, 3) == "341")
                        TipoArq = TipoArquivo.EXTRATOItau;
                    else
                        TipoArq = TipoArquivo.EXTRATO;
                }
                else if ((Amostra.Length == 463) && (Amostra.Substring(183, 1) == "7"))
                    TipoArq = TipoArquivo.GPS;
                else if ((Amostra.Length == 463) && (Amostra.Substring(183, 1) == "2"))
                    TipoArq = TipoArquivo.DARF;

            };
            return TipoArq;
        }

        /// <summary>
        /// Processa o arquivo
        /// </summary>
        /// <param name="Arquivo">Nome do arquivo</param>
        /// <returns></returns>
        public bool ProcessaArquivo(string Arquivo)
        {
            string Erro;
            //string cabecalhoperdido = "";
            ImprimeResposta(String.Format("Arquivo:{0}", Arquivo));            
            dllCNAB.CNAB240 cnab;            
            BoletoRetornoBra BoletoRetornoBra;
            BoletoRetornoItau BoletoRetornoItau;            
            ChequeRetornoBra ChequeRetornoBra;            
            TributoRetornoBra TributoRetornoBra;
            TipoArquivo TipoArq = IdentificaTipo(Arquivo, out cnab, out BoletoRetornoBra, out BoletoRetornoItau, out ChequeRetornoBra, out TributoRetornoBra, out Erro);            
            switch (TipoArq)
            {                
                case TipoArquivo.COBRANCABradesco:
                    if (!Fr.Carregar(BoletoRetornoBra))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", Fr.Erro));
                    break;
                case TipoArquivo.COBRANCAItau:
                    if (!Fr.Carregar(BoletoRetornoItau))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", Fr.Erro));
                    break;             
                case TipoArquivo.COBRANCA:                
                    if (!Fr.Carregar(Arquivo))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", Fr.Erro));
                    break;                
                case TipoArquivo.PAGAMENTOELETRONICOBradesco:
                    if (!Pe.Carregar(ChequeRetornoBra))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", Pe.Erro));
                    else
                    {
                        string strRetorno = Pe.ProcessarDados();
                        if (strRetorno != "")
                            ImprimeResposta(String.Format("Falha:{0}\r\n", strRetorno));
                    }
                    break;                
                case TipoArquivo.TRIBUTOELETRONICOBradesco:
                    if (!Te.Carregar(TributoRetornoBra))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", Pe.Erro));
                    else
                    {
                        string strRetorno = Te.ProcessarDados();
                        if (strRetorno != "")
                            ImprimeResposta(String.Format("Falha:{0}\r\n", strRetorno));
                    }
                    break;                
                case TipoArquivo.FEBRABANCobranca:
                    if (!Fr.Carregar(cnab))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", Fr.Erro));
                    else
                    {
                        ImprimeResposta("ok");
                        return true;
                    }
                    break;
                case TipoArquivo.FEBRABANExtrato:
                    if (cnab.Header.Banco == 237)
                    {
                        ImprimeResposta("Arquivo CNAB240 ignorado para Bradesco\r\n");
                        return false;
                    }
                    CarregaExtrato.carga carga1 = new CarregaExtrato.carga();
                    if (!carga1.Carregar(cnab, Gau))//, Arquivo, TipoArq,Gau))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", carga1.Erro));
                    else
                    {
                        ImprimeResposta("ok");
                        return true;
                    }
                    break;
                case TipoArquivo.EXTRATO:
                case TipoArquivo.EXTRATOItau:
                    CarregaExtrato.carga carga2 = new CarregaExtrato.carga();
                    if (!carga2.Carregar(Arquivo, Gau))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", carga2.Erro));
                    else
                    {
                        ImprimeResposta("ok");
                        return true;
                    }
                    /*
                    Cn.CabecalhoPerdido = cabecalhoperdido;
                    if (!Cn.Carregar(Arquivo, TipoArq))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", Cn.Erro));
                    */
                    break;
                case TipoArquivo.GPS:
                    if (!dGPS.dGPSSt.TratarRetorno(Arquivo, TipoImposto.INSS))
                        ImprimeResposta(String.Format("Falha:{0}\r\n", dGPS.dGPSSt.Ultimoerro));
                    else
                        return true;
                    break;
                case TipoArquivo.DARF:
                    if (!dGPS.dGPSSt.TratarRetorno(Arquivo, TipoImposto.PIS))   
                        ImprimeResposta(String.Format("Falha:{0}\r\n", dGPS.dGPSSt.Ultimoerro));
                    break;
                case TipoArquivo.INVALIDO:
                    ImprimeResposta("Arquivo n�o identificado\r\n");
                    if((Erro != null) && (Erro != ""))
                        ImprimeResposta(string.Format("-----\r\n{0}\r\n-----\r\n",Erro));                    
                    break;
                case TipoArquivo.JACARREGADO:
                    ImprimeResposta(String.Format("{0}\r\n", Erro));
                    break;
            }
            return false;
        }
    }
}
