/*
MR - 24/04/2014 12:00 -           - Tratamento de Status (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (24/04/2014 12:00) ***)
MR - 15/05/2014 11:45 - 13.2.9.0  - Tratamento de Status (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (15/05/2014 11:45) ***)
MR - 01/07/2014 19:00 - 13.2.9.22 - Alterado o endere�o de e-mail para notifica��o de retorno n�o homologado de vcommerce (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (01/07/2014 19:00) ***)
MR - 01/07/2014 19:00 - 14.1.4.1  - Tratamento de Status para Cancelamento (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (14/07/2014 15:00) ***)
MR - 06/08/2014 13:00 -           - Registro de Status de Insconsist�ncia no status do cheque, exceto quando o mesmo j� esta cancelado (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***)
MR - 12/08/2014 10:00 - 14.1.4.22 - Apontamento de impress�o de comprovante de pagamento eletr�nico autom�tico (Altera��es indicadas por *** MRC - INICIO (12/08/2014 10:00) ***)
MR - 15/08/2014 12:00 -           - Inclus�o de observa��o no cheque mesmo em caso de retorno de status n�o homologado (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (15/08/2014 12:00) ***)
MR - 10/12/2014 12:00 -           - Inclus�o do tratamento de Status GO e GP como inconsistente especifico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (10/12/2014 12:00) ***)
MR - 26/12/2014 10:00 -           - Inclus�o do tratamento de Status FL como inconsistente especifico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (26/12/2014 10:00) ***)
MR - 28/04/2016 12:00 -           - Inclus�o do tratamento de Status FH, LG, KR e AA como inconsistente especifico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (28/04/2016 12:00) ***)
*/

using System;
using dllCheques;
using Framework;
using CompontesBasicosProc;
using VirEnumeracoesNeon;

namespace dllbanco
{
    /// <summary>
    /// Pagamento Eletr�nico
    /// </summary>
    public class PagamentoEletronico
    {
        /// <summary>
        /// �ltimo erro
        /// </summary>
        public string Erro;

        private dPagamentoEletronico _dPagamentoEletronico;

        /*
        /// <summary>
        /// dPagamentoEletronico
        /// </summary>
        private dPagamentoEletronico dPagamentoEletronico
        {
           get{
              if (_dPagamentoEletronico == null)
               {
                  _dPagamentoEletronico = new dPagamentoEletronico();
                  _dPagamentoEletronico.ARQuivosTableAdapter.Fill(_dPagamentoEletronico.ARQuivos);
               }
              return _dPagamentoEletronico;
           }
        }*/

        private dPagamentoEletronico dPagamentoEletronico { get => _dPagamentoEletronico ?? (_dPagamentoEletronico = new dPagamentoEletronico()); }

        //private dPagamentoEletronico.ARquivoLidoDataTable ARquivoLidoDataTable;

        /// <summary>
        /// Retorno Pagamento Eletr�nico Bradesco
        /// </summary>
        /// <param name="ChequeRetornoBra"></param>
        /// <returns></returns>
        public bool Carregar(EDIBoletos.ChequeRetornoBra ChequeRetornoBra)
        {
            dPagamentoEletronico.ARquivoLido.Clear();
            Erro = "";
            //ARquivoLidoDataTable = dPagamentoEletronico.ARquivoLido;

            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco PagamentoEletronico - 49", dPagamentoEletronico.ARQuivosTableAdapter,
                                                                                       dPagamentoEletronico.ARquivoLidoTableAdapter);
            try
            {
                dPagamentoEletronico.ARQuivosRow novalinha = dPagamentoEletronico.ARQuivos.NewARQuivosRow();
                novalinha.ARQBanco = 237;
                novalinha.ARQIData = DateTime.Now;
                novalinha.ARQNome = ChequeRetornoBra.Header.LinhaLida;
                novalinha.ARQNomeArquivo = System.IO.Path.GetFileName(ChequeRetornoBra.NomeArquivo);
                dPagamentoEletronico.ARQuivos.AddARQuivosRow(novalinha);
                dPagamentoEletronico.ARQuivosTableAdapter.Update(novalinha);
                novalinha.AcceptChanges();

                foreach (EDIBoletos.RegistrosOBJCheque.regTransacao_1_2 che in ChequeRetornoBra.Cheques)
                {
                    dPagamentoEletronico.ARquivoLidoRow rowARL = dPagamentoEletronico.ARquivoLido.NewARquivoLidoRow();
                    rowARL.ALP_CONAgencia = che.AgenciaForn.ToString("00000");
                    rowARL.ALP_CONDigitoAgencia = che.DigAgenciaForn;
                    rowARL.ALP_CONConta = che.ContaForn.ToString("0000000");
                    rowARL.ALP_CONDigitoConta = che.DigContaForn;
                    rowARL.ALP_CHE = che.NumeroPagamento;
                    if (che.DataEfetivaPagamento.Trim() != "" && che.DataEfetivaPagamento.Trim() != "000000")
                        rowARL.ALP_CHEPagamento = Convert.ToDateTime(che.DataEfetivaPagamento.Substring(6, 2) + "/" + che.DataEfetivaPagamento.Substring(4, 2) + "/" + che.DataEfetivaPagamento.Substring(0, 4));
                    rowARL.ALP_Valor = che.ValorPagamento;
                    rowARL.ALP_StatusN = (int)ARLStatusN.Nova;
                    rowARL.ALP_ARQ = novalinha.ARQ;
                    rowARL.ALP_Situacao = che.SituacaoAgendamento;
                    rowARL.ALP_Retorno1 = che.InfoRetorno1;
                    rowARL.ALP_Retorno2 = che.InfoRetorno2;
                    rowARL.ALP_Retorno3 = che.InfoRetorno3;
                    rowARL.ALP_Retorno4 = che.InfoRetorno4;
                    rowARL.ALP_Retorno5 = che.InfoRetorno5;
                    rowARL.ALP_TipoMovimento = che.TipoMovimento;
                    rowARL.ALP_TipoPag = che.ModalidadePagamento;
                    dPagamentoEletronico.ARquivoLido.AddARquivoLidoRow(rowARL);
                    dPagamentoEletronico.ARquivoLidoTableAdapter.Update(rowARL);
                    rowARL.AcceptChanges();
                }

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return true;
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                Erro = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Processamento dos Dados do Retorno Pagamento Eletr�nico Bradesco
        /// </summary>
        public string ProcessarDados()
        {
            DateTime Inicio = DateTime.Now;
            string strRetorno = "";

            //Varre cada linha do arquivo anda nao processado
            int Total = dPagamentoEletronico.ARquivoLidoTableAdapter.FillNaoProcessado(dPagamentoEletronico.ARquivoLido);
            if (Total == 0)
                return "sem entradas";
            //ARquivoLidoDataTable = dPagamentoEletronico.ARquivoLido;
            foreach (dPagamentoEletronico.ARquivoLidoRow rowARL in dPagamentoEletronico.ARquivoLido)
            {
                try
                {
                    //Abre a transacao
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco PagamentoEletronico - 109", dPagamentoEletronico.ARquivoLidoTableAdapter);

                    //Processa a linha
                    if (!Processa(rowARL))
                        strRetorno = strRetorno + "\r\nErro APL:" + rowARL.ALP + " - Retorno: " + rowARL.ALPSTATUS;

                    //Atualiza linha do arquivo
                    dPagamentoEletronico.ARquivoLidoTableAdapter.Update(rowARL);
                    rowARL.AcceptChanges();

                    //Fecha a trabsacao
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                    strRetorno = strRetorno + "\r\nErro Geral:" + e.Message;
                };
            };

            TimeSpan TempoTotal = DateTime.Now - Inicio;

            return string.Format("Tempo total: {0:n0}s\r\nTotal de linhas:{1}{2}{3}",
                                 TempoTotal.TotalSeconds,
                                 Total,
                                 strRetorno == "" ? "" : "\r\nErros:",
                                 strRetorno);
        }

        //*** MRC - INICIO - PAG-FOR (24/04/2014 12:00) ***
        /// <summary>
        /// Processa o cheque (pagamento eletronico)
        /// </summary>
        /// <param name="rowARL"></param>
        /// <returns></returns>
        private bool Processa(dPagamentoEletronico.ARquivoLidoRow rowARL)
        {
            //Verifica se existe cheque do pagamento eletr�nico
            Cheque Che = new Cheque(rowARL.ALP_CHE, Cheque.TipoChave.CHE);
            if (!Che.Encontrado)
            {
                rowARL.ALPSTATUS = "Cheque (Pagamento Eletr�nico) n�o encontrado";
                rowARL.ALP_StatusN = (int)ARLStatusN.ChequeNaoEncontrado;
                return false;
            }

            //Seta status do pagamento de acordo com o retorno do banco                        

            string strRetornos = " - c�digos: " + rowARL.ALP_Retorno1 + (" " + rowARL.ALP_Retorno2).TrimEnd() + (" " + rowARL.ALP_Retorno3).TrimEnd() + (" " + rowARL.ALP_Retorno4).TrimEnd() + (" " + rowARL.ALP_Retorno5).TrimEnd();
            if (rowARL.ALP_Retorno1 == "BT")
            {
                Che.AlteraStatus(CHEStatus.AguardaRetorno, "Enviado para o Banco, aguardando aprova��o do S�ndico (" + rowARL.ARQNomeArquivo + ")");
                rowARL.ALPSTATUS = "Aguardando Aprova��o";
                rowARL.ALP_StatusN = (int)ARLStatusN.Baixado;
                return true;
            }
            else if ((rowARL.ALP_Retorno1 == "BU" && rowARL.ALP_Retorno2 == "HA") ||
                     (rowARL.ALP_Retorno1 == "BU" && rowARL.ALP_Retorno2 == "BD"))
            {
                Che.AlteraStatus(CHEStatus.Retornado, "Pagamento aprovado pelo S�ndico (" + rowARL.ARQNomeArquivo + ")");
                rowARL.ALPSTATUS = "Pagamento aprovado";
                rowARL.ALP_StatusN = (int)ARLStatusN.Baixado;
                return true;
            }
            else if ((rowARL.ALP_Retorno1 == "BU" && rowARL.ALP_Retorno2 == "GU"))
            {
                Che.AlteraStatus(CHEStatus.Retornado, "Pagamento aprovado pelo S�ndico e agendado por aumento de limite (" + rowARL.ARQNomeArquivo + ")");
                rowARL.ALPSTATUS = "Pagamento aprovado";
                rowARL.ALP_StatusN = (int)ARLStatusN.Baixado;
                return true;
            }
            else if (rowARL.ALP_Retorno1 == "BW" || rowARL.ALP_Retorno1 == "HF" || rowARL.ALP_Retorno1 == "HI")
            {
                if ((CHEStatus)Che.CHErow.CHEStatus != CHEStatus.Compensado)
                    Che.AlteraStatus(CHEStatus.PagamentoConfirmadoBancoPE, "Pagamento confirmado pelo Banco (" + rowARL.ARQNomeArquivo + ")", rowARL.ALP_CHEPagamento, rowARL.ALP_Valor);
                rowARL.ALPSTATUS = "Pagamento confirmado";
                rowARL.ALP_StatusN = (int)ARLStatusN.Baixado;
                //*** MRC - INICIO (12/08/2014 10:00) ***
                DSCentral.IMPressoesTableAdapter.Incluir(Che.CHErow.CHE_CON, 159, 1, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Pagamento No " + Che.CHErow.CHENumero, false);
                //*** MRC - TERMINO (12/08/2014 10:00) ***
                return true;
            }
            else if (rowARL.ALP_Retorno1 == "BV")
            {
                //*** MRC - INICIO - PAG-FOR (14/07/2014 15:00) ***
                Che.AlteraPECancelamentoStatus(PECancelamentoStatus.ProcessadoCancelamentoPE, "Cancelamento efetuado pelo banco, pagamento exclu�do (" + rowARL.ARQNomeArquivo + ")");
                //*** MRC - TERMINO - PAG-FOR (14/07/2014 15:00) ***
                rowARL.ALPSTATUS = "Pagamento exclu�do";
                rowARL.ALP_StatusN = (int)ARLStatusN.Baixado;
                return true;
            }
            else if (rowARL.ALP_Retorno1 == "JB")
            {
                if (Che.Status.EstaNoGrupo(CHEStatus.Compensado, CHEStatus.CompensadoAguardaCopia))
                {
                    if (Che.Estorna())
                    {
                        Che.GravaObs("INCONSIST�NCIA: DOC/TED/T�tulo devolvido e estornado (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                        rowARL.ALPSTATUS = "Inconsist�ncia: DOC/TED/T�tulo devolvido e estornado" + strRetornos;
                        rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                        return false;
                    }
                }
                else if (Che.Status.EstaNoGrupo(CHEStatus.PagamentoConfirmadoBancoPE, CHEStatus.AguardaRetorno))
                    return false; //Aguarda compensar   
            }

            //Inconsistencias
            switch (Che.Status)
            {
                //Cheques com estes status n�o ser�o alterados pelo retorno da inconsist�ncia mas a mensagem ser� gravada no hist�rico                   
                case CHEStatus.Aguardacopia:
                case CHEStatus.Antigo:
                case CHEStatus.BaixaManual:
                case CHEStatus.Caixinha:
                case CHEStatus.Cancelado:
                case CHEStatus.ComSindico:
                case CHEStatus.Compensado:
                case CHEStatus.CompensadoAguardaCopia:
                case CHEStatus.NaoEncontrado:
                case CHEStatus.PagamentoCancelado:
                case CHEStatus.PagamentoInconsistentePE:
                case CHEStatus.PagamentoNaoAprovadoSindicoPE:
                case CHEStatus.PagamentoNaoEfetivadoPE:
                case CHEStatus.Retirado:
                case CHEStatus.Cadastrado:
                case CHEStatus.EnviadoBanco:
                    break;
                case CHEStatus.AguardaEnvioPE:
                case CHEStatus.AguardaConfirmacaoBancoPE:
                case CHEStatus.CadastradoBloqueado:
                case CHEStatus.AguardaRetorno:
                    Che.AlteraStatus(CHEStatus.PagamentoInconsistentePE, "Pagamento recusado pelo Banco (" + rowARL.ARQNomeArquivo + ")");
                    break;
                case CHEStatus.Retornado:
                case CHEStatus.DebitoAutomatico:
                case CHEStatus.Eletronico:
                case CHEStatus.PagamentoConfirmadoBancoPE:
                    Che.AlteraStatus(CHEStatus.PagamentoNaoEfetivadoPE, "Pagamento n�o efetivado (" + rowARL.ARQNomeArquivo + ")");
                    break;
                default:
                    throw new NotImplementedException(string.Format("Status n�o tratado em PagamentoEletronico.cs CHEStatus:{0}", Che.CHErow.CHEStatus));
            }
            //if ((CHEStatus)Che.CHErow.CHEStatus != CHEStatus.Cancelado && (CHEStatus)Che.CHErow.CHEStatus != CHEStatus.PagamentoCancelado)
            //    Che.AlteraStatus(CHEStatus.PagamentoInconsistentePE, "Pagamento recusado pelo Banco (" + rowARL.ARQNomeArquivo + ")");
            if (rowARL.ALP_Retorno1 == "AE" || rowARL.ALP_Retorno1 == "BF" || rowARL.ALP_Retorno1 == "BG" || rowARL.ALP_Retorno1 == "FT")
            {
                Che.GravaObs("INCONSIST�NCIA: Identifica��o, CNPJ/CPF ou Tipo de inscri��o do cliente pagador inv�lido (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Identifica��o, CNPJ/CPF ou Tipo de inscri��o do cliente pagador inv�lido" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "BO" || rowARL.ALP_Retorno1 == "KP")
            {
                Che.GravaObs("INCONSIST�NCIA: Cliente n�o cadastrado ou n�o cadastrado no PAGFOR (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Cliente n�o cadastrado ou n�o cadastrado no PAGFOR" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "FU")
            {
                Che.GravaObs("INCONSIST�NCIA: Contrato inexistente ou inativo (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Contrato inexistente ou inativo" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "FV")
            {
                Che.GravaObs("INCONSIST�NCIA: Cliente com conv�nio cancelado (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Cliente com conv�nio cancelado" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GN")
            {
                Che.GravaObs("INCONSIST�NCIA: Conta complementar inv�lida (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Conta complementar inv�lida" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GZ")
            {
                Che.GravaObs("INCONSIST�NCIA: Conta corrente do pagador encerrada ou bloqueada (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Conta corrente do pagador encerrada ou bloqueada" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            //*** MRC - INICIO - PAG-FOR (10/12/2014 12:00) ***
            else if (rowARL.ALP_Retorno1 == "AG" || rowARL.ALP_Retorno1 == "AT" || rowARL.ALP_Retorno1 == "BH" || rowARL.ALP_Retorno1 == "BK")
            //*** MRC - TERMINO - PAG-FOR (10/12/2014 12:00) ***
            {
                Che.GravaObs("INCONSIST�NCIA: CNPJ/CPF ou Tipo de Inscri��o do favorecido inv�lido ou divergente (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: CNPJ/CPF ou Tipo de Inscri��o do favorecido inv�lido ou divergente" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "AF" || rowARL.ALP_Retorno1 == "FK")
            {
                Che.GravaObs("INCONSIST�NCIA: Valores n�o num�ricos, zerados ou faltando (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Valores n�o num�ricos, zerados ou faltando" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "F5")
            {
                Che.GravaObs("INCONSIST�NCIA: Valor do pagamento e do trailler n�o confere (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Valor do pagamento e do trailler n�o confere" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "AL" || rowARL.ALP_Retorno1 == "AM" || rowARL.ALP_Retorno1 == "AN" || rowARL.ALP_Retorno1 == "AZ")
            {
                Che.GravaObs("INCONSIST�NCIA: Banco, ag�ncia ou conta do favorecido inv�lida (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Banco, ag�ncia ou conta do favorecido inv�lida" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "AO")
            {
                Che.GravaObs("INCONSIST�NCIA: Nome do favorecido n�o informado (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Nome do favorecido n�o informado" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "AU" || rowARL.ALP_Retorno1 == "AX")
            {
                Che.GravaObs("INCONSIST�NCIA: Endere�o ou CEP do favorecido n�o informado ou inv�lido (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Endere�o ou CEP do favorecido n�o informado ou inv�lido" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "BI")
            {
                Che.GravaObs("INCONSIST�NCIA: Data de vencimento inv�lida ou n�o preenchida (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Data de vencimento inv�lida ou n�o preenchida" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "BJ")
            {
                Che.GravaObs("INCONSIST�NCIA: Data de emiss�o inv�lida (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Data de emiss�o inv�lida" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "BM" || rowARL.ALP_Retorno1 == "BN")
            {
                Che.GravaObs("INCONSIST�NCIA: Data para efetiva��o do pagamento inv�lida ou anterior a do processamento (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Data para efetiva��o do pagamento inv�lida ou anterior a do processamento" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "HE")
            {
                Che.GravaObs("INCONSIST�NCIA: Data de vencimento ou pagamento fora do prazo de opera��o do banco (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Data de vencimento ou pagamento fora do prazo de opera��o do banco" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "ME")
            {
                Che.GravaObs("INCONSIST�NCIA: Data de pagamento alterada devido a feriado local (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Data de pagamento alterada devido a feriado local" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "BQ")
            {
                Che.GravaObs("INCONSIST�NCIA: Data do documento posterior ao vencimento (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Data do documento posterior ao vencimento" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GA" || rowARL.ALP_Retorno1 == "GB" || rowARL.ALP_Retorno1 == "GC" || rowARL.ALP_Retorno1 == "GQ" || rowARL.ALP_Retorno1 == "JK")
            {
                Che.GravaObs("INCONSIST�NCIA: Tipo de opera��o, Tipo de conta, N�mero ou Finalidade do DOC/TED inv�lido (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Tipo de opera��o, Tipo de conta, N�mero ou Finalidade do DOC/TED inv�lido" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GD" || rowARL.ALP_Retorno1 == "GE" || rowARL.ALP_Retorno1 == "GJ" || rowARL.ALP_Retorno1 == "GK" || rowARL.ALP_Retorno1 == "GL" || rowARL.ALP_Retorno1 == "GY")
            {
                Che.GravaObs("INCONSIST�NCIA: Conta corrente ou Conta poupan�a do favorecido inv�lida, encerrada, bloqueada, n�o recadastrada ou n�o encontrada (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Conta corrente ou Conta poupan�a do favorecido inv�lida, encerrada, bloqueada, n�o recadastrada ou n�o encontrada" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GG" || rowARL.ALP_Retorno1 == "GH" || rowARL.ALP_Retorno1 == "GI")
            {
                Che.GravaObs("INCONSIST�NCIA: Linha digit�vel inv�lida (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Linha digit�vel inv�lida" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "F8")
            {
                Che.GravaObs("INCONSIST�NCIA: Pagamento enviado ap�s o hor�rio estipulado (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Pagamento enviado ap�s o hor�rio estipulado" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "HB" || rowARL.ALP_Retorno1 == "HC" || rowARL.ALP_Retorno1 == "HD" || rowARL.ALP_Retorno1 == "HG")
            {
                Che.GravaObs("INCONSIST�NCIA: Pagamento n�o efetuado, saldo insuficiente (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Pagamento n�o efetuado, saldo insuficiente" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GS")
            {
                Che.GravaObs("INCONSIST�NCIA: Limite de pagamento excedido, falar com o gerente da ag�ncia (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Limite de pagamento excedido, falar com o gerente da ag�ncia " + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "JP")
            {
                Che.GravaObs("INCONSIST�NCIA: Pagamento com Limite TED excedido, falar com o gerente da ag�ncia (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Pagamento com Limite TED excedido, falar com o gerente da ag�ncia " + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }

            else if (rowARL.ALP_Retorno1 == "JB")
            {
                //ATEN��O este c�digo � usando quando recebemos um c�digo de estorno mas o cheque n�o est� nos status previstos!!!
                Che.GravaObs("INCONSIST�NCIA: DOC/TED/T�tulo devolvido e estornado (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: DOC/TED/T�tulo devolvido e estornado" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }

            else if (rowARL.ALP_Retorno1 == "FN" || rowARL.ALP_Retorno1 == "F9")
            {
                Che.GravaObs("INCONSIST�NCIA: Tentativa de inclus�o de registro existente (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Tentativa de inclus�o de registro existente" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "FO")
            {
                Che.GravaObs("INCONSIST�NCIA: Tentativa de altera��o para registro inexistente (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Tentativa de altera��o para registro inexistente" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "F3" || rowARL.ALP_Retorno1 == "F7" || rowARL.ALP_Retorno1 == "GR")
            {
                Che.GravaObs("INCONSIST�NCIA: Tentativa de altera��o inv�lida, pagamento j� enviado ou confirma��o de d�bito j� efetuada (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Tentativa de altera��o inv�lida, pagamento j� enviado ou confirma��o de d�bito j� efetuada" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "FI")
            {
                Che.GravaObs("INCONSIST�NCIA: Exclus�o de agendamento n�o dispon�vel (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Exclus�o de agendamento n�o dispon�vel" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "FL")
            {
                Che.GravaObs("INCONSIST�NCIA: Tipo de pagamento inv�lido para o contrato (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Tipo de pagamento inv�lido para o contrato" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GO")
            {
                Che.GravaObs("INCONSIST�NCIA: Inclus�o de DOC/TED para Banco 237 n�o permitido (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Inclus�o de DOC/TED para Banco 237 n�o permitido" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GP")
            {
                Che.GravaObs("INCONSIST�NCIA: CGC/CPF do favorecido divergente do cadastro do Banco (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: CGC/CPF do favorecido divergente do cadastro do Banco" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "FH")
            {
                Che.GravaObs("INCONSIST�NCIA: N�mero do documento (ou nota) inv�lido ou faltando (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: N�mero do documento (ou nota) inv�lido ou faltando" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1.EstaNoGrupo("LG","LH"))
            {
                Che.GravaObs("INCONSIST�NCIA: Conta sal�rio n�o permitida para este conv�nio (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Conta sal�rio n�o permitida para este conv�nio" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "KR")
            {
                Che.GravaObs("INCONSIST�NCIA: Banco destinat�rio n�o operante nesta data (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Banco destinat�rio n�o operante nesta data" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "AA")
            {
                Che.GravaObs("INCONSIST�NCIA: Arquivo duplicado ou sequencial remessa repetido (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Arquivo duplicado ou sequencial remessa repetido" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "CD")
            {
                Che.GravaObs("INCONSIST�NCIA: C�digo de barras - valor do t�tulo divergente/inv�lido (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: C�digo de barras - valor do t�tulo divergente/inv�lido" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "ZE")
            {
                Che.GravaObs("INCONSIST�NCIA: T�tulo bloqueado na base (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: T�tulo bloqueado na base" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "ZK")
            {
                Che.GravaObs("INCONSIST�NCIA: Boleto j� liquidado (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Boleto j� liquidado" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "FD")
            {
                Che.GravaObs("INCONSIST�NCIA: Valor da parcela inv�lida (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Valor da parcela inv�lida" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "FJ")
            {
                Che.GravaObs("INCONSIST�NCIA: Soma dos valores n�o confere (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Soma dos valores n�o confere" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "GW")
            {
                Che.GravaObs("INCONSIST�NCIA: Conta corrente ou conta poupan�a com raz�o n�o permitida para efetiva��o de cr�dito (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Inconsist�ncia: Conta corrente ou conta poupan�a com raz�o n�o permitida para efetiva��o de cr�dito" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "ZG")
            {
                Che.GravaObs("INCONSIST�NCIA: Sistema em conting�ncia - t�tulo vencido (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Sistema em conting�ncia - t�tulo vencido" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else if (rowARL.ALP_Retorno1 == "YA")
            {
                Che.GravaObs("INCONSIST�NCIA: T�tulo n�o encontrado (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "T�tulo n�o encontrado" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.ErronaBaixa;
                return false;
            }
            else
            {
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;neon@vcommerce.com.br",
                                                            string.Format("Retorno n�o homologado{2}\r\nArquivo:{0}\r\nlinha: APL = {1}", rowARL.ARQNomeArquivo, rowARL.ALP, strRetornos),
                                                            "Pagamento eletr�nico");
                Che.GravaObs("INCONSIST�NCIA: Retorno n�o homologado, verificar c�digos na documenta��o (" + rowARL.ARQNomeArquivo + ")" + strRetornos);
                rowARL.ALPSTATUS = "Retorno n�o homologado" + strRetornos;
                rowARL.ALP_StatusN = (int)ARLStatusN.RetornoNaoHomologado;
                return false;
            }

            //*** MRC - TERMINO - PAG-FOR (06/08/2014 13:00) ***
            //*** MRC - TERMINO - PAG-FOR (15/05/2014 11:45) ***
        }
        //*** MRC - TERMINO - PAG-FOR (24/04/2014 12:00) ***       
    }
}
