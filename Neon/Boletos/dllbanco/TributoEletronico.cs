/*
MR - 03/12/2014 10:00 -           - Ajustes no comprovante GPS/INSS (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***)
MR - 10/12/2014 12:00 -           - Ajustes no comprovante GPS/INSS, retirada do Vencimento (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
MR - 26/12/2014 10:00 -           - Implementado comprovante FGTS (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
MR - 27/04/2015 12:00 -           - Tratamento de "quebra de cheques" para tributo eletronico referente "guia de recolhimento - eletr�nico" (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
MR - 18/08/2015 10:00             - Retorno de cancelamento de tributo (Altera��es indicadas por *** MRC - INICIO (18/08/2015 10:00) ***)
                                    Corre��o de poss�vel bug de transa��o (transacao inicia ap�s verificar se GPS encontrada)
                                    Seta tamb�m o ID do documento recebido pelo banco ao setar status
MR - 27/05/2016 12:00 -           - Tratamento espec�fico para o Status 7 com mais detalhes (Altera��es indicadas por *** MRC - INICIO (27/05/2016 12:00) ***)
*/

using System;
using FrameworkProc;
using CompontesBasicos;
using EDIBoletos.RegistrosOBJTributo;
using EDIBoletosNeon;
using dllCheques;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using dllImpostoNeonProc;


namespace dllbanco
{
    /// <summary>
    /// Tributo Eletr�nico
    /// </summary>
    public class TributoEletronico
    {
        /// <summary>
        /// �ltimo erro
        /// </summary>
        public string Erro;

        private dTributoEletronico _dTributoEletronico;

        /// <summary>
        /// dTributoEletronico
        /// </summary>
        public dTributoEletronico dTributoEletronico
        {
            get
            {
                if (_dTributoEletronico == null)
                {
                    _dTributoEletronico = new dTributoEletronico(new EMPTProc(EMPTipo.Local));                    
                }
                return _dTributoEletronico;
            }
        }

        private dTributoEletronico _dTributoEletronicoF;

        /// <summary>
        /// dTributoEletronico
        /// </summary>
        public dTributoEletronico dTributoEletronicoF
        {
            get
            {
                if (_dTributoEletronicoF == null)
                {
                    _dTributoEletronicoF = new dTributoEletronico(new EMPTProc(FrameworkProc.EMPTipo.Filial));
                    //_dTributoEletronicoF.EMPTipo = 
                }
                return _dTributoEletronicoF;
            }
        }

        private dTributoEletronico.ARquivoLidoDataTable ARquivoLidoDataTable;

        /// <summary>
        /// Retorno Tributo Eletr�nico Bradesco
        /// </summary>
        /// <param name="TributoRetornoBra"></param>
        /// <returns></returns>
        public bool Carregar(EDIBoletos.TributoRetornoBra TributoRetornoBra)
        {
            dTributoEletronico.ARquivoLido.Clear();
            Erro = "";
            ARquivoLidoDataTable = dTributoEletronico.ARquivoLido;

            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco TributoEletronico - 51", dTributoEletronico.ARQuivosTableAdapter,
                                                                                     dTributoEletronico.ARquivoLidoTableAdapter);
            try
            {
                dTributoEletronico.ARQuivosRow novalinha = dTributoEletronico.ARQuivos.NewARQuivosRow();
                novalinha.ARQBanco = 237;
                novalinha.ARQIData = DateTime.Now;
                novalinha.ARQNome = TributoRetornoBra.Header.LinhaLida;
                novalinha.ARQNomeArquivo = System.IO.Path.GetFileName(TributoRetornoBra.NomeArquivo);
                dTributoEletronico.ARQuivos.AddARQuivosRow(novalinha);
                dTributoEletronico.ARQuivosTableAdapter.Update(novalinha);
                novalinha.AcceptChanges();

                foreach (EDIBoletos.RegistrosOBJTributo.regTransacao_1_2_TRIBUTO_001_BRA tri in TributoRetornoBra.Tributos1)
                {
                   dTributoEletronico.ARquivoLidoRow rowARL = dTributoEletronico.ARquivoLido.NewARquivoLidoRow();
                   rowARL.ALT_CONAgencia = tri.DAAgencia.ToString("00000");
                   rowARL.ALT_CONDigitoAgencia = tri.DADigAgencia;
                   rowARL.ALT_CONConta = tri.DANumConta.ToString("0000000");
                   rowARL.ALT_CONDigitoConta = tri.DADigNumConta;
                   //Corre��o de erro ocorrido em 14/02/2016 pode ser removido ap�s esta data
                   if (int.Parse(tri.UsoEmpresa) <= 1000000162)
                       rowARL.ALT_GPS = int.Parse(tri.UsoEmpresa) + 1000000000;
                   else
                       rowARL.ALT_GPS = Convert.ToInt32(tri.UsoEmpresa);
                   //Corre��o de erro ocorrido em 14/02/2016 pode ser removido ap�s esta data
                   rowARL.ALT_GPSPagamento = Convert.ToDateTime(tri.DataDebito);
                   rowARL.ALT_Valor = tri.ValorTotal;
                   rowARL.ALT_StatusN = (int)ARLStatusN.Nova;
                   rowARL.ALT_ARQ = novalinha.ARQ;
                   rowARL.ALT_TipoTributo = tri.TipoTributo;
                   rowARL.ALT_IDDocumento = tri.IDDocumento;
                   rowARL.ALT_CodConsistencia = tri.CodConsistenciaArq;
                   //*** MRC - INICIO (18/08/2015 10:00) ***
                   rowARL.ALT_TipoMovimento = tri.TipoMovimento;
                   //*** MRC - TERMINO (18/08/2015 10:00) ***
                   rowARL.ALT_Autenticacao = tri.Autenticacao;
                   dTributoEletronico.ARquivoLido.AddARquivoLidoRow(rowARL);
                   dTributoEletronico.ARquivoLidoTableAdapter.Update(rowARL);
                   rowARL.AcceptChanges();
                }
                foreach (EDIBoletos.RegistrosOBJTributo.regTransacao_1_2_TRIBUTO_005_BRA tri in TributoRetornoBra.Tributos5)
                {
                    dTributoEletronico.ARquivoLidoRow rowARL = dTributoEletronico.ARquivoLido.NewARquivoLidoRow();
                    rowARL.ALT_CONAgencia = tri.DAAgencia.ToString("00000");
                    rowARL.ALT_CONDigitoAgencia = tri.DADigAgencia;
                    rowARL.ALT_CONConta = tri.DANumConta.ToString("0000000");
                    rowARL.ALT_CONDigitoConta = tri.DADigNumConta;
                    //Corre��o de erro ocorrido em 14/02/2016 pode ser removido ap�s esta data
                    if (int.Parse(tri.UsoEmpresa) <= 1000000162)
                        rowARL.ALT_GPS = int.Parse(tri.UsoEmpresa) + 1000000000;
                    else
                        rowARL.ALT_GPS = Convert.ToInt32(tri.UsoEmpresa);
                    //Corre��o de erro ocorrido em 14/02/2016 pode ser removido ap�s esta data
                    rowARL.ALT_GPSPagamento = Convert.ToDateTime(tri.DataDebito);
                    rowARL.ALT_Valor = tri.ValorTotal;
                    rowARL.ALT_StatusN = (int)ARLStatusN.Nova;
                    rowARL.ALT_ARQ = novalinha.ARQ;
                    rowARL.ALT_TipoTributo = tri.TipoTributo;
                    rowARL.ALT_IDDocumento = tri.IDDocumento;
                    rowARL.ALT_CodConsistencia = tri.CodConsistenciaArq;
                    //*** MRC - INICIO (18/08/2015 10:00) ***
                    rowARL.ALT_TipoMovimento = tri.TipoMovimento;
                    //*** MRC - TERMINO (18/08/2015 10:00) ***
                    rowARL.ALT_Autenticacao = tri.Autenticacao;
                    dTributoEletronico.ARquivoLido.AddARquivoLidoRow(rowARL);
                    dTributoEletronico.ARquivoLidoTableAdapter.Update(rowARL);
                    rowARL.AcceptChanges();
                }
                //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
                foreach (EDIBoletos.RegistrosOBJTributo.regTransacao_1_2_TRIBUTO_006_BRA tri in TributoRetornoBra.Tributos6)
                {
                    dTributoEletronico.ARquivoLidoRow rowARL = dTributoEletronico.ARquivoLido.NewARquivoLidoRow();
                    rowARL.ALT_CONAgencia = tri.DAAgencia.ToString("00000");
                    rowARL.ALT_CONDigitoAgencia = tri.DADigAgencia;
                    rowARL.ALT_CONConta = tri.DANumConta.ToString("0000000");
                    rowARL.ALT_CONDigitoConta = tri.DADigNumConta;
                    //Corre��o de erro ocorrido em 14/02/2016 pode ser removido ap�s esta data
                    if (int.Parse(tri.UsoEmpresa) <= 1000000162)
                        rowARL.ALT_GPS = int.Parse(tri.UsoEmpresa) + 1000000000;
                    else
                        rowARL.ALT_GPS = Convert.ToInt32(tri.UsoEmpresa);
                    //Corre��o de erro ocorrido em 14/02/2016 pode ser removido ap�s esta data
                    rowARL.ALT_GPSPagamento = Convert.ToDateTime(tri.DataDebito);
                    rowARL.ALT_Valor = tri.ValorPago;
                    rowARL.ALT_StatusN = (int)ARLStatusN.Nova;
                    rowARL.ALT_ARQ = novalinha.ARQ;
                    rowARL.ALT_TipoTributo = tri.TipoTributo;
                    rowARL.ALT_IDDocumento = tri.IDDocumento;
                    rowARL.ALT_CodConsistencia = tri.CodConsistenciaArq;
                    //*** MRC - INICIO (18/08/2015 10:00) ***
                    rowARL.ALT_TipoMovimento = tri.TipoMovimento;
                    //*** MRC - TERMINO (18/08/2015 10:00) ***
                    rowARL.ALT_Autenticacao = tri.Autenticacao;
                    rowARL.ALT_IDPagamento = tri.IDPagamento;
                    rowARL.ALT_IDDocumentoPago = tri.IDDocumentoPago;
                    rowARL.ALT_NumIdentificacao = tri.NumIdentificacao;
                    rowARL.ALT_AutenticacaoCodBarra = tri.AutenticacaoCodBarra;
                    rowARL.ALT_DataValidade = Convert.ToDateTime(tri.DataValidade);
                    dTributoEletronico.ARquivoLido.AddARquivoLidoRow(rowARL);
                    dTributoEletronico.ARquivoLidoTableAdapter.Update(rowARL);
                    rowARL.AcceptChanges();
                }
                //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return true;
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                Erro =  e.Message;
                return false;
            }
        }

        private void TranfereFilial(dTributoEletronico.ARquivoLidoRow rowARL)
        {                        
            if(dTributoEletronico.ARQuivosTableAdapter.FillByARQ(dTributoEletronico.ARQuivos,rowARL.ALT_ARQ) > 0)
            {
                dTributoEletronico.ARQuivosRow rowARQ = dTributoEletronico.ARQuivos[0];
                int? ARQFilial = dTributoEletronicoF.ARQuivosTableAdapter.BuscaARQ2(rowARQ.ARQNome);
                if (!ARQFilial.HasValue)
                {
                    dTributoEletronico.ARQuivosRow rowARQF = dTributoEletronicoF.ARQuivos.NewARQuivosRow();
                    rowARQF.ARQBanco = rowARQ.ARQBanco;
                    rowARQF.ARQIData = rowARQ.ARQIData;
                    rowARQF.ARQNome = rowARQ.ARQNome;
                    rowARQF.ARQNomeArquivo = rowARQ.ARQNomeArquivo;
                    dTributoEletronicoF.ARQuivos.AddARQuivosRow(rowARQF);
                    dTributoEletronicoF.ARQuivosTableAdapter.Update(rowARQF);
                    ARQFilial = rowARQF.ARQ;
                }
                dTributoEletronico.ARquivoLidoRow rowARLF = dTributoEletronicoF.ARquivoLido.NewARquivoLidoRow();
                foreach (System.Data.DataColumn DC in dTributoEletronico.ARquivoLido.Columns)
                {
                    if (!DC.ReadOnly)
                        if (DC.ColumnName == "ALT_ARQ")
                            rowARLF["ALT_ARQ"] = ARQFilial;
                        else
                            rowARLF[DC.ColumnName] = rowARL[DC];
                }
                dTributoEletronicoF.ARquivoLido.AddARquivoLidoRow(rowARLF);
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("transfere ARL TributoEletronico.cs 228");
                    dTributoEletronico.ARquivoLidoTableAdapter.EmbarcaEmTransST();
                    dTributoEletronicoF.ARquivoLidoTableAdapter.EmbarcaEmTransST();
                    dTributoEletronicoF.ARquivoLidoTableAdapter.Update(rowARLF);
                    rowARL.Delete();
                    dTributoEletronico.ARquivoLidoTableAdapter.Update(rowARL);
                    VirMSSQL.TableAdapter.CommitSQLDuplo();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                }
            }
        }

        /// <summary>
        /// Processamento dos Dados do Retorno Tributo Eletr�nico Bradesco
        /// </summary>
        public string ProcessarDados()
        {
           //Inicia retorno
           string strRetorno = "";

           //Seta o destino dos comprovantes
           string DestinoPDF = dEstacao.dEstacaoSt.GetValor("Destino PDF");
           if (DestinoPDF == "")
           {
               DestinoPDF = @"\\email\volume_1\publicacoes\";
               dEstacao.dEstacaoSt.SetValor("Destino PDF", DestinoPDF);
           }
           DestinoPDF = DestinoPDF + @"Financeiro\Recibos\Individual\";
                      
           //Varre cada linha do arquivo anda nao processado
           dTributoEletronico.ARquivoLidoTableAdapter.FillNaoProcessado(dTributoEletronico.ARquivoLido);
           ARquivoLidoDataTable = dTributoEletronico.ARquivoLido;
           dTributoEletronico.ARquivoLidoRow[] rowARLs = new dTributoEletronico.ARquivoLidoRow[ARquivoLidoDataTable.Count];
            for(int i=0;i< ARquivoLidoDataTable.Count;i++)
                rowARLs[i] = ARquivoLidoDataTable[i];
           foreach (dTributoEletronico.ARquivoLidoRow rowARL in rowARLs)
           {
               if ((Framework.DSCentral.EMP == 1) && (rowARL.ALT_GPS > 2000000000))
                   TranfereFilial(rowARL);
               else
               {
                   try
                   {
                       //Abre a transacao
                       VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco TributoEletronico - 160", dTributoEletronico.ARquivoLidoTableAdapter);

                       //Processa a linha
                       if (!Processa(rowARL, DestinoPDF))
                           strRetorno = string.Format("{0}\r\nErro ALT:{1} - Retorno: {2}", strRetorno, rowARL.ALT, rowARL.ALTSTATUS);

                       //Atualiza linha do arquivo
                       dTributoEletronico.ARquivoLidoTableAdapter.Update(rowARL);
                       rowARL.AcceptChanges();

                       //Fecha a transacao
                       VirMSSQL.TableAdapter.STTableAdapter.Commit();
                   }
                   catch (Exception e)
                   {
                       VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                       strRetorno = string.Format("{0}\r\nErro Geral:{1}", strRetorno, e.Message);
                   };
               }
           };

           //Retorna
           return strRetorno;
        }

        /// <summary>
        /// Processa o tributo (Tributo eletr�nico)
        /// </summary>
        /// <param name="rowARL"></param>
        /// <param name="DestinoPDF"></param>
        /// <returns></returns>
        public bool Processa(dTributoEletronico.ARquivoLidoRow rowALT, string DestinoPDF)
        {
            //Verifica se existe gps do tributo eletr�nico
            dGPS.dGPSSt.GPSTableAdapter.FillByGPS(dGPS.dGPSSt.GPS, rowALT.ALT_GPS);
            if (dGPS.dGPSSt.GPS.Count == 0)
            {
                rowALT.ALTSTATUS = "GPS (Tributo Eletr�nico) n�o encontrado";
                rowALT.ALT_StatusN = (int)ARLStatusN.TributoNaoEncontrado;
                return false;
            }
            dGPS.GPSRow rowGPS = dGPS.dGPSSt.GPS[0];
            dEDIBoletosNeon dEDI = new dEDIBoletosNeon();
            bool booRetorno = false;
            try
            {
                //Abre a transacao
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco TributoEletronico - 466", dGPS.dGPSSt.GPSTableAdapter, dEDI.PAGamentosTableAdapter);
                //Seta status do tributo de acordo com o retorno do banco                        
                int intGPSStatusNovo = 0;
                string strRetornos = string.Format(" - c�digos: {0}{1}{2}{3}{4}",
                                                       rowALT.ALT_CodConsistencia.Substring(0, 2),
                                                       (" " + rowALT.ALT_CodConsistencia.Substring(2, 2)).TrimEnd(),
                                                       (" " + rowALT.ALT_CodConsistencia.Substring(4, 2)).TrimEnd(),
                                                       (" " + rowALT.ALT_CodConsistencia.Substring(6, 2)).TrimEnd(),
                                                       (" " + rowALT.ALT_CodConsistencia.Substring(8, 2)).TrimEnd());
                if (Convert.ToInt32(rowALT.ALT_CodConsistencia.Substring(0, 2)) == 0 && rowALT.ALT_TipoMovimento == "I")
                {
                    intGPSStatusNovo = (int)StatusArquivo.Agendado;
                    dGPS.dGPSSt.GPSTableAdapter.AlteraStatus(intGPSStatusNovo, rowALT.ALT_IDDocumento, rowALT.ALT_Autenticacao, String.Format("{0:dd/MM/yy HH:mm:ss} - {1}", DateTime.Now, "Pagamento agendado (" + rowALT.ARQNomeArquivo + ")"), rowALT.ALT_GPS);
                    rowALT.ALTSTATUS = "Pagamento agendado";
                    rowALT.ALT_StatusN = (int)ARLStatusN.Baixado;
                    booRetorno = true;
                }

                else if (Convert.ToInt32(rowALT.ALT_CodConsistencia.Substring(0, 2)) == 0 && rowALT.ALT_TipoMovimento == "E")
                {
                    intGPSStatusNovo = (int)StatusArquivo.Excluido;
                    dGPS.dGPSSt.GPSTableAdapter.AlteraStatus(intGPSStatusNovo, rowALT.ALT_IDDocumento, rowALT.ALT_Autenticacao, String.Format("{0:dd/MM/yy HH:mm:ss} - {1}", DateTime.Now, "Pagamento exclu�do (" + rowALT.ARQNomeArquivo + ")"), rowALT.ALT_GPS);
                    rowALT.ALTSTATUS = "Pagamento exclu�do";
                    rowALT.ALT_StatusN = (int)ARLStatusN.Baixado;
                    booRetorno = true;
                }

                else if (Convert.ToInt32(rowALT.ALT_CodConsistencia.Substring(0, 2)) == 62)
                {
                    if ((StatusArquivo)rowGPS.GPSStatus != StatusArquivo.Efetuado)
                    {
                        intGPSStatusNovo = (int)StatusArquivo.Confirmado_Banco;
                        dGPS.dGPSSt.GPSTableAdapter.AlteraStatus(intGPSStatusNovo, rowALT.ALT_IDDocumento, rowALT.ALT_Autenticacao, String.Format("{0:dd/MM/yy HH:mm:ss} - {1}", DateTime.Now, "Pagamento confirmado pelo Banco (" + rowALT.ARQNomeArquivo + ")"), rowALT.ALT_GPS);
                    }
                    rowALT.ALTSTATUS = "Pagamento confirmado";
                    rowALT.ALT_StatusN = (int)ARLStatusN.Baixado;
                    //Gera o comprovante
                    switch (rowALT.ALT_TipoTributo)
                    {
                        case (int)TributoRegistroBase.TiposTipoTributo.DARF:
                            dllImpostos.Comprovantes.ComprDARFPT_Bradesco ComprDARF = new dllImpostos.Comprovantes.ComprDARFPT_Bradesco();
                            ComprDARF.dImpCompr.DARFPT_Bradesco.Clear();
                            ComprDARF.dImpCompr.DARFPT_Bradesco.AddDARFPT_BradescoRow(rowGPS.GPSNomeIdentificador, rowGPS.GPSApuracao.ToString("dd/MM/yyyy"),
                                                                                      (new DocBacarios.CPFCNPJ(rowGPS.GPSIdentificador)).ToString(),
                                                                                      rowGPS.GPSCodigoReceita.ToString("0000"), "", rowGPS.GPSVencimento.ToString("dd/MM/yyyy"),
                                                                                      rowGPS.GPSValorPrincipal, rowGPS.GPSValorMulta, rowGPS.GPSValorJuros, rowALT.ALT_Valor,
                                                                                      rowALT.ALT_Autenticacao, rowALT.ALT_GPSPagamento.ToString("dd/MM/yyyy"), rowALT.ALT_IDDocumento,
                                                                                      Convert.ToInt32(rowALT.ALT_CONAgencia).ToString().PadLeft(4, '0') + "-" + rowALT.ALT_CONDigitoAgencia,
                                                                                      rowGPS.GPSNomeClientePagador + " - ID. " + rowGPS.GPS);
                            ComprDARF.CreateDocument();
                            ComprDARF.ExportToPdf(DestinoPDF + @"\recibo_tributo_" + rowALT.ALT_GPS.ToString().Trim() + ".pdf");
                            break;
                        case (int)TributoRegistroBase.TiposTipoTributo.GPS:
                            dllImpostos.Comprovantes.ComprGPS_Bradesco ComprGPS = new dllImpostos.Comprovantes.ComprGPS_Bradesco();
                            ComprGPS.dImpCompr.GPS_Bradesco.Clear();
                            //*** MRC - INICIO - TRIBUTOS (10/12/2014 10:00) ***
                            //*** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***
                            ComprGPS.dImpCompr.GPS_Bradesco.AddGPS_BradescoRow(rowGPS.GPSNomeIdentificador + (rowGPS.GPSEndereco == "" ? "" : " / " + rowGPS.GPSEndereco) + (rowGPS.GPSCEP == 0 ? "" : " - " + rowGPS.GPSCEP.ToString("00000000")),
                                                                               "", rowGPS.GPSCodigoReceita.ToString(),
                                                                               rowGPS.GPSMes.ToString().PadLeft(2, '0') + "/" + rowGPS.GPSAno,
                                                                               (new DocBacarios.CPFCNPJ(rowGPS.GPSIdentificador)).ToString(),
                                                                               rowGPS.GPSValorPrincipal, rowGPS.GPSOutras, rowGPS.GPSValorJuros, rowALT.ALT_Valor,
                                                                               rowALT.ALT_Autenticacao, rowALT.ALT_GPSPagamento.ToString("dd/MM/yyyy"), rowALT.ALT_IDDocumento,
                                                                               Convert.ToInt32(rowALT.ALT_CONAgencia).ToString().PadLeft(4, '0') + "-" + rowALT.ALT_CONDigitoAgencia,
                                                                               rowGPS.GPSNomeClientePagador + " - ID. " + rowGPS.GPS);
                            ComprGPS.CreateDocument();
                            //*** MRC - TERMINO - TRIBUTOS (03/12/2014 10:00) ***
                            //*** MRC - TERMINO - TRIBUTOS (10/12/2014 10:00) ***
                            ComprGPS.ExportToPdf(DestinoPDF + @"\recibo_tributo_" + rowALT.ALT_GPS.ToString().Trim() + ".pdf");
                            break;
                        case (int)TributoRegistroBase.TiposTipoTributo.CodigoBarras:
                            dllImpostos.Comprovantes.ComprFGTS_Bradesco Comprovante = new dllImpostos.Comprovantes.ComprFGTS_Bradesco();
                            Comprovante.dImpCompr.FGTS_Bradesco.Clear();
                            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
                            Comprovante.dImpCompr.FGTS_Bradesco.AddFGTS_BradescoRow(rowGPS.GPSCodigoBarra.Substring(0, 11) + rowGPS.GPSCodigoBarra.Substring(12, 11) + rowGPS.GPSCodigoBarra.Substring(24, 11) + rowGPS.GPSCodigoBarra.Substring(36, 11),
                                                                                    (new DocBacarios.CPFCNPJ(rowGPS.GPSIdentificador)).ToString(),
                                                                                    rowGPS.GPSCodigoReceita.ToString(), rowALT.ALT_NumIdentificacao,
                                                                                    rowGPS.GPSMes.ToString().PadLeft(2, '0') + "/" + rowGPS.GPSAno,
                                                                                    rowALT.ALT_DataValidade.ToString("dd/MM/yyyy"), rowALT.ALT_IDPagamento, rowALT.ALT_IDPagamento,
                                                                                    rowALT.ALT_GPSPagamento.ToString("dd/MM/yyyy"), rowGPS.GPSVencimento.ToString("dd/MM/yyyy"),
                                                                                    rowGPS.GPSValorPrincipal, rowGPS.GPSValorJuros, 0, rowGPS.GPSValorMulta, rowALT.ALT_Valor,
                                                                                    rowALT.ALT_AutenticacaoCodBarra, rowALT.ALT_Autenticacao, rowALT.ALT_IDDocumento,
                                                                                    Convert.ToInt32(rowALT.ALT_CONAgencia).ToString().PadLeft(4, '0') + "-" + rowALT.ALT_CONDigitoAgencia,
                                                                                    rowGPS.GPSNomeClientePagador + " - ID. " + rowGPS.GPS);
                            Comprovante.CreateDocument();

                            Comprovante.ExportToPdf(DestinoPDF + @"\recibo_tributo_" + rowALT.ALT_GPS.ToString().Trim() + ".pdf");
                            break;
                    }
                    Framework.DSCentral.IMPressoesTableAdapter.Incluir(rowGPS.GPS_CON, 159, 1, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Tributo No " + rowGPS.GPS, false);
                    booRetorno = true;
                }
                else
                {
                    //Inconsistencias
                    if ((StatusArquivo)rowGPS.GPSStatus != StatusArquivo.Excluido)
                    {
                        intGPSStatusNovo = (int)StatusArquivo.Inconsistente;
                        dGPS.dGPSSt.GPSTableAdapter.AlteraStatus(intGPSStatusNovo, rowALT.ALT_IDDocumento, rowALT.ALT_Autenticacao, String.Format("{0:dd/MM/yy HH:mm:ss} - {1}", DateTime.Now, "Pagamento recusado pelo Banco (" + rowALT.ARQNomeArquivo + ")"), rowALT.ALT_GPS);
                    }
                    int codigoErro = Convert.ToInt32(rowALT.ALT_CodConsistencia.Substring(0, 2));
                    switch (codigoErro)
                    {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 49:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Erro de layout no arquivo (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Erro de layout no arquivo " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 7:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Totaliza��o dos valores inv�lida, por exemplo, inferior a R$ 10,00 (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Totaliza��o dos valores inv�lida, por exemplo, inferior a R$ 10,00 " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 46:
                        case 47:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Dados da empresa administradora inv�lidos ou inexistentes (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Dados da empresa administradora inv�lidos ou inexistentes " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 16:
                        case 17:
                        case 32:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Dados do cliente inv�lidos ou inexistentes (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Dados do cliente inv�lidos ou inexistentes " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 45:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Dados de conta de d�bito inv�lidos ou inexistentes (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Dados de conta de d�bito inv�lidos ou inexistentes " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 22:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Data para efetiva��o do d�bito inv�lida (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Data para efetiva��o do d�bito inv�lida " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 24:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Data de vencimento inv�lida (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Data de vencimento inv�lida " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 26:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Per�odo de apura��o darf inv�lido (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Per�odo de apura��o darf inv�lido " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 27:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Percentual c�lculo darf inv�lido (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Percentual c�lculo darf inv�lido " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 31:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Valores inv�lido (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Valores inv�lido " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 38:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Compet�ncia inv�lida (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Compet�ncia inv�lida " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 51:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Tributo n�o agendado (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Tributo n�o agendado " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 53:
                        case 54:
                        case 55:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Devolu��o por exclus�o ou substitui��o de conta ou exclus�o de empresa (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Devolu��o por exclus�o ou substitui��o de conta ou exclus�o de empresa " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 56:
                        case 57:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Data de pagamento menor ou maior que os limites (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Data de pagamento menor ou maior que os limites " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 59:
                        case 60:
                        case 67:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: N�mero sequencial de remessa irregular ou n�o conferindo com o Header(" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: N�mero sequencial de remessa irregular ou n�o conferindo com o Header" + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 63:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: D�bito n�o efetuado, poss�veis motivos: tributo n�o autorizado para pagamento ou saldo insuficiente para d�bito (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: D�bito n�o efetuado, poss�veis motivos: tributo n�o autorizado para pagamento ou saldo insuficiente para d�bito " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 68:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Feriado local (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Feriado local " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 70:
                        case 71:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Dados do contribuinte inv�lidos ou inexistentes (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Dados do contribuinte inv�lidos ou inexistentes " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 75:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Ano exerc�cio inv�lido (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Ano exerc�cio inv�lido " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 77:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: C�digo de barras ou d�gito inv�lido (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: C�digo de barras ou d�gito inv�lido " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 78:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: EMPRESA/CONCESSION�RIA N�O CADASTRADA (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: EMPRESA/CONCESSION�RIA N�O CADASTRADA " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 80:
                        case 81:
                        case 82:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Recebimento inv�lido, poss�veis motivos: cota �nica ou parcela vencida ou documento atrasado (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Recebimento inv�lido, poss�veis motivos: cota �nica ou parcela vencida ou documento atrasado " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 83:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Conta j� programada para pagamento (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Conta j� programada para pagamento " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 84:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Data de d�bito excede o prazo limite de pagamento (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Data de d�bito excede o prazo limite de pagamento " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 87:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Valor recebido inv�lido (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Valor recebido inv�lido " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        case 98:
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Sistema indispon�vel (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Inconsist�ncia: Sistema indispon�vel " + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.ErronaBaixa;
                            break;
                        default:
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;neon@vcommerce.com.br",
                                                                  string.Format("Retorno n�o homologado{2}\r\nArquivo:{0}\r\nARL:{1}", rowALT.ARQNomeArquivo, rowALT.ALT, strRetornos),
                                                                  "Tributo eletr�nico");
                            dGPS.dGPSSt.GPSTableAdapter.GravaObs("INCONSIST�NCIA: Retorno n�o homologado, verificar c�digos na documenta��o (" + rowALT.ARQNomeArquivo + ")" + strRetornos, rowALT.ALT_GPS);
                            rowALT.ALTSTATUS = "Retorno n�o homologado" + strRetornos;
                            rowALT.ALT_StatusN = (int)ARLStatusN.RetornoNaoHomologado;
                            break;
                    }
                    booRetorno = false;
                }

                //ATEN��O: podemos ter pagamentos (PAG) recusados e aprovados no mesmos cheque

                //Verifica se � gps de nota para checagem de status de pagamento e status de cheque
                if (rowGPS.GPSOrigem == (int)OrigemImposto.Nota)
                {
                    //Pega os dados originais do pagamento
                    dEDI.PAGamentosTableAdapter.FillByGPS(dEDI.PAGamentos, rowGPS.GPS);
                    dEDIBoletosNeon.PAGamentosRow rowPag = dEDI.PAGamentos[0];
                    int intPAG = rowPag.PAG;
                    int intPAGCHEOriginal = rowPag.PAG_CHE;
                    decimal decPAGValor = rowPag.PAGValor;

                    //Seta status do pagamento
                    int intPAGStatusNovo;
                    if ((intGPSStatusNovo == 0) && (rowGPS.GPSStatus == (int)StatusArquivo.Efetuado))
                        intPAGStatusNovo = (int)CHEStatus.Compensado;
                    else if ((intGPSStatusNovo == 0) && (rowGPS.GPSStatus == (int)StatusArquivo.Excluido))
                        intPAGStatusNovo = (int)CHEStatus.Cancelado;
                    else if (intGPSStatusNovo == (int)StatusArquivo.Excluido)
                        intPAGStatusNovo = (int)CHEStatus.Cancelado;
                    else if (intGPSStatusNovo == (int)StatusArquivo.Agendado)
                        intPAGStatusNovo = (int)CHEStatus.Retornado;
                    else if (intGPSStatusNovo == (int)StatusArquivo.Confirmado_Banco)
                        intPAGStatusNovo = (int)CHEStatus.PagamentoConfirmadoBancoPE;
                    else if (intGPSStatusNovo == (int)StatusArquivo.Inconsistente)
                        if (rowGPS.GPSStatus == (int)StatusArquivo.Agendado)
                            intPAGStatusNovo = (int)CHEStatus.PagamentoNaoEfetivadoPE;
                        else
                            intPAGStatusNovo = (int)CHEStatus.PagamentoInconsistentePE;
                    else
                        throw new NotImplementedException(string.Format("Status n�o tratado em TributoEletronico.cs GPSStatus:{0}", rowGPS.GPSStatus));
                    dEDI.PAGamentosTableAdapter.SetaStatus(intPAGStatusNovo, rowGPS.GPS);
                    dEDI.PAGamentos.AcceptChanges();

                    //Seta status do cheque se todos os pagamentos com mesmo status
                    CHEStatus strStatusNovo;
                    bool Dividir = false;
                    try
                    {
                        if (dEDI.PAGamentosTableAdapter.VerificaPAGStatusGPS(dEDI.PAGamentos, rowGPS.GPS) == 1)
                        {
                            strStatusNovo = (CHEStatus)dEDI.PAGamentos[0].PAGStatus;
                            dEDI.PAGamentosTableAdapter.SetaStatusChequeGPS(dEDI.PAGamentos[0].PAGStatus, String.Format("{0:dd/MM/yy HH:mm:ss} - {1} ({2}{3})", DateTime.Now, "Status alterado para: " + strStatusNovo.ToString(), rowALT.ALTSTATUS, dEDI.PAGamentos[0].PAGStatus == (int)CHEStatus.Cancelado ? " " + rowGPS.GPS.ToString() : ""), rowGPS.GPS);
                            dEDI.PAGamentos.AcceptChanges();
                        }
                        else
                        {
                            Dividir = true;                            
                        }
                    }
                    catch (Exception ex)
                    {
                        Dividir = true;
                        Console.WriteLine("************************************");
                        Console.WriteLine("************************************");
                        Console.WriteLine("************************************");
                    }
                    if (Dividir)
                    {
                        //Separa o pagamento caso gps inconsistente e perten�a a um cheque com mais de um pagamento
                        if ((intGPSStatusNovo == (int)StatusArquivo.Inconsistente))
                        {
                            //Separa o pagamento do cheque em um novo pagamento
                            Cheque Cheque = new Cheque(intPAGCHEOriginal, Cheque.TipoChave.CHE);
                            Cheque = Cheque.SeparaPAG(intPAG, decPAGValor);

                            //Seta status do novo cheque
                            strStatusNovo = (CHEStatus)intPAGStatusNovo;
                            dEDI.PAGamentosTableAdapter.SetaStatusChequeCHE(intPAGStatusNovo, String.Format("{0:dd/MM/yy HH:mm:ss} - {1} ({2})", DateTime.Now, "Status alterado para: " + strStatusNovo.ToString(), rowALT.ALTSTATUS), (int)Cheque.CHE);
                            dEDI.PAGamentos.AcceptChanges();

                            //Seta status do cheque original se todos os pagamentos restantes com mesmo status
                            if (dEDI.PAGamentosTableAdapter.VerificaPAGStatusCHE(dEDI.PAGamentos, intPAGCHEOriginal) == 1)
                            {
                                strStatusNovo = (CHEStatus)dEDI.PAGamentos[0].PAGStatus;
                                dEDI.PAGamentosTableAdapter.SetaStatusChequeCHE(dEDI.PAGamentos[0].PAGStatus, String.Format("{0:dd/MM/yy HH:mm:ss} - {1} ({2})", DateTime.Now, "Status alterado para: " + strStatusNovo.ToString(), rowALT.ALTSTATUS), intPAGCHEOriginal);
                                dEDI.PAGamentos.AcceptChanges();
                            }
                        }
                    }
                }

                //Fecha a transacao
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
            };

            return booRetorno;
            //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
        }
    }
}
