﻿namespace dllbanco {
    
    
    public partial class dFrancesinhaCorrecao {
        private dFrancesinhaCorrecaoTableAdapters.FranAbertaTableAdapter franAbertaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FranAberta
        /// </summary>
        public dFrancesinhaCorrecaoTableAdapters.FranAbertaTableAdapter FranAbertaTableAdapter
        {
            get
            {
                if (franAbertaTableAdapter == null)
                {
                    franAbertaTableAdapter = new dFrancesinhaCorrecaoTableAdapters.FranAbertaTableAdapter();
                    franAbertaTableAdapter.TrocarStringDeConexao();
                };
                return franAbertaTableAdapter;
            }
        }
    }
}
