﻿namespace dllbanco {
    
    
    public partial class dConciliaCaixa 
    {
        private dConciliaCaixaTableAdapters.ARquivoLidoRangeTableAdapter aRquivoLidoRangeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ARquivoLidoRange
        /// </summary>
        public dConciliaCaixaTableAdapters.ARquivoLidoRangeTableAdapter ARquivoLidoRangeTableAdapter
        {
            get
            {
                if (aRquivoLidoRangeTableAdapter == null)
                {
                    aRquivoLidoRangeTableAdapter = new dConciliaCaixaTableAdapters.ARquivoLidoRangeTableAdapter();
                    aRquivoLidoRangeTableAdapter.TrocarStringDeConexao();
                };
                return aRquivoLidoRangeTableAdapter;
            }
        }

        private dConciliaCaixaTableAdapters.CreditoRangeTableAdapter creditoRangeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CreditoRange
        /// </summary>
        public dConciliaCaixaTableAdapters.CreditoRangeTableAdapter CreditoRangeTableAdapter
        {
            get
            {
                if (creditoRangeTableAdapter == null)
                {
                    creditoRangeTableAdapter = new dConciliaCaixaTableAdapters.CreditoRangeTableAdapter();
                    creditoRangeTableAdapter.TrocarStringDeConexao();
                };
                return creditoRangeTableAdapter;
            }
        }

        private dConciliaCaixaTableAdapters.GruposRangeTableAdapter gruposRangeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: GruposRange
        /// </summary>
        public dConciliaCaixaTableAdapters.GruposRangeTableAdapter GruposRangeTableAdapter
        {
            get
            {
                if (gruposRangeTableAdapter == null)
                {
                    gruposRangeTableAdapter = new dConciliaCaixaTableAdapters.GruposRangeTableAdapter();
                    gruposRangeTableAdapter.TrocarStringDeConexao();
                };
                return gruposRangeTableAdapter;
            }
        }

        private dConciliaCaixaTableAdapters.ItensRangeTableAdapter itensRangeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ItensRange
        /// </summary>
        public dConciliaCaixaTableAdapters.ItensRangeTableAdapter ItensRangeTableAdapter
        {
            get
            {
                if (itensRangeTableAdapter == null)
                {
                    itensRangeTableAdapter = new dConciliaCaixaTableAdapters.ItensRangeTableAdapter();
                    itensRangeTableAdapter.TrocarStringDeConexao();
                };
                return itensRangeTableAdapter;
            }
        }

        private dConciliaCaixaTableAdapters.ARquivoLidoTableAdapter aRquivoLidoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ARquivoLido
        /// </summary>
        public dConciliaCaixaTableAdapters.ARquivoLidoTableAdapter ARquivoLidoTableAdapter
        {
            get
            {
                if (aRquivoLidoTableAdapter == null)
                {
                    aRquivoLidoTableAdapter = new dConciliaCaixaTableAdapters.ARquivoLidoTableAdapter();
                    aRquivoLidoTableAdapter.TrocarStringDeConexao();
                };
                return aRquivoLidoTableAdapter;
            }
        }

        private dConciliaCaixaTableAdapters.CreditoDiarioTableAdapter creditoDiarioTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CreditoDiario
        /// </summary>
        public dConciliaCaixaTableAdapters.CreditoDiarioTableAdapter CreditoDiarioTableAdapter
        {
            get
            {
                if (creditoDiarioTableAdapter == null)
                {
                    creditoDiarioTableAdapter = new dConciliaCaixaTableAdapters.CreditoDiarioTableAdapter();
                    creditoDiarioTableAdapter.TrocarStringDeConexao();
                };
                return creditoDiarioTableAdapter;
            }
        }

        private dConciliaCaixaTableAdapters.GruposTableAdapter gruposTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Grupos
        /// </summary>
        public dConciliaCaixaTableAdapters.GruposTableAdapter GruposTableAdapter
        {
            get
            {
                if (gruposTableAdapter == null)
                {
                    gruposTableAdapter = new dConciliaCaixaTableAdapters.GruposTableAdapter();
                    gruposTableAdapter.TrocarStringDeConexao();
                };
                return gruposTableAdapter;
            }
        }

        private dConciliaCaixaTableAdapters.ItensTableAdapter itensTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Itens
        /// </summary>
        public dConciliaCaixaTableAdapters.ItensTableAdapter ItensTableAdapter
        {
            get
            {
                if (itensTableAdapter == null)
                {
                    itensTableAdapter = new dConciliaCaixaTableAdapters.ItensTableAdapter();
                    itensTableAdapter.TrocarStringDeConexao();
                };
                return itensTableAdapter;
            }
        }
    }
}
