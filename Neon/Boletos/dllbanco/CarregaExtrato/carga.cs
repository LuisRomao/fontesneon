﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Framework;
using FrameworkProc.datasets;
using DocBacarios;
using dllCNAB;
using VirEnumeracoesNeon;


namespace dllbanco.CarregaExtrato
{
    /// <summary>
    /// Classe para carga de arquivos de extrato
    /// </summary>
    public class carga
    {
        /// <summary>
        /// Dataset
        /// </summary>
        public dCarga dCarga;

        /// <summary>
        /// Construtor
        /// </summary>
        public carga()
        {
            dCarga = new dCarga();            
        }

        /// <summary>
        /// Erro
        /// </summary>
        public string Erro = "";

        private DateTime Tiradatadalinha(string LinhaLida)
        {
            int dia = int.Parse(LinhaLida.Substring(80, 2));
            int mes = int.Parse(LinhaLida.Substring(82, 2));
            int ano = 2000 + int.Parse(LinhaLida.Substring(84, 2));
            return new DateTime(ano, mes, dia);
        }

        private decimal TiraValordalinha(string LinhaLida,int numero = 1)
        {            
            decimal Valor = decimal.Parse(LinhaLida.Substring(numero == 1 ? 86 : 106, 18)) / 100M;
            string sinal = LinhaLida.Substring(numero == 1 ? 104: 124, 1);
            if (sinal == "C")
                return Valor;
            if (sinal == "D")
                return -Valor;
            else
                if (LinhaLida.Substring(42, 1) == "1")
                    return -Valor;
                else
                    return Valor;
        }        

        private int BCO;
        private int PConta;
        private int PContaT;

        private TlinhaDecodificada DecodificaLinha(string LinhaLida)
        {
            if (LinhaLida.Length != 200)
                throw new Exception(string.Format("Erro de layout: tamanho esperado 200 encontrado: {0}", LinhaLida.Length));
            TlinhaDecodificada retorno = new TlinhaDecodificada();
            if (LinhaLida.Substring(0, 1) == "0")
                retorno.Tipo = TipoDeLinha.cabecalho;
            else if (LinhaLida.Substring(0, 1) == "1")
            {
                if (LinhaLida.Substring(41, 1) == "0")
                    retorno.Tipo = TipoDeLinha.saldoI;
                else if (LinhaLida.Substring(41, 1) == "1")
                    retorno.Tipo = TipoDeLinha.lancamento;
                else if (LinhaLida.Substring(41, 1) == "2")
                    retorno.Tipo = TipoDeLinha.saldoF;
                else if (LinhaLida.Substring(41, 1) == "4")
                    retorno.Tipo = TipoDeLinha.cabecalhoLancamentosFuturos;
                else if (LinhaLida.Substring(41, 1) == "5")
                    retorno.Tipo = TipoDeLinha.LancamentoFuturo;
                else
                    retorno.Tipo = TipoDeLinha.invalido;
            }
            else if (LinhaLida.Substring(0, 1) == "9")
                retorno.Tipo = TipoDeLinha.rodape;
            else
                retorno.Tipo = TipoDeLinha.invalido;            
            switch (retorno.Tipo)
            {
                case TipoDeLinha.cabecalho:
                    BCO = int.Parse(LinhaLida.Substring(76, 3));
                    if (BCO == 341)
                    {
                        PConta = 35;
                        PContaT = 5;
                    }
                    else
                    {
                        PConta = 29;
                        PContaT = 11;
                    }
                    break;
                case TipoDeLinha.saldoI:
                    retorno.Agencia = int.Parse(LinhaLida.Substring(17, 4));
                    retorno.Conta = int.Parse(LinhaLida.Substring(PConta, PContaT));
                    retorno.ContaDg = LinhaLida.Substring(40, 1);
                    if (BCO == 237)
                        retorno.Aplicacao = (LinhaLida.Substring(25, 4) == "1051");
                    else
                        retorno.Aplicacao = false;
                    retorno.Data = Tiradatadalinha(LinhaLida);
                    retorno.Valor = TiraValordalinha(LinhaLida);//V1i
                    retorno.CNPJ = (new CPFCNPJ(LinhaLida.Substring(3, 14))).ToString();
                    break;
                case TipoDeLinha.lancamento:
                    retorno.Data = Tiradatadalinha(LinhaLida);
                    retorno.Valor = TiraValordalinha(LinhaLida);
                    retorno.Categoria = int.Parse(LinhaLida.Substring(42, 3));
                    retorno.Hitorico = (BCO == 341) ? 0 : int.Parse(LinhaLida.Substring(45, 4));
                    retorno.Documento = int.Parse(LinhaLida.Substring(74, 6));
                    retorno.Descricao = LinhaLida.Substring(49, 25);
                    retorno.DescricaoComp = LinhaLida.Substring(105, 45);
                    retorno.definitiva = true;
                    if ((BCO == 237) && ((LinhaLida.Substring(185, 1) == "1")))
                        retorno.definitiva = false;
                    if ((BCO == 237) && ((LinhaLida.Substring(172, 3) == "INV")))
                        retorno.RegistroINV = true;
                    break;
                case TipoDeLinha.saldoF:
                    retorno.Data = Tiradatadalinha(LinhaLida);
                    retorno.Valor = TiraValordalinha(LinhaLida,1);//V1f     
                    if (BCO == 237)
                        retorno.Valor2 = TiraValordalinha(LinhaLida, 2);
                    string PosSaldo = LinhaLida.Substring(105, 1);
                    //if(PosSaldo != "P")
                    //    throw new Exception(string.Format("Verificar PosSaldo"));                        
                    if (BCO == 237)
                    {
                        if ((PosSaldo != "F") && (PosSaldo != "P"))
                            Erro += String.Format("\r\nNão é F : {0}", PosSaldo);
                    };
                    break;
                case TipoDeLinha.cabecalhoLancamentosFuturos:
                    break;
                case TipoDeLinha.LancamentoFuturo:
                    //Erro += String.Format("\r\nLancamento Futuro : {0:dd/MM/yyyy} {1:n2}", Tiradatadalinha(),TiraValordalinha());
                    break;
                case TipoDeLinha.rodape:
                    break;
                case TipoDeLinha.invalido:
                case TipoDeLinha.cabecalhodelote:
                    throw new Exception(string.Format("Verificar Tipo de linha"));                                            
                default:
                    break;
            }
            return retorno;
        }

        private int RegistaArquivo(string NomeArquivo, string cabecalho, int BCO)
        {
            NomeArquivo = Path.GetFileName(NomeArquivo);
            string comando = "INSERT INTO ARQuivos (ARQBanco, ARQIData, ARQNome, ARQTipo, ARQNomeArquivo)  VALUES(@P1,@P2,@P3,@P4,@P5)";
            return VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc(comando, BCO, DateTime.Now, cabecalho.Replace(" ", ""), (int)ARQTipo.Arquivo, NomeArquivo);
        }      

        /// <summary>
        /// Carrega o arquivo para a tabela temporária (formato 200)
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <param name="Gau"></param>
        /// <returns></returns>
        internal bool Carregar(string Arquivo, DevExpress.XtraEditors.ProgressBarControl Gau)
        {            
            Erro = "";            
            if (!File.Exists(Arquivo))
            {
                Erro = "Arquivo não encontrado";
                return false;
            };
            int nlinha = 1;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco carga.cs - 179",
                                                                       dCarga.TMP_SCCTableAdapter,
                                                                       dCarga.TMP_CCDTableAdapter);                                
                DateTime Atualizarem = DateTime.Now.AddSeconds(3);
                int contador = 0;                
                using (StreamReader Entrada = new StreamReader(Arquivo, Encoding.GetEncoding(1252)))
                {
                    if (Gau != null)
                    {
                        Gau.Position = 0;
                        Gau.Properties.Maximum = (int)Entrada.BaseStream.Length;
                        Gau.Visible = true;
                    };
                    string LinhaLida = Entrada.ReadLine();
                    contador += LinhaLida.Length;
                    int ARQ = 0;
                    List<TipoDeLinha> TiposEsperados = new List<TipoDeLinha>();
                    TiposEsperados.Add(TipoDeLinha.cabecalho);
                    BlocoDia Bloco = null;                    
                    while ((LinhaLida != null) && (LinhaLida != ""))
                    {                                                
                        if ((Gau != null) && (DateTime.Now > Atualizarem))
                        {
                            Gau.Position = contador;
                            Gau.Visible = true;
                            System.Windows.Forms.Application.DoEvents();                            
                            Atualizarem = DateTime.Now.AddSeconds(3);
                        }                        
                        TlinhaDecodificada linhaDecodificada = DecodificaLinha(LinhaLida);
                        //if (linhaDecodificada.Conta == 854)
                        //    Console.WriteLine("854");
                        if(!TiposEsperados.Contains(linhaDecodificada.Tipo))
                            throw new Exception(string.Format("Erro em layout\r\nArquivo: {0}\r\n    linha {1}\r\n    Tipo {2}", Arquivo, nlinha, linhaDecodificada.Tipo));                        
                        switch (linhaDecodificada.Tipo)
                        {
                            case TipoDeLinha.cabecalho:                                                                
                                ARQ = RegistaArquivo(Arquivo, LinhaLida,BCO);
                                TiposEsperados.Clear();
                                TiposEsperados.Add(TipoDeLinha.saldoI);
                                TiposEsperados.Add(TipoDeLinha.rodape);
                                break;
                            case TipoDeLinha.saldoI:
                                //bool retroativo = false;
                                DateTime? DataDados = null;
                                if(BCO == 237) 
                                {
                                    string NomeArquivo = System.IO.Path.GetFileName(Arquivo);
                                    if ((NomeArquivo.Length != 13) || (NomeArquivo.Substring(0, 2) != "CC") || (NomeArquivo.Substring(6, 1) != "A") || (NomeArquivo.Substring(9, 4) != ".RET"))
                                        throw new Exception(string.Format("Nome de arquivo inválido: {0}", NomeArquivo));
                                    DataDados = new DateTime(DateTime.Today.Year,int.Parse(NomeArquivo.Substring(4,2)),int.Parse(NomeArquivo.Substring(2,2))).AddDays(-1);
                                    //if(linhaDecodificada.Data < DataDados)
                                    //    retroativo = true;
                                };
                                Bloco = new BlocoDia(dCarga, ARQ,BCO,linhaDecodificada,DataDados);                                
                                TiposEsperados.Clear();
                                TiposEsperados.Add(TipoDeLinha.lancamento);
                                TiposEsperados.Add(TipoDeLinha.saldoF);                                         
                                break;
                            case TipoDeLinha.lancamento:
                                if (!linhaDecodificada.RegistroINV)
                                {
                                    Bloco.Lancamento(linhaDecodificada.Data, linhaDecodificada.Descricao, linhaDecodificada.DescricaoComp, linhaDecodificada.Documento, linhaDecodificada.Categoria, linhaDecodificada.Hitorico, linhaDecodificada.Valor, linhaDecodificada.definitiva);
                                    TiposEsperados.Clear();
                                    TiposEsperados.Add(TipoDeLinha.lancamento);
                                    TiposEsperados.Add(TipoDeLinha.saldoF);
                                }
                                break;
                            case TipoDeLinha.saldoF:
                                //if (BCO == 237)
                                //    Bloco.Fechamento(linhaDecodificada.Valor, linhaDecodificada.Valor2);
                                //else
                                //    Bloco.Fechamento();
                                Bloco.Grava();
                                TiposEsperados.Clear();
                                TiposEsperados.Add(TipoDeLinha.saldoI);
                                TiposEsperados.Add(TipoDeLinha.rodape);                                
                                break;
                            case TipoDeLinha.rodape:
                                break;
                            case TipoDeLinha.invalido:
                            default:
                                throw new Exception(String.Format("Registro inválido\r\n    Arquivo:{0}\r\n    Linha:{1}", Arquivo, nlinha));                                
                            //case TipoDeLinha.cabecalhodelote:
                            //    break;                                                            
                        }                        
                        LinhaLida = Entrada.ReadLine();
                        if (LinhaLida != null)
                            contador += LinhaLida.Length;
                        nlinha++;                        
                    };                    
                    if (Gau != null)                    
                        Gau.Visible = false;                                        

                };                                
                VirMSSQL.TableAdapter.STTableAdapter.Commit();                                
            }
            catch (Exception e)
            {
                Erro += e.Message;
                VirException.cVirException virE = new VirException.cVirException(string.Format("Erro em TryTrataArquivoExtrato.\r\n    Arquivo: {0}\r\n    Linha: {1}",Arquivo, nlinha), e);
                virE.AvisarUsuario = false;
                VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(virE);
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                return false;
            };
            /*
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU == 30)
            {
                string Relat = CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();
                System.Windows.Forms.Clipboard.SetText(Relat);
                System.Windows.Forms.MessageBox.Show(Relat);                
            }
            */
            return true;
        }

        /// <summary>
        /// Carrega o arquivo para a tabela temporária (CNAB)
        /// </summary>
        /// <param name="cnab"></param>
        /// <param name="Gau"></param>
        /// <returns></returns>
        internal bool Carregar(CNAB240 cnab, DevExpress.XtraEditors.ProgressBarControl Gau)
        {
            //Documentação no onenote item "Retornos Bradesco com conta plus"
            try
            {                
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("carga.cs Carregar - 347",dCarga.TMP_SCCTableAdapter,
                                                                                dCarga.TMP_CCDTableAdapter);                             
                int ARQ = RegistaArquivo(cnab.NomeArquivo, cnab.Header.LinhaLida, cnab.Header.Banco);
                if (Gau != null)
                {
                    Gau.Position = 0;
                    Gau.Properties.Maximum = cnab.LotesExtrato.Count;
                    Gau.Visible = true;
                };
                DateTime Atualizarem = DateTime.Now.AddSeconds(3);
                int contador = 1;
                BlocoDia Bloco = null;                    
                foreach (regHeaderLote_1_E_04 lote in cnab.LotesExtrato)
                {
                    contador++;
                    if ((Gau != null) && (DateTime.Now > Atualizarem))
                    {
                        Gau.Position = contador;
                        Gau.Visible = true;
                        System.Windows.Forms.Application.DoEvents();                        
                        Atualizarem = DateTime.Now.AddSeconds(3);
                    }
                    bool LoteAplicacao = false;
                    switch (cnab.Header.Banco)
                    {
                        case 104:
                            LoteAplicacao = ((lote.conta / 10000000000) == 1);
                            break;
                        case 341:
                            if (lote.strConvenioE.Substring(0, 4) == "0202")
                                continue;
                            //Alguna arquivos estão trazendo informações referentes as aplicações internas. poderia ser usado para ajustar o saldo mas no momento está sendo ignorado. 
                            break;
                            
                    }
                    Bloco = new BlocoDia(dCarga,
                                         ARQ,
                                         cnab.Header.Banco,
                                         lote.agencia,
                                         int.Parse(lote.djagencia),
                                         (cnab.Header.Banco == 104) ? (int)(lote.conta % 1000000) : (int)lote.conta,
                                         lote.djconta,
                                         LoteAplicacao,                                         
                                         lote.CNPJ.ToString(),                                         
                                         lote.ValorInicialcomsinal,
                                         //ATEMÇÃO a datadados só está sendo gravada para o banco 104 por não ter sido testado para os demais
                                         lote.Banco == 104 ? lote.DataInicial : (DateTime?)null
                                         );
                    
                    foreach (regDetalhe_3_E Lancamento in lote.Lancamentos)
                    {
                        int nDoc = 0;
                        int.TryParse(Lancamento.Documento, out nDoc);
                        Bloco.Lancamento(Lancamento.DataLancamento,
                                         Lancamento.Historico,
                                         Lancamento.Complemento,
                                         nDoc,
                                         Lancamento.Categoria,
                                         int.Parse(Lancamento.CodigoHistorico),
                                         Lancamento.Valorcomsinal,
                                         (Lancamento.TipoDeLancamentoExtrato != TiposDeLancamentoExtrato.futuro));
                        //dCarga.TMP_CCDRow novaCCD = dCarga.TMP_CCD.NewTMP_CCDRow();
                        //novaCCD.TMP_SCCRow = novaSCC;
                        //novaCCD.TMPCategoria = Lancamento.Categoria;
                        //novaCCD.TMPDescricao = Lancamento.Historico;
                        //novaCCD.TMPDescricaoComp = Lancamento.Complemento;
                        //novaCCD.TMPDocumento = int.Parse(Lancamento.Documento);
                        //novaCCD.TMPHitorico = int.Parse(Lancamento.CodigoHistorico);
                        //novaCCD.TMPDefinitivo = (Lancamento.TipoDeLancamentoExtrato != TiposDeLancamentoExtrato.futuro);
                        //novaCCD.TMPValor = Lancamento.Valorcomsinal;
                        //novaCCD.TMPData = Lancamento.DataLancamento;
                        //dCarga.TMP_CCD.AddTMP_CCDRow(novaCCD);

                        //if (Lancamento.TipoDeLancamentoExtrato == TiposDeLancamentoExtrato.Retroativo)                                                    
                          //  throw new Exception("retroativo");                        
                    };
                    //Bloco.Fechamento();
                    Bloco.Grava();
                    //dCarga.TMP_SCCTableAdapter.Update(novaSCC);
                    //dCarga.TMP_CCDTableAdapter.Update(dCarga.TMP_CCD);
                    //novaSCC.AcceptChanges();                                                                
                }
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                VirEmailNeon.EmailDiretoNeon.EmalST.EnviarArquivos("luis@virweb.com.br", string.Format("ERRO NO TRARAMENTO DO ARQUIVO:{0}", e.Message), "Arquivo CNAB para analizar", cnab.NomeArquivo);                
                Erro += e.Message;
                VirException.cVirException virE = new VirException.cVirException("Erro em CarregaNovo", e);
                virE.AvisarUsuario = false;
                VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(virE);
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                return false;
            };
            return true;
        }

        private Extrato.dExtrato dExtrato;

        private dllbanco.Extrato.dExtrato.ContaCorrenTeRow BuscaCCT(int BCO, int Agencia, int Conta, string ContaDg, bool Aplicacao, CPFCNPJ CNPJ)
        {
            int? CCT = dExtrato.BuscaCCT(Aplicacao, BCO, Agencia, Conta);
            int? CON;
            dllbanco.Extrato.dExtrato.ContaCorrenTeRow rowCCT = null;
            if (!CCT.HasValue)
            {
                CCTTipo = Aplicacao ? CCTTipo.Aplicacao : CCTTipo.ContaCorrete;
                CON = null;
                if (CNPJ != null)
                {
                    //dExtrato.ContaCorrenTe.Clear();
                    if (CNPJ.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                        CON = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CON from condominios where CONCNPJ = @P1 and constatus <> 0", CNPJ.ToString());
                    if (CON.HasValue)
                    {
                        rowCCT = dExtrato.ContaCorrenTe.NewContaCorrenTeRow();
                        rowCCT.CCT_BCO = BCO;
                        rowCCT.CCTAgencia = Agencia;
                        rowCCT.CCTConta = Conta;
                        rowCCT.CCTContaDg = ContaDg;
                        rowCCT.CCT_CON = CON.Value;
                        rowCCT.CCTAplicacao = Aplicacao;
                        rowCCT.CCTTitulo = "Conta Corrente";
                        rowCCT.CCTTipo = (int)CCTTipo;
                        rowCCT.CCTAtiva = true;
                        rowCCT.CCTPrincipal = false;
                        dExtrato.ContaCorrenTe.AddContaCorrenTeRow(rowCCT);
                        dExtrato.ContaCorrenTeTableAdapter.Update(rowCCT);

                        //CCT = VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc("insert ContaCorrenTe (CCT_BCO,CCTAgencia,CCTConta,CCTContaDg,CCT_CON,CCTPrincipal,CCTAplicacao,CCTTitulo,CCTTipo) values (@P1,@P2,@P3,@P4,@P5,1,0,@P6,0)",
                        //        BCO,
                          //      Agencia,
                            //    Conta,
                              //  ContaDg,
                                //CON.Value,
                           //     "Conta Corrente",
                             //   Aplicacao
                               // );
                        //dExtrato.ContaCorrenTeTableAdapter.FillTodas(dExtrato.ContaCorrenTe);
                    }
                    /*
                    else
                        CCT = VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc("insert ContaCorrenTe (CCT_BCO,CCTAgencia,CCTConta,CCTContaDg,CCT_CON,CCTPrincipal,CCTAplicacao,CCTTitulo,CCTTipo) values (@P1,@P2,@P3,@P4,@P5,1,0,@P6,0)",
                                BCO,
                                Agencia,
                                Conta,
                                ContaDg,
                                DBNull.Value,
                                "Conta Corrente",
                                Aplicacao
                                );*/
                };
                //dExtrato.ContaCorrenTeTableAdapter.EmbarcaEmTransST();
                //dExtrato.ContaCorrenTeTableAdapter.FillTodas(dExtrato.ContaCorrenTe);
            }
            
            if (CCT.HasValue)
            {
                rowCCT = dExtrato.ContaCorrenTe.FindByCCT(CCT.Value);
                CCTTipo = (CCTTipo)rowCCT.CCTTipo;
                if (rowCCT.IsCCT_CONNull())
                {
                    CON = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CON from condominios where CONCNPJ = @P1", CNPJ.ToString());
                    if (CON.HasValue)
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ContaCorrente set CCT_CON = @P1 where CCT = @P2", CON, CCT);
                }
            }
            return rowCCT;
        }

        /// <summary>
        /// Carrega os dados
        /// </summary>
        public void CarregaTMP()
        {
            if (dExtrato == null)
                dExtrato = new Extrato.dExtrato();
            dCarga.TMP_CCD.Clear();
            dCarga.TMP_SCCTableAdapter.Fill(dCarga.TMP_SCC);
            dCarga.TMP_CCDTableAdapter.Fill(dCarga.TMP_CCD);
            //substituir por Framework.datasets.dContasLogicas.dContasLogicasSt                        
            dExtrato.ContaCorrenTeTableAdapter.FillTodas(dExtrato.ContaCorrenTe);
        }

        /// <summary>
        /// Carrega os dados da tabela TMP para o local definitivo
        /// </summary>
        public string CarregarSaldos(ref string strLocal, ref int iTotal,ref int iAtual)
        {
            if (DateTime.Today < new DateTime(2016, 4, 15))
                AbreLog();
            strLocal = "TMP -> SCC";
            iTotal = 0;
            iAtual = 0;
            DateTime dataInicio = DateTime.Now;                                    
            CarregaTMP();                        
            List<dCarga.TMP_SCCRow> TMPSCCs = new List<dCarga.TMP_SCCRow>();
            foreach (dCarga.TMP_SCCRow TMPSCCrow in dCarga.TMP_SCC)
                TMPSCCs.Add(TMPSCCrow);            
            iTotal = TMPSCCs.Count;            
            foreach (dCarga.TMP_SCCRow TMPSCCrow in TMPSCCs)
            {                
                if (!UnidadeCarga(TMPSCCrow))                                    
                    break;                
                iAtual++;
            };
            strLocal = "TMP -> SCC terminado";
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            dContasLogicas.LimpaSt();
            GravaLog();
            return string.Format("({0:n0}.{1:000} s) TMP -> SCC: {2} {3}/{4}\r\n{5}\r\n",
                                 TempoTotal.TotalSeconds,
                                 TempoTotal.Milliseconds,
                                 UltimoErro == null ? "ok" : "ERRO",
                                 iAtual, 
                                 iTotal,
                                 UltimoErro == null ? "" : VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(UltimoErro,false,true,false));               
        }

        private Exception UltimoErro;

        /*
        [Obsolete("Centralizar")]
        private bool ETransInterna(Extrato.dExtrato.ContaCorrenteDetalheRow CCDrow)
        {
            if (BCO == 341)
            {
                if ((CCDrow.CCDCategoria == 206) && (CCDrow.CCDCodHistorico == 48) && (CCDrow.CCDDescricao.Trim() == "RES APLIC AUT MAIS"))
                    return true;
                if ((CCDrow.CCDCategoria == 106) && (CCDrow.CCDCodHistorico == 45) && (CCDrow.CCDDescricao.Trim() == "APL APLIC AUT MAIS"))
                    return true;
            }
            return false;
        }*/

        private CCTTipo CCTTipo;


        /*
public bool UnidadeCarga(dCarga.TMP_SCCRow TMPSCCrow)
        {            
            int SCC = 0;
            Extrato.dExtrato.SaldoContaCorrenteRow SCCrowAnterior;
            Extrato.dExtrato.SaldoContaCorrenteRow SCCrowAtual;
            Extrato.dExtrato.SaldoContaCorrenteRow SCCrowProxima;            
            DateTime DataA;
            decimal ValorA;
            decimal ValorApA;
            SCC = TMPSCCrow.TMPSCC;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco carga.cs - 340", dCarga.TMP_SCCTableAdapter,
                                                                                 dCarga.TMP_CCDTableAdapter,
                                                                                 dExtrato.SaldoContaCorrenteTableAdapter,
                                                                                 dExtrato.ContaCorrenteDetalheTableAdapter);
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Saldo I");
                BCO = TMPSCCrow.TMP_BCO;
                int CCT = BuscaCCT(TMPSCCrow.TMP_BCO, TMPSCCrow.TMPAgencia, TMPSCCrow.TMPConta, TMPSCCrow.TMPContaDg, TMPSCCrow.TMPAplicacao, new CPFCNPJ(TMPSCCrow.TMPCNPJ));
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Saldo I 1");
                SCCrowAnterior = dExtrato.DataAnterior_SCC(CCT,BCO, TMPSCCrow.TMPData);
                if (SCCrowAnterior != null)
                {
                    DataA = SCCrowAnterior.SCCData;
                    ValorA = SCCrowAnterior.SCCValorF;
                    ValorApA = SCCrowAnterior.SCCValorApF;
                }
                else
                {
                    DataA = TMPSCCrow.TMPData;
                    ValorA = 0;
                    ValorApA = 0;
                }
                //bool PermiteAbertos = true;
                dCarga.TMP_CCDRow[] TMPCCDrows = TMPSCCrow.GetTMP_CCDRows();
                if (TMPCCDrows.Length > 0)
                {                    
                    //DateTime DataCorrente = TMPCCDrows[0].TMPData;
                    decimal SaldoCorrente = TMPSCCrow.TMPValorI;                    
                    SCCrowAtual = dExtrato.BuscaSCC(CCT, TMPSCCrow.TMPData);
                    if (SCCrowAtual == null)
                    {
                        SCCrowAtual = dExtrato.SaldoContaCorrente.NewSaldoContaCorrenteRow();
                        SCCrowAtual.SCC_CCT = CCT;
                        SCCrowAtual.SCCData = TMPSCCrow.TMPData;
                        SCCrowAtual.SCC_ARQ = TMPSCCrow.TMP_ARQ;
                        SCCrowAtual.SCCValorI = TMPSCCrow.TMPValorI;
                        if (SCCrowAnterior != null)
                                SCCrowAtual.SCCValorApI = SCCrowAnterior.SCCValorApF;
                        else
                                SCCrowAtual.SCCValorApI = 0;                        
                        SCCrowAtual.SCCValorF = TMPSCCrow.TMPValorI;
                        SCCrowAtual.SCCValorApF = SCCrowAtual.SCCValorApI;
                        SCCrowAtual.SCCValidoAnterior = (ValorA == TMPSCCrow.TMPValorI);
                        SCCrowAtual.SCCDataA = DataA;
                        SCCrowAtual.SCCAberto = false;
                        dExtrato.SaldoContaCorrente.AddSaldoContaCorrenteRow(SCCrowAtual);
                    }
                    else
                    {
                        dExtrato.Limpa(SCCrowAtual,BCO);
                        if (SCCrowAtual.SCCValorF != TMPSCCrow.TMPValorI)
                        {
                            if (ValorA == TMPSCCrow.TMPValorI)
                            {
                                SCCrowAtual.SCCValorI = ValorA;
                                SCCrowAtual.SCCValorF = ValorA;
                                SCCrowAtual.SCCValorApI = ValorApA;
                                SCCrowAtual.SCCValorApF = ValorApA;
                                foreach (Extrato.dExtrato.ContaCorrenteDetalheRow CCDrowSomar in SCCrowAtual.GetContaCorrenteDetalheRows())
                                {
                                    SCCrowAtual.SCCValorF += CCDrowSomar.CCDValor;
                                    if (dllbanco.Extrato.diaExtrato.TransferenciaInterna(BCO, CCDrowSomar.CCDDescricao, CCDrowSomar.CCDCategoria, CCDrowSomar.CCDCodHistorico))
                                    {
                                        SCCrowAtual.SCCValorApF -= CCDrowSomar.CCDValor;                                        
                                    }
                                }
                            }
                            else
                            {
                                SCCrowAtual.SCCValorI = TMPSCCrow.TMPValorI;
                                SCCrowAtual.SCCValorF = TMPSCCrow.TMPValorI;
                                foreach (Extrato.dExtrato.ContaCorrenteDetalheRow CCDrowSomar in SCCrowAtual.GetContaCorrenteDetalheRows())
                                {
                                    SCCrowAtual.SCCValorI -= CCDrowSomar.CCDValor;
                                    if (dllbanco.Extrato.diaExtrato.TransferenciaInterna(BCO,  CCDrowSomar.CCDDescricao, CCDrowSomar.CCDCategoria, CCDrowSomar.CCDCodHistorico))
                                    //if (ETransInterna(CCDrowSomar))
                                        SCCrowAtual.SCCValorApI += CCDrowSomar.CCDValor;
                                }
                            }
                        }
                    }
                    SCCrowAtual.SCCAberto = false;
                    foreach (dCarga.TMP_CCDRow TMPCCDrow in TMPSCCrow.GetTMP_CCDRows())
                    {                        
                        if (TMPCCDrow.TMPData > SCCrowAtual.SCCData)
                        {
                            SCCrowAtual.SCCValidoConteudo = (SCCrowAtual.SCCValorF == SaldoCorrente);
                            SCCrowProxima = dExtrato.BuscaSCC(CCT, TMPCCDrow.TMPData);
                            if (SCCrowProxima == null)
                            {
                                SCCrowProxima = dExtrato.SaldoContaCorrente.NewSaldoContaCorrenteRow();
                                SCCrowProxima.SCC_ARQ = TMPSCCrow.TMP_ARQ;
                                SCCrowProxima.SCC_CCT = CCT;
                                SCCrowProxima.SCCAberto = true;
                                SCCrowProxima.SCCData = TMPCCDrow.TMPData;
                                SCCrowProxima.SCCDataA = SCCrowAtual.SCCData;
                                SCCrowProxima.SCCValidoAnterior = true;
                                SCCrowProxima.SCCValidoConteudo = true;
                                SCCrowProxima.SCCValorI = SCCrowAtual.SCCValorF;
                                SCCrowProxima.SCCValorF = SCCrowAtual.SCCValorF;
                                SCCrowProxima.SCCValorApI = SCCrowAtual.SCCValorApF;
                                SCCrowProxima.SCCValorApF = SCCrowAtual.SCCValorApF;
                                dExtrato.SaldoContaCorrente.AddSaldoContaCorrenteRow(SCCrowProxima);
                            }
                            else
                            {
                                SCCrowProxima.SCCValorI = SCCrowAtual.SCCValorF; 
                                SCCrowProxima.SCCValorApI = SCCrowAtual.SCCValorApF;                                
                                dExtrato.Limpa(SCCrowProxima,BCO);
                            }
                            //RemoveFantasmas(CCT,SCCrowAtual.sccdata = SCCrowProxima.sccdata);
                            SCCrowAtual = SCCrowProxima;
                        }
                        //if (TMPCCDrow.TMPDefinitivo || (SCCrowAtual.SCCData >= dExtrato.UltimaData(SCCrowAtual.SCC_CCT)))
                        if ((SCCrowAtual.SCCData >= dExtrato.UltimaData(SCCrowAtual.SCC_CCT)))
                        {
                            bool TranfInt = (dllbanco.Extrato.diaExtrato.TransferenciaInterna(BCO, TMPCCDrow.TMPDescricao, TMPCCDrow.TMPCategoria, TMPCCDrow.TMPHitorico));
                            if (TranfInt && (CCTTipo == Framework.CCTTipo.ContaCorrete))
                            {
                                CCTTipo = Framework.CCTTipo.ContaCorrete_com_Oculta;
                                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update contacorrente set ccttipo = @P1 where CCT = @P2", (int)CCTTipo, CCT);
                            }
                            RegistraLancamento(SCCrowAtual, TMPSCCrow.TMP_ARQ, TMPCCDrow, false,TranfInt);
                            SaldoCorrente += TMPCCDrow.TMPValor;
                        }
                    }
                    SCCrowAtual.SCCValidoConteudo = (SCCrowAtual.SCCValorF == TMPSCCrow.TMPValorF);                                        
                }
                TMPSCCrow.Delete();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("UPDATE");
                dCarga.TMP_SCCTableAdapter.Update(dCarga.TMP_SCC);
                dCarga.TMP_SCC.AcceptChanges();                                
                dExtrato.SaldoContaCorrenteTableAdapter.Update(dExtrato.SaldoContaCorrente);
                dExtrato.SaldoContaCorrente.AcceptChanges();
                dExtrato.ContaCorrenteDetalheTableAdapter.Update(dExtrato.ContaCorrenteDetalhe);                
                dExtrato.ContaCorrenteDetalhe.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("GERAL");
            }
            catch (Exception e)
            {
                UltimoErro = new Exception(string.Format("Erro em CarregarSaldos TMP_SCC = {0}", SCC), e);
                VirMSSQL.TableAdapter.VircatchSQL(e);                                
            }
            return true;
        }
        */

        private StringBuilder LOG;

        /// <summary>
        /// Abre registro de log
        /// </summary>
        public void AbreLog()
        {
            LOG = new StringBuilder();
        }

        /// <summary>
        /// inclui informações no log
        /// </summary>
        /// <param name="Linha"></param>
        /// <param name="Dados"></param>
        private void GravaLog(string Linha,params object[] Dados)
        {
            if (LOG == null)
                return;
            LOG.Append(Linha);
            foreach (object Dado in Dados)
            {
                LOG.Append(" - ");
                LOG.Append(Dado);
            }
            LOG.Append("\r\n");
        }

        /// <summary>
        /// Grava log em arquivo
        /// </summary>
        public void GravaLog()
        {
            if (LOG == null)
                return;
            if (LOG.Length == 0)
                return;
            string CaminhoLocal = Path.GetDirectoryName(Environment.CommandLine.Replace("\"", ""));
            if (!Directory.Exists(CaminhoLocal + @"\log"))
                Directory.CreateDirectory(CaminhoLocal + @"\log");
            string Arquivo = string.Format("{0}\\log\\CARGA_{1:dd_MM_yyyy_HH_mm}.log",CaminhoLocal,DateTime.Now);
            File.WriteAllText(Arquivo, LOG.ToString());
        }
    
        

        /// <summary>
        /// Carrega uma linha do TMPSCC
        /// </summary>
        /// <param name="TMPSCCrow"></param>
        /// <returns></returns>
        public bool UnidadeCarga(dCarga.TMP_SCCRow TMPSCCrow)
        {
            GravaLog("****************");
            GravaLog("Data Banco Conta Aplicação Retroativo", TMPSCCrow.TMPData, TMPSCCrow.TMP_BCO, TMPSCCrow.TMPConta, TMPSCCrow.TMPAplicacao,TMPSCCrow.TMPRetroativo);
            if (TMPSCCrow.GetTMP_CCDRows().Length == 0)
            {
                TMPSCCrow.Delete();
                dCarga.TMP_SCCTableAdapter.Update(dCarga.TMP_SCC);
                GravaLog("Zerado");
                return true;
            };
            bool PropagarCalculos = false;
            int SCC = 0;
            Extrato.dExtrato.SaldoContaCorrenteRow SCCrowAnterior;
            Extrato.dExtrato.SaldoContaCorrenteRow SCCrowAtual;
            //Extrato.dExtrato.SaldoContaCorrenteRow SCCrowProxima;            
            DateTime DataA;
            decimal ValorA;
            decimal ValorApA;
            decimal jurosApurado;
            //SCC = TMPSCCrow.TMPSCC;
            try
            {

                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco carga.cs - 340", dCarga.TMP_SCCTableAdapter,
                                                                                 dCarga.TMP_CCDTableAdapter,
                                                                                 dExtrato.SaldoContaCorrenteTableAdapter,
                                                                                 dExtrato.ContaCorrenteDetalheTableAdapter);
                BCO = TMPSCCrow.TMP_BCO;
                jurosApurado = 0;
                Extrato.dExtrato.ContaCorrenTeRow CCTrow = BuscaCCT(TMPSCCrow.TMP_BCO, TMPSCCrow.TMPAgencia, TMPSCCrow.TMPConta, TMPSCCrow.TMPContaDg, TMPSCCrow.TMPAplicacao, new CPFCNPJ(TMPSCCrow.TMPCNPJ));
                if (CCTrow == null)
                {
                    TMPSCCrow.Delete();
                    dCarga.TMP_SCCTableAdapter.Update(dCarga.TMP_SCC);
                    dCarga.TMP_SCC.AcceptChanges();
                }
                else
                {

                    bool ContaComPlus = (TMPSCCrow.TMP_BCO == 237) && (CCTrow.IsCCT_CCTPlusNull()) && (!TMPSCCrow.TMPAplicacao);
                    int? CCTPlus = CCTrow.IsCCT_CCTPlusNull() ? (int?)null : CCTrow.CCT_CCTPlus;
                    int CCT = CCTrow.CCT;
                    GravaLog("dExtrato.DataAnterior_SCC", CCT, BCO, TMPSCCrow.TMPData);
                    SCCrowAnterior = dExtrato.DataAnterior_SCC(CCT, BCO, TMPSCCrow.TMPData);
                    if (SCCrowAnterior != null)
                    {
                        GravaLog("Encontrado", SCCrowAnterior.SCCData);
                        DataA = SCCrowAnterior.SCCData;
                        //if (SCCrowAnterior.SCCValorApF != 0)
                        if ((!TMPSCCrow.TMPAplicacao) && (BCO != 104))
                        {
                            GravaLog("Não Aplicação ValorA ValorApA", SCCrowAnterior.SCCValorF, SCCrowAnterior.SCCValorApF);
                            ValorA = SCCrowAnterior.SCCValorF;
                            ValorApA = SCCrowAnterior.SCCValorApF;
                        }
                        else
                        {
                            GravaLog("Aplicação Adotado como ValorA", TMPSCCrow.TMPValorI);
                            ValorA = TMPSCCrow.TMPValorI;
                            ValorApA = 0;
                        }
                    }
                    else
                    {
                        GravaLog("Não Encontrado");
                        DataA = TMPSCCrow.TMPData;
                        ValorA = TMPSCCrow.TMPValorI;
                        ValorApA = 0;
                    }
                    GravaLog("dExtrato.BuscaSCC", CCT, TMPSCCrow.TMPData);
                    SCCrowAtual = dExtrato.BuscaSCC(CCT, TMPSCCrow.TMPData);

                    if (SCCrowAtual != null)
                    {
                        GravaLog("Encontrado");
                        dExtrato.Limpa(SCCrowAtual);
                    }

                    if (SCCrowAtual == null)
                    {
                        GravaLog("SCCrowAtual Não Encontrado");
                        SCCrowAtual = dExtrato.SaldoContaCorrente.NewSaldoContaCorrenteRow();
                        SCCrowAtual.SCC_CCT = CCT;
                        SCCrowAtual.SCCData = TMPSCCrow.TMPData;
                        SCCrowAtual.SCC_ARQ = TMPSCCrow.TMP_ARQ;
                        SCCrowAtual.SCCValorI = ValorA;
                        SCCrowAtual.SCCValorApI = ValorApA;
                        SCCrowAtual.SCCValorF = ValorA;
                        SCCrowAtual.SCCValorApF = ValorApA;
                        SCCrowAtual.SCCValidoAnterior = true;

                        //if (BCO == 237)
                        switch (BCO)
                        {
                            case 237:
                                if (!TMPSCCrow.TMPRetroativo)
                                {
                                    GravaLog("Não Retroativa SCCValArquivo jurosApurado", TMPSCCrow.TMPValorI, TMPSCCrow.TMPValorI - (ValorA + ValorApA));
                                    SCCrowAtual.SCCValArquivo = TMPSCCrow.TMPValorI;
                                    jurosApurado = TMPSCCrow.TMPValorI - (ValorA + ValorApA);
                                }
                                else
                                {
                                    SCCrowAtual.SCCValidoAnterior = false;
                                }
                                break;

                            case 104:
                                if (!TMPSCCrow.TMPRetroativo)
                                {
                                    GravaLog("Não Retroativa");
                                    SCCrowAtual.SCCValArquivo = TMPSCCrow.TMPValorI;
                                    SCCrowAtual.SCCValidoAnterior = SCCrowAtual.SCCValorI == SCCrowAnterior.SCCValorF;
                                }
                                else
                                {
                                    SCCrowAtual.SCCValidoAnterior = false;
                                }
                                break;

                            default:
                                if (TMPSCCrow.TMPValorI != ValorA)
                                    SCCrowAtual.SCCValidoAnterior = false;
                                break;

                        }
                        SCCrowAtual.SCCDataA = DataA;
                        SCCrowAtual.SCCAberto = false;
                        dExtrato.SaldoContaCorrente.AddSaldoContaCorrenteRow(SCCrowAtual);
                    }
                    else
                    {
                        GravaLog("SCCrowAtual Encontrado");
                        if (BCO == 237)
                        {
                            if (!TMPSCCrow.TMPRetroativo)
                                PropagarCalculos = true;
                        }
                        else
                            PropagarCalculos = true;
                    }

                    //Ajuste dos juros
                    if (jurosApurado != 0)
                    {
                        //decimal jurosApurado = SCCrowAtual.SCCValorApI - SCCrowAnterior.SCCValorApF;
                        decimal Teto = Math.Abs(SCCrowAtual.SCCValorApI + SCCrowAtual.SCCValorI) * 0.1M / 100;
                        if (Teto < 100)
                            Teto = 100;
                        GravaLog("Juros Teto JurosApurado", Teto, jurosApurado);
                        if ((jurosApurado != 0) && (Math.Abs(jurosApurado) < Teto))
                        {
                            SCCrowAnterior.SCCValorApF += jurosApurado;
                            SCCrowAtual.SCCValorApI += jurosApurado;
                            SCCrowAtual.SCCValorApF += jurosApurado;
                            Extrato.dExtrato.ContaCorrenteDetalheRow novaCCD = dExtrato.ContaCorrenteDetalhe.NewContaCorrenteDetalheRow();
                            novaCCD.CCD_ARQ = TMPSCCrow.TMP_ARQ;
                            novaCCD.CCD_SCC = SCCrowAnterior.SCC;
                            novaCCD.CCDCategoria = 0;
                            novaCCD.CCDCodHistorico = 0;
                            novaCCD.CCDConsolidado = (int)statusconsiliado.Automatico;
                            novaCCD.CCDDescricao = jurosApurado > 0 ? "Rendimentos" : "IR Rendimentos";
                            novaCCD.CCDDescComplemento = "";
                            novaCCD.CCDDocumento = 0;
                            novaCCD.CCDValidado = true;
                            novaCCD.CCDValor = jurosApurado;
                            novaCCD.CCDTipoLancamento = (int)TipoLancamentoNeon.RendimentoContaInvestimento;
                            dExtrato.ContaCorrenteDetalhe.AddContaCorrenteDetalheRow(novaCCD);
                            //SCCrowAtual.SCCValorApI += jurosApurado;
                            //SCCrowAtual.SCCValorApF += jurosApurado;
                        }
                    }
                    
                    dCarga.TMP_CCDRow[] TMPCCDrows = TMPSCCrow.GetTMP_CCDRows();
                    if (TMPCCDrows.Length > 0)
                    {
                        SCCrowAtual.SCCAberto = false;
                        foreach (dCarga.TMP_CCDRow TMPCCDrow in TMPSCCrow.GetTMP_CCDRows())
                        {
                            if (TMPCCDrow.TMPTransfInterna && (CCTTipo == CCTTipo.ContaCorrete) && (TMPSCCrow.TMP_BCO == 341))
                            {
                                CCTTipo = CCTTipo.ContaCorrete_com_Oculta;
                                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update contacorrente set ccttipo = @P1 where CCT = @P2", (int)CCTTipo, CCT);
                            }
                            RegistraLancamento(SCCrowAtual, TMPSCCrow.TMP_ARQ, TMPCCDrow, true);
                        }
                    }
                    TMPSCCrow.Delete();
                    dCarga.TMP_SCCTableAdapter.Update(dCarga.TMP_SCC);
                    dCarga.TMP_SCC.AcceptChanges();
                    dExtrato.SaldoContaCorrenteTableAdapter.Update(dExtrato.SaldoContaCorrente);
                    dExtrato.SaldoContaCorrente.AcceptChanges();
                    dExtrato.ContaCorrenteDetalheTableAdapter.Update(dExtrato.ContaCorrenteDetalhe);
                    dExtrato.ContaCorrenteDetalhe.AcceptChanges();
                }
                VirMSSQL.TableAdapter.CommitSQL();                
            }
            catch (Exception e)
            {
                Exception eX = e;
                while (eX != null)
                {
                    GravaLog("catch", e.Message);
                    GravaLog(e.StackTrace);
                    eX = eX.InnerException;
                }
                UltimoErro = new Exception(string.Format("Erro em CarregarSaldos TMP_SCC = {0}", SCC), e);
                VirMSSQL.TableAdapter.VircatchSQL(e);
                return false;               
            }
            return true;
        }

        //public bool FazerTeste = false;

        /*
        private void Teste(int CCT)
        {
            dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(CCT, false, new DateTime(2015, 8, 18));  
        }*/

        private decimal RegistraLancamento(Extrato.dExtrato.SaldoContaCorrenteRow SCCPai, int ARQ, dCarga.TMP_CCDRow TMP, bool definitivo)
        {            
            Extrato.dExtrato.ContaCorrenteDetalheRow novaCCD = dExtrato.ContaCorrenteDetalhe.NewContaCorrenteDetalheRow();
            novaCCD.CCD_ARQ = ARQ;
            novaCCD.CCD_SCC = SCCPai.SCC;
            novaCCD.CCDCategoria = TMP.TMPCategoria;
            novaCCD.CCDCodHistorico = TMP.TMPHitorico;
            novaCCD.CCDConsolidado = (int)statusconsiliado.Aguardando;
            novaCCD.CCDDescricao = TMP.TMPDescricao;
            novaCCD.CCDDescComplemento = TMP.TMPDescricaoComp;
            novaCCD.CCDDocumento = TMP.TMPDocumento;
            novaCCD.CCDValidado = definitivo;
            novaCCD.CCDValor = TMP.TMPValor;           
            dExtrato.ContaCorrenteDetalhe.AddContaCorrenteDetalheRow(novaCCD);
            SCCPai.SCCValorF += TMP.TMPValor;           
            if (TMP.TMPTransfInterna)
            {
                novaCCD.CCDConsolidado = (int)statusconsiliado.Automatico;
                novaCCD.CCDTipoLancamento = (int)TipoLancamentoNeon.TransferenciaInterna;
                SCCPai.SCCValorApF -= TMP.TMPValor;           
            }
            return SCCPai.SCCValorF;
        }     

        internal enum TipoDeLinha
        {
            cabecalho,
            saldoI,
            lancamento,
            saldoF,
            rodape,
            rodapelote,
            cabecalhodelote,
            invalido,
            cabecalhoLancamentosFuturos,
            LancamentoFuturo
        }

        /*
        private enum TipoLancamento
        {
            dia = 1,
            retroativo = 2,
            futuro = 5
        }*/

        internal class TlinhaDecodificada
        {
            public TipoDeLinha Tipo;            
            public int Agencia;
            public int? AgenciaDg = null;
            public int Conta;
            public string ContaDg;
            public bool Aplicacao;
            public string CNPJ;
            public DateTime Data;            
            public decimal Valor;
            public decimal Valor2;

            public int Categoria;
            public string Descricao;
            public string DescricaoComp;
            public int Documento = 0;
            public int Hitorico;
            public bool definitiva;
            public bool RegistroINV;
        }
    }    
}
