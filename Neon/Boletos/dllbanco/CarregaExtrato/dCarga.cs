﻿namespace dllbanco.CarregaExtrato
{


    public partial class dCarga
    {
        private dCargaTableAdapters.TMP_SCCTableAdapter tMP_SCCTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TMP_SCC
        /// </summary>
        public dCargaTableAdapters.TMP_SCCTableAdapter TMP_SCCTableAdapter
        {
            get
            {
                if (tMP_SCCTableAdapter == null)
                {
                    tMP_SCCTableAdapter = new dCargaTableAdapters.TMP_SCCTableAdapter();
                    tMP_SCCTableAdapter.TrocarStringDeConexao();
                };
                return tMP_SCCTableAdapter;
            }
        }

        private dCargaTableAdapters.TMP_CCDTableAdapter tMP_CCDTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TMP_CCD
        /// </summary>
        public dCargaTableAdapters.TMP_CCDTableAdapter TMP_CCDTableAdapter
        {
            get
            {
                if (tMP_CCDTableAdapter == null)
                {
                    tMP_CCDTableAdapter = new dCargaTableAdapters.TMP_CCDTableAdapter();
                    tMP_CCDTableAdapter.TrocarStringDeConexao();
                };
                return tMP_CCDTableAdapter;
            }
        }
    }
}
