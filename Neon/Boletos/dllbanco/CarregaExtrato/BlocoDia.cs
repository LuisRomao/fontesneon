﻿using System;
using System.Collections.Generic;


namespace dllbanco.CarregaExtrato
{
    /// <summary>
    /// Bloco de lançamentos diário do arquivo retorno. Ver documentação no OneNote "Retornos Bradesco com conta plus"
    /// </summary>
    internal class BlocoDia
    {
        //ATENÇÂO: o banco alterou o calculo dos valeres V1f e V2f tornando inviável a sua utilização em 22/10/2015 passamos a trabalhar somente com V1i 
        private dCarga dCarga;
        private int ARQ;
        private int BCO;
        private int Agencia;
        private int? AgenciaDg;
        private int Conta;
        private string ContaDg;
        private bool Aplicacao;
        private string CNPJ;
        private DateTime? DataDados;
        /// <summary>
        /// Para banco bradesco CCi = CC+Plus
        /// </summary>
        private decimal CCi;
        //private decimal CCfa;
        //private decimal CCff;
        //private decimal Plusi;        
        //private decimal Plusff;
        //private decimal Juros;
        //private decimal V1i;
        //private decimal V1f;        
        private decimal Sd;
        private decimal S;
        private decimal Sdt;
        private DateTime? Data1;
        private DateTime? Data2;
        private List<Lanc> Lancs1;
        private List<Lanc> Lancs2;
        //Valor fechado intermediário da CC
        private decimal CCmf;
        //Valor fechado intermediário da Aplicação
        //private decimal Plusmf;

        internal BlocoDia(dCarga dCarga, int ARQ, int BCO, carga.TlinhaDecodificada Linha, DateTime? DataDados)
            : this(dCarga, ARQ, BCO, Linha.Agencia, Linha.AgenciaDg, Linha.Conta, Linha.ContaDg, Linha.Aplicacao, Linha.CNPJ, Linha.Valor, DataDados)
        { 
        }

        internal BlocoDia(dCarga dCarga, int ARQ, int BCO, int Agencia, int? AgenciaDg, int Conta, string ContaDg, bool Aplicacao, string CNPJ, decimal V1, DateTime? DataDados = null)
        {
            this.dCarga = dCarga;
            //V1i = V1;
            CCi = CCmf = V1;
            Sd = S = Sdt = 0;            
            this.ARQ = ARQ;
            this.BCO = BCO;
            this.Agencia = Agencia;
            this.AgenciaDg = AgenciaDg;
            this.Conta = Conta;
            this.ContaDg = ContaDg;
            this.Aplicacao = Aplicacao;
            this.CNPJ = CNPJ;
            this.DataDados = DataDados;
            Lancs1 = new System.Collections.Generic.List<Lanc>();
        }

        internal bool Lancamento(DateTime Data, string Descricao, string DescricaoComp, int Doc, int Categoria, int CodHistorico, decimal Valor, bool Definitivo)
        {
            if (!Data1.HasValue)
                Data1 = Data;
            bool TranferenciaInterna = Conciliacao.TransferenciaInterna(BCO, Descricao, Categoria, CodHistorico);
            S += Valor;
            if (Definitivo)
            {
                Sd += Valor;                
                if (TranferenciaInterna)                
                    Sdt += Valor;                                    
                Lanc LancX = new Lanc(Data, Descricao, DescricaoComp, Categoria, CodHistorico, Doc, Valor, TranferenciaInterna);
                if (Data == Data1.Value)
                {
                    if (!TranferenciaInterna)
                        CCmf += Valor;
                    //if (TranferenciaInterna)
                    //    Plusmf -= Valor;
                    Lancs1.Add(LancX);
                }
                else
                {
                    if (!Data2.HasValue)
                    {
                        Data2 = Data;
                        Lancs2 = new System.Collections.Generic.List<Lanc>();
                        //CCmf = CCi;
                    }
                    if (Data == Data2.Value)
                        Lancs2.Add(LancX);
                    else
                        return false;
                }
            }
            return true;
        }

        //DESCONTINUADO
        /*
        internal void Fechamento(decimal V1, decimal V2)
        {
            V1f = V1;
            CCff = V2;
            CCi = CCff - Sd;
            Plusi = V1i - CCi;
            CCfa = CCi + S;
            Plusff = Plusi - Sdt;
            //Juros = V1f - CCfa - Plusff;
            //ATENÇÃO As variaveis CCmf e Plusmf só acumularam os lançamentos sem o valor inicial
            CCmf += CCi;
            Plusmf += Plusi;
        }

        internal void Fechamento()
        {
            //CCi = V1i;            
            //CCff = CCi+Sd;            
            //Plusi = 0;            
            //Plusff = -Sdt;
            //Juros = 0;
            //ATENÇÃO As variaveis CCmf e Plusmf só acumularam os lançamentos sem o valor inicial
            //CCmf += CCi;
            //Plusmf += Plusi;
        }*/

        private dCarga.TMP_SCCRow IniciaLinhaSCC(DateTime Data,decimal ValorI)
        {
            dCarga.TMP_SCCRow SCCRow = dCarga.TMP_SCC.NewTMP_SCCRow();
            SCCRow.TMP_ARQ = ARQ;
            SCCRow.TMP_BCO = BCO;
            SCCRow.TMPAgencia = Agencia;
            if (AgenciaDg.HasValue)
                SCCRow.TMPAgenciaDg = AgenciaDg.Value;
            SCCRow.TMPAplicacao = Aplicacao;
            SCCRow.TMPCNPJ = CNPJ;
            SCCRow.TMPConta = Conta;
            SCCRow.TMPContaDg = ContaDg;
            SCCRow.TMPData = Data;
            SCCRow.TMPRetroativo = Data < DataDados.GetValueOrDefault(Data);

            SCCRow.TMPValorI = ValorI;            
            dCarga.TMP_SCC.AddTMP_SCCRow(SCCRow);
            dCarga.TMP_SCCTableAdapter.Update(SCCRow);

            return SCCRow;
        }

        private void CarregaLanc(dCarga.TMP_SCCRow SCCRow, List<Lanc> Lancs)
        {
            foreach (Lanc LancX in Lancs)
            {
                dCarga.TMP_CCDRow novaCCD = dCarga.TMP_CCD.NewTMP_CCDRow();
                novaCCD.TMP_SCCRow = SCCRow;
                novaCCD.TMPCategoria = LancX.Categoria;
                novaCCD.TMPDescricao = LancX.Descricao;
                novaCCD.TMPDescricaoComp = LancX.DescricaoComp;
                novaCCD.TMPDocumento = LancX.Doc;
                novaCCD.TMPHitorico = LancX.CodHistorico;
                novaCCD.TMPValor = LancX.Valor;                
                novaCCD.TMPTransfInterna = LancX.TransfInterna;
                dCarga.TMP_CCD.AddTMP_CCDRow(novaCCD);
                dCarga.TMP_CCDTableAdapter.Update(novaCCD);
            }
        }

        internal bool Grava()
        {
            if (!Data1.HasValue)
                return true;
            dCarga.TMP_SCCRow SCCRow = IniciaLinhaSCC(Data1.Value, CCi);
                                    
            CarregaLanc(SCCRow, Lancs1);
            if (Data2.HasValue)
            {
                SCCRow = IniciaLinhaSCC(Data2.Value, CCmf);                                
                CarregaLanc(SCCRow,Lancs2);
            }
            return false;
        }

        private class Lanc
        {
            internal DateTime Data;
            internal string Descricao;
            internal string DescricaoComp;
            internal int Categoria;
            internal int CodHistorico;
            internal int Doc;
            internal decimal Valor;
            internal bool TransfInterna;

            public Lanc(DateTime Data, string Descricao, string DescricaoComp, int Categoria, int CodHistorico, int Doc, decimal Valor, bool TransfInterna)
            {
                this.Data = Data;
                this.Descricao = Descricao;
                this.DescricaoComp = DescricaoComp;
                this.Categoria = Categoria;
                this.CodHistorico = CodHistorico;
                this.Doc = Doc;
                this.Valor = Valor;
                this.TransfInterna = TransfInterna;
            }
        }
    }
}
