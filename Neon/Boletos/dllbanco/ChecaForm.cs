using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace dllbanco
{
    public partial class ChecaForm : DevExpress.XtraEditors.XtraForm
    {
        public ChecaForm()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = "Inicio\r\n";
            dFrancesinha dF = new dFrancesinha();
            dFrancesinhaTableAdapters.ChecaAccessTableAdapter TAAccess = new dllbanco.dFrancesinhaTableAdapters.ChecaAccessTableAdapter();
            dFrancesinhaTableAdapters.ChecaMSSQLTableAdapter TAMSSQL = new dllbanco.dFrancesinhaTableAdapters.ChecaMSSQLTableAdapter();
            TAAccess.TrocarStringDeConexao();
            TAMSSQL.TrocarStringDeConexao();
            int n1 = TAAccess.Fill(dF.ChecaAccess);
            int n2 = TAMSSQL.Fill(dF.ChecaMSSQL);
            memoEdit1.Text += n1.ToString() + " " + n2.ToString()+"\r\n";
            int ok = 0;
            int erro = 0;
            int naoEncontrado = 0;
            string Cor1 = "UPDATE BOLetos SET BOLValorPago = 0, BOLPagamento = NULL WHERE (BOL = @P1)";
            string Cor2 = "UPDATE ARquivoLido SET ARLStatusN = - 1 WHERE (ARL_BOL = @P1)";
            foreach (dFrancesinha.ChecaMSSQLRow rowM in dF.ChecaMSSQL) {
                dFrancesinha.ChecaAccessRow rowA = dF.ChecaAccess.FindByN�mero_Boleto(rowM.ARL_BOL);                
                if(rowA != null){
                    if (rowA.Pago)
                        ok++;
                    else
                    {
                        int c1=0;
                        int c2=0;
                        c1 = VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Cor1, rowM.ARL_BOL);
                        c2 = VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Cor2, rowM.ARL_BOL);
                        memoEdit1.Text += "Erro:" + rowM.ARL_BOL +" "+c1.ToString()+" "+c2.ToString()+ "\r\n";
                        erro++;
                    }
                }
                else {
                    memoEdit1.Text += "Nao Encontrado:" + rowM.ARL_BOL + "\r\n";
                    naoEncontrado++;
                };
            }
            memoEdit1.Text += "Ok:" + ok.ToString() + " Erro:" + erro.ToString() + " Nao encontrado:" + naoEncontrado.ToString() + "\r\n\r\n\r\n";

            //n2 = TAMSSQL.FillBy(dF.ChecaMSSQL);
            n2 = TAMSSQL.FillBy1(dF.ChecaMSSQL,1057495);
            memoEdit1.Text += n1.ToString() + " " + n2.ToString() + "\r\n";
            ok = 0;
            erro = 0;
            naoEncontrado = 0;
            
            foreach (dFrancesinha.ChecaMSSQLRow rowM in dF.ChecaMSSQL)
            {
                dFrancesinha.ChecaAccessRow rowA = dF.ChecaAccess.FindByN�mero_Boleto(rowM.ARL_BOL);
                if (rowA != null)
                {
                    if (rowA.Pago)
                        ok++;
                    else
                    {
                        memoEdit1.Text += "Erro:" + rowM.ARL_BOL + "\r\n";
                        Boletos.Boleto.Boleto BOL = new Boletos.Boleto.Boleto(rowM.ARL_BOL);
                        
                        erro++;
                    }
                }
                else
                {
                    memoEdit1.Text += "Nao Encontrado:" + rowM.ARL_BOL + "\r\n";
                    naoEncontrado++;
                };
            }
            memoEdit1.Text += "Ok:" + ok.ToString() + " Erro:" + erro.ToString() + " Nao encontrado:" + naoEncontrado.ToString() + "\r\n\r\n\r\n";
        }
    }
}