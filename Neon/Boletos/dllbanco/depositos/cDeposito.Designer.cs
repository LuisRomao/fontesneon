namespace dllbanco.depositos
{
    partial class cDeposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.BotOkD = new DevExpress.XtraEditors.SimpleButton();
            this.BotOkS = new DevExpress.XtraEditors.SimpleButton();
            this.cErro = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.BotOk = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.LabelTotal = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dDepositoboletoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dDeposito_boleto = new dllbanco.depositos.dDeposito_boleto();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorOrig = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorCorri = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorQuitar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMulta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cErro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dDepositoboletoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dDeposito_boleto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
          
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.BotOkD);
            this.panelControl1.Controls.Add(this.BotOkS);
            this.panelControl1.Controls.Add(this.cErro);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.BotOk);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.spinEdit1);
            this.panelControl1.Controls.Add(this.LabelTotal);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(673, 132);
            this.panelControl1.TabIndex = 0;
            // 
            // BotOkD
            // 
            this.BotOkD.Enabled = false;
            this.BotOkD.Location = new System.Drawing.Point(15, 103);
            this.BotOkD.Name = "BotOkD";
            this.BotOkD.Size = new System.Drawing.Size(181, 23);
            this.BotOkD.TabIndex = 9;
            this.BotOkD.Text = "OK Com quita��o parcial";
            this.BotOkD.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // BotOkS
            // 
            this.BotOkS.Enabled = false;
            this.BotOkS.Location = new System.Drawing.Point(15, 74);
            this.BotOkS.Name = "BotOkS";
            this.BotOkS.Size = new System.Drawing.Size(181, 23);
            this.BotOkS.TabIndex = 8;
            this.BotOkS.Text = "OK com saldo";
            this.BotOkS.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cErro
            // 
            this.cErro.Location = new System.Drawing.Point(447, 42);
            this.cErro.Name = "cErro";
            this.cErro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, false)});
            this.cErro.Properties.DisplayFormat.FormatString = "n2";
            this.cErro.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cErro.Properties.Precision = 2;
            this.cErro.Properties.ReadOnly = true;
            this.cErro.Size = new System.Drawing.Size(140, 22);
            this.cErro.TabIndex = 7;
            this.cErro.TabStop = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(392, 47);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(35, 17);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Erro:";
            // 
            // simpleButton2
            // 
            this.simpleButton2.AllowFocus = false;
            this.simpleButton2.Location = new System.Drawing.Point(392, 74);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(195, 52);
            this.simpleButton2.TabIndex = 4;
            this.simpleButton2.TabStop = false;
            this.simpleButton2.Text = "Cancela";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // BotOk
            // 
            this.BotOk.Enabled = false;
            this.BotOk.Location = new System.Drawing.Point(204, 74);
            this.BotOk.Name = "BotOk";
            this.BotOk.Size = new System.Drawing.Size(176, 53);
            this.BotOk.TabIndex = 3;
            this.BotOk.Text = "OK";
            this.BotOk.Click += new System.EventHandler(this.BotOk_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(15, 47);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(97, 17);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Incluir boleto:";
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(118, 42);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit1.Properties.Appearance.Options.UseFont = true;
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.spinEdit1.Size = new System.Drawing.Size(161, 26);
            this.spinEdit1.TabIndex = 1;
            this.spinEdit1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.spinEdit1_KeyDown);
            // 
            // LabelTotal
            // 
            this.LabelTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTotal.Appearance.ForeColor = System.Drawing.Color.Red;
            this.LabelTotal.Location = new System.Drawing.Point(5, 5);
            this.LabelTotal.Name = "LabelTotal";
            this.LabelTotal.Size = new System.Drawing.Size(203, 36);
            this.LabelTotal.TabIndex = 0;
            this.LabelTotal.Text = "labelControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.DataMember = "BoletosQuitar";
            this.gridControl1.DataSource = this.dDepositoboletoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 132);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(673, 411);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.TabStop = false;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dDepositoboletoBindingSource
            // 
            this.dDepositoboletoBindingSource.DataSource = this.dDeposito_boleto;
            this.dDepositoboletoBindingSource.Position = 0;
            // 
            // dDeposito_boleto
            // 
            this.dDeposito_boleto.DataSetName = "dDeposito_boleto";
            this.dDeposito_boleto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL,
            this.colValorOrig,
            this.colValorCorri,
            this.colValorQuitar,
            this.colVencimento,
            this.colMulta,
            this.colApart,
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colBOL
            // 
            this.colBOL.Caption = "Boleto";
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.ReadOnly = true;
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 0;
            // 
            // colValorOrig
            // 
            this.colValorOrig.Caption = "Valor Original";
            this.colValorOrig.DisplayFormat.FormatString = "n2";
            this.colValorOrig.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorOrig.FieldName = "ValorOrig";
            this.colValorOrig.Name = "colValorOrig";
            this.colValorOrig.OptionsColumn.ReadOnly = true;
            this.colValorOrig.Visible = true;
            this.colValorOrig.VisibleIndex = 3;
            this.colValorOrig.Width = 77;
            // 
            // colValorCorri
            // 
            this.colValorCorri.Caption = "Valor Corrigido";
            this.colValorCorri.DisplayFormat.FormatString = "n2";
            this.colValorCorri.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorCorri.FieldName = "ValorCorri";
            this.colValorCorri.Name = "colValorCorri";
            this.colValorCorri.OptionsColumn.ReadOnly = true;
            this.colValorCorri.Visible = true;
            this.colValorCorri.VisibleIndex = 4;
            this.colValorCorri.Width = 80;
            // 
            // colValorQuitar
            // 
            this.colValorQuitar.Caption = "Valor Quitar";
            this.colValorQuitar.DisplayFormat.FormatString = "n2";
            this.colValorQuitar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorQuitar.FieldName = "ValorQuitar";
            this.colValorQuitar.Name = "colValorQuitar";
            this.colValorQuitar.Visible = true;
            this.colValorQuitar.VisibleIndex = 5;
            this.colValorQuitar.Width = 70;
            // 
            // colVencimento
            // 
            this.colVencimento.Caption = "Vencimento";
            this.colVencimento.DisplayFormat.FormatString = "d";
            this.colVencimento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colVencimento.FieldName = "Vencimento";
            this.colVencimento.Name = "colVencimento";
            this.colVencimento.OptionsColumn.ReadOnly = true;
            this.colVencimento.Visible = true;
            this.colVencimento.VisibleIndex = 2;
            this.colVencimento.Width = 69;
            // 
            // colMulta
            // 
            this.colMulta.Caption = "Multa";
            this.colMulta.DisplayFormat.FormatString = "n2";
            this.colMulta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMulta.FieldName = "Multa";
            this.colMulta.Name = "colMulta";
            this.colMulta.OptionsColumn.ReadOnly = true;
            this.colMulta.Visible = true;
            this.colMulta.VisibleIndex = 6;
            // 
            // colApart
            // 
            this.colApart.Caption = "Unidade";
            this.colApart.FieldName = "Apart";
            this.colApart.Name = "colApart";
            this.colApart.OptionsColumn.ReadOnly = true;
            this.colApart.Visible = true;
            this.colApart.VisibleIndex = 1;
            this.colApart.Width = 148;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 23;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // cDeposito
            // 
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cDeposito";
            this.Size = new System.Drawing.Size(673, 543);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cErro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dDepositoboletoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dDeposito_boleto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dDepositoboletoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colValorOrig;
        private DevExpress.XtraGrid.Columns.GridColumn colValorCorri;
        private DevExpress.XtraGrid.Columns.GridColumn colValorQuitar;
        private DevExpress.XtraGrid.Columns.GridColumn colVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colMulta;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.LabelControl LabelTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colApart;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton BotOk;
        private DevExpress.XtraEditors.CalcEdit cErro;
        /// <summary>
        /// 
        /// </summary>
        public dDeposito_boleto dDeposito_boleto;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SimpleButton BotOkS;
        private DevExpress.XtraEditors.SimpleButton BotOkD;
    }
}
