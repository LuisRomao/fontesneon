using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace dllbanco.depositos
{
    /// <summary>
    /// Deposito
    /// </summary>
    public partial class cDeposito : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cDeposito()
        {
            InitializeComponent();         
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Erro;
        /// <summary>
        /// 
        /// </summary>
        public string PLA;
        /// <summary>
        /// 
        /// </summary>
        public string Descricao;

        /// <summary>
        /// 
        /// </summary>
        public void Somar() {
            Erro = Meta;
            foreach (dDeposito_boleto.BoletosQuitarRow rowBQ in dDeposito_boleto.BoletosQuitar)
                Erro -= rowBQ.ValorQuitar;
            
            BotOk.Enabled = (Erro == 0);
            BotOkS.Enabled = ((Erro != 0) && (Erro != Meta));
            BotOkD.Enabled = (Erro < 0);
            cErro.Value = Erro;    
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;

            dDeposito_boleto.BoletosQuitarRow row = (dDeposito_boleto.BoletosQuitarRow)(DRV.Row);
            row.Multa = row.ValorQuitar - row.ValorOrig;
            Somar();
        }

        /// <summary>
        /// 
        /// </summary>
        public dConciliacao.ContaCorrenteDetalheRow LinhaMae;

        /// <summary>
        /// 
        /// </summary>
        public decimal Meta;

        /// <summary>
        /// 
        /// </summary>
        public System.Collections.SortedList BoletosList = new System.Collections.SortedList();


        private void spinEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (spinEdit1.EditValue == null)
                return;

            if (e.KeyCode == Keys.Enter)
            {
                int Bol = (int)((decimal)spinEdit1.EditValue);
                if (BoletosList.ContainsKey(Bol))
                {
                    spinEdit1.EditValue = 0;
                    spinEdit1.Focus();
                    return;
                }
                Boletos.Boleto.Boleto BoletoBaixar = new Boletos.Boleto.Boleto(Bol);

                if (!BoletoBaixar.Encontrado)
                    MessageBox.Show("Boleto n�o encontrado");
                else
                {
                    if (!BoletoBaixar.rowPrincipal.IsBOLPagamentoNull())
                        MessageBox.Show("Boleto j� quitado");
                    else
                    {
                        if (BoletoBaixar.rowPrincipal.BOLCancelado)
                            MessageBox.Show("Boleto cancelado");
                        else
                        {
                            bool ok = true;
                            if (BoletoBaixar.CON != LinhaMae.CON)
                            {
                                try
                                {
                                    DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("SELECT CCTAgencia, CCTConta FROM ContaCorrenTe WHERE (CCT = @P1)", LinhaMae.CCT);
                                    if ( (DR == null)
                                           ||
                                         (LinhaMae.CCT_BCO != BoletoBaixar.rowCON.CON_BCO)
                                           ||
                                         (int.Parse(BoletoBaixar.rowCON.CONAgencia) != (int)DR["CCTAgencia"])
                                           ||
                                         (int.Parse(BoletoBaixar.rowCON.CONConta) != (int)DR["CCTConta"])
                                       )
                                    {
                                        MessageBox.Show("Boleto de condom�nio errado\r\nBoleto: " + BoletoBaixar.rowCON.CONCodigo + "\r\nDep�sito: " + LinhaMae.CONCodigo);
                                        ok = false;
                                    }
                                }
                                catch
                                {
                                    ok = false;
                                }
                            }
                            if (ok)
                            {
                                //BoletoBaixar.AjustaBoletoAntig
                                spinEdit1.EditValue = 0;
                                BoletoBaixar.Valorpara(LinhaMae.SCCData);
                                BoletosList.Add(Bol, BoletoBaixar);
                                dDeposito_boleto.BoletosQuitarRow Novarow = dDeposito_boleto.BoletosQuitar.NewBoletosQuitarRow();
                                Novarow.BOL = Bol;
                                if(BoletoBaixar.Apartamento == null)
                                    Novarow.Apart = " - ";
                                else
                                    Novarow.Apart = BoletoBaixar.Apartamento.BLOCodigo + " - " + BoletoBaixar.Apartamento.APTNumero;
                                Novarow.ValorOrig = BoletoBaixar.rowPrincipal.BOLValorPrevisto;
                                Novarow.ValorCorri = Novarow.ValorQuitar = BoletoBaixar.valorfinal;
                                Novarow.Vencimento = BoletoBaixar.rowPrincipal.BOLVencto;
                                Novarow.Multa = Novarow.ValorQuitar - Novarow.ValorOrig;
                                dDeposito_boleto.BoletosQuitar.AddBoletosQuitarRow(Novarow);
                                Somar();
                            }
                        }
                    }
                }

            }
        }

        private void BotOk_Click(object sender, EventArgs e)
        {
            PLA = "";
            FechaTela(DialogResult.OK);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.Cancel);
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colMulta)
            {
                dDeposito_boleto.BoletosQuitarRow row = (dDeposito_boleto.BoletosQuitarRow)gridView1.GetDataRow(e.RowHandle);
                if (row.Multa < 0)
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.White;
                }
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dDeposito_boleto.BoletosQuitarRow rowBol = (dDeposito_boleto.BoletosQuitarRow)gridView1.GetFocusedDataRow();
            if (rowBol != null) {
                Boletos.Boleto.Boleto BoletoBaixar = (Boletos.Boleto.Boleto)BoletosList[rowBol.BOL];
                Boletos.Boleto.cBoleto cBol = new Boletos.Boleto.cBoleto(BoletoBaixar.rowPrincipal,false);
                if (cBol.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK) {
                    BoletoBaixar.Valorpara(LinhaMae.SCCData);
                    rowBol.ValorOrig = BoletoBaixar.rowPrincipal.BOLValorPrevisto;
                    rowBol.ValorCorri = rowBol.ValorQuitar = BoletoBaixar.valorfinal;
                    rowBol.Vencimento = BoletoBaixar.rowPrincipal.BOLVencto;
                    rowBol.Multa = rowBol.ValorQuitar - rowBol.ValorOrig;                    
                    Somar();
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            cClassificar cClasse = new cClassificar();
            cClasse.AjustaRow(LinhaMae,Erro);
            if (cClasse.ShowEmPopUp() == DialogResult.OK)
            {
                PLA = cClasse.lookUpEditPLA1.Text;
                Descricao = cClasse.textEdit1.Text;
                FechaTela(DialogResult.OK);
            }
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            PLA = "";
            FechaTela(DialogResult.OK);
        }

        
    }
}

