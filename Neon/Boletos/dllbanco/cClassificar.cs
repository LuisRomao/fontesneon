using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace dllbanco
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cClassificar : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public cClassificar()
        {
            InitializeComponent();
        }

        bool SemafaroPLA = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        public void AjustaRow(dConciliacao.ContaCorrenteDetalheRow row) {
            AjustaRow(row, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="Valor"></param>
        public void AjustaRow(dConciliacao.ContaCorrenteDetalheRow row,decimal Valor) {

            contaCorrenteDetalheBindingSource.DataSource = row;
            pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas;
            if (row.CCDValor < 0)
                pLAnocontasBindingSource.Filter = "PLA like '2%'";
            else
                pLAnocontasBindingSource.Filter = "(PLA like '1%') or (PLA like '6%')";
            if (Valor != 0)
            {
                cCDValorCalcEdit.DataBindings.Clear();
                cCDValorCalcEdit.Value = Valor;
                checkEdit1.Visible = false;
            }
        }

        private void lookUpEditPLA2_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroPLA)
            {
                SemafaroPLA = true;
                if (sender == lookUpEditPLA1)
                    lookUpEditPLA2.EditValue = lookUpEditPLA1.EditValue;
                if (sender == lookUpEditPLA2)
                    lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue;
                if (lookUpEditPLA1.Text == "213000")
                    textEdit1.Text = cCDDescricaoTextEdit.Text;
                else
                    textEdit1.Text = lookUpEditPLA2.Text;
                SemafaroPLA = false;                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            Validate();
            if ( (Resultado == DialogResult.OK) 
                 &&
                 ((lookUpEditPLA1.EditValue == null) || (textEdit1.Text == ""))
               )
                    MessageBox.Show("Dados incompletos");            
            else
                base.FechaTela(Resultado);
        }

        private void textEdit1_Validating(object sender, CancelEventArgs e)
        {
            string Corrigido = "";
            bool primeiro = true;
            for (int i = 0; i < textEdit1.Text.Length; i++)
            {
                if (primeiro)
                    Corrigido += char.ToUpper(textEdit1.Text[i]);
                else
                    Corrigido += char.ToLower(textEdit1.Text[i]);
                if (textEdit1.Text[i] != ' ')
                    primeiro = false;
            }
            if (textEdit1.Text != Corrigido)
                textEdit1.Text = Corrigido;
        }
    }
    
}

