using Framework.ComponentesNeon;
using System;
using System.Data;
using System.Windows.Forms;
using VirEnumeracoesNeon;

namespace dllbanco
{
    /// <summary>
    /// Francesinha manual
    /// </summary>
    public partial class cFrancesinhaManual : NavegadorCondominio
    {

        private int _ARQ;

        private FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow _CONRow;

        private dFrancesinha.ContaCorrenTeRow CCTrowSel;

        private string comandoBuscaBOL =
"SELECT     BOLetos.BOLValorPrevisto, BLOCOS.BLO_CON\r\n" +
"FROM         BOLetos INNER JOIN\r\n" +
"                      APARTAMENTOS ON BOLetos.BOL_APT = APARTAMENTOS.APT INNER JOIN\r\n" +
"                      BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO\r\n" +
"WHERE     (BOLetos.BOL = @P1);";
        private dFrancesinha dFrancesinha;

        /// <summary>
        /// Construtor
        /// </summary>
        public cFrancesinhaManual()
        {
            InitializeComponent();
            BindingSource_F.DataSource = dFrancesinha = new dFrancesinha();
        }

        private void cFrancesinhaManual_CondominioAlterado(object sender, EventArgs e)
        {
            if (CON > 0)
            {
                dFrancesinha.ContaCorrenTeTableAdapter.FillBy(dFrancesinha.ContaCorrenTe, CON);
                comboBoxEdit1.Properties.Items.Clear();
                int principal = 0;
                int i = 0;
                foreach (dFrancesinha.ContaCorrenTeRow CCTrow in dFrancesinha.ContaCorrenTe)
                {
                    if (CCTrow.CCTPrincipal)
                        principal = i;
                    comboBoxEdit1.Properties.Items.Add(string.Format("{0} {1} {2}: {3}{4}",
                                                                     CCTrow.CCT_BCO,
                                                                     CCTrow.CCTAgencia,
                                                                     CCTrow.CCTConta,
                                                                     CCTrow.IsCCTTituloNull() ? string.Empty : CCTrow.CCTTitulo,
                                                                     CCTrow.CCTPrincipal ? "*" : string.Empty));
                    i++;
                }
                comboBoxEdit1.SelectedIndex = principal;
            }
        }

        /*
        private void GridView_F_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            dFrancesinha.ARquivoLidoRow NovaLinha = (dFrancesinha.ARquivoLidoRow)GridView_F.GetDataRow(e.RowHandle);
            CCTrowSel = dFrancesinha.ContaCorrenTe[comboBoxEdit1.SelectedIndex];
            NovaLinha.ARL_ARQ = ARQ;
            NovaLinha.ARL_BCO = CCTrowSel.CCT_BCO;
            NovaLinha.ARL_CONAgencia = CCTrowSel.CCTAgencia.ToString();
            NovaLinha.ARL_CONConta = CCTrowSel.CCTConta.ToString();
            NovaLinha.ARL_CONDigitoAgencia = CCTrowSel.CCTAgenciaDg.ToString();
            NovaLinha.ARL_CONDigitoConta = CCTrowSel.CCTContaDg;
            NovaLinha.ARL_CCT = CCTrowSel.CCT;
            NovaLinha.ARL_BOLPagamento = dataCred.DateTime;
            NovaLinha.ARL_Data = dataPag.DateTime;
            NovaLinha.ARLStatusN = (int)Framework.Enumeracoes.ARLStatusN.Nova;
        }*/

        private void GridView_F_ValidateRow_1(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            dFrancesinha.ARquivoLidoRow Linha = (dFrancesinha.ARquivoLidoRow)(((DataRowView)e.Row).Row);
            GridView_F.ClearColumnErrors();
            if (Linha["ARL_BOL"] != DBNull.Value)
                Linha.ARLNossoNumero = Linha.ARL_BOL;
            else
            {
                GridView_F.SetColumnError(colARL_BOL, "Campo Obrigat�rio");
                e.Valid = false;
            }
            if (Linha["ARL_BOLPagamento"] == DBNull.Value)
            {
                GridView_F.SetColumnError(colARL_BOLPagamento, "Campo Obrigat�rio");
                e.Valid = false;
            }
            if (Linha["ARL_Data"] == DBNull.Value)
            {
                GridView_F.SetColumnError(colARL_Data, "Campo Obrigat�rio");
                e.Valid = false;
            }
            if (Linha["ARLValor"] == DBNull.Value)
            {
                GridView_F.SetColumnError(colARLValor, "Campo Obrigat�rio");
                e.Valid = false;
            }
            else
                if (Linha.ARLValor <= 0)
            {
                GridView_F.SetColumnError(colARLValor, "Valor inv�lido");
                e.Valid = false;
            };
            if ((Linha["ARL_BOLPagamento"] != DBNull.Value) && (Linha["ARL_Data"] != DBNull.Value))
                if (Linha.ARL_Data > Linha.ARL_BOLPagamento)
                {
                    GridView_F.SetColumnError(colARL_Data, "Cr�dito anterior ao pagamento!!");
                    e.Valid = false;
                };
        }

        private void GridView_F_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            if (GridView_F.FocusedColumn == colARL_BOL)
            {

                DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(comandoBuscaBOL, int.Parse(e.Value.ToString()));
                if (DR == null)
                    MessageBox.Show(string.Format("Boleto {0} n�o encontrado", e.Value));
                else
                    if ((int)DR["BLO_CON"] != CONRow.CON)
                    MessageBox.Show(string.Format("Boleto {0} n�o � do condom�nio {1}", e.Value, CONRow.CONCodigo));
                else
                {
                    //NovaLinha.ARL_BOLValorPago = 123.45M;
                    //dFrancesinha.ARquivoLidoRow Linha = (dFrancesinha.ARquivoLidoRow)GridView_F.GetFocusedDataRow();
                    //Linha.ARL_BOLValorPago = (decimal)DR["BOLValorPrevisto"];
                    //GridView_F.SetRowCellValue(GridView_F.FocusedRowHandle, "BOLValorPrevisto", 100);
                }
            }
        }

        private int ARQ
        {
            get
            {
                if (_ARQ == 0)
                {
                    dFrancesinha.ARQuivosRow ARQrow = dFrancesinha.ARQuivos.NewARQuivosRow();
                    CCTrowSel = dFrancesinha.ContaCorrenTe[comboBoxEdit1.SelectedIndex];
                    ARQrow.ARQBanco = CCTrowSel.CCT_BCO;
                    ARQrow.ARQIData = DateTime.Now;
                    ARQrow.ARQNome = string.Format("Arquivo Manual. {0} {1}", Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome, DateTime.Now);
                    ARQrow.ARQTipo = (int)Framework.ARQTipo.Manual;
                    dFrancesinha.ARQuivos.AddARQuivosRow(ARQrow);
                    dFrancesinha.ARQuivosTableAdapter.Update(ARQrow);
                    dFrancesinha.ARQuivos.AcceptChanges();
                    _ARQ = ARQrow.ARQ;
                }
                return _ARQ;
            }
        }

        private FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow CONRow
        {
            get
            {
                return _CONRow ?? (_CONRow = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(CON));
            }
        }

        /// <summary>
        /// Alterar
        /// </summary>
        protected override void Alterar()
        {
            if (CON == -1)
                return;
            if (dataPag.EditValue == null)
            {
                dataPag.ErrorText = "Data n�o informada";
                return;
            };
            if (dataCred.EditValue == null)
            {
                dataCred.ErrorText = "Data n�o informada";
                return;
            };
            ShowToolBarByRowState(true);
            GridControl_F.MainView.ShowEditor();
            panelControl1.Enabled = false;
            GridControl_F.Enabled = true;
            int NN;
            while (VirInput.Input.Execute("Nosso N�mero", out NN))
            {
                if (NN == 0)
                    break;
                dFrancesinha.ARquivoLidoRow NovaLinha = dFrancesinha.ARquivoLido.NewARquivoLidoRow();
                NovaLinha.ARL_ARQ = ARQ;
                NovaLinha.ARL_CCT = CCTrowSel.CCT;
                NovaLinha.ARL_BCO = CCTrowSel.CCT_BCO;
                NovaLinha.ARL_CONAgencia = CCTrowSel.CCTAgencia.ToString();
                if (CCTrowSel.CCT_BCO != 237)
                {
                    NovaLinha.ARL_CONConta = CCTrowSel.CCTCNR.ToString();
                    NovaLinha.ARL_CONDigitoAgencia = "0";
                }
                else
                {
                    NovaLinha.ARL_CONConta = CCTrowSel.CCTConta.ToString();
                    if (!CCTrowSel.IsCCTAgenciaDgNull())
                        NovaLinha.ARL_CONDigitoAgencia = CCTrowSel.CCTAgenciaDg.ToString();
                    else
                        NovaLinha.ARL_CONDigitoAgencia = string.Empty;
                }
                if (!CCTrowSel.IsCCTContaDgNull())
                    NovaLinha.ARL_CONDigitoConta = CCTrowSel.CCTContaDg;
                else
                    NovaLinha.ARL_CONDigitoConta = string.Empty;
                NovaLinha.ARL_BOLPagamento = dataCred.DateTime;
                NovaLinha.ARL_Data = dataPag.DateTime;
                NovaLinha.ARLStatusN = (int)ARLStatusN.Nova;
                NovaLinha.ARL_BOL = NN;
                NovaLinha.ARLValor = 0;
                NovaLinha.ARLNossoNumero = NN;
                DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(comandoBuscaBOL, NN);
                if (DR == null)
                    MessageBox.Show(string.Format("Boleto {0} n�o encontrado", NN));
                else
                    if ((int)DR["BLO_CON"] != CONRow.CON)
                    MessageBox.Show(string.Format("Boleto {0} n�o � do condom�nio {1}", NN, CONRow.CONCodigo));
                else
                    NovaLinha.ARLValor = (decimal)DR["BOLValorPrevisto"];
                dFrancesinha.ARquivoLido.AddARquivoLidoRow(NovaLinha);
            }
        }

        /// <summary>
        /// BtnIncluir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Alterar();
        }

        /// <summary>
        /// Cancela
        /// </summary>
        /// <param name="TesteCanClose"></param>
        /// <returns></returns>
        public override bool Cancela_F(bool TesteCanClose)
        {
            FechaTela(DialogResult.Cancel);
            return true;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            GridView_F.CloseEditor();
            GridView_F.ClearColumnErrors();
            if (!GridView_F.UpdateCurrentRow())
                return false;
            BindingSource_F.EndEdit();
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco cFrancesinhaManual - 200", dFrancesinha.ARQuivosTableAdapter);
                dFrancesinha.ARquivoLidoTableAdapter.Update(dFrancesinha.ARquivoLido);
                dFrancesinha.ARquivoLido.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
                FechaTela(DialogResult.OK);
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                MessageBox.Show(string.Format("Erro ao gravar:{0}", e.Message));
                VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(e);
                return false;
            }
            return true;
        }
    }
}
