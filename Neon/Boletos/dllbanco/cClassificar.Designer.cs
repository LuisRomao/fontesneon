namespace dllbanco
{
    partial class cClassificar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cCDValorLabel;
            System.Windows.Forms.Label cCDDescricaoLabel;
            System.Windows.Forms.Label cCDCategoriaLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.contaCorrenteDetalheBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cCDValorCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.cCDDescricaoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cCDCategoriaTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cCDDescComplementoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEditPLA1 = new DevExpress.XtraEditors.LookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpEditPLA2 = new DevExpress.XtraEditors.LookUpEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            cCDValorLabel = new System.Windows.Forms.Label();
            cCDDescricaoLabel = new System.Windows.Forms.Label();
            cCDCategoriaLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contaCorrenteDetalheBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCDValorCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCDDescricaoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCDCategoriaTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCDDescComplementoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
        
            // 
            // cCDValorLabel
            // 
            cCDValorLabel.AutoSize = true;
            cCDValorLabel.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cCDValorLabel.Location = new System.Drawing.Point(713, 44);
            cCDValorLabel.Name = "cCDValorLabel";
            cCDValorLabel.Size = new System.Drawing.Size(75, 29);
            cCDValorLabel.TabIndex = 5;
            cCDValorLabel.Text = "Valor";
            // 
            // cCDDescricaoLabel
            // 
            cCDDescricaoLabel.AutoSize = true;
            cCDDescricaoLabel.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cCDDescricaoLabel.Location = new System.Drawing.Point(3, 44);
            cCDDescricaoLabel.Name = "cCDDescricaoLabel";
            cCDDescricaoLabel.Size = new System.Drawing.Size(129, 29);
            cCDDescricaoLabel.TabIndex = 7;
            cCDDescricaoLabel.Text = "Descrição";
            // 
            // cCDCategoriaLabel
            // 
            cCDCategoriaLabel.AutoSize = true;
            cCDCategoriaLabel.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cCDCategoriaLabel.Location = new System.Drawing.Point(3, 152);
            cCDCategoriaLabel.Name = "cCDCategoriaLabel";
            cCDCategoriaLabel.Size = new System.Drawing.Size(137, 29);
            cCDCategoriaLabel.TabIndex = 9;
            cCDCategoriaLabel.Text = "Categoria:";
            cCDCategoriaLabel.Visible = false;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(3, 191);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(211, 29);
            label1.TabIndex = 13;
            label1.Text = "Plano de contas:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(3, 229);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(215, 29);
            label2.TabIndex = 16;
            label2.Text = "Texto Balancete:";
            // 
            // contaCorrenteDetalheBindingSource
            // 
            this.contaCorrenteDetalheBindingSource.DataSource = typeof(dllbanco.dConciliacao.ContaCorrenteDetalheRow);
            // 
            // cCDValorCalcEdit
            // 
            this.cCDValorCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contaCorrenteDetalheBindingSource, "CCDValor", true));
            this.cCDValorCalcEdit.Location = new System.Drawing.Point(718, 76);
            this.cCDValorCalcEdit.Name = "cCDValorCalcEdit";
            this.cCDValorCalcEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCDValorCalcEdit.Properties.Appearance.Options.UseFont = true;
            this.cCDValorCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.cCDValorCalcEdit.Properties.DisplayFormat.FormatString = "n2";
            this.cCDValorCalcEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cCDValorCalcEdit.Properties.EditFormat.FormatString = "n2";
            this.cCDValorCalcEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cCDValorCalcEdit.Properties.Mask.EditMask = "n2";
            this.cCDValorCalcEdit.Properties.ReadOnly = true;
            this.cCDValorCalcEdit.Size = new System.Drawing.Size(135, 36);
            this.cCDValorCalcEdit.TabIndex = 4;
            this.cCDValorCalcEdit.TabStop = false;
            // 
            // cCDDescricaoTextEdit
            // 
            this.cCDDescricaoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contaCorrenteDetalheBindingSource, "CCDDescricao", true));
            this.cCDDescricaoTextEdit.Location = new System.Drawing.Point(8, 76);
            this.cCDDescricaoTextEdit.Name = "cCDDescricaoTextEdit";
            this.cCDDescricaoTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCDDescricaoTextEdit.Properties.Appearance.Options.UseFont = true;
            this.cCDDescricaoTextEdit.Properties.ReadOnly = true;
            
            this.cCDDescricaoTextEdit.Size = new System.Drawing.Size(704, 36);
            
            this.cCDDescricaoTextEdit.TabIndex = 2;
            this.cCDDescricaoTextEdit.TabStop = false;
            // 
            // cCDCategoriaTextEdit
            // 
            this.cCDCategoriaTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contaCorrenteDetalheBindingSource, "CCDCategoria", true));
            this.cCDCategoriaTextEdit.Location = new System.Drawing.Point(118, 149);
            this.cCDCategoriaTextEdit.Name = "cCDCategoriaTextEdit";
            this.cCDCategoriaTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCDCategoriaTextEdit.Properties.Appearance.Options.UseFont = true;
            
            this.cCDCategoriaTextEdit.Size = new System.Drawing.Size(85, 36);
            
            this.cCDCategoriaTextEdit.TabIndex = 5;
            this.cCDCategoriaTextEdit.TabStop = false;
            this.cCDCategoriaTextEdit.Visible = false;
            // 
            // cCDDescComplementoTextEdit
            // 
            this.cCDDescComplementoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contaCorrenteDetalheBindingSource, "CCDDescComplemento", true));
            this.cCDDescComplementoTextEdit.Location = new System.Drawing.Point(8, 118);
            this.cCDDescComplementoTextEdit.Name = "cCDDescComplementoTextEdit";
            this.cCDDescComplementoTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCDDescComplementoTextEdit.Properties.Appearance.Options.UseFont = true;
            this.cCDDescComplementoTextEdit.Properties.ReadOnly = true;
            
            this.cCDDescComplementoTextEdit.Size = new System.Drawing.Size(704, 36);
            
            this.cCDDescComplementoTextEdit.TabIndex = 3;
            this.cCDDescComplementoTextEdit.TabStop = false;
            // 
            // lookUpEditPLA1
            // 
            this.lookUpEditPLA1.Location = new System.Drawing.Point(224, 192);
            this.lookUpEditPLA1.Name = "lookUpEditPLA1";
            this.lookUpEditPLA1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditPLA1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditPLA1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditPLA1.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA1.Properties.DisplayMember = "PLA";
            this.lookUpEditPLA1.Properties.ValueMember = "PLA";
            this.lookUpEditPLA1.Size = new System.Drawing.Size(111, 30);
            this.lookUpEditPLA1.TabIndex = 0;
            this.lookUpEditPLA1.EditValueChanged += new System.EventHandler(this.lookUpEditPLA2_EditValueChanged);
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // lookUpEditPLA2
            // 
            this.lookUpEditPLA2.Location = new System.Drawing.Point(341, 192);
            this.lookUpEditPLA2.Name = "lookUpEditPLA2";
            this.lookUpEditPLA2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditPLA2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditPLA2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditPLA2.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA2.Properties.DisplayMember = "PLADescricao";
            this.lookUpEditPLA2.Properties.ValueMember = "PLA";
            this.lookUpEditPLA2.Size = new System.Drawing.Size(512, 30);
            this.lookUpEditPLA2.TabIndex = 1;
            this.lookUpEditPLA2.TabStop = false;
            this.lookUpEditPLA2.EditValueChanged += new System.EventHandler(this.lookUpEditPLA2_EditValueChanged);
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(8, 270);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.checkEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.checkEdit1.Properties.Caption = "Adotar sempre esta classificação";
            this.checkEdit1.Size = new System.Drawing.Size(435, 33);
            this.checkEdit1.TabIndex = 14;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(224, 228);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            
            this.textEdit1.Size = new System.Drawing.Size(629, 36);
            
            this.textEdit1.TabIndex = 15;
            this.textEdit1.TabStop = false;
            this.textEdit1.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit1_Validating);
            // 
            // cClassificar
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.Controls.Add(label2);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.checkEdit1);
            this.Controls.Add(this.lookUpEditPLA1);
            this.Controls.Add(this.lookUpEditPLA2);
            this.Controls.Add(label1);
            this.Controls.Add(this.cCDDescComplementoTextEdit);
            this.Controls.Add(cCDCategoriaLabel);
            this.Controls.Add(this.cCDCategoriaTextEdit);
            this.Controls.Add(cCDDescricaoLabel);
            this.Controls.Add(this.cCDDescricaoTextEdit);
            this.Controls.Add(cCDValorLabel);
            this.Controls.Add(this.cCDValorCalcEdit);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cClassificar";
            this.Size = new System.Drawing.Size(856, 358);
            this.Titulo = "Classificar";
            this.Controls.SetChildIndex(this.cCDValorCalcEdit, 0);
            this.Controls.SetChildIndex(cCDValorLabel, 0);
            this.Controls.SetChildIndex(this.cCDDescricaoTextEdit, 0);
            this.Controls.SetChildIndex(cCDDescricaoLabel, 0);
            this.Controls.SetChildIndex(this.cCDCategoriaTextEdit, 0);
            this.Controls.SetChildIndex(cCDCategoriaLabel, 0);
            this.Controls.SetChildIndex(this.cCDDescComplementoTextEdit, 0);
            this.Controls.SetChildIndex(label1, 0);
            this.Controls.SetChildIndex(this.lookUpEditPLA2, 0);
            this.Controls.SetChildIndex(this.lookUpEditPLA1, 0);
            this.Controls.SetChildIndex(this.checkEdit1, 0);
            this.Controls.SetChildIndex(this.textEdit1, 0);
            this.Controls.SetChildIndex(label2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contaCorrenteDetalheBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCDValorCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCDDescricaoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCDCategoriaTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCDDescComplementoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CalcEdit cCDValorCalcEdit;
        private DevExpress.XtraEditors.TextEdit cCDDescricaoTextEdit;
        private DevExpress.XtraEditors.TextEdit cCDCategoriaTextEdit;
        private DevExpress.XtraEditors.TextEdit cCDDescComplementoTextEdit;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA2;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private System.Windows.Forms.BindingSource contaCorrenteDetalheBindingSource;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit textEdit1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.LookUpEdit lookUpEditPLA1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CheckEdit checkEdit1;
    }
}
