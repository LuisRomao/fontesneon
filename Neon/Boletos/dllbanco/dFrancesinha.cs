﻿/*
MR - 21/03/2016 20:00 -           - Atualização (com exclusao do antigo e inclusão do novo) do campo ARL_MotivoOcorrencia devido a mudanca de int para varchar(10) no ARquivoLidoTableAdapter
MR - 01/04/2016 19:00 -           - Tratamento do novo status de tarifas no FillLimpo do ARquivoLidoTableAdapter
*/

using System;
using FrameworkProc;
using System.Data;

namespace dllbanco
{

    /// <summary>
    /// dFrancesinha
    /// </summary>
    partial class dFrancesinha : IEMPTProc
    {

        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dFrancesinha(EMPTProc _EMPTProc1)
        : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }


        [Obsolete("Não utilizar mais a camada de persistência como estática")]
        private static dFrancesinha _dFrancesinhaSTA;

        [Obsolete("Não utilizar mais a camada de persistência como estática")]
        /// <summary>
        /// dFrancesinhaSTA
        /// </summary>
        public static dFrancesinha dFrancesinhaSTA
        {
            get
            {
                if (_dFrancesinhaSTA == null)
                {
                    _dFrancesinhaSTA = new dFrancesinha();
                }
                return _dFrancesinhaSTA;
            }
        }


        #region Adapters
        private dFrancesinhaTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dFrancesinhaTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dFrancesinhaTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dFrancesinhaTableAdapters.RedirecionaCCtTableAdapter redirecionaCCtTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RedirecionaCCt
        /// </summary>
        public dFrancesinhaTableAdapters.RedirecionaCCtTableAdapter RedirecionaCCtTableAdapter
        {
            get
            {
                if (redirecionaCCtTableAdapter == null)
                {
                    redirecionaCCtTableAdapter = new dFrancesinhaTableAdapters.RedirecionaCCtTableAdapter();
                    redirecionaCCtTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return redirecionaCCtTableAdapter;
            }
        }

        private dFrancesinhaTableAdapters.ARquivoLidoTableAdapter aRquivoLidoTableAdapter;
        private dFrancesinhaTableAdapters.ARQuivosTableAdapter aRQuivosTableAdapter;



        /// <summary>
        /// ARQuivosTableAdapter
        /// </summary>
        public dFrancesinhaTableAdapters.ARQuivosTableAdapter ARQuivosTableAdapter
        {
            get
            {
                if (aRQuivosTableAdapter == null)
                {
                    aRQuivosTableAdapter = new dllbanco.dFrancesinhaTableAdapters.ARQuivosTableAdapter();
                    aRQuivosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return aRQuivosTableAdapter;
            }

        }

        /// <summary>
        /// ARquivoLidoTableAdapter
        /// </summary>
        public dFrancesinhaTableAdapters.ARquivoLidoTableAdapter ARquivoLidoTableAdapter
        {
            get
            {
                if (aRquivoLidoTableAdapter == null)
                {
                    aRquivoLidoTableAdapter = new dllbanco.dFrancesinhaTableAdapters.ARquivoLidoTableAdapter();
                    aRquivoLidoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);

                };
                return aRquivoLidoTableAdapter;
            }

        }

        #endregion




        private DataView dV_ARQuivos_ARQNome;

        private DataView DV_ARQuivos_ARQNome
        {
            get
            {
                if (dV_ARQuivos_ARQNome == null)
                {
                    dV_ARQuivos_ARQNome = new DataView(ARQuivos);
                    dV_ARQuivos_ARQNome.Sort = "ARQNome";
                };
                return dV_ARQuivos_ARQNome;
            }
        }

        internal dFrancesinha.ARQuivosRow Busca_rowARQuivos(string ARQNome)
        {
            int ind = DV_ARQuivos_ARQNome.Find(new object[] { ARQNome });
            return (ind < 0) ? null : (dFrancesinha.ARQuivosRow)(DV_ARQuivos_ARQNome[ind].Row);
        }
    }
}
