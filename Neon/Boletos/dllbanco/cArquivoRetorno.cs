using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EDIBoletos;
using EDIBoletosNeon;

namespace dllbanco
{
    /// <summary>
    /// Arquivo Retorno
    /// </summary>
    public partial class cArquivoRetorno : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// construtor padr�o
        /// </summary>
        public cArquivoRetorno()
        {
            InitializeComponent();
            dateEdit1.DateTime = DateTime.Today;
            if (CompontesBasicos.FormPrincipalBase.USULogado == 30)
                simpleButton3.Visible = true;
        }

        private dllbanco.IdentificaArquivo _IdentificaArquivo;

        private dllbanco.IdentificaArquivo identificaArquivo {
            get {
                if (_IdentificaArquivo == null) {
                    _IdentificaArquivo = new IdentificaArquivo(memoEdit1, progressBarControl1);
                };
                return _IdentificaArquivo;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Conciliacao.ConciliacaoSt.Gau = progressBarControl1;            
            identificaArquivo.prompt();
            Calcular();        
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Conciliacao.ConciliacaoSt.Gau = progressBarControl1;
            identificaArquivo.ArquivosData(dateEdit1.DateTime);
            Calcular();
        }

        

        private void simpleButton3_Click(object sender, EventArgs e)
        {   
                     
            CarregaExtrato.carga carga1 = new CarregaExtrato.carga();
            int iTotal = 0;
            int iAtual = 0;
            string strLocal = "";
            carga1.CarregarSaldos(ref strLocal,ref iTotal,ref iAtual);
            Calcular();
            //Corre��o das CCTs duplicadas
            /*
            string comandoGeral = "SELECT COUNT(CCT) AS Expr1, CCT_BCO, CCTConta, CCTAgencia, CCTAplicacao FROM dbo.ContaCorrenTe where ccttipo <> 4 GROUP BY CCT_BCO, CCTConta, CCTAgencia, CCTAplicacao HAVING (COUNT(CCT) > 1)";
            string comandoGrupo = "SELECT CCT, CCTPrincipal, CCTAtiva FROM dbo.ContaCorrenTe WHERE (CCT_BCO = @P1) AND (CCTConta = @P2) AND (CCTAgencia = @P3) AND (CCTAplicacao = @P4)";
            string correcao1 = "update SaldoContaCorrente set scc_cct = @P1 where scc_cct = @P2";
            string correcao2 = "delete contacorrente where CCT = @P1";
            DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comandoGeral);
            StringBuilder Relatorio = new StringBuilder();
            Relatorio.AppendFormat("Tratar: {0}\r\n", DT.Rows.Count);
            Relatorio.AppendLine();
            foreach (DataRow DR in DT.Rows)
            {
                Relatorio.AppendFormat("CCTConta: {0}", DR["CCTConta"]);
                DataTable DT1 = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comandoGrupo, DR["CCT_BCO"], DR["CCTConta"], DR["CCTAgencia"], DR["CCTAplicacao"]);
                List<int> CCTremover = new List<int>();
                int? CCT = null;
                bool Cancelar = true;
                foreach (DataRow DR1 in DT1.Rows)
                {
                    if (DR1["CCTAtiva"] == DBNull.Value)
                        CCTremover.Add((int)DR1["CCT"]);
                    else if (!(bool)DR1["CCTAtiva"])
                        continue;
                    else 
                        if (CCT.HasValue)
                        {
                            Relatorio.AppendFormat(" 2 ATIVAS\r\n");
                            Cancelar = true;
                            break;
                        }
                        else
                        {
                            CCT = (int)DR1["CCT"];
                            Cancelar = false;
                        }
                }
                if (Cancelar)
                {
                    Relatorio.AppendFormat("          CANCELADA\r\n");
                    continue;
                }
                try
                {
                    foreach (int CCTmorre in CCTremover)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(correcao1, CCT, CCTmorre);
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(correcao2, CCTmorre);
                    }
                    Relatorio.AppendFormat("          FEITO\r\n");
                }
                catch (Exception ex)
                {
                    Relatorio.AppendFormat("          CATCH:{0}\r\n",ex.Message);
                }
                //break;
            }
            memoEdit1.Text = Relatorio.ToString();           
            */
        }

        private void SBTesta_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenF = new OpenFileDialog();
            OpenF.Multiselect = true;
            if (OpenF.ShowDialog() == DialogResult.OK)
            {
                StringBuilder SB = new StringBuilder();                
                foreach (string Arquivo in OpenF.FileNames)
                {
                    dllCNAB.CNAB240 cnab;
                    BoletoRetornoBra BoletoRetornoBra;
                    BoletoRetornoItau BoletoRetornoItau;
                    ChequeRetornoBra ChequeRetornoBra;
                    TributoRetornoBra TributoRetornoBra;
                    string Erro;
                    IdentificaArquivo.TipoArquivo TipoArq = identificaArquivo.IdentificaTipo(Arquivo, out cnab, out BoletoRetornoBra, out BoletoRetornoItau, out ChequeRetornoBra, out TributoRetornoBra, out Erro,false);
                    SB.AppendLine("************************************************************************************");
                    SB.AppendFormat("** Arquivo: {0,-70}**\r\n",Arquivo);
                    SB.AppendLine("************************************************************************************");                    
                    switch (TipoArq)
                    {
                        case IdentificaArquivo.TipoArquivo.FEBRABANExtrato:                            
                        case IdentificaArquivo.TipoArquivo.FEBRABANCobranca:
                            SB.AppendFormat("Tipo do arquivo: Modelo padr�o CNAB cobran�a/extrato\r\n{0}", cnab.Relatorio());                                                       
                            break;
                        case IdentificaArquivo.TipoArquivo.COBRANCA:
                            SB.AppendFormat("Tipo do arquivo: Cobran�a em formato antigo, relat�rio n�o implementado!!!!\r\n");
                            break;
                        case IdentificaArquivo.TipoArquivo.EXTRATO:                            
                        case IdentificaArquivo.TipoArquivo.EXTRATOItau:
                            SB.AppendFormat("Tipo do arquivo: Extrato em formato antigo, relat�rio n�o implementado!!!!\r\n");
                            break;
                        case IdentificaArquivo.TipoArquivo.GPS:
                            SB.AppendFormat("Tipo do arquivo: GPS em formato antigo, relat�rio n�o implementado!!!!\r\n");
                            break;
                        case IdentificaArquivo.TipoArquivo.DARF:
                            SB.AppendFormat("Tipo do arquivo: DARF em formato antigo, relat�rio n�o implementado!!!!\r\n");
                            break;
                        case IdentificaArquivo.TipoArquivo.INVALIDO:
                            SB.AppendFormat("Tipo do arquivo: INV�LIDO!!!!\r\n");
                            if(Erro != null)
                                SB.AppendFormat("Erro reportado:\r\n{0}\r\n",Erro);
                            break;
                        case IdentificaArquivo.TipoArquivo.JACARREGADO:
                            SB.AppendFormat("arquivo j� carregado!!!!\r\n");
                            break;
                        case IdentificaArquivo.TipoArquivo.COBRANCABradesco:
                            SB.AppendFormat("Tipo do arquivo: Cobran�a Bradesco\r\n{0}", BoletoRetornoBra.Relatorio());
                            break;
                        case IdentificaArquivo.TipoArquivo.PAGAMENTOELETRONICOBradesco:
                            SB.AppendFormat("Tipo do arquivo: Pagameto eletr�nico Bradesco\r\n{0}", ChequeRetornoBra.Relatorio());                            
                            break;
                        case IdentificaArquivo.TipoArquivo.TRIBUTOELETRONICOBradesco:
                            SB.AppendFormat("Tipo do arquivo: Tributo eletr�nico Bradesco\r\n{0}", TributoRetornoBra.Relatorio());                                                        
                            break;
                        case IdentificaArquivo.TipoArquivo.COBRANCAItau:
                            SB.AppendFormat("Tipo do arquivo: Cobran�a Ita�\r\n{0}", BoletoRetornoItau.Relatorio());                            
                            break;
                        default:
                            break;
                    }
                    SB.AppendLine("");
                    SB.AppendLine("");
                    SB.AppendLine("");
                }
                memoEdit1.Text = SB.ToString();
                Clipboard.SetText(SB.ToString());
                MessageBox.Show("Relat�rio dispon�vel na �rea de tranfer�ncia");
            }
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            PHistorico.Visible = checkEdit1.Checked;
            if (dArquivoRetorno.ARQuivos.Count == 0)
                Calcular();
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            Calcular();
        }

        private void Calcular()
        {
            dArquivoRetorno.ARQuivosTableAdapter.FillByData(dArquivoRetorno.ARQuivos, dateEdit2.DateTime);
        }

        private void cArquivoRetorno_Load(object sender, EventArgs e)
        {
            dateEdit2.DateTime = DateTime.Today;
        }
        
    }
}

