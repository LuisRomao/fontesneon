/*
LH - 08/05/2014       - 13.2.8.44 - FIX erro na baixa dos boletos cancelados 
MR - 21/03/2016 20:00 -           - Tratamento Arquivo de Cobran�a Itau (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
MR - 01/04/2016 19:00 -           - Inclus�o da taifa no arquivo lido para Cobran�a Itau (Altera��es indicadas por *** MRC - INICIO (01/04/2016 19:00) ***)
                                    Trata retorno de tarifas no Cobran�a Itau (status tratado e ignorado)
MR - 28/04/2016 12:00 -           - Inclus�o do tratamento de Status 29 / 95 para Bradesco - status tratado e ignorado (Altera��es indicadas por *** MRC - INICIO (28/04/2016 12:00) ***)
MR - 27/05/2016 12:00 -           - Inclus�o do tratamento de Status 9 para Itau - status tratado e ignorado (Altera��es indicadas por *** MRC - INICIO (27/05/2016 12:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Text;
using ArquivosTXT;
using System.IO;
using Framework;
using Framework.datasets;
using System.Data;
using CompontesBasicosProc;
using dllCNAB;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using CadastrosProc;

namespace dllbanco
{
    /// <summary>
    /// Francesinha
    /// </summary>
    public class Francesinha : IDisposable
    {
        private dFrancesinha _dFrancesinha1;

        private dFrancesinha _dFrancesinha1F;

        private dFrancesinha.ARquivoLidoDataTable ARquivoLidoDataTable1;

        private bool Baixaok;        

        private int Banco;

        private TipoRetorno DetalhamentoRetorno;

        //int iTESTE = 0;

        private DataView DV_ContaCorrenTe;
        private StringBuilder log;
        private SortedList<ARLStatusN, int> Resumo;

        private int Tipo = 0;
        /// <summary>
        /// �ltimo erro
        /// </summary>
        public string Erro;

        /// <summary>
        /// Resultado da opera��o
        /// </summary>
        public string Identificacao;

        /// <summary>
        /// Componente que ir� mostrar o gauge (pode ser null)
        /// </summary>
        public System.Windows.Forms.Control LocalVisivel;

        /// <summary>
        /// Bolto para rastrear
        /// </summary>
        public int? Rastrear;        

        private void addLog(dFrancesinha.ARquivoLidoRow ARLrow, string resposta)
        {
            ARLStatusN Status = (ARLStatusN)ARLrow.ARLStatusN;
            if (Resumo.ContainsKey(Status))
                Resumo[Status] = Resumo[Status] + 1;
            else
                Resumo[Status] = 1;
            if (log == null)
                return;

            log.AppendLine(string.Format("{0:mm:ss:ffff} ARL:{1} BOL{2} status:{3} - {4}", DateTime.Now, ARLrow.ARL, ARLrow.ARL_BOL, Status, resposta));
            if (ARLrow.ARLSTATUS != "")
                log.AppendLine(string.Format("\t{0}", ARLrow.ARLSTATUS));
        }

        private SortedList<string, ContaCorrenteNeon> _PoolDeContas;

        private SortedList<string, ContaCorrenteNeon> PoolDeContas
        {
            get
            {
                if (_PoolDeContas == null)
                {
                    _PoolDeContas = new SortedList<string, ContaCorrenteNeon>();
                    EMPTProcF = new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Filial);
                }
                return _PoolDeContas;
            }
        }
        private FrameworkProc.EMPTProc EMPTProcF;

        private ContaCorrenteNeon BuscaConta(int BCO,int Agencia,int Conta, bool Filial)
        {            
            string Chave = string.Format("{0}-{1}-{2}-{3}",BCO,Agencia,Conta,Filial);
            if (!PoolDeContas.ContainsKey(Chave))
                PoolDeContas.Add(Chave, new ContaCorrenteNeon(BCO, Agencia, Conta,TipoConta.CC, Filial ? EMPTProcF : null));            
            return PoolDeContas[Chave];
        }

        private ContaCorrenteNeon BuscaConta(int BCO, int CNR,bool Filial)
        {            
            string Chave = string.Format("{0}-{1}-{2}", BCO, CNR,Filial);
            if (!PoolDeContas.ContainsKey(Chave))
                PoolDeContas.Add(Chave, new ContaCorrenteNeon(BCO, CNR, Filial ? EMPTProcF : null));
            return PoolDeContas[Chave];
        }



        private string Baixas(int? unico_BOL_Baixar)
        {
            if (DSCentral.USU == 30)
                DetalhamentoRetorno = TipoRetorno.Log;
            CompontesBasicos.Espera.cEspera TelaEspera = null;
            if (LocalVisivel != null)
            {
                TelaEspera = new CompontesBasicos.Espera.cEspera(LocalVisivel);
                TelaEspera.Espere("Efetuando Baixas");
                TelaEspera.AtivaGauge(ARquivoLidoDataTable1.Rows.Count);
            }
            Baixaok = true;
            log = null;
            if (DetalhamentoRetorno == TipoRetorno.Log)
                log = new StringBuilder("INICIO\r\nARL\tBOL\tStatus\tRef\r\n");
            Resumo = new SortedList<ARLStatusN, int>();
            
            List<dFrancesinha.ARquivoLidoRow> rowsBaixar = new List<dFrancesinha.ARquivoLidoRow>();

            //As rows s�o copiadas para rowBaixar para evitar que tenhamos erro ao deletar algumas no processo BaixaUm. A dele��o geraria um erro no foreach se este fosse feito na table e na List
            rowsBaixar.AddRange(ARquivoLidoDataTable1);

            foreach (dFrancesinha.ARquivoLidoRow ARLrow in rowsBaixar)
            {
                if (unico_BOL_Baixar.HasValue && (unico_BOL_Baixar.Value != ARLrow.ARL_BOL))
                    continue;
                int intAgencia = 0;
                int intConta = 0;
                int.TryParse(ARLrow.ARL_CONAgencia, out intAgencia);
                int.TryParse(ARLrow.ARL_CONConta, out intConta);
                ContaCorrenteNeon ContaDoRetorno = null;
                if (ARLrow.ARL_BCO != (int)CodigoBancos.Caixa)
                    ContaDoRetorno = BuscaConta(ARLrow.ARL_BCO, intAgencia, intConta,false);
                else
                    ContaDoRetorno = BuscaConta(ARLrow.ARL_BCO, intConta,false);
                if (!ContaDoRetorno.Encontrado)
                {
                    if (TranfereFilial(ARLrow))
                        continue;                    
                    else
                    {
                        ARLrow.ARLSTATUS = "Boleto de conta n�o cadastrada";
                        ARLrow.ARLStatusN = (int)ARLStatusN.ContaNaoEncontrada;
                        dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                        if (DetalhamentoRetorno == TipoRetorno.Log)
                            log.AppendFormat("{0}\t{1}\t{2}\t1\r\n",ARLrow.ARL,ARLrow.ARL_BOL, ARLStatusN.ContaNaoEncontrada);
                        continue;
                    }
                }
                if ((!ContaDoRetorno.CON.HasValue) || (ContaDoRetorno.Condominio.Status == CONStatus.Inativo))
                {
                    if (TranfereFilial(ARLrow))
                        continue;
                    else
                    {
                        ARLrow.ARLSTATUS = "Boleto de condom�nio inativo";
                        ARLrow.ARLStatusN = (int)ARLStatusN.CondominioInativo;
                        dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                        if (DetalhamentoRetorno == TipoRetorno.Log)
                            log.AppendFormat("{0}\t{1}\t{2}\t2\r\n", ARLrow.ARL, ARLrow.ARL_BOL, ARLStatusN.CondominioInativo);
                        continue;
                    }
                }

                //if (rowCCT.GetRedirecionaCCtRows().Length > 0)
                //    ARLrow.ARL_CCT = rowCCT.GetRedirecionaCCtRows()[0].RCC_CCTDestino;

                ARLrow.ARL_CCT = ContaDoRetorno.CCT.Value;
                ARLrow.ARL_CON = ContaDoRetorno.CON.Value;

                Boletos.Boleto.Boleto BoletoRetornado = new Boletos.Boleto.Boleto(ARLrow.ARLNossoNumero);

                if (!BoletoRetornado.Encontrado)
                {
                    BoletosProc.Boleto.BoletoProc BoletoFilial = new BoletosProc.Boleto.BoletoProc(ARLrow.ARLNossoNumero, new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Filial));
                    if (BoletoFilial.Encontrado && TranfereFilial(ARLrow))
                        continue;
                    ARLrow.ARLSTATUS += "Boleto n�o encontrado";
                    ARLrow.ARLStatusN = (int)ARLStatusN.BoletoNaoEncontrado;
                    dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                    if (DetalhamentoRetorno == TipoRetorno.Log)
                        log.AppendFormat("{0}\t{1}\t{2}\t3\r\n", ARLrow.ARL, ARLrow.ARL_BOL, ARLStatusN.BoletoNaoEncontrado);
                    continue;
                }

                if (BoletoRetornado.Condominio.CON != ContaDoRetorno.CON)
                {
                    bool encontrado = false;
                    foreach (ContaCorrenteNeon SubConta in ContaDoRetorno.ContasFilhasPool.Values)
                    {                        
                        if (SubConta.CON.HasValue && (SubConta.CON == BoletoRetornado.Condominio.CON))
                        {
                            ARLrow.ARL_CON = SubConta.CON.Value;
                            encontrado = true;
                            break;
                        }                        
                    }
                    if (!encontrado)
                    {
                        ARLrow.ARLSTATUS += "O n�mero do boleto retornado n�o se refere a este condom�nio";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BoletoNaoEncontrado;
                        dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                        if (DetalhamentoRetorno == TipoRetorno.Log)
                            log.AppendFormat("{0}\t{1}\t{2}\t4\r\n", ARLrow.ARL, ARLrow.ARL_BOL, ARLStatusN.BoletoNaoEncontrado);
                        continue;
                    }
                }
                
                                                  

                retornoRetornoRegistro retornoRegistro;

                switch (ARLrow.ARL_BCO)
                {
                    case 237:
                        EDIBoletos.BoletoRetornoBra.TiposOcorrencia TipoOcBra = EDIBoletos.BoletoRetornoBra.TiposOcorrencia.LiquidacaoNormal;
                        TipoOcBra = ARLrow.IsARL_IDOcorrenciaNull() ? TipoOcBra : (EDIBoletos.BoletoRetornoBra.TiposOcorrencia)ARLrow.ARL_IDOcorrencia;
                        retornoRegistro = RetornoRegistro(TipoOcBra, BoletoRetornado, ARLrow);
                        break;
                    case 341:
                        EDIBoletos.BoletoRetornoItau.TiposOcorrencia TipoOcItau = EDIBoletos.BoletoRetornoItau.TiposOcorrencia.LiquidacaoNormal;
                        TipoOcItau = ARLrow.IsARL_IDOcorrenciaNull() ? TipoOcItau : (EDIBoletos.BoletoRetornoItau.TiposOcorrencia)ARLrow.ARL_IDOcorrencia;
                        retornoRegistro = RetornoRegistro(TipoOcItau, BoletoRetornado, ARLrow);
                        break;
                    case 104:
                        TiposOcorrenciaCAIXA TipoOcCaixa = TiposOcorrenciaCAIXA.LiquidacaoNormal;
                        TipoOcCaixa = ARLrow.IsARL_IDOcorrenciaNull() ? TipoOcCaixa : (TiposOcorrenciaCAIXA)ARLrow.ARL_IDOcorrencia;
                        retornoRegistro = RetornoRegistro(TipoOcCaixa, BoletoRetornado, ARLrow);
                        break;
                    case 33:
                        TiposOcorrenciaSANTANDER TipoOcSantander = TiposOcorrenciaSANTANDER.LiquidacaoNormal;
                        TipoOcSantander = ARLrow.IsARL_IDOcorrenciaNull() ? TipoOcSantander : (TiposOcorrenciaSANTANDER)ARLrow.ARL_IDOcorrencia;
                        retornoRegistro = RetornoRegistro(TipoOcSantander, BoletoRetornado, ARLrow);
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Banco {0} n�o implementado em Francesinha.cs 1032", ARLrow.ARL_BCO));
                }                
                                                

                if (retornoRegistro == retornoRetornoRegistro.continuar)
                {
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Francesinha Baixas - 504", dFrancesinha1.ARquivoLidoTableAdapter);
                        BaixaUm(ARLrow, BoletoRetornado);
                        addLog(ARLrow, "BaixaUm");
                        dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                        ARLrow.AcceptChanges();
                        VirMSSQL.TableAdapter.CommitSQL();
                    }
                    catch (Exception e)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(e);
                        ARLrow.ARLSTATUS = "Erro Geral:" + e.Message;
                        Baixaok = false;
                        addLog(ARLrow, VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false));
                    };
                }

                if (TelaEspera != null)
                {
                    TelaEspera.Gauge();
                    if (TelaEspera.Abortar)
                    {
                        TelaEspera.Dispose();
                        TelaEspera = null;
                        Baixaok = false;                        
                        return RetornoBaixas();
                    }
                }
            };            

            if (TelaEspera != null)
            {
                TelaEspera.Dispose();
                TelaEspera = null;
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();

            Limpa();
            return RetornoBaixas();
        }

        private dFrancesinha.ContaCorrenTeRow BuscaCCT(int BCO, int Conta, int? CONPreferencial = null, int? CCT = null)
        {
            if (dFrancesinha1.ContaCorrenTe.Count == 0)
            {
                dFrancesinha1.ContaCorrenTeTableAdapter.Fill(dFrancesinha1.ContaCorrenTe);
                _dFrancesinha1.RedirecionaCCtTableAdapter.Fill(dFrancesinha1.RedirecionaCCt);
                DV_ContaCorrenTe = new DataView(dFrancesinha1.ContaCorrenTe);
            }

            if (CCT.HasValue)
                return dFrancesinha1.ContaCorrenTe.FindByCCT(CCT.Value);

            DV_ContaCorrenTe.Sort = "CCT_BCO,CCTConta";
            DataRowView[] DRVs = DV_ContaCorrenTe.FindRows(new object[] { BCO, Conta });
            if (DRVs.Length == 0)
            {
                DV_ContaCorrenTe.Sort = "CCT_BCO,CCTCNR";
                DRVs = DV_ContaCorrenTe.FindRows(new object[] { BCO, Conta });
            }
            if (DRVs.LongLength == 0)
                return null;
            else
            {
                if ((DRVs.LongLength > 1) && (CONPreferencial.HasValue))
                {
                    foreach (DataRowView DRV in DRVs)
                    {
                        dFrancesinha.ContaCorrenTeRow rowCandidato = (dFrancesinha.ContaCorrenTeRow)DRV.Row;
                        if (rowCandidato.CON == CONPreferencial.Value)
                            return rowCandidato;
                    }
                }
                return (dFrancesinha.ContaCorrenTeRow)DRVs[0].Row;
            }
        }

        private bool FrancesinhaManual(int ARL)
        {
            const string comando = "SELECT ARQuivos.ARQTipo FROM ARquivoLido INNER JOIN ARQuivos ON ARquivoLido.ARL_ARQ = ARQuivos.ARQ WHERE (ARquivoLido.ARL = @P1)";
            return VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(comando, ARL) == (int)ARQTipo.Manual;
        }       

        private bool NumeracaoValida(int BOL)
        {
            if (DSCentral.EMP == 1)
                return (((BOL > 10000000) && (BOL < 30000000))
                             ||
                         ((BOL > 40000000) && (BOL < 50000000))
                       );
            else
                return (((BOL > 10000000) && (BOL < 20000000))
                             ||
                         ((BOL > 30000000) && (BOL < 40000000))
                             ||
                         ((BOL > 50000000) && (BOL < 60000000)));
        }
        //private SortedList<Framework.ARLStatusN, int> Resumo;

        private string RetornoBaixas()
        {
            string strResumo = "";
            foreach (ARLStatusN st in Resumo.Keys)
                strResumo += string.Format("\t{0,10}:\t{1:n0}\r\n", Enumeracoes.virEnumARLStatusN.virEnumARLStatusNSt.Descritivo(st), Resumo[st]);
            switch (DetalhamentoRetorno)
            {
                case TipoRetorno.ok_Falha:
                    return Baixaok ? "ok" : "falha";
                case TipoRetorno.resumo:
                    return strResumo;
                case TipoRetorno.DetalharErro:
                    return "n�o implementado";
                case TipoRetorno.DetalhaPendentes_erro:
                    return "n�o implementado";
                case TipoRetorno.Log:
                    return strResumo + log.ToString() + "FIM";
                default:
                    return "Retorno n�o previsto";
            }
        }

        private List<int> SeparaCodigos(string Motivo)
        {
            List<int> retorno = new List<int>();
            for(int i = 0;i<5;i++)
            {
                int codigo = int.Parse(Motivo.Substring(2 * i, 2));
                if (!retorno.Contains(codigo))
                    retorno.Add(codigo);
            }
            return retorno;
        }

        private retornoRetornoRegistro RetornoRegistro(EDIBoletos.BoletoRetornoBra.TiposOcorrencia TipoOc,
                                                       Boletos.Boleto.Boleto BoletoRetornado,
                                                       dFrancesinha.ARquivoLidoRow ARLrow)
        {
            retornoRetornoRegistro retorno = retornoRetornoRegistro.parar;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllboletos Francesinha Baixas - 662", dFrancesinha1.ARquivoLidoTableAdapter);
                switch (TipoOc)
                {
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.EntradaConfirmada:
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        if (BoletoRetornado.BOLStatusRegistro.EstaNoGrupo(StatusRegistroBoleto.BoletoRegistrado,
                                                                          StatusRegistroBoleto.BoletoRegistradoDA,
                                                                          StatusRegistroBoleto.BoletoRegistrando,
                                                                          StatusRegistroBoleto.BoletoRegistrandoDA,
                                                                          StatusRegistroBoleto.BoletoAguardandoRetorno, 
                                                                          StatusRegistroBoleto.BoletoAguardandoRetornoDA))
                        {
                            List<int> codigos = SeparaCodigos(ARLrow.ARL_MotivoOcorrencia);
                            if (codigos.Contains(76))
                            {
                                BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistradoDDA);
                                ARLrow.ARLSTATUS = "Retorno de Registro (confirmado) DDA";                                
                            }
                            else
                            {
                                if (codigos.Contains(67))
                                {
                                    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistradoDA);
                                    ARLrow.ARLSTATUS = "Cadastro de DA (confirmado)";                                    
                                }
                                else
                                {
                                    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                                    ARLrow.ARLSTATUS = "Retorno de Registro (confirmado)";                                    
                                }
                                if (codigos.Contains(68))
                                {
                                    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Registrado com DA recusado: erro nos dados de remessa");
                                    ARLrow.ARLSTATUS = "Registrado com DA recusado: erro nos dados de remessa";
                                    ARLrow.ARLStatusN = (int)ARLStatusN.DebitoAutomaticoRecusado;
                                }
                                if (codigos.Contains(69))
                                {
                                    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Registrado com DA recusado: Sacado n�o consta no cadastro de autorizante");
                                    ARLrow.ARLSTATUS = "Registrado com DA recusado: Sacado n�o consta no cadastro de autorizante";
                                    ARLrow.ARLStatusN = (int)ARLStatusN.DebitoAutomaticoRecusado;
                                }
                                if (codigos.Contains(70))
                                {
                                    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Registrado com DA recusado: Cedente n�o autorizado pelo Sacado");
                                    ARLrow.ARLSTATUS = "Registrado com DA recusado: Cedente n�o autorizado pelo Sacado";
                                    ARLrow.ARLStatusN = (int)ARLStatusN.DebitoAutomaticoRecusado;
                                }
                                if (codigos.Contains(71))
                                {
                                    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Registrado com DA recusado: Cedente n�o participa da modalidade de d�b.autom�tico");
                                    ARLrow.ARLSTATUS = "Registrado com DA recusado: Cedente n�o participa da modalidade de d�b.autom�tico";
                                    ARLrow.ARLStatusN = (int)ARLStatusN.DebitoAutomaticoRecusado;
                                }
                                if (codigos.Contains(73))
                                {
                                    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Registrado com DA recusado: Data de vencimento inv�lida/vencida");
                                    ARLrow.ARLSTATUS = "Registrado com DA recusado: Data de vencimento inv�lida/vencida";
                                    ARLrow.ARLStatusN = (int)ARLStatusN.DebitoAutomaticoRecusado;
                                }
                                if (codigos.Contains(74))
                                {
                                    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado,"Registrado com DA recusado: erro nos dados de remessa");
                                    ARLrow.ARLSTATUS = "Registrado com DA recusado: erro nos dados de remessa";
                                    ARLrow.ARLStatusN = (int)ARLStatusN.DebitoAutomaticoRecusado;
                                }
                            }
                        }
                        
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.EntradaRejeitada:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ErroNoRegistro);
                        ARLrow.ARLSTATUS = "Retorno de Registro (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.LiquidacaoNaoRegistrado:
                        if (BoletoRetornado.Encontrado)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ComregistroNaoRegistrado,"Liquida��o n�o registrado");
                        //n�o altera ARLrow.ARLStatusN
                        retorno = retornoRetornoRegistro.continuar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.LiquidacaoNormal:
                        //if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                        //    BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        //n�o altera ARLrow.ARLStatusN
                        retorno = retornoRetornoRegistro.continuar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.AbatimentoConcedido:
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.ConfPedidoAlterOutros:
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.VencimentoAlterado:
                        BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Altera��o de dados (confirmado)");
                        ARLrow.ARLSTATUS = "Altera��o de dados (confirmado)";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.AlteracaoOutrosRejeitada:
                        ARLrow.ARLSTATUS = "Retorno de Altera��o de Registro (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                        {
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ErroNoRegistro, "Retorno de Altera��o de Registro (rejeitado)");
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        }
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.InstrucaoRejeitada:
                        ARLrow.ARLSTATUS = "Retorno Instru��o (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                        {
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ErroNoRegistro, "Retorno Instru��o (rejeitado)");
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        }
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.BaixadoAutomatVArquivo:
                        ARLrow.ARLSTATUS = "Registro Cancelado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        if (BoletoRetornado.Encontrado)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistroVencido,"Baixando via arquivo segundo o banco.");
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.BaixadoAgencia:                    
                        ARLrow.ARLSTATUS = "Registro Cancelado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        if (BoletoRetornado.Encontrado)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistroVencido);
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.AbatimentoCancelado:
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.TituloComPagamentoCancelado:
                        /*
                        PODERIA CONTROLAR SE O REGISTRO FOI CANCELADO
                        if (BoletoRetornado.BOLStatusRegistro == CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistrado)
                        {                            
                            if (Convert.ToInt32(ARLrow.ARL_MotivoOcorrencia.Substring(0, 2)) == 76)                         
                                BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistradoDDA);
                            else
                                BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistrado);
                        }*/
                        ARLrow.ARLSTATUS = "Registro Cancelado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.TituloChequeVinculado:
                        if (!BoletoRetornado.rowPrincipal.IsBOLPagamentoNull())
                        {
                            ARLrow.ARLSTATUS = "Pagamento em Cheque OK";
                            ARLrow.ARLStatusN = (int)ARLStatusN.PagoEmChequeOK;
                        }
                        else
                        {
                            ARLrow.ARLSTATUS = "Pagamento em Cheque Aguardando";
                            ARLrow.ARLStatusN = (int)ARLStatusN.PagoEmChequeAguardo;
                        }
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.Sacado:
                        if (Convert.ToInt32(ARLrow.ARL_MotivoOcorrencia.Substring(0, 2)).EstaNoGrupo(78))
                        {
                            ARLrow.ARLSTATUS = "Sacado DDA recusado";
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        }
                        else if (Convert.ToInt32(ARLrow.ARL_MotivoOcorrencia.Substring(0, 2)).EstaNoGrupo(0, 95))
                        {
                            //N�o documentado, deve ser que o cliente aceitou o DDA
                            ARLrow.ARLSTATUS = "Sacado - DDA aceito";
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        }
                        else
                        {
                            ARLrow.ARLSTATUS = "Retorno n�o Homologado";
                            ARLrow.ARLStatusN = (int)ARLStatusN.RetornoNaoHomologado;
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Retorno de registro n�o homologado ARL = {0} IDOCORENCIA = {1} Motivo = {2} - Francesinha.cs 613", ARLrow.ARL, TipoOc, ARLrow.ARL_MotivoOcorrencia), "Registro n�o homologado");
                        }
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.ProtestoAceito:
                        ARLrow.ARLSTATUS = "Altera��o de dados (confirmado)";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        BoletoRetornado.rowPrincipal.BOLProtestado = true;
                        BoletoRetornado.GravaJustificativa("Protesto solicitado no site do banco");
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.CanProtestoAceito:
                        ARLrow.ARLSTATUS = "Altera��o de dados (confirmado)";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        BoletoRetornado.rowPrincipal.BOLProtestado = false;
                        BoletoRetornado.GravaJustificativa("Cancelamento de Protesto solicitado no site do banco");
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.DebitoTarifas:
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoBra.TiposOcorrencia.DesagendamentoDebitoAutomatico:
                        ARLrow.ARLSTATUS = "Desagendamento do D�bito Autom�tico";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        if (BoletoRetornado.StatusRegistro == StatusRegistroBoleto.BoletoRegistradoDA)
                        {
                            List<int> codigos = SeparaCodigos(ARLrow.ARL_MotivoOcorrencia);
                            if (codigos.Contains(81))
                                BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistroVencido, "Desagendamento do D�bito Autom�tico - Tentativas esgotadas");
                            else if (codigos.Contains(84))
                                BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistroVencido, "Desagendamento do D�bito Autom�tico - Cancelado pelo sacado");
                            else if (codigos.Contains(82))
                                BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Desagendamento do D�bito Autom�tico - Tentativas esgotadas");
                            else if (codigos.Contains(83))
                                BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Desagendamento do D�bito Autom�tico - Cancelado pelo sacado");
                            else
                                BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado, "Desagendamento do D�bito Autom�tico");
                        }
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    default:
                        ARLrow.ARLSTATUS = "Retorno n�o Homologado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RetornoNaoHomologado;
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Retorno de registro n�o homologado ARL = {0} Ocorr�ncia = {1} Motivo = {2} - Francesinha.cs 657", ARLrow.ARL, TipoOc, ARLrow.ARL_MotivoOcorrencia), "Registro rejeitado");
                        retorno = retornoRetornoRegistro.parar;
                        break;
                }
                dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                ARLrow.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }
            return retorno;
        }

        private retornoRetornoRegistro RetornoRegistro(EDIBoletos.BoletoRetornoItau.TiposOcorrencia TipoOc,
                                                       Boletos.Boleto.Boleto BoletoRetornado,
                                                       dFrancesinha.ARquivoLidoRow ARLrow)
        {
            retornoRetornoRegistro retorno = retornoRetornoRegistro.parar;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllboletos Francesinha Baixas - 810", dFrancesinha1.ARquivoLidoTableAdapter);
                switch (TipoOc)
                {
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.EntradaConfirmada:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        ARLrow.ARLSTATUS = "Retorno de Registro (confirmado)";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.EntradaRejeitada:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ErroNoRegistro);
                        ARLrow.ARLSTATUS = "Retorno de Registro (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.LiquidacaoNormal:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        //n�o altera ARLrow.ARLStatusN
                        retorno = retornoRetornoRegistro.continuar;
                        break;
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AbatimentoConcedido:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.VencimentoAlterado:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AlteracaoConfirmada:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        ARLrow.ARLSTATUS = "Altera��o de Dados (confirmado)";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AlteracaoExclusaoRejeitada:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.CobrancaContratualInstrucaoAlteracaoRejeitada:
                        ARLrow.ARLSTATUS = "Retorno de Altera��o de Registro (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                        {
                            if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                                BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ErroNoRegistro);
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        }
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.InstrucaoRejeitada:
                        ARLrow.ARLSTATUS = "Retorno Instru��o (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                        {
                            if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                                BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ErroNoRegistro);
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        }
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AbatimentoCancelado:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaPorProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaTransferenciaDesconto:
                        ARLrow.ARLSTATUS = "Registro Cancelado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.ChequeCompensado:
                        if (!BoletoRetornado.rowPrincipal.IsBOLPagamentoNull())
                        {
                            ARLrow.ARLSTATUS = "Pagamento em Cheque OK";
                            ARLrow.ARLStatusN = (int)ARLStatusN.PagoEmChequeOK;
                        }
                        else
                        {
                            ARLrow.ARLSTATUS = "Pagamento em Cheque Aguardando";
                            ARLrow.ARLStatusN = (int)ARLStatusN.PagoEmChequeAguardo;
                        }
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    //*** MRC - INICIO (01/04/2016 19:00) ***
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaAvisoCobranca:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaExtratoPosicao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaRelacaoLiquidacao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaManutencaoTitulosVencidos:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifasMensais:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaEmissaoBoletoOuDuplicata:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaInstrucao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaOcorrencia:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEmissaoBoletoOuDuplicata:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalExtratoPosicao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalInstrucoes:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalManutencaoTitulosVencidos:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalOcorrencias:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalSustacaoProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEntradaBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalBaixasCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalBaixasBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalLiquidacaoCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalLiquidacaoBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaEmissaoAvisoMovimentacao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEmissaoAvisoMovimentacao:
                        ARLrow.ARLSTATUS = "Cobran�a de tarifa";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    //*** MRC - TERMINO (01/04/2016 19:00) ***
                    //*** MRC - INICIO (27/05/2016 12:00) ***
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaSimples:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaLiquidacao:
                        ARLrow.ARLSTATUS = "Baixa Simples de Tarifa";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.ConfRecInstNaoProtesto:
                        ARLrow.ARLSTATUS = "instru��o aceita";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    default:
                        ARLrow.ARLSTATUS = "Retorno n�o Homologado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RetornoNaoHomologado;
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;neon@vcommerce.com.br", string.Format("Retorno de registro n�o homologado ARL = {0} Ocorr�ncia = {1} Motivo = {2} - Francesinha.cs 802", ARLrow.ARL, TipoOc, ARLrow.ARL_MotivoOcorrencia), "Registro rejeitado");
                        retorno = retornoRetornoRegistro.parar;
                        break;
                }
                dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                ARLrow.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }
            return retorno;
        }

        private retornoRetornoRegistro RetornoRegistro(TiposOcorrenciaCAIXA TipoOc,
                                                       Boletos.Boleto.Boleto BoletoRetornado,
                                                       dFrancesinha.ARquivoLidoRow ARLrow)
        {
            retornoRetornoRegistro retorno = retornoRetornoRegistro.parar;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllboletos Francesinha Baixas - 986", dFrancesinha1.ARquivoLidoTableAdapter);
                switch (TipoOc)
                {
                    case TiposOcorrenciaCAIXA.EntradaConfirmada:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        ARLrow.ARLSTATUS = "Retorno de Registro (confirmado)";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case TiposOcorrenciaCAIXA.EntradaRejeitada:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ErroNoRegistro);
                        ARLrow.ARLSTATUS = "Retorno de Registro (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case TiposOcorrenciaCAIXA.LiquidacaoNormal:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        //n�o altera ARLrow.ARLStatusN
                        retorno = retornoRetornoRegistro.continuar;
                        break;
                    case TiposOcorrenciaCAIXA.AbatimentoConcedido:
                    //case TiposOcorrenciaCAIXA.ConfPedidoAlterOutros:
                    case TiposOcorrenciaCAIXA.VencimentoAlterado:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        ARLrow.ARLSTATUS = "Altera��o de dados (confirmado)";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        if (TipoOc == TiposOcorrenciaCAIXA.AbatimentoConcedido)
                        {
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Retorno de altera��o de registro confirmado CAIXA ARL = {0} Ocorrencia = {1} Motivo = {2}", ARLrow.ARL, TipoOc, ARLrow.ARL_MotivoOcorrencia), "Registro Confirmado");
                        }
                        break;
                    case TiposOcorrenciaCAIXA.Baixa:
                        /*
                        PODERIA CONTROLAR SE O REGISTRO FOI CANCELADO
                        if (BoletoRetornado.BOLStatusRegistro == CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistrado)
                        {                            
                            if (Convert.ToInt32(ARLrow.ARL_MotivoOcorrencia.Substring(0, 2)) == 76)                         
                                BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistradoDDA);
                            else
                                BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistrado);
                        }*/
                        ARLrow.ARLSTATUS = "Registro Cancelado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistroVencido);
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case TiposOcorrenciaCAIXA.AceiteDDA:
                        ARLrow.ARLSTATUS = "Sacado - DDA aceito";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case TiposOcorrenciaCAIXA.RecusaDDA:
                        ARLrow.ARLSTATUS = "Sacado - DDA recusado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    /*
                case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AbatimentoConcedido:
                case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.VencimentoAlterado:
                case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AlteracaoConfirmada:
                    if (BoletoRetornado.BOLStatusRegistro == CompontesBasicosProc.StatusRegistroBoleto.BoletoAguardandoRetorno)
                        BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistrado);
                    ARLrow.ARLSTATUS = "Altera��o de Dados (confirmado)";
                    ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                    retorno = retornoRetornoRegistro.parar;
                    break;
                    */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AlteracaoExclusaoRejeitada:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.CobrancaContratualInstrucaoAlteracaoRejeitada:
                        ARLrow.ARLSTATUS = "Retorno de Altera��o de Registro (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                        {
                            if (BoletoRetornado.BOLStatusRegistro == CompontesBasicosProc.StatusRegistroBoleto.BoletoAguardandoRetorno)
                                BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.ErroNoRegistro);
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        }
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.InstrucaoRejeitada:
                        ARLrow.ARLSTATUS = "Retorno Instru��o (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                        {
                            if (BoletoRetornado.BOLStatusRegistro == CompontesBasicosProc.StatusRegistroBoleto.BoletoAguardandoRetorno)
                                BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.ErroNoRegistro);
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        }
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AbatimentoCancelado:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaPorProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaTransferenciaDesconto:
                        ARLrow.ARLSTATUS = "Registro Cancelado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.ChequeCompensado:
                        if (!BoletoRetornado.rowPrincipal.IsBOLPagamentoNull())
                        {
                            ARLrow.ARLSTATUS = "Pagamento em Cheque OK";
                            ARLrow.ARLStatusN = (int)ARLStatusN.PagoEmChequeOK;
                        }
                        else
                        {
                            ARLrow.ARLSTATUS = "Pagamento em Cheque Aguardando";
                            ARLrow.ARLStatusN = (int)ARLStatusN.PagoEmChequeAguardo;
                        }
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaAvisoCobranca:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaExtratoPosicao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaRelacaoLiquidacao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaManutencaoTitulosVencidos:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifasMensais:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaEmissaoBoletoOuDuplicata:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaInstrucao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaOcorrencia:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEmissaoBoletoOuDuplicata:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalExtratoPosicao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalInstrucoes:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalManutencaoTitulosVencidos:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalOcorrencias:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalSustacaoProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEntradaBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalBaixasCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalBaixasBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalLiquidacaoCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalLiquidacaoBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaEmissaoAvisoMovimentacao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEmissaoAvisoMovimentacao:
                        ARLrow.ARLSTATUS = "Cobran�a de tarifa";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    */
                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaSimples:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaLiquidacao:
                        ARLrow.ARLSTATUS = "Baixa Simples de Tarifa";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */
                    /*
                case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.ConfRecInstNaoProtesto:
                    ARLrow.ARLSTATUS = "instru��o aceita";
                    ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                    retorno = retornoRetornoRegistro.parar;
                    break;
                    */
                    default:
                        ARLrow.ARLSTATUS = "Retorno n�o Homologado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RetornoNaoHomologado;
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Retorno de registro n�o homologado CAIXA ARL = {0} Ocorrencia = {1} Motivo = {2}", ARLrow.ARL, TipoOc, ARLrow.ARL_MotivoOcorrencia), "Registro rejeitado");
                        retorno = retornoRetornoRegistro.parar;
                        break;
                }
                dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                ARLrow.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }
            return retorno;
        }

        private retornoRetornoRegistro RetornoRegistro(TiposOcorrenciaSANTANDER TipoOc,
                                                       Boletos.Boleto.Boleto BoletoRetornado,
                                                       dFrancesinha.ARquivoLidoRow ARLrow)
        {
            retornoRetornoRegistro retorno = retornoRetornoRegistro.parar;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllboletos Francesinha Baixas - 986", dFrancesinha1.ARquivoLidoTableAdapter);
                switch (TipoOc)
                {
                    case TiposOcorrenciaSANTANDER.EntradaConfirmada:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        ARLrow.ARLSTATUS = "Retorno de Registro (confirmado)";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case TiposOcorrenciaSANTANDER.EntradaRejeitada:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.ErroNoRegistro);
                        ARLrow.ARLSTATUS = "Retorno de Registro (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    case TiposOcorrenciaSANTANDER.LiquidacaoNormal:
                        if (BoletoRetornado.BOLStatusRegistro == StatusRegistroBoleto.BoletoAguardandoRetorno)
                            BoletoRetornado.AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        //n�o altera ARLrow.ARLStatusN
                        retorno = retornoRetornoRegistro.continuar;
                        break;
                    /*
                case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AbatimentoConcedido:
                case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.VencimentoAlterado:
                case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AlteracaoConfirmada:
                    if (BoletoRetornado.BOLStatusRegistro == CompontesBasicosProc.StatusRegistroBoleto.BoletoAguardandoRetorno)
                        BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistrado);
                    ARLrow.ARLSTATUS = "Altera��o de Dados (confirmado)";
                    ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                    retorno = retornoRetornoRegistro.parar;
                    break;
                    */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AlteracaoExclusaoRejeitada:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.CobrancaContratualInstrucaoAlteracaoRejeitada:
                        ARLrow.ARLSTATUS = "Retorno de Altera��o de Registro (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                        {
                            if (BoletoRetornado.BOLStatusRegistro == CompontesBasicosProc.StatusRegistroBoleto.BoletoAguardandoRetorno)
                                BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.ErroNoRegistro);
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        }
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.InstrucaoRejeitada:
                        ARLrow.ARLSTATUS = "Retorno Instru��o (rejeitado)";
                        if (Boletos.Boleto.Boleto.StatusRegistroAguarda.Contains(BoletoRetornado.BOLStatusRegistro) && (!BoletoRetornado.rowPrincipal.BOLCancelado) && (BoletoRetornado.rowPrincipal.IsBOLPagamentoNull()))
                        {
                            if (BoletoRetornado.BOLStatusRegistro == CompontesBasicosProc.StatusRegistroBoleto.BoletoAguardandoRetorno)
                                BoletoRetornado.AlteraStatusRegistro(CompontesBasicosProc.StatusRegistroBoleto.ErroNoRegistro);
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistroRejeitado;
                        }
                        else
                            ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.AbatimentoCancelado:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaPorProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaTransferenciaDesconto:
                        ARLrow.ARLSTATUS = "Registro Cancelado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RegistoRejeitadoJaTratado;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.ChequeCompensado:
                        if (!BoletoRetornado.rowPrincipal.IsBOLPagamentoNull())
                        {
                            ARLrow.ARLSTATUS = "Pagamento em Cheque OK";
                            ARLrow.ARLStatusN = (int)ARLStatusN.PagoEmChequeOK;
                        }
                        else
                        {
                            ARLrow.ARLSTATUS = "Pagamento em Cheque Aguardando";
                            ARLrow.ARLStatusN = (int)ARLStatusN.PagoEmChequeAguardo;
                        }
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */

                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaAvisoCobranca:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaExtratoPosicao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaRelacaoLiquidacao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaManutencaoTitulosVencidos:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifasMensais:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaEmissaoBoletoOuDuplicata:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaInstrucao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaOcorrencia:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEmissaoBoletoOuDuplicata:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalExtratoPosicao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalInstrucoes:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalManutencaoTitulosVencidos:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalOcorrencias:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalSustacaoProtesto:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEntradaBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalBaixasCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalBaixasBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalLiquidacaoCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalLiquidacaoBancosCarteira:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaEmissaoAvisoMovimentacao:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.TarifaMensalEmissaoAvisoMovimentacao:
                        ARLrow.ARLSTATUS = "Cobran�a de tarifa";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                    */
                    /*
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaSimples:
                    case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.BaixaLiquidacao:
                        ARLrow.ARLSTATUS = "Baixa Simples de Tarifa";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BaixadoTarifa;
                        retorno = retornoRetornoRegistro.parar;
                        break;
                        */
                    /*
                case EDIBoletos.BoletoRetornoItau.TiposOcorrencia.ConfRecInstNaoProtesto:
                    ARLrow.ARLSTATUS = "instru��o aceita";
                    ARLrow.ARLStatusN = (int)ARLStatusN.RegistroConfirmado;
                    retorno = retornoRetornoRegistro.parar;
                    break;
                    */
                    default:
                        ARLrow.ARLSTATUS = "Retorno n�o Homologado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.RetornoNaoHomologado;
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Retorno de registro n�o homologado Santander ARL = {0} Ocorrencia = {1} Motivo = {2}", ARLrow.ARL, TipoOc, ARLrow.ARL_MotivoOcorrencia), "Registro rejeitado");
                        retorno = retornoRetornoRegistro.parar;
                        break;
                }
                dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                ARLrow.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }
            return retorno;
        }

        internal bool RegistraBOLExtra(dFrancesinha.ARquivoLidoRow ARLrow, Boletos.Boleto.Boleto Boleto, TipoExtra Tipo)
        {
            return RegistraBOLExtra(ARLrow, Boleto.CON, Boleto.APT, Boleto.Proprietario, Boleto.Competencia, Boleto.BOL, Boleto.Unidade, Tipo);
        }

        internal bool RegistraBOLExtra(dFrancesinha.ARquivoLidoRow ARLrow, int CON, int? APT, bool Proprietario, Framework.objetosNeon.Competencia comp, int BOL, string Unidade, TipoExtra Tipo)
        {
            int CTL = dPLAnocontas.dPLAnocontasSt.CTLPadrao(CON, CTLTipo.CreditosAnteriores);
            ARLrow.ARLSTATUS = Tipo == TipoExtra.Duplicidade ? "Duplicidade Autom�tico" : "Extra";
            Boletos.Boleto.Boleto BoletoExtra = new Boletos.Boleto.Boleto(CON, APT, Proprietario, comp, ARLrow.ARL_BOLPagamento, "E", StatusBoleto.Extra);
            string Descritivo1;
            string Historico;
            switch (Tipo)
            {
                case TipoExtra.Duplicidade:
                    Descritivo1 = string.Format("Pagamento em duplicidade. Unidade {0}", Unidade);
                    Historico = "Francesinha - Duplicidade ";
                    break;
                case TipoExtra.Cancelado:
                    Descritivo1 = string.Format("Cr�dito boleto {0}. Unidade {1}", BOL, Unidade);
                    Historico = "Francesinha - Pagamento de boleto cancelado ";
                    break;
                case TipoExtra.NaoEncontrado:
                default:
                    Descritivo1 = string.Format("Cr�dito boleto {0}", BOL);
                    Historico = "Francesinha - Pagamento de boleto com n�mero inv�lido";
                    break;
            }
            string PLAC = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraC);
            string PLAD = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD);
            BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBODO = BoletoExtra.IncluiBOD(PLAC, Descritivo1, ARLrow.ARLValor, false, null, CTL);
            int? CCD = null;
            if (!ARLrow.IsARL_CCDNull())
                CCD = ARLrow.ARL_CCD;
            //if (Tipo == TipoExtra.Duplicidade)
            BoletoExtra.TravaBaixaAutomaticaExtra = true;
            BoletoExtra.Baixa(Historico, ARLrow.ARL_BOLPagamento, ARLrow.ARL_BOLValorPago, 0, CCD, ARLrow.ARL);
            Framework.objetosNeon.Competencia proxComp = Framework.objetosNeon.Competencia.Ultima(CON);
            proxComp++;
            ARLrow.ARLStatusN = (int)ARLStatusN.PagamentoExtra;
            return true;
        }




        private bool TranfereFilial(dFrancesinha.ARquivoLidoRow ARLrow)
        {
            int intAgencia = 0;
            int intConta = 0;
            int.TryParse(ARLrow.ARL_CONAgencia, out intAgencia);
            int.TryParse(ARLrow.ARL_CONConta, out intConta);
            ContaCorrenteNeon ContaFilial;
            if (ARLrow.ARL_BCO == (int)CodigoBancos.Bradesco)
                ContaFilial = BuscaConta(ARLrow.ARL_BCO, intAgencia, intConta, true);
            else
                ContaFilial = BuscaConta(ARLrow.ARL_BCO, intConta, true);
            if ((ContaFilial.Encontrado) && (ContaFilial.Condominio != null) && (ContaFilial.Condominio.Status != CONStatus.Inativo))
            {
                dFrancesinha.ARQuivosRow rowARQ = ARLrow.ARQuivosRow;
                if (rowARQ == null)
                {
                    dFrancesinha1.ARQuivosTableAdapter.FillByARQ(dFrancesinha1.ARQuivos, ARLrow.ARL_ARQ);
                    rowARQ = ARLrow.ARQuivosRow;
                }

                dFrancesinha.ARQuivosRow rowARQF = dFrancesinha1F.Busca_rowARQuivos(rowARQ.ARQNome);

                if (rowARQF == null)
                {
                    if (dFrancesinha1F.ARQuivosTableAdapter.FillByNome(dFrancesinha1F.ARQuivos, rowARQ.ARQNome) > 0)
                        rowARQF = dFrancesinha1F.ARQuivos[0];

                }

                if (rowARQF == null)
                {
                    rowARQF = dFrancesinha1F.ARQuivos.NewARQuivosRow();
                    rowARQF.ARQBanco = rowARQ.ARQBanco;
                    rowARQF.ARQIData = rowARQ.ARQIData;
                    rowARQF.ARQNome = rowARQ.ARQNome;
                    if (!rowARQ.IsARQNomeArquivoNull())
                        rowARQF.ARQNomeArquivo = rowARQ.ARQNomeArquivo;
                    if (!rowARQ.IsARQTipoNull())
                        rowARQF.ARQTipo = rowARQ.ARQTipo;
                    dFrancesinha1F.ARQuivos.AddARQuivosRow(rowARQF);
                    dFrancesinha1F.ARQuivosTableAdapter.Update(rowARQF);
                }

                dFrancesinha.ARquivoLidoRow rowARLF = dFrancesinha1F.ARquivoLido.NewARquivoLidoRow();
                foreach (DataColumn DC in dFrancesinha1.ARquivoLido.Columns)
                {
                    if (DC.ColumnName.EstaNoGrupo("ARL_CCT"))
                        continue;
                    if (!DC.ReadOnly)
                        if (DC.ColumnName == "ARL_ARQ")
                            rowARLF["ARL_ARQ"] = rowARQF.ARQ;
                        else                                                    
                            rowARLF[DC.ColumnName] = ARLrow[DC];                        
                }
                dFrancesinha1F.ARquivoLido.AddARquivoLidoRow(rowARLF);
                try
                {
                    if (DetalhamentoRetorno == TipoRetorno.Log)
                        log.AppendFormat("{0}\t{1}\t{2}\tT\r\n", ARLrow.ARL, ARLrow.ARL_BOL, "TRANFERIDO");
                    VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("transfere ARL Fracesinha.cs 1171");
                    dFrancesinha1.ARquivoLidoTableAdapter.EmbarcaEmTransST();
                    dFrancesinha1F.ARquivoLidoTableAdapter.EmbarcaEmTransST();
                    dFrancesinha1F.ARquivoLidoTableAdapter.Update(rowARLF);
                    ARLrow.Delete();
                    dFrancesinha1.ARquivoLidoTableAdapter.Update(ARLrow);
                    VirMSSQL.TableAdapter.CommitSQLDuplo();                    
                    return true;
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                    return false;
                }
            }
            else
                return false;

            

        }

        /// <summary>
        /// Faz a baixa de um boleto
        /// </summary>
        /// <param name="ARLrow"></param>        
        /// <param name="Boleto"></param>
        /// <returns></returns>
        private bool BaixaUm(dFrancesinha.ARquivoLidoRow ARLrow,  Boletos.Boleto.Boleto Boleto)
        {
            

            //int nConta = int.Parse(ARLrow.ARL_CONConta);
            //if (!Boleto.Encontrado)
            //{                
                /*
                
                if (rowCCT != null)
                {
                    ARLrow.ARL_CON = rowCCT.CON;
                    ARLrow.ARL_CCT = rowCCT.CCT;
                    //BuscaEMP = (int)BuscaCONEMP[1];
                    if (rowCCT.CONStatus == 0)
                    {
                        ARLrow.ARLSTATUS += "|Inativo";
                        ARLrow.ARLStatusN = (int)ARLStatusN.CondominioInativo;
                    }
                    else
                        if (rowCCT.CON_EMP == DSCentral.EMP)
                    {
                        ARLrow.ARLSTATUS += "|Boleto n�o encontrado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BoletoNaoEncontrado;
                    }
                    else
                    {
                        //ARLrow.ARLSTATUS += "|Boleto de outra filial";
                        //ARLrow.ARLStatusN = (int)ARLStatusN.CondominioInativo;
                        TranfereFilial(ARLrow);
                    }
                }
                else
                {
                    ARLrow.ARLSTATUS += "|Condominio/Conta n�o encontrado para esta conta corrente";
                    if (NumeracaoValida(ARLrow.ARL_BOL))
                        ARLrow.ARLStatusN = (int)ARLStatusN.ContaNaoEncontrada;
                    else
                        ARLrow.ARLStatusN = (int)ARLStatusN.CondominioInativo;
                };

                */

            //}
            //else
            //{
                
                //ARLrow.ARL_CON = Boleto.CON;
                //int? CCT = null;
                //if (!ARLrow.IsARL_CCTNull())
                //    CCT = ARLrow.ARL_CCT;
                //dFrancesinha.ContaCorrenTeRow rowCCT = BuscaCCT(ARLrow.ARL_BCO, nConta, Boleto.CON, CCT);
                //if (rowCCT != null)
                //{
                  //  ARLrow.ARL_CCT = rowCCT.CCT;
                    //int? CCTASub = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select RCC_CCTDestino from RedirecionaCCt where RCC_CCTOrigem = @P1", ARLrow.ARL_CCT);
                 //   if (rowCCT.GetRedirecionaCCtRows().Length > 0)
                 //       ARLrow.ARL_CCT = rowCCT.GetRedirecionaCCtRows()[0].RCC_CCTDestino;
                //}
                if (!Boleto.rowPrincipal.IsBOL_APTNull())
                    ARLrow.ARL_APT = Boleto.rowPrincipal.BOL_APT;
                ARLrow.ARLTotal = ARLrow.ARLValorOriginal = Boleto.rowPrincipal.BOLValorPrevisto;
                ARLrow.ARL_BOLVencto = Boleto.rowPrincipal.BOLVencto;
                /*  //caso a data de pagamento nao tenha vindo no arquivo
                if (ARLrow.IsARL_DataNull())
                {
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Data ausente ARL = {0}", ARLrow.ARL), "Confirma��o");
                    if ((Boleto.rowComplementar.IsCOND0Null()) || (!Boleto.rowComplementar.COND0))
                        ARLrow.ARL_Data = Framework.datasets.dFERiados.DiasUteis(ARLrow.ARL_BOLPagamento,1,Framework.datasets.dFERiados.Sentido.retorna);
                    else
                        ARLrow.ARL_Data = ARLrow.ARL_BOLPagamento;
                };
                */


                //***** ATEN��O *****
                //esta checagem da ag�ncia contra o rowComplementar deve cotinuar at� ser implementado a conta POOL
                //O tratamento EXTRA (logo abaixo) vai vai virar o oficial quando tiver o conta POOL
                //Verifica a agencia
                /*
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("BaixaUm B");
                string strConta = "";
                if (Boleto.Conta.BCO == 104)
                {
                    if (Boleto.Conta.CNR.HasValue)
                        strConta = Boleto.Conta.strCNR;
                }
                else
                    strConta = Boleto.Conta.strNumeroConta;

                if ((ARLrow.ARL_CONAgencia != Boleto.Conta.strAgencia) || (ARLrow.ARL_CONConta != strConta))
                {
                    bool resolvido = false;
                    //O tratamento EXTRA para verificar se nao � uma conta do condominio nao pricipal.
                    //Tem uma data de corte para nao trazer lixo do passado
                    if ((ARLrow.ARL_Data > new DateTime(2014, 10, 08)) && (rowCCT != null))
                        if (rowCCT.CON == Boleto.CON)
                            resolvido = true;
                    if (!resolvido)
                    {
                        ARLrow.ARLSTATUS = "Ag�ncia/Conta incorreta";
                        ARLrow.ARLStatusN = (int)ARLStatusN.ContaIncorreta;
                        return false;
                    }
                }*/

                if (Boleto.rowPrincipal.BOLCancelado)
                {
                    if (Boleto.rowPrincipal.BOLTipoCRAI == "E")
                        Boleto.rowPrincipal.BOLCancelado = false;
                    else
                    {
                        ARLrow.ARLSTATUS = "BOLETO CANCELADO";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BoletoCancelado;
                        return false;
                    }
                }

                if (Boleto.rowSBO != null)
                    return true;
                //Verifica o seguro
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("BaixaUm D");
                if (Boleto.rowPrincipal.IsBOLPagamentoNull() && Boleto.ComSeguro())
                {
                    if (Boleto.AjustaSeguro(ARLrow.ARL_Data))
                    {
                        Boleto = new Boletos.Boleto.Boleto(ARLrow.ARL_BOL);
                        ARLrow.ARLValorOriginal = Boleto.rowPrincipal.BOLValorPrevisto;
                    }

                    if (Boleto.rowPrincipal.BOLCancelado)
                    {
                        ARLrow.ARLSTATUS = "BOLETO CANCELADO";
                        ARLrow.ARLStatusN = (int)ARLStatusN.BoletoCancelado;
                        return false;
                    }
                }

                decimal MultaCalculada = 0;
                if (Boleto.rowPrincipal.BOLTipoCRAI == "E")
                {
                    ARLrow.ARLCorrecao = 0;
                    ARLrow.ARLjuros = 0;
                    ARLrow.ARLMulta = 0;
                    ARLrow.ARLTotal = ARLrow.ARL_BOLValorPago;
                    MultaCalculada = 0;
                }
                else
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("BaixaUm F");
                    if ((!Boleto.rowPrincipal.IsBOLDataLimiteJustNull()) && (Boleto.rowPrincipal.BOLDataLimiteJust >= ARLrow.ARL_Data))
                    {
                        ARLrow.ARLCorrecao = 0;
                        ARLrow.ARLjuros = 0;
                        ARLrow.ARLMulta = Boleto.rowPrincipal.BOLValorAcordado - Boleto.rowPrincipal.BOLValorPrevisto;
                        ARLrow.ARLTotal = Boleto.rowPrincipal.BOLValorAcordado;
                        MultaCalculada = Math.Round(Boleto.rowPrincipal.BOLValorAcordado - Boleto.rowPrincipal.BOLValorPrevisto, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        if (!Boleto.Valorpara(ARLrow.ARL_Data))
                        {
                            ARLrow.ARLSTATUS = "Erro no c�lculo da corre��o do boleto";
                            ARLrow.ARLStatusN = (int)ARLStatusN.ErroNoCalculoDoBoleto;
                            return false;
                        };
                        if (Boleto.rowPrincipal.BOLTipoCRAI == "S")
                        {
                            //os boletos S nao devem ser pagos apos o vencimento mas caso isso ocorra e a planilha ainda nao tenha sido gerada ele ser� aceito sem multa
                            ARLrow.ARLCorrecao = Boleto.rowPrincipal.BOLValorPrevisto;
                            ARLrow.ARLjuros = 0;
                            ARLrow.ARLMulta = 0;
                            ARLrow.ARLTotal = Boleto.valorfinal;
                            MultaCalculada = 0;
                        }
                        else
                        {
                            //corrigir o valor pela data
                            //if (Boleto.VencimentoCorrigido < ARLrow.ARL_Data)                        
                            ARLrow.ARLCorrecao = Boleto.valorcorrigido - Boleto.rowPrincipal.BOLValorPrevisto;
                            ARLrow.ARLjuros = Boleto.juros;
                            ARLrow.ARLMulta = Boleto.multa;
                            ARLrow.ARLTotal = Boleto.valorfinal;
                            MultaCalculada = Math.Round(Boleto.valorfinal - Boleto.rowPrincipal.BOLValorPrevisto, 2, MidpointRounding.AwayFromZero);
                        }
                    }
                }

                CompontesBasicos.Performance.Performance.PerformanceST.Registra("BaixaUm G");
                //Verifica se n�o esta em duplicidade
                if (!Boleto.rowPrincipal.IsBOLPagamentoNull())
                {
                    if (!Boleto.rowPrincipal.IsBOL_ARLNull() && (Boleto.rowPrincipal.BOL_ARL == ARLrow.ARL))
                    {
                        //n�o deveria acontecer!!!
                        ARLrow.ARLSTATUS = "Corrigido";
                        ARLrow.ARLStatusN = (int)ARLStatusN.Baixado;
                        return true;
                    };

                    ARLrow.ARLSTATUS = "Duplicado";

                    bool duplicidadeAutomatica = true;
                    if ((DSCentral.EMP == 1) && (Boleto.CON.EstaNoGrupo(370, 466))) //Caso particular do versao e lazur que nao quer que desconte o pagamento em duplicidade
                        duplicidadeAutomatica = false;
                    if (Boleto.ValorPrevisto != 0)
                    {
                        if (Boleto.rowPrincipal.IsBOL_ARLNull()) //boleto n�o foi baixado por francesinha e n�o � zerado
                            duplicidadeAutomatica = false;
                        else
                            if (
                                  (ARLrow.IsARL_CCDNull() || Boleto.rowPrincipal.IsBOL_CCDNull())
                                &&
                                  (FrancesinhaManual(ARLrow.ARL) || (FrancesinhaManual(Boleto.rowPrincipal.BOL_ARL)))
                               )
                        {
                            duplicidadeAutomatica = false; //se um dos retornos nao estiver ligado aos cr�ditos e uma francesinha for manual aguarda at� a concilia��o
                            ARLrow.ARLSTATUS = "Duplicado aguardando conciliar";
                        }
                    }

                    if (!Boleto.APT.HasValue) // boleto gen�rico
                        duplicidadeAutomatica = false;
                    if (ARLrow.ARL_BOLPagamento < new DateTime(2014, 07, 01))
                        duplicidadeAutomatica = false; //data de corte da implanta��o
                    if (Boleto.rowPrincipal.BOLTipoCRAI == "S")
                        duplicidadeAutomatica = false; //Boleto S n�o credita na conta do condom�nio
                    //if(ARLrow.IsARL_CCDNull())
                    //    duplicidadeAutomatica = false; //s� libera quando o cr�dito for conciliado para evitar duplica��o na digita��o

                    //Console.WriteLine(string.Format("ARLrow.IsARL_CCDNull() {0}", ARLrow.IsARL_CCDNull()));
                    //Console.WriteLine(string.Format("Boleto.rowPrincipal.IsBOL_CCDNull() {0}", Boleto.rowPrincipal.IsBOL_CCDNull()));
                    //Console.WriteLine(string.Format("FrancesinhaManual(ARLrow.ARL) {0}",FrancesinhaManual(ARLrow.ARL)));
                    //Console.WriteLine(string.Format("FrancesinhaManual(Boleto.rowPrincipal.BOL_ARL) {0}", FrancesinhaManual(Boleto.rowPrincipal.BOL_ARL)));


                    if (duplicidadeAutomatica)
                    {
                        return RegistraBOLExtra(ARLrow, Boleto, TipoExtra.Duplicidade);
                    }

                    ARLrow.ARLStatusN = (int)ARLStatusN.Duplicidade;
                    return false;
                }






                bool Aprovado = true;
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("BaixaUm H");

                if (!Boleto.rowPrincipal.IsBOLDataLimiteJustNull())
                    if ((Boleto.rowPrincipal.BOLDataLimiteJust >= ARLrow.ARL_BOLPagamento) && (ARLrow.ARL_BOLValorPago >= Boleto.rowPrincipal.BOLValorAcordado))
                        Aprovado = true;

                if (Boleto.rowPrincipal.BOLTipoCRAI == "S")
                {
                    if (ARLrow.ARL_BOLValorPago < Boleto.rowPrincipal.BOLValorPrevisto)
                        Aprovado = false;
                    else
                        if (ARLrow.ARL_BOLValorPago > Boleto.rowPrincipal.BOLValorPrevisto)
                        MultaCalculada += (ARLrow.ARL_BOLValorPago - Boleto.rowPrincipal.BOLValorPrevisto);
                }
                else if (!Boleto.APT.HasValue && (ARLrow.ARL_BOLValorPago < Boleto.rowPrincipal.BOLValorPrevisto))
                    Aprovado = false;

                if (!Aprovado)
                {
                    ARLrow.ARLSTATUS = "Valor incorreto";
                    ARLrow.ARLStatusN = (int)ARLStatusN.ValorIncorreto;
                }
                else
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("BaixaUm I");

                    int? CCD = null;
                    if (!ARLrow.IsARL_CCDNull())
                        CCD = ARLrow.ARL_CCD;
                    //if (Boleto.Baixa(ARLrow.ARL_BOLPagamento, ARLrow.ARL_BOLValorPago, ARLrow.ARL_BOLValorPago - Math.Round(ARLrow.ARLValorOriginal, 2), CCD, ARLrow.ARL))
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Boleto.Baixa", true);
                    if (Boleto.Baixa("Francesinha", ARLrow.ARL_BOLPagamento, ARLrow.ARL_BOLValorPago, MultaCalculada, CCD, ARLrow.ARL))
                    {
                        ARLrow.ARLSTATUS = "Baixado";
                        ARLrow.ARLStatusN = (int)ARLStatusN.Baixado;
                        //ARLrow.ARLStatusN = (ComDiferenca ? 0 : 10);
                    }
                    else
                    {
                        ARLrow.ARLSTATUS = "Erro na baixa: " + Boleto.UltimoErro;
                        ARLrow.ARLStatusN = (int)ARLStatusN.ErronaBaixa;
                        //LOG += "\r\n\r\nErro na baixa\r\n\r\n";
                    }
                    CompontesBasicos.Performance.Performance.PerformanceST.StackPop();


                    /*
                    try
                    {
                        int CCD;
                        if (ARLrow.IsARL_CCDNull())
                            CCD = -1;
                        else
                            CCD = ARLrow.ARL_CCD;
                        if (Boleto.Baixa(ARLrow.ARL_BOLPagamento, ARLrow.ARL_BOLValorPago, ARLrow.ARL_BOLValorPago - Math.Round(ARLrow.ARLValorOriginal,2),CCD,ARLrow.ARL))
                        {
                            ARLrow.ARLSTATUS = "Baixado";
                            ARLrow.ARLStatusN = (int)Framework.ARLStatusN.Baixado;
                            //ARLrow.ARLStatusN = (ComDiferenca ? 0 : 10);
                        }
                        else
                        {
                            ARLrow.ARLSTATUS = "Erro na baixa: " + Boleto.UltimoErro;
                            ARLrow.ARLStatusN = (int)Framework.ARLStatusN.ErronaBaixa;
                            //LOG += "\r\n\r\nErro na baixa\r\n\r\n";
                        }
                        //LOG += DateTime.Now.ToString() + "\r\n" + Boleto.LOG;
                    }
                    catch (Exception e)
                    {
                        //LOG += "Erro:" + e.Message + "\r\n";
                        ARLrow.ARLSTATUS = "Erro Geral";
                    }
                    */


                }
            //};

            return ((ARLStatusN)ARLrow.ARLStatusN == ARLStatusN.Baixado);
        }

        /// <summary>
        /// Carregar
        /// </summary>
        public void Carregar()
        {
            dFrancesinha1.ARquivoLidoTableAdapter.FillLimpo(dFrancesinha1.ARquivoLido);
            dFrancesinha1.ARQuivosTableAdapter.FillLimpo(dFrancesinha1.ARQuivos);
            ARquivoLidoDataTable1 = dFrancesinha1.ARquivoLido;
        }

        /// <summary>
        /// Carrega arquivo CNAB
        /// </summary>
        /// <param name="cnab"></param>
        /// <returns></returns>
        public bool Carregar(dllCNAB.CNAB240 cnab)
        {
            dFrancesinha1.ARquivoLido.Clear();
            Erro = "";

            ARquivoLidoDataTable1 = dFrancesinha1.ARquivoLido;

            //DateTime DataMov;

            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Francesinha - 647",
                                                                    dFrancesinha1.ARQuivosTableAdapter,
                                                                    dFrancesinha1.ARquivoLidoTableAdapter);
            try
            {
                dFrancesinha.ARQuivosRow novalinha = dFrancesinha1.ARQuivos.NewARQuivosRow();
                novalinha.ARQBanco = cnab.Header.Banco;
                novalinha.ARQIData = DateTime.Now;
                novalinha.ARQNome = cnab.Header.LinhaLida;
                novalinha.ARQNomeArquivo = cnab.NomeArquivo;
                novalinha.ARQTipo = (int)ARQTipo.Arquivo;
                dFrancesinha1.ARQuivos.AddARQuivosRow(novalinha);
                dFrancesinha1.ARQuivosTableAdapter.Update(novalinha);
                novalinha.AcceptChanges();
                foreach (dllCNAB.regHeaderLote_1_01 Lote in cnab.LotesCobranca)
                    foreach (dllCNAB.ConjuntoDetalheCobranca conjCob in Lote.regTs)
                    {
                        if (conjCob.reg3T.NossoNumero == 0)
                            continue;
                        dFrancesinha.ARquivoLidoRow rowARL = dFrancesinha1.ARquivoLido.NewARquivoLidoRow();
                        rowARL.ARL_ARQ = novalinha.ARQ;
                        rowARL.ARL_BCO = cnab.Header.Banco;
                        rowARL.ARL_BOL = conjCob.reg3T.NossoNumero;
                        if (conjCob.reg3U.DataCred != DateTime.MinValue)
                            rowARL.ARL_BOLPagamento = conjCob.reg3U.DataCred;
                        rowARL.ARL_CONAgencia = Lote.agencia.ToString("0000");
                        string mascara = "000000";
                        if (cnab.Header.Banco == 341)
                            mascara = "00000";
                        else if (cnab.Header.Banco == 104)
                            mascara = "0000000";

                        rowARL.ARL_CONConta = Lote.conta.ToString(mascara);
                        rowARL.ARL_CONDigitoAgencia = Lote.djagencia;
                        rowARL.ARL_CONDigitoConta = Lote.djconta;
                        rowARL.ARL_Data = conjCob.reg3U.DataPag;
                        rowARL.ARLNossoNumero = conjCob.reg3T.NossoNumero;
                        rowARL.ARLStatusN = (int)ARLStatusN.Nova;
                        rowARL.ARLTar = conjCob.reg3T.Tarifa;
                        rowARL.ARLValor = conjCob.reg3U.VlPago;
                        rowARL.ARL_CONCarteira = conjCob.reg3T.Carteira;
                        rowARL.ARL_IDOcorrencia = conjCob.reg3T.Ocorrencia;
                        rowARL.ARL_MotivoOcorrencia = conjCob.reg3T.MotivoOcorrencia;
                        dFrancesinha1.ARquivoLido.AddARquivoLidoRow(rowARL);
                        dFrancesinha1.ARquivoLidoTableAdapter.Update(rowARL);
                        rowARL.AcceptChanges();
                    }

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return true;
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                Erro = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Carrega Arquivo (n�o cnab) e (n�o bradesco) e (n�o itau)
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <returns></returns>
        public bool Carregar(string Arquivo)
        {
            try
            {
                dFrancesinha1.ARquivoLido.Clear();
                Erro = "";
                if (!File.Exists(Arquivo))
                {
                    Erro = "Arquivo n�o encontrado";
                    return false;
                };
                //string NomeArquivo = System.IO.Path.GetFileName(Arquivo);
                //if (dFrancesinha1.ARQuivosTableAdapter.BuscaARQ(NomeArquivo) != null) {
                //    Erro = "Arquivo j� processado em " + dFrancesinha1.ARQuivosTableAdapter.BuscaARQ(NomeArquivo).ToString();
                //    return false;
                //};

                cTabelaTXT ArqTXT = new cTabelaTXT(dFrancesinha1.ARquivoLido, ModelosTXT.layout);
                ARquivoLidoDataTable1 = dFrancesinha1.ARquivoLido;
                string cab = ArqTXT.LeCabecalho(Arquivo);
                if (cab != "")
                {
                    Tipo = 0;
                    Identificacao = "Arquivo inv�lido";
                    if ((cab.Length >= 400)
                        &&
                        (cab.Substring(0, 19) == "02RETORNO01COBRANCA")
                       )
                    {
                        Tipo = 1;
                        Identificacao = "Cobran�a";
                    };
                    DateTime DataMov;
                    switch (Tipo)
                    {
                        case 1:
                            //Verifica se � um arquivo do BRADESCO
                            Banco = int.Parse(cab.Substring(76, 3));
                            ArqTXT.LimpaMapa();
                            switch (Banco)
                            {
                                case 237:
                                    throw new Exception("Arquivos Bradesco tem um tratamento espec�fico");
                                /*
                                DataMov = new DateTime(2000 + int.Parse(cab.Substring(383, 2)), int.Parse(cab.Substring(381, 2)), int.Parse(cab.Substring(379, 2)));
                                Identificacao += " - BRADESCO";
                                ArqTXT.RegistraMapa("ARL_CONAgencia", 26, 4);
                                ArqTXT.RegistraMapa("ARL_CONDigitoAgencia", 30, 1);
                                ArqTXT.RegistraMapa("ARL_CONConta", 31, 6);
                                ArqTXT.RegistraMapa("ARL_CONDigitoConta", 37, 1);
                                ArqTXT.RegistraMapa("ARL_BOLPagamento", 296, 6);
                                ArqTXT.RegistraMapa("ARLValor", 254, 13);
                                ArqTXT.RegistraMapa("ARL_Data", 111, 6);
                                ArqTXT.RegistraMapa("ARL_BOL", 71, 11);
                                ArqTXT.RegistraMapa("ARLNossoNumero", 71, 11);
                                break;*/
                                case 341:
                                    DataMov = new DateTime(2000 + int.Parse(cab.Substring(98, 2)), int.Parse(cab.Substring(96, 2)), int.Parse(cab.Substring(94, 2)));
                                    Identificacao += " - ITAU";
                                    ArqTXT.RegistraMapa("ARL_CONAgencia", 18, 4);
                                    ArqTXT.RegistraMapa("ARL_CONDigitoAgencia", 22, 1);
                                    ArqTXT.RegistraMapa("ARL_CONConta", 24, 5);
                                    ArqTXT.RegistraMapa("ARL_CONDigitoConta", 29, 1);
                                    ArqTXT.RegistraMapa("ARL_BOLPagamento", 296, 6);
                                    ArqTXT.RegistraMapa("ARLValor", 254, 13);
                                    ArqTXT.RegistraMapa("ARLTar", 176, 13);
                                    ArqTXT.RegistraMapa("ARL_Data", 111, 6);
                                    ArqTXT.RegistraMapa("ARL_BOL", 86, 8);
                                    ArqTXT.RegistraMapa("ARLNossoNumero", 86, 8);
                                    //tarifa 254, 13
                                    break;
                                case 409:
                                    DataMov = new DateTime(int.Parse(cab.Substring(98, 2)), int.Parse(cab.Substring(96, 2)), int.Parse(cab.Substring(94, 2)));
                                    Identificacao += " - UNIBANCO";
                                    ArqTXT.RegistraMapa("ARL_CONAgencia", 18, 4);
                                    //ArqTXT.RegistraMapa("ARL_CONDigitoAgencia", 28, 1);
                                    ArqTXT.RegistraMapa("ARL_CONConta", 22, 6);
                                    ArqTXT.RegistraMapa("ARL_CONDigitoConta", 28, 1);
                                    //A data cr�dito nao est� presente no arquivo usada a data de processamento
                                    //ArqTXT.RegistraMapa("ARL_BOLPagamento", 386, 6);
                                    //ANTIGO ArqTXT.RegistraMapa("ARL_BOLPagamento", 111, 6);
                                    ArqTXT.RegistraMapa("ARL_BOLPagamento", 111, 6);
                                    UnidadeMapa NovaUn = ArqTXT.RegistraMapa("ARL_Data", 293, 6);
                                    NovaUn.Formato = FormatoData.yyMMdd;
                                    ArqTXT.RegistraMapa("ARLValor", 254, 13);
                                    //ArqTXT.RegistraMapa("ARL_Data", 147, 6);
                                    //data de pagamento nao esta vido
                                    ArqTXT.RegistraMapa("ARL_BOL", 63, 14);
                                    ArqTXT.RegistraMapa("ARLNossoNumero", 63, 14);
                                    break;
                            };

                            string sumario = ArqTXT.Carrega("1");

                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Francesinha - 788",
                                                                                   dFrancesinha1.ARQuivosTableAdapter,
                                                                                   dFrancesinha1.ARquivoLidoTableAdapter);
                            try
                            {
                                dFrancesinha.ARQuivosRow novalinha = dFrancesinha1.ARQuivos.NewARQuivosRow();
                                novalinha.ARQBanco = Banco;
                                novalinha.ARQIData = DateTime.Now;
                                novalinha.ARQNome = cab;
                                dFrancesinha1.ARQuivos.AddARQuivosRow(novalinha);
                                dFrancesinha1.ARQuivosTableAdapter.Update(novalinha);
                                novalinha.AcceptChanges();

                                foreach (dFrancesinha.ARquivoLidoRow rowARL in dFrancesinha1.ARquivoLido)
                                {
                                    rowARL.ARLStatusN = (int)ARLStatusN.Nova;
                                    rowARL.ARL_BCO = Banco;
                                    rowARL.ARL_ARQ = novalinha.ARQ;
                                };
                                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                                return Salvar();
                            }
                            catch (Exception e)
                            {
                                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                                return false;
                            }




                        default:
                            return false;
                    };
                }
                else
                {
                    Erro = "Sem conte�do";
                    return false;
                };

            }
            catch (Exception e)
            {
                Exception NovaE = new Exception(string.Format("{0} Erro no arquivo de retorno:{1}\r\nFonte:Francesinha.cs\r\n\r\n", DateTime.Now, Arquivo), e);
                VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(NovaE);
                return false;
            }
        }

        //*** MRC - INICIO (21/03/2016 20:00) ***
        /// <summary>
        /// Retorno Bradesco
        /// </summary>
        /// <param name="BoletoRetornoBra"></param>
        /// <returns></returns>
        public bool Carregar(EDIBoletos.BoletoRetornoBra BoletoRetornoBra)
        {
            dFrancesinha1.ARquivoLido.Clear();
            Erro = "";
            ARquivoLidoDataTable1 = dFrancesinha1.ARquivoLido;
            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Francesinha - 846",
                                                                    dFrancesinha1.ARQuivosTableAdapter,
                                                                    dFrancesinha1.ARquivoLidoTableAdapter);
            try
            {
                dFrancesinha.ARQuivosRow novalinha = dFrancesinha1.ARQuivos.NewARQuivosRow();
                novalinha.ARQBanco = BoletoRetornoBra.Header.NumeroBanco;
                novalinha.ARQIData = DateTime.Now;
                novalinha.ARQNome = BoletoRetornoBra.Header.LinhaLida;
                novalinha.ARQTipo = (int)ARQTipo.Arquivo;
                novalinha.ARQNomeArquivo = BoletoRetornoBra.NomeArquivo;
                dFrancesinha1.ARQuivos.AddARQuivosRow(novalinha);
                dFrancesinha1.ARQuivosTableAdapter.Update(novalinha);
                novalinha.AcceptChanges();

                foreach (EDIBoletos.regTransacao_1_2_BRA bol in BoletoRetornoBra.Boletos)
                {
                    dFrancesinha.ARquivoLidoRow rowARL = dFrancesinha1.ARquivoLido.NewARquivoLidoRow();
                    rowARL.ARL_CONAgencia = bol.Agencia.ToString("0000");
                    rowARL.ARL_CONDigitoAgencia = "0";
                    rowARL.ARL_CONConta = bol.Conta.ToString("000000");
                    rowARL.ARL_CONDigitoConta = bol.DigConta;
                    int BOL = 0;
                    int.TryParse(bol.NossoNumero, out BOL);
                    rowARL.ARL_BOL = BOL;
                    if (bol.DataOcorrencia.Trim() != "" && bol.DataOcorrencia.Trim() != "000000")
                        rowARL.ARL_Data = Convert.ToDateTime(bol.DataOcorrencia.Substring(0, 2) + "/" + bol.DataOcorrencia.Substring(2, 2) + "/" + bol.DataOcorrencia.Substring(4, 2));
                    if (bol.DataCredito.Trim() != "" && bol.DataCredito.Trim() != "000000")
                        rowARL.ARL_BOLPagamento = Convert.ToDateTime(bol.DataCredito.Substring(0, 2) + "/" + bol.DataCredito.Substring(2, 2) + "/" + bol.DataCredito.Substring(4, 2));
                    rowARL.ARLStatusN = (int)ARLStatusN.Nova;
                    rowARL.ARLValor = bol.ValorPago;
                    rowARL.ARLNossoNumero = BOL;// Convert.ToInt32(bol.NossoNumero);
                    rowARL.ARL_CONCarteira = bol.Carteira;
                    rowARL.ARL_IDOcorrencia = bol.IDOcorrencia;
                    rowARL.ARL_MotivoOcorrencia = bol.MotivoOcorrencia;
                    rowARL.ARL_BCO = BoletoRetornoBra.Header.NumeroBanco;
                    rowARL.ARL_ARQ = novalinha.ARQ;
                    dFrancesinha1.ARquivoLido.AddARquivoLidoRow(rowARL);
                    dFrancesinha1.ARquivoLidoTableAdapter.Update(rowARL);
                    rowARL.AcceptChanges();
                }

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return true;
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                Erro = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Retorno Ita�
        /// </summary>
        /// <param name="BoletoRetornoItau"></param>
        /// <returns></returns>
        public bool Carregar(EDIBoletos.BoletoRetornoItau BoletoRetornoItau)
        {
            dFrancesinha1.ARquivoLido.Clear();
            Erro = "";
            ARquivoLidoDataTable1 = dFrancesinha1.ARquivoLido;
            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Francesinha - 846",
                                                                    dFrancesinha1.ARQuivosTableAdapter,
                                                                    dFrancesinha1.ARquivoLidoTableAdapter);
            try
            {
                dFrancesinha.ARQuivosRow novalinha = dFrancesinha1.ARQuivos.NewARQuivosRow();
                novalinha.ARQBanco = BoletoRetornoItau.Header.NumeroBanco;
                novalinha.ARQIData = DateTime.Now;
                novalinha.ARQNome = BoletoRetornoItau.Header.LinhaLida;
                novalinha.ARQNomeArquivo = BoletoRetornoItau.NomeArquivo;
                novalinha.ARQTipo = (int)ARQTipo.Arquivo;
                dFrancesinha1.ARQuivos.AddARQuivosRow(novalinha);
                dFrancesinha1.ARQuivosTableAdapter.Update(novalinha);
                novalinha.AcceptChanges();

                foreach (EDIBoletos.regTransacao_1_2_ITAU bol in BoletoRetornoItau.Boletos)
                {
                    dFrancesinha.ARquivoLidoRow rowARL = dFrancesinha1.ARquivoLido.NewARquivoLidoRow();
                    rowARL.ARL_CONAgencia = bol.Agencia.ToString("0000");
                    rowARL.ARL_CONDigitoAgencia = "0";
                    rowARL.ARL_CONConta = bol.Conta.ToString("000000");
                    rowARL.ARL_CONDigitoConta = bol.DigConta;
                    int BOL = 0;
                    int.TryParse(bol.NossoNumero, out BOL);
                    rowARL.ARL_BOL = BOL;
                    if (bol.DataOcorrencia.Trim() != "" && bol.DataOcorrencia.Trim() != "000000")
                        rowARL.ARL_Data = Convert.ToDateTime(bol.DataOcorrencia.Substring(0, 2) + "/" + bol.DataOcorrencia.Substring(2, 2) + "/" + bol.DataOcorrencia.Substring(4, 2));
                    if (bol.DataCredito.Trim() != "" && bol.DataCredito.Trim() != "000000")
                        rowARL.ARL_BOLPagamento = Convert.ToDateTime(bol.DataCredito.Substring(0, 2) + "/" + bol.DataCredito.Substring(2, 2) + "/" + bol.DataCredito.Substring(4, 2));
                    rowARL.ARLStatusN = (int)ARLStatusN.Nova;
                    //*** MRC - INICIO (01/04/2016 19:00) ***
                    rowARL.ARLTar = bol.Tarifa1;
                    //*** MRC - TERMINO (01/04/2016 19:00) ***
                    rowARL.ARLValor = bol.ValorPago;
                    rowARL.ARLNossoNumero = BOL;// Convert.ToInt32(bol.NossoNumero);
                    rowARL.ARL_CONCarteira = bol.Carteira;
                    rowARL.ARL_IDOcorrencia = bol.IDOcorrencia;
                    rowARL.ARL_MotivoOcorrencia = bol.MotivoOcorrencia;
                    rowARL.ARL_BCO = BoletoRetornoItau.Header.NumeroBanco;
                    rowARL.ARL_ARQ = novalinha.ARQ;
                    dFrancesinha1.ARquivoLido.AddARquivoLidoRow(rowARL);
                    dFrancesinha1.ARquivoLidoTableAdapter.Update(rowARL);
                    rowARL.AcceptChanges();
                }

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return true;
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                Erro = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Liberara mem�ria
        /// </summary>
        public void Dispose()
        {
            if (_dFrancesinha1 != null)
            {
                _dFrancesinha1.Dispose();
                _dFrancesinha1 = null;
            }
            if (ARquivoLidoDataTable1 != null)
            {
                ARquivoLidoDataTable1.Dispose();
                ARquivoLidoDataTable1 = null;
            }
            if (DV_ContaCorrenTe != null)
            {
                DV_ContaCorrenTe.Dispose();
                DV_ContaCorrenTe = null;
            }
            if (LocalVisivel != null)
            {
                LocalVisivel.Dispose();
                LocalVisivel = null;
            }
        }
        /// <summary>
        /// DivideSuperBoleto
        /// </summary>
        public void DivideSuperBoleto()
        {
            string comandoValor =
"SELECT     SBOValorPrevisto\r\n" +
"FROM         SuperBOleto\r\n" +
"WHERE     (SBO = @P1);";
            string comadoGrava =
"INSERT INTO [ARquivoLido]\r\n" +
"([ARL_CONAgencia]\r\n" +
",[ARL_CONDigitoAgencia]\r\n" +
",[ARL_CONConta]\r\n" +
",[ARL_CONDigitoConta]\r\n" +
",[ARL_BOL]\r\n" +
",[ARL_Data]\r\n" +
",[ARL_BOLPagamento]\r\n" +
",[ARLStatusN]\r\n" +
",[ARLTar]\r\n" +
",[ARLValor]\r\n" +
",[ARLNossoNumero]\r\n" +
",[ARL_IDOcorrencia]" +
",[ARL_BCO]\r\n" +
",[ARL_ARQ]\r\n" +
",[ARL_APT])\r\n" +
"SELECT     ARquivoLido.ARL_CONAgencia, ARquivoLido.ARL_CONDigitoAgencia, ARquivoLido.ARL_CONConta, ARquivoLido.ARL_CONDigitoConta, \r\n" +
"                      SuperBoletoDetalhe.SBD_BOL AS ARL_BOL, ARquivoLido.ARL_Data, ARquivoLido.ARL_BOLPagamento, ARquivoLido.ARLStatusN, 0, \r\n" +
"                      SuperBoletoDetalhe.SBDValorPrevisto AS ARLValor, SuperBoletoDetalhe.SBD_BOL AS ARLNossoNumero, ARquivoLido.ARL_IDOcorrencia, ARquivoLido.ARL_BCO, ARquivoLido.ARL_ARQ, \r\n" +
"                      ARquivoLido.ARL_APT\r\n" +
"FROM         ARquivoLido INNER JOIN\r\n" +
"                      SuperBOleto ON ARquivoLido.ARL_BOL = SuperBOleto.SBO INNER JOIN\r\n" +
"                      SuperBoletoDetalhe ON SuperBOleto.SBO = SuperBoletoDetalhe.SBD_SBO\r\n" +
"WHERE     (ARquivoLido.ARL = @P1);";

            string comandoApaga = "delete from ARquivoLido where ARquivoLido.ARL = @P1";

            string comandoValoriconrreto = "update ARquivoLido set ARLStatusN = @P1 where ARquivoLido.ARL = @P2";

            dFrancesinha1.ARquivoLidoTableAdapter.FillBySuper(dFrancesinha1.ARquivoLido);
            foreach (dFrancesinha.ARquivoLidoRow rowSuper in dFrancesinha1.ARquivoLido)
            {
                decimal ValorTotal;
                if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(comandoValor, out ValorTotal, rowSuper.ARL_BOL))
                {

                    if (ValorTotal == rowSuper.ARL_BOLValorPago)
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Francesinha - 108");
                            if (
                                (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comadoGrava, rowSuper.ARL) > 0)
                                &&
                                (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoApaga, rowSuper.ARL) == 1))
                                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                            else
                                VirMSSQL.TableAdapter.STTableAdapter.RollBack("Falha");
                        }
                        catch (Exception e)
                        {
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                        }
                    else
                    {
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Francesinha - 137");
                            if (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoValoriconrreto,
                                                                                         ARLStatusN.SuperBoletoVencido,
                                                                                         rowSuper.ARL) == 1)
                                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                            else
                                VirMSSQL.TableAdapter.STTableAdapter.RollBack("Falha");
                        }
                        catch (Exception e)
                        {
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Limpa
        /// </summary>
        public void Limpa()
        {
            System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
            foreach (dFrancesinha.ARquivoLidoRow rowARL in dFrancesinha1.ARquivoLido)
                if ((rowARL.ARLStatusN == 0) || (rowARL.ARLStatusN == 1) || (rowARL.ARLStatusN == 12) || (rowARL.ARLStatusN == 13))
                    Apagar.Add(rowARL);
            foreach (dFrancesinha.ARquivoLidoRow rowARL in Apagar)
                rowARL.Delete();
            dFrancesinha1.ARquivoLido.AcceptChanges();
        }

        /// <summary>
        /// Processar
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <returns></returns>
        public bool Processar(string Arquivo)
        {
            if (Carregar(Arquivo))
                return (ProcessarDados() == "ok");
            else
                return false;
        }

        /// <summary>
        /// Processa todos os registros parados
        /// </summary>
        /// <param name="Detalhamento">Detalhamento do retorno</param>
        /// <param name="BOL">Boleto</param>
        /// <returns></returns>
        public string ProcessarDados(TipoRetorno Detalhamento = TipoRetorno.ok_Falha, int? BOL = null)
        {
            DetalhamentoRetorno = Detalhamento;
            return Baixas(BOL);
        }


        /// <summary>
        /// Salvar
        /// </summary>
        /// <returns></returns>
        public bool Salvar()
        {
            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Francesinha - 130", dFrancesinha1.ARquivoLidoTableAdapter);
            try
            {
                dFrancesinha1.ARquivoLidoTableAdapter.Update(dFrancesinha1.ARquivoLido);
                dFrancesinha1.ARquivoLido.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
                return true;
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                return false;
            }
        }

        /// <summary>
        /// dFrancesinha
        /// </summary>
        public dFrancesinha dFrancesinha1
        {
            get
            {
                if (_dFrancesinha1 == null)
                {
                    _dFrancesinha1 = new dFrancesinha();                    
                }
                return _dFrancesinha1;
            }
        }

        /// <summary>
        /// dFrancesinha
        /// </summary>
        public dFrancesinha dFrancesinha1F
        {
            get
            {
                if (_dFrancesinha1F == null)
                {
                    _dFrancesinha1F = new dFrancesinha(new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Filial,DSCentral.USU));                    
                }
                return _dFrancesinha1F;
            }
        }

        internal enum TipoExtra { Duplicidade, Cancelado, NaoEncontrado }

        private enum retornoRetornoRegistro { continuar, parar }

        //private string GerarErro;

        /// <summary>
        /// Indica o deltalhamento do string de retorno
        /// </summary>
        public enum TipoRetorno
        {
            /// <summary>
            /// Retorna apenas ok ou falha
            /// </summary>
            ok_Falha,
            /// <summary>
            /// Total por status
            /// </summary>
            resumo,
            /// <summary>
            /// Detalha somente os errados
            /// </summary>
            DetalharErro,
            /// <summary>
            /// Detalha os pendentes e os erros
            /// </summary>
            DetalhaPendentes_erro,
            /// <summary>
            /// Detalha todos 
            /// </summary>
            Log
        }
        //*** MRC - TERMINO (21/03/2016 20:00) ***
    }
}
