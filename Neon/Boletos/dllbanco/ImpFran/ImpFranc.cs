using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllbanco.ImpFran
{
    /// <summary>
    /// Impresso da francesinha
    /// </summary>
    public partial class ImpFranc : dllImpresso.ImpLogoCond
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ImpFranc()
        {
            InitializeComponent();
            RemoverLogo();
        }

        private void xrLabel7_Draw(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.Degrade(sender, e);
            //Degrade(sender, e);
        }

        private void xrLabel3_Draw(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.Degrade(sender, e);
            //Degrade(sender, e);
        }
    }
}

