﻿namespace dllbanco.ImpFran {


    partial class dImpFranc
    {
        private dImpFrancTableAdapters.ResumoDataTableAdapter resumoDataTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ResumoData
        /// </summary>
        public dImpFrancTableAdapters.ResumoDataTableAdapter ResumoDataTableAdapter
        {
            get
            {
                if (resumoDataTableAdapter == null)
                {
                    resumoDataTableAdapter = new dImpFrancTableAdapters.ResumoDataTableAdapter();
                    resumoDataTableAdapter.TrocarStringDeConexao();
                };
                return resumoDataTableAdapter;
            }
        }

        private dImpFrancTableAdapters.FrancTableAdapter francTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Franc
        /// </summary>
        public dImpFrancTableAdapters.FrancTableAdapter FrancTableAdapter
        {
            get
            {
                if (francTableAdapter == null)
                {
                    francTableAdapter = new dImpFrancTableAdapters.FrancTableAdapter();
                    francTableAdapter.TrocarStringDeConexao();
                };
                return francTableAdapter;
            }
        }

        private void CorrecaoDataFran(int CON, System.DateTime DataAntiga, System.DateTime NovaData,int CCD)
        {
            try 
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco dImpFranc - 44",ResumoDataTableAdapter);
                ResumoDataTableAdapter.AjustaData(NovaData, CON, DataAntiga);
                ResumoDataTableAdapter.AjustaData1(NovaData, CON, DataAntiga);
                dllbanco.Conciliacao.ConciliacaoSt.Consolidar(CCD);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (System.Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="DataI"></param>
        /// <param name="DataF"></param>
        /// <returns></returns>
        public string Verifica(int CON,System.DateTime DataI,System.DateTime DataF) {
            string retorno = "";
            ResumoDataTableAdapter.Fill(ResumoData, CON, DataI, DataF);
            foreach (ResumoDataRow Row in ResumoData) 
            {
                if (ResumoDataTableAdapter.Verifica(Row.ARL_BOLPagamento, CON, Row.Total) == null)
                {
                    System.DateTime DataTeste = Row.ARL_BOLPagamento;
                    bool corrigido = false;
                    while (!Framework.datasets.dFERiados.IsUtil(DataTeste,Framework.datasets.dFERiados.Sabados.naoUteis,Framework.datasets.dFERiados.Feriados.naoUteis))
                    {
                        DataTeste = DataTeste.AddDays(1);
                        decimal? ValorAberto = ResumoDataTableAdapter.ValorFranAberto(CON, Row.ARL_BOLPagamento, DataTeste);
                        if (ValorAberto.HasValue)
                        {
                            object oCCD = ResumoDataTableAdapter.VerificaLivre(DataTeste, CON, ValorAberto.Value);
                            if (oCCD != null)
                            {
                                CorrecaoDataFran(CON, Row.ARL_BOLPagamento, DataTeste, (int)oCCD);
                                corrigido = true;
                                break;
                            }
                        }
                    }
                    if (!corrigido)
                        retorno += Row.ARL_BOLPagamento.ToString() + "\r\n";
                }
            }
            return retorno;
        }

       

    }
}
