﻿using FrameworkProc;

namespace dllbanco 
{
    /// <summary>
    /// dTributoEletronico
    /// </summary>
    public partial class dTributoEletronico 
    {
        /// <summary>
        /// 
        /// </summary>
        private EMPTProc _EMPTipo;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTipo
        {
            get
            {
                if (_EMPTipo == null)
                    _EMPTipo = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTipo;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="EMPTipo"></param>
        public dTributoEletronico(EMPTProc EMPTipo):this()
        {
            _EMPTipo = EMPTipo;
        }

        private dTributoEletronicoTableAdapters.ARQuivosTableAdapter aRQuivosTableAdapter;

        /// <summary>
        /// ARQuivosTableAdapter
        /// </summary>
        public dTributoEletronicoTableAdapters.ARQuivosTableAdapter ARQuivosTableAdapter
        {
            get
            {
                if (aRQuivosTableAdapter == null)
                {
                    aRQuivosTableAdapter = new dllbanco.dTributoEletronicoTableAdapters.ARQuivosTableAdapter();
                    aRQuivosTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return aRQuivosTableAdapter;
            }

        }

        private dTributoEletronicoTableAdapters.ARquivoLidoTableAdapter aRquivoLidoTableAdapter;

        /// <summary>
        /// ARquivoLidoTableAdapter
        /// </summary>
        public dTributoEletronicoTableAdapters.ARquivoLidoTableAdapter ARquivoLidoTableAdapter
        {
            get
            {
                if (aRquivoLidoTableAdapter == null)
                {
                    aRquivoLidoTableAdapter = new dllbanco.dTributoEletronicoTableAdapters.ARquivoLidoTableAdapter();
                    aRquivoLidoTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);

                };
                return aRquivoLidoTableAdapter;
            }

        }
    }
}
