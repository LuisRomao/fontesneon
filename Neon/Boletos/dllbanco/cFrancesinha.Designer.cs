namespace dllbanco
{
    partial class cFrancesinha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cFrancesinha));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.aRquivoLidoDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONDOMINIOSDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARLStatusN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BandBanco = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARL_BCO = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BandAg = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARL_CONAgencia = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARL_CONDigitoAgencia = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BandConta = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARL_CONConta = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARL_CONDigitoConta = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARLNossoNumero = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARL_BOL = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARL_Data = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARL_BOLPagamento = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARLValor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARLTar = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARL_BOLValorPago = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARL_IDOcorrencia = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARL_MotivoOcorrencia = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BandBoleto = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARLValorOriginal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARLCorrecao = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARLjuros = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARLMulta = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARLTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARL_BOLVencto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BandEncontrado = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colARLCON = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colARL_APT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.LookUpAPT = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.chavesAPTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colARLcErro = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARLSTATUS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colARL_ARQ = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.BotTeste2 = new DevExpress.XtraEditors.SimpleButton();
            this.BotTeste = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.panelB = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRquivoLidoDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpAPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chavesAPTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelB)).BeginInit();
            this.panelB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.Enabled = false;
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.Enabled = false;
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.aRquivoLidoDataTableBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Location = new System.Drawing.Point(0, 93);
            this.GridControl_F.MainView = this.bandedGridView1;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.LookUpAPT,
            this.repositoryItemImageComboBox1});
            this.GridControl_F.Size = new System.Drawing.Size(1477, 654);
            this.GridControl_F.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // GridView_F
            // 
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowFooter = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.InitialDirectory = "c:\\";
            // 
            // aRquivoLidoDataTableBindingSource
            // 
            this.aRquivoLidoDataTableBindingSource.DataSource = typeof(dllbanco.dFrancesinha.ARquivoLidoDataTable);
            // 
            // cONDOMINIOSDataTableBindingSource
            // 
            this.cONDOMINIOSDataTableBindingSource.DataSource = typeof(Framework.Buscas.dBuscas.CONDOMINIOSDataTable);
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand2,
            this.BandBoleto,
            this.BandEncontrado});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colARL_CONAgencia,
            this.colARL_CONDigitoAgencia,
            this.colARL_CONConta,
            this.colARL_CONDigitoConta,
            this.colARL_BOL,
            this.colARL_Data,
            this.colARL_BOLValorPago,
            this.colARL_BOLPagamento,
            this.colARLSTATUS,
            this.colARLTar,
            this.colARLValor,
            this.colARLCON,
            this.colARLNossoNumero,
            this.colARLValorOriginal,
            this.colARLCorrecao,
            this.colARLTotal,
            this.colARL_BOLVencto,
            this.colARLjuros,
            this.colARLMulta,
            this.colARLcErro,
            this.colARL_BCO,
            this.colARL_APT,
            this.colARL_ARQ,
            this.colARL_IDOcorrencia,
            this.colARL_MotivoOcorrencia,
            this.colARLStatusN});
            this.bandedGridView1.GridControl = this.GridControl_F;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.bandedGridView1.OptionsLayout.Columns.StoreAppearance = true;
            this.bandedGridView1.OptionsLayout.StoreAllOptions = true;
            this.bandedGridView1.OptionsLayout.StoreAppearance = true;
            this.bandedGridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.bandedGridView1_RowStyle);
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "Status";
            this.gridBand4.Columns.Add(this.colARLStatusN);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 150;
            // 
            // colARLStatusN
            // 
            this.colARLStatusN.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colARLStatusN.FieldName = "ARLStatusN";
            this.colARLStatusN.Name = "colARLStatusN";
            this.colARLStatusN.OptionsColumn.ReadOnly = true;
            this.colARLStatusN.Visible = true;
            this.colARLStatusN.Width = 150;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Arquivo";
            this.gridBand2.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.BandBanco,
            this.BandAg,
            this.BandConta,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand5});
            this.gridBand2.ImageOptions.Alignment = System.Drawing.StringAlignment.Center;
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 633;
            // 
            // BandBanco
            // 
            this.BandBanco.Caption = "Banco";
            this.BandBanco.Columns.Add(this.colARL_BCO);
            this.BandBanco.Name = "BandBanco";
            this.BandBanco.VisibleIndex = 0;
            this.BandBanco.Width = 51;
            // 
            // colARL_BCO
            // 
            this.colARL_BCO.Caption = "Banco";
            this.colARL_BCO.FieldName = "ARL_BCO";
            this.colARL_BCO.Name = "colARL_BCO";
            this.colARL_BCO.OptionsColumn.ReadOnly = true;
            this.colARL_BCO.Visible = true;
            this.colARL_BCO.Width = 51;
            // 
            // BandAg
            // 
            this.BandAg.Caption = "Ag�ncia";
            this.BandAg.Columns.Add(this.colARL_CONAgencia);
            this.BandAg.Columns.Add(this.colARL_CONDigitoAgencia);
            this.BandAg.Name = "BandAg";
            this.BandAg.VisibleIndex = 1;
            this.BandAg.Width = 53;
            // 
            // colARL_CONAgencia
            // 
            this.colARL_CONAgencia.Caption = "Ag";
            this.colARL_CONAgencia.FieldName = "ARL_CONAgencia";
            this.colARL_CONAgencia.Name = "colARL_CONAgencia";
            this.colARL_CONAgencia.OptionsColumn.ReadOnly = true;
            this.colARL_CONAgencia.Visible = true;
            this.colARL_CONAgencia.Width = 33;
            // 
            // colARL_CONDigitoAgencia
            // 
            this.colARL_CONDigitoAgencia.FieldName = "ARL_CONDigitoAgencia";
            this.colARL_CONDigitoAgencia.Name = "colARL_CONDigitoAgencia";
            this.colARL_CONDigitoAgencia.OptionsColumn.ReadOnly = true;
            this.colARL_CONDigitoAgencia.Visible = true;
            this.colARL_CONDigitoAgencia.Width = 20;
            // 
            // BandConta
            // 
            this.BandConta.Caption = "Conta";
            this.BandConta.Columns.Add(this.colARL_CONConta);
            this.BandConta.Columns.Add(this.colARL_CONDigitoConta);
            this.BandConta.Name = "BandConta";
            this.BandConta.VisibleIndex = 2;
            // 
            // colARL_CONConta
            // 
            this.colARL_CONConta.Caption = "Conta";
            this.colARL_CONConta.FieldName = "ARL_CONConta";
            this.colARL_CONConta.Name = "colARL_CONConta";
            this.colARL_CONConta.OptionsColumn.ReadOnly = true;
            this.colARL_CONConta.Visible = true;
            this.colARL_CONConta.Width = 50;
            // 
            // colARL_CONDigitoConta
            // 
            this.colARL_CONDigitoConta.FieldName = "ARL_CONDigitoConta";
            this.colARL_CONDigitoConta.Name = "colARL_CONDigitoConta";
            this.colARL_CONDigitoConta.OptionsColumn.ReadOnly = true;
            this.colARL_CONDigitoConta.Visible = true;
            this.colARL_CONDigitoConta.Width = 20;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "N�mero";
            this.gridBand6.Columns.Add(this.colARLNossoNumero);
            this.gridBand6.Columns.Add(this.colARL_BOL);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 3;
            this.gridBand6.Width = 148;
            // 
            // colARLNossoNumero
            // 
            this.colARLNossoNumero.Caption = "N�mero";
            this.colARLNossoNumero.FieldName = "ARLNossoNumero";
            this.colARLNossoNumero.Name = "colARLNossoNumero";
            this.colARLNossoNumero.OptionsColumn.ReadOnly = true;
            this.colARLNossoNumero.Visible = true;
            this.colARLNossoNumero.Width = 59;
            // 
            // colARL_BOL
            // 
            this.colARL_BOL.Caption = "N�mero Neon";
            this.colARL_BOL.FieldName = "ARL_BOL";
            this.colARL_BOL.Name = "colARL_BOL";
            this.colARL_BOL.OptionsColumn.ReadOnly = true;
            this.colARL_BOL.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colARL_BOL.Visible = true;
            this.colARL_BOL.Width = 89;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "Data";
            this.gridBand7.Columns.Add(this.colARL_Data);
            this.gridBand7.Columns.Add(this.colARL_BOLPagamento);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 4;
            this.gridBand7.Width = 138;
            // 
            // colARL_Data
            // 
            this.colARL_Data.Caption = "Pagamento";
            this.colARL_Data.FieldName = "ARL_Data";
            this.colARL_Data.Name = "colARL_Data";
            this.colARL_Data.OptionsColumn.ReadOnly = true;
            this.colARL_Data.Visible = true;
            this.colARL_Data.Width = 76;
            // 
            // colARL_BOLPagamento
            // 
            this.colARL_BOLPagamento.Caption = "Cr�dito";
            this.colARL_BOLPagamento.FieldName = "ARL_BOLPagamento";
            this.colARL_BOLPagamento.Name = "colARL_BOLPagamento";
            this.colARL_BOLPagamento.OptionsColumn.ReadOnly = true;
            this.colARL_BOLPagamento.Visible = true;
            this.colARL_BOLPagamento.Width = 62;
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "Valor";
            this.gridBand8.Columns.Add(this.colARLValor);
            this.gridBand8.Columns.Add(this.colARLTar);
            this.gridBand8.Columns.Add(this.colARL_BOLValorPago);
            this.gridBand8.MinWidth = 20;
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 5;
            this.gridBand8.Width = 173;
            // 
            // colARLValor
            // 
            this.colARLValor.Caption = "Valor";
            this.colARLValor.DisplayFormat.FormatString = "N2";
            this.colARLValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLValor.FieldName = "ARLValor";
            this.colARLValor.Name = "colARLValor";
            this.colARLValor.OptionsColumn.ReadOnly = true;
            this.colARLValor.Visible = true;
            this.colARLValor.Width = 49;
            // 
            // colARLTar
            // 
            this.colARLTar.Caption = "Tarifa";
            this.colARLTar.DisplayFormat.FormatString = "N2";
            this.colARLTar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLTar.FieldName = "ARLTar";
            this.colARLTar.Name = "colARLTar";
            this.colARLTar.OptionsColumn.ReadOnly = true;
            this.colARLTar.Visible = true;
            this.colARLTar.Width = 50;
            // 
            // colARL_BOLValorPago
            // 
            this.colARL_BOLValorPago.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colARL_BOLValorPago.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colARL_BOLValorPago.AppearanceCell.Options.UseBackColor = true;
            this.colARL_BOLValorPago.AppearanceCell.Options.UseForeColor = true;
            this.colARL_BOLValorPago.Caption = "Valor Pago";
            this.colARL_BOLValorPago.DisplayFormat.FormatString = "N2";
            this.colARL_BOLValorPago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARL_BOLValorPago.FieldName = "ARL_BOLValorPago";
            this.colARL_BOLValorPago.Name = "colARL_BOLValorPago";
            this.colARL_BOLValorPago.OptionsColumn.ReadOnly = true;
            this.colARL_BOLValorPago.Visible = true;
            this.colARL_BOLValorPago.Width = 74;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "C�digos";
            this.gridBand5.Columns.Add(this.colARL_IDOcorrencia);
            this.gridBand5.Columns.Add(this.colARL_MotivoOcorrencia);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Visible = false;
            this.gridBand5.VisibleIndex = -1;
            this.gridBand5.Width = 150;
            // 
            // colARL_IDOcorrencia
            // 
            this.colARL_IDOcorrencia.Caption = "C�digo Ocorr�ncia";
            this.colARL_IDOcorrencia.FieldName = "ARL_IDOcorrencia";
            this.colARL_IDOcorrencia.Name = "colARL_IDOcorrencia";
            this.colARL_IDOcorrencia.OptionsColumn.ReadOnly = true;
            this.colARL_IDOcorrencia.Visible = true;
            // 
            // colARL_MotivoOcorrencia
            // 
            this.colARL_MotivoOcorrencia.Caption = "Motivo";
            this.colARL_MotivoOcorrencia.FieldName = "ARL_MotivoOcorrencia";
            this.colARL_MotivoOcorrencia.Name = "colARL_MotivoOcorrencia";
            this.colARL_MotivoOcorrencia.OptionsColumn.ReadOnly = true;
            this.colARL_MotivoOcorrencia.Visible = true;
            // 
            // BandBoleto
            // 
            this.BandBoleto.Caption = "Dados do Boleto";
            this.BandBoleto.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand9,
            this.gridBand10});
            this.BandBoleto.MinWidth = 20;
            this.BandBoleto.Name = "BandBoleto";
            this.BandBoleto.VisibleIndex = 2;
            this.BandBoleto.Width = 348;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "Valor Esperado";
            this.gridBand9.Columns.Add(this.colARLValorOriginal);
            this.gridBand9.Columns.Add(this.colARLCorrecao);
            this.gridBand9.Columns.Add(this.colARLjuros);
            this.gridBand9.Columns.Add(this.colARLMulta);
            this.gridBand9.Columns.Add(this.colARLTotal);
            this.gridBand9.MinWidth = 20;
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 0;
            this.gridBand9.Width = 293;
            // 
            // colARLValorOriginal
            // 
            this.colARLValorOriginal.Caption = "Valor Original";
            this.colARLValorOriginal.DisplayFormat.FormatString = "N2";
            this.colARLValorOriginal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLValorOriginal.FieldName = "ARLValorOriginal";
            this.colARLValorOriginal.Name = "colARLValorOriginal";
            this.colARLValorOriginal.OptionsColumn.ReadOnly = true;
            this.colARLValorOriginal.Visible = true;
            this.colARLValorOriginal.Width = 57;
            // 
            // colARLCorrecao
            // 
            this.colARLCorrecao.Caption = "Corre��o";
            this.colARLCorrecao.DisplayFormat.FormatString = "N2";
            this.colARLCorrecao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLCorrecao.FieldName = "ARLCorrecao";
            this.colARLCorrecao.Name = "colARLCorrecao";
            this.colARLCorrecao.OptionsColumn.ReadOnly = true;
            this.colARLCorrecao.Visible = true;
            this.colARLCorrecao.Width = 48;
            // 
            // colARLjuros
            // 
            this.colARLjuros.Caption = "Juros";
            this.colARLjuros.DisplayFormat.FormatString = "N2";
            this.colARLjuros.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLjuros.FieldName = "ARLjuros";
            this.colARLjuros.Name = "colARLjuros";
            this.colARLjuros.OptionsColumn.ReadOnly = true;
            this.colARLjuros.Visible = true;
            this.colARLjuros.Width = 54;
            // 
            // colARLMulta
            // 
            this.colARLMulta.Caption = "Multa";
            this.colARLMulta.DisplayFormat.FormatString = "n2";
            this.colARLMulta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLMulta.FieldName = "ARLMulta";
            this.colARLMulta.Name = "colARLMulta";
            this.colARLMulta.OptionsColumn.ReadOnly = true;
            this.colARLMulta.Visible = true;
            this.colARLMulta.Width = 54;
            // 
            // colARLTotal
            // 
            this.colARLTotal.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colARLTotal.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colARLTotal.AppearanceCell.Options.UseBackColor = true;
            this.colARLTotal.AppearanceCell.Options.UseForeColor = true;
            this.colARLTotal.Caption = "Total";
            this.colARLTotal.DisplayFormat.FormatString = "N2";
            this.colARLTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLTotal.FieldName = "ARLTotal";
            this.colARLTotal.Name = "colARLTotal";
            this.colARLTotal.OptionsColumn.ReadOnly = true;
            this.colARLTotal.Visible = true;
            this.colARLTotal.Width = 80;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "Data";
            this.gridBand10.Columns.Add(this.colARL_BOLVencto);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 1;
            this.gridBand10.Width = 55;
            // 
            // colARL_BOLVencto
            // 
            this.colARL_BOLVencto.Caption = "Vencimento";
            this.colARL_BOLVencto.FieldName = "ARL_BOLVencto";
            this.colARL_BOLVencto.Name = "colARL_BOLVencto";
            this.colARL_BOLVencto.OptionsColumn.ReadOnly = true;
            this.colARL_BOLVencto.Visible = true;
            this.colARL_BOLVencto.Width = 55;
            // 
            // BandEncontrado
            // 
            this.BandEncontrado.Caption = "Encontrado";
            this.BandEncontrado.Columns.Add(this.colARLCON);
            this.BandEncontrado.Columns.Add(this.colARL_APT);
            this.BandEncontrado.Columns.Add(this.colARLcErro);
            this.BandEncontrado.Columns.Add(this.colARLSTATUS);
            this.BandEncontrado.MinWidth = 20;
            this.BandEncontrado.Name = "BandEncontrado";
            this.BandEncontrado.VisibleIndex = 3;
            this.BandEncontrado.Width = 391;
            // 
            // colARLCON
            // 
            this.colARLCON.Caption = "CODCON";
            this.colARLCON.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colARLCON.FieldName = "ARL_CON";
            this.colARLCON.Name = "colARLCON";
            this.colARLCON.OptionsColumn.ReadOnly = true;
            this.colARLCON.Visible = true;
            this.colARLCON.Width = 59;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.DataSource = this.cONDOMINIOSDataTableBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "CONCodigo";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "???";
            this.repositoryItemLookUpEdit1.ValueMember = "CON";
            // 
            // colARL_APT
            // 
            this.colARL_APT.Caption = "APT";
            this.colARL_APT.ColumnEdit = this.LookUpAPT;
            this.colARL_APT.FieldName = "ARL_APT";
            this.colARL_APT.Name = "colARL_APT";
            this.colARL_APT.OptionsColumn.ReadOnly = true;
            this.colARL_APT.Visible = true;
            this.colARL_APT.Width = 56;
            // 
            // LookUpAPT
            // 
            this.LookUpAPT.AutoHeight = false;
            this.LookUpAPT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpAPT.DataSource = this.chavesAPTBindingSource;
            this.LookUpAPT.DisplayMember = "BlocoApt";
            this.LookUpAPT.Name = "LookUpAPT";
            this.LookUpAPT.NullText = "???";
            this.LookUpAPT.ValueMember = "APT";
            // 
            // chavesAPTBindingSource
            // 
            this.chavesAPTBindingSource.DataMember = "ChavesAPT";
            this.chavesAPTBindingSource.DataSource = typeof(Framework.Buscas.dBuscas);
            // 
            // colARLcErro
            // 
            this.colARLcErro.AppearanceCell.BackColor = System.Drawing.Color.LightCoral;
            this.colARLcErro.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colARLcErro.AppearanceCell.Options.UseBackColor = true;
            this.colARLcErro.AppearanceCell.Options.UseForeColor = true;
            this.colARLcErro.Caption = "Erro";
            this.colARLcErro.DisplayFormat.FormatString = "n2";
            this.colARLcErro.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colARLcErro.FieldName = "ARLcErro";
            this.colARLcErro.Name = "colARLcErro";
            this.colARLcErro.OptionsColumn.ReadOnly = true;
            this.colARLcErro.Visible = true;
            this.colARLcErro.Width = 47;
            // 
            // colARLSTATUS
            // 
            this.colARLSTATUS.Caption = "Status";
            this.colARLSTATUS.FieldName = "ARLSTATUS";
            this.colARLSTATUS.Name = "colARLSTATUS";
            this.colARLSTATUS.OptionsColumn.ReadOnly = true;
            this.colARLSTATUS.Visible = true;
            this.colARLSTATUS.Width = 229;
            // 
            // colARL_ARQ
            // 
            this.colARL_ARQ.Caption = "ARQ";
            this.colARL_ARQ.FieldName = "ARL_ARQ";
            this.colARL_ARQ.Name = "colARL_ARQ";
            this.colARL_ARQ.OptionsColumn.ReadOnly = true;
            this.colARL_ARQ.Width = 69;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.BotTeste2);
            this.panelControl1.Controls.Add(this.BotTeste);
            this.panelControl1.Controls.Add(this.simpleButton8);
            this.panelControl1.Controls.Add(this.panelB);
            this.panelControl1.Controls.Add(this.radioGroup1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1477, 93);
            this.panelControl1.TabIndex = 5;
            // 
            // BotTeste2
            // 
            this.BotTeste2.Location = new System.Drawing.Point(1147, 34);
            this.BotTeste2.Name = "BotTeste2";
            this.BotTeste2.Size = new System.Drawing.Size(113, 23);
            this.BotTeste2.TabIndex = 12;
            this.BotTeste2.Text = "Recarregar";
            this.BotTeste2.Visible = false;
            this.BotTeste2.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // BotTeste
            // 
            this.BotTeste.Location = new System.Drawing.Point(1147, 5);
            this.BotTeste.Name = "BotTeste";
            this.BotTeste.Size = new System.Drawing.Size(113, 23);
            this.BotTeste.TabIndex = 11;
            this.BotTeste.Text = "Baixa 1";
            this.BotTeste.Visible = false;
            this.BotTeste.Click += new System.EventHandler(this.BotTeste_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(5, 63);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(113, 23);
            this.simpleButton8.TabIndex = 10;
            this.simpleButton8.Text = "Francesinha Manual";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // panelB
            // 
            this.panelB.Controls.Add(this.simpleButton6);
            this.panelB.Controls.Add(this.simpleButton5);
            this.panelB.Controls.Add(this.simpleButton7);
            this.panelB.Controls.Add(this.simpleButton3);
            this.panelB.Controls.Add(this.simpleButton4);
            this.panelB.Enabled = false;
            this.panelB.Location = new System.Drawing.Point(273, 3);
            this.panelB.Name = "panelB";
            this.panelB.Size = new System.Drawing.Size(868, 73);
            this.panelB.TabIndex = 9;
            // 
            // simpleButton6
            // 
            this.simpleButton6.ImageOptions.Image = global::dllbanco.Properties.Resources.btnCancelar_F_Glyph;
            this.simpleButton6.Location = new System.Drawing.Point(5, 5);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(125, 23);
            this.simpleButton6.TabIndex = 7;
            this.simpleButton6.Text = "Remover";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Enabled = false;
            this.simpleButton5.ImageOptions.Image = global::dllbanco.Properties.Resources.novobol;
            this.simpleButton5.Location = new System.Drawing.Point(336, 31);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(167, 42);
            this.simpleButton5.TabIndex = 6;
            this.simpleButton5.Text = "Gerar novo boleto";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.ImageOptions.Image = global::dllbanco.Properties.Resources.Boleto1;
            this.simpleButton7.Location = new System.Drawing.Point(5, 31);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(125, 42);
            this.simpleButton7.TabIndex = 8;
            this.simpleButton7.Text = "Ver Boleto";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(136, 31);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(194, 42);
            this.simpleButton3.TabIndex = 4;
            this.simpleButton3.Text = "Liberar Cr�dito";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(136, 5);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(194, 23);
            this.simpleButton4.TabIndex = 5;
            this.simpleButton4.Text = "Cancelar diferen�a";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // radioGroup1
            // 
            this.radioGroup1.EditValue = 1;
            this.radioGroup1.Location = new System.Drawing.Point(124, 12);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Completo"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Dados Do Arquivo"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Anal�tico")});
            this.radioGroup1.Size = new System.Drawing.Size(124, 57);
            this.radioGroup1.TabIndex = 3;
            this.radioGroup1.EditValueChanged += new System.EventHandler(this.radioGroup1_EditValueChanged);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(5, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(113, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Carregar";
            this.simpleButton2.Visible = false;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 34);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(113, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Baixa";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Arquivo";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = -1;
            this.gridBand1.Width = 75;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Arquivo";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = -1;
            this.gridBand3.Width = 75;
            // 
            // cFrancesinha
            // 
            this.BindingSourcePrincipal = this.aRquivoLidoDataTableBindingSource;
            this.Controls.Add(this.panelControl1);
            this.dataLayout = new System.DateTime(2017, 3, 29, 14, 39, 41, 0);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cFrancesinha";
            this.Size = new System.Drawing.Size(1477, 807);
            this.cargaFinal += new System.EventHandler(this.cFrancesinha_cargaFinal);
            this.Load += new System.EventHandler(this.cFrancesinha_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRquivoLidoDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpAPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chavesAPTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelB)).EndInit();
            this.panelB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.BindingSource aRquivoLidoDataTableBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_CONAgencia;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_CONDigitoAgencia;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_CONConta;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_CONDigitoConta;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLNossoNumero;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_BOL;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_Data;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_BOLPagamento;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLValor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLTar;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_BOLValorPago;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLValorOriginal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLCorrecao;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLjuros;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLMulta;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_BOLVencto;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLCON;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLcErro;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLSTATUS;
        private System.Windows.Forms.BindingSource cONDOMINIOSDataTableBindingSource;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_BCO;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpAPT;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.BindingSource chavesAPTBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_APT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_ARQ;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.PanelControl panelB;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_IDOcorrencia;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARL_MotivoOcorrencia;
        private DevExpress.XtraEditors.SimpleButton BotTeste;
        private DevExpress.XtraEditors.SimpleButton BotTeste2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colARLStatusN;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandBanco;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandAg;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandConta;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandBoleto;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandEncontrado;
    }
}
