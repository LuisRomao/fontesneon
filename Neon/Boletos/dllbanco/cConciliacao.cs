using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Framework;
using VirEnumeracoesNeon;

namespace dllbanco
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cConciliacao : CompontesBasicos.ComponenteBaseBindingSource
    {
        /// <summary>
        /// �ltimo erro
        /// </summary>
        public string UltimoErro = "";

        /// <summary>
        /// Construtor
        /// </summary>
        public cConciliacao()
        {
            InitializeComponent();
            Enumeracoes.virEnumstatusconsiliado.virEnumstatusconsiliadoSt.CarregaEditorDaGrid(colCCDConsolidado);
            Enumeracoes.virEnumTipoLancamentoNeon.virEnumTipoLancamentoNeonSt.CarregaEditorDaGrid(colCCDTipoLancamento);
            if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                spinDEBUG_CCD.Visible = true;
            //Extrato.
            //ComboBoxStatus
        }

        

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Conciliacao.ConciliacaoSt.dConciliacao = dConciliacao;
            Conciliacao.ConciliacaoSt.assistido = true;
            
            foreach (int linha in gridView2.GetSelectedRows())
            {
                if (linha < 0)
                    continue;
                dConciliacao.ContaCorrenteDetalheRow Linhaconciliar = (dConciliacao.ContaCorrenteDetalheRow)gridView2.GetDataRow(linha);
                
                if ((Linhaconciliar.CCDConsolidado != (int)statusconsiliado.Aguardando)
                    &&
                    (Linhaconciliar.CCDConsolidado != (int)statusconsiliado.AguardaConta)
                    )
                    continue;                
                Conciliacao.ConciliacaoSt.Consolidar(Linhaconciliar);                
            }
        }

        private void cConciliacao_cargaPrincipal(object sender, EventArgs e)
        {
            dConciliacao.ContaCorrenteDetalheTableAdapter.FillPendentes(dConciliacao.ContaCorrenteDetalhe);
        }

        private void atualizar() {
            switch (radioGroup1.SelectedIndex)
            {
                case -1:
                case 0:
                    dConciliacao.ContaCorrenteDetalheTableAdapter.FillPendentes(dConciliacao.ContaCorrenteDetalhe);
                    break;
                case 1:
                    dConciliacao.ContaCorrenteDetalheTableAdapter.FillByDeposito(dConciliacao.ContaCorrenteDetalhe);
                    break;
                case 2:                    
                    dConciliacao.ContaCorrenteDetalhe.Clear();
                    if (lookupCondominio_F1.CON_sel > 0)
                    {
                        if (Dini.EditValue == null)
                            Dini.DateTime = DateTime.Today.AddDays(-DateTime.Today.Day + 1);
                        if (Dfim.EditValue == null)
                            Dfim.DateTime = DateTime.Today;
                        dConciliacao.ContaCorrenteDetalheTableAdapter.FillByBalancete(dConciliacao.ContaCorrenteDetalhe,Dini.DateTime,Dfim.DateTime,lookupCondominio_F1.CON_sel);                        
                    };
                    break;
                case 3:
                    dConciliacao.ContaCorrenteDetalheTableAdapter.FillByDeposito(dConciliacao.ContaCorrenteDetalhe);
                    break;
                default:
                    break;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            atualizar();            
        }

        private void advBandedGridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colCCDErro)
                e.Appearance.ForeColor = Color.Red;
        }

        private void Botoes(bool Ligar){
            BTParar.Visible = Gau.Visible = !Ligar;
            BTAtualizar.Visible = BTClasse.Visible = BTDep.Visible = BTPro1.Visible = BTProTodos.Visible = Ligar;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Conciliacao.ConciliacaoSt.dConciliacao = dConciliacao;
            Conciliacao.ConciliacaoSt.assistido = true;
            parar = false;
            Gau.Position = 0;
            Gau.Properties.Maximum = dConciliacao.ContaCorrenteDetalhe.Count; 
            Botoes(false);
            
            DateTime Atualizarem = DateTime.Now.AddSeconds(3);
            int contador = 0;
            CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
            foreach (dConciliacao.ContaCorrenteDetalheRow Linhaconciliar in dConciliacao.ContaCorrenteDetalhe)
            {
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Loop");
                if ((Linhaconciliar.CCDConsolidado != (int)statusconsiliado.Aguardando)
                    &&
                    (Linhaconciliar.CCDConsolidado != (int)statusconsiliado.AguardaConta))
                    continue;
                Conciliacao.ConciliacaoSt.Consolidar(Linhaconciliar);
                contador++;
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("GAU");
                if (DateTime.Now > Atualizarem)
                {
                    Gau.Position = contador;
                    Application.DoEvents();
                    if (parar)
                        break;
                    Atualizarem = DateTime.Now.AddSeconds(3);
                };
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Loop");
            };
            MessageBox.Show(CompontesBasicos.Performance.Performance.PerformanceST.Relatorio());
            Botoes(true);
            
            if (!parar)
            {
                gridView2.ActiveFilterString = "CCDConsolidado <> " + ((int)statusconsiliado.Automatico).ToString();
                gridView2.ActiveFilterString += " and CCDConsolidado <> " + ((int)statusconsiliado.Filial).ToString();
                gridView2.ActiveFilterString += " and CCDConsolidado <> " + ((int)statusconsiliado.Inativo).ToString();
            }
        }

        private bool parar;

        private void BTParar_Click(object sender, EventArgs e)
        {
            parar = true;
        }

        private cClassificar _cClasse;

        private cClassificar cClasse {
            get {
                if (_cClasse == null)
                    _cClasse = new cClassificar();
                return _cClasse;
            }
            set {
                _cClasse = value;
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e){
             ClassificacaoManual();
        }

        private void ClassificacaoManual()
        {
            dConciliacao.ContaCorrenteDetalheRow LinhaMae = (dConciliacao.ContaCorrenteDetalheRow)gridView2.GetFocusedDataRow();
            if (LinhaMae == null)
                return;
            cClasse.AjustaRow(LinhaMae);
            if (cClasse.ShowEmPopUp() == DialogResult.OK)
            {
                bool Feito = false;
                
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco cConciliacao - 228",
                                                                           dConciliacao.ContaCorrenteDetalheTableAdapter,
                                                                           dConciliacao.ARquivoLidoTableAdapter);
                    VirOleDb.TableAdapter.AbreTrasacaoOLEDB("dllbanco cConciliacao - 231",
                                                                          dConciliacao.Lancamentos_BradescoTableAdapter,
                                                                          dConciliacao.Cheques_DevolvidosTableAdapter
                                                                          );
                    Conciliacao.ConciliacaoSt.RegistroAntigoLancBradesco(LinhaMae, cClasse.lookUpEditPLA1.Text, cClasse.textEdit1.Text,false);
                    LinhaMae.CCDTipoLancamento = (int)TipoLancamentoNeon.PelaDescricao;
                    //dConciliacao.ContaCorrenteDetalheTableAdapter.SetStatus((int)statusconsiliado.Manual, (int)Enumeracoes.TipoLancamentoNeon.PelaDescricao, LinhaMae.CCD);
                    LinhaMae.CCDConsolidado = (int)statusconsiliado.Manual;
                    LinhaMae.CCDErro = "Ok";
                    LinhaMae.CCDConsolidado = (int)statusconsiliado.Manual;
                    LinhaMae.CCDTipoLancamento = (int)TipoLancamentoNeon.PelaDescricao;
                    dConciliacao.ContaCorrenteDetalheTableAdapter.Update(LinhaMae);
                    LinhaMae.AcceptChanges();
                    VirOleDb.TableAdapter.STTableAdapter.Commit();
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();                    
                    Feito = true;
                }
                catch (Exception e)
                {
                    LinhaMae.CCDErro = e.Message;
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(null);
                    VirOleDb.TableAdapter.STTableAdapter.Vircatch(e);
                    Feito = false;
                };
                if (Feito)
                {
                    if (dConciliacao.Descricoes.Count == 0)
                        dConciliacao.DescricoesTableAdapter.Fill(dConciliacao.Descricoes);
                    dConciliacao.DescricoesRow rowDesc = dConciliacao.Descricoes.FindByCategoria_lancamentoDescricao(LinhaMae.CCDCategoria.ToString("000"), LinhaMae.CCDDescricao);
                    if (rowDesc == null) {
                        if (cClasse.checkEdit1.Checked)
                        {
                            rowDesc = dConciliacao.Descricoes.NewDescricoesRow();
                            rowDesc.Categoria_lancamento = LinhaMae.CCDCategoria.ToString("000");
                            rowDesc.Descricao = LinhaMae.CCDDescricao;
                            rowDesc.Plano_Contas = cClasse.textEdit1.Text;
                            rowDesc.TipoPag = cClasse.lookUpEditPLA1.Text;
                            dConciliacao.Descricoes.AddDescricoesRow(rowDesc);
                            dConciliacao.DescricoesTableAdapter.Update(rowDesc);
                            rowDesc.AcceptChanges();
                        };
                    }
                }
                cClasse = null;
            }            
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            atualizar();
        }

        //private dConciliacao.ContaCorrenteDetalheRow[] LinhaMaes;
        private List<dConciliacao.ContaCorrenteDetalheRow> LinhaMaes;
        private int iLinhaMae;
        private decimal SaldoLinhaMae;
        private bool Terminado;

        private bool PegaProximoDep()
        {
            if (iLinhaMae >= 0)
            {
                LinhaMaes[iLinhaMae].CCDConsolidado = (int)statusconsiliado.Deposito;
                LinhaMaes[iLinhaMae].CCDErro = "Ok";
                LinhaMaes[iLinhaMae].CCDTipoLancamento = (int)TipoLancamentoNeon.deposito;
                dConciliacao.ContaCorrenteDetalheTableAdapter.Update(LinhaMaes[iLinhaMae]);
            }
            iLinhaMae++;
            if (iLinhaMae < LinhaMaes.Count)
            {
                SaldoLinhaMae = LinhaMaes[iLinhaMae].CCDValor;
                return true;
            }
            else
            {
                Terminado = true;
                return false;
            }
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            Terminado = false;
            //dConciliacao.ContaCorrenteDetalheRow LinhaMae = null;
            int[] LinhasSel = gridView2.GetSelectedRows();
            decimal Total = 0;
            
            if (LinhasSel.Length <= 1)
            {
                dConciliacao.ContaCorrenteDetalheRow LinhaMae = (dConciliacao.ContaCorrenteDetalheRow)gridView2.GetFocusedDataRow();

                if ((LinhaMae == null) || (LinhaMae.CCDValor < 0))
                  return;
                switch ((statusconsiliado)LinhaMae.CCDConsolidado)
                {
                    case statusconsiliado.Aguardando:
                        break;
                    case statusconsiliado.Manual:
                        if(LinhaMae.CCDTipoLancamento == (int)TipoLancamentoNeon.deposito)
                            {
                            if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select count(BOL) as boletos from boletos where bol_ccd = @P1",LinhaMae.CCD) == 0)
                                break;
                            }
                        return;
                    default:
                        return;
                }

                Total = LinhaMae.CCDValor;                
                LinhaMaes = new List<dConciliacao.ContaCorrenteDetalheRow>();
                LinhaMaes.Add( LinhaMae);
            }
            else {
                int? CCT = null;
                LinhaMaes = new List<dConciliacao.ContaCorrenteDetalheRow>();
                for (int h = 0; h < LinhasSel.Length;h++ )
                {
                    if (LinhasSel[h] < 0)
                        continue;
                    dConciliacao.ContaCorrenteDetalheRow LinhaMae = (dConciliacao.ContaCorrenteDetalheRow)gridView2.GetDataRow(LinhasSel[h]);
                    if (!CCT.HasValue)
                        CCT = LinhaMae.CCT;
                    else 
                    {
                        if (CCT.Value != LinhaMae.CCT)
                        {
                            MessageBox.Show("Contas diferentes selecionadas!!");
                            return;
                        };
                    };
                    if (LinhaMae.CCDValor < 0)
                        continue;
                    if (LinhaMae.CCDConsolidado != (int)statusconsiliado.Aguardando)
                        continue;
                    LinhaMaes.Add(LinhaMae);
                    Total += LinhaMae.CCDValor;
                }
            };

            if (LinhaMaes.Count == 0)
                return;

            depositos.cDeposito cDep = new dllbanco.depositos.cDeposito();
            cDep.LinhaMae = LinhaMaes[0];
            cDep.Meta = Total;
            cDep.LabelTotal.Text = string.Format("Total do dep�sito: {0:n2} em {1:dd/MM/yyyy}", Total, LinhaMaes[0].SCCData);            
            cDep.Somar();
            if(cDep.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK){
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco cConciliacao - 349", dConciliacao.ContaCorrenteDetalheTableAdapter,
                                                                                         dConciliacao.ContaCorrenteSubdetalheTableAdapter);
                    //VirOleDb.TableAdapter.AbreTrasacaoOLEDB("dllbanco cConciliacao - 350",Conciliacao.ConciliacaoSt.dConciliacao.Lancamentos_BradescoTableAdapter);
                    iLinhaMae = -1;
                    if (!PegaProximoDep())
                        throw new Exception("Erro na baixa do deposito");                    
                    
                    if (cDep.PLA != "") //lan�a como saldo linhas inteiras e um parte
                    {
                        Conciliacao.ConciliacaoSt.RegistroAntigoLancBradesco(LinhaMaes[iLinhaMae], cDep.PLA, cDep.Descricao, cDep.Erro, false);                        
                        //Transformar linha toda de dep�sito em credito se o valor a classificar for maior que o total da linha
                        while (SaldoLinhaMae <= cDep.Erro)
                        {
                            //LinhaMaes[iLinhaMae].CCDConsolidado = (int)statusconsiliado.Deposito; //foi conciliado em uma opera��o de deposito
                            //LinhaMaes[iLinhaMae].CCDErro = "Ok";                            
                            //LinhaMaes[iLinhaMae].CCDTipoLancamento = (int)Enumeracoes.TipoLancamentoNeon.deposito; //� um dep�sito  
                            LinhaMaes[iLinhaMae].CCD_CTL = Framework.datasets.dPLAnocontas.dPLAnocontasSt.CTLPadrao(LinhaMaes[iLinhaMae].CON, CTLTipo.Caixa);
                            LinhaMaes[iLinhaMae].CCD_DescricaoBAL=cDep.Descricao;
                            LinhaMaes[iLinhaMae].CCD_PLA=cDep.PLA;                                                                                                                
                            cDep.Erro -= SaldoLinhaMae;
                            if (!PegaProximoDep())
                                throw new Exception("Erro na baixa do deposito");
                        }
                        //Saldo
                        dConciliacao.ContaCorrenteSubdetalheRow rowCCS = dConciliacao.ContaCorrenteSubdetalhe.NewContaCorrenteSubdetalheRow();                        
                        rowCCS.CCS_CCD = LinhaMaes[iLinhaMae].CCD;
                        rowCCS.CCS_CTL = Framework.datasets.dPLAnocontas.dPLAnocontasSt.CTLPadrao(LinhaMaes[iLinhaMae].CON,CTLTipo.Caixa);
                        rowCCS.CCS_PLA = cDep.PLA;
                        rowCCS.CCSDescricaoBAL = cDep.Descricao;
                        rowCCS.CCSValor = cDep.Erro;
                        dConciliacao.ContaCorrenteSubdetalhe.AddContaCorrenteSubdetalheRow(rowCCS);
                        dConciliacao.ContaCorrenteSubdetalheTableAdapter.Update(rowCCS);
                        rowCCS.AcceptChanges();

                        SaldoLinhaMae -= cDep.Erro;                        
                    }

                    
                    foreach (depositos.dDeposito_boleto.BoletosQuitarRow rowBQ in cDep.dDeposito_boleto.BoletosQuitar)
                    {
                        if (Terminado)
                            break;
                        decimal ValorQuitar = rowBQ.ValorQuitar;
                        decimal Multa = rowBQ.Multa;
                        Boletos.Boleto.Boleto BoletoBaixar = (Boletos.Boleto.Boleto)cDep.BoletosList[rowBQ.BOL];
                        while (ValorQuitar > SaldoLinhaMae) // tem que dividir o boleto. a segunda metade vai ser quitada depois se tiver saldo
                        {
                            Boletos.Boleto.Boleto[] BolParciais;
                            decimal[] multas = new decimal[2] { 0, 0 };
                            if (Multa == 0)
                                BolParciais = BoletoBaixar.Dividir("Dividido para quita��o parcial", SaldoLinhaMae);
                            else
                            {
                                BolParciais = BoletoBaixar.DividirPorc("Dividido para quita��o parcial", SaldoLinhaMae / ValorQuitar);
                                multas[0] = Math.Round(Multa * (SaldoLinhaMae / ValorQuitar), 2);
                                multas[1] = Multa - multas[0];
                            }
                            if (!BolParciais[0].Baixa("Dep�sito 1", LinhaMaes[iLinhaMae].SCCData, SaldoLinhaMae, multas[0], LinhaMaes[iLinhaMae].CCD))
                            {
                                MessageBox.Show("Erro na baixa do boleto: " + BoletoBaixar.rowPrincipal.BOL);
                                throw new Exception(BoletoBaixar.UltimoErro);
                            };                                                        
                            //prepara o boleto saldo para quita��o
                            ValorQuitar -= SaldoLinhaMae;
                            Multa = multas[1];                            
                            BoletoBaixar = BolParciais[1];

                            if (!PegaProximoDep())
                                break;
                        }

                        //Quita o boleto ou o saldo
                        //if (SaldoLinhaMae >= ValorQuitar)
                        if (!Terminado)
                        {
                            if (!BoletoBaixar.Baixa("Dep�sito 1", LinhaMaes[iLinhaMae].SCCData, ValorQuitar, Multa, LinhaMaes[iLinhaMae].CCD))
                            {
                                MessageBox.Show("Erro na baixa do boleto: " + BoletoBaixar.rowPrincipal.BOL);
                                throw new Exception(BoletoBaixar.UltimoErro);
                            };
                            SaldoLinhaMae -= ValorQuitar;
                            if (SaldoLinhaMae == 0)
                                PegaProximoDep();
                        }
                    };                    
                    //VirOleDb.TableAdapter.STTableAdapter.Commit();
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    
                }
                catch (Exception ex) {
                    UltimoErro = ex.Message;
                    //VirOleDb.TableAdapter.STTableAdapter.Vircatch(null);
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                    throw new Exception("Erro na baixa de dep�sito", ex);
                }
            };                                                        
        }

        

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
           
            MessageBox.Show("ok");
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            //DateTime DataCorte = Framework.datasets.dFERiados.DiasUteis(DateTime.Today,3,Framework.datasets.dFERiados.Sentido.retorna,Framework.datasets.dFERiados.Sabados.naoUteis,Framework.datasets.dFERiados.Feriados.naoUteis);
            Conciliacao.ConciliacaoSt.VerificarDebitoAutomaticoPendente();
        }

        private void simpleButton1_Click_2(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                Conciliacao Conciliacao1 = new Conciliacao();
                Conciliacao1.AmarraBoletosCaixa();
            }
        }

        private void simpleButton3_Click_1(object sender, EventArgs e)
        {
            if ((Dini.DateTime == DateTime.MinValue) || (Dfim.DateTime == DateTime.MinValue))
                MessageBox.Show("Data inv�lida");
            else
            {
                using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                {
                    Application.DoEvents();
                    Conciliacao Conciliacao1 = new Conciliacao();
                    Conciliacao1.AmarraBoletosCaixaRarge(Dini.DateTime, Dfim.DateTime);                    
                }
            }

        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            if ((Dini.DateTime == DateTime.MinValue) || (Dfim.DateTime == DateTime.MinValue))
                MessageBox.Show("Data inv�lida");
            else
            {
                using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                {
                    Application.DoEvents();
                    Conciliacao Conciliacao1 = new Conciliacao();
                    if (lookupCondominio_F1.CON_sel > 0)
                    {
                        FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(lookupCondominio_F1.CON_sel);
                        if (rowCON != null)
                            Conciliacao1.AmarraBoletosRarge(rowCON.CON, rowCON.CON_BCO, Dini.DateTime, Dfim.DateTime);
                    }
                }
            }
        }

        private void spinDEBUG_CCD_EditValueChanged(object sender, EventArgs e)
        {
            Conciliacao.ConciliacaoSt.CCDDebug = (int)spinDEBUG_CCD.Value;
        }

        
    }
}

