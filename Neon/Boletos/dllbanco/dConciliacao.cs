﻿namespace dllbanco {


    partial class dConciliacao
    {
        private dConciliacaoTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FORNECEDORES
        /// </summary>
        public dConciliacaoTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new dConciliacaoTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao();
                };
                return fORNECEDORESTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.ContaCorrenteSubdetalheTableAdapter contaCorrenteSubdetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteSubdetalhe
        /// </summary>
        public dConciliacaoTableAdapters.ContaCorrenteSubdetalheTableAdapter ContaCorrenteSubdetalheTableAdapter
        {
            get
            {
                if (contaCorrenteSubdetalheTableAdapter == null)
                {
                    contaCorrenteSubdetalheTableAdapter = new dConciliacaoTableAdapters.ContaCorrenteSubdetalheTableAdapter();
                    contaCorrenteSubdetalheTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenteSubdetalheTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.DebitoAutomatico2TableAdapter debitoAutomatico2TableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DebitoAutomatico2
        /// </summary>
        public dConciliacaoTableAdapters.DebitoAutomatico2TableAdapter DebitoAutomatico2TableAdapter
        {
            get
            {
                if (debitoAutomatico2TableAdapter == null)
                {
                    debitoAutomatico2TableAdapter = new dConciliacaoTableAdapters.DebitoAutomatico2TableAdapter();
                    debitoAutomatico2TableAdapter.TrocarStringDeConexao();
                };
                return debitoAutomatico2TableAdapter;
            }
        }

        private dConciliacaoTableAdapters.ExtratoAteTableAdapter extratoAteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ExtratoAte
        /// </summary>
        public dConciliacaoTableAdapters.ExtratoAteTableAdapter ExtratoAteTableAdapter
        {
            get
            {
                if (extratoAteTableAdapter == null)
                {
                    extratoAteTableAdapter = new dConciliacaoTableAdapters.ExtratoAteTableAdapter();
                    extratoAteTableAdapter.TrocarStringDeConexao();
                };
                return extratoAteTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.BALancetesTableAdapter bALancetesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BALancetes
        /// </summary>
        public dConciliacaoTableAdapters.BALancetesTableAdapter BALancetesTableAdapter
        {
            get
            {
                if (bALancetesTableAdapter == null)
                {
                    bALancetesTableAdapter = new dConciliacaoTableAdapters.BALancetesTableAdapter();
                    bALancetesTableAdapter.TrocarStringDeConexao();
                };
                return bALancetesTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.ContaCorrenteDetalheTableAdapter contaCorrenteDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteDetalhe
        /// </summary>
        public dConciliacaoTableAdapters.ContaCorrenteDetalheTableAdapter ContaCorrenteDetalheTableAdapter
        {
            get
            {
                if (contaCorrenteDetalheTableAdapter == null)
                {
                    contaCorrenteDetalheTableAdapter = new dConciliacaoTableAdapters.ContaCorrenteDetalheTableAdapter();
                    contaCorrenteDetalheTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenteDetalheTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.PagamentosdoCHETableAdapter pagamentosdoCHETableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PagamentosdoCHE
        /// </summary>
        public dConciliacaoTableAdapters.PagamentosdoCHETableAdapter PagamentosdoCHETableAdapter
        {
            get
            {
                if (pagamentosdoCHETableAdapter == null)
                {
                    pagamentosdoCHETableAdapter = new dConciliacaoTableAdapters.PagamentosdoCHETableAdapter();
                    pagamentosdoCHETableAdapter.TrocarStringDeConexao();
                };
                return pagamentosdoCHETableAdapter;
            }
        }

        

        private dConciliacaoTableAdapters.Lancamentos_BradescoTableAdapter lancamentos_BradescoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Lancamentos_Bradesco
        /// </summary>
        public dConciliacaoTableAdapters.Lancamentos_BradescoTableAdapter Lancamentos_BradescoTableAdapter
        {
            get
            {
                if (lancamentos_BradescoTableAdapter == null)
                {
                    lancamentos_BradescoTableAdapter = new dConciliacaoTableAdapters.Lancamentos_BradescoTableAdapter();
                    lancamentos_BradescoTableAdapter.TrocarStringDeConexao();
                };
                return lancamentos_BradescoTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.DescricoesTableAdapter descricoesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Descricoes
        /// </summary>
        public dConciliacaoTableAdapters.DescricoesTableAdapter DescricoesTableAdapter
        {
            get
            {
                if (descricoesTableAdapter == null)
                {
                    descricoesTableAdapter = new dConciliacaoTableAdapters.DescricoesTableAdapter();
                    descricoesTableAdapter.TrocarStringDeConexao();
                };
                return descricoesTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.Cheques_DevolvidosTableAdapter cheques_DevolvidosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Cheques_Devolvidos
        /// </summary>
        public dConciliacaoTableAdapters.Cheques_DevolvidosTableAdapter Cheques_DevolvidosTableAdapter
        {
            get
            {
                if (cheques_DevolvidosTableAdapter == null)
                {
                    cheques_DevolvidosTableAdapter = new dConciliacaoTableAdapters.Cheques_DevolvidosTableAdapter();
                    cheques_DevolvidosTableAdapter.TrocarStringDeConexao();
                };
                return cheques_DevolvidosTableAdapter;
            }
        }

        

        private dConciliacaoTableAdapters.ARquivoLidoTableAdapter aRquivoLidoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ARquivoLido
        /// </summary>
        public dConciliacaoTableAdapters.ARquivoLidoTableAdapter ARquivoLidoTableAdapter
        {
            get
            {
                if (aRquivoLidoTableAdapter == null)
                {
                    aRquivoLidoTableAdapter = new dConciliacaoTableAdapters.ARquivoLidoTableAdapter();
                    aRquivoLidoTableAdapter.TrocarStringDeConexao();
                };
                return aRquivoLidoTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.PaGamentosFixosTableAdapter paGamentosFixosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PaGamentosFixos
        /// </summary>
        public dConciliacaoTableAdapters.PaGamentosFixosTableAdapter PaGamentosFixosTableAdapter
        {
            get
            {
                if (paGamentosFixosTableAdapter == null)
                {
                    paGamentosFixosTableAdapter = new dConciliacaoTableAdapters.PaGamentosFixosTableAdapter();
                    paGamentosFixosTableAdapter.TrocarStringDeConexao();
                };
                return paGamentosFixosTableAdapter;
            }
        }

        private dConciliacaoTableAdapters.DebitoNaoOcorridoTableAdapter debitoNaoOcorridoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DebitoNaoOcorrido
        /// </summary>
        public dConciliacaoTableAdapters.DebitoNaoOcorridoTableAdapter DebitoNaoOcorridoTableAdapter
        {
            get
            {
                if (debitoNaoOcorridoTableAdapter == null)
                {
                    debitoNaoOcorridoTableAdapter = new dConciliacaoTableAdapters.DebitoNaoOcorridoTableAdapter();
                    debitoNaoOcorridoTableAdapter.TrocarStringDeConexao();
                };
                return debitoNaoOcorridoTableAdapter;
            }
        }
    }
}

namespace dllbanco.dConciliacaoTableAdapters {
    
    
    public partial class DebitoAutomatico2TableAdapter {
    }
}
