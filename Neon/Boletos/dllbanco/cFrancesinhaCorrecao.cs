﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicosProc;
using CompontesBasicos;
using VirEnumeracoesNeon;

namespace dllbanco
{
    /// <summary>
    /// Correção de francesinha
    /// </summary>
    public partial class cFrancesinhaCorrecao : ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cFrancesinhaCorrecao()
        {
            InitializeComponent();
            virEnumStatus.virEnumStatusSt.CarregaEditorDaGrid(colSTATUS);
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            dFrancesinhaCorrecao.FranAberta.Clear();            
        }

       

        private void Calcular()
        {
            if (chNConciliados.Checked)
                dFrancesinhaCorrecao.FranAbertaTableAdapter.Fill(dFrancesinhaCorrecao.FranAberta, dateEdit1.DateTime);
            else
                dFrancesinhaCorrecao.FranAbertaTableAdapter.FillTodos(dFrancesinhaCorrecao.FranAberta, dateEdit1.DateTime);
            foreach (dFrancesinhaCorrecao.FranAbertaRow row in dFrancesinhaCorrecao.FranAberta)
            {
                if (row.ARLStatusN == (int)ARLStatusN.CanceladoManualmente)
                    row.STATUS = (int)Status.Cancelado;
                else if (row.IsBOLNull())
                    row.STATUS = (int)Status.Digitado;
                else if (!row.IsARL_CCDNull())
                    row.STATUS = (int)Status.Conciliado;
                else if (row.ARLNossoNumero == row.BOL)
                    row.STATUS = (int)Status.BaixaSimples;
                else if(row.BOLTipoCRAI == "E")
                    row.STATUS = (int)Status.Duplicidade;
                else
                    row.STATUS = (int)Status.YYY;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.BeginDataUpdate();
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    Application.DoEvents();
                    Calcular();
                }
            }
            finally
            {
                gridView1.EndDataUpdate();
            }
        }

        private void cFrancesinhaCorrecao_Load(object sender, EventArgs e)
        {
            DateTime DataRef = DateTime.Today.AddMonths(-3);
            dateEdit1.DateTime = new DateTime(DataRef.Year, DataRef.Month, 1);
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            dFrancesinhaCorrecao.FranAbertaRow row = (dFrancesinhaCorrecao.FranAbertaRow)gridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            try
            {
                VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoLocal("gridView1_RowUpdated cFrancesinhaCorrecao.cs 56", dFrancesinhaCorrecao.FranAbertaTableAdapter);
                DateTime BOLPagamentoAnterior = (DateTime)row["ARL_BOLPagamento", DataRowVersion.Original];
                DateTime ARL_DataAnterior = (DateTime)row["ARL_Data", DataRowVersion.Original];
                decimal ARLValorAnterior = (decimal)row["ARLValor", DataRowVersion.Original];
                int ARLNossoNumeroAnterior = (int)row["ARLNossoNumero", DataRowVersion.Original];
                if (   (row.ARL_BOLPagamento != BOLPagamentoAnterior)
                    || (row.ARL_Data != ARL_DataAnterior) 
                    || (row.ARLValor != ARLValorAnterior)
                    || (row.ARLNossoNumero != ARLNossoNumeroAnterior)
                   )
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ARquivoLido set ARL_BOLPagamento = @P1,ARL_Data = @P2, ARLNossoNumero = @P3, ARLValor = @P4 where ARL = @P5", 
                                                                              row.ARL_BOLPagamento, 
                                                                              row.ARL_Data, 
                                                                              row.ARLNossoNumero, 
                                                                              row.ARLValor,
                                                                              row.ARL);
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOLetos set BOLPagamento = @P1 where BOL_ARL = @P2", row.ARL_BOLPagamento, row.ARL);
                    if (!row.IsARL_CCDNull())
                        Conciliacao.CancelaConcEstratoFran(row.ARL_CCD,true);
                }
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
            }
        }

        private bool editavel(DevExpress.XtraGrid.Columns.GridColumn Coluna,Status status)
        {
            switch (status)
            {
                case Status.Digitado:
                    return Coluna.EstaNoGrupo(colARL_BOLPagamento, colARL_Data, colARLNossoNumero, colARLValor);
                case Status.BaixaSimples:
                    return Coluna.EstaNoGrupo(colARL_BOLPagamento);
            }
            return false;
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dFrancesinhaCorrecao.FranAbertaRow row = (dFrancesinhaCorrecao.FranAbertaRow)gridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if (e.Column == colSTATUS)
            {                
                e.Appearance.BackColor = virEnumStatus.virEnumStatusSt.GetCor(row.STATUS);
                e.Appearance.ForeColor = Color.Black;
            }
            else
                if (e.Column.EstaNoGrupo(colARL_BOLPagamento, colARL_Data, colARLNossoNumero, colARLValor))
                {
                    if (editavel(e.Column, (Status)row.STATUS))
                    {
                        e.Appearance.BackColor = Color.LightGoldenrodYellow;
                        e.Appearance.ForeColor = Color.Black;
                    }                    
                }
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column == colBotao)
            {
                dFrancesinhaCorrecao.FranAbertaRow row = (dFrancesinhaCorrecao.FranAbertaRow)gridView1.GetDataRow(e.RowHandle);
                if ((row == null) || row.IsSTATUSNull())
                    return;
                //ATENÇÃO PROGRAMADOR. SÓ FOI IMPLEMENTADO PARA BOLETOS DIGTADOS E BAIXAS SIMPLES e duplicidade. CASO HAJA DEMANDA PODERÃO SE IMPLANTADAS AS DEMAIS
                //CADA CASO DEVE SER BEM ANALISADO E TRATADO, NÃO BASTA INCLUIR NO GRUPO!!!
                if (row.STATUS.EstaNoGrupo((int)Status.Digitado,(int)Status.BaixaSimples,(int)Status.Duplicidade))
                    e.RepositoryItem = BotaoDel;
            }
        }

        private void BotaoDel_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dFrancesinhaCorrecao.FranAbertaRow row = (dFrancesinhaCorrecao.FranAbertaRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            switch ((Status)row.STATUS)
            {
                case Status.Digitado:
                    if (MessageBox.Show("Confirma a exclusão ?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("BotaoDel_ButtonClick cFrancesinhaCorrecao.cs 140");
                            if (!row.IsARL_CCDNull())
                                Conciliacao.CancelaConcEstratoFran(row.ARL_CCD,true);
                            if (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete arquivolido where ARL = @P1", row.ARL) == 1)
                            {
                                row.Delete();
                                row.AcceptChanges();
                            }
                            else
                                throw new Exception("Erro ao apagar ARL");
                            VirMSSQL.TableAdapter.CommitSQL();
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.VircatchSQL(ex);
                            throw new Exception(string.Format("Erro: {0}", ex.Message), ex);
                        }
                    }
                    break;
                case Status.BaixaSimples:
                    if (MessageBox.Show("A quitação do boleto terá que ser cancelada.\r\nConfirma a exclusão e o cancelamento?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        Boletos.Boleto.Boleto Boleto = new Boletos.Boleto.Boleto(row.BOL);
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("BotaoDel_ButtonClick cFrancesinhaCorrecao.cs 149");
                            if (Boleto.CancelaBaixa(string.Format("Francesinha manual removida por {0}", Framework.DSCentral.USUNome)))
                            {
                                if (!row.IsARL_CCDNull())
                                    Conciliacao.CancelaConcEstratoFran(row.ARL_CCD,true);
                                if (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete arquivolido where ARL = @P1", row.ARL) == 1)
                                {
                                    row.Delete();
                                    row.AcceptChanges();
                                }
                                else
                                    throw new Exception("Erro ao apagar ARL");
                            }
                            VirMSSQL.TableAdapter.CommitSQL();
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.VircatchSQL(ex);
                            throw new Exception(string.Format("Erro: {0}",ex.Message), ex);
                        }                        
                    }
                    break;
                case Status.Duplicidade:
                    string ComandoRastreador =
"SELECT        dbo.BOLetos.BOL, dbo.BOletoDetalhe.BOD AS BODO, BOletoDetalhe_1.BOD AS BODD, BOLetos_1.BOL AS BOLDescontado, BOLetos_1.BOLValorPago, BOletoDetalhe_1.BODValor AS BODDesconto\r\n" +
"FROM            dbo.BOLetos INNER JOIN\r\n" +
"                         dbo.BOletoDetalhe ON dbo.BOLetos.BOL = dbo.BOletoDetalhe.BOD_BOL INNER JOIN\r\n" +
"                         dbo.BODxBOD ON dbo.BOletoDetalhe.BOD = dbo.BODxBOD.BODOrigem INNER JOIN\r\n" +
"                         dbo.BOletoDetalhe AS BOletoDetalhe_1 ON dbo.BODxBOD.BODDestino = BOletoDetalhe_1.BOD LEFT OUTER JOIN\r\n" +
"                         dbo.BOLetos AS BOLetos_1 ON BOletoDetalhe_1.BOD_BOL = BOLetos_1.BOL\r\n" +
"WHERE        (dbo.BOLetos.BOL = @P1) AND (dbo.BOLetos.BOLTipoCRAI = 'E');";
                    DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(ComandoRastreador, row.BOL);
                    //Checagem
                    decimal TotalDescontos = 0;
                    //bool DescontoUsado = false;
                    int? BODCredito = null;
                    foreach (DataRow DR in DT.Rows)
                    {
                        if (!BODCredito.HasValue)
                            BODCredito = (int)DR["BODO"];
                        else
                            if (BODCredito.Value != (int)DR["BODO"])
                            {
                                MessageBox.Show("Mais de um Crédito.\r\nCancelado");
                                return;
                            }
                        TotalDescontos += (decimal)DR["BODDesconto"];
                        if (DR["BOLDescontado"] != DBNull.Value)
                        {
                            MessageBox.Show("Desconto utiizado!! Cancele a quitação se necessário e libere o desconto!");
                            return;
                        }; 
                    }
                    if (TotalDescontos != -row.ARLValor)
                    {
                        MessageBox.Show("O valor dos descontos não bate com o valor pago.\r\nCancelado");
                        //return;
                    }                    
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("BotaoDel_ButtonClick cFrancesinhaCorrecao.cs - 253");                        
                        foreach (DataRow DR in DT.Rows)
                        {
                            //Apara BODxBOD
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from BODxBOD where BODDestino = @P1", DR["BODD"]);
                            //Apaga os descontos
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from BOletoDetalhe where BOD = @P1", DR["BODD"]);
                        }
                        //Apaga credito
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from BOletoDetalhe where BOD = @P1", BODCredito.Value);
                        //Apaga Boleto E
                        Boletos.Boleto.Boleto BoletoE = new Boletos.Boleto.Boleto(row.BOL);
                        BoletoE.BOLStatus = StatusBoleto.Cancelado;
                        BoletoE.rowPrincipal.BOLCancelado = true;
                        BoletoE.GravaJustificativa("Cancelado: Boleto de crédito havia sido gerado por francesinha em duplicidade");
                        if(VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update arquivolido set ARLStatusN = 12 where ARL = @P1 and ARLStatusN = @P2", row.ARL, row.ARLStatusN) != 1)
                            throw new Exception("Erro de concorrência");                        
                        VirMSSQL.TableAdapter.CommitSQL();
                        row.Delete();
                        row.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(ex);
                        throw new Exception("Erro: " + ex.Message, ex);
                    }
                    break;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            new cFrancesinhaManual().VirShowModulo(EstadosDosComponentes.PopUp);
            Calcular();
        }

        private void checkEdit1_Click(object sender, EventArgs e)
        {
            dFrancesinhaCorrecao.FranAberta.Clear();    
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dFrancesinhaCorrecao.FranAbertaRow row = (dFrancesinhaCorrecao.FranAbertaRow)gridView1.GetDataRow(e.FocusedRowHandle);
            if ((row == null) || row.IsSTATUSNull())
                return;
            colARL_BOLPagamento.OptionsColumn.ReadOnly = !editavel(colARL_BOLPagamento,(Status)row.STATUS); 
            colARL_Data.OptionsColumn.ReadOnly = !editavel(colARL_Data,(Status)row.STATUS);
            colARLNossoNumero.OptionsColumn.ReadOnly = !editavel(colARLNossoNumero, (Status)row.STATUS);
            colARLValor.OptionsColumn.ReadOnly = !editavel(colARLValor, (Status)row.STATUS);                             
        }
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    internal enum Status
    {
        Digitado = 0,
        BaixaSimples = 1,
        Duplicidade = 2,
        Cancelado = 3,
        YYY = 4,
        Conciliado = 10,
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    internal class virEnumStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumStatus(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumStatus _virEnumStatusSt;

        /// <summary>
        /// VirEnum estático para campo Status
        /// </summary>
        public static virEnumStatus virEnumStatusSt
        {
            get
            {
                if (_virEnumStatusSt == null)
                {
                    _virEnumStatusSt = new virEnumStatus(typeof(Status));
                    _virEnumStatusSt.GravaNomes(Status.Digitado, "Digitado", System.Drawing.Color.LemonChiffon);
                    _virEnumStatusSt.GravaNomes(Status.YYY, "???", System.Drawing.Color.White);
                    _virEnumStatusSt.GravaNomes(Status.Conciliado, "Conciliado", System.Drawing.Color.Gray);
                    _virEnumStatusSt.GravaNomes(Status.BaixaSimples, "Baixa Normal", System.Drawing.Color.Lime);
                    _virEnumStatusSt.GravaNomes(Status.Duplicidade, "Duplicidade", System.Drawing.Color.Orange);
                    _virEnumStatusSt.GravaNomes(Status.Cancelado, "Cancelado", System.Drawing.Color.Silver);                    
                }
                return _virEnumStatusSt;
            }
        }
    }
}
