﻿namespace dllbanco {

    /// <summary>
    /// dPagamentoEletronico
    /// </summary>    
    partial class dPagamentoEletronico 
    {       
       private dPagamentoEletronicoTableAdapters.ARQuivosTableAdapter aRQuivosTableAdapter;

       /// <summary>
       /// ARQuivosTableAdapter
       /// </summary>
       public dPagamentoEletronicoTableAdapters.ARQuivosTableAdapter ARQuivosTableAdapter
       {
          get
          {
             if (aRQuivosTableAdapter == null)
             {
                aRQuivosTableAdapter = new dllbanco.dPagamentoEletronicoTableAdapters.ARQuivosTableAdapter();
                aRQuivosTableAdapter.TrocarStringDeConexao();
             };
             return aRQuivosTableAdapter;
          }

       }

       
       private dPagamentoEletronicoTableAdapters.ARquivoLidoTableAdapter aRquivoLidoTableAdapter;

       /// <summary>
       /// ARquivoLidoTableAdapter
       /// </summary>
       public dPagamentoEletronicoTableAdapters.ARquivoLidoTableAdapter ARquivoLidoTableAdapter
       {
          get
          {
             if (aRquivoLidoTableAdapter == null)
             {
                aRquivoLidoTableAdapter = new dllbanco.dPagamentoEletronicoTableAdapters.ARquivoLidoTableAdapter();
                aRquivoLidoTableAdapter.TrocarStringDeConexao();

             };
             return aRquivoLidoTableAdapter;
          }

       }
    }
}
