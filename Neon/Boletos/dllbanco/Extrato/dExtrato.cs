﻿using dllVirEnum;
using System;
using System.Data;
using System.Collections.Generic;
using VirEnumeracoesNeon;


namespace dllbanco.Extrato
{

    partial class dExtrato
    {

        #region TableAdapters

        private dExtratoTableAdapters.CHEquesTableAdapter cHEquesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CHEques
        /// </summary>
        public dExtratoTableAdapters.CHEquesTableAdapter CHEquesTableAdapter
        {
            get
            {
                if (cHEquesTableAdapter == null)
                {
                    cHEquesTableAdapter = new dExtratoTableAdapters.CHEquesTableAdapter();
                    cHEquesTableAdapter.TrocarStringDeConexao();
                };
                return cHEquesTableAdapter;
            }
        }

        private dExtratoTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dExtratoTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dExtratoTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dllbanco.Extrato.dExtratoTableAdapters.SaldoContaCorrenteTableAdapter saldoContaCorrenteTableAdapter;
        private dllbanco.Extrato.dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter contaCorrenteDetalheTableAdapter;

        /// <summary>
        /// SaldoContaCorrenteTableAdapter
        /// </summary>
        public dllbanco.Extrato.dExtratoTableAdapters.SaldoContaCorrenteTableAdapter SaldoContaCorrenteTableAdapter
        {
            get
            {
                if (saldoContaCorrenteTableAdapter == null)
                {
                    saldoContaCorrenteTableAdapter = new dllbanco.Extrato.dExtratoTableAdapters.SaldoContaCorrenteTableAdapter();
                    saldoContaCorrenteTableAdapter.TrocarStringDeConexao();
                };
                return saldoContaCorrenteTableAdapter;
            }
        }

        /// <summary>
        /// ContaCorrenteDetalheTableAdapter
        /// </summary>
        public dllbanco.Extrato.dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter ContaCorrenteDetalheTableAdapter
        {
            get
            {
                if (contaCorrenteDetalheTableAdapter == null)
                {
                    contaCorrenteDetalheTableAdapter = new dllbanco.Extrato.dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter();
                    contaCorrenteDetalheTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenteDetalheTableAdapter;
            }
        }

        private dExtratoTableAdapters.ARQuivosTableAdapter aRQuivosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ARQuivos
        /// </summary>
        public dExtratoTableAdapters.ARQuivosTableAdapter ARQuivosTableAdapter
        {
            get
            {
                if (aRQuivosTableAdapter == null)
                {
                    aRQuivosTableAdapter = new dExtratoTableAdapters.ARQuivosTableAdapter();
                    aRQuivosTableAdapter.TrocarStringDeConexao();
                };
                return aRQuivosTableAdapter;
            }
        }

        #endregion

        private DateTime DATAMinima = DateTime.MaxValue;

        /// <summary>
        /// Apaga os dados
        /// </summary>
        public void Limpa()
        {
            ContaCorrenteDetalhe.Clear();
            SaldoContaCorrente.Clear();
            DatasExtrasCarregadas = null;
        }

        private void CarregaDataMinima(DateTime DataM)
        {
            DATAMinima = DataM.AddDays(-4);
            ContaCorrenteDetalhe.Clear();
            SaldoContaCorrente.Clear();
            SaldoContaCorrenteTableAdapter.FillDataMinima(SaldoContaCorrente, DATAMinima);
            ContaCorrenteDetalheTableAdapter.FillByDataMinima(ContaCorrenteDetalhe, DATAMinima);
            DatasExtrasCarregadas = new List<DateTime>();
        }

        List<DateTime> DatasExtrasCarregadas;


        private void CarregaDataExtra(DateTime Data)
        {
            if (!DatasExtrasCarregadas.Contains(Data))
            {
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("CarregaDataExtra", true);
                DatasExtrasCarregadas.Add(Data);
                try
                {
                    SaldoContaCorrenteTableAdapter.ClearBeforeFill = false;
                    ContaCorrenteDetalheTableAdapter.ClearBeforeFill = false;
                    SaldoContaCorrenteTableAdapter.FillByExtra(SaldoContaCorrente, Data);
                    ContaCorrenteDetalheTableAdapter.FillByExtra(ContaCorrenteDetalhe, Data);
                }
                finally
                {
                    SaldoContaCorrenteTableAdapter.ClearBeforeFill = true;
                    ContaCorrenteDetalheTableAdapter.ClearBeforeFill = true;
                }
                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
            }
        }

        private DataView _DV_ContaCorrenTe;

        internal DataView DV_ContaCorrenTe
        {
            get
            {
                if (_DV_ContaCorrenTe == null)
                {
                    _DV_ContaCorrenTe = new DataView(ContaCorrenTe);
                }
                return _DV_ContaCorrenTe;
            }
        }

        private DataView _DV_SaldoContaCorrente;

        internal DataView DV_SaldoContaCorrente
        {
            get
            {
                if (_DV_SaldoContaCorrente == null)
                {
                    _DV_SaldoContaCorrente = new DataView(SaldoContaCorrente);
                }
                return _DV_SaldoContaCorrente;
            }
        }

        internal dExtrato.SaldoContaCorrenteRow BuscaSCC(int? CCT, DateTime Data)
        {
            if (SaldoContaCorrente.Count == 0)
                CarregaDataMinima(Data);
            DV_SaldoContaCorrente.Sort = "SCC_CCT,SCCData";
            DataRowView[] DRVs = DV_SaldoContaCorrente.FindRows(new object[] { CCT.Value, Data });
            if (DRVs.Length > 0)
                return (dExtrato.SaldoContaCorrenteRow)DRVs[0].Row;
            else
                return null;
        }

        internal int? BuscaCCT(bool Aplicacao, int BCO, int Agencia, int Conta)
        {
            DV_ContaCorrenTe.RowFilter = Aplicacao ? "CCTTipo in (1,2)" : "CCTTipo in (0,3)";
            DV_ContaCorrenTe.Sort = string.Format("CCT_BCO,CCTAgencia,CCTConta");
            DataRowView[] Encontrados = DV_ContaCorrenTe.FindRows(new object[] { BCO, Agencia, Conta });
            if (Encontrados.Length == 0)
                return null;
            else
                return ((dExtrato.ContaCorrenTeRow)Encontrados[0].Row).CCT;
        }

        /*
        [Obsolete("Centralizar")]
        private bool ETransInterna(Extrato.dExtrato.ContaCorrenteDetalheRow CCDrow,int CCT)
        {
            ContaCorrenTeRow CCTrow = ContaCorrenTe.FindByCCT(CCT);
            if (CCTrow.CCT_BCO == 341)
            {
                if ((CCDrow.CCDCategoria == 206) && (CCDrow.CCDCodHistorico == 48) && (CCDrow.CCDDescricao.Trim() == "RES APLIC AUT MAIS"))
                    return true;
                if ((CCDrow.CCDCategoria == 106) && (CCDrow.CCDCodHistorico == 45) && (CCDrow.CCDDescricao.Trim() == "APL APLIC AUT MAIS"))
                    return true;
            }
            return false;
        }*/

        /// <summary>
        /// Remove linhas provisorias
        /// </summary>
        /// <param name="Candidato"></param>
        /// <returns>retorna true se a linha não ficou vazia</returns>
        internal bool Limpa(dExtrato.SaldoContaCorrenteRow Candidato)
        {
            bool vazia = true;
            List<Extrato.dExtrato.ContaCorrenteDetalheRow> Apagar = new List<Extrato.dExtrato.ContaCorrenteDetalheRow>();
            decimal Saldo = Candidato.SCCValorI;
            decimal SaldoAp = Candidato.SCCValorApI;
            foreach (Extrato.dExtrato.ContaCorrenteDetalheRow rowCCD in Candidato.GetContaCorrenteDetalheRows())
            {
                if (!rowCCD.CCDValidado)
                    Apagar.Add(rowCCD);
                else
                {
                    vazia = false;
                    if ((!rowCCD.IsCCDTipoLancamentoNull()) && ((TipoLancamentoNeon)rowCCD.CCDTipoLancamento == TipoLancamentoNeon.RendimentoContaInvestimento))
                        SaldoAp += rowCCD.CCDValor;
                    else
                    {
                        Saldo += rowCCD.CCDValor;
                        if ((!rowCCD.IsCCDTipoLancamentoNull()) && (rowCCD.CCDTipoLancamento == (int)TipoLancamentoNeon.TransferenciaInterna))
                            //if(diaExtrato.TransferenciaInterna(BCO,rowCCD.CCDDescricao,rowCCD.CCDCategoria,rowCCD.CCDCodHistorico))                    
                            SaldoAp -= rowCCD.CCDValor;
                    }
                }
            }
            if (Candidato.SCCValorF != Saldo)
                Candidato.SCCValorF = Saldo;
            if (Candidato.SCCValorApF != SaldoAp)
                Candidato.SCCValorApF = SaldoAp;
            if (Candidato.SCCAberto)
                Candidato.SCCAberto = false;
            foreach (Extrato.dExtrato.ContaCorrenteDetalheRow rowCCD in Apagar)
            {
                rowCCD.Delete();
                ContaCorrenteDetalheTableAdapter.Update(rowCCD);
            }
            return vazia;
        }

        internal DateTime UltimaData(int CCT)
        {
            DV_SaldoContaCorrente.Sort = "SCCData Desc";
            DV_SaldoContaCorrente.RowFilter = string.Format("SCC_CCT = {0}", CCT);
            if (DV_SaldoContaCorrente.Count == 0)
                return DateTime.MinValue;
            dExtrato.SaldoContaCorrenteRow MAXSCCrow = (dExtrato.SaldoContaCorrenteRow)DV_SaldoContaCorrente[0].Row;
            return MAXSCCrow.SCCData;
        }

        internal dExtrato.SaldoContaCorrenteRow DataAnterior_SCC(int CCT, int BCO, DateTime Dataposterior, bool ComExtras = false)
        {
            if (SaldoContaCorrente.Count == 0)
                CarregaDataMinima(Dataposterior);
            List<dExtrato.SaldoContaCorrenteRow> Apagar = new List<SaldoContaCorrenteRow>();
            DV_SaldoContaCorrente.Sort = "SCCData Desc";
            DV_SaldoContaCorrente.RowFilter = string.Format("SCC_CCT = {0}", CCT);
            foreach (DataRowView DRV in DV_SaldoContaCorrente)
            {
                dExtrato.SaldoContaCorrenteRow Candidato = (dExtrato.SaldoContaCorrenteRow)DRV.Row;
                if ((Candidato.SCCData < DATAMinima) && (!ComExtras))
                    break;
                if (Candidato.SCCData < Dataposterior)
                    if (!Limpa(Candidato))
                        return Candidato;
                    else
                        Apagar.Add(Candidato);
            }
            foreach (dExtrato.SaldoContaCorrenteRow Candidato in Apagar)
            {
                Candidato.Delete();
                SaldoContaCorrenteTableAdapter.Update(Candidato);
            }
            DateTime? DataAnteriorExtra = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_DateTime("SELECT MAX(SCCData) FROM SaldoContaCorrente WHERE (SCC_CCT = @P1) AND (SCCData < @P2)", CCT, Dataposterior);
            if (DataAnteriorExtra.HasValue)
            {
                if ((!DatasExtrasCarregadas.Contains(DataAnteriorExtra.Value)) && (DataAnteriorExtra.Value < DATAMinima))
                {
                    CarregaDataExtra(DataAnteriorExtra.Value);
                    return DataAnterior_SCC(CCT, BCO, Dataposterior, true);
                }
            }
            return null;
        }

    }




}
