﻿using System;
using System.Collections.Generic;
using Framework;
using VirEnumeracoesNeon;

namespace dllbanco.Extrato
{
    /// <summary>
    /// 
    /// </summary>
    public class diaExtrato
    {
        /// <summary>
        /// 
        /// </summary>
        public bool encontrado;

        /// <summary>
        /// 
        /// </summary>
        public int CCT;
        /// <summary>
        /// 
        /// </summary>
        public DateTime Data;

        /// <summary>
        /// 
        /// </summary>
        public dExtrato dExtrato;
        /// <summary>
        /// 
        /// </summary>
        public dExtrato dExtratooc;

        /// <summary>
        /// 
        /// </summary>
        public dExtrato.ContaCorrenTeRow CCTrow;
        /// <summary>
        /// 
        /// </summary>
        public dExtrato.SaldoContaCorrenteRow SCCrow;
        /// <summary>
        /// 
        /// </summary>
        public dExtrato.SaldoContaCorrenteRow SCCrowA;

        /// <summary>
        /// 
        /// </summary>
        public CCTTipo TipoConta;

        /// <summary>
        /// 
        /// </summary>
        public dExtrato.ContaCorrenTeRow CCTrowoc;
        
        
        //public dExtrato.SaldoContaCorrenteRow SCCrowoc;
         
        /// <summary>
        /// 
        /// </summary>
        public dExtrato.SaldoContaCorrenteRow SCCrowAoc;

        private dExtrato.ContaCorrenTeRow CriarCCToc()
        {
            CCTrowoc = dExtratooc.ContaCorrenTe.NewContaCorrenTeRow();
            CCTrowoc.CCT_BCO = CCTrow.CCT_BCO;
            CCTrowoc.CCT_CON = CCTrow.CCT_CON;
            CCTrowoc.CCTAgencia = CCTrow.CCTAgencia;
            CCTrowoc.CCTConta = CCTrow.CCTConta;
            CCTrowoc.CCTTipo = (int)CCTTipo.ContaCorreteOculta;
            CCTrowoc.CCTPrincipal = false;
            CCTrowoc.CCTAplicacao = false;
            dExtratooc.ContaCorrenTe.AddContaCorrenTeRow(CCTrowoc);
            dExtratooc.ContaCorrenTeTableAdapter.EmbarcaEmTransST();
            dExtratooc.ContaCorrenTeTableAdapter.Update(CCTrowoc);
            CCTrowoc.AcceptChanges();
            return CCTrowoc;
        }

        private dExtrato.ARQuivosRow ARQNovorow;

        /// <summary>
        /// 
        /// </summary>
        public int? ARQ;

        /// <summary>
        /// 
        /// </summary>
        public int ARQNovo
        {
            get
            {
                if (ARQ.HasValue)
                    return ARQ.Value;
                if (ARQNovorow == null)
                {
                    ARQNovorow = dExtrato.ARQuivos.NewARQuivosRow();
                    ARQNovorow.ARQBanco = CCTrow.CCT_BCO;
                    ARQNovorow.ARQIData = DateTime.Now;
                    ARQNovorow.ARQNome = "";
                    ARQNovorow.ARQNomeArquivo = string.Format("Arquivo manual digitado em {0:dd/MM/yyyy} por {1}", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome);
                    ARQNovorow.ARQTipo = (int)ARQTipo.Manual;
                    dExtrato.ARQuivos.AddARQuivosRow(ARQNovorow);
                    dExtrato.ARQuivosTableAdapter.Update(ARQNovorow);
                    ARQNovorow.AcceptChanges();
                    ARQ = ARQNovorow.ARQ;
                }
                return ARQNovorow.ARQ;
            }
        }

        /*
        private dExtrato.SaldoContaCorrenteRow CriarSCCoc()
        {
            if (TipoConta != CCTTipo.ContaCorrete_com_Oculta)
                return null;
            if (SCCrow.SCCAberto)
                return null;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco diaExtrato - 81",dExtrato.SaldoContaCorrenteTableAdapter, dExtratooc.SaldoContaCorrenteTableAdapter);
                SCCrowoc = dExtratooc.SaldoContaCorrente.NewSaldoContaCorrenteRow();
                SCCrowoc.SCC_CCT = CCTrowoc.CCT;
                SCCrowoc.SCCData = Data;
                SCCrowoc.SCCDataA = SCCrow.SCCDataA;
                if (SCCrow.SCC_ARQ == -1)
                    SCCrow.SCC_ARQ = ARQNovo;
                SCCrowoc.SCC_ARQ = SCCrow.SCC_ARQ;
                SCCrowoc.SCCValorI = 0;
                SCCrowoc.SCCValorF = 0;
                SCCrowoc.SCCValidoAnterior = SCCrow.SCCValidoAnterior;
                SCCrowoc.SCCAberto = false;
                dExtratooc.SaldoContaCorrente.AddSaldoContaCorrenteRow(SCCrowoc);
                dExtratooc.SaldoContaCorrenteTableAdapter.Update(SCCrowoc);
                SCCrowoc.AcceptChanges();
                //SCCrow.SCCoculta_SCC = SCCrowoc.SCC;
                dExtrato.SaldoContaCorrenteTableAdapter.Update(SCCrow);
                SCCrow.AcceptChanges();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return SCCrowoc;
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw (e);
            }
        }
        */

        private bool CarregaDataAnterior()
        {
            if (SCCrowA != null)
                return true;

            //SCCrowA = dExtrato.DataAnterior_SCC(CCT, Data);

            dExtrato.SaldoContaCorrenteDataTable SaldoAnt = dExtrato.SaldoContaCorrenteTableAdapter.GetUltimaData(Data, CCT);
            if (SaldoAnt.Count != 0)
            //if (SCCrowA != null)
            {
                SCCrowA = SaldoAnt[0];
                /*
                if (TipoConta == CCTTipo.ContaCorrete_com_Oculta)
                {
                    if (!SCCrowA.IsSCCoculta_SCCNull())
                        SCCrowAoc = dExtratooc.SaldoContaCorrenteTableAdapter.GetDataBySCC(SCCrowA.SCCoculta_SCC)[0];
                }*/
                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool VerificarDiaAnterior()
        {
            bool ValidoAnterior = false;
            //dExtrato.SaldoContaCorrenteDataTable SaldoAnt = dExtrato.SaldoContaCorrenteTableAdapter.GetUltimaData(Data, CCT);
            if (CarregaDataAnterior())
            {                                
                if ((SCCrowA.SCCData == SCCrow.SCCDataA) && (SCCrowA.SCCValorF == SCCrow.SCCValorI))
                    ValidoAnterior = true;
                /*
                if (TipoConta == CCTTipo.ContaCorrete_com_Oculta)
                {                    
                    if ((SCCrowAoc == null) || (SCCrowoc == null))
                        ValidoAnterior = false;
                    else
                        if ((SCCrowAoc.SCCData != SCCrowoc.SCCDataA) || (SCCrowAoc.SCCValorF != SCCrowoc.SCCValorI))
                            ValidoAnterior = false;
                }*/
            }
            if (SCCrow.IsSCCValidoAnteriorNull() || (SCCrow.SCCValidoAnterior != ValidoAnterior))
                SCCrow.SCCValidoAnterior = ValidoAnterior;
            return ValidoAnterior;
        }

        private bool VerificaIntegridade()
        {
            decimal Valor = SCCrow.SCCValorI;
            foreach (dExtrato.ContaCorrenteDetalheRow CCDx in SCCrow.GetContaCorrenteDetalheRows())
                if ((CCDCategoria)CCDx.CCDCategoria != CCDCategoria.JurosContaInterna)
                    Valor += CCDx.CCDValor;
            bool Valido = (Valor == SCCrow.SCCValorF);
            if (SCCrow.IsSCCValidoConteudoNull() || (SCCrow.SCCValidoConteudo != Valido))
                SCCrow.SCCValidoConteudo = Valido;
            return Valido;
        }

        /// <summary>
        /// Eliminas registros provisórios
        /// </summary>
        public void Limpa()
        {
            List<dExtrato.ContaCorrenteDetalheRow> Apagar = new List<dExtrato.ContaCorrenteDetalheRow>();
            foreach (dExtrato.ContaCorrenteDetalheRow CCDx in dExtrato.ContaCorrenteDetalhe)
                if (!CCDx.CCDValidado)
                    Apagar.Add(CCDx);
            if (Apagar.Count != 0)
            {
                foreach (dExtrato.ContaCorrenteDetalheRow rowApagar in Apagar)
                    rowApagar.Delete();
                VirMSSQL.TableAdapter.EmbarcaEmTrans(dExtrato.ContaCorrenteDetalheTableAdapter);
                dExtrato.ContaCorrenteDetalheTableAdapter.Update(dExtrato.ContaCorrenteDetalhe);
                dExtrato.ContaCorrenteDetalhe.AcceptChanges();
            }
            /*
             
                    foreach (dExtrato.ContaCorrenteDetalheRow rowCCD in dExtrato.ContaCorrenteDetalhe)
                        if (rowCCD.CCDValidado)
                            SaldoValido += rowCCD.CCDValor;
                        else
                            Apagar.Add(rowCCD);
                    foreach (dExtrato.ContaCorrenteDetalheRow rowApagar in Apagar)
                        rowApagar.Delete();
             */
        }

        private bool CargaInicial(int CCT, DateTime Data,int? SCC = null)
        {
            encontrado = false;
            this.CCT = CCT;
            this.Data = Data;

            VirMSSQL.TableAdapter.EmbarcaEmTrans(dExtrato.ContaCorrenTeTableAdapter,                                                                 
                                                                dExtrato.SaldoContaCorrenteTableAdapter,                                                                
                                                                dExtrato.ContaCorrenteDetalheTableAdapter);

            if (dExtrato.ContaCorrenTeTableAdapter.Fill(dExtrato.ContaCorrenTe, CCT) != 0)
            {

                CCTrow = dExtrato.ContaCorrenTe[0];
                if(CCTrow.IsCCTTipoNull())
                    TipoConta = CCTTipo.ContaCorrete;
                else
                    TipoConta = (CCTTipo)CCTrow.CCTTipo;
                /*
                if (TipoConta == CCTTipo.ContaCorrete_com_Oculta)
                {
                    dExtratooc = new dExtrato();
                    VirMSSQL.TableAdapter.STTableAdapter.EmbarcaEmTrans(dExtratooc.ContaCorrenTeTableAdapter,                                                                
                                                                        dExtratooc.SaldoContaCorrenteTableAdapter);
                    if (dExtratooc.ContaCorrenTeTableAdapter.FillBy(dExtratooc.ContaCorrenTe, CCTrow.CCT_CON, CCTrow.CCTConta, (int)CCTTipo.ContaCorreteOculta) != 0)
                        CCTrowoc = dExtratooc.ContaCorrenTe[0];
                    else
                        CriarCCToc();                    
                }
                */
                

                
                if(dExtrato.SaldoContaCorrenteTableAdapter.FillByData(dExtrato.SaldoContaCorrente,Data,CCT) > 0)
                {
                    SCCrow = null;
                    if (SCC.HasValue)
                        SCCrow = dExtrato.SaldoContaCorrente.FindBySCC(SCC.Value);
                    if (SCCrow == null)
                    {
                        SCCrow = dExtrato.SaldoContaCorrente[0];
                        SCC = SCCrow.SCC;
                    }
                    dExtrato.ContaCorrenteDetalheTableAdapter.FillBySCCcomARQ(dExtrato.ContaCorrenteDetalhe, SCCrow.SCC);
                    dExtrato.ARQuivosTableAdapter.FillByARQ(dExtrato.ARQuivos, SCCrow.SCC_ARQ);
                    encontrado = true;
                    /*
                    if (TipoConta == CCTTipo.ContaCorrete_com_Oculta)
                    {
                        if (!SCCrow.IsSCCoculta_SCCNull())
                        {
                            dExtratooc.SaldoContaCorrenteTableAdapter.FillBySCC(dExtratooc.SaldoContaCorrente, SCCrow.SCCoculta_SCC);
                            SCCrowoc = dExtratooc.SaldoContaCorrente[0];
                        }
                        else                        
                            CriarSCCoc();                        
                    }*/
                    VerificarDiaAnterior();
                    VerificaIntegridade();
                }
                else
                {
                    dExtrato.ContaCorrenteDetalhe.Clear();
                    encontrado = false;
                    CarregaDataAnterior();
                }

                
            }
            return encontrado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataA"></param>
        /// <param name="ValorI"></param>
        /// <param name="ValorF"></param>
        /// <param name="ValorApI"></param>
        /// <param name="ValorApF"></param>
        internal void Cadastrar(DateTime? DataA, decimal? ValorI, decimal? ValorF, decimal? ValorApI, decimal? ValorApF)
        {
            SCCrow = dExtrato.SaldoContaCorrente.NewSaldoContaCorrenteRow();
            SCCrow.SCC_CCT = CCT;
            SCCrow.SCCAberto = false;
            SCCrow.SCCData = Data;
            SCCrow.SCC_ARQ = ARQ.GetValueOrDefault(-1);
            SCCrow.SCCValorI = ValorI.GetValueOrDefault(0);
            SCCrow.SCCValorF = ValorF.GetValueOrDefault(0);
            SCCrow.SCCValorApI = ValorApI.GetValueOrDefault(0);
            SCCrow.SCCValorApF = ValorApF.GetValueOrDefault(0);
            SCCrow.SCCDataA = DataA.GetValueOrDefault(Data.AddDays(-1));            
            SCCrow.SCCValidoAnterior = false;
            SCCrow.SCCValidoConteudo = true;
            dExtrato.SaldoContaCorrente.AddSaldoContaCorrenteRow(SCCrow);
            //CriarSCCoc();
            //LigaContaOculta();
            VerificarDiaAnterior();           
        }
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Categoria"></param>
        /// <param name="historico"></param>
        /// <param name="Descricao"></param>
        /// <param name="documento"></param>
        /// <param name="registroExtra"></param>
        /// <param name="ValorCorrente"></param>
        /// <param name="ComplementoDesc"></param>
        internal void RegistraLancamento(int Categoria, int historico, string Descricao, int documento, bool registroExtra, decimal ValorCorrente, string ComplementoDesc)
        {

            dExtrato.ContaCorrenteDetalheRow novaCCD = dExtrato.ContaCorrenteDetalhe.NewContaCorrenteDetalheRow();
            novaCCD.CCDConsolidado = (int)statusconsiliado.Aguardando;
            novaCCD.CCDCategoria = Categoria;
            novaCCD.CCDCodHistorico = historico;
            novaCCD.CCDDescricao = Descricao;
            novaCCD.CCDDocumento = documento;
            novaCCD.CCDValidado = !registroExtra;
            novaCCD.CCDValor = ValorCorrente;
            novaCCD.CCDDescComplemento = ComplementoDesc.Trim();
            novaCCD.CCD_ARQ = ARQ.GetValueOrDefault(-1);
            novaCCDs.Add(novaCCD);
            SCCrow.SCCValorF += ValorCorrente;
            if (registroExtra)
                SCCrow.SCCAberto = true;
        }

        private List<dExtrato.ContaCorrenteDetalheRow> _novaCCDs;
        private List<dExtrato.ContaCorrenteDetalheRow> novaCCDs
        {
            get
            {
                if (_novaCCDs == null)
                    _novaCCDs = new List<dExtrato.ContaCorrenteDetalheRow>();
                return _novaCCDs;
            }
        }

        /*
        private decimal  CalculaSaldoInterno()
        {
            if ((TipoConta != CCTTipo.ContaCorrete_com_Oculta) || (SCCrowoc == null))
                return 0;
            decimal Finaloc = SCCrowoc.SCCValorI;           
            foreach (dExtrato.ContaCorrenteDetalheRow CCDrow in dExtrato.ContaCorrenteDetalhe)
            {
                if (CCDrow.RowState == DataRowState.Deleted)
                    continue;
                
                    if ((CCDCategoria)CCDrow.CCDCategoria == CCDCategoria.JurosContaInterna)
                    {
                        Finaloc += CCDrow.CCDValor;                        
                    }
                    else
                    {
                        if (TransferenciaInterna((CCDCategoria)CCDrow.CCDCategoria, CCDrow.CCDDescricao))                        
                            Finaloc -= CCDrow.CCDValor;                                                
                    }                                              
            }
            SCCrowoc.SCCValorF = Finaloc;
            return Finaloc;
        }*/

        /// <summary>
        /// 
        /// </summary>
        public void RegistraSaldoFinal(decimal SCCValorF)
        {
            SCCrow.SCCValidoConteudo = (SCCrow.SCCValorF == SCCValorF);
            SCCrow.SCCValorF = SCCValorF;                        
            if (SCCrow.RowState == System.Data.DataRowState.Detached)
                dExtrato.SaldoContaCorrente.AddSaldoContaCorrenteRow(SCCrow);
            dExtrato.SaldoContaCorrenteTableAdapter.Update(SCCrow);
            SCCrow.AcceptChanges();            
            foreach (dExtrato.ContaCorrenteDetalheRow CCD in novaCCDs)
            {
                CCD.CCD_SCC = SCCrow.SCC;
                CCD.CCD_ARQ = ARQ.Value;
                dExtrato.ContaCorrenteDetalhe.AddContaCorrenteDetalheRow(CCD);
                dExtrato.ContaCorrenteDetalheTableAdapter.Update(CCD);
                dExtrato.ContaCorrenteDetalhe.AcceptChanges();
            };
            /*
            if ((TipoConta == CCTTipo.ContaCorrete_com_Oculta) && (SCCrowoc != null))
            {
                CalculaSaldoInterno();
                dExtratooc.SaldoContaCorrenteTableAdapter.Update(SCCrowoc);
                SCCrowoc.AcceptChanges();
            }
            */
            novaCCDs.Clear();
        }

        /*
        private void LigaContaOculta()
        {
            if (TipoConta == CCTTipo.ContaCorrete_com_Oculta)
            {
                if (SCCrowAoc != null)
                {
                    if (SCCrowoc.SCCDataA != SCCrowAoc.SCCData)
                        SCCrowoc.SCCDataA = SCCrowAoc.SCCData;
                    if (!SCCrowAoc.IsSCCValorFNull() && (SCCrowoc.SCCValorI != SCCrowAoc.SCCValorF))
                        SCCrowoc.SCCValorI = SCCrowAoc.SCCValorF;
                    SCCrow.SCCValidoAnterior = true;
                }
            }
        }*/

        /// <summary>
        /// Tipo da conta
        /// </summary>
        public CCTTipo CCTTipo
        {
            get { return (CCTTipo)CCTrow.CCTTipo; }
            set 
            {
                if (CCTrow.CCTTipo != (int)value)
                {
                    CCTrow.CCTTipo = (int)value;
                    dExtrato.ContaCorrenTeTableAdapter.EmbarcaEmTransST();
                    dExtrato.ContaCorrenTeTableAdapter.Update(CCTrow);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void LigarAoDiaAnterior()
        {

            if (SCCrowA != null)
            {                
                if (SCCrow.SCCDataA != SCCrowA.SCCData)                
                    SCCrow.SCCDataA = SCCrowA.SCCData;
                if (SCCrowA.IsSCCValorFNull())
                    SCCrowA.SCCValorF = 0;
                if (CCTTipo == CCTTipo.ContaCorrete_com_Oculta)
                {
                    if ((SCCrow.SCCValorI + SCCrow.SCCValorApI) != (SCCrowA.SCCValorF + SCCrowA.SCCValorApF))
                        if ((SCCrow.ARQuivosRow != null) && ((ARQTipo)SCCrow.ARQuivosRow.ARQTipo == ARQTipo.Arquivo))
                        {
                            SCCrow.SCCValorApI = SCCrowA.SCCValorF + SCCrowA.SCCValorApF - SCCrow.SCCValorI;
                        }
                        else
                        {
                            SCCrow.SCCValorI = SCCrowA.SCCValorF;
                            SCCrow.SCCValorApI = SCCrowA.SCCValorApF;
                        }
                }
                else                                    
                    if (SCCrow.SCCValorI != SCCrowA.SCCValorF)                        
                            SCCrow.SCCValorI = SCCrowA.SCCValorF;                                                    
                
                SCCrow.SCCValidoAnterior = true;
                //LigaContaOculta();                
            }            
            else
                SCCrow.SCCValidoAnterior = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public diaExtrato(int? CCT, DateTime Data)
        {
            dExtrato = new Extrato.dExtrato();
            CargaInicial(CCT.Value, Data);            
        }

        /// <summary>
        /// 
        /// </summary>
        public diaExtrato(dExtrato _dExtrato, int CCT, DateTime Data,int? SCC = null)
        {
            dExtrato = _dExtrato;
            CargaInicial(CCT, Data, SCC);
        }

        #region Importacao Unibanco

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Valor"></param>
        public void AjustaValorInternoInicial(decimal Valor)
        {
            if (TipoConta != CCTTipo.ContaCorrete_com_Oculta)
                throw new Exception("A conta não está cadastrar como \"com conta oculta\"");
            Valor -= SCCrow.SCCValorI;
            if (SCCrowoc.SCCValorI != Valor)
                SCCrowoc.SCCValorI = Valor;
        }
        */

        private SortedList<int, string> ParaValidar;

        

        /// <summary>
        /// Verifica se o lançamento está presente.
        /// </summary>
        /// <param name="Descricao"></param>
        /// <param name="Valor"></param>
        /// <returns></returns>
        public bool ValidaLinha(string Descricao, decimal Valor)
        {
            if (ParaValidar == null)
            {
                ParaValidar = new SortedList<int, string>();
                foreach (dExtrato.ContaCorrenteDetalheRow CCDx in dExtrato.ContaCorrenteDetalhe)
                    //if (!TransferenciaInterna(CCTrow.CCT_BCO,  CCDx.CCDDescricao, CCDx.CCDCategoria, CCDx.CCDCodHistorico))
                    if (CCDx.CCDTipoLancamento != (int)TipoLancamentoNeon.TransferenciaInterna)
                        if ((CCDCategoria)CCDx.CCDCategoria != CCDCategoria.JurosContaInterna)
                            ParaValidar.Add(CCDx.CCD, string.Format("{0} - {1:n2}", CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(CCDx.CCDDescricao, CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.RemoveBrancosDuplicados), CCDx.CCDValor));
            }
            string procurar = string.Format("{0} - {1:n2}", CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(Descricao,CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.RemoveBrancosDuplicados), Valor);

            if (ParaValidar.ContainsValue(procurar))
            {
                ParaValidar.RemoveAt(ParaValidar.IndexOfValue(procurar));
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private decimal SaldoTotalFinalEsperado()
        {
            decimal ValorTotal = SCCrow.SCCValorI;// +SCCrowoc.SCCValorI;
            foreach (dExtrato.ContaCorrenteDetalheRow CCDx in dExtrato.ContaCorrenteDetalhe)
                if (CCDx.CCDTipoLancamento != (int)TipoLancamentoNeon.TransferenciaInterna)
                    ValorTotal += CCDx.CCDValor;                                
            return ValorTotal;
        }

        /*
        /// <summary>
        /// Fecha o dia lançando a retabilidade
        /// </summary>
        /// <param name="SaldoFinal"></param>
        /// <returns></returns>
        public bool FechaDia(decimal SaldoFinal)
        {
            if (ParaValidar == null)
                return false;
            if (ParaValidar.Count != 0)
                return false;
            //SCCrowoc.SCCValorF = SaldoFinal - SCCrow.SCCValorF;
            decimal juros = (SaldoFinal - SaldoTotalFinalEsperado());
            if ((juros / SaldoFinal) > 0.03M)
            {                
                return false;
            }
            
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco diaExtrato - 471",
                                                                       dExtrato.SaldoContaCorrenteTableAdapter,
                                                                       dExtrato.ContaCorrenteDetalheTableAdapter,
                                                                       dExtratooc.SaldoContaCorrenteTableAdapter);
                if (juros != 0)
                {
                    dExtrato.ContaCorrenteDetalheRow CCDrowJuros = dExtrato.ContaCorrenteDetalhe.NewContaCorrenteDetalheRow();
                    CCDrowJuros.CCD_ARQ = ARQNovo;
                    CCDrowJuros.CCD_SCC = SCCrow.SCC;
                    CCDrowJuros.CCDCategoria = (int)CCDCategoria.JurosContaInterna;
                    CCDrowJuros.CCDCodHistorico = 0;
                    CCDrowJuros.CCDConsolidado = (int)Enumeracoes.statusconsiliado.Aguardando;
                    CCDrowJuros.CCDDescComplemento = "";
                    CCDrowJuros.CCDDescricao = "Rentabilidade";
                    CCDrowJuros.CCDDocumento = 0;
                    CCDrowJuros.CCDTipoLancamento = (int)Enumeracoes.TipoLancamentoNeon.rentabilidade;
                    CCDrowJuros.CCDValidado = true;
                    CCDrowJuros.CCDValor = juros;
                    dExtrato.ContaCorrenteDetalhe.AddContaCorrenteDetalheRow(CCDrowJuros);
                    dExtrato.ContaCorrenteDetalheTableAdapter.Update(CCDrowJuros);
                    CCDrowJuros.AcceptChanges();
                };
                dExtrato.SaldoContaCorrenteTableAdapter.Update(SCCrow);
                SCCrow.AcceptChanges();
                dExtratooc.SaldoContaCorrenteTableAdapter.Update(SCCrowoc);
                SCCrow.AcceptChanges();
                dExtrato.ContaCorrenteDetalheTableAdapter.Update(dExtrato.ContaCorrenteDetalhe);
                dExtrato.ContaCorrenteDetalhe.AcceptChanges();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e) 
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro ao gravar", e);
            }
            return true;
        }*/

        #endregion
    }
}
