namespace dllbanco.Extrato
{
    partial class cExtrato
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cExtrato));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCCDCategoria = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCDCodHistorico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDescComplemento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDTipoLancamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCDConsolidado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colARQTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCDValidado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARQIData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARQNomeArquivo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSCCData = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSCCDataA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colcalcValorInicial = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colcalcValorFinal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSCCValidoConteudo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSCCValidoAnterior = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSCCAberto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandPrin = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSCCValorI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSCCValorF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandOc = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSCCValorApI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSCCValorApF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colSCC = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSCCValArquivo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.dExtrato = new dllbanco.Extrato.dExtrato();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CHPropagar = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.chLFuturos = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.dataF = new DevExpress.XtraEditors.DateEdit();
            this.dataI = new DevExpress.XtraEditors.DateEdit();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCHEValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colCHEVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHENumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEFavorecido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHETipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboCHETIPO = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCHEStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboCHESTATUS = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colSaldo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.GLancamentosFuturos = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtrato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CHPropagar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chLFuturos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboCHETIPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboCHESTATUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GLancamentosFuturos)).BeginInit();
            this.GLancamentosFuturos.SuspendLayout();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "SaldoContaCorrente";
            this.BindingSource_F.DataSource = this.dExtrato;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(218)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite;
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(58)))), ((int)(((byte)(81)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridView2.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(178)))), ((int)(((byte)(201)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(195)))), ((int)(((byte)(210)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(195)))), ((int)(((byte)(210)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Silver;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(220)))), ((int)(((byte)(227)))));
            this.gridView2.Appearance.OddRow.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.Preview.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9F);
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(138)))), ((int)(((byte)(161)))));
            this.gridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCCDCategoria,
            this.colCCDCodHistorico,
            this.colCCDDocumento,
            this.colCCDDescricao,
            this.colCCDValor,
            this.colCCDDescComplemento,
            this.colCCDTipoLancamento,
            this.colCCDConsolidado,
            this.colARQTipo,
            this.colCCDValidado,
            this.colARQIData,
            this.colARQNomeArquivo,
            this.colCCD});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView2_RowStyle);
            // 
            // colCCDCategoria
            // 
            this.colCCDCategoria.Caption = "Cat.";
            this.colCCDCategoria.ColumnEdit = this.repositoryItemImageComboBox4;
            this.colCCDCategoria.FieldName = "CCDCategoria";
            this.colCCDCategoria.Name = "colCCDCategoria";
            this.colCCDCategoria.OptionsColumn.ReadOnly = true;
            this.colCCDCategoria.Visible = true;
            this.colCCDCategoria.VisibleIndex = 0;
            this.colCCDCategoria.Width = 153;
            // 
            // repositoryItemImageComboBox4
            // 
            this.repositoryItemImageComboBox4.AutoHeight = false;
            this.repositoryItemImageComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox4.Name = "repositoryItemImageComboBox4";
            // 
            // colCCDCodHistorico
            // 
            this.colCCDCodHistorico.Caption = "C�d.";
            this.colCCDCodHistorico.FieldName = "CCDCodHistorico";
            this.colCCDCodHistorico.Name = "colCCDCodHistorico";
            this.colCCDCodHistorico.OptionsColumn.ReadOnly = true;
            this.colCCDCodHistorico.Visible = true;
            this.colCCDCodHistorico.VisibleIndex = 1;
            this.colCCDCodHistorico.Width = 56;
            // 
            // colCCDDocumento
            // 
            this.colCCDDocumento.Caption = "Doc.";
            this.colCCDDocumento.FieldName = "CCDDocumento";
            this.colCCDDocumento.Name = "colCCDDocumento";
            this.colCCDDocumento.OptionsColumn.ReadOnly = true;
            this.colCCDDocumento.Visible = true;
            this.colCCDDocumento.VisibleIndex = 2;
            this.colCCDDocumento.Width = 60;
            // 
            // colCCDDescricao
            // 
            this.colCCDDescricao.Caption = "Descri��o";
            this.colCCDDescricao.FieldName = "CCDDescricao";
            this.colCCDDescricao.Name = "colCCDDescricao";
            this.colCCDDescricao.OptionsColumn.ReadOnly = true;
            this.colCCDDescricao.Visible = true;
            this.colCCDDescricao.VisibleIndex = 3;
            this.colCCDDescricao.Width = 251;
            // 
            // colCCDValor
            // 
            this.colCCDValor.Caption = "Valor";
            this.colCCDValor.DisplayFormat.FormatString = "n2";
            this.colCCDValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCCDValor.FieldName = "CCDValor";
            this.colCCDValor.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colCCDValor.Name = "colCCDValor";
            this.colCCDValor.OptionsColumn.ReadOnly = true;
            this.colCCDValor.Visible = true;
            this.colCCDValor.VisibleIndex = 8;
            this.colCCDValor.Width = 77;
            // 
            // colCCDDescComplemento
            // 
            this.colCCDDescComplemento.Caption = "Complemento";
            this.colCCDDescComplemento.FieldName = "CCDDescComplemento";
            this.colCCDDescComplemento.Name = "colCCDDescComplemento";
            this.colCCDDescComplemento.OptionsColumn.ReadOnly = true;
            this.colCCDDescComplemento.Visible = true;
            this.colCCDDescComplemento.VisibleIndex = 4;
            this.colCCDDescComplemento.Width = 100;
            // 
            // colCCDTipoLancamento
            // 
            this.colCCDTipoLancamento.Caption = "Tipo de Lan�amento";
            this.colCCDTipoLancamento.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCCDTipoLancamento.FieldName = "CCDTipoLancamento";
            this.colCCDTipoLancamento.Name = "colCCDTipoLancamento";
            this.colCCDTipoLancamento.OptionsColumn.ReadOnly = true;
            this.colCCDTipoLancamento.Visible = true;
            this.colCCDTipoLancamento.VisibleIndex = 5;
            this.colCCDTipoLancamento.Width = 123;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colCCDConsolidado
            // 
            this.colCCDConsolidado.Caption = "Concilia��o";
            this.colCCDConsolidado.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colCCDConsolidado.FieldName = "CCDConsolidado";
            this.colCCDConsolidado.Name = "colCCDConsolidado";
            this.colCCDConsolidado.OptionsColumn.ReadOnly = true;
            this.colCCDConsolidado.Visible = true;
            this.colCCDConsolidado.VisibleIndex = 6;
            this.colCCDConsolidado.Width = 86;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colARQTipo
            // 
            this.colARQTipo.Caption = "Cadastro";
            this.colARQTipo.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colARQTipo.FieldName = "ARQTipo";
            this.colARQTipo.Name = "colARQTipo";
            this.colARQTipo.OptionsColumn.ReadOnly = true;
            this.colARQTipo.Visible = true;
            this.colARQTipo.VisibleIndex = 7;
            this.colARQTipo.Width = 70;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colCCDValidado
            // 
            this.colCCDValidado.FieldName = "CCDValidado";
            this.colCCDValidado.Name = "colCCDValidado";
            // 
            // colARQIData
            // 
            this.colARQIData.Caption = "Data cadastro";
            this.colARQIData.FieldName = "ARQIData";
            this.colARQIData.Name = "colARQIData";
            // 
            // colARQNomeArquivo
            // 
            this.colARQNomeArquivo.Caption = "Arquivo";
            this.colARQNomeArquivo.FieldName = "ARQNomeArquivo";
            this.colARQNomeArquivo.Name = "colARQNomeArquivo";
            // 
            // colCCD
            // 
            this.colCCD.Caption = "CCD";
            this.colCCD.FieldName = "CCD";
            this.colCCD.Name = "colCCD";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.BindingSource_F;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "FK_ContaCorrenteDetalhe_SaldoContaCorrente";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 122);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemImageComboBox1,
            this.repositoryItemImageComboBox2,
            this.repositoryItemImageComboBox3,
            this.repositoryItemImageComboBox4,
            this.repositoryItemCalcEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1477, 392);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1,
            this.gridView2});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(212)))), ((int)(((byte)(204)))));
            this.bandedGridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(212)))), ((int)(((byte)(204)))));
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(223)))), ((int)(((byte)(217)))));
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(212)))), ((int)(((byte)(204)))));
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(212)))), ((int)(((byte)(204)))));
            this.bandedGridView1.Appearance.Empty.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.bandedGridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(138)))), ((int)(((byte)(131)))));
            this.bandedGridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.bandedGridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(48)))), ((int)(((byte)(41)))));
            this.bandedGridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.bandedGridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.bandedGridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(74)))), ((int)(((byte)(66)))));
            this.bandedGridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(224)))), ((int)(((byte)(216)))));
            this.bandedGridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.Navy;
            this.bandedGridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(178)))));
            this.bandedGridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(202)))), ((int)(((byte)(194)))));
            this.bandedGridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(202)))), ((int)(((byte)(194)))));
            this.bandedGridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(78)))), ((int)(((byte)(71)))));
            this.bandedGridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(144)))), ((int)(((byte)(136)))));
            this.bandedGridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Silver;
            this.bandedGridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.bandedGridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(252)))), ((int)(((byte)(244)))));
            this.bandedGridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.Preview.ForeColor = System.Drawing.Color.Navy;
            this.bandedGridView1.Appearance.Preview.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.Preview.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(224)))), ((int)(((byte)(216)))));
            this.bandedGridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.bandedGridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.Row.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(212)))), ((int)(((byte)(204)))));
            this.bandedGridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(138)))));
            this.bandedGridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(232)))), ((int)(((byte)(226)))));
            this.bandedGridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(192)))), ((int)(((byte)(184)))));
            this.bandedGridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand4,
            this.gridBand5,
            this.gridBandPrin,
            this.gridBandOc,
            this.gridBand6});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colSCCData,
            this.colSCCValorI,
            this.colSCCValorF,
            this.colSCCDataA,
            this.colSCCValidoConteudo,
            this.colSCCValidoAnterior,
            this.colSCCAberto,
            this.gridColumn1,
            this.colcalcValorFinal,
            this.colcalcValorInicial,
            this.colSCCValorApI,
            this.colSCCValorApF,
            this.colSCC,
            this.colSCCValArquivo});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsCustomization.AllowFilter = false;
            this.bandedGridView1.OptionsCustomization.AllowGroup = false;
            this.bandedGridView1.OptionsCustomization.AllowSort = false;
            this.bandedGridView1.OptionsDetail.ShowDetailTabs = false;
            this.bandedGridView1.OptionsPrint.ExpandAllDetails = true;
            this.bandedGridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.bandedGridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSCCData, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.bandedGridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Data";
            this.gridBand1.Columns.Add(this.colSCCData);
            this.gridBand1.Columns.Add(this.colSCCDataA);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 109;
            // 
            // colSCCData
            // 
            this.colSCCData.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colSCCData.AppearanceCell.Options.UseFont = true;
            this.colSCCData.Caption = "Movimento";
            this.colSCCData.FieldName = "SCCData";
            this.colSCCData.Name = "colSCCData";
            this.colSCCData.OptionsColumn.ReadOnly = true;
            this.colSCCData.Visible = true;
            this.colSCCData.Width = 109;
            // 
            // colSCCDataA
            // 
            this.colSCCDataA.Caption = "Anterior";
            this.colSCCDataA.FieldName = "SCCDataA";
            this.colSCCDataA.Name = "colSCCDataA";
            this.colSCCDataA.OptionsColumn.ReadOnly = true;
            this.colSCCDataA.Width = 109;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "Extrato";
            this.gridBand4.Columns.Add(this.colcalcValorInicial);
            this.gridBand4.Columns.Add(this.colcalcValorFinal);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 1;
            this.gridBand4.Width = 260;
            // 
            // colcalcValorInicial
            // 
            this.colcalcValorInicial.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colcalcValorInicial.AppearanceCell.Options.UseFont = true;
            this.colcalcValorInicial.Caption = "Valor Inicial";
            this.colcalcValorInicial.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colcalcValorInicial.FieldName = "calcValorInicial";
            this.colcalcValorInicial.Name = "colcalcValorInicial";
            this.colcalcValorInicial.Visible = true;
            this.colcalcValorInicial.Width = 130;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colcalcValorFinal
            // 
            this.colcalcValorFinal.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colcalcValorFinal.AppearanceCell.Options.UseFont = true;
            this.colcalcValorFinal.Caption = "Valor Final";
            this.colcalcValorFinal.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colcalcValorFinal.FieldName = "calcValorFinal";
            this.colcalcValorFinal.Name = "colcalcValorFinal";
            this.colcalcValorFinal.Visible = true;
            this.colcalcValorFinal.Width = 130;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "Valida��o";
            this.gridBand5.Columns.Add(this.colSCCValidoConteudo);
            this.gridBand5.Columns.Add(this.colSCCValidoAnterior);
            this.gridBand5.Columns.Add(this.colSCCAberto);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 2;
            this.gridBand5.Width = 231;
            // 
            // colSCCValidoConteudo
            // 
            this.colSCCValidoConteudo.Caption = "Saldo OK";
            this.colSCCValidoConteudo.FieldName = "SCCValidoConteudo";
            this.colSCCValidoConteudo.Name = "colSCCValidoConteudo";
            this.colSCCValidoConteudo.OptionsColumn.ReadOnly = true;
            this.colSCCValidoConteudo.Visible = true;
            this.colSCCValidoConteudo.Width = 67;
            // 
            // colSCCValidoAnterior
            // 
            this.colSCCValidoAnterior.Caption = "Sequ�ncia Ok";
            this.colSCCValidoAnterior.FieldName = "SCCValidoAnterior";
            this.colSCCValidoAnterior.Name = "colSCCValidoAnterior";
            this.colSCCValidoAnterior.OptionsColumn.ReadOnly = true;
            this.colSCCValidoAnterior.Visible = true;
            this.colSCCValidoAnterior.Width = 89;
            // 
            // colSCCAberto
            // 
            this.colSCCAberto.Caption = "Aberto";
            this.colSCCAberto.FieldName = "SCCAberto";
            this.colSCCAberto.Name = "colSCCAberto";
            this.colSCCAberto.OptionsColumn.ReadOnly = true;
            this.colSCCAberto.Visible = true;
            // 
            // gridBandPrin
            // 
            this.gridBandPrin.Caption = "Conta Principal";
            this.gridBandPrin.Columns.Add(this.colSCCValorI);
            this.gridBandPrin.Columns.Add(this.colSCCValorF);
            this.gridBandPrin.Name = "gridBandPrin";
            this.gridBandPrin.VisibleIndex = 3;
            this.gridBandPrin.Width = 260;
            // 
            // colSCCValorI
            // 
            this.colSCCValorI.Caption = "Valor Inicial CC";
            this.colSCCValorI.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colSCCValorI.DisplayFormat.FormatString = "n2";
            this.colSCCValorI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSCCValorI.FieldName = "SCCValorI";
            this.colSCCValorI.Name = "colSCCValorI";
            this.colSCCValorI.OptionsColumn.ReadOnly = true;
            this.colSCCValorI.Visible = true;
            this.colSCCValorI.Width = 130;
            // 
            // colSCCValorF
            // 
            this.colSCCValorF.Caption = "Valor Final CC";
            this.colSCCValorF.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colSCCValorF.DisplayFormat.FormatString = "n2";
            this.colSCCValorF.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSCCValorF.FieldName = "SCCValorF";
            this.colSCCValorF.Name = "colSCCValorF";
            this.colSCCValorF.OptionsColumn.ReadOnly = true;
            this.colSCCValorF.Visible = true;
            this.colSCCValorF.Width = 130;
            // 
            // gridBandOc
            // 
            this.gridBandOc.Caption = "Aplica��o Interna";
            this.gridBandOc.Columns.Add(this.colSCCValorApI);
            this.gridBandOc.Columns.Add(this.colSCCValorApF);
            this.gridBandOc.Name = "gridBandOc";
            this.gridBandOc.VisibleIndex = 4;
            this.gridBandOc.Width = 297;
            // 
            // colSCCValorApI
            // 
            this.colSCCValorApI.Caption = "Valor Inicial Aplicado";
            this.colSCCValorApI.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colSCCValorApI.FieldName = "SCCValorApI";
            this.colSCCValorApI.Name = "colSCCValorApI";
            this.colSCCValorApI.Visible = true;
            this.colSCCValorApI.Width = 147;
            // 
            // colSCCValorApF
            // 
            this.colSCCValorApF.Caption = "Valor Final Aplicado";
            this.colSCCValorApF.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colSCCValorApF.FieldName = "SCCValorApF";
            this.colSCCValorApF.Name = "colSCCValorApF";
            this.colSCCValorApF.Visible = true;
            this.colSCCValorApF.Width = 150;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "Bot�o";
            this.gridBand6.Columns.Add(this.gridColumn1);
            this.gridBand6.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.OptionsBand.ShowCaption = false;
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 25;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.Width = 25;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colSCC
            // 
            this.colSCC.Caption = "SCC";
            this.colSCC.FieldName = "SCC";
            this.colSCC.Name = "colSCC";
            this.colSCC.Visible = true;
            // 
            // colSCCValArquivo
            // 
            this.colSCCValArquivo.Caption = "Valor do arquivo";
            this.colSCCValArquivo.DisplayFormat.FormatString = "n2";
            this.colSCCValArquivo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSCCValArquivo.FieldName = "SCCValArquivo";
            this.colSCCValArquivo.Name = "colSCCValArquivo";
            this.colSCCValArquivo.Visible = true;
            // 
            // dExtrato
            // 
            this.dExtrato.DataSetName = "dExtrato";
            this.dExtrato.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.CHPropagar);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.chLFuturos);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.checkEdit1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.cBotaoImpBol1);
            this.panelControl1.Controls.Add(this.dataF);
            this.panelControl1.Controls.Add(this.dataI);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1477, 87);
            this.panelControl1.TabIndex = 4;
            // 
            // CHPropagar
            // 
            this.CHPropagar.Location = new System.Drawing.Point(1126, 64);
            this.CHPropagar.MenuManager = this.BarManager_F;
            this.CHPropagar.Name = "CHPropagar";
            this.CHPropagar.Properties.Caption = "Recalcular";
            this.CHPropagar.Size = new System.Drawing.Size(272, 19);
            this.CHPropagar.TabIndex = 13;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.Location = new System.Drawing.Point(1200, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(133, 52);
            this.simpleButton2.TabIndex = 12;
            this.simpleButton2.Text = "Exportar";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // chLFuturos
            // 
            this.chLFuturos.Location = new System.Drawing.Point(331, 61);
            this.chLFuturos.MenuManager = this.BarManager_F;
            this.chLFuturos.Name = "chLFuturos";
            this.chLFuturos.Properties.Caption = "Mostrar lan�amentos Futuros";
            this.chLFuturos.Size = new System.Drawing.Size(196, 19);
            this.chLFuturos.TabIndex = 11;
            this.chLFuturos.CheckedChanged += new System.EventHandler(this.chLFuturos_CheckedChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 13);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "labelControl1";
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(331, 36);
            this.checkEdit1.MenuManager = this.BarManager_F;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Mostrar lan�amentos";
            this.checkEdit1.Size = new System.Drawing.Size(196, 19);
            this.checkEdit1.TabIndex = 8;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Location = new System.Drawing.Point(1339, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(133, 52);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "Incluir data";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(5, 29);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(160, 52);
            this.cBotaoImpBol1.TabIndex = 6;
            this.cBotaoImpBol1.Titulo = "Recalcular";
            this.cBotaoImpBol1.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // dataF
            // 
            this.dataF.EditValue = null;
            this.dataF.Location = new System.Drawing.Point(171, 61);
            this.dataF.Name = "dataF";
            this.dataF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dataF.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataF.Size = new System.Drawing.Size(142, 20);
            this.dataF.TabIndex = 5;
            // 
            // dataI
            // 
            this.dataI.EditValue = null;
            this.dataI.Location = new System.Drawing.Point(171, 35);
            this.dataI.Name = "dataI";
            this.dataI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dataI.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataI.Size = new System.Drawing.Size(142, 20);
            this.dataI.TabIndex = 4;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitterControl1.Location = new System.Drawing.Point(0, 509);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1477, 5);
            this.splitterControl1.TabIndex = 5;
            this.splitterControl1.TabStop = false;
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "CHEques";
            this.gridControl2.DataSource = this.dExtrato;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(2, 20);
            this.gridControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl2.MainView = this.gridView1;
            this.gridControl2.MenuManager = this.BarManager_F;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit2,
            this.ComboCHETIPO,
            this.ComboCHESTATUS,
            this.repositoryItemButtonEdit2});
            this.gridControl2.Size = new System.Drawing.Size(1473, 246);
            this.gridControl2.TabIndex = 6;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseBorderColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCHEValor,
            this.colCHEVencimento,
            this.colCHE,
            this.colCHENumero,
            this.colCHEEmissao,
            this.colCHEFavorecido,
            this.colCHETipo,
            this.colCHEStatus,
            this.colSaldo,
            this.gridColumn2});
            this.gridView1.GridControl = this.gridControl2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PaintStyleName = "Flat";
            // 
            // colCHEValor
            // 
            this.colCHEValor.Caption = "Valor";
            this.colCHEValor.ColumnEdit = this.repositoryItemCalcEdit2;
            this.colCHEValor.FieldName = "calcCHEValor";
            this.colCHEValor.Name = "colCHEValor";
            this.colCHEValor.Visible = true;
            this.colCHEValor.VisibleIndex = 3;
            // 
            // repositoryItemCalcEdit2
            // 
            this.repositoryItemCalcEdit2.AutoHeight = false;
            this.repositoryItemCalcEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit2.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit2.Name = "repositoryItemCalcEdit2";
            // 
            // colCHEVencimento
            // 
            this.colCHEVencimento.Caption = "Data";
            this.colCHEVencimento.FieldName = "CHEVencimento";
            this.colCHEVencimento.Name = "colCHEVencimento";
            this.colCHEVencimento.Visible = true;
            this.colCHEVencimento.VisibleIndex = 0;
            this.colCHEVencimento.Width = 115;
            // 
            // colCHE
            // 
            this.colCHE.Caption = "CHE";
            this.colCHE.FieldName = "CHE";
            this.colCHE.Name = "colCHE";
            // 
            // colCHENumero
            // 
            this.colCHENumero.Caption = "N�mero";
            this.colCHENumero.FieldName = "CHENumero";
            this.colCHENumero.Name = "colCHENumero";
            this.colCHENumero.Visible = true;
            this.colCHENumero.VisibleIndex = 4;
            // 
            // colCHEEmissao
            // 
            this.colCHEEmissao.Caption = "Emiss�o";
            this.colCHEEmissao.FieldName = "CHEEmissao";
            this.colCHEEmissao.Name = "colCHEEmissao";
            this.colCHEEmissao.Visible = true;
            this.colCHEEmissao.VisibleIndex = 1;
            this.colCHEEmissao.Width = 116;
            // 
            // colCHEFavorecido
            // 
            this.colCHEFavorecido.FieldName = "CHEFavorecido";
            this.colCHEFavorecido.Name = "colCHEFavorecido";
            this.colCHEFavorecido.Visible = true;
            this.colCHEFavorecido.VisibleIndex = 2;
            this.colCHEFavorecido.Width = 383;
            // 
            // colCHETipo
            // 
            this.colCHETipo.Caption = "Tipo";
            this.colCHETipo.ColumnEdit = this.ComboCHETIPO;
            this.colCHETipo.FieldName = "CHETipo";
            this.colCHETipo.Name = "colCHETipo";
            this.colCHETipo.Visible = true;
            this.colCHETipo.VisibleIndex = 5;
            this.colCHETipo.Width = 111;
            // 
            // ComboCHETIPO
            // 
            this.ComboCHETIPO.AutoHeight = false;
            this.ComboCHETIPO.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboCHETIPO.Name = "ComboCHETIPO";
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.Caption = "Status";
            this.colCHEStatus.ColumnEdit = this.ComboCHESTATUS;
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.Visible = true;
            this.colCHEStatus.VisibleIndex = 6;
            this.colCHEStatus.Width = 118;
            // 
            // ComboCHESTATUS
            // 
            this.ComboCHESTATUS.AutoHeight = false;
            this.ComboCHESTATUS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboCHESTATUS.Name = "ComboCHESTATUS";
            // 
            // colSaldo
            // 
            this.colSaldo.Caption = "Saldo";
            this.colSaldo.ColumnEdit = this.repositoryItemCalcEdit2;
            this.colSaldo.FieldName = "Saldo";
            this.colSaldo.Name = "colSaldo";
            this.colSaldo.Visible = true;
            this.colSaldo.VisibleIndex = 7;
            this.colSaldo.Width = 124;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 8;
            this.gridColumn2.Width = 32;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // GLancamentosFuturos
            // 
            this.GLancamentosFuturos.Controls.Add(this.gridControl2);
            this.GLancamentosFuturos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GLancamentosFuturos.Location = new System.Drawing.Point(0, 514);
            this.GLancamentosFuturos.Name = "GLancamentosFuturos";
            this.GLancamentosFuturos.Size = new System.Drawing.Size(1477, 268);
            this.GLancamentosFuturos.TabIndex = 7;
            this.GLancamentosFuturos.Text = ":: LAN�AMENTOS FUTUROS ::";
            this.GLancamentosFuturos.Visible = false;
            // 
            // cExtrato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.GLancamentosFuturos);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.Editavel = false;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cExtrato";
            this.Size = new System.Drawing.Size(1477, 807);
            this.Controls.SetChildIndex(this.GLancamentosFuturos, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            this.Controls.SetChildIndex(this.splitterControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtrato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CHPropagar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chLFuturos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboCHETIPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboCHESTATUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GLancamentosFuturos)).EndInit();
            this.GLancamentosFuturos.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private dExtrato dExtrato;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDCategoria;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDCodHistorico;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDValor;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private DevExpress.XtraEditors.DateEdit dataF;
        private DevExpress.XtraEditors.DateEdit dataI;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDescComplemento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDTipoLancamento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDConsolidado;
        private DevExpress.XtraGrid.Columns.GridColumn colARQTipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox4;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCDataA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colcalcValorInicial;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colcalcValorFinal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCValorI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCValorF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCValidoConteudo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCValidoAnterior;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCAberto;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDValidado;
        private DevExpress.XtraGrid.Columns.GridColumn colARQIData;
        private DevExpress.XtraGrid.Columns.GridColumn colARQNomeArquivo;
        private DevExpress.XtraGrid.Columns.GridColumn colCCD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCValorApI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCValorApF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCC;
        private DevExpress.XtraEditors.CheckEdit chLFuturos;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl GLancamentosFuturos;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colCHE;
        private DevExpress.XtraGrid.Columns.GridColumn colCHENumero;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEFavorecido;
        private DevExpress.XtraGrid.Columns.GridColumn colCHETipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboCHETIPO;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboCHESTATUS;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldo;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandPrin;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandOc;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCValArquivo;
        private DevExpress.XtraEditors.CheckEdit CHPropagar;
    }
}
