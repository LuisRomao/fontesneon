﻿namespace dllbanco.Extrato {
    
    /// <summary>
    /// dCorrecaoBradesco 
    /// </summary>
    public partial class dCorrecaoBradesco 
    {
        private dCorrecaoBradescoTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dCorrecaoBradescoTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dCorrecaoBradescoTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dCorrecaoBradescoTableAdapters.SaldoContaCorrenteTableAdapter saldoContaCorrenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoContaCorrente
        /// </summary>
        public dCorrecaoBradescoTableAdapters.SaldoContaCorrenteTableAdapter SaldoContaCorrenteTableAdapter
        {
            get
            {
                if (saldoContaCorrenteTableAdapter == null)
                {
                    saldoContaCorrenteTableAdapter = new dCorrecaoBradescoTableAdapters.SaldoContaCorrenteTableAdapter();
                    saldoContaCorrenteTableAdapter.TrocarStringDeConexao();
                };
                return saldoContaCorrenteTableAdapter;
            }
        }

        /// <summary>
        /// Correção Bradesco
        /// </summary>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        /// <returns></returns>
        public string Correcao(System.DateTime DI, System.DateTime DF)
        {
            string Retorno = string.Format("Correção Bradesco {0}\r\n\r\n", System.DateTime.Now);
            int contas = ContaCorrenTeTableAdapter.Fill(ContaCorrenTe);
            int Dias = SaldoContaCorrenteTableAdapter.FillByBCO(SaldoContaCorrente, DI, DF);
            Retorno += string.Format(" Contas:{0} Dias:{1}\r\n\r\n", contas,Dias);
            try
            {
                foreach (ContaCorrenTeRow CCTrow in ContaCorrenTe)
                {
                    Retorno += string.Format(" Conta: {0} {1} CCT:{2}\r\n", CCTrow.CONCodigo, CCTrow.CCTConta, CCTrow.CCT);                    
                    SaldoContaCorrenteRow[] SCCs = CCTrow.GetSaldoContaCorrenteRows();
                    if(SCCs.Length == 0)
                        Retorno += string.Format("    Sem Dias\r\n\r\n");
                    else
                        if (SCCs.Length < 2)
                            Retorno += string.Format("    1 Dia\r\n\r\n");
                        else
                        {
                            decimal SaldoFinal_Esperado = SCCs[0].SCCValorF;
                            bool ComErro = false;
                            for(int i = 1; i<SCCs.Length;i++)
                            {
                                decimal somaIt = SaldoContaCorrenteTableAdapter.SomaCCD(SCCs[i].SCC).GetValueOrDefault(0);
                                SaldoFinal_Esperado += somaIt;
                                if (SCCs[i].SCCValorI != SCCs[i - 1].SCCValorF)
                                    ComErro = true;
                                if (SCCs[i].SCCValorF != SCCs[i].SCCValorI + somaIt)
                                    ComErro = true;
                            }
                            if (SCCs[SCCs.Length - 1].SCCValorF != SaldoFinal_Esperado)
                            {
                                Retorno += string.Format("     ******* ERRO nos Lançamentos: Saldo em {0} = {1:n2} soma dos lançamentos: {2:n2} \r\n\r\n",
                                                         SCCs[SCCs.Length - 1].SCCData, SCCs[SCCs.Length - 1].SCCValorF, SaldoFinal_Esperado);
                                SaldoFinal_Esperado = SCCs[0].SCCValorF;
                                Retorno += string.Format("                 {0:dd/MM/yyyy} Salao Final: {1:n2}\r\n", SCCs[0].SCCData, SCCs[0].SCCValorF);
                                for (int i = 1; i < SCCs.Length; i++)
                                {
                                    decimal somaIt = SaldoContaCorrenteTableAdapter.SomaCCD(SCCs[i].SCC).GetValueOrDefault(0);
                                    SaldoFinal_Esperado += somaIt;
                                    Retorno += string.Format("                      {0:dd/MM/yyyy}: {2:n2} {2:n2}\r\n",SCCs[i].SCCData, somaIt, SaldoFinal_Esperado);
                                }
                            }
                            else
                                if (!ComErro)
                                    Retorno += string.Format("     OK\r\n");
                                else
                                {
                                    Retorno += string.Format("     Inicio Correçôes\r\n");
                                    for (int i = 1; i < SCCs.Length; i++)
                                    {
                                        bool Gravar = false;
                                        if (SCCs[i].SCCDataA != SCCs[i - 1].SCCData)
                                        {
                                            Retorno += string.Format("{2:dd/MM/yyyy}       DataA {0} -> {1}\r\n", SCCs[i].SCCDataA, SCCs[i - 1].SCCData, SCCs[i].SCCData);
                                            SCCs[i].SCCDataA = SCCs[i - 1].SCCData;
                                            Gravar = true;
                                        }                                        
                                        if (SCCs[i].SCCValorI != SCCs[i - 1].SCCValorF)
                                        {
                                            Retorno += string.Format("{2:dd/MM/yyyy}       ValorI {0} -> {1}\r\n", SCCs[i].SCCValorI, SCCs[i - 1].SCCValorF, SCCs[i].SCCData);
                                            SCCs[i].SCCValorI = SCCs[i - 1].SCCValorF;
                                            Gravar = true;
                                        }
                                        decimal ValorFcalc = SCCs[i].SCCValorI + SaldoContaCorrenteTableAdapter.SomaCCD(SCCs[i].SCC).GetValueOrDefault(0);
                                        if (SCCs[i].SCCValorF != ValorFcalc)
                                        {
                                            Retorno += string.Format("{2:dd/MM/yyyy}       ValorF {0} -> {1}\r\n", SCCs[i].SCCValorI, ValorFcalc, SCCs[i].SCCData);
                                            SCCs[i].SCCValorF = ValorFcalc;
                                            Gravar = true;
                                        }
                                        if (Gravar)
                                        {
                                            SaldoContaCorrenteTableAdapter.Update(SCCs[i]);
                                            SCCs[i].AcceptChanges();
                                        }                                        
                                    }
                                    Retorno += string.Format("     Fim Correçôes\r\n\r\n");
                                }
                        }
                }
                Retorno += string.Format("FIM {0}\r\n\r\n", System.DateTime.Now);
            }
            finally
            {
                Retorno += "Finally";
            }
            return Retorno;
        }
    }
}
