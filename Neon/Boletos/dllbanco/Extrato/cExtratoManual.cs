﻿/*
MR - 12/11/2014 10:00 -          - Inclusão do novo status consiliado de TributoNaoEncontrado como editável (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
                                 - Tratamento no novo tipo de lancamento neon "Tributo" ao verifica valor válido ao carregar o grid (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using CompontesBasicos;
using Framework;
using VirEnumeracoesNeon;

namespace dllbanco.Extrato
{
    /// <summary>
    /// cExtratoManual
    /// </summary>
    public partial class cExtratoManual : ComponenteBaseDialog
    {

        private dExtrato.ARQuivosRow _ArquivoManual = null;

        private Font _FonteIta;

        private Font _FonteItacorte;

        private bool CalculoTravado = true;
        private bool PodeEditar = true;

        private void calcEditANT_EditValueChanged(object sender, EventArgs e)
        {
            if (CalculoTravado)
                return;

            if ((diaExtrato1.TipoConta == CCTTipo.ContaCorrete_com_Oculta) && ((ARQTipo)diaExtrato1.SCCrow.ARQuivosRow.ARQTipo == ARQTipo.Arquivo))
            {
                diaExtrato1.SCCrow.SCCValorApI = calcEditANT.Value - diaExtrato1.SCCrow.SCCValorI;
                calcEditANTcc.Value = diaExtrato1.SCCrow.SCCValorI;
                calcEditANToc.Value = diaExtrato1.SCCrow.SCCValorApI;
            }
            else
            {
                diaExtrato1.SCCrow.SCCValorI = calcEditANT.Value;
                diaExtrato1.SCCrow.SCCValorApI = 0;
            }
            /*
            if ((ARQTipo)diaExtrato1.SCCrow.ARQuivosRow.ARQTipo == ARQTipo.Arquivo)
            {
                diaExtrato1.SCCrow.SCCValorI = calcEditANT.Value;
                diaExtrato1.SCCrow.SCCValorApI = calcEditANT.Value;
            }
            else
                diaExtrato1.SCCrow.SCCValorApI = calcEditANT.Value - diaExtrato1.SCCrow.SCCValorI;
            */
            CalculaSaldo();
            Alteracoes = true;
        }

        private void calcEditANTcc_EditValueChanged(object sender, EventArgs e)
        {
            if (CalculoTravado)
                return;

            if (diaExtrato1.CCTrow.CCT_BCO == 237)
            {
                diaExtrato1.SCCrow.SCCValorI = calcEditANTcc.Value;
                calcEditANT.Value = calcEditANTcc.Value + diaExtrato1.SCCrow.SCCValorApI;
            }
            else
            {
                if ((diaExtrato1.TipoConta == CCTTipo.ContaCorrete_com_Oculta) && ((ARQTipo)diaExtrato1.SCCrow.ARQuivosRow.ARQTipo == ARQTipo.Arquivo))
                {
                    diaExtrato1.SCCrow.SCCValorI = calcEditANTcc.Value;
                    calcEditANT.Value = calcEditANTcc.Value + diaExtrato1.SCCrow.SCCValorApI;
                    //calcEditANToc.Value = diaExtrato1.SCCrow.SCCValorApI;
                }
                else
                {
                    diaExtrato1.SCCrow.SCCValorI = calcEditANT.Value;
                    diaExtrato1.SCCrow.SCCValorApI = 0;
                }
            };
            CalculaSaldo();
            Alteracoes = true;
        }

        private void calcEditANToc_EditValueChanged(object sender, EventArgs e)
        {
            if (CalculoTravado)
                return;
            diaExtrato1.SCCrow.SCCValorApI = calcEditANToc.Value;
            calcEditANT.Value = calcEditANTcc.Value + diaExtrato1.SCCrow.SCCValorApI;
            CalculaSaldo();
            Alteracoes = true;
        }

        private void CalculaSaldo()
        {
            if (CalculoTravado)
                return;
            decimal Final = calcEditANT.Value;
            decimal Finaloc = calcEditANToc.Value;
            foreach (dExtrato.ContaCorrenteDetalheRow CCDrow in dExtrato.ContaCorrenteDetalhe)
            {
                if (CCDrow.RowState == DataRowState.Deleted)
                    continue;
                if (!CCDrow.IsCCDTipoLancamentoNull() && (CCDrow.CCDTipoLancamento == (int)TipoLancamentoNeon.RendimentoContaInvestimento))
                {
                    Finaloc += CCDrow.CCDValor;
                    Final += CCDrow.CCDValor;
                }
                else
                {
                    /*
                    //CORREÇÃO
                    if ((CCDrow.IsCCDTipoLancamentoNull()
                           ||
                          ((Framework.TipoLancamentoNeon)CCDrow.CCDTipoLancamento != Framework.TipoLancamentoNeon.TransferenciaInterna)
                        )
                        &&
                        //TransferenciaInterna(BCO, (CCDCategoria)rowCCD.CCDCategoria, rowCCD.CCDDescricao)
                        diaExtrato.TransferenciaInterna(diaExtrato1.CCTrow.CCT_BCO, CCDrow.CCDDescricao, CCDrow.CCDCategoria, CCDrow.CCDCodHistorico)
                      )
                    {
                        CCDrow.CCDTipoLancamento = (int)Framework.TipoLancamentoNeon.TransferenciaInterna;
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ContaCorrenteDetalhe set CCDTipoLancamento = @P2 where CCD = @P1", CCDrow.CCD, (int)TipoLancamentoNeon.TransferenciaInterna);
                    }
                    */

                    if ((!CCDrow.IsCCDTipoLancamentoNull()) && (CCDrow.CCDTipoLancamento == (int)TipoLancamentoNeon.TransferenciaInterna))
                    {
                        Finaloc -= CCDrow.CCDValor;
                        if ((diaExtrato1.CCTTipo == CCTTipo.ContaCorrete) && (diaExtrato1.CCTrow.CCT_BCO == 341))
                            diaExtrato1.CCTTipo = CCTTipo.ContaCorrete_com_Oculta;
                    }
                    else
                        Final += CCDrow.CCDValor;
                }
                CCDrow.Saldo = Final;
                CCDrow.SaldoCC = Final - Finaloc;
            }
            calcEditFIN.Value = Final;
            calcEditFINoc.Value = Finaloc;
            calcEditFINcc.Value = Final - Finaloc;
        }

        private void dateEditANT_EditValueChanged(object sender, EventArgs e)
        {
            if (CalculoTravado)
                return;
            diaExtrato1.SCCrow.SCCDataA = dateEditANT.DateTime;
            Alteracoes = true;
        }

        private Font FonteIta(Font prototipo)
        {
            if (_FonteIta == null)
                _FonteIta = new Font(prototipo, FontStyle.Italic | FontStyle.Bold);
            return _FonteIta;
        }

        private Font FonteItacorte(Font prototipo)
        {
            if (_FonteItacorte == null)
                _FonteItacorte = new Font(prototipo, FontStyle.Italic | FontStyle.Bold | FontStyle.Strikeout);
            return _FonteItacorte;
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dExtrato.ContaCorrenteDetalheRow CCDEditar = (dExtrato.ContaCorrenteDetalheRow)gridView1.GetDataRow(e.FocusedRowHandle);
            if (CCDEditar == null)
                gridView1.OptionsBehavior.Editable = true;
            else
                gridView1.OptionsBehavior.Editable = RegistroEditavel(CCDEditar);
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            Alteracoes = true;
            dExtrato.ContaCorrenteDetalheRow CCDnew = (dExtrato.ContaCorrenteDetalheRow)gridView1.GetDataRow(e.RowHandle);
            CCDnew.CCD_SCC = diaExtrato1.SCCrow.SCC;
            CCDnew.CCDCodHistorico = -1;
            CCDnew.CCDConsolidado = (int)statusconsiliado.Aguardando;
            CCDnew.CCDDocumento = 0;
            CCDnew.CCDValidado = true;
            CCDnew.CCD_ARQ = ArquivoManual.ARQ;
            CCDnew.CCDCodHistorico = 0;
            CCDnew.CCDDocumento = 0;
            CCDnew.CCDValidado = true;
            CCDnew.CCDDescComplemento = "";
            CCDnew.CCDDescricao = "";
            CCDnew.CCDCategoria = 0;
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dExtrato.ContaCorrenteDetalheRow CCDEditar = (dExtrato.ContaCorrenteDetalheRow)gridView1.GetDataRow(e.RowHandle);
            if (CCDEditar == null)
                return;
            if (!RegistroEditavel(CCDEditar))
            {
                if (diaExtrato1.CCTrow.IsCCT_CCTPlusNull())
                {
                    if (!CCDEditar.IsCCDTipoLancamentoNull() && (CCDEditar.CCDTipoLancamento == (int)TipoLancamentoNeon.TransferenciaInterna))
                        //if (diaExtrato.TransferenciaInterna(diaExtrato1.CCTrow.CCT_BCO, CCDEditar.CCDDescricao, CCDEditar.CCDCategoria, CCDEditar.CCDCodHistorico))
                        e.Appearance.Font = FonteItacorte(e.Appearance.Font);
                    else
                        e.Appearance.Font = FonteIta(e.Appearance.Font);
                }
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            dExtrato.ContaCorrenteDetalheRow CCDrow = (dExtrato.ContaCorrenteDetalheRow)DRV.Row;
            switch ((TipoLancamentoNeon)CCDrow.CCDTipoLancamento)
            {
                case TipoLancamentoNeon.RendimentoContaInvestimento:
                    CCDrow.CCDConsolidado = (int)statusconsiliado.Manual;
                    break;
                case TipoLancamentoNeon.TransferenciaInterna:
                    if (CCDrow.CCDValor < 0)
                    {
                        CCDrow.CCDConsolidado = (int)statusconsiliado.Manual;
                        CCDrow.CCDCategoria = 106;
                        CCDrow.CCDCodHistorico = 294;
                        CCDrow.CCDDescricao = "APLICACOES EM PAPEIS";
                    }
                    else
                    {
                        CCDrow.CCDConsolidado = (int)statusconsiliado.Manual;
                        CCDrow.CCDCategoria = 206;
                        CCDrow.CCDCodHistorico = 63;
                        CCDrow.CCDDescricao = "RESGATE MERCADO ABERTO";
                    }
                    break;
            }
            Alteracoes = true;
            CalculaSaldo();
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            Alteracoes = true;
            dExtrato.ContaCorrenteDetalheRow CCDValidar = (dExtrato.ContaCorrenteDetalheRow)gridView1.GetDataRow(e.RowHandle);
            if (CCDValidar == null)
                return;
            if (CCDValidar.IsCCDTipoLancamentoNull())
            {
                e.ErrorText = "Tipo de lançamento não definido";
                e.Valid = false;
                return;
            }
            switch ((TipoLancamentoNeon)CCDValidar.CCDTipoLancamento)
            {
                case TipoLancamentoNeon.RendimentoContaInvestimento:
                    CCDValidar.CCDCategoria = 0;
                    break;
                case TipoLancamentoNeon.Cheque:
                case TipoLancamentoNeon.DespesasBancarias:
                case TipoLancamentoNeon.Salarios:
                case TipoLancamentoNeon.ChequeDevolvido_Terceiro:
                case TipoLancamentoNeon.debitoautomatico:
                //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
                case TipoLancamentoNeon.Tributo:
                    //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
                    if ((CCDValidar["CCDValor"] == DBNull.Value) || (CCDValidar.CCDValor > 0))
                    {
                        e.ErrorText = "Valor incompatível com o tipo de lançamento";
                        e.Valid = false;
                    }
                    break;
                case TipoLancamentoNeon.Estorno:
                case TipoLancamentoNeon.ChequeDevolvido_Condominio:
                case TipoLancamentoNeon.deposito:
                case TipoLancamentoNeon.creditodecobranca:
                case TipoLancamentoNeon.rentabilidade:
                    if ((CCDValidar["CCDValor"] == DBNull.Value) || (CCDValidar.CCDValor < 0))
                    {
                        e.ErrorText = "Valor incompatível com o tipo de lançamento";
                        e.Valid = false;
                    }
                    break;
                case TipoLancamentoNeon.PelaDescricao:
                case TipoLancamentoNeon.NaoIdentificado:
                default:
                    break;
            }
        }

        private bool RegistroEditavel(dExtrato.ContaCorrenteDetalheRow row)
        {
            if (!row.IsCCDTipoLancamentoNull() && ((TipoLancamentoNeon)row.CCDTipoLancamento == TipoLancamentoNeon.RendimentoContaInvestimento))
                return true;
            /*
            if (!row.IsCCDTipoLancamentoNull() && ((TipoLancamentoNeon)row.CCDTipoLancamento == TipoLancamentoNeon.TransferenciaInterna))
                return true;*/

            List<statusconsiliado> StatusEditavel = new List<statusconsiliado> { statusconsiliado.Aguardando,
                                                                                                         statusconsiliado.Antigo,
                                                                                                         statusconsiliado.Inativo,
                                                                                                         statusconsiliado.Automatico,
                                                                                                         statusconsiliado.Manual,
                                                                                                         //statusconsiliado.ChequeNaoEncontrado,
                                                                                                         //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***            
                                                                                                         statusconsiliado.TributoNaoEncontrado};
            //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
            if ((row.RowState == DataRowState.Added) || (row.RowState == DataRowState.Detached))
                return PodeEditar;


            //Bug: arquivos da caixa poupança estão vindo com problema a partir do dia 13/07/2016 então estamos liberando a edição. como data de termino está 01/11/2016 se for o caso prorrogaremos
            //Data liberada porque o arquivo vem com erro.
            if (
                (diaExtrato1.CCTrow.CCT_BCO == 104)
                &&
                (diaExtrato1.CCTTipo == CCTTipo.Poupanca)
                &&
                (diaExtrato1.Data > new DateTime(2016, 07, 10))
                //&&
                //(diaExtrato1.Data < new DateTime(2016, 11, 01))
               )
                return true;

            if (row.IsARQTipoNull())
                return false;

            if (((ARQTipo)row.ARQTipo == ARQTipo.Manual) && (StatusEditavel.Contains((statusconsiliado)row.CCDConsolidado)))
                return PodeEditar;
            else
                return false;
        }





        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dExtrato.ContaCorrenteDetalheRow CCDDel = (dExtrato.ContaCorrenteDetalheRow)gridView1.GetFocusedDataRow();
            if (CCDDel == null)
                return;
            if (!RegistroEditavel(CCDDel))
                return;
            CCDDel.Delete();
            Alteracoes = true;
            CalculaSaldo();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("O valor do saldo inicial foi obtido de um arquivo retorno.\r\nConfirma a edição?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                calcEditANTcc.Enabled = true;
                BotEditar.Visible = false;
            }
        }

        private dExtrato.ARQuivosRow ArquivoManual
        {
            get
            {
                if (_ArquivoManual == null)
                {
                    _ArquivoManual = dExtrato.ARQuivos.NewARQuivosRow();
                    _ArquivoManual.ARQBanco = diaExtrato1.CCTrow.CCT_BCO;
                    _ArquivoManual.ARQIData = DateTime.Now;
                    _ArquivoManual.ARQNome = string.Format("Extrato manual cadastrado por {0} em {1}", Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome, DateTime.Now);
                    _ArquivoManual.ARQNomeArquivo = "Digitado";
                    _ArquivoManual.ARQTipo = (int)ARQTipo.Manual;
                    dExtrato.ARQuivos.AddARQuivosRow(_ArquivoManual);
                }
                return _ArquivoManual;
            }
        }

        /*
        //private dExtrato.ARQuivosRow ARQNovorow;

        private int ARQNovo
        {
            get 
            {
                if (ARQNovorow == null)
                {
                    ARQNovorow = dExtrato.ARQuivos.NewARQuivosRow();
                    ARQNovorow.ARQBanco = diaExtrato1.CCTrow.CCT_BCO;
                    ARQNovorow.ARQIData = DateTime.Now;
                    ARQNovorow.ARQNome = "";
                    ARQNovorow.ARQNomeArquivo = string.Format("Arquivo manual digitado em {0:dd/MM/yyyy} por {1}",DateTime.Now,Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome);
                    ARQNovorow.ARQTipo = (int)ARQTipo.Manual;
                    dExtrato.ARQuivos.AddARQuivosRow(ARQNovorow);
                    dExtrato.ARQuivosTableAdapter.Update(ARQNovorow);
                    ARQNovorow.AcceptChanges();
                }
                return ARQNovorow.ARQ;
            }
        }*/

        /// <summary>
        /// Fecha a tela e salva as alterações
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if (!Validate())
                return;
            if (Resultado == DialogResult.OK)
            {
                try
                {
                    /*
                    if (diaExtrato1.TipoConta == CCTTipo.ContaCorrete_com_Oculta)
                    {
                        diaExtrato1.SCCrow.SCCValorF = calcEditFIN.Value - calcEditFINoc.Value;
                        diaExtrato1.SCCrowoc.SCCValorF = calcEditFINoc.Value;
                    }
                    else
                        diaExtrato1.SCCrow.SCCValorF = calcEditFIN.Value;
                    */
                    diaExtrato1.SCCrow.SCCValorI = calcEditANTcc.Value;
                    diaExtrato1.SCCrow.SCCValorApI = calcEditANToc.Value;
                    diaExtrato1.SCCrow.SCCValorF = calcEditFIN.Value - calcEditFINoc.Value;
                    diaExtrato1.SCCrow.SCCValorApF = calcEditFINoc.Value;
                    diaExtrato1.SCCrow.SCCValidoConteudo = true;
                    //diaExtrato1.SCCrow.SCCValidoAnterior = ((SCCrowAnt != null) && (SCCrowAnt.SCCData == diaExtrato1.SCCrow.SCCDataA) && (SCCrowAnt.SCCValorF == diaExtrato1.SCCrow.SCCValorI));
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco cExtratoManual - 375", dExtrato.ARQuivosTableAdapter, dExtrato.ContaCorrenteDetalheTableAdapter, dExtrato.SaldoContaCorrenteTableAdapter);
                    if ((diaExtrato1.SCCrow.RowState == DataRowState.Added) || (diaExtrato1.SCCrow.SCC_ARQ == -1))
                    {
                        diaExtrato1.SCCrow.SCC_ARQ = diaExtrato1.ARQNovo;
                        //if(diaExtrato1.SCCrowoc != null)
                        //    diaExtrato1.SCCrowoc.SCC_ARQ = diaExtrato1.ARQNovo;
                    };
                    dExtrato.SaldoContaCorrenteTableAdapter.Update(diaExtrato1.SCCrow);
                    diaExtrato1.SCCrow.AcceptChanges();
                    /*
                    if (diaExtrato1.TipoConta == CCTTipo.ContaCorrete_com_Oculta)
                    {
                        diaExtrato1.dExtratooc.SaldoContaCorrenteTableAdapter.EmbarcaEmTransST();
                        
                        if ((diaExtrato1.SCCrowAoc != null) && (diaExtrato1.SCCrowAoc.RowState != DataRowState.Unchanged))
                        {
                            diaExtrato1.dExtratooc.SaldoContaCorrenteTableAdapter.Update(diaExtrato1.SCCrowAoc);
                            diaExtrato1.SCCrowAoc.AcceptChanges();
                        }
                        
                        if ((diaExtrato1.SCCrowoc != null) && (diaExtrato1.SCCrowoc.RowState != DataRowState.Unchanged))
                        {
                            diaExtrato1.dExtratooc.SaldoContaCorrenteTableAdapter.Update(diaExtrato1.SCCrowoc);
                            diaExtrato1.SCCrowoc.AcceptChanges();
                        }
                    }
                    */
                    foreach (dExtrato.ContaCorrenteDetalheRow CCDrow in dExtrato.ContaCorrenteDetalhe)
                    {
                        if (CCDrow.RowState == DataRowState.Deleted)
                            continue;
                        if (CCDrow.RowState == DataRowState.Added)
                            CCDrow.CCD_ARQ = diaExtrato1.ARQNovo;
                    }
                    diaExtrato1.VerificarDiaAnterior();
                    dExtrato.ContaCorrenteDetalheTableAdapter.Update(dExtrato.ContaCorrenteDetalhe);
                    dExtrato.ContaCorrenteDetalhe.AcceptChanges();
                    if (diaExtrato1.SCCrow.GetContaCorrenteDetalheRows().Length == 0)
                    {
                        diaExtrato1.SCCrow.Delete();
                        dExtrato.SaldoContaCorrenteTableAdapter.Update(dExtrato.SaldoContaCorrente);
                        dExtrato.SaldoContaCorrente.AcceptChanges();
                    }
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                    throw e;
                };
                base.FechaTela(Resultado);
            }
            else
            {
                if (!Alteracoes || (MessageBox.Show("Confirma o cancelamento das alterações", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
                    base.FechaTela(Resultado);
            }

        }

        #region Construtores

        /// <summary>
        /// Construtor padrão
        /// </summary>
        public cExtratoManual()
        {
            InitializeComponent();
        }

        private bool Alteracoes = false;

        private diaExtrato diaExtrato1;

        /*
        private bool VericaContaOculta(DataRow DR)
        {
            CCTOculta = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CCT from ContaCorrenTe where CCTTipo = @P1 and CCT_CON = @P2 and CCTConta = @P3",
                                                                               (int)CCTTipo.ContaCorreteOculta,
                                                                               CON,
                                                                               DR["CCTConta"]);

            if (!CCTOculta.HasValue)
            {
                MessageBox.Show("Erro no cadastro: Conta oculta não encontrada.");
                return false;
            }
            else
                labelControloc.Visible = calcEditANToc.Visible = calcEditFINoc.Visible = true;
            
            return true;
        }
        */

        /*
        private void CargaInicialContaPrincipalContaOculta(DateTime Data)
        {
            dExtratooc = new Extrato.dExtrato();
            SCCrowAntoc = null;
            SCCrowoc = null;
            if (SCCrowAnt != null)
            {
                if (SCCrowAnt.IsSCCoculta_SCCNull())
                {
                    CriarSCCoculta(SCCrowAnt);
                }
                else
                {
                    dExtrato.SaldoContaCorrenteDataTable SaldoAntoc = dExtratooc.SaldoContaCorrenteTableAdapter.GetDataBySCC(SCCrowAnt.SCCoculta_SCC);
                    if (SaldoAntoc.Count != 0)                    
                        SCCrowAntoc = SaldoAntoc[0];                                            
                }
            }

            if (SCCrow.IsSCCoculta_SCCNull())
            {
                CriarSCCoculta(SCCrow);
            }
            
                dExtratooc.SaldoContaCorrenteTableAdapter.FillBySCC(dExtratooc.SaldoContaCorrente, SCCrow.SCCoculta_SCC);
                SCCrowoc = dExtratooc.SaldoContaCorrente[0];
                dExtratooc.ContaCorrenteDetalheTableAdapter.FillBySCCcomARQ(dExtratooc.ContaCorrenteDetalhe, SCCrowoc.SCC);
                
                if (SCCrowAntoc != null)
                {
                    if (SCCrowoc.SCCDataA != SCCrowAntoc.SCCData)
                    {
                        SCCrowoc.SCCDataA = SCCrowAntoc.SCCData;
                    }

                    if (SCCrowoc.SCCValorI != SCCrowAntoc.SCCValorF)
                    {
                        SCCrowoc.SCCValorI = SCCrowAntoc.SCCValorF;
                    }
                };
                calcEditANToc.Value = SCCrowoc.SCCValorI;
                calcEditANT.Value += SCCrowoc.SCCValorI;
        }*/


        /*
        private void CargaInicialContaPrincipal(DateTime Data)
        {
            dExtrato.SaldoContaCorrenteDataTable SaldoAnt = dExtrato.SaldoContaCorrenteTableAdapter.GetUltimaData(Data, CCT);
            SCCrowAnt = null;
            SCCrow = null;
            if (SaldoAnt.Count != 0)
            {
                SCCrowAnt = SaldoAnt[0];
                dateEditANT.DateTime = SCCrowAnt.SCCData;
                calcEditANT.Value = SCCrowAnt.SCCValorF;
                dateEditANT.Properties.MinValue = SCCrowAnt.SCCData;
                dateEditANT.Properties.MaxValue = Data.AddDays(-1);
            }
            else
            {
                dateEditANT.DateTime = Data;
                calcEditANT.Value = 0;
            }

            diaExtrato1 = new diaExtrato(dExtrato, CCT, Data);
            if (!diaExtrato1.encontrado)
                diaExtrato1.Cadastrar(dateEditANT.DateTime, calcEditANT.Value, null);            

            if (SCCrowAnt != null)
            {
                if (SCCrow.SCCDataA != SCCrowAnt.SCCData)
                {
                    SCCrow.SCCDataA = SCCrowAnt.SCCData;
                }
                if (SCCrow.SCCValorI != SCCrowAnt.SCCValorF)
                {
                    SCCrow.SCCValorI = SCCrowAnt.SCCValorF;
                }
            };
        }*/

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CCT"></param>
        /// <param name="Data"></param>
        /// <param name="SCC"></param>
        public cExtratoManual(int CCT, DateTime Data, int? SCC = null)
        {
            InitializeComponent();
            virEnumCCTTipo.virEnumCCTTipoSt.CarregaEditorDaGrid(ComboCCTTipo);
            virEnumCCDCategoria.virEnumCCDCategoriaSt.CarregaEditorDaGrid(colCCDCategoria);
            Enumeracoes.virEnumstatusconsiliado.virEnumstatusconsiliadoSt.CarregaEditorDaGrid(colCCDConsolidado);
            Enumeracoes.virEnumTipoLancamentoNeon.virEnumTipoLancamentoNeonSt.CarregaEditorDaGrid(colCCDTipoLancamento);

            dateEditMOV.DateTime = Data;
            diaExtrato1 = new diaExtrato(dExtrato, CCT, Data, SCC);
            if (!diaExtrato1.encontrado)
                diaExtrato1.Cadastrar(null, null, null, null, null);
            diaExtrato1.LigarAoDiaAnterior();
            dateEditANT.DateTime = diaExtrato1.SCCrow.SCCDataA;

            if((diaExtrato1.CCTrow.CCT_BCO == 104) && (diaExtrato1.CCTTipo != CCTTipo.Poupanca) && (!diaExtrato1.SCCrow.IsSCCValArquivoNull()))
            {
                calcEditANT.Value = calcEditANTcc.Value = diaExtrato1.SCCrow.SCCValArquivo;
                calcEditANToc.Value = 0;
                BotEditar.Visible = false;
            }
            else
            {
                if (diaExtrato1.CCTTipo == CCTTipo.ContaCorrete_com_Oculta)
                {
                    calcEditANT.Value = diaExtrato1.SCCrow.SCCValorI + diaExtrato1.SCCrow.SCCValorApI;
                    calcEditANTcc.Value = diaExtrato1.SCCrow.SCCValorI;
                    calcEditANToc.Value = diaExtrato1.SCCrow.SCCValorApI;
                    if ((diaExtrato1.SCCrow.ARQuivosRow == null) || ((ARQTipo)diaExtrato1.SCCrow.ARQuivosRow.ARQTipo == ARQTipo.Arquivo))
                    {
                        calcEditANTcc.Enabled = false;
                        BotEditar.Visible = true;
                    }
                    else
                    {
                        calcEditANTcc.Enabled = true;
                        BotEditar.Visible = false;
                    }
                }
                else
                {
                    calcEditANT.Value = calcEditANTcc.Value = diaExtrato1.SCCrow.SCCValorI;
                    calcEditANToc.Value = 0;
                }
            }
            if (diaExtrato1.SCCrowA != null)
            {
                dateEditANT.Properties.MinValue = diaExtrato1.SCCrowA.SCCData;
                dateEditANT.Properties.MaxValue = Data.AddDays(-1);
            }

            //if (CompontesBasicos.FormPrincipalBase.USULogado == 30)
            //{
            labelControloc.Visible = calcEditANToc.Visible = calcEditFINoc.Visible = true;
            //    calcEditANToc.Value = diaExtrato1.SCCrowoc.SCCValorApI;
            //    calcEditANT.Value = calcEditANT.Value + calcEditANToc.Value;
            //}

            lookupCondominio_F1.CON_sel = diaExtrato1.CCTrow.CCT_CON;
            colSaldo.Visible = false;

            ComboCCTTipo.EditValue = diaExtrato1.CCTrow.CCTTipo;
            CalculoTravado = false;
            CalculaSaldo();
        }
        #endregion

    }
}
