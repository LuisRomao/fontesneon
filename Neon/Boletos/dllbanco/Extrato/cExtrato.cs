using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Framework;
using FrameworkProc.datasets;
using DevExpress.XtraReports.UI;
using VirEnumeracoesNeon;

namespace dllbanco.Extrato
{
    /// <summary>
    /// Componente demontrativo do extrato
    /// </summary>
    public partial class cExtrato : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// N�o gera messagebox para usar em processos n�o assistidos
        /// </summary>
        public bool Silencioso = false;
        private int CCT;
        private int CON;
        //private int? CCTOculta;
        private CCTTipo TipoConta;
        private bool AplicacaoSeparada;
        private int BCO;

        private void VerificarTipoDeConta()
        {
            dContasLogicas.ContaCorrenTeRow CCTRow = dContasLogicas.dContasLogicasSt.ContaCorrenTe.FindByCCT(CCT);
            if (!CCTRow.IsCCT_CONNull())
                CON = CCTRow.CCT_CON;
            if (CCTRow.IsCCTTipoNull())
                TipoConta = CCTTipo.ContaCorrete;
            else
                TipoConta = (CCTTipo)CCTRow.CCTTipo;
            AplicacaoSeparada = !CCTRow.IsCCT_CCTPlusNull();
            /*
            DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select CCT_CON, CCTConta, CCTTipo, CCT_BCO,CCTAgencia from ContaCorrenTe where CCT = @P1", CCT);
            CON = (int)DR["CCT_CON"];
            if (DR["CCTTipo"] == DBNull.Value)
                TipoConta = CCTTipo.ContaCorrete;
            else
                TipoConta = (CCTTipo)DR["CCTTipo"];
            */
            BCO = CCTRow.CCT_BCO;
            labelControl1.Text = string.Format("{0} - {1} - {2} {3}", CCTRow.CCT_BCO, CCTRow.CCTAgencia, CCTRow.CCTConta, virEnumCCTTipo.virEnumCCTTipoSt.Descritivo(TipoConta));

        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="DataI">Data inicial</param>
        /// <param name="DataF">Data Final</param>     
        /// <param name="_CCT">Conta corrente</param>
        /// <param name="_silencioso">nao exibe mensagens</param>
        public cExtrato(DateTime DataI,DateTime DataF,int _CCT,bool _silencioso)
        {
            InitializeComponent();
            virEnumCCDCategoria.virEnumCCDCategoriaSt.CarregaEditorDaGrid(colCCDCategoria);
            Enumeracoes.virEnumstatusconsiliado.virEnumstatusconsiliadoSt.CarregaEditorDaGrid(colCCDConsolidado);
            Enumeracoes.virEnumTipoLancamentoNeon.virEnumTipoLancamentoNeonSt.CarregaEditorDaGrid(colCCDTipoLancamento);
            virEnumARQTipo.virEnumARQTipoSt.CarregaEditorDaGrid(colARQTipo);
            Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(colCHETipo);
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);                        
            dataI.DateTime = DataI;
            dataF.DateTime = DataF;
            this.CCT = _CCT;
            VerificarTipoDeConta();
            Silencioso = _silencioso;
            if (AplicacaoSeparada)
            {
                dExtrato.SaldoContaCorrente.calcValorInicialColumn.Expression = "SCCValorI";
                dExtrato.SaldoContaCorrente.calcValorFinalColumn.Expression = "SCCValorF";
            }
            Calcula();
            ExpandMasters(true);            
        }

        
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_CCT"></param>
        [Obsolete("somente para corre��o")]
        public cExtrato(int _CCT,DateTime DataCorrecao)
        {
            InitializeComponent();                        
            this.CCT = _CCT;
            VerificarTipoDeConta();
            Silencioso = true;
            Correcao(DataCorrecao);            
        }

        private bool ValidaDia(dExtrato.SaldoContaCorrenteRow LinhaEmTeste) {            
            decimal Saldo = LinhaEmTeste.SCCValorI;
            foreach (dExtrato.ContaCorrenteDetalheRow rowCCD in LinhaEmTeste.GetContaCorrenteDetalheRows())
                if (rowCCD.IsCCDTipoLancamentoNull() || ((TipoLancamentoNeon)rowCCD.CCDTipoLancamento != TipoLancamentoNeon.RendimentoContaInvestimento))
                    Saldo += rowCCD.CCDValor;
            if (LinhaEmTeste.IsSCCValorFNull())
                return false;
            if (LinhaEmTeste.SCCValorF == Saldo)            
                LinhaEmTeste.SCCValidoConteudo = true;                            
            else
            {
                LinhaEmTeste.SCCValidoConteudo = false;
                /*
                if (LinhaEmTeste.SCCValorF == -Saldo)
                {
                    LinhaEmTeste.SCCValorF = Saldo;
                    LinhaEmTeste.SCCValidoConteudo = true;

                }
                else
                    LinhaEmTeste.SCCValidoConteudo = false;
                 */ 
                
            }
            dExtrato.SaldoContaCorrenteTableAdapter.Update(LinhaEmTeste);
            LinhaEmTeste.AcceptChanges();
            return LinhaEmTeste.SCCValidoConteudo;
        }        

        /// <summary>
        /// Valida Todos
        /// </summary>
        /// <returns></returns>
        public bool ValidaTodos(bool Recalcular = false) {
            bool ComErro = false;
            DateTime? DataA = null;
            //decimal? SaldoFA = null;
            decimal? ValorFA = null;
            decimal? ValorFApA = null;
            bool validoanterior;
            bool propagandoContaoc = false;
            foreach (dExtrato.SaldoContaCorrenteRow LinhaEmTeste in dExtrato.SaldoContaCorrente)
            {
                Saldo = LinhaEmTeste.calcValorFinal;
                //Console.WriteLine(string.Format("Saldo: {0} SCC = {1}",Saldo, LinhaEmTeste.SCC));
                if (DataA.HasValue)
                {
                    //if (SaldoFA != LinhaEmTeste.SCCValorI + LinhaEmTeste.SCCValorApI)
                    if ((ValorFA != LinhaEmTeste.SCCValorI) || (ValorFApA != LinhaEmTeste.SCCValorApI))
                    {
                        if (propagandoContaoc)
                        {
                            validoanterior = true;
                            //LinhaEmTeste.SCCValorApI = SaldoFA.Value - LinhaEmTeste.SCCValorI;                            
                            LinhaEmTeste.SCCValorApI = ValorFApA.Value;
                            LinhaEmTeste.SCCValorI = ValorFA.Value;                            
                        }
                        else
                        {
                            validoanterior = false;
                            if (LinhaEmTeste.IsSCCValidoAnteriorNull() || LinhaEmTeste.SCCValidoAnterior)
                                LinhaEmTeste.SCCValidoAnterior = false;
                            ComErro = true;
                        }
                    }
                    else
                    {
                        if (DataA != LinhaEmTeste.SCCDataA)
                        {
                            validoanterior = true;
                            LinhaEmTeste.SCCDataA = DataA.Value;
                            LinhaEmTeste.SCCValidoAnterior = true;
                        }
                        else
                            validoanterior = true;
                    }
                    if (propagandoContaoc)
                    {
                        LinhaEmTeste.SCCValorI = ValorFA.Value;
                        LinhaEmTeste.SCCValorApI = ValorFApA.Value;
                        decimal SCCValorApF = LinhaEmTeste.SCCValorApI;
                        decimal SCCValorF = LinhaEmTeste.SCCValorI;
                        foreach (dExtrato.ContaCorrenteDetalheRow rowCCD in LinhaEmTeste.GetContaCorrenteDetalheRows())
                        {                            
                            if ((!rowCCD.IsCCDTipoLancamentoNull()) && ((TipoLancamentoNeon)rowCCD.CCDTipoLancamento == TipoLancamentoNeon.TransferenciaInterna))
                                SCCValorApF -= rowCCD.CCDValor;
                            if ((!rowCCD.IsCCDTipoLancamentoNull()) && ((TipoLancamentoNeon)rowCCD.CCDTipoLancamento == TipoLancamentoNeon.RendimentoContaInvestimento))
                                SCCValorApF += rowCCD.CCDValor;
                            else
                                SCCValorF += rowCCD.CCDValor;
                        }
                        LinhaEmTeste.SCCValorApF = SCCValorApF;
                        LinhaEmTeste.SCCValorF = SCCValorF;                        
                    }
                    if (LinhaEmTeste.IsSCCValidoAnteriorNull() || (LinhaEmTeste.SCCValidoAnterior != validoanterior))
                        LinhaEmTeste.SCCValidoAnterior = validoanterior;
                    
                    if (LinhaEmTeste.RowState == DataRowState.Modified)
                    {
                        dExtrato.SaldoContaCorrenteTableAdapter.Update(LinhaEmTeste);
                        LinhaEmTeste.AcceptChanges();
                    }                    
                }

                if ((LinhaEmTeste.SCCValorApF != 0) || (CHPropagar.Checked))
                {
                    if (SCCrecalc.HasValue && (LinhaEmTeste.SCC == SCCrecalc.Value))
                        propagandoContaoc = true;
                }
                if (!LinhaEmTeste.IsSCCDataNull() && (!LinhaEmTeste.IsSCCValorFNull()))
                {
                    DataA = LinhaEmTeste.SCCData;
                    ValorFA = LinhaEmTeste.SCCValorF;
                    ValorFApA = LinhaEmTeste.SCCValorApF;
                    if (Recalcular)
                        propagandoContaoc = true;
                }
                else
                    DataA = null;
                if ((LinhaEmTeste.IsSCCValidoConteudoNull()) || (!LinhaEmTeste.SCCValidoConteudo))
                    if (!ValidaDia(LinhaEmTeste))
                        ComErro = true;                
            };
            return !ComErro;
        }

        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;

        private bool Calcula()
        {
            bool ComErro = false;
            dExtrato.ContaCorrenteDetalhe.Clear();
            dExtrato.SaldoContaCorrente.Clear();            
            if (SCCrecalc.HasValue)
                dataF.DateTime = DateTime.Today.AddYears(1);
            dExtrato.SaldoContaCorrenteTableAdapter.EmbarcaEmTransST();
            dExtrato.ContaCorrenteDetalheTableAdapter.EmbarcaEmTransST();
            dExtrato.SaldoContaCorrenteTableAdapter.Fill(dExtrato.SaldoContaCorrente, dataI.DateTime, dataF.DateTime, CCT);
            dExtrato.ContaCorrenteDetalheTableAdapter.FillByPeriodo(dExtrato.ContaCorrenteDetalhe, dataI.DateTime, dataF.DateTime, CCT);
            if (!ValidaTodos())
            {
                UltimoErro = new Exception("Extrato com erros");
                ComErro = true;
                if (!Silencioso)
                    MessageBox.Show("Aten��o: Extrato com erros");
                
            }

            ExpandMasters(false);
            if (checkEdit1.Checked)
                ExpandMasters(true);

            return !ComErro;
        }

        private decimal Saldo;

        private void CarregaCheques()
        {            
            dExtrato.CHEquesTableAdapter.Fill(dExtrato.CHEques, CCT);
            foreach (dExtrato.CHEquesRow rowCHE in dExtrato.CHEques)
            {
                Saldo -= rowCHE.CHEValor;
                rowCHE.Saldo = Saldo;
            }
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            if (cBotaoImpBol1.BotaoClicado == dllImpresso.Botoes.Botao.botao)
            {
                Calcula();
                ExpandMasters(checkEdit1.Checked);
            }
            else
            {
                /*
                //TESTE
                dllImpresso.ExtratoBancario.ImpExtratoB ImpressoTESTE = new dllImpresso.ExtratoBancario.ImpExtratoB();
                int carregados = ImpressoTESTE.dImpExtratoB1.DadosExtratoBTableAdapter.Fill(ImpressoTESTE.dImpExtratoB1.DadosExtratoB, dataI.DateTime, dataF.DateTime, CCT);
                if (carregados == 0)
                    return;

                ImpressoTESTE.Nomecondominio.Text = "TESTE";
                ImpressoTESTE.DataInicial.Text = dataI.DateTime.ToString("dd/MM/yyyy");
                ImpressoTESTE.DataFinal.Text = dataF.DateTime.ToString("dd/MM/yyyy");
                ImpressoTESTE.dImpExtratoB1.CalculosDadosExtratoB(true);
                ImpressoTESTE.CreateDocument();
                ImpressoTESTE.Print();
                //TESTE
                */

                
                dllImpresso.ExtratoBancario.ImpExtratoBal Impresso = new dllImpresso.ExtratoBancario.ImpExtratoBal(Titulo.Substring(8),dataI.DateTime, dataF.DateTime, CCT);
                
                if (!Impresso.ContemDados)
                    return;
                

                

                switch (cBotaoImpBol1.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.email:
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Extrato", Impresso, "Extrato em anexo.", "Extrato"))
                            System.Windows.Forms.MessageBox.Show("E.mail enviado");
                        else
                        {
                            if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                                System.Windows.Forms.MessageBox.Show("Falha no envio");
                            else
                                System.Windows.Forms.MessageBox.Show("Cancelado");
                        };
                        break;
                    case dllImpresso.Botoes.Botao.imprimir:
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                        Impresso.Print();
                        break;
                    case dllImpresso.Botoes.Botao.pdf:
                    case dllImpresso.Botoes.Botao.PDF_frente:
                        System.Windows.Forms.SaveFileDialog saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog1.DefaultExt = "pdf";
                        if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            Impresso.ExportOptions.Pdf.Compressed = true;
                            Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                            Impresso.ExportToPdf(saveFileDialog1.FileName);
                        }
                        break;
                    case dllImpresso.Botoes.Botao.tela:
                        Impresso.ShowPreviewDialog();
                        break;
                }
            }
        }

        private int? SCCrecalc;

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
            //if (GV == null)
            //    return;
            //dExtrato.SaldoContaCorrenteRow rowSCC = (dExtrato.SaldoContaCorrenteRow)GV.GetFocusedDataRow();
            dExtrato.SaldoContaCorrenteRow rowSCC = (dExtrato.SaldoContaCorrenteRow)bandedGridView1.GetFocusedDataRow();
            if (rowSCC == null)
                return;

            cExtratoManual cExtratoManual1 = new cExtratoManual(CCT, rowSCC.SCCData, rowSCC.SCC);
            SCCrecalc = null;
            if ((cExtratoManual1.ShowEmPopUp() == DialogResult.OK) && ((TipoConta == CCTTipo.ContaCorrete_com_Oculta) || (rowSCC.SCCValorApF != 0) || (CHPropagar.Checked)))
                SCCrecalc = rowSCC.SCC;
            Calcula();
        }
        

        
        [Obsolete("remover ap�s a corre��o")]
        public void Correcao(DateTime DataCorrecao)
        {
            dExtrato.ContaCorrenteDetalhe.Clear();
            dExtrato.SaldoContaCorrente.Clear();
            //dExtrato.SaldoContaCorrenteTableAdapter.Fill(dExtrato.SaldoContaCorrente, new DateTime(2015 , 7, 1), dataF.DateTime, CCT);            
            //SCCrecalc = dExtrato.SaldoContaCorrente[0].SCC;
            //DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("SELECT top 1 SCC,SCCData FROM SaldoContaCorrente WHERE (SCCData < @P1) AND (SCC_CCT = @P2) ORDER BY SCCData DESC", new DateTime(2015, 8, 18),CCT);
            DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("SELECT top 1 SCC,SCCData FROM SaldoContaCorrente WHERE (SCCData < @P1) AND (SCC_CCT = @P2) ORDER BY SCCData DESC", DataCorrecao, CCT);
            if (DR != null)
            {
                SCCrecalc = (int)DR["SCC"];
                dataI.DateTime = (DateTime)DR["SCCData"];
                dataF.DateTime = DateTime.Today.AddDays(30);
                if(!Calcula())
                    throw new Exception();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DateTime? Data = null;                        
            if(VirInput.Input.Execute("Data:",ref Data,dataI.DateTime,dataF.DateTime))
            {
                cExtratoManual cExtratoManual1 = new cExtratoManual(CCT, Data.Value);
                cExtratoManual1.ShowEmPopUp();
                Calcula(); 
            }
        }

        private void ExpandMasters(bool Exp)
        {
            for (int rowHandle = 0; rowHandle < dExtrato.SaldoContaCorrente.Count; rowHandle++)
                if (Exp)
                    bandedGridView1.ExpandMasterRow(rowHandle);
                else
                    bandedGridView1.CollapseMasterRow(rowHandle);
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            ExpandMasters(checkEdit1.Checked);
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dExtrato.SaldoContaCorrenteRow rowSCC = (dExtrato.SaldoContaCorrenteRow)bandedGridView1.GetDataRow(e.RowHandle);
            if (rowSCC == null)
                return;
            if (rowSCC.IsSCCValidoAnteriorNull() || rowSCC.IsSCCValidoConteudoNull() || !rowSCC.SCCValidoAnterior || !rowSCC.SCCValidoConteudo)
            {
                e.Appearance.BackColor = Color.Red;
                e.Appearance.ForeColor = Color.White;
            }
        }

        
        

        private Font _FonteItacorte;
        
        private Font FonteItacorte(Font prototipo)
        {
            if (_FonteItacorte == null)
                _FonteItacorte = new Font(prototipo, FontStyle.Italic | FontStyle.Bold | FontStyle.Strikeout);
            return _FonteItacorte;
        }

        private void gridView2_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView Grid = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            dExtrato.ContaCorrenteDetalheRow rowCCD = (dExtrato.ContaCorrenteDetalheRow)Grid.GetDataRow(e.RowHandle);
            if (rowCCD == null)
                return;
            if(!AplicacaoSeparada)
                if ((!rowCCD.IsCCDTipoLancamentoNull()) && ((TipoLancamentoNeon)rowCCD.CCDTipoLancamento == TipoLancamentoNeon.TransferenciaInterna))
                {
                    e.Appearance.Font = FonteItacorte(e.Appearance.Font);
                }
        }

        private void chLFuturos_CheckedChanged(object sender, EventArgs e)
        {
            GLancamentosFuturos.Visible = chLFuturos.Checked;
            if (chLFuturos.Visible && (dExtrato.CHEques.Count == 0))
                CarregaCheques();
        }

        /// <summary>
        /// Mostrar lan�amentos futuros
        /// </summary>
        /// <param name="Ligado"></param>
        public void LancamentosFuturos(bool Ligado)
        {
            chLFuturos.Checked = Ligado;
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dExtrato.CHEquesRow row = (dExtrato.CHEquesRow)gridView1.GetFocusedDataRow();
            if (row != null)
            {
                dllCheques.cCheque cChequeX = new dllCheques.cCheque(row.CHE);
                cChequeX.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog SF = new SaveFileDialog();
            SF.DefaultExt = ".xlsx";
            if (SF.ShowDialog() == DialogResult.OK)
            {
                DevExpress.XtraPrinting.XlsxExportOptions op = new DevExpress.XtraPrinting.XlsxExportOptions();
                bandedGridView1.OptionsPrint.ExpandAllDetails = true;
                bandedGridView1.OptionsPrint.PrintDetails = true;
                bandedGridView1.ExportToXlsx(SF.FileName);
            }
        }
        
    }
}
