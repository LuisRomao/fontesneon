﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace dllbanco.Extrato
{
    /// <summary>
    /// ExtratoArquivoItau
    /// </summary>
    public class ExtratoArquivoItau
    {
        private int CCT;
        private int CCToc;
        //private dExtratoArquivoItau dExtratoArquivoItau1;
        private decimal SaldoAnteriro;
        private DateTime DataAnterior;
        //private DateTime DataAtual;

        /// <summary>
        /// erro
        /// </summary>
        public string erro;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CCT"></param>
        /// <param name="CCToc"></param>
        public ExtratoArquivoItau(int CCT, int CCToc)
        {
            this.CCT = CCT;
            this.CCToc = CCToc;
        }

        private int? ano;

        private DateTime ConverteData(string strData)
        {
            if (ano == null)
                ano = DateTime.Today.Year;
            DateTime retorno = new DateTime(ano.Value, int.Parse(strData.Substring(3, 2)), int.Parse(strData.Substring(0, 2)));
            if (retorno > DateTime.Today)
                while ((retorno - DateTime.Today).TotalDays > 365)
                    retorno.AddYears(-1);
            else
                while ((DateTime.Today - retorno).TotalDays > 365)
                    retorno.AddYears(1);
            return retorno;
        }

        enum faseImporta 
        {
            saldoAnterior,
            primeiroLancamentodia,
            dentrododia,
            saldoFechado,
            saldoAberto,
            terminado
        }

        /// <summary>
        /// Importar
        /// </summary>
        /// <returns></returns>
        public bool Importar()
        {
            erro = "";
            VirInput.Importador.cImportador Importador = new VirInput.Importador.cImportador("EXTRATOITAU");
            //dExtratoArquivoItau dExtratoArquivoItau1 = new dExtratoArquivoItau();
            //Importador._DataTable = dExtratoArquivoItau1.Entrada;
            Importador.AddColunaImp("data", "Data", typeof(string), true, 0);
            Importador.AddColunaImp("Lancamento", "Lançamento", typeof(string), true, 3);
            Importador.AddColunaImp("Valor", "Valor", typeof(decimal), false, 4);
            Importador.AddColunaImp("Saldo", "Saldo", typeof(decimal), false, 5);
            VirInput.Importador.cImportador.Validacao_decimal = VirInput.Importador.cImportador.TratamentoErro.TratarBrancocomoNulo;
            Importador.Carrega();
            if (Importador.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                dExtratoArquivoItau dExtratoArquivoItau1 = new dExtratoArquivoItau();
                foreach (System.Data.DataRow DR in Importador._DataTable.Rows)
                {
                    if ((bool)DR["OK"])
                    {
                        dExtratoArquivoItau.EntradaRow novarow = dExtratoArquivoItau1.Entrada.NewEntradaRow();                        
                        novarow.Data = ConverteData((string)DR["data"]);
                        novarow.Lancamento = (string)DR["Lancamento"];
                        if (DR["Valor"] != DBNull.Value)
                            novarow.Valor = (decimal)DR["Valor"];
                        if (DR["Saldo"] != DBNull.Value)
                            novarow.Saldo = (decimal)DR["Saldo"];
                        dExtratoArquivoItau1.Entrada.AddEntradaRow(novarow);
                    }
                }
  
                if (dExtratoArquivoItau1.Entrada.Count < 3)
                {
                    erro = "Dados não encontrados";
                    return false;
                };


                if ((dExtratoArquivoItau1.Entrada[0].Lancamento.ToUpper().Trim() != "SALDO ANTERIOR") || (dExtratoArquivoItau1.Entrada[0].IsSaldoNull()))
                {
                    erro = "O extrato deve iniciar com \"SALDO ANTERIOR\"";
                    return false;
                }
                faseImporta fase = faseImporta.saldoAnterior;                
                int linha = 0;
                DateTime? DataAtual = null;
                diaExtrato diaExtrato1 = null;
                while (fase != faseImporta.terminado)
                {
                    switch (fase)
                    {
                        case faseImporta.saldoAnterior:
                            SaldoAnteriro = dExtratoArquivoItau1.Entrada[linha].Saldo;
                            DataAnterior = dExtratoArquivoItau1.Entrada[linha].Data;
                            break;
                        case faseImporta.primeiroLancamentodia:
                            diaExtrato1 = new diaExtrato(CCT, DataAtual.Value);
                            if (!diaExtrato1.encontrado)
                            {
                                erro = string.Format("Arquivo retorno referênte ao dia {0} não carregado\r\nCarregar o arquivo ou usar carga manual", DataAtual);
                                return false;
                            }
                            else
                            {
                                //diaExtrato1.LigarAoDiaAnterior();
                                diaExtrato1.AjustaValorInternoInicial(SaldoAnteriro);
                            }
                            break;
                        case faseImporta.dentrododia:
                            if (!diaExtrato1.ValidaLinha(dExtratoArquivoItau1.Entrada[linha].Lancamento, dExtratoArquivoItau1.Entrada[linha].Valor))
                            {
                                erro = string.Format("Erro na valicação da linha\r\nData: {0}\r\nLinha inválida: {1}\r\nValor: {2:n2}\r\nCarregar o arquivo ou usar carga manual", DataAtual, dExtratoArquivoItau1.Entrada[linha].Lancamento, dExtratoArquivoItau1.Entrada[linha].Valor);
                                return false;
                            }
                            break;
                        case faseImporta.saldoFechado:
                            if (!diaExtrato1.FechaDia(dExtratoArquivoItau1.Entrada[linha].Saldo))
                            {
                                erro = string.Format("Lançamento do dia {0} não conferem\r\nCarregar o arquivo ou usar carga manual", DataAtual);
                                return false;
                            }
                            else
                            {
                                //diaExtrato1.registrajuros();
                                SaldoAnteriro = dExtratoArquivoItau1.Entrada[linha].Saldo;
                                DataAnterior = dExtratoArquivoItau1.Entrada[linha].Data;
                                while (linha + 1 < dExtratoArquivoItau1.Entrada.Count) 
                                {                                    
                                    if (!dExtratoArquivoItau1.Entrada[linha + 1].IsValorNull())
                                        break;
                                    linha++;
                                }
                                
                            }
                            break;
                        case faseImporta.saldoAberto:
                            fase = faseImporta.terminado;
                            break;
                    }
                    if (fase == faseImporta.primeiroLancamentodia)
                        fase = faseImporta.dentrododia;
                    else
                    {
                        linha++;
                        if (linha >= dExtratoArquivoItau1.Entrada.Count)
                        {
                            erro = "O extrato deve Terminar com \"S A L D O\"";
                            return false;
                        }
                        DataAtual = dExtratoArquivoItau1.Entrada[linha].Data;
                        if (dExtratoArquivoItau1.Entrada[linha].IsValorNull())
                        {
                            if (dExtratoArquivoItau1.Entrada[linha].Lancamento.Trim().ToUpper() == "SDO CTA/APL AUTOMATICAS")
                                fase = faseImporta.saldoFechado;
                            else if (dExtratoArquivoItau1.Entrada[linha].Lancamento.Trim().ToUpper() == "S A L D O")
                                fase = faseImporta.saldoAberto;
                        }
                        else
                            if (fase != faseImporta.dentrododia)
                                fase = faseImporta.primeiroLancamentodia;
                    }
                }                
                return true;
            }
            return false;
        }
    }
}
