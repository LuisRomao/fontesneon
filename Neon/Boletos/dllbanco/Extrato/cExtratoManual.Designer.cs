﻿namespace dllbanco.Extrato
{
    partial class cExtratoManual
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cExtratoManual));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.dateEditMOV = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditANT = new DevExpress.XtraEditors.DateEdit();
            this.calcEditANT = new DevExpress.XtraEditors.CalcEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.BotEditar = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelMais = new DevExpress.XtraEditors.LabelControl();
            this.calcEditFINcc = new DevExpress.XtraEditors.CalcEdit();
            this.calcEditANTcc = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.ComboCCTTipo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.labelControloc = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditFINoc = new DevExpress.XtraEditors.CalcEdit();
            this.calcEditANToc = new DevExpress.XtraEditors.CalcEdit();
            this.calcEditFIN = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dExtratoBindingSource = new System.Windows.Forms.BindingSource();
            this.dExtrato = new dllbanco.Extrato.dExtrato();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCCDValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colCCDDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDCategoria = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCDCodHistorico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDValidado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCD_SCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDConsolidado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCDTipoLancamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCDDescComplemento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditMOV.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditMOV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditANT.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditANT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditANT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditFINcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditANTcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboCCTTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditFINoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditANToc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditFIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtratoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtrato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(18, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(195, 23);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Data do movimento:";
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Enabled = false;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(16, 13);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(308, 20);
            this.lookupCondominio_F1.somenteleitura = true;
            this.lookupCondominio_F1.TabIndex = 1;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            // 
            // dateEditMOV
            // 
            this.dateEditMOV.EditValue = null;
            this.dateEditMOV.Location = new System.Drawing.Point(218, 39);
            this.dateEditMOV.Name = "dateEditMOV";
            this.dateEditMOV.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditMOV.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.dateEditMOV.Properties.Appearance.Options.UseFont = true;
            this.dateEditMOV.Properties.Appearance.Options.UseForeColor = true;
            this.dateEditMOV.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditMOV.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditMOV.Properties.ReadOnly = true;
            this.dateEditMOV.Size = new System.Drawing.Size(193, 40);
            this.dateEditMOV.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(447, 42);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(144, 23);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Saldo Anterior:";
            // 
            // dateEditANT
            // 
            this.dateEditANT.EditValue = null;
            this.dateEditANT.Location = new System.Drawing.Point(593, 39);
            this.dateEditANT.Name = "dateEditANT";
            this.dateEditANT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditANT.Properties.Appearance.Options.UseFont = true;
            this.dateEditANT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditANT.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditANT.Size = new System.Drawing.Size(161, 30);
            this.dateEditANT.TabIndex = 4;
            this.dateEditANT.EditValueChanged += new System.EventHandler(this.dateEditANT_EditValueChanged);
            // 
            // calcEditANT
            // 
            this.calcEditANT.Location = new System.Drawing.Point(1074, 39);
            this.calcEditANT.Name = "calcEditANT";
            this.calcEditANT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditANT.Properties.Appearance.Options.UseFont = true;
            this.calcEditANT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditANT.Properties.Mask.EditMask = "n2";
            this.calcEditANT.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEditANT.Size = new System.Drawing.Size(151, 30);
            this.calcEditANT.TabIndex = 5;
            this.calcEditANT.EditValueChanged += new System.EventHandler(this.calcEditANT_EditValueChanged);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.BotEditar);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelMais);
            this.panelControl1.Controls.Add(this.calcEditFINcc);
            this.panelControl1.Controls.Add(this.calcEditANTcc);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.ComboCCTTipo);
            this.panelControl1.Controls.Add(this.labelControloc);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.calcEditFINoc);
            this.panelControl1.Controls.Add(this.calcEditANToc);
            this.panelControl1.Controls.Add(this.calcEditFIN);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.dateEditMOV);
            this.panelControl1.Controls.Add(this.calcEditANT);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.dateEditANT);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1558, 151);
            this.panelControl1.TabIndex = 6;
            // 
            // BotEditar
            // 
            this.BotEditar.Location = new System.Drawing.Point(760, 111);
            this.BotEditar.Name = "BotEditar";
            this.BotEditar.Size = new System.Drawing.Size(151, 27);
            this.BotEditar.TabIndex = 19;
            this.BotEditar.Text = "Editar Saldo CC";
            this.BotEditar.Visible = false;
            this.BotEditar.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(1063, 10);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(16, 23);
            this.labelControl7.TabIndex = 18;
            this.labelControl7.Text = "=";
            // 
            // labelMais
            // 
            this.labelMais.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMais.Appearance.Options.UseFont = true;
            this.labelMais.Location = new System.Drawing.Point(906, 10);
            this.labelMais.Name = "labelMais";
            this.labelMais.Size = new System.Drawing.Size(16, 23);
            this.labelMais.TabIndex = 17;
            this.labelMais.Text = "+";
            // 
            // calcEditFINcc
            // 
            this.calcEditFINcc.Location = new System.Drawing.Point(760, 75);
            this.calcEditFINcc.Name = "calcEditFINcc";
            this.calcEditFINcc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditFINcc.Properties.Appearance.Options.UseFont = true;
            this.calcEditFINcc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions1)});
            this.calcEditFINcc.Properties.Mask.EditMask = "n2";
            this.calcEditFINcc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEditFINcc.Properties.ReadOnly = true;
            this.calcEditFINcc.Size = new System.Drawing.Size(151, 30);
            this.calcEditFINcc.TabIndex = 16;
            // 
            // calcEditANTcc
            // 
            this.calcEditANTcc.Enabled = false;
            this.calcEditANTcc.Location = new System.Drawing.Point(760, 39);
            this.calcEditANTcc.Name = "calcEditANTcc";
            this.calcEditANTcc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditANTcc.Properties.Appearance.Options.UseFont = true;
            this.calcEditANTcc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditANTcc.Properties.Mask.EditMask = "n2";
            this.calcEditANTcc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEditANTcc.Size = new System.Drawing.Size(151, 30);
            this.calcEditANTcc.TabIndex = 15;
            this.calcEditANTcc.EditValueChanged += new System.EventHandler(this.calcEditANTcc_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(1125, 10);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(49, 23);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Total";
            // 
            // ComboCCTTipo
            // 
            this.ComboCCTTipo.Location = new System.Drawing.Point(330, 13);
            this.ComboCCTTipo.MenuManager = this.BarManager_F;
            this.ComboCCTTipo.Name = "ComboCCTTipo";
            this.ComboCCTTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboCCTTipo.Size = new System.Drawing.Size(228, 20);
            this.ComboCCTTipo.TabIndex = 13;
            // 
            // labelControloc
            // 
            this.labelControloc.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControloc.Appearance.Options.UseFont = true;
            this.labelControloc.Location = new System.Drawing.Point(948, 10);
            this.labelControloc.Name = "labelControloc";
            this.labelControloc.Size = new System.Drawing.Size(82, 23);
            this.labelControloc.TabIndex = 12;
            this.labelControloc.Text = "Aplicado";
            this.labelControloc.Visible = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(800, 10);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(85, 23);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Saldo CC";
            // 
            // calcEditFINoc
            // 
            this.calcEditFINoc.Location = new System.Drawing.Point(917, 75);
            this.calcEditFINoc.Name = "calcEditFINoc";
            this.calcEditFINoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditFINoc.Properties.Appearance.Options.UseFont = true;
            this.calcEditFINoc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions2)});
            this.calcEditFINoc.Properties.Mask.EditMask = "n2";
            this.calcEditFINoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEditFINoc.Properties.ReadOnly = true;
            this.calcEditFINoc.Size = new System.Drawing.Size(151, 30);
            this.calcEditFINoc.TabIndex = 10;
            this.calcEditFINoc.Visible = false;
            // 
            // calcEditANToc
            // 
            this.calcEditANToc.Location = new System.Drawing.Point(917, 39);
            this.calcEditANToc.Name = "calcEditANToc";
            this.calcEditANToc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditANToc.Properties.Appearance.Options.UseFont = true;
            this.calcEditANToc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditANToc.Properties.Mask.EditMask = "n2";
            this.calcEditANToc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEditANToc.Size = new System.Drawing.Size(151, 30);
            this.calcEditANToc.TabIndex = 9;
            this.calcEditANToc.Visible = false;
            this.calcEditANToc.EditValueChanged += new System.EventHandler(this.calcEditANToc_EditValueChanged);
            // 
            // calcEditFIN
            // 
            this.calcEditFIN.Location = new System.Drawing.Point(1074, 75);
            this.calcEditFIN.Name = "calcEditFIN";
            this.calcEditFIN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditFIN.Properties.Appearance.Options.UseFont = true;
            this.calcEditFIN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions3)});
            this.calcEditFIN.Properties.Mask.EditMask = "n2";
            this.calcEditFIN.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEditFIN.Properties.ReadOnly = true;
            this.calcEditFIN.Size = new System.Drawing.Size(151, 30);
            this.calcEditFIN.TabIndex = 8;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(447, 77);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(112, 23);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Saldo Final:";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "ContaCorrenteDetalhe";
            this.gridControl1.DataSource = this.dExtratoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 186);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemButtonEdit1,
            this.repositoryItemImageComboBox1,
            this.repositoryItemImageComboBox2,
            this.repositoryItemImageComboBox3,
            this.repositoryItemImageComboBox4});
            this.gridControl1.Size = new System.Drawing.Size(1558, 621);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dExtratoBindingSource
            // 
            this.dExtratoBindingSource.DataSource = this.dExtrato;
            this.dExtratoBindingSource.Position = 0;
            // 
            // dExtrato
            // 
            this.dExtrato.DataSetName = "dExtrato";
            this.dExtrato.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseBorderColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCCDValor,
            this.colCCDDescricao,
            this.colCCDCategoria,
            this.colCCDCodHistorico,
            this.colCCDDocumento,
            this.colCCDValidado,
            this.colCCD_SCC,
            this.colCCD,
            this.colCCDConsolidado,
            this.colCCDTipoLancamento,
            this.colCCDDescComplemento,
            this.colSaldo,
            this.gridColumn2,
            this.gridColumn1,
            this.colSaldoCC});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCCD, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colCCDValor
            // 
            this.colCCDValor.Caption = "Valor";
            this.colCCDValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colCCDValor.FieldName = "CCDValor";
            this.colCCDValor.Name = "colCCDValor";
            this.colCCDValor.Visible = true;
            this.colCCDValor.VisibleIndex = 5;
            this.colCCDValor.Width = 130;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colCCDDescricao
            // 
            this.colCCDDescricao.Caption = "Descrição";
            this.colCCDDescricao.FieldName = "CCDDescricao";
            this.colCCDDescricao.Name = "colCCDDescricao";
            this.colCCDDescricao.Visible = true;
            this.colCCDDescricao.VisibleIndex = 0;
            this.colCCDDescricao.Width = 263;
            // 
            // colCCDCategoria
            // 
            this.colCCDCategoria.Caption = "Categoria";
            this.colCCDCategoria.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colCCDCategoria.FieldName = "CCDCategoria";
            this.colCCDCategoria.Name = "colCCDCategoria";
            this.colCCDCategoria.Visible = true;
            this.colCCDCategoria.VisibleIndex = 4;
            this.colCCDCategoria.Width = 92;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colCCDCodHistorico
            // 
            this.colCCDCodHistorico.Caption = "Histórico";
            this.colCCDCodHistorico.FieldName = "CCDCodHistorico";
            this.colCCDCodHistorico.Name = "colCCDCodHistorico";
            this.colCCDCodHistorico.Visible = true;
            this.colCCDCodHistorico.VisibleIndex = 2;
            this.colCCDCodHistorico.Width = 94;
            // 
            // colCCDDocumento
            // 
            this.colCCDDocumento.FieldName = "CCDDocumento";
            this.colCCDDocumento.Name = "colCCDDocumento";
            this.colCCDDocumento.Width = 100;
            // 
            // colCCDValidado
            // 
            this.colCCDValidado.FieldName = "CCDValidado";
            this.colCCDValidado.Name = "colCCDValidado";
            // 
            // colCCD_SCC
            // 
            this.colCCD_SCC.FieldName = "CCD_SCC";
            this.colCCD_SCC.Name = "colCCD_SCC";
            // 
            // colCCD
            // 
            this.colCCD.FieldName = "CCD";
            this.colCCD.Name = "colCCD";
            this.colCCD.OptionsColumn.ReadOnly = true;
            // 
            // colCCDConsolidado
            // 
            this.colCCDConsolidado.Caption = "Conciliação";
            this.colCCDConsolidado.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colCCDConsolidado.FieldName = "CCDConsolidado";
            this.colCCDConsolidado.Name = "colCCDConsolidado";
            this.colCCDConsolidado.OptionsColumn.ReadOnly = true;
            this.colCCDConsolidado.Visible = true;
            this.colCCDConsolidado.VisibleIndex = 8;
            this.colCCDConsolidado.Width = 139;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colCCDTipoLancamento
            // 
            this.colCCDTipoLancamento.Caption = "Tipo de Lançamento";
            this.colCCDTipoLancamento.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCCDTipoLancamento.FieldName = "CCDTipoLancamento";
            this.colCCDTipoLancamento.Name = "colCCDTipoLancamento";
            this.colCCDTipoLancamento.Visible = true;
            this.colCCDTipoLancamento.VisibleIndex = 3;
            this.colCCDTipoLancamento.Width = 165;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colCCDDescComplemento
            // 
            this.colCCDDescComplemento.Caption = "Complemento";
            this.colCCDDescComplemento.FieldName = "CCDDescComplemento";
            this.colCCDDescComplemento.Name = "colCCDDescComplemento";
            this.colCCDDescComplemento.Visible = true;
            this.colCCDDescComplemento.VisibleIndex = 1;
            this.colCCDDescComplemento.Width = 226;
            // 
            // colSaldo
            // 
            this.colSaldo.Caption = "Saldo Total";
            this.colSaldo.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colSaldo.FieldName = "Saldo";
            this.colSaldo.Name = "colSaldo";
            this.colSaldo.OptionsColumn.ReadOnly = true;
            this.colSaldo.Visible = true;
            this.colSaldo.VisibleIndex = 7;
            this.colSaldo.Width = 130;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "DEL";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 9;
            this.gridColumn2.Width = 25;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Código Categoria";
            this.gridColumn1.FieldName = "CCDCategoria";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // colSaldoCC
            // 
            this.colSaldoCC.Caption = "Saldo C.C.";
            this.colSaldoCC.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colSaldoCC.FieldName = "SaldoCC";
            this.colSaldoCC.Name = "colSaldoCC";
            this.colSaldoCC.Visible = true;
            this.colSaldoCC.VisibleIndex = 6;
            this.colSaldoCC.Width = 103;
            // 
            // repositoryItemImageComboBox4
            // 
            this.repositoryItemImageComboBox4.AutoHeight = false;
            this.repositoryItemImageComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox4.Name = "repositoryItemImageComboBox4";
            // 
            // cExtratoManual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cExtratoManual";
            this.Size = new System.Drawing.Size(1558, 807);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditMOV.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditMOV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditANT.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditANT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditANT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditFINcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditANTcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboCCTTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditFINoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditANToc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditFIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtratoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtrato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.DateEdit dateEditMOV;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateEditANT;
        private DevExpress.XtraEditors.CalcEdit calcEditANT;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource dExtratoBindingSource;
        private dExtrato dExtrato;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDCategoria;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDCodHistorico;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDValidado;
        private DevExpress.XtraGrid.Columns.GridColumn colCCD_SCC;
        private DevExpress.XtraGrid.Columns.GridColumn colCCD;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDConsolidado;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDTipoLancamento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDescComplemento;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.CalcEdit calcEditFIN;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.LabelControl labelControloc;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CalcEdit calcEditFINoc;
        private DevExpress.XtraEditors.CalcEdit calcEditANToc;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboCCTTipo;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelMais;
        private DevExpress.XtraEditors.CalcEdit calcEditFINcc;
        private DevExpress.XtraEditors.CalcEdit calcEditANTcc;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton BotEditar;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox4;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoCC;
    }
}
