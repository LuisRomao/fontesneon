using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Boletos.Boleto;
using CompontesBasicosProc;
using CompontesBasicos;
using VirEnumeracoesNeon;

namespace dllbanco
{
    /// <summary>
    /// Tela para baixa da francesinha
    /// </summary>
    public partial class cFrancesinha : CompontesBasicos.ComponenteGradeNavegador
    {
        /*0,1,10,11*/

        private dllbanco.Francesinha Fr;

        /// <summary>
        /// Construtor
        /// </summary>
        public cFrancesinha()
        {
            InitializeComponent();
            Framework.Enumeracoes.virEnumARLStatusN.virEnumARLStatusNSt.CarregaEditorDaGrid(colARLStatusN);
            if (DesignMode == false)
            {
                try
                {
                    cONDOMINIOSDataTableBindingSource.DataSource = Framework.Buscas.dBuscas.EdBuscas.CONDOMINIOS;
                    chavesAPTBindingSource.DataSource = Framework.Buscas.dBuscas.EdBuscas;
                    Anteriores();
                    AjustaTela();                    
                }
                catch (Exception e){
                    MessageBox.Show(e.Message);
                };
            };
            if (FormPrincipalBase.USULogado == 30)
                BotTeste.Visible = BotTeste2.Visible = true;
        }

        private void Anteriores()
        {
            Fr = new dllbanco.Francesinha();
            Fr.DivideSuperBoleto();
            Fr.Carregar();
            GridControl_F.DataSource = Fr.dFrancesinha1.ARquivoLido;            
        }
        /*
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Fr = new dllbanco.Francesinha();
            Fr.Carregar();
            GridControl_F.DataSource = Fr.dFrancesinha1.ARquivoLido;
            identificacao.Text = "Todos";
        }*/

        private void bandedGridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dllbanco.dFrancesinha.ARquivoLidoRow Linha = (dllbanco.dFrancesinha.ARquivoLidoRow)bandedGridView1.GetDataRow(e.RowHandle);
            if (Linha != null)
            {
                if (!Linha.IsARLStatusNNull())
                {
                    e.Appearance.BackColor = Framework.Enumeracoes.virEnumARLStatusN.virEnumARLStatusNSt.GetCor((ARLStatusN)Linha.ARLStatusN);
                    e.Appearance.ForeColor = e.Appearance.BackColor.EstaNoGrupo(Color.Red, Color.Blue, Color.Green) ? Color.White : Color.Black;
                    /*
                    switch ((ARLStatusN)Linha.ARLStatusN) 
                    {
                        case ARLStatusN.Baixado:  //baixado
                        case ARLStatusN.baixadoMultaAgendada: //baixado - Multa agendada
                        case ARLStatusN.baixadoComMulta: //baixado - com multa
                            e.Appearance.ForeColor = Color.Black;
                            e.Appearance.BackColor = Color.Aqua;
                            break;
                        case ARLStatusN.CondominioInativo: // outra filial
                        case ARLStatusN.CanceladoManualmente: // cancelado manualmente
                            e.Appearance.ForeColor = Color.Black;
                            e.Appearance.BackColor = Color.Yellow;
                            break;
                        case ARLStatusN.ContaIncorreta: // conta errada
                            e.Appearance.ForeColor = Color.White;
                            e.Appearance.BackColor = Color.Red;
                            break;
                        case ARLStatusN.ValorIncorreto: // Valor incorreto
                            e.Appearance.ForeColor = Color.Black;
                            e.Appearance.BackColor = Color.FromArgb(254, 200, 200);
                            break;
                        case ARLStatusN.ErroNoCalculoDoBoleto: // Erro no boleto
                            e.Appearance.ForeColor = Color.White;
                            e.Appearance.BackColor = Color.Red;
                            break;
                        case ARLStatusN.Duplicidade: // Duplicidade
                            e.Appearance.ForeColor = Color.White;
                            e.Appearance.BackColor = Color.Blue;
                            break;
                        case ARLStatusN.BoletoNaoEncontrado: // Boleto n�o encontrado
                        case ARLStatusN.ContaNaoEncontrada: // Conta n�o encontrada
                        case ARLStatusN.ErronaBaixa: // Erro na baixa
                            e.Appearance.ForeColor = Color.White;
                            e.Appearance.BackColor = Color.Red;
                            break;
                        case ARLStatusN.BoletoCancelado:
                            e.Appearance.ForeColor = Color.Black;
                            e.Appearance.BackColor = Color.Wheat;
                            break;

                    }*/
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                Fr = new dllbanco.Francesinha();


                if (Fr.Carregar(openFileDialog1.FileName))
                {
                    GridControl_F.DataSource = Fr.dFrancesinha1.ARquivoLido;
                    //aRQuivosDataTableBindingSource.DataSource = Fr.dFrancesinha1.ARQuivos;
                    //identificacao.Text = Fr.Identificacao;
                    radioGroup1.EditValue = 1;

                }
                else
                    MessageBox.Show("Falha:" + Fr.Erro);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Fr.DivideSuperBoleto();
            Fr.Carregar();
            if (Fr == null)
                return;
            Fr.LocalVisivel = this;            
            string Relatorio = Fr.ProcessarDados(Francesinha.TipoRetorno.resumo);
            if (Fr.Erro == null)
            {                
                MessageBox.Show(Relatorio);
                if (Framework.DSCentral.USU == 30)
                    Clipboard.SetText(Relatorio);
            }
            else
                MessageBox.Show("Falha:" + Fr.Erro + "\r\n");
            
            
            radioGroup1.EditValue = 2;
            //Form2 Res = new Form2();
            //Res.memoEdit1.Text = Fr.LOG;
            //Res.ShowDialog();
            panelB.Enabled = true;
        }        

        private void AjustaTela() {
            int Tipo = (int)radioGroup1.EditValue;
            // remover do analitico
            colARL_ARQ.Visible = BandConta.Visible = BandBanco.Visible = BandAg.Visible =
                colARLNossoNumero.Visible = colARL_CONDigitoConta.Visible = colARL_CONDigitoAgencia.Visible = colARL_CONConta.Visible = colARL_BCO.Visible = colARL_CONAgencia.Visible = ((Tipo == 0) || (Tipo == 1));
            // remover do arquivo
            BandBoleto.Visible = BandEncontrado.Visible =
            colARLMulta.Visible = colARLjuros.Visible = colARLCorrecao.Visible = colARLCON.Visible = colARLcErro.Visible = ((Tipo == 0) || (Tipo == 2));
            // remover do analitico e do arquivo
            colARLValor.Visible = colARLTar.Visible = (Tipo == 0);
        }

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            AjustaTela();            
        }



        dFrancesinha.ARquivoLidoRow LinhaMae {
            get 
            {
                if (LinhaMae_F == null)
                {
                    DataRowView DRV = (DataRowView)bandedGridView1.GetRow(bandedGridView1.FocusedRowHandle);
                    if (DRV == null)
                        return null;
                    else
                        return (dFrancesinha.ARquivoLidoRow)DRV.Row;
                }
                else
                    return (dFrancesinha.ARquivoLidoRow)LinhaMae_F;
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            bool NumeroBOLIncorreto = false;
            if (LinhaMae != null)
            {
                bool Liberado = false;
                ARLStatusN Status = (ARLStatusN)LinhaMae.ARLStatusN;
                switch (Status)
                {
                    case ARLStatusN.BoletoCancelado:
                    case ARLStatusN.Duplicidade:
                    case ARLStatusN.ValorIncorreto:
                        Liberado = true;
                        break;
                    case ARLStatusN.ContaIncorreta:
                        string Comando1 = 
"SELECT CCT_CON, CCTAtiva\r\n" + 
"FROM     ContaCorrenTe\r\n" + 
"WHERE  (CCT_BCO = @P1) AND (CCTAgencia = @P2) AND (CCTConta = @P3) AND (CCTAtiva = 1);";
                        string Comando2 =
"SELECT CCT_CON, CCTAtiva\r\n" +
"FROM     ContaCorrenTe\r\n" +
"WHERE  (CCT_BCO = @P1) AND (CCTAgencia = @P2) AND (CCTConta = @P3);";
                        int? CON = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(Comando1, LinhaMae.ARL_BCO, LinhaMae.ARL_CONAgencia, LinhaMae.ARL_CONConta);
                        if(!CON.HasValue)
                            CON = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(Comando2, LinhaMae.ARL_BCO, LinhaMae.ARL_CONAgencia, LinhaMae.ARL_CONConta);
                        if (CON.HasValue)
                        {
                            Framework.Lookup.LookupBloAptDialog Buscador = new Framework.Lookup.LookupBloAptDialog(CON.Value);
                            if (Buscador.ShowEmPopUp() == DialogResult.OK)
                            {
                                LinhaMae.ARL_APT = Buscador.APT_Sel.Value;
                                LinhaMae.ARL_CON = CON.Value;
                                NumeroBOLIncorreto = true;
                                Liberado = true;
                            }
                        }
                        break;
                    case ARLStatusN.BoletoNaoEncontrado:
                    case ARLStatusN.SuperBoletoVencido:
                        if (!LinhaMae.IsARL_CONNull())
                        {
                            if (LinhaMae.IsARL_APTNull())
                            {
                                Framework.Lookup.LookupBloAptDialog Buscador = new Framework.Lookup.LookupBloAptDialog(LinhaMae.ARL_CON);
                                if (Buscador.ShowEmPopUp() == DialogResult.OK)
                                {
                                    LinhaMae.ARL_APT = Buscador.APT_Sel.Value;
                                    Liberado = true;
                                }
                            }
                            else
                                Liberado = true;
                        }
                        break;
                }
                
                if (Liberado)
                {
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("cFrancesinha.cs - 201", Fr.dFrancesinha1.ARquivoLidoTableAdapter);

                        if (NumeroBOLIncorreto)
                            Fr.RegistraBOLExtra(LinhaMae, LinhaMae.ARL_CON, LinhaMae.ARL_APT, true, new Framework.objetosNeon.Competencia(), LinhaMae.ARL_BOL, "", Francesinha.TipoExtra.NaoEncontrado);
                        else
                        {
                            Boleto BoletoRef = new Boleto(LinhaMae.ARL_BOL);
                            if (BoletoRef.Encontrado)
                                Fr.RegistraBOLExtra(LinhaMae, BoletoRef, Francesinha.TipoExtra.Cancelado);
                            else
                                Fr.RegistraBOLExtra(LinhaMae, LinhaMae.ARL_CON, LinhaMae.ARL_APT, true, new Framework.objetosNeon.Competencia(), LinhaMae.ARL_BOL, "", Francesinha.TipoExtra.NaoEncontrado);
                        }
                        LinhaMae.ARLStatusN = (int)ARLStatusN.baixadoComMulta;
                        Fr.dFrancesinha1.ARquivoLidoTableAdapter.Update(LinhaMae);
                        LinhaMae.AcceptChanges();
                        VirMSSQL.TableAdapter.CommitSQL();
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(ex);
                    }
                }
            }
        }

        /*
        private void JogarDiferencaParaProximoMes() {
            return;
            //string comandoIncBOD = "INSERT INTO BOletoDetalhe (BOD_PLA, BODValor, BODMensagem, BOD_APT, BODProprietario, BODCompetencia, BODComCondominio,BODData,BOD_CON) VALUES\r\n"+
            //    "('120001',@P1, @P2,@P3,@P4, @P5, 1,@P6,@P7)";
            //object[] Parametros = new object[7];
            Boletos.Boleto.Boleto BolAtual = new Boletos.Boleto.Boleto(LinhaMae.ARL_BOL);
            
            if(!BolAtual.Encontrado){
                MessageBox.Show("Boleto n�o encontrado");
                return;
            };
            Framework.objetosNeon.Competencia comp = Framework.objetosNeon.Competencia.Proxima(LinhaMae.ARL_CON);// BolAtual.Competencia.CloneCompet();
            
            comp.Add(1);
            //////////////////// ajustar a data de referencia
            //////////////////// e eventual valor
            decimal ValorDif = LinhaMae.ARLcErro;
            if (!VirInput.Input.Execute("Valor a cobrar", ref ValorDif))
                return;
            
            if (Fr.BaixaUm(LinhaMae, true))
            {
                LinhaMae.ARLStatusN = (int)ARLStatusN.baixadoMultaAgendada;
                Fr.dFrancesinha1.ARquivoLidoTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
                Boletos.Boleto.Boleto.GerarBOD(BolAtual.rowPrincipal.BOL_CON, BolAtual.rowPrincipal.BOL_APT, BolAtual.rowPrincipal.BOLProprietario, "120001", true, comp,
                                               comp.CalculaVencimento(),
                                               string.Format("Diferen�a refer�nte ao vencimento {0:dd/MM/yyyy}", BolAtual.rowPrincipal.BOLVencto),
                                               ValorDif, true);                
            }
            else {
                MessageBox.Show("Erro na baixa");
            }
        }
        */
        
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if ((LinhaMae.ARLStatusN == 3) || (LinhaMae.ARLStatusN == 2))
                CancelarDiferenca();
        }
        
        
        private void CancelarDiferenca() {

            return;
            /*
            if (LinhaMae.ARL_BOLValorPago < LinhaMae.ARLValorOriginal) {
                if(MessageBox.Show("ATEN��O: Valor pago inferior ao original\r\nContinuar","ATEN��O",MessageBoxButtons.YesNo) == DialogResult.No)
                    return;
            };
            ////////Verificar se a diferenca � s� os juros
            if (Fr.BaixaUm(LinhaMae, true))
            {
                LinhaMae.ARLStatusN = (int)ARLStatusN.baixadoComMulta;
                Fr.dFrancesinha1.ARquivoLidoTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();                
            }
            else
            {
                MessageBox.Show("Erro na baixa");
            }*/
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            
        }

        

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma", "Confirma��o", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                LinhaMae.ARLStatusN = (int)ARLStatusN.CanceladoManualmente;
                LinhaMae.ARLSTATUS += "|Cancelado manualmente";
                Fr.dFrancesinha1.ARquivoLidoTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
            }
        }

        private void cFrancesinha_cargaFinal(object sender, EventArgs e)
        {
            BtnImprimir_F.Enabled = true;
        }

        private void cFrancesinha_Load(object sender, EventArgs e)
        {
            BtnImprimir_F.Enabled = true;
        }

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            ChecaForm CF = new ChecaForm();
            CF.ShowDialog();
        }*/

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if ((!LinhaMae.IsARL_BCONull()) && (LinhaMae.ARL_BOL > 0)) {
                Boletos.Boleto.Boleto Bol = new Boletos.Boleto.Boleto(LinhaMae.ARL_BOL);
                if (Bol.Encontrado)
                {
                    Boletos.Boleto.cBoleto cBoleto = new cBoleto(Bol.rowPrincipal,false);
                    cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
                }
            }
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            InclusaoManual();            
        }

        private void InclusaoManual() 
        {
            new cFrancesinhaManual().VirShowModulo(EstadosDosComponentes.PopUp);            
        }

        /// <summary>
        /// Incluir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            InclusaoManual();
        }

        private void BotTeste_Click(object sender, EventArgs e)
        {
            dFrancesinha.ARquivoLidoRow rowARL = (dFrancesinha.ARquivoLidoRow)bandedGridView1.GetFocusedDataRow();
            if (rowARL == null)
                return;            
            Fr.LocalVisivel = this;            
            string Relatorio = Fr.ProcessarDados(Francesinha.TipoRetorno.resumo,rowARL.ARL_BOL);
            if (Fr.Erro == null)
            {
                MessageBox.Show(Relatorio);
                if (Framework.DSCentral.USU == 30)
                {
                    System.Windows.Forms.Clipboard.SetText(Relatorio);
                }
            }
            else
                MessageBox.Show("Falha:" + Fr.Erro + "\r\n");
            radioGroup1.EditValue = 2;            
            panelB.Enabled = true;
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            Fr.Carregar();
            AjustaTela();
        }
    }
}

