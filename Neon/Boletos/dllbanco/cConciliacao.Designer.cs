namespace dllbanco
{
    partial class cConciliacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescrição = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipopag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDatadeLançamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNúmeroDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVersão = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.contaCorrenteDetalheBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dConciliacao = new dllbanco.dConciliacao();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCD_SCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDCategoria = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDCodHistorico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDValidado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDConsolidado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboBoxStatus = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCDErro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSCCData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON_EMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDTipoLancamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCT_BCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDescComplemento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.spinDEBUG_CCD = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.Dfim = new DevExpress.XtraEditors.DateEdit();
            this.Dini = new DevExpress.XtraEditors.DateEdit();
            this.BTDep = new DevExpress.XtraEditors.SimpleButton();
            this.BTClasse = new DevExpress.XtraEditors.SimpleButton();
            this.Gau = new DevExpress.XtraEditors.ProgressBarControl();
            this.BTParar = new DevExpress.XtraEditors.SimpleButton();
            this.BTProTodos = new DevExpress.XtraEditors.SimpleButton();
            this.BTAtualizar = new DevExpress.XtraEditors.SimpleButton();
            this.BTPro1 = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contaCorrenteDetalheBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dConciliacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinDEBUG_CCD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dfim.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dfim.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dini.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dini.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescrição,
            this.colValorPago,
            this.colTipopag,
            this.colDatadeLançamento,
            this.colNúmeroDocumento,
            this.colVersão});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colDescrição
            // 
            this.colDescrição.Caption = "Descrição";
            this.colDescrição.FieldName = "Descrição";
            this.colDescrição.Name = "colDescrição";
            this.colDescrição.Visible = true;
            this.colDescrição.VisibleIndex = 0;
            this.colDescrição.Width = 362;
            // 
            // colValorPago
            // 
            this.colValorPago.Caption = "Valor Pago";
            this.colValorPago.FieldName = "Valor Pago";
            this.colValorPago.Name = "colValorPago";
            this.colValorPago.Visible = true;
            this.colValorPago.VisibleIndex = 1;
            this.colValorPago.Width = 80;
            // 
            // colTipopag
            // 
            this.colTipopag.Caption = "Tipopag";
            this.colTipopag.FieldName = "Tipopag";
            this.colTipopag.Name = "colTipopag";
            this.colTipopag.Visible = true;
            this.colTipopag.VisibleIndex = 2;
            this.colTipopag.Width = 88;
            // 
            // colDatadeLançamento
            // 
            this.colDatadeLançamento.Caption = "Data";
            this.colDatadeLançamento.FieldName = "Data de Lançamento";
            this.colDatadeLançamento.Name = "colDatadeLançamento";
            this.colDatadeLançamento.Visible = true;
            this.colDatadeLançamento.VisibleIndex = 3;
            this.colDatadeLançamento.Width = 162;
            // 
            // colNúmeroDocumento
            // 
            this.colNúmeroDocumento.Caption = "Documento";
            this.colNúmeroDocumento.FieldName = "Número Documento";
            this.colNúmeroDocumento.Name = "colNúmeroDocumento";
            this.colNúmeroDocumento.Visible = true;
            this.colNúmeroDocumento.VisibleIndex = 4;
            this.colNúmeroDocumento.Width = 82;
            // 
            // colVersão
            // 
            this.colVersão.Caption = "Versão";
            this.colVersão.FieldName = "Versão";
            this.colVersão.Name = "colVersão";
            this.colVersão.Visible = true;
            this.colVersão.VisibleIndex = 5;
            this.colVersão.Width = 68;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.contaCorrenteDetalheBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 138);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ComboBoxStatus,
            this.repositoryItemImageComboBox1});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1477, 669);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2,
            this.gridView1});
            // 
            // contaCorrenteDetalheBindingSource
            // 
            this.contaCorrenteDetalheBindingSource.DataMember = "ContaCorrenteDetalhe";
            this.contaCorrenteDetalheBindingSource.DataSource = this.dConciliacao;
            // 
            // dConciliacao
            // 
            this.dConciliacao.DataSetName = "dConciliacao";
            this.dConciliacao.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.Aqua;
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.Aqua;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseBorderColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.Aqua;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCCD,
            this.colCCD_SCC,
            this.colCCDValor,
            this.colCCDDescricao,
            this.colCCDCategoria,
            this.colCCDCodHistorico,
            this.colCCDDocumento,
            this.colCCDValidado,
            this.colCCDConsolidado,
            this.colCCDErro,
            this.colSCCData,
            this.colCON,
            this.colCONCodigo,
            this.colCON_EMP,
            this.colCCDTipoLancamento,
            this.colCCT_BCO,
            this.colCCDDescComplemento});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsSelection.CheckBoxSelectorColumnWidth = 15;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView2.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupedColumns = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowSeparatorHeight = 6;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSCCData, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCodigo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCCDCategoria, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCCDValor, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCCD
            // 
            this.colCCD.Caption = "CCD";
            this.colCCD.CustomizationCaption = "CCD";
            this.colCCD.FieldName = "CCD";
            this.colCCD.Name = "colCCD";
            this.colCCD.OptionsColumn.ReadOnly = true;
            // 
            // colCCD_SCC
            // 
            this.colCCD_SCC.Caption = "SCC";
            this.colCCD_SCC.FieldName = "CCD_SCC";
            this.colCCD_SCC.Name = "colCCD_SCC";
            this.colCCD_SCC.OptionsColumn.ReadOnly = true;
            // 
            // colCCDValor
            // 
            this.colCCDValor.Caption = "Valor";
            this.colCCDValor.DisplayFormat.FormatString = "n2";
            this.colCCDValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCCDValor.FieldName = "CCDValor";
            this.colCCDValor.Name = "colCCDValor";
            this.colCCDValor.OptionsColumn.ReadOnly = true;
            this.colCCDValor.Visible = true;
            this.colCCDValor.VisibleIndex = 5;
            this.colCCDValor.Width = 102;
            // 
            // colCCDDescricao
            // 
            this.colCCDDescricao.Caption = "Descrição";
            this.colCCDDescricao.FieldName = "CCDDescricao";
            this.colCCDDescricao.Name = "colCCDDescricao";
            this.colCCDDescricao.OptionsColumn.ReadOnly = true;
            this.colCCDDescricao.Visible = true;
            this.colCCDDescricao.VisibleIndex = 3;
            this.colCCDDescricao.Width = 274;
            // 
            // colCCDCategoria
            // 
            this.colCCDCategoria.Caption = "Categoria";
            this.colCCDCategoria.FieldName = "CCDCategoria";
            this.colCCDCategoria.Name = "colCCDCategoria";
            this.colCCDCategoria.OptionsColumn.ReadOnly = true;
            this.colCCDCategoria.Width = 69;
            // 
            // colCCDCodHistorico
            // 
            this.colCCDCodHistorico.Caption = "Cod. Historico";
            this.colCCDCodHistorico.FieldName = "CCDCodHistorico";
            this.colCCDCodHistorico.Name = "colCCDCodHistorico";
            this.colCCDCodHistorico.OptionsColumn.ReadOnly = true;
            this.colCCDCodHistorico.Width = 53;
            // 
            // colCCDDocumento
            // 
            this.colCCDDocumento.Caption = "Doc";
            this.colCCDDocumento.FieldName = "CCDDocumento";
            this.colCCDDocumento.Name = "colCCDDocumento";
            this.colCCDDocumento.OptionsColumn.ReadOnly = true;
            this.colCCDDocumento.Visible = true;
            this.colCCDDocumento.VisibleIndex = 6;
            this.colCCDDocumento.Width = 92;
            // 
            // colCCDValidado
            // 
            this.colCCDValidado.Caption = "Validado";
            this.colCCDValidado.FieldName = "CCDValidado";
            this.colCCDValidado.Name = "colCCDValidado";
            this.colCCDValidado.OptionsColumn.ReadOnly = true;
            this.colCCDValidado.Width = 50;
            // 
            // colCCDConsolidado
            // 
            this.colCCDConsolidado.Caption = "Status";
            this.colCCDConsolidado.ColumnEdit = this.ComboBoxStatus;
            this.colCCDConsolidado.FieldName = "CCDConsolidado";
            this.colCCDConsolidado.Name = "colCCDConsolidado";
            this.colCCDConsolidado.OptionsColumn.ReadOnly = true;
            this.colCCDConsolidado.Visible = true;
            this.colCCDConsolidado.VisibleIndex = 9;
            this.colCCDConsolidado.Width = 123;
            // 
            // ComboBoxStatus
            // 
            this.ComboBoxStatus.AutoHeight = false;
            this.ComboBoxStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxStatus.Name = "ComboBoxStatus";
            // 
            // colCCDErro
            // 
            this.colCCDErro.Caption = "Erro";
            this.colCCDErro.FieldName = "CCDErro";
            this.colCCDErro.Name = "colCCDErro";
            this.colCCDErro.OptionsColumn.ReadOnly = true;
            this.colCCDErro.Visible = true;
            this.colCCDErro.VisibleIndex = 8;
            this.colCCDErro.Width = 71;
            // 
            // colSCCData
            // 
            this.colSCCData.Caption = "Data";
            this.colSCCData.FieldName = "SCCData";
            this.colSCCData.Name = "colSCCData";
            this.colSCCData.OptionsColumn.ReadOnly = true;
            this.colSCCData.Visible = true;
            this.colSCCData.VisibleIndex = 1;
            // 
            // colCON
            // 
            this.colCON.Caption = "CON";
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 2;
            this.colCONCodigo.Width = 89;
            // 
            // colCON_EMP
            // 
            this.colCON_EMP.Caption = "EMP";
            this.colCON_EMP.FieldName = "CON_EMP";
            this.colCON_EMP.Name = "colCON_EMP";
            this.colCON_EMP.OptionsColumn.ReadOnly = true;
            // 
            // colCCDTipoLancamento
            // 
            this.colCCDTipoLancamento.Caption = "Tipo";
            this.colCCDTipoLancamento.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCCDTipoLancamento.FieldName = "CCDTipoLancamento";
            this.colCCDTipoLancamento.Name = "colCCDTipoLancamento";
            this.colCCDTipoLancamento.OptionsColumn.ReadOnly = true;
            this.colCCDTipoLancamento.Visible = true;
            this.colCCDTipoLancamento.VisibleIndex = 7;
            this.colCCDTipoLancamento.Width = 144;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colCCT_BCO
            // 
            this.colCCT_BCO.Caption = "Banco";
            this.colCCT_BCO.FieldName = "CCT_BCO";
            this.colCCT_BCO.Name = "colCCT_BCO";
            this.colCCT_BCO.OptionsColumn.ReadOnly = true;
            this.colCCT_BCO.Width = 55;
            // 
            // colCCDDescComplemento
            // 
            this.colCCDDescComplemento.Caption = "Complemento";
            this.colCCDDescComplemento.FieldName = "CCDDescComplemento";
            this.colCCDDescComplemento.Name = "colCCDDescComplemento";
            this.colCCDDescComplemento.OptionsColumn.ReadOnly = true;
            this.colCCDDescComplemento.Visible = true;
            this.colCCDDescComplemento.VisibleIndex = 4;
            this.colCCDDescComplemento.Width = 177;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.spinDEBUG_CCD);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Controls.Add(this.Dfim);
            this.panelControl1.Controls.Add(this.Dini);
            this.panelControl1.Controls.Add(this.BTDep);
            this.panelControl1.Controls.Add(this.BTClasse);
            this.panelControl1.Controls.Add(this.Gau);
            this.panelControl1.Controls.Add(this.BTParar);
            this.panelControl1.Controls.Add(this.BTProTodos);
            this.panelControl1.Controls.Add(this.BTAtualizar);
            this.panelControl1.Controls.Add(this.BTPro1);
            this.panelControl1.Controls.Add(this.radioGroup1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1477, 138);
            this.panelControl1.TabIndex = 1;
            // 
            // spinDEBUG_CCD
            // 
            this.spinDEBUG_CCD.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinDEBUG_CCD.Location = new System.Drawing.Point(500, 89);
            this.spinDEBUG_CCD.Name = "spinDEBUG_CCD";
            this.spinDEBUG_CCD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinDEBUG_CCD.Size = new System.Drawing.Size(100, 20);
            this.spinDEBUG_CCD.TabIndex = 16;
            this.spinDEBUG_CCD.Visible = false;
            this.spinDEBUG_CCD.EditValueChanged += new System.EventHandler(this.spinDEBUG_CCD_EditValueChanged);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(850, 34);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(169, 23);
            this.simpleButton4.TabIndex = 15;
            this.simpleButton4.Text = "Boletos Range";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click_1);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(850, 5);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(169, 23);
            this.simpleButton3.TabIndex = 14;
            this.simpleButton3.Text = "Créditos da caixa Range";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click_1);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(675, 34);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(169, 23);
            this.simpleButton1.TabIndex = 13;
            this.simpleButton1.Text = "Créditos da caixa";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_2);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(675, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(169, 23);
            this.simpleButton2.TabIndex = 12;
            this.simpleButton2.Text = "Verificar Débitos Automáticos";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click_1);
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(151, 63);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(622, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 11;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = "Framework.Lookup.LookupCondominio_F - ";
            // 
            // Dfim
            // 
            this.Dfim.EditValue = null;
            this.Dfim.Location = new System.Drawing.Point(324, 89);
            this.Dfim.Name = "Dfim";
            this.Dfim.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Dfim.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Dfim.Size = new System.Drawing.Size(100, 20);
            this.Dfim.TabIndex = 10;
            // 
            // Dini
            // 
            this.Dini.EditValue = null;
            this.Dini.Location = new System.Drawing.Point(218, 89);
            this.Dini.Name = "Dini";
            this.Dini.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Dini.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Dini.Size = new System.Drawing.Size(100, 20);
            this.Dini.TabIndex = 9;
            // 
            // BTDep
            // 
            this.BTDep.Location = new System.Drawing.Point(500, 34);
            this.BTDep.Name = "BTDep";
            this.BTDep.Size = new System.Drawing.Size(169, 23);
            this.BTDep.TabIndex = 7;
            this.BTDep.Text = "Deposisto";
            this.BTDep.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // BTClasse
            // 
            this.BTClasse.Enabled = false;
            this.BTClasse.Location = new System.Drawing.Point(325, 34);
            this.BTClasse.Name = "BTClasse";
            this.BTClasse.Size = new System.Drawing.Size(169, 23);
            this.BTClasse.TabIndex = 6;
            this.BTClasse.Text = "Classificar para balancete";
            this.BTClasse.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // Gau
            // 
            this.Gau.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Gau.EditValue = 50;
            this.Gau.Location = new System.Drawing.Point(2, 118);
            this.Gau.Name = "Gau";
            this.Gau.Size = new System.Drawing.Size(1473, 18);
            this.Gau.TabIndex = 5;
            this.Gau.Visible = false;
            // 
            // BTParar
            // 
            this.BTParar.Location = new System.Drawing.Point(500, 5);
            this.BTParar.Name = "BTParar";
            this.BTParar.Size = new System.Drawing.Size(169, 23);
            this.BTParar.TabIndex = 4;
            this.BTParar.Text = "PARAR";
            this.BTParar.Visible = false;
            this.BTParar.Click += new System.EventHandler(this.BTParar_Click);
            // 
            // BTProTodos
            // 
            this.BTProTodos.Location = new System.Drawing.Point(325, 5);
            this.BTProTodos.Name = "BTProTodos";
            this.BTProTodos.Size = new System.Drawing.Size(169, 23);
            this.BTProTodos.TabIndex = 3;
            this.BTProTodos.Text = "Processar todos";
            this.BTProTodos.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // BTAtualizar
            // 
            this.BTAtualizar.Location = new System.Drawing.Point(150, 34);
            this.BTAtualizar.Name = "BTAtualizar";
            this.BTAtualizar.Size = new System.Drawing.Size(169, 23);
            this.BTAtualizar.TabIndex = 2;
            this.BTAtualizar.Text = "Atualizar";
            this.BTAtualizar.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // BTPro1
            // 
            this.BTPro1.Location = new System.Drawing.Point(150, 5);
            this.BTPro1.Name = "BTPro1";
            this.BTPro1.Size = new System.Drawing.Size(169, 23);
            this.BTPro1.TabIndex = 1;
            this.BTPro1.Text = "Processar os selecionados";
            this.BTPro1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(5, 5);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Pendentes"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Depósitos pendentes"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Balancete"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Débito Automático")});
            this.radioGroup1.Size = new System.Drawing.Size(140, 107);
            this.radioGroup1.TabIndex = 0;
            this.radioGroup1.SelectedIndexChanged += new System.EventHandler(this.radioGroup1_SelectedIndexChanged);
            // 
            // cConciliacao
            // 
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cConciliacao";
            this.Size = new System.Drawing.Size(1477, 807);
            this.Titulo = "Consiliação";
            this.cargaPrincipal += new System.EventHandler(this.cConciliacao_cargaPrincipal);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contaCorrenteDetalheBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dConciliacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinDEBUG_CCD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dfim.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dfim.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dini.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dini.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private System.Windows.Forms.BindingSource contaCorrenteDetalheBindingSource;
        private dConciliacao dConciliacao;
        private DevExpress.XtraEditors.SimpleButton BTPro1;
        private DevExpress.XtraEditors.SimpleButton BTAtualizar;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboBoxStatus;
        private DevExpress.XtraEditors.SimpleButton BTProTodos;
        private DevExpress.XtraEditors.SimpleButton BTParar;
        private DevExpress.XtraEditors.ProgressBarControl Gau;
        private DevExpress.XtraEditors.SimpleButton BTClasse;
        private DevExpress.XtraEditors.SimpleButton BTDep;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescrição;
        private DevExpress.XtraGrid.Columns.GridColumn colValorPago;
        private DevExpress.XtraGrid.Columns.GridColumn colTipopag;
        private DevExpress.XtraEditors.DateEdit Dfim;
        private DevExpress.XtraEditors.DateEdit Dini;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraGrid.Columns.GridColumn colDatadeLançamento;
        private DevExpress.XtraGrid.Columns.GridColumn colNúmeroDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn colVersão;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SpinEdit spinDEBUG_CCD;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colCCD;
        private DevExpress.XtraGrid.Columns.GridColumn colCCD_SCC;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDCategoria;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDCodHistorico;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDValidado;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDConsolidado;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDErro;
        private DevExpress.XtraGrid.Columns.GridColumn colSCCData;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_EMP;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDTipoLancamento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT_BCO;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDescComplemento;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
    }
}
