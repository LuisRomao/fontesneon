/*
MR - 24/04/2014 12:00 -          - Concilia��o de Pagamento Eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (24/04/2014 12:00) ***)
LH - 21/05/2014 11:47 - 13.2.9.0 - inclusao do CCD na mensagem de tempo de transa��o
MR - 20/05/2014 13:00 - 13.2.9.3 - Concilia��o de Pagamento Eletr�nico, considerando v�rios CODHISTORICO (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (20/05/2014 13:00) ***)
MR - 12/11/2014 10:00 -          - Concilia��o de Tributo Eletr�nico (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Data;
using CompontesBasicosProc;
using CompontesBasicos.ObjetosEstaticos;
using dllbanco.Extrato;
using dllCNAB;
using Framework;
using FrameworkProc;
using FrameworkProc.datasets;
using dllImpostoNeonProc;
using VirEnumeracoesNeon;



namespace dllbanco
{
    /// <summary>
    /// 
    /// </summary>
    public class Conciliacao : AbstratosNeon.ABS_Conciliacao
    {
        /// <summary>
        /// Fun��o para cancelamento da concilia��o do extrato com a francesinha
        /// </summary>
        /// <param name="CCD"></param>
        public static void CancelaConcEstratoFran(int CCD,bool ResetParaNaoIdentificado)
        {
            string comandoBuscaDados = "select CCDconsolidado,CCDTipoLancamento from Contacorrentedetalhe where ccd = @P1";
            string comandoLiberaARL = "update arquivolido set ARL_CCD = null where ARL_CCD = @P1";
            string comandoLiberaBoletos = "update BOLetos set BOL_CCD = null where bol_ccd = @P1";
            string comandoResetCCD = string.Format("update Contacorrentedetalhe set CCDconsolidado = {0},CCDTipoLancamento= {1} where ccd = @P1 and CCDTipoLancamento = {2}",
                                                    (int)(ResetParaNaoIdentificado ? statusconsiliado.Aguardando : statusconsiliado.Manual),
                                                    (int)(ResetParaNaoIdentificado?TipoLancamentoNeon.NaoIdentificado: TipoLancamentoNeon.NaoIdentificado),
                                                    (int)TipoLancamentoNeon.creditodecobranca);
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("CancelaConcEstratoFran Conciliacao.cs - 39");
                //Checagem
                DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(comandoBuscaDados, CCD);
                statusconsiliado CCDconsolidado = (statusconsiliado)((int)DR["CCDconsolidado"]);
                TipoLancamentoNeon CCDTipoLancamento = (TipoLancamentoNeon)((int)DR["CCDTipoLancamento"]);
                if (CCDTipoLancamento != TipoLancamentoNeon.creditodecobranca)
                    throw new Exception(string.Format("Erro ao cancelar concilia��o: status = {0} tipo = {1}", CCDconsolidado, CCDTipoLancamento));
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLiberaARL, CCD);
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLiberaBoletos, CCD);
                if (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoResetCCD, CCD) < 1)
                    throw new Exception(string.Format("Erro ao cancelar concilia��o: status = {0} tipo = {1}", CCDconsolidado, CCDTipoLancamento));

                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                throw new Exception(string.Format("Erro ao cancelar concilia��o: {0}", e.Message), e);
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BCO"></param>
        /// <param name="Descricao"></param>
        /// <param name="Categoria"></param>
        /// <param name="CodHistorico"></param>
        /// <returns></returns>        
        public static bool TransferenciaInterna(int BCO, string Descricao, int Categoria, int CodHistorico)
        {
            switch (BCO)
            {
                case 341:
                    if ((Descricao.ToUpper().Trim() == "APL APLIC AUT MAIS") || (Descricao.ToUpper().Trim() == "RES APLIC AUT MAIS"))
                        return true;
                    break;
                case 237:
                    //C�digos antigoa usados tanto para PLUS como CDB. teoricamente agora � s� CDB!!!
                    //if ((Descricao.ToUpper().Trim() == "APLICACOES EM PAPEIS") && (Categoria == 106) && (CodHistorico == 294))
                    //    return true;
                    //if ((Descricao.ToUpper().Trim() == "RESGATE MERCADO ABERTO") && (Categoria == 206) && (CodHistorico == 63))
                    //    return true;
                    //if ((Descricao.ToUpper().Trim() == "BX AUTOMATICA APLICACOES") && (Categoria == 206) && (CodHistorico == 101))
                    //    return true;
                    //if ((Descricao.ToUpper().Trim() == "APLICACAO AUTOMATICA") && (Categoria == 106) && (CodHistorico == 101))
                    //    return true;
                    if ((Categoria == 106) && (CodHistorico.EstaNoGrupo(2570,2573)))
                        return true;
                    if ((Categoria == 206) && (CodHistorico.EstaNoGrupo(2571,2572)))
                        return true;                    
                    break;
            }

            //if ((BCO == 341) && ((Descricao.ToUpper().Trim() == "APL APLIC AUT MAIS") || (Descricao.ToUpper().Trim() == "RES APLIC AUT MAIS")))
            //    return true;            
            //if ((BCO == 237) && (Descricao.ToUpper().Trim() == "APLICACOES EM PAPEIS") && (Categoria == 106) && (CodHistorico == 294))
            //    return true;
            //******************************CDB
            //if ((BCO == 237) && (Descricao.ToUpper().Trim() == "RESGATE DE PAPEIS") && (Categoria == 206) && (CodHistorico == 62))
            //    return true;
            //******************************CDB
            //if ((BCO == 237) && (Descricao.ToUpper().Trim() == "RESGATE MERCADO ABERTO") && (Categoria == 206) && (CodHistorico == 63))
            //    return true;
            //if ((BCO == 237) && (Descricao.ToUpper().Trim() == "BX AUTOMATICA APLICACOES") && (Categoria == 206) && (CodHistorico == 101))
            //    return true;
            //if ((BCO == 237) && (Descricao.ToUpper().Trim() == "APLICACAO AUTOMATICA") && (Categoria == 106) && (CodHistorico == 101))
            //    return true;
            return false;            
        }

       
        private static Conciliacao _ConciliacaoSt;

        /// <summary>
        /// Est�tico
        /// </summary>
        public static Conciliacao ConciliacaoSt {
            get {
                if (_ConciliacaoSt == null)
                    _ConciliacaoSt = new Conciliacao();
                return _ConciliacaoSt;
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        public string Erro = "";

        //IdentificaArquivo.TipoArquivo TipoAtual = IdentificaArquivo.TipoArquivo.INVALIDO;
        /// <summary>
        /// 
        /// </summary>
        public bool assistido;

        /// <summary>
        /// Os arquivo do unibanco tem o cabecalho no meio do arquivo!!!
        /// </summary>
        public string CabecalhoPerdido;

        private dExtrato _dExtrato;
        
        /// <summary>
        /// 
        /// </summary>
        public dExtrato dExtrato
        {
            get
            {
                if (_dExtrato == null)
                    _dExtrato = new dExtrato();
                return _dExtrato;
            }
        }

  

        
        

   

        /// <summary>
        /// Faz a amarra��o dos boletos da CAIXA
        /// </summary>
        /// <returns></returns>
        public string AmarraBoletosCaixa()
        {
            string Relatorio = "\r\nCobran�a CAIXA ";
            dConciliaCaixa dConciliaCaixa1 = new dConciliaCaixa();
            int Total = dConciliaCaixa1.CreditoDiarioTableAdapter.Fill(dConciliaCaixa1.CreditoDiario);
            Relatorio += string.Format("({0})\r\n",Total);
            dConciliaCaixa1.ARquivoLidoTableAdapter.Fill(dConciliaCaixa1.ARquivoLido,DateTime.Today.AddYears(-1));
            dConciliaCaixa1.GruposTableAdapter.Fill(dConciliaCaixa1.Grupos);
            dConciliaCaixa1.ItensTableAdapter.Fill(dConciliaCaixa1.Itens,DateTime.Today.AddYears(-1));
            int i = 1;
            foreach (dConciliaCaixa.CreditoDiarioRow rowCred in dConciliaCaixa1.CreditoDiario)
            {
                dConciliaCaixa.ARquivoLidoRow rowARLs = dConciliaCaixa1.ARquivoLido.FindByDataCCT(rowCred.Data, rowCred.CCT);
                if (rowARLs != null)
                {
                    if (rowARLs.Valor == rowCred.Valor)
                    {
                        SortedList<int, decimal> Totais = new SortedList<int, decimal>();
                        foreach (dConciliaCaixa.GruposRow rowG in rowCred.GetGruposRows())
                            Totais.Add(rowG.CCD, rowG.Valor);
                        SortedList<int, decimal> Itens = new SortedList<int, decimal>();
                        foreach (dConciliaCaixa.ItensRow rowIt in rowARLs.GetItensRows())
                            Itens.Add(rowIt.ARL, rowIt.Valor);
                        dllC12.Agrupador Ag = new dllC12.Agrupador();
                        //Ag.IniciaLog(string.Format(@"D:\logCaixa{0}.log",i++));
                        Ag.TempoMaximo = 120;
                        SortedList<int, List<int>> Resposta = Ag.Agrupar(Totais, Itens);
                        if (Resposta != null)
                        {
                            Relatorio += string.Format("  {0}) ok Data {1:dd/MM/yyyy} CCT {2} Val {3:n2} Tempo {4:n4} s\r\n", i, rowCred.Data, rowCred.CCT, rowARLs.Valor, Ag.TempoTotal);
                            foreach (int CCD in Resposta.Keys)
                            {
                                try
                                {
                                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Conciliacao - 141");
                                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ContaCorrenteDetalhe set CCDConsolidado = 2,CCDTipoLancamento = 9 where CCD = @P1", CCD);
                                    foreach (int ARL in Resposta[CCD])
                                    {
                                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ARquivoLido set ARL_CCD = @P1 where ARL = @P2", CCD, ARL);
                                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("UPDATE BOLetos SET BOL_CCD = @P1 FROM ARquivoLido INNER JOIN BOLetos ON ARquivoLido.ARL = BOLetos.BOL_ARL WHERE (ARquivoLido.ARL = @P2)", CCD, ARL);
                                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Relatorio += string.Format("Erro na grava��o:\r\n-----\r\n{0}\r\n-----\r\n",ex.Message);
                                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                                }
                            }
                        }
                    }
                    else
                        Relatorio += string.Format("  {0}) Valor incorreto: Data {1:dd/MM/yyyy} CCT {2} Fracesinha {3:n2} Conta {4:n2}\r\n", i, rowCred.Data, rowCred.CCT, rowARLs.Valor, rowCred.Valor);
                }
                else
                    Relatorio += string.Format("  {0}) Francesinha n�o encontrada: Data {1:dd/MM/yyyy} CCT {2}\r\n", i, rowCred.Data, rowCred.CCT);
            };
            return Relatorio;
        }

        /// <summary>
        /// Amarra os boletos ao CCD em um range de datas
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="BCO"></param>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        public void AmarraBoletosRarge(int CON, int BCO, DateTime DI, DateTime DF)
        {
            dConciliaCaixa dConciliaCaixa1 = new dConciliaCaixa();
            if (BCO == 104)
            {
                if (dConciliaCaixa1.CreditoRangeTableAdapter.FillBy104(dConciliaCaixa1.CreditoRange, DI, DF,CON) != 1)
                    return;
                dConciliaCaixa1.GruposRangeTableAdapter.FillBy104(dConciliaCaixa1.GruposRange, DI, DF,CON);
            }
            else
            {
                if (dConciliaCaixa1.CreditoRangeTableAdapter.FillBy237(dConciliaCaixa1.CreditoRange, DI, DF, CON) != 1)
                    return;
                dConciliaCaixa1.GruposRangeTableAdapter.FillBy237(dConciliaCaixa1.GruposRange, DI, DF, CON);
            }
            //dConciliaCaixa1.ARquivoLidoRangeTableAdapter.Fill(dConciliaCaixa1.ARquivoLidoRange, DI, DF);
            //dConciliaCaixa1.ItensRangeTableAdapter.Fill(dConciliaCaixa1.ItensRange, DI, DF);

            dConciliaCaixa.CreditoRangeRow rowCred = dConciliaCaixa1.CreditoRange[0];
            string comandoTotal = "SELECT SUM(BOLValorPago) FROM BOLetos WHERE (BOL_CON = @P1) AND (BOLStatus in (0,3,4)) AND (BOL_CCD IS NULL) AND (BOLPagamento BETWEEN @P2 AND @P3)";
            string comandoBOLs = "SELECT BOL,BOLValorPago FROM BOLetos WHERE (BOL_CON = @P1) AND (BOLStatus in (0,3,4)) AND (BOL_CCD IS NULL) AND (BOLPagamento BETWEEN @P2 AND @P3)";
            decimal? TotalBoletos = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_decimal(comandoTotal, CON, DI, DF);


            if (TotalBoletos.GetValueOrDefault(0) == rowCred.Valor)
            {
                SortedList<int, decimal> Totais = new SortedList<int, decimal>();
                foreach (dConciliaCaixa.GruposRangeRow rowG in rowCred.GetGruposRangeRows())
                    Totais.Add(rowG.CCD, rowG.Valor);
                SortedList<int, decimal> Itens = new SortedList<int, decimal>();
                System.Data.DataTable TabBOLs = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comandoBOLs, CON, DI, DF);
                foreach (System.Data.DataRow rowIt in TabBOLs.Rows)
                    Itens.Add((int)rowIt["BOL"], (decimal)rowIt["BOLValorPago"]);
                dllC12.Agrupador Ag = new dllC12.Agrupador();
                //Ag.IniciaLog(string.Format(@"D:\logCaixa{0}.log", nArquivo++));
                Ag.TempoMaximo = 120;
                SortedList<int, List<int>> Resposta = Ag.Agrupar(Totais, Itens);
                if (Resposta != null)
                {
                    foreach (int CCD in Resposta.Keys)
                    {
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Conciliacao AmarraBoletosRarge - 230");
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ContaCorrenteDetalhe set CCDConsolidado = 2,CCDTipoLancamento = 9 where CCD = @P1", CCD);
                            foreach (int BOL in Resposta[CCD])
                            {
                                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("UPDATE BOLetos SET BOL_CCD = @P1 FROM BOLetos WHERE BOL = @P2", CCD, BOL);
                                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("UPDATE ARquivoLido SET ARL_CCD = @P1 FROM ARquivoLido INNER JOIN BOLetos ON ARquivoLido.ARL = BOLetos.BOL_ARL WHERE (BOLetos.BOL = @P2)", CCD, BOL);
                            }
                            VirMSSQL.TableAdapter.STTableAdapter.Commit();
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Faz a amarra��o dos boletos da caixa por per�do - usar em feriados
        /// </summary>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        public void AmarraBoletosCaixaRarge(DateTime DI,DateTime DF)
        {
            dConciliaCaixa dConciliaCaixa1 = new dConciliaCaixa();            
            dConciliaCaixa1.CreditoRangeTableAdapter.Fill(dConciliaCaixa1.CreditoRange, DI, DF);                
            dConciliaCaixa1.GruposRangeTableAdapter.Fill(dConciliaCaixa1.GruposRange, DI, DF);                                        
            dConciliaCaixa1.ARquivoLidoRangeTableAdapter.Fill(dConciliaCaixa1.ARquivoLidoRange, DI, DF);
            dConciliaCaixa1.ItensRangeTableAdapter.Fill(dConciliaCaixa1.ItensRange, DI, DF);
            //int nArquivo = 1;
            foreach (dConciliaCaixa.CreditoRangeRow rowCred in dConciliaCaixa1.CreditoRange)
            {
                dConciliaCaixa.ARquivoLidoRangeRow rowARLs = dConciliaCaixa1.ARquivoLidoRange.FindByCCT(rowCred.CCT);
                if (rowARLs != null)
                {
                    if (rowARLs.Valor == rowCred.Valor)
                    {
                        SortedList<int, decimal> Totais = new SortedList<int, decimal>();
                        foreach (dConciliaCaixa.GruposRangeRow rowG in rowCred.GetGruposRangeRows())
                            Totais.Add(rowG.CCD, rowG.Valor);
                        SortedList<int, decimal> Itens = new SortedList<int, decimal>();
                        foreach (dConciliaCaixa.ItensRangeRow rowIt in rowARLs.GetItensRangeRows())
                            Itens.Add(rowIt.ARL, rowIt.Valor);
                        dllC12.Agrupador Ag = new dllC12.Agrupador();
                        //Ag.IniciaLog(string.Format(@"D:\logCaixa{0}.log", nArquivo++));
                        Ag.TempoMaximo = 120;
                        SortedList<int, List<int>> Resposta = Ag.Agrupar(Totais, Itens);
                        if (Resposta != null)
                        {
                            foreach (int CCD in Resposta.Keys)
                            {
                                try
                                {
                                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Conciliacao - 203");
                                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ContaCorrenteDetalhe set CCDConsolidado = 2,CCDTipoLancamento = 9 where CCD = @P1", CCD);
                                    foreach (int ARL in Resposta[CCD])
                                    {
                                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ARquivoLido set ARL_CCD = @P1 where ARL = @P2", CCD, ARL);
                                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("UPDATE BOLetos SET BOL_CCD = @P1 FROM ARquivoLido INNER JOIN BOLetos ON ARquivoLido.ARL = BOLetos.BOL_ARL WHERE (ARquivoLido.ARL = @P2)", CCD, ARL);                                        
                                    }
                                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                                }
                                catch (Exception ex)
                                {
                                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                                }
                            }
                        }
                    }
                }
            }
        }

        
        //private const string comandoBuscaCCT03 = "SELECT CCT FROM ContaCorrenTe WHERE (CCT_BCO = @P1) AND (CCTAgencia = @P2) AND (CCTConta = @P3) AND (CCTTipo in (0,3))";
        //private const string comandoBuscaCCT12 = "SELECT CCT FROM ContaCorrenTe WHERE (CCT_BCO = @P1) AND (CCTAgencia = @P2) AND (CCTConta = @P3) AND (CCTTipo in (1,2))";
        private const string comandoBuscaCON = "SELECT CON FROM CONDOMINIOS WHERE (CON_BCO = @P1) AND (CONAgencia like @P2) AND (CONConta like @P3)";
        private const string comandoBuscaCONdoCCT = "SELECT CCT_CON FROM ContaCorrenTe WHERE (CCT = @P1)"; 
        

        

        


        private System.Collections.ArrayList _novaCCDs;

        private System.Collections.ArrayList novaCCDs {
            get {
                if (_novaCCDs == null)
                    _novaCCDs = new System.Collections.ArrayList();
                return _novaCCDs;
            }
        }
      

        enum TipoLancamento
        {
            disponivel = 1,
            compensar =2,
            futuro = 5
        }

    
       

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ProgressBarControl Gau;

        





  




        private dConciliacao _dConciliacao;

        /// <summary>
        /// 
        /// </summary>
        public dConciliacao dConciliacao {
            get
            {
                if (_dConciliacao == null)
                    _dConciliacao = new dConciliacao();
                return _dConciliacao;
            }
            set { _dConciliacao = value; }
        }


        private static string GetComandoCondominiosdapool()
        {
            return
            "SELECT CONCodigo FROM CONDOMINIOS WHERE (CON_BCO = @P1) AND (CONAgencia = @P2) AND (CONConta = @P3);";            
        }

        private static string GetComandoUpChSindico()
        {
            return
            "UPDATE [cheques sindico] SET Valor = @P1, Datacaiu = @P2, Descri��o = @P3 WHERE (Codcon = @P4) AND (Numcheque = @P5);";             
        }

        private int? BuscaFRN(int Codigo)
        {
            if (dConciliacao.FORNECEDORES.Count == 0)
                dConciliacao.FORNECEDORESTableAdapter.Fill(dConciliacao.FORNECEDORES);
            dConciliacao.FORNECEDORESRow rowFRN = dConciliacao.FORNECEDORES.FindByFRNCodigoDebito(Codigo);
            if (rowFRN == null)
                return null;
            else
                return rowFRN.FRN;
        }

        private statusconsiliado ConciliaDebitoAutomatico(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            statusconsiliado Resposta = statusconsiliado.Aguardando;
            int? FRN = null;
            long Codigo;
            dConciliacao.PaGamentosFixos.Clear();
            if (Linhaconciliar.CCT_BCO == 341)
            {
                if (Linhaconciliar.CCDDescricao.Contains("ELETROPAULO"))
                {
                    FRN = 1499;
                    Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                    dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                }
                else if (Linhaconciliar.CCDDescricao.Contains("SABESP"))
                {
                    FRN = 1595;
                    Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                    Codigo = Codigo / 100;
                    dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                    //Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                    //dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                }                
                else if (Linhaconciliar.CCDDescricao.Contains("DA  NET SERVICOS"))
                {
                    FRN = 330;
                    Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                    Codigo = Codigo / 10;
                    dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                    //Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                    //dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                }
                else if (Linhaconciliar.CCDDescricao.Contains("TELEFONICA"))
                {
                    FRN = 1114;
                    Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                    dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                }
                else if (Linhaconciliar.CCDDescricao.Contains("COMGAS"))
                {
                    FRN = 602;
                    if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                    {
                        Codigo = Codigo % 10000000;
                        dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                    }
                }
                else if (Linhaconciliar.CCDDescricao.Contains("DA  TIM"))
                {
                    FRN = BuscaFRN(109);
                    if (FRN.HasValue)
                    {

                        Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                        Codigo = Codigo % 100000000000;
                        Codigo = Codigo / 10;
                        dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                    }
                }
                else if (Linhaconciliar.CCDDescricao.Contains("DA  CLARO"))
                {
                    FRN = BuscaFRN(162);
                    if (FRN.HasValue)
                    //FRN = 2273;
                    {
                        string manobra = StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros });

                        if (long.TryParse(manobra, out Codigo))
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);

                        if ((dConciliacao.PaGamentosFixos.Count == 0) && (manobra.Length > 2) && (long.TryParse(manobra.Substring(2), out Codigo)))
                        {
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                }
            }
            else if (Linhaconciliar.CCT_BCO == 237)
            {
                if (!Linhaconciliar.IsCCDDescComplementoNull())
                {


                    if (Linhaconciliar.CCDDescComplemento.Contains("ELETROPAULO"))
                    {

                        FRN = Framework.DSCentral.EMP == 1 ? 1499 : 188;

                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            //caso do bota
                            if ((dConciliacao.PaGamentosFixos.Count == 0) && (Linhaconciliar.CON == 629))
                            {
                                for (int ibota = 615; ibota <= 626; ibota++)
                                {
                                    dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, ibota, FRN, Codigo);
                                    if (dConciliacao.PaGamentosFixos.Count == 1)
                                        break;
                                }
                            }


                        }
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("SABESP"))
                    {
                        FRN = 1595;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            Codigo = Codigo / 100;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            //caso do bota
                            if ((dConciliacao.PaGamentosFixos.Count == 0) && (Linhaconciliar.CON == 629))
                            {
                                for (int ibota = 615; ibota <= 626; ibota++)
                                {
                                    dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, ibota, FRN, Codigo);
                                    if (dConciliacao.PaGamentosFixos.Count == 1)
                                        break;
                                }
                            }


                        }
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("SANED"))
                    {
                        FRN = 2865;
                        dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Linhaconciliar.CCDDocumento);
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("COMGAS"))
                    {
                        FRN = 602;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            Codigo = Codigo % 10000000;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("ULTRAGAZ"))
                    {
                        FRN = 190;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            //Codigo = Codigo / 10;                            
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("EBT LIVRE"))
                    {
                        FRN = 852;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("NEXTEL"))
                    {
                        FRN = 358;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            if (dConciliacao.PaGamentosFixos.Count == 0)
                            {
                                Codigo = Codigo / 10;
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            }
                        }
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("CLARO"))
                    {
                        FRN = BuscaFRN(162);
                        if (FRN.HasValue)
                        //FRN = 2273;
                        {
                            string manobra = StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros });

                            if (long.TryParse(manobra, out Codigo))
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);

                            if ((dConciliacao.PaGamentosFixos.Count == 0) && (manobra.Length > 2) && (long.TryParse(manobra.Substring(2), out Codigo)))
                            {
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            }
                        }

                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("NET SERVICOS"))
                    {
                        FRN = 330;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            Codigo = Codigo / 10;
                        Codigo = Codigo % 1000000;
                        if (dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo) == 0)
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Linhaconciliar.CCDDocumento);
                            
                        
                        //Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                        //dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("TIM CELULAR"))
                    {
                        FRN = BuscaFRN(109);
                        if (FRN.HasValue)
                        {

                            Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                            Codigo = Codigo % 100000000000;
                            Codigo = Codigo / 10;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("TELEFONICA"))
                    {
                        FRN = 1114;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                    }
                    else if (Linhaconciliar.CCDDescComplemento.Contains("GVT-"))
                    {
                        FRN = BuscaFRN(82);
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            dConciliacao.PaGamentosFixosTableAdapter.FillByIdent(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                    }
                    if (Linhaconciliar.CCDDescComplemento.Contains("DAE-SAO CAETANO/SP-"))
                    {
                        FRN = BuscaFRN(35);
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            Codigo /= 10;
                            dConciliacao.PaGamentosFixosTableAdapter.FillByIdent(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                    if (Linhaconciliar.CCDDescComplemento.Contains("SEMASA-STO"))
                    {
                        FRN = BuscaFRN(113);
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            Codigo /= 10;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                    if (Linhaconciliar.CCDDescComplemento.Contains("VIVO"))
                    {
                        FRN = BuscaFRN(82);
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            Codigo /= 10;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                }
            }
            

            if (dConciliacao.PaGamentosFixos.Count >= 1)
            {
                dConciliacao.PaGamentosFixosRow rowPGF = null;
                if (dConciliacao.PaGamentosFixos.Count == 1)
                    rowPGF = dConciliacao.PaGamentosFixos[0];
                else
                {
                    //DESAMBIGUA��O:
                    //no caso do Bradesco o c�digo vem truncado criando um umbiguidade que deve ser resolvida primeiro procurando o pagamento agendado e em segundo pelo valor nominal
                    int? PGF = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(ComandoBuscaGererico, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CON);
                    if (PGF.HasValue)
                        rowPGF = dConciliacao.PaGamentosFixos.FindByPGF(PGF.Value);
                    if (rowPGF == null)
                        foreach (dConciliacao.PaGamentosFixosRow rowCandito in dConciliacao.PaGamentosFixos)
                        {
                            if (rowCandito.PGFValor == -Linhaconciliar.CCDValor)
                                rowPGF = rowCandito;
                            //AbstratosNeon.ABS_PagamentoPeriodico pagPerCandidato = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(rowCandito.PGF);                                    
                            //if (pagPerCandidato.ValorProximoDebitoProgramado() == -Linhaconciliar.CCDValor)
                            //    rowPGF = rowCandito;
                        }
                }
                if (rowPGF != null)
                {
                    AbstratosNeon.ABS_PagamentoPeriodico pagPer = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(rowPGF.PGF);
                    //ContaPagar.Follow.PagamentoPeriodico pagPer = new ContaPagar.Follow.PagamentoPeriodico(rowPGF.PGF);
                    if (pagPer.ConciliaDebitoAutomatico(Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CCD))                    
                        Resposta = RegistroAntigoLancBradesco(Linhaconciliar, rowPGF.PGF_PLA, rowPGF.PGFDescricao, false);                                            
                }
                else
                    return statusconsiliado.Aguardando;
            }
            else 
            {
                if (FRN.HasValue)
                {
                    Framework.Alarmes.Aviso Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, Linhaconciliar.CCD);
                    if (!Aviso.Encontrado)
                    {
                        dCondominiosAtivos.CONDOMINIOSRow rowCON = dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(Linhaconciliar.CON);
                        System.Data.DataRow DRForn = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select FRNCnpj,FRNNome,FRNFantasia from FORNECEDORES where FRN = @P1", FRN.Value);
                        string DescritivoErro = string.Format("\r\nCondom�nio: {0} - {1}\r\n\r\nFornecedor: {2} - {3} / {4}\r\n\r\nData: {5:dd/MM/yyyy}\r\nDescri��o: {6}/{7}",
                                                              rowCON.CONCodigo,
                                                              rowCON.CONNome,
                                                              DRForn["FRNCnpj"] == DBNull.Value ? "" : (string)DRForn["FRNCnpj"],
                                                              DRForn["FRNNome"] == DBNull.Value ? "" : (string)DRForn["FRNNome"],
                                                              DRForn["FRNFantasia"] == DBNull.Value ? "" : (string)DRForn["FRNFantasia"],
                                                              Linhaconciliar.SCCData,
                                                              Linhaconciliar.CCDDescricao,
                                                              Linhaconciliar.CCDDescComplemento);
                        Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, Linhaconciliar.CON, Linhaconciliar.CCD, null, "D�bito autom�tico n�o cadastrado nos peri�dicos", DescritivoErro);
                    }
                }
                else
                {
                    //GENERICO
                    int? PGF = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(ComandoBuscaGererico, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CON);
                    //System.Data.DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoBuscaGererico, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CON);
                    if (PGF.HasValue)
                    {
                        AbstratosNeon.ABS_PagamentoPeriodico PagamentoPeriodico1 = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(PGF.Value);
                        if (PagamentoPeriodico1.Encontrado)
                            if (PagamentoPeriodico1.ConciliaDebitoAutomatico(Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CCD))
                            {
                                Resposta = RegistroAntigoLancBradesco(Linhaconciliar, PagamentoPeriodico1.PLA, PagamentoPeriodico1.Descricao, false);
                                //if (Resposta == statusconsiliado.Automatico)
                                //    Classificacao = TipoLancamentoNeon.debitoautomatico;
                            }
                    }
                }
            }            
            return Resposta;
        }

        private statusconsiliado ConciliachequeEletronico(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            Linhaconciliar.CCDErro = "";
            statusconsiliado RetornoConciliacheque = statusconsiliado.Automatico;            
            List<dllCheques.Cheque> Cheques = dllCheques.Cheque.BuscaChequesEletronicosSomados(Linhaconciliar.CCT, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor);            
            if (Cheques != null)
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("ConciliachequeEletronico - 1670");
                    foreach (dllCheques.Cheque Cheque in Cheques)
                    {
                        if (!Cheque.Baixar(Linhaconciliar.CCD, Linhaconciliar.SCCData, Linhaconciliar.CCDCategoria, false))
                        {
                            Linhaconciliar.CCDErro = Cheque.UltimaException.Message;
                            throw new Exception("Erro na baixa:", Cheque.UltimaException);
                        }
                    }
                    VirMSSQL.TableAdapter.CommitSQL();
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(e);
                    return statusconsiliado.Aguardando;
                }
            }
            else
            {
                Linhaconciliar.CCDErro = "Pagamento Eletr�nico n�o encontrado";
                return statusconsiliado.Aguardando;
                //AVISO               
            };
            return RetornoConciliacheque;

        }

        private statusconsiliado Conciliacheque(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            Linhaconciliar.CCDErro = "";
            statusconsiliado RetornoConciliacheque = statusconsiliado.Automatico;
            dllCheques.Cheque Cheque;
            Cheque = new dllCheques.Cheque(Linhaconciliar.CCD_SCC, Linhaconciliar.CCDDocumento, false);
            if (Cheque.Encontrado)
            {
                if (!Cheque.CHErow.IsCHE_CCDNull())
                {
                    Linhaconciliar.CCDErro = "Cheque j� compensado";
                    return statusconsiliado.Aguardando;
                    //AVISO
                };
                if (Cheque.CHErow.CHEValor != -Linhaconciliar.CCDValor)
                {
                    Linhaconciliar.CCDErro = String.Format("Valor errado: cheque:{0:n2} Extrato:{1:n2}", Cheque.CHErow.CHEValor, -Linhaconciliar.CCDValor);
                    return statusconsiliado.Aguardando;
                    //AVISO
                    
                    
                    /*
                    decimal erro = Cheque.CHErow.CHEValor + Linhaconciliar.CCDValor;
                    if (Math.Abs(erro) <= 2.0M)
                    {
                        if (!Cheque.Baixar(Linhaconciliar.CCD, Linhaconciliar.SCCData, Linhaconciliar.CCDCategoria, false))
                        {
                            Linhaconciliar.CCDErro = Cheque.UltimaException.Message;
                            return statusconsiliado.Aguardando;
                        }
                    }
                    else
                    {
                        Linhaconciliar.CCDErro = String.Format("Valor errado: cheque:{0:n2} Extrato:{1:n2}", Cheque.CHErow.CHEValor, -Linhaconciliar.CCDValor);
                        return statusconsiliado.Aguardando;
                        //AVISO
                    }
                    */
                }
                else
                    if (!Cheque.Baixar(Linhaconciliar.CCD, Linhaconciliar.SCCData, Linhaconciliar.CCDCategoria, false))
                    {
                        Linhaconciliar.CCDErro = Cheque.UltimaException.Message;
                        return statusconsiliado.Aguardando;
                    }
            }
            else
            {

                Linhaconciliar.CCDErro = "Cheque n�o encontrado";
                return statusconsiliado.Aguardando;
                //AVISO
            };
            return RetornoConciliacheque;
        }

        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
        private statusconsiliado ConciliaTributo(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            dGPS.dGPSSt.GPSTableAdapter.FillByCONDataPagtoValorPagtoCodigo(dGPS.dGPSSt.GPS, Linhaconciliar.CON, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Convert.ToInt32(Linhaconciliar.CCDDocumento.ToString("000000").Substring(2,4)));
            if (dGPS.dGPSSt.GPS.Count > 0)
            {
                dGPS.GPSRow rowGPS = dGPS.dGPSSt.GPS[0];
                if (!rowGPS.IsGPS_CCDNull())
                {
                    Linhaconciliar.CCDErro = "Tributo j� compensado";
                    return statusconsiliado.Aguardando;
                    //AVISO
                };

                try
                {
                    dGPS.dGPSSt.GPSTableAdapter.Baixar((int)StatusArquivo.Efetuado, Linhaconciliar.CCD, String.Format("{0:dd/MM/yy HH:mm:ss} - {1}", DateTime.Now, "Pagamento compensado"), rowGPS.GPS); 
                    Linhaconciliar.CCDErro = "";
                    return statusconsiliado.Automatico;
                }
                catch (Exception e)
                {
                    Linhaconciliar.CCDErro = e.Message;
                    return statusconsiliado.Aguardando;
                    //AVISO
                };
            }
            else
            {
                Linhaconciliar.CCDErro = "Tributo Eletr�nico n�o encontrado";
                return statusconsiliado.TributoNaoEncontrado;
                //AVISO
            };
       }
        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***

        private System.Collections.SortedList descricoesNaoEncontradas;

        /// <summary>
        /// 
        /// </summary>
        public System.Collections.SortedList DescricoesNaoEncontradas
        {
            get { return descricoesNaoEncontradas ?? (descricoesNaoEncontradas = new System.Collections.SortedList()); }
        }

        private statusconsiliado ConciliarPelaDescricao(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            if ((Linhaconciliar.CCT_BCO == 341) && (Linhaconciliar.CCDDescricao.Contains("RSHOP-")))
            {
                RegistroAntigoLancBradesco(Linhaconciliar, "213000", Linhaconciliar.CCDDescricao,true);
                return statusconsiliado.Automatico;
            }
            else
            {
                if (dConciliacao.Descricoes.Count == 0)
                    dConciliacao.DescricoesTableAdapter.Fill(dConciliacao.Descricoes);
                dConciliacao.DescricoesRow rowDesc = dConciliacao.Descricoes.FindByCategoria_lancamentoDescricao(Linhaconciliar.CCDCategoria.ToString("000"), Linhaconciliar.CCDDescricao);
                if ((rowDesc == null) || (rowDesc.IsTipoPagNull()))
                {

                    Linhaconciliar.CCDErro = "Descri��o n�o prevista";
                    if (!DescricoesNaoEncontradas.ContainsKey(Linhaconciliar.CCDDescricao))
                        DescricoesNaoEncontradas.Add(Linhaconciliar.CCDDescricao, (int)1);
                    else
                        DescricoesNaoEncontradas[Linhaconciliar.CCDDescricao] = 1 + (int)DescricoesNaoEncontradas[Linhaconciliar.CCDDescricao];
                    return statusconsiliado.Aguardando;
                }
                else
                {
                    string descricao;
                    if (rowDesc.TipoPag == "213000")
                        descricao = Linhaconciliar.CCDDescricao;
                    else
                        descricao = Framework.datasets.dPLAnocontas.dPLAnocontasSt.GetDescricao(rowDesc.TipoPag);
                    return RegistroAntigoLancBradesco(Linhaconciliar, rowDesc.TipoPag, descricao, true);
                    
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Linhaconciliar"></param>
        /// <param name="PLA"></param>
        /// <param name="descricao"></param>
        /// <param name="ComTeste"></param>
        /// <returns></returns>
        public statusconsiliado RegistroAntigoLancBradesco(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar, string PLA, string descricao,bool ComTeste) {
            return RegistroAntigoLancBradesco(Linhaconciliar, PLA, descricao, Linhaconciliar.CCDValor,ComTeste);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Linhaconciliar"></param>
        /// <param name="PLA"></param>
        /// <param name="descricao"></param>
        /// <param name="Valor"></param>
        /// <param name="ComTeste"></param>
        /// <returns></returns>
        public statusconsiliado RegistroAntigoLancBradesco(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar,string PLA,string descricao,decimal Valor,bool ComTeste)
        {            
            //RegistroAntigoLancBradesco(Linhaconciliar.CONCodigo, Math.Abs(-(float)Valor), Linhaconciliar.CCDCategoria, Linhaconciliar.SCCData, PLA, descricao, (double)Linhaconciliar.CCDDocumento, Linhaconciliar.CCD, Linhaconciliar.CCDDescricao,"");
            Linhaconciliar.CCDErro = "Ok";
            return statusconsiliado.Automatico;
        }

        /*
        private void RegistroAntigoLancBradesco(string Codcon,float Valor,int Categoria,DateTime Data,string PLA,string descricao,double Documento,int CCD,string DescricaoOrig,string Competencia) 
        {
            
            dConciliacao.Lancamentos_BradescoRow novaLBrow = dConciliacao.Lancamentos_Bradesco.NewLancamentos_BradescoRow();
            novaLBrow.Codcon = Codcon;
            novaLBrow.Valor_Pago = Valor;
            novaLBrow.Cr�dito = (Categoria > 199);
            novaLBrow.C�digo_Categoria = Categoria.ToString("000");
            novaLBrow.Data_de_Leitura = DateTime.Today;
            novaLBrow.Data_de_Lan�amento = Data;
            novaLBrow.Tipopag = PLA;
            novaLBrow.Descri��o = descricao;
            novaLBrow.N�mero_Documento = Documento;
            novaLBrow.CCD = CCD;
            if (Competencia != "")
                novaLBrow.M�s_Compet�ncia = Competencia;
            else
                novaLBrow.M�s_Compet�ncia = Data.ToString("MMyyyy");
            dConciliacao.Lancamentos_Bradesco.AddLancamentos_BradescoRow(novaLBrow);
            dConciliacao.Lancamentos_BradescoTableAdapter.Update(novaLBrow);
            novaLBrow.AcceptChanges();
            if (PLA == "260003")
            {
                dConciliacao.Cheques_DevolvidosRow novaDev = dConciliacao.Cheques_Devolvidos.NewCheques_DevolvidosRow();
                novaDev.Codcon = Codcon;
                novaDev.Data = Data;
                novaDev.Descri��o = DescricaoOrig;
                novaDev.N�mero_Documento = Documento;
                novaDev.Valor = -(double)Valor;
                dConciliacao.Cheques_Devolvidos.AddCheques_DevolvidosRow(novaDev);
                dConciliacao.Cheques_DevolvidosTableAdapter.Update(novaDev);
                novaDev.AcceptChanges();
            }
        }*/

        private int[] Grupo;
        private decimal[] Valores;
        private decimal meta;

        /// <summary>
        /// Esta funcao testa de forma recursiva todas as combina��es dos boletos de forma a somar o cr�dito
        /// </summary>        
        /// <param name="PosicaoAtestar">indice do vetor a ser testado na chamada recursiva</param>
        /// <param name="de">base do teste</param>
        /// <param name="ate">teto do teste e dimensao do vetor - o teto � o mesmo pois a cobinacao � de nXn</param>
        /// <param name="Acumulado">soma dos boletos selecionados as posicoes anteriores do vetor</param>        
        /// <returns></returns>
        private bool GrupoValor(int PosicaoAtestar, int de ,int ate,decimal Acumulado/*,decimal meta*/) {
            //ATENCAO o 'ate' significa duas coisas: o tamanho do vetor e o numero de iten. 
            if ((DateTime.Now - InicioAmarra).TotalMinutes >= 2)
            {
                interrompido = true;
                if (SBRetorno != null)
                    SBRetorno.AppendLine(string.Format("Amarra��o TIME-OUT: {0} s\r\n", (DateTime.Now - InicioAmarra).TotalSeconds));
                return false;
            }
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Teste Tempo");
            //if (LimiteTeste < DateTime.Now)
            //{
            //    interrompido = true;
            //    return true;
            //}
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Teste Inicial");
            if (Acumulado == meta)
            {
                for (int i = PosicaoAtestar; i <= ate; i++) //loop nas posicoes do vetor
                    Grupo[i] = -1;
                return true;
            }
            else if (Acumulado < meta)
            {
                if (PosicaoAtestar > ate)
                    return false;
                else
                {
                    //CompontesBasicos.Performance.Performance.PerformanceST.Registra("FOR");
                    decimal Anterior = 0;
                    for (int Val_i = de; Val_i <= ate; Val_i++) //loop no coteudo da PosicaoAtestar
                    {
                        if (Valores[Val_i] == Anterior)
                            continue;
                        Anterior = Valores[Val_i];
                        Grupo[PosicaoAtestar] = Val_i;
                        //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Chamada");
                        
                        if (GrupoValor(PosicaoAtestar + 1, Val_i + 1, ate, Acumulado + Valores[Val_i] /*, meta*/ ))
                        {                            
                            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Retorna OK");
                            return true;
                        };
                        if (interrompido)
                            break;
                        //CompontesBasicos.Performance.Performance.PerformanceST.Registra("FOR");
                    };
                    //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Retorna Falso");
                    return false;
                }
            }
            else
            {
                //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Retorna Falso");
                return false;
            }
        }

        private DateTime InicioAmarra;
        bool interrompido = false;

        private bool AjustaDataFrancesinhas(DateTime Data,int CCT,decimal Valor)
        {
            if (Data <= DateTime.Today.AddDays(-5))
            {
                string comandoVerificaTotal =
    "SELECT        SUM(ARLValor) AS Expr1\r\n" +
    "FROM            dbo.ARquivoLido\r\n" +
    "WHERE        (ARL_CCT = @P1) AND (ARL_CCD IS NULL) AND (ARL_BOLPagamento BETWEEN @P2 AND @P3);";
                decimal? ValorEmAberto = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_decimal(comandoVerificaTotal, CCT, Data.AddDays(-5), Data.AddDays(4));
                if (ValorEmAberto.HasValue && (ValorEmAberto.Value == Valor))
                {
                    string comandoAjuste1 =
"UPDATE      BOLetos\r\n" +
"SET         BOLPagamento = @P1\r\n" +
"FROM        ARquivoLido INNER JOIN\r\n" +
"            BOLetos ON ARquivoLido.ARL = BOLetos.BOL_ARL\r\n" +
"WHERE    (ARL_CCT = @P2) AND   (ARL_CCD IS NULL) AND (ARL_BOLPagamento BETWEEN @P3 AND @P4) ;";
                    string comandoAjuste2 =
"UPDATE ARquivoLido\r\n" +
"SET    ARL_BOLPagamento = @P1\r\n" +
"FROM   ARquivoLido                                    \r\n" +
"WHERE  (ARL_CCT = @P2) AND   (ARL_CCD IS NULL) AND (ARL_BOLPagamento BETWEEN @P3 AND @P4) ;";
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoAjuste1, Data, CCT, Data.AddDays(-5), Data.AddDays(4));
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoAjuste2, Data, CCT, Data.AddDays(-5), Data.AddDays(4));
                    return true;
                }
            }
            return false;
        }

        private statusconsiliado AmarrarBoletos(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            if (Linhaconciliar.CCT_BCO == 104)
                return statusconsiliado.Aguardando;
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("202 Amarrar");
            InicioAmarra = DateTime.Now;
            interrompido = false;
            dConciliacao.ARquivoLidoTableAdapter.EmbarcaEmTransST();
            dConciliacao.ARquivoLidoTableAdapter.FillByCCT(dConciliacao.ARquivoLido, Linhaconciliar.SCCData, Linhaconciliar.CCT);
            int TotBol = dConciliacao.ARquivoLido.Count;
            Grupo = new int[TotBol];
            Valores = new decimal[TotBol];
            decimal TotalCandidatos = 0;
            bool Complementar = false;
            for (int i = 0; i < TotBol; i++)
            {
                if (Linhaconciliar.CCT_BCO == 341)
                    Valores[i] = dConciliacao.ARquivoLido[i].ARLValor + dConciliacao.ARquivoLido[i].ARLTar;
                else
                    Valores[i] = dConciliacao.ARquivoLido[i].ARLValor;
                TotalCandidatos += Valores[i];
            }
            meta = Linhaconciliar.CCDValor;

            if (meta != TotalCandidatos)
            {
                if (meta > TotalCandidatos)
                {
                    if (AjustaDataFrancesinhas(Linhaconciliar.SCCData, Linhaconciliar.CCT,meta))
                    {
                        dConciliacao.ARquivoLidoTableAdapter.FillByCCT(dConciliacao.ARquivoLido, Linhaconciliar.SCCData, Linhaconciliar.CCT);
                        TotBol = dConciliacao.ARquivoLido.Count;
                        Grupo = new int[TotBol];
                        Valores = new decimal[TotBol];
                        TotalCandidatos = 0;                        
                        for (int i = 0; i < TotBol; i++)
                        {
                            if (Linhaconciliar.CCT_BCO == 341)
                                Valores[i] = dConciliacao.ARquivoLido[i].ARLValor + dConciliacao.ARquivoLido[i].ARLTar;
                            else
                                Valores[i] = dConciliacao.ARquivoLido[i].ARLValor;
                            TotalCandidatos += Valores[i];
                        }
                    }
                    else
                        return statusconsiliado.Aguardando;
                }
                if (meta > (TotalCandidatos / 2))
                {
                    meta = TotalCandidatos - Linhaconciliar.CCDValor;
                    Complementar = true;
                }
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("202 Busca");
            if (GrupoValor(0, 0, TotBol - 1, 0.0M/*, Linhaconciliar.CCDValor*/))
            {
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("202 Amarrar");
                ArrayList Encontrados = new ArrayList();
                for (int i = 0; i < TotBol; i++)
                    if (Grupo[i] > -1)
                        Encontrados.Add(Grupo[i]);
                for (int i = 0; i < TotBol; i++)
                    if ((Encontrados.Contains(i) && !Complementar)
                        ||
                        (!Encontrados.Contains(i) && Complementar)
                       )
                    {
                        dConciliacao.ARquivoLido[i].ARL_CCD = Linhaconciliar.CCD;
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("202 Update 1");
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("UPDATE BOLetos SET BOL_CCD = @P1 WHERE (BOL_ARL = @P2);", Linhaconciliar.CCD, dConciliacao.ARquivoLido[i].ARL);
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("202 Amarrar");
                    }
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("202 Gravacao");
                dConciliacao.ARquivoLidoTableAdapter.Update(dConciliacao.ARquivoLido);
                dConciliacao.ARquivoLido.AcceptChanges();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("202 Amarrar");
                if (SBRetorno != null)
                    SBRetorno.AppendLine(string.Format("Amarra��o OK: {0} s\r\n", (DateTime.Now - InicioAmarra).TotalSeconds));

                return statusconsiliado.Automatico;
            }
            else
            {
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("202 Amarrar");
                Linhaconciliar.CCDErro = "N�o encotrada francesinha com este valor";
                return statusconsiliado.Aguardando;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int CarregaPendentes() 
        {
            return dConciliacao.ContaCorrenteDetalheTableAdapter.FillPendentes(dConciliacao.ContaCorrenteDetalhe);
        }


        //[Obsolete("Usar sem parametros")]
        //public void VerificarDebitoAutomaticoPendente(DateTime DataCorte)
        //{
        //    VerificarDebitoAutomaticoPendente();
        //}

        /// <summary>
        /// Verifica os d�bitos autom�ticos n�o ocorridos
        /// </summary>       
        public void VerificarDebitoAutomaticoPendente()
        {            
            DateTime DataCorte = Framework.datasets.dFERiados.DiasUteis(DateTime.Today, 4, Framework.datasets.dFERiados.Sentido.retorna, Framework.datasets.dFERiados.Sabados.naoUteis, Framework.datasets.dFERiados.Feriados.naoUteis);                        
            dConciliacao.DebitoNaoOcorridoTableAdapter.Fill(dConciliacao.DebitoNaoOcorrido,DateTime.Today);
            dConciliacao.ExtratoAteTableAdapter.Fill(dConciliacao.ExtratoAte);
            foreach (dConciliacao.DebitoNaoOcorridoRow rowNao in dConciliacao.DebitoNaoOcorrido)
            {                
                bool dispararAlarme = (rowNao.CHEVencimento < DataCorte);
                if (!dispararAlarme)
                {
                    dConciliacao.ExtratoAteRow Extrow = dConciliacao.ExtratoAte.FindByCON(rowNao.CHE_CON);
                    if((Extrow != null) && ((Extrow.MaxData.Date.AddDays(-1)) > rowNao.CHEVencimento.Date))
                        dispararAlarme = true;
                }
                if (dispararAlarme)
                {
                    Framework.Alarmes.Aviso Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoOcorreu, rowNao.CHE);
                    if (!Aviso.Encontrado)
                    {
                        Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoOcorreu, rowNao.CHE_CON, rowNao.CHE, new Framework.objetosNeon.Competencia(rowNao.NOACompet), "D�bito autom�tico agendado n�o encontrado", string.Format("{0:dd/MM/yyyy HH:mm:ss} - Cadastro do alarme\r\n", DateTime.Now));
                    }
                }
            }
            //DataCorte = Framework.datasets.dFERiados.DiasUteis(DateTime.Today, 4, Framework.datasets.dFERiados.Sentido.avanca, Framework.datasets.dFERiados.Sabados.naoUteis, Framework.datasets.dFERiados.Feriados.naoUteis);                        
            dConciliacao.DebitoAutomatico2TableAdapter.Fill(dConciliacao.DebitoAutomatico2, DataCorte);
            foreach (dConciliacao.DebitoAutomatico2Row rowDeb in dConciliacao.DebitoAutomatico2)
            {                                                
                    Framework.Alarmes.Aviso Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoOcorreu1, rowDeb.PGF);
                    if (!Aviso.Encontrado)
                    {
                        string Mensagem = "D�bito autom�tico n�o ocorreu";
                        AbstratosNeon.ABS_PagamentoPeriodico per = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(rowDeb.PGF);
                        if ((per != null) && (per.Encontrado))
                            per.Salvar("Alarme disparado");
                        Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoOcorreu1, rowDeb.PGF_CON, rowDeb.PGF, new Framework.objetosNeon.Competencia(rowDeb.AGBCompetenciaMes,rowDeb.AGBCompetenciaAno), Mensagem, string.Format("{0:dd/MM/yyyy HH:mm:ss} - Cadastro do alarme\r\n", DateTime.Now));
                    }                
            }
        }

        private statusconsiliado ConciliaSalarios(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            Linhaconciliar.CCDErro = "";
            statusconsiliado RetornoConciliacheque = statusconsiliado.Automatico;            
            List<dllCheques.Cheque> Cheques = dllCheques.Cheque.BuscaChequesEletronicosSomados(Linhaconciliar.CCT, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor,true);
            if ((Cheques == null) && (Linhaconciliar.SCCData.SomaDiasUteis(1) < DateTime.Today))
            {
                for (int i = 1; i <= 6; i++)
                {
                    Cheques = dllCheques.Cheque.BuscaChequesEletronicosSomados(Linhaconciliar.CCT, Linhaconciliar.SCCData.AddDays(-i), -Linhaconciliar.CCDValor, true);
                    if (Cheques != null)
                        break;
                }
            }
            if (Cheques != null)
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("ConciliachequeEletronico - 1330");
                    foreach (dllCheques.Cheque Cheque in Cheques)
                    {
                        if (!Cheque.Baixar(Linhaconciliar.CCD, Linhaconciliar.SCCData, Linhaconciliar.CCDCategoria, false))
                        {
                            Linhaconciliar.CCDErro = Cheque.UltimaException.Message;
                            throw new Exception("Erro na baixa:", Cheque.UltimaException);
                        }
                    }
                    VirMSSQL.TableAdapter.CommitSQL();
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(e);
                    return statusconsiliado.Aguardando;
                }
            }
            else
            {
                Linhaconciliar.CCDErro = "Pagamento de sal�rios n�o encotrado";
                return statusconsiliado.Aguardando;
                //AVISO               
            };
            return RetornoConciliacheque;

        }

        //public string Teste;

        private statusconsiliado ConsolidarEfetivo(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar, ref TipoLancamentoNeon Classificacao) 
        {
            
            int? FRN = null;
            long Codigo = 0;
            statusconsiliado Resposta =statusconsiliado.Aguardando;

            //o retorno deve ser concentrado nesta rotina

            if (TransferenciaInterna(Linhaconciliar.CCT_BCO, Linhaconciliar.CCDDescricao, Linhaconciliar.CCDCategoria, Linhaconciliar.CCDCodHistorico))
            { 
                Classificacao = TipoLancamentoNeon.TransferenciaInterna;
                    return statusconsiliado.Automatico;                       
            }

            switch (CodigosDeRetorno(Linhaconciliar.CCT_BCO, Linhaconciliar.CCDCategoria, Linhaconciliar.CCDCodHistorico))                
            {
                case TipoLancamentoNeon.Estorno:
                    Classificacao = TipoLancamentoNeon.Estorno;
                    return statusconsiliado.Automatico;
                case TipoLancamentoNeon.DespesasBancarias:                    
                    Classificacao = TipoLancamentoNeon.DespesasBancarias;
                    return statusconsiliado.Automatico;                   
                case TipoLancamentoNeon.Salarios:
                    Classificacao = TipoLancamentoNeon.Salarios;
                    return ConciliaSalarios(Linhaconciliar);
                case TipoLancamentoNeon.TransferenciaInterna:
                    Classificacao = TipoLancamentoNeon.TransferenciaInterna;
                    return statusconsiliado.Automatico;                       
                case TipoLancamentoNeon.creditodecobranca:
                    if (AmarrarBoletos(Linhaconciliar) ==statusconsiliado.Automatico)
                    {
                        Classificacao = TipoLancamentoNeon.creditodecobranca;
                        return statusconsiliado.Automatico;
                    }
                    break;
                case TipoLancamentoNeon.resgate:
                    {
                        Classificacao = TipoLancamentoNeon.resgate;
                        return statusconsiliado.Automatico;
                    }
                case TipoLancamentoNeon.PagamentoEletronico:                    
                    statusconsiliado Conciliacao = ConciliachequeEletronico(Linhaconciliar);
                    if (Conciliacao == statusconsiliado.Automatico)
                        Classificacao = TipoLancamentoNeon.PagamentoEletronico;                                            
                    return Conciliacao;
                case TipoLancamentoNeon.Cheque:                    
                    statusconsiliado Conciliacao1 = Conciliacheque(Linhaconciliar);
                    if (Conciliacao1 == statusconsiliado.Automatico)
                    {
                        Classificacao = TipoLancamentoNeon.Cheque;
                        return Conciliacao1;
                    }                                        
                    break;                
                case TipoLancamentoNeon.Tributo:
                    Classificacao = TipoLancamentoNeon.Tributo;
                    return ConciliaTributo(Linhaconciliar);                
                case TipoLancamentoNeon.rentabilidade:
                    {
                        Classificacao = TipoLancamentoNeon.rentabilidade;
                        return statusconsiliado.Automatico;
                    }
                case TipoLancamentoNeon.debitoautomatico:
                    statusconsiliado Conciliacao2 = ConciliaDebitoAutomatico(Linhaconciliar);
                    if (Conciliacao2 == statusconsiliado.Automatico)                    
                        Classificacao = TipoLancamentoNeon.debitoautomatico;                                            
                    return Conciliacao2;                    
                case TipoLancamentoNeon.deposito:
                    Classificacao = TipoLancamentoNeon.deposito;
                    return statusconsiliado.Aguardando;
            }

            //se nao foi tratado pelo processo novo

            switch ((CCDCategoria)Linhaconciliar.CCDCategoria)
            {
                case CCDCategoria.Cheque:                                        
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("101");
                    Resposta = Conciliacheque(Linhaconciliar);
                    if (Resposta ==statusconsiliado.Automatico)                        
                            Classificacao = TipoLancamentoNeon.Cheque;                                                                                                
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("default");                                                           
                    break;
                case CCDCategoria.DebitoAutomatico102:

                    //resgate autom�tico
                    if ((Linhaconciliar.CCT_BCO == 237) && (Linhaconciliar.CCDCodHistorico == 670))
                    {
                        Classificacao = TipoLancamentoNeon.resgate;
                        Resposta =statusconsiliado.Automatico;
                        break;
                    }


                    //ONENOTE debito passo 2
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("102");
                    FRN = null;
                    Codigo = 0;
                    dConciliacao.PaGamentosFixos.Clear();
                    if (!Linhaconciliar.IsCCDDescComplementoNull())
                    {
                        if (Linhaconciliar.CCDDescComplemento.Contains("ELETROPAULO"))
                        {

                            FRN = Framework.DSCentral.EMP == 1 ? 1499 : 188;

                            if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            {
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                                //caso do bota
                                if ((dConciliacao.PaGamentosFixos.Count == 0) && (Linhaconciliar.CON == 629))
                                {
                                    for (int ibota = 615; ibota <= 626; ibota++)
                                    {
                                        dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, ibota, FRN, Codigo);
                                        if (dConciliacao.PaGamentosFixos.Count == 1)
                                            break;
                                    }
                                }


                            }
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("SABESP"))
                        {
                            FRN = 1595;
                            if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            {
                                Codigo = Codigo / 100;
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                                //caso do bota
                                if ((dConciliacao.PaGamentosFixos.Count == 0) && (Linhaconciliar.CON == 629))
                                {
                                    for (int ibota = 615; ibota <= 626; ibota++)
                                    {
                                        dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, ibota, FRN, Codigo);
                                        if (dConciliacao.PaGamentosFixos.Count == 1)
                                            break;
                                    }
                                }


                            }
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("SANED"))
                        {
                            FRN = 2865;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Linhaconciliar.CCDDocumento);
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("COMGAS"))
                        {
                            FRN = 602;
                            if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            {
                                Codigo = Codigo % 10000000;
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            }
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("ULTRAGAZ"))
                        {
                            FRN = 190;
                            if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            {
                                Codigo = Codigo / 10;
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            }
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("EBT LIVRE"))
                        {
                            FRN = 852;
                            if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            {
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            }
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("NEXTEL"))
                        {
                            FRN = 358;
                            if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            {
                                Codigo = Codigo / 10;
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            }
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("CLARO"))
                        {
                            FRN = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select FRN from FORNECEDORES where FRNCodigoDebito = 162");
                            if (FRN.HasValue)
                            //FRN = 2273;
                            {
                                string manobra = StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros });

                                if (long.TryParse(manobra, out Codigo))
                                    dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);

                                if ((dConciliacao.PaGamentosFixos.Count == 0) && (manobra.Length > 2) && (long.TryParse(manobra.Substring(2), out Codigo)))
                                {
                                    dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                                }
                            }

                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("NET SERVICOS"))
                        {
                            FRN = 330;
                            if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                                Codigo = Codigo / 10;
                            Codigo = Codigo % 1000000;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            //Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                            //dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("TIM CELULAR"))
                        {
                            FRN = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select FRN from FORNECEDORES where FRNCodigoDebito = 109");
                            if (FRN.HasValue)
                            {

                                Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                                Codigo = Codigo % 100000000000;
                                Codigo = Codigo / 10;
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            }
                        }
                        else if (Linhaconciliar.CCDDescComplemento.Contains("TELEFONICA"))
                        {
                            FRN = 1114;
                            if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescComplemento, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }


                    if (dConciliacao.PaGamentosFixos.Count == 1)
                    {
                        dConciliacao.PaGamentosFixosRow rowPGF = dConciliacao.PaGamentosFixos[0];
                        AbstratosNeon.ABS_PagamentoPeriodico pagPer = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(rowPGF.PGF);
                        if (pagPer.ConciliaDebitoAutomatico(Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CCD))
                        {
                            Resposta = RegistroAntigoLancBradesco(Linhaconciliar, rowPGF.PGF_PLA, rowPGF.PGFDescricao, false);
                            if (Resposta ==statusconsiliado.Automatico)
                                Classificacao = TipoLancamentoNeon.debitoautomatico;
                        }
                    }
                    else
                        //if ((FRN == 1499) || (FRN == 1595) || (FRN == 2865))
                        if(FRN.HasValue)
                        {
                            Framework.Alarmes.Aviso Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, Linhaconciliar.CCD);
                            if (!Aviso.Encontrado)
                            {
                                dCondominiosAtivos.CONDOMINIOSRow rowCON = dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(Linhaconciliar.CON);
                                System.Data.DataRow DRForn = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select FRNCnpj,FRNNome,FRNFantasia from FORNECEDORES where FRN = @P1", FRN.Value);                                
                                string DescritivoErro = string.Format("\r\nCondom�nio: {0} - {1}\r\n\r\nFornecedor: {2} - {3} / {4}\r\n\r\nData: {5:dd/MM/yyyy}\r\nDescri��o: {6}/{7}",                                                                      
                                                                      rowCON.CONCodigo,
                                                                      rowCON.CONNome,
                                                                      DRForn["FRNCnpj"] == DBNull.Value ? "" : (string)DRForn["FRNCnpj"],
                                                                      DRForn["FRNNome"] == DBNull.Value ? "" : (string)DRForn["FRNNome"],
                                                                      DRForn["FRNFantasia"] == DBNull.Value ? "" : (string)DRForn["FRNFantasia"],
                                                                      Linhaconciliar.SCCData,
                                                                      Linhaconciliar.CCDDescricao,
                                                                      Linhaconciliar.CCDDescComplemento);
                                Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, Linhaconciliar.CON, Linhaconciliar.CCD, null, "D�bito autom�tico n�o cadastrado nos peri�dicos", DescritivoErro);
                            }
                        }
                        else
                        {
                            if (Linhaconciliar.CCT_BCO == 341)
                            {
                                Classificacao = TipoLancamentoNeon.DespesasBancarias;
                                Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "240005", "Despesas Banc�rias", true);
                            }
                            else
                            {
                                Resposta = ConciliarPelaDescricao(Linhaconciliar);
                                if (Resposta ==statusconsiliado.Automatico)
                                    Classificacao = TipoLancamentoNeon.PelaDescricao;
                            }
                        }
                    break;
                case CCDCategoria.Estorno:
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("103");
                    Classificacao = TipoLancamentoNeon.Estorno;
                    Resposta = ConciliarPelaDescricao(Linhaconciliar);
                    //tratar amarra��o
                    break;
                case CCDCategoria.DebitoAutomatico104:
                case CCDCategoria.DebitoAutomatico112:
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("104");
                    FRN = 0;
                    Codigo = 0;
                    dConciliacao.PaGamentosFixos.Clear();
                    if (Linhaconciliar.CCDDescricao.Contains("ELETROPAULO"))
                    {
                        FRN = 1499;
                        long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo);
                        dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                    }
                    else if (Linhaconciliar.CCDDescricao.Contains("SABESP"))
                    {
                        FRN = 1595;
                        Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                        Codigo = Codigo / 100;
                        dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        //Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                        //dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                    }
                    else if (Linhaconciliar.CCDDescricao.Contains("ELETROPAULO"))
                    {
                        FRN = 1499;
                        Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                        dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                    }
                    else if (Linhaconciliar.CCDDescricao.Contains("DA  NET SERVICOS"))
                    {
                        FRN = 330;
                        Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                        Codigo = Codigo/10;                        
                        dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        //Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                        //dConciliacao.PaGamentosFixosTableAdapter.FillEletro(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo * 100, Codigo * 100 + 99);
                    }
                    else if (Linhaconciliar.CCDDescricao.Contains("TELEFONICA"))
                    {
                        FRN = 1114;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        else
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("CCD = {0} - {1} ", Linhaconciliar.CCD, Linhaconciliar.CCDDescricao), "Erro para verificar");
                        

                        //Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));                        
                        //dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);                        
                    }
                    else if (Linhaconciliar.CCDDescricao.Contains("COMGAS"))
                    {
                        FRN = 602;
                        if (long.TryParse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }), out Codigo))
                        {
                            Codigo = Codigo % 10000000;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                        }
                    }
                    else if (Linhaconciliar.CCDDescricao.Contains("DA  TIM"))
                    {
                        FRN = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select FRN from FORNECEDORES where FRNCodigoDebito = 109");
                        if (FRN.HasValue)
                        {

                            Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                            Codigo = Codigo % 100000000000;
                            Codigo = Codigo / 10;
                            dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);                            
                        }
                    }
                    else if (Linhaconciliar.CCDDescricao.Contains("DA  CLARO"))
                    {
                        FRN = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select FRN from FORNECEDORES where FRNCodigoDebito = 162");
                        if (FRN.HasValue)
                        //FRN = 2273;
                        {
                            string manobra = StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros });

                            if (long.TryParse(manobra, out Codigo))
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);

                            if ((dConciliacao.PaGamentosFixos.Count == 0) && (manobra.Length > 2) && (long.TryParse(manobra.Substring(2), out Codigo)))
                            {
                                dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);
                            }
                        }
                    }
                    
                    //GENERICO
                    if(dConciliacao.PaGamentosFixos.Count == 0)
                    {
                        int? PGF = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(ComandoBuscaGererico, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CON);
                        //System.Data.DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoBuscaGererico, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CON);
                        if (PGF.HasValue)
                        {                            
                            AbstratosNeon.ABS_PagamentoPeriodico PagamentoPeriodico1 = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(PGF.Value);                            
                            if (PagamentoPeriodico1.Encontrado)
                                if (PagamentoPeriodico1.ConciliaDebitoAutomatico(Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CCD))
                                {
                                    Resposta = RegistroAntigoLancBradesco(Linhaconciliar, PagamentoPeriodico1.PLA, PagamentoPeriodico1.Descricao, false);
                                    if (Resposta ==statusconsiliado.Automatico)
                                        Classificacao = TipoLancamentoNeon.debitoautomatico;
                                }                            
                        }
                    }

                    if (dConciliacao.PaGamentosFixos.Count >= 1)
                    {
                        dConciliacao.PaGamentosFixosRow rowPGF = null;
                        if (dConciliacao.PaGamentosFixos.Count == 1)
                            rowPGF = dConciliacao.PaGamentosFixos[0];
                        else
                        {
                            //DESAMBIGUA��O:
                            //no caso do Bradesco o c�digo vem truncado criando um umbiguidade que deve ser resolvida primeiro procurando o pagamento agendado e em segundo pelo valor nominal
                            int? PGF = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(ComandoBuscaGererico, Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CON);
                            if (PGF.HasValue)
                                rowPGF = dConciliacao.PaGamentosFixos.FindByPGF(PGF.Value);
                            if(rowPGF == null)
                                foreach (dConciliacao.PaGamentosFixosRow rowCandito in dConciliacao.PaGamentosFixos)
                                {
                                    if(rowCandito.PGFValor == -Linhaconciliar.CCDValor)
                                        rowPGF = rowCandito;
                                    //AbstratosNeon.ABS_PagamentoPeriodico pagPerCandidato = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(rowCandito.PGF);                                    
                                    //if (pagPerCandidato.ValorProximoDebitoProgramado() == -Linhaconciliar.CCDValor)
                                    //    rowPGF = rowCandito;
                                }
                        }
                        if (rowPGF != null)
                        {
                            AbstratosNeon.ABS_PagamentoPeriodico pagPer = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(rowPGF.PGF);
                            //ContaPagar.Follow.PagamentoPeriodico pagPer = new ContaPagar.Follow.PagamentoPeriodico(rowPGF.PGF);
                            if (pagPer.ConciliaDebitoAutomatico(Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CCD))
                            {
                                Resposta = RegistroAntigoLancBradesco(Linhaconciliar, rowPGF.PGF_PLA, rowPGF.PGFDescricao, false);
                                if (Resposta ==statusconsiliado.Automatico)
                                    Classificacao = TipoLancamentoNeon.debitoautomatico;
                            }
                        }
                        else
                            return statusconsiliado.Aguardando;
                    }

                    else
                    {
                        if (Linhaconciliar.CCT_BCO == 341)
                        {
                            if (Linhaconciliar.CCDDescricao.Contains(String.Format("CHQ  {0:000000}", Linhaconciliar.CCDDocumento)))
                            {
                                Resposta = Conciliacheque(Linhaconciliar);
                                if (Resposta ==statusconsiliado.Automatico)
                                    Classificacao = TipoLancamentoNeon.Cheque;
                            }
                            else if (Linhaconciliar.CCDDescricao.Contains("SABESP      "))
                            {
                                Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "202000", "SABESP", true);
                                if (Resposta ==statusconsiliado.Automatico)
                                    Classificacao = TipoLancamentoNeon.debitoautomatico;
                            }
                            else if (Linhaconciliar.CCDDescricao.Contains("ELETROPAULO   "))
                            {
                                Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "201000", "ELETROPAULO", true);
                                if (Resposta ==statusconsiliado.Automatico)
                                    Classificacao = TipoLancamentoNeon.debitoautomatico;
                            }

                        }
                        if (Linhaconciliar.CCT_BCO == 409)
                        {
                            if (Linhaconciliar.CCDDescricao.Contains("*DEB.SABESP"))
                            {
                                Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "202000", "SABESP", true);
                                if (Resposta ==statusconsiliado.Automatico)
                                    Classificacao = TipoLancamentoNeon.debitoautomatico;
                            }
                            else if (Linhaconciliar.CCDDescricao.Contains("*DEB.ELETROPAULO"))
                            {
                                Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "201000", "ELETROPAULO", true);
                                if (Resposta ==statusconsiliado.Automatico)
                                    Classificacao = TipoLancamentoNeon.debitoautomatico;
                            }

                        }
                    }
                    if (Resposta ==statusconsiliado.Aguardando)
                    {
                        Resposta = ConciliarPelaDescricao(Linhaconciliar);
                        if (Resposta ==statusconsiliado.Automatico)
                            Classificacao = TipoLancamentoNeon.PelaDescricao;
                    }
                    break;
                case CCDCategoria.Tarifas105://tarifas
                case CCDCategoria.Tarifas110://IOF                        
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("105");
                    Classificacao = TipoLancamentoNeon.DespesasBancarias;
                    Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "240005", "Despesas Banc�rias", true);
                    break;
                    /*
                case CCDCategoria.Salarios:
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("113");
                    Classificacao = TipoLancamentoNeon.Salarios;
                    Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "220001", "Salarios", true);
                    break;
                    */
                case CCDCategoria.ChequeDevolvido118:
                case CCDCategoria.ChequeDevolvido119:
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("118");
                    Classificacao = TipoLancamentoNeon.ChequeDevolvido_Terceiro;
                    Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "260003", "Devolucao Cheque", true);
                    break;
                case CCDCategoria.Deposito:
                    throw new Exception("Todos os casos de CCDCategoria.Deposito j� est�o tratados no processo novo!!");
                    //Classificacao = TipoLancamentoNeon.deposito;                    
                    //break;
                case CCDCategoria.Cobranca:
                case CCDCategoria.TranInt_Rentabilidade:
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("202");
                    if (Linhaconciliar.CCT_BCO == 341)
                    {
                        // tratar transferencia interna
                        if (Linhaconciliar.CCDDescricao.Contains("REND.LIQ MULTCTA"))
                        {
                            Classificacao = TipoLancamentoNeon.rentabilidade;
                            Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "170000", "REN.LIQ MULTCTA", true);
                        }
                        else
                        {
                            //(nao deveria chegar aqui (novo processo))
                            Resposta = AmarrarBoletos(Linhaconciliar);
                            if (Resposta ==statusconsiliado.Automatico)
                                Classificacao = TipoLancamentoNeon.creditodecobranca;
                            //fim (nao deveria chegar aqui (novo processo)) 
                        }
                    }
                    else if (Linhaconciliar.CCT_BCO == 104)
                    {
                        Resposta = AmarrarBoletos(Linhaconciliar);
                        if (Resposta ==statusconsiliado.Automatico)
                            Classificacao = TipoLancamentoNeon.creditodecobranca;
                        //Resposta =statusconsiliado.Automatico;
                    }
                    //else if ((Linhaconciliar.CCT_BCO == 237) && (Linhaconciliar.CCDCodHistorico == 617))
                    //{
                    //    Resposta =statusconsiliado.Automatico;
                    //    Classificacao = TipoLancamentoNeon.rentabilidade;                        
                    //}
                    else if ((Linhaconciliar.CCT_BCO == 237) && (Linhaconciliar.CCDCodHistorico == 30))
                    {
                        //(nao deveria chegar aqui (novo processo))
                        Resposta = AmarrarBoletos(Linhaconciliar);
                        if (Resposta ==statusconsiliado.Automatico)
                            Classificacao = TipoLancamentoNeon.creditodecobranca;
                        //fim (nao deveria chegar aqui (novo processo))
                    }
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Loop");
                    break;
                case CCDCategoria.Estorno203:
                case CCDCategoria.Estorno204:
                case CCDCategoria.Estorno215:
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("215");
                    Classificacao = TipoLancamentoNeon.Estorno;
                    //dConciliacao.PaGamentosFixos.Clear();
                    //
                    if (Linhaconciliar.CCDDescricao.Contains("EST "))
                    {
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Verificar se ocorreu tudo certo. No caso afirmativo remover esta linha do c�digo!\r\nDescri��o:{0}\r\nCCD:{1}", Linhaconciliar.CCDDescricao, Linhaconciliar.CCD), "estorno EFETUADO");
                        //procura linha de estorno
                        int linhasantes = 1;
                        bool terminado = false;
                        bool encontrado = false;
                        while (!terminado)
                        {
                            dConciliacao.ContaCorrenteDetalheDataTable TabeladeTeste = dConciliacao.ContaCorrenteDetalheTableAdapter.GETDataByCCD(Linhaconciliar.CCD - linhasantes++);
                            if (TabeladeTeste.Count == 1)
                            {
                                dConciliacao.ContaCorrenteDetalheRow LinhaEstornar = TabeladeTeste[0];
                                /*
                                 //string Compara1 = StringEdit.Limpa(Linhaconciliar.CCDDescricao.Substring(4), new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.RemoveBrancosDuplicados });
                                //string Compara2 = StringEdit.Limpa(LinhaEstornar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.RemoveBrancosDuplicados });
                                if ( (LinhaEstornar.CCDValor == -Linhaconciliar.CCDValor) 
                                      && 
                                     (Linhaconciliar.CCDDescricao.Substring(4,7).Trim() == LinhaEstornar.CCDDescricao.Substring(0,7).Trim())
                                      &&
                                     (Linhaconciliar.CCDDescricao.Substring(12).Trim() == LinhaEstornar.CCDDescricao.Substring(12).Trim())
                                    )
                                 */


                                string Compara1 = StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros });
                                string Compara2 = StringEdit.Limpa(LinhaEstornar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros });
                                statusconsiliado Status = (statusconsiliado)LinhaEstornar.CCDConsolidado;
                                if ( (LinhaEstornar.CCDValor == -Linhaconciliar.CCDValor) 
                                      && 
                                     (Compara1 == Compara2)
                                      &&
                                     (Status !=statusconsiliado.Aguardando)
                                      &&
                                     (!LinhaEstornar.IsCCDTipoLancamentoNull())
                                    )
                                {
                                    encontrado = true;
                                    terminado = true;
                                    TipoLancamentoNeon Tipo = (TipoLancamentoNeon)LinhaEstornar.CCDTipoLancamento;
                                    //statusconsiliado Status = (statusconsiliado)LinhaEstornar.CCDConsolidado;
                                    if (Tipo != TipoLancamentoNeon.debitoautomatico)
                                    {
                                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Estorno de tipo nao previsto.\r\nDescri��o:{0}\r\nCCD:{1}", Linhaconciliar.CCDDescricao, Linhaconciliar.CCD), "estorno n�o EFETUADO");
                                        return statusconsiliado.Aguardando;
                                    }
                                    if (Status ==statusconsiliado.Aguardando)
                                        return statusconsiliado.Aguardando;
                                    else if (Status ==statusconsiliado.Automatico)
                                    {
                                        dllCheques.Cheque ChequeEletronicoEstornar = new dllCheques.Cheque(LinhaEstornar.CCD, dllCheques.Cheque.TipoChave.CCD);
                                        if (!ChequeEletronicoEstornar.Encontrado)
                                            return statusconsiliado.Aguardando;
                                        if ((ChequeEletronicoEstornar.Status != CHEStatus.Compensado) || (ChequeEletronicoEstornar.Tipo != PAGTipo.DebitoAutomatico))
                                            return statusconsiliado.Aguardando;
                                        if (ChequeEletronicoEstornar.Estorna(LinhaEstornar.CCD))
                                        {
                                            Classificacao = TipoLancamentoNeon.Estorno;
                                            return statusconsiliado.Automatico;
                                        }
                                    }


                                }
                                else
                                    if (LinhaEstornar.CCD_SCC != Linhaconciliar.CCD_SCC)
                                        terminado = true;
                                    
                            }
                            else
                                terminado = true;
                        }
                        if(!encontrado)
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Linha de estorno n�o encontrada.\r\nDescri��o:{0}\r\nCCD:{1}", Linhaconciliar.CCDDescricao, Linhaconciliar.CCD), "estorno n�o EFETUADO");
                            //DEVERIA GERAR UM AVISO DE QUE O ESTORNO N�O FOI LOCALIZADO
                        //REFERENCIA ONENOTE : GERAR AVISO


                        //FRN = 1595;
                        //Codigo = long.Parse(StringEdit.Limpa(Linhaconciliar.CCDDescricao, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.SoNumeros }));
                        //Codigo = Codigo / 100;
                        //dConciliacao.PaGamentosFixosTableAdapter.Fill(dConciliacao.PaGamentosFixos, Linhaconciliar.CON, FRN, Codigo);

                    }
                    /* ... outros estornos
                    if (dConciliacao.PaGamentosFixos.Count >= 1)
                    {
                        dConciliacao.PaGamentosFixosRow rowPGF = null;
                        if (dConciliacao.PaGamentosFixos.Count == 1)
                            rowPGF = dConciliacao.PaGamentosFixos[0];
                        else
                        {
                            //verifircar se procede
                            foreach (dConciliacao.PaGamentosFixosRow rowCandito in dConciliacao.PaGamentosFixos)
                            {
                                ContaPagar.Follow.PagamentoPeriodico pagPerCandidato = new ContaPagar.Follow.PagamentoPeriodico(rowCandito.PGF);
                                if (pagPerCandidato.ValorProximoDebitoProgramado() == -Linhaconciliar.CCDValor)
                                    rowPGF = rowCandito;
                            }
                        }
                        if (rowPGF != null)
                        {
                            ContaPagar.Follow.PagamentoPeriodico pagPer = new ContaPagar.Follow.PagamentoPeriodico(rowPGF.PGF);
                            if (pagPer.CadastrarProximoDebitoAutomatico(Linhaconciliar.SCCData, -Linhaconciliar.CCDValor, Linhaconciliar.CCD))
                            {
                                Resposta = RegistroAntigoLancBradesco(Linhaconciliar, rowPGF.PGF_PLA, rowPGF.PGFDescricao, false);
                                if (Resposta ==statusconsiliado.Automatico)
                                    Classificacao = TipoLancamentoNeon.debitoautomatico;
                            }
                        }
                        else
                            return statusconsiliado.Aguardando;
                    }*/
                    else
                    {
                        Resposta = ConciliarPelaDescricao(Linhaconciliar);
                        //VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Descri��o:{0}\r\nCCD:{1}", Linhaconciliar.CCDDescricao, Linhaconciliar.CCD), "estorno n�o tratado");
                    }
                    
                    break;
                case CCDCategoria.Resgate:
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("206");
                    Classificacao = TipoLancamentoNeon.resgate;
                    Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "400000", "Resg.Aplic.Finan. Curto Prazo", true);
                    break;
                case CCDCategoria.Rendimento:
                
                    
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("214");
                    if (Linhaconciliar.CCT_BCO == 409)
                    {
                        if (Linhaconciliar.CCDDescricao.Contains("REND.LIQ MULTCTA"))
                        {
                            Classificacao = TipoLancamentoNeon.rentabilidade;
                            Resposta = RegistroAntigoLancBradesco(Linhaconciliar, "170000", "REN.LIQ MULTCTA", true);
                        }
                        else
                        {
                            Resposta = AmarrarBoletos(Linhaconciliar);
                            if (Resposta ==statusconsiliado.Automatico)
                                Classificacao = TipoLancamentoNeon.creditodecobranca;
                        }
                    }
                    
                    else
                    {
                        Resposta = ConciliarPelaDescricao(Linhaconciliar);
                        if (Resposta ==statusconsiliado.Automatico)
                            Classificacao = TipoLancamentoNeon.PelaDescricao;
                    }
                    break;
                default: //112 117 205 209 211 212 213
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("default");
                    Resposta = ConciliarPelaDescricao(Linhaconciliar);
                    if (Resposta ==statusconsiliado.Automatico)
                        Classificacao = TipoLancamentoNeon.PelaDescricao;
                    break;
            };

            return Resposta;
        }

        private string ComandoBuscaGererico =
"SELECT     NOA_PGF\r\n" +
"FROM         CHEques INNER JOIN\r\n" +
"                      PAGamentos ON CHEques.CHE = PAGamentos.PAG_CHE INNER JOIN\r\n" +
"                      NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA\r\n" +
"WHERE     (CHEques.CHEVencimento = @P1) AND (CHEques.CHEValor = @P2) AND (CHETipo = 8) and (CHEStatus = 10) AND \r\n" +
"                      (CHEques.CHE_CON = @P3);";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BCO"></param>
        /// <param name="iCCDCategoria"></param>
        /// <param name="CCDCodHistorico"></param>
        /// <returns></returns>
        public TipoLancamentoNeon CodigosDeRetorno(int BCO,int iCCDCategoria, int CCDCodHistorico)
        {            
            CCDCategoria CCDCategoria1 = (CCDCategoria)iCCDCategoria;
            switch (CCDCategoria1)
            {           
                case CCDCategoria.Salarios:
                    return TipoLancamentoNeon.Salarios;
                case CCDCategoria.Tarifas105:
                case CCDCategoria.Tarifas110:
                    return TipoLancamentoNeon.DespesasBancarias;
                case CCDCategoria.DebitoAutomatico102:
                    if (BCO == 237)
                        switch(CCDCodHistorico)
                        {
                            //case 670:
                            //    return XXX; baixa automatica
                            //case 787:
                            //    return XXX; PEND.TARIFAS BANCARIA    
                            //case 803:
                            //case 820:
                            //case 832:
                            //case 835:
                            //case 837:
                            //case 839:
                            //case 841:
                            //case 846:
                            //case 850:
                            //    return XXX; MORA
                            //case 922:    
                            //    ENCARGOS C GARANTIDA    
                            //case 928:
                            //    INSS DEBITO AUTOMATICO   
                            //case 941:
                            //    ENCARGO SALDO VINCULADO
                            //case 950
                            //    ENCARGOS LIMITE DE CRED  
                            //case 1777:
                            //    ENCARGOS EXCESSO LIMITE  
                            case 916:
                            case 920:
                            case 932:
                            case 935:
                            case 937:
                            case 978:
                            case 982:
                            case 983:
                            case 984:
                                return TipoLancamentoNeon.debitoautomatico;
                            case 950:
                                return TipoLancamentoNeon.DespesasBancarias;
                        }
                    break;
                case CCDCategoria.Cobranca:
                    if (BCO == 237)
                    {
                        if (CCDCodHistorico == 30)
                            return TipoLancamentoNeon.creditodecobranca;
                    }
                    if (BCO == 341)
                    {
                        if (CCDCodHistorico == 38)
                            return TipoLancamentoNeon.creditodecobranca;
                    }
                    break;
                case CCDCategoria.TransFContas117:
                case CCDCategoria.TransFContas213:
                    if ((BCO == 237) && ((CCDCodHistorico == 699) || (CCDCodHistorico == 299)))                    
                        return TipoLancamentoNeon.resgate;
                    if ((BCO == 237) && (CCDCodHistorico == 630))
                        return TipoLancamentoNeon.deposito;
                    break;
                case CCDCategoria.DebitoAutomatico104:
                    if ((BCO == 104) && (CCDCodHistorico == 611))
                        return TipoLancamentoNeon.Cheque;
                    if ((BCO == 104) && (CCDCodHistorico == 911))
                        return TipoLancamentoNeon.debitoautomatico;
                    //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
                    if ((BCO == 237) && (CCDCodHistorico == 907))
                        return TipoLancamentoNeon.Tributo;
                    //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
                    break;
                case CCDCategoria.IR_Rendimentos:
                    if ((BCO == 237) && (CCDCodHistorico == 607))
                        return TipoLancamentoNeon.rentabilidade;
                    break;
                case CCDCategoria.TranInt_Rentabilidade:
                    if ((BCO == 237) && (CCDCodHistorico == 617))
                        return TipoLancamentoNeon.rentabilidade;
                    if ((BCO == 237) && (CCDCodHistorico == 1734))
                        return TipoLancamentoNeon.Estorno;
                    break;                
                case CCDCategoria.DebitoAutomatico112:                    
                    if (BCO == 237)
                    {
                        //142 - PGTO.MEDIANTE AUT.DBTP  = carta para pagamento de adiantamento no momento n�o � tratado
                        //311 - PAGTO ELETRON  COBRANCA = CHEQUE S�NDICO ELETRONICO
                        if (CCDCodHistorico.EstaNoGrupo(358,539, 712,1076,1078,1079, 1130,1137, 1132,1142, 1145, 1146))
                            return TipoLancamentoNeon.PagamentoEletronico;
                        else if (CCDCodHistorico.EstaNoGrupo(69))
                            return TipoLancamentoNeon.debitoautomatico;
                    }
                    else if (BCO == 341)
                    {
                        if (CCDCodHistorico.EstaNoGrupo(69))
                            return TipoLancamentoNeon.debitoautomatico;
                    }
                    break;         
                case CCDCategoria.Deposito:
                case CCDCategoria.PagamentoFornecedores:                  
                    return TipoLancamentoNeon.deposito;                                       
                case CCDCategoria.TED_DOC:
                    if ((BCO == 237) && (CCDCodHistorico.EstaNoGrupo(282, 318, 1643, 1652)))// CODHistoricoTED_DOC.Contains(CCDCodHistorico))                    
                        return TipoLancamentoNeon.deposito;
                    else if ((BCO == 104) && (CCDCodHistorico == 812))
                        return TipoLancamentoNeon.deposito;                    

                    break;
            }
            return TipoLancamentoNeon.NaoIdentificado;
        }

        private bool BalanceteFechado(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {            
            if (dConciliacao.BALancetes.Count == 0)
            {
                dConciliacao.BALancetesTableAdapter.Fill(dConciliacao.BALancetes);
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("BalanceteFechado - FILL");
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("BalanceteFechado");
            dConciliacao.BALancetesRow BALrow = dConciliacao.BALancetes.FindByBAL_CON(Linhaconciliar.CON);
            if (BALrow == null)
                return (Linhaconciliar.SCCData <= DateTime.Today.AddMonths(-5));
            DateTime DataCorte = BALrow.DataCorte;
            if (DataCorte < DateTime.Today.AddMonths(-6))
                DataCorte = DateTime.Today.AddMonths(-6);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("default");
            return (Linhaconciliar.SCCData <= DataCorte);
        }

        /// <summary>
        /// 
        /// </summary>
        public string Retorno;
        /// <summary>
        /// 
        /// </summary>
        public int baixados;
        /// <summary>
        /// 
        /// </summary>
        public int antigos;
        /// <summary>
        /// 
        /// </summary>
        public int aguardo;
        /// <summary>
        /// 
        /// </summary>
        public int inativo;
        /// <summary>
        /// 
        /// </summary>
        public int chequesNao;
        /// <summary>
        /// 
        /// </summary>
        public int indiceConciliar;
        /// <summary>
        /// 
        /// </summary>
        public int descricaonaoencontrada;
        /// <summary>
        /// 
        /// </summary>
        public StringBuilder SBRetorno;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="CCD"></param>
        /// <param name="PGF"></param>
        /// <param name="Data"></param>
        /// <returns></returns>
        public override statusconsiliado ConciliarDebitoAutomatico(int CCD, int PGF,DateTime Data,Abstratos.ABS_Competencia competencia)
        {
            statusconsiliado Resposta =statusconsiliado.Aguardando;
            if (dConciliacao.ContaCorrenteDetalheTableAdapter.FillByCCD(dConciliacao.ContaCorrenteDetalhe, CCD) == 1)
            {
                dConciliacao.ContaCorrenteDetalheRow Linhaconciliar = dConciliacao.ContaCorrenteDetalhe[0];
                AbstratosNeon.ABS_PagamentoPeriodico pagPer = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(PGF);
                //ContaPagar.Follow.PagamentoPeriodico pagPer = new ContaPagar.Follow.PagamentoPeriodico(PGF);
                if (pagPer.ConciliaDebitoAutomatico(Data, -Linhaconciliar.CCDValor, Linhaconciliar.CCD,competencia))
                {
                    Resposta = RegistroAntigoLancBradesco(Linhaconciliar, pagPer.PLA, pagPer.Descricao, false);
                    if (Resposta ==statusconsiliado.Automatico)
                    {                        
                        Linhaconciliar.CCDTipoLancamento = (int)TipoLancamentoNeon.debitoautomatico;
                        Linhaconciliar.CCDConsolidado = (int)Resposta;                        
                        //dConciliacao.ContaCorrenteDetalheTableAdapter.SetStatus((int)Resposta, (int)TipoLancamentoNeon.debitoautomatico, Linhaconciliar.CCD);
                        dConciliacao.ContaCorrenteDetalheTableAdapter.Update(Linhaconciliar);
                        Linhaconciliar.AcceptChanges();
                    }
                }
            }
            return Resposta;
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            if (args.Length == 3)
            {
                int CCD = (int)args[0];
                int PGF = (int)args[1];
                DateTime Data = (DateTime)args[2];
                if (ConciliarDebitoAutomatico(CCD, PGF, Data) ==statusconsiliado.Automatico)
                    return new object[] { true };
                else
                    return new object[] { false };
            }
            else
                return new object[] { false };
        }*/



        /*
        private void TESTE(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            dConciliacao.ContaCorrenteDetalheTableAdapter.Update(Linhaconciliar);
            Linhaconciliar.AcceptChanges();
            int? TipoTESTE = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CCDTipoLancamento from ContaCorrenteDetalhe where CCD = 2315704");
            System.Console.WriteLine(string.Format("****************TIPO :{0}",TipoTESTE.HasValue ? TipoTESTE.Value.ToString() : "NULO"));
        }
        */


        /// <summary>
        /// 
        /// </summary>
        /// <param name="CCD"></param>
        public void Consolidar(int CCD)
        {

            if (dConciliacao.ContaCorrenteDetalheTableAdapter.FillByCCD(dConciliacao.ContaCorrenteDetalhe, CCD) == 1)
            {
                dConciliacao.ContaCorrenteDetalheRow Linhaconciliar = dConciliacao.ContaCorrenteDetalhe[0];                
                Consolidar(Linhaconciliar);
            }
        }
        
        //esta fun��o deve ser desmontada para que nao seja uma transa��o �nica
        /// <summary>
        /// 
        /// </summary>
        /// <param name="TempoMax"></param>        
        /// <param name="ComRelatorio"></param>        
        /// <returns></returns>
        public int Consolidar(int TempoMax,bool ComRelatorio)
        {            
            DateTime Marco = DateTime.Now;
            DateTime TempoLimete = DateTime.Now.AddMinutes(TempoMax);
            SBRetorno = null;
            //TimeSpan TS;
            int CCDMAX = 0;
            double Tempomax = 0;
            
            if (ComRelatorio)
                SBRetorno = new StringBuilder(string.Format("{0:HH:mm:ss} - INICIO\r\n", Marco));
            
            int Teto = dConciliacao.ContaCorrenteDetalhe.Count;
            chequesNao = baixados = aguardo = antigos = descricaonaoencontrada = 0;
            try
            {
                for (indiceConciliar = 0; indiceConciliar < Teto; indiceConciliar++)
                {
                    if (DateTime.Now > TempoLimete)
                    {
                        if (ComRelatorio)
                            SBRetorno.AppendLine("Tempo Limite");
                        break;
                    };
                    dConciliacao.ContaCorrenteDetalheRow Linhaconciliar = dConciliacao.ContaCorrenteDetalhe[indiceConciliar];
                    if (ComRelatorio)
                    {
                        //if (Linhaconciliar.CCD == 862260)
                        //    SBRetorno.AppendLine("***************");
                        string strTipo = Linhaconciliar.IsCCDTipoLancamentoNull() ? "??" : string.Format("{0}", (TipoLancamentoNeon)Linhaconciliar.CCDTipoLancamento);
                        TimeSpan TempoAc = DateTime.Now - Marco;
                        SBRetorno.AppendLine(string.Format("{0:HH:mm:ss} ({7:n2}) - {1:0000} - TIPO: {5} CCD {2} {3:dd/MM/yyyy} {4} {6}",
                            DateTime.Now,
                            indiceConciliar,
                            Linhaconciliar.CCD,
                            Linhaconciliar.SCCData,
                            Linhaconciliar.CONCodigo,
                            strTipo,
                            Linhaconciliar.CCDDescricao,
                            TempoAc.TotalMinutes                            
                            ));
                    }
                    if (!BalanceteFechado(Linhaconciliar))
                        //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Loop");
                        if ((Linhaconciliar.CCDConsolidado != (int)statusconsiliado.Aguardando)
                            &&
                            (Linhaconciliar.CCDConsolidado != (int)statusconsiliado.AguardaConta))
                        {
                            SBRetorno.AppendLine(string.Format("         PULADO: {0}", Linhaconciliar.CCDConsolidado));
                            continue;
                        }
                    DateTime Marco1 = DateTime.Now;
                    try
                    {
                        ConciliacaoSt.Consolidar(Linhaconciliar);                        
                    }
                    catch (Exception e0)
                    {
                        SBRetorno.AppendLine(string.Format("\r\n*****ERRO INTERNO*****\r\nerro:\r\n{0}\r\nCCD={1}\r\n",e0.Message,Linhaconciliar.CCD));
                    }
                    statusconsiliado Resposta = (statusconsiliado)Linhaconciliar.CCDConsolidado;
                    if (ComRelatorio)
                    {
                        TimeSpan TS = DateTime.Now - Marco1;
                        string strTipo = Linhaconciliar.IsCCDTipoLancamentoNull() ? "??" : string.Format("{0}", (TipoLancamentoNeon)Linhaconciliar.CCDTipoLancamento);
                        SBRetorno.AppendLine(string.Format("                                                Resultado: {0} / {2}    Tempo: {1}", Resposta, TS.TotalSeconds, strTipo));
                        if (Tempomax < TS.TotalSeconds)
                        {
                            Tempomax = TS.TotalSeconds;
                            CCDMAX = Linhaconciliar.CCD;
                        }
                    };
                    switch (Resposta)
                    {
                        case statusconsiliado.Antigo:
                            antigos++;
                            break;
                        case statusconsiliado.Aguardando:
                        case statusconsiliado.AguardaConta:
                        case statusconsiliado.Deposito:
                            if ((!Linhaconciliar.IsCCDErroNull()) && (Linhaconciliar.CCDErro == "Descri��o n�o prevista"))
                                descricaonaoencontrada++;
                            else
                                aguardo++;
                            break;
                        case statusconsiliado.Automatico:
                        case statusconsiliado.Manual:
                            baixados++;
                            break;
                        case statusconsiliado.Inativo:
                        case statusconsiliado.Filial:
                            inativo++;
                            break;
                        //case statusconsiliado.ChequeNaoEncontrado:
                        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
                        case statusconsiliado.TributoNaoEncontrado:
                        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
                            chequesNao++;
                            break;
                        default:
                            break;
                    }


                };
            }
            
            finally
            {
                if (ComRelatorio)
                {
                    SBRetorno.AppendLine(string.Format("M�ximo CCD {0} - tempo:{1}", CCDMAX, Tempomax));
                    SBRetorno.Append(CompontesBasicos.Performance.Performance.PerformanceST.Relatorio());

                    foreach (System.Collections.DictionaryEntry DE in DescricoesNaoEncontradas)
                        SBRetorno.AppendLine(string.Format("{0}: {1:0000}", DE.Key, DE.Value));

                    Retorno = SBRetorno.ToString();


                }
                else
                    Retorno = "";
            }
            return baixados;
        }
        /// <summary>
        /// 
        /// </summary>
        public bool Detravado = false;

        /// <summary>
        /// Contem o ccd da linha a rastrear (uso somente em debug)
        /// </summary>
        public int? CCDDebug;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Linhaconciliar"></param>
        public void Consolidar(dConciliacao.ContaCorrenteDetalheRow Linhaconciliar)
        {
            //System.Console.WriteLine(string.Format("CCD: {0} - {1}",Linhaconciliar.CCD,Linhaconciliar.CCDDescricao));
            CCDDebug = 2315704;
            if (CCDDebug.HasValue && (CCDDebug.Value == Linhaconciliar.CCD))
                if (System.Windows.Forms.MessageBox.Show(string.Format("Continuar debug para ccd: == {0}?", Linhaconciliar.CCD),"DEBUG",System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                    CCDDebug = null;
            
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Prepara");
            if (!BalanceteFechado(Linhaconciliar))
            {
                if ((Linhaconciliar.CCDConsolidado != (int)statusconsiliado.Aguardando)
                    &&
                    (Linhaconciliar.CCDConsolidado != (int)statusconsiliado.AguardaConta))
                    return;
                if (!Linhaconciliar.IsCCDTipoLancamentoNull())
                    if (Linhaconciliar.CCDTipoLancamento == (int)TipoLancamentoNeon.deposito)
                        return;
            }
            statusconsiliado Resposta =statusconsiliado.Aguardando;
            TipoLancamentoNeon Classificacao = TipoLancamentoNeon.NaoIdentificado;
            try
            {
                string MenTrans = string.Format("dllbanco Conciliacao Consolidar = 3339 CCD = {0}", Linhaconciliar.CCD);
                VirMSSQL.TableAdapter.AbreTrasacaoSQL(MenTrans, dConciliacao.ContaCorrenteDetalheTableAdapter);
                
                //VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllbanco Conciliacao Consolidar = 3292", dConciliacao.ContaCorrenteDetalheTableAdapter);
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("if");
                if (Linhaconciliar.CON_EMP != Framework.DSCentral.EMP)
                {
                    Linhaconciliar.CCDErro = "Outra Filial";
                    Resposta = statusconsiliado.Filial;
                }
                else if (Linhaconciliar.CONStatus == 0)
                {
                    Linhaconciliar.CCDErro = "Codom�nio inativo";
                    Resposta = statusconsiliado.Inativo;
                }
                else
                    if (!Detravado && (BalanceteFechado(Linhaconciliar)))
                    {
                        Linhaconciliar.CCDErro = "Balancete Terminado";
                        Resposta = statusconsiliado.Antigo;
                    }
                    else
                    {                        
                        Resposta = ConsolidarEfetivo(Linhaconciliar, ref Classificacao);
                    }
                
                if ((Linhaconciliar.IsCCDTipoLancamentoNull()) || (Linhaconciliar.CCDTipoLancamento != (int)Classificacao) || (Linhaconciliar.CCDConsolidado != (int)Resposta))
                {
                    
                    //dConciliacao.ContaCorrenteDetalheTableAdapter.SetStatus((int)Resposta, (int)Classificacao, Linhaconciliar.CCD);
                    Linhaconciliar.CCDTipoLancamento = (int)Classificacao;
                    
                    Linhaconciliar.CCDConsolidado = (int)Resposta;
                    
                    dConciliacao.ContaCorrenteDetalheTableAdapter.Update(Linhaconciliar);
                    Linhaconciliar.AcceptChanges();
                }
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch(Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                Console.WriteLine(e.Message);
                VirExcepitionNeon.VirExceptionNeon.VirExceptionSt.NotificarErro(e);
                Resposta = statusconsiliado.Aguardando;
            }

            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Loop");
        }
        
    }
}
