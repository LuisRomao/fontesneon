﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Votacao
{
    static class Program
    {
        [DllImport("User32.dll")]
        private static extern IntPtr FindWindow(String lpClassName, String lpWindowName);
        [DllImport("User32.dll")]
        private static extern Int32 SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern Int32 ShowWindow(IntPtr hWnd, int nCmdShow);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool Primeiro;
            using (Mutex Mut = new Mutex(true, "VotaNeon", out Primeiro))
            {
                if (Primeiro)
                {
                    try
                    {
                        Application.EnableVisualStyles();
                        Application.SetCompatibleTextRenderingDefault(false);
#if (DEBUG)
                        CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao = false;
#else
                        CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao = true;
#endif
                        //Application.ThreadException += VirExceptionNeon.VirExceptionNeonSt.OnThreadException;
                        Application.ThreadException += VirException.VirException.VirExceptionSt.OnThreadException;
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo("pt-BR");
                        //new Framework.usuarioLogado.UsuarioLogadoNeon(30);
                        Grade.Fabertura Seleciona = new Grade.Fabertura();
                        if (Seleciona.ShowDialog() == DialogResult.OK)
                        {
                            CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt = new UsuarioOff();
                            Application.Run(new FVotacao());
                        }
                        Mut.ReleaseMutex();
                    }
                    catch 
                    {
                        //VirExceptionNeon.VirExceptionNeonSt.NotificarErro(e);
                        //TrataErro.OnThreadException(null, new System.Threading.ThreadExceptionEventArgs(e));
                        //TrataExcessao(e);                
                    }
                }
                else
                {
                    IntPtr HJanela = FindWindow(null, "NEON - Votação");
                    if (HJanela != IntPtr.Zero)
                    {
                        SetForegroundWindow(HJanela);
                        ShowWindow(HJanela, 3);
                    }
                }
            }
        }
    }
}
