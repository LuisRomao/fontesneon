﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Votacao
{
    /// <summary>
    /// 
    /// </summary>
    public class UsuarioOff : CompontesBasicos.Bancovirtual.UsuarioLogado
    {
        /// <summary>
        /// 
        /// </summary>
        public override int USU
        {
            get { return 0; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string USUNome
        {
            get { return "Off-Line"; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="USU"></param>
        /// <returns></returns>
        public override string BuscaUSUNome(object USU)
        {
            return "Off-Line";
        }

        /// <summary>
        /// 
        /// </summary>
        public override System.Net.WebProxy Proxy
        {
            get { return null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override System.Net.NetworkCredential EmailNetCredenciais
        {
            get { return null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string EmailRemetente
        {
            get { return ""; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string ServidorSMTP
        {
            get { return null; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Funcionalidade"></param>
        /// <returns></returns>
        public override int VerificaFuncionalidade(string Funcionalidade)
        {
            return 0;
        }
    }
}
