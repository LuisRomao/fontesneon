﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using dllAssembleia;

namespace Votacao
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cInadVotacao : CompontesBasicos.ComponenteBase
    {
        
        //public cInadVotacao()
        //{
        //    InitializeComponent();
        //}

        private dVotacao dVotacao1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_dVotacao"></param>
        //public cInadVotacao(dVotacao _dVotacao)
        public cInadVotacao()
        {
            InitializeComponent();
            if (Grade.cVotacaoGrade.dVotacao1 != null)
            {
                dVotacao1 = Grade.cVotacaoGrade.dVotacao1;
                bindingSource1.DataSource = Grade.cVotacaoGrade.dVotacao1;
                //gridView1.OptionsDetail.ShowDetailTabs = false;
                //gridControl1.ShowOnlyPredefinedDetails = true;            
                //_dVotacao.AjustaBuscador();
                Grade.cVotacaoGrade.dVotacao1.AjustaBuscador();
            }
        }

        

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dVotacao.DadosCargaRow row = (dVotacao.DadosCargaRow)gridView1.GetDataRow(e.RowHandle);
            if (row != null)
                if ((row.GetACOrdosRows().Length > 0) || (row.GetBOLetosRows().Length > 0))
                    e.Appearance.BackColor = Color.Red; 
        }

        private SortedList<int, dVotacao.VOTacaoRow> Ordenado;

        private void Imprimecedulas()
        {            
            dVotacao.DadosCargaRow row = (dVotacao.DadosCargaRow)gridView1.GetFocusedDataRow();
            if (row != null)
            {
                if (row.Presente == 0)
                    row.Presente = 1;
                if (row.IsPP_APTNull())
                {
                    DataView DV = new DataView(dVotacao1.VOtO);
                    DV.RowFilter = string.Format("VOO_APT = {0}", row.APT);
                    DV.Sort = "VOO";
                    int nCedulas = DV.Count;
                    foreach (DataRowView DRV in DV)
                    {
                        nCedulas--;
                        dVotacao.VOtORow rowCedula = (dVotacao.VOtORow)DRV.Row;
                        dVotacao1.ImprimeCedula(rowCedula, nCedulas == 0);
                    }
                    //dVotacao.VOtORow teste;
                    /*

                    int nCedulas = dVotacao1.VOTacao.Count;
                    //int nCedulas = 0;
                    if (Ordenado == null)
                    {
                        Ordenado = new SortedList<int, dVotacao.VOTacaoRow>();
                        foreach (dVotacao.VOTacaoRow rowVOT in dVotacao1.VOTacao)
                            Ordenado.Add(rowVOT.VOTOrdem, rowVOT);                        
                    }
                    //foreach (dVotacao.VOTacaoRow rowVOT in dVotacao1.VOTacao)
                    //foreach (dVotacao.VOTacaoRow rowVOT in dVotacao1.Ordenado.Values)
                    //    if (dVotacao1.TemCedula(rowVOT, row.APT))
                    //        nCedulas++;
                    foreach (dVotacao.VOTacaoRow rowVOT in dVotacao1.Ordenado.Values)
                    {
                        nCedulas--;
                        //if (dVotacao1.TemCedula(rowVOT,row.APT))
                        dVotacao1.ImprimeCedula(rowVOT, row.APT, nCedulas == 0);
                    }
                   */
                }
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Imprimecedulas();
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Imprimecedulas();                              
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dVotacao.DadosCargaRow row = (dVotacao.DadosCargaRow)gridView1.GetDataRow(e.RowHandle);
            if (row.IsPP_APTNull())
                row.Presente = 1;
            else
            {
                if (row.PP_APT == row.APT)
                {
                    row.Presente = 1;
                    row.SetPP_APTNull();
                }
                else
                    row.Presente = 2;
            }
        }

        private void simpleButton1_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Confirma o cancelamento de todas as procurações visíveis ?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                DataView filteredDataView = new DataView(dVotacao1.DadosCarga);
                filteredDataView.RowFilter = DevExpress.Data.Filtering.CriteriaToWhereClauseHelper.GetDataSetWhere(gridView1.ActiveFilterCriteria);
                foreach (DataRowView DRV in filteredDataView)
                {
                    dVotacao.DadosCargaRow rowDados = (dVotacao.DadosCargaRow)DRV.Row;
                    if (!rowDados.IsPP_APTNull())
                    {
                        rowDados.SetPP_APTNull();
                        rowDados.Presente = 0;
                    }
                }
            }
        }

        private void lookUpEdit1_EditValueChanged(object sender, System.EventArgs e)
        {

        }

        private void simpleButton2_Click(object sender, System.EventArgs e)
        {
            if(lookUpEdit1.EditValue == null)
                return;
            dVotacao.DadosCargaRow rowDadosProcuradora = dVotacao1.DadosCarga.FindByAPT((int)lookUpEdit1.EditValue);
            DataView filteredDataView = new DataView(dVotacao1.DadosCarga);
            filteredDataView.RowFilter = DevExpress.Data.Filtering.CriteriaToWhereClauseHelper.GetDataSetWhere(gridView1.ActiveFilterCriteria);
            string Pergunta = string.Format("Confirma procuração para todas as {0} unidades visíveis ?", filteredDataView.Count);
            if (MessageBox.Show(Pergunta, "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                {
                    
                    foreach (DataRowView DRV in filteredDataView)
                    {
                        dVotacao.DadosCargaRow rowDados = (dVotacao.DadosCargaRow)DRV.Row;
                        if (rowDados.APT == rowDadosProcuradora.APT)
                        {
                            rowDados.SetPP_APTNull();
                            rowDados.Presente = 1;
                        }
                        else
                            if ((rowDados.GetACOrdosRows().Length == 0) && (rowDados.GetBOLetosRows().Length == 0))
                            {
                                rowDados.PP_APT = rowDadosProcuradora.APT;
                                rowDados.Presente = 2;
                            }
                    }
                }
            }
        }        
        
    }
}
