﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace Votacao.Votacao
{
    public partial class cVotacaoGrade : ComponenteGradeNavegadorPesquisa
    {
        public cVotacaoGrade()
        {
            InitializeComponent();
        }

        private Votacao.dVotacoes dVotacoes1;

        private void cVotacaoGrade_cargaPrincipal(object sender, EventArgs e)
        {
            dVotacoes1 = new dVotacoes();
            BindingSource_F.DataSource = dVotacoes1;
            dVotacoes1.Votacoes.AddVotacoesRow("1");
            dVotacoes1.Votacoes.AddVotacoesRow("2");
        }
    }
}
