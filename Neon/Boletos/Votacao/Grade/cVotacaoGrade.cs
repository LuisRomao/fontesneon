﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using System.IO;
using VirHTTPServer;
using dllAssembleia;

namespace Votacao.Grade
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cVotacaoGrade : ComponenteGradeNavegadorPesquisa
    {
        /// <summary>
        /// 
        /// </summary>
        public cVotacaoGrade()
        {
            InitializeComponent();
            dllAssembleia.virEnumVOTStatus.virEnumVOTStatusSt.CarregaEditorDaGrid(colStatus);
        }

        private dllAssembleia.OffLine.dVotacoes dVotacoes1;
        public static dVotacao dVotacao1;


        private void cVotacaoGrade_cargaPrincipal(object sender, EventArgs e)
        {
            dVotacoes1 = new dllAssembleia.OffLine.dVotacoes();
            BindingSource_F.DataSource = dVotacoes1;
            //dVotacoes1.CarregaXMLs();
            dVotacao1 = new dVotacao();
            dVotacao1.ReadXml(Abertura.ArquivoAbrir);
            bool GerarCedulas = (dVotacao1.Alternativas.Count == 0);
                foreach (dVotacao.VOTacaoRow rowVOT in dVotacao1.VOTacao)
                {
                    if (GerarCedulas)
                        dVotacao1.GeraCedulas(rowVOT);
                    dVotacoes1.Votacoes.AddVotacoesRow(rowVOT.VOTPergunta, rowVOT.VOT, rowVOT.Condominio, "", rowVOT.VOTStatus);
                }
        }


        /// <summary>
        /// 
        /// </summary>
        protected override void Alterar()
        {
            if (LinhaMae_F == null)
                return;
            dllAssembleia.OffLine.dVotacoes.VotacoesRow row = (dllAssembleia.OffLine.dVotacoes.VotacoesRow)LinhaMae_F;
            if (dllAssembleia.cVotacao.VotacoesAbertas == null)
                dllAssembleia.cVotacao.VotacoesAbertas = new SortedList<int, dllAssembleia.cVotacao>();
            if (!dllAssembleia.cVotacao.VotacoesAbertas.ContainsKey(row.VOT))
            {
                //dllAssembleia.cVotacao cVotacao1 = dVotacoes1.GeracVotacao(row.VOT);
                dllAssembleia.cVotacao cVotacao1 = new dllAssembleia.cVotacao(dVotacao1, row.VOT);            
                dllAssembleia.cVotacao.VotacoesAbertas.Add(row.VOT, cVotacao1);
                cVotacao1.nomeXML = Abertura.ArquivoAbrir;
                cVotacao1.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
            }
        }

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colStatus)
            {
                dllAssembleia.OffLine.dVotacoes.VotacoesRow rowVOT = (dllAssembleia.OffLine.dVotacoes.VotacoesRow)GridView_F.GetDataRow(e.RowHandle);
                if (rowVOT == null)
                    return;
                if (e.CellValue != DBNull.Value)
                {
                    e.Appearance.BackColor = dllAssembleia.virEnumVOTStatus.virEnumVOTStatusSt.GetCor(e.CellValue);
                    e.Appearance.ForeColor = Color.Black;
                }
            }
        }

        private cInadVotacao cInadVotacao1;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //dllAssembleia.OffLine.dVotacoes.VotacoesRow rowVOT = (dllAssembleia.OffLine.dVotacoes.VotacoesRow)GridView_F.GetFocusedDataRow();
            //if (rowVOT == null)
            //    return;
            //cInadVotacao cInadVotacao1 = new Votacao.cInadVotacao(dVotacoes1.GetdVotacao(rowVOT.VOT));
            if (cInadVotacao1 == null)
            {
                cInadVotacao1 = new Votacao.cInadVotacao();
                //cInadVotacao1.Titulo = rowVOT.Condominio;
                cInadVotacao1.Titulo = "Presença";
                cInadVotacao1.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
            };
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Alterar();
            /*
            if (LinhaMae_F == null)
                return;
            dllAssembleia.OffLine.dVotacoes.VotacoesRow row = (dllAssembleia.OffLine.dVotacoes.VotacoesRow)LinhaMae_F;
            //dllAssembleia.dVotacao novadVotacao = (dllAssembleia.dVotacao)XMLs[row.VOT];
            //dllAssembleia.cVotacao cVotacao1 = new dllAssembleia.cVotacao(novadVotacao);
            dllAssembleia.cVotacao cVotacao1 = dVotacoes1.GeracVotacao(row.VOT);
            cVotacao1.nomeXML = row.Arquivo;
            cVotacao1.VirShowModulo(EstadosDosComponentes.JanelasAtivas);*/
        }

        private VirHTTPServer.VirHTTPServer _Servidor;

        private VirHTTPServer.VirHTTPServer Servidor
        {
            get
            {
                if (_Servidor == null)
                {
                    _Servidor = new VirHTTPServer.VirHTTPServer(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao ? "" : @"C:\fontesVS\neon\DLLBoletos\dllBoletos\Votacao\ViaRede", 8081, Chamada);
                    _Servidor.NivelDoLog = CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao ? CsHTTPServer.NivelLog.soerros : CsHTTPServer.NivelLog.completo;
                }
                return _Servidor;
            }
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e)
        {
            if (chServidor.Checked)
            {
                Servidor.Start();
                labelURL.Text = Servidor.URL();
                labelURL.Visible = true;
            }
            else
            {
                Servidor.Stop();
                labelURL.Visible = false;
            }
        }

        /// <summary>
        /// Pode fechar
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            if (base.CanClose())
            {
                if (Servidor != null)
                    Servidor.Stop();
                return true;
            }
            else
                return false;
        }

        private SortedList<string, string[]> Gabaritos;
        private SortedList<string, string> GabaritosFragmentos;

        private string[] Gabarito(string Nome)
        {
            string Arqvivo = string.Format("{0}\\{1}.htm", Servidor.RepositorioPadrao, Nome);
            if (Gabaritos == null)
            {
                Gabaritos = new SortedList<string, string[]>();
                GabaritosFragmentos = new SortedList<string, string>();
                GabaritosFragmentos.Add("MenuVotacoes", "<tr align=\"center\"><td class=\"Botoes\">{0}</td><td><input id=\"B{1}\" name=\"B{1}\" type=\"submit\" value=\"Votar\" class=\"Botoes\" /></td></tr><tr align=\"center\"><td class=\"Botoes\" colspan=\"2\">&nbsp;</td></tr>");
                GabaritosFragmentos.Add("MenuVotacoes1", "<tr align=\"center\"><td class=\"Botoes\">{0}</td><td>&nbsp;</td></tr><tr align=\"center\"><td colspan=\"2\">&nbsp;</td></tr>");
                GabaritosFragmentos.Add("Votacao", "<tr align=\"center\"><td><input id=\"Radio{0}\" onchange='this.form.submit();' type=\"radio\" value=\"{1}\" name=\"RadVoto\" /></td><td class=\"Botoes\">{2}</td></tr><tr><td class=\"Botoes\" colspan = \"2\">&nbsp;</td></tr>");
            }
            if (!Gabaritos.ContainsKey(Nome))
            {
                if (File.Exists(Arqvivo))
                    Gabaritos.Add(Nome, File.ReadAllText(Arqvivo).Split(new string[] { "<!--#-->" }, StringSplitOptions.RemoveEmptyEntries));
                else
                    if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                        return new string[] {string.Format("\\")};
                    else
                        return new string[] {string.Format("arquivo {0} não encontrado", Nome)};
            }
            return Gabaritos[Nome];

        }

        /*
        private string ListaVotacoes(dllAssembleia.dVotacao.DadosCargaRow rowAPT, dllAssembleia.dVotacao dVotacaoX)
        {
            string[] Partes = Gabarito("MenuVotacoes");
            StringBuilder Linhas = new StringBuilder();
            Linhas.Append(Partes[0]);
            Linhas.Append(rowAPT.APT.ToString());
            Linhas.Append(Partes[1]);
            //Linhas.Append(rowAPT.APT.ToString());
            //Linhas.Append(Partes[2]);
            foreach (dVotacao.VOTacaoRow rowVOT in dVotacaoX.Ordenado.Values)
                if (dVotacaoX.TemCedula(rowVOT, rowAPT.APT))
                {
                    string modelo = ((dllAssembleia.cVotacao.VotacoesAbertas != null) && (dllAssembleia.cVotacao.VotacoesAbertas.ContainsKey(rowVOT.VOT))) ? "MenuVotacoes" : "MenuVotacoes1";
                    Linhas.AppendFormat(GabaritosFragmentos[modelo],
                                         rowVOT.VOTPergunta,
                                         rowVOT.VOT);
                }
            //foreach (dllAssembleia.OffLine.dVotacoes.VotacoesRow Vota in dVotacoes1.Votacoes)
            //if (dllAssembleia.OffLine.dVotacoes.XMLs[Vota.VOT] == dVotacaoX)
            //        Linhas.AppendFormat(GabaritosFragmentos["MenuVotacoes"],
            //                           Vota.Nome,
            //                         Vota.VOT);
            Linhas.Append(Partes[3]);
            Linhas.Append(rowAPT.APT.ToString());
            Linhas.Append(Partes[4]);
            return Linhas.ToString();
        }
        */
          
        private string Cedula(int APT, dllAssembleia.dVotacao dVotacaoX, int VOT)
        {
            dllAssembleia.dVotacao.VOTacaoRow VOTrow = dVotacaoX.VOTacao.FindByVOT(VOT);
            foreach (dllAssembleia.dVotacao.VOtORow VOOrow in VOTrow.GetVOtORows())
                if (VOOrow.VOO_APT == APT)
                {
                    string[] Partes = Gabarito("Votacao");
                    StringBuilder Linhas = new StringBuilder();
                    Linhas.Append(Partes[0]);
                    Linhas.Append(APT.ToString());
                    Linhas.Append(Partes[1]);
                    Linhas.Append(VOOrow.VOO.ToString());
                    Linhas.Append(Partes[2]);
                    Linhas.Append(VOTrow.VOTPergunta);
                    Linhas.Append(Partes[4]);
                    foreach (dllAssembleia.dVotacao.AlternativasRow rowALT in VOOrow.GetAlternativasRows())
                    {
                        Linhas.AppendFormat(GabaritosFragmentos["Votacao"],
                                                     rowALT.VOD,
                                                     rowALT.Codigo,
                                                     rowALT.Texto);
                    }
                    Linhas.Append(Partes[6]);
                    return Linhas.ToString();
                }

            return "sem cedula";


        }

        private string VotoComputado(int APT)
        {
            string[] Partes = Gabarito("VotoGravado");
            StringBuilder Linhas = new StringBuilder();
            Linhas.Append(Partes[0]);
            Linhas.Append(APT.ToString());
            Linhas.Append(Partes[1]);            
            return Linhas.ToString();
        }

        private bool RecuperaUsuario(out int Usuario, out dllAssembleia.dVotacao dVotacao, SortedList<string, string> Args, SortedList<string, string> PostArgs)
        {
            dVotacao = null;
            if (int.TryParse(Parametro("Usuario",Args,PostArgs), out Usuario))
            {
                foreach (dllAssembleia.dVotacao dVotacaoX in dllAssembleia.OffLine.dVotacoes.XMLs.Values)
                {
                    dllAssembleia.dVotacao.DadosCargaRow rowAPT = dVotacaoX.DadosCarga.FindByAPT(Usuario);
                    if (rowAPT != null)
                    {
                        dVotacao = dVotacaoX;
                        return true;
                    }
                }
            }
            return false;
        }

        private string Parametro(string Chave,SortedList<string, string> Args, SortedList<string, string> PostArgs)
        {
            if ((PostArgs != null) && (PostArgs.ContainsKey(Chave)))
                return PostArgs[Chave];
            else if ((Args != null) && (Args.ContainsKey(Chave)))
                return Args[Chave];
            else
                return "";
        }

        private bool Chamada(SortedList<string, string> Args, SortedList<string, string> PostArgs, out string Conteudo, out string Arquivo)
        {
            
            
            Arquivo = Conteudo = null;
            //if ((PostArg != null) && (PostArg.ContainsKey("Comando")))
            if (Parametro("Comando",Args,PostArgs)!="")
            {
                try
                {
                    int Usuario;
                    dllAssembleia.dVotacao dVotacao;
                    dllAssembleia.dVotacao.DadosCargaRow rowAPT;
                    if (RecuperaUsuario(out Usuario, out dVotacao,Args, PostArgs))
                    {
                        switch (Parametro("Comando",Args,PostArgs))
                        {                            
                            case "login":
                                rowAPT = dVotacao.DadosCarga.FindByAPT(Usuario);
                                if (rowAPT != null)
                                {
                                    //Conteudo = ListaVotacoes(rowAPT, dVotacao);
                                    return true;
                                }
                                Arquivo = "\\CodigoInvalido.htm";
                                break;
                            case "votar":
                                int VOT = int.Parse(PostArgs.Keys[PostArgs.IndexOfValue("Votar")].Substring(1));                                
                                Conteudo = Cedula(Usuario, dVotacao, VOT);
                                return true;                                                                
                            case "contabilizar":
                                string RadVoto = PostArgs["RadVoto"];
                                int voo = int.Parse(RadVoto.Substring(0, 6));
                                int vod = int.Parse(RadVoto.Substring(6, 5));
                                foreach (cVotacao cVotacaoX in dllAssembleia.cVotacao.VotacoesAbertas.Values)
                                    if (cVotacaoX.Apura(voo, vod))
                                        break;
                                Conteudo = VotoComputado(Usuario);
                                break;
                            default:
                                if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                                    Arquivo = "\\";
                                else
                                    Conteudo = string.Format("Comando não tratado: {0}", Parametro("Comando",Args,PostArgs));
                                break;
                        }
                    }
                    else
                    {
                        Arquivo = "\\CodigoInvalido.htm";
                    }
                    return true;
                }
                catch (Exception e)
                {
                    Servidor.WriteLog(CsHTTPServer.NivelLog.soerros, "Erro:{0}\r\n{1}", e.Message, e.StackTrace);
                    return false;
                }
            }
            
            return false;                        
        }
    }
}
