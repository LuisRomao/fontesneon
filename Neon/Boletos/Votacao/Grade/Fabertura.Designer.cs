﻿namespace Votacao.Grade
{
    partial class Fabertura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.abertura1 = new Votacao.Grade.Abertura();
            this.SuspendLayout();
            // 
            // abertura1
            // 
            this.abertura1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abertura1.Location = new System.Drawing.Point(0, 0);
            this.abertura1.Name = "abertura1";
            this.abertura1.Size = new System.Drawing.Size(586, 527);
            this.abertura1.TabIndex = 0;
            // 
            // Fabertura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 527);
            this.Controls.Add(this.abertura1);
            this.Name = "Fabertura";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VOTAÇÃO - NEON";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Fabertura_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Abertura abertura1;
    }
}