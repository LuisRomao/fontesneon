﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Votacao.Grade
{
    public partial class Abertura : UserControl
    {
        public Abertura()
        {
            InitializeComponent();
        }

        private Form Pai;

        private string PastaXML;

        public static string ArquivoAbrir;

        public void Carrega(Form _Pai)
        {
            Pai = _Pai;
            int i = 1;
            PastaXML = string.Format("{0}\\VOTACOES", Path.GetDirectoryName(Application.ExecutablePath));
            if (!Directory.Exists(PastaXML))
                Directory.CreateDirectory(PastaXML);
            string[] Arquivos = Directory.GetFiles(PastaXML, "VOTCON_*.xml");
            foreach (string Arquivo in Arquivos)
            {
                string[] partes = Path.GetFileNameWithoutExtension(Arquivo).Split('_');
                if (partes.Length != 6)
                    continue;
                try
                {
                    DateTime DataVOT = new DateTime(int.Parse(partes[3]), int.Parse(partes[2]), int.Parse(partes[1]));
                    dAbertura.Arquivos.AddArquivosRow(i,string.Format("Votação: {0:dd/MM/yyyy}",DataVOT),Arquivo);
                    i++;    
                }
                catch { };                
            }
            dAbertura.Arquivos.AddArquivosRow(100, "Importar Rede","");
            dAbertura.Arquivos.AddArquivosRow(101, "Importar Arquivo","");
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {
            dAbertura.ArquivosRow row = (dAbertura.ArquivosRow)gridView1.GetFocusedDataRow();            
            if (row != null)
            {
                if (row.numero == 100)
                {
                    string RepositorioVOT = string.Format("{0}\\VOTACOES", CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Repositorio"));
                    if (!Directory.Exists(RepositorioVOT))
                        return;
                    string[] Arquivos = Directory.GetFiles(RepositorioVOT, "VOTCON_*.xml");
                    foreach (string Arquivo in Arquivos)
                    {
                        string ArquivoDestino = string.Format("{0}//{1}", PastaXML, Path.GetFileName(Arquivo));
                        File.Copy(Arquivo, ArquivoDestino,true);
                    }
                    dAbertura.Arquivos.Clear();
                    Carrega(Pai);
                }
                else if (row.numero == 101)
                {
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        ArquivoAbrir = string.Format("{0}//{1}", PastaXML, Path.GetFileName(openFileDialog1.FileName));
                        File.Copy(openFileDialog1.FileName, ArquivoAbrir);
                        Pai.DialogResult = DialogResult.OK;
                    }
                }
                else
                {
                    ArquivoAbrir = row.Arquivo;
                    Pai.DialogResult = DialogResult.OK;
                }
            }
        }
    }
}
