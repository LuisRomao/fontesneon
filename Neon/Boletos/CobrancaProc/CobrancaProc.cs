﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FrameworkProc;
using System.Data;
using VirMSSQL;

namespace CobrancaProc
{
    /// <summary>
    /// Classe para cobrança
    /// </summary>
    public class CobrancaProc : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                {
                    if (TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                }
                return _EMPTProc1;
            }
            set => _EMPTProc1 = value;
        }

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc"></param>
        public CobrancaProc(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
        }        
        #endregion

        private BoletosProc.Boleto.dBoletos _dBoletos;
        //private dCobrancaV _dCobranca;
        private dLinhaCobranca _dLinhaCobranca;

        /// <summary>
        /// dataset boletos
        /// </summary>
        public BoletosProc.Boleto.dBoletos dBoletos { get => _dBoletos ?? (_dBoletos = new BoletosProc.Boleto.dBoletos()); }

        /// <summary>
        /// dataset interno
        /// </summary>
        public dLinhaCobranca DLinhaCobranca { get => _dLinhaCobranca ?? (_dLinhaCobranca = new dLinhaCobranca(EMPTProc1)); }

        /// <summary>
        /// Inclui novas unidades nos processos de cobrança
        /// </summary>
        /// <returns></returns>
        public int IncluirNovos()
        {
            UltimoErro = null;
            try
            {
                DateTime DataDeCorte = DateTime.Today.SomaDiasUteis(4, Sentido.Traz);
                return DLinhaCobranca.APARTAMENTOSTableAdapter.IncluirNovos(DataDeCorte);
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            return 0;
        }

        /// <summary>
        /// Último erro
        /// </summary>
        public Exception UltimoErro = null;

        /// <summary>
        /// Remove os pagos. Não deveria porque o certo é remover ao quitar o boleto mas é uma segurança.
        /// </summary>
        /// <returns></returns>
        public int RemoverPagos()
        {
            UltimoErro = null;
            string ComandoBuscaAPTSCandidatos =
"SELECT APT\r\n" +
"FROM APARTAMENTOS INNER JOIN\r\n" +
"     BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO INNER JOIN\r\n" +
"     CONDOMINIOS ON BLOCOS.BLO_CON = CONDOMINIOS.CON\r\n" +
"WHERE        \r\n" +
"    (NOT (APARTAMENTOS.APTStatusCobranca IN (0, 1, 5))) \r\n" +
"AND \r\n" +
"    (CONDOMINIOS.CONStatus = 1)\r\n" +
"and \r\n" +
"not (APARTAMENTOS.APT in \r\n" +
"       (\r\n" +
"         SELECT distinct BOLetos.BOL_APT\r\n" +
"         FROM BOLetos INNER JOIN\r\n" +
"              CONDOMINIOS ON BOLetos.BOL_CON = CONDOMINIOS.CON\r\n" +
"              WHERE  (BOLetos.BOLPagamento IS NULL) \r\n" +
"			  AND    (BOLetos.BOLCancelado = 0) \r\n" +
"			  AND    (BOLetos.BOLVencto < @P1) \r\n" +
"			  AND    (NOT (BOLetos.BOLStatus IN (14, 15, 16))) \r\n" +
"			  AND    (BOLetos.BOLTipoCRAI <> 'S') \r\n" +
"			  AND    (CONDOMINIOS.CONStatus <> 0) \r\n" +
"			  AND    (BOL_APT IS NOT NULL)\r\n" +
"       )\r\n" +
"    )\r\n" +
";";
            int Extintos = 0;
            try
            {
                DateTime dataCorte = DateTime.Today.SomaDiasUteis(4, Sentido.Traz);
                DataTable APTs = EMPTProc1.STTA.BuscaSQLTabela(ComandoBuscaAPTSCandidatos, dataCorte);                
                foreach (DataRow DR in APTs.Rows)
                {
                    int APT = (int)DR["APT"];
                    LinhaCobrancaProc LinhaCobrancaProc1 = new LinhaCobrancaProc(AbstratosNeon.ABS_Apartamento.GetApartamentoProc(APT, EMPTProc1), true, EMPTProc1);
                    if (LinhaCobrancaProc1.Reavalia() == VirEnumeracoesNeon.APTStatusCobranca.Terminada)
                        Extintos++;
                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
            }
            return Extintos;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <returns></returns>
        public int CarregarCobranca(int? CON)
        {
            if (!CON.HasValue)
            {
                dBoletos.BOLetosTableAdapter.FillByCobranca(dBoletos.BOLetos, DateTime.Today.AddDays(-3));
                dBoletos.CarregaAPTs(true);                
            }
            else
            {
                dBoletos.BOLetosTableAdapter.FillByCobrancaCON(dBoletos.BOLetos, DateTime.Today.AddDays(-3),CON.Value);
                dBoletos.CarregaAPTs(CON.Value,false,true);                
            }
            dBoletos.CamposComplementares(DateTime.Now, false);
            return dBoletos.BOLetos.Count;
        }
    }
}
