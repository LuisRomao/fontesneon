﻿using System;
using System.Collections.Generic;
using System.Text;
using FrameworkProc;
using VirMSSQL;
using AbstratosNeon;
using System.Data;
using BoletosProc.Boleto;
using VirEnumeracoesNeon;
using CompontesBasicosProc;

namespace CobrancaProc
{

    /// <summary>
    /// 
    /// </summary>
    public class LinhaCobrancaProc : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                {
                    if (TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                }
                return _EMPTProc1;
            }
            set => _EMPTProc1 = value;
        }

        /*
        /// <summary>
        /// Só agora. descontinuar
        /// </summary>
        [Obsolete("remover o OLEDb")]
        public void ImportacaoInicial()
        {
            if ((LinhaMae != null) || (StatusCobranca.EstaNoGrupo(APTStatusCobranca.Novo, APTStatusCobranca.SemCobranca)))
                return;
            const string ComandoVerificaJuridico = "SELECT Status, Data, Cobrar, Observacoes FROM Cobrança WHERE (Codcon = @P1) AND (Bloco = @P2) AND (Apartamento = @P3)";
            string ComandoH =
"SELECT     Codcon, Bloco, Apartamento, [Data do Registro], Histórico\r\n" +
"FROM         [Cobrança - Histórico]\r\n" +
"WHERE     (Codcon = @P1) AND (Bloco = @P2) AND (Apartamento = @P3)\r\n" +
"ORDER BY Apartamento, [Data do Registro];";
            StringBuilder SB = new StringBuilder("IMPORTAÇÂO DE DADOS\r\n");
            DataRow rowCobranca = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoVerificaJuridico, Apartamento.CONCodigo, Apartamento.BLOCodigo, Apartamento.APTNumero);
            if (rowCobranca != null)
            {
                if (rowCobranca["Observacoes"] != DBNull.Value)
                {
                    DataTable Historico = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLTabela(ComandoH, Apartamento.CONCodigo, Apartamento.BLOCodigo, Apartamento.APTNumero);                    
                    foreach (DataRow rowH in Historico.Rows)
                        SB.AppendFormat("{0:dd/MM/yyyy} - {1}\r\n", (DateTime)rowH["Data do Registro"], rowH["Histórico"]);
                    SB.AppendFormat("obs: {0}\r\nStatus: {1}",rowCobranca["Observacoes"], rowCobranca["Status"]);                    
                }
            }
            StatusCobranca = Apartamento.StatusCobranca;            
            FrameworkProc.objetosNeon.LOGGeralProc.RegistraLogST(LLOTipo.Sistema, 0, DateTime.Now, SB.ToString(), LLOxXXX.LIC, linhaMae.LIC);            
        }*/

        /// <summary>
        /// 
        /// </summary>
        public APTStatusCobranca StatusCobranca
        {
            get => LinhaMae != null ? (APTStatusCobranca)LinhaMae.LICStatusCobranca : Apartamento.StatusCobranca;
            set
            {
                DateTime NovaData = CalculaProxRef();
                if ((LinhaMae != null) && (LinhaMae.LICStatusCobranca == (int)value) && (linhaMae.LICDataRev == NovaData))
                    return;
                if ((linhaMae == null) && (value == APTStatusCobranca.Terminada))
                {
                    Apartamento.StatusCobranca = APTStatusCobranca.SemCobranca;
                    return;
                }
                APTStatusCobranca Antigo;// = LinhaMae != null ? (APTStatusCobranca)LinhaMae.LICStatusCobranca : Apartamento.StatusCobranca;
                if (linhaMae != null)
                {
                    object AntigoOBJ = LinhaMae["LICStatusCobranca", DataRowVersion.Original];
                    if (AntigoOBJ != DBNull.Value)
                        Antigo = (APTStatusCobranca)AntigoOBJ;
                    else
                        Antigo = APTStatusCobranca.Novo;
                    //Antigo = (APTStatusCobranca)LinhaMae.LICStatusCobranca;
                }
                else
                {
                    Antigo = APTStatusCobranca.SemCobranca;
                }

                DateTime AntigaData = DateTime.MinValue;
                if (linhaMae != null)
                    AntigaData = linhaMae.LICDataRev;
                if (LinhaMae == null)
                {
                    linhaMae = DLinhaCobranca.LInhaCobranca.NewLInhaCobrancaRow();
                    linhaMae.LICDataI = DateTime.Now;
                    linhaMae.LICStatusCobranca = (int)APTStatusCobranca.Novo;
                    linhaMae.LIC_APT = Apartamento.APT;
                    linhaMae.LICDataRev = DateTime.Today;
                    linhaMae.LICDataBloc = CalculaDataBlocPorEmissao(value);
                    DLinhaCobranca.LInhaCobranca.AddLInhaCobrancaRow(linhaMae);
                }
                string strregistrar = "";
                if (Antigo != value)
                    strregistrar += string.Format("Status de cobrança alterado {0} => {1}\r\n", Antigo, value);
                if (NovaData != AntigaData)
                {
                    if (AntigaData == DateTime.MinValue)
                        strregistrar += string.Format("Próxima data Alterada para {0:dd/MM/yyyy}\r\n", NovaData);
                    else
                        strregistrar += string.Format("Próxima data Alterada {0:dd/MM/yyyy} => {1:dd/MM/yyyy}\r\n", AntigaData, NovaData);
                }
                LinhaMae.LICStatusCobranca = (int)value;
                if (value == APTStatusCobranca.Terminada)
                { 
                    linhaMae.LICDataF = DateTime.Now;
                }
                linhaMae.LICDataRev = CalculaProxRef();
                try
                {
                    EMPTProc1.AbreTrasacaoSQL("set do StatusCobranca LinhaCobranacaProc.cs 59", DLinhaCobranca.LInhaCobrancaTableAdapter);
                    DLinhaCobranca.LInhaCobrancaTableAdapter.Update(linhaMae);
                    Apartamento.StatusCobranca = value;
                    FrameworkProc.objetosNeon.LOGGeralProc.RegistraLogST(LLOTipo.Sistema, 0, DateTime.Now, strregistrar, LLOxXXX.LIC, linhaMae.LIC);
                    EMPTProc1.Commit();
                }
                catch (Exception ex)
                {
                    EMPTProc1.Vircatch(ex);
                    throw new Exception("Erro na gravação da cobrança", ex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataInicio => (LinhaMae != null) ? linhaMae.LICDataI : (DateTime?)null;

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataFim => ((LinhaMae != null) && (!LinhaMae.IsLICDataFNull())) ? linhaMae.LICDataF : (DateTime?)null;

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataBloc => ((LinhaMae != null) && (!LinhaMae.IsLICDataBlocNull())) ? linhaMae.LICDataBloc : (DateTime?)null;

        /// <summary>
        /// 
        /// </summary>
        public DateTime ProximaData => LinhaMae != null ? linhaMae.LICDataRev : CalculaProxRef();

        private FrameworkProc.objetosNeon.LOGGeralProc _LOGGeralProc;

        /// <summary>
        /// 
        /// </summary>
        public FrameworkProc.objetosNeon.LOGGeralProc LOGGeralProc => _LOGGeralProc ?? (_LOGGeralProc = new FrameworkProc.objetosNeon.LOGGeralProc(LLOxXXX.LIC, LIC));

        /// <summary>
        /// 
        /// </summary>
        public string RelatorioLog => LOGGeralProc.Relatorio();

        private DateTime VencimentoMaisAntigo()
        {            
            CarregaBoletos();
            if (Boletos.Count == 0)
                return DateTime.Today;
            else
                return Boletos[0].VencimentoOriginal;

            //DateTime retorno = DateTime.Today;
            //foreach (BoletoProc BOL in Boletos)
            //    if (BOL.VencimentoOriginal < retorno)
            //        retorno = BOL.VencimentoOriginal;
            //return retorno;

            //if ((DVBoletos == null) || (DVBoletos.Count == 0))
            //    return DateTime.Today;
            //DataRowView DRV = DVBoletos[0];
            //dBoletos.BOLetosRow rowBOL = (dBoletos.BOLetosRow)DRV.Row;            
            //return rowBOL.BOLVencto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DateTime CalculaProxRef()
        {            
            int dias = 30;
            switch (StatusCobranca)
            {
                case APTStatusCobranca.Novo:
                    dias = 30;
                    break;
                case APTStatusCobranca.PrimeiraCarta:
                    dias = 60;
                    break;
                case APTStatusCobranca.Extra:
                    dias = 90;
                    break;
                case APTStatusCobranca.UltimoAviso:
                    dias = 90;
                    break;
                case APTStatusCobranca.juridico:
                case APTStatusCobranca.SemCobranca:
                case APTStatusCobranca.Terminada:
                    dias = 0;
                    break;
                default: throw new NotImplementedException(string.Format("CalculaProxRef para {0} não implementado",StatusCobranca));
            }            
            //return VencimentoMaisAntigo().AddDays(dias);
            DateTime retorno = VencimentoMaisAntigo().AddDays(dias);            
            return retorno;
        }

        /// <summary>
        /// Reavalia o status de cobrança
        /// </summary>
        /// <returns></returns>
        public APTStatusCobranca Reavalia()
        {
            //No momento só estamos tirando da cobrança quem pagou tudo mas o certo é reavaliar o status completo
            CarregaBoletos();
            if (Boletos.Count == 0)
            {
                try
                {
                    EMPTProc1.AbreTrasacaoSQL("LinhaCobrancaProc.cs 231");
                    if (LinhaMae != null)
                        FrameworkProc.objetosNeon.LOGGeralProc.RegistraLogST(LLOTipo.Sistema, 0, DateTime.Now, "Cobrança extinta", LLOxXXX.LIC, linhaMae.LIC);
                    StatusCobranca = APTStatusCobranca.Terminada;
                    EMPTProc1.Commit();
                }
                catch (Exception ex)
                {
                    EMPTProc1.Vircatch(ex);
                }
            }
            return StatusCobranca;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NovoStatus"></param>
        /// <returns></returns>
        public DateTime CalculaDataBlocPorEmissao(APTStatusCobranca NovoStatus)
        {
            int dias = 30;
            switch (NovoStatus)
            {
                case APTStatusCobranca.Novo:                    
                case APTStatusCobranca.PrimeiraCarta:                    
                case APTStatusCobranca.Extra:
                    dias = 15;
                    break;
                case APTStatusCobranca.UltimoAviso:
                    dias = 5;
                    break;
                case APTStatusCobranca.juridico:
                case APTStatusCobranca.SemCobranca:
                case APTStatusCobranca.Terminada:
                    dias = 0;
                    break;
                default: throw new NotImplementedException(string.Format("CalculaProxRef para {0} não implementado", StatusCobranca));
            }
            return DateTime.Today.AddDays(dias);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Pendente()
        {
            if (DateTime.Today < ProximaData)
                return true;
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        protected List<BoletoProc> _Boletos;

        /// <summary>
        /// 
        /// </summary>
        public List<BoletoProc> Boletos
        {
            get => _Boletos ?? (_Boletos = new List<BoletoProc>());
            set => _Boletos = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool BoletosCarregados;

        private dLinhaCobranca _dLinhaCobranca;

        /// <summary>
        /// 
        /// </summary>
        public dLinhaCobranca DLinhaCobranca
        {
            get
            {
                if (_dLinhaCobranca == null)
                {
                    if (linhaMae == null)
                        _dLinhaCobranca = new dLinhaCobranca(EMPTProc1);
                    else
                        _dLinhaCobranca = (dLinhaCobranca)linhaMae.Table.DataSet;
                }
                return _dLinhaCobranca;
            }
        }

        /// <summary>
        /// Linha mãe
        /// </summary>
        protected dLinhaCobranca.LInhaCobrancaRow linhaMae;

        /// <summary>
        /// Linha mãe
        /// </summary>
        public dLinhaCobranca.LInhaCobrancaRow LinhaMae => linhaMae;
        

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc"></param>
        private LinhaCobrancaProc(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
        }

        /// <summary>
        /// Construtor para uma linha já existente. verifique depois se "encontrada" está como true
        /// </summary>
        /// <param name="LIC"></param>
        /// <param name="_EMPTProc1"></param>
        public LinhaCobrancaProc(int LIC, EMPTProc _EMPTProc1 = null)
            : this(_EMPTProc1)
        {
            EMPTProc1.EmbarcaEmTrans(DLinhaCobranca.LInhaCobrancaTableAdapter);
            if (DLinhaCobranca.LInhaCobrancaTableAdapter.FillByLIC(DLinhaCobranca.LInhaCobranca, LIC) > 0)
                linhaMae = DLinhaCobranca.LInhaCobranca[0];
        }        

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_LinhaMae"></param>        
        /// <param name="_apartamento"></param>
        public LinhaCobrancaProc(dLinhaCobranca.LInhaCobrancaRow _LinhaMae,ABS_Apartamento _apartamento = null)
            : this(((dLinhaCobranca)(_LinhaMae.Table.DataSet)).EMPTProc1)
        {
            linhaMae = _LinhaMae;
            if (_apartamento != null)
                Apartamento = _apartamento;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_apartamento"></param>
        /// <param name="ChecarLICnoBanco"></param>
        /// <param name="_EMPTProc1"></param>
        public LinhaCobrancaProc(ABS_Apartamento _apartamento,bool ChecarLICnoBanco=true, EMPTProc _EMPTProc1 = null) : this(_EMPTProc1)
        {
            _Apartamento = _apartamento;            
            if (ChecarLICnoBanco)
            {
                int encontradas = DLinhaCobranca.LInhaCobrancaTableAdapter.FillAberta_APT(DLinhaCobranca.LInhaCobranca, _apartamento.APT);
                if (encontradas == 1)
                    linhaMae = DLinhaCobranca.LInhaCobranca[0];
            }
        }

        #endregion

        /// <summary>
        /// ID
        /// </summary>
        public int LIC => LinhaMae != null ? LinhaMae.LIC : 0;

        private string CodigoModelo(ModelosCarta modelo)
        {
            switch (modelo)
            {
                case ModelosCarta.procuracao:
                    return "Cob Proc";
                default:
                    return "????";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected ABS_Apartamento _Apartamento;

        /// <summary>
        /// 
        /// </summary>
        public virtual ABS_Apartamento Apartamento
        {            
            get => _Apartamento ?? (_Apartamento = ABS_Apartamento.GetApartamentoProc(LinhaMae.LIC_APT));     
            set => _Apartamento = value;
        }

        private dBoletos _dBoletos;

        private dBoletos DBoletos
        {
            get => _dBoletos ?? (_dBoletos = new dBoletos());
            set => _dBoletos = value;            
        }

        private DataView _DVBoletos;

        private DataView DVBoletos
        {
            get
            {
                if (_DVBoletos == null)
                {                    
                    _DVBoletos = new DataView(DBoletos.BOLetos);
                    _DVBoletos.Sort = "BOLVencto";
                }
                return _DVBoletos;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Forcar"></param>
        protected void CarregaBoletos(bool Forcar = false)
        {            
            if (Forcar || !BoletosCarregados)
            {                                
                DBoletos.BOLetosTableAdapter.FillByCobrancaAPT(DBoletos.BOLetos, DateTime.Today.AddDays(-3), Apartamento.APT);                
                DBoletos.CamposComplementares(DateTime.Now, false);                
                foreach (DataRowView DRV in DVBoletos)
                {
                    dBoletos.BOLetosRow rowBOL = (dBoletos.BOLetosRow)DRV.Row;
                    Boletos.Add(DBoletos.Boletos[rowBOL.BOL]);                    
                }
                BoletosCarregados = true;
            }
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="M"></param>
        /// <returns></returns>
        protected static string NomeModelo(ModelosCarta M)
        {
            switch (M)
            {
                case ModelosCarta.procuracao:
                    return "Cob Proc";
                case ModelosCarta.implantacao:
                    return "Cob Imp.";
                case ModelosCarta.ultimoAviso:
                    return "Cob Último";
                case ModelosCarta.cartasimples:
                    return "Cob Carta Simples";
                case ModelosCarta.cobrancaextra:
                    return "Cob Extra";
                default: throw new NotImplementedException(string.Format("ModeloCarta:{0}",M));
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public enum ModelosCarta
    {
        /// <summary>
        /// 
        /// </summary>
        procuracao,
        /// <summary>
        /// 
        /// </summary>
        implantacao,
        /// <summary>
        /// 
        /// </summary>
        cartasimples,
        /// <summary>
        /// 
        /// </summary>
        cobrancaextra,
        /// <summary>
        /// 
        /// </summary>
        ultimoAviso,
    }
}
