﻿using FrameworkProc;
using System;

namespace CobrancaProc
{


    partial class dLinhaCobranca : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dLinhaCobranca(EMPTProc _EMPTProc1)
        : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        #region Adapters

        private dLinhaCobrancaTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dLinhaCobrancaTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dLinhaCobrancaTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dLinhaCobrancaTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dLinhaCobrancaTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dLinhaCobrancaTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return aPARTAMENTOSTableAdapter;
            }
        }

        private dLinhaCobrancaTableAdapters.LInhaCobrancaTableAdapter lInhaCobrancaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: LInhaCobranca
        /// </summary>
        public dLinhaCobrancaTableAdapters.LInhaCobrancaTableAdapter LInhaCobrancaTableAdapter
        {
            get
            {
                if (lInhaCobrancaTableAdapter == null)
                {
                    lInhaCobrancaTableAdapter = new dLinhaCobrancaTableAdapters.LInhaCobrancaTableAdapter();
                    lInhaCobrancaTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return lInhaCobrancaTableAdapter;
            }
        }

        #endregion

    }

}
