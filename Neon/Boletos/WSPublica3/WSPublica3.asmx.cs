using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using VirCrip;
using System.Configuration;
using System.IO;

namespace WSPublica3
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://neonimoveis.com.br/WSPublica3")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class WSPublica3 : System.Web.Services.WebService
    {
        private static string UltimoErro = "";

        private static System.Collections.ArrayList _Usados;

        private static System.Collections.ArrayList Usados
        { get { return _Usados ?? (_Usados = new ArrayList()); } }

        private int DecodificaChave(byte[] Chave)
        {
            string Aberto = VirCripWS.Uncrip(Chave);
            if ((Aberto.Length != 32) || (Aberto.Substring(25, 7) != "<-*W*->"))
                return -1;
            if (Usados.Contains(Aberto))
                return -1;
            Usados.Add(Aberto);
            //yyyyMMddHHmmssEMPRRRRRRRR<-*W*->
            DateTime DataHora = new DateTime(int.Parse(Aberto.Substring(0, 4)), int.Parse(Aberto.Substring(4, 2)), int.Parse(Aberto.Substring(6, 2)), int.Parse(Aberto.Substring(8, 2)), int.Parse(Aberto.Substring(10, 2)), int.Parse(Aberto.Substring(12, 2)));
            if (DataHora.AddDays(1).AddHours(2) < DateTime.Now)
                return -1;
            return int.Parse(Aberto.Substring(14, 3));
        }

        private string RetornodeErro(Exception e)
        {
            string Retorno = string.Format("ERRO {0:dd/MM/yyyy HH:mm:ss}", DateTime.Now);
            while (e != null)
            {
                Retorno += string.Format("\n{0}\n{1}", e.Message, e.StackTrace);
                e = e.InnerException;
            }
            return Retorno;
        }

        [WebMethod]
        public dAPartamentoDoc ListaAPT(byte[] Chave, int APT)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                }

                dAPartamentoDoc dAPD = new dAPartamentoDoc();

                dAPD.APartamentoDocTableAdapter.FillByAPT(dAPD.APartamentoDoc, EMP, APT);
 

                return dAPD;
            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return null;
            }
        }

        [WebMethod]
        public dAPartamentoDoc ListaAGB(byte[] Chave, int AGB)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                }

                dAPartamentoDoc dAPD = new dAPartamentoDoc();

                dAPD.AGendaDocTableAdapter.FillByAGB(dAPD.AGendaDoc, EMP, AGB);


                return dAPD;
            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return null;
            }
        }

        [WebMethod]
        public string PublicaDOC(byte[] Chave, string Nome, byte[] Dados)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return UltimoErro = "Erro na chave";
                string Arquivo = string.Format(@"{0}{1}", ConfigurationManager.AppSettings["publicacoesIND"], Nome);

                using (MemoryStream ms = new MemoryStream(Dados))
                {
                    using (FileStream fs = new FileStream(Arquivo, FileMode.Create))
                    {
                        ms.WriteTo(fs);
                        fs.Close();
                    };
                };
                return "ok";
            }
            catch (Exception e)
            {
                return UltimoErro = RetornodeErro(e);
            }
        }

        [WebMethod]
        public string PublicaDOCAPT(byte[] Chave, dAPartamentoDoc dAPartamentoDocNovo, byte[] Dados)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return UltimoErro = "Erro na chave";
                dAPartamentoDoc.APartamentoDocRow novaLinha = dAPartamentoDocNovo.APartamentoDoc[0];
                dAPartamentoDocNovo.APartamentoDocTableAdapter.Update(novaLinha);
                string Arquivo = string.Format(@"{0}APTDOC{1}.{2}", ConfigurationManager.AppSettings["publicacoesIND"], novaLinha.APD,novaLinha.APDEXT);

                using (MemoryStream ms = new MemoryStream(Dados))
                {
                    using (FileStream fs = new FileStream(Arquivo, FileMode.Create))
                    {
                        ms.WriteTo(fs);
                        fs.Close();
                    };
                };
                return "ok";
            }
            catch (Exception e)
            {
                return UltimoErro = RetornodeErro(e);
            }
        }

        [WebMethod]
        public string PublicaDOCAGD(byte[] Chave, dAPartamentoDoc dAPartamentoDocNovo, byte[] Dados)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return UltimoErro = "Erro na chave";
                dAPartamentoDoc.AGendaDocRow novaLinha = dAPartamentoDocNovo.AGendaDoc[0];
                dAPartamentoDocNovo.AGendaDocTableAdapter.Update(novaLinha);
                string Arquivo = string.Format(@"{0}AGDDOC{1}.{2}", ConfigurationManager.AppSettings["publicacoesIND"], novaLinha.AGD, novaLinha.AGDEXT);

                using (MemoryStream ms = new MemoryStream(Dados))
                {
                    using (FileStream fs = new FileStream(Arquivo, FileMode.Create))
                    {
                        ms.WriteTo(fs);
                        fs.Close();
                    };
                };
                return "ok";
            }
            catch (Exception e)
            {
                return UltimoErro = RetornodeErro(e);
            }
        }

        [WebMethod]
        public string PublicaCDR(byte[] Chave, dAPartamentoDoc dAPartamentoDocNovo)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return UltimoErro = "Erro na chave";
                dAPartamentoDoc dsLocal = new dAPartamentoDoc();
                dAPartamentoDocTableAdapters.CORPODIRETIVOTableAdapter CORPODIRETIVOTableAdapter = new dAPartamentoDocTableAdapters.CORPODIRETIVOTableAdapter();
                CORPODIRETIVOTableAdapter.Fill(dsLocal.CORPODIRETIVO);
                int nu = 0;
                int na = 0;
                foreach (dAPartamentoDoc.CORPODIRETIVORow novaRow in dAPartamentoDocNovo.CORPODIRETIVO)
                {
                    dAPartamentoDoc.CORPODIRETIVORow antigaRow = dsLocal.CORPODIRETIVO.FindByCDR_EMPCDR(EMP, novaRow.CDR);
                    if (antigaRow == null)
                    {
                        antigaRow = dsLocal.CORPODIRETIVO.NewCORPODIRETIVORow();
                        antigaRow.CDR_EMP = EMP;
                        antigaRow.CDR = novaRow.CDR;
                        na++;
                    }
                    else
                        nu++;
                    antigaRow.CDR_CON = novaRow.CDR_CON;
                    if (novaRow.IsAPTNumeroNull())
                        antigaRow.SetAPTNumeroNull();
                    else
                        antigaRow.APTNumero = novaRow.APTNumero;
                    if (novaRow.IsBLOCodigoNull())
                        antigaRow.SetBLOCodigoNull();
                    else
                        antigaRow.BLOCodigo = novaRow.BLOCodigo;
                    antigaRow.CDR_CGO = novaRow.CDR_CGO;
                    if (novaRow.IsPESEmailNull())
                        antigaRow.SetPESEmailNull();
                    else
                        antigaRow.PESEmail = novaRow.PESEmail;
                    if (novaRow.IsPESFone1Null())
                        antigaRow.SetPESFone1Null();
                    else
                        antigaRow.PESFone1 = novaRow.PESFone1;
                    antigaRow.PESNome = novaRow.PESNome;
                    if (antigaRow.RowState == DataRowState.Detached)
                        dsLocal.CORPODIRETIVO.AddCORPODIRETIVORow(antigaRow);
                };
                CORPODIRETIVOTableAdapter.Update(dsLocal.CORPODIRETIVO);
                return string.Format("ok insert: {0}, update: {1}", na, nu);
            }
            catch (Exception e)
            {
                return UltimoErro = RetornodeErro(e);
            }
        }

    }
}
