﻿namespace WSPublica3
{


    partial class dAPartamentoDoc
    {
        private dAPartamentoDocTableAdapters.APartamentoDocTableAdapter aPartamentoDocTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APartamentoDoc
        /// </summary>
        public dAPartamentoDocTableAdapters.APartamentoDocTableAdapter APartamentoDocTableAdapter
        {
            get
            {
                if (aPartamentoDocTableAdapter == null)
                {
                    aPartamentoDocTableAdapter = new dAPartamentoDocTableAdapters.APartamentoDocTableAdapter();
                };
                return aPartamentoDocTableAdapter;
            }
        }

        private dAPartamentoDocTableAdapters.AGendaDocTableAdapter aGendaDocTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: AGendaDoc
        /// </summary>
        public dAPartamentoDocTableAdapters.AGendaDocTableAdapter AGendaDocTableAdapter
        {
            get
            {
                if (aGendaDocTableAdapter == null)
                {
                    aGendaDocTableAdapter = new dAPartamentoDocTableAdapters.AGendaDocTableAdapter();
                };
                return aGendaDocTableAdapter;
            }
        }
    }
}
