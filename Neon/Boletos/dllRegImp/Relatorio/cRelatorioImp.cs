using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace dllRegImp.Relatorio
{
    public partial class cRelatorioImp : CompontesBasicos.ComponenteGradeNavegador
    {
       

        public cRelatorioImp()
        {
            
            InitializeComponent();
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if ((dateEdit1.Text == "") || (dateEdit2.Text == ""))
            {
                MessageBox.Show("Data inv�lida");
                return;
            }
            dRelatorio.ImpressoesTableAdapter.Fill(dRelatorio.Impressoes,dateEdit1.DateTime, dateEdit2.DateTime);
        }

        private void GridControl_F_Load(object sender, EventArgs e)
        {
            podeexportar = true;
            podeimprimir = true;
            BtnImprimir_F.Enabled = true;
            BtnExportar_F.Enabled = true;
        }
    }
}

