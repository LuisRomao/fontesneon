﻿namespace dllRegImp.Relatorio {


    partial class dRelatorio
    {
        private dRelatorioTableAdapters.RelatorioTableAdapter relatorioTableAdapter;
        private dRelatorioTableAdapters.ImpressoesTableAdapter impressoesTableAdapter;

        public dRelatorioTableAdapters.RelatorioTableAdapter RelatorioTableAdapter
        {
            get {
                if (relatorioTableAdapter == null) {
                    relatorioTableAdapter = new dllRegImp.Relatorio.dRelatorioTableAdapters.RelatorioTableAdapter();
                    relatorioTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return relatorioTableAdapter; 
            }
            
        }

        

        public dRelatorioTableAdapters.ImpressoesTableAdapter ImpressoesTableAdapter
        {
            get {
                if (impressoesTableAdapter == null) {
                    impressoesTableAdapter = new dllRegImp.Relatorio.dRelatorioTableAdapters.ImpressoesTableAdapter();
                    impressoesTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return impressoesTableAdapter; 
            }
        }
 
    }
}
