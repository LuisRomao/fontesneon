using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace dllRegImp
{
    /// <summary>
    /// Aponta impress�o
    /// </summary>
    public partial class cApontador : CompontesBasicos.ComponenteBaseBindingSource
    {
         
        /// <summary>
        /// Construtor
        /// </summary>
        public cApontador()
        {
            InitializeComponent();
            //tableAdapter = Framework.DSCentral.TiPoImpressaoTableAdapter;
            
            
        }

        
        private void FillLocal() {
            //Framework.DSCentral.DCentral.fill_CON();
            Framework.DSCentral.DCentral.fill_TPI();
            Framework.DSCentral.IMPressoesTableAdapter.Ultimos(Framework.DSCentral.DCentral.IMPressoes, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, DateTime.Today.AddDays(-5));
        }

        private void cApontador_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                iMPressoesBindingSource.DataSource =  tiPoImpressaoBindingSource.DataSource = Framework.DSCentral.DCentral;
                BindingSource_F.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST;
                FillLocal();                
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Framework.DSCentral.IMPressoesRow IMPressoesRow;
            IMPressoesRow = Framework.DSCentral.DCentral.IMPressoes.NewIMPressoesRow();            
            IMPressoesRow.IMP_CON = (int)lookUpCON.EditValue;
            IMPressoesRow.IMP_TPI = (int)lookUpTPI.EditValue;
            IMPressoesRow.IMPDATAI = DateTime.Now;
            IMPressoesRow.IMPQuantidade = (int)spinQuantidade.Value;
            IMPressoesRow.IMPObs = textEdit1.Text;
            IMPressoesRow.IMPI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
            Framework.DSCentral.DCentral.IMPressoes.AddIMPressoesRow(IMPressoesRow);
            Grava();
            textEdit1.Text = "";
            Gravado.Visible = true;
            gridView1.MoveLast();
            
            
        }

        private void Grava() {

            Framework.DSCentral.IMPressoesTableAdapter.Update(Framework.DSCentral.DCentral.IMPressoes);
            Framework.DSCentral.DCentral.IMPressoes.AcceptChanges();
        }

        private void lookUpCON_EditValueChanged(object sender, EventArgs e)
        {
            Gravado.Visible = false;
        }

        private void spinQuantidade_EditValueChanged(object sender, EventArgs e)
        {
            Gravado.Visible = false;
        }

        private void lookUpTPI_EditValueChanged(object sender, EventArgs e)
        {
            Gravado.Visible = false;
        }

        private void cApontador_Leave(object sender, EventArgs e)
        {
            Grava();
        }
          
        /// <summary>
        /// 
        /// </summary>
        public override void Refresh()
        {
            base.Refresh();
            Fill();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            Grava();
            return base.CanClose();
        }

        private void cApontador_Enter(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}

