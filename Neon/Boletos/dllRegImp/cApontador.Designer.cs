namespace dllRegImp
{
    partial class cApontador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lookUpCON = new DevExpress.XtraEditors.LookUpEdit();
            this.spinQuantidade = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpTPI = new DevExpress.XtraEditors.LookUpEdit();
            this.tiPoImpressaoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.Gravado = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.iMPressoesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIMP_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpCond = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colIMP_TPI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpTipo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colIMPQuantidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMPDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMPObs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpCON.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinQuantidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tiPoImpressaoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iMPressoesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CONDOMINIOS";
            this.BindingSource_F.DataSource = typeof(FrameworkProc.datasets.dCondominiosAtivos);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // lookUpCON
            // 
            this.lookUpCON.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CON", true));
            this.lookUpCON.Location = new System.Drawing.Point(97, 20);
            this.lookUpCON.Name = "lookUpCON";
            this.lookUpCON.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpCON.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CONCodigo", 74, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CONNome", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpCON.Properties.DataSource = this.BindingSource_F;
            this.lookUpCON.Properties.DisplayMember = "CONCodigo";
            this.lookUpCON.Properties.ShowHeader = false;
            this.lookUpCON.Properties.ValueMember = "CON";
            this.lookUpCON.Size = new System.Drawing.Size(404, 20);
            this.lookUpCON.TabIndex = 0;
            this.lookUpCON.EditValueChanged += new System.EventHandler(this.lookUpCON_EditValueChanged);
            // 
            // spinQuantidade
            // 
            this.spinQuantidade.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinQuantidade.Location = new System.Drawing.Point(97, 46);
            this.spinQuantidade.Name = "spinQuantidade";
            this.spinQuantidade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinQuantidade.Size = new System.Drawing.Size(50, 20);
            this.spinQuantidade.TabIndex = 1;
            this.spinQuantidade.EditValueChanged += new System.EventHandler(this.spinQuantidade_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(8, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(69, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Condom�nio:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Location = new System.Drawing.Point(10, 49);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(40, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "C�pias:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Location = new System.Drawing.Point(10, 75);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(42, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Motivo:";
            // 
            // lookUpTPI
            // 
            this.lookUpTPI.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.tiPoImpressaoBindingSource, "TPI", true));
            this.lookUpTPI.Location = new System.Drawing.Point(97, 72);
            this.lookUpTPI.Name = "lookUpTPI";
            this.lookUpTPI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpTPI.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TPIDescricao", "TPIDescricao", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpTPI.Properties.DataSource = this.tiPoImpressaoBindingSource;
            this.lookUpTPI.Properties.DisplayMember = "TPIDescricao";
            this.lookUpTPI.Properties.ShowHeader = false;
            this.lookUpTPI.Properties.ValueMember = "TPI";
            this.lookUpTPI.Size = new System.Drawing.Size(404, 20);
            this.lookUpTPI.TabIndex = 5;
            this.lookUpTPI.EditValueChanged += new System.EventHandler(this.lookUpTPI_EditValueChanged);
            // 
            // tiPoImpressaoBindingSource
            // 
            this.tiPoImpressaoBindingSource.DataMember = "TiPoImpressao";
            this.tiPoImpressaoBindingSource.DataSource = typeof(Framework.DSCentral);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(8, 140);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(313, 46);
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "GRAVAR";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // Gravado
            // 
            this.Gravado.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Gravado.Appearance.ForeColor = System.Drawing.Color.Red;
            this.Gravado.Location = new System.Drawing.Point(359, 152);
            this.Gravado.Name = "Gravado";
            this.Gravado.Size = new System.Drawing.Size(105, 25);
            this.Gravado.TabIndex = 7;
            this.Gravado.Text = "GRAVADO";
            this.Gravado.Visible = false;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.iMPressoesBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(8, 192);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpCond,
            this.LookUpTipo});
            this.gridControl1.Size = new System.Drawing.Size(615, 280);
            this.gridControl1.TabIndex = 8;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // iMPressoesBindingSource
            // 
            this.iMPressoesBindingSource.DataMember = "IMPressoes";
            this.iMPressoesBindingSource.DataSource = typeof(Framework.DSCentral);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIMP_CON,
            this.colIMP_TPI,
            this.colIMPQuantidade,
            this.colIMPDATAI,
            this.colIMPObs});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIMPDATAI, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colIMP_CON
            // 
            this.colIMP_CON.Caption = "Condom�nio";
            this.colIMP_CON.ColumnEdit = this.LookUpCond;
            this.colIMP_CON.FieldName = "IMP_CON";
            this.colIMP_CON.Name = "colIMP_CON";
            this.colIMP_CON.Visible = true;
            this.colIMP_CON.VisibleIndex = 0;
            this.colIMP_CON.Width = 140;
            // 
            // LookUpCond
            // 
            this.LookUpCond.AutoHeight = false;
            this.LookUpCond.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpCond.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CONCodigo", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CONNome", 300, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpCond.DataSource = this.BindingSource_F;
            this.LookUpCond.DisplayMember = "CONCodigo";
            this.LookUpCond.Name = "LookUpCond";
            this.LookUpCond.PopupWidth = 400;
            this.LookUpCond.ShowHeader = false;
            this.LookUpCond.ValueMember = "CON";
            // 
            // colIMP_TPI
            // 
            this.colIMP_TPI.Caption = "Tipo";
            this.colIMP_TPI.ColumnEdit = this.LookUpTipo;
            this.colIMP_TPI.FieldName = "IMP_TPI";
            this.colIMP_TPI.Name = "colIMP_TPI";
            this.colIMP_TPI.Visible = true;
            this.colIMP_TPI.VisibleIndex = 1;
            this.colIMP_TPI.Width = 148;
            // 
            // LookUpTipo
            // 
            this.LookUpTipo.AutoHeight = false;
            this.LookUpTipo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpTipo.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TPIDescricao", "TPIDescricao", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpTipo.DataSource = this.tiPoImpressaoBindingSource;
            this.LookUpTipo.DisplayMember = "TPIDescricao";
            this.LookUpTipo.Name = "LookUpTipo";
            this.LookUpTipo.PopupWidth = 400;
            this.LookUpTipo.ShowHeader = false;
            this.LookUpTipo.ValueMember = "TPI";
            // 
            // colIMPQuantidade
            // 
            this.colIMPQuantidade.Caption = "Quantidade";
            this.colIMPQuantidade.FieldName = "IMPQuantidade";
            this.colIMPQuantidade.Name = "colIMPQuantidade";
            this.colIMPQuantidade.Visible = true;
            this.colIMPQuantidade.VisibleIndex = 2;
            this.colIMPQuantidade.Width = 88;
            // 
            // colIMPDATAI
            // 
            this.colIMPDATAI.Caption = "Data";
            this.colIMPDATAI.FieldName = "IMPDATAI";
            this.colIMPDATAI.Name = "colIMPDATAI";
            this.colIMPDATAI.Visible = true;
            this.colIMPDATAI.VisibleIndex = 3;
            this.colIMPDATAI.Width = 93;
            // 
            // colIMPObs
            // 
            this.colIMPObs.Caption = "Observa��o";
            this.colIMPObs.FieldName = "IMPObs";
            this.colIMPObs.Name = "colIMPObs";
            this.colIMPObs.Visible = true;
            this.colIMPObs.VisibleIndex = 4;
            this.colIMPObs.Width = 336;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Location = new System.Drawing.Point(10, 101);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(24, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Obs:";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(97, 98);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.MaxLength = 40;
            this.textEdit1.Size = new System.Drawing.Size(404, 20);
            this.textEdit1.TabIndex = 10;
            // 
            // cApontador
            // 
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.Gravado);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.lookUpTPI);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.spinQuantidade);
            this.Controls.Add(this.lookUpCON);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cApontador";
            this.Size = new System.Drawing.Size(640, 475);
            this.Load += new System.EventHandler(this.cApontador_Load);
            this.Enter += new System.EventHandler(this.cApontador_Enter);
            this.Leave += new System.EventHandler(this.cApontador_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpCON.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinQuantidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tiPoImpressaoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iMPressoesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private DevExpress.XtraEditors.LookUpEdit lookUpCON;
        private DevExpress.XtraEditors.SpinEdit spinQuantidade;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpTPI;
        private System.Windows.Forms.BindingSource tiPoImpressaoBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl Gravado;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource iMPressoesBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colIMP_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colIMP_TPI;
        private DevExpress.XtraGrid.Columns.GridColumn colIMPQuantidade;
        private DevExpress.XtraGrid.Columns.GridColumn colIMPDATAI;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpCond;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colIMPObs;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit textEdit1;
    }
}
