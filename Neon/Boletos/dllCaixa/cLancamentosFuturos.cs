using System;
using System.Collections;
using System.Data;
using System.Drawing;
using dllCheques;
using dllVirEnum;
using Framework;
using FrameworkProc;
using Framework.objetosNeon;
using VirEnumeracoesNeon;

namespace dllCaixa
{
    /// <summary>
    /// Lan�amentos Futuros
    /// </summary>
    public partial class cLancamentosFuturos : Framework.ComponentesNeon.NavegadorCondominio
    {
        private dLancamentosFuturos.LancamentosRow LinhaMae
        {
            get { return (dLancamentosFuturos.LancamentosRow)LinhaMae_F; }
        }

        /// <summary>
        /// Rastreia
        /// </summary>
        protected override void Alterar()
        {            
            if (LinhaMae_F != null)
            {
                if (!LinhaMae.IsCHENull())                
                    Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Cheque, LinhaMae.CHE, false, false);
                if (!LinhaMae.IsPGFNull())
                    Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Periodicos, LinhaMae.PGF, false, false);
                //dLancamentosFuturos.LancamentosRow row
            }

        }

        private static VirEnum virEnumTipoLancFut;

        /// <summary>
        /// VirEnum para TipoLancFut
        /// </summary>
        public static VirEnum VirEnumTipoLancFut
        {
            get
            {
                if (virEnumTipoLancFut == null)
                {
                    virEnumTipoLancFut = new VirEnum(typeof(TipoLancFut));
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.Vago, " - ");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.Boleto, "Arrecada��o");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.FundoReserva, "Fundo de Reserva");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.ChequeCadastrado, "CH Emitir");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.ChequeEmitido, "CH Emitido");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.DebitoAutomatico, "D�bito Auto.");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.Eletronico, "Eletr�nico");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.Extrato, "Extrato");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.ExtratoAberto, "Extrato Aberto");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.Acordo, "Acordo");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.Rateio, "Rateio");
                    virEnumTipoLancFut.GravaNomes(TipoLancFut.Futuros, "Lan�amentos Futuros");
                    //
                    //virEnumTipoLancFut.GravaNomes(TipoLancFut.ChequeCadastrado, "CH Emitir");


                }
                return virEnumTipoLancFut;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public cLancamentosFuturos()
        {
            InitializeComponent();
            dateI.DateTime = DateTime.Today;
            dateF.DateTime = DateTime.Today.AddMonths(1);
            VirEnumTipoLancFut.CarregaEditorDaGrid(colTipo);
            GridView_F.OptionsBehavior.Editable = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                decimal ValorA = 0;
                decimal ValorD = 0;
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONValorCondominio from condominios where CON = @P1", out ValorA, CON);
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("SELECT SUM(PGFValor) FROM PaGamentosFixos WHERE (PGF_CON = @P1) AND (PGFStatus IN (2,3))", out ValorD, CON);
                calcEditA.Value = ValorA;
                calcEditD.Value = ValorD;
                calcEditM.Value = ValorA - ValorD;
                GridView_F.BeginDataUpdate();
                Popular();
            }
            finally 
            {
                GridView_F.EndDataUpdate();
            };
            GridView_F.Focus();
        }

        private DateTime ProximoDia;

        private decimal EntradaAberta = 0;

        private bool CarregarCondominioMes(Competencia comp, decimal Total,TipoArrecadacao Tipo)
        {
            bool retorno = false;
            decimal JaEntrou = 0;
            //descartar se o mes for anterior e abater valor se for no mes
            DateTime DataBase = comp.VencimentoPadrao;
            cDiaUtil cDiaUtilBase = new cDiaUtil(DataBase, Sentido.Frente);
            if (comp == compAtiva)
            {
                string PLA = Tipo == TipoArrecadacao.condominio ? "101000" : "110000";
                object oJaEntrou = dLancamentosFuturos.EstatisticaTableAdapter.Recebido((short)compAtiva.Ano, (short)compAtiva.Mes, CON,PLA);
                JaEntrou = oJaEntrou == null ? 0 : (decimal)oJaEntrou;
                if (EntradaAberta > 0)
                {
                    DataRow DadosCON = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select CONValorCondominio,CONValorFR,CONTipoFR,CONTotalApartamento from condominios where CON = @P1", CON);
                    if (DadosCON != null)
                    {
                        decimal ValCond = (decimal)DadosCON["CONValorCondominio"];
                        decimal ValFR = 0;
                        if ((DadosCON["CONTipoFR"] != DBNull.Value) && (DadosCON["CONValorFR"] != DBNull.Value))
                        {
                            if ((string)DadosCON["CONTipoFR"] == "F")
                            {
                                if (DadosCON["CONTotalApartamento"] != DBNull.Value)
                                    ValFR = (decimal)DadosCON["CONValorFR"] * (short)DadosCON["CONTotalApartamento"];
                            }
                            else if ((string)DadosCON["CONTipoFR"] == "P")
                                ValFR = (decimal)DadosCON["CONValorFR"] * (decimal)DadosCON["CONValorCondominio"] / 100;
                            decimal porcFR = ValFR / (ValFR + ValCond);
                            decimal FREntrou = Math.Round(EntradaAberta * porcFR, 2);
                            if (Tipo == TipoArrecadacao.FundoReserva)
                                JaEntrou += FREntrou;
                            else
                                JaEntrou += (EntradaAberta - FREntrou);
                        }
                    }                    
                }
            };
            foreach (DictionaryEntry DE in Hgeral)
            {
                int deltaDiasUteis = (int)DE.Key;
                DateTime Data = cDiaUtilBase.AddDiasUteis(deltaDiasUteis);

                if (deltaDiasUteis > 10)
                    continue;

                //cuidado com o parcial
                if (Data <= dateF.DateTime)
                {
                    retorno = true;
                    decimal Valor = Total * (decimal)DE.Value;
                    if (JaEntrou > 0)
                    {
                        if (Valor <= JaEntrou)
                        {
                            JaEntrou -= Valor;
                            continue;
                        }
                        else
                        {
                            Valor -= JaEntrou;
                            JaEntrou = 0;
                        }
                    };
                    dLancamentosFuturos.LancamentosRow rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                    rowNova.Vencimento = DataBase;
                    rowNova.Data = Data;
                    if (Data < ProximoDia)
                        rowNova.Data = cDiaUtil.Ajusta(ProximoDia, Sentido.Frente);
                    //if (rowNova.Data < ProximoDia)
                    //    rowNova.Data = ProximoDia;
                    //rowNova.Data = rowNova.Data.AddMinutes(1);
                    rowNova.Descricao = string.Format("{0} {1} {2:n2} ({3}) {4:n0}%",
                                              Tipo == TipoArrecadacao.condominio ? "Condom�nio" : "Fundo de Reserva",
                                              comp, 
                                              Total, 
                                              (int)DE.Key, 
                                              (decimal)DE.Value * 100);
                    rowNova.Tipo = Tipo == TipoArrecadacao.condominio ? (int)TipoLancFut.Boleto : (int)TipoLancFut.FundoReserva;
                    rowNova.Valor = Valor;
                    dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                    if (!Datas.Contains(rowNova.Data))
                        Datas.Add(rowNova.Data);
                }
            }

            return retorno;
        }

        SortedList EstatisticasUsadas;
        SortedList Hgeral;

        private SortedList HistoricoEntrada()
        {
            if (EstatisticasUsadas != null)
                return EstatisticasUsadas;
            compAtiva = new Competencia(ProximoDia.Month, ProximoDia.Year, CON);
            while (compAtiva.CalculaVencimento().AddDays(-10) > ProximoDia)
                compAtiva--;
            while (compAtiva.CalculaVencimento().AddDays(10) < ProximoDia)
                compAtiva++;
            EstatisticasUsadas = new SortedList();
            for (Competencia comp = compAtiva.CloneCompet(-3); comp < compAtiva; comp++)
            {
                if (comp < new Competencia(5, 2009))
                    continue;
                object oValorPrevisto = dLancamentosFuturos.EstatisticaTableAdapter.ValorPrevisto((short)comp.Ano, (short)comp.Mes, CON);
                if (oValorPrevisto == null)
                    continue;
                SortedList H = new SortedList();
                //EstatisticasUsadas.Add(comp.ToString(), H);
                decimal ValorPrevisto = (decimal)oValorPrevisto;

                DateTime Vencimento = comp.VencimentoPadrao;
                dLancamentosFuturos.EstatisticaTableAdapter.Fill(dLancamentosFuturos.Estatistica, CON, (short)comp.Ano, (short)comp.Mes, Vencimento.AddMonths(-1));
                foreach (dLancamentosFuturos.EstatisticaRow Estrow in dLancamentosFuturos.Estatistica)
                {
                    if (Estrow.ValorPago == 0)
                        continue;
                    int delta = cDiaUtil.DiasUtes(Vencimento, Estrow.SCCData);
                    if (H.Contains(delta))
                        H[delta] = (decimal)H[delta] + ValorPrevisto / Estrow.ValorPago;
                    else
                        H.Add(delta, Estrow.ValorPago / ValorPrevisto);
                };
                if (H.Count != 0)
                    EstatisticasUsadas.Add(comp.ToString(), H);
            }

            int usadas = EstatisticasUsadas.Count;
            if (usadas == 0)
            {
                Hgeral = new SortedList();
                Hgeral.Add(-1, 0.1M);
                Hgeral.Add(0, 0.6M);
                Hgeral.Add(1, 0.1M);
            }
            else
            {
                bEstatistica.Enabled = true;
                if (usadas == 1)
                    Hgeral = (SortedList)EstatisticasUsadas.GetByIndex(0);
                else
                {
                    Hgeral = new SortedList();
                    foreach (DictionaryEntry DE in EstatisticasUsadas)
                    {
                        SortedList Hx = (SortedList)DE.Value;
                        foreach (DictionaryEntry HDE in Hx)
                        {
                            if (Hgeral.Contains(HDE.Key))
                                Hgeral[HDE.Key] = (decimal)Hgeral[HDE.Key] + (decimal)HDE.Value / usadas;
                            else
                                Hgeral.Add(HDE.Key, (decimal)HDE.Value / usadas);
                        }
                    }
                };
            }
            return Hgeral;
        }

        //private int CONDiaVencimento;
        //private decimal CONValorCondominio;
        private Competencia compAtiva;

        enum TipoArrecadacao
        {
            condominio,
            FundoReserva
        }

        private void CarregarCondominio(TipoArrecadacao Tipo)
        {
            DataRow DadosCON = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select CONValorCondominio,CONValorFR,CONTipoFR,CONTotalApartamento from condominios where CON = @P1", CON);
            if (DadosCON == null)
                return;
            decimal Valor;

            if (Tipo == TipoArrecadacao.condominio)
            {
                if (DadosCON["CONValorCondominio"] == DBNull.Value)
                    return;
                Valor = (decimal)DadosCON["CONValorCondominio"];
            }
            else
            {
                if ((DadosCON["CONTipoFR"] == DBNull.Value) || (DadosCON["CONValorFR"] == DBNull.Value))
                    return;
                if ((string)DadosCON["CONTipoFR"] == "F")
                {
                    if (DadosCON["CONTotalApartamento"] == DBNull.Value)
                        return;
                    Valor = (decimal)DadosCON["CONValorFR"] * (short)DadosCON["CONTotalApartamento"];
                }
                else if ((string)DadosCON["CONTipoFR"] == "P")
                    Valor = (decimal)DadosCON["CONValorFR"] * (decimal)DadosCON["CONValorCondominio"] / 100;
                else
                    return;

            }

            //if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONValorCondominio from condominios where CON = @P1",out CONValorCondominio,CON))
            //    return;

            
            HistoricoEntrada();
            for (Competencia Comp = compAtiva.CloneCompet(); ; Comp++)
                if (!CarregarCondominioMes(Comp, Valor,Tipo))
                    break;
        }

        private void CarregarAcordos() 
        {
            
            dLancamentosFuturos.BOLetosTableAdapter.FillAcordos(dLancamentosFuturos.BOLetos, CON, new cDiaUtil(ProximoDia, Sentido.Traz).AddDiasUteis(-2),dateF.DateTime);
            foreach (dLancamentosFuturos.BOLetosRow rowAcordo in dLancamentosFuturos.BOLetos)
            {
                dLancamentosFuturos.LancamentosRow rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                rowNova.Vencimento = rowAcordo.BOLVencto;
                rowNova.Data = (new cDiaUtil(rowAcordo.BOLVencto, Sentido.Frente)).AddDiasUteis(2);                
                if (rowNova.Data < ProximoDia)
                    rowNova.Data = cDiaUtil.Ajusta(ProximoDia, Sentido.Frente);
                rowNova.Descricao = "Acordos";
                rowNova.Tipo = (int)TipoLancFut.Acordo;
                rowNova.Valor = rowAcordo.ValorPrevisto;
                dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                if (!Datas.Contains(rowNova.Data))
                    Datas.Add(rowNova.Data);
            }
        }

        private bool CarregarRateioMes(Competencia comp, dLancamentosFuturos.RAteioDetalhesRow rowRAD)
        {
            bool retorno = false;

            decimal JaEntrou = 0;
            //descartar se o mes for anterior e abater valor se for no mes
            DateTime DataBase = rowRAD.RADData;
            cDiaUtil cDiaUtilBase = new cDiaUtil(DataBase, Sentido.Frente);
            if (comp == compAtiva)
            {
                object oJaEntrou = dLancamentosFuturos.EstatisticaTableAdapter.RecebidoRateio(rowRAD.RAD);
                JaEntrou = oJaEntrou == null ? 0 : (decimal)oJaEntrou;
            };
            foreach (DictionaryEntry DE in Hgeral)
            {
                DateTime Data = cDiaUtilBase.AddDiasUteis((int)DE.Key);

                //cuidado com o parcial
                if (Data <= dateF.DateTime)
                {
                    retorno = true;
                    decimal Valor = rowRAD.RADValor * (decimal)DE.Value;
                    if (JaEntrou > 0)
                    {
                        if (Valor <= JaEntrou)
                        {
                            JaEntrou -= Valor;
                            continue;
                        }
                        else
                        {
                            Valor -= JaEntrou;
                            JaEntrou = 0;
                        }
                    };
                    dLancamentosFuturos.LancamentosRow rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                    rowNova.Vencimento = DataBase;
                    rowNova.Data = Data;
                    if (Data < ProximoDia)
                        rowNova.Data = cDiaUtil.Ajusta(ProximoDia, Sentido.Frente);
                    //if (rowNova.Data < ProximoDia)
                    //    rowNova.Data = ProximoDia;
                    rowNova.Descricao = string.Format("Rateio: {0} {1:n2} ({2}) {3:n0}%", rowRAD.RADMensagem, rowRAD.RADValor, (int)DE.Key, (decimal)DE.Value * 100);
                    rowNova.Tipo = (int)TipoLancFut.Rateio;
                    rowNova.Valor = Valor;
                    dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                    if (!Datas.Contains(rowNova.Data))
                        Datas.Add(rowNova.Data);
                }
            }

            return retorno;
        }

        private void CarregarRateios() 
        {
            HistoricoEntrada();
            foreach (DataRowView DRV in checkedListBoxControl1.CheckedItems)
            {
                dLancamentosFuturos.RATeiosRow RATrow = (dLancamentosFuturos.RATeiosRow)DRV.Row;
                
                dLancamentosFuturos.RAteioDetalhesTableAdapter.Fill(dLancamentosFuturos.RAteioDetalhes, RATrow.RAT);
                foreach (dLancamentosFuturos.RAteioDetalhesRow rowRAD in dLancamentosFuturos.RAteioDetalhes) 
                {
                    Competencia comp = new Competencia(rowRAD.RADCompetenciaMes, rowRAD.RADCompetenciaAno);
                    if (comp < compAtiva)
                        continue;
                    if (!CarregarRateioMes(comp,rowRAD))
                        break;
                }
            };
            
            /*
            dLancamentosFuturos.RateiosTableAdapter.Fill(dLancamentosFuturos.Rateios, CON, new cDiaUtil(ProximoDia, Sentido.Traz).AddDiasUteis(-2), dateF.DateTime);
            foreach (dLancamentosFuturos.RateiosRow rowRat in dLancamentosFuturos.Rateios)
            {
                dLancamentosFuturos.LancamentosRow rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                rowNova.Vencimento = rowRat.BOLVencto;
                rowNova.Data = (new cDiaUtil(rowRat.BOLVencto, Sentido.Frente)).AddDiasUteis(2);
                if (rowNova.Data < ProximoDia)
                    rowNova.Data = cDiaUtil.Ajusta(ProximoDia, Sentido.Frente);
                rowNova.Descricao = string.Format("Rateio {0}",rowRat.BODMensagem);
                rowNova.Tipo = (int)TipoLancFut.Rateio;
                rowNova.Valor = rowRat.ValorPrevisto;
                dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                if (!Datas.Contains(rowNova.Data))
                    Datas.Add(rowNova.Data);
            }
            */
        }

        private void CarregarCheques(CHEStatus CHEStatus,TipoLancFut TipoLF) 
        {
            foreach (dLancamentosFuturos.CHEquesRow rowChe in dLancamentosFuturos.CHEquesTableAdapter.GetDados(CON, (int)CHEStatus, dateF.DateTime)) 
            {
                dLancamentosFuturos.LancamentosRow rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                rowNova.CHE = rowChe.CHE;
                rowNova.Vencimento = rowChe.CHEVencimento;
                switch ((PAGTipo)rowChe.CHETipo)
                {
                    case PAGTipo.Guia:
                    case PAGTipo.boleto:
                    case PAGTipo.deposito:
                        rowNova.Data = Framework.cDiaUtil.Ajusta(rowChe.CHEVencimento,Sentido.Frente);
                        break;                                            
                    case PAGTipo.cheque:
                    case PAGTipo.sindico:
                        rowNova.Data = (new cDiaUtil(rowChe.CHEVencimento,Sentido.Frente)).AddDiasUteis(1);
                        break;
                    default:
                        rowNova.Data = rowChe.CHEVencimento;
                        break;
                }
                if (rowNova.Data < ProximoDia)
                    rowNova.Data =  cDiaUtil.Ajusta(ProximoDia,Sentido.Frente);
                rowNova.Descricao = string.Format("Cheque {0}: {1}",rowChe.IsCHENumeroNull() ? "":rowChe.CHENumero.ToString(),rowChe.CHEFavorecido);                
                rowNova.Tipo = (int)TipoLF;
                rowNova.Valor = -rowChe.CHEValor;
                dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                if (!Datas.Contains(rowNova.Data))
                    Datas.Add(rowNova.Data);
            }
        }

        ArrayList Datas;

        private void CalculaSaldos() 
        {
            decimal Saldo = 0;
            bool SaldoInicial = true;
            dLancamentosFuturosBindingSource.Sort = "Data,Tipo,Vencimento";
            foreach (DataRowView DRV in dLancamentosFuturosBindingSource.List)
            {
                dLancamentosFuturos.LancamentosRow rowL = (dLancamentosFuturos.LancamentosRow)DRV.Row;
                //if ((TipoLancFut)rowL.Tipo == TipoLancFut.ExtratoAberto)
                //    rowL.SetSaldoNull();
                //else
                //{
                    rowL.Saldo = Saldo = Saldo + rowL.Valor;
                    if (rowL.Valor > 0)
                        rowL.Credito = rowL.Valor;
                    else
                        if (rowL.Valor < 0)
                            rowL.Debito = -rowL.Valor;
                    if (SaldoInicial)
                    {
                        SaldoInicial = false;
                        rowL.SetCreditoNull();
                        rowL.SetDebitoNull();
                    }
                //}
            }
            
        }

        private void CarregarExtrato() 
        {                        
            bool Primeiro = true;
            EntradaAberta = 0;
            dLancamentosFuturos.LancamentosRow rowNova;
            dLancamentosFuturos.ExtratoDataTable TabelaResposta = dLancamentosFuturos.ExtratoTableAdapter.GetData(CON, dateI.DateTime);
            if (TabelaResposta.Rows.Count <= 0)
            {
                object oDataCorrigida = dLancamentosFuturos.ExtratoTableAdapter.UltimoDia(CON);
                if (oDataCorrigida != null)
                {
                    dateI.DateTime = (DateTime)oDataCorrigida;
                    TabelaResposta = dLancamentosFuturos.ExtratoTableAdapter.GetData(CON, dateI.DateTime);
                }
            }
            if (TabelaResposta.Rows.Count > 0)
                foreach (dLancamentosFuturos.ExtratoRow rowChe in TabelaResposta)
                {
                    if (Primeiro)
                    {
                        rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                        rowNova.Vencimento = rowNova.Data = rowChe.SCCDataA;
                        rowNova.Descricao = "Saldo anterior";
                        rowNova.Tipo = (int)TipoLancFut.Extrato;
                        rowNova.Valor = rowChe.SCCValorI;
                        dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                        if (!Datas.Contains(rowNova.Data))
                            Datas.Add(rowNova.Data);
                        Primeiro = false;
                    };
                    rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                    rowNova.Vencimento = rowNova.Data = rowChe.SCCData;
                    rowNova.Descricao = string.Format("{0} ({1})", rowChe.CCDDescricao, rowChe.CCDDocumento);
                    rowNova.Tipo = rowChe.SCCAberto ? (int)TipoLancFut.ExtratoAberto : (int)TipoLancFut.Extrato;
                    rowNova.Valor = rowChe.CCDValor;
                    if ((rowChe.SCCAberto) && (rowChe.CCDCategoria == 202))
                    {
                        EntradaAberta += rowChe.CCDValor;
                    }
                    if (!rowChe.SCCAberto)
                        ProximoDia = (new cDiaUtil(rowNova.Data,Sentido.Frente)).AddDiasUteis(1);
                    dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                    if (!Datas.Contains(rowNova.Data))
                        Datas.Add(rowNova.Data);
                }
        }

        private void Popular()
        {
            dLancamentosFuturos.Lancamentos.Clear();
            Datas = new ArrayList();
            ProximoDia = DateTime.Today;
            CarregarExtrato();
            if (chCheques.Checked)
            {
                CarregarCheques(CHEStatus.Aguardacopia, TipoLancFut.ChequeEmitido);
                CarregarCheques(CHEStatus.AguardaRetorno, TipoLancFut.ChequeEmitido);
                CarregarCheques(CHEStatus.EnviadoBanco, TipoLancFut.ChequeEmitido);
                CarregarCheques(CHEStatus.Retirado, TipoLancFut.ChequeEmitido);
                CarregarCheques(CHEStatus.Retornado, TipoLancFut.ChequeEmitido);
            };
            if (chChequesCa.Checked)
            {
                CarregarCheques(CHEStatus.Cadastrado, TipoLancFut.ChequeCadastrado);
            };
            if (chBoletos.Checked)
            {
                CarregarCondominio(TipoArrecadacao.condominio);
            };
            if (chFR.Checked)
            {
                CarregarCondominio(TipoArrecadacao.FundoReserva);
            };
            if (chAcordos.Checked)
            {
                CarregarAcordos();
            };
            if (chRateios.Checked)
            {
                CarregarRateios();
            };

            if (chPagFut.Checked)
            {
                CarregarCheques(CHEStatus.DebitoAutomatico, TipoLancFut.DebitoAutomatico);
                CarregaLancamentosFuturos();
            }

            if (chDias.Checked)
            {
                for (DateTime Data = dateI.DateTime; Data <= dateF.DateTime; Data = Data.AddDays(1))
                {
                    if (!Datas.Contains(Data))
                    {
                        dLancamentosFuturos.LancamentosRow rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                        rowNova.Data = rowNova.Vencimento = Data;
                        rowNova.Tipo = (int)TipoLancFut.Vago;
                        rowNova.Valor = 0;
                        dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                    }
                };
            }
            CalculaSaldos();
        }

        private void CarregaLancamentosFuturos()
        {            
            foreach (dLancamentosFuturos.PaGamentosFixosRow PGFrow in dLancamentosFuturos.PaGamentosFixosTableAdapter.GetData(CON))
            {
                Competencia CompI = new Competencia(PGFrow.PGFProximaCompetencia,CON,null);
                if (!PGFrow.IsNOANull())
                    CompI++;
                Competencia CompF = null;
                if (!PGFrow.IsPGFCompetFNull())
                    CompF = new Competencia(PGFrow.PGFCompetF, CON, null);
                else
                    CompF = new Competencia(dateF.DateTime.Month,dateF.DateTime.Year, CON).Add(PGFrow.PGFRef + 2);
                while (CompI <= CompF)
                {
                    Competencia compPAG = CompI.CloneCompet(PGFrow.PGFRef);
                    DateTime data = compPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);
                    if (data > dateF.DateTime)
                        break;
                    dLancamentosFuturos.LancamentosRow rowNova = dLancamentosFuturos.Lancamentos.NewLancamentosRow();
                    rowNova.Vencimento = data;
                    //o avanco de 2 dias uteis � bom para cheques mas pode ser melhorado para d�bito autom�tico 
                    rowNova.Data = (new cDiaUtil(data, Sentido.Frente)).AddDiasUteis(2);
                    if (rowNova.Data < ProximoDia)
                        rowNova.Data = cDiaUtil.Ajusta(ProximoDia, Sentido.Frente);
                    string Identificacao = string.Format("{0}{1}{2}",
                                               PGFrow.IsPGFCodigoDebitoNull() ? "" : PGFrow.PGFCodigoDebito.ToString(),
                                               PGFrow.IsPGFCodigoDebitoNull() || PGFrow.IsPGFIdentificacaoNull() ? "" : " - ",
                                               PGFrow.IsPGFIdentificacaoNull() ? "" : PGFrow.PGFIdentificacao.ToString());
                    rowNova.Descricao = string.Format("{0} {1} REF:{2}", PGFrow.PGFDescricao,Identificacao, CompI);
                    rowNova.Tipo = (int)TipoLancFut.Futuros;
                    rowNova.Valor = -PGFrow.PGFValor;
                    rowNova.PGF = PGFrow.PGF;
                    dLancamentosFuturos.Lancamentos.AddLancamentosRow(rowNova);
                    if (!Datas.Contains(rowNova.Data))
                        Datas.Add(rowNova.Data);
                    CompI++;
                }
                
            }
        }

        private Font FonteBold;
                               
        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {           
            if ((e.Column != colSaldo) || (e.CellValue == null))
                return;
            if ((e.CellValue != null) && (e.CellValue != DBNull.Value))
                if ((decimal)e.CellValue < 0)
                {
                    e.Appearance.ForeColor = Color.Red;
                    if (FonteBold == null)
                        FonteBold = new Font(e.Appearance.Font, FontStyle.Bold);
                    e.Appearance.Font = FonteBold;
                }
        }

        private void Limpa() 
        {
            dLancamentosFuturos.Lancamentos.Clear();
            bEstatistica.Enabled = false;
            EstatisticasUsadas = null;
        }

        private void cLancamentosFuturos_CondominioAlterado(object sender, EventArgs e)
        {
            BotCarregar.Enabled = (CON != -1);            
            Limpa();
        }

        private void Limpar(object sender, EventArgs e)
        {
            Limpa();
        }

        private void GridView_F_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            
            dLancamentosFuturos.LancamentosRow row = (dLancamentosFuturos.LancamentosRow)GridView_F.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            Color CorFundo1 = Color.FromArgb(247, 251, 255);
            Color CorFundo2 = Color.FromArgb(236, 246, 255);
            if (((TipoLancFut)row.Tipo == TipoLancFut.Extrato) || ((TipoLancFut)row.Tipo == TipoLancFut.ExtratoAberto))
            {
                CorFundo1 = Color.FromArgb(243, 255, 243);
                CorFundo2 = Color.FromArgb(236, 255, 246);
                e.Appearance.ForeColor = Color.Blue;
            };
            switch (row.Data.DayOfWeek)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Friday:
                    e.Appearance.BackColor = CorFundo1;
                    break;
                case DayOfWeek.Sunday:
                case DayOfWeek.Saturday:
                    e.Appearance.BackColor = Color.FromArgb(255, 200, 200);
                    break;
                case DayOfWeek.Tuesday:
                case DayOfWeek.Thursday:
                    e.Appearance.BackColor = CorFundo2;
                    break;
                default:
                    e.Appearance.BackColor = Color.Silver;
                    break;
            }            
        }

        private void bEstatistica_Click(object sender, EventArgs e)
        {
            cGraficoArrecada cGra = new cGraficoArrecada();
            dLancamentosFuturos.DadosGrafico.Clear();
            cGra.dadosGraficoDataTableBindingSource.DataSource = dLancamentosFuturos.DadosGrafico;

            foreach (DictionaryEntry DE in Hgeral)
            {
                dLancamentosFuturos.DadosGraficoRow rowNova = dLancamentosFuturos.DadosGrafico.NewDadosGraficoRow();
                rowNova.Dias = (int)DE.Key;
                rowNova.Media = ((decimal)DE.Value);
                dLancamentosFuturos.DadosGrafico.AddDadosGraficoRow(rowNova);
            };
            int indice = -1;
            DataColumn NovaCol;
            foreach (DictionaryEntry DE in EstatisticasUsadas)
            {
                SortedList Hx = (SortedList)DE.Value;
                if (indice == -1)
                {
                    indice = 1;
                    NovaCol = dLancamentosFuturos.DadosGrafico.Columns["V1"];
                }
                else
                {
                    indice = cGra.chartControl1.Series.Add((DevExpress.XtraCharts.Series)cGra.chartControl1.Series[1].Clone());
                    NovaCol = dLancamentosFuturos.DadosGrafico.Columns["Vn" + indice.ToString()];
                    if (NovaCol == null)
                        NovaCol = dLancamentosFuturos.DadosGrafico.Columns.Add("Vn" + indice.ToString(), typeof(decimal));
                    cGra.chartControl1.Series[indice].ValueDataMembers.AddRange(NovaCol.ColumnName);
                }
                foreach (DictionaryEntry HDE in Hx)
                {
                    dLancamentosFuturos.DadosGraficoRow rowG = dLancamentosFuturos.DadosGrafico.FindByDias((int)HDE.Key);
                    rowG[NovaCol] = (decimal)HDE.Value;
                };
                cGra.chartControl1.Series[indice].LegendText = DE.Key.ToString();

            }
            cGra.dadosGraficoDataTableBindingSource.DataSource = dLancamentosFuturos.DadosGrafico;
            cGra.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }

        
        private void chRateios_EditValueChanged(object sender, EventArgs e)
        {
            if (chRateios.Checked)
            {
                checkedListBoxControl1.Visible = true;                
                dLancamentosFuturos.RATeiosTableAdapter.Fill(dLancamentosFuturos.RATeios, CON, (short)dateI.DateTime.Year);
            }
            else 
            {
                checkedListBoxControl1.Visible = false;
            }
        }

        private class ChRat : Object
        {
            private string Titulo;
            private int RAT;
            public ChRat(string _Titulo, int _RAT)
            {
                RAT = _RAT;
                Titulo = _Titulo;
            }
            public override string ToString()
            {
                return Titulo;
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    Alterar();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Delete:
                    if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.VerificaFuncionalidade("GER�NCIA") > 1)
                    {
                        if ((LinhaMae_F != null) && (!LinhaMae.IsCHENull()))
                        {
                            if (chTrava.Checked)
                                System.Windows.Forms.MessageBox.Show("ERRO: Travado");
                            else
                            {
                                Cheque Cheque = new dllCheques.Cheque(LinhaMae.CHE,dllCheques.Cheque.TipoChave.CHE);
                                CHEStatus StatusAnterior = (CHEStatus)Cheque.CHErow.CHEStatus;
                                Cheque.CHErow.CHEStatus = (int)CHEStatus.BaixaManual;
                                Cheque.GravaObs(string.Format("O cheque n�o foi conciliado e foi baixado manualmente: STatus {0} -> {1}\r\nJustificativa:\r\n{2}\r\n",
                                                             Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.Descritivo(StatusAnterior),
                                                             Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.Descritivo(CHEStatus.BaixaManual),
                                                             "Acerto em lote"));
                            }
                        }
                    }
                    break;
            }
        }
    }

    
}

