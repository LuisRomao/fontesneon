﻿namespace dllCaixa {


    partial class dLoteExtratos
    {
        private dllCaixa.dLoteExtratosTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public dllCaixa.dLoteExtratosTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter {
            get {
                if (contaCorrenTeTableAdapter == null) {
                    contaCorrenTeTableAdapter = new dllCaixa.dLoteExtratosTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }
    }
}
