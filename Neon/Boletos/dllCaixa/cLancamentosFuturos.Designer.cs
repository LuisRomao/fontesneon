namespace dllCaixa
{
    partial class cLancamentosFuturos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BotCarregar = new System.Windows.Forms.Button();
            this.dateI = new DevExpress.XtraEditors.DateEdit();
            this.dateF = new DevExpress.XtraEditors.DateEdit();
            this.dLancamentosFuturos = new dllCaixa.dLancamentosFuturos();
            this.dLancamentosFuturosBindingSource = new System.Windows.Forms.BindingSource();
            this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colData1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chCheques = new DevExpress.XtraEditors.CheckEdit();
            this.chDias = new DevExpress.XtraEditors.CheckEdit();
            this.chBoletos = new DevExpress.XtraEditors.CheckEdit();
            this.chChequesCa = new DevExpress.XtraEditors.CheckEdit();
            this.chEletronicos = new DevExpress.XtraEditors.CheckEdit();
            this.colVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bEstatistica = new System.Windows.Forms.Button();
            this.colDebito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chAcordos = new DevExpress.XtraEditors.CheckEdit();
            this.chRateios = new DevExpress.XtraEditors.CheckEdit();
            this.checkedListBoxControl1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.rATeiosBindingSource = new System.Windows.Forms.BindingSource();
            this.chPagFut = new DevExpress.XtraEditors.CheckEdit();
            this.chFR = new DevExpress.XtraEditors.CheckEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.chTrava = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditA = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditD = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditM = new DevExpress.XtraEditors.CalcEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateI.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateF.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dLancamentosFuturos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dLancamentosFuturosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCheques.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chBoletos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chChequesCa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chEletronicos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAcordos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chRateios.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rATeiosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chPagFut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chTrava.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditM.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.calcEditM);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.calcEditD);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.calcEditA);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.chTrava);
            this.panelControl1.Controls.Add(this.chFR);
            this.panelControl1.Controls.Add(this.chPagFut);
            this.panelControl1.Controls.Add(this.chRateios);
            this.panelControl1.Controls.Add(this.chAcordos);
            this.panelControl1.Controls.Add(this.bEstatistica);
            this.panelControl1.Controls.Add(this.chEletronicos);
            this.panelControl1.Controls.Add(this.chChequesCa);
            this.panelControl1.Controls.Add(this.chBoletos);
            this.panelControl1.Controls.Add(this.chDias);
            this.panelControl1.Controls.Add(this.chCheques);
            this.panelControl1.Controls.Add(this.dateF);
            this.panelControl1.Controls.Add(this.dateI);
            this.panelControl1.Controls.Add(this.BotCarregar);
            this.panelControl1.Size = new System.Drawing.Size(1247, 117);
            this.panelControl1.Controls.SetChildIndex(this.BotCarregar, 0);
            this.panelControl1.Controls.SetChildIndex(this.dateI, 0);
            this.panelControl1.Controls.SetChildIndex(this.dateF, 0);
            this.panelControl1.Controls.SetChildIndex(this.chCheques, 0);
            this.panelControl1.Controls.SetChildIndex(this.chDias, 0);
            this.panelControl1.Controls.SetChildIndex(this.chBoletos, 0);
            this.panelControl1.Controls.SetChildIndex(this.chChequesCa, 0);
            this.panelControl1.Controls.SetChildIndex(this.chEletronicos, 0);
            this.panelControl1.Controls.SetChildIndex(this.bEstatistica, 0);
            this.panelControl1.Controls.SetChildIndex(this.chAcordos, 0);
            this.panelControl1.Controls.SetChildIndex(this.chRateios, 0);
            this.panelControl1.Controls.SetChildIndex(this.chPagFut, 0);
            this.panelControl1.Controls.SetChildIndex(this.chFR, 0);
            this.panelControl1.Controls.SetChildIndex(this.chTrava, 0);
            this.panelControl1.Controls.SetChildIndex(this.labelControl1, 0);
            this.panelControl1.Controls.SetChildIndex(this.calcEditA, 0);
            this.panelControl1.Controls.SetChildIndex(this.labelControl2, 0);
            this.panelControl1.Controls.SetChildIndex(this.calcEditD, 0);
            this.panelControl1.Controls.SetChildIndex(this.labelControl3, 0);
            this.panelControl1.Controls.SetChildIndex(this.calcEditM, 0);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.dLancamentosFuturosBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Location = new System.Drawing.Point(0, 160);
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemButtonEdit1});
            this.GridControl_F.Size = new System.Drawing.Size(1247, 360);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView_F.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView_F.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colData,
            this.colVencimento,
            this.colData1,
            this.colTipo,
            this.colDescricao,
            this.colValor,
            this.colSaldo,
            this.colDebito,
            this.colCredito,
            this.gridColumn1});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsCustomization.AllowColumnMoving = false;
            this.GridView_F.OptionsCustomization.AllowFilter = false;
            this.GridView_F.OptionsCustomization.AllowGroup = false;
            this.GridView_F.OptionsCustomization.AllowSort = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.AllowCellMerge = true;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colData1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTipo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVencimento, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridView_F_RowCellStyle);
            this.GridView_F.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.GridView_F_RowStyle);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
          
            // 
            // BotCarregar
            // 
            this.BotCarregar.Enabled = false;
            this.BotCarregar.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotCarregar.Location = new System.Drawing.Point(435, 3);
            this.BotCarregar.Name = "BotCarregar";
            this.BotCarregar.Size = new System.Drawing.Size(112, 48);
            this.BotCarregar.TabIndex = 1;
            this.BotCarregar.Text = "Carregar";
            this.BotCarregar.UseVisualStyleBackColor = true;
            this.BotCarregar.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateI
            // 
            this.dateI.EditValue = null;
            this.dateI.Location = new System.Drawing.Point(319, 5);
            this.dateI.Name = "dateI";
            this.dateI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateI.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateI.Size = new System.Drawing.Size(110, 20);
            this.dateI.TabIndex = 3;
            this.dateI.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // dateF
            // 
            this.dateF.EditValue = null;
            this.dateF.Location = new System.Drawing.Point(319, 31);
            this.dateF.Name = "dateF";
            this.dateF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateF.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateF.Size = new System.Drawing.Size(110, 20);
            this.dateF.TabIndex = 4;
            this.dateF.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // dLancamentosFuturos
            // 
            this.dLancamentosFuturos.DataSetName = "dLancamentosFuturos";
            this.dLancamentosFuturos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dLancamentosFuturosBindingSource
            // 
            this.dLancamentosFuturosBindingSource.DataMember = "Lancamentos";
            this.dLancamentosFuturosBindingSource.DataSource = this.dLancamentosFuturos;
            // 
            // colData
            // 
            this.colData.DisplayFormat.FormatString = "ddd";
            this.colData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colData.FieldName = "Data";
            this.colData.Name = "colData";
            this.colData.OptionsColumn.FixedWidth = true;
            this.colData.OptionsColumn.ReadOnly = true;
            this.colData.OptionsColumn.ShowCaption = false;
            this.colData.Visible = true;
            this.colData.VisibleIndex = 0;
            // 
            // colData1
            // 
            this.colData1.DisplayFormat.FormatString = "dd/MM/yy";
            this.colData1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colData1.FieldName = "Data";
            this.colData1.Name = "colData1";
            this.colData1.OptionsColumn.FixedWidth = true;
            this.colData1.OptionsColumn.ReadOnly = true;
            this.colData1.Visible = true;
            this.colData1.VisibleIndex = 1;
            // 
            // colTipo
            // 
            this.colTipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colTipo.FieldName = "Tipo";
            this.colTipo.Name = "colTipo";
            this.colTipo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colTipo.OptionsColumn.FixedWidth = true;
            this.colTipo.OptionsColumn.ReadOnly = true;
            this.colTipo.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.colTipo.Visible = true;
            this.colTipo.VisibleIndex = 3;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colDescricao
            // 
            this.colDescricao.Caption = "Descri��o";
            this.colDescricao.FieldName = "Descricao";
            this.colDescricao.Name = "colDescricao";
            this.colDescricao.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colDescricao.OptionsColumn.ReadOnly = true;
            this.colDescricao.Visible = true;
            this.colDescricao.VisibleIndex = 4;
            this.colDescricao.Width = 391;
            // 
            // colValor
            // 
            this.colValor.DisplayFormat.FormatString = "n2";
            this.colValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValor.FieldName = "Valor";
            this.colValor.Name = "colValor";
            this.colValor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colValor.OptionsColumn.FixedWidth = true;
            this.colValor.OptionsColumn.ReadOnly = true;
            this.colValor.Visible = true;
            this.colValor.VisibleIndex = 5;
            // 
            // colSaldo
            // 
            this.colSaldo.DisplayFormat.FormatString = "n2";
            this.colSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSaldo.FieldName = "Saldo";
            this.colSaldo.Name = "colSaldo";
            this.colSaldo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colSaldo.OptionsColumn.FixedWidth = true;
            this.colSaldo.OptionsColumn.ReadOnly = true;
            this.colSaldo.Visible = true;
            this.colSaldo.VisibleIndex = 8;
            // 
            // chCheques
            // 
            this.chCheques.EditValue = true;
            this.chCheques.Location = new System.Drawing.Point(553, 1);
            this.chCheques.MenuManager = this.BarManager_F;
            this.chCheques.Name = "chCheques";
            this.chCheques.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chCheques.Properties.Appearance.Options.UseFont = true;
            this.chCheques.Properties.Caption = "Ch. Emitidos";
            this.chCheques.Size = new System.Drawing.Size(151, 19);
            this.chCheques.TabIndex = 5;
            this.chCheques.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // chDias
            // 
            this.chDias.EditValue = true;
            this.chDias.Location = new System.Drawing.Point(770, 35);
            this.chDias.MenuManager = this.BarManager_F;
            this.chDias.Name = "chDias";
            this.chDias.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chDias.Properties.Appearance.Options.UseFont = true;
            this.chDias.Properties.Caption = "Dias sem lan�amento";
            this.chDias.Size = new System.Drawing.Size(151, 19);
            this.chDias.TabIndex = 6;
            this.chDias.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // chBoletos
            // 
            this.chBoletos.EditValue = true;
            this.chBoletos.Location = new System.Drawing.Point(670, 1);
            this.chBoletos.MenuManager = this.BarManager_F;
            this.chBoletos.Name = "chBoletos";
            this.chBoletos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chBoletos.Properties.Appearance.Options.UseFont = true;
            this.chBoletos.Properties.Caption = "Arrecada��o";
            this.chBoletos.Size = new System.Drawing.Size(96, 19);
            this.chBoletos.TabIndex = 7;
            this.chBoletos.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // chChequesCa
            // 
            this.chChequesCa.EditValue = true;
            this.chChequesCa.Location = new System.Drawing.Point(553, 18);
            this.chChequesCa.MenuManager = this.BarManager_F;
            this.chChequesCa.Name = "chChequesCa";
            this.chChequesCa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chChequesCa.Properties.Appearance.Options.UseFont = true;
            this.chChequesCa.Properties.Caption = "Ch Cadastrados";
            this.chChequesCa.Size = new System.Drawing.Size(151, 19);
            this.chChequesCa.TabIndex = 8;
            this.chChequesCa.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // chEletronicos
            // 
            this.chEletronicos.EditValue = true;
            this.chEletronicos.Location = new System.Drawing.Point(553, 35);
            this.chEletronicos.MenuManager = this.BarManager_F;
            this.chEletronicos.Name = "chEletronicos";
            this.chEletronicos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chEletronicos.Properties.Appearance.Options.UseFont = true;
            this.chEletronicos.Properties.Caption = "Pag Eletr�nicos";
            this.chEletronicos.Size = new System.Drawing.Size(117, 19);
            this.chEletronicos.TabIndex = 9;
            this.chEletronicos.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // colVencimento
            // 
            this.colVencimento.Caption = "Vencto";
            this.colVencimento.DisplayFormat.FormatString = "dd/MM/yy";
            this.colVencimento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colVencimento.FieldName = "Vencimento";
            this.colVencimento.Name = "colVencimento";
            this.colVencimento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colVencimento.OptionsColumn.FixedWidth = true;
            this.colVencimento.OptionsColumn.ReadOnly = true;
            this.colVencimento.Visible = true;
            this.colVencimento.VisibleIndex = 2;
            // 
            // bEstatistica
            // 
            this.bEstatistica.Enabled = false;
            this.bEstatistica.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bEstatistica.Location = new System.Drawing.Point(927, 1);
            this.bEstatistica.Name = "bEstatistica";
            this.bEstatistica.Size = new System.Drawing.Size(68, 19);
            this.bEstatistica.TabIndex = 10;
            this.bEstatistica.Text = "Est.";
            this.bEstatistica.UseVisualStyleBackColor = true;
            this.bEstatistica.Click += new System.EventHandler(this.bEstatistica_Click);
            // 
            // colDebito
            // 
            this.colDebito.CustomizationCaption = "D�bito";
            this.colDebito.DisplayFormat.FormatString = "n2";
            this.colDebito.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDebito.FieldName = "Debito";
            this.colDebito.Name = "colDebito";
            this.colDebito.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colDebito.OptionsColumn.FixedWidth = true;
            this.colDebito.OptionsColumn.ReadOnly = true;
            this.colDebito.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Debito", "{0:n2}")});
            this.colDebito.Visible = true;
            this.colDebito.VisibleIndex = 6;
            // 
            // colCredito
            // 
            this.colCredito.CustomizationCaption = "Cr�dito";
            this.colCredito.DisplayFormat.FormatString = "n2";
            this.colCredito.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCredito.FieldName = "Credito";
            this.colCredito.Name = "colCredito";
            this.colCredito.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colCredito.OptionsColumn.FixedWidth = true;
            this.colCredito.OptionsColumn.ReadOnly = true;
            this.colCredito.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Credito", "{0:n2}")});
            this.colCredito.Visible = true;
            this.colCredito.VisibleIndex = 7;
            // 
            // chAcordos
            // 
            this.chAcordos.EditValue = true;
            this.chAcordos.Location = new System.Drawing.Point(670, 18);
            this.chAcordos.MenuManager = this.BarManager_F;
            this.chAcordos.Name = "chAcordos";
            this.chAcordos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chAcordos.Properties.Appearance.Options.UseFont = true;
            this.chAcordos.Properties.Caption = "Acordos";
            this.chAcordos.Size = new System.Drawing.Size(96, 19);
            this.chAcordos.TabIndex = 11;
            this.chAcordos.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // chRateios
            // 
            this.chRateios.Location = new System.Drawing.Point(670, 35);
            this.chRateios.MenuManager = this.BarManager_F;
            this.chRateios.Name = "chRateios";
            this.chRateios.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chRateios.Properties.Appearance.Options.UseFont = true;
            this.chRateios.Properties.Caption = "Rateios";
            this.chRateios.Size = new System.Drawing.Size(96, 19);
            this.chRateios.TabIndex = 12;
            this.chRateios.EditValueChanged += new System.EventHandler(this.chRateios_EditValueChanged);
            // 
            // checkedListBoxControl1
            // 
            this.checkedListBoxControl1.CheckOnClick = true;
            this.checkedListBoxControl1.DataSource = this.rATeiosBindingSource;
            this.checkedListBoxControl1.DisplayMember = "Titulo";
            this.checkedListBoxControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxControl1.Location = new System.Drawing.Point(0, 117);
            this.checkedListBoxControl1.MultiColumn = true;
            this.checkedListBoxControl1.Name = "checkedListBoxControl1";
            this.checkedListBoxControl1.Size = new System.Drawing.Size(1247, 43);
            this.checkedListBoxControl1.TabIndex = 7;
            this.checkedListBoxControl1.ValueMember = "RAT";
            this.checkedListBoxControl1.Visible = false;
            // 
            // rATeiosBindingSource
            // 
            this.rATeiosBindingSource.DataMember = "RATeios";
            this.rATeiosBindingSource.DataSource = this.dLancamentosFuturos;
            // 
            // chPagFut
            // 
            this.chPagFut.EditValue = true;
            this.chPagFut.Location = new System.Drawing.Point(770, 18);
            this.chPagFut.MenuManager = this.BarManager_F;
            this.chPagFut.Name = "chPagFut";
            this.chPagFut.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chPagFut.Properties.Appearance.Options.UseFont = true;
            this.chPagFut.Properties.Caption = "Pagamentos futuros";
            this.chPagFut.Size = new System.Drawing.Size(151, 19);
            this.chPagFut.TabIndex = 13;
            this.chPagFut.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // chFR
            // 
            this.chFR.EditValue = true;
            this.chFR.Location = new System.Drawing.Point(770, 1);
            this.chFR.MenuManager = this.BarManager_F;
            this.chFR.Name = "chFR";
            this.chFR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chFR.Properties.Appearance.Options.UseFont = true;
            this.chFR.Properties.Caption = "Fundo de Reserva";
            this.chFR.Size = new System.Drawing.Size(151, 19);
            this.chFR.TabIndex = 14;
            this.chFR.EditValueChanged += new System.EventHandler(this.Limpar);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 9;
            this.gridColumn1.Width = 40;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // chTrava
            // 
            this.chTrava.EditValue = true;
            this.chTrava.Location = new System.Drawing.Point(927, 35);
            this.chTrava.MenuManager = this.BarManager_F;
            this.chTrava.Name = "chTrava";
            this.chTrava.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chTrava.Properties.Appearance.Options.UseFont = true;
            this.chTrava.Properties.Caption = "Baixa manual travada";
            this.chTrava.Size = new System.Drawing.Size(151, 19);
            this.chTrava.TabIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(21, 59);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 16);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "Arrecada��o";
            // 
            // calcEditA
            // 
            this.calcEditA.Location = new System.Drawing.Point(5, 81);
            this.calcEditA.MenuManager = this.BarManager_F;
            this.calcEditA.Name = "calcEditA";
            this.calcEditA.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditA.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditA.Properties.Precision = 2;
            this.calcEditA.Properties.ReadOnly = true;
            this.calcEditA.Size = new System.Drawing.Size(116, 20);
            this.calcEditA.TabIndex = 17;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Location = new System.Drawing.Point(153, 59);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(96, 16);
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "Despesas fixas";
            // 
            // calcEditD
            // 
            this.calcEditD.Location = new System.Drawing.Point(143, 81);
            this.calcEditD.MenuManager = this.BarManager_F;
            this.calcEditD.Name = "calcEditD";
            this.calcEditD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditD.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditD.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditD.Properties.Precision = 2;
            this.calcEditD.Properties.ReadOnly = true;
            this.calcEditD.Size = new System.Drawing.Size(116, 20);
            this.calcEditD.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelControl3.Location = new System.Drawing.Point(305, 59);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(52, 16);
            this.labelControl3.TabIndex = 20;
            this.labelControl3.Text = "Margem";
            // 
            // calcEditM
            // 
            this.calcEditM.Location = new System.Drawing.Point(279, 81);
            this.calcEditM.MenuManager = this.BarManager_F;
            this.calcEditM.Name = "calcEditM";
            this.calcEditM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditM.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditM.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditM.Properties.Precision = 2;
            this.calcEditM.Properties.ReadOnly = true;
            this.calcEditM.Size = new System.Drawing.Size(116, 20);
            this.calcEditM.TabIndex = 21;
            // 
            // cLancamentosFuturos
            // 
            this.Controls.Add(this.checkedListBoxControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cLancamentosFuturos";
            this.Size = new System.Drawing.Size(1247, 580);
            this.CondominioAlterado += new System.EventHandler(this.cLancamentosFuturos_CondominioAlterado);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.checkedListBoxControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateI.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateF.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dLancamentosFuturos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dLancamentosFuturosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCheques.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chBoletos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chChequesCa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chEletronicos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAcordos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chRateios.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rATeiosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chPagFut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chTrava.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditM.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BotCarregar;
        private DevExpress.XtraEditors.DateEdit dateF;
        private DevExpress.XtraEditors.DateEdit dateI;
        private System.Windows.Forms.BindingSource dLancamentosFuturosBindingSource;
        private dLancamentosFuturos dLancamentosFuturos;
        private DevExpress.XtraGrid.Columns.GridColumn colData;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colData1;
        private DevExpress.XtraGrid.Columns.GridColumn colTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colValor;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldo;
        private DevExpress.XtraEditors.CheckEdit chBoletos;
        private DevExpress.XtraEditors.CheckEdit chDias;
        private DevExpress.XtraEditors.CheckEdit chCheques;
        private DevExpress.XtraEditors.CheckEdit chEletronicos;
        private DevExpress.XtraEditors.CheckEdit chChequesCa;
        private DevExpress.XtraGrid.Columns.GridColumn colVencimento;
        private System.Windows.Forms.Button bEstatistica;
        private DevExpress.XtraGrid.Columns.GridColumn colDebito;
        private DevExpress.XtraGrid.Columns.GridColumn colCredito;
        private DevExpress.XtraEditors.CheckEdit chRateios;
        private DevExpress.XtraEditors.CheckEdit chAcordos;
        private DevExpress.XtraEditors.CheckedListBoxControl checkedListBoxControl1;
        private System.Windows.Forms.BindingSource rATeiosBindingSource;
        private DevExpress.XtraEditors.CheckEdit chPagFut;
        private DevExpress.XtraEditors.CheckEdit chFR;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.CheckEdit chTrava;
        private DevExpress.XtraEditors.CalcEdit calcEditM;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CalcEdit calcEditD;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CalcEdit calcEditA;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}
