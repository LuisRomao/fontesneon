using System;
using System.Drawing;
using System.Windows.Forms;
using Framework;
using FrameworkProc.datasets;

namespace dllCaixa
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cSaldosGeral : CompontesBasicos.ComponenteGradeNavegadorPesquisa
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cSaldosGeral()
        {
            InitializeComponent();
            virEnumCCTTipo.virEnumCCTTipoSt.CarregaEditorDaGrid(colCCTTipo);
            uSUariosBindingSource.DataSource = dUSUarios.dUSUariosSt;
            if (dUSUarios.dUSUariosStGerentes.USUarios.FindByUSU(Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU) != null)
                lookupGerente1.USUSel = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
            BindingSource_F.DataSource = dSaldosGeral.dSaldosGeralSt;
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CONDOCULTOS") < 1)
                BindingSource_F.Filter = "CONOculto = false";
            GridView_F.OptionsBehavior.Editable = true;
            if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU == 30)
            {
                simpleButton2.Visible = simpleButton1.Visible = CHCorrigir.Visible = true;
            }
        }
              
        private dSaldosGeral.SaldosRow LinhaMae {
            get {
                return (LinhaMae_F == null)?null:(dSaldosGeral.SaldosRow)LinhaMae_F;
            }
        }

        private void DethaExtrato() {
            if (LinhaMae_F != null)
            {
                dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(LinhaMae.Data.AddMonths(-1), LinhaMae.Data, LinhaMae.CCT,false);
                Extrato.Titulo = "Extrato " + LinhaMae.CONNome;
                Extrato.somenteleitura = false;
                Extrato.LancamentosFuturos(true);
                Extrato.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                                   
            }
        }

        
        /// <summary>
        /// Esta fun��o normalmente n�o fica dispon�vel e � usada para corre��o de extratos em caso de "acidentes"
        /// </summary>
        private void Correcao()
        {            
            string comandoDATA =
"SELECT        MAX(dbo.BALancetes.BALdataFim) AS DataFEC, dbo.ContaCorrenTe.CCT\r\n" +
"FROM            dbo.BALancetes INNER JOIN\r\n" +
"                         dbo.ContaCorrenTe ON dbo.BALancetes.BAL_CON = dbo.ContaCorrenTe.CCT_CON\r\n" +
"WHERE        (NOT (dbo.BALancetes.BALSTATUS IN (0, 1)))\r\n" +
"GROUP BY dbo.ContaCorrenTe.CCT;"; 

            System.Data.DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comandoDATA);
            System.Data.DataView DV = new System.Data.DataView(DT) { Sort = "CCT" };

            bool ComCorrecao = CHCorrigir.Checked;
            int i = 1;
            int k = 0;
            int erros = 0;
            string listaErros = "";
            foreach (dSaldosGeral.SaldosRow row in dSaldosGeral.dSaldosGeralSt.Saldos)
            {
                Console.WriteLine("{0}/{1} {2} CCT = {3}", i++, dSaldosGeral.dSaldosGeralSt.Saldos.Count,row.CCTConta,row.CCT);
                /*
                if (row.CCT != 334)
                {
                    Console.WriteLine("PULADO       - {0}/{1} {2}", i++, dSaldosGeral.dSaldosGeralSt.Saldos.Count, row.CCTConta);
                    continue;
                }
                else
                    Console.WriteLine("ACHEI *****************************************     - {0}/{1} {2}", i++, dSaldosGeral.dSaldosGeralSt.Saldos.Count, row.CCTConta);
                */
                if (row.CCT_BCO == 237)
                {
                    k++;
                    Console.WriteLine("{0}/{1} *********************** {2}", k, dSaldosGeral.dSaldosGeralSt.Saldos.Count, row.CCT);
                    //dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(row.CCT, DataCorrecao);                    
                    int indice = DV.Find(row.CCT);
                    if (indice < 0)
                        continue;
                    System.Data.DataRowView DRV = DV[indice];
                    DateTime DataITesteOriginal = (DateTime)DRV["DataFEC"];
                    //DateTime DataITesteOriginal = new DateTime(2016, 04, 4);
                    if (DataITesteOriginal < new DateTime(2016, 06, 1))
                        continue;
                    DateTime? DataITeste = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_DateTime("select max(SCCDATA) as Data from SaldoContaCorrente where (SCC_CCT = @P1) and (SCCDATA < @P2)", row.CCT, DataITesteOriginal);
                    dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(DataITeste.GetValueOrDefault(DataITesteOriginal), DateTime.Today, row.CCT, true);
                    if (Extrato.UltimoErro != null)
                    {
                        Console.WriteLine("ERRO");
                        erros++;
                        listaErros += string.Format("{0}\r\n",row.CCT);
                        if (ComCorrecao)                                                    
                            Extrato.ValidaTodos(true);                        
                        //if (erros > 10)
                        //    break;
                    }
                    Extrato.Dispose();
                }
                else
                    Console.WriteLine("PULADO");                
            }
            if (listaErros != "")
                Clipboard.SetText(listaErros);
            RefreshDados();
            MessageBox.Show(string.Format("Terminado:{0}", erros));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Visualizar()
        {
            DethaExtrato();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Alterar()
        {
            DethaExtrato();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dSaldosGeral.dSaldosGeralSt.RefreshDados();
            //dSaldosGeral.dSaldosGeralSt.SaldosTableAdapter.FillCompleto(dSaldosGeral.dSaldosGeralSt.Saldos);
            //BuscaSaldo();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string ComErros = "";
            foreach(dSaldosGeral.SaldosRow Linha in dSaldosGeral.dSaldosGeralSt.Saldos)
            {
                
                dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(DateTime.Today.AddMonths(-6), DateTime.Today.AddMonths(2), LinhaMae.CCT,true);
                if (!Extrato.ValidaTodos())
                    //if (Linha.IsCONCodigoNull() || Linha.IsCONNomeNull())
                    //    ComErros += Linha.CCTAgencia + " " + Linha.CCTConta + "\r\n";
                    //else
                        ComErros += Linha.CONCodigo + " " + Linha.CONNome + "\r\n";

            };
            if (ComErros != "")
                MessageBox.Show(ComErros);
            else
                MessageBox.Show("Sem erros");
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string Titulo = (LinhaMae.IsCCTTituloNull() ? "" : LinhaMae.CCTTitulo);
            if (VirInput.Input.Execute("Titulo", ref Titulo, false))
            {
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("UPDATE ContaCorrenTe SET CCTTitulo = @P1 WHERE (CCT = @P2)", Titulo, LinhaMae.CCT);
                LinhaMae.CCTTitulo = Titulo;
            }
            
        }

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dSaldosGeral.SaldosRow row = (dSaldosGeral.SaldosRow)GridView_F.GetDataRow(e.RowHandle);
            if (row != null)
            {
                if (e.Column == colSaldo)
                {                   
                    if (row.Saldo < 0)
                    {
                        e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                    }
                };
                if (e.Column == colSaldoFuturo)
                {                    
                    if ((!row.IsSaldoFuturoNull()) && (row.SaldoFuturo < 0))
                    {
                        e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                    }
                };                
                if (e.Column == colCCTTipo)
                {                                        
                        if (!row.IsCCTTipoNull())
                        {
                            e.Appearance.BackColor = virEnumCCTTipo.virEnumCCTTipoSt.GetCor(row.CCTTipo);
                            e.Appearance.BackColor2 = Color.White;
                            e.Appearance.ForeColor = Color.Black;
                        }                    
                }                
            }
        }

        private void lookupGerente1_alterado(object sender, EventArgs e)
        {
            if (lookupGerente1.USUSel == -1)
                GridView_F.ActiveFilterString = "";
            else
                GridView_F.ActiveFilterString = string.Format("[CONAuditor1_USU]= {0}", lookupGerente1.USUSel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            if (args.Length == 1)
            {                
                lookupGerente1.USUSel = (int)args[0];                
                //if (lookupGerente1.USUgenSel == -1)
                  //  GridView_F.ActiveFilterString = "";
                //else
                  //  GridView_F.ActiveFilterString = string.Format("[CONAuditor1_USU]= {0}", lookupGerente1.USUgenSel);
            }
            return null;
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DethaExtrato();
            //Correcao();
        }

        

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            int CCT = 0;
            if (VirInput.Input.Execute("CCT", out CCT))
            {
                dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(new DateTime(2015, 8, 1), new DateTime(2015, 9, 1), CCT, false);
                
                Extrato.somenteleitura = false;
                Extrato.LancamentosFuturos(false);
                Extrato.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            dSaldosGeral.dSaldosGeralSt.RefreshDados();
        }

        [Obsolete("remover ap�s a corre��o")]
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Correcao();           
        }

        /*
        
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
        }
        */
        
    }
}

