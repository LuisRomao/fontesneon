﻿namespace dllCaixa {


    partial class dLancamentosFuturos        
    {
        private dLancamentosFuturosTableAdapters.PaGamentosFixosTableAdapter paGamentosFixosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PaGamentosFixos
        /// </summary>
        public dLancamentosFuturosTableAdapters.PaGamentosFixosTableAdapter PaGamentosFixosTableAdapter
        {
            get
            {
                if (paGamentosFixosTableAdapter == null)
                {
                    paGamentosFixosTableAdapter = new dLancamentosFuturosTableAdapters.PaGamentosFixosTableAdapter();
                    paGamentosFixosTableAdapter.TrocarStringDeConexao();
                };
                return paGamentosFixosTableAdapter;
            }
        }

        private dLancamentosFuturosTableAdapters.CHEquesTableAdapter cHEquesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CHEques
        /// </summary>
        public dLancamentosFuturosTableAdapters.CHEquesTableAdapter CHEquesTableAdapter
        {
            get
            {
                if (cHEquesTableAdapter == null)
                {
                    cHEquesTableAdapter = new dLancamentosFuturosTableAdapters.CHEquesTableAdapter();
                    cHEquesTableAdapter.TrocarStringDeConexao();
                };
                return cHEquesTableAdapter;
            }
        }


        private dLancamentosFuturosTableAdapters.ExtratoTableAdapter extratoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Extrato
        /// </summary>
        public dLancamentosFuturosTableAdapters.ExtratoTableAdapter ExtratoTableAdapter
        {
            get
            {
                if (extratoTableAdapter == null)
                {
                    extratoTableAdapter = new dLancamentosFuturosTableAdapters.ExtratoTableAdapter();
                    extratoTableAdapter.TrocarStringDeConexao();
                };
                return extratoTableAdapter;
            }
        }

        private dLancamentosFuturosTableAdapters.EstatisticaTableAdapter estatisticaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Estatistica
        /// </summary>
        public dLancamentosFuturosTableAdapters.EstatisticaTableAdapter EstatisticaTableAdapter
        {
            get
            {
                if (estatisticaTableAdapter == null)
                {
                    estatisticaTableAdapter = new dLancamentosFuturosTableAdapters.EstatisticaTableAdapter();
                    estatisticaTableAdapter.TrocarStringDeConexao();
                };
                return estatisticaTableAdapter;
            }
        }

        private dLancamentosFuturosTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLetos
        /// </summary>
        public dLancamentosFuturosTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (bOLetosTableAdapter == null)
                {
                    bOLetosTableAdapter = new dLancamentosFuturosTableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao();
                };
                return bOLetosTableAdapter;
            }
        }

        private dLancamentosFuturosTableAdapters.RATeiosTableAdapter rATeiosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RATeios
        /// </summary>
        public dLancamentosFuturosTableAdapters.RATeiosTableAdapter RATeiosTableAdapter
        {
            get
            {
                if (rATeiosTableAdapter == null)
                {
                    rATeiosTableAdapter = new dLancamentosFuturosTableAdapters.RATeiosTableAdapter();
                    rATeiosTableAdapter.TrocarStringDeConexao();
                };
                return rATeiosTableAdapter;
            }
        }

        private dLancamentosFuturosTableAdapters.RAteioDetalhesTableAdapter rAteioDetalhesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RAteioDetalhes
        /// </summary>
        public dLancamentosFuturosTableAdapters.RAteioDetalhesTableAdapter RAteioDetalhesTableAdapter
        {
            get
            {
                if (rAteioDetalhesTableAdapter == null)
                {
                    rAteioDetalhesTableAdapter = new dLancamentosFuturosTableAdapters.RAteioDetalhesTableAdapter();
                    rAteioDetalhesTableAdapter.TrocarStringDeConexao();
                };
                return rAteioDetalhesTableAdapter;
            }
        }
    }
}
