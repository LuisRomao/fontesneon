namespace dllCaixa
{
    partial class cSaldosGeral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cSaldosGeral));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCTLTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCT_BCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAgenciaDg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTContaDg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.colCCTAplicacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.colCONAuditor1_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colCONValorCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONDiaVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CHCorrigir = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.lookupGerente1 = new Framework.Lookup.LookupGerente();
            this.colCCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colSCCValorF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSCCValorApF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCT_CCTPlus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoFuturo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colDataBAL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoBAL = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CHCorrigir.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Location = new System.Drawing.Point(0, 35);
            this.groupControl1.Size = new System.Drawing.Size(1529, 84);
            // 
            // LayoutPesquisa_F
            // 
            this.LayoutPesquisa_F.Size = new System.Drawing.Size(1525, 84);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3)});
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.BarManager_F.MaxItemId = 26;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.Enabled = false;
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            this.BtnIncluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            this.BtnAlterar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            this.BtnExcluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            gridLevelNode1.LevelTemplate = this.gridView1;
            gridLevelNode1.RelationName = "Saldos_DataTable1";
            this.GridControl_F.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.GridControl_F.Location = new System.Drawing.Point(0, 119);
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemButtonEdit1,
            this.repositoryItemCalcEdit1,
            this.repositoryItemImageComboBox1});
            this.GridControl_F.ShowOnlyPredefinedDetails = true;
            this.GridControl_F.Size = new System.Drawing.Size(1529, 311);
            this.GridControl_F.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.GridView_F.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.GridView_F.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.GridView_F.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.GridView_F.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colData,
            this.colCONCodigo,
            this.colCONNome,
            this.colCCT_BCO,
            this.colCCTAgencia,
            this.colCCTAgenciaDg,
            this.colCCTConta,
            this.colCCTContaDg,
            this.colSaldo,
            this.colCCTAplicacao,
            this.colCCTTitulo,
            this.colCONAuditor1_USU,
            this.colCONValorCondominio,
            this.colCONDiaVencimento,
            this.colCCT,
            this.gridColumn1,
            this.colSCCValorF,
            this.colSCCValorApF,
            this.colCCTTipo,
            this.colCCT_CCTPlus,
            this.colSaldoFuturo,
            this.colDataBAL,
            this.colSaldoBAL});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsDetail.AllowZoomDetail = false;
            this.GridView_F.OptionsDetail.ShowDetailTabs = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSaldo, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridView_F_RowCellStyle);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "Saldos";
            this.BindingSource_F.DataSource = typeof(dllCaixa.dSaldosGeral);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCTLTitulo});
            this.gridView1.GridControl = this.GridControl_F;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.AllowZoomDetail = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.ShowColumnHeaders = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colCTLTitulo
            // 
            this.colCTLTitulo.FieldName = "CTLTitulo";
            this.colCTLTitulo.Name = "colCTLTitulo";
            this.colCTLTitulo.OptionsColumn.ReadOnly = true;
            this.colCTLTitulo.OptionsColumn.ShowCaption = false;
            this.colCTLTitulo.Visible = true;
            this.colCTLTitulo.VisibleIndex = 0;
            // 
            // colData
            // 
            this.colData.Caption = "Data";
            this.colData.FieldName = "Data";
            this.colData.Name = "colData";
            this.colData.OptionsColumn.AllowEdit = false;
            this.colData.OptionsColumn.ReadOnly = true;
            this.colData.Visible = true;
            this.colData.VisibleIndex = 8;
            this.colData.Width = 79;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 1;
            this.colCONCodigo.Width = 73;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Nome";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.AllowEdit = false;
            this.colCONNome.OptionsColumn.ReadOnly = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 2;
            this.colCONNome.Width = 192;
            // 
            // colCCT_BCO
            // 
            this.colCCT_BCO.Caption = "Banco";
            this.colCCT_BCO.FieldName = "CCT_BCO";
            this.colCCT_BCO.Name = "colCCT_BCO";
            this.colCCT_BCO.OptionsColumn.AllowEdit = false;
            this.colCCT_BCO.OptionsColumn.ReadOnly = true;
            this.colCCT_BCO.Visible = true;
            this.colCCT_BCO.VisibleIndex = 3;
            this.colCCT_BCO.Width = 48;
            // 
            // colCCTAgencia
            // 
            this.colCCTAgencia.Caption = "Ag�ncia";
            this.colCCTAgencia.FieldName = "CCTAgencia";
            this.colCCTAgencia.Name = "colCCTAgencia";
            this.colCCTAgencia.OptionsColumn.AllowEdit = false;
            this.colCCTAgencia.OptionsColumn.ReadOnly = true;
            this.colCCTAgencia.Visible = true;
            this.colCCTAgencia.VisibleIndex = 4;
            this.colCCTAgencia.Width = 56;
            // 
            // colCCTAgenciaDg
            // 
            this.colCCTAgenciaDg.Caption = "CCTAgenciaDg";
            this.colCCTAgenciaDg.FieldName = "CCTAgenciaDg";
            this.colCCTAgenciaDg.Name = "colCCTAgenciaDg";
            this.colCCTAgenciaDg.OptionsColumn.AllowEdit = false;
            this.colCCTAgenciaDg.OptionsColumn.ReadOnly = true;
            this.colCCTAgenciaDg.OptionsColumn.ShowCaption = false;
            this.colCCTAgenciaDg.OptionsFilter.AllowAutoFilter = false;
            this.colCCTAgenciaDg.OptionsFilter.AllowFilter = false;
            this.colCCTAgenciaDg.Visible = true;
            this.colCCTAgenciaDg.VisibleIndex = 5;
            this.colCCTAgenciaDg.Width = 20;
            // 
            // colCCTConta
            // 
            this.colCCTConta.Caption = "Conta";
            this.colCCTConta.FieldName = "CCTConta";
            this.colCCTConta.Name = "colCCTConta";
            this.colCCTConta.OptionsColumn.ReadOnly = true;
            this.colCCTConta.Visible = true;
            this.colCCTConta.VisibleIndex = 6;
            this.colCCTConta.Width = 74;
            // 
            // colCCTContaDg
            // 
            this.colCCTContaDg.Caption = "CCTContaDg";
            this.colCCTContaDg.FieldName = "CCTContaDg";
            this.colCCTContaDg.Name = "colCCTContaDg";
            this.colCCTContaDg.OptionsColumn.AllowEdit = false;
            this.colCCTContaDg.OptionsColumn.ReadOnly = true;
            this.colCCTContaDg.OptionsColumn.ShowCaption = false;
            this.colCCTContaDg.OptionsFilter.AllowAutoFilter = false;
            this.colCCTContaDg.OptionsFilter.AllowFilter = false;
            this.colCCTContaDg.Visible = true;
            this.colCCTContaDg.VisibleIndex = 7;
            this.colCCTContaDg.Width = 20;
            // 
            // colSaldo
            // 
            this.colSaldo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colSaldo.AppearanceCell.Options.UseFont = true;
            this.colSaldo.Caption = "Saldo";
            this.colSaldo.DisplayFormat.FormatString = "n2";
            this.colSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSaldo.FieldName = "Saldo";
            this.colSaldo.Name = "colSaldo";
            this.colSaldo.OptionsColumn.ReadOnly = true;
            this.colSaldo.Visible = true;
            this.colSaldo.VisibleIndex = 9;
            this.colSaldo.Width = 81;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Todas as contas";
            this.barButtonItem1.Id = 23;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Validar Todas";
            this.barButtonItem2.Id = 24;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // colCCTAplicacao
            // 
            this.colCCTAplicacao.Caption = "Aplica��o";
            this.colCCTAplicacao.FieldName = "CCTAplicacao";
            this.colCCTAplicacao.Name = "colCCTAplicacao";
            this.colCCTAplicacao.OptionsColumn.AllowEdit = false;
            this.colCCTAplicacao.OptionsColumn.ReadOnly = true;
            this.colCCTAplicacao.Visible = true;
            this.colCCTAplicacao.VisibleIndex = 11;
            this.colCCTAplicacao.Width = 66;
            // 
            // colCCTTitulo
            // 
            this.colCCTTitulo.Caption = "T�tulo";
            this.colCCTTitulo.FieldName = "CCTTitulo";
            this.colCCTTitulo.Name = "colCCTTitulo";
            this.colCCTTitulo.OptionsColumn.AllowEdit = false;
            this.colCCTTitulo.OptionsColumn.ReadOnly = true;
            this.colCCTTitulo.Visible = true;
            this.colCCTTitulo.VisibleIndex = 12;
            this.colCCTTitulo.Width = 154;
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Alterar T�tulo";
            this.barButtonItem3.Id = 25;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // colCONAuditor1_USU
            // 
            this.colCONAuditor1_USU.Caption = "Gerente";
            this.colCONAuditor1_USU.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colCONAuditor1_USU.FieldName = "CONAuditor1_USU";
            this.colCONAuditor1_USU.Name = "colCONAuditor1_USU";
            this.colCONAuditor1_USU.OptionsColumn.AllowEdit = false;
            this.colCONAuditor1_USU.OptionsColumn.ReadOnly = true;
            this.colCONAuditor1_USU.Visible = true;
            this.colCONAuditor1_USU.VisibleIndex = 0;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpEdit1.DataSource = this.uSUariosBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "USUNome";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "N�O CADASTRADO";
            this.repositoryItemLookUpEdit1.ValueMember = "USU";
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colCONValorCondominio
            // 
            this.colCONValorCondominio.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colCONValorCondominio.AppearanceCell.Options.UseBackColor = true;
            this.colCONValorCondominio.Caption = "Valor Cond.";
            this.colCONValorCondominio.DisplayFormat.FormatString = "n2";
            this.colCONValorCondominio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCONValorCondominio.FieldName = "CONValorCondominio";
            this.colCONValorCondominio.Name = "colCONValorCondominio";
            this.colCONValorCondominio.OptionsColumn.AllowEdit = false;
            this.colCONValorCondominio.OptionsColumn.ReadOnly = true;
            this.colCONValorCondominio.Visible = true;
            this.colCONValorCondominio.VisibleIndex = 14;
            this.colCONValorCondominio.Width = 79;
            // 
            // colCONDiaVencimento
            // 
            this.colCONDiaVencimento.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colCONDiaVencimento.AppearanceCell.Options.UseBackColor = true;
            this.colCONDiaVencimento.Caption = "Dia";
            this.colCONDiaVencimento.FieldName = "CONDiaVencimento";
            this.colCONDiaVencimento.Name = "colCONDiaVencimento";
            this.colCONDiaVencimento.OptionsColumn.AllowEdit = false;
            this.colCONDiaVencimento.OptionsColumn.ReadOnly = true;
            this.colCONDiaVencimento.Visible = true;
            this.colCONDiaVencimento.VisibleIndex = 13;
            this.colCONDiaVencimento.Width = 36;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.CHCorrigir);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.lookupGerente1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1529, 35);
            this.panelControl1.TabIndex = 6;
            // 
            // CHCorrigir
            // 
            this.CHCorrigir.Location = new System.Drawing.Point(903, 5);
            this.CHCorrigir.MenuManager = this.BarManager_F;
            this.CHCorrigir.Name = "CHCorrigir";
            this.CHCorrigir.Properties.Caption = "Corrigir";
            this.CHCorrigir.Size = new System.Drawing.Size(75, 19);
            this.CHCorrigir.TabIndex = 6;
            this.CHCorrigir.Visible = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(702, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(171, 23);
            this.simpleButton1.TabIndex = 5;
            this.simpleButton1.Text = "Recalcular";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(303, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(152, 26);
            this.simpleButton3.TabIndex = 4;
            this.simpleButton3.Text = "Calcular";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(525, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(171, 23);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "abrir CCT";
            this.simpleButton2.Visible = false;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // lookupGerente1
            // 
            this.lookupGerente1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente1.Appearance.Options.UseBackColor = true;
            this.lookupGerente1.CaixaAltaGeral = true;
            this.lookupGerente1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente1.Location = new System.Drawing.Point(6, 6);
            this.lookupGerente1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupGerente1.Name = "lookupGerente1";
            this.lookupGerente1.Size = new System.Drawing.Size(290, 20);
            this.lookupGerente1.somenteleitura = false;
            this.lookupGerente1.TabIndex = 0;
            this.lookupGerente1.Titulo = null;
            this.lookupGerente1.USUSel = -1;
            this.lookupGerente1.alterado += new System.EventHandler(this.lookupGerente1_alterado);
            // 
            // colCCT
            // 
            this.colCCT.FieldName = "CCT";
            this.colCCT.Name = "colCCT";
            this.colCCT.OptionsColumn.AllowEdit = false;
            this.colCCT.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 18;
            this.gridColumn1.Width = 30;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colSCCValorF
            // 
            this.colSCCValorF.Caption = "Saldo CC";
            this.colSCCValorF.DisplayFormat.FormatString = "n2";
            this.colSCCValorF.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSCCValorF.FieldName = "SCCValorF";
            this.colSCCValorF.Name = "colSCCValorF";
            // 
            // colSCCValorApF
            // 
            this.colSCCValorApF.Caption = "Salso Aplica��o";
            this.colSCCValorApF.DisplayFormat.FormatString = "n2";
            this.colSCCValorApF.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSCCValorApF.FieldName = "SCCValorApF";
            this.colSCCValorApF.Name = "colSCCValorApF";
            // 
            // colCCTTipo
            // 
            this.colCCTTipo.Caption = "Tipo Conta";
            this.colCCTTipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCCTTipo.FieldName = "CCTTipo";
            this.colCCTTipo.Name = "colCCTTipo";
            this.colCCTTipo.Visible = true;
            this.colCCTTipo.VisibleIndex = 10;
            this.colCCTTipo.Width = 111;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colCCT_CCTPlus
            // 
            this.colCCT_CCTPlus.Caption = "CCT PLUS";
            this.colCCT_CCTPlus.FieldName = "CCT_CCTPlus";
            this.colCCT_CCTPlus.Name = "colCCT_CCTPlus";
            // 
            // colSaldoFuturo
            // 
            this.colSaldoFuturo.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSaldoFuturo.AppearanceCell.Options.UseBackColor = true;
            this.colSaldoFuturo.Caption = "Saldo Futuro";
            this.colSaldoFuturo.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colSaldoFuturo.DisplayFormat.FormatString = "n2";
            this.colSaldoFuturo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSaldoFuturo.FieldName = "SaldoFuturo";
            this.colSaldoFuturo.Name = "colSaldoFuturo";
            this.colSaldoFuturo.Visible = true;
            this.colSaldoFuturo.VisibleIndex = 15;
            this.colSaldoFuturo.Width = 107;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colDataBAL
            // 
            this.colDataBAL.Caption = "Data BAL";
            this.colDataBAL.FieldName = "DataBAL";
            this.colDataBAL.Name = "colDataBAL";
            this.colDataBAL.OptionsColumn.ReadOnly = true;
            this.colDataBAL.Visible = true;
            this.colDataBAL.VisibleIndex = 16;
            // 
            // colSaldoBAL
            // 
            this.colSaldoBAL.Caption = "Saldo BAL";
            this.colSaldoBAL.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colSaldoBAL.FieldName = "SaldoBAL";
            this.colSaldoBAL.Name = "colSaldoBAL";
            this.colSaldoBAL.OptionsColumn.ReadOnly = true;
            this.colSaldoBAL.Visible = true;
            this.colSaldoBAL.VisibleIndex = 17;
            // 
            // cSaldosGeral
            // 
            this.BotaoAlterar = false;
            this.BotaoExcluir = false;
            this.BotaoIncluir = false;
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cSaldosGeral";
            this.Size = new System.Drawing.Size(1529, 490);
            this.somenteleitura = true;
            this.Titulo = "SALDOS";
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CHCorrigir.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colData;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT_BCO;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAgenciaDg;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTContaDg;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldo;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAplicacao;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTTitulo;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAuditor1_USU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource uSUariosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCONValorCondominio;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDiaVencimento;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private Framework.Lookup.LookupGerente lookupGerente1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colSCCValorF;
        private DevExpress.XtraGrid.Columns.GridColumn colSCCValorApF;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT_CCTPlus;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoFuturo;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit CHCorrigir;
        private DevExpress.XtraGrid.Columns.GridColumn colDataBAL;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoBAL;
    }
}
