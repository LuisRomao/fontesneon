﻿using System;
using VirEnumeracoesNeon;

namespace dllCaixa
{


    partial class dSaldosGeral
    {
        private static dSaldosGeral _dSaldosGeralSt;

        /// <summary>
        /// dataset estático:dSaldosGeral
        /// </summary>
        public static dSaldosGeral dSaldosGeralSt
        {
            get
            {
                if (_dSaldosGeralSt == null)
                {
                    _dSaldosGeralSt = new dSaldosGeral();
                    _dSaldosGeralSt.RefreshDados();
                }
                return _dSaldosGeralSt;
            }
        }

        private dSaldosGeralTableAdapters.SaldoBalanceteTableAdapter saldoBalanceteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoBalancete
        /// </summary>
        public dSaldosGeralTableAdapters.SaldoBalanceteTableAdapter SaldoBalanceteTableAdapter
        {
            get
            {
                if (saldoBalanceteTableAdapter == null)
                {
                    saldoBalanceteTableAdapter = new dSaldosGeralTableAdapters.SaldoBalanceteTableAdapter();
                    saldoBalanceteTableAdapter.TrocarStringDeConexao();
                };
                return saldoBalanceteTableAdapter;
            }
        }

        private dSaldosGeralTableAdapters.ContasLTableAdapter contasLTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContasL
        /// </summary>
        public dSaldosGeralTableAdapters.ContasLTableAdapter ContasLTableAdapter
        {
            get
            {
                if (contasLTableAdapter == null)
                {
                    contasLTableAdapter = new dSaldosGeralTableAdapters.ContasLTableAdapter();
                    contasLTableAdapter.TrocarStringDeConexao();
                };
                return contasLTableAdapter;
            }
        }

        private dSaldosGeralTableAdapters.FuturosTableAdapter futurosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Futuros
        /// </summary>
        public dSaldosGeralTableAdapters.FuturosTableAdapter FuturosTableAdapter
        {
            get
            {
                if (futurosTableAdapter == null)
                {
                    futurosTableAdapter = new dSaldosGeralTableAdapters.FuturosTableAdapter();
                    futurosTableAdapter.TrocarStringDeConexao();
                };
                return futurosTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RefreshDados()
        {
            ContasL.Clear();
            Futuros.Clear();
            SaldosTableAdapter.Fill(Saldos, Framework.DSCentral.EMP);
            FuturosTableAdapter.Fill(Futuros);
            SaldoBalanceteTableAdapter.Fill(SaldoBalancete);
            AjustaSaldo();
            ContasLTableAdapter.Fill(ContasL);
        }


        private void AjustaSaldo()
        {
            foreach (SaldosRow RowS in Saldos)
            {
                if ((RowS.CCTTipo == (int)CCTTipo.ContaCorrete_com_Oculta) && (RowS.IsCCT_CCTPlusNull()))
                {
                    RowS.Saldo = RowS.SCCValorF + RowS.SCCValorApF;
                    if (RowS.SaldoBalanceteRow != null)
                    {
                        RowS.DataBAL = RowS.SaldoBalanceteRow.SCCData;
                        RowS.SaldoBAL = RowS.SaldoBalanceteRow.SCCValorF + RowS.SaldoBalanceteRow.SCCValorApF;
                    }
                }
                else
                {
                    RowS.Saldo = RowS.SCCValorF;
                    if (RowS.SaldoBalanceteRow != null)
                    {
                        RowS.DataBAL = RowS.SaldoBalanceteRow.SCCData;
                        RowS.SaldoBAL = RowS.SaldoBalanceteRow.SCCValorF;
                    }
                }
                RowS.SaldoFuturo = RowS.Saldo;
                DateTime DiaFuturo = DateTime.Today;
                int dia = RowS.CONDiaVencimento;
                bool ok = false;
                while (!ok)
                {
                    try
                    {
                        DiaFuturo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, dia);
                        ok = true;
                    }
                    catch
                    {
                        dia--;
                    }
                }

                if (DiaFuturo <= DateTime.Today)
                    DiaFuturo = DiaFuturo.AddMonths(1);
                DiaFuturo = Framework.datasets.dFERiados.DiasUteis(DiaFuturo, 1, Framework.datasets.dFERiados.Sentido.avanca, Framework.datasets.dFERiados.Sabados.naoUteis, Framework.datasets.dFERiados.Feriados.naoUteis);
                foreach (FuturosRow rowFut in RowS.GetFuturosRows())
                    if (rowFut.CHEVencimento < DiaFuturo)
                        RowS.SaldoFuturo -= rowFut.CHEValor;
            }
        }

        private dSaldosGeralTableAdapters.SaldosTableAdapter saldosTableAdapter;

        /// <summary>
        /// TableAdapter
        /// </summary>
        public dSaldosGeralTableAdapters.SaldosTableAdapter SaldosTableAdapter
        {
            get
            {
                if (saldosTableAdapter == null)
                {
                    saldosTableAdapter = new dllCaixa.dSaldosGeralTableAdapters.SaldosTableAdapter();
                    saldosTableAdapter.TrocarStringDeConexao();
                };
                return saldosTableAdapter;
            }

        }
    }
}


