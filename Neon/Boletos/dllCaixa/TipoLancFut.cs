using System;
using System.Collections.Generic;
using System.Text;

namespace dllCaixa
{
    enum TipoLancFut
    {
        Extrato = 0,
        ExtratoAberto = 1,
        Boleto = 2,
        FundoReserva = 3,
        Rateio = 4,
        Eletronico = 5,
        DebitoAutomatico = 6,
        ChequeEmitido = 7,
        ChequeCadastrado = 8,                                        
        Acordo = 9,        
        Futuros = 10,
        Vago = 11
    }

    
}
