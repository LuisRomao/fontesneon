using System;
using DevExpress.XtraReports.UI;
using CompontesBasicosProc;
using dllImpresso.Botoes;

namespace dllCaixa
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cLoteExtratos : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cLoteExtratos()
        {
            InitializeComponent();
            TableAdapterPrincipal = dLoteExtratos.ContaCorrenTeTableAdapter;
            dLoteExtratos.ContaCorrenTeTableAdapter.FillcomBloqueio(dLoteExtratos.ContaCorrenTe,Framework.DSCentral.EMP);
            //dLoteExtratos.ContaCorrenTeTableAdapter.Fill(dLoteExtratos.ContaCorrenTe);
            cONDOMINIOSBindingSource.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST;
        }

        //private dllImpresso.ExtratoBancario.ImpExtratoBal Impresso;

        private dllImpresso.ExtratoBancario.ImpExtratoBal ImpressoCarregado()            
            {
                dllImpresso.ExtratoBancario.ImpExtratoBal retorno;  
                
                    string TituloImp = "";
                    FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(RowCCTMae.CCT_CON);
                    if (rowCON == null)
                    {
                        FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CarregaCondominios();
                        rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(RowCCTMae.CCT_CON);
                    };
                    if (rowCON != null)
                        TituloImp = rowCON.CONNome;
                    retorno = new dllImpresso.ExtratoBancario.ImpExtratoBal(TituloImp, dataI.DateTime, dataF.DateTime, RowCCTMae.CCT);
                    retorno.Apontamento(RowCCTMae.CCT_CON, 7);
                    //int carregados = Impresso.dImpExtratoB1.DadosExtratoBTableAdapter.Fill(Impresso.dImpExtratoB1.DadosExtratoB, dataI.DateTime, dataF.DateTime, RowCCTMae.CCT);
                    //if (carregados == 0)
                    //    return;                    
                    //Impresso.Nomecondominio.Text = Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(RowCCTMae.CCT_CON).CONNome;
                    
                    //Impresso.DataInicial.Text = dataI.DateTime.ToString("dd/MM/yyyy");
                    //Impresso.DataFinal.Text = dataF.DateTime.ToString("dd/MM/yyyy");
                    //Impresso.dImpExtratoB1.CalculosDadosExtratoB(true,true);
                    //Impresso.CreateDocument();
                
                return retorno;
            
        }

        //private int ContadorGeral;

        /*
        private void Imprime(){
            ImpressoCarregado.Print();
            //ContadorGeral = ContadorGeral + Impresso.Apontadas;
        }*/

        /*
        private void Email() {
            //VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(RowCCTMae.CCTLoteEmail,"Extrato",
            
        }*/

        private dLoteExtratos.ContaCorrenTeRow RowCCTMae;

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {                                                                     
            foreach (dLoteExtratos.ContaCorrenTeRow RowCCT in dLoteExtratos.ContaCorrenTe) {
                //Impresso = ImpressoCarregado();
                RowCCTMae = RowCCT;
                bool Imprimir = (!RowCCT.IsCCTLoteNull()) && (RowCCT.CCTLote && cBotaoImpBol1.BotaoClicado.EstaNoGrupo(Botao.botao, Botao.imprimir, Botao.imprimir_frente));
                bool enviar = (!RowCCT.IsCCTLoteEmailNull()) && (RowCCT.CCTLoteEmail != "") && cBotaoImpBol1.BotaoClicado.EstaNoGrupo(Botao.botao, Botao.email);
                if (Imprimir || enviar)
                {
                    dllImpresso.ExtratoBancario.ImpExtratoBal impresso = ImpressoCarregado();
                    if (impresso.ContemDados)
                    {
                        if (Imprimir)
                            impresso.Print();
                        if (enviar)
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(RowCCTMae.CCTLoteEmail, "Extrato", impresso, "Extrato Anexo", "Extrato");            
                    }
                }
            }
            //if (ContadorGeral > 0)
              //  MessageBox.Show(string.Format("Condom�nios com apontamentos: {0}",ContadorGeral));
        }

        private void dataI_EditValueChanged(object sender, EventArgs e)
        {
            cBotaoImpBol1.Enabled = ((dataI.EditValue != null) && ((dataF.EditValue != null)));
        }

        /*
        private void simpleButton1_Click(object sender, EventArgs e)
        {            
            string ComandoPrincipal =
"SELECT     SaldoContaCorrente.SCCValorI, SaldoContaCorrente.SCCValorF, SaldoContaCorrente.SCCData, SaldoContaCorrente.SCC_CCT, SaldoContaCorrente.SCC, \r\n" +
"                      CONDOMINIOS.CONCodigo\r\n" +
"FROM         SaldoContaCorrente INNER JOIN\r\n" +
"                      ContaCorrenTe ON SaldoContaCorrente.SCC_CCT = ContaCorrenTe.CCT INNER JOIN\r\n" +
"                      CONDOMINIOS ON ContaCorrenTe.CCT_CON = CONDOMINIOS.CON\r\n" +
"WHERE     (SaldoContaCorrente.SCCData = @P1);";
            string ComandoAnteriores =
"SELECT     TOP (2) SCCValorI, SCCValorF, SCCData, SCC_CCT\r\n" +
"FROM         SaldoContaCorrente\r\n" +
"WHERE     (SCCData < @P1) AND (SCC_CCT = @P2)\r\n" +
"ORDER BY SCCData DESC;";
            string ComandoPosteriores =
"SELECT     TOP (2) SCCValorI, SCCValorF, SCCData, SCC_CCT\r\n" +
"FROM         SaldoContaCorrente\r\n" +
"WHERE     (SCCData > @P1) AND (SCC_CCT = @P2)\r\n" +
"ORDER BY SCCData;";
            string ComandoCorrecao =
"UPDATE    SaldoContaCorrente\r\n" +
"SET              SCCValorI = @P1, SCCValorF = @P2\r\n" +
"WHERE     (SCC = @P3);";
            DateTime? Data = DateTime.Today;
            DataTable TA;
            DataTable TP;
            if(VirInput.Input.Execute("Data com erro:",ref Data,null,null))
            {
                string Relatorio = string.Format("Relat�rio de corre��o\r\nData da opera��o: {0}\r\nData a ser corrigida {1:dd/MM/yyyy}\r\n",DateTime.Now,Data.Value);
                DataTable Primcipal = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(ComandoPrincipal,Data.Value);
                if (Primcipal == null)
                    return;
                Relatorio += string.Format("Linhas a Verificar: {0}\r\n\r\n", Primcipal.Rows.Count);
                int i = 0;
                foreach(DataRow DR in Primcipal.Rows)
                {
                    i++;
                    TA = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(ComandoAnteriores, Data.Value, DR["SCC_CCT"]);
                    TP = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(ComandoPosteriores, Data.Value, DR["SCC_CCT"]);
                    if ((TA == null) || (TP == null) || (TA.Rows.Count != 2) || (TP.Rows.Count == 0))
                    {
                        Relatorio += string.Format("{0} - Conta:{1} SEQUENCIA INCOMPLETA\r\n", i, DR["CONCodigo"]);
                        continue;
                    }
                    if ((decimal)TA.Rows[1]["SCCValorF"] != (decimal)TA.Rows[0]["SCCValorI"])
                    {
                        Relatorio += string.Format("{0} - Conta:{1} Erro anterior � data solicitada!\r\n", i, DR["CONCodigo"]);
                        continue;
                    }
                    if (TP.Rows.Count == 2)
                    {
                        if ((decimal)TP.Rows[0]["SCCValorF"] != (decimal)TP.Rows[1]["SCCValorI"])
                        {
                            Relatorio += string.Format("{0} - Conta:{1} Erro posterior � data solicitada!\r\n", i, DR["CONCodigo"]);
                            continue;
                        }
                    }
                    if (((decimal)TA.Rows[0]["SCCValorF"] == (decimal)DR["SCCValorI"]) && ((decimal)TP.Rows[0]["SCCValorI"] == (decimal)DR["SCCValorF"]))
                    {
                        Relatorio += string.Format("{0} - Conta:{1} OK - sem necessidade de corre��o!\r\n", i, DR["CONCodigo"]);
                        continue;
                    }
                    if (((decimal)TA.Rows[0]["SCCValorF"] - (decimal)DR["SCCValorI"]) != ((decimal)TP.Rows[0]["SCCValorI"] - (decimal)DR["SCCValorF"]))
                    {
                        Relatorio += string.Format("{0} - Conta:{1} Erro fora do esperado!\r\n", i, DR["CONCodigo"]);
                        continue;
                    }
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoCorrecao, TA.Rows[0]["SCCValorF"], TP.Rows[0]["SCCValorI"], DR["SCC"]);
                    Relatorio += string.Format("{0} - Conta:{1} CORRIGIDO\r\n", i, DR["CONCodigo"]);                    
                };
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br",Relatorio,"Corre��o de extratos "+Data.Value.ToString());
                MessageBox.Show(Relatorio);
            }
        }
        */
    }
}

