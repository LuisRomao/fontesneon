﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using CompontesBasicos;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace dllAssembleia
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cVotacoes : ComponenteBase
    {
        #region Geração de votos



        //private void GeraVotos(int VOT, string VOTPergunta)
        private void GeraVotos(dVotacao dVotExp,dVotacao.VOTacaoRow rowVOT)
        {            
            //dVotacao.VOtOTableAdapter.Fill(dVotacao.VOtO, rowVOT.VOT);

            if (dVotacao.VOtO.Rows.Count == 0)
            {
                dVotacao.DadosCargaTableAdapter.Fill(dVotacao.DadosCarga, lookupCondominio_F1.CON_sel);
                if (rowVOT.IsVOTTodosBlocosNull() || !rowVOT.VOTTodosBlocos)
                {

                    System.Collections.SortedList Blocos = new System.Collections.SortedList();
                    foreach (dVotacao.DadosCargaRow rowAmostra in dVotacao.DadosCarga)
                        if (!Blocos.Contains(rowAmostra.BLO))
                            Blocos.Add(rowAmostra.BLO, string.Format("{0} - {1}", rowAmostra.BLOCodigo, rowAmostra.BLONome));
                    if (Blocos.Count > 1)
                    {
                        List<int> Selecionados;
                        if (!VirInput.Input.Execute(rowVOT.VOTPergunta, "Blocos votantes", Blocos, out Selecionados))
                            return;
                        if (Selecionados.Count == Blocos.Count)
                        {
                            rowVOT.VOTTodosBlocos = true;
                        }
                        else
                        {
                            rowVOT.VOTTodosBlocos = false;
                            dVotacao.DadosCarga.Clear();
                            try
                            {
                                dVotacao.DadosCargaTableAdapter.ClearBeforeFill = false;
                                foreach (int BLO in Selecionados)
                                    dVotacao.DadosCargaTableAdapter.FillByBLO(dVotacao.DadosCarga, BLO);
                            }
                            finally
                            {
                                dVotacao.DadosCargaTableAdapter.ClearBeforeFill = true;
                            }
                        }
                    }
                    else
                    {
                        rowVOT.VOTTodosBlocos = true;
                    }
                }

                foreach (dVotacao.DadosCargaRow rowDados in dVotacao.DadosCarga)
                {
                    dVotacao.VOtORow nova = dVotExp.VOtO.NewVOtORow();
                    nova.VOO_APT = rowDados.APT;
                    nova.VOO_VOT = rowVOT.VOT;
                    dVotExp.VOtO.AddVOtORow(nova);
                    /*
                foreach (dVotacao.VOtacaoDetalheRow vodrow in VOTrow.GetVOtacaoDetalheRows())
                {
                    dVotacao.AlternativasRow novaAlt = Alternativas.NewAlternativasRow();
                    novaAlt.VOO = votorow.VOO;
                    novaAlt.VOD = vodrow.VOD;
                    //novaAlt.Codigo = string.Format("{0:00000}{1:00000}", votorow.VOO, vodrow.VOD);
                    novaAlt.Codigo = string.Format("{0:000000}{1:00000}", votorow.VOO, vodrow.VOD);
                    novaAlt.Texto = vodrow.VODDescricao;
                    Alternativas.AddAlternativasRow(novaAlt);
                }
                dVotacao.DadosCargaRow rowCarga = DadosCarga.FindByAPT(votorow.VOO_APT);
                if (rowCarga != null)
                {
                    string bloco = rowCarga.BLOCodigo.ToUpper() == "SB" ? "" : rowCarga.BLOCodigo + " - ";
                    votorow.Identificacao = string.Format("{0:dd/MM/yyyy} {1}{2}", VOTacao[0].VOTData, bloco, rowCarga.APTNumero);
                    votorow.AcceptChanges();
                }
                    */
                    //dVotacao.VOtOTableAdapter.Update(nova);
                    nova.AcceptChanges();
                }
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public cVotacoes()
        {
            InitializeComponent();
            virEnumVOTStatus.virEnumVOTStatusSt.CarregaEditorDaGrid(colVOTStatus);
        }                

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            dVotacao.DadosCarga.Clear();
            dVotacao.VOtO.Clear();
            dVotacao.VOtacaoDetalhe.Clear();
            dVotacao.VOTacao.Clear();
            if (lookupCondominio_F1.CON_sel > 0)
            {
                dVotacao.VOTacaoTableAdapter.Fill(dVotacao.VOTacao, lookupCondominio_F1.CON_sel);
                //Legado não permitir ordem = zero                
                foreach (dVotacao.VOTacaoRow rowCorrigir in dVotacao.VOTacao)
                    if (rowCorrigir.IsVOTOrdemNull())
                        rowCorrigir.VOTOrdem = 0;
                dVotacao.VOtacaoDetalheTableAdapter.FillByCON(dVotacao.VOtacaoDetalhe, lookupCondominio_F1.CON_sel);
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if(DRV == null)
                return;
            dVotacao.VOTacaoRow rowVOT = (dVotacao.VOTacaoRow)DRV.Row;
            if (rowVOT != null)
            {
                dVotacao.VOTacaoTableAdapter.Update(rowVOT);
                rowVOT.AcceptChanges();
            }
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            dVotacao.VOTacaoRow rowVOT = (dVotacao.VOTacaoRow)gridView1.GetDataRow(e.RowHandle);
            if (rowVOT != null)
            {
                rowVOT.VOT_CON = lookupCondominio_F1.CON_sel;
                rowVOT.VOTStatus = virEnumVOTStatus.ParaBanco(VOTStatus.Cadastrado);
                rowVOT.VOTFracao = true;
                rowVOT.VOTnVotos = 1;
                rowVOT.VOTOrdem = ProxOrd();
                rowVOT.VOTTodosBlocos = true;
            }
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if (DRV == null)
                return;
            
            dVotacao.VOtacaoDetalheRow rowVOD = (dVotacao.VOtacaoDetalheRow)DRV.Row;
            if (rowVOD != null)
            {                
                dVotacao.VOtacaoDetalheTableAdapter.Update(rowVOD);
                rowVOD.AcceptChanges();
            }
        }

        #region Desligados temporariamente
        private void cBotaoImp1_clicado(object sender, dllBotao.BotaoArgs args)
        {
            /*
            dVotacao.VOTacaoRow rowVOT = (dVotacao.VOTacaoRow)gridView1.GetFocusedDataRow();            
            if (rowVOT == null)
                return;
            VOTStatus status = virEnumVOTStatus.DoBanco(rowVOT.VOTStatus);
                        
            GeraVotos(rowVOT.VOT,rowVOT.VOTPergunta);

            dVotacao.GeraCedulas();
            
            dVotacao.VOTacaoTableAdapter.Update(rowVOT);
            rowVOT.AcceptChanges();
            impCedulas impresso = new impCedulas();
            impresso.ajustaDataset(dVotacao);
            impresso.xrLabel1.Text = rowVOT.VOTPergunta;
            if (args.Botao == dllBotao.Botao.imprimir)
                impresso.Apontamento(lookupCondominio_F1.CON_sel, 11);
            else
                impresso.Apontamento(0, 11);
            cBotaoImp1.Impresso = impresso;*/
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            /*
            DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
            if (GV == null)
                return;
            DataRow DR = GV.GetFocusedDataRow();
            if (DR == null)
                return;


            if (DR is dVotacao.VOtacaoDetalheRow)
            {
                rowVOD = (dVotacao.VOtacaoDetalheRow)DR;
                if (dVotacao.VOtOTableAdapter.Fill(dVotacao.VOtO, rowVOD.VOD_VOT) != 0)
                {
                    MessageBox.Show("As cédulas já foram gerados","ERRO",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }
                rowVOD.Delete();
                dVotacao.VOtacaoDetalheTableAdapter.Update(dVotacao.VOtacaoDetalhe);
                dVotacao.VOtacaoDetalhe.AcceptChanges();

            }
            else
            {
                if (DR is dVotacao.VOTacaoRow)
                {
                    rowVOT = (dVotacao.VOTacaoRow)DR;
                    if (rowVOT.GetVOtacaoDetalheRows().Length == 0)
                    {
                        rowVOT.Delete();
                        dVotacao.VOTacaoTableAdapter.Update(dVotacao.VOTacao);
                        dVotacao.VOTacao.AcceptChanges();
                    }
                }
            }*/
        } 
        #endregion

        private void limpaPasta(string Pasta, int? CON, int? EMP)
        {
            string[] arquivosAntigos = Directory.GetFiles(Pasta, "VOTCON_*.xml");
            foreach (string arquivocandidato in arquivosAntigos)
            {
                string[] partes = Path.GetFileNameWithoutExtension(arquivocandidato).Split('_');
                if (partes.Length != 6)
                    continue;
                try
                {
                    DateTime DataVOT = new DateTime(int.Parse(partes[3]), int.Parse(partes[2]), int.Parse(partes[1]));
                    if (DataVOT < DateTime.Today.AddDays(-7))
                        File.Delete(arquivocandidato);
                    if (CON.HasValue && EMP.HasValue && (CON.Value == int.Parse(partes[4])) && (EMP.Value == int.Parse(partes[5])))
                        File.Delete(arquivocandidato);
                }
                catch { };
            }
        }

        //private void PublicaUm(dVotacao.VOTacaoRow rowVOTlocal, string Diretorio, string DiretorioBAK)
        private void Publicar(params string[] Diretorios)
        {
            DateTime MaiorData = DateTime.Today;
            string Arquivo = "";
            string ArquivoTMP = "";
            foreach (dVotacao.VOTacaoRow rowVOTlocal in dVotacao.VOTacao)
            {
                VOTStatus status = (VOTStatus)rowVOTlocal.VOTStatus;
                if (status != VOTStatus.Concluido)
                {
                    if (rowVOTlocal.VOTStatus != virEnumVOTStatus.ParaBanco(VOTStatus.OffLine))
                        rowVOTlocal.VOTStatus = virEnumVOTStatus.ParaBanco(VOTStatus.OffLine);
                    if (rowVOTlocal.VOTData < DateTime.Today)
                    {
                        MessageBox.Show("ATENÇÃO: Votação vencida!\r\n Mudea data ou o status!", "ARQUIVO NÃO GRAVADO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                        if (rowVOTlocal.VOTData > MaiorData)
                            MaiorData = rowVOTlocal.VOTData;
                }
            }
            Arquivo = string.Format("VOTCON_{0}_{1}_{2}_{3}_{4}.xml", MaiorData.Day, MaiorData.Month, MaiorData.Year, lookupCondominio_F1.CON_sel, Framework.DSCentral.EMP);
            ArquivoTMP = string.Format("{0}\\TMP\\{1}", Path.GetDirectoryName(Application.ExecutablePath), Arquivo);
            dVotacao.VOTacaoTableAdapter.Update(dVotacao.VOTacao);
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {

                /*
                string Arquivo = string.Format("VOTCON{0}_{1}_{2}_{3}_{4}.xml", Diretorio, lookupCondominio_F1.CON_sel, Framework.DSCentral.EMP);
                string ArquivoBAK = string.Format("{0}\\VOTCON{1}_{2}.xml", DiretorioBAK, lookupCondominio_F1.CON_sel, Framework.DSCentral.EMP);
                if (File.Exists(Arquivo))
                    File.Delete(Arquivo);
                if (File.Exists(ArquivoBAK))
                    File.Delete(ArquivoBAK);
                */
                //VOTStatus VOTStatus1 = (VOTStatus)rowVOTlocal.VOTStatus;
                //if (VOTStatus1 == VOTStatus.Concluido)
                //    return;
                //GeraVotos(rowVOTlocal);
                ESP.Espere("Preparando arquivo");
                dVotacao dVotExp = new dVotacao();
                if (dVotacao.VOTacaoTableAdapter.FillByPublicar(dVotExp.VOTacao, lookupCondominio_F1.CON_sel) == 0)
                    return;
                dVotacao.VOtacaoDetalheTableAdapter.FillByPublicar(dVotExp.VOtacaoDetalhe, lookupCondominio_F1.CON_sel);
                foreach (dVotacao.VOTacaoRow rowVOT in dVotExp.VOTacao)
                {
                    rowVOT.Condominio = string.Format("{0} - {1}", lookupCondominio_F1.CON_selrow.CONCodigo, lookupCondominio_F1.CON_selrow.CONNome);
                    ESP.Espere("Gerando votos");
                    GeraVotos(dVotExp, rowVOT);
                };
                ESP.Espere("Carregando Unidades");
                dVotacao.DadosCargaTableAdapter.Fill(dVotExp.DadosCarga, lookupCondominio_F1.CON_sel);
                //dVotacao.VOtOTableAdapter.Fill(dVotExp.VOtO, rowVOTlocal.VOT);                       
                //inadimplencia
                DateTime datacorte = Framework.datasets.dFERiados.DiasUteis(DateTime.Today, 3, Framework.datasets.dFERiados.Sentido.retorna, Framework.datasets.dFERiados.Sabados.naoUteis, Framework.datasets.dFERiados.Feriados.naoUteis);
                ESP.Espere("Carregando inadimplência - boletos");
                dVotacao.BOLetosTableAdapter.Fill(dVotExp.BOLetos, datacorte, lookupCondominio_F1.CON_sel);
                ESP.Espere("Carregando inadimplência - Acordos");
                dVotacao.ACOrdosTableAdapter.Fill(dVotExp.ACOrdos, lookupCondominio_F1.CON_sel);
                
                ESP.Espere("Gerando arquivos");
                dVotExp.WriteXml(ArquivoTMP);
                foreach (string diretorio in Diretorios)
                {
                    limpaPasta(diretorio, lookupCondominio_F1.CON_sel, Framework.DSCentral.EMP);
                    try
                    {
                        File.Copy(ArquivoTMP, string.Format("{0}\\{1}", diretorio, Arquivo), true);
                    }
                    catch { };
                }
            }
            if(folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                try
                {
                    File.Copy(ArquivoTMP, string.Format("{0}{1}{2}", folderBrowserDialog1.SelectedPath ,
                                                                     folderBrowserDialog1.SelectedPath.EndsWith("\\") ? "" : "\\", 
                                                                     Arquivo), true);
                }
                catch { };
        }

       

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string RepositorioVOT = string.Format("{0}\\VOTACOES", dEstacao.dEstacaoSt.GetValor("Repositorio"));
            if (!Directory.Exists(RepositorioVOT))
                Directory.CreateDirectory(RepositorioVOT);
            else
            {
                /*
                limpar
                string[] arquivosAntigos = Directory.GetFiles(RepositorioVOT, "VOT*.xml");
                foreach (string arquivocandidato in arquivosAntigos)
                {
                    try
                    {
                        string NomeCandidato = CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(Path.GetFileNameWithoutExtension(arquivocandidato), CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.SoNumeros);
                        if (NomeCandidato.Length >= 8)
                        {
                            DateTime DataVOT = new DateTime(int.Parse(NomeCandidato.Substring(0, 2)), int.Parse(NomeCandidato.Substring(2, 2)), int.Parse(NomeCandidato.Substring(4, 4)));
                            if (DataVOT < DateTime.Today.AddDays(-7))
                                File.Delete(arquivocandidato);

                        }
                    }
                    catch { };
                }
                */
            }

            string Diretorio = string.Format("{0}\\VOTACOES", Path.GetDirectoryName(Application.ExecutablePath));
            string DiretorioBAK = string.Format("{0}\\VOTACOES_BAK", Path.GetDirectoryName(Application.ExecutablePath));
            if (!Directory.Exists(Diretorio))
                Directory.CreateDirectory(Diretorio);
            if (!Directory.Exists(DiretorioBAK))
                Directory.CreateDirectory(DiretorioBAK);
            Publicar(Diretorio, DiretorioBAK,RepositorioVOT);
            //foreach (dVotacao.VOTacaoRow rowVOT in dVotacao.VOTacao)                            
            //    PublicaUm(rowVOT, Diretorio,DiretorioBAK);
            
        }

        private void CarregaDatosVotacao(int VOT, dVotacao dVotacao1)
        {
            dVotacao.VOTacaoRow novarowVOT = dVotacao1.VOTacao[0];
            if (dVotacao.VOTacaoTableAdapter.FillByVOT(dVotacao1.VOTacao, VOT) != 1)
                return;
            dVotacao.DadosCargaTableAdapter.Fill(dVotacao1.DadosCarga, novarowVOT.VOT_CON);
            dVotacao.VOtacaoDetalheTableAdapter.FillBy(dVotacao1.VOtacaoDetalhe, VOT);
            if (dVotacao.VOtOTableAdapter.Fill(dVotacao1.VOtO, VOT) == 0)
            {
                MessageBox.Show("As cédulas ainda não foram geradas");
                return;
            };
           
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            /*
            dVotacao.VOTacaoRow rowVOT = (dVotacao.VOTacaoRow)gridView1.GetFocusedDataRow();
            if (rowVOT == null)
                return;
            int VOT = rowVOT.VOT;

            dVotacao novadVotacao = new dllAssembleia.dVotacao();

            CarregaDatosVotacao(VOT,novadVotacao);

            

            cVotacao cVotacao1 = new cVotacao(novadVotacao,VOT,Abertura.ArquivoAbrir);
            cVotacao1.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
            */
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            try
            {
                if (e.Column == colVOTStatus)
                {
                    dVotacao.VOTacaoRow rowVOT = (dVotacao.VOTacaoRow)gridView1.GetDataRow(e.RowHandle);
                    if (rowVOT == null)
                        return;
                    if (e.CellValue != DBNull.Value)
                    {
                        e.Appearance.BackColor = virEnumVOTStatus.virEnumVOTStatusSt.GetCor(e.CellValue);
                        e.Appearance.ForeColor = Color.Black;
                    }
                }
            }
            catch
            { 
            }
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            dVotacao.VOTacaoRow rowVOT = (dVotacao.VOTacaoRow)gridView1.GetDataRow(e.RowHandle);
            if (rowVOT != null)

                if (rowVOT["VOTData"] == DBNull.Value)
                {                    
                    DateTime? Data = DateTime.Today;
                    if (VirInput.Input.Execute("Data da votação", ref Data, DateTime.Today.AddMonths(-2), DateTime.Today.AddMonths(6)))
                        rowVOT.VOTData = Data.GetValueOrDefault(DateTime.Today);
                }
        }

        private void gridView2_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            dVotacao.VOtacaoDetalheRow rowVOD = (dVotacao.VOtacaoDetalheRow)GV.GetDataRow(e.RowHandle);
            if (rowVOD != null)
            {
                rowVOD.VODCor = dVotacao.CorPrao;                
            }
        }

        private GridHitInfo downHitInfoGrupo = null;

        private int OrigemVOTacaorowhandle;

        private GridView viewTarget;

        private int dropTargetRowHandle = -1;

        private int DropTargetRowHandle
        {
            get { return dropTargetRowHandle; }
            set
            {
                dropTargetRowHandle = value;
                if (value == -1)
                    viewTarget = null;
                gridControl1.Invalidate();
            }
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            downHitInfoGrupo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None)
                return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.RowHandle != GridControl.NewItemRowHandle)
                downHitInfoGrupo = hitInfo;
        }

        private void gridView1_MouseMove(object sender, MouseEventArgs e)
        {        
            GridView view = sender as GridView;
            dVotacao.VOTacaoRow rowMover = (dVotacao.VOTacaoRow)view.GetFocusedDataRow();            
            if ((e.Button == MouseButtons.Left)
                &&
                (downHitInfoGrupo != null)
                &&
                (rowMover != null))
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfoGrupo.HitPoint.X - dragSize.Width / 2,
                    downHitInfoGrupo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    OrigemVOTacaorowhandle = view.FocusedRowHandle;
                    view.GridControl.DoDragDrop(rowMover, DragDropEffects.All);
                    downHitInfoGrupo = null;
                }
            }        
        }

        private void gridControl1_DragOver(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(typeof(dVotacao.VOTacaoRow)))
            {
                e.Effect = DragDropEffects.None;
                return;
            }
            GridControl grid = sender as GridControl;
            Point pt = grid.PointToClient(new Point(e.X, e.Y));
            GridView view = (GridView)grid.GetViewAt(pt);
            if (view == null)
            {
                e.Effect = DragDropEffects.None;
                return;
            }
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new Point(e.X, e.Y)));

            viewTarget = view;
            if (view.Name != "gridView1")
            {
                e.Effect = DragDropEffects.None;
                DropTargetRowHandle = -1;
            }
            else
            {
                if (hitInfo.HitTest == GridHitTest.EmptyRow)
                    DropTargetRowHandle = view.DataRowCount;
                else
                    DropTargetRowHandle = hitInfo.RowHandle;

                if ((OrigemVOTacaorowhandle != hitInfo.RowHandle) // a propria linha
                        &&
                        (hitInfo.InRow || (hitInfo.HitTest == GridHitTest.EmptyRow)) // tem que estar dentro de uma linha
                    //&&
                    //(hitInfo.RowHandle != GridControl.NewItemRowHandle)  // linha de inclusão 
                    //&& 
                    //(hitInfo.RowHandle >= 0)  //linha viva
                    )
                {
                    dVotacao.VOTacaoRow rowTargetVOTO = (dVotacao.VOTacaoRow)view.GetDataRow(DropTargetRowHandle);
                    dVotacao.VOTacaoRow rowMovidaVOTO = (dVotacao.VOTacaoRow)e.Data.GetData(typeof(dVotacao.VOTacaoRow));
                    if ((rowTargetVOTO == null) || (rowTargetVOTO.VOTData != rowMovidaVOTO.VOTData))
                    {
                        e.Effect = DragDropEffects.None;
                        DropTargetRowHandle = -1;
                    }
                    else
                        e.Effect = DragDropEffects.Move;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                    DropTargetRowHandle = -1;
                }
            }


        }

        private void gridControl1_DragLeave(object sender, EventArgs e)
        {
            DropTargetRowHandle = -1;
        }

        private void gridControl1_DragDrop(object sender, DragEventArgs e)
        {            
            GridControl grid = sender as GridControl;
            Point pt = grid.PointToClient(new Point(e.X, e.Y));
            GridView view = (GridView)grid.GetViewAt(pt);
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new Point(e.X, e.Y)));
            int TargetRowHandle = hitInfo.RowHandle;
            //bool isMaster = true;
            if (view == null)
                return;
            if (e.Data.GetDataPresent(typeof(dVotacao.VOTacaoRow))) //Arrastando grupo
            {
                dVotacao.VOTacaoRow rowMovida = (dVotacao.VOTacaoRow)e.Data.GetData(typeof(dVotacao.VOTacaoRow));
                if (TargetRowHandle >= 0)
                {
                    dVotacao.VOTacaoRow rowTargetVOTO = (dVotacao.VOTacaoRow)view.GetDataRow(TargetRowHandle);
                    MoveVOTacao(rowTargetVOTO.VOTOrdem, rowMovida);
                    dVotacao.VOTacaoTableAdapter.Update(dVotacao.VOTacao);
                    dVotacao.VOTacao.AcceptChanges();                    
                }
                else
                {
                    rowMovida.VOTOrdem = ProxOrd();
                    dVotacao.VOTacaoTableAdapter.Update(rowMovida);
                    rowMovida.AcceptChanges();                    
                }
            }
            
            DropTargetRowHandle = -1;
        }

        private void MoveVOTacao(int OrdemDestino, dVotacao.VOTacaoRow Movido)
        {
            if (Movido.VOTOrdem == OrdemDestino)
                return;
            else if (Movido.VOTOrdem > OrdemDestino)
            {
                foreach (dVotacao.VOTacaoRow rowRenumera in dVotacao.VOTacao)
                    if ((rowRenumera.VOTOrdem >= OrdemDestino) && (rowRenumera.VOTOrdem < Movido.VOTOrdem))
                        rowRenumera.VOTOrdem++;
                Movido.VOTOrdem = OrdemDestino;
            }
            else
            {
                foreach (dVotacao.VOTacaoRow rowRenumera in dVotacao.VOTacao)
                    if ((rowRenumera.VOTOrdem < OrdemDestino) && (rowRenumera.VOTOrdem > Movido.VOTOrdem))
                        rowRenumera.VOTOrdem--;
                Movido.VOTOrdem = OrdemDestino - 1;
            }
        }

        private int? _MaiorOrdem;

        private int MaiorOrdem
        {
            get
            {
                if (!_MaiorOrdem.HasValue)
                {
                    _MaiorOrdem = 0;
                    foreach (dVotacao.VOTacaoRow rowi in dVotacao.VOTacao)
                        if (rowi.VOTOrdem > _MaiorOrdem)
                            _MaiorOrdem = rowi.VOTOrdem;
                };
                return _MaiorOrdem.Value;
            }
            set { _MaiorOrdem = value; }
        }

        private int ProxOrd()
        {
            MaiorOrdem = MaiorOrdem + 1;
            return MaiorOrdem;
        }

        private void gridControl1_Paint(object sender, PaintEventArgs e)
        {
            if (DropTargetRowHandle < 0)
                return;
            GridControl grid = (GridControl)sender;

            bool isBottomLine = (DropTargetRowHandle == viewTarget.DataRowCount);

            GridViewInfo viewInfo = viewTarget.GetViewInfo() as GridViewInfo;
            GridRowInfo rowInfo = viewInfo.GetGridRowInfo(isBottomLine ? DropTargetRowHandle - 1 : DropTargetRowHandle);

            if (rowInfo == null) return;

            Point p1, p2;
            if (isBottomLine)
            {
                //p1 = new Point(rowInfo.Bounds.Left, rowInfo.Bounds.Bottom - 1);
                //p2 = new Point(rowInfo.Bounds.Right, rowInfo.Bounds.Bottom - 1);
                p1 = new Point(rowInfo.Bounds.Left, rowInfo.TotalBounds.Bottom - 1);
                p2 = new Point(rowInfo.Bounds.Right, rowInfo.TotalBounds.Bottom - 1);
            }
            else
            {
                p1 = new Point(rowInfo.Bounds.Left, rowInfo.Bounds.Top - 1);
                p2 = new Point(rowInfo.Bounds.Right, rowInfo.Bounds.Top - 1);
            }
            e.Graphics.DrawLine(Pens.Red, p1, p2);
        }

    }
}
