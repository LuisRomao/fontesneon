﻿using System;

namespace dllAssembleia
{

    /// <summary>
    /// Enumeração para campo VOTStatus
    /// </summary>
    public enum VOTStatus
    {
        /// <summary>
        /// 
        /// </summary>
        Cadastrado = 0,
        /// <summary>
        /// 
        /// </summary>
        ComCedulas = 1,
        /// <summary>
        /// 
        /// </summary>
        OffLine = 2,   
        /// <summary>
        /// 
        /// </summary>
        ParaRetornar = 3,
        /// <summary>
        /// 
        /// </summary>
        Concluido = 4
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumVOTStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumVOTStatus(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumVOTStatus _virEnumVOTStatusSt;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int ParaBanco(VOTStatus status)
        {
            return (int)status;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="istatus"></param>
        /// <returns></returns>
        public static VOTStatus DoBanco(int istatus)
        {
            return (VOTStatus)istatus;
        }

        /// <summary>
        /// VirEnum estático para campo VOTStatus
        /// </summary>
        public static virEnumVOTStatus virEnumVOTStatusSt
        {
            get
            {
                if (_virEnumVOTStatusSt == null)
                {
                    _virEnumVOTStatusSt = new virEnumVOTStatus(typeof(VOTStatus));
                    _virEnumVOTStatusSt.GravaNomes(VOTStatus.Cadastrado, "Cadastrado", System.Drawing.Color.White);
                    _virEnumVOTStatusSt.GravaNomes(VOTStatus.ComCedulas, "Cédulas Geradas", System.Drawing.Color.Yellow);
                    _virEnumVOTStatusSt.GravaNomes(VOTStatus.OffLine, "Off line", System.Drawing.Color.Pink);
                    _virEnumVOTStatusSt.GravaNomes(VOTStatus.ParaRetornar, "Para retornar", System.Drawing.Color.Lime);                    
                    _virEnumVOTStatusSt.GravaNomes(VOTStatus.Concluido, "Concluído", System.Drawing.Color.Silver);
                }
                return _virEnumVOTStatusSt;
            }
        }
    }
}