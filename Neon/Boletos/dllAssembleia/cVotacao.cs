﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace dllAssembleia
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cVotacao : ComponenteBase
    {
        /// <summary>
        /// Construtor padrão para tempo de projeto
        /// </summary>
        public cVotacao()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        public string nomeXML = "";

        private dVotacao dVotacao1;

        private DevExpress.XtraCharts.Palette palette;

        private System.Collections.SortedList Indicepallet;

        private int icores = 0;

        private dVotacao.VOTacaoRow rowVOT;

        private void CriaCampos(int n)
        {
            int VisibleIndexMAX = colVOO_VOD.VisibleIndex + 1;
            string strNomeCampo = dVotacao1.NomeCampo(n);
            if (!dVotacao1.VOtO.Columns.Contains(strNomeCampo))
                dVotacao1.VOtO.Columns.Add(strNomeCampo, typeof(int));
            DevExpress.XtraGrid.Columns.GridColumn Coluna = new DevExpress.XtraGrid.Columns.GridColumn();
            
            Coluna.Caption = string.Format("Voto {0}", n);
            Coluna.ColumnEdit = this.repositoryItemLookVOTO;
            Coluna.FieldName = strNomeCampo;
            Coluna.Name = string.Format("colVOO_VOD_{0}", n);
            Coluna.Visible = true;
            gridView2.Columns.Add(Coluna);
            ColunasNovas.Add(Coluna);
            //Coluna.VisibleIndex = VisibleIndexMAX++;
        }

        private List<DevExpress.XtraGrid.Columns.GridColumn> ColunasNovas;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_dVotacao"></param>
        /// <param name="VOT"></param>
        public cVotacao(dVotacao _dVotacao,int VOT)
        {
            InitializeComponent();
            dVotacao1 = _dVotacao;
            //nomeXML = _nomeXML;
            bindingSource1.DataSource = _dVotacao;
            dadosCargaBindingSource.DataSource = dVotacao1;
            vOtacaoDetalheBindingSource.DataSource = dVotacao1;
            bindingSource1.Filter = string.Format("VOT={0}", VOT);
            vOtacaoDetalheBindingSource.Filter = string.Format("VOD_VOT={0}", VOT);
            rowVOT = _dVotacao.VOTacao.FindByVOT(VOT);
            if (rowVOT.IsVOTnVotosNull())
                rowVOT.VOTnVotos = 1;
            _dVotacao.nVotos = rowVOT.VOTnVotos;
            Titulo = rowVOT.VOTPergunta;
            foreach (dVotacao.VOtacaoDetalheRow rowVOD in rowVOT.GetVOtacaoDetalheRows())
                if (rowVOD.IsVODCorNull())
                    rowVOD.VODCor = _dVotacao.CorPrao;
            palette = new DevExpress.XtraCharts.Palette("novoP");
            Indicepallet = new System.Collections.SortedList();
            icores = 0;
            foreach (dVotacao.VOtacaoDetalheRow rowVOD in rowVOT.GetVOtacaoDetalheRows())
            {
                palette.Add(Color.FromArgb(rowVOD.VODCor));
                Indicepallet.Add(rowVOD.VOD, icores++);
            }
            chartControl1.PaletteRepository.Add("novoP", palette);
            chartControl1.PaletteName = "novoP";

            dVotacao1.PopulaLookup();
            gridView1.ExpandAllGroups();
            rowVOT.VOTStatus = dllAssembleia.virEnumVOTStatus.ParaBanco(VOTStatus.ParaRetornar);
            if (rowVOT.IsVOTFracaoNull())
                rowVOT.VOTFracao = true;
            //dVotacao1.Fracao = checkBox1.Checked = rowVOT.VOTFracao;
            ColunasNovas = new List<DevExpress.XtraGrid.Columns.GridColumn>();
            if (rowVOT.IsVOTnVotosNull())
                rowVOT.VOTnVotos = 1;
            if (rowVOT.VOTnVotos > 1)
            {
                colVOO_VOD.Caption = string.Format("Voto {0}", 1);
                for (int i = 2; i <= rowVOT.VOTnVotos; i++)
                    CriaCampos(i);

                foreach (dVotacao.MultiplosRow rowM in dVotacao1.Multiplos)
                {
                    dVotacao.VOtORow rowVOO = dVotacao1.VOtO.FindByVOO(rowM.VOO);
                    rowVOO[dVotacao1.NomeCampo(rowM.n)] = rowM.VOD;
                }
            }
            dVotacao1.Fracao = checkBox1.Checked = rowVOT.VOTFracao;
            dVotacao1.blockcalc = false;
            dVotacao1.CalTotais();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = memoEdit1.Font;
            fontDialog1.Color = memoEdit1.ForeColor;
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                memoEdit1.Font = fontDialog1.Font;
                memoEdit1.ForeColor = fontDialog1.Color;
            }
        }

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if ((e.RowHandle >= 0))
                if ((e.Column == colVOO_VOD) || (ColunasNovas.Contains(e.Column)))
                {
                    dVotacao.VOtORow rowVOO = (dVotacao.VOtORow)gridView2.GetDataRow(e.RowHandle);
                    if (rowVOO != null) //&& (!rowVOO.IsVOO_VODNull()))
                    {
                        if (rowVOO[e.Column.FieldName] != DBNull.Value)
                        {
                            int VOD = (int)rowVOO[e.Column.FieldName];
                            dVotacao.VOtacaoDetalheRow VODrow = dVotacao1.VOtacaoDetalhe.FindByVOD(VOD);
                            e.Appearance.BackColor = Color.FromArgb(VODrow.VODCor);
                            e.Appearance.ForeColor = Color.Black;
                        }
                    }
                }
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dVotacao1.CalTotais();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            dVotacao1.Fracao = checkBox1.Checked;
            dVotacao1.CalTotais();
        }

        private void cVotacao_OnClose(object sender, EventArgs e)
        {
            //dVotacao1.WriteXml(Abertura.ArquivoAbrir);
            if (nomeXML != "")
                dVotacao1.WriteXml(nomeXML);
        }

        private List<dVotacao.DadosCargaRow> testeProcuracoes(int procurador)
        {
            List<dVotacao.DadosCargaRow> rowAPTProcuracoes = new List<dVotacao.DadosCargaRow>();
            foreach (dVotacao.DadosCargaRow candidato in dVotacao1.DadosCarga)
            {
                if (!candidato.IsPP_APTNull() && (candidato.PP_APT == procurador))
                    rowAPTProcuracoes.Add(candidato);
            }
            return rowAPTProcuracoes;
        }

        private bool ApuraInterno(int voo, int vod)
        {
            dVotacao.VOtORow rowVOO = dVotacao1.VOtO.FindByVOO(voo);
            if (rowVOO != null)
            {
                dVotacao.DadosCargaRow rowAPT = rowVOO.DadosCargaRow;
                dVotacao.DadosCargaRow[] rowAPTProcuracoes = rowAPT.GetDadosCargaRows();
                //MessageBox.Show(string.Format("n = {0}",testeProcuracoes(rowAPT.APT).Count));
                foreach (dVotacao.DadosCargaRow rowAPTProcurao in rowAPTProcuracoes)
                    if (rowAPT.APT != rowAPTProcurao.APT)
                        foreach (dVotacao.VOtORow VOOrowProcuracao in rowAPTProcurao.GetVOtORows())
                            if (VOOrowProcuracao.VOO_VOT == rowVOO.VOO_VOT)
                                ApuraInterno(VOOrowProcuracao.VOO, vod);

                if (rowVOT.VOTnVotos == 1)
                    rowVOO.VOO_VOD = vod;
                else
                {
                    for (int i = 1; i <= rowVOT.VOTnVotos; i++)
                    {
                        if (rowVOO[dVotacao1.NomeCampo(i)] == DBNull.Value)
                        {
                            rowVOO[dVotacao1.NomeCampo(i)] = vod;
                            break;
                        }
                        else
                        {
                            if ((int)rowVOO[dVotacao1.NomeCampo(i)] == vod)
                                break;
                        }
                    }
                }
                //dVotacao1.CalTotais();
                //gridView2.MakeRowVisible(gridView2.LocateByValue("VOO", voo));
                return true;
            }
            else
                return false;
        }

        delegate bool del_Apura(int voo, int vod);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="voo"></param>
        /// <param name="vod"></param>
        /// <returns></returns>
        public bool Apura(int voo, int vod)
        {
            if (this.InvokeRequired)
            {
                del_Apura d = new del_Apura(Apura);
                return (bool)Invoke(d, new object[] {voo, vod });
            }
            else
            {
                if (ApuraInterno(voo, vod))
                {
                    dVotacao.VOtORow rowVOO = dVotacao1.VOtO.FindByVOO(voo);
                    dVotacao1.CalTotais();
                    gridView2.MakeRowVisible(gridView2.LocateByValue("VOO", voo));
                    return true;
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static SortedList<int, dllAssembleia.cVotacao> VotacoesAbertas;

        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string leitura = textEdit1.Text;
                textEdit1.Text = "";
                string strvoo = "";
                string strvod = "";
                if (leitura.Length == 11)
                {
                    strvoo = leitura.Substring(0, 6);
                    strvod = leitura.Substring(6, 5);
                }
                else
                    if (leitura.Length == 12)
                    {
                        strvoo = leitura.Substring(0, 6);
                        strvod = leitura.Substring(6, 5);
                    }
                    else
                        if (leitura.Length == 13)
                        {
                            strvoo = leitura.Substring(1, 6);
                            strvod = leitura.Substring(7, 5);
                        }


                if (strvoo != "")
                {
                    int voo = 0;
                    int vod = 0;
                    if (int.TryParse(strvoo, out voo) && int.TryParse(strvod, out vod))
                    {
                        if (!Apura(voo, vod))
                            foreach (cVotacao cVotacaoX in dllAssembleia.cVotacao.VotacoesAbertas.Values)
                                if (cVotacaoX.Apura(voo, vod))
                                    break;
                    }
                }
                e.Handled = true;
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dVotacao.VOtacaoDetalheRow rowVOD = (dVotacao.VOtacaoDetalheRow)gridView1.GetDataRow(e.RowHandle);
            if (rowVOD != null)
            {
                Color cor = Color.FromArgb(rowVOD.VODCor);
                if (palette[(int)Indicepallet[rowVOD.VOD]].Color != cor)
                {
                    palette[(int)Indicepallet[rowVOD.VOD]].Color = cor;
                    palette[(int)Indicepallet[rowVOD.VOD]].Color2 = cor;
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Reiniciar contagem ?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                foreach (dVotacao.VOtORow rowVOO in dVotacao1.VOtO)
                {
                    rowVOO.SetVOO_VODNull();
                    for (int i = 2; i <= rowVOT.VOTnVotos; i++)
                        rowVOO[dVotacao1.NomeCampo(i)] = DBNull.Value;
                }
                dVotacao1.Multiplos.Clear();
                dVotacao1.blockcalc = false;
                dVotacao1.CalTotais();
            }
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            dVotacao.VOtacaoDetalheRow rowVOD = (dVotacao.VOtacaoDetalheRow)gridView1.GetDataRow(e.RowHandle);
            if (rowVOD != null)
            {
                rowVOD.VODCor = dVotacao1.CorPrao;
                palette.Add(Color.FromArgb(rowVOD.VODCor));
                Indicepallet.Add(rowVOD.VOD, icores++);
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            /*
            dVotacao.VOtORow rowVOO = (dVotacao.VOtORow)gridView2.GetFocusedDataRow();
            if (rowVOO != null)
            {
                foreach (dVotacao dVotacaoX in dllAssembleia.OffLine.dVotacoes.XMLs.Values)
                    dVotacaoX.ImprimeCedula(rowVOO.VOO_APT);
            }*/
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                gridView2.ExportToPdf(saveFileDialog1.FileName);
                string segundo = System.IO.Path.GetFileNameWithoutExtension(saveFileDialog1.FileName) + "_tot.pdf";
                gridView1.ExportToPdf(saveFileDialog1.FileName.Replace(".pdf", "_tot.pdf"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public string NomeCampo(int n)
        {
            if (n == 1)
                return "VOO_VOD";
            else
                return string.Format("VOO_VOD_{0}", n);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            StringBuilder resultado = new StringBuilder();
            resultado.Append(string.Format("0x1d0xf90x200x000x1bE0x1b0x0e0x1ba0x01{0}0x1bF\r\n0x1ba0x00\r\n", rowVOT.VOTPergunta));
            foreach (dVotacao.VOtORow rowVoto in dVotacao1.VOtO)
            {
                if (rowVOT.VOTnVotos == 1)
                {
                    if (!rowVoto.IsVOO_VODNull())
                    {

                        dVotacao.VOtacaoDetalheRow rowVOD = dVotacao1.VOtacaoDetalhe.FindByVOD(rowVoto.VOO_VOD);
                        resultado.Append(string.Format("{0,4} - {1,4} {2}: {3}\r\n",
                                            rowVoto.calBloco,
                                            rowVoto.calApartamento,
                                            dVotacao1.Fracao ? string.Format("({0:0.000000})", rowVoto.calFracao) : "",
                                            rowVOD.VODDescricao));
                    }
                }
                else
                {
                    for (int i = 1; i <= rowVOT.VOTnVotos; i++)
                    {
                        if (rowVoto[NomeCampo(i)] == DBNull.Value)
                            continue;
                        dVotacao.VOtacaoDetalheRow rowVOD = dVotacao1.VOtacaoDetalhe.FindByVOD((int)rowVoto[NomeCampo(i)]);
                        resultado.Append(string.Format("{0,4} - {1,4} {2}: {3}\r\n",
                                            rowVoto.calBloco,
                                            rowVoto.calApartamento,
                                            dVotacao1.Fracao ? string.Format("({0:0.000000})", rowVoto.calFracao) : "",
                                            rowVOD.VODDescricao));
                    }
                }


            }
            resultado.Append(string.Format("---------------------------------------\r\n"));
            resultado.Append(string.Format("0x1b0x0e0x1ba0x010x1bETOTAL0x1bF\r\n0x1ba0x00\r\n"));
            foreach (dVotacao.VOtacaoDetalheRow rowVOD in dVotacao1.VOtacaoDetalhe)
            {

                resultado.Append(string.Format("{0,30} - ", rowVOD.VODDescricao));
                if (dVotacao1.Fracao)
                    resultado.Append(string.Format("{0:000.00}\r\n", rowVOD.Total));
                else
                    resultado.Append(string.Format("{0:00000}\r\n", rowVOD.Total));

            }
            resultado.Append("0x1bw");
            dVotacao.ImpVoto.Reset();
            dVotacao.ImpVoto.ImprimeDOC(dVotacao.TradutorEXA(resultado.ToString()));
        }

    }
}
