﻿using System;
using CodBar;
using System.Collections.Generic;

namespace dllAssembleia {
    
    
    /// <summary>
    /// 
    /// </summary>
    public partial class dVotacao 
    {
        //private bool EmTeste = true;

        private static CodBar.Impress _ImpVoto;

        private enum TiposImpressos { Impressora,Tela,}
       
        private static TiposImpressos TipoImpress;

        /// <summary>
        /// 
        /// </summary>
        public static CodBar.Impress ImpVoto
        {
            get
            {
                if (_ImpVoto == null)
                {
                    string NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora cédulas");

                    //só para legado, remover após versão 13.3.9.2 inicio
                    if (NomeImpressora == "")
                    {
                        NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de cópias de Cheque");
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora cédulas", NomeImpressora);
                    }
                    //só para legado, remover após versão 13.3.9.2 fim

                    if (NomeImpressora == "")
                    {
                        System.Windows.Forms.PrintDialog PD = new System.Windows.Forms.PrintDialog();
                        PD.ShowDialog();
                        NomeImpressora = PD.PrinterSettings.PrinterName;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora cédulas", NomeImpressora);
                    }

                    if (NomeImpressora == "TELA")
                    {
                        TipoImpress = TiposImpressos.Tela;
                        MascaraVoto = MascaraVotoTela;
                        MacaraAlternativas = MacaraAlternativasTela;
                    }
                    else
                    {
                        TipoImpress = TiposImpressos.Impressora;
                        MascaraVoto = TradutorEXA(MascaraVoto);
                        MacaraAlternativas = TradutorEXA(MacaraAlternativas);
                    }                                        
                    _ImpVoto = new CodBar.Impress(LinguagemCodBar.PM42,"", MascaraVoto);
                    _ImpVoto.RemoverAcentos = true;
                    
                    
                    _ImpVoto.NomeImpressora = NomeImpressora;
                    
                }
                return _ImpVoto;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int nVotos = 1;

        /// <summary>
        /// 
        /// </summary>
        public void GeraCedulas(VOTacaoRow VOTrow)
        {
            //if (Alternativas.Count != 0)
            //    return;
            foreach (dVotacao.VOtORow votorow in VOTrow.GetVOtORows())
            {
                foreach (dVotacao.VOtacaoDetalheRow vodrow in VOTrow.GetVOtacaoDetalheRows())
                {
                    dVotacao.AlternativasRow novaAlt = Alternativas.NewAlternativasRow();
                    novaAlt.VOO = votorow.VOO;
                    novaAlt.VOD = vodrow.VOD;
                    //novaAlt.Codigo = string.Format("{0:00000}{1:00000}", votorow.VOO, vodrow.VOD);
                    novaAlt.Codigo = string.Format("{0:000000}{1:00000}", votorow.VOO, vodrow.VOD);
                    novaAlt.Texto = vodrow.VODDescricao;
                    Alternativas.AddAlternativasRow(novaAlt);
                }
                dVotacao.DadosCargaRow rowCarga = DadosCarga.FindByAPT(votorow.VOO_APT);
                if (rowCarga != null)
                {
                    string bloco = rowCarga.BLOCodigo.ToUpper() == "SB" ? "" : rowCarga.BLOCodigo + " - ";
                    votorow.Identificacao = string.Format("{0:dd/MM/yyyy} {1}{2}", VOTacao[0].VOTData, bloco, rowCarga.APTNumero);
                    votorow.AcceptChanges();
                }
            }
        }

        private SortedList<int, dVotacao.VOTacaoRow> _Ordenado;

        
        /// <summary>
        /// Votações ordenadas
        /// </summary>
        public SortedList<int, dVotacao.VOTacaoRow> Ordenado
        {
            get
            {
                if (_Ordenado == null)
                {
                    _Ordenado = new SortedList<int, dVotacao.VOTacaoRow>();
                    foreach (dVotacao.VOTacaoRow rowVOT in VOTacao)
                        Ordenado.Add(rowVOT.VOTOrdem, rowVOT);
                }
                return _Ordenado;
            }
        }

        private static string MascaraVoto =
"0x1d0xf90x200x000x1ba0x010x1b0x0e0x1bd0x010x1bE%PERGUNTA%0x1bF0x1bd0x00\r\n\r\n0x1dh0x2d0x1dH0x00" +
"%DETALHES%" +
"0x1ba0x00%IDENT%%CORTE%";

        private static string MascaraVotoTela =
"PERGUNTA: %PERGUNTA%\r\n\r\n" +
"%DETALHES%\r\n" +
"Identificação: %IDENT%";

        private static string MacaraAlternativas = "0x1ba0x000x1b0x0e0x1bE0x1bd0x010xCF  0x1bd0x00{0}\r\n0x1ba0x020x1dk0x00{1}0x00\r\n";

        private static string MacaraAlternativasTela = "{1} - {0}\r\n";

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="comando"></param>
        /// <returns></returns>
        public static string TradutorEXA(string comando)
        {
            while (comando.Contains("0x"))
            {
                int local = comando.IndexOf("0x");
                string codigo = comando.Substring(local + 2, 2);
                int numerocod = Convert.ToInt32(codigo, 16);
                string codigoCorrigido = char.ConvertFromUtf32(numerocod);
                comando = comando.Replace("0x" + codigo, codigoCorrigido);
            }
            return comando;
        }

/*
        private static string MascaraVoto =
Environment.NewLine +
"OC,Fr\r\n" +
"Q20,0\r\n" +
"q750\r\n" +
"N\r\n" +
"A10,10,0,2,2,2,R,\"%PERGUNTA%\"\r\n" +
"%DETALHES%" +
"A0,%120+100i%,0,2,2,2,N,\"%IDENT%\"\r\n" +
"P1\r\n";
        

        private static string MacaraAlternativas =
"X0,{0},10,40,{1}\r\n" +
"A50,{2},0,4,1,1,N,\"{3}\"\r\n" +
"B600,{4},0,2C,1,4,50,N,\"{5}\"\r\n";
*/

        //private int i;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="APT"></param>
        /// <param name="VOTrow"></param>
        /// <returns></returns>
        public bool TemCedula(VOTacaoRow VOTrow, int APT)
        {
            if (Alternativas.Count == 0)
                foreach (VOTacaoRow VOTrowX in VOTacao)
                    GeraCedulas(VOTrowX);
            //if (VOTacao.Count == 0)
            //    return false;
            //GeraCedulas();
            foreach (VOtORow votorow in VOTrow.GetVOtORows())
                if (votorow.VOO_APT == APT)
                    return true;
            return false;
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="APT"></param>
        /// <param name="Ultimo"></param>
        public void ImprimeCedula(int APT,bool Ultimo)
        {                       
            foreach (VOtORow votorow in VOtO)
                if (votorow.VOO_APT == APT)
                {
                    ImpVoto.Reset();                    
                    ImpVoto.SetCampo("%PERGUNTA%", VOTacao[0].VOTPergunta);
                    ImpVoto.SetCampo("%DETALHES%", DetAlternativas(votorow));
                    //ImpVoto.SetCampo("%120+100i%", string.Format("{0}",120+i*100));
                    ImpVoto.SetCampo("%IDENT%", votorow.Identificacao);
                    ImpVoto.SetCampo("%CORTE%", TradutorEXA(Ultimo ? "0x1bw" : "0x1bm"));
                    ImpVoto.ImprimeDOC();                    
                }            
        }*/

        /// <summary>
        /// ImprimeCedula
        /// </summary>
        /// <param name="VOTrow"></param>
        /// <param name="APT"></param>
        /// <param name="Ultimo"></param>
        public void ImprimeCedula(VOTacaoRow VOTrow,int APT, bool Ultimo)
        {
            
            foreach (VOtORow votorow in VOTrow.GetVOtORows())
                if (votorow.VOO_APT == APT)
                {
                    ImpVoto.Reset();
                    ImpVoto.SetCampo("%PERGUNTA%", VOTrow.VOTPergunta);
                    ImpVoto.SetCampo("%DETALHES%", DetAlternativas(votorow));
                    //ImpVoto.SetCampo("%120+100i%", string.Format("{0}",120+i*100));
                    ImpVoto.SetCampo("%IDENT%", votorow.Identificacao);
                    ImpVoto.SetCampo("%CORTE%", TradutorEXA(Ultimo ? "0x1bw" : "0x1bm"));
                    ImpVoto.ImprimeDOC();
                }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="VOOrow"></param>
        /// <param name="Ultimo"></param>
        public void ImprimeCedula(VOtORow VOOrow, bool Ultimo)
        {
            VOTacaoRow VOTrow = VOOrow.VOTacaoRow;
            ImpVoto.Reset();
            ImpVoto.SetCampo("%PERGUNTA%", VOTrow.VOTPergunta);
            ImpVoto.SetCampo("%DETALHES%", DetAlternativas(VOOrow));
            //ImpVoto.SetCampo("%120+100i%", string.Format("{0}",120+i*100));
            ImpVoto.SetCampo("%IDENT%", VOOrow.Identificacao);
            ImpVoto.SetCampo("%CORTE%", TradutorEXA(Ultimo ? "0x1bw" : "0x1bm"));
            ImpVoto.ImprimeDOC();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="votorow"></param>
        /// <returns></returns>
        public string DetAlternativas(VOtORow votorow)
        {
            System.Text.StringBuilder retorno = new System.Text.StringBuilder();
            foreach (AlternativasRow rowALT in votorow.GetAlternativasRows())
            {
                retorno.Append(string.Format(MacaraAlternativas,
                                             //110 + 100 * i,
                                             //150 + 100 * i,
                                             //120 + 100 * i,
                                             rowALT.Texto,
                                             //100 + 100 * i,
                                             rowALT.Codigo));
                //i++;
            }
            return retorno.ToString();
        }

        private dVotacaoTableAdapters.VOTacaoTableAdapter vOTacaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: VOTacao
        /// </summary>
        public dVotacaoTableAdapters.VOTacaoTableAdapter VOTacaoTableAdapter
        {
            get
            {
                if (vOTacaoTableAdapter == null)
                {
                    vOTacaoTableAdapter = new dVotacaoTableAdapters.VOTacaoTableAdapter();
                    vOTacaoTableAdapter.TrocarStringDeConexao();
                };
                return vOTacaoTableAdapter;
            }
        }

        private dVotacaoTableAdapters.VOtacaoDetalheTableAdapter vOtacaoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: VOtacaoDetalhe
        /// </summary>
        public dVotacaoTableAdapters.VOtacaoDetalheTableAdapter VOtacaoDetalheTableAdapter
        {
            get
            {
                if (vOtacaoDetalheTableAdapter == null)
                {
                    vOtacaoDetalheTableAdapter = new dVotacaoTableAdapters.VOtacaoDetalheTableAdapter();
                    vOtacaoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return vOtacaoDetalheTableAdapter;
            }
        }

        private dVotacaoTableAdapters.VOtOTableAdapter vOtOTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: VOtO
        /// </summary>
        public dVotacaoTableAdapters.VOtOTableAdapter VOtOTableAdapter
        {
            get
            {
                if (vOtOTableAdapter == null)
                {
                    vOtOTableAdapter = new dVotacaoTableAdapters.VOtOTableAdapter();
                    vOtOTableAdapter.TrocarStringDeConexao();
                };
                return vOtOTableAdapter;
            }
        }

        private dVotacaoTableAdapters.DadosCargaTableAdapter dadosCargaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DadosCarga
        /// </summary>
        public dVotacaoTableAdapters.DadosCargaTableAdapter DadosCargaTableAdapter
        {
            get
            {
                if (dadosCargaTableAdapter == null)
                {
                    dadosCargaTableAdapter = new dVotacaoTableAdapters.DadosCargaTableAdapter();
                    dadosCargaTableAdapter.TrocarStringDeConexao();
                };
                return dadosCargaTableAdapter;
            }
        }

        private dVotacaoTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLetos
        /// </summary>
        public dVotacaoTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (bOLetosTableAdapter == null)
                {
                    bOLetosTableAdapter = new dVotacaoTableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao();
                };
                return bOLetosTableAdapter;
            }
        }

        private dVotacaoTableAdapters.ACOrdosTableAdapter aCOrdosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ACOrdos
        /// </summary>
        public dVotacaoTableAdapters.ACOrdosTableAdapter ACOrdosTableAdapter
        {
            get
            {
                if (aCOrdosTableAdapter == null)
                {
                    aCOrdosTableAdapter = new dVotacaoTableAdapters.ACOrdosTableAdapter();
                    aCOrdosTableAdapter.TrocarStringDeConexao();
                };
                return aCOrdosTableAdapter;
            }
        }

        private int[] cores;
        private int icores;

        /// <summary>
        /// Cores padrão
        /// </summary>
        public int CorPrao
        {
            get 
            {
                if (cores == null)
                {
                    cores = new int[] { -16711936, -65536, -16711681, -256, -65281, -8372224, -32768, -4144897, -8000, -16744448 };
                    icores = -1;
                }
                icores++;
                if (icores >= cores.Length)
                    icores = 0;
                return cores[icores];
            }
        }

        /// <summary>
        /// Completa os campos de busca
        /// </summary>
        public void PopulaLookup()
        {
            foreach (dVotacao.VOtORow rowVOO in VOtO)
            {
                dVotacao.DadosCargaRow rowDados = DadosCarga.FindByAPT(rowVOO.VOO_APT);
                if (rowDados != null)
                {
                    rowVOO.calBloco = rowDados.BLOCodigo;
                    rowVOO.calApartamento = rowDados.APTNumero;                                         
                    rowVOO.calFracao = (rowDados.IsAPTFracaoIdealNull()) ? 0 : rowDados.APTFracaoIdeal;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool blockcalc = true;

        /// <summary>
        /// 
        /// </summary>
        public bool Fracao;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public string NomeCampo(int n)
        {
            if (n == 1)
                return "VOO_VOD";
            else
                return string.Format("VOO_VOD_{0}", n);
        }

        /// <summary>
        /// 
        /// </summary>
        public void CalTotais()
        {
            if (blockcalc)
                return;
            try
            {
                blockcalc = true;
                foreach (dVotacao.VOtacaoDetalheRow rowVODz in VOtacaoDetalhe)
                    rowVODz.Total = 0;
                foreach (dVotacao.VOtORow rowVOO in VOtO)
                {
                    for (int i = 1; i <= nVotos; i++)
                    {
                        //if (!rowVOO.IsVOO_VODNull())

                        if (rowVOO[NomeCampo(i)] != DBNull.Value)
                        {
                            int VOD = (int)rowVOO[NomeCampo(i)];
                            dVotacao.VOtacaoDetalheRow rowVOD = VOtacaoDetalhe.FindByVOD(VOD);
                            if (!Fracao)
                                rowVOD.Total += 1;
                            else
                                rowVOD.Total += rowVOO.calFracao;
                            if (i > 1)
                            {
                                MultiplosRow rowM = Multiplos.FindBynVOO(i, rowVOO.VOO);
                                if (rowM == null)
                                    Multiplos.AddMultiplosRow(i, rowVOO.VOO, VOD);
                                else
                                    rowM.VOD = VOD;
                            }
                        }
                        else
                        {
                            if (i > 1)
                            {
                                MultiplosRow rowM = Multiplos.FindBynVOO(i, rowVOO.VOO);
                                if (rowM != null)
                                    rowM.Delete();
                                
                            }
                        }
                    }
                }
            }
            finally
            {
                blockcalc = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AjustaBuscador()
        {
            foreach (DadosCargaRow row in DadosCarga)
            {
                string strBloco = row.BLOCodigo.Trim();
                if (strBloco.ToUpper() == "SB")
                    strBloco = "";
                while ((strBloco.Length > 0) && (strBloco[0] == '0'))
                    strBloco = strBloco.Substring(1);
                string strNumero = row.APTNumero.Trim();
                while ((strNumero.Length > 0) && (strNumero[0] == '0'))
                    strNumero = strNumero.Substring(1);
                
                row.calcBusca = strBloco + strNumero;
            }
        }
    }
}
