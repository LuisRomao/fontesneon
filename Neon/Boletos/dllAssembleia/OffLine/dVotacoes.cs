﻿using System.IO;
using System.Windows.Forms;

namespace dllAssembleia.OffLine
{    
    /// <summary>
    /// 
    /// </summary>
    public partial class dVotacoes 
    {
        /// <summary>
        /// 
        /// </summary>
        public static System.Collections.Generic.SortedList<int,dVotacao> XMLs;

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="VOT"></param>
        /// <returns></returns>
        public dllAssembleia.cVotacao GeracVotacao(int VOT)
        {
            dllAssembleia.dVotacao novadVotacao = (dllAssembleia.dVotacao)XMLs[VOT];
            return new dllAssembleia.cVotacao(novadVotacao,VOT);            
        }
*/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="VOT"></param>
        /// <returns></returns>
        public dllAssembleia.dVotacao GetdVotacao(int VOT)
        {            
            return (dllAssembleia.dVotacao)XMLs[VOT];
        }

        /// <summary>
        /// 
        /// </summary>
        public void CarregaXMLs()
        {
            XMLs = new System.Collections.Generic.SortedList<int, dVotacao>();
            string DiretorioXML = string.Format("{0}\\VOTACOES", Path.GetDirectoryName(Application.ExecutablePath));
            if (Directory.Exists(DiretorioXML))
            {
                foreach (string nomeArquivos in Directory.GetFiles(DiretorioXML, "*.xml"))
                {
                    try
                    {
                        dllAssembleia.dVotacao dVotacaoX = new dllAssembleia.dVotacao();
                        dVotacaoX.ReadXml(nomeArquivos);
                        foreach (dllAssembleia.dVotacao.VOTacaoRow rowVOT in dVotacaoX.VOTacao)
                        {
                            dllAssembleia.OffLine.dVotacoes.VotacoesRow novarow = Votacoes.NewVotacoesRow();
                            novarow.Arquivo = nomeArquivos;
                            if (!rowVOT.IsCondominioNull())
                                novarow.Condominio = rowVOT.Condominio;
                            novarow.Nome = rowVOT.VOTPergunta;
                            novarow.VOT = rowVOT.VOT;
                            if (!rowVOT.IsVOTStatusNull())
                                novarow.Status = rowVOT.VOTStatus;
                            Votacoes.AddVotacoesRow(novarow);
                            XMLs.Add(rowVOT.VOT, dVotacaoX);
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Erro em " + nomeArquivos);
                    }
                }
            }
        }
    }
}
