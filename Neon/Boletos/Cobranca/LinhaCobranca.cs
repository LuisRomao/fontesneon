﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbstratosNeon;
using CobrancaProc;
using System.Data;
using dllImpresso;
using System.Collections;
using VirEnumeracoesNeon;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraReports.UI;

namespace Cobranca
{
    /// <summary>
    /// 
    /// </summary>
    public class LinhaCobranca:LinhaCobrancaProc
    {
        

        #region Construtores
        /// <summary>
        /// 
        /// </summary>
        /// <param name="LIC"></param>
        public LinhaCobranca(int LIC) : base(LIC)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_apartamento"></param>
        /// <param name="ChecarLICnoBanco"></param>
        public LinhaCobranca(ABS_Apartamento _apartamento, bool ChecarLICnoBanco = true) : base(_apartamento,ChecarLICnoBanco)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linhamae"></param>
        /// <param name="_apartamento"></param>
        public LinhaCobranca(dLinhaCobranca.LInhaCobrancaRow linhamae, ABS_Apartamento _apartamento = null) : base(linhamae, _apartamento)
        {
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public override ABS_Apartamento Apartamento => _Apartamento ?? (_Apartamento = ABS_Apartamento.GetApartamento(LinhaMae.LIC_APT));

        /// <summary>
        /// 
        /// </summary>
        public string UltimoErro = "";

        /*
              ***************************************************************************************************
              * 
              *       Avança deve passar para o proc depois de passar o GeraCarta
              * 
              * *************************************************************************************************
        */

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public AcumulaImpressos Avanca()
        {
            AcumulaImpressos retorno = new AcumulaImpressos();
            CarregaBoletos();
            if (DateTime.Today >= ProximaData)
                switch (StatusCobranca)
                {
                    case APTStatusCobranca.Novo:
                        retorno.Acumula(GeraCarta(ModelosCarta.cartasimples));
                        StatusCobranca = APTStatusCobranca.PrimeiraCarta;
                        break;
                    case APTStatusCobranca.PrimeiraCarta:
                        retorno.Acumula(GeraCarta(ModelosCarta.cobrancaextra));
                        StatusCobranca = APTStatusCobranca.Extra;
                        break;
                    case APTStatusCobranca.Extra:
                        retorno.Acumula(GeraCarta(ModelosCarta.ultimoAviso));
                        retorno.Acumula(GeraCarta(ModelosCarta.procuracao));
                        StatusCobranca = APTStatusCobranca.UltimoAviso;
                        break;
                    case APTStatusCobranca.UltimoAviso:                        
                    case APTStatusCobranca.juridico:
                    case APTStatusCobranca.SemCobranca:
                    case APTStatusCobranca.Terminada:
                        break;
                    default: throw new NotImplementedException(string.Format("CalculaProxRef para {0} não implementado", StatusCobranca));
                }
            return retorno;
        }


        /*
              ***************************************************************************************************
              * 
              *       GeraCarta deve passar para o proc mas para isso tem que checar se o impresso está no proc
              * 
              * *************************************************************************************************
        */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Modelo"></param>
        /// <returns></returns>
        public ImpBase GeraCarta(ModelosCarta Modelo)
        {
            UltimoErro = "";            
            ImpBase Impresso;            
            SortedList<string, DataTable> TabelasImpresso = null;
            switch (Modelo)
            {
                case ModelosCarta.implantacao:
                case ModelosCarta.cartasimples:
                case ModelosCarta.cobrancaextra:
                case ModelosCarta.ultimoAviso:
                    CarregaBoletos();                    
                    Impresso = new ImpRTF();
                    ((ImpRTF)Impresso).AjustaDestinatario(Apartamento);
                    TabelasImpresso = new SortedList<string, DataTable>();
                    TabelasImpresso.Add("Boletos Abertos", DLinhaCobranca.TabelaImpressoBoletos);
                    DLinhaCobranca.TabelaImpressoBoletos.Clear();
                    DLinhaCobranca.TabelaImpressoBoletos.AddTabelaImpressoBoletosRow("Compet.", " ", "Venc.", "Valor Original", "Valor Corrigido", "Multa", "Juros", "Total");
                    decimal TotalO = 0;
                    decimal TotalC = 0;
                    decimal TotalG = 0;
                    foreach (BoletosProc.Boleto.BoletoProc Boleto in Boletos)
                    {                        
                        dLinhaCobranca.TabelaImpressoBoletosRow rowTab = DLinhaCobranca.TabelaImpressoBoletos.NewTabelaImpressoBoletosRow();
                        rowTab.Competencia = Boleto.Competencia.ToString();
                        rowTab.Vencimento = Boleto.VencimentoOriginal.ToString("dd/MM/yyyy");
                        rowTab.ValorOriginal = Boleto.ValorOriginal.ToString("n2");
                        TotalO += Boleto.ValorOriginal;
                        rowTab.ValorCorrigido = Boleto.valorcorrigido.ToString("n2");
                        TotalC += Boleto.valorcorrigido;
                        rowTab.Multa = Boleto.Valormulta.ToString("n2");
                        rowTab.Juros = Boleto.Valorjuros == 0 ? "-" : Boleto.Valorjuros.ToString("n2");
                        rowTab.Total = Boleto.ValorFinal.ToString("n2");
                        TotalG += Boleto.ValorFinal;
                        rowTab.TipoCRAI = Boleto.TipoCRAI;
                        DLinhaCobranca.TabelaImpressoBoletos.AddTabelaImpressoBoletosRow(rowTab);
                    }
                    DLinhaCobranca.TabelaImpressoBoletos.AddTabelaImpressoBoletosRow("TOTAL", "", "", TotalO.ToString("n2"), TotalC.ToString("n2"), "", "", TotalG.ToString("n2"));
                    break;
                case ModelosCarta.procuracao:
                    ImpRTFLogoSimples ImpProc = new ImpRTFLogoSimples("", -1, 0);
                    ImpProc.RemoverLogo();
                    Impresso = ImpProc;
                    break;                    
                default:
                    throw new NotImplementedException(string.Format("Não implementado:{0}", Modelo));
            }
            SortedList SL = new SortedList();
            SL.Add("Nome Bloco", Apartamento.Condominio.CONNomeBloco);
            SL.Add("Nome Unidade", Apartamento.Condominio.CONNomeApto);
            SL.Add("BLOCO", Apartamento.BLOCodigo);
            SL.Add("APT", Apartamento.APTNumero);
            SL.Add("CODCON", Apartamento.Condominio.CONCodigo);
            SL.Add("NONE CONDOMINIO", Apartamento.Condominio.Nome);
            SL.Add("Nome Proprietario", Apartamento.Proprietario.Nome);
            if (Apartamento.Proprietario.CPF.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO )
            {
                SL.Add("CPF", Apartamento.Proprietario.CPF);
                SL.Add("CPF/CNPJ", Apartamento.Proprietario.CPF.Tipo == DocBacarios.TipoCpfCnpj.CPF ? "CPF:" : "CNPJ:");
            }
            else
                SL.Add("CPF/CNPJ", "");
            if (Modelo == ModelosCarta.procuracao)
            {
                SL.Add("CNPJ Cond", Apartamento.Condominio.CNPJ.ToString());
                SL.Add("Endereço Cond", Apartamento.Condominio.Endereco(ModeloEndereco.UmaLinha));
                if (Apartamento.Condominio.Sindico == null)
                {
                    UltimoErro = string.Format("Condomínio sem síndico: {0}\r\n", Apartamento.Condominio.Nome);
                    return null;
                }
                SL.Add("Nome Síndico", Apartamento.Condominio.Sindico.Nome);
                string enderecoSindico = Apartamento.Condominio.Sindico.Endereco(ModeloEndereco.UmaLinha);
                if (enderecoSindico == "")
                {
                    UltimoErro = string.Format("Síndico sem endereço: {0} {1}\r\n", Apartamento.Condominio.CONCodigo, Apartamento.Condominio.Nome);
                    return null;
                    //PARA IMPLEMENTAR
                    //if (Apartamento.Condominio.CorpoDiretivo(Sindico).TipoUnidade != virtual)
                    //    enderecoSindico = Apartamento.Condominio.CorpoDiretivo(Sindico).Endereco(ModeloEndereco.UmaLinha);
                }
                SL.Add("Endereço Síndico", enderecoSindico);
                SL.Add("CPF Síndico", Apartamento.Condominio.Sindico.CPF.ToString());
                if (Apartamento.Condominio.EscritorioAdvocacia == null)
                {
                    UltimoErro = string.Format("Condomínio sem escritório de advocacia cadastrado: {0} {1}\r\n", Apartamento.Condominio.CONCodigo, Apartamento.Condominio.Nome);
                    return null;
                }
                if (!Apartamento.Condominio.EscritorioAdvocacia.TemProcuracaoCadastrada)
                {
                    UltimoErro = string.Format("Procuração não cadastrada para o escritório {0}\r\n", Apartamento.Condominio.EscritorioAdvocacia.FRNNome);
                    return null;
                }
                Impresso.CarregarImpresso(Apartamento.Condominio.EscritorioAdvocacia.ModeloProcuracao, SL);
            }
            else
            {
                Impresso.PreCarga(NomeModelo(Modelo), SL, TabelasImpresso, true);
                Impresso.VirSetLarguras_mm("Boletos Abertos", 20, 10, 25, 21, 21, 16, 21, 24);
                Impresso.VirSetAlinhamentoCel("Boletos Abertos", ParagraphAlignment.Right, ParagraphAlignment.Center, null, null, new List<int> { 3, 4, 5, 6, 7, 8 });
                Impresso.FimCarrega();
            }

            Impresso.CreateDocument();
            return Impresso;

        }
    }
    
}
