﻿namespace Cobranca
{
    partial class cLinhaCobranca
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cLinhaCobranca));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtNomeUnidade = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditI = new DevExpress.XtraEditors.DateEdit();
            this.dateEditF = new DevExpress.XtraEditors.DateEdit();
            this.dateEditP = new DevExpress.XtraEditors.DateEdit();
            this.dateEditB = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditHistorico = new DevExpress.XtraEditors.MemoEdit();
            this.ComboStatusCobranca = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeUnidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditP.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditB.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditHistorico.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboStatusCobranca.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(16, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 16);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Unidade: ";
            // 
            // txtNomeUnidade
            // 
            this.txtNomeUnidade.Location = new System.Drawing.Point(82, 22);
            this.txtNomeUnidade.Name = "txtNomeUnidade";
            this.txtNomeUnidade.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeUnidade.Properties.Appearance.Options.UseFont = true;
            this.txtNomeUnidade.Properties.ReadOnly = true;
            this.txtNomeUnidade.Size = new System.Drawing.Size(656, 30);
            this.txtNomeUnidade.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(16, 67);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(43, 16);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Início: ";
            // 
            // dateEditI
            // 
            this.dateEditI.EditValue = null;
            this.dateEditI.Location = new System.Drawing.Point(82, 58);
            this.dateEditI.Name = "dateEditI";
            this.dateEditI.Properties.Appearance.BackColor = System.Drawing.Color.PapayaWhip;
            this.dateEditI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditI.Properties.Appearance.Options.UseBackColor = true;
            this.dateEditI.Properties.Appearance.Options.UseFont = true;
            this.dateEditI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditI.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditI.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEditI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditI.Properties.ReadOnly = true;
            this.dateEditI.Size = new System.Drawing.Size(234, 30);
            this.dateEditI.TabIndex = 3;
            // 
            // dateEditF
            // 
            this.dateEditF.EditValue = null;
            this.dateEditF.Location = new System.Drawing.Point(504, 58);
            this.dateEditF.Name = "dateEditF";
            this.dateEditF.Properties.Appearance.BackColor = System.Drawing.Color.PapayaWhip;
            this.dateEditF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditF.Properties.Appearance.Options.UseBackColor = true;
            this.dateEditF.Properties.Appearance.Options.UseFont = true;
            this.dateEditF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditF.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditF.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEditF.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditF.Properties.ReadOnly = true;
            this.dateEditF.Size = new System.Drawing.Size(234, 30);
            this.dateEditF.TabIndex = 4;
            // 
            // dateEditP
            // 
            this.dateEditP.EditValue = null;
            this.dateEditP.Location = new System.Drawing.Point(82, 94);
            this.dateEditP.Name = "dateEditP";
            this.dateEditP.Properties.Appearance.BackColor = System.Drawing.Color.PapayaWhip;
            this.dateEditP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditP.Properties.Appearance.Options.UseBackColor = true;
            this.dateEditP.Properties.Appearance.Options.UseFont = true;
            this.dateEditP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditP.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditP.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEditP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditP.Properties.ReadOnly = true;
            this.dateEditP.Size = new System.Drawing.Size(234, 30);
            this.dateEditP.TabIndex = 5;
            // 
            // dateEditB
            // 
            this.dateEditB.EditValue = null;
            this.dateEditB.Location = new System.Drawing.Point(504, 94);
            this.dateEditB.Name = "dateEditB";
            this.dateEditB.Properties.Appearance.BackColor = System.Drawing.Color.PapayaWhip;
            this.dateEditB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditB.Properties.Appearance.Options.UseBackColor = true;
            this.dateEditB.Properties.Appearance.Options.UseFont = true;
            this.dateEditB.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditB.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditB.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEditB.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditB.Properties.ReadOnly = true;
            this.dateEditB.Size = new System.Drawing.Size(234, 30);
            this.dateEditB.TabIndex = 6;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(435, 67);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(59, 16);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "término: ";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(16, 103);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 16);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Próximo: ";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(435, 103);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(63, 16);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Bloqueio: ";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(16, 141);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(48, 16);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Status:";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(16, 181);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(62, 16);
            this.labelControl7.TabIndex = 11;
            this.labelControl7.Text = "Histórico:";
            // 
            // memoEditHistorico
            // 
            this.memoEditHistorico.Location = new System.Drawing.Point(84, 168);
            this.memoEditHistorico.Name = "memoEditHistorico";
            this.memoEditHistorico.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEditHistorico.Properties.Appearance.Options.UseFont = true;
            this.memoEditHistorico.Properties.ReadOnly = true;
            this.memoEditHistorico.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.memoEditHistorico.Properties.WordWrap = false;
            this.memoEditHistorico.Size = new System.Drawing.Size(687, 457);
            this.memoEditHistorico.TabIndex = 13;
            // 
            // ComboStatusCobranca
            // 
            this.ComboStatusCobranca.Location = new System.Drawing.Point(82, 130);
            this.ComboStatusCobranca.Name = "ComboStatusCobranca";
            this.ComboStatusCobranca.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ComboStatusCobranca.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboStatusCobranca.Properties.Appearance.Options.UseBackColor = true;
            this.ComboStatusCobranca.Properties.Appearance.Options.UseFont = true;
            this.ComboStatusCobranca.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboStatusCobranca.Properties.ReadOnly = true;
            this.ComboStatusCobranca.Size = new System.Drawing.Size(280, 32);
            this.ComboStatusCobranca.TabIndex = 14;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(602, 133);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(136, 29);
            this.simpleButton1.TabIndex = 15;
            this.simpleButton1.Text = "incluir observação";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cLinhaCobranca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.ComboStatusCobranca);
            this.Controls.Add(this.memoEditHistorico);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.dateEditB);
            this.Controls.Add(this.dateEditP);
            this.Controls.Add(this.dateEditF);
            this.Controls.Add(this.dateEditI);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtNomeUnidade);
            this.Controls.Add(this.labelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cLinhaCobranca";
            this.Size = new System.Drawing.Size(791, 640);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeUnidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditP.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditB.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditHistorico.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboStatusCobranca.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtNomeUnidade;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateEditI;
        private DevExpress.XtraEditors.DateEdit dateEditF;
        private DevExpress.XtraEditors.DateEdit dateEditP;
        private DevExpress.XtraEditors.DateEdit dateEditB;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEditHistorico;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboStatusCobranca;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
