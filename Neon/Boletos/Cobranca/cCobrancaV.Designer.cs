﻿namespace Cobranca
{
    partial class cCobrancaV
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCobrancaV));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLICDataI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLICDataRev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLICStatusCobranca = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colLIC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLICDataBloc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repButtonEditLIC = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dLinhaCobrancaBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTStatusCobranca = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESCpfCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProxLICDataRev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnJuridico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repButtonEditJuridico = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.aPARTAMENTOSBindingSource = new System.Windows.Forms.BindingSource();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.SBRemove = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.BotCarta1 = new VirBotaoNeon.cBotaoImpNeon();
            this.BotCarta2 = new VirBotaoNeon.cBotaoImpNeon();
            this.BotCarta3 = new VirBotaoNeon.cBotaoImpNeon();
            this.cBotaoImpNeon1 = new VirBotaoNeon.cBotaoImpNeon();
            this.BotProcuracao = new VirBotaoNeon.cBotaoImpNeon();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCON1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONRevisaoCob = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONMalote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCONDiaVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.chTodos = new DevExpress.XtraEditors.CheckEdit();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repButtonEditLIC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dLinhaCobrancaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repButtonEditJuridico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPARTAMENTOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chTodos.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLICDataI,
            this.colLICDataRev,
            this.colLICStatusCobranca,
            this.colLIC,
            this.colLICDataBloc,
            this.gridColumn1});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView2_RowUpdated);
            // 
            // colLICDataI
            // 
            this.colLICDataI.Caption = "Data Início";
            this.colLICDataI.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colLICDataI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colLICDataI.FieldName = "LICDataI";
            this.colLICDataI.Name = "colLICDataI";
            this.colLICDataI.OptionsColumn.ReadOnly = true;
            this.colLICDataI.Visible = true;
            this.colLICDataI.VisibleIndex = 0;
            this.colLICDataI.Width = 113;
            // 
            // colLICDataRev
            // 
            this.colLICDataRev.Caption = "Proxima Ação";
            this.colLICDataRev.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colLICDataRev.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colLICDataRev.FieldName = "LICDataRev";
            this.colLICDataRev.Name = "colLICDataRev";
            this.colLICDataRev.OptionsColumn.ReadOnly = true;
            this.colLICDataRev.Visible = true;
            this.colLICDataRev.VisibleIndex = 1;
            this.colLICDataRev.Width = 111;
            // 
            // colLICStatusCobranca
            // 
            this.colLICStatusCobranca.Caption = "Status";
            this.colLICStatusCobranca.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colLICStatusCobranca.FieldName = "LICStatusCobranca";
            this.colLICStatusCobranca.Name = "colLICStatusCobranca";
            this.colLICStatusCobranca.Visible = true;
            this.colLICStatusCobranca.VisibleIndex = 3;
            this.colLICStatusCobranca.Width = 121;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colLIC
            // 
            this.colLIC.Caption = "LIC";
            this.colLIC.FieldName = "LIC";
            this.colLIC.Name = "colLIC";
            this.colLIC.OptionsColumn.ReadOnly = true;
            // 
            // colLICDataBloc
            // 
            this.colLICDataBloc.Caption = "Bloqueio";
            this.colLICDataBloc.FieldName = "LICDataBloc";
            this.colLICDataBloc.Name = "colLICDataBloc";
            this.colLICDataBloc.Visible = true;
            this.colLICDataBloc.VisibleIndex = 2;
            this.colLICDataBloc.Width = 95;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repButtonEditLIC;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 4;
            this.gridColumn1.Width = 30;
            // 
            // repButtonEditLIC
            // 
            this.repButtonEditLIC.AutoHeight = false;
            this.repButtonEditLIC.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repButtonEditLIC.Name = "repButtonEditLIC";
            this.repButtonEditLIC.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repButtonEditLIC.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repButtonEditLIC_ButtonClick);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "APARTAMENTOS";
            this.gridControl1.DataSource = this.dLinhaCobrancaBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.LevelTemplate = this.gridView2;
            gridLevelNode2.RelationName = "FK_LInhaCobranca_APARTAMENTOS";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repButtonEditLIC,
            this.repButtonEditJuridico});
            this.gridControl1.Size = new System.Drawing.Size(979, 653);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // dLinhaCobrancaBindingSource
            // 
            this.dLinhaCobrancaBindingSource.DataSource = typeof(CobrancaProc.dLinhaCobranca);
            this.dLinhaCobrancaBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPT,
            this.colAPTNumero,
            this.colAPTStatusCobranca,
            this.colCON,
            this.colCONCodigo,
            this.colCONNome,
            this.colBLO,
            this.colBLOCodigo,
            this.colPESCpfCnpj,
            this.colPESNome,
            this.colProxLICDataRev,
            this.gridColumnJuridico});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "APT", this.colAPTNumero, "{0:n0}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupedColumns = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PaintStyleName = "Flat";
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCodigo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBLOCodigo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAPTNumero, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            // 
            // colAPT
            // 
            this.colAPT.FieldName = "APT";
            this.colAPT.Name = "colAPT";
            this.colAPT.OptionsColumn.ReadOnly = true;
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "Unidade";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.OptionsColumn.ReadOnly = true;
            this.colAPTNumero.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "APTNumero", "{0:n0}")});
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 4;
            this.colAPTNumero.Width = 74;
            // 
            // colAPTStatusCobranca
            // 
            this.colAPTStatusCobranca.Caption = "Status Cobrança";
            this.colAPTStatusCobranca.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colAPTStatusCobranca.FieldName = "APTStatusCobranca";
            this.colAPTStatusCobranca.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colAPTStatusCobranca.Name = "colAPTStatusCobranca";
            this.colAPTStatusCobranca.OptionsColumn.ReadOnly = true;
            this.colAPTStatusCobranca.Visible = true;
            this.colAPTStatusCobranca.VisibleIndex = 8;
            this.colAPTStatusCobranca.Width = 139;
            // 
            // colCON
            // 
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 1;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Condomínio";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colCONNome.OptionsColumn.ReadOnly = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 2;
            this.colCONNome.Width = 239;
            // 
            // colBLO
            // 
            this.colBLO.FieldName = "BLO";
            this.colBLO.Name = "colBLO";
            this.colBLO.OptionsColumn.ReadOnly = true;
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "Bloco";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colBLOCodigo.OptionsColumn.ReadOnly = true;
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 3;
            this.colBLOCodigo.Width = 59;
            // 
            // colPESCpfCnpj
            // 
            this.colPESCpfCnpj.Caption = "CPF/CNPJ";
            this.colPESCpfCnpj.FieldName = "PESCpfCnpj";
            this.colPESCpfCnpj.Name = "colPESCpfCnpj";
            this.colPESCpfCnpj.OptionsColumn.ReadOnly = true;
            this.colPESCpfCnpj.Visible = true;
            this.colPESCpfCnpj.VisibleIndex = 6;
            this.colPESCpfCnpj.Width = 128;
            // 
            // colPESNome
            // 
            this.colPESNome.Caption = "Proprietário";
            this.colPESNome.FieldName = "PESNome";
            this.colPESNome.Name = "colPESNome";
            this.colPESNome.OptionsColumn.ReadOnly = true;
            this.colPESNome.Visible = true;
            this.colPESNome.VisibleIndex = 5;
            this.colPESNome.Width = 227;
            // 
            // colProxLICDataRev
            // 
            this.colProxLICDataRev.Caption = "Proxima revisão";
            this.colProxLICDataRev.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colProxLICDataRev.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colProxLICDataRev.FieldName = "ProxLICDataRev";
            this.colProxLICDataRev.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colProxLICDataRev.Name = "colProxLICDataRev";
            this.colProxLICDataRev.OptionsColumn.ReadOnly = true;
            this.colProxLICDataRev.Visible = true;
            this.colProxLICDataRev.VisibleIndex = 7;
            this.colProxLICDataRev.Width = 93;
            // 
            // gridColumnJuridico
            // 
            this.gridColumnJuridico.Caption = "gridColumn2";
            this.gridColumnJuridico.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnJuridico.Name = "gridColumnJuridico";
            this.gridColumnJuridico.OptionsColumn.ShowCaption = false;
            this.gridColumnJuridico.Visible = true;
            this.gridColumnJuridico.VisibleIndex = 9;
            this.gridColumnJuridico.Width = 30;
            // 
            // repButtonEditJuridico
            // 
            this.repButtonEditJuridico.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.repButtonEditJuridico.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repButtonEditJuridico.Name = "repButtonEditJuridico";
            this.repButtonEditJuridico.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repButtonEditJuridico.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repButtonEditJuridico_ButtonClick);
            // 
            // aPARTAMENTOSBindingSource
            // 
            this.aPARTAMENTOSBindingSource.DataMember = "APARTAMENTOS";
            this.aPARTAMENTOSBindingSource.DataSource = typeof(CobrancaProc.dLinhaCobranca);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.SBRemove);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(542, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(983, 150);
            this.panelControl1.TabIndex = 0;
            // 
            // SBRemove
            // 
            this.SBRemove.Location = new System.Drawing.Point(881, 72);
            this.SBRemove.Name = "SBRemove";
            this.SBRemove.Size = new System.Drawing.Size(142, 23);
            this.SBRemove.TabIndex = 46;
            this.SBRemove.Text = "Remove";
            this.SBRemove.Visible = false;
            this.SBRemove.Click += new System.EventHandler(this.SBRemove_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.BotCarta1);
            this.groupControl1.Controls.Add(this.BotCarta2);
            this.groupControl1.Controls.Add(this.BotCarta3);
            this.groupControl1.Controls.Add(this.cBotaoImpNeon1);
            this.groupControl1.Controls.Add(this.BotProcuracao);
            this.groupControl1.Location = new System.Drawing.Point(442, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(412, 134);
            this.groupControl1.TabIndex = 44;
            this.groupControl1.Text = "Impressos Avulsos";
            // 
            // BotCarta1
            // 
            this.BotCarta1.AbrirArquivoExportado = false;
            this.BotCarta1.AcaoBotao = dllBotao.Botao.imprimir;
            this.BotCarta1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.BotCarta1.Appearance.Options.UseBackColor = true;
            this.BotCarta1.AssuntoEmail = "Aviso";
            this.BotCarta1.BotaoEmail = true;
            this.BotCarta1.BotaoImprimir = true;
            this.BotCarta1.BotaoPDF = true;
            this.BotCarta1.BotaoTela = true;
            this.BotCarta1.CorpoEmail = "Aviso";
            this.BotCarta1.CreateDocAutomatico = false;
            this.BotCarta1.Impresso = null;
            this.BotCarta1.Location = new System.Drawing.Point(5, 24);
            this.BotCarta1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BotCarta1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BotCarta1.Name = "BotCarta1";
            this.BotCarta1.NaoUsarxlsX = true;
            this.BotCarta1.NomeArquivoAnexo = "Aviso";
            this.BotCarta1.PriComp = null;
            this.BotCarta1.Size = new System.Drawing.Size(196, 27);
            this.BotCarta1.TabIndex = 35;
            this.BotCarta1.Titulo = "Carta Simples";
            this.BotCarta1.clicado += new dllBotao.BotaoEventHandler(this.BotCarta1_clicado);
            // 
            // BotCarta2
            // 
            this.BotCarta2.AbrirArquivoExportado = false;
            this.BotCarta2.AcaoBotao = dllBotao.Botao.imprimir;
            this.BotCarta2.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.BotCarta2.Appearance.Options.UseBackColor = true;
            this.BotCarta2.AssuntoEmail = "Cobrança";
            this.BotCarta2.BotaoEmail = true;
            this.BotCarta2.BotaoImprimir = true;
            this.BotCarta2.BotaoPDF = true;
            this.BotCarta2.BotaoTela = true;
            this.BotCarta2.CorpoEmail = "Cobrança";
            this.BotCarta2.CreateDocAutomatico = false;
            this.BotCarta2.Impresso = null;
            this.BotCarta2.Location = new System.Drawing.Point(5, 59);
            this.BotCarta2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BotCarta2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BotCarta2.Name = "BotCarta2";
            this.BotCarta2.NaoUsarxlsX = true;
            this.BotCarta2.NomeArquivoAnexo = "Cobrança";
            this.BotCarta2.PriComp = null;
            this.BotCarta2.Size = new System.Drawing.Size(196, 27);
            this.BotCarta2.TabIndex = 36;
            this.BotCarta2.Titulo = "Cobrança Extra";
            this.BotCarta2.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImpNeon1_clicado);
            // 
            // BotCarta3
            // 
            this.BotCarta3.AbrirArquivoExportado = false;
            this.BotCarta3.AcaoBotao = dllBotao.Botao.imprimir;
            this.BotCarta3.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.BotCarta3.Appearance.Options.UseBackColor = true;
            this.BotCarta3.AssuntoEmail = "Cobrança";
            this.BotCarta3.BotaoEmail = true;
            this.BotCarta3.BotaoImprimir = true;
            this.BotCarta3.BotaoPDF = true;
            this.BotCarta3.BotaoTela = true;
            this.BotCarta3.CorpoEmail = "Cobrança";
            this.BotCarta3.CreateDocAutomatico = false;
            this.BotCarta3.Impresso = null;
            this.BotCarta3.Location = new System.Drawing.Point(5, 92);
            this.BotCarta3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BotCarta3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BotCarta3.Name = "BotCarta3";
            this.BotCarta3.NaoUsarxlsX = true;
            this.BotCarta3.NomeArquivoAnexo = "Cobrança";
            this.BotCarta3.PriComp = null;
            this.BotCarta3.Size = new System.Drawing.Size(196, 27);
            this.BotCarta3.TabIndex = 37;
            this.BotCarta3.Titulo = "Último Aviso";
            this.BotCarta3.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImpNeon1_clicado_1);
            // 
            // cBotaoImpNeon1
            // 
            this.cBotaoImpNeon1.AbrirArquivoExportado = false;
            this.cBotaoImpNeon1.AcaoBotao = dllBotao.Botao.imprimir;
            this.cBotaoImpNeon1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.cBotaoImpNeon1.Appearance.Options.UseBackColor = true;
            this.cBotaoImpNeon1.AssuntoEmail = "Aviso";
            this.cBotaoImpNeon1.BotaoEmail = true;
            this.cBotaoImpNeon1.BotaoImprimir = true;
            this.cBotaoImpNeon1.BotaoPDF = true;
            this.cBotaoImpNeon1.BotaoTela = true;
            this.cBotaoImpNeon1.CorpoEmail = "Aviso";
            this.cBotaoImpNeon1.CreateDocAutomatico = false;
            this.cBotaoImpNeon1.Impresso = null;
            this.cBotaoImpNeon1.Location = new System.Drawing.Point(207, 59);
            this.cBotaoImpNeon1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoImpNeon1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpNeon1.Name = "cBotaoImpNeon1";
            this.cBotaoImpNeon1.NaoUsarxlsX = true;
            this.cBotaoImpNeon1.NomeArquivoAnexo = "Aviso";
            this.cBotaoImpNeon1.PriComp = null;
            this.cBotaoImpNeon1.Size = new System.Drawing.Size(196, 27);
            this.cBotaoImpNeon1.TabIndex = 39;
            this.cBotaoImpNeon1.Titulo = "Implantação";
            this.cBotaoImpNeon1.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImpNeon1_clicado_2);
            this.cBotaoImpNeon1.Load += new System.EventHandler(this.cBotaoImpNeon1_Load);
            // 
            // BotProcuracao
            // 
            this.BotProcuracao.AbrirArquivoExportado = false;
            this.BotProcuracao.AcaoBotao = dllBotao.Botao.imprimir;
            this.BotProcuracao.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.BotProcuracao.Appearance.Options.UseBackColor = true;
            this.BotProcuracao.AssuntoEmail = "Procuração";
            this.BotProcuracao.BotaoEmail = true;
            this.BotProcuracao.BotaoImprimir = true;
            this.BotProcuracao.BotaoPDF = true;
            this.BotProcuracao.BotaoTela = true;
            this.BotProcuracao.CorpoEmail = "Procuração";
            this.BotProcuracao.CreateDocAutomatico = false;
            this.BotProcuracao.Impresso = null;
            this.BotProcuracao.Location = new System.Drawing.Point(207, 24);
            this.BotProcuracao.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BotProcuracao.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BotProcuracao.Name = "BotProcuracao";
            this.BotProcuracao.NaoUsarxlsX = true;
            this.BotProcuracao.NomeArquivoAnexo = "Procuração";
            this.BotProcuracao.PriComp = null;
            this.BotProcuracao.Size = new System.Drawing.Size(196, 27);
            this.BotProcuracao.TabIndex = 38;
            this.BotProcuracao.Titulo = "Procuração";
            this.BotProcuracao.clicado += new dllBotao.BotaoEventHandler(this.BotProcuracao_clicado);
            // 
            // simpleButton5
            // 
            this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.Location = new System.Drawing.Point(195, 6);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(230, 55);
            this.simpleButton5.TabIndex = 43;
            this.simpleButton5.Text = "Efetuar Cobrança Condomíio";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(195, 83);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(230, 57);
            this.simpleButton2.TabIndex = 40;
            this.simpleButton2.Text = "Efetuar Cobrança";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(6, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(183, 134);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Carregar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(542, 150);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(983, 657);
            this.panelControl2.TabIndex = 1;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.gridControl2);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(542, 807);
            this.panelControl3.TabIndex = 1;
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "CONDOMINIOS";
            this.gridControl2.DataSource = this.dLinhaCobrancaBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(2, 47);
            this.gridControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox2});
            this.gridControl2.ShowOnlyPredefinedDetails = true;
            this.gridControl2.Size = new System.Drawing.Size(538, 758);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView3.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.Empty.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView3.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.gridView3.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView3.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.gridView3.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.gridView3.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView3.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.gridView3.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView3.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView3.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView3.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView3.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView3.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView3.Appearance.Preview.Options.UseFont = true;
            this.gridView3.Appearance.Preview.Options.UseForeColor = true;
            this.gridView3.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.Row.Options.UseBackColor = true;
            this.gridView3.Appearance.Row.Options.UseForeColor = true;
            this.gridView3.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView3.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView3.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView3.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCON1,
            this.colCONCodigo1,
            this.colCONNome1,
            this.colCONRevisaoCob,
            this.colCONMalote,
            this.colCONDiaVencimento});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.Editable = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.EnableAppearanceOddRow = true;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.PaintStyleName = "Flat";
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONRevisaoCob, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView3_RowCellStyle);
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView3_FocusedRowChanged);
            // 
            // colCON1
            // 
            this.colCON1.FieldName = "CON";
            this.colCON1.Name = "colCON1";
            this.colCON1.Width = 90;
            // 
            // colCONCodigo1
            // 
            this.colCONCodigo1.Caption = "Código";
            this.colCONCodigo1.FieldName = "CONCodigo";
            this.colCONCodigo1.Name = "colCONCodigo1";
            this.colCONCodigo1.Visible = true;
            this.colCONCodigo1.VisibleIndex = 0;
            this.colCONCodigo1.Width = 63;
            // 
            // colCONNome1
            // 
            this.colCONNome1.Caption = "Nome";
            this.colCONNome1.FieldName = "CONNome";
            this.colCONNome1.Name = "colCONNome1";
            this.colCONNome1.Visible = true;
            this.colCONNome1.VisibleIndex = 1;
            this.colCONNome1.Width = 210;
            // 
            // colCONRevisaoCob
            // 
            this.colCONRevisaoCob.Caption = "Próx. Rev.";
            this.colCONRevisaoCob.FieldName = "CONRevisaoCob";
            this.colCONRevisaoCob.Name = "colCONRevisaoCob";
            this.colCONRevisaoCob.Visible = true;
            this.colCONRevisaoCob.VisibleIndex = 2;
            this.colCONRevisaoCob.Width = 84;
            // 
            // colCONMalote
            // 
            this.colCONMalote.Caption = "Malote";
            this.colCONMalote.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colCONMalote.FieldName = "CONMalote";
            this.colCONMalote.Name = "colCONMalote";
            this.colCONMalote.Visible = true;
            this.colCONMalote.VisibleIndex = 3;
            this.colCONMalote.Width = 78;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colCONDiaVencimento
            // 
            this.colCONDiaVencimento.Caption = "Venc.";
            this.colCONDiaVencimento.FieldName = "CONDiaVencimento";
            this.colCONDiaVencimento.Name = "colCONDiaVencimento";
            this.colCONDiaVencimento.Visible = true;
            this.colCONDiaVencimento.VisibleIndex = 4;
            this.colCONDiaVencimento.Width = 53;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.chTodos);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(538, 45);
            this.panelControl4.TabIndex = 0;
            // 
            // chTodos
            // 
            this.chTodos.Location = new System.Drawing.Point(6, 11);
            this.chTodos.Name = "chTodos";
            this.chTodos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chTodos.Properties.Appearance.Options.UseFont = true;
            this.chTodos.Properties.Caption = "Todos os condomínios";
            this.chTodos.Size = new System.Drawing.Size(172, 22);
            this.chTodos.TabIndex = 0;
            this.chTodos.CheckedChanged += new System.EventHandler(this.chTodos_CheckedChanged);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(542, 150);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 657);
            this.splitterControl1.TabIndex = 2;
            this.splitterControl1.TabStop = false;
            // 
            // cCobrancaV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl3);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCobrancaV";
            this.Size = new System.Drawing.Size(1525, 807);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repButtonEditLIC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dLinhaCobrancaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repButtonEditJuridico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPARTAMENTOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chTodos.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource aPARTAMENTOSBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPT;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTStatusCobranca;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colBLO;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private VirBotaoNeon.cBotaoImpNeon BotCarta1;
        private VirBotaoNeon.cBotaoImpNeon BotCarta2;
        private DevExpress.XtraGrid.Columns.GridColumn colPESCpfCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colPESNome;
        private VirBotaoNeon.cBotaoImpNeon BotProcuracao;
        private VirBotaoNeon.cBotaoImpNeon BotCarta3;
        private VirBotaoNeon.cBotaoImpNeon cBotaoImpNeon1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colLICDataI;
        private DevExpress.XtraGrid.Columns.GridColumn colLICDataRev;
        private DevExpress.XtraGrid.Columns.GridColumn colLICStatusCobranca;
        private DevExpress.XtraGrid.Columns.GridColumn colProxLICDataRev;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.CheckEdit chTodos;
        private System.Windows.Forms.BindingSource dLinhaCobrancaBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCON1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONRevisaoCob;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colLIC;
        private DevExpress.XtraGrid.Columns.GridColumn colLICDataBloc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repButtonEditLIC;
        private DevExpress.XtraGrid.Columns.GridColumn colCONMalote;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDiaVencimento;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraEditors.SimpleButton SBRemove;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnJuridico;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repButtonEditJuridico;
    }
}
