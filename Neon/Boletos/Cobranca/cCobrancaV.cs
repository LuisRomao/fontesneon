﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using Framework;
using dllImpresso;
using System.Collections;
using AbstratosNeon;
using DevExpress.XtraRichEdit.API.Native;
using VirEnumeracoesNeon;
using CompontesBasicosProc;
using CobrancaProc;



namespace Cobranca
{
    /// <summary>
    /// Classe para cobrança
    /// </summary>
    public partial class cCobrancaV : ComponenteBase
    {
        private int? CONSel
        {
            get
            {
                if (chTodos.Checked)
                    return null;
                else
                {
                    dLinhaCobranca.CONDOMINIOSRow rowSel = (dLinhaCobranca.CONDOMINIOSRow)gridView3.GetFocusedDataRow();
                    if (rowSel == null)
                        return null;
                    else
                        return rowSel.CON;
                }
            }
        }

        private dLinhaCobranca DLinhaCobranca { get => CobrancaProc1.DLinhaCobranca; }

        private CobrancaProc.CobrancaProc CobrancaProc1;

        /// <summary>
        /// Construtor
        /// </summary>
        public cCobrancaV()
        {
            InitializeComponent();
            CobrancaProc1 = new CobrancaProc.CobrancaProc();
            CobrancaProc1.IncluirNovos();
            //aPARTAMENTOSBindingSource.DataSource = DLinhaCobranca;
            dLinhaCobrancaBindingSource.DataSource = DLinhaCobranca;
            Enumeracoes.virEnumAPTStatusCobranca.virEnumAPTStatusCobrancaSt.CarregaEditorDaGrid(colAPTStatusCobranca);
            Enumeracoes.VirEnumTiposMalote.CarregaEditorDaGrid(colCONMalote);
            DLinhaCobranca.CONDOMINIOSTableAdapter.Fill(DLinhaCobranca.CONDOMINIOS);
            if (DSCentral.USU == 30)
                SBRemove.Visible = true;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {                                                
            CarregaDados();            
        }

        private SortedList<int, ABS_Apartamento> _PoolApartamentos;

        private SortedList<int, ABS_Apartamento> PoolApartamentos => _PoolApartamentos ?? (_PoolApartamentos = new SortedList<int, ABS_Apartamento>());

        private ABS_Apartamento GetApartamento(int APT)
        {
            if (!PoolApartamentos.ContainsKey(APT))
                if (CobrancaProc1.dBoletos.Apartamentos.ContainsKey(APT))
                    PoolApartamentos.Add(APT, CobrancaProc1.dBoletos.Apartamentos[APT]);
                else
                    PoolApartamentos.Add(APT, ABS_Apartamento.GetApartamento(APT));
            return PoolApartamentos[APT];
        }

        private void GuardaApartamento(ABS_Apartamento apartamento)
        {            
            if (!PoolApartamentos.ContainsKey(apartamento.APT))
                PoolApartamentos.Add(apartamento.APT, apartamento);
        }

        private SortedList<int, ABS_Condominio> _PoolCondominos;

        private SortedList<int, ABS_Condominio> PoolCondominos => _PoolCondominos ?? (_PoolCondominos = new SortedList<int, ABS_Condominio>());
                

        private void PreCargaPorCondominio(int CON)
        {
            if (PoolCondominos.ContainsKey(CON))
                return;
            PoolCondominos.Add(CON, ABS_Condominio.GetCondominio(CON));
            foreach (ABS_Apartamento apartamento in PoolCondominos[CON].Apartamentos.Values)
                GuardaApartamento(apartamento);
        }

        private void CarregaDados()
        {
            //CompontesBasicos.Performance.Performance.Ativado = true;
            //CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("geral");                        
            try
            {
                gridView1.BeginDataUpdate();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Precar Boletos");
                PrecargaBoletos();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Prepara Tela");
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Carregando Dados");
                    if(CONSel.HasValue)
                    {
                        DLinhaCobranca.APARTAMENTOSTableAdapter.FillByCONCompleto(DLinhaCobranca.APARTAMENTOS,CONSel.Value);
                        DLinhaCobranca.LInhaCobrancaTableAdapter.FillByCONAbertas(DLinhaCobranca.LInhaCobranca, CONSel.Value);
                    }
                    else
                    {
                        DLinhaCobranca.APARTAMENTOSTableAdapter.FillCompleto(DLinhaCobranca.APARTAMENTOS);
                        DLinhaCobranca.LInhaCobrancaTableAdapter.FillByAbertas(DLinhaCobranca.LInhaCobranca);
                    }
                    ESP.AtivaGauge(DLinhaCobranca.APARTAMENTOS.Count);
                    int contador = 0;
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Campos Calculados");
                    foreach (dLinhaCobranca.APARTAMENTOSRow row in DLinhaCobranca.APARTAMENTOS)
                    {
                        contador++;
                        ESP.GaugeTempo(contador);
                        if (ESP.Abortar)
                        {
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CANCELADO");
                            MessageBox.Show("Cancelado pelo usuário");
                            DLinhaCobranca.Clear();
                            DLinhaCobranca.Clear();
                            break;
                        }
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Pega Apartamento");
                        ABS_Apartamento Apartamento = GetApartamento(row.APT);
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Cria LinhaCobranca1");
                        LinhaCobranca LinhaCobranca1;
                        dLinhaCobranca.LInhaCobrancaRow[] rowLICs = row.GetLInhaCobrancaRows();
                        LinhaCobranca1 = (rowLICs.Length > 0) ? new LinhaCobranca(rowLICs[0], Apartamento): LinhaCobranca1 = new LinhaCobranca(Apartamento, false);                                                                                
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Carrega Boletos na linha inicio");
                        if (PrecargaFeita)
                        {
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Carrega Boletos COM FILTRO");
                            DVBoletos.RowFilter = string.Format("BOL_APT = {0}", Apartamento.APT);
                            DVBoletos.Sort = "BOLVencto";
                            foreach (DataRowView DRV in DVBoletos)
                            {
                                BoletosProc.Boleto.dBoletos.BOLetosRow rowBOL = (BoletosProc.Boleto.dBoletos.BOLetosRow)DRV.Row;
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Carrega Boletos na linha ADD", true);
                                LinhaCobranca1.Boletos.Add(CobrancaProc1.dBoletos.Boletos[rowBOL.BOL]);
                                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                            }
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral");
                            LinhaCobranca1.BoletosCarregados = true;
                        }
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Calcula Próxima Data");
                        row.APTStatusCobranca = (int)LinhaCobranca1.Reavalia();
                        row.ProxLICDataRev = LinhaCobranca1.ProximaData;
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("loop 1");
                    }
                }
            }
            finally
            {
                gridView1.EndDataUpdate();
            }
            //CompontesBasicos.Performance.Performance.PerformanceST.Relatorio(true, true);
        }

        private DataView DVBoletos;

        /*
        private void GeraCarta(string Modelo)
        {
            StringBuilder SB_ERROS = new StringBuilder();
            if ((Modelo != "Cob Proc") &&(DVBoletos == null))
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Carregando boletos");
                    DVBoletos = new DataView(CobrancaProc1.dBoletos.BOLetos);
                    CobrancaProc1.CarregarCobranca();
                };
            }
            ImpressoTotal = null;
            ImpBase Impresso;
            foreach (int i in gridView1.GetSelectedRows())
            {
                if (i < 0)
                    continue;
                dCobrancaV.APARTAMENTOSRow row = (dCobrancaV.APARTAMENTOSRow)gridView1.GetDataRow(i);
                ABS_Apartamento Apartamento = ABS_Apartamento.GetApartamento(row.APT);
                SortedList<string,DataTable> TabelasImpresso = null;                                
                if (Modelo != "Cob Proc")
                {
                    DVBoletos.RowFilter = string.Format("BOL_APT = {0}", Apartamento.APT);
                    DVBoletos.Sort = "BOLVencto";
                    
                    Impresso = new ImpRTF();
                    ((ImpRTF)Impresso).AjustaDestinatario(Apartamento);
                    TabelasImpresso = new SortedList<string, DataTable>();
                    TabelasImpresso.Add("Boletos Abertos", dCobrancaV1.TabelaImpressoBoletos);
                    dCobrancaV1.TabelaImpressoBoletos.Clear();                    
                    dCobrancaV1.TabelaImpressoBoletos.AddTabelaImpressoBoletosRow("Compet."," ", "Venc.", "Valor Original", "Valor Corrigido", "Multa", "Juros", "Total");
                    decimal TotalO = 0;
                    decimal TotalC = 0;
                    decimal TotalG = 0;
                    foreach (DataRowView DRV in DVBoletos)
                    {
                        BoletosProc.Boleto.dBoletos.BOLetosRow rowBOL = (BoletosProc.Boleto.dBoletos.BOLetosRow)DRV.Row;
                        dCobrancaV.TabelaImpressoBoletosRow rowTab = dCobrancaV1.TabelaImpressoBoletos.NewTabelaImpressoBoletosRow();
                        rowTab.Competencia = string.Format("{0:00}/{1:0000}", rowBOL.BOLCompetenciaMes, rowBOL.BOLCompetenciaAno);
                        rowTab.Vencimento = rowBOL.BOLVencto.ToString("dd/MM/yyyy");
                        rowTab.ValorOriginal = rowBOL.BOLValorPrevisto.ToString("n2");
                        TotalO += rowBOL.BOLValorPrevisto;
                        rowTab.ValorCorrigido = rowBOL.ValorCorrigido.ToString("n2");
                        TotalC += rowBOL.ValorCorrigido;
                        rowTab.Multa = rowBOL.Multa.ToString("n2");
                        rowTab.Juros = rowBOL.IsJurosNull() ? "-" :  rowBOL.Juros.ToString("n2");
                        rowTab.Total = rowBOL.SubTotal.ToString("n2");
                        TotalG += rowBOL.SubTotal;
                        rowTab.TipoCRAI = rowBOL.BOLTipoCRAI;
                        dCobrancaV1.TabelaImpressoBoletos.AddTabelaImpressoBoletosRow(rowTab);                        
                    }
                    dCobrancaV1.TabelaImpressoBoletos.AddTabelaImpressoBoletosRow("TOTAL", "", "", TotalO.ToString("n2"), TotalC.ToString("n2"), "", "", TotalG.ToString("n2"));
                }
                else
                {
                    ImpRTFLogoSimples ImpProc = new ImpRTFLogoSimples("", -1, 0);
                    ImpProc.RemoverLogo();
                    Impresso = ImpProc;                    
                }
                SortedList SL = new SortedList();
                SL.Add("Nome Bloco", Apartamento.Condominio.CONNomeBloco);
                SL.Add("Nome Unidade", Apartamento.Condominio.CONNomeApto);
                SL.Add("BLOCO", row.BLOCodigo);
                SL.Add("APT", row.APTNumero);
                SL.Add("CODCON", row.CONCodigo);
                SL.Add("NONE CONDOMINIO", row.CONNome);
                SL.Add("Nome Proprietario", row.PESNome);
                if (!row.IsPESCpfCnpjNull())
                {
                    SL.Add("CPF", row.PESCpfCnpj);
                    DocBacarios.CPFCNPJ cpf = new DocBacarios.CPFCNPJ(row.PESCpfCnpj);
                    SL.Add("CPF/CNPJ", cpf.Tipo == DocBacarios.TipoCpfCnpj.CPF ? "CPF:":"CNPJ:");
                }
                else
                    SL.Add("CPF/CNPJ", "");
                if (Modelo == "Cob Proc")
                {
                    SL.Add("CNPJ Cond", Apartamento.Condominio.CNPJ.ToString());
                    SL.Add("Endereço Cond", Apartamento.Condominio.Endereco(ModeloEndereco.UmaLinha));
                    if (Apartamento.Condominio.Sindico == null)
                    {
                        SB_ERROS.AppendFormat("Condomínio sem síndico: {0}\r\n", Apartamento.Condominio.Nome);
                        continue;
                    }
                    SL.Add("Nome Síndico", Apartamento.Condominio.Sindico.Nome);
                    string enderecoSindico = Apartamento.Condominio.Sindico.Endereco(ModeloEndereco.UmaLinha);
                    if (enderecoSindico == "")
                    {
                        SB_ERROS.AppendFormat("Síndico sem endereço: {0} {1}\r\n", row.CONCodigo, row.CONNome);
                        continue;
                        //PARA IMPLEMENTAR
                        //if (Apartamento.Condominio.CorpoDiretivo(Sindico).TipoUnidade != virtual)
                        //    enderecoSindico = Apartamento.Condominio.CorpoDiretivo(Sindico).Endereco(ModeloEndereco.UmaLinha);
                    }
                    SL.Add("Endereço Síndico", enderecoSindico);
                    SL.Add("CPF Síndico", Apartamento.Condominio.Sindico.CPF.ToString());
                    if (Apartamento.Condominio.EscritorioAdvocacia == null)
                    {
                        SB_ERROS.AppendFormat("Condomínio sem escritório de advocacia cadastrado: {0} {1}\r\n", row.CONCodigo, row.CONNome);
                        continue;
                    }
                    if (!Apartamento.Condominio.EscritorioAdvocacia.TemProcuracaoCadastrada)
                    {
                        SB_ERROS.AppendFormat("Procuração não cadastrada para o escritório {0}\r\n", Apartamento.Condominio.EscritorioAdvocacia.FRNNome);
                        continue;
                    }
                    Impresso.CarregarImpresso(Apartamento.Condominio.EscritorioAdvocacia.ModeloProcuracao, SL);
                }
                else
                {
                    Impresso.PreCarga(Modelo, SL, TabelasImpresso,true);
                    Impresso.VirSetLarguras_mm("Boletos Abertos", 20,10, 25, 21, 21, 16,21, 24);
                    Impresso.VirSetAlinhamentoCel("Boletos Abertos", ParagraphAlignment.Right, ParagraphAlignment.Center, null, null, new List<int> { 3,4, 5, 6, 7, 8 });
                    Impresso.FimCarrega();
                }
                Impresso.CreateDocument();
                if (ImpressoTotal == null)
                    ImpressoTotal = Impresso;
                else
                    ImpressoTotal.Pages.AddRange(Impresso.Pages);

            }
            if (SB_ERROS.ToString() != "")
                MessageBox.Show(SB_ERROS.ToString(), "ERROS");
        }
        */

        private void cBotaoImpNeon1_clicado_1(object sender, dllBotao.BotaoArgs args)
        {
            //GeraCarta("Cob Último");            
            ((VirBotaoNeon.cBotaoImpNeon)sender).Impresso = Geraimpresso(ModelosCarta.ultimoAviso);
        }

        private void BotProcuracao_clicado(object sender, dllBotao.BotaoArgs args)
        {
            //GeraCarta("Cob Proc");            
            ((VirBotaoNeon.cBotaoImpNeon)sender).Impresso = Geraimpresso(ModelosCarta.procuracao);
        }

        private void cBotaoImpNeon1_clicado_2(object sender, dllBotao.BotaoArgs args)
        {
            //GeraCarta("Cob Imp.");            
            ((VirBotaoNeon.cBotaoImpNeon)sender).Impresso = Geraimpresso(ModelosCarta.implantacao);
        }

        private void BotCarta1_clicado(object sender, dllBotao.BotaoArgs args)
        {
            //GeraCarta("Cob Carta Simples");
            ((VirBotaoNeon.cBotaoImpNeon)sender).Impresso = Geraimpresso(ModelosCarta.cartasimples);
        }

        private void cBotaoImpNeon1_clicado(object sender, dllBotao.BotaoArgs args)
        {
            //GeraCarta("Cob Extra");
            ((VirBotaoNeon.cBotaoImpNeon)sender).Impresso = Geraimpresso(ModelosCarta.cobrancaextra);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool PrecargaFeita = false;

        private void PrecargaBoletos()
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                if (!PrecargaFeita)
                {
                    ESP.Espere("Carregando boletos");
                    DVBoletos = new DataView(CobrancaProc1.dBoletos.BOLetos);
                    CobrancaProc1.CarregarCobranca(CONSel);
                    PrecargaFeita = true;
                }
            };
        }

        private ImpBase Geraimpresso(ModelosCarta Modelo)
        {
            StringBuilder SB_ERROS = new StringBuilder();
            if ((Modelo != ModelosCarta.procuracao) && (DVBoletos == null) && (!PrecargaFeita))            
                PrecargaBoletos();                            
            ImpBase ImpTotal = null;
            //AcumulaImpressos Ac = new AcumulaImpressos();
            foreach (int i in gridView1.GetSelectedRows())
            {
                if (i < 0)
                    continue;
                dLinhaCobranca.APARTAMENTOSRow row = (dLinhaCobranca.APARTAMENTOSRow)gridView1.GetDataRow(i);
                ABS_Apartamento Apartamento = ABS_Apartamento.GetApartamento(row.APT);
                LinhaCobranca LinhaCobranca1 = new LinhaCobranca(Apartamento);
                if ((Modelo != ModelosCarta.procuracao) && (PrecargaFeita))
                {
                    DVBoletos.RowFilter = string.Format("BOL_APT = {0}", Apartamento.APT);
                    DVBoletos.Sort = "BOLVencto";
                    foreach (DataRowView DRV in DVBoletos)
                    {
                        BoletosProc.Boleto.dBoletos.BOLetosRow rowBOL = (BoletosProc.Boleto.dBoletos.BOLetosRow)DRV.Row;
                        LinhaCobranca1.Boletos.Add(CobrancaProc1.dBoletos.Boletos[rowBOL.BOL]);
                        LinhaCobranca1.BoletosCarregados = true;
                    }
                }
                ImpBase Impresso = LinhaCobranca1.GeraCarta(Modelo);

                //Ac.Acumula(Impresso);

                if (Impresso == null)
                    SB_ERROS.AppendLine(LinhaCobranca1.UltimoErro);
                else
                {
                    if (ImpTotal == null)
                        ImpTotal = Impresso;
                    else
                        ImpTotal.Pages.AddRange(Impresso.Pages);
                    LinhaCobranca1.LOGGeralProc.RegistraLog(LLOTipo.Usuario, 0, DateTime.Now, string.Format("Gerada carta avulsa do tipo: {0}",Modelo));                    
                }
            }
            if (SB_ERROS.ToString() != "")
                MessageBox.Show(SB_ERROS.ToString(), "ERROS");

            //Ac.Imprimir();

            return ImpTotal;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            AcumulaImpressos Ac = new AcumulaImpressos();
            foreach (int i in gridView1.GetSelectedRows())
            {
                if (i < 0)
                    continue;
                dLinhaCobranca.APARTAMENTOSRow row = (dLinhaCobranca.APARTAMENTOSRow)gridView1.GetDataRow(i);                
                LinhaCobranca LinhaCobranca1 = new LinhaCobranca(GetApartamento(row.APT));
                Ac.Acumula(LinhaCobranca1.Avanca());
            }
            Ac.Imprimir();
            CarregaDados();
        }

        

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            dLinhaCobranca.APARTAMENTOSRow row = (dLinhaCobranca.APARTAMENTOSRow)gridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if (e.Column == colAPTStatusCobranca)
                Enumeracoes.virEnumAPTStatusCobranca.virEnumAPTStatusCobrancaSt.CellStyle(row.APTStatusCobranca, e);
            else if (e.Column == colProxLICDataRev)
            {
                if (!row.IsProxLICDataRevNull() && (row.ProxLICDataRev < DateTime.Today))
                {
                    e.Appearance.BackColor = Color.LightPink;
                    e.Appearance.BackColor2 = Color.LightPink;
                    e.Appearance.ForeColor = Color.Black;
                }
            }
        }

        

        private void gridView3_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (!chTodos.Checked)            
                LimpaDados();            
        }

        private void chTodos_CheckedChanged(object sender, EventArgs e)
        {
            LimpaDados();
            if (chTodos.Checked)
                CarregaDados();                            
        }

        private void LimpaDados()
        {
            PrecargaFeita = false;
            DLinhaCobranca.LInhaCobranca.Clear();
            DLinhaCobranca.APARTAMENTOS.Clear();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (chTodos.Checked)
                return;
            AcumulaImpressos Ac = new AcumulaImpressos();
            dLinhaCobranca.CONDOMINIOSRow rowCON = (dLinhaCobranca.CONDOMINIOSRow)gridView3.GetFocusedDataRow();
            if (rowCON == null)
                return;
            bool Cancelado = false;
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.Espere("Efetuando cobrança");
                dLinhaCobranca.APARTAMENTOSRow[] rows = rowCON.GetAPARTAMENTOSRows();
                ESP.AtivaGauge(rows.Length);                
                int contador = 0;
                foreach (dLinhaCobranca.APARTAMENTOSRow row in rows)
                {
                    ESP.GaugeTempo(contador++);
                    if (ESP.Abortar)
                    {
                        Cancelado = true;
                        MessageBox.Show("Cancelado pelo usuário");
                        break;
                    }
                    LinhaCobranca LinhaCobranca1 = new LinhaCobranca(GetApartamento(row.APT));
                    Ac.Acumula(LinhaCobranca1.Avanca());
                }
            }
            Ac.Imprimir();
            if (!Cancelado)
            {
                rowCON.CONRevisaoCob = DateTime.Now.AddMonths(1);
                CobrancaProc1.DLinhaCobranca.CONDOMINIOSTableAdapter.Update(rowCON);
            }
            CarregaDados();
        }

        private void repButtonEditLIC_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView GridClicada = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
            dLinhaCobranca.LInhaCobrancaRow rowLIC = (dLinhaCobranca.LInhaCobrancaRow)GridClicada.GetFocusedDataRow();
            cLinhaCobranca cLinhaCobranca1 = new cLinhaCobranca(rowLIC);
            cLinhaCobranca1.ShowEmPopUp();
        }

        private void SBImportacao_Click(object sender, EventArgs e)
        {
            /*
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.Espere("Importando dados...");
                ESP.AtivaGauge(gridView1.GetSelectedRows().Length);
                int contador = 0;
                foreach (int i in gridView1.GetSelectedRows())
                {
                    ESP.GaugeTempo(contador++);
                    if (i < 0)
                        continue;
                    dLinhaCobranca.APARTAMENTOSRow row = (dLinhaCobranca.APARTAMENTOSRow)gridView1.GetDataRow(i);
                    LinhaCobranca LinhaCobranca1 = new LinhaCobranca(GetApartamento(row.APT));
                    //try
                    //{
                        LinhaCobranca1.ImportacaoInicial();
                    //}
                    //catch { }
                }
            }*/
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView GridClicada = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
            dLinhaCobranca.LInhaCobrancaRow rowLIC = (dLinhaCobranca.LInhaCobrancaRow)GridClicada.GetFocusedDataRow();            
            string Justificativa = "";
            if (VirInput.Input.Execute("Motivo da alteração", ref Justificativa, true))
            {
                FrameworkProc.objetosNeon.LOGGeralProc.RegistraLogST(LLOTipo.Usuario, 0, DateTime.Now, "ALTERAÇÃO Manual:\r\n"+Justificativa, LLOxXXX.LIC, rowLIC.LIC);                
                LinhaCobranca LinhaCobranca1 = new LinhaCobranca(rowLIC);
                LinhaCobranca1.StatusCobranca = (APTStatusCobranca)rowLIC.LICStatusCobranca;
                rowLIC.APARTAMENTOSRow.APTStatusCobranca = rowLIC.LICStatusCobranca;
                rowLIC.APARTAMENTOSRow.ProxLICDataRev = rowLIC.LICDataRev;
                //rowLIC.APARTAMENTOSRow.ProxLICDataRev = rowLIC.LICDataBloc;                                
            }
            else
            {
                MessageBox.Show("Alteração Cancelada");
                CarregaDados();
            }
        }

        private void SBRemove_Click(object sender, EventArgs e)
        {
            CobrancaProc1.RemoverPagos();
        }

        private void cBotaoImpNeon1_Load(object sender, EventArgs e)
        {

        }

        private void repButtonEditJuridico_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dLinhaCobranca.APARTAMENTOSRow row = (dLinhaCobranca.APARTAMENTOSRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            if(((APTStatusCobranca)row.APTStatusCobranca).EstaNoGrupo(APTStatusCobranca.Novo, APTStatusCobranca.Terminada))
            {
                string Observacao = "";
                if (VirInput.Input.Execute("Justificativa:", ref Observacao, true) && (Observacao.Trim() != ""))
                {                                        
                    LinhaCobranca NovaLinha = new LinhaCobranca(GetApartamento(row.APT));                    
                    NovaLinha.StatusCobranca = APTStatusCobranca.juridico;
                    NovaLinha.LOGGeralProc.RegistraLog(LLOTipo.Usuario, 0, DateTime.Now, Observacao);
                    CarregaDados();
                }
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if ((e.RowHandle >= 0) && (e.Column == gridColumnJuridico))
            {
                dLinhaCobranca.APARTAMENTOSRow row = (dLinhaCobranca.APARTAMENTOSRow)gridView1.GetDataRow(e.RowHandle);
                if (row == null)
                    return;
                if (row.GetLInhaCobrancaRows().Length == 0)
                    e.RepositoryItem = repButtonEditJuridico;
                else
                    e.RepositoryItem = null;                
            }
        }

        private void gridView3_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dLinhaCobranca.CONDOMINIOSRow rowCON = (dLinhaCobranca.CONDOMINIOSRow)gridView3.GetDataRow(e.RowHandle);
            if (rowCON != null)
            {
                if (!rowCON.IsCONRevisaoCobNull())
                {
                    if (rowCON.CONRevisaoCob < DateTime.Today.AddDays(-10))
                        e.Appearance.BackColor = Color.Red;
                    else if (rowCON.CONRevisaoCob < DateTime.Today)
                        e.Appearance.BackColor = Color.Yellow;
                }
            }
        }
    }
}
