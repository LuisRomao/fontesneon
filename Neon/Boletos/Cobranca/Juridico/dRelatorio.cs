﻿namespace Cobranca.Juridico {


    partial class dRelatorio
    {
        private dRelatorioTableAdapters.HonorariosTableAdapter honorariosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Honorarios
        /// </summary>
        public dRelatorioTableAdapters.HonorariosTableAdapter HonorariosTableAdapter
        {
            get
            {
                if (honorariosTableAdapter == null)
                {
                    honorariosTableAdapter = new dRelatorioTableAdapters.HonorariosTableAdapter();
                    honorariosTableAdapter.TrocarStringDeConexao();
                };
                return honorariosTableAdapter;
            }
        }
    }
}
