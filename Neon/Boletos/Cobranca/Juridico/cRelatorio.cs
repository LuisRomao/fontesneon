using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Cobranca.Juridico
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cRelatorio : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cRelatorio()
        {
            InitializeComponent();
            fORNECEDORESBindingSource.DataSource = Framework.datasets.dFornecedores.GetdFornecedoresST("ADV");
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (lookUpEdit1.EditValue == null)
                return;
            if ((int)lookUpEdit1.EditValue == -1)
                return;
            if ((radioGroup1.SelectedIndex > 0) && ((dateI.DateTime == DateTime.MaxValue) || (dateF.DateTime == DateTime.MaxValue)))
                return;
            switch (radioGroup1.SelectedIndex)
            {
                case 0:
                    dRelatorio.HonorariosTableAdapter.FillByAberto(dRelatorio.Honorarios, (int)lookUpEdit1.EditValue);
                    break;
                case 1:
                    dRelatorio.HonorariosTableAdapter.FillByPago(dRelatorio.Honorarios, (int)lookUpEdit1.EditValue,dateI.DateTime,dateF.DateTime);
                    break;
                case 2:
                    dRelatorio.HonorariosTableAdapter.FillByGeralPeriodo(dRelatorio.Honorarios, (int)lookUpEdit1.EditValue, dateI.DateTime, dateF.DateTime);
                    break;
                default:
                    break;
            }
            
                
            //dRelatorio.HonorariosTableAdapter.Fill(dRelatorio.Honorarios, (int)lookUpEdit1.EditValue);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog SF = new SaveFileDialog();
            SF.DefaultExt = ".xls";
            if (SF.ShowDialog() == DialogResult.OK)
            {
                gridView1.ExportToXls(SF.FileName);
            }
        }
    }
}
