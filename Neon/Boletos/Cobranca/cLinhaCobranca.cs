﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using CobrancaProc;
using Framework;

namespace Cobranca
{
    /// <summary>
    /// Componente visual para linha de cobrança
    /// </summary>
    public partial class cLinhaCobranca : ComponenteBase
    {
        private LinhaCobranca _LinhaCobranca1;

        /// <summary>
        /// 
        /// </summary>
        public LinhaCobranca LinhaCobranca1 => _LinhaCobranca1;        

        private cLinhaCobranca()
        {
            InitializeComponent();
            Enumeracoes.virEnumAPTStatusCobranca.virEnumAPTStatusCobrancaSt.CarregaEditorDaGrid(ComboStatusCobranca);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_linhaCobranca"></param>
        public cLinhaCobranca(LinhaCobranca _linhaCobranca) : this()
        {
            _LinhaCobranca1 = _linhaCobranca;
            AjustaTela();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="LIC"></param>
        public cLinhaCobranca(int LIC):this()
        {
            _LinhaCobranca1 = new LinhaCobranca(LIC);
            AjustaTela();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        public cLinhaCobranca(dLinhaCobranca.LInhaCobrancaRow row) : this()
        {
            _LinhaCobranca1 = new LinhaCobranca(row);
            AjustaTela();
        }

        private void AjustaTela()
        {            
            txtNomeUnidade.Text = LinhaCobranca1.Apartamento.ToString();
            ComboStatusCobranca.EditValue = (int)LinhaCobranca1.StatusCobranca;
            if (LinhaCobranca1.DataInicio.HasValue)
            {
                dateEditI.DateTime = LinhaCobranca1.DataInicio.Value;
                dateEditP.DateTime = LinhaCobranca1.ProximaData;
                if (LinhaCobranca1.DataBloc.HasValue)
                    dateEditB.DateTime = LinhaCobranca1.DataBloc.Value;
                if (LinhaCobranca1.DataFim.HasValue)
                    dateEditF.DateTime = LinhaCobranca1.DataFim.Value;
                memoEditHistorico.Text = LinhaCobranca1.RelatorioLog;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string Observacao = "";
            if (VirInput.Input.Execute("Obs.", ref Observacao, true) && (Observacao.Trim() != ""))
            {
                LinhaCobranca1.LOGGeralProc.RegistraLog(VirEnumeracoesNeon.LLOTipo.Usuario,0,DateTime.Now,Observacao);
                memoEditHistorico.Text = LinhaCobranca1.RelatorioLog;
            }
        }
    }
}
