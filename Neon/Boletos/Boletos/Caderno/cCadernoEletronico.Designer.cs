namespace Boletos.Caderno
{
    partial class cCadernoEletronico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCadernoEletronico));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dCaderno = new Boletos.Caderno.dCaderno();
            this.cADernoEletronicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cADernoEletronicoTableAdapter = new Boletos.Caderno.dCadernoTableAdapters.CADernoEletronicoTableAdapter();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.cADCompetenciaMesSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.cADCompetenciaAnoSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCaderno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADernoEletronicoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADCompetenciaMesSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADCompetenciaAnoSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
           
            // 
            // dCaderno
            // 
            this.dCaderno.DataSetName = "dCaderno";
            this.dCaderno.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cADernoEletronicoBindingSource
            // 
            this.cADernoEletronicoBindingSource.DataMember = "CADernoEletronico";
            this.cADernoEletronicoBindingSource.DataSource = this.dCaderno;
            // 
            // cADernoEletronicoTableAdapter
            // 
            this.cADernoEletronicoTableAdapter.ClearBeforeFill = true;
            this.cADernoEletronicoTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("cADernoEletronicoTableAdapter.GetNovosDados")));
            this.cADernoEletronicoTableAdapter.TabelaDataTable = null;
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cADernoEletronicoBindingSource, "CADTexto", true));
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(0, 123);
            this.memoEdit1.Name = "memoEdit1";
            
            this.memoEdit1.Size = new System.Drawing.Size(990, 152);
            
            this.memoEdit1.TabIndex = 4;
            // 
            // cADCompetenciaMesSpinEdit
            // 
            this.cADCompetenciaMesSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cADernoEletronicoBindingSource, "CADCompetenciaMes", true));
            this.cADCompetenciaMesSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cADCompetenciaMesSpinEdit.Location = new System.Drawing.Point(5, 23);
            this.cADCompetenciaMesSpinEdit.Name = "cADCompetenciaMesSpinEdit";
            this.cADCompetenciaMesSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cADCompetenciaMesSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.cADCompetenciaMesSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cADCompetenciaMesSpinEdit.Properties.ReadOnly = true;
            this.cADCompetenciaMesSpinEdit.Size = new System.Drawing.Size(56, 32);
            this.cADCompetenciaMesSpinEdit.TabIndex = 6;
            // 
            // cADCompetenciaAnoSpinEdit
            // 
            this.cADCompetenciaAnoSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cADernoEletronicoBindingSource, "CADCompetenciaAno", true));
            this.cADCompetenciaAnoSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cADCompetenciaAnoSpinEdit.Location = new System.Drawing.Point(67, 23);
            this.cADCompetenciaAnoSpinEdit.Name = "cADCompetenciaAnoSpinEdit";
            this.cADCompetenciaAnoSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cADCompetenciaAnoSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.cADCompetenciaAnoSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cADCompetenciaAnoSpinEdit.Properties.ReadOnly = true;
            this.cADCompetenciaAnoSpinEdit.Size = new System.Drawing.Size(97, 32);
            this.cADCompetenciaAnoSpinEdit.TabIndex = 8;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.cADernoEletronicoBindingSource;
            this.dataNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataNavigator1.Location = new System.Drawing.Point(4, 54);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(161, 24);
            this.dataNavigator1.TabIndex = 10;
            this.dataNavigator1.Text = "dataNavigator1";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cADCompetenciaMesSpinEdit);
            this.groupControl1.Controls.Add(this.cADCompetenciaAnoSpinEdit);
            this.groupControl1.Controls.Add(this.dataNavigator1);
            this.groupControl1.Location = new System.Drawing.Point(5, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(169, 83);
            this.groupControl1.TabIndex = 11;
            this.groupControl1.Text = "CompetÍncia";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 28);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(990, 95);
            this.panelControl1.TabIndex = 12;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(180, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(115, 83);
            this.simpleButton1.TabIndex = 12;
            this.simpleButton1.Text = "Imprimir";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cCadernoEletronico
            // 
            this.BindingSourcePrincipal = this.cADernoEletronicoBindingSource;
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCadernoEletronico";
            this.Size = new System.Drawing.Size(990, 296);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.memoEdit1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCaderno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADernoEletronicoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADCompetenciaMesSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADCompetenciaAnoSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.SpinEdit cADCompetenciaMesSpinEdit;
        private DevExpress.XtraEditors.SpinEdit cADCompetenciaAnoSpinEdit;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        /// <summary>
        /// 
        /// </summary>
        public dCaderno dCaderno;
        /// <summary>
        /// 
        /// </summary>
        public Boletos.Caderno.dCadernoTableAdapters.CADernoEletronicoTableAdapter cADernoEletronicoTableAdapter;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource cADernoEletronicoBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
