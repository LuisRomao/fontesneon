using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace Boletos.Caderno
{
    /// <summary>
    /// Caderninho
    /// </summary>
    public partial class cCadernoEletronico : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cCadernoEletronico()
        {
            InitializeComponent();
            cADernoEletronicoTableAdapter.TrocarStringDeConexao();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            impCaderno ImpCaderno = new impCaderno();
            object obj = cADernoEletronicoBindingSource.Current;
            DataRowView DRV = (DataRowView)obj;
            dCaderno.CADernoEletronicoRow CADernoEletronicoRow = (dCaderno.CADernoEletronicoRow)DRV.Row;
            //dCaderno.CADernoEletronico.Select("CAD = "+)
            ImpCaderno.bindingSource1.DataSource = CADernoEletronicoRow;
            //ImpCaderno.bindingSource1.Position = cADernoEletronicoBindingSource.Position;
            
            ImpCaderno.ShowPreviewDialog();
        }
    }
}

