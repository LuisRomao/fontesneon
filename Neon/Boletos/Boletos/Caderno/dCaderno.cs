﻿namespace Boletos.Caderno {


    partial class dCaderno
    {
        private dCadernoTableAdapters.CADernoEletronicoTableAdapter cADernoEletronicoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CADernoEletronico
        /// </summary>
        public dCadernoTableAdapters.CADernoEletronicoTableAdapter CADernoEletronicoTableAdapter
        {
            get
            {
                if (cADernoEletronicoTableAdapter == null)
                {
                    cADernoEletronicoTableAdapter = new dCadernoTableAdapters.CADernoEletronicoTableAdapter();
                    cADernoEletronicoTableAdapter.TrocarStringDeConexao();
                };
                return cADernoEletronicoTableAdapter;
            }
        }
    }
}
