using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Boletos
{
    /// <summary>
    /// input competÍncia
    /// </summary>
    public partial class FInputCompetencia : CompontesBasicos.FormBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public FInputCompetencia()
        {
            InitializeComponent();
            cCompet1.Comp.Add(1);// = new Framework.objetosNeon.Competencia(DateTime.Today).Add(1);
            cCompet1.AjustaTela();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

