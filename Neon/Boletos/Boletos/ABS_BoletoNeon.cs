﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abstratos;
using Boletos.Boleto;

namespace Boletos
{
    /// <summary>
    /// Classe abstrata par boleto e supeboleto
    /// </summary>
    public abstract class ABS_BoletoNeon : ABS_Boleto
    {
        /// <summary>
        /// Database usado
        /// </summary>
        protected dBoletos _dBoletos;

        /// <summary>
        /// ds interno
        /// </summary>
        public dBoletos dBoletos
        {
            get
            {
                if (_dBoletos == null)                                    
                    _dBoletos = new dBoletos();                
                return _dBoletos;
            }
            set
            {
                _dBoletos = value;
            }
        }        

        /// <summary>
        /// Informaçoes complementares (Banco conta etc)
        /// </summary>
        protected dBoletos.CONDOMINIOSRow _rowComplementar;

        /// <summary>
        /// Dados complementares
        /// </summary>
        [Obsolete("Usar o objeto Apartamento e Condominio - cuidado na troca")]
        public abstract dBoletos.CONDOMINIOSRow rowComplementar { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Remessa"></param>
        /// <returns></returns>
        public abstract bool Registrar(EDIBoletos.BoletoRemessaBase Remessa);

        /// <summary>
        /// Altera o status de registro do boleto
        /// </summary>
        public abstract void AlteraStatusRegistro(StatusRegistroBoleto intStatusRegistro);

        /// <summary>
        /// Condomínio
        /// </summary>
        public abstract int CON { get; }
        
    }
}
