﻿/*
LH - 09/05/2014       - 13.2.8.47 - GravaJustificativa não implementado
MR - 25/02/2016 11:00 -           - Arquivo de Cobrança Itau (Alterações indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
MR - 15/03/2016 10:00 -           - Ajustes de Versão (Alterações indicadas por *** MRC - INICIO - REMESSA (15/03/2016 10:00) ***)
MR - 21/03/2016 20:00 -           - Código de Empresa para remessa Bradesco diferenciado entre SA e SBC (Alterações indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
*/

using System;
using EDIBoletos;
using FrameworkProc.datasets;
using BoletosProc.Boleto;
using VirEnumeracoes;

namespace Boletos.Super
{
    /// <summary>
    /// Classe para superboletos
    /// </summary>
    public class SuperBoleto : SuperBoletoProc
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_SBO"></param>
        public SuperBoleto(int _SBO):base(_SBO){}        

        /*
        /// <summary>
        /// Cria um super boleto (registra no bando de dados)
        /// </summary>
        /// <param name="Modelo"></param>
        /// <param name="Vencimento"></param>
        public SuperBoleto(Boletos.Boleto.Boleto Modelo, DateTime Vencimento)
        {
            rowSBO = dBoletos.SuperBOleto.NewSuperBOletoRow();
            rowSBO.SBOValorPrevisto = Modelo.rowPrincipal.BOLValorPrevisto;
            //rowSBO.SBOVencto = Modelo.rowPrincipal.BOLVencto;
            rowSBO.SBOVencto = Vencimento;
            rowSBO.SBORegistrar = ComRegistro;
            rowSBO.SBODataI = DateTime.Now;
            dBoletos.SuperBOleto.AddSuperBOletoRow(rowSBO);
            dBoletos.SuperBOletoTableAdapter.Update(rowSBO);
            rowSBO.AcceptChanges();
            Boletos = new List<Boleto.Boleto>();
            Boletos.Add(Modelo);
            addBolentonoSuper(Modelo);
        }
        */

        

        

        
        //private bool Super_CarregaDados(Boletos.Boleto.Boleto Modelo)
        //{
            //para que este objeto possa funcionar como deveria as funções de super deve ser migradas para cá. 
            //No momento a implantação desta abordagem se limita ao registro por isso esta função nao foi ajustada e implantada
          //  return true;
            /*
            rowPrincipal = dBoletos.BOLetos.NewBOLetosRow();
            Encontrado = true; //simula um boleto válido
            Competencia = Modelo.Competencia;
            for (int i = 0; i < rowPrincipal.ItemArray.Length; i++)
                rowPrincipal[i] = Modelo.rowPrincipal[i];
            rowPrincipal.BOLJustificativa = "SUPER BOLETO";
            rowPrincipal.BOLMensagem = "Não receber após o vencimento";
            rowPrincipal.BOL = rowPrincipal.BOLNNAntigo = rowSBO.SBO;
            rowPrincipal.BOLValorPrevisto = 0;
            rowPrincipal.BOLVencto = rowSBO.SBOVencto;
            rowPrincipal.BOLStatus = (int)StatusBoleto.Valido;
            rowPrincipal.BOLTipoCRAI = "I";
            rowPrincipal.BOLCompetenciaAno = 1;
            rowPrincipal.BOLCompetenciaMes = 1;
            if (!PegadadosAPT())
                throw new Exception("Dados do Apartamento não encontrados");
            if (!PegadadosCondominio_APT(rowPrincipal.BOL_APT))
                throw new Exception("Dados do Conomínio não encontrados");
            rowComplementar.CONDigitoAgencia = rowDadosCON.CONDigitoAgencia;
            rowComplementar.CONDigitoConta = rowDadosCON.CONDigitoConta;
            dBoletos.BOLetos.AddBOLetosRow(rowPrincipal);
            boletoCalculado = null;
            IniciaCampos();
            */
        //}


        
        /*
        /// <summary>
        /// Condomínio
        /// </summary>
        public int CON
        {
            get { return Boletos[0].rowPrincipal.BOL_CON; }
        }

        /*
        /// <summary>
        /// Dados complementares
        /// </summary>
        [Obsolete("Usar o objeto Apartamento e Condominio - cuidado na troca")]
        public override dBoletos.CONDOMINIOSRow rowComplementar
        {
            get {
                if ((_rowComplementar == null) && (Encontrado))
                {                    
                    dBoletos.CONDOMINIOSDataTable DT;
                    dBoletos.CONDOMINIOSTableAdapter.EmbarcaEmTransST();
                    DT = dBoletos.CONDOMINIOSTableAdapter.GetDataByCON(CON);                    
                    if (DT.Rows.Count == 1)
                        _rowComplementar = (dBoletos.CONDOMINIOSRow)DT.Rows[0];
                }
                return _rowComplementar;
            }
        }*/


        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        /// <summary>
        /// Dados do condomínio (poderá ser substituido por um objeto)
        /// </summary>
        public dCondominiosAtivos.CONDOMINIOSRow rowCON { get { return dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(CON); } }

        
        //private ABS_Apartamento apartamento;

        /*
        /// <summary>
        /// Apartamento do boleto
        /// </summary>
        public ABS_Apartamento Apartamento
        {
            get
            {
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Buscando APT", true);
                if ((apartamento == null) && (APT.HasValue))
                {
                    if ((dBoletos.Apartamentos != null) && (dBoletos.Apartamentos.ContainsKey(APT.Value)))
                        apartamento = dBoletos.Apartamentos[APT.Value];
                    else
                    {
                        apartamento = ABS_Apartamento.GetApartamento(Boletos[0].rowPrincipal.BOL_APT);
                        if (dBoletos.Apartamentos == null)
                            dBoletos.Apartamentos = new SortedList<int, ABS_Apartamento>();
                        dBoletos.Apartamentos.Add(APT.Value, apartamento);
                    }
                }
                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                return apartamento;
            }
            set
            {
                apartamento = value;
            }
        }*/

        
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        /// <summary>
        /// Registra Boleto
        /// </summary>
        /// <param name="Remessa"></param>
        public bool Registrar(EDIBoletos.BoletoRemessaBase Remessa)
        {
            
            if (rowSBO.SBOVencto < DateTime.Today)
            {
                AlteraStatusRegistro(StatusRegistroBoleto.ComregistroNaoRegistrado);                                
                return false;
            }

            bool SoRegistra = (Remessa != null);

            if (!SoRegistra)
            {                
                if (rowCON.CON_BCO == 237)
                {
                    Remessa = new EDIBoletos.BoletoRemessaBra();
                    Remessa.IniciaRemessa();
                    Remessa.CodEmpresa = Framework.DSCentral.EMP == 1 ? 602270 : 4854082;
                    Remessa.NomeEmpresa = "Neon Imoveis";
                    Remessa.Carteira = "9";
                    Remessa.SequencialRemessa = 1;
                }
                else
                {
                    Remessa = new EDIBoletos.BoletoRemessaItau();
                    Remessa.IniciaRemessa();
                    Remessa.AgenciaMae = int.Parse(rowCON.CONAgencia);
                    Remessa.ContaMae = int.Parse(rowCON.CONConta);
                    Remessa.ContaMaeDg = rowCON.CONDigitoConta;
                    Remessa.NomeEmpresa = rowCON.CONNome;
                    Remessa.Carteira = BoletosFilhos[0].CarteiraBanco.ToString();
                    Remessa.SequencialRemessa = 1;
                }                
            }

            BoletoReg boletoreg = new BoletoReg() { Agencia = rowCON.CONAgencia, 
                                                    Conta = rowCON.CONConta, 
                                                    DigConta = rowCON.CONDigitoConta, 
                                                    CNPJ = new DocBacarios.CPFCNPJ(rowCON.CONCnpj), 
                                                    NomeEmpresa = rowCON.CONNome };            
            try
            {
                boletoreg.Convenio = int.Parse(rowCON.CONCodigoCNR);
            }
            catch { };
            //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***        
            boletoreg.CobrarMulta = true;
            boletoreg.PercMulta = BoletosFilhos[0].rowPrincipal.BOLMulta;
            boletoreg.NossoNumero = rowSBO.SBO;            
            boletoreg.EmissaoBoleto = (int)BoletoRemessaBra.TiposEmissaoBoleto.Cliente;
            //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
            boletoreg.IDOcorrencia = (rowCON.CON_BCO == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.Remessa : (int)BoletoRemessaItau.TiposOcorrencia.Remessa;
            //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
            boletoreg.Vencimento = rowSBO.SBOVencto;
            boletoreg.Valor = rowSBO.SBOValorPrevisto;
            boletoreg.NumDocumento = rowSBO.SBO.ToString();
            //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
            boletoreg.EspecieTitulo = (rowCON.CON_BCO == 237) ? (int)BoletoRemessaBra.TiposEspecieTitulo.Outros : (int)BoletoRemessaItau.TiposEspecieTitulo.Condominio;
            //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
            boletoreg.Emissao = rowSBO.SBODataI;
            boletoreg.Instrucao1 = (int)BoletoRemessaBra.TiposInstrucao.BaixaDecursoPrazo;
            boletoreg.Instrucao2 = 30;
            //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
            ////PegadadosAPT();
            ////if (!rowAPT.IsPESCpfCnpjNull()) { boletoreg.CPF = new DocBacarios.CPFCNPJ(rowAPT.PESCpfCnpj); }
            //boletoreg.NomeSacado = Boletos[0].rowPrincipal.BOLNome;
            //boletoreg.EnderecoSacado = Boletos[0].EnderecoEnvioEfetivo().Replace("\r\n"," ");
            //boletoreg.CEPSacado = Boletos[0].CEPEnvioEfetivo();
            boletoreg.CPF = Apartamento.PESCpfCnpj(Proprietario);
            boletoreg.NomeSacado = Apartamento.PESNome(Proprietario);
            boletoreg.EnderecoSacado = rowCON.CONEndereco.Trim(); // EnderecoEnvioEfetivo();           
            boletoreg.CEPSacado = (!rowCON.IsCONCepNull() && (rowCON.CONCep != "")) ? rowCON.CONCep : "0"; // CEPEnvioEfetivo(); 
            boletoreg.BairroSacado = rowCON.CONBairro.Trim();
            boletoreg.CidadeSacado = (!rowCON.IsCIDNomeNull()) ? rowCON.CIDNome : "";
            boletoreg.EstadoSacado = (!rowCON.IsCIDUfNull()) ? rowCON.CIDUf : "";
            //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

            //desconto (não implementado)
            //dBoletos.BoletoDEscontoRow[] rowDES = rowPrincipal.GetBoletoDEscontoRows();
            //if (rowDES.Length > 0)
            //{
            //    boletoreg.Desconto = rowDES[0].BDEDesconto;
            //    boletoreg.LimDesconto = rowDES[0].BDEData;
            //}

            Remessa.Boletos.Add(boletoreg);

            if (!SoRegistra)
            {
                throw new NotImplementedException("ajustar a path");
                //Remessa.GravaArquivo(@"C:\Temp\Neon\ArquivosBradesco\");
            }
            return true;
        }

        /// <summary>
        /// Altera o status de registro do boleto
        /// </summary>
        public void AlteraStatusRegistro(StatusRegistroBoleto intStatusRegistro)
        {            
            //Altera Status do Registro
            rowSBO.SBOStatusRegistro = (int)intStatusRegistro;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Superboleto AlteraStatusRegistro - 232", DSuperBoleto.SuperBOletoTableAdapter);
                DSuperBoleto.SuperBOletoTableAdapter.Update(rowSBO);
                rowSBO.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }
        }
    }
}
