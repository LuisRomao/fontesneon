﻿namespace Boletos.SeguroConteudo
{


    public partial class dSeguros
    {

        private dSegurosTableAdapters.BOLetosTipoSTableAdapter bOLetosTipoSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLetosTipoS
        /// </summary>
        public dSegurosTableAdapters.BOLetosTipoSTableAdapter BOLetosTipoSTableAdapter
        {
            get
            {
                if (bOLetosTipoSTableAdapter == null)
                {
                    bOLetosTipoSTableAdapter = new dSegurosTableAdapters.BOLetosTipoSTableAdapter();
                    bOLetosTipoSTableAdapter.TrocarStringDeConexao();
                };
                return bOLetosTipoSTableAdapter;
            }
        }

        private dSegurosTableAdapters.APTSeguroExternoTableAdapter aPTSeguroExternoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APTSeguroExterno
        /// </summary>
        public dSegurosTableAdapters.APTSeguroExternoTableAdapter APTSeguroExternoTableAdapter
        {
            get
            {
                if (aPTSeguroExternoTableAdapter == null)
                {
                    aPTSeguroExternoTableAdapter = new dSegurosTableAdapters.APTSeguroExternoTableAdapter();
                    aPTSeguroExternoTableAdapter.TrocarStringDeConexao();
                };
                return aPTSeguroExternoTableAdapter;
            }
        }

        partial class SeguroPagoDataTable
        {
        }

        private dSegurosTableAdapters.SeguroPagoTableAdapter seguroPagoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SeguroPago
        /// </summary>
        public dSegurosTableAdapters.SeguroPagoTableAdapter SeguroPagoTableAdapter
        {
            get
            {
                if (seguroPagoTableAdapter == null)
                {
                    seguroPagoTableAdapter = new dSegurosTableAdapters.SeguroPagoTableAdapter();
                    seguroPagoTableAdapter.TrocarStringDeConexao();
                };
                return seguroPagoTableAdapter;
            }
        }

        private dSegurosTableAdapters.SindicoTableAdapter sindicoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Sindico
        /// </summary>
        public dSegurosTableAdapters.SindicoTableAdapter SindicoTableAdapter
        {
            get
            {
                if (sindicoTableAdapter == null)
                {
                    sindicoTableAdapter = new dSegurosTableAdapters.SindicoTableAdapter();
                    sindicoTableAdapter.TrocarStringDeConexao();
                };
                return sindicoTableAdapter;
            }
        }

        private dSegurosTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalhe
        /// </summary>
        public dSegurosTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dSegurosTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheTableAdapter;
            }
        }

        private dSegurosTableAdapters.CAmpanhaSeguroTableAdapter cAmpanhaSeguroTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CAmpanhaSeguro
        /// </summary>
        public dSegurosTableAdapters.CAmpanhaSeguroTableAdapter CAmpanhaSeguroTableAdapter
        {
            get
            {
                if (cAmpanhaSeguroTableAdapter == null)
                {
                    cAmpanhaSeguroTableAdapter = new dSegurosTableAdapters.CAmpanhaSeguroTableAdapter();
                    cAmpanhaSeguroTableAdapter.TrocarStringDeConexao();
                };
                return cAmpanhaSeguroTableAdapter;
            }
        }

    }


}


