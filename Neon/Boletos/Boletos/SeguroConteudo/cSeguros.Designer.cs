﻿namespace Boletos.SeguroConteudo
{
    partial class cSeguros
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cSeguros));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaAno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPrevisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.aPTSeguroExternoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSegurosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSeguros = new Boletos.SeguroConteudo.dSeguros();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colImprimir = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPSCValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBotAvulso = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryBAvulso = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODValor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCancelado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.dSeguroAcBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSeguroAc = new Boletos.SeguroConteudo.dSeguroAc();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPagamentos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTSeguro1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON_PSC1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanoApt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanoCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.dataVF = new DevExpress.XtraEditors.DateEdit();
            this.dataVI = new DevExpress.XtraEditors.DateEdit();
            this.dataF = new DevExpress.XtraEditors.DateEdit();
            this.dataI = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.BPlanilha = new DevExpress.XtraEditors.SimpleButton();
            this.BArquivo = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtEmitir = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOD_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON_CID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONEndereco2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTSeguro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON_PSC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocatario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdentificaAPT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdentificaCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.TabCampanha = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.cAmpanhaSeguroBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCAS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCASCompet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCAS_PSC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCAS_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCAS_BOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCASPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.lookUpEditPSCCamp = new DevExpress.XtraEditors.LookUpEdit();
            this.dPlanosSeguroConteudoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.SBCampanha = new DevExpress.XtraEditors.SimpleButton();
            this.lookupCondominio_F2 = new Framework.Lookup.LookupCondominio_F();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.cAmpanhaSeguroTableAdapter = new Boletos.SeguroConteudo.dSegurosTableAdapters.CAmpanhaSeguroTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTSeguroExternoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSegurosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSeguros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBAvulso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSeguroAcBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSeguroAc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataVF.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataVF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataVI.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataVI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.TabCampanha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cAmpanhaSeguroBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPSCCamp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPlanosSeguroConteudoBindingSource)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL1,
            this.colBOLCompetenciaMes,
            this.colBOLCompetenciaAno,
            this.colBOLValorPrevisto,
            this.colBOLPagamento2});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsDetail.ShowDetailTabs = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // colBOL1
            // 
            this.colBOL1.Caption = "Boleto";
            this.colBOL1.FieldName = "BOL";
            this.colBOL1.Name = "colBOL1";
            this.colBOL1.OptionsColumn.ReadOnly = true;
            this.colBOL1.Visible = true;
            this.colBOL1.VisibleIndex = 0;
            this.colBOL1.Width = 95;
            // 
            // colBOLCompetenciaMes
            // 
            this.colBOLCompetenciaMes.Caption = "Mês";
            this.colBOLCompetenciaMes.FieldName = "BOLCompetenciaMes";
            this.colBOLCompetenciaMes.Name = "colBOLCompetenciaMes";
            this.colBOLCompetenciaMes.OptionsColumn.ReadOnly = true;
            this.colBOLCompetenciaMes.Visible = true;
            this.colBOLCompetenciaMes.VisibleIndex = 1;
            // 
            // colBOLCompetenciaAno
            // 
            this.colBOLCompetenciaAno.Caption = "Ano";
            this.colBOLCompetenciaAno.FieldName = "BOLCompetenciaAno";
            this.colBOLCompetenciaAno.Name = "colBOLCompetenciaAno";
            this.colBOLCompetenciaAno.OptionsColumn.ReadOnly = true;
            this.colBOLCompetenciaAno.Visible = true;
            this.colBOLCompetenciaAno.VisibleIndex = 2;
            // 
            // colBOLValorPrevisto
            // 
            this.colBOLValorPrevisto.Caption = "Valor";
            this.colBOLValorPrevisto.DisplayFormat.FormatString = "n2";
            this.colBOLValorPrevisto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBOLValorPrevisto.FieldName = "BOLValorPrevisto";
            this.colBOLValorPrevisto.Name = "colBOLValorPrevisto";
            this.colBOLValorPrevisto.OptionsColumn.ReadOnly = true;
            this.colBOLValorPrevisto.Visible = true;
            this.colBOLValorPrevisto.VisibleIndex = 3;
            this.colBOLValorPrevisto.Width = 118;
            // 
            // colBOLPagamento2
            // 
            this.colBOLPagamento2.Caption = "Pagamento";
            this.colBOLPagamento2.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colBOLPagamento2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBOLPagamento2.FieldName = "BOLPagamento";
            this.colBOLPagamento2.Name = "colBOLPagamento2";
            this.colBOLPagamento2.Visible = true;
            this.colBOLPagamento2.VisibleIndex = 4;
            this.colBOLPagamento2.Width = 124;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.aPTSeguroExternoBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView3;
            gridLevelNode1.RelationName = "FK_BOLetos_APARTAMENTOS";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 72);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryBAvulso});
            this.gridControl2.ShowOnlyPredefinedDetails = true;
            this.gridControl2.Size = new System.Drawing.Size(1428, 348);
            this.gridControl2.TabIndex = 17;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2,
            this.gridView3});
            // 
            // aPTSeguroExternoBindingSource
            // 
            this.aPTSeguroExternoBindingSource.DataMember = "APTSeguroExterno";
            this.aPTSeguroExternoBindingSource.DataSource = this.dSegurosBindingSource;
            // 
            // dSegurosBindingSource
            // 
            this.dSegurosBindingSource.DataSource = this.dSeguros;
            this.dSegurosBindingSource.Position = 0;
            // 
            // dSeguros
            // 
            this.dSeguros.DataSetName = "dSeguros";
            this.dSeguros.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseBorderColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView2.Appearance.SelectedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colImprimir,
            this.colCONCodigo1,
            this.colCONNome1,
            this.colBLOCodigo,
            this.colBLONome,
            this.colAPTNumero1,
            this.colPSCValor,
            this.colBotAvulso});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsDetail.ShowDetailTabs = false;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colImprimir
            // 
            this.colImprimir.Caption = "Gerado";
            this.colImprimir.FieldName = "Imprimir";
            this.colImprimir.Name = "colImprimir";
            this.colImprimir.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colImprimir.Visible = true;
            this.colImprimir.VisibleIndex = 1;
            // 
            // colCONCodigo1
            // 
            this.colCONCodigo1.Caption = "CODCON";
            this.colCONCodigo1.FieldName = "CONCodigo";
            this.colCONCodigo1.Name = "colCONCodigo1";
            this.colCONCodigo1.OptionsColumn.ReadOnly = true;
            this.colCONCodigo1.Visible = true;
            this.colCONCodigo1.VisibleIndex = 2;
            // 
            // colCONNome1
            // 
            this.colCONNome1.Caption = "Condoimínio";
            this.colCONNome1.FieldName = "CONNome";
            this.colCONNome1.Name = "colCONNome1";
            this.colCONNome1.OptionsColumn.ReadOnly = true;
            this.colCONNome1.Visible = true;
            this.colCONNome1.VisibleIndex = 3;
            this.colCONNome1.Width = 225;
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "BLOCO";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.OptionsColumn.ReadOnly = true;
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 4;
            // 
            // colBLONome
            // 
            this.colBLONome.Caption = "BLOCO";
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.OptionsColumn.ReadOnly = true;
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 5;
            // 
            // colAPTNumero1
            // 
            this.colAPTNumero1.Caption = "Apartamento";
            this.colAPTNumero1.FieldName = "APTNumero";
            this.colAPTNumero1.Name = "colAPTNumero1";
            this.colAPTNumero1.OptionsColumn.ReadOnly = true;
            this.colAPTNumero1.Visible = true;
            this.colAPTNumero1.VisibleIndex = 6;
            this.colAPTNumero1.Width = 111;
            // 
            // colPSCValor
            // 
            this.colPSCValor.Caption = "Valor";
            this.colPSCValor.DisplayFormat.FormatString = "n2";
            this.colPSCValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPSCValor.FieldName = "PSCValor";
            this.colPSCValor.Name = "colPSCValor";
            this.colPSCValor.OptionsColumn.ReadOnly = true;
            this.colPSCValor.Visible = true;
            this.colPSCValor.VisibleIndex = 7;
            // 
            // colBotAvulso
            // 
            this.colBotAvulso.AppearanceHeader.Options.UseTextOptions = true;
            this.colBotAvulso.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBotAvulso.Caption = "Múltiplo";
            this.colBotAvulso.ColumnEdit = this.repositoryBAvulso;
            this.colBotAvulso.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBotAvulso.Name = "colBotAvulso";
            this.colBotAvulso.Visible = true;
            this.colBotAvulso.VisibleIndex = 8;
            this.colBotAvulso.Width = 50;
            // 
            // repositoryBAvulso
            // 
            this.repositoryBAvulso.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.repositoryBAvulso.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryBAvulso.Name = "repositoryBAvulso";
            this.repositoryBAvulso.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryBAvulso.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryBAvulso_ButtonClick);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL2,
            this.colBOLVencto1,
            this.colBOLPagamento1,
            this.colBODValor1,
            this.colBOLCancelado});
            this.gridView5.GridControl = this.gridControl3;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsDetail.ShowDetailTabs = false;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOLVencto1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colBOL2
            // 
            this.colBOL2.Caption = "Boleto";
            this.colBOL2.FieldName = "BOL";
            this.colBOL2.Name = "colBOL2";
            this.colBOL2.OptionsColumn.ReadOnly = true;
            this.colBOL2.Visible = true;
            this.colBOL2.VisibleIndex = 0;
            // 
            // colBOLVencto1
            // 
            this.colBOLVencto1.Caption = "Vencimento";
            this.colBOLVencto1.FieldName = "BOLVencto";
            this.colBOLVencto1.Name = "colBOLVencto1";
            this.colBOLVencto1.OptionsColumn.ReadOnly = true;
            this.colBOLVencto1.Visible = true;
            this.colBOLVencto1.VisibleIndex = 1;
            // 
            // colBOLPagamento1
            // 
            this.colBOLPagamento1.Caption = "Pagamento";
            this.colBOLPagamento1.FieldName = "BOLPagamento";
            this.colBOLPagamento1.Name = "colBOLPagamento1";
            this.colBOLPagamento1.OptionsColumn.ReadOnly = true;
            this.colBOLPagamento1.Visible = true;
            this.colBOLPagamento1.VisibleIndex = 2;
            // 
            // colBODValor1
            // 
            this.colBODValor1.Caption = "Valor";
            this.colBODValor1.DisplayFormat.FormatString = "n2";
            this.colBODValor1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor1.FieldName = "BODValor";
            this.colBODValor1.Name = "colBODValor1";
            this.colBODValor1.OptionsColumn.ReadOnly = true;
            this.colBODValor1.Visible = true;
            this.colBODValor1.VisibleIndex = 3;
            // 
            // colBOLCancelado
            // 
            this.colBOLCancelado.Caption = "Cancelado";
            this.colBOLCancelado.FieldName = "BOLCancelado";
            this.colBOLCancelado.Name = "colBOLCancelado";
            this.colBOLCancelado.OptionsColumn.ReadOnly = true;
            this.colBOLCancelado.Visible = true;
            this.colBOLCancelado.VisibleIndex = 4;
            // 
            // gridControl3
            // 
            this.gridControl3.DataMember = "Geral";
            this.gridControl3.DataSource = this.dSeguroAcBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.LevelTemplate = this.gridView5;
            gridLevelNode2.RelationName = "Geral_Detalhe";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl3.Location = new System.Drawing.Point(0, 73);
            this.gridControl3.MainView = this.gridView4;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(1428, 347);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4,
            this.gridView5});
            // 
            // dSeguroAcBindingSource
            // 
            this.dSeguroAcBindingSource.DataSource = this.dSeguroAc;
            this.dSeguroAcBindingSource.Position = 0;
            // 
            // dSeguroAc
            // 
            this.dSeguroAc.DataSetName = "dSeguroAc";
            this.dSeguroAc.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView4.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView4.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView4.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView4.Appearance.Empty.Options.UseBackColor = true;
            this.gridView4.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView4.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView4.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView4.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView4.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView4.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView4.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView4.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.gridView4.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView4.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView4.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView4.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView4.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.gridView4.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.gridView4.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView4.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView4.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView4.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView4.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView4.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView4.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView4.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView4.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView4.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView4.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView4.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView4.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView4.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView4.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView4.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.gridView4.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView4.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView4.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView4.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView4.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView4.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView4.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView4.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView4.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView4.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView4.Appearance.Preview.Options.UseFont = true;
            this.gridView4.Appearance.Preview.Options.UseForeColor = true;
            this.gridView4.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.Row.Options.UseBackColor = true;
            this.gridView4.Appearance.Row.Options.UseForeColor = true;
            this.gridView4.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView4.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView4.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView4.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView4.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView4.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView4.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView4.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView4.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView4.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView4.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPagamentos,
            this.colTotalPago,
            this.colCONNome2,
            this.colCON1,
            this.colCONCodigo2,
            this.colAPTSeguro1,
            this.colAPTNumero2,
            this.colBLOCodigo1,
            this.colAPT,
            this.colCON_PSC1,
            this.colPlanoApt,
            this.colPlanoCondominio});
            this.gridView4.GridControl = this.gridControl3;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsDetail.ShowDetailTabs = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.EnableAppearanceOddRow = true;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowFooter = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONNome2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colPagamentos
            // 
            this.colPagamentos.FieldName = "Pagamentos";
            this.colPagamentos.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPagamentos.Name = "colPagamentos";
            this.colPagamentos.OptionsColumn.ReadOnly = true;
            this.colPagamentos.Visible = true;
            this.colPagamentos.VisibleIndex = 1;
            this.colPagamentos.Width = 81;
            // 
            // colTotalPago
            // 
            this.colTotalPago.DisplayFormat.FormatString = "n2";
            this.colTotalPago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalPago.FieldName = "TotalPago";
            this.colTotalPago.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTotalPago.Name = "colTotalPago";
            this.colTotalPago.OptionsColumn.ReadOnly = true;
            this.colTotalPago.Visible = true;
            this.colTotalPago.VisibleIndex = 2;
            this.colTotalPago.Width = 78;
            // 
            // colCONNome2
            // 
            this.colCONNome2.Caption = "Condomínio";
            this.colCONNome2.FieldName = "CONNome";
            this.colCONNome2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCONNome2.Name = "colCONNome2";
            this.colCONNome2.OptionsColumn.ReadOnly = true;
            this.colCONNome2.Visible = true;
            this.colCONNome2.VisibleIndex = 2;
            this.colCONNome2.Width = 269;
            // 
            // colCON1
            // 
            this.colCON1.FieldName = "CON";
            this.colCON1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCON1.Name = "colCON1";
            this.colCON1.OptionsColumn.ReadOnly = true;
            // 
            // colCONCodigo2
            // 
            this.colCONCodigo2.Caption = "Código";
            this.colCONCodigo2.FieldName = "CONCodigo";
            this.colCONCodigo2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCONCodigo2.Name = "colCONCodigo2";
            this.colCONCodigo2.OptionsColumn.ReadOnly = true;
            this.colCONCodigo2.Visible = true;
            this.colCONCodigo2.VisibleIndex = 0;
            this.colCONCodigo2.Width = 87;
            // 
            // colAPTSeguro1
            // 
            this.colAPTSeguro1.FieldName = "APTSeguro";
            this.colAPTSeguro1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAPTSeguro1.Name = "colAPTSeguro1";
            this.colAPTSeguro1.OptionsColumn.ReadOnly = true;
            // 
            // colAPTNumero2
            // 
            this.colAPTNumero2.Caption = "Apartamento";
            this.colAPTNumero2.FieldName = "APTNumero";
            this.colAPTNumero2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAPTNumero2.Name = "colAPTNumero2";
            this.colAPTNumero2.OptionsColumn.ReadOnly = true;
            this.colAPTNumero2.Visible = true;
            this.colAPTNumero2.VisibleIndex = 4;
            // 
            // colBLOCodigo1
            // 
            this.colBLOCodigo1.Caption = "Bloco";
            this.colBLOCodigo1.FieldName = "BLOCodigo";
            this.colBLOCodigo1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBLOCodigo1.Name = "colBLOCodigo1";
            this.colBLOCodigo1.OptionsColumn.ReadOnly = true;
            this.colBLOCodigo1.Visible = true;
            this.colBLOCodigo1.VisibleIndex = 3;
            this.colBLOCodigo1.Width = 50;
            // 
            // colAPT
            // 
            this.colAPT.FieldName = "APT";
            this.colAPT.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAPT.Name = "colAPT";
            this.colAPT.OptionsColumn.ReadOnly = true;
            // 
            // colCON_PSC1
            // 
            this.colCON_PSC1.FieldName = "CON_PSC";
            this.colCON_PSC1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCON_PSC1.Name = "colCON_PSC1";
            this.colCON_PSC1.OptionsColumn.ReadOnly = true;
            // 
            // colPlanoApt
            // 
            this.colPlanoApt.Caption = "Plano Apt";
            this.colPlanoApt.FieldName = "PlanoApt";
            this.colPlanoApt.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPlanoApt.Name = "colPlanoApt";
            this.colPlanoApt.OptionsColumn.ReadOnly = true;
            this.colPlanoApt.Visible = true;
            this.colPlanoApt.VisibleIndex = 5;
            // 
            // colPlanoCondominio
            // 
            this.colPlanoCondominio.Caption = "Plano Condomínio";
            this.colPlanoCondominio.FieldName = "PlanoCondominio";
            this.colPlanoCondominio.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPlanoCondominio.Name = "colPlanoCondominio";
            this.colPlanoCondominio.OptionsColumn.ReadOnly = true;
            this.colPlanoCondominio.Visible = true;
            this.colPlanoCondominio.VisibleIndex = 6;
            this.colPlanoCondominio.Width = 100;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.dataVF);
            this.panelControl1.Controls.Add(this.dataVI);
            this.panelControl1.Controls.Add(this.dataF);
            this.panelControl1.Controls.Add(this.dataI);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1434, 42);
            this.panelControl1.TabIndex = 0;
            // 
            // dataVF
            // 
            this.dataVF.EditValue = null;
            this.dataVF.Location = new System.Drawing.Point(804, 7);
            this.dataVF.Name = "dataVF";
            this.dataVF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVF.Properties.Appearance.Options.UseFont = true;
            this.dataVF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dataVF.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataVF.Properties.ReadOnly = true;
            this.dataVF.Size = new System.Drawing.Size(114, 24);
            this.dataVF.TabIndex = 8;
            // 
            // dataVI
            // 
            this.dataVI.EditValue = null;
            this.dataVI.Location = new System.Drawing.Point(684, 7);
            this.dataVI.Name = "dataVI";
            this.dataVI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVI.Properties.Appearance.Options.UseFont = true;
            this.dataVI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dataVI.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataVI.Properties.ReadOnly = true;
            this.dataVI.Size = new System.Drawing.Size(114, 24);
            this.dataVI.TabIndex = 7;
            // 
            // dataF
            // 
            this.dataF.EditValue = null;
            this.dataF.Location = new System.Drawing.Point(409, 7);
            this.dataF.Name = "dataF";
            this.dataF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataF.Properties.Appearance.Options.UseFont = true;
            this.dataF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dataF.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataF.Properties.ReadOnly = true;
            this.dataF.Size = new System.Drawing.Size(114, 24);
            this.dataF.TabIndex = 6;
            // 
            // dataI
            // 
            this.dataI.EditValue = null;
            this.dataI.Location = new System.Drawing.Point(289, 7);
            this.dataI.Name = "dataI";
            this.dataI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataI.Properties.Appearance.Options.UseFont = true;
            this.dataI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dataI.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataI.Properties.ReadOnly = true;
            this.dataI.Size = new System.Drawing.Size(114, 24);
            this.dataI.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(609, 10);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(69, 18);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Vigência:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(184, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(99, 18);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Arrecadação:";
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(5, 5);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 1;
            this.cCompet1.Titulo = null;
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // BPlanilha
            // 
            this.BPlanilha.Location = new System.Drawing.Point(358, 11);
            this.BPlanilha.Name = "BPlanilha";
            this.BPlanilha.Size = new System.Drawing.Size(164, 59);
            this.BPlanilha.TabIndex = 10;
            this.BPlanilha.Text = "Gerar Planilha";
            this.BPlanilha.Click += new System.EventHandler(this.BPlanilha_Click);
            // 
            // BArquivo
            // 
            this.BArquivo.Location = new System.Drawing.Point(188, 11);
            this.BArquivo.Name = "BArquivo";
            this.BArquivo.Size = new System.Drawing.Size(164, 59);
            this.BArquivo.TabIndex = 9;
            this.BArquivo.Text = "Gerar Arquivo";
            this.BArquivo.Click += new System.EventHandler(this.BArquivo_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(5, 9);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(130, 59);
            this.simpleButton2.TabIndex = 2;
            this.simpleButton2.Text = "Calcular";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // BtEmitir
            // 
            this.BtEmitir.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtEmitir.Appearance.Options.UseFont = true;
            this.BtEmitir.Enabled = false;
            this.BtEmitir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtEmitir.ImageOptions.Image")));
            this.BtEmitir.Location = new System.Drawing.Point(141, 7);
            this.BtEmitir.Name = "BtEmitir";
            this.BtEmitir.Size = new System.Drawing.Size(180, 59);
            this.BtEmitir.TabIndex = 11;
            this.BtEmitir.Text = "Emitir Boletos";
            this.BtEmitir.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "SeguroPago";
            this.gridControl1.DataSource = this.dSegurosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 73);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1428, 347);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOD_PLA,
            this.colBOD,
            this.colBOLPagamento,
            this.colBOLVencto,
            this.colBOL,
            this.colBODValor,
            this.colCONNome,
            this.colCON,
            this.colCONCodigo,
            this.colCONBairro,
            this.colCON_CID,
            this.colCONCep,
            this.colCONEndereco2,
            this.colCONNumero,
            this.colCIDNome,
            this.colCIDUf,
            this.colAPTSeguro,
            this.colAPTNumero,
            this.colCON_PSC,
            this.colLocatario,
            this.colProprietario,
            this.colIdentificaAPT,
            this.colIdentificaCON,
            this.colPAG});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", this.colBODValor, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "SeguroPago.BOD", this.colCIDUf, "")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIdentificaCON, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colBOD_PLA
            // 
            this.colBOD_PLA.FieldName = "BOD_PLA";
            this.colBOD_PLA.Name = "colBOD_PLA";
            this.colBOD_PLA.OptionsColumn.ReadOnly = true;
            // 
            // colBOD
            // 
            this.colBOD.FieldName = "BOD";
            this.colBOD.Name = "colBOD";
            this.colBOD.OptionsColumn.ReadOnly = true;
            // 
            // colBOLPagamento
            // 
            this.colBOLPagamento.Caption = "Pagamento";
            this.colBOLPagamento.FieldName = "BOLPagamento";
            this.colBOLPagamento.Name = "colBOLPagamento";
            this.colBOLPagamento.OptionsColumn.ReadOnly = true;
            this.colBOLPagamento.Visible = true;
            this.colBOLPagamento.VisibleIndex = 10;
            // 
            // colBOLVencto
            // 
            this.colBOLVencto.FieldName = "BOLVencto";
            this.colBOLVencto.Name = "colBOLVencto";
            this.colBOLVencto.OptionsColumn.ReadOnly = true;
            // 
            // colBOL
            // 
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.ReadOnly = true;
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.DisplayFormat.FormatString = "n2";
            this.colBODValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.OptionsColumn.ReadOnly = true;
            this.colBODValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", "{0:n2}")});
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 11;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Condomínio";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.ReadOnly = true;
            // 
            // colCON
            // 
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "Código";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            this.colCONCodigo.Width = 61;
            // 
            // colCONBairro
            // 
            this.colCONBairro.Caption = "Bairro";
            this.colCONBairro.FieldName = "CONBairro";
            this.colCONBairro.Name = "colCONBairro";
            this.colCONBairro.OptionsColumn.ReadOnly = true;
            this.colCONBairro.Visible = true;
            this.colCONBairro.VisibleIndex = 3;
            this.colCONBairro.Width = 89;
            // 
            // colCON_CID
            // 
            this.colCON_CID.FieldName = "CON_CID";
            this.colCON_CID.Name = "colCON_CID";
            this.colCON_CID.OptionsColumn.ReadOnly = true;
            // 
            // colCONCep
            // 
            this.colCONCep.Caption = "CEP";
            this.colCONCep.FieldName = "CONCep";
            this.colCONCep.Name = "colCONCep";
            this.colCONCep.OptionsColumn.ReadOnly = true;
            this.colCONCep.Visible = true;
            this.colCONCep.VisibleIndex = 6;
            // 
            // colCONEndereco2
            // 
            this.colCONEndereco2.Caption = "Endereço";
            this.colCONEndereco2.FieldName = "CONEndereco2";
            this.colCONEndereco2.Name = "colCONEndereco2";
            this.colCONEndereco2.OptionsColumn.ReadOnly = true;
            this.colCONEndereco2.Visible = true;
            this.colCONEndereco2.VisibleIndex = 4;
            this.colCONEndereco2.Width = 142;
            // 
            // colCONNumero
            // 
            this.colCONNumero.Caption = "Número";
            this.colCONNumero.FieldName = "CONNumero";
            this.colCONNumero.Name = "colCONNumero";
            this.colCONNumero.OptionsColumn.ReadOnly = true;
            this.colCONNumero.Visible = true;
            this.colCONNumero.VisibleIndex = 5;
            // 
            // colCIDNome
            // 
            this.colCIDNome.Caption = "Cidade";
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            this.colCIDNome.OptionsColumn.ReadOnly = true;
            this.colCIDNome.Visible = true;
            this.colCIDNome.VisibleIndex = 2;
            this.colCIDNome.Width = 93;
            // 
            // colCIDUf
            // 
            this.colCIDUf.Caption = "UF";
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.OptionsColumn.ReadOnly = true;
            this.colCIDUf.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colCIDUf.Visible = true;
            this.colCIDUf.VisibleIndex = 1;
            this.colCIDUf.Width = 54;
            // 
            // colAPTSeguro
            // 
            this.colAPTSeguro.FieldName = "APTSeguro";
            this.colAPTSeguro.Name = "colAPTSeguro";
            this.colAPTSeguro.OptionsColumn.ReadOnly = true;
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "Apartamento";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.OptionsColumn.ReadOnly = true;
            this.colAPTNumero.Width = 82;
            // 
            // colCON_PSC
            // 
            this.colCON_PSC.FieldName = "CON_PSC";
            this.colCON_PSC.Name = "colCON_PSC";
            this.colCON_PSC.OptionsColumn.ReadOnly = true;
            // 
            // colLocatario
            // 
            this.colLocatario.Caption = "Locatário";
            this.colLocatario.FieldName = "Locatario";
            this.colLocatario.Name = "colLocatario";
            this.colLocatario.OptionsColumn.ReadOnly = true;
            this.colLocatario.Visible = true;
            this.colLocatario.VisibleIndex = 9;
            this.colLocatario.Width = 138;
            // 
            // colProprietario
            // 
            this.colProprietario.Caption = "Proprietário";
            this.colProprietario.FieldName = "Proprietario";
            this.colProprietario.Name = "colProprietario";
            this.colProprietario.OptionsColumn.ReadOnly = true;
            this.colProprietario.Visible = true;
            this.colProprietario.VisibleIndex = 8;
            this.colProprietario.Width = 161;
            // 
            // colIdentificaAPT
            // 
            this.colIdentificaAPT.Caption = "Apartamento";
            this.colIdentificaAPT.FieldName = "IdentificaAPT";
            this.colIdentificaAPT.Name = "colIdentificaAPT";
            this.colIdentificaAPT.Visible = true;
            this.colIdentificaAPT.VisibleIndex = 7;
            // 
            // colIdentificaCON
            // 
            this.colIdentificaCON.FieldName = "IdentificaCON";
            this.colIdentificaCON.Name = "colIdentificaCON";
            // 
            // colPAG
            // 
            this.colPAG.FieldName = "PAG";
            this.colPAG.Name = "colPAG";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 42);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1434, 448);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.TabCampanha,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.panelControl3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1428, 420);
            this.xtraTabPage1.Text = "Seguros Contratados";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.BPlanilha);
            this.panelControl3.Controls.Add(this.BArquivo);
            this.panelControl3.Controls.Add(this.simpleButton2);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1428, 73);
            this.panelControl3.TabIndex = 2;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Controls.Add(this.panelControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1428, 420);
            this.xtraTabPage2.Text = "Seguro externo \"S\"";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButton3);
            this.panelControl2.Controls.Add(this.simpleButton4);
            this.panelControl2.Controls.Add(this.BtEmitir);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1428, 72);
            this.panelControl2.TabIndex = 16;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.ImageOptions.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(327, 9);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(245, 59);
            this.simpleButton3.TabIndex = 18;
            this.simpleButton3.Text = "Imprimir Selecionados";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click_1);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(5, 7);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(130, 59);
            this.simpleButton4.TabIndex = 16;
            this.simpleButton4.Text = "Calcular";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // TabCampanha
            // 
            this.TabCampanha.Controls.Add(this.gridControl4);
            this.TabCampanha.Controls.Add(this.panelControl5);
            this.TabCampanha.Name = "TabCampanha";
            this.TabCampanha.Size = new System.Drawing.Size(1428, 420);
            this.TabCampanha.Text = "Campanha";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.cAmpanhaSeguroBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.Location = new System.Drawing.Point(0, 72);
            this.gridControl4.MainView = this.gridView6;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(1428, 348);
            this.gridControl4.TabIndex = 18;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // cAmpanhaSeguroBindingSource
            // 
            this.cAmpanhaSeguroBindingSource.DataMember = "CAmpanhaSeguro";
            this.cAmpanhaSeguroBindingSource.DataSource = this.dSegurosBindingSource;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCAS,
            this.colCASCompet,
            this.colCAS_PSC,
            this.colCAS_APT,
            this.colCAS_BOL,
            this.colCASPago,
            this.colAPTNumero3,
            this.colBLOCodigo2,
            this.colCONCodigo3,
            this.colCONNome3});
            this.gridView6.GridControl = this.gridControl4;
            this.gridView6.GroupCount = 1;
            this.gridView6.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "CAS_BOL", null, " = {0} Boletos")});
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.ShowAutoFilterRow = true;
            this.gridView6.OptionsView.ShowFooter = true;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCodigo3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBLOCodigo2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAPTNumero3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCAS
            // 
            this.colCAS.FieldName = "CAS";
            this.colCAS.Name = "colCAS";
            this.colCAS.OptionsColumn.ReadOnly = true;
            // 
            // colCASCompet
            // 
            this.colCASCompet.Caption = "Competência";
            this.colCASCompet.FieldName = "CASCompet";
            this.colCASCompet.Name = "colCASCompet";
            this.colCASCompet.OptionsColumn.ReadOnly = true;
            // 
            // colCAS_PSC
            // 
            this.colCAS_PSC.FieldName = "CAS_PSC";
            this.colCAS_PSC.Name = "colCAS_PSC";
            this.colCAS_PSC.OptionsColumn.ReadOnly = true;
            // 
            // colCAS_APT
            // 
            this.colCAS_APT.FieldName = "CAS_APT";
            this.colCAS_APT.Name = "colCAS_APT";
            this.colCAS_APT.OptionsColumn.ReadOnly = true;
            // 
            // colCAS_BOL
            // 
            this.colCAS_BOL.Caption = "Boleto";
            this.colCAS_BOL.FieldName = "CAS_BOL";
            this.colCAS_BOL.Name = "colCAS_BOL";
            this.colCAS_BOL.OptionsColumn.ReadOnly = true;
            this.colCAS_BOL.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "CAS_BOL", "{0}")});
            this.colCAS_BOL.Visible = true;
            this.colCAS_BOL.VisibleIndex = 3;
            // 
            // colCASPago
            // 
            this.colCASPago.Caption = "Pago";
            this.colCASPago.FieldName = "CASPago";
            this.colCASPago.Name = "colCASPago";
            this.colCASPago.OptionsColumn.ReadOnly = true;
            this.colCASPago.Visible = true;
            this.colCASPago.VisibleIndex = 4;
            // 
            // colAPTNumero3
            // 
            this.colAPTNumero3.Caption = "Apartamento";
            this.colAPTNumero3.FieldName = "APTNumero";
            this.colAPTNumero3.Name = "colAPTNumero3";
            this.colAPTNumero3.OptionsColumn.ReadOnly = true;
            this.colAPTNumero3.Visible = true;
            this.colAPTNumero3.VisibleIndex = 2;
            this.colAPTNumero3.Width = 138;
            // 
            // colBLOCodigo2
            // 
            this.colBLOCodigo2.Caption = "Bloco";
            this.colBLOCodigo2.FieldName = "BLOCodigo";
            this.colBLOCodigo2.Name = "colBLOCodigo2";
            this.colBLOCodigo2.OptionsColumn.ReadOnly = true;
            this.colBLOCodigo2.Visible = true;
            this.colBLOCodigo2.VisibleIndex = 1;
            this.colBLOCodigo2.Width = 94;
            // 
            // colCONCodigo3
            // 
            this.colCONCodigo3.Caption = "CODCON";
            this.colCONCodigo3.FieldName = "CONCodigo";
            this.colCONCodigo3.Name = "colCONCodigo3";
            this.colCONCodigo3.OptionsColumn.ReadOnly = true;
            this.colCONCodigo3.Visible = true;
            this.colCONCodigo3.VisibleIndex = 0;
            this.colCONCodigo3.Width = 97;
            // 
            // colCONNome3
            // 
            this.colCONNome3.Caption = "Condomínio";
            this.colCONNome3.FieldName = "CONNome";
            this.colCONNome3.Name = "colCONNome3";
            this.colCONNome3.OptionsColumn.ReadOnly = true;
            this.colCONNome3.Visible = true;
            this.colCONNome3.VisibleIndex = 0;
            this.colCONNome3.Width = 217;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.lookUpEditPSCCamp);
            this.panelControl5.Controls.Add(this.simpleButton6);
            this.panelControl5.Controls.Add(this.SBCampanha);
            this.panelControl5.Controls.Add(this.lookupCondominio_F2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1428, 72);
            this.panelControl5.TabIndex = 17;
            // 
            // lookUpEditPSCCamp
            // 
            this.lookUpEditPSCCamp.Location = new System.Drawing.Point(431, 18);
            this.lookUpEditPSCCamp.Name = "lookUpEditPSCCamp";
            editorButtonImageOptions6.EnableTransparency = false;
            this.lookUpEditPSCCamp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Selecionar", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.lookUpEditPSCCamp.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PSCDescricao", "Descrição", 78, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PSCValor", "Valor", 56, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditPSCCamp.Properties.DataSource = this.dPlanosSeguroConteudoBindingSource;
            this.lookUpEditPSCCamp.Properties.DisplayMember = "PSCDescricao";
            this.lookUpEditPSCCamp.Properties.ValueMember = "PSC";
            this.lookUpEditPSCCamp.Size = new System.Drawing.Size(309, 20);
            this.lookUpEditPSCCamp.TabIndex = 17;
            this.lookUpEditPSCCamp.EditValueChanged += new System.EventHandler(this.lookUpEdit1_EditValueChanged);
            // 
            // dPlanosSeguroConteudoBindingSource
            // 
            this.dPlanosSeguroConteudoBindingSource.DataMember = "PlanosSeguroConteudo";
            this.dPlanosSeguroConteudoBindingSource.DataSource = typeof(Framework.datasets.dPlanosSeguroConteudo);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.ImageOptions.Image")));
            this.simpleButton6.Location = new System.Drawing.Point(5, 7);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(130, 59);
            this.simpleButton6.TabIndex = 16;
            this.simpleButton6.Text = "Calcular";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // SBCampanha
            // 
            this.SBCampanha.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SBCampanha.Appearance.Options.UseFont = true;
            this.SBCampanha.Enabled = false;
            this.SBCampanha.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("SBCampanha.ImageOptions.Image")));
            this.SBCampanha.Location = new System.Drawing.Point(141, 8);
            this.SBCampanha.Name = "SBCampanha";
            this.SBCampanha.Size = new System.Drawing.Size(269, 59);
            this.SBCampanha.TabIndex = 11;
            this.SBCampanha.Text = "Emitir/Imprimir Boletos";
            this.SBCampanha.Click += new System.EventHandler(this.SBCampanha_Click);
            // 
            // lookupCondominio_F2
            // 
            this.lookupCondominio_F2.Autofill = true;
            this.lookupCondominio_F2.CaixaAltaGeral = true;
            this.lookupCondominio_F2.CON_sel = -1;
            this.lookupCondominio_F2.CON_selrow = null;
            this.lookupCondominio_F2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F2.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F2.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F2.LinhaMae_F = null;
            this.lookupCondominio_F2.Location = new System.Drawing.Point(432, 45);
            this.lookupCondominio_F2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCondominio_F2.Name = "lookupCondominio_F2";
            this.lookupCondominio_F2.NivelCONOculto = 2;
            this.lookupCondominio_F2.Size = new System.Drawing.Size(308, 20);
            this.lookupCondominio_F2.somenteleitura = false;
            this.lookupCondominio_F2.TabIndex = 15;
            this.lookupCondominio_F2.TableAdapterPrincipal = null;
            this.lookupCondominio_F2.Titulo = null;
            this.lookupCondominio_F2.alterado += new System.EventHandler(this.lookupCondominio_F2_alterado);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl3);
            this.xtraTabPage3.Controls.Add(this.panelControl4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1428, 420);
            this.xtraTabPage3.Text = "Acompanhamento";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.simpleButton5);
            this.panelControl4.Controls.Add(this.simpleButton8);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1428, 73);
            this.panelControl4.TabIndex = 3;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(172, 5);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(164, 59);
            this.simpleButton5.TabIndex = 11;
            this.simpleButton5.Text = "Gerar Planilha Comparativa";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(2, 5);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(164, 59);
            this.simpleButton8.TabIndex = 2;
            this.simpleButton8.Text = "Calcular";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // cAmpanhaSeguroTableAdapter
            // 
            this.cAmpanhaSeguroTableAdapter.ClearBeforeFill = true;
            this.cAmpanhaSeguroTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("cAmpanhaSeguroTableAdapter.GetNovosDados")));
            this.cAmpanhaSeguroTableAdapter.TabelaDataTable = null;
            // 
            // cSeguros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cSeguros";
            this.Size = new System.Drawing.Size(1434, 490);
            this.Load += new System.EventHandler(this.cSeguros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTSeguroExternoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSegurosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSeguros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBAvulso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSeguroAcBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSeguroAc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataVF.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataVF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataVI.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataVI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.TabCampanha.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cAmpanhaSeguroBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPSCCamp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPlanosSeguroConteudoBindingSource)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dSegurosBindingSource;
        private dSeguros dSeguros;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_CID;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCep;
        private DevExpress.XtraGrid.Columns.GridColumn colCONEndereco2;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTSeguro;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_PSC;
        private DevExpress.XtraGrid.Columns.GridColumn colLocatario;
        private DevExpress.XtraGrid.Columns.GridColumn colProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colIdentificaAPT;
        private DevExpress.XtraGrid.Columns.GridColumn colIdentificaCON;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.DateEdit dataVF;
        private DevExpress.XtraEditors.DateEdit dataVI;
        private DevExpress.XtraEditors.DateEdit dataF;
        private DevExpress.XtraEditors.DateEdit dataI;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton BPlanilha;
        private DevExpress.XtraEditors.SimpleButton BArquivo;
        private DevExpress.XtraEditors.SimpleButton BtEmitir;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource aPTSeguroExternoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private System.Windows.Forms.BindingSource dPlanosSeguroConteudoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaMes;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaAno;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPrevisto;
        private DevExpress.XtraGrid.Columns.GridColumn colImprimir;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome1;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG;
        private DevExpress.XtraGrid.Columns.GridColumn colPSCValor;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.BindingSource dSeguroAcBindingSource;
        private dSeguroAc dSeguroAc;
        private DevExpress.XtraGrid.Columns.GridColumn colPagamentos;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalPago;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome2;
        private DevExpress.XtraGrid.Columns.GridColumn colCON1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTSeguro1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero2;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPT;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_PSC1;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanoApt;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanoCondominio;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL2;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento1;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCancelado;
        private DevExpress.XtraGrid.Columns.GridColumn colBotAvulso;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryBAvulso;
        private DevExpress.XtraTab.XtraTabPage TabCampanha;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPSCCamp;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton SBCampanha;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F2;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento2;
        private System.Windows.Forms.BindingSource cAmpanhaSeguroBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCAS;
        private DevExpress.XtraGrid.Columns.GridColumn colCASCompet;
        private DevExpress.XtraGrid.Columns.GridColumn colCAS_PSC;
        private DevExpress.XtraGrid.Columns.GridColumn colCAS_APT;
        private DevExpress.XtraGrid.Columns.GridColumn colCAS_BOL;
        private DevExpress.XtraGrid.Columns.GridColumn colCASPago;
        private dSegurosTableAdapters.CAmpanhaSeguroTableAdapter cAmpanhaSeguroTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero3;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo2;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo3;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome3;
    }
}
