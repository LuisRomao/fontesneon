﻿namespace Boletos.SeguroConteudo {
    
    
    public partial class dSeguroAc 
    {
        private dSeguroAcTableAdapters.GeralTableAdapter geralTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Geral
        /// </summary>
        public dSeguroAcTableAdapters.GeralTableAdapter GeralTableAdapter
        {
            get
            {
                if (geralTableAdapter == null)
                {
                    geralTableAdapter = new dSeguroAcTableAdapters.GeralTableAdapter();
                    geralTableAdapter.TrocarStringDeConexao();
                };
                return geralTableAdapter;
            }
        }

        private dSeguroAcTableAdapters.DetalheTableAdapter detalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Detalhe
        /// </summary>
        public dSeguroAcTableAdapters.DetalheTableAdapter DetalheTableAdapter
        {
            get
            {
                if (detalheTableAdapter == null)
                {
                    detalheTableAdapter = new dSeguroAcTableAdapters.DetalheTableAdapter();
                    detalheTableAdapter.TrocarStringDeConexao();
                };
                return detalheTableAdapter;
            }
        }
    }
}
