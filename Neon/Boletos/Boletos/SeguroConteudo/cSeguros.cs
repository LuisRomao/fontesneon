﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using Framework.objetosNeon;
using dllImpresso;
using DevExpress.XtraReports.UI;
using AbstratosNeon;
using VirEnumeracoesNeon;
using CompontesBasicosProc;
using FrameworkProc;
using Framework;

namespace Boletos.SeguroConteudo
{
    /// <summary>
    /// Seguro conteúdo
    /// </summary>
    public partial class cSeguros : ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cSeguros()
        {
            InitializeComponent();
            dPlanosSeguroConteudoBindingSource.DataSource = Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt;
        }

        /// <summary>
        /// Remove o seguro dos boletos em aberto.
        /// </summary>
        /// <param name="DataCorte"></param>
        /// <param name="ESP"></param>
        /// <param name="dSeguros1"></param>
        public static void LimparBoletos(DateTime DataCorte, CompontesBasicos.Espera.cEspera ESP = null, dSeguros dSeguros1 = null)
        {
            if (dSeguros1 == null)
                dSeguros1 = new dSeguros();
            dSeguros1.BOletoDetalheTableAdapter.DeleteSeguroSolto(DataCorte);

            ESP.Espere("Removendo seguro dos em aberto");
            BoletosProc.Boleto.dBoletos dBoletos1 = new BoletosProc.Boleto.dBoletos();
            if (dBoletos1.BOLetosTableAdapter.FillBySeguroVencido(dBoletos1.BOLetos, DataCorte) > 0)
            {
                dBoletos1.BOletoDetalheTableAdapter.FillBySeguroVencido(dBoletos1.BOletoDetalhe, DataCorte);
                ESP.AtivaGauge(dBoletos1.BOLetos.Count);
                int pos = 0;
                foreach (BoletosProc.Boleto.dBoletos.BOLetosRow rowBOL in dBoletos1.BOLetos)
                {
                    ESP.GaugeTempo(pos++);
                    Boleto.Boleto BolRemover = new Boleto.Boleto(rowBOL);
                    if (BolRemover.Encontrado)
                        BolRemover.RemoveSeguro(Boleto.Boleto.TipoRemoveSeguro.remove_ArquivoGerado);
                }
            }                        
        }

        /*
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            SaveFileDialog SF = new SaveFileDialog();
            SF.DefaultExt = ".csv";
            if (SF.ShowDialog() == DialogResult.OK)
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Gerando Arquivo");
                    SortedList<int, decimal> geraCheques = new SortedList<int, decimal>();
                    
                    DateTime DataI = new DateTime(cCompet1.Comp.Ano, cCompet1.Comp.Mes, 1);
                    DateTime DataF = DataI.AddMonths(1).AddDays(-1);

                    DateTime DataIArrecada = DataI.AddMonths(-1);
                    DateTime DataFArrecada = DataI.AddDays(-1);


                    dSeguros.SeguroPagoTableAdapter.Fill(dSeguros.SeguroPago,DataIArrecada,DataFArrecada);
                    int CON = 0;                    
                    List<dSeguros.SeguroPagoRow> Sindicos = new List<dSeguros.SeguroPagoRow>();
                    foreach (dSeguros.SeguroPagoRow row in dSeguros.SeguroPago)
                    {
                        row.CONCep = CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(row.CONCep, CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.SoNumeros);
                        if (row.BLOCodigo.ToUpper().Trim() != "SB")
                            row.IdentificaAPT = string.Format("{0} - {1}", row.BLOCodigo, row.APTNumero);
                        else
                            row.IdentificaAPT = string.Format("{0}", row.APTNumero);
                        string CodigoFolha = "";
                        if (!row.IsCONCodigoFolha1Null())
                            CodigoFolha = string.Format("{0} - ", row.CONCodigoFolha1);
                        row.IdentificaCON = string.Format("{0}{1} - {2}", CodigoFolha, row.CONCodigo, row.CONNome);
                        if (row.CON != CON)
                        {
                            CON = row.CON;
                            if (CON != 0)
                                geraCheques.Add(CON, row.BODValor);                                                        
                            if (dSeguros.SindicoTableAdapter.Fill(dSeguros.Sindico, CON) > 0)
                            {
                                dSeguros.SeguroPagoRow rowS = dSeguros.SeguroPago.NewSeguroPagoRow();
                                rowS.BOD_PLA = "";
                                rowS.BOD = 0;
                                rowS.BODValor = row.BODValor;
                                rowS.BOL = 0;
                                rowS.BOLVencto = row.BOLVencto;
                                rowS.CON = row.CON;
                                rowS.CONBairro = row.CONBairro;
                                rowS.CONCep = row.CONCep;
                                rowS.CONCodigo = row.CONCodigo;
                                rowS.CONCodigoFolha1 = row.CONCodigoFolha1;
                                rowS.CONNumero = row.CONNumero;
                                rowS.CON_CID = row.CON_CID;
                                rowS.CONCodigo = row.CONCodigo;
                                rowS.CONNome = row.CONNome;
                                rowS.CONEndereco = row.CONEndereco;
                                rowS.CONEndereco2 = row.CONEndereco2;
                                rowS.CIDNome = row.CIDNome;
                                rowS.CIDUf = row.CIDUf;

                                rowS.Locatario = "";
                                rowS.Proprietario = dSeguros.Sindico[0].PESNome;
                                rowS.IdentificaCON = row.IdentificaCON;
                                if (dSeguros.Sindico[0].BLOCodigo.ToUpper().Trim() != "SB")
                                    rowS.IdentificaAPT = string.Format("{0} - {1}", dSeguros.Sindico[0].BLOCodigo, dSeguros.Sindico[0].APTNumero);
                                else
                                    rowS.IdentificaAPT = string.Format("{0}", dSeguros.Sindico[0].APTNumero);
                                rowS.APTNumero = dSeguros.Sindico[0].APTNumero;
                                rowS.BLOCodigo = dSeguros.Sindico[0].BLOCodigo;

                                Sindicos.Add(rowS);
                            }
                        }
                        else
                            if (CON != 0)
                                geraCheques[CON] += row.BODValor;
                    }
                    
                    foreach (dSeguros.SeguroPagoRow rowS in Sindicos)
                    {
                        dSeguros.SeguroPago.AddSeguroPagoRow(rowS);
                    };

                    //Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(1,2013);
                    ContaPagar.dNOtAs dNOtAs = new ContaPagar.dNOtAs();
                    int? SPL = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT SPL FROM SubPLano where SPL_PLA = @P1","920001");
                    if (!SPL.HasValue)
                    {
                        string comando =
"INSERT INTO SubPLano\r\n" +
"                      (SPL_PLA, SPLCodigo, SPLDescricao, SPLServico, SPLNoCondominio, SPLReterISS, SPLValorISS)\r\n" +
"VALUES     ('920001', '0', 'Repasse Seguro', 1, 1, 0, 2);";
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando);
                        SPL = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT SPL FROM SubPLano where SPL_PLA = @P1", "920001");
                    }

                    int FRNNeon = (Framework.DSCentral.EMP == 1) ? 1119 : 172;                    

                    string Favorecido;
                    VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("SELECT FRNNome FROM FORNECEDORES WHERE (FRN = @P1)", out Favorecido, FRNNeon);

                    foreach (int CONch in geraCheques.Keys)
                        dNOtAs.IncluiNotaSimples(0, DateTime.Today, DateTime.Today.AddDays(10), cCompet1.Comp, NOATipo.Repasse, Favorecido, CONch, PAGTipo.cheque, true, "920001", SPL.Value, "Seguro Conteúdo", geraCheques[CONch], FRNNeon);

                    ArquivosTXT.cTabelaTXT geraArquivo = new ArquivosTXT.cTabelaTXT(dSeguros.SeguroPago, ArquivosTXT.ModelosTXT.delimitado, ArquivosTXT.TipoMapeamento.manual);
                    geraArquivo.Separador = new char[] { ';' };
                    geraArquivo.GravaSeparadorFinal = false;
                    geraArquivo.DefaultTamanhoVariavel = true;
                    geraArquivo.GravarCamposNaPrimeiraLinha = true;
                    geraArquivo.Formatodata = ArquivosTXT.FormatoData.dd_MM_YYYY;

                    geraArquivo.EquivalenciaDeNomes.Add("CONEndereco2", "ENDERECO RISCO");
                    geraArquivo.EquivalenciaDeNomes.Add("CONNumero", "NUMERO ENDERECO");
                    geraArquivo.EquivalenciaDeNomes.Add("APT", "");
                    geraArquivo.EquivalenciaDeNomes.Add("CONCep", "CEP");
                    geraArquivo.EquivalenciaDeNomes.Add("CIDNome", "CIDADE");
                    geraArquivo.EquivalenciaDeNomes.Add("CIDUF", "UF");
                    geraArquivo.EquivalenciaDeNomes.Add("IdentificaAPT", "ECONOMIA");

                    geraArquivo.RegistraMapa("CODIGO IDENTIFICADOR", 1, 0);
                    geraArquivo.RegistraMapa("PROPRIETARIO", 2, 0);
                    geraArquivo.RegistraMapa("LOCATARIO", 3, 0);
                    geraArquivo.RegistraMapa("PRODUTO", 4, 0, ArquivosTXT.TipoMapa.detalhe, "R");
                    geraArquivo.RegistraMapa("TIPO LOGRADOURO", 5, 0, ArquivosTXT.TipoMapa.detalhe, "Rua");
                    geraArquivo.RegistraMapa("CONEndereco2", 6, 0);
                    geraArquivo.RegistraMapa("CONNumero", 7, 0);
                    //geraArquivo.RegistraMapa("ECONOMIA", 8, 0, ArquivosTXT.TipoMapa.detalhe, 0);
                    geraArquivo.RegistraMapa("IdentificaAPT", 8, 0);
                    geraArquivo.RegistraMapa("CONBairro", 9, 0);
                    geraArquivo.RegistraMapa("CONCep", 10, 0);
                    geraArquivo.RegistraMapa("CIDNome", 11, 0);
                    geraArquivo.RegistraMapa("CIDUF", 12, 0, ArquivosTXT.TipoMapa.detalhe, "SP");
                    geraArquivo.RegistraMapa("VIGENCIA INICIAL", 13, 0, ArquivosTXT.TipoMapa.detalhe, DataI);
                    geraArquivo.RegistraMapa("VIGENCIA FINAL", 14, 0, ArquivosTXT.TipoMapa.detalhe, DataF);
                    geraArquivo.RegistraMapa("E-MAIL CORRETORA", 15, 0, ArquivosTXT.TipoMapa.detalhe, "landia@sicura.com.br");
                    geraArquivo.RegistraMapa("IMPORTANCIA SEGURADA", 16, 0);
                    //geraArquivo.RegistraMapa("", 5, 0, ArquivosTXT.TipoMapa.detalhe, "");
                    geraArquivo.RegistraMapa("IMOBILIÁRIA", 18, 0, ArquivosTXT.TipoMapa.detalhe, "Neon Imóveis");
                    geraArquivo.Salva(SF.FileName);

                    LimparBoletos(DataI, ESP,dSeguros);
                    
                };
                MessageBox.Show("Arquivo Gerado");
            }
            
        }
        */

        private void Levantamento()
        {

            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                try
                {
                    gridView1.BeginDataUpdate();
                    ESP.Espere("Levantando dados");
                    dSeguros.SeguroPagoTableAdapter.Fill(dSeguros.SeguroPago, dataI.DateTime, dataF.DateTime);
                    //int CON = 0;
                    List<dSeguros.SeguroPagoRow> Sindicos = new List<dSeguros.SeguroPagoRow>();
                    List<int> APTs = new List<int>();
                    SortedList<int, dSeguros.SeguroPagoRow> CONs = new SortedList<int, dSeguros.SeguroPagoRow>();
                    foreach (dSeguros.SeguroPagoRow row in dSeguros.SeguroPago)
                    {
                        if (!CONs.ContainsKey(row.CON))
                            CONs.Add(row.CON, row);
                        APTs.Add(row.APT);
                        //string testeCEP = CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(row.CONCep, CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.SoNumeros);
                        row.CONCep = row.CONCep.Limpa(FuncoesBasicasProc.TiposLimpesa.SoNumeros);
                        //if (row.CONCep != testeCEP)
                        //    throw new Exception("validador");
                        if (row.BLOCodigo.ToUpper().Trim() != "SB")
                            row.IdentificaAPT = string.Format("{0} - {1}", row.BLOCodigo, row.APTNumero);
                        else
                            row.IdentificaAPT = string.Format("{0}", row.APTNumero);
                        string CodigoFolha = "";
                        if (!row.IsCONCodigoFolha1Null())
                            CodigoFolha = string.Format("{0} - ", row.CONCodigoFolha1);
                        row.IdentificaCON = string.Format("{0}{1} - {2}", CodigoFolha, row.CONCodigo, row.CONNome);


                    }

                    foreach (int CON in CONs.Keys)
                    {

                        if (dSeguros.SindicoTableAdapter.Fill(dSeguros.Sindico, CON) > 0)
                        {
                            dSeguros.SeguroPagoRow row = CONs[CON];
                            if (APTs.Contains(dSeguros.Sindico[0].APT))
                                continue;
                            dSeguros.SeguroPagoRow rowS = dSeguros.SeguroPago.NewSeguroPagoRow();
                            rowS.BOD_PLA = "";
                            rowS.BOD = 0;
                            rowS.BODValor = dSeguros.Sindico[0].PSCValor;
                            rowS.BOL = 0;
                            rowS.BOLVencto = row.BOLVencto;
                            rowS.BOLTipoCRAI = "B";
                            rowS.CON = row.CON;
                            rowS.CONBairro = row.CONBairro;
                            rowS.CONCep = row.CONCep;
                            rowS.CONCodigo = row.CONCodigo;
                            rowS.CONCodigoFolha1 = row.CONCodigoFolha1;
                            rowS.CONNumero = row.CONNumero;
                            rowS.CON_CID = row.CON_CID;
                            rowS.CONCodigo = row.CONCodigo;
                            rowS.CONNome = row.CONNome;
                            rowS.CONEndereco = row.CONEndereco;
                            rowS.CONEndereco2 = row.CONEndereco2;
                            rowS.CIDNome = row.CIDNome;
                            rowS.CIDUf = row.CIDUf;

                            rowS.Locatario = "";
                            rowS.Proprietario = dSeguros.Sindico[0].PESNome;
                            rowS.IdentificaCON = row.IdentificaCON;
                            if (dSeguros.Sindico[0].BLOCodigo.ToUpper().Trim() != "SB")
                                rowS.IdentificaAPT = string.Format("{0} - {1}", dSeguros.Sindico[0].BLOCodigo, dSeguros.Sindico[0].APTNumero);
                            else
                                rowS.IdentificaAPT = string.Format("{0}", dSeguros.Sindico[0].APTNumero);
                            rowS.APTNumero = dSeguros.Sindico[0].APTNumero;
                            rowS.BLOCodigo = dSeguros.Sindico[0].BLOCodigo;

                            Sindicos.Add(rowS);
                        }
                    }

                    foreach (dSeguros.SeguroPagoRow rowS in Sindicos)
                    {
                        dSeguros.SeguroPago.AddSeguroPagoRow(rowS);
                    };
                }
                finally
                {
                    gridView1.EndDataUpdate();
                }
            };


        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Competencia CompetenciaHoje = new Competencia(DateTime.Today.AddDays(3));
            if (cCompet1.Comp <= CompetenciaHoje)
            {
                Levantamento();
                BPlanilha.Enabled = (dSeguros.SeguroPago.Count > 0);
                BArquivo.Enabled = (dSeguros.SeguroPago.Count > 0);
            }
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            cCompet1_OnChange();
        }

        private void cCompet1_OnChange()
        {
            dataVI.DateTime = new DateTime(cCompet1.Comp.Ano, cCompet1.Comp.Mes, 1);
            dataVF.DateTime = dataVI.DateTime.AddMonths(1).AddDays(-1);
            dataI.DateTime = dataVI.DateTime.AddMonths(-1);
            dataF.DateTime = dataI.DateTime.AddMonths(1).AddDays(-1);            
            dSeguros.SeguroPago.Clear();
            BArquivo.Enabled = BPlanilha.Enabled = false;
            dSeguros.BOLetosTipoS.Clear();
            dSeguros.APTSeguroExterno.Clear();
            BtEmitir.Enabled = false;
            repositoryBAvulso.Buttons[0].Enabled = false;
            ValidaCampanha();
            dSeguros.CAmpanhaSeguro.Clear();
        }

        private void ValidaCampanha()
        {
            if ((lookupCondominio_F2.CON_sel == -1) || (lookUpEditPSCCamp.EditValue == null))
            {
                SBCampanha.Enabled = false;
                return;
            }
            Competencia CompetenciaHoje = new Competencia(DateTime.Today.AddDays(3));
            if (cCompet1.Comp <= CompetenciaHoje)
            {
                SBCampanha.Enabled = false;
                return;
            }
            SBCampanha.Enabled = true;
        }

        private void cSeguros_Load(object sender, EventArgs e)
        {
            cCompet1.Comp = new Framework.objetosNeon.Competencia(DateTime.Today);
            cCompet1.AjustaTela();
            cCompet1_OnChange();
        }

        private void CadastraCheque(CompontesBasicos.Espera.cEspera ESP)
        {
            int i = 0;
            ESP.Espere("Verificando Cheques");
            BoletosProc.Boleto.dBoletos dBoletosSeg = new BoletosProc.Boleto.dBoletos();
            dBoletosSeg.BOLetosTableAdapter.FillBySeguroSemCheque(dBoletosSeg.BOLetos, dataI.DateTime, dataF.DateTime);
            dBoletosSeg.BOletoDetalheTableAdapter.FillBySeguroPagoSemCheque(dBoletosSeg.BOletoDetalhe, dataI.DateTime, dataF.DateTime);
            ESP.AtivaGauge(dBoletosSeg.BOLetos.Count);
            foreach (BoletosProc.Boleto.dBoletos.BOLetosRow rowSEG in dBoletosSeg.BOLetos)
            {
                i++;
                Console.WriteLine(string.Format("i={0}/{1}", i, dBoletosSeg.BOLetos.Count));
                ESP.GaugeTempo(i);
                Boleto.Boleto Bol = new Boleto.Boleto(rowSEG);
                if (Bol.Encontrado)
                {
                    Bol.RegistraChSeguro();
                    Console.WriteLine(string.Format("Cadastrado cheque para BOL = {0}", rowSEG.BOL));
                }
            }
        }

        private void CadastraPagante()
        {
            string comando =
"UPDATE    APARTAMENTOS\r\n" +
"SET              APT_PSC = PlanosSeguroConteudo.PSC\r\n" +
"FROM         BOLetos INNER JOIN\r\n" +
"                      APARTAMENTOS ON BOLetos.BOL_APT = APARTAMENTOS.APT INNER JOIN\r\n" +
"                      BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO INNER JOIN\r\n" +
"                      CONDOMINIOS ON BLOCOS.BLO_CON = CONDOMINIOS.CON INNER JOIN\r\n" +
"                      PlanosSeguroConteudo ON BOLetos.BOLValorPrevisto = PlanosSeguroConteudo.PSCValor\r\n" +
"WHERE     (BOLetos.BOLTipoCRAI = 'S') AND (NOT (BOLetos.BOLPagamento IS NULL)) AND (BOLetos.BOLCompetenciaAno = @P1) AND (BOLetos.BOLCompetenciaMes = @P2) AND \r\n" +
"                      (CONDOMINIOS.CON_PSC IS NULL) AND (APARTAMENTOS.APT_PSC IS NULL);";
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
        }

        private void BArquivo_Click(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                CadastraCheque(ESP);                
            }
            GeraArquivo();
            CadastraPagante();
        }

        private void GeraArquivo()
        {
            if (dSeguros.SeguroPago.Count == 0)
                return;
            SaveFileDialog SF = new SaveFileDialog();
            SF.DefaultExt = ".csv";
            if (SF.ShowDialog() == DialogResult.OK)
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Gerando Arquivo");
                    Application.DoEvents();
                    ArquivosTXT.cTabelaTXT geraArquivo = new ArquivosTXT.cTabelaTXT(dSeguros.SeguroPago, ArquivosTXT.ModelosTXT.delimitado, ArquivosTXT.TipoMapeamento.manual);
                    geraArquivo.Separador = new char[] { ';' };
                    geraArquivo.GravaSeparadorFinal = false;
                    geraArquivo.DefaultTamanhoVariavel = true;
                    geraArquivo.GravarCamposNaPrimeiraLinha = true;
                    geraArquivo.Formatodata = ArquivosTXT.FormatoData.dd_MM_YYYY;


                    geraArquivo.EquivalenciaDeNomes.Add("CONEndereco2", "ENDERECO RISCO");
                    geraArquivo.EquivalenciaDeNomes.Add("CONNumero", "NUMERO ENDERECO");
                    geraArquivo.EquivalenciaDeNomes.Add("APT", "");
                    geraArquivo.EquivalenciaDeNomes.Add("CONCep", "CEP");
                    geraArquivo.EquivalenciaDeNomes.Add("CIDNome", "CIDADE");
                    geraArquivo.EquivalenciaDeNomes.Add("CIDUF", "UF");
                    geraArquivo.EquivalenciaDeNomes.Add("IdentificaAPT", "ECONOMIA");

                    geraArquivo.RegistraMapa("CODIGO IDENTIFICADOR", 1, 0);
                    geraArquivo.RegistraMapa("PROPRIETARIO", 2, 0);
                    geraArquivo.RegistraMapa("LOCATARIO", 3, 0);
                    geraArquivo.RegistraMapa("PRODUTO", 4, 0, ArquivosTXT.TipoMapa.detalhe, "R");
                    geraArquivo.RegistraMapa("TIPO LOGRADOURO", 5, 0, ArquivosTXT.TipoMapa.detalhe, "Rua");
                    geraArquivo.RegistraMapa("CONEndereco2", 6, 0);
                    geraArquivo.RegistraMapa("CONNumero", 7, 0);
                    geraArquivo.RegistraMapa("IdentificaAPT", 8, 0);
                    geraArquivo.RegistraMapa("CONBairro", 9, 0);
                    geraArquivo.RegistraMapa("CONCep", 10, 0);
                    geraArquivo.RegistraMapa("CIDNome", 11, 0);
                    geraArquivo.RegistraMapa("CIDUF", 12, 0, ArquivosTXT.TipoMapa.detalhe, "SP");
                    geraArquivo.RegistraMapa("VIGENCIA INICIAL", 13, 0, ArquivosTXT.TipoMapa.detalhe, dataVI.DateTime);
                    geraArquivo.RegistraMapa("VIGENCIA FINAL", 14, 0, ArquivosTXT.TipoMapa.detalhe, dataVF.DateTime);
                    geraArquivo.RegistraMapa("E-MAIL CORRETORA", 15, 0, ArquivosTXT.TipoMapa.detalhe, "landia@sicura.com.br");
                    geraArquivo.RegistraMapa("IMPORTANCIA SEGURADA", 16, 0);
                    geraArquivo.RegistraMapa("IMOBILIÁRIA", 18, 0, ArquivosTXT.TipoMapa.detalhe, "Neon Imóveis");
                    geraArquivo.RegistraMapa("calcBODVALOR", 19, 0);
                    geraArquivo.Salva(SF.FileName);
                    LimparBoletos(dataVI.DateTime, ESP, dSeguros);
                };

                MessageBox.Show("Arquivo Gerado");
            }
        }

        private void BPlanilha_Click(object sender, EventArgs e)
        {
            if (dSeguros.SeguroPago.Count == 0)
                return;
            SaveFileDialog SF = new SaveFileDialog();
            SF.DefaultExt = ".xlsx";
            if (SF.ShowDialog() == DialogResult.OK)
            {
                gridView1.ExportToXlsx(SF.FileName, new DevExpress.XtraPrinting.XlsxExportOptions(DevExpress.XtraPrinting.TextExportMode.Text, false));
                //gridView1.ExportToXlsx(SF.FileName, new DevExpress.XtraPrinting.XlsxExportOptions(DevExpress.XtraPrinting.TextExportMode.Value, true));
                try
                {
                    using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                    {
                        process.StartInfo.FileName = SF.FileName;
                        process.StartInfo.Verb = "Open";
                        process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                        process.Start();
                    }
                }
                catch
                {
                    MessageBox.Show(this, "Não foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private int? _CONseg;

        private int CONseg
        {
            get
            {
                if (!_CONseg.HasValue)
                {
                    _CONseg = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CON from condominios where CONCodigo = 'SEGCON'");
                    if (!_CONseg.HasValue)
                    {
                        string comando =
"INSERT INTO CONDOMINIOS\r\n" +
"                      (CONCodigo, CONNome, CONEndereco, CONBairro, CON_CID,CON_EMP,CONStatus,CONRelAcordoDet,\r\n" +
"                      CONEmiteBoletos,CONSefip,CONNotaAutomaica)\r\n" +
"VALUES     ('SEGCON', 'Neon/Sicura Corretora','','', 366,1,1,0,0,0,0);";
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando);
                    }
                }
                return _CONseg.Value;
            }
        }

        //private SortedList<int,Boleto.Boleto> NovosBoletos;

        private void EmiteBoletosS(CompontesBasicos.Espera.cEspera ESP,DateTime Vencimento,decimal? Valor = null)
        {
            ESP.Espere("Gerando boletos");
            ESP.AtivaGauge(dSeguros.APTSeguroExterno.Count);
            int pos = 0;
            foreach (dSeguros.APTSeguroExternoRow rowcandidato in dSeguros.APTSeguroExterno)
            {
                ESP.GaugeTempo(pos++);
                if (rowcandidato.GetBOLetosTipoSRows().Length == 0)
                {
                    if (Valor.HasValue)
                        GeraUmBoleto(rowcandidato, Valor.Value, Vencimento, true, 1);
                    else
                        GeraUmBoleto(rowcandidato, rowcandidato.PSCValor, Vencimento, false, 1);
                }
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (dataF.DateTime < DateTime.Today)
            {
                MessageBox.Show("Boletos de seguro não podem ser emitido após o vencimento");
                return;
            }
            if (dataF.DateTime.AddDays(-5) < DateTime.Today)
            {
                if (MessageBox.Show("Atenção, menos de 5 dias para o vencimento", "Confirmação", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Cancel)
                    return;
            }
            
            DateTime? Vencimento = dataF.DateTime.AddDays(-5);
            if (VirInput.Input.Execute("Vencimento", ref Vencimento, DateTime.Today.AddDays(3), dataF.DateTime.AddDays(-5)))
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    if (dBoletos1 == null)
                    {
                        dBoletos1 = new BoletosProc.Boleto.dBoletos();
                        ESP.Espere("Pre-Carga Apartamentos");
                        Application.DoEvents();
                        dBoletos1.CarregaAPTSeguro(false);
                    }
                    ESP.Espere("Gerando boletos");
                    Application.DoEvents();
                    EmiteBoletosS(ESP,Vencimento.Value);
                    Carregar();
                }
            }
        }



        private void Carregar()
        {
            dSeguros.BOLetosTipoS.Clear();            
            dSeguros.APTSeguroExternoTableAdapter.FillByAtivos(dSeguros.APTSeguroExterno);
            dSeguros.BOLetosTipoSTableAdapter.Fill(dSeguros.BOLetosTipoS, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);            
            for (int cursor = 0; cursor < dSeguros.APTSeguroExterno.Count; cursor++)
            {                
                dSeguros.APTSeguroExternoRow row = (dSeguros.APTSeguroExternoRow)gridView2.GetDataRow(cursor);
                row.Imprimir = (row.GetBOLetosTipoSRows().Length > 0);                
            }                
            if (dataF.DateTime >= DateTime.Today)
            {
                BtEmitir.Enabled = true;
                repositoryBAvulso.Buttons[0].Enabled = true;
            }
            gridView2.SelectAll();            
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Carregar();            
        }
        

        private void lookupCondominio_F1_Click(object sender, EventArgs e)
        {
            cCompet1_OnChange();
        }

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            cCompet1_OnChange();
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            cCompet1_OnChange();
        }

        

        private BoletosProc.Boleto.dBoletos dBoletos1;

        private void ImpressaoS(CompontesBasicos.Espera.cEspera ESP)
        {
            ImpSegundaVia ImpressosTipoS = new ImpSegundaVia();
            ImpressosTipoS.xrTitulo.Text = "Seguro Conteúdo";
            bool Cancelar = true;
            int pos = 0;
            //Precarga dos boletos para melhorar desempenho                        
            if (dBoletos1 == null)
                dBoletos1 = new BoletosProc.Boleto.dBoletos();
            ESP.Espere("Pre-Carga Apartamentos");
            dBoletos1.CarregaAPTSeguro(false);
            ESP.Espere("Pre-Carga Boletos 1/2");
            Application.DoEvents();
            dBoletos1.BOLetosTableAdapter.FillByTipoS(dBoletos1.BOLetos, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);
            ESP.Espere("Pre-Carga Boletos 2/2");
            Application.DoEvents();
            dBoletos1.BOletoDetalheTableAdapter.FillByTipoS(dBoletos1.BOletoDetalhe, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);
            ESP.Espere("Preparando impressos 1/2");
            Application.DoEvents();
            ESP.AtivaGauge(dSeguros.APTSeguroExterno.Count);
            //foreach (dSeguros.APTSeguroExternoRow row in dSeguros.APTSeguroExterno)            
            for (int cursor = 0; cursor < dSeguros.APTSeguroExterno.Count; cursor++)
            {
                dSeguros.APTSeguroExternoRow row = (dSeguros.APTSeguroExternoRow)gridView2.GetDataRow(cursor);
                if (!gridView2.IsRowSelected(cursor))
                    continue;
                ESP.GaugeTempo(pos++);
                if (row.Imprimir)
                {
                    if (row.GetBOLetosTipoSRows().Length != 0)
                    {
                        Boleto.Boleto Bol;
                        if (row.GetBOLetosTipoSRows()[0].IsBOLPagamentoNull())
                        {

                            Bol = new Boleto.Boleto(dBoletos1.BOLetos.FindByBOL(row.GetBOLetosTipoSRows()[0].BOL));
                            if (Bol.rowPrincipal.IsBOLPagamentoNull())
                                Bol.CarregaLinha(ImpressosTipoS, true, false);
                            CompontesBasicosProc.Performance.Performance.PerformanceST.StackPop();
                            Cancelar = false;
                        }
                    }
                }
            }
            if (!Cancelar)
            {
                ESP.Espere("Preparando impressos 2/2");
                ImpressosTipoS.CreateDocument();
                ImpressosTipoS.Print();
            }
        }

        private void ImpressaoCampanha(CompontesBasicos.Espera.cEspera ESP)
        {
            ImpBoletoSeguro ImpressosTipoS = new ImpBoletoSeguro();
            ImpressosTipoS.CarregarImpresso(string.Format("Seguro Conteúdo - {0}", rowPSC.PSC));            
            bool Cancelar = true;
            int pos = 0;
            //Precarga dos boletos para melhorar desempenho                        
            if (dBoletos1 == null)
                dBoletos1 = new BoletosProc.Boleto.dBoletos();            
            ESP.Espere("Pre-Carga Boletos 1/2");
            dBoletos1.BOLetosTableAdapter.FillByTipoS(dBoletos1.BOLetos, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);
            ESP.Espere("Pre-Carga Boletos 2/2");
            Application.DoEvents();
            dBoletos1.BOletoDetalheTableAdapter.FillByTipoS(dBoletos1.BOletoDetalhe, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);
            ESP.Espere("Preparando impressos 1/2");
            Application.DoEvents();
            ESP.AtivaGauge(dSeguros.APTSeguroExterno.Count);
            foreach (dSeguros.APTSeguroExternoRow row in dSeguros.APTSeguroExterno)                        
            {                
                ESP.GaugeTempo(pos++);
                
                    if (row.GetBOLetosTipoSRows().Length != 0)
                    {
                        Boleto.Boleto Bol;
                        if (row.GetBOLetosTipoSRows()[0].IsBOLPagamentoNull())
                        {
                            Bol = new Boleto.Boleto(dBoletos1.BOLetos.FindByBOL(row.GetBOLetosTipoSRows()[0].BOL));
                            if (Bol.rowPrincipal.IsBOLPagamentoNull())
                                Bol.CarregaLinha(ImpressosTipoS, true, false);
                            CompontesBasicosProc.Performance.Performance.PerformanceST.StackPop();
                            Cancelar = false;
                        }
                    }
                
            }
            if (!Cancelar)
            {
                ESP.Espere("Preparando impressos 2/2");
                ImpressosTipoS.CreateDocument();
                ImpressosTipoS.Print();
            }
        }

        /*
        private void ImpressaoNovoS(CompontesBasicos.Espera.cEspera ESP)
        {
            Impressos = new SortedList<int, ImpBoletoSeguro>();                        
            bool Cancelar = true;
            ESP.Espere("Preparando impressos (Novos) 1/2");
            ESP.AtivaGauge(NovosBoletos.Count);
            int pos = 0;
            foreach (Boleto.Boleto Bol in NovosBoletos.Values)
            {
                ESP.GaugeTempo(pos++);
                dSeguros.APTSeguroExternoRow rowAPTSeguroExterno = dSeguros.APTSeguroExterno.FindByAPT(Bol.APT.Value);
                if (!Impressos.ContainsKey(rowAPTSeguroExterno.APT_PSC))
                {
                    Impressos.Add(rowAPTSeguroExterno.APT_PSC, new ImpBoletoSeguro());
                    Impressos[rowAPTSeguroExterno.APT_PSC].CarregarImpresso(string.Format("Seguro Conteúdo - {0}", rowAPTSeguroExterno.APT_PSC));
                }
                Bol.CarregaLinha(Impressos[rowAPTSeguroExterno.APT_PSC], true, false);                                
                Cancelar = false;
            }
            if (!Cancelar)
            {
                ESP.Espere("Preparando impressos (Novos) 2/2");
                foreach (ImpBoletoSeguro Imp in Impressos.Values)
                {
                    Imp.CreateDocument();
                    Imp.Print();
                }
                
            }
        }*/

        private void simpleButton3_Click_1(object sender, EventArgs e)
        {                        
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ImpressaoS(ESP);                
            }
        }

        private void chCadastrados_CheckedChanged(object sender, EventArgs e)
        {
            cCompet1_OnChange();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

        }

        private List<DataColumn> novos;
        private List<DevExpress.XtraGrid.Columns.GridColumn> novosDev;

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (novos != null)
            {
                foreach (DataColumn DC in novos)
                    dSeguroAc.Geral.Columns.Remove(DC);
                foreach (DevExpress.XtraGrid.Columns.GridColumn CD in novosDev)
                    gridView4.Columns.Remove(CD);

            }
            novos = new List<DataColumn>();
            novosDev = new List<DevExpress.XtraGrid.Columns.GridColumn>();

            dSeguroAc.GeralTableAdapter.Fill(dSeguroAc.Geral);
            dSeguroAc.DetalheTableAdapter.Fill(dSeguroAc.Detalhe);
            DateTime? DI = dSeguroAc.DetalheTableAdapter.Minimo();
            DateTime? DF = dSeguroAc.DetalheTableAdapter.Maximo();
            Competencia compI = new Competencia(DI.Value).Add(1);
            Competencia compF = new Competencia(DF.Value).Add(1);
            for (Competencia compX = compI; compX <= compF; compX++)
            {
                novos.Add(dSeguroAc.Geral.Columns.Add(compX.ToString(), typeof(decimal)));
                DevExpress.XtraGrid.Columns.GridColumn nova = new DevExpress.XtraGrid.Columns.GridColumn();
                nova.Caption = compX.ToString();
                nova.DisplayFormat.FormatString = "n2";
                nova.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                nova.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.None;
                nova.Name = compX.ToString();
                nova.OptionsColumn.ReadOnly = true;
                nova.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                                                     //new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, compX.ToString(), "{0:n2}"),
                                                     new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, compX.ToString(), "{0:n2}")});
                nova.Visible = true;
                nova.FieldName = compX.ToString();
                if (compX == cCompet1.Comp)
                {
                    nova.AppearanceCell.BackColor = Color.Aqua;
                    nova.AppearanceCell.Options.UseBackColor = true;
                }
                gridView4.Columns.Add(nova);
                novosDev.Add(nova);
            }
            foreach (dSeguroAc.GeralRow row in dSeguroAc.Geral)
            {
                foreach (dSeguroAc.DetalheRow rowD in row.GetDetalheRows())
                {
                    if (!rowD.IsBOLPagamentoNull())
                    {
                        Competencia compX = new Competencia(rowD.BOLVencto).Add(1);
                        if (row[compX.ToString()] == DBNull.Value)
                            row[compX.ToString()] = rowD.BODValor;
                        else
                            row[compX.ToString()] = (decimal)row[compX.ToString()] + rowD.BODValor;
                    }
                }
            }
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (dSeguroAc.Geral.Count == 0)
                return;
            SaveFileDialog SF = new SaveFileDialog();
            SF.DefaultExt = ".xlsx";
            if (SF.ShowDialog() == DialogResult.OK)
            {
                gridView4.ExportToXlsx(SF.FileName, new DevExpress.XtraPrinting.XlsxExportOptions(DevExpress.XtraPrinting.TextExportMode.Text, false));
                //gridView1.ExportToXlsx(SF.FileName, new DevExpress.XtraPrinting.XlsxExportOptions(DevExpress.XtraPrinting.TextExportMode.Value, true));
                try
                {
                    using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                    {
                        process.StartInfo.FileName = SF.FileName;
                        process.StartInfo.Verb = "Open";
                        process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                        process.Start();
                    }
                }
                catch
                {
                    MessageBox.Show(this, "Não foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        
        private Framework.datasets.dPlanosSeguroConteudo.PlanosSeguroConteudoRow rowPSC
        {
            get
            {
                if (lookUpEditPSCCamp.EditValue == null)
                    return null;
                else
                    return Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt.PlanosSeguroConteudo.FindByPSC((int)lookUpEditPSCCamp.EditValue);
            }
        }
        
        
        private Boleto.Boleto ImprimeBoletoMultiplo(dSeguros.APTSeguroExternoRow row,int BOL, decimal Valor , int meses)
        {                        
            try
            {            
                Valor = row.PSCValor;
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos cSeguros - 766");                

                Boleto.Boleto novoBol = new Boleto.Boleto(BOL);
                novoBol.rowPrincipal.BOLValorOriginal = Valor * meses;

                string Mem = string.Format("BOLETO PAGÁVEL ATÉ O VENCIMENTO\r\n" +
                                           "NÃO RECEBER APÓS O VENCIMENTO\r\n\r\n" +
                                           "PARA PAGAMENTO MEMSAL COLOCAR NO VALOR TOTAL R$ {0:n2}\r\n" +
                                           "PARA PAGAMENTO SEMESTRAL COLOCAR NO VALOR TOTAL R$ {1:n2}\r\n" +
                                           "PARA PAGAMENTO ANUAL COLOCAR NO VALOR TOTAL R$ {2:n2}",
                                            Valor,
                                            Valor * 6,
                                            Valor * 12);
                novoBol.rowPrincipal.BOLMensagem = Mem;

                novoBol.SalvarAlteracoesSemRegistrar();

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return novoBol;
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                throw ex;
            }
        }
        
        private Boleto.Boleto GeraUmBoleto(dSeguros.APTSeguroExternoRow row, decimal Valor, DateTime Vencimento, bool Campanha, int meses)
        {
            string PLA = PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoC);
            string Mensagem = string.Format("Seguro conteúdo ({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", dataVI.DateTime, dataVF.DateTime);
            try
            {
                //decimal Valor = rowPSC.PSCValor;
                //Valor = row.PSCValor;
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos cSeguros - 756", dSeguros.CAmpanhaSeguroTableAdapter);
                BoletosProc.Boleto.dBoletos.BOletoDetalheRow novoBOD = Boleto.Boleto.GerarBOD(CONseg, 
                                                                                              row.APT, 
                                                                                              row.IsAPTInquilino_PESNull(),
                                                                                              PLA, 
                                                                                              false, 
                                                                                              cCompet1.Comp, 
                                                                                              dataF.DateTime, 
                                                                                              Mensagem, 
                                                                                              Valor, 
                                                                                              true);

                ABS_Apartamento Apartamento;
                if ((dBoletos1 != null) && (dBoletos1.Apartamentos.ContainsKey(row.APT)))
                    Apartamento = dBoletos1.Apartamentos[row.APT];
                else
                    Apartamento = ABS_Apartamento.GetApartamento(row.APT);
                Boleto.Boleto novoBol = new Boleto.Boleto(CONseg, Apartamento, row.IsAPTInquilino_PESNull(), cCompet1.Comp, Vencimento, "S",
                                                          StatusBoleto.Valido, novoBOD.BOD);
                novoBol.rowPrincipal.BOLValorOriginal = Valor * meses;

                string Mem = string.Format("BOLETO PAGÁVEL ATÉ O VENCIMENTO\r\n"+
                                           "NÃO RECEBER APÓS O VENCIMENTO\r\n\r\n"+
                                           "PARA PAGAMENTO MEMSAL COLOCAR NO VALOR TOTAL R$ {0:n2}\r\n" +
                                           "PARA PAGAMENTO SEMESTRAL COLOCAR NO VALOR TOTAL R$ {1:n2}\r\n" +
                                           "PARA PAGAMENTO ANUAL COLOCAR NO VALOR TOTAL R$ {2:n2}",
                                            Valor,
                                            Valor * 6,
                                            Valor * 12);
                novoBol.rowPrincipal.BOLMensagem = Mem;

                novoBol.SalvarAlteracoesSemRegistrar();

                if (Campanha)
                {
                    dSeguros.CAmpanhaSeguroRow novaRow = dSeguros.CAmpanhaSeguro.NewCAmpanhaSeguroRow();
                    novaRow.CASCompet = cCompet1.Comp.CompetenciaBind;                    
                    novaRow.CAS_APT = row.APT;
                    novaRow.CAS_BOL = novoBol.BOL;                    
                    novaRow.CAS_PSC = rowPSC.PSC;
                    dSeguros.CAmpanhaSeguro.AddCAmpanhaSeguroRow(novaRow);
                    dSeguros.CAmpanhaSeguroTableAdapter.Update(novaRow);
                }

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return novoBol;
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                throw ex;
            }
        }

        private void repositoryBAvulso_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {            
            dSeguros.APTSeguroExternoRow row = (dSeguros.APTSeguroExternoRow)gridView2.GetFocusedDataRow();
            if (row == null)
                return;
            //if (lookUpEditPSC.EditValue == null)
            //{
            //    MessageBox.Show("Valor do seguro não selecionado");
            //    return;
            //}
            int meses = 12;
            if (!VirInput.Input.Execute("mêses", out meses, 12))
                return;

            object oTipo;
            dllImpresso.Botoes.Botao Tipo;
            System.Collections.SortedList Alternativas = new System.Collections.SortedList();
            Alternativas.Add(dllImpresso.Botoes.Botao.email, "e-mail");
            Alternativas.Add(dllImpresso.Botoes.Botao.imprimir, "impresso");
            Alternativas.Add(dllImpresso.Botoes.Botao.tela, "Na tela");
            Alternativas.Add(dllImpresso.Botoes.Botao.nulo, "Somente gerar");
            Alternativas.Add(dllImpresso.Botoes.Botao.pdf, "pdf");
            Alternativas.Add(dllImpresso.Botoes.Botao.imprimir_frente, "impresso somente frente");
            Alternativas.Add(dllImpresso.Botoes.Botao.PDF_frente, "pdf somente frente");
            if (!VirInput.Input.Execute("Tipo de impresso", Alternativas, out oTipo, VirInput.Formularios.cRadioCh.TipoSelecao.radio))
                return;
            Tipo = (dllImpresso.Botoes.Botao)oTipo;
            dSeguros.BOLetosTipoSRow[] rowS = row.GetBOLetosTipoSRows();
            Boleto.Boleto Novo;
            if (rowS.Length != 0)            
                Novo = ImprimeBoletoMultiplo(row, rowS[0].BOL, row.PSCValor, meses);             
            else                            
                Novo = GeraUmBoleto(row, row.PSCValor, dataF.DateTime.AddDays(-5),false, meses);                                            
            Novo.Imprimir(Tipo);
            int foco = gridView2.FocusedRowHandle;
            Carregar();
            gridView2.FocusedRowHandle = foco;
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ValidaCampanha();
        }

        private void lookupCondominio_F2_alterado(object sender, EventArgs e)
        {
            ValidaCampanha();
        }

        /*
        private void CarregaBoletosJaEmitidos()
        {
            dSeguros.BOLetosTipoSTableAdapter.Fill(dSeguros.BOLetosTipoS, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);
            Framework.datasets.dPlanosSeguroConteudo.PlanosSeguroConteudoRow rowPSCSel = rowPSC;
            foreach (dSeguros.APTSeguroExternoRow row in dSeguros.APTSeguroExterno)
            {
                row.Imprimir = (row.GetBOLetosTipoSRows().Length > 0);
                row.PSCValor = rowPSCSel.PSCValor;
            }
            gridView2.SelectAll();
        }*/

        private void SBCampanha_Click(object sender, EventArgs e)
        {                        
            if (dataF.DateTime < DateTime.Today)
            {
                MessageBox.Show("Boletos de seguro não podem ser emitido após o vencimento");
                return;
            }
            if (dataF.DateTime.AddDays(-5) < DateTime.Today)
            {
                if (MessageBox.Show("Atenção, menos de 5 dias para o vencimento", "Confirmação", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Cancel)
                    return;
            }
            if (lookUpEditPSCCamp.EditValue == null)
                MessageBox.Show("Valor não selecionado");
            else
            {
                
                Framework.datasets.dPlanosSeguroConteudo.PlanosSeguroConteudoRow rowPSCSel = rowPSC;
                DateTime? Vencimento = dataF.DateTime.AddDays(-5);
                if (!VirInput.Input.Execute("Vencimento", ref Vencimento, DateTime.Today.AddDays(3), dataF.DateTime.AddDays(-5)))
                    return;                 
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    Application.DoEvents();
                    ESP.Espere("Carregando Boletos");
                    dSeguros.BOLetosTipoSTableAdapter.Fill(dSeguros.BOLetosTipoS, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);
                    ESP.Espere("Carregando unidades");                    
                    if (dSeguros.APTSeguroExternoTableAdapter.FillOferta(dSeguros.APTSeguroExterno, lookupCondominio_F2.CON_sel) == 0)
                        return;                                                                                                    
                    if (dBoletos1 == null)                    
                        dBoletos1 = new BoletosProc.Boleto.dBoletos();                                            
                    ESP.Espere("Pre-Carga Apartamentos");
                    dBoletos1.CarregaAPTs(lookupCondominio_F2.CON_sel, true);
                    try
                    {
                        gridView6.BeginDataUpdate();
                        EmiteBoletosS(ESP, Vencimento.Value, rowPSCSel.PSCValor);
                    }
                    finally
                    {
                        gridView6.EndDataUpdate();
                    }

                    ESP.Espere("Carregando Boletos");
                    dSeguros.BOLetosTipoSTableAdapter.Fill(dSeguros.BOLetosTipoS, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);
                    ESP.Espere("Imprimindo");                    
                    ImpressaoCampanha(ESP);
                    dSeguros.CAmpanhaSeguroTableAdapter.FillByCompetencia(dSeguros.CAmpanhaSeguro, cCompet1.Comp.CompetenciaBind);
                }
            }
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            dSeguros.CAmpanhaSeguroTableAdapter.FillByCompetencia(dSeguros.CAmpanhaSeguro, cCompet1.Comp.CompetenciaBind);
        }

        
    }
}
