using System;
using System.Windows.Forms;
using CompontesBasicos.Espera;

namespace Boletos.Protesto
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cProtesto : CompontesBasicos.ComponenteBaseBindingSource
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cProtesto()
        {
            InitializeComponent();
        }


        private void Atualizar()
        {
            using (cEspera Esp = new cEspera(this)) 
            {
                BotProtestar.Enabled = false;
                dProtesto.Protestaveis.Clear();
                dProtesto.CarregaDadosCob(Esp);
                gridView1.CollapseAllGroups();
                BotProtestar.Enabled = true;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            dProtesto.Protestar();
            BotProtestar.Enabled = false;
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            Atualizar();                        
        }

        private void cProtesto_cargaInicial(object sender, EventArgs e)
        {
            uSUariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
        }

        private void OpenFile(string fileName)
        {
            if (MessageBox.Show("Voc� gostaria de abrir o arquivo?", "Exportar Para...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                    {
                        process.StartInfo.FileName = fileName;
                        process.StartInfo.Verb = "Open";
                        process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                        process.Start();
                    }
                }
                catch
                {
                    MessageBox.Show(this, "N�o foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                dlg.Title = "Exportar para XLS";
                dlg.FileName = "Protestar";
                dlg.Filter = "Microsoft Excel|*.xls";
                if (dlg.ShowDialog() == DialogResult.OK)
                    if (dlg.FileName != "")
                    {
                        gridView1.ExportToXls(dlg.FileName, new DevExpress.XtraPrinting.XlsExportOptions(DevExpress.XtraPrinting.TextExportMode.Value, false));
                        OpenFile(dlg.FileName);
                    }
            };


            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            Atualizar();
        }
    }
}

