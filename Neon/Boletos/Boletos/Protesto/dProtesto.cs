﻿using System;
using CompontesBasicos.Espera;

namespace Boletos.Protesto {


    partial class dProtesto
    {
        partial class CobrancaDataTable
        {
        }

        private dProtestoTableAdapters.CondominiosProtestaveisTableAdapter condominiosProtestaveisTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CondominiosProtestaveis
        /// </summary>
        public dProtestoTableAdapters.CondominiosProtestaveisTableAdapter CondominiosProtestaveisTableAdapter
        {
            get
            {
                if (condominiosProtestaveisTableAdapter == null)
                {
                    condominiosProtestaveisTableAdapter = new dProtestoTableAdapters.CondominiosProtestaveisTableAdapter();
                    condominiosProtestaveisTableAdapter.TrocarStringDeConexao();
                };
                return condominiosProtestaveisTableAdapter;
            }
        }

        private VirMSSQL.TableAdapter _TableAdapterInternet2;

        private VirMSSQL.TableAdapter TableAdapterInternet2 {
            get {
                if (_TableAdapterInternet2 == null)
                    _TableAdapterInternet2 = new VirMSSQL.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                return _TableAdapterInternet2;
            }
        }

        //private System.Collections.SortedList Boletos;

        /// <summary>
        /// 
        /// </summary>
        public void Protestar() {
            foreach (dProtesto.ProtestaveisRow rowProt in Protestaveis)
            {
                if (rowProt.Protestar) {
                    TableAdapterInternet2.ExecutarSQLNonQuery("INSERT INTO Protesto (BOL) VALUES (@P1)", rowProt.BOL);
                    //Boleto.Boleto Bol = (Boleto.Boleto)Boletos[rowProt.BOL];
                    Boleto.Boleto Bol = new Boleto.Boleto(rowProt.BOL);
                    Bol.rowPrincipal.BOLProtestado = true;                    
                    Bol.GravaJustificativa("Solicitado protesto", "blanca@advocaciamendes.com.br");                    
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public void CarregaDadosCob(cEspera Esp)
        {
            if (Protestaveis.Count == 0)
            {
                Esp.AtivaGauge(CondominiosProtestaveisTableAdapter.Fill(CondominiosProtestaveis));                
                ProtestaveisTableAdapter.ClearBeforeFill = false;
                Protestaveis.Clear();
                foreach (CondominiosProtestaveisRow rowCON in CondominiosProtestaveis)
                {
                    ProtestaveisTableAdapter.Fill(Protestaveis,rowCON.CONProtesto,rowCON.CON);
                    Esp.Gauge();
                    if (Esp.Abortar)
                        break;
                    //System.Windows.Forms.Application.DoEvents();
                }
            }
            //Boletos = new System.Collections.SortedList();
            System.Collections.ArrayList Del = new System.Collections.ArrayList();
            //CobrancaTableAdapter.ClearBeforeFill = false;
            //Cobranca.Clear();
            Esp.AtivaGauge(Protestaveis.Count);
            DateTime prox = DateTime.Now.AddSeconds(3);
            int i = 0;
            foreach (dProtesto.ProtestaveisRow rowProt in Protestaveis) {
                i++;
                if (DateTime.Now > prox)
                {
                    prox = DateTime.Now.AddSeconds(3);
                    Esp.Gauge(i);
                    if (Esp.Abortar)
                        break;
                }
                /*
                CobrancaRow rowCob = Cobranca.FindByCodconBlocoApartamento(rowProt.CONCodigo, rowProt.BLOCodigo, rowProt.APTNumero);
                if (rowCob == null) {
                    cobrancaTableAdapter.Fill(Cobranca, rowProt.CONCodigo, rowProt.BLOCodigo, rowProt.APTNumero);
                    rowCob = Cobranca.FindByCodconBlocoApartamento(rowProt.CONCodigo, rowProt.BLOCodigo, rowProt.APTNumero);
                };
                if (rowCob == null)
                    rowProt.StatusCobranca = "?";
                else
                    rowProt.StatusCobranca = rowCob.Status;

                if (rowProt.StatusCobranca.ToUpper() == "J")
                    Del.Add(rowProt);
                else
                {
                    rowProt.Apartamento = String.Format("{0} - {1} Cobrança: {2}", rowProt.BLOCodigo, rowProt.APTNumero, rowProt.StatusCobranca);
                    Boleto.Boleto Bol = new Boletos.Boleto.Boleto(rowProt.BOL);
                    Boletos.Add(rowProt.BOL, Bol);
                    Bol.Valorpara(System.DateTime.Today);
                    rowProt.ValorCorrigido = Bol.valorfinal;
                }*/

                //rowProt.Apartamento = String.Format("{0} - {1} Cobrança: {2}", rowProt.BLOCodigo, rowProt.APTNumero, rowProt.StatusCobranca);
                rowProt.Apartamento = String.Format("{0} - {1} Cobrança: {2}", rowProt.BLOCodigo, rowProt.APTNumero, rowProt.APTStatusCobranca);
                //Boleto.Boleto Bol = new Boletos.Boleto.Boleto(rowProt.BOL);
                //Boletos.Add(rowProt.BOL, Bol);
                //Bol.Valorpara(System.DateTime.Today);
                //rowProt.ValorCorrigido = Bol.valorfinal;

                rowProt.Protestar = false;
            };
            foreach (dProtesto.ProtestaveisRow rowProt in Del)
                rowProt.Delete();
            Protestaveis.AcceptChanges();
        }

        /*
        private dProtestoTableAdapters.CobrancaTableAdapter cobrancaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Cobranca
        /// </summary>
        public dProtestoTableAdapters.CobrancaTableAdapter CobrancaTableAdapter
        {
            get
            {
                if (cobrancaTableAdapter == null)
                {
                    cobrancaTableAdapter = new dProtestoTableAdapters.CobrancaTableAdapter();
                    cobrancaTableAdapter.TrocarStringDeConexao();
                };
                return cobrancaTableAdapter;
            }
        }*/

        private dProtestoTableAdapters.ProtestaveisTableAdapter protestaveisTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Protestaveis
        /// </summary>
        public dProtestoTableAdapters.ProtestaveisTableAdapter ProtestaveisTableAdapter
        {
            get
            {
                if (protestaveisTableAdapter == null)
                {
                    protestaveisTableAdapter = new dProtestoTableAdapters.ProtestaveisTableAdapter();
                    protestaveisTableAdapter.TrocarStringDeConexao();
                };
                return protestaveisTableAdapter;
            }
        }
    }
}
