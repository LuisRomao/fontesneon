﻿namespace Boletos.Eventual
{
    /// <summary>
    /// 
    /// </summary>
    partial class dGradeEventual
    {
        private dGradeEventualTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dGradeEventualTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dGradeEventualTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao();
                };
                return conTasLogicasTableAdapter;
            }
        }

        private Boletos.Eventual.dGradeEventualTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public Boletos.Eventual.dGradeEventualTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new Boletos.Eventual.dGradeEventualTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return bOletoDetalheTableAdapter; 
            
            }
            
        }
    }
}
