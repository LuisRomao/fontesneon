using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Framework.Lookup;
using Framework.objetosNeon;
using AbstratosNeon;
using VirEnumeracoesNeon;

namespace Boletos.Eventual
{
    /// <summary>
    /// boletos eventuais
    /// </summary>
    public partial class cGradeEventual : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cGradeEventual()
        {
            InitializeComponent();
            //aPARTAMENTOSBindingSource.DataSource = Framework.Lookup.LookupCondominio_F.EdComponente_F;
            aPARTAMENTOSBindingSource.DataSource = lookupBlocosAptos_F1.ApartamentosBindingSource;
            pLAnocontasBindingSource.DataSource = Framework.DSCentral.DCentral;
            if(Framework.DSCentral.DCentral.PLAnocontas.Rows.Count == 0)
                 Framework.DSCentral.PLAnocontasTableAdapter.Fill(Framework.DSCentral.DCentral.PLAnocontas);
            
        }
        
        /// <summary>
        /// Grava
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            if (base.Update_F())
            {
                dGradeEventual.BOletoDetalheTableAdapter.Update(dGradeEventual.BOletoDetalhe);
                dGradeEventual.BOletoDetalhe.AcceptChanges();
                return true;
            }
            else
                return false;
        }
        
        /// <summary>
        /// Refresh Dados
        /// </summary>
        public override void RefreshDados()
        {
            Popular();
        }

        private void Popular() {
            GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None;
            BtnIncluir_F.Enabled = false;
            BtnAlterar_F.Enabled = false;
            if(lookupBlocosAptos_F1.CON_sel > 0)
                dGradeEventual.ConTasLogicasTableAdapter.Fill(dGradeEventual.ConTasLogicas, lookupBlocosAptos_F1.CON_sel);
            if (lookupBlocosAptos_F1.APT_Sel > 0)
            {

                dGradeEventual.BOletoDetalheTableAdapter.FillBy(dGradeEventual.BOletoDetalhe, lookupBlocosAptos_F1.APT_Sel);                
                BtnIncluir_F.Enabled = true;
                BtnAlterar_F.Enabled = true;
            }
            else
            {
                //ParametroMetodoFieldBy = -1;
                if (lookupBlocosAptos_F1.BLO_Sel != -1)
                {                    
                    dGradeEventual.BOletoDetalheTableAdapter.FillByBLO(dGradeEventual.BOletoDetalhe, lookupBlocosAptos_F1.BLO_Sel);
                    BtnIncluir_F.Enabled = true;
                  //  eventuaisAtivosTableAdapter.FillByBLO(dGradeEventual.EventuaisAtivos, lookupBlocosAptos_F1.BLO_Sel);

                }
                else
                    if (lookupBlocosAptos_F1.CON_sel != -1)
                    {
                        dGradeEventual.BOletoDetalheTableAdapter.FillByCON(dGradeEventual.BOletoDetalhe, lookupBlocosAptos_F1.CON_sel);
                        BtnIncluir_F.Enabled = true;
                    //    eventuaisAtivosTableAdapter.FillByCON(dGradeEventual.EventuaisAtivos, lookupBlocosAptos_F1.CON_sel);
                    }
                    else
                    {
                        dGradeEventual.BOletoDetalhe.Clear();
                      //  dGradeEventual.EventuaisAtivos.Clear();
                    }
            }
        }

        private void Limpar()
        {
            _CompetProx = null;
            GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None;
            BtnIncluir_F.Enabled = false;
            BtnAlterar_F.Enabled = false;
            dGradeEventual.ConTasLogicas.Clear();
            dGradeEventual.BOletoDetalhe.Clear();
        }

        private void lookupBlocosAptos_F1_alterado(object sender, EventArgs e)
        {
            Limpar();
            //Popular();            
        }

        /// <summary>
        /// Cancela a edi��o
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Popular();
            ShowToolBarByRowState(false);
        }

        private dGradeEventual.BOletoDetalheRow LinhaMae
        {
            get 
            {
                return (dGradeEventual.BOletoDetalheRow)LinhaMae_F;
            }
        }

        /// <summary>
        /// Excluir
        /// </summary>
        protected override void ExcluirRegistro_F()
        {
            if (LinhaMae_F != null)
            {
                if((LinhaMae == null) || (!LinhaMae.BOD_PLA.StartsWith("1")))
                    return;
                
                string Justificativa = "";
                if (VirInput.Input.Execute("Justificativa:", ref Justificativa, true))
                {
                    try
                    {
                        //Abre transacao
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos cGradeEventual ExcluirRegistro_F - 98", dGradeEventual.BOletoDetalheTableAdapter);

                        //Pega BOD
                        //Boletos.Eventual.dGradeEventual.BOletoDetalheRow BOD = ((Boletos.Eventual.dGradeEventual.BOletoDetalheRow)(LinhaMae_F));
                        //List<int> BODs = new List<int>();
                        //BODs.Add(LinhaMae.BOD);

                        //Cria boleto ja cancelado
                        Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(LinhaMae.BODCompetencia);
                        ABS_Apartamento Apartamento = ABS_Apartamento.GetApartamento(LinhaMae.BOD_APT);
                        Boletos.Boleto.Boleto newBol = new Boletos.Boleto.Boleto(LinhaMae.BOD_CON, Apartamento, true, Comp, LinhaMae.BODData, "I", StatusBoleto.Cancelado, LinhaMae.BOD /*BODs.ToArray()*/);

                        //Salva justificativa
                        string strJust = String.Format("Boleto alterado por {0} \r\nJustificativa:{1}", CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome, Justificativa);
                        newBol.GravaJustificativa(strJust);

                        dGradeEventual.BOletoDetalheTableAdapter.Update(dGradeEventual.BOletoDetalhe);
                        dGradeEventual.BOletoDetalhe.AcceptChanges();
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                    }                                    
                    Popular();
                }                
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(lookupBlocosAptos_F1.CON_sel > 0)
                base.BtnIncluir_F_ItemClick(sender, e);
        }

        
        private void LookUpEditConta_EditValueChanged(object sender, EventArgs e)
        {
            
            DataRowView DRV = (DataRowView)GridView_F.GetRow(GridView_F.FocusedRowHandle);
            if (DRV == null)
                return;
            if (DRV.Row == null)
                return;

            dGradeEventual.BOletoDetalheRow  LinhaEd = (dGradeEventual.BOletoDetalheRow)DRV.Row;
            //LinhaEd.BODMensagem = sender.ToString();
            DevExpress.XtraEditors.LookUpEdit EditorPla = (DevExpress.XtraEditors.LookUpEdit)sender;
            GridView_F.SetFocusedRowCellValue(colBODProprietario, EditorPla.GetColumnValue("PLAProprietario"));
            GridView_F.SetFocusedRowCellValue(colBODMensagem, EditorPla.GetColumnValue("PLADescricaoRes"));                        
        }

        private void GridView_F_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)GridView_F.GetRow(e.RowHandle);
            if (DRV == null)
                return;
            if (DRV.Row == null)
                return;

            dGradeEventual.BOletoDetalheRow  LinhaEd = (dGradeEventual.BOletoDetalheRow)DRV.Row;
            Competencia Compet = Competencia.Ultima(lookupBlocosAptos_F1.CON_sel).Add(1);
            LinhaEd.BODData = Compet.CalculaVencimento();// VencimentoPadrao;
            LinhaEd.BODCompetencia = Compet.CompetenciaBind;
            LinhaEd.BODPrevisto = true;
            LinhaEd.BODComCondominio = true;
            LinhaEd.BOD_CON = lookupBlocosAptos_F1.CON_sel;
            LinhaEd.BODProprietario = false;            
        }

        private Competencia _CompetProx;

        private Competencia CompetProx
        {
            get 
            {
                if(_CompetProx == null)
                    _CompetProx = Competencia.Ultima(lookupBlocosAptos_F1.CON_sel).Add(1);
                return _CompetProx;
            }
        }

        private void GridView_F_ValidateRow_1(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if (DRV == null)
                return;
            if (DRV.Row == null)
                return;
            dGradeEventual.BOletoDetalheRow LinhaEd = (dGradeEventual.BOletoDetalheRow)DRV.Row;

            //Competencia CompetProx = Competencia.Ultima(lookupBlocosAptos_F1.CON_sel).Add(1);
            Competencia CompetSol = new Competencia(LinhaEd.BODCompetencia,lookupBlocosAptos_F1.CON_sel,null);

            if (LinhaEd.BODComCondominio)
            {
                if (CompetSol < CompetProx)
                {                    
                        e.ErrorText = "Compet�ncia inv�lida";
                        e.Valid = false;
                        GridView_F.SetColumnError(GridView_F.Columns["BODCompetencia"], "Compet�ncia inv�lida", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);                                            
                }
            }
            else
                if(CompetSol < new Framework.objetosNeon.Competencia())
                    GridView_F.SetColumnError(GridView_F.Columns["BODCompetencia"], "Compet�ncia inv�lida", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);
            string PLAOriginal = LinhaEd.RowState == DataRowState.Detached ? "" : (string)LinhaEd["BOD_PLA", DataRowVersion.Original];
            if ((LinhaEd["BOD_PLA"] != DBNull.Value) && (PLAOriginal != (string)LinhaEd["BOD_PLA"]))
                if (!LinhaEd.BOD_PLA.StartsWith("1") && !LinhaEd.BOD_PLA.StartsWith("2"))
                {
                    e.ErrorText = "Conta inv�lida";
                    e.Valid = false;
                    GridView_F.SetColumnError(GridView_F.Columns["BOD_PLA"], "Conta inv�lida", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);
                }
             
        }

        private void cGradeEventual_editando(object sender, EventArgs e)
        {
            panelControl1.Enabled = false;
            GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            conTasLogicasBindingSource.Filter = "CTLAtiva = 1";
        }

        private void cGradeEventual_naoeditando(object sender, EventArgs e)
        {
            panelControl1.Enabled = true;
            GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None;
            conTasLogicasBindingSource.Filter = "";
        }

        private void cGradeEventual_Load(object sender, EventArgs e)
        {
            Popular();
        }

        private void cGradeEventual_cargaInicial(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
        }

        private void cGradeEventual_Enter(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
        }

        private void cGradeEventual_Leave(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = true;
        }

        private void GridView_F_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            dGradeEventual.BOletoDetalheRow row = (dGradeEventual.BOletoDetalheRow)GridView_F.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if (row.BOD_PLA ==PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD))
            {
                e.Appearance.BackColor = Color.Aqua;
                e.Appearance.ForeColor = Color.Black;
            }            
            if (row.BOD_PLA ==PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraC))
            {
                //errado, n�o deveria existir
                e.Appearance.BackColor = Color.Red;
                e.Appearance.ForeColor = Color.White;
            }             
        }

        private void GridView_F_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            //conTasLogicasBindingSource.Filter = "";
            pLAnocontasBindingSource.Filter = "PLAEVE = 1";
            dGradeEventual.BOletoDetalheRow row = (dGradeEventual.BOletoDetalheRow)GridView_F.GetFocusedDataRow();
            bool PLABLOC = false;
            if (row != null)
            {
                if (row.RowState != DataRowState.Detached)
                {
                    {
                        if (row.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD))
                            PLABLOC = true;
                        //Console.Write("\r\nBOD = {0}\r\n", row.BOD);
                    }
                }
                else
                {
                    pLAnocontasBindingSource.Filter = "PLAEVE = 1 and ((PLA like '1%') or (PLA like '2%'))";
                    //conTasLogicasBindingSource.Filter = "CTLAtiva = 1";
                }
            }
            else
                pLAnocontasBindingSource.Filter = "PLAEVE = 1 and ((PLA like '1%') or (PLA like '2%'))";
            //if(!PLABLOC)
            //    pLAnocontasBindingSource.Filter = "PLAEVE = 1 and PLA like '1%'";
            LookUpEditConta.ReadOnly = repositoryItemTextEdit1.ReadOnly = LookUpEditNumero.ReadOnly = LookUpEditContaPLA.ReadOnly = PLABLOC;
            CalcEditN2.ReadOnly = PLABLOC;
            LookUpEditCTL.ReadOnly = PLABLOC;
            //repositoryItemCheckEdit1 = PLABLOC;
            //repositoryItemCompetenciaEdit1.ReadOnly = PLABLOC;
            //EditorPI.ReadOnly = PLABLOC;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Popular();            
        }

        private string FormataBODMensagem(string DescricaoBase,int Parcela,int Parcelas)
        {            
            string BODMensagem = string.Format("{0} ({1}/{2})", DescricaoBase, Parcela, Parcelas);
            if (BODMensagem.Length > dGradeEventual.BOletoDetalhe.Columns["BODMensagem"].MaxLength)
            {
                int Erro = BODMensagem.Length - dGradeEventual.BOletoDetalhe.Columns["BODMensagem"].MaxLength;
                BODMensagem = string.Format("{0} ({1}/{2})", DescricaoBase.Substring(0,DescricaoBase.Length - Erro - 1), Parcela, Parcelas);
            }
            return BODMensagem;
        }

        private void BotParcelar_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!Validate())
                return;
            int Parcelas;
            if (VirInput.Input.Execute("N�mero TOTAL de Parcelas", out Parcelas))
            {
                if (Parcelas > 120)
                {
                    MessageBox.Show("N�mero m�ximo 120");
                    return;
                }
                else if (Parcelas > 12)
                {
                    string Mensagem = string.Format("Confirma o n�mero de parcelas = {0}?",Parcelas);
                    if (MessageBox.Show(Mensagem, "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return;
                }
                if (Parcelas > 1)
                {
                    int nParcela1 = 1;
                    if (!VirInput.Input.Execute("N�mero da primeira parcela", out nParcela1, 1))
                        return;
                    if (nParcela1 > Parcelas)
                    {
                        MessageBox.Show("N�mero da primeira maior do que o n�mero total de parcelas");
                        return;
                    }
                    dGradeEventual.BOletoDetalheRow rowModelo = (dGradeEventual.BOletoDetalheRow)GridView_F.GetFocusedDataRow();
                    if (rowModelo == null)
                        return;
                    decimal Total = rowModelo.BODValor;
                    decimal Vparcela = Math.Round(Total / Parcelas, 2, MidpointRounding.AwayFromZero);
                    string DescricaoBase = rowModelo.BODMensagem;
                    string Descricao = FormataBODMensagem(DescricaoBase, nParcela1, Parcelas);
                    Competencia Comp = new Competencia(rowModelo.BODCompetencia);
                    rowModelo.BODValor = Total - (Parcelas - 1) * Vparcela;
                    rowModelo.BODMensagem = Descricao;
                    for (int i = nParcela1 + 1; i <= Parcelas; i++)
                    {                        
                        dGradeEventual.BOletoDetalheRow rowX = dGradeEventual.BOletoDetalhe.NewBOletoDetalheRow();
                        if (!rowModelo.IsBOD_APTNull())
                            rowX.BOD_APT = rowModelo.BOD_APT;
                        rowX.BOD_CON = rowModelo.BOD_CON;
                        if (!rowModelo.IsBOD_CTLNull())
                            rowX.BOD_CTL = rowModelo.BOD_CTL;
                        rowX.BOD_PLA = rowModelo.BOD_PLA;
                        rowX.BODComCondominio = rowModelo.BODComCondominio;
                        Comp++;
                        rowX.BODCompetencia = Comp.CompetenciaBind;
                        rowX.BODData = rowModelo.BODData;
                        rowX.BODMensagem = FormataBODMensagem(DescricaoBase,i, Parcelas);
                        if (!rowModelo.IsBODPrevistoNull())
                            rowX.BODPrevisto = rowModelo.BODPrevisto;
                        if (!rowModelo.IsBODProprietarioNull())
                            rowX.BODProprietario = rowModelo.BODProprietario;
                        rowX.BODValor = Vparcela;
                        dGradeEventual.BOletoDetalhe.AddBOletoDetalheRow(rowX);
                    }
                }
            }
        }                
    }
}

