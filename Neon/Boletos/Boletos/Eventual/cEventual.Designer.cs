namespace Boletos.Eventual
{
    /// <summary>
    /// Componente para cadastro de boletos eventuais
    /// </summary>
    partial class cEventual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cEventual));
            this.LookUpPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.dEventual = new Boletos.Eventual.dEventual();
            this.EditorPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.itensBODnovosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemLookUpEditPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pLAnocontasBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.colProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCTL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemLookUpEditCTL = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bOletoDetalheBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.LBFornecedor = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.spinValor = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditProp = new DevExpress.XtraEditors.CheckEdit();
            this.cBotaoExtra = new dllImpresso.Botoes.cBotaoImpBol();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            this.memoMensagemImportanteRTF = new DevExpress.XtraRichEdit.RichEditControl();
            this.textEditDestinatario = new DevExpress.XtraEditors.TextEdit();
            this.labelControlDestinatario = new DevExpress.XtraEditors.LabelControl();
            this.CHBanco = new DevExpress.XtraEditors.CheckEdit();
            this.CHCorreio = new DevExpress.XtraEditors.CheckEdit();
            this.cBotaoImpPara = new dllImpresso.Botoes.cBotaoImpBol();
            this.cCompetData1 = new Framework.objetosNeon.cCompetData();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEventual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itensBODnovosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemLookUpEditPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemLookUpEditCTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOletoDetalheBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinValor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditProp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDestinatario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHBanco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHCorreio.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(1, 238);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(110, 13);
            label1.TabIndex = 49;
            label1.Text = "Mensagem do boleto:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(220, 28);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(35, 13);
            label2.TabIndex = 62;
            label2.Text = "Valor:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(410, 28);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(34, 13);
            label3.TabIndex = 63;
            label3.Text = "Data:";
            // 
            // LookUpPLA
            // 
            this.LookUpPLA.AutoHeight = false;
            this.LookUpPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpPLA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 24, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricaoRes", "PLADescricaoRes", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpPLA.DisplayMember = "PLA";
            this.LookUpPLA.Name = "LookUpPLA";
            this.LookUpPLA.NullText = "Novo item";
            this.LookUpPLA.ShowHeader = false;
            this.LookUpPLA.ValueMember = "PLA";
            // 
            // dEventual
            // 
            this.dEventual.DataSetName = "dEventual";
            this.dEventual.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // EditorPI
            // 
            this.EditorPI.AutoHeight = false;
            this.EditorPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EditorPI.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EditorPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("P", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("I", false, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("-", null, -1)});
            this.EditorPI.Name = "EditorPI";
            this.EditorPI.SmallImages = this.imagensPI1;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.itensBODnovosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 432);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.ItemLookUpEditPLA,
            this.repositoryItemTextEdit1,
            this.ItemLookUpEditCTL});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1529, 317);
            this.gridControl1.TabIndex = 23;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2,
            this.gridView1});
            // 
            // itensBODnovosBindingSource
            // 
            this.itensBODnovosBindingSource.DataMember = "ItensBODnovos";
            this.itensBODnovosBindingSource.DataSource = this.dEventual;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMensagem,
            this.colPLA,
            this.colProprietario,
            this.colValor,
            this.gridColumn1,
            this.colCTL});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.NewItemRowText = "Incluir apartamentos";
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsDetail.AllowZoomDetail = false;
            this.gridView2.OptionsDetail.ShowDetailTabs = false;
            this.gridView2.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.AlwaysEnabled;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView2_InitNewRow);
            // 
            // colMensagem
            // 
            this.colMensagem.Caption = "Mensagem";
            this.colMensagem.ColumnEdit = this.repositoryItemTextEdit1;
            this.colMensagem.FieldName = "Mensagem";
            this.colMensagem.Name = "colMensagem";
            this.colMensagem.OptionsColumn.AllowIncrementalSearch = false;
            this.colMensagem.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colMensagem.Visible = true;
            this.colMensagem.VisibleIndex = 1;
            this.colMensagem.Width = 330;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 50;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colPLA
            // 
            this.colPLA.Caption = "Conta";
            this.colPLA.ColumnEdit = this.ItemLookUpEditPLA;
            this.colPLA.FieldName = "PLA";
            this.colPLA.Name = "colPLA";
            this.colPLA.Visible = true;
            this.colPLA.VisibleIndex = 0;
            this.colPLA.Width = 119;
            // 
            // ItemLookUpEditPLA
            // 
            this.ItemLookUpEditPLA.AutoHeight = false;
            this.ItemLookUpEditPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ItemLookUpEditPLA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 24, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 83, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.ItemLookUpEditPLA.DataSource = this.pLAnocontasBindingSource1;
            this.ItemLookUpEditPLA.DisplayMember = "PLA";
            this.ItemLookUpEditPLA.Name = "ItemLookUpEditPLA";
            this.ItemLookUpEditPLA.ShowHeader = false;
            this.ItemLookUpEditPLA.ValueMember = "PLA";
            this.ItemLookUpEditPLA.EditValueChanged += new System.EventHandler(this.LookUpPLA_EditValueChanged);
            // 
            // pLAnocontasBindingSource1
            // 
            this.pLAnocontasBindingSource1.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource1.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colProprietario
            // 
            this.colProprietario.Caption = "Destino";
            this.colProprietario.ColumnEdit = this.EditorPI;
            this.colProprietario.FieldName = "Proprietario";
            this.colProprietario.Name = "colProprietario";
            this.colProprietario.Visible = true;
            this.colProprietario.VisibleIndex = 2;
            this.colProprietario.Width = 50;
            // 
            // colValor
            // 
            this.colValor.Caption = "Valor";
            this.colValor.DisplayFormat.FormatString = "n2";
            this.colValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValor.FieldName = "Valor";
            this.colValor.Name = "colValor";
            this.colValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:n2}")});
            this.colValor.Visible = true;
            this.colValor.VisibleIndex = 3;
            this.colValor.Width = 90;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 5;
            this.gridColumn1.Width = 23;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colCTL
            // 
            this.colCTL.Caption = "Conta";
            this.colCTL.ColumnEdit = this.ItemLookUpEditCTL;
            this.colCTL.FieldName = "CTL";
            this.colCTL.Name = "colCTL";
            this.colCTL.Visible = true;
            this.colCTL.VisibleIndex = 4;
            // 
            // ItemLookUpEditCTL
            // 
            this.ItemLookUpEditCTL.AutoHeight = false;
            this.ItemLookUpEditCTL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ItemLookUpEditCTL.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.ItemLookUpEditCTL.DataSource = this.conTasLogicasBindingSource;
            this.ItemLookUpEditCTL.DisplayMember = "CTLTitulo";
            this.ItemLookUpEditCTL.Name = "ItemLookUpEditCTL";
            this.ItemLookUpEditCTL.NullText = "-- Padrão --";
            this.ItemLookUpEditCTL.ShowHeader = false;
            this.ItemLookUpEditCTL.ValueMember = "CTL";
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = typeof(FrameworkProc.datasets.dContasLogicas);
            // 
            // bOletoDetalheBindingSource
            // 
            this.bOletoDetalheBindingSource.DataMember = "BOletoDetalhe";
            this.bOletoDetalheBindingSource.DataSource = this.dEventual;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.LBFornecedor);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.memoMensagemImportanteRTF);
            this.panelControl1.Controls.Add(this.textEditDestinatario);
            this.panelControl1.Controls.Add(this.labelControlDestinatario);
            this.panelControl1.Controls.Add(this.CHBanco);
            this.panelControl1.Controls.Add(this.CHCorreio);
            this.panelControl1.Controls.Add(this.cBotaoImpPara);
            this.panelControl1.Controls.Add(this.cCompetData1);
            this.panelControl1.Controls.Add(label1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1529, 432);
            this.panelControl1.TabIndex = 43;
            // 
            // LBFornecedor
            // 
            this.LBFornecedor.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBFornecedor.Appearance.ForeColor = System.Drawing.Color.Red;
            this.LBFornecedor.Appearance.Options.UseFont = true;
            this.LBFornecedor.Appearance.Options.UseForeColor = true;
            this.LBFornecedor.Location = new System.Drawing.Point(362, 211);
            this.LBFornecedor.Name = "LBFornecedor";
            this.LBFornecedor.Size = new System.Drawing.Size(135, 13);
            this.LBFornecedor.TabIndex = 61;
            this.LBFornecedor.Text = "Destinatário CPF / CNPJ:";
            this.LBFornecedor.Visible = false;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BackColor = System.Drawing.Color.SeaShell;
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.Controls.Add(this.dateEdit1);
            this.groupControl1.Controls.Add(label3);
            this.groupControl1.Controls.Add(label2);
            this.groupControl1.Controls.Add(this.spinValor);
            this.groupControl1.Controls.Add(this.checkEditProp);
            this.groupControl1.Controls.Add(this.cBotaoExtra);
            this.groupControl1.Location = new System.Drawing.Point(5, 77);
            this.groupControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(592, 85);
            this.groupControl1.TabIndex = 60;
            this.groupControl1.Text = "Boleto antecipado";
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(413, 50);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(158, 26);
            this.dateEdit1.TabIndex = 64;
            // 
            // spinValor
            // 
            this.spinValor.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinValor.Location = new System.Drawing.Point(220, 50);
            this.spinValor.Name = "spinValor";
            this.spinValor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinValor.Properties.Appearance.Options.UseFont = true;
            this.spinValor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinValor.Properties.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinValor.Properties.Mask.EditMask = "c";
            this.spinValor.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinValor.Size = new System.Drawing.Size(165, 26);
            this.spinValor.TabIndex = 61;
            this.spinValor.EditValueChanged += new System.EventHandler(this.spinValor_EditValueChanged);
            // 
            // checkEditProp
            // 
            this.checkEditProp.Location = new System.Drawing.Point(5, 60);
            this.checkEditProp.Name = "checkEditProp";
            this.checkEditProp.Properties.Caption = "Proprietário";
            this.checkEditProp.Size = new System.Drawing.Size(95, 17);
            this.checkEditProp.TabIndex = 60;
            // 
            // cBotaoExtra
            // 
            this.cBotaoExtra.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBotaoExtra.Appearance.Options.UseFont = true;
            this.cBotaoExtra.Enabled = false;
            this.cBotaoExtra.ItemComprovante = false;
            this.cBotaoExtra.Location = new System.Drawing.Point(5, 28);
            this.cBotaoExtra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoExtra.Name = "cBotaoExtra";
            this.cBotaoExtra.Size = new System.Drawing.Size(209, 30);
            this.cBotaoExtra.TabIndex = 59;
            this.cBotaoExtra.Titulo = "Gerar Boleto Antecipado";
            this.cBotaoExtra.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.lookupBlocosAptos_F1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1525, 69);
            this.panelControl2.TabIndex = 58;
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.CON_selrow = null;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.lookupBlocosAptos_F1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(2, 2);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.NivelCONOculto = 2;
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(1521, 65);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 45;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = null;
            this.lookupBlocosAptos_F1.alteradoAPT += new System.EventHandler(this.lookupBlocosAptos_F1_alteradoAPT);
            this.lookupBlocosAptos_F1.alterado += new System.EventHandler(this.lookupBlocosAptos_F1_alterado);
            // 
            // memoMensagemImportanteRTF
            // 
            this.memoMensagemImportanteRTF.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.memoMensagemImportanteRTF.Location = new System.Drawing.Point(5, 254);
            this.memoMensagemImportanteRTF.Name = "memoMensagemImportanteRTF";
            this.memoMensagemImportanteRTF.Options.AutoCorrect.UseSpellCheckerSuggestions = true;
            this.memoMensagemImportanteRTF.Size = new System.Drawing.Size(571, 110);
            this.memoMensagemImportanteRTF.TabIndex = 57;
            // 
            // textEditDestinatario
            // 
            this.textEditDestinatario.Location = new System.Drawing.Point(129, 203);
            this.textEditDestinatario.Name = "textEditDestinatario";
            this.textEditDestinatario.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditDestinatario.Properties.Appearance.Options.UseFont = true;
            this.textEditDestinatario.Properties.MaxLength = 40;
            this.textEditDestinatario.Size = new System.Drawing.Size(227, 26);
            this.textEditDestinatario.TabIndex = 56;
            this.textEditDestinatario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEditDestinatario_KeyDown);
            // 
            // labelControlDestinatario
            // 
            this.labelControlDestinatario.Location = new System.Drawing.Point(4, 211);
            this.labelControlDestinatario.Name = "labelControlDestinatario";
            this.labelControlDestinatario.Size = new System.Drawing.Size(119, 13);
            this.labelControlDestinatario.TabIndex = 55;
            this.labelControlDestinatario.Text = "Destinatário CPF / CNPJ:";
            // 
            // CHBanco
            // 
            this.CHBanco.Location = new System.Drawing.Point(427, 395);
            this.CHBanco.Name = "CHBanco";
            this.CHBanco.Properties.Caption = "Cobrar despesa Bancária";
            this.CHBanco.Size = new System.Drawing.Size(170, 19);
            this.CHBanco.TabIndex = 54;
            // 
            // CHCorreio
            // 
            this.CHCorreio.Location = new System.Drawing.Point(427, 370);
            this.CHCorreio.Name = "CHCorreio";
            this.CHCorreio.Properties.Caption = "Cobrar Correio";
            this.CHCorreio.Size = new System.Drawing.Size(170, 19);
            this.CHCorreio.TabIndex = 53;
            // 
            // cBotaoImpPara
            // 
            this.cBotaoImpPara.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBotaoImpPara.Appearance.Options.UseFont = true;
            this.cBotaoImpPara.ItemComprovante = false;
            this.cBotaoImpPara.Location = new System.Drawing.Point(5, 370);
            this.cBotaoImpPara.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpPara.Name = "cBotaoImpPara";
            this.cBotaoImpPara.Size = new System.Drawing.Size(416, 47);
            this.cBotaoImpPara.TabIndex = 52;
            this.cBotaoImpPara.Titulo = "Gerar";
            this.cBotaoImpPara.clicado += new System.EventHandler(this.cBotaoImpPara_clicado);
            // 
            // cCompetData1
            // 
            this.cCompetData1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetData1.Appearance.Options.UseBackColor = true;
            this.cCompetData1.CaixaAltaGeral = true;
            this.cCompetData1.Comcondominio = true;
            this.cCompetData1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetData1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetData1.Location = new System.Drawing.Point(5, 168);
            this.cCompetData1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.cCompetData1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetData1.Name = "cCompetData1";
            this.cCompetData1.Size = new System.Drawing.Size(571, 27);
            this.cCompetData1.somenteleitura = false;
            this.cCompetData1.TabIndex = 51;
            this.cCompetData1.Titulo = null;
            // 
            // cEventual
            // 
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cEventual";
            this.Size = new System.Drawing.Size(1529, 749);
            this.cargaFinal += new System.EventHandler(this.cEventual_cargaFinal);
            this.Load += new System.EventHandler(this.cEventual_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEventual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itensBODnovosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemLookUpEditPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemLookUpEditCTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOletoDetalheBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinValor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditProp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditDestinatario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHBanco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHCorreio.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dEventual dEventual;
        private System.Windows.Forms.BindingSource bOletoDetalheBindingSource;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private Framework.Lookup.LookupBlocosAptos_F lookupBlocosAptos_F1;
        private System.Windows.Forms.BindingSource itensBODnovosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colPLA;
        private DevExpress.XtraGrid.Columns.GridColumn colProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colValor;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private Framework.objetosNeon.cCompetData cCompetData1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpPLA;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox EditorPI;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit ItemLookUpEditPLA;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpPara;
        private DevExpress.XtraEditors.CheckEdit CHBanco;
        private DevExpress.XtraEditors.CheckEdit CHCorreio;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource1;
        private DevExpress.XtraEditors.TextEdit textEditDestinatario;
        private DevExpress.XtraEditors.LabelControl labelControlDestinatario;
        private DevExpress.XtraRichEdit.RichEditControl memoMensagemImportanteRTF;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoExtra;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SpinEdit spinValor;
        private DevExpress.XtraEditors.CheckEdit checkEditProp;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCTL;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit ItemLookUpEditCTL;
        private DevExpress.XtraEditors.LabelControl LBFornecedor;
    }
}
