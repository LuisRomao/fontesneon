//23/04/2014 LH 13.2.8.29 - confirmar se � para imprimir.

using AbstratosNeon;
using Framework;
using Framework.datasets;
using FrameworkProc.datasets;
using Framework.objetosNeon;
using System;
using System.Data;
using System.Windows.Forms;
using VirEnumeracoesNeon;

namespace Boletos.Eventual
{
    public partial class cEventual : CompontesBasicos.ComponenteBaseBindingSource
    {


        //private bool Alteracao = false;


        /// <summary>
        /// Construtor
        /// </summary>
        public cEventual()
        {
            InitializeComponent();
            //dBuscasBindingSource.DataSource = Framework.Buscas.dBuscas.EdBuscas;
            pLAnocontasBindingSource1.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasStEVE;            
        }

        private void cEventual_cargaFinal(object sender, EventArgs e)
        {
            cCompetData1.Comcondominio = false;
            dateEdit1.DateTime = DateTime.Today.AddMonths(1);
        }

        ABS_Fornecedor Fornecedor;

        private bool BuscaFRN()
        {
            Fornecedor = null;
            LBFornecedor.Visible = false;
            if ((textEditDestinatario.Text == null) || (textEditDestinatario.Text == ""))
            {
                MessageBox.Show("Unidade / Fornecedor n�o definidos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                LBFornecedor.Visible = true;
                LBFornecedor.Text = "Fornecedor n�o definido";
                return false;
            }
            DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(textEditDestinatario.Text);
            if (cnpj.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO)
            {
                Fornecedor = ABS_Fornecedor.ABSGetFornecedor(cnpj, true);
                if (Fornecedor != null)
                {
                    textEditDestinatario.Text = cnpj.ToString();
                    LBFornecedor.Visible = true;
                    LBFornecedor.Text = Fornecedor.FRNNome;
                    if (Fornecedor.Endereco(ModeloEndereco.TresLinhas) == "")
                    {
                        LBFornecedor.Text = "Endere�o do fornecedor incompleto";
                        return false;
                    }
                    else
                        return true;
                }
                else
                {
                    textEditDestinatario.Text = "";
                    LBFornecedor.Visible = true;
                    LBFornecedor.Text = "Fornecedor n�o definido";
                    return false;
                }
            }
            else
            {
                MessageBox.Show("CPF/CNPJ inv�lido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                textEditDestinatario.ErrorText = "CPF/CNPJ inv�lido";
                LBFornecedor.Visible = true;
                LBFornecedor.Text = "CPF/CNPJ inv�lido";
                return false;
            }
        }

        private bool DadosValidos()
        {
            if (lookupBlocosAptos_F1.CON_sel == -1)
            {
                MessageBox.Show("Condom�nio n�o definido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            };

            if (lookupBlocosAptos_F1.APT_Sel == -1)
            {
                if (cCompetData1.Comcondominio)
                {
                    MessageBox.Show("Boletos sem apartamento nao podem ser com condom�nio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return false;
                }
                if (!BuscaFRN())
                    return false;
            }

            if (!cCompetData1.Comcondominio)
            {
                if ((cCompetData1.dateEdit1.DateTime == null) || (cCompetData1.dateEdit1.DateTime == DateTime.MinValue))
                {
                    MessageBox.Show("Data n�o definida", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    cCompetData1.dateEdit1.ErrorText = "Data inv�lida";
                    return false;
                }
                else
                    cCompetData1.dateEdit1.ErrorText = "";
                if ((CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("IMPLANTACAO") >= 1)
                    ||
                    (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") >= 1))
                {
                    if (cCompetData1.dateEdit1.DateTime < DateTime.Today)
                    {
                        if (MessageBox.Show("Data vencida!!\r\n\r\nConfirma ?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                            return false;
                    };
                    if (cCompetData1.dateEdit1.DateTime > DateTime.Today.AddMonths(1))
                    {
                        if (MessageBox.Show("Data superior a 30 dias!!\r\n\r\nConfirma ?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                            return false;
                    }
                }
                else
                {
                    if ((cCompetData1.dateEdit1.DateTime < DateTime.Today) || (cCompetData1.dateEdit1.DateTime > DateTime.Today.AddMonths(1)))
                    {
                        MessageBox.Show("Data inv�lida", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        cCompetData1.dateEdit1.ErrorText = "Data inv�lida";
                        return false;
                    }
                }
            };
            if (dEventual.ItensBODnovos.Rows.Count == 0)
            {
                MessageBox.Show("Nenhum item no boleto", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            };

            foreach (dEventual.ItensBODnovosRow LinhaEd in dEventual.ItensBODnovos)
            {

                LinhaEd.RowError = "";
                if (LinhaEd.IsPLANull())
                {
                    MessageBox.Show("Tipo de pagameto inv�lido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    LinhaEd.RowError = "Tipo de pagameto inv�lido";
                    return false;
                };

                /*
                if (Alteracao)
                {
                    if ((LinhaEd.IsValorNull()) || (LinhaEd.Valor == 0))
                    {
                        MessageBox.Show("Valor inv�lido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        LinhaEd.RowError = "Valor inv�lido";
                        return false;
                    };
                }*/
                //else
                if ((LinhaEd.IsValorNull()) || (LinhaEd.Valor <= 0))
                {
                    MessageBox.Show("Valor inv�lido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    LinhaEd.RowError = "Valor inv�lido";
                    return false;
                };


                if (LinhaEd.Valor > 5000)
                {
                    if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") >= 1)
                    {
                        if (MessageBox.Show("Valor superior a 5.000,00\r\n\r\nConfirma ?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                            return false;

                    }
                    else
                    {
                        MessageBox.Show("Valor inv�lido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        LinhaEd.RowError = "Valor inv�lido";
                        return false;
                    };
                };
            };
            return true;
        }

        private void LookUpPLA_EditValueChanged(object sender, EventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView2.GetRow(gridView2.FocusedRowHandle);
            if (DRV == null)
                return;
            if (DRV.Row == null)
                return;
            dEventual.ItensBODnovosRow LinhaEd = (dEventual.ItensBODnovosRow)DRV.Row;


            dPLAnocontas.PLAnocontasRow planorow = dPLAnocontas.dPLAnocontasStEVE.PLAnocontas.FindByPLA(((DevExpress.XtraEditors.LookUpEdit)sender).Text);
            if (planorow != null)
            {
                if (!planorow.IsPLAProprietarioNull())
                {
                    LinhaEd.Proprietario = planorow.PLAProprietario;
                    gridView2.SetFocusedRowCellValue(colProprietario, planorow.PLAProprietario);
                    gridView2.SetFocusedRowCellValue(colMensagem, planorow.PLADescricaoRes);
                };
            };

        }

        //private bool BloConCond = false;

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView2.GetRow(gridView2.FocusedRowHandle);
            if (DRV == null)
                return;
            if (DRV.Row == null)
                return;
            dEventual.ItensBODnovosRow LinhaEd = (dEventual.ItensBODnovosRow)DRV.Row;
            LinhaEd.Delete();
        }

        private void cEventual_Load(object sender, EventArgs e)
        {
            //if (!Alteracao)
            //{
            if ((CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") > 0)
                ||
                (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CADASTRO") > 2))
            {
                cCompetData1.dateEdit1.Properties.MinValue = DateTime.Today.AddYears(-20);
                cCompetData1.dateEdit1.Properties.MaxValue = DateTime.Today.AddYears(10);
            };
            //};
        }

        private void lookupBlocosAptos_F1_alteradoAPT(object sender, EventArgs e)
        {
            if ((lookupBlocosAptos_F1.APT_Sel == -1) || (lookupBlocosAptos_F1.APT_Selrow == null))
            {
                cCompetData1.Comcondominio = false;
                textEditDestinatario.Visible = labelControlDestinatario.Visible = LBFornecedor.Visible = true;
            }
            else
                textEditDestinatario.Visible = labelControlDestinatario.Visible = LBFornecedor.Visible = false;
            EnableExtra();
        }

        private decimal DespBan;
        private decimal Correio;

        private dContasLogicas dContasLogicas1;
        private int? CTLPadrao;

        private void AjustaCTL(int CON)
        {
            dContasLogicas1 = dContasLogicas.GetdContasLogicas(CON, true);
            conTasLogicasBindingSource.DataSource = dContasLogicas1;
            CTLPadrao = null;
            foreach (dContasLogicas.ConTasLogicasRow rowCTL in dContasLogicas1.ConTasLogicas)
            {
                if (CTLPadrao != null)
                    break;
                switch ((Framework.CTLTipo)rowCTL.CTLTipo)
                {
                    case Framework.CTLTipo.Caixa:
                        CTLPadrao = rowCTL.CTL;
                        break;
                }
            }
            foreach (dEventual.ItensBODnovosRow LinhaEd in dEventual.ItensBODnovos)
                if (CTLPadrao.HasValue)
                    LinhaEd.CTL = CTLPadrao.Value;
                else
                    LinhaEd.SetCTLNull();
        }

        private void lookupBlocosAptos_F1_alterado(object sender, EventArgs e)
        {
            if (lookupBlocosAptos_F1.CON_sel == -1)
            {
                dateEdit1.DateTime = DateTime.Today.AddMonths(1);
                cCompetData1.Comcondominio = false;                
                return;
            }
            else
                AjustaCTL(lookupBlocosAptos_F1.CON_sel);
            if (lookupBlocosAptos_F1.APT_Sel == -1)
            {
                textEditDestinatario.Visible = labelControlDestinatario.Visible = LBFornecedor.Visible = true;
                //return;
            }
            
            Competencia procomp = Competencia.Ultima(lookupBlocosAptos_F1.CON_sel).Add(1);
            cCompetData1.cCompet1.CompMinima = procomp;
            cCompetData1.SetCompetencia(cCompetData1.cCompet1.CompMinima);
            dateEdit1.DateTime = procomp.VencimentoPadrao;


            DataRow rowDadosCON = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select CONRepassaCorreio,CONRepassaDespBanc,CONValorBoleto,CONValorCartaSimples from condominios where CON = @P1", lookupBlocosAptos_F1.CON_sel);

            if (rowDadosCON == null)
                return;
            //Repassa BANCO
            DespBan = 0;
            CHBanco.Enabled = CHBanco.Checked = false;
            if (rowDadosCON["CONValorBoleto"] != DBNull.Value)
            {
                DespBan = (decimal)rowDadosCON["CONValorBoleto"];
                if (DespBan > 0)
                {
                    CHBanco.Enabled = true;
                    if ((rowDadosCON["CONRepassaDespBanc"] != DBNull.Value) && ((bool)rowDadosCON["CONRepassaDespBanc"]))
                        CHBanco.Checked = true;
                    else
                        CHBanco.Checked = false;
                }
            };

            Correio = 0;
            CHCorreio.Enabled = CHCorreio.Checked = false;
            if (rowDadosCON["CONValorCartaSimples"] != DBNull.Value)
            {
                Correio = (decimal)rowDadosCON["CONValorCartaSimples"];
                if (Correio > 0)
                {
                    CHCorreio.Enabled = true;
                    if ((rowDadosCON["CONRepassaCorreio"] != DBNull.Value) && ((bool)rowDadosCON["CONRepassaCorreio"]))
                        CHCorreio.Checked = true;
                    else
                        CHCorreio.Checked = false;
                }
            };
        }

        private BoletosProc.Boleto.dBoletos _dBoletos;

        private BoletosProc.Boleto.dBoletos dBoletos
        {
            get
            {
                if (_dBoletos == null)
                    _dBoletos = new BoletosProc.Boleto.dBoletos();
                return _dBoletos;
            }
        }

        private void IncluiBOD(string PLA, string BODMensagem, decimal Valor, bool previsto, int? CTL)
        {
            BoletosProc.Boleto.dBoletos.BOletoDetalheRow NovoBod = dBoletos.BOletoDetalhe.NewBOletoDetalheRow();
            if (lookupBlocosAptos_F1.APT_Sel != -1)
                NovoBod.BOD_APT = lookupBlocosAptos_F1.APT_Sel;
            NovoBod.BOD_CON = lookupBlocosAptos_F1.CON_sel;
            NovoBod.BOD_PLA = PLA;
            NovoBod.BODComCondominio = cCompetData1.Comcondominio;
            NovoBod.BODCompetencia = Comp.CompetenciaBind;
            NovoBod.BODData = cCompetData1.dateEdit1.DateTime;
            NovoBod.BODPrevisto = previsto;
            NovoBod.BODMensagem = BODMensagem;
            NovoBod.BODProprietario = proprietario;
            NovoBod.BODValor = Valor;
            if (CTL.HasValue)
                NovoBod.BOD_CTL = CTL.Value;
            dBoletos.BOletoDetalhe.AddBOletoDetalheRow(NovoBod);
            dBoletos.BOletoDetalheTableAdapter.Update(NovoBod);
            NovoBod.AcceptChanges();
            if (!cCompetData1.Comcondominio)
                BODs.Add(NovoBod.BOD);
        }

        private bool proprietario;

        private Framework.objetosNeon.Competencia Comp;

        private System.Collections.ArrayList _BODs;

        private System.Collections.ArrayList BODs
        {
            get
            {
                if (_BODs == null)
                    _BODs = new System.Collections.ArrayList();
                return _BODs;
            }
        }

        private void cBotaoImpPara_clicado(object sender, EventArgs e)
        {
            if (!DadosValidos())
                return;
            ABS_Apartamento Apartamento = null;
            if (lookupBlocosAptos_F1.APT_Sel != -1)
                Apartamento = ABS_Apartamento.GetApartamento(lookupBlocosAptos_F1.APT_Sel);

            if ((Apartamento != null) && (Apartamento.Alugado))
            {
                proprietario = dEventual.ItensBODnovos[0].Proprietario;
                foreach (dEventual.ItensBODnovosRow LinhaEd in dEventual.ItensBODnovos)
                    if (proprietario != LinhaEd.Proprietario)
                    {
                        MessageBox.Show("Erro: Existem itens para o propriet�rio e para o inquilino no mesmo boleto");
                        return;
                    }
            }
            else
                proprietario = true;
            if (!cCompetData1.Comcondominio)
            {
                Comp = new Framework.objetosNeon.Competencia(cCompetData1.dateEdit1.DateTime.Month, cCompetData1.dateEdit1.DateTime.Year, lookupBlocosAptos_F1.CON_sel);
            }
            else
            {
                Comp = cCompetData1.cCompet1.Comp;
                Comp.CON = lookupBlocosAptos_F1.CON_sel;
                cCompetData1.dateEdit1.DateTime = Comp.CalculaVencimento();
                if (cCompetData1.dateEdit1.DateTime == DateTime.MinValue)
                {
                    MessageBox.Show("Processo Cancelado");
                    return;
                };
            };

            Boleto.Boleto NovoBoleto = null;
            bool BODCadastrado = false;

            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos cEventual - 371", dBoletos.BOletoDetalheTableAdapter);
                BODs.Clear();
                foreach (dEventual.ItensBODnovosRow LinhaEd in dEventual.ItensBODnovos)
                {
                    int? CTL = null;
                    if (!LinhaEd.IsCTLNull())
                        CTL = LinhaEd.CTL;
                    IncluiBOD(LinhaEd.PLA, LinhaEd.Mensagem, LinhaEd.Valor, true, CTL);
                }
                if (cCompetData1.Comcondominio)
                    BODCadastrado = true;
                else
                {
                    if (Apartamento != null)
                        NovoBoleto = new Boleto.Boleto(proprietario, Apartamento, memoMensagemImportanteRTF.RtfText, Comp, cCompetData1.dateEdit1.DateTime, "I", StatusBoleto.Valido, CHCorreio.Enabled && CHCorreio.Checked, CHBanco.Enabled && CHBanco.Checked, (int[])BODs.ToArray(typeof(int)));
                    else
                        NovoBoleto = new Boleto.Boleto(lookupBlocosAptos_F1.CON_sel, memoMensagemImportanteRTF.RtfText, Comp, cCompetData1.dateEdit1.DateTime, "I", StatusBoleto.Valido, CHCorreio.Enabled && CHCorreio.Checked, CHBanco.Enabled && CHBanco.Checked, Fornecedor, (int[])BODs.ToArray(typeof(int)));
                };
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception eOriginal)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(eOriginal);
                throw new Exception("Boleto: " + eOriginal.Message, eOriginal);
            };
            if (BODCadastrado)
                MessageBox.Show("Cadastrado");
            else
                if (NovoBoleto != null)
            {
                if ((cBotaoImpPara.BotaoClicado != dllImpresso.Botoes.Botao.botao) || (MessageBox.Show("Imprimir ?", "Impress�o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes))
                    NovoBoleto.Imprimir(cBotaoImpPara.BotaoClicado, false);
            }
            NovoBoleto = null;
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            if ((lookupBlocosAptos_F1.APT_Sel == -1) || (spinValor.Value < 10))
                return;
            ABS_Apartamento Apartamento = ABS_Apartamento.GetApartamento(lookupBlocosAptos_F1.APT_Sel);
            Boletos.Boleto.Boleto BoletoExtra;
            if (Apartamento.Alugado)
                proprietario = checkEditProp.Checked;
            else
                proprietario = true;


            Comp = new Framework.objetosNeon.Competencia(cCompetData1.dateEdit1.DateTime.Month, cCompetData1.dateEdit1.DateTime.Year, lookupBlocosAptos_F1.CON_sel);
            int CTL = dPLAnocontas.dPLAnocontasSt.CTLPadrao(lookupBlocosAptos_F1.CON_sel, CTLTipo.CreditosAnteriores);
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos cEventual - 411");//, dBoletos.BOletoDetalheTableAdapter);
                BoletoExtra = new Boletos.Boleto.Boleto(lookupBlocosAptos_F1.CON_sel,
                                                        Apartamento,
                                                        proprietario,
                                                        Comp,
                                                        dateEdit1.DateTime,
                                                        "E",
                                                        StatusBoleto.Extra);
                string Descritivo1 = string.Format("Pagamento antecipado. Unidade {0}", BoletoExtra.Unidade);
                string PLAC = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraC);
                BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBODO = BoletoExtra.IncluiBOD(PLAC, Descritivo1, spinValor.Value, false, null, CTL);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception eOriginal)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(eOriginal);
                throw new Exception("Boleto: " + eOriginal.Message, eOriginal);
            };
            if ((cBotaoExtra.BotaoClicado != dllImpresso.Botoes.Botao.botao) || (MessageBox.Show("Imprimir ?", "Impress�o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes))
                BoletoExtra.Imprimir(cBotaoExtra.BotaoClicado, false);
        }

        private void EnableExtra()
        {
            cBotaoExtra.Enabled = ((lookupBlocosAptos_F1.APT_Sel > 0) && (spinValor.Value > 10));
        }

        private void spinValor_EditValueChanged(object sender, EventArgs e)
        {
            EnableExtra();
        }

        private void gridView2_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            dEventual.ItensBODnovosRow LinhaNova = (dEventual.ItensBODnovosRow)gridView2.GetDataRow(e.RowHandle);
            if (LinhaNova != null)
            {
                if (CTLPadrao.HasValue)
                    LinhaNova.CTL = CTLPadrao.Value;
            }
        }

        private void textEditDestinatario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                BuscaFRN();
            }
        }
    }
}

