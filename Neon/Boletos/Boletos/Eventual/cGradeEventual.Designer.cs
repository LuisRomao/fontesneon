namespace Boletos.Eventual
{
    partial class cGradeEventual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cGradeEventual));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.dGradeEventual = new Boletos.Eventual.dGradeEventual();
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.colBOD_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditConta = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CalcEditN2 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colBODMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBOD_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditNumero = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.aPARTAMENTOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colBODProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EditorPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI2 = new Framework.objetosNeon.ImagensPI();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bLOCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colBODComCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditContaPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBODCompetencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCompetenciaEdit1 = new CompontesBasicos.RepositoryItemCompetenciaEdit();
            this.colBOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BotParcelar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colBOD_CTL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditCTL = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGradeEventual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditConta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEditN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditNumero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPARTAMENTOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditContaPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCompetenciaEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotParcelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditCTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.Enabled = false;
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Location = new System.Drawing.Point(0, 75);
            this.GridControl_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.GridControl_F.LookAndFeel.UseDefaultLookAndFeel = false;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.EditorPI,
            this.LookUpEditNumero,
            this.LookUpEditConta,
            this.LookUpEditContaPLA,
            this.repositoryItemTextEdit1,
            this.CalcEditN2,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCompetenciaEdit1,
            this.BotParcelar,
            this.LookUpEditCTL});
            this.GridControl_F.Size = new System.Drawing.Size(1504, 664);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.GridView_F.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.GridView_F.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.GridView_F.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.GridView_F.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseFont = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.GridView_F.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.GridView_F.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.Linen;
            this.GridView_F.Appearance.Row.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOD_PLA,
            this.colBODValor,
            this.colBODMensagem,
            this.colBOD_APT,
            this.colBODProprietario,
            this.colBODComCondominio,
            this.gridColumn1,
            this.colBODCompetencia,
            this.colBOD,
            this.gridColumn2,
            this.colBOD_CTL});
            this.GridView_F.NewItemRowText = "Incluir nova linha";
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsCustomization.AllowRowSizing = true;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.PaintStyleName = "Flat";
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBODCompetencia, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOD_APT, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.GridView_F_RowStyle);
            this.GridView_F.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.GridView_F_InitNewRow);
            this.GridView_F.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_F_FocusedRowChanged);
            this.GridView_F.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.GridView_F_ValidateRow_1);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "BOletoDetalhe";
            this.BindingSource_F.DataSource = this.dGradeEventual;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // dGradeEventual
            // 
            this.dGradeEventual.DataSetName = "dGradeEventual";
            this.dGradeEventual.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.CON_selrow = null;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.lookupBlocosAptos_F1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(2, 2);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.NivelCONOculto = 2;
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(1394, 67);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 7;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = null;            
            this.lookupBlocosAptos_F1.alterado += new System.EventHandler(this.lookupBlocosAptos_F1_alterado);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1504, 75);
            this.panelControl1.TabIndex = 8;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.lookupBlocosAptos_F1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1398, 71);
            this.panelControl2.TabIndex = 10;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(1400, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(102, 71);
            this.panelControl3.TabIndex = 11;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(21, 21);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(76, 31);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "CALCULAR";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // colBOD_PLA
            // 
            this.colBOD_PLA.Caption = "Conta";
            this.colBOD_PLA.ColumnEdit = this.LookUpEditConta;
            this.colBOD_PLA.FieldName = "BOD_PLA";
            this.colBOD_PLA.Name = "colBOD_PLA";
            this.colBOD_PLA.Visible = true;
            this.colBOD_PLA.VisibleIndex = 1;
            this.colBOD_PLA.Width = 69;
            // 
            // LookUpEditConta
            // 
            this.LookUpEditConta.AutoHeight = false;
            this.LookUpEditConta.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditConta.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricaoRes", "PLADescricaoRes", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLAProprietario", "PLAProprietario", 80, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditConta.DataSource = this.pLAnocontasBindingSource;
            this.LookUpEditConta.DisplayMember = "PLA";
            this.LookUpEditConta.Name = "LookUpEditConta";
            this.LookUpEditConta.NullText = "-";
            this.LookUpEditConta.ShowHeader = false;
            this.LookUpEditConta.ValueMember = "PLA";
            this.LookUpEditConta.EditValueChanged += new System.EventHandler(this.LookUpEditConta_EditValueChanged);
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.DSCentral);
            this.pLAnocontasBindingSource.Filter = "PLAEVE = 1";
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.ColumnEdit = this.CalcEditN2;
            this.colBODValor.DisplayFormat.FormatString = "n2";
            this.colBODValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 6;
            // 
            // CalcEditN2
            // 
            this.CalcEditN2.AutoHeight = false;
            this.CalcEditN2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEditN2.Mask.EditMask = "n2";
            this.CalcEditN2.Mask.UseMaskAsDisplayFormat = true;
            this.CalcEditN2.Name = "CalcEditN2";
            // 
            // colBODMensagem
            // 
            this.colBODMensagem.Caption = "Mensagem";
            this.colBODMensagem.ColumnEdit = this.repositoryItemTextEdit1;
            this.colBODMensagem.FieldName = "BODMensagem";
            this.colBODMensagem.Name = "colBODMensagem";
            this.colBODMensagem.Visible = true;
            this.colBODMensagem.VisibleIndex = 3;
            this.colBODMensagem.Width = 162;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 50;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colBOD_APT
            // 
            this.colBOD_APT.Caption = "N�mero";
            this.colBOD_APT.ColumnEdit = this.LookUpEditNumero;
            this.colBOD_APT.FieldName = "BOD_APT";
            this.colBOD_APT.Name = "colBOD_APT";
            this.colBOD_APT.Visible = true;
            this.colBOD_APT.VisibleIndex = 0;
            // 
            // LookUpEditNumero
            // 
            this.LookUpEditNumero.AutoHeight = false;
            this.LookUpEditNumero.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditNumero.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLOCONUMERO", "BLOCONUMERO", 83, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditNumero.DataSource = this.aPARTAMENTOSBindingSource;
            this.LookUpEditNumero.DisplayMember = "BLOCONUMERO";
            this.LookUpEditNumero.Name = "LookUpEditNumero";
            this.LookUpEditNumero.NullText = " -";
            this.LookUpEditNumero.ShowHeader = false;
            this.LookUpEditNumero.ValueMember = "APT";
            // 
            // aPARTAMENTOSBindingSource
            // 
            this.aPARTAMENTOSBindingSource.DataMember = "APARTAMENTOS";
            this.aPARTAMENTOSBindingSource.DataSource = typeof(Framework.Lookup.dComponente_F);
            // 
            // colBODProprietario
            // 
            this.colBODProprietario.Caption = "Dest";
            this.colBODProprietario.ColumnEdit = this.EditorPI;
            this.colBODProprietario.FieldName = "BODProprietario";
            this.colBODProprietario.Name = "colBODProprietario";
            this.colBODProprietario.Visible = true;
            this.colBODProprietario.VisibleIndex = 5;
            this.colBODProprietario.Width = 46;
            // 
            // EditorPI
            // 
            this.EditorPI.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.EditorPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EditorPI.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EditorPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("P", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("I", false, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("-", null, -1)});
            this.EditorPI.LargeImages = this.imagensPI2;
            this.EditorPI.Name = "EditorPI";
            this.EditorPI.SmallImages = this.imagensPI2;
            // 
            // imagensPI2
            // 
            this.imagensPI2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI2.ImageStream")));
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(Framework.Buscas.dBuscas);
            // 
            // bLOCOSBindingSource
            // 
            this.bLOCOSBindingSource.DataMember = "BLOCOS";
            this.bLOCOSBindingSource.DataSource = typeof(Framework.Lookup.dComponente_F);
            // 
            // colBODComCondominio
            // 
            this.colBODComCondominio.Caption = "Com condominio";
            this.colBODComCondominio.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colBODComCondominio.FieldName = "BODComCondominio";
            this.colBODComCondominio.Name = "colBODComCondominio";
            this.colBODComCondominio.Visible = true;
            this.colBODComCondominio.VisibleIndex = 7;
            this.colBODComCondominio.Width = 109;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Tipo";
            this.gridColumn1.ColumnEdit = this.LookUpEditContaPLA;
            this.gridColumn1.FieldName = "BOD_PLA";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 118;
            // 
            // LookUpEditContaPLA
            // 
            this.LookUpEditContaPLA.AutoHeight = false;
            this.LookUpEditContaPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditContaPLA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricaoRes", "PLADescricaoRes", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLAProprietario", "PLAProprietario", 80, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditContaPLA.DataSource = this.pLAnocontasBindingSource;
            this.LookUpEditContaPLA.DisplayMember = "PLADescricaoRes";
            this.LookUpEditContaPLA.Name = "LookUpEditContaPLA";
            this.LookUpEditContaPLA.NullText = "-";
            this.LookUpEditContaPLA.ShowHeader = false;
            this.LookUpEditContaPLA.ValueMember = "PLA";
            this.LookUpEditContaPLA.EditValueChanged += new System.EventHandler(this.LookUpEditConta_EditValueChanged);
            // 
            // colBODCompetencia
            // 
            this.colBODCompetencia.AppearanceCell.Options.UseTextOptions = true;
            this.colBODCompetencia.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBODCompetencia.Caption = "Compet�ncia";
            this.colBODCompetencia.ColumnEdit = this.repositoryItemCompetenciaEdit1;
            this.colBODCompetencia.FieldName = "BODCompetencia";
            this.colBODCompetencia.Name = "colBODCompetencia";
            this.colBODCompetencia.Visible = true;
            this.colBODCompetencia.VisibleIndex = 4;
            this.colBODCompetencia.Width = 99;
            // 
            // repositoryItemCompetenciaEdit1
            // 
            this.repositoryItemCompetenciaEdit1.AutoHeight = false;
            this.repositoryItemCompetenciaEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Left, "", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right)});
            this.repositoryItemCompetenciaEdit1.Mask.EditMask = "00/0000";
            this.repositoryItemCompetenciaEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.repositoryItemCompetenciaEdit1.Name = "repositoryItemCompetenciaEdit1";
            // 
            // colBOD
            // 
            this.colBOD.FieldName = "BOD";
            this.colBOD.Name = "colBOD";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.BotParcelar;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 9;
            this.gridColumn2.Width = 30;
            // 
            // BotParcelar
            // 
            this.BotParcelar.AutoHeight = false;
            this.BotParcelar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("BotParcelar.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.BotParcelar.Name = "BotParcelar";
            this.BotParcelar.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotParcelar.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotParcelar_ButtonClick);
            // 
            // colBOD_CTL
            // 
            this.colBOD_CTL.Caption = "Conta";
            this.colBOD_CTL.ColumnEdit = this.LookUpEditCTL;
            this.colBOD_CTL.FieldName = "BOD_CTL";
            this.colBOD_CTL.Name = "colBOD_CTL";
            this.colBOD_CTL.Visible = true;
            this.colBOD_CTL.VisibleIndex = 8;
            this.colBOD_CTL.Width = 141;
            // 
            // LookUpEditCTL
            // 
            this.LookUpEditCTL.AutoHeight = false;
            this.LookUpEditCTL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditCTL.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditCTL.DataSource = this.conTasLogicasBindingSource;
            this.LookUpEditCTL.DisplayMember = "CTLTitulo";
            this.LookUpEditCTL.Name = "LookUpEditCTL";
            this.LookUpEditCTL.NullText = " --";
            this.LookUpEditCTL.ShowHeader = false;
            this.LookUpEditCTL.ValueMember = "CTL";
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = this.dGradeEventual;
            // 
            // cGradeEventual
            // 
            this.BindingSourcePrincipal = this.BindingSource_F;
            this.Controls.Add(this.panelControl1);
            this.dataLayout = new System.DateTime(2014, 7, 14, 14, 33, 35, 0);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGradeEventual";
            this.Size = new System.Drawing.Size(1504, 799);
            this.editando += new System.EventHandler(this.cGradeEventual_editando);
            this.naoeditando += new System.EventHandler(this.cGradeEventual_naoeditando);
            this.cargaInicial += new System.EventHandler(this.cGradeEventual_cargaInicial);
            this.Load += new System.EventHandler(this.cGradeEventual_Load);
            this.Enter += new System.EventHandler(this.cGradeEventual_Enter);
            this.Leave += new System.EventHandler(this.cGradeEventual_Leave);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGradeEventual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditConta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEditN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditNumero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPARTAMENTOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditContaPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCompetenciaEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotParcelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditCTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dGradeEventual dGradeEventual;
        /// <summary>
        /// 
        /// </summary>
        public Framework.Lookup.LookupBlocosAptos_F lookupBlocosAptos_F1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraGrid.Columns.GridColumn colBODMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_APT;
        private DevExpress.XtraGrid.Columns.GridColumn colBODProprietario;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox EditorPI;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditNumero;
        private System.Windows.Forms.BindingSource aPARTAMENTOSBindingSource;
        private System.Windows.Forms.BindingSource bLOCOSBindingSource;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditConta;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBODComCondominio;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditContaPLA;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBODCompetencia;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit CalcEditN2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD;        
        private CompontesBasicos.RepositoryItemCompetenciaEdit repositoryItemCompetenciaEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private Framework.objetosNeon.ImagensPI imagensPI2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotParcelar;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditCTL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_CTL;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
    }
}
