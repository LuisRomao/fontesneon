﻿namespace Boletos.Eventual {


    partial class dEventual
    {
        
        private static dEventualTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public static dEventualTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get {
                if (bOletoDetalheTableAdapter == null) {
                    bOletoDetalheTableAdapter = new Boletos.Eventual.dEventualTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return dEventual.bOletoDetalheTableAdapter; 
            }
            
        }
         
    }
}
