namespace Boletos.Rateios
{
    partial class cGradeRateios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cGradeRateios));
            this.dRateiosGrade = new Boletos.Rateios.dRateiosGrade();
            this.gradeRateiosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gradeRateiosTableAdapter = new Boletos.Rateios.dRateiosGradeTableAdapters.GradeRateiosTableAdapter();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRAT_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRATProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboDestinatario = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colRATHistorico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRATValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRADData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRADValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRADMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRADGerado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRADComCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcompetencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLADescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.chSoGerados = new DevExpress.XtraEditors.CheckEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTLTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRateiosGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradeRateiosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboDestinatario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chSoGerados.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Location = new System.Drawing.Point(0, 53);
            this.groupControl1.Size = new System.Drawing.Size(1567, 84);
            // 
            // LayoutPesquisa_F
            // 
            this.LayoutPesquisa_F.Size = new System.Drawing.Size(1563, 84);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.gradeRateiosBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Location = new System.Drawing.Point(0, 137);
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ComboDestinatario,
            this.repositoryItemLookUpEdit1});
            this.GridControl_F.Size = new System.Drawing.Size(1567, 400);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.GridView_F.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.GridView_F.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.GridView_F.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.GridView_F.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCodigo,
            this.colCONNome,
            this.colRAT,
            this.colRAT_PLA,
            this.colPLADescricao,
            this.colRATProprietario,
            this.colRATHistorico,
            this.colRATValor,
            this.colRADValor,
            this.colRADData,
            this.colRADMensagem,
            this.colRADGerado,
            this.colRADComCondominio,
            this.colcompetencia,
            this.gridColumn1,
            this.colCTLTitulo});
            this.GridView_F.GroupCount = 1;
            this.GridView_F.GroupFormat = "{0}: [#image]{1}  {2} ";
            this.GridView_F.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Max, "RATValor", null, "Valor Total : {0:n2}")});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.AutoExpandAllGroups = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupedColumns = true;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRAT, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRADData, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRATProprietario, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // dRateiosGrade
            // 
            this.dRateiosGrade.DataSetName = "dRateiosGrade";
            this.dRateiosGrade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gradeRateiosBindingSource
            // 
            this.gradeRateiosBindingSource.DataMember = "GradeRateios";
            this.gradeRateiosBindingSource.DataSource = this.dRateiosGrade;
            // 
            // gradeRateiosTableAdapter
            // 
            this.gradeRateiosTableAdapter.ClearBeforeFill = true;
            this.gradeRateiosTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("gradeRateiosTableAdapter.GetNovosDados")));
            this.gradeRateiosTableAdapter.TabelaDataTable = null;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.Width = 99;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Condom�nio";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.Width = 262;
            // 
            // colRAT_PLA
            // 
            this.colRAT_PLA.Caption = "Conta";
            this.colRAT_PLA.FieldName = "RAT_PLA";
            this.colRAT_PLA.Name = "colRAT_PLA";
            this.colRAT_PLA.Visible = true;
            this.colRAT_PLA.VisibleIndex = 4;
            this.colRAT_PLA.Width = 66;
            // 
            // colRATProprietario
            // 
            this.colRATProprietario.Caption = "Propriet�rio";
            this.colRATProprietario.FieldName = "RATProprietario";
            this.colRATProprietario.Name = "colRATProprietario";
            this.colRATProprietario.Visible = true;
            this.colRATProprietario.VisibleIndex = 6;
            this.colRATProprietario.Width = 93;
            // 
            // ComboDestinatario
            // 
            this.ComboDestinatario.AutoHeight = false;
            this.ComboDestinatario.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboDestinatario.Items.AddRange(new object[] {
            "Propriet�rio",
            "Inquilino"});
            this.ComboDestinatario.Name = "ComboDestinatario";
            this.ComboDestinatario.ReadOnly = true;
            // 
            // colRATHistorico
            // 
            this.colRATHistorico.Caption = "Hist�rico";
            this.colRATHistorico.FieldName = "RATHistorico";
            this.colRATHistorico.Name = "colRATHistorico";
            this.colRATHistorico.Width = 71;
            // 
            // colRATValor
            // 
            this.colRATValor.Caption = "Valor TOTAL";
            this.colRATValor.FieldName = "RATValor";
            this.colRATValor.GroupFormat.FormatString = "n2";
            this.colRATValor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRATValor.Name = "colRATValor";
            this.colRATValor.Width = 117;
            // 
            // colRADData
            // 
            this.colRADData.Caption = "Vencto";
            this.colRADData.FieldName = "RADData";
            this.colRADData.Name = "colRADData";
            this.colRADData.Visible = true;
            this.colRADData.VisibleIndex = 3;
            this.colRADData.Width = 77;
            // 
            // colRADValor
            // 
            this.colRADValor.Caption = "Valor da Parcela";
            this.colRADValor.DisplayFormat.FormatString = "n2";
            this.colRADValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRADValor.FieldName = "RADValor";
            this.colRADValor.Name = "colRADValor";
            this.colRADValor.Visible = true;
            this.colRADValor.VisibleIndex = 7;
            this.colRADValor.Width = 101;
            // 
            // colRADMensagem
            // 
            this.colRADMensagem.Caption = "Mensagem";
            this.colRADMensagem.FieldName = "RADMensagem";
            this.colRADMensagem.Name = "colRADMensagem";
            this.colRADMensagem.Visible = true;
            this.colRADMensagem.VisibleIndex = 1;
            this.colRADMensagem.Width = 96;
            // 
            // colRADGerado
            // 
            this.colRADGerado.Caption = "Emitido";
            this.colRADGerado.FieldName = "RADGerado";
            this.colRADGerado.Name = "colRADGerado";
            this.colRADGerado.Visible = true;
            this.colRADGerado.VisibleIndex = 8;
            this.colRADGerado.Width = 54;
            // 
            // colRADComCondominio
            // 
            this.colRADComCondominio.Caption = "Com Condominio";
            this.colRADComCondominio.FieldName = "RADComCondominio";
            this.colRADComCondominio.Name = "colRADComCondominio";
            this.colRADComCondominio.Width = 115;
            // 
            // colcompetencia
            // 
            this.colcompetencia.Caption = "Compet�ncia";
            this.colcompetencia.FieldName = "Competencia";
            this.colcompetencia.Name = "colcompetencia";
            this.colcompetencia.OptionsColumn.ReadOnly = true;
            this.colcompetencia.Visible = true;
            this.colcompetencia.VisibleIndex = 2;
            this.colcompetencia.Width = 95;
            // 
            // colRAT
            // 
            this.colRAT.Caption = "Rateio";
            this.colRAT.FieldName = "RAT";
            this.colRAT.Name = "colRAT";
            this.colRAT.OptionsColumn.ReadOnly = true;
            this.colRAT.Visible = true;
            this.colRAT.VisibleIndex = 0;
            this.colRAT.Width = 84;
            // 
            // colPLADescricao
            // 
            this.colPLADescricao.Caption = "Descri��o da conta";
            this.colPLADescricao.FieldName = "PLADescricao";
            this.colPLADescricao.Name = "colPLADescricao";
            this.colPLADescricao.Visible = true;
            this.colPLADescricao.VisibleIndex = 5;
            this.colPLADescricao.Width = 145;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.Top;
            this.lookupCondominio_F1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(2, 2);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(1563, 28);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 10;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;            
            this.lookupCondominio_F1.alterado += new System.EventHandler(this.lookupCondominio_F1_alterado);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.chSoGerados);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1567, 53);
            this.panelControl1.TabIndex = 11;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(190, 24);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(256, 23);
            this.simpleButton1.TabIndex = 12;
            this.simpleButton1.Text = "Revisar Rateios";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // chSoGerados
            // 
            this.chSoGerados.EditValue = true;
            this.chSoGerados.Location = new System.Drawing.Point(7, 28);
            this.chSoGerados.Name = "chSoGerados";
            this.chSoGerados.Properties.Caption = "Somente parcelas n�o geradas";
            this.chSoGerados.Size = new System.Drawing.Size(244, 19);
            this.chSoGerados.TabIndex = 11;
            this.chSoGerados.CheckedChanged += new System.EventHandler(this.chSoGerados_CheckedChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Obs";
            this.gridColumn1.FieldName = "Obs";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 10;
            this.gridColumn1.Width = 121;
            // 
            // colCTLTitulo
            // 
            this.colCTLTitulo.Caption = "Conta";
            this.colCTLTitulo.FieldName = "CTLTitulo";
            this.colCTLTitulo.Name = "colCTLTitulo";
            this.colCTLTitulo.Visible = true;
            this.colCTLTitulo.VisibleIndex = 9;
            this.colCTLTitulo.Width = 142;
            // 
            // cGradeRateios
            // 
            this.CampoTab = "RAT";
            this.ComponenteCampos = typeof(Boletos.Rateios.cRateio);
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGradeRateios";
            this.PrefixoTab = "Rateio:";
            this.Size = new System.Drawing.Size(1567, 597);
            this.Titulo = "Rateios";
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRateiosGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradeRateiosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboDestinatario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chSoGerados.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource gradeRateiosBindingSource;
        private dRateiosGrade dRateiosGrade;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colRAT_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colPLADescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colRATProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colRATHistorico;
        private DevExpress.XtraGrid.Columns.GridColumn colRATValor;
        private DevExpress.XtraGrid.Columns.GridColumn colRADData;
        private DevExpress.XtraGrid.Columns.GridColumn colRADValor;
        private DevExpress.XtraGrid.Columns.GridColumn colRADMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colRADGerado;
        private DevExpress.XtraGrid.Columns.GridColumn colRADComCondominio;
        private DevExpress.XtraGrid.Columns.GridColumn colcompetencia;
        private DevExpress.XtraGrid.Columns.GridColumn colRAT;
        private Boletos.Rateios.dRateiosGradeTableAdapters.GradeRateiosTableAdapter gradeRateiosTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox ComboDestinatario;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        /// <summary>
        /// 
        /// </summary>
        public Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chSoGerados;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLTitulo;
    }
}
