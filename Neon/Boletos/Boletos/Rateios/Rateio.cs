using System;
using System.Collections.Generic;
using VirEnumeracoesNeon;
using Framework.objetosNeon;

namespace Boletos.Rateios
{
    /// <summary>
    /// Inteligencia dos rateios
    /// </summary>
    public class Rateio
    {
        private dRateios _dRateios;

        /// <summary>
        /// Dataset
        /// </summary>
        public dRateios dRateios
        {
            get 
            {
                if (_dRateios == null)
                    _dRateios = new dRateios();
                return _dRateios;
            }
            set { _dRateios = value; }
        }

        private dRateios.RATeiosRow _RATrow;

        /// <summary>
        /// 
        /// </summary>
        public dRateios.RATeiosRow RATrow
        {
            get { return _RATrow; }
            set { _RATrow = value; }
        }

        /// <summary>
        /// Encontrado
        /// </summary>
        public bool Encontrado;

        /// <summary>
        /// Construitor
        /// </summary>
        /// <param name="RAT"></param>
        public Rateio(int RAT)
        {
            Iniciar();
            if (dRateios.RATeiosTableAdapter.FillBy(dRateios.RATeios, RAT) == 1)
            {
                _RATrow = dRateios.RATeios[0];
                dRateios.RAteioDetalhesTableAdapter.FillEspecifico(dRateios.RAteioDetalhes, RAT);
                dRateios.BOletoDetalheRADTableAdapter.FillGeral(dRateios.BOletoDetalheRAD, RAT);
                Encontrado = true;
            }
            else
                Encontrado = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        public void CriarNovo(int CON)
        {
            //_RATrow = dRateios.RATeios.NewRATeiosRow();
            //_RATrow.RAT_CON = CON;
            //_RATrow.RATComCondominio = true;
            //_RATrow.RAT_PLA = "";
            //_RATrow.RATI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
            //_RATrow.RATDATAI = DateTime.Now;
            //dRateios.RATeios.AddRATeiosRow(_RATrow);
            Encontrado = true;
        }

        private dRateios.CONDOMINIOSRow _CONrow;

        /// <summary>
        /// 
        /// </summary>
        public dRateios.CONDOMINIOSRow CONrow
        {
            get
            {
                if(_CONrow == null)
                {
                    dRateios.CONDOMINIOSTableAdapter.FillByCON(dRateios.CONDOMINIOS, RATrow.RAT_CON);
                    _CONrow = dRateios.CONDOMINIOS[0];
                }
                return _CONrow;
            }
        }


        private dRateios.ListaFracaoDataTable _TabLista;

        /// <summary>
        /// Lista de fra��es adaptada e normalizada
        /// </summary>
        public dRateios.ListaFracaoDataTable TabLista
        {
            get 
            {
                if (_TabLista == null)
                {
                    _TabLista = dRateios.ListaFracaoTableAdapter.GetData(RATrow.RAT_CON);
                    foreach (dRateios.ListaFracaoRow ListaFrow in _TabLista)
                        if (!dRateios.ChBlocos.FindByBLO(ListaFrow.BLO).Ch)
                            ListaFrow.Delete();
                    _TabLista.AcceptChanges();
                }
                return _TabLista;
            }            
        }

        /// <summary>
        /// Re-inicia a tabela de fra��es
        /// </summary>
        public void ResetTabLista()
        {
            _TabLista = null;
            _FM = null;
        }

        private bool DadosParaGerarCarregados = false;

        private void GravaDescontos()
        {
            foreach (dRateios.DescontosRow rowDesc in dRateios.Descontos)
            {
                dRateios.RAteioAplicardescontoRow rowRAA = rowDesc.RAteioAplicardescontoRow;
                if (rowRAA == null)                    
                {                    
                    if (RATrow.RAT > 0)
                    {
                        dRateios.RAteioAplicardesconto.AddRAteioAplicardescontoRow(RATrow.RAT, rowDesc.CDR, rowDesc.Aplicar);
                        dRateios.RAteioAplicardescontoTableAdapter.Update(dRateios.RAteioAplicardesconto);
                    }
                }
            }
        }

        /// <summary>
        /// Carga dos dados necessarios para a gera��o de um novo rateio ou re-gera��o
        /// </summary>
        public void CarregarDadosParaGerar()
        {
            if (DadosParaGerarCarregados)
                return;
            DadosParaGerarCarregados = true;
            dRateios.DescontosTableAdapter.Fill(dRateios.Descontos, RATrow.RAT_CON);
            dRateios.RAteioAplicardescontoTableAdapter.Fill(dRateios.RAteioAplicardesconto, RATrow.RAT);

            foreach (dRateios.DescontosRow rowDesc in dRateios.Descontos)
            {
                dRateios.RAteioAplicardescontoRow rowRAA = rowDesc.RAteioAplicardescontoRow;
                if (rowRAA != null)
                    rowDesc.Aplicar = rowRAA.RAAAplicar;
                else
                {
                    rowDesc.Aplicar = true;
                    if (RATrow.RAT > 0)
                    {
                        dRateios.RAteioAplicardesconto.AddRAteioAplicardescontoRow(RATrow.RAT, rowDesc.CDR, true);
                        dRateios.RAteioAplicardescontoTableAdapter.Update(dRateios.RAteioAplicardesconto);
                    }
                }
            }
            dRateios.RAteioBlocoTableAdapter.LeBlocos(dRateios.RAteioBloco, RATrow.RAT, RATrow.RAT_CON);
            bool TodosNulos = true;
            dRateios.ChBlocos.Clear();
            foreach (dRateios.RAteioBlocoRow linhabloco in dRateios.RAteioBloco)
                if (!linhabloco.IsBAB_BLONull())
                {
                    TodosNulos = false;
                    break;
                }
            foreach (dRateios.RAteioBlocoRow linhabloco in dRateios.RAteioBloco)
                dRateios.ChBlocos.AddChBlocosRow(TodosNulos || (!linhabloco.IsBAB_BLONull()),string.Format("{0} - {1}",linhabloco.BLOCodigo,linhabloco.BLONome),linhabloco.BLO);            
        }

        private void Iniciar()
        {
            dRateios.TiposDeFracao.Clear();
            dRateios.TiposDeFracao.AddTiposDeFracaoRow(0, "Partes iguais");
            dRateios.TiposDeFracao.AddTiposDeFracaoRow(1, "Fra��o Ideal padr�o");
            dRateios.TiposDeFracao.AddTiposDeFracaoRow(-1, "Manual");
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal DescontoEmValor;

        private void AplicaDescontosCorpoDiretivo()
        {
            DescontoEmValor = 0;
            foreach (dRateios.ListaFracaoRow linhaLista in TabLista)
            {
                if (linhaLista.IsCDRDescontoRateiosNull())
                    continue;
                if ((linhaLista.CDRDescontoRateios > 0) && dRateios.Descontos.FindByCDR(linhaLista.CDR).Aplicar)
                {
                    if (!linhaLista.CDRDescontoRateiosValor)
                        linhaLista.FatorFracao = 1 - (linhaLista.CDRDescontoRateios / 100);
                    else
                        DescontoEmValor += linhaLista.CDRDescontoRateios;
                }
                
            };
        }

        private FracaoManual _FM;

        private FracaoManual FM
        {
            get 
            {
                if (_FM == null)
                {
                     _FM = new FracaoManual();
                     _FM.DRateio = dRateios;
                     _FM.Tabela = TabLista;
                     _FM.listaFracaoDataTableBindingSource.DataSource = TabLista;
                }
                return _FM;
            }
        }

        private void AlteraManual()
        {                        
            if ((RATrow["RATValor"] != null) && (RATrow["RATValor"] != DBNull.Value))
                    FM.ValorTotalSim = RATrow.RATValor;
            else
                    FM.ValorTotalSim = 0;
            FM.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);            
        }

        /// <summary>
        /// Prepara os dados para geracao.
        /// </summary>
        /// <param name="ReabrirTelaseManual"></param>
        public void preparagerar(bool ReabrirTelaseManual)
        {                                    
            switch (RATrow.RATFracao)
            {
                case -1:
                    if (_TabLista == null)
                    {
                        AplicaDescontosCorpoDiretivo();
                        ReabrirTelaseManual = true;
                    }
                    if (ReabrirTelaseManual)                                            
                        AlteraManual();
                    break;
                case 0:
                    AplicaDescontosCorpoDiretivo();
                    dRateios.CalculaFracaoCorrigida(dRateios.TipoSimula.SemFracao, TabLista, 0);
                    break;
                case 1:
                    AplicaDescontosCorpoDiretivo();
                    dRateios.CalculaFracaoCorrigida(dRateios.TipoSimula.ComFracao, TabLista, 0);
                    break;
            };
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cCompetData1"></param>
        /// <param name="TotalDeParcelas"></param>
        /// <param name="NumeroInicial"></param>
        public void DividirParcelas(cCompetData cCompetData1, int TotalDeParcelas, int NumeroInicial)
        {
            Salvar(false,false);
            int ParcelasGeradas;
            //Int16 mes;
            //Int16 ano;
            if (RATrow.RAT == 0)
            {                
                dRateios.BOletoDetalheRAD.Clear();
                dRateios.RAteioDetalhes.Clear();
                ParcelasGeradas = 0;
            }
            else
            {                
                dRateios.RAteioDetalhesTableAdapter.ApagaBODnaoGerado(RATrow.RAT);
                dRateios.RAteioDetalhesTableAdapter.ApagaRADnaoGerado(RATrow.RAT);
                dRateios.BOletoDetalheRAD.Clear();
                ParcelasGeradas = dRateios.RAteioDetalhesTableAdapter.FillEspecifico(dRateios.RAteioDetalhes, RATrow.RAT);
                dRateios.BOletoDetalheRADTableAdapter.FillGeral(dRateios.BOletoDetalheRAD, RATrow.RAT);
            }                        
            
            Competencia Compet = cCompetData1.GetCompetencia();
            decimal TotalParaDividir = RATrow.RATValor;
            foreach (dRateios.RAteioDetalhesRow linhaGeradaAntes in dRateios.RAteioDetalhes)
                TotalParaDividir -= linhaGeradaAntes.RADValor;
            decimal Saldo = TotalParaDividir;
            for (int i = ParcelasGeradas; i < TotalDeParcelas; i++)
            {
                dRateios.RAteioDetalhesRow novaLinha = dRateios.RAteioDetalhes.NewRAteioDetalhesRow();
                novaLinha.RAD_RAT = RATrow.RAT;
                novaLinha.RADComCondominio = RATrow.RATComCondominio = cCompetData1.Comcondominio;
                if (!RATrow.IsRAT_CTLNull())
                    novaLinha.RAD_CTL = RATrow.RAT_CTL;
                if (cCompetData1.Comcondominio)
                {
                    novaLinha.RADData = Compet.CalculaVencimento();
                    novaLinha.RADCompetenciaAno = (Int16)Compet.Ano;
                    novaLinha.RADCompetenciaMes = (Int16)Compet.Mes;
                    Compet.Add(1);
                }
                else
                {
                    novaLinha.RADData = cCompetData1.dateEdit1.DateTime.AddMonths(i - ParcelasGeradas);
                    novaLinha.RADCompetenciaAno = (Int16)novaLinha.RADData.Year;
                    novaLinha.RADCompetenciaMes = (Int16)novaLinha.RADData.Month;
                }


                novaLinha.RADGerado = false;
                novaLinha.RADMensagem = RATrow.RATMensagem;
                if ((TotalDeParcelas > 1) || (NumeroInicial > 1))
                    novaLinha.RADMensagem += " (" + (i + NumeroInicial).ToString() + "/" + (TotalDeParcelas + NumeroInicial - 1).ToString() + ")";
                if (i == (TotalDeParcelas - 1))
                    novaLinha.RADValor = Saldo;
                else
                {
                    novaLinha.RADValor = decimal.Round(TotalParaDividir / (TotalDeParcelas - ParcelasGeradas), 2);
                    Saldo -= novaLinha.RADValor;
                };
                novaLinha.EndEdit();
                dRateios.RAteioDetalhes.AddRAteioDetalhesRow(novaLinha);
                dRateios.RAteioDetalhesTableAdapter.Update(novaLinha);
                novaLinha.AcceptChanges();
            }
            //Salvar(false);
        }

        
        /// <summary>
        /// 
        /// </summary>
        public void GerarTodos()
        {
            CarregarDadosParaGerar();
            foreach (dRateios.RAteioDetalhesRow LinhaDetalhe in dRateios.RAteioDetalhes)
                if (!LinhaDetalhe.RADGerado)
                    GeraUM(LinhaDetalhe,false);
            //AjustaTotaldoRateio();
            Salvar(false,true);
        }

        /// <summary>
        /// Gera uma parcela do rateio
        /// </summary>
        /// <param name="LinhaDetalhe"></param>
        /// <param name="Simular"></param>
        public bool GeraUM(dRateios.RAteioDetalhesRow LinhaDetalhe,bool Simular)
        {
            CarregarDadosParaGerar();
            if (LinhaDetalhe.RADGerado)
                return false;            
            preparagerar(false);
            decimal Total = 0;
            if (!Simular)
            {
                System.Collections.ArrayList Matar = new System.Collections.ArrayList();
                foreach (dRateios.BOletoDetalheRADRow velhalinha in dRateios.BOletoDetalheRAD.Rows)
                    if (velhalinha.RowState != System.Data.DataRowState.Deleted)
                        if (velhalinha.BOD_RAD == LinhaDetalhe.RAD)
                            Matar.Add(velhalinha);
                foreach (dRateios.BOletoDetalheRADRow velhalinha in Matar)
                    velhalinha.Delete();
            };
            decimal ValorRatear = LinhaDetalhe.RADValor + DescontoEmValor;
            foreach (dRateios.ListaFracaoRow linhaLista in TabLista)
            {
                decimal ValorLiquido = 0;
                dRateios.BOletoDetalheRADRow novalinha;
                if (!Simular)
                    novalinha = dRateios.BOletoDetalheRAD.NewBOletoDetalheRADRow();
                else
                    novalinha = TabelaSimulaBOletoDetalheRAD.NewBOletoDetalheRADRow();
                novalinha.BOD_RAD = LinhaDetalhe.RAD;
                novalinha.BOD_CON = CONrow.CON;
                novalinha.BOD_APT = linhaLista.APT;
                novalinha.BOD_PLA = RATrow.RAT_PLA;
                if (!RATrow.IsRAT_CTLNull())
                    novalinha.BOD_CTL = RATrow.RAT_CTL;
                //novalinha.BODFracaoEfetiva = linhaLista.FracaoCorrigida;                
                novalinha.BODPrevisto = true;                
                novalinha.BODValor = decimal.Round(ValorRatear * linhaLista.FracaoReferenciaSemDesconto, 2);
                ValorLiquido = decimal.Round(ValorRatear * linhaLista.FracaoCorrigida, 2);                
                novalinha.BODComCondominio = LinhaDetalhe.RADComCondominio;
                novalinha.BODData = LinhaDetalhe.RADData;
                novalinha.BODCompetencia = LinhaDetalhe.RADCompetenciaAno *100 + LinhaDetalhe.RADCompetenciaMes;
                Total += novalinha.BODValor;
                novalinha.BODMensagem = LinhaDetalhe.RADMensagem;
                novalinha.BODProprietario = RATrow.RATProprietario;
                if (!Simular)
                {
                    dRateios.BOletoDetalheRAD.AddBOletoDetalheRADRow(novalinha);
                    dRateios.BOletoDetalheRADTableAdapter.Update(novalinha);
                    novalinha.AcceptChanges();
                }
                else
                    TabelaSimulaBOletoDetalheRAD.AddBOletoDetalheRADRow(novalinha);
                if (!linhaLista.IsCDRDescontoRateiosValorNull())
                        if ((linhaLista.CDRDescontoRateiosValor) && (dRateios.Descontos.FindByCDR(linhaLista.CDR).Aplicar))
                            ValorLiquido -= linhaLista.CDRDescontoRateios;


                if (ValorLiquido != novalinha.BODValor)
                {
                    dRateios.BOletoDetalheRADRow novalinhadesc;
                    if (!Simular)
                        novalinhadesc = dRateios.BOletoDetalheRAD.NewBOletoDetalheRADRow();
                    else
                        novalinhadesc = TabelaSimulaBOletoDetalheRAD.NewBOletoDetalheRADRow();
                    novalinhadesc.BOD_RAD = novalinha.BOD_RAD;
                    novalinhadesc.BOD_CON = CONrow.CON;
                    novalinhadesc.BOD_APT = novalinha.BOD_APT;
                    novalinhadesc.BOD_PLA = novalinha.BOD_PLA;
                    novalinhadesc.BODPrevisto = true;
                    novalinhadesc.BODValor = ValorLiquido - novalinha.BODValor;
                    novalinhadesc.BODComCondominio = novalinha.BODComCondominio;
                    novalinhadesc.BODData = novalinha.BODData;
                    novalinhadesc.BODCompetencia = novalinha.BODCompetencia;                    
                    Total += novalinhadesc.BODValor;
                    if (linhaLista.IsCGONomeNull())
                        novalinhadesc.BODMensagem = string.Format("Desconto - {0}", LinhaDetalhe.RADMensagem);
                    else
                        novalinhadesc.BODMensagem = string.Format("Desconto ({0}) - {1}", linhaLista.CGONome, LinhaDetalhe.RADMensagem);
                    if (novalinhadesc.BODMensagem.Length > 50)
                        novalinhadesc.BODMensagem = string.Format("Desconto - {0}", LinhaDetalhe.RADMensagem);
                    novalinhadesc.BODProprietario = novalinha.BODProprietario;
                    if (!linhaLista.IsCDRINSSNull())
                        novalinhadesc.BODRetencao = linhaLista.CDRINSS;
                    else
                        novalinhadesc.BODRetencao = (int)INSSSindico.NaoRecolhe;
                    if (!Simular)
                    {
                        dRateios.BOletoDetalheRAD.AddBOletoDetalheRADRow(novalinhadesc);
                        dRateios.BOletoDetalheRADTableAdapter.Update(novalinhadesc);
                        novalinhadesc.AcceptChanges();
                    }
                    else
                        TabelaSimulaBOletoDetalheRAD.AddBOletoDetalheRADRow(novalinhadesc);
                };

            };
            LinhaDetalhe.RADValor = Total;            
            return true;
        }

        /// <summary>
        /// Re-Calcula o total pelas parcelas
        /// </summary>
        public void AjustaTotaldoRateio()
        {
            decimal TotalDoRateio = 0;
            foreach (dRateios.RAteioDetalhesRow LinhaDetalhe in dRateios.RAteioDetalhes)
            {
                decimal TotalRAD = 0;
                foreach (dRateios.BOletoDetalheRADRow LinhaBOD in LinhaDetalhe.GetBOletoDetalheRADRows())
                    TotalRAD += LinhaBOD.BODValor;
                if (TotalRAD > 0)
                    LinhaDetalhe.RADValor = TotalRAD;
                TotalDoRateio += LinhaDetalhe.RADValor;
            };
            if (TotalDoRateio != 0)
                RATrow.RATValor = TotalDoRateio;
        }

        /// <summary>
        /// Salva as altera��e feitas nas tabelas de dRateio
        /// </summary>
        /// <returns></returns>
        public bool Salvar(bool Apagaroszeros,bool AjustarValor)
        {
            if (AjustarValor)
                AjustaTotaldoRateio();           
            dRateios.RATeiosTableAdapter.Update(RATrow);
            RATrow.AcceptChanges();            
            dRateios.RATeiosTableAdapter.ApagaBlocos(RATrow.RAT);            
            foreach (dRateios.ChBlocosRow rowCH in dRateios.ChBlocos)
                if (rowCH.Ch)
                    dRateios.RATeiosTableAdapter.GravaBloco(RATrow.RAT, rowCH.BLO);
            dRateios.RAteioDetalhesTableAdapter.Update(dRateios.RAteioDetalhes);
            dRateios.RAteioDetalhes.AcceptChanges();
            if (Apagaroszeros)
                Apagazeros();
            int gravados = dRateios.BOletoDetalheRADTableAdapter.Update(dRateios.BOletoDetalheRAD);
            dRateios.BOletoDetalheRAD.AcceptChanges();
            GravaDescontos();
            return true;
        }


        private void Apagazeros()
        {
            System.Collections.ArrayList Matar = new System.Collections.ArrayList();
            foreach (dRateios.BOletoDetalheRADRow linhax in dRateios.BOletoDetalheRAD.Rows)
                if ((linhax.RowState != System.Data.DataRowState.Deleted) && (linhax.BODValor == 0))
                    Matar.Add(linhax);
            foreach (dRateios.BOletoDetalheRADRow linhax in Matar)
                linhax.Delete();
        }

        private dRateios.BOletoDetalheRADDataTable TabelaSimulaBOletoDetalheRAD;

        private StatusRegerar TestaUnidade(dRateios.RAteioDetalhesRow LinhaDetalhe, int APT, decimal ValorSimula, StatusRegerar StatusAnterior, System.Collections.ArrayList Parcelas)
        {
            RelatorioSimulacao += string.Format("");
            bool APTcomcargo = false;
            foreach (decimal ValorParc in Parcelas)
                if (ValorParc < 0)
                    APTcomcargo = true;
            decimal TotalAPTReal = 0;
            dRateios.BOletoDetalheRADRow Equivalente = null;
            foreach (dRateios.BOletoDetalheRADRow rowBODReal in LinhaDetalhe.GetBOletoDetalheRADRows())
            {
                if (rowBODReal.BOD_APT == APT)
                {
                    Equivalente = rowBODReal;
                    TotalAPTReal += rowBODReal.BODValor;
                    if (rowBODReal.BODValor < 0)
                        APTcomcargo = true;
                    int iParcela = Parcelas.IndexOf(rowBODReal.BODValor);
                    if (iParcela == -1)
                        StatusAnterior = StatusAnterior > StatusRegerar.ParcelasEquivaletes ? StatusAnterior : StatusRegerar.ParcelasEquivaletes;
                    else
                        Parcelas.RemoveAt(iParcela);
                }
            };
            if (Equivalente == null)
            {
                Equivalente = dRateios.BOletoDetalheRAD.NewBOletoDetalheRADRow();
                Equivalente.BOD_APT = APT;
                Equivalente.BOD_RAD = LinhaDetalhe.RAD;
                Equivalente.BOD_CON = CONrow.CON;
                Equivalente.BODValor = 0;
                Equivalente.BODMensagem = "N�o Gerado";
                Equivalente.BOD_PLA = LinhaDetalhe.RATeiosRow.RAT_PLA;
                Equivalente.BODProprietario = true;
                Equivalente.BODPrevisto = true;
                Equivalente.BODData = LinhaDetalhe.RADData;
                Equivalente.BODCompetencia = LinhaDetalhe.RADCompetenciaAno * 100 + LinhaDetalhe.RADCompetenciaMes;
                Equivalente.BODComCondominio = LinhaDetalhe.RADComCondominio;
                dRateios.BOletoDetalheRAD.AddBOletoDetalheRADRow(Equivalente);               
            }
            Equivalente.Simulado = ValorSimula;
            
                
            
            if (Math.Abs(ValorSimula - TotalAPTReal) > 0.02M)
            {
                if (!APTcomcargo)
                    return StatusAnterior > StatusRegerar.Diferente ? StatusAnterior : StatusRegerar.Diferente;
                else
                    return StatusAnterior > StatusRegerar.TrocaCargo ? StatusAnterior : StatusRegerar.TrocaCargo;
            }
            else
                if (Parcelas.Count != 0)
                    return StatusAnterior > StatusRegerar.ParcelasEquivaletes ? StatusAnterior : StatusRegerar.ParcelasEquivaletes;
                else
                    return StatusAnterior;
            
        }

        /// <summary>
        /// 
        /// </summary>
        public string RelatorioSimulacao;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public StatusRegerar SimularRegerar()
        {
            StatusRegerar Resposta = StatusRegerar.Identico;
            RelatorioSimulacao = "      Relatorio de simula��o\r\n----------------------------------\r\n";
            foreach (dRateios.RAteioDetalhesRow LinhaDetalhe in dRateios.RAteioDetalhes)
            {
                RelatorioSimulacao += string.Format("\r\nParcela :{0} Valor {1:n2}\r\n", LinhaDetalhe.RADMensagem, LinhaDetalhe.RADValor);
                if (RATrow.RATFracao == -1)
                {
                    RelatorioSimulacao += string.Format("Rateio manual\r\n");
                    return StatusRegerar.Manual;
                }
                TabelaSimulaBOletoDetalheRAD = new dRateios.BOletoDetalheRADDataTable();
                if (!LinhaDetalhe.RADGerado)
                {
                    GeraUM(LinhaDetalhe, true);
                    int APT = 0;
                    decimal TotalAPT = 0;
                    decimal TotaGeral = 0;
                    System.Collections.ArrayList Parcelas = new System.Collections.ArrayList();
                    foreach (dRateios.BOletoDetalheRADRow LinhaBODSimula in TabelaSimulaBOletoDetalheRAD)
                    {
                        if (LinhaBODSimula.BODValor == 0)
                            continue;
                        TotaGeral += LinhaBODSimula.BODValor;
                        if (LinhaBODSimula.BOD_APT == APT)
                        {
                            TotalAPT += LinhaBODSimula.BODValor;
                            Parcelas.Add(LinhaBODSimula.BODValor);
                        }
                        else
                        {
                            if (APT != 0)
                                Resposta = TestaUnidade(LinhaDetalhe, APT, TotalAPT, Resposta, Parcelas);
                            TotalAPT = LinhaBODSimula.BODValor;
                            Parcelas.Clear();
                            Parcelas.Add(LinhaBODSimula.BODValor);
                            APT = LinhaBODSimula.BOD_APT;
                        }
                    }
                    if (APT != 0)
                        Resposta = TestaUnidade(LinhaDetalhe, APT, TotalAPT, Resposta, Parcelas);
                    if (TotaGeral != LinhaDetalhe.RADValor)
                        Resposta = StatusRegerar.Diferente;
                }
            }
            Console.WriteLine(RelatorioSimulacao);
            return Resposta;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public static SortedList<int,StatusRegerar> RevisarRateios(int CON)
        {
            SortedList<int, StatusRegerar> Retorno = new SortedList<int, StatusRegerar>();
            string BuscaRATs =
"SELECT     distinct RATeios.RAT\r\n" +
"FROM         RATeios INNER JOIN\r\n" +
"                      RAteioDetalhes ON RATeios.RAT = RAteioDetalhes.RAD_RAT\r\n" +
"WHERE     (RAteioDetalhes.RADGerado = 0) AND (RATeios.RAT_CON = @P1)\r\n" +
"ORDER BY RATeios.RAT;";
            System.Data.DataTable RATs = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(BuscaRATs, CON);
            foreach (System.Data.DataRow DR in RATs.Rows)
            {
                Rateio NovoRateio = new Rateio((int)DR[0]);
                StatusRegerar ResultadoSimulacao = NovoRateio.SimularRegerar();
                Retorno.Add((int)DR[0], ResultadoSimulacao);
                switch (ResultadoSimulacao)
                {
                    case StatusRegerar.Identico:
                        break;
                    case StatusRegerar.ParcelasEquivaletes:
                        NovoRateio.GerarTodos();
                        NovoRateio.Salvar(true,true);
                        break;
                    case StatusRegerar.TrocaCargo:
                        NovoRateio.GerarTodos();
                        NovoRateio.Salvar(true,true);
                        break;
                    case StatusRegerar.Diferente:                                            
                    case StatusRegerar.Manual:                       
                    default:                        
                        break;
                }
            }
            return Retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        public enum StatusRegerar {
            /// <summary>
            /// O recalculo resulto no mesmo resultado
            /// </summary>
            Identico,
            /// <summary>
            /// Os valores de cada unidade sao igurais porem com parcela de desconto
            /// </summary>
            ParcelasEquivaletes,
            /// <summary>
            /// Troca de valor somente para unidades com troca de cargo
            /// </summary>
            TrocaCargo,
            /// <summary>
            /// Parcelamento diferente
            /// </summary>
            Diferente,
            /// <summary>
            /// Rateio gerado manualmente
            /// </summary>
            Manual
        };
    }
}
