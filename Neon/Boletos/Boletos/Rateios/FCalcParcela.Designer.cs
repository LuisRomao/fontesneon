namespace Boletos.Rateios
{
    partial class FCalcParcela
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditEntrada = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.NumeroParcelas = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditTotPar = new DevExpress.XtraEditors.CalcEdit();
            this.TotRat = new DevExpress.XtraEditors.CalcEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.oksimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditEntrada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroParcelas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditTotPar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotRat.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // radioGroup1
            // 
            this.radioGroup1.EditValue = 0;
            this.radioGroup1.Location = new System.Drawing.Point(75, 12);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Valor m�ximo"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Valor m�nimo")});
            this.radioGroup1.Size = new System.Drawing.Size(104, 59);
            this.radioGroup1.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Crit�rio:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(203, 12);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(28, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Valor:";
            // 
            // calcEditEntrada
            // 
            this.calcEditEntrada.Location = new System.Drawing.Point(260, 9);
            this.calcEditEntrada.Name = "calcEditEntrada";
            this.calcEditEntrada.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditEntrada.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.calcEditEntrada.Properties.Appearance.Options.UseFont = true;
            this.calcEditEntrada.Properties.Appearance.Options.UseForeColor = true;
            this.calcEditEntrada.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditEntrada.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditEntrada.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditEntrada.Properties.EditFormat.FormatString = "n2";
            this.calcEditEntrada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditEntrada.Size = new System.Drawing.Size(130, 22);
            this.calcEditEntrada.TabIndex = 3;
            this.calcEditEntrada.EditValueChanged += new System.EventHandler(this.calcEditEntrada_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 92);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(99, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "N�mero de parcelas:";
            // 
            // NumeroParcelas
            // 
            this.NumeroParcelas.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NumeroParcelas.Location = new System.Drawing.Point(117, 89);
            this.NumeroParcelas.Name = "NumeroParcelas";
            this.NumeroParcelas.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.NumeroParcelas.Size = new System.Drawing.Size(100, 20);
            this.NumeroParcelas.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(16, 191);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(108, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Valor Total da Parcela:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(16, 233);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(101, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Valor Total do rateio:";
            // 
            // calcEditTotPar
            // 
            this.calcEditTotPar.Location = new System.Drawing.Point(130, 186);
            this.calcEditTotPar.Name = "calcEditTotPar";
            this.calcEditTotPar.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditTotPar.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.calcEditTotPar.Properties.Appearance.Options.UseFont = true;
            this.calcEditTotPar.Properties.Appearance.Options.UseForeColor = true;
            this.calcEditTotPar.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditTotPar.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditTotPar.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditTotPar.Properties.EditFormat.FormatString = "n2";
            this.calcEditTotPar.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditTotPar.Size = new System.Drawing.Size(130, 22);
            this.calcEditTotPar.TabIndex = 8;
            // 
            // TotRat
            // 
            this.TotRat.Location = new System.Drawing.Point(130, 228);
            this.TotRat.Name = "TotRat";
            this.TotRat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotRat.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.TotRat.Properties.Appearance.Options.UseFont = true;
            this.TotRat.Properties.Appearance.Options.UseForeColor = true;
            this.TotRat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotRat.Properties.DisplayFormat.FormatString = "n2";
            this.TotRat.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TotRat.Properties.EditFormat.FormatString = "n2";
            this.TotRat.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TotRat.Size = new System.Drawing.Size(130, 22);
            this.TotRat.TabIndex = 9;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(16, 152);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(244, 23);
            this.simpleButton1.TabIndex = 10;
            this.simpleButton1.Text = "Calcular";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // oksimpleButton
            // 
            this.oksimpleButton.Location = new System.Drawing.Point(16, 363);
            this.oksimpleButton.Name = "oksimpleButton";
            this.oksimpleButton.Size = new System.Drawing.Size(244, 23);
            this.oksimpleButton.TabIndex = 11;
            this.oksimpleButton.Text = "OK";
            this.oksimpleButton.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(282, 363);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(244, 23);
            this.simpleButton3.TabIndex = 12;
            this.simpleButton3.Text = "Cancela";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // FCalcParcela
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(545, 422);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.oksimpleButton);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.TotRat);
            this.Controls.Add(this.calcEditTotPar);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.NumeroParcelas);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.calcEditEntrada);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.radioGroup1);
            this.Name = "FCalcParcela";
            this.Text = "Calculo do valor do rateio pela parcela";
            this.Shown += new System.EventHandler(this.FCalcParcela_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditEntrada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroParcelas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditTotPar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotRat.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CalcEdit calcEditEntrada;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton oksimpleButton;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.SpinEdit NumeroParcelas;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CalcEdit calcEditTotPar;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CalcEdit TotRat;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.LabelControl labelControl5;
    }
}
