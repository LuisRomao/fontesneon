using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Boletos.Rateios;

namespace Boletos.Rateios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cGradeRateios : CompontesBasicos.ComponenteGradeNavegadorPesquisa
    {
        /// <summary>
        /// 
        /// </summary>
        public cGradeRateios()
        {
            InitializeComponent();
            
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            Popular();
        }

        private void Popular() {
            BtnIncluir_F.Enabled = false;
            if (lookupCondominio_F1.CON_sel > 0)
            {
                string comando1 = 
"SELECT RAteioDetalhes.RAD\r\n" + 
"FROM RAteioDetalhes INNER JOIN RATeios ON RAteioDetalhes.RAD_RAT = RATeios.RAT\r\n" + 
"WHERE (RAteioDetalhes.RADGerado = 0) AND (RATeios.RAT_CON = @P1);";
                string comando2 =
"SELECT top 1 BOD\r\n" +
"FROM BOletoDetalhe\r\n" +
"WHERE (BOD_BOL IS not NULL) AND (BOD_RAD = @P1);";
                DataTable RADsTestar = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comando1, lookupCondominio_F1.CON_sel);
                foreach (DataRow DR in RADsTestar.Rows)
                {
                    int RAD = (int)DR["RAD"];
                    if (VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado(comando2, RAD))
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update RAteioDetalhes set RAteioDetalhes.RADGerado = 1 where RAteioDetalhes.RAD = @P1", RAD);
                }
                //remover comando MarcaGerados
                //gradeRateiosTableAdapter.EmbarcaEmTransST();
                //gradeRateiosTableAdapter.MarcaGerados(lookupCondominio_F1.CON_sel);
                if(chSoGerados.Checked)
                    gradeRateiosTableAdapter.FillSoNGerados(dRateiosGrade.GradeRateios, lookupCondominio_F1.CON_sel);
                else
                    gradeRateiosTableAdapter.FillByCON(dRateiosGrade.GradeRateios, lookupCondominio_F1.CON_sel);
                ParametroMetodoFieldBy = lookupCondominio_F1.CON_sel;
                BtnIncluir_F.Enabled = true;
            }
            else
            {
                ParametroMetodoFieldBy = -1;
                dRateiosGrade.GradeRateios.Clear();
            }
        }

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            Popular();
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(lookupCondominio_F1.CON_sel > 0)
                base.BtnIncluir_F_ItemClick(sender, e);
        }

        dRateiosGrade.GradeRateiosRow linhamae;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnExcluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRowView DRV = (DataRowView)GridView_F.GetRow(GridView_F.FocusedRowHandle);
            if (DRV == null)
                return;
            linhamae = (dRateiosGrade.GradeRateiosRow)DRV.Row;
            int RAT = linhamae.RAT;
            int NBol = (int)gradeRateiosTableAdapter.BotetosDoRateio(RAT);
            if (NBol != 0)
                MessageBox.Show("O Rateio n�o pode ser excluido por existirem " + NBol.ToString() + " Boletos gerados");
            else
            {
                if (MessageBox.Show("Confirma a exclus�o do rateio ?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    gradeRateiosTableAdapter.DELBOD(RAT);
                    gradeRateiosTableAdapter.DELRAD(RAT);
                    gradeRateiosTableAdapter.DELRAT(RAT);

                    this.RefreshDados();
                };
            }
        }

        private void chSoGerados_CheckedChanged(object sender, EventArgs e)
        {
            Popular();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (lookupCondominio_F1.CON_sel != -1)
            {                
                SortedList<int,Rateio.StatusRegerar> StatusList = Rateio.RevisarRateios(lookupCondominio_F1.CON_sel);
                //foreach (System.Collections.DictionaryEntry DE in Rateio.RevisarRateios(lookupCondominio_F1.CON_sel))
                foreach (int RAT in StatusList.Keys)
                {
                    //int RAT = (int)DE.Key;
                    Rateio.StatusRegerar Status = StatusList[RAT];
                    foreach (dRateiosGrade.GradeRateiosRow rowRAD in dRateiosGrade.GradeRateios)
                        if (rowRAD.RAT == RAT)
                            switch (Status)
                            {
                                case Rateio.StatusRegerar.Identico:
                                    rowRAD.Obs = "OK";
                                    break;
                                case Rateio.StatusRegerar.ParcelasEquivaletes:
                                    rowRAD.Obs = "Equivalente";
                                    break;
                                case Rateio.StatusRegerar.TrocaCargo:
                                    rowRAD.Obs = "Troca Sindico";
                                    break;
                                case Rateio.StatusRegerar.Diferente:
                                    rowRAD.Obs = "* ALTERADO";
                                    break;
                                case Rateio.StatusRegerar.Manual:
                                    rowRAD.Obs = "MANUAL";
                                    break;
                                default:
                                    break;
                            }
                            
                }
            }
        }

        

        

    }
}

