using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Boletos.Rateios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FracaoManual : CompontesBasicos.ComponenteBaseBindingSource
    {
        /// <summary>
        /// 
        /// </summary>
        public FracaoManual()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        public dRateios DRateio;
        /// <summary>
        /// 
        /// </summary>
        public dRateios.ListaFracaoDataTable Tabela;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTotalSim = 0;
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //DRateio.CalculaFracaoCorrigida(dRateios.TipoSimula.ComFracao, Tabela, ValorTotalSim);
            calcEdit1_ValueChanged(null, null);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DRateio.CalculaFracaoCorrigida(dRateios.TipoSimula.ComFracao, Tabela, ValorTotalSim);
            FechaTela(DialogResult.OK);
            
        }

        bool bloc = false;

        private void calcEdit1_ValueChanged(object sender, EventArgs e)
        {
            decimal confereFracaoCorrigida = 0;
            if (bloc)
                return;
            try
            {
                bloc = true;
                dRateios.TipoSimula TipoCalc = chUsarFracao.Checked ? dRateios.TipoSimula.ComFracao : dRateios.TipoSimula.SemFracao;
                if ((sender == calcEdit1) || (sender == null))
                    ValorTotalSim = calcEdit1.Value;
                else if (sender == calcEdit2)
                {
                    if(DRateio.ReferenciaMaxima == null)
                        DRateio.CalculaFracaoCorrigida(TipoCalc, Tabela, 100);
                    ValorTotalSim = calcEdit2.Value / DRateio.ReferenciaMaxima.FracaoCorrigida;
                    confereFracaoCorrigida = DRateio.ReferenciaMaxima.FracaoCorrigida;
                }
                else if (sender == calcEdit3)
                {
                    if (DRateio.ReferenciaMinima == null)
                        DRateio.CalculaFracaoCorrigida(TipoCalc, Tabela, 100);
                    ValorTotalSim = calcEdit3.Value / DRateio.ReferenciaMinima.FracaoCorrigida;
                    confereFracaoCorrigida = DRateio.ReferenciaMinima.FracaoCorrigida;
                }
                else if (sender == calcEdit4)
                {
                    ValorTotalSim = calcEdit4.Value;
                    TipoCalc = chUsarFracao.Checked ? dRateios.TipoSimula.NominalComFracao : dRateios.TipoSimula.NominalSemFracao;
                }
                                    
                DRateio.CalculaFracaoCorrigida(TipoCalc, Tabela, ValorTotalSim);
                if (sender != calcEdit1)
                    calcEdit1.Value = DRateio.NovoTotalCorrigido;
                if (sender != calcEdit2)
                    calcEdit2.Value = DRateio.ReferenciaMaxima.ValorCorrigido;
                if (sender != calcEdit3)
                    calcEdit3.Value = DRateio.ReferenciaMinima.ValorCorrigido;
                if (sender != calcEdit4)
                    calcEdit4.Value = DRateio.Nominal;                
            }
            finally
            {
                bloc = false;
            }
            //Confere se o m�nimo o maximo foi alterado
            if (sender == calcEdit2)
            {
                if (confereFracaoCorrigida != DRateio.ReferenciaMaxima.FracaoCorrigida)
                    calcEdit1_ValueChanged(sender, e);
            }
            else if (sender == calcEdit3)
            {
                if (confereFracaoCorrigida != DRateio.ReferenciaMinima.FracaoCorrigida)
                    calcEdit1_ValueChanged(sender, e);
            }
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dRateios.AjustaDescontos((dRateios.TiposDesconto) radioGroup1.SelectedIndex,Tabela);
            calcEdit1_ValueChanged(null, null);
        }

        private void chUsarFracao_CheckedChanged(object sender, EventArgs e)
        {
            //dRateios.AjustaDescontos((dRateios.TiposDesconto)radioGroup1.SelectedIndex, Tabela);
            calcEdit1_ValueChanged(null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            if ((args.Length > 0) && (args[0] is string) && ((string)args[0] == "Simulador"))
            {
                int CON = (int)args[1];
                string CONCodigo = (string)args[2];
                string CONNome = (string)args[3];
                bool CONFracaoIdeal = (bool)args[4];
                DRateio = new Boletos.Rateios.dRateios();
                Boletos.Rateios.dRateios.ListaFracaoDataTable TabLista = Boletos.Rateios.dRateios.ListaFracaoTableAdapter.GetData(CON);
                Tabela = TabLista;
                listaFracaoDataTableBindingSource.DataSource = TabLista;
                Titulo = string.Format("Simulador - {0} {1}",CONCodigo,CONNome);
                chUsarFracao.Checked = CONFracaoIdeal;
                return new object[]{VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas)};
                
            }
            else
                return base.ChamadaGenerica(args);
        }
       
    }
}

