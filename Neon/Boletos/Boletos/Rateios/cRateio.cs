using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Framework.objetosNeon;
using System.Collections;

namespace Boletos.Rateios
{
    
    
    /// <summary>
    /// 
    /// </summary>
    public partial class cRateio : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// 
        /// </summary>
        public Rateio Rateio;
 
        /// <summary>
        /// 
        /// </summary>
        public dRateios.RATeiosRow RATrow
        {
            get
            {
                return Rateio.RATrow;
            }
            set
            {
                Rateio.RATrow = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dRateios dRateios
        {
            get 
            {
                return Rateio.dRateios;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cRateio()
        {
            InitializeComponent();
            podeincluirlinha = false;
            //Rateio = new Rateio(pk);            
            //rATeiosBindingSource.DataSource = bindingSourceAUXCondominio.DataSource = cONDOMINIOSBindingSource.DataSource = pLAnocontasBindingSource.DataSource = Rateio.dRateios;
            //tiposDeFracaoBindingSource.DataSource = listaFracaoBindingSource.DataSource = Rateio.dRateios;
            
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            rATeiosBindingSource.EndEdit();
            if (!base.Update_F())
                return false;
            return Rateio.Salvar(true,true);                        
        }

        private cGradeRateios GradeChamadoraRAT 
        {
            get 
            {
                return (cGradeRateios)GradeChamadora;
            }
        }
        

        private void cRateio_cargaInicial(object sender, EventArgs e)
        {
            Rateio = new Rateio(pk);
            rATeiosBindingSource.DataSource = bindingSourceAUXCondominio.DataSource = cONDOMINIOSBindingSource.DataSource = pLAnocontasBindingSource.DataSource = Rateio.dRateios;
            dRateiosBindingSource.DataSource = tiposDeFracaoBindingSource.DataSource = listaFracaoBindingSource.DataSource = Rateio.dRateios;
            chBlocosBindingSource.DataSource = Rateio.dRateios.ChBlocos;
            if (pk == 0)
                Rateio.CriarNovo(GradeChamadoraRAT.lookupCondominio_F1.CON_sel);
            if (!Rateio.Encontrado)
                throw new Exception("Rateio n�o encontrado");                                          
            dRateios.PLAnocontasTableAdapter.Fill1(dRateios.PLAnocontas);
            dRateios.ConTasLogicasTableAdapter.Fill(dRateios.ConTasLogicas, GradeChamadoraRAT.lookupCondominio_F1.CON_sel);
            conTasLogicasBindingSource.DataSource = Rateio.dRateios;
        }

        private void cRateio_cargaFinal(object sender, EventArgs e)
        {    
            if (pk == 0) {
                //DataRow DR = (dRateios.RATeiosRow)((DataRowView)rATeiosBindingSource.AddNew()).Row;
                RATrow = (dRateios.RATeiosRow)((DataRowView)rATeiosBindingSource.AddNew()).Row;
                RATrow.RAT_CON = GradeChamadoraRAT.lookupCondominio_F1.CON_sel;
                RATrow.RATFracao = Rateio.CONrow.CONFracaoIdeal ? 1 : 0;
                cCompetData1.Comcondominio = RATrow.RATComCondominio;                
                cCompetData1.cCompet1.CompMinima = Competencia.Ultima(RATrow.RAT_CON).Add(1);
                cCompetData1.SetCompetencia(cCompetData1.cCompet1.CompMinima);                
                //btnConfirmar_F.Enabled = false;
                barButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                barButtonItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            }
            else
            {
                cCompetData1.Comcondominio = RATrow.RATComCondominio;
                cCompetData1.cCompet1.CompMinima = Competencia.Ultima(RATrow.RAT_CON).Add(1);
                cCompetData1.cCompet1.Comp.CON = RATrow.RAT_CON;
                spinParcelas.Value = dRateios.RAteioDetalhes.Rows.Count;                
                foreach(dRateios.RAteioDetalhesRow row in dRateios.RAteioDetalhes){
                    if(!row.RADGerado){
                        cCompetData1.SetCompetencia(row.RADCompetenciaMes, row.RADCompetenciaAno);                        
                        break;
                    }

                }
            }
            if (Rateio.CONrow.CONFracaoIdeal)
                lookUpEdit1.EditValue = 1;
            else
                lookUpEdit1.EditValue = 0;
            Rateio.CarregarDadosParaGerar();
            dRateios.ListaFracaoTableAdapter.Fill(dRateios.ListaFracao, RATrow.RAT_CON);
                        
        }

        
        private bool bloc = false;

        
        /*
        private void rAT_CONLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            

            if (!bloc)
            {
                try
                {
                    //listaFracaoTableAdapter.Fill(dRateios.ListaFracao, RATrow.RAT_CON);
                    TabLista = null;
                    bloc = true;
                    if (sender is DevExpress.XtraEditors.LookUpEdit)
                    {
                        if (((DevExpress.XtraEditors.LookUpEdit)(sender)).Text.Trim() != "")
                        {
                            DevExpress.XtraEditors.LookUpEdit Sender = (DevExpress.XtraEditors.LookUpEdit)sender;
                            if (rAT_CONLookUpEdit != sender)
                                rAT_CONLookUpEdit.EditValue = rAT_CONLookUpEdit1.EditValue;
                            if (rAT_CONLookUpEdit1 != sender)
                                rAT_CONLookUpEdit1.EditValue = rAT_CONLookUpEdit.EditValue;
                            
                            
                            if ((bool)rAT_CONLookUpEdit1.GetColumnValue("CONFracaoIdeal"))
                            {
                                //RATrow.RATFracao = 1;
                                lookUpEdit1.EditValue = 1;
                            }
                            else
                            {
                                //RATrow.RATFracao = 0;
                                lookUpEdit1.EditValue = 0;
                            };
                            rATeiosBindingSource.EndEdit();
                            //fKBLOCOSCONDOMINIOSBindingSource.Filter = "BLO_CON = " + Sender.EditValue.ToString();
                            CarregaBlocos();
                            //for (int i = 0; i < checkedListBoxControl1.ItemCount; i++) 
                            //     checkedListBoxControl1.SetItemChecked(i,true);
                            //else
                            // for (int i = 0; i < checkedListBoxControl1.ItemCount; i++)
                            //   if(dRateios.RAteioBloco.Rows.Find((int)checkedListBoxControl1.Items[i].Value) != null)
                            //       checkedListBoxControl1.SetItemChecked(i, true);

                            if(dRateios.RAteioBloco.Count == 0)
                                for (int i = 0; i < checkedListBoxControl1.ItemCount; i++) 
                                   checkedListBoxControl1.SetItemChecked(i,true);
                            else
                               for (int i = 0; i < checkedListBoxControl1.ItemCount; i++)
                                   if(dRateios.RAteioBloco.Rows.Find((int)checkedListBoxControl1.Items[i].Value) != null)
                                         checkedListBoxControl1.SetItemChecked(i, true);
                             
                        }
                    }
                }
                
                finally
                {
                    bloc = false;
                }
            }
        }

*/
        private void rAT_PLALookUpEdit1_EditValueChanged(object sender, EventArgs e)
        { 
            if (!bloc)
            {
                try
                {
                    bloc = true;
                    if (sender is DevExpress.XtraEditors.LookUpEdit)
                    {
                        if (((DevExpress.XtraEditors.LookUpEdit)(sender)).Text.Trim() != "")
                        {
                            DevExpress.XtraEditors.LookUpEdit Sender = (DevExpress.XtraEditors.LookUpEdit)sender;
                            if (rAT_PLALookUpEdit != sender)
                                rAT_PLALookUpEdit.EditValue = Sender.EditValue;
                            if (rAT_PLALookUpEdit1 != sender)
                                rAT_PLALookUpEdit1.EditValue = Sender.EditValue;
                            dRateios.PLAnocontasRow planorow = dRateios.PLAnocontas.FindByPLA((String)Sender.EditValue);
                            if (planorow != null)
                            {
                                rATMensagemTextEdit.Text = planorow.PLADescricaoRes;
                                if (!planorow.IsPLAProprietarioNull())
                                {
                                    imageComboBoxEdit1.EditValue = planorow.PLAProprietario;
                                    
                                }
                            };
                        }
                    }
                }
                finally
                {
                    bloc = false;
                }
            }
        }

        private bool VerificaCTL()
        {
            if ((Rateio.dRateios.ConTasLogicas.Count > 0) && (lookUpEdit2.EditValue == DBNull.Value))
            {
                lookUpEdit2.ErrorText = "Campo Obrigat�rio";
                lookUpEdit2.Focus();
                return false;
            }
            lookUpEdit2.ErrorText = "";
            return true;
        }

        private void DividirParcelas(object sender, EventArgs e){
            rATeiosBindingSource.EndEdit();            
            if (!VerificaCTL() || !grava())
                return;            
            Rateio.DividirParcelas(cCompetData1, (int)spinParcelas.Value, (int)spinPrimeira.Value);
            Rateio.Salvar(false,true);
            rAteioDetalhesGridControl.Refresh();
        }


        /*
        private void DividirParcelas_()
        {
            
         //   if (!CamposObrigatoriosOk(rATeiosBindingSource))
            
            //rATeiosBindingSource.EndEdit();
            int ParcelasGeradas;
            Int16 mes;
            Int16 ano;
            if (pk == 0)
            {
                btnConfirmar_F.Enabled = true;
                dRateios.BOletoDetalheRAD.Clear();
                dRateios.RAteioDetalhes.Clear();
                ParcelasGeradas = 0;
            }
            else
            {
                btnCancelar_F.Enabled = false;
                dRateios.RAteioDetalhesTableAdapter.ApagaBODnaoGerado(pk);
                dRateios.RAteioDetalhesTableAdapter.ApagaRADnaoGerado(pk);
                dRateios.BOletoDetalheRAD.Clear();
                ParcelasGeradas = dRateios.RAteioDetalhesTableAdapter.FillEspecifico(dRateios.RAteioDetalhes, pk);
                dRateios.BOletoDetalheRADTableAdapter.FillGeral(dRateios.BOletoDetalheRAD, pk);
            }
            
            rAteioDetalhesGridControl.Refresh();            
            Competencia Compet = cCompetData1.GetCompetencia();
            int TotalDeParcelas = (int)spinParcelas.Value;
            int NumeroInicial = (int)spinPrimeira.Value ;
            
            decimal TotalParaDividir = RATrow.RATValor;
            foreach (dRateios.RAteioDetalhesRow linhaGeradaAntes in dRateios.RAteioDetalhes)
                TotalParaDividir -= linhaGeradaAntes.RADValor;
            decimal Saldo = TotalParaDividir;
            for (int i = ParcelasGeradas; i < TotalDeParcelas; i++)
            {                
                dRateios.RAteioDetalhesRow novaLinha = dRateios.RAteioDetalhes.NewRAteioDetalhesRow();
                novaLinha.RAD_RAT = RATrow.RAT;
                novaLinha.RADComCondominio = RATrow.RATComCondominio = cCompetData1.Comcondominio;                
                if (cCompetData1.Comcondominio)
                {
                    novaLinha.RADData = Compet.CalculaVencimento();
                    novaLinha.RADCompetenciaAno = (Int16)Compet.Ano;
                    novaLinha.RADCompetenciaMes = (Int16)Compet.Mes;
                    Compet.Add(1);
                }
                else
                {
                    novaLinha.RADData = cCompetData1.dateEdit1.DateTime.AddMonths(i-ParcelasGeradas);
                    novaLinha.RADCompetenciaAno = (Int16)novaLinha.RADData.Year;
                    novaLinha.RADCompetenciaMes = (Int16)novaLinha.RADData.Month;
                }
                
                
                novaLinha.RADGerado = false;
                novaLinha.RADMensagem = RATrow.RATMensagem;
                if ((TotalDeParcelas > 1) || (NumeroInicial > 1))
                    novaLinha.RADMensagem += " (" + (i + NumeroInicial).ToString() + "/" + (TotalDeParcelas + NumeroInicial -1).ToString() + ")";
                if (i == (TotalDeParcelas-1))
                    novaLinha.RADValor = Saldo;
                else{
                    novaLinha.RADValor = decimal.Round(TotalParaDividir / (TotalDeParcelas - ParcelasGeradas), 2);
                    Saldo -= novaLinha.RADValor;
                };
                novaLinha.EndEdit();
                dRateios.RAteioDetalhes.AddRAteioDetalhesRow(novaLinha);
            }
          
        }

        //public dRateios.RATeiosRow RATrow;

        //private dRateios.ListaFracaoDataTable TabLista;

        /*
        private void RemoveBlocos(){
            
            //TabLista = dRateios.ListaFracaoTableAdapter.GetData(RATrow.RAT_CON);
            foreach (dRateios.ListaFracaoRow linhaLista in Rateio.TabLista)
            {
                bool marcado = false;
                for (int i = 0; i < checkedListBoxControl2.CheckedItems.Count; i++)
                {
                    LinhaCh LCH = (LinhaCh)checkedListBoxControl2.CheckedItems[i];
                    if (LCH.BLO == linhaLista.BLO)
                        marcado = true;
                };
                if(!marcado)
                    linhaLista.Delete();


            };
            Rateio.TabLista.AcceptChanges();
        }
        

        private void AjustaTotaldoRateio() {
            decimal TotalDoRateio = 0;
            foreach (dRateios.RAteioDetalhesRow LinhaDetalhe in dRateios.RAteioDetalhes)
            {
                decimal TotalRAD = 0;
                foreach (dRateios.BOletoDetalheRADRow LinhaBOD in LinhaDetalhe.GetBOletoDetalheRADRows())
                    TotalRAD += LinhaBOD.BODValor;
                LinhaDetalhe.RADValor = TotalRAD;
                TotalDoRateio += LinhaDetalhe.RADValor;
            };
            RATrow.RATValor = TotalDoRateio;
        }
        */

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (VerificaCTL() && grava())
            {
                Rateio.DividirParcelas(cCompetData1, (int)spinParcelas.Value, (int)spinPrimeira.Value);
                Rateio.GerarTodos();
            }
        }

        /*
        //FracaoManual FM;

        private void AlteraManual(bool forcar){
            bool abrir = (forcar || (FM == null));
            if (FM == null)
            {
                FM = new FracaoManual();
                FM.DRateio = dRateios;
                FM.Tabela = Rateio.TabLista;
                FM.listaFracaoDataTableBindingSource.DataSource = Rateio.TabLista;
                
            };
            if (abrir)
            {
                if ((RATrow["RATValor"] != null) && (RATrow["RATValor"] != DBNull.Value))
                    FM.ValorTotalSim = RATrow.RATValor;
                else
                    FM.ValorTotalSim = 0;
                ShowModulo(FM);
            };
        }

        
        public void preparagerar(bool forcar)
        {
            //if (TabLista == null)
            //{
            //    TabLista = dRateios.ListaFracaoTableAdapter.GetData(RATrow.RAT_CON);                
            //};
            //RemoveBlocos();
            //CorrigeSindicoSubsindico();
            Rateio.AplicaDescontosCorpoDiretivo();
            switch (RATrow.RATFracao){
                case -1:
                    AlteraManual(forcar);                    
                    break;
                case 0:
                    dRateios.CalculaFracaoCorrigida(false, Rateio.TabLista, 0);
                    break;
                case 1:
                    dRateios.CalculaFracaoCorrigida(true, Rateio.TabLista, 0);
                    break;
            };
        }
        

        public void GeraUM(dRateios.RAteioDetalhesRow LinhaDetalhe) 
        {            
            if (LinhaDetalhe.RADGerado)
                return;
            Rateio.preparagerar(false);
            //preparagerar(false);
            decimal Total = 0;
            System.Collections.ArrayList Matar = new System.Collections.ArrayList();
            foreach (dRateios.BOletoDetalheRADRow velhalinha in dRateios.BOletoDetalheRAD.Rows)
                if (velhalinha.BOD_RAD == LinhaDetalhe.RAD)
                    Matar.Add(velhalinha);
            foreach (dRateios.BOletoDetalheRADRow velhalinha in Matar)
                velhalinha.Delete();
            decimal ValorRatear = LinhaDetalhe.RADValor + Rateio.DescontoEmValor;
            foreach (dRateios.ListaFracaoRow linhaLista in Rateio.TabLista)
            {
                decimal ValorLiquido = 0;
                dRateios.BOletoDetalheRADRow novalinha = dRateios.BOletoDetalheRAD.NewBOletoDetalheRADRow();
                novalinha.BOD_RAD = LinhaDetalhe.RAD;
                novalinha.BOD_APT = linhaLista.APT;
                novalinha.BOD_PLA = RATrow.RAT_PLA;                
                novalinha.BODFracaoEfetiva = linhaLista.FracaoCorrigida;
                if (ModeloAntigo)
                    novalinha.BODValor = decimal.Round(LinhaDetalhe.RADValor * novalinha.BODFracaoEfetiva, 2);
                else
                {
                    novalinha.BODValor = decimal.Round(ValorRatear * linhaLista.FracaoReferenciaSemDesconto, 2);
                    ValorLiquido = decimal.Round(ValorRatear * linhaLista.FracaoCorrigida, 2);
                    
                }
                novalinha.BODComCondominio = LinhaDetalhe.RADComCondominio;
                novalinha.BODData = LinhaDetalhe.RADData;                
                novalinha.BODCompetenciaAno = LinhaDetalhe.RADCompetenciaAno;
                novalinha.BODCompetenciaMes = LinhaDetalhe.RADCompetenciaMes;
                Total += novalinha.BODValor;
                novalinha.BODMensagem = LinhaDetalhe.RADMensagem;
                novalinha.BODProprietario = RATrow.RATProprietario;                
                dRateios.BOletoDetalheRAD.AddBOletoDetalheRADRow(novalinha);
                if(!ModeloAntigo)
                    if (!linhaLista.IsCDRDescontoRateiosValorNull())
                        if ((linhaLista.CDRDescontoRateiosValor) && (dRateios.Descontos.FindByCDR(linhaLista.CDR).Aplicar))
                            ValorLiquido -= linhaLista.CDRDescontoRateios;


                if (ValorLiquido != novalinha.BODValor)
                {
                    dRateios.BOletoDetalheRADRow novalinhadesc = dRateios.BOletoDetalheRAD.NewBOletoDetalheRADRow();
                    novalinhadesc.BOD_RAD = novalinha.BOD_RAD;
                    novalinhadesc.BOD_APT = novalinha.BOD_APT;
                    novalinhadesc.BOD_PLA = novalinha.BOD_PLA;
                    novalinhadesc.BODFracaoEfetiva = 0;
                    novalinhadesc.BODValor = ValorLiquido - novalinha.BODValor;
                    novalinhadesc.BODComCondominio = novalinha.BODComCondominio;
                    novalinhadesc.BODData = novalinha.BODData;
                    novalinhadesc.BODCompetenciaAno = novalinha.BODCompetenciaAno;
                    novalinhadesc.BODCompetenciaMes = novalinha.BODCompetenciaMes;
                    Total += novalinhadesc.BODValor;
                    if (linhaLista.IsCGONomeNull())
                        novalinhadesc.BODMensagem = string.Format("Desconto - {0}", LinhaDetalhe.RADMensagem);
                    else
                        novalinhadesc.BODMensagem = string.Format("Desconto ({0}) - {1}", linhaLista.CGONome, LinhaDetalhe.RADMensagem);
                    if(novalinhadesc.BODMensagem.Length > 50)
                        novalinhadesc.BODMensagem = string.Format("Desconto - {0}", LinhaDetalhe.RADMensagem);
                    novalinhadesc.BODProprietario = novalinha.BODProprietario;
                    if (!linhaLista.IsCDRINSSNull())
                        novalinhadesc.BODRetencao = linhaLista.CDRINSS;
                    else
                        novalinhadesc.BODRetencao = (int)Framework.Enumeracoes.INSSSindico.NaoRecolhe;
                    dRateios.BOletoDetalheRAD.AddBOletoDetalheRADRow(novalinhadesc);
                };
                    
            };
            LinhaDetalhe.RADValor = Total;
            LinhaDetalhe.EndEdit();            
            gridView1.RefreshData();
        }
        */

        //private bool ModeloAntigo = false;

        /*
        private decimal DescontoEmValor = 0;

        public void CorrigeSindicoSubsindico()
        {
            DescontoEmValor = 0;
            foreach (dRateios.ListaFracaoRow linhaLista in Rateio.TabLista)
            {
                if (linhaLista.IsCDRDescontoRateiosNull())
                    continue;

                if (linhaLista.CDRDescontoRateios == -1)
                    ModeloAntigo = true;

                if (!ModeloAntigo)
                {
                    if ((linhaLista.CDRDescontoRateios > 0) && dRateios.Descontos.FindByCDR(linhaLista.CDR).Aplicar)
                    {
                        if (!linhaLista.CDRDescontoRateiosValor)
                            linhaLista.FatorFracao = 1 - (linhaLista.CDRDescontoRateios / 100);
                        else
                            DescontoEmValor += linhaLista.CDRDescontoRateios;
                    }
                }
                else

                    if (!linhaLista.IsCDR_CGONull())
                    {
                        if (linhaLista.CDR_CGO == 1) //Sindico
                        {
                            //if (!rATSindicoCheckEdit.Checked)
                            //    linhaLista.FatorFracao = 0;
                            //else
                            //    linhaLista.FatorFracao = decimal.Parse(CONSindico.Text) / 100;
                        };
                        if (linhaLista.CDR_CGO == 2) //Sub-sidico
                        {
                            //if (!rATSubSindicoCheckEdit.Checked)
                            //    linhaLista.FatorFracao = 0;
                            //else
                            //    linhaLista.FatorFracao = decimal.Parse(CONSubSindico.Text) / 100;
                        }
                    };
            };
        }
        */

        private Rateios.FCalcParcela Calculadora = null;

        private DialogResult ChamaCalc(bool Geral)
        {
            if (Calculadora == null)
            {
                Calculadora = new FCalcParcela();
            };
            if (Geral)            
                Calculadora.NumeroParcelas.Value = spinParcelas.Value;
            else
                Calculadora.NumeroParcelas.Value = 1;
            Calculadora.NumeroParcelas.Visible = Geral;
            Calculadora.labelControl5.Visible = Geral;
            Calculadora.TotRat.Visible = Geral;
            Calculadora.Chamador = this;
            return Calculadora.ShowDialog();
            
        }

        private bool grava() {
            rATeiosBindingSource.EndEdit();
            if (!CamposObrigatoriosOk(rATeiosBindingSource))
                return false;
            else {
               
                return true;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (!VerificaCTL())
                return;
            if (RATrow["RATValor"] == DBNull.Value)
                RATrow.RATValor = 0;
            if (grava())
            {
                if (ChamaCalc(true) == DialogResult.OK)
                {
                    rATValorCalcEdit.Value = Calculadora.TotRat.Value;
                    spinParcelas.Value = Calculadora.NumeroParcelas.Value;
                    rATeiosBindingSource.EndEdit();
                    Rateio.DividirParcelas(cCompetData1, (int)spinParcelas.Value, (int)spinPrimeira.Value);
                }
            }
            
            
            
        }

       

        private void repositoryItemCalcEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
            {
                if (ChamaCalc(false) == DialogResult.OK)
                {
                    DataRowView drv = (DataRowView)fKRAteioDetalhesRAteioDetalhesBindingSource.Current;
                    dRateios.RAteioDetalhesRow linha = (dRateios.RAteioDetalhesRow)drv.Row;
                    linha.RADValor = Calculadora.TotRat.Value;
                    Rateio.GeraUM(linha,false);
                    Rateio.AjustaTotaldoRateio();
                }
            };
        }        

        

        



        private void AjustaData(object sender, EventArgs e)
        {            
            gridView1.PostEditor();                        
            gridView1.RefreshData();
        }

        
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (!VerificaCTL())
                return;
            Rateio.preparagerar(true);
        }
        
          
        private void rATMensagemTextEdit_Enter(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
        }

        private void rATMensagemTextEdit_Leave(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = true;
        }

        private void rATSindicoCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            Rateio.ResetTabLista();
        }

        private void rAteioDetalhesGridControl1_Click(object sender, EventArgs e)
        {

        }        

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DataRowView DRV;
            DRV = (DataRowView)gridView1.GetRow(e.FocusedRowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return;
            Boletos.Rateios.dRateios.RAteioDetalhesRow Row = (Boletos.Rateios.dRateios.RAteioDetalhesRow)DRV.Row;
            
            gridView1.OptionsBehavior.Editable = !(Row.RADGerado);
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DataRowView DRV;
            DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return;
            Boletos.Rateios.dRateios.RAteioDetalhesRow Row = (Boletos.Rateios.dRateios.RAteioDetalhesRow)DRV.Row;
            if (Row.RADGerado)
            {
                if (gridView1.FocusedRowHandle == e.RowHandle)
                {
                    e.Appearance.BackColor = Color.FromArgb(100, 100, 100);
                    e.Appearance.ForeColor = Color.FromArgb(255, 200, 200);
                }
                else
                {
                    e.Appearance.BackColor = Color.FromArgb(220, 220, 220);
                    e.Appearance.ForeColor = Color.Red;
                }

            }
        }

        private void gridView3_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dRateios.DescontosRow row = (dRateios.DescontosRow)gridView3.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            dRateios.RAteioAplicardescontoRow rowRAA = row.RAteioAplicardescontoRow;
            if (rowRAA == null)
            {
                if (RATrow.RAT > 0)
                    dRateios.RAteioAplicardesconto.AddRAteioAplicardescontoRow(RATrow.RAT, row.CDR, row.Aplicar);
            }
            else
                rowRAA.RAAAplicar = row.Aplicar;
            dRateios.RAteioAplicardescontoTableAdapter.Update(dRateios.RAteioAplicardesconto);
            Rateio.ResetTabLista();
        }

        private void checkedListBoxControl2_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            Rateio.ResetTabLista();
        }

        private void gridView4_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            Rateio.ResetTabLista();
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            Rateio.ResetTabLista();
        }

        private void gridView2_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            dRateios.BOletoDetalheRADRow Row = (dRateios.BOletoDetalheRADRow)((DataRowView)e.Row).Row;
            if (!Row.IsBOD_BOLNull())
            {
                e.Valid = false;
                MessageBox.Show("Boleto j� emitido");
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            switch (Rateio.SimularRegerar())
            {
                case Rateio.StatusRegerar.Identico:
                    MessageBox.Show("Sem altera��es");
                    break;
                case Rateio.StatusRegerar.ParcelasEquivaletes:
                    MessageBox.Show("Equivalente: Todas as unidades mant�m o valor");
                    break;
                case Rateio.StatusRegerar.TrocaCargo:
                    MessageBox.Show("Altera��o no corpo diretivo sem altera��o nas demais unidades");
                    break;
                case Rateio.StatusRegerar.Diferente:
                    MessageBox.Show("A T E N � � O\r\n\r\n Altera��o no valor das unidades");
                    break;
                case Rateio.StatusRegerar.Manual:
                    MessageBox.Show("Rateio manual");
                    break;
                default:
                    break;
            }            
        }

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName.ToUpper() == "SIMULADO")
            {
                dRateios.BOletoDetalheRADRow row = (dRateios.BOletoDetalheRADRow)gridView2.GetDataRow(e.RowHandle);
                if (row != null)
                    if (!row.IsSimuladoNull())
                        if (Math.Abs(row.Simulado - row.BODValor) > 0.02M)
                            e.Appearance.BackColor = Color.Red;
                        else
                            if (row.Simulado != row.BODValor)
                                e.Appearance.BackColor = Color.Yellow;
            }
        }

        

        private void BtImplantacao_Click(object sender, EventArgs e)
        {
            cCompetData1.cCompet1.CompMinima = new Competencia(1, 2000);
            cCompetData1.dateEdit1.Properties.MinValue = new DateTime(2000, 1, 1);
        }

        private void cBotaoImpNeon1_clicado(object sender, dllBotao.BotaoArgs args)
        {
            SortedList Boletos = new SortedList();
            for(int h = 0; h < gridView2.RowCount;h++)
            {
                dRateios.BOletoDetalheRADRow rowBOD = (dRateios.BOletoDetalheRADRow)gridView2.GetDataRow(h);
                if (!rowBOD.IsBOD_BOLNull())
                    if (!Boletos.ContainsKey(rowBOD.BOD_BOL))
                        Boletos.Add(rowBOD.BOD_BOL, new Boleto.Boleto(rowBOD.BOD_BOL));
            }
            
            /*
            foreach (dPrevBolCampos.BOletoDetalheRow rowBOD in linhamae.GetBOletoDetalheRows())
                if (!rowBOD.IsBOD_BOLNull())
                    if (!Boletos.ContainsKey(rowBOD.BOD_BOL))
                        Boletos.Add(rowBOD.BOD_BOL, new Boleto.Boleto(rowBOD.BOD_BOL));
            */
            switch (args.Botao)
            {
                case dllBotao.Botao.botao:
                case dllBotao.Botao.imprimir:
                    break;
                case dllBotao.Botao.tela:
                    break;
                case dllBotao.Botao.email:
                    MessageBox.Show(Email(Boletos));
                    break;

            }
        }

        
        private string Email(SortedList Boletos)
        {
            string ConteudoEmail =
            "\r\n" +
            "Prezado Cliente," + "\r\n" +
            "\r\n" +
            "Para sua maior seguran�a e comodidade, a Neon Online, passa a enviar a partir de agora o Boleto Banc�rio do %C por email." + "\r\n" +
            "" + "\r\n" +
            "O arquivo (pdf) do Boleto Banc�rio est� anexo. Voc� poder� imprimi-lo e efetuar o pagamento em qualquer banco integrado ao sistema de compensa��o ou via internet." + "\r\n" +
            "" + "\r\n" +
            "Para pagamento via internet disponibilizamos tamb�m o n�mero do C�digo de barras:" + "\r\n" +
            "" + "\r\n" +
            " %B " + "\r\n" +
            "" + "\r\n" +
            "que identificar� este boleto no site de seu banco." + "\r\n" +
            "" + "\r\n" +
            "Neon Online, facilitando sua vida." + "\r\n" +
            "" + "\r\n" +
            "Caso v�ce n�o queira receber os boletos por e-mail basta desmarcar esta op��o no seu cadastro no Neon Online." + "\r\n" +
            "" + "\r\n" +
            "Att," + "\r\n" +
            "" + "\r\n" +
            "Neon Im�veis" + "\r\n" +
            "\r\n";
            string Relatorio = "";
            dllImpresso.ImpBoletoBal ImpSB = new dllImpresso.ImpBoletoBal();
            
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.AtivaGauge(Boletos.Count);
                Esp.Espere("E.mail");
                ImpSB.ComRemetente = false;
                foreach (Boleto.Boleto BoletosE in Boletos.GetValueList())
                {
                    if (Esp.Abortar)
                        break;                    

                    if (BoletosE.QuerEmail)
                    {
                        ImpSB.Apontamento(-1, -1);
                        BoletosE.CarregaLinha(ImpSB, true, false);
                        ImpSB.bindingSourcePrincipal.Filter = "BOL = " + BoletosE.rowPrincipal.BOL.ToString();
                        ImpSB.CreateDocument();
                        string Corpo = ConteudoEmail.Replace("%C", BoletosE.Apartamento.Condominio.Nome).Replace("%B", BoletosE.LinhaDigitavel);
                        try
                        {
                            VirEmailNeon.EmailDiretoNeon.EmalSTSemRetorno.Enviar(BoletosE.EmailResponsavel, BoletosE.rowPrincipal.BOL.ToString(), ImpSB, Corpo, "Boleto de condom�nio");
                            Relatorio += "ok: " + BoletosE.Apartamento.BLOCodigo + " " + BoletosE.Apartamento.APTNumero + " " + BoletosE.EmailResponsavel + "\r\n";
                        }
                        catch
                        {
                            Relatorio += "E.mail n�o enviado: " + BoletosE.Apartamento.BLOCodigo + " " + BoletosE.Apartamento.APTNumero + " " + BoletosE.EmailResponsavel + "\r\n";
                            Relatorio += "\r\n" + VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro + "\r\n";
                        };
                        Esp.Gauge();
                        Application.DoEvents();
                    };
                };
            };

            return Relatorio;

        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (Rateio.RATrow.IsRAT_CTLNull())
                return;

            rATeiosBindingSource.EndEdit();
            if (!grava())
                return;

            if (Rateio.Salvar(false, false))
            {

                string comando1 = "UPDATE RAteioDetalhes SET RAD_CTL = @P1 WHERE (RAD_RAT = @P2)";
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando1, Rateio.RATrow.RAT_CTL, Rateio.RATrow.RAT);
                string comando2 =
    "UPDATE    BOletoDetalhe\r\n" +
    "SET              BOD_CTL = @P1\r\n" +
    "FROM         BOletoDetalhe INNER JOIN\r\n" +
    "                      RAteioDetalhes ON BOletoDetalhe.BOD_RAD = RAteioDetalhes.RAD\r\n" +
    "WHERE     (RAteioDetalhes.RAD_RAT = @P2);";
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando2, Rateio.RATrow.RAT_CTL, Rateio.RATrow.RAT);
                FechaTela(DialogResult.OK);
            }
        }

        private void cRateio_Load(object sender, EventArgs e)
        {
            cCompetData1.dateEdit1.Properties.MaxValue = DateTime.Today.AddMonths(6);
        }
        




    }

    /*
    public class LinhaCh:Object {
        public string Ident;
        public int BLO;
        public override string ToString()
        {
            return Ident;
        }
        public LinhaCh(string _Ident,int _BLO) {
            Ident = _Ident;
            BLO = _BLO;
        }
    }
    */
}

