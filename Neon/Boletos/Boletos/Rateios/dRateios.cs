﻿namespace Boletos.Rateios
{


    partial class dRateios
    {
        partial class ListaFracaoDataTable
        {
        }

        private dRateiosTableAdapters.RAteioAplicardescontoTableAdapter rAteioAplicardescontoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RAteioAplicardesconto
        /// </summary>
        public dRateiosTableAdapters.RAteioAplicardescontoTableAdapter RAteioAplicardescontoTableAdapter
        {
            get
            {
                if (rAteioAplicardescontoTableAdapter == null)
                {
                    rAteioAplicardescontoTableAdapter = new dRateiosTableAdapters.RAteioAplicardescontoTableAdapter();
                    rAteioAplicardescontoTableAdapter.TrocarStringDeConexao();
                };
                return rAteioAplicardescontoTableAdapter;
            }
        }

        private static dRateiosTableAdapters.ListaFracaoTableAdapter listaFracaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ListaFracao
        /// </summary>
        public static dRateiosTableAdapters.ListaFracaoTableAdapter ListaFracaoTableAdapter
        {
            get
            {
                if (listaFracaoTableAdapter == null)
                {
                    listaFracaoTableAdapter = new dRateiosTableAdapters.ListaFracaoTableAdapter();
                    listaFracaoTableAdapter.TrocarStringDeConexao();
                };
                return listaFracaoTableAdapter;
            }
        }

        private static dRateiosTableAdapters.DescontosTableAdapter descontosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Descontos
        /// </summary>
        public static dRateiosTableAdapters.DescontosTableAdapter DescontosTableAdapter
        {
            get
            {
                if (descontosTableAdapter == null)
                {
                    descontosTableAdapter = new dRateiosTableAdapters.DescontosTableAdapter();
                    descontosTableAdapter.TrocarStringDeConexao();
                };
                return descontosTableAdapter;
            }
        }

        private static dRateiosTableAdapters.RATeiosTableAdapter rATeiosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RATeios
        /// </summary>
        public static dRateiosTableAdapters.RATeiosTableAdapter RATeiosTableAdapter
        {
            get
            {
                if (rATeiosTableAdapter == null)
                {
                    rATeiosTableAdapter = new dRateiosTableAdapters.RATeiosTableAdapter();
                    rATeiosTableAdapter.TrocarStringDeConexao();
                };
                return rATeiosTableAdapter;
            }
        }

        private static dRateiosTableAdapters.RAteioDetalhesTableAdapter rAteioDetalhesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RAteioDetalhes
        /// </summary>
        public static dRateiosTableAdapters.RAteioDetalhesTableAdapter RAteioDetalhesTableAdapter
        {
            get
            {
                if (rAteioDetalhesTableAdapter == null)
                {
                    rAteioDetalhesTableAdapter = new dRateiosTableAdapters.RAteioDetalhesTableAdapter();
                    rAteioDetalhesTableAdapter.TrocarStringDeConexao();
                };
                return rAteioDetalhesTableAdapter;
            }
        }

        private static dRateiosTableAdapters.BOletoDetalheRADTableAdapter bOletoDetalheRADTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalheRAD
        /// </summary>
        public static dRateiosTableAdapters.BOletoDetalheRADTableAdapter BOletoDetalheRADTableAdapter
        {
            get
            {
                if (bOletoDetalheRADTableAdapter == null)
                {
                    bOletoDetalheRADTableAdapter = new dRateiosTableAdapters.BOletoDetalheRADTableAdapter();
                    bOletoDetalheRADTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheRADTableAdapter;
            }
        }

        private static dRateiosTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public static dRateiosTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dRateiosTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private static dRateiosTableAdapters.PLAnocontasTableAdapter pLAnocontasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PLAnocontas
        /// </summary>
        public static dRateiosTableAdapters.PLAnocontasTableAdapter PLAnocontasTableAdapter
        {
            get
            {
                if (pLAnocontasTableAdapter == null)
                {
                    pLAnocontasTableAdapter = new dRateiosTableAdapters.PLAnocontasTableAdapter();
                    pLAnocontasTableAdapter.TrocarStringDeConexao();
                };
                return pLAnocontasTableAdapter;
            }
        }

        private static dRateiosTableAdapters.RAteioBlocoTableAdapter rAteioBlocoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RAteioBloco
        /// </summary>
        public static dRateiosTableAdapters.RAteioBlocoTableAdapter RAteioBlocoTableAdapter
        {
            get
            {
                if (rAteioBlocoTableAdapter == null)
                {
                    rAteioBlocoTableAdapter = new dRateiosTableAdapters.RAteioBlocoTableAdapter();
                    rAteioBlocoTableAdapter.TrocarStringDeConexao();
                };
                return rAteioBlocoTableAdapter;
            }
        }

        private dRateiosTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dRateiosTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dRateiosTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao();
                };
                return conTasLogicasTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dRateios.ListaFracaoRow ReferenciaMaxima;// A princicio a referencia máxima é igual à LinhaMaior mas poderemos ter uma diferênça em função da não cosideração das linhas com fator (como ocorre na referenciaminima)
        private dRateios.ListaFracaoRow LinhaMaior;  //Esta é a linha de maior peso no rateio devendo recer o ajuste fino
        /// <summary>
        /// 
        /// </summary>
        public dRateios.ListaFracaoRow ReferenciaMinima;
        /// <summary>
        /// 
        /// </summary>
        public decimal NovoTotalCorrigido;
        /// <summary>
        /// 
        /// </summary>
        public decimal Nominal;

        /// <summary>
        /// 
        /// </summary>
        public enum TiposDesconto
        {
            /// <summary>
            /// 
            /// </summary>
            SemDesconto = 0,
            /// <summary>
            /// 
            /// </summary>
            Condominio = 1,
            /// <summary>
            /// 
            /// </summary>
            Rateio = 2
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TD"></param>
        /// <param name="Tabela"></param>
        public static void AjustaDescontos(TiposDesconto TD, dRateios.ListaFracaoDataTable Tabela)
        {
            //TotalDescontoEmReais = 0;
            foreach (dRateios.ListaFracaoRow ListaFracaoRow in Tabela)
            {
                ListaFracaoRow.FatorFracao = 1;
                ListaFracaoRow.DescontoEmReais = 0;
                switch (TD)
                {
                    case TiposDesconto.SemDesconto:
                        break;
                    case TiposDesconto.Condominio:
                        if (!ListaFracaoRow.IsCDRDescontoCondominioValorNull() && !ListaFracaoRow.IsCDRDescontoCondominioNull())
                            if (ListaFracaoRow.CDRDescontoCondominioValor)
                                ListaFracaoRow.DescontoEmReais = ListaFracaoRow.CDRDescontoCondominio;
                            else
                                ListaFracaoRow.FatorFracao = (100 - ListaFracaoRow.CDRDescontoCondominio) / 100;
                        break;
                    case TiposDesconto.Rateio:
                        if (!ListaFracaoRow.IsCDRDescontoRateiosValorNull() && !ListaFracaoRow.IsCDRDescontoRateiosNull())
                            if (ListaFracaoRow.CDRDescontoRateiosValor)
                                ListaFracaoRow.DescontoEmReais = ListaFracaoRow.CDRDescontoRateios;
                            else
                                ListaFracaoRow.FatorFracao = (100 - ListaFracaoRow.CDRDescontoRateios) / 100;
                        break;
                };
                //TotalDescontoEmReais += ListaFracaoRow.DescontoEmReais;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TipoSimula
        {
            /// <summary>
            /// 
            /// </summary>
            SemFracao,
            /// <summary>
            /// 
            /// </summary>
            ComFracao,
            /// <summary>
            /// 
            /// </summary>
            NominalSemFracao,
            /// <summary>
            /// 
            /// </summary>
            NominalComFracao
        }

        /// <summary>
        /// Calcula
        /// </summary>
        /// <param name="usarFracao"></param>
        /// <param name="Tabela"></param>
        /// <param name="ValortotalSim"></param>
        public void CalculaFracaoCorrigida(TipoSimula usarFracao, dRateios.ListaFracaoDataTable Tabela, decimal ValortotalSim)
        {
            //Cada apartamento tem uma APTFracaoIdeal
            //para os apartamentos com desconto temos o FatorFracao que reduz a APTFracaoIdeal ()
            //FracaoCorrigida é a APTFracaoIdeal * FatorFracao normalizada para 100%
            //FracaoReferenciaSemDesconto é a fracao * fator de normalizacao ou seja o valor a pagar sem desconto (notar que a somatorio de FracaoReferenciaSemDesconto NAO vai dar 100% porque 100% é o valor com desconto

            decimal Total = 0;
            decimal TotalNominal = 0;
            decimal TotalDescontoReais = 0;
            decimal Maior = 0;
            decimal Menor = 100000000;
            if ((usarFracao == TipoSimula.SemFracao) || (usarFracao == TipoSimula.NominalSemFracao))
                foreach (dRateios.ListaFracaoRow ListaFracaoRow in Tabela)
                {
                    if (ListaFracaoRow.IsAPTFracaoIdealReservaNull())
                        ListaFracaoRow.APTFracaoIdealReserva = ListaFracaoRow.APTFracaoIdeal;
                    ListaFracaoRow.APTFracaoIdeal = 1;
                }
            else
                foreach (dRateios.ListaFracaoRow ListaFracaoRow in Tabela)
                    if (!ListaFracaoRow.IsAPTFracaoIdealReservaNull())
                        ListaFracaoRow.APTFracaoIdeal = ListaFracaoRow.APTFracaoIdealReserva;


            //Aplica desconto
            foreach (dRateios.ListaFracaoRow ListaFracaoRow in Tabela)
            {
                decimal Corrigido = ListaFracaoRow.APTFracaoIdeal * ListaFracaoRow.FatorFracao;
                Total += Corrigido;
                TotalNominal += ListaFracaoRow.APTFracaoIdeal;
                if (Corrigido > Maior)
                {
                    Maior = Corrigido;
                    LinhaMaior = ListaFracaoRow;
                    ReferenciaMaxima = ListaFracaoRow;
                };
                if ((Corrigido < Menor) && (ListaFracaoRow.FatorFracao == 1) && (Corrigido > 0))
                {
                    Menor = Corrigido;
                    ReferenciaMinima = ListaFracaoRow;
                };
                TotalDescontoReais += ListaFracaoRow.DescontoEmReais;
            };
            if ((usarFracao == TipoSimula.SemFracao) || (usarFracao == TipoSimula.ComFracao))
                ValortotalSim += TotalDescontoReais;


            //Quando as frações não estiverem cadastradas
            if (Total == 0)
            {
                TotalNominal = 0;
                foreach (dRateios.ListaFracaoRow ListaFracaoRow in Tabela)
                {
                    ListaFracaoRow.APTFracaoIdeal = 1;
                    decimal Corrigido = ListaFracaoRow.APTFracaoIdeal * ListaFracaoRow.FatorFracao;
                    TotalNominal += ListaFracaoRow.APTFracaoIdeal;
                    Total += Corrigido;
                    if (Corrigido > Maior)
                    {
                        Maior = Corrigido;
                        LinhaMaior = ListaFracaoRow;
                        ReferenciaMaxima = ListaFracaoRow;
                    };
                    if ((Corrigido < Menor) && (ListaFracaoRow.FatorFracao == 1) && (Corrigido > 0))
                    {
                        Menor = Corrigido;
                        ReferenciaMinima = ListaFracaoRow;
                    };
                };
            }


            //Normalizar para 100%
            //ATENCAO o Total passa a ser o fator de correçao para levar a soma das Fracoes para 100%
            foreach (dRateios.ListaFracaoRow ListaFracaoRow in Tabela)
            {
                decimal Corrigido = ListaFracaoRow.APTFracaoIdeal * ListaFracaoRow.FatorFracao;
                ListaFracaoRow.FracaoCorrigida = Corrigido / Total;
                ListaFracaoRow.FracaoNominalNormalizada = Corrigido / TotalNominal;
                if ((usarFracao == TipoSimula.NominalComFracao) || (usarFracao == TipoSimula.NominalSemFracao))
                    ListaFracaoRow.FracaoReferenciaSemDesconto = ListaFracaoRow.APTFracaoIdeal / TotalNominal;
                else
                    ListaFracaoRow.FracaoReferenciaSemDesconto = ListaFracaoRow.APTFracaoIdeal / Total;


                NovoTotalCorrigido += ListaFracaoRow.FracaoCorrigida;
            };


            //Rateio efetivo
            NovoTotalCorrigido = 0;
            Nominal = 0;
            foreach (dRateios.ListaFracaoRow ListaFracaoRow in Tabela)
            {
                if ((usarFracao == TipoSimula.NominalComFracao) || (usarFracao == TipoSimula.NominalSemFracao))
                    ListaFracaoRow.ValorCorrigido = System.Math.Round(ListaFracaoRow.FracaoNominalNormalizada * ValortotalSim, 2, System.MidpointRounding.AwayFromZero);
                else
                    ListaFracaoRow.ValorCorrigido = System.Math.Round(ListaFracaoRow.FracaoCorrigida * ValortotalSim, 2, System.MidpointRounding.AwayFromZero);
                if (ListaFracaoRow.ValorCorrigido > ListaFracaoRow.DescontoEmReais)
                    ListaFracaoRow.ValorCorrigido -= ListaFracaoRow.DescontoEmReais;
                else
                    ListaFracaoRow.ValorCorrigido = 0;
                NovoTotalCorrigido += ListaFracaoRow.ValorCorrigido;
                ListaFracaoRow.ValorNominal = System.Math.Round(ListaFracaoRow.FracaoReferenciaSemDesconto * ValortotalSim, 2, System.MidpointRounding.AwayFromZero);
                Nominal += ListaFracaoRow.ValorNominal;// System.Math.Round(ListaFracaoRow.FracaoReferenciaSemDesconto * ValortotalSim, 2, System.MidpointRounding.AwayFromZero);
            };



        }

    }
}
