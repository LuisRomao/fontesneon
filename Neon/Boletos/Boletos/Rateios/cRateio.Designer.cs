namespace Boletos.Rateios
{
    /// <summary>
    /// Rateio
    /// </summary>
    partial class cRateio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cRateio));
            System.Windows.Forms.Label rAT_PLALabel;
            System.Windows.Forms.Label rATFracaoLabel;
            System.Windows.Forms.Label rATMensagemLabel;
            System.Windows.Forms.Label rAT_BLOLabel;
            System.Windows.Forms.Label rATValorLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label4;
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.richEditControl1 = new DevExpress.XtraRichEdit.RichEditControl();
            this.rATeiosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoImpNeon1 = new VirBotaoNeon.cBotaoImpNeon();
            this.BtImplantacao = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.chBlocosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dRateiosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCGONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDRDescontoRateios = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDRDescontoRateiosValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTipo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colAplicar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cCompetData1 = new Framework.objetosNeon.cCompetData();
            this.spinPrimeira = new DevExpress.XtraEditors.SpinEdit();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.button1 = new System.Windows.Forms.Button();
            this.spinParcelas = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.tiposDeFracaoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rAT_PLALookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rATValorCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.rATMensagemTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.rAT_PLALookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listaFracaoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fKBOletoDetalheRAteioDetalhesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fKRAteioDetalhesRAteioDetalhesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rAteioDetalhesGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRADData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRADValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colRADMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit35 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRADGerado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRADComCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRADCompetenciaAno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpinEditAno = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colRADCompetenciaMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpinEditMes = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colRAD_CTL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditCTL = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rAteioDetalhesGridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit50 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBOD_BOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOD_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditNumero = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBODProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EditorPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colBODFracaoEfetiva = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCopiaBOD_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditBloco = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBODData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bindingSourceAUXCondominio = new System.Windows.Forms.BindingSource(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            rAT_PLALabel = new System.Windows.Forms.Label();
            rATFracaoLabel = new System.Windows.Forms.Label();
            rATMensagemLabel = new System.Windows.Forms.Label();
            rAT_BLOLabel = new System.Windows.Forms.Label();
            rATValorLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rATeiosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chBlocosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRateiosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPrimeira.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinParcelas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tiposDeFracaoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAT_PLALookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rATValorCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rATMensagemTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAT_PLALookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaFracaoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBOletoDetalheRAteioDetalhesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKRAteioDetalhesRAteioDetalhesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAteioDetalhesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEditAno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEditMes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditCTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAteioDetalhesGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditNumero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditBloco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceAUXCondominio)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // rAT_PLALabel
            // 
            rAT_PLALabel.AutoSize = true;
            rAT_PLALabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            rAT_PLALabel.Location = new System.Drawing.Point(14, 47);
            rAT_PLALabel.Name = "rAT_PLALabel";
            rAT_PLALabel.Size = new System.Drawing.Size(34, 13);
            rAT_PLALabel.TabIndex = 3;
            rAT_PLALabel.Text = "Tipo:";
            // 
            // rATFracaoLabel
            // 
            rATFracaoLabel.AutoSize = true;
            rATFracaoLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            rATFracaoLabel.Location = new System.Drawing.Point(14, 101);
            rATFracaoLabel.Name = "rATFracaoLabel";
            rATFracaoLabel.Size = new System.Drawing.Size(92, 13);
            rATFracaoLabel.TabIndex = 8;
            rATFracaoLabel.Text = "Tipo de Fra��o:";
            // 
            // rATMensagemLabel
            // 
            rATMensagemLabel.AutoSize = true;
            rATMensagemLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            rATMensagemLabel.Location = new System.Drawing.Point(14, 73);
            rATMensagemLabel.Name = "rATMensagemLabel";
            rATMensagemLabel.Size = new System.Drawing.Size(72, 13);
            rATMensagemLabel.TabIndex = 16;
            rATMensagemLabel.Text = "Mensagem:";
            // 
            // rAT_BLOLabel
            // 
            rAT_BLOLabel.AutoSize = true;
            rAT_BLOLabel.Location = new System.Drawing.Point(205, 101);
            rAT_BLOLabel.Name = "rAT_BLOLabel";
            rAT_BLOLabel.Size = new System.Drawing.Size(41, 13);
            rAT_BLOLabel.TabIndex = 20;
            rAT_BLOLabel.Text = "Blocos:";
            // 
            // rATValorLabel
            // 
            rATValorLabel.AutoSize = true;
            rATValorLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            rATValorLabel.Location = new System.Drawing.Point(19, 15);
            rATValorLabel.Name = "rATValorLabel";
            rATValorLabel.Size = new System.Drawing.Size(71, 13);
            rATValorLabel.TabIndex = 22;
            rATValorLabel.Text = "Valor Total:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(9, 20);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(51, 13);
            label1.TabIndex = 42;
            label1.Text = "Parcelas:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(9, 46);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(76, 13);
            label3.TabIndex = 43;
            label3.Text = "N�mero inicial:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            label2.Location = new System.Drawing.Point(138, 26);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(80, 13);
            label2.TabIndex = 45;
            label2.Text = "Destinat�rio:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            label4.Location = new System.Drawing.Point(175, 52);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(43, 13);
            label4.TabIndex = 46;
            label4.Text = "Conta:";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.richEditControl1);
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.cBotaoImpNeon1);
            this.panelControl1.Controls.Add(this.BtImplantacao);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.gridControl2);
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.lookUpEdit1);
            this.panelControl1.Controls.Add(this.rAT_PLALookUpEdit1);
            this.panelControl1.Controls.Add(rATValorLabel);
            this.panelControl1.Controls.Add(this.rATValorCalcEdit);
            this.panelControl1.Controls.Add(rAT_BLOLabel);
            this.panelControl1.Controls.Add(rATMensagemLabel);
            this.panelControl1.Controls.Add(this.rATMensagemTextEdit);
            this.panelControl1.Controls.Add(rATFracaoLabel);
            this.panelControl1.Controls.Add(rAT_PLALabel);
            this.panelControl1.Controls.Add(this.rAT_PLALookUpEdit);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1476, 249);
            this.panelControl1.TabIndex = 0;
            // 
            // richEditControl1
            // 
            this.richEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControl1.DataBindings.Add(new System.Windows.Forms.Binding("RtfText", this.rATeiosBindingSource, "RATHistorico", true));
            this.richEditControl1.Location = new System.Drawing.Point(799, 4);
            this.richEditControl1.MenuManager = this.BarManager_F;
            this.richEditControl1.Name = "richEditControl1";
            this.richEditControl1.Options.AutoCorrect.UseSpellCheckerSuggestions = true;
            this.richEditControl1.Size = new System.Drawing.Size(374, 239);
            this.richEditControl1.TabIndex = 48;
            this.richEditControl1.Text = "richEditControl1";
            // 
            // rATeiosBindingSource
            // 
            this.rATeiosBindingSource.DataMember = "RATeios";
            this.rATeiosBindingSource.DataSource = typeof(Boletos.Rateios.dRateios);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(430, 150);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(152, 46);
            this.simpleButton5.TabIndex = 47;
            this.simpleButton5.Text = "Gravar contas retroativas";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // cBotaoImpNeon1
            // 
            this.cBotaoImpNeon1.AbrirArquivoExportado = true;
            this.cBotaoImpNeon1.AcaoBotao = dllBotao.Botao.imprimir;
            this.cBotaoImpNeon1.AssuntoEmail = null;
            this.cBotaoImpNeon1.BotaoEmail = true;
            this.cBotaoImpNeon1.BotaoImprimir = true;
            this.cBotaoImpNeon1.BotaoPDF = true;
            this.cBotaoImpNeon1.BotaoTela = true;
            this.cBotaoImpNeon1.CorpoEmail = null;
            this.cBotaoImpNeon1.CreateDocAutomatico = true;
            this.cBotaoImpNeon1.Impresso = null;
            this.cBotaoImpNeon1.Location = new System.Drawing.Point(588, 203);
            this.cBotaoImpNeon1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpNeon1.Name = "cBotaoImpNeon1";
            this.cBotaoImpNeon1.NaoUsarxlsX = false;
            this.cBotaoImpNeon1.NomeArquivoAnexo = null;
            this.cBotaoImpNeon1.PriComp = null;
            this.cBotaoImpNeon1.Size = new System.Drawing.Size(200, 41);
            this.cBotaoImpNeon1.TabIndex = 46;
            this.cBotaoImpNeon1.Titulo = "Imp. Boletos";
            this.cBotaoImpNeon1.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImpNeon1_clicado);
            // 
            // BtImplantacao
            // 
            this.BtImplantacao.AllowFocus = false;
            this.BtImplantacao.Location = new System.Drawing.Point(260, 8);
            this.BtImplantacao.Name = "BtImplantacao";
            this.BtImplantacao.Size = new System.Drawing.Size(159, 23);
            this.BtImplantacao.TabIndex = 45;
            this.BtImplantacao.Text = "Implanta��o";
            this.BtImplantacao.Click += new System.EventHandler(this.BtImplantacao_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(430, 202);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(152, 42);
            this.simpleButton4.TabIndex = 44;
            this.simpleButton4.Text = "Simular Re-Gera��o";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.chBlocosBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(250, 92);
            this.gridControl2.MainView = this.gridView4;
            this.gridControl2.MenuManager = this.BarManager_F;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(169, 151);
            this.gridControl2.TabIndex = 43;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // chBlocosBindingSource
            // 
            this.chBlocosBindingSource.DataSource = typeof(Boletos.Rateios.dRateios.ChBlocosDataTable);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCh,
            this.colDescricao});
            this.gridView4.GridControl = this.gridControl2;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsView.ShowColumnHeaders = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescricao, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView4_RowUpdated);
            // 
            // colCh
            // 
            this.colCh.FieldName = "Ch";
            this.colCh.Name = "colCh";
            this.colCh.OptionsColumn.FixedWidth = true;
            this.colCh.Visible = true;
            this.colCh.VisibleIndex = 0;
            this.colCh.Width = 31;
            // 
            // colDescricao
            // 
            this.colDescricao.FieldName = "Descricao";
            this.colDescricao.Name = "colDescricao";
            this.colDescricao.Visible = true;
            this.colDescricao.VisibleIndex = 1;
            this.colDescricao.Width = 881;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Descontos";
            this.gridControl1.DataSource = this.dRateiosBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(5, 120);
            this.gridControl1.MainView = this.gridView3;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTipo});
            this.gridControl1.Size = new System.Drawing.Size(214, 123);
            this.gridControl1.TabIndex = 42;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // dRateiosBindingSource
            // 
            this.dRateiosBindingSource.DataSource = typeof(Boletos.Rateios.dRateios);
            this.dRateiosBindingSource.Position = 0;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPTNumero,
            this.colBLONome,
            this.colCGONome,
            this.colCDRDescontoRateios,
            this.colCDRDescontoRateiosValor,
            this.colAplicar});
            this.gridView3.GridControl = this.gridControl1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowColumnHeaders = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView3_RowUpdated);
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.OptionsColumn.ReadOnly = true;
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 3;
            this.colAPTNumero.Width = 40;
            // 
            // colBLONome
            // 
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.OptionsColumn.ReadOnly = true;
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 2;
            this.colBLONome.Width = 25;
            // 
            // colCGONome
            // 
            this.colCGONome.AppearanceCell.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colCGONome.AppearanceCell.Options.UseFont = true;
            this.colCGONome.FieldName = "CGONome";
            this.colCGONome.Name = "colCGONome";
            this.colCGONome.OptionsColumn.ReadOnly = true;
            this.colCGONome.Visible = true;
            this.colCGONome.VisibleIndex = 1;
            this.colCGONome.Width = 60;
            // 
            // colCDRDescontoRateios
            // 
            this.colCDRDescontoRateios.DisplayFormat.FormatString = "n2";
            this.colCDRDescontoRateios.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCDRDescontoRateios.FieldName = "CDRDescontoRateios";
            this.colCDRDescontoRateios.Name = "colCDRDescontoRateios";
            this.colCDRDescontoRateios.OptionsColumn.ReadOnly = true;
            this.colCDRDescontoRateios.Visible = true;
            this.colCDRDescontoRateios.VisibleIndex = 4;
            this.colCDRDescontoRateios.Width = 35;
            // 
            // colCDRDescontoRateiosValor
            // 
            this.colCDRDescontoRateiosValor.ColumnEdit = this.repositoryTipo;
            this.colCDRDescontoRateiosValor.FieldName = "CDRDescontoRateiosValor";
            this.colCDRDescontoRateiosValor.Name = "colCDRDescontoRateiosValor";
            this.colCDRDescontoRateiosValor.OptionsColumn.ReadOnly = true;
            this.colCDRDescontoRateiosValor.Visible = true;
            this.colCDRDescontoRateiosValor.VisibleIndex = 5;
            this.colCDRDescontoRateiosValor.Width = 26;
            // 
            // repositoryTipo
            // 
            this.repositoryTipo.AutoHeight = false;
            this.repositoryTipo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTipo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("R$", true, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("%", false, -1)});
            this.repositoryTipo.Name = "repositoryTipo";
            // 
            // colAplicar
            // 
            this.colAplicar.FieldName = "Aplicar";
            this.colAplicar.Name = "colAplicar";
            this.colAplicar.Visible = true;
            this.colAplicar.VisibleIndex = 0;
            this.colAplicar.Width = 21;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lookUpEdit2);
            this.groupControl1.Controls.Add(label4);
            this.groupControl1.Controls.Add(this.cCompetData1);
            this.groupControl1.Controls.Add(label2);
            this.groupControl1.Controls.Add(this.spinPrimeira);
            this.groupControl1.Controls.Add(this.imageComboBoxEdit1);
            this.groupControl1.Controls.Add(label3);
            this.groupControl1.Controls.Add(label1);
            this.groupControl1.Controls.Add(this.button1);
            this.groupControl1.Controls.Add(this.spinParcelas);
            this.groupControl1.Location = new System.Drawing.Point(425, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(368, 144);
            this.groupControl1.TabIndex = 41;
            this.groupControl1.Text = "Parcelar";
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rATeiosBindingSource, "RAT_CTL", true));
            this.lookUpEdit2.Location = new System.Drawing.Point(217, 49);
            this.lookUpEdit2.MenuManager = this.BarManager_F;
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit2.Properties.DataSource = this.conTasLogicasBindingSource;
            this.lookUpEdit2.Properties.DisplayMember = "CTLTitulo";
            this.lookUpEdit2.Properties.NullText = "CAIXA";
            this.lookUpEdit2.Properties.ShowHeader = false;
            this.lookUpEdit2.Properties.ValueMember = "CTL";
            this.lookUpEdit2.Size = new System.Drawing.Size(146, 20);
            this.lookUpEdit2.TabIndex = 47;
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = typeof(Boletos.Rateios.dRateios);
            // 
            // cCompetData1
            // 
            this.cCompetData1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetData1.Appearance.Options.UseBackColor = true;
            this.cCompetData1.CaixaAltaGeral = true;
            this.cCompetData1.Comcondominio = false;
            this.cCompetData1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetData1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetData1.Location = new System.Drawing.Point(8, 70);
            this.cCompetData1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.cCompetData1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetData1.Name = "cCompetData1";
            this.cCompetData1.Size = new System.Drawing.Size(278, 27);
            this.cCompetData1.somenteleitura = false;
            this.cCompetData1.TabIndex = 42;
            this.cCompetData1.Titulo = null;
            // 
            // spinPrimeira
            // 
            this.spinPrimeira.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinPrimeira.Location = new System.Drawing.Point(79, 48);
            this.spinPrimeira.Name = "spinPrimeira";
            this.spinPrimeira.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinPrimeira.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spinPrimeira.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinPrimeira.Size = new System.Drawing.Size(53, 20);
            this.spinPrimeira.TabIndex = 44;
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rATeiosBindingSource, "RATProprietario", true));
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(263, 23);
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Propriet�rio", true, 0)});
            this.imageComboBoxEdit1.Properties.SmallImages = this.imagensPI1;
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(100, 20);
            this.imageComboBoxEdit1.TabIndex = 44;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(358, 23);
            this.button1.TabIndex = 26;
            this.button1.Text = "Parcelar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.DividirParcelas);
            // 
            // spinParcelas
            // 
            this.spinParcelas.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinParcelas.Location = new System.Drawing.Point(79, 23);
            this.spinParcelas.Name = "spinParcelas";
            this.spinParcelas.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinParcelas.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spinParcelas.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinParcelas.Size = new System.Drawing.Size(53, 20);
            this.spinParcelas.TabIndex = 25;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(179, 92);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(19, 23);
            this.simpleButton3.TabIndex = 40;
            this.simpleButton3.Text = "C";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(225, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(17, 27);
            this.simpleButton2.TabIndex = 33;
            this.simpleButton2.Text = "C";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(588, 150);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(200, 46);
            this.simpleButton1.TabIndex = 30;
            this.simpleButton1.Text = "GERAR";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rATeiosBindingSource, "RATFracao", true));
            this.lookUpEdit1.Location = new System.Drawing.Point(98, 94);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Descricao", "Descricao", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit1.Properties.DataSource = this.tiposDeFracaoBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "Descricao";
            this.lookUpEdit1.Properties.PopupWidth = 400;
            this.lookUpEdit1.Properties.ValueMember = "Codigo";
            this.lookUpEdit1.Size = new System.Drawing.Size(80, 20);
            this.lookUpEdit1.TabIndex = 23;
            this.lookUpEdit1.EditValueChanged += new System.EventHandler(this.lookUpEdit1_EditValueChanged);
            // 
            // tiposDeFracaoBindingSource
            // 
            this.tiposDeFracaoBindingSource.DataMember = "TiposDeFracao";
            this.tiposDeFracaoBindingSource.DataSource = typeof(Boletos.Rateios.dRateios);
            // 
            // rAT_PLALookUpEdit1
            // 
            this.rAT_PLALookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rATeiosBindingSource, "RAT_PLA", true));
            this.rAT_PLALookUpEdit1.Location = new System.Drawing.Point(98, 42);
            this.rAT_PLALookUpEdit1.Name = "rAT_PLALookUpEdit1";
            this.rAT_PLALookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rAT_PLALookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "C�digo", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descric�o", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.rAT_PLALookUpEdit1.Properties.DataSource = this.pLAnocontasBindingSource;
            this.rAT_PLALookUpEdit1.Properties.DisplayMember = "PLA";
            this.rAT_PLALookUpEdit1.Properties.PopupWidth = 400;
            this.rAT_PLALookUpEdit1.Properties.ValueMember = "PLA";
            this.rAT_PLALookUpEdit1.Size = new System.Drawing.Size(100, 20);
            this.rAT_PLALookUpEdit1.TabIndex = 2;
            this.rAT_PLALookUpEdit1.EditValueChanged += new System.EventHandler(this.rAT_PLALookUpEdit1_EditValueChanged);
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Boletos.Rateios.dRateios);
            // 
            // rATValorCalcEdit
            // 
            this.rATValorCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rATeiosBindingSource, "RATValor", true));
            this.rATValorCalcEdit.Location = new System.Drawing.Point(94, 5);
            this.rATValorCalcEdit.Name = "rATValorCalcEdit";
            this.rATValorCalcEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rATValorCalcEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.rATValorCalcEdit.Properties.Appearance.Options.UseFont = true;
            this.rATValorCalcEdit.Properties.Appearance.Options.UseForeColor = true;
            this.rATValorCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rATValorCalcEdit.Properties.DisplayFormat.FormatString = "n2";
            this.rATValorCalcEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.rATValorCalcEdit.Properties.EditFormat.FormatString = "n2";
            this.rATValorCalcEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.rATValorCalcEdit.Size = new System.Drawing.Size(125, 26);
            this.rATValorCalcEdit.TabIndex = 11;
            // 
            // rATMensagemTextEdit
            // 
            this.rATMensagemTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rATeiosBindingSource, "RATMensagem", true));
            this.rATMensagemTextEdit.Location = new System.Drawing.Point(98, 69);
            this.rATMensagemTextEdit.Name = "rATMensagemTextEdit";
            this.rATMensagemTextEdit.Size = new System.Drawing.Size(323, 20);
            this.rATMensagemTextEdit.TabIndex = 4;
            this.rATMensagemTextEdit.Enter += new System.EventHandler(this.rATMensagemTextEdit_Enter);
            this.rATMensagemTextEdit.Leave += new System.EventHandler(this.rATMensagemTextEdit_Leave);
            // 
            // rAT_PLALookUpEdit
            // 
            this.rAT_PLALookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rATeiosBindingSource, "RAT_PLA", true));
            this.rAT_PLALookUpEdit.Location = new System.Drawing.Point(204, 42);
            this.rAT_PLALookUpEdit.Name = "rAT_PLALookUpEdit";
            this.rAT_PLALookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rAT_PLALookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descri��o", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "C�digo", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.rAT_PLALookUpEdit.Properties.DataSource = this.pLAnocontasBindingSource;
            this.rAT_PLALookUpEdit.Properties.DisplayMember = "PLADescricao";
            this.rAT_PLALookUpEdit.Properties.PopupWidth = 400;
            this.rAT_PLALookUpEdit.Properties.ValueMember = "PLA";
            this.rAT_PLALookUpEdit.Size = new System.Drawing.Size(217, 20);
            this.rAT_PLALookUpEdit.TabIndex = 3;
            this.rAT_PLALookUpEdit.TabStop = false;
            this.rAT_PLALookUpEdit.EditValueChanged += new System.EventHandler(this.rAT_PLALookUpEdit1_EditValueChanged);
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(Boletos.Rateios.dRateios);
            // 
            // listaFracaoBindingSource
            // 
            this.listaFracaoBindingSource.DataMember = "ListaFracao";
            this.listaFracaoBindingSource.DataSource = typeof(Boletos.Rateios.dRateios);
            // 
            // fKBOletoDetalheRAteioDetalhesBindingSource
            // 
            this.fKBOletoDetalheRAteioDetalhesBindingSource.DataMember = "FK_BOletoDetalhe_RAteioDetalhes";
            this.fKBOletoDetalheRAteioDetalhesBindingSource.DataSource = this.fKRAteioDetalhesRAteioDetalhesBindingSource;
            // 
            // fKRAteioDetalhesRAteioDetalhesBindingSource
            // 
            this.fKRAteioDetalhesRAteioDetalhesBindingSource.DataMember = "FK_RAteioDetalhes_RAteioDetalhes";
            this.fKRAteioDetalhesRAteioDetalhesBindingSource.DataSource = this.rATeiosBindingSource;
            // 
            // rAteioDetalhesGridControl
            // 
            this.rAteioDetalhesGridControl.DataSource = this.fKRAteioDetalhesRAteioDetalhesBindingSource;
            this.rAteioDetalhesGridControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.rAteioDetalhesGridControl.Location = new System.Drawing.Point(0, 284);
            this.rAteioDetalhesGridControl.MainView = this.gridView1;
            this.rAteioDetalhesGridControl.Name = "rAteioDetalhesGridControl";
            this.rAteioDetalhesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.SpinEditAno,
            this.SpinEditMes,
            this.repositoryItemCalcEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit35,
            this.LookUpEditCTL});
            this.rAteioDetalhesGridControl.Size = new System.Drawing.Size(1476, 133);
            this.rAteioDetalhesGridControl.TabIndex = 1;
            this.rAteioDetalhesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(120)))), ((int)(((byte)(88)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(136)))), ((int)(((byte)(91)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(172)))), ((int)(((byte)(134)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(234)))), ((int)(((byte)(216)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(224)))), ((int)(((byte)(190)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(237)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(120)))), ((int)(((byte)(88)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(159)))), ((int)(((byte)(114)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRADData,
            this.colRADValor,
            this.colRADMensagem,
            this.colRADGerado,
            this.colRADComCondominio,
            this.colRADCompetenciaAno,
            this.colRADCompetenciaMes,
            this.colRAD_CTL});
            this.gridView1.GridControl = this.rAteioDetalhesGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // colRADData
            // 
            this.colRADData.Caption = "Vencimento";
            this.colRADData.FieldName = "RADData";
            this.colRADData.Name = "colRADData";
            this.colRADData.Visible = true;
            this.colRADData.VisibleIndex = 0;
            this.colRADData.Width = 111;
            // 
            // colRADValor
            // 
            this.colRADValor.Caption = "Valor";
            this.colRADValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colRADValor.DisplayFormat.FormatString = "n2";
            this.colRADValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRADValor.FieldName = "RADValor";
            this.colRADValor.Name = "colRADValor";
            this.colRADValor.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colRADValor.Visible = true;
            this.colRADValor.VisibleIndex = 4;
            this.colRADValor.Width = 100;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Calcular Valor", null, null, true)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            this.repositoryItemCalcEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemCalcEdit1_ButtonClick);
            // 
            // colRADMensagem
            // 
            this.colRADMensagem.Caption = "Mensagem";
            this.colRADMensagem.ColumnEdit = this.repositoryItemTextEdit35;
            this.colRADMensagem.FieldName = "RADMensagem";
            this.colRADMensagem.Name = "colRADMensagem";
            this.colRADMensagem.Visible = true;
            this.colRADMensagem.VisibleIndex = 5;
            this.colRADMensagem.Width = 196;
            // 
            // repositoryItemTextEdit35
            // 
            this.repositoryItemTextEdit35.AutoHeight = false;
            this.repositoryItemTextEdit35.MaxLength = 35;
            this.repositoryItemTextEdit35.Name = "repositoryItemTextEdit35";
            // 
            // colRADGerado
            // 
            this.colRADGerado.Caption = "Emitido";
            this.colRADGerado.FieldName = "RADGerado";
            this.colRADGerado.Name = "colRADGerado";
            this.colRADGerado.OptionsColumn.AllowEdit = false;
            this.colRADGerado.OptionsColumn.AllowFocus = false;
            this.colRADGerado.OptionsColumn.ReadOnly = true;
            this.colRADGerado.Visible = true;
            this.colRADGerado.VisibleIndex = 6;
            this.colRADGerado.Width = 59;
            // 
            // colRADComCondominio
            // 
            this.colRADComCondominio.Caption = "Com Condom�nio";
            this.colRADComCondominio.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRADComCondominio.FieldName = "RADComCondominio";
            this.colRADComCondominio.Name = "colRADComCondominio";
            this.colRADComCondominio.Visible = true;
            this.colRADComCondominio.VisibleIndex = 1;
            this.colRADComCondominio.Width = 105;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.CheckedChanged += new System.EventHandler(this.AjustaData);
            // 
            // colRADCompetenciaAno
            // 
            this.colRADCompetenciaAno.Caption = "Competencia Ano";
            this.colRADCompetenciaAno.ColumnEdit = this.SpinEditAno;
            this.colRADCompetenciaAno.FieldName = "RADCompetenciaAno";
            this.colRADCompetenciaAno.Name = "colRADCompetenciaAno";
            this.colRADCompetenciaAno.Visible = true;
            this.colRADCompetenciaAno.VisibleIndex = 3;
            this.colRADCompetenciaAno.Width = 108;
            // 
            // SpinEditAno
            // 
            this.SpinEditAno.AutoHeight = false;
            this.SpinEditAno.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SpinEditAno.MaxValue = new decimal(new int[] {
            2107,
            0,
            0,
            0});
            this.SpinEditAno.MinValue = new decimal(new int[] {
            2007,
            0,
            0,
            0});
            this.SpinEditAno.Name = "SpinEditAno";
            this.SpinEditAno.EditValueChanged += new System.EventHandler(this.AjustaData);
            // 
            // colRADCompetenciaMes
            // 
            this.colRADCompetenciaMes.Caption = "Competencia M�s";
            this.colRADCompetenciaMes.ColumnEdit = this.SpinEditMes;
            this.colRADCompetenciaMes.FieldName = "RADCompetenciaMes";
            this.colRADCompetenciaMes.Name = "colRADCompetenciaMes";
            this.colRADCompetenciaMes.Visible = true;
            this.colRADCompetenciaMes.VisibleIndex = 2;
            this.colRADCompetenciaMes.Width = 115;
            // 
            // SpinEditMes
            // 
            this.SpinEditMes.AutoHeight = false;
            this.SpinEditMes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SpinEditMes.MaxValue = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.SpinEditMes.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SpinEditMes.Name = "SpinEditMes";
            this.SpinEditMes.EditValueChanged += new System.EventHandler(this.AjustaData);
            // 
            // colRAD_CTL
            // 
            this.colRAD_CTL.Caption = "Conta";
            this.colRAD_CTL.ColumnEdit = this.LookUpEditCTL;
            this.colRAD_CTL.FieldName = "RAD_CTL";
            this.colRAD_CTL.Name = "colRAD_CTL";
            this.colRAD_CTL.Visible = true;
            this.colRAD_CTL.VisibleIndex = 7;
            this.colRAD_CTL.Width = 162;
            // 
            // LookUpEditCTL
            // 
            this.LookUpEditCTL.AutoHeight = false;
            this.LookUpEditCTL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditCTL.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditCTL.DataSource = this.conTasLogicasBindingSource;
            this.LookUpEditCTL.DisplayMember = "CTLTitulo";
            this.LookUpEditCTL.Name = "LookUpEditCTL";
            this.LookUpEditCTL.ShowHeader = false;
            this.LookUpEditCTL.ValueMember = "CTL";
            // 
            // rAteioDetalhesGridControl1
            // 
            this.rAteioDetalhesGridControl1.DataSource = this.fKBOletoDetalheRAteioDetalhesBindingSource;
            this.rAteioDetalhesGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rAteioDetalhesGridControl1.Location = new System.Drawing.Point(0, 417);
            this.rAteioDetalhesGridControl1.MainView = this.gridView2;
            this.rAteioDetalhesGridControl1.Name = "rAteioDetalhesGridControl1";
            this.rAteioDetalhesGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEditNumero,
            this.LookUpEditBloco,
            this.EditorPI,
            this.repositoryItemTextEdit50});
            this.rAteioDetalhesGridControl1.Size = new System.Drawing.Size(1476, 365);
            this.rAteioDetalhesGridControl1.TabIndex = 2;
            this.rAteioDetalhesGridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.rAteioDetalhesGridControl1.Click += new System.EventHandler(this.rAteioDetalhesGridControl1_Click);
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView2.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView2.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView2.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView2.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView2.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView2.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBODValor,
            this.colBODMensagem,
            this.colBOD_BOL,
            this.colBOD_APT,
            this.colBODProprietario,
            this.colBODFracaoEfetiva,
            this.colCopiaBOD_APT,
            this.colBODData,
            this.gridColumn1});
            this.gridView2.GridControl = this.rAteioDetalhesGridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCopiaBOD_APT, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOD_APT, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView2_RowCellStyle);
            this.gridView2.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView2_ValidateRow);
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.DisplayFormat.FormatString = "n2";
            this.colBODValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", "{0:n2}")});
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 5;
            this.colBODValor.Width = 110;
            // 
            // colBODMensagem
            // 
            this.colBODMensagem.Caption = "Mensagem";
            this.colBODMensagem.ColumnEdit = this.repositoryItemTextEdit50;
            this.colBODMensagem.FieldName = "BODMensagem";
            this.colBODMensagem.Name = "colBODMensagem";
            this.colBODMensagem.Visible = true;
            this.colBODMensagem.VisibleIndex = 6;
            this.colBODMensagem.Width = 252;
            // 
            // repositoryItemTextEdit50
            // 
            this.repositoryItemTextEdit50.AutoHeight = false;
            this.repositoryItemTextEdit50.MaxLength = 50;
            this.repositoryItemTextEdit50.Name = "repositoryItemTextEdit50";
            // 
            // colBOD_BOL
            // 
            this.colBOD_BOL.Caption = "Boleto";
            this.colBOD_BOL.FieldName = "BOD_BOL";
            this.colBOD_BOL.Name = "colBOD_BOL";
            this.colBOD_BOL.OptionsColumn.AllowEdit = false;
            this.colBOD_BOL.Visible = true;
            this.colBOD_BOL.VisibleIndex = 7;
            this.colBOD_BOL.Width = 87;
            // 
            // colBOD_APT
            // 
            this.colBOD_APT.Caption = "N�mero";
            this.colBOD_APT.ColumnEdit = this.LookUpEditNumero;
            this.colBOD_APT.FieldName = "BOD_APT";
            this.colBOD_APT.Name = "colBOD_APT";
            this.colBOD_APT.OptionsColumn.AllowEdit = false;
            this.colBOD_APT.Visible = true;
            this.colBOD_APT.VisibleIndex = 1;
            this.colBOD_APT.Width = 90;
            // 
            // LookUpEditNumero
            // 
            this.LookUpEditNumero.AutoHeight = false;
            this.LookUpEditNumero.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditNumero.DataSource = this.listaFracaoBindingSource;
            this.LookUpEditNumero.DisplayMember = "APTNumero";
            this.LookUpEditNumero.Name = "LookUpEditNumero";
            this.LookUpEditNumero.ValueMember = "APT";
            // 
            // colBODProprietario
            // 
            this.colBODProprietario.Caption = "Destinat�rio";
            this.colBODProprietario.ColumnEdit = this.EditorPI;
            this.colBODProprietario.FieldName = "BODProprietario";
            this.colBODProprietario.Name = "colBODProprietario";
            this.colBODProprietario.Visible = true;
            this.colBODProprietario.VisibleIndex = 3;
            this.colBODProprietario.Width = 81;
            // 
            // EditorPI
            // 
            this.EditorPI.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.EditorPI.AutoHeight = false;
            this.EditorPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EditorPI.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EditorPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Proprietario", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 1)});
            this.EditorPI.Name = "EditorPI";
            this.EditorPI.SmallImages = this.imagensPI1;
            // 
            // colBODFracaoEfetiva
            // 
            this.colBODFracaoEfetiva.Caption = "Fra��o Efetiva";
            this.colBODFracaoEfetiva.DisplayFormat.FormatString = "d4";
            this.colBODFracaoEfetiva.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODFracaoEfetiva.FieldName = "BODFracaoEfetiva";
            this.colBODFracaoEfetiva.Name = "colBODFracaoEfetiva";
            this.colBODFracaoEfetiva.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colBODFracaoEfetiva.Visible = true;
            this.colBODFracaoEfetiva.VisibleIndex = 4;
            this.colBODFracaoEfetiva.Width = 91;
            // 
            // colCopiaBOD_APT
            // 
            this.colCopiaBOD_APT.Caption = "Bloco";
            this.colCopiaBOD_APT.ColumnEdit = this.LookUpEditBloco;
            this.colCopiaBOD_APT.FieldName = "CopiaBOD_APT";
            this.colCopiaBOD_APT.Name = "colCopiaBOD_APT";
            this.colCopiaBOD_APT.OptionsColumn.AllowEdit = false;
            this.colCopiaBOD_APT.Visible = true;
            this.colCopiaBOD_APT.VisibleIndex = 0;
            this.colCopiaBOD_APT.Width = 134;
            // 
            // LookUpEditBloco
            // 
            this.LookUpEditBloco.AutoHeight = false;
            this.LookUpEditBloco.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditBloco.DataSource = this.listaFracaoBindingSource;
            this.LookUpEditBloco.DisplayMember = "BLONome";
            this.LookUpEditBloco.Name = "LookUpEditBloco";
            this.LookUpEditBloco.ValueMember = "APT";
            // 
            // colBODData
            // 
            this.colBODData.Caption = "Data";
            this.colBODData.FieldName = "BODData";
            this.colBODData.Name = "colBODData";
            this.colBODData.Visible = true;
            this.colBODData.VisibleIndex = 2;
            this.colBODData.Width = 76;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Simulado";
            this.gridColumn1.DisplayFormat.FormatString = "n2";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn1.FieldName = "Simulado";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Simulado", "{0:n2}")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 8;
            // 
            // bindingSourceAUXCondominio
            // 
            this.bindingSourceAUXCondominio.DataMember = "CONDOMINIOS";
            this.bindingSourceAUXCondominio.DataSource = typeof(Boletos.Rateios.dRateios);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Liberar Competencias";
            this.barButtonItem1.Id = 9;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Implanta��o";
            this.barButtonItem2.Id = 9;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // cRateio
            // 
            this.Autofill = false;
            this.AutoScroll = true;
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.rAteioDetalhesGridControl1);
            this.Controls.Add(this.rAteioDetalhesGridControl);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cRateio";
            this.Size = new System.Drawing.Size(1476, 807);
            this.Titulo = "Rateio";
            this.cargaInicial += new System.EventHandler(this.cRateio_cargaInicial);
            this.cargaFinal += new System.EventHandler(this.cRateio_cargaFinal);
            this.Load += new System.EventHandler(this.cRateio_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.rAteioDetalhesGridControl, 0);
            this.Controls.SetChildIndex(this.rAteioDetalhesGridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rATeiosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chBlocosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRateiosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPrimeira.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinParcelas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tiposDeFracaoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAT_PLALookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rATValorCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rATMensagemTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAT_PLALookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaFracaoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBOletoDetalheRAteioDetalhesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKRAteioDetalhesRAteioDetalhesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAteioDetalhesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEditAno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEditMes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditCTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAteioDetalhesGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditNumero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditBloco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceAUXCondominio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.BindingSource rATeiosBindingSource;
        private DevExpress.XtraEditors.LookUpEdit rAT_PLALookUpEdit;
        private DevExpress.XtraEditors.CalcEdit rATValorCalcEdit;
        private DevExpress.XtraEditors.TextEdit rATMensagemTextEdit;
        private DevExpress.XtraGrid.GridControl rAteioDetalhesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colRADData;
        private DevExpress.XtraGrid.Columns.GridColumn colRADValor;
        private DevExpress.XtraGrid.Columns.GridColumn colRADMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colRADGerado;
        private DevExpress.XtraGrid.Columns.GridColumn colRADComCondominio;
        private DevExpress.XtraGrid.Columns.GridColumn colRADCompetenciaAno;
        private DevExpress.XtraGrid.Columns.GridColumn colRADCompetenciaMes;
        private DevExpress.XtraGrid.GridControl rAteioDetalhesGridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
                
        private System.Windows.Forms.BindingSource bindingSourceAUXCondominio;
        private DevExpress.XtraEditors.LookUpEdit rAT_PLALookUpEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.BindingSource tiposDeFracaoBindingSource;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraEditors.SpinEdit spinParcelas;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit SpinEditAno;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit SpinEditMes;
        private System.Windows.Forms.BindingSource fKBOletoDetalheRAteioDetalhesBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraGrid.Columns.GridColumn colBODMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_BOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_APT;
        private DevExpress.XtraGrid.Columns.GridColumn colBODProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colBODFracaoEfetiva;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditNumero;
        private System.Windows.Forms.BindingSource listaFracaoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCopiaBOD_APT;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditBloco;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.Columns.GridColumn colBODData;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SpinEdit spinPrimeira;
        private Framework.objetosNeon.cCompetData cCompetData1;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox EditorPI;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraGrid.Columns.GridColumn colCGONome;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRDescontoRateios;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRDescontoRateiosValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colAplicar;
        private System.Windows.Forms.BindingSource fKRAteioDetalhesRAteioDetalhesBindingSource;
        private System.Windows.Forms.BindingSource dRateiosBindingSource;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.BindingSource chBlocosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCh;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit35;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit50;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.SimpleButton BtImplantacao;
        private VirBotaoNeon.cBotaoImpNeon cBotaoImpNeon1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraGrid.Columns.GridColumn colRAD_CTL;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditCTL;
        private DevExpress.XtraRichEdit.RichEditControl richEditControl1;
    }
}
