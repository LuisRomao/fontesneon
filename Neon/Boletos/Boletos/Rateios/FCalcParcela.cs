using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Boletos.Rateios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FCalcParcela : CompontesBasicos.FormBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public FCalcParcela()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// 
        /// </summary>
        public Rateios.cRateio Chamador;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Chamador.Rateio.preparagerar(false);
            
            decimal Total;
            if (((int)radioGroup1.EditValue) == 0)
                Total = calcEditEntrada.Value / Chamador.dRateios.ReferenciaMaxima.FracaoCorrigida;
            else
                Total = calcEditEntrada.Value / Chamador.dRateios.ReferenciaMinima.FracaoCorrigida;
            calcEditTotPar.Value = Total;
            TotRat.Value = Total * NumeroParcelas.Value;
            oksimpleButton.Enabled = true;
        }

        private void calcEditEntrada_EditValueChanged(object sender, EventArgs e)
        {
            oksimpleButton.Enabled = false;
        }

        private void FCalcParcela_Shown(object sender, EventArgs e)
        {
            TotRat.EditValue = null;
            calcEditTotPar.EditValue = null;
        }
    }
}

