namespace Boletos.Acordo
{
    partial class SubImpNov
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrH2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrH1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovValor = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovVencimento = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovNumero = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovParcela = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.QuadroHonorarios = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroNovosBoletos = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.NovosbindingSource = new System.Windows.Forms.BindingSource(this.components);
            
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.NovosbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrH2,
            this.xrH1,
            this.xrLabel1,
            this.labNovValor,
            this.labNovVencimento,
            this.labNovNumero,
            this.labNovParcela});
            this.Detail.HeightF = 11F;
            this.Detail.KeepTogether = true;
            this.Detail.KeepTogetherWithDetailReports = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrH2
            // 
            this.xrH2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrH2.CanGrow = false;
            this.xrH2.CanShrink = true;
            this.xrH2.Font = new System.Drawing.Font("Tahoma", 7F);
            this.xrH2.LocationFloat = new DevExpress.Utils.PointFloat(367F, 0F);
            this.xrH2.Multiline = true;
            this.xrH2.Name = "xrH2";
            this.xrH2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrH2.SizeF = new System.Drawing.SizeF(52F, 11F);
            this.xrH2.StylePriority.UseBorders = false;
            this.xrH2.Text = "Pago";
            this.xrH2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrH1
            // 
            this.xrH1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrH1.CanGrow = false;
            this.xrH1.CanShrink = true;
            this.xrH1.Font = new System.Drawing.Font("Tahoma", 7F);
            this.xrH1.LocationFloat = new DevExpress.Utils.PointFloat(292F, 0F);
            this.xrH1.Multiline = true;
            this.xrH1.Name = "xrH1";
            this.xrH1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrH1.SizeF = new System.Drawing.SizeF(56F, 10F);
            this.xrH1.StylePriority.UseBorders = false;
            this.xrH1.Text = "xrH1";
            this.xrH1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.CanGrow = false;
            this.xrLabel1.CanShrink = true;
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLPagamento", "{0:dd/MM/yyyy}")});
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 7F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(217F, 0F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(59F, 10F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labNovValor
            // 
            this.labNovValor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovValor.CanGrow = false;
            this.labNovValor.CanShrink = true;
            this.labNovValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:n2}")});
            this.labNovValor.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovValor.LocationFloat = new DevExpress.Utils.PointFloat(158F, 0F);
            this.labNovValor.Multiline = true;
            this.labNovValor.Name = "labNovValor";
            this.labNovValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labNovValor.SizeF = new System.Drawing.SizeF(56F, 10F);
            this.labNovValor.StylePriority.UseBorders = false;
            this.labNovValor.Text = "R$ 999.999.99";
            this.labNovValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labNovVencimento
            // 
            this.labNovVencimento.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovVencimento.CanGrow = false;
            this.labNovVencimento.CanShrink = true;
            this.labNovVencimento.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yyyy}")});
            this.labNovVencimento.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovVencimento.LocationFloat = new DevExpress.Utils.PointFloat(98F, 0F);
            this.labNovVencimento.Multiline = true;
            this.labNovVencimento.Name = "labNovVencimento";
            this.labNovVencimento.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labNovVencimento.SizeF = new System.Drawing.SizeF(59F, 10F);
            this.labNovVencimento.StylePriority.UseBorders = false;
            this.labNovVencimento.Text = "R$ 999.999.99";
            this.labNovVencimento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labNovNumero
            // 
            this.labNovNumero.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovNumero.CanGrow = false;
            this.labNovNumero.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.labNovNumero.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovNumero.LocationFloat = new DevExpress.Utils.PointFloat(42F, 0F);
            this.labNovNumero.Multiline = true;
            this.labNovNumero.Name = "labNovNumero";
            this.labNovNumero.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labNovNumero.SizeF = new System.Drawing.SizeF(55F, 10F);
            this.labNovNumero.StylePriority.UseBorders = false;
            this.labNovNumero.Text = "PROPRIET�RIO";
            // 
            // labNovParcela
            // 
            this.labNovParcela.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovParcela.CanGrow = false;
            this.labNovParcela.CanShrink = true;
            this.labNovParcela.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovParcela.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.labNovParcela.Multiline = true;
            this.labNovParcela.Name = "labNovParcela";
            this.labNovParcela.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labNovParcela.SizeF = new System.Drawing.SizeF(37F, 10F);
            this.labNovParcela.StylePriority.UseBorders = false;
            this.labNovParcela.Text = "CONDOM�NIO";
            this.labNovParcela.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroHonorarios,
            this.QuadroNovosBoletos});
            this.PageHeader.HeightF = 27F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QuadroHonorarios
            // 
            this.QuadroHonorarios.BorderColor = System.Drawing.Color.Silver;
            this.QuadroHonorarios.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.QuadroHonorarios.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrPanel2});
            this.QuadroHonorarios.LocationFloat = new DevExpress.Utils.PointFloat(292F, 0F);
            this.QuadroHonorarios.Name = "QuadroHonorarios";
            this.QuadroHonorarios.SizeF = new System.Drawing.SizeF(128F, 27F);
            this.QuadroHonorarios.StylePriority.UseBorders = false;
            
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(124F, 15F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.Text = "Honor�rios";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel10.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel10_Draw);
            // 
            // xrPanel2
            // 
            this.xrPanel2.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5});
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 15F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(124F, 11F);
            this.xrPanel2.StylePriority.UseBorders = false;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.ForeColor = System.Drawing.Color.Black;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(19F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(69F, 10F);
            this.xrLabel5.Text = "Valor";
            // 
            // QuadroNovosBoletos
            // 
            this.QuadroNovosBoletos.BorderColor = System.Drawing.Color.Silver;
            this.QuadroNovosBoletos.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.QuadroNovosBoletos.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel4,
            this.xrLabel19});
            this.QuadroNovosBoletos.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.QuadroNovosBoletos.Name = "QuadroNovosBoletos";
            this.QuadroNovosBoletos.SizeF = new System.Drawing.SizeF(279F, 27F);
            this.QuadroNovosBoletos.StylePriority.UseBorders = false;
            // 
            // xrPanel4
            // 
            this.xrPanel4.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel12,
            this.xrLabel3,
            this.xrLabel4,
            this.xrLabel13});
            this.xrPanel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 15F);
            this.xrPanel4.Name = "xrPanel4";
            this.xrPanel4.SizeF = new System.Drawing.SizeF(276F, 11F);
            this.xrPanel4.StylePriority.UseBorders = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Black;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(217F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(51F, 10F);
            this.xrLabel2.Text = "Pagamento";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.ForeColor = System.Drawing.Color.Black;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(37F, 10F);
            this.xrLabel12.Text = "Parcela";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.ForeColor = System.Drawing.Color.Black;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(167F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(30F, 10F);
            this.xrLabel3.Text = "Valor";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.ForeColor = System.Drawing.Color.Black;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(50F, 10F);
            this.xrLabel4.Text = "Vencimento";
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.ForeColor = System.Drawing.Color.Black;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(58F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(37F, 10F);
            this.xrLabel13.Text = "N�mero";
            // 
            // xrLabel19
            // 
            this.xrLabel19.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(276F, 15F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.Text = "Novos Boletos";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel19.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel19_Draw);
            // 
            // NovosbindingSource
            // 
            this.NovosbindingSource.DataSource = typeof(BoletosProc.Acordo.dAcordo.NovosDataTable);
            
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubImpNov
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            
            this.bottomMarginBand1});
            this.DataSource = this.NovosbindingSource;
            this.SnappingMode = DevExpress.XtraReports.UI.SnappingMode.None;
            this.Version = "10.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SubImpNov_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.NovosbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource NovosbindingSource;
        private DevExpress.XtraReports.UI.XRLabel labNovParcela;
        private DevExpress.XtraReports.UI.XRPanel QuadroNovosBoletos;
        private DevExpress.XtraReports.UI.XRPanel xrPanel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel labNovValor;
        private DevExpress.XtraReports.UI.XRLabel labNovVencimento;
        private DevExpress.XtraReports.UI.XRLabel labNovNumero;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRLabel xrH2;
        private DevExpress.XtraReports.UI.XRLabel xrH1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRPanel QuadroHonorarios;
        
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
    }
}
