using System;
using AbstratosNeon;
using BoletosProc;

namespace Boletos
{
    /// <summary>
    /// Relatorio de acordos
    /// </summary>
    public partial class ImpAcordo : dllImpresso.ImpLogoCond
    {
        private bool Detalhado;


        private void Iniciar()
        {
            subImpOrig2.OriginaisbindingSource.DataSource = aCOrdosOriginaisBindingSource;
            subImpNov1.NovosbindingSource.DataSource = aCOrdosNovosBindingSource;                        
            if (Detalhado)
            {
                Landscape = true;
                xrLine1.WidthF = 2600;
                xrTitulo.Text += " (Detalhado)";
                Boletos.Acordo.SubImpNovDet SubDet = new Boletos.Acordo.SubImpNovDet();
                xrSubreport2.ReportSource = SubDet;
                SubDet.NovosbindingSource.DataSource = aCOrdosNovosBindingSource;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ImpAcordo()
        {
            InitializeComponent();
            Detalhado = false;
            Iniciar();            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Detalhado"></param>
        public ImpAcordo(bool _Detalhado)
        {
            InitializeComponent();
            Detalhado = _Detalhado;
            Iniciar();            
        }

        

        /// <summary>
        /// Carrega os dados de um condominio inteiro
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        /// <param name="ComCancelados"></param>
        public void CarregaDados(int CON,DateTime DI,DateTime DF,bool ComCancelados)
        {
            BoletosProc.Acordo.dAcordo dAcordo = new BoletosProc.Acordo.dAcordo();
            if (ComCancelados)
            {
                dAcordo.ACOrdosTableAdapter.FillByCON(dAcordo.ACOrdos, CON, DF, DI);
                dAcordo.NovosTableAdapter.FillByCON(dAcordo.Novos, CON, DF, DI);                
                dAcordo.OriginaisTableAdapter.FillByCON(dAcordo.Originais, CON, DF, DI);
            }
            else
            {
                dAcordo.ACOrdosTableAdapter.FillByCONsemCancelado(dAcordo.ACOrdos, CON, DF, DI);
                dAcordo.NovosTableAdapter.FillByCONSemCancelados(dAcordo.Novos, CON, DF, DI);                
                dAcordo.OriginaisTableAdapter.FillByCONSemCancelado(dAcordo.Originais, CON, DF, DI);
            }
            if (Detalhado)
            {
                dAcordo.BOletoDetalheTableAdapter.ClearBeforeFill = false;
                foreach (BoletosProc.Acordo.dAcordo.NovosRow rowBOL in dAcordo.Novos)
                    dAcordo.BOletoDetalheTableAdapter.Fill(dAcordo.BOletoDetalhe, rowBOL.BOL);
            };
            ACOrdosbindingSource.DataSource = dAcordo;
        }

        /// <summary>
        /// Carrega dados
        /// </summary>
        /// <param name="ACO"></param>
        public void CarregaDados(int ACO)
        {
            BoletosProc.Acordo.dAcordo dAcordo = new BoletosProc.Acordo.dAcordo();
            
            dAcordo.ACOrdosTableAdapter.FillByACO(dAcordo.ACOrdos, ACO);
            dAcordo.NovosTableAdapter.FillByACO(dAcordo.Novos, ACO);
            dAcordo.OriginaisTableAdapter.FillByACO(dAcordo.Originais, ACO);
            if (Detalhado)
            {
                dAcordo.BOletoDetalheTableAdapter.ClearBeforeFill = false;
                foreach (BoletosProc.Acordo.dAcordo.NovosRow rowBOL in dAcordo.Novos)
                    dAcordo.BOletoDetalheTableAdapter.Fill(dAcordo.BOletoDetalhe, rowBOL.BOL);
            };
            ACOrdosbindingSource.DataSource = dAcordo;
        }

        private BoletosProc.Acordo.dAcordo.ACOrdosRow LinhaMae {
            get {
                System.Data.DataRowView DRV1 = (System.Data.DataRowView)GetCurrentRow();
                if (DRV1 == null)
                    return null;
                return (BoletosProc.Acordo.dAcordo.ACOrdosRow)DRV1.Row;                        
            }
        }

        /// <summary>
        /// Ocultar advogado
        /// </summary>
        public bool OcultarAdvogado;

        private bool primeirogrupo = false;

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (LinhaMae == null)
                return;
            ACOrdosbindingSource.Position = ACOrdosbindingSource.Find("ACO",LinhaMae.ACO);
            xrjuridico.Visible = (!LinhaMae.IsACOJuridicoNull() && LinhaMae.ACOJuridico);
            xrjudicial.Visible = (!LinhaMae.IsACOJudicialNull() && LinhaMae.ACOJudicial);

            

            subImpNov1.QuadroHonorarios.Visible = xrjuridico.Visible;

            if (OcultarAdvogado || LinhaMae.IsACOADV_FRNNull())
            {
                xradvogado.Visible = false;
            }
            else {
                string NomeADV = LinhaMae.ACOADV_FRN.ToString();
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select FRNNome from fornecedores where FRN = @P1", out NomeADV, LinhaMae.ACOADV_FRN);
                xradvogado.Text = "Adv.: "+NomeADV;
                xradvogado.Visible = true;
            };
            string strI = LinhaMae.IsACODATAINull() ? "-" : string.Format("{0:dd/MM/yy}",LinhaMae.ACODATAI); 
            string strF = LinhaMae.IsACOTerminoNull() ? "-" : string.Format("{0:dd/MM/yy}", LinhaMae.ACOTermino);
            switch ((BoletosProc.Acordo.StatusACO)LinhaMae.ACOStatus)
            {
                case BoletosProc.Acordo.StatusACO.NaoGerado:
                    xrLabel4.Text = "N�o Gerado";
                    break;
                case BoletosProc.Acordo.StatusACO.Ativo:
                    xrLabel4.Text = string.Format("Em andamento In�cio: {0}",strI);
                    break;
                case BoletosProc.Acordo.StatusACO.Terminado:
                    xrLabel4.Text = string.Format("PAGO Vig�ncia: {0} a {1}", strI,strF);
                    break;
                case BoletosProc.Acordo.StatusACO.Cancelado:
                    xrLabel4.Text = string.Format("Cancelado Vig�ncia: {0} a {1}", strI, strF);
                    break;
                case BoletosProc.Acordo.StatusACO.Divide:
                    xrLabel4.Text = "Divis�o";
                    break;
                default:
                    break;
            }
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (LinhaMae == null)
            {
                xrTituloUnidade.Text = "";
                return;
            }
            ABS_Apartamento Ap = ABS_Apartamento.GetApartamento(LinhaMae.ACO_APT);

            xrTituloUnidade.Text = String.Format("Bloco: {0} Apartamento: {1}", Ap.BLOCodigo, Ap.APTNumero);
            xrLine1.Visible = !primeirogrupo;
            primeirogrupo = false;
        }

        private void ImpAcordo_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            primeirogrupo = true;
        }
    }
}

