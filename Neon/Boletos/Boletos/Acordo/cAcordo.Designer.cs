namespace Boletos.Acordo
{
    partial class cAcordo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cAcordo));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.aCOrdosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dAcordo = new BoletosProc.Acordo.dAcordo();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colACO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACOStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EditorStatus = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colACODATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACODATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACOJudicial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACOJuridico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACOADV_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpADV = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.fORNECEDORESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colACOTermino = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chDetalhado = new DevExpress.XtraEditors.CheckEdit();
            this.cBotaoImpAcordo = new dllImpresso.Botoes.cBotaoImpBol();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.BotTrocaADV = new DevExpress.XtraEditors.SimpleButton();
            this.BCancela = new DevExpress.XtraEditors.SimpleButton();
            this.BNAcordo = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridOriginais = new DevExpress.XtraGrid.GridControl();
            this.aCOrdosOriginaisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.editorPI1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.colBOLTipoCRAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPrevisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.aCOrdosNovosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLEmissao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPrevisto1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.bOLetosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAcordo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpADV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORNECEDORESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chDetalhado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOriginais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosOriginaisBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editorPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosNovosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLetosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.aCOrdosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 20);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.EditorStatus,
            this.LookUpADV});
            this.gridControl1.Size = new System.Drawing.Size(871, 168);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // aCOrdosBindingSource
            // 
            this.aCOrdosBindingSource.DataMember = "ACOrdos";
            this.aCOrdosBindingSource.DataSource = this.dAcordo;
            // 
            // dAcordo
            // 
            this.dAcordo.DataSetName = "dAcordo";
            this.dAcordo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colACO,
            this.colACOStatus,
            this.colACODATAI,
            this.colACODATAA,
            this.colACOJudicial,
            this.colACOJuridico,
            this.colACOADV_FRN,
            this.colACOTermino});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colACODATAI, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // colACO
            // 
            this.colACO.Caption = "Acordo";
            this.colACO.FieldName = "ACO";
            this.colACO.Name = "colACO";
            this.colACO.OptionsColumn.ReadOnly = true;
            this.colACO.Visible = true;
            this.colACO.VisibleIndex = 0;
            this.colACO.Width = 53;
            // 
            // colACOStatus
            // 
            this.colACOStatus.Caption = "Status";
            this.colACOStatus.ColumnEdit = this.EditorStatus;
            this.colACOStatus.FieldName = "ACOStatus";
            this.colACOStatus.Name = "colACOStatus";
            this.colACOStatus.OptionsColumn.ReadOnly = true;
            this.colACOStatus.Visible = true;
            this.colACOStatus.VisibleIndex = 3;
            // 
            // EditorStatus
            // 
            this.EditorStatus.AutoHeight = false;
            this.EditorStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EditorStatus.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Encerrado", 2, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Aberto", 1, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cancelado", 3, -1)});
            this.EditorStatus.Name = "EditorStatus";
            // 
            // colACODATAI
            // 
            this.colACODATAI.Caption = "Cria��o";
            this.colACODATAI.FieldName = "ACODATAI";
            this.colACODATAI.Name = "colACODATAI";
            this.colACODATAI.OptionsColumn.ReadOnly = true;
            this.colACODATAI.Visible = true;
            this.colACODATAI.VisibleIndex = 1;
            this.colACODATAI.Width = 80;
            // 
            // colACODATAA
            // 
            this.colACODATAA.Caption = "Altera��o";
            this.colACODATAA.FieldName = "ACODATAA";
            this.colACODATAA.Name = "colACODATAA";
            this.colACODATAA.OptionsColumn.ReadOnly = true;
            // 
            // colACOJudicial
            // 
            this.colACOJudicial.Caption = "Judicial";
            this.colACOJudicial.FieldName = "ACOJudicial";
            this.colACOJudicial.Name = "colACOJudicial";
            this.colACOJudicial.OptionsColumn.ReadOnly = true;
            this.colACOJudicial.Visible = true;
            this.colACOJudicial.VisibleIndex = 5;
            this.colACOJudicial.Width = 52;
            // 
            // colACOJuridico
            // 
            this.colACOJuridico.Caption = "Jur�dico";
            this.colACOJuridico.FieldName = "ACOJuridico";
            this.colACOJuridico.Name = "colACOJuridico";
            this.colACOJuridico.OptionsColumn.ReadOnly = true;
            this.colACOJuridico.Visible = true;
            this.colACOJuridico.VisibleIndex = 4;
            this.colACOJuridico.Width = 53;
            // 
            // colACOADV_FRN
            // 
            this.colACOADV_FRN.Caption = "Advogado";
            this.colACOADV_FRN.ColumnEdit = this.LookUpADV;
            this.colACOADV_FRN.FieldName = "ACOADV_FRN";
            this.colACOADV_FRN.Name = "colACOADV_FRN";
            this.colACOADV_FRN.OptionsColumn.ReadOnly = true;
            this.colACOADV_FRN.Visible = true;
            this.colACOADV_FRN.VisibleIndex = 6;
            this.colACOADV_FRN.Width = 157;
            // 
            // LookUpADV
            // 
            this.LookUpADV.AutoHeight = false;
            this.LookUpADV.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpADV.DataSource = this.fORNECEDORESBindingSource;
            this.LookUpADV.DisplayMember = "FRNNome";
            this.LookUpADV.Name = "LookUpADV";
            this.LookUpADV.NullText = "";
            this.LookUpADV.ValueMember = "FRN";
            // 
            // fORNECEDORESBindingSource
            // 
            this.fORNECEDORESBindingSource.DataMember = "FRNLookup";
            this.fORNECEDORESBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // colACOTermino
            // 
            this.colACOTermino.Caption = "T�rmino";
            this.colACOTermino.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colACOTermino.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colACOTermino.FieldName = "ACOTermino";
            this.colACOTermino.Name = "colACOTermino";
            this.colACOTermino.Visible = true;
            this.colACOTermino.VisibleIndex = 2;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Controls.Add(this.panelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 35);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1327, 190);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Acordos da Unidade";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chDetalhado);
            this.panelControl1.Controls.Add(this.cBotaoImpAcordo);
            this.panelControl1.Controls.Add(this.lookUpEdit1);
            this.panelControl1.Controls.Add(this.BotTrocaADV);
            this.panelControl1.Controls.Add(this.BCancela);
            this.panelControl1.Controls.Add(this.BNAcordo);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(873, 20);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(452, 168);
            this.panelControl1.TabIndex = 1;
            // 
            // chDetalhado
            // 
            this.chDetalhado.Location = new System.Drawing.Point(160, 64);
            this.chDetalhado.MenuManager = this.BarManager_F;
            this.chDetalhado.Name = "chDetalhado";
            this.chDetalhado.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chDetalhado.Properties.Appearance.Options.UseFont = true;
            this.chDetalhado.Properties.Caption = "Detalhado";
            this.chDetalhado.Size = new System.Drawing.Size(85, 19);
            this.chDetalhado.TabIndex = 29;
            // 
            // cBotaoImpAcordo
            // 
            this.cBotaoImpAcordo.ItemComprovante = false;
            this.cBotaoImpAcordo.Location = new System.Drawing.Point(7, 63);
            this.cBotaoImpAcordo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpAcordo.Name = "cBotaoImpAcordo";
            this.cBotaoImpAcordo.Size = new System.Drawing.Size(147, 23);
            this.cBotaoImpAcordo.TabIndex = 28;
            this.cBotaoImpAcordo.Titulo = "Imprimir Acordo";
            this.cBotaoImpAcordo.clicado += new System.EventHandler(this.cBotaoImpAcordo_clicado);
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Location = new System.Drawing.Point(7, 128);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Name5")});
            this.lookUpEdit1.Properties.DataSource = this.fORNECEDORESBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "FRNNome";
            this.lookUpEdit1.Properties.ShowHeader = false;
            this.lookUpEdit1.Properties.ValueMember = "FRN";
            this.lookUpEdit1.Properties.PopupFilter += new DevExpress.XtraEditors.Controls.PopupFilterEventHandler(this.lookUpEdit1_Properties_PopupFilter);
            this.lookUpEdit1.Size = new System.Drawing.Size(238, 20);
            this.lookUpEdit1.TabIndex = 3;
            this.lookUpEdit1.Visible = false;
            // 
            // BotTrocaADV
            // 
            this.BotTrocaADV.Location = new System.Drawing.Point(7, 99);
            this.BotTrocaADV.Name = "BotTrocaADV";
            this.BotTrocaADV.Size = new System.Drawing.Size(238, 23);
            this.BotTrocaADV.TabIndex = 2;
            this.BotTrocaADV.Text = "Troca Advogado";
            this.BotTrocaADV.Visible = false;
            this.BotTrocaADV.Click += new System.EventHandler(this.simpleButton1_Click_2);
            // 
            // BCancela
            // 
            this.BCancela.Location = new System.Drawing.Point(6, 34);
            this.BCancela.Name = "BCancela";
            this.BCancela.Size = new System.Drawing.Size(239, 23);
            this.BCancela.TabIndex = 1;
            this.BCancela.Text = "Cancelar Acordo";
            this.BCancela.Click += new System.EventHandler(this.BCancela_Click);
            // 
            // BNAcordo
            // 
            this.BNAcordo.Location = new System.Drawing.Point(6, 5);
            this.BNAcordo.Name = "BNAcordo";
            this.BNAcordo.Size = new System.Drawing.Size(239, 23);
            this.BNAcordo.TabIndex = 0;
            this.BNAcordo.Text = "Novo Acordo";
            this.BNAcordo.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(17, 114);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(264, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 30;
            this.progressBar1.Visible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.progressBar1);
            this.groupControl2.Controls.Add(this.label1);
            this.groupControl2.Controls.Add(this.gridOriginais);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 225);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(950, 557);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Boletos Originais";
            // 
            // gridOriginais
            // 
            this.gridOriginais.DataSource = this.aCOrdosOriginaisBindingSource;
            this.gridOriginais.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridOriginais.Location = new System.Drawing.Point(2, 20);
            this.gridOriginais.MainView = this.gridView2;
            this.gridOriginais.Name = "gridOriginais";
            this.gridOriginais.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.editorPI1,
            this.repositoryItemButtonEdit1});
            this.gridOriginais.Size = new System.Drawing.Size(946, 535);
            this.gridOriginais.TabIndex = 0;
            this.gridOriginais.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // aCOrdosOriginaisBindingSource
            // 
            this.aCOrdosOriginaisBindingSource.DataMember = "ACOrdos_Originais";
            this.aCOrdosOriginaisBindingSource.DataSource = this.aCOrdosBindingSource;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL,
            this.colBOLProprietario,
            this.colBOLTipoCRAI,
            this.colBOLEmissao,
            this.colBOLVencto,
            this.colBOLPagamento,
            this.colBOLValorPrevisto,
            this.gridColumn1});
            this.gridView2.GridControl = this.gridOriginais;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView2_RowStyle);
            // 
            // colBOL
            // 
            this.colBOL.Caption = "Boleto";
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.ReadOnly = true;
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 0;
            // 
            // colBOLProprietario
            // 
            this.colBOLProprietario.Caption = "Destinat�rio";
            this.colBOLProprietario.ColumnEdit = this.editorPI1;
            this.colBOLProprietario.FieldName = "BOLProprietario";
            this.colBOLProprietario.Name = "colBOLProprietario";
            this.colBOLProprietario.OptionsColumn.ReadOnly = true;
            this.colBOLProprietario.Visible = true;
            this.colBOLProprietario.VisibleIndex = 1;
            this.colBOLProprietario.Width = 51;
            // 
            // editorPI1
            // 
            this.editorPI1.AutoHeight = false;
            this.editorPI1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.editorPI1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.editorPI1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Propriet�rio", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 1)});
            this.editorPI1.Name = "editorPI1";
            this.editorPI1.SmallImages = this.imagensPI1;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // colBOLTipoCRAI
            // 
            this.colBOLTipoCRAI.Caption = "Tipo";
            this.colBOLTipoCRAI.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI.Name = "colBOLTipoCRAI";
            this.colBOLTipoCRAI.OptionsColumn.ReadOnly = true;
            this.colBOLTipoCRAI.Visible = true;
            this.colBOLTipoCRAI.VisibleIndex = 2;
            this.colBOLTipoCRAI.Width = 45;
            // 
            // colBOLEmissao
            // 
            this.colBOLEmissao.Caption = "Emiss�o";
            this.colBOLEmissao.FieldName = "BOLEmissao";
            this.colBOLEmissao.Name = "colBOLEmissao";
            this.colBOLEmissao.OptionsColumn.ReadOnly = true;
            this.colBOLEmissao.Visible = true;
            this.colBOLEmissao.VisibleIndex = 3;
            this.colBOLEmissao.Width = 61;
            // 
            // colBOLVencto
            // 
            this.colBOLVencto.Caption = "Vencto";
            this.colBOLVencto.FieldName = "BOLVencto";
            this.colBOLVencto.Name = "colBOLVencto";
            this.colBOLVencto.OptionsColumn.ReadOnly = true;
            this.colBOLVencto.Visible = true;
            this.colBOLVencto.VisibleIndex = 4;
            this.colBOLVencto.Width = 65;
            // 
            // colBOLPagamento
            // 
            this.colBOLPagamento.Caption = "Pagamento";
            this.colBOLPagamento.FieldName = "BOLPagamento";
            this.colBOLPagamento.Name = "colBOLPagamento";
            this.colBOLPagamento.OptionsColumn.ReadOnly = true;
            // 
            // colBOLValorPrevisto
            // 
            this.colBOLValorPrevisto.Caption = "Valor";
            this.colBOLValorPrevisto.DisplayFormat.FormatString = "n2";
            this.colBOLValorPrevisto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBOLValorPrevisto.FieldName = "BOLValorPrevisto";
            this.colBOLValorPrevisto.Name = "colBOLValorPrevisto";
            this.colBOLValorPrevisto.OptionsColumn.ReadOnly = true;
            this.colBOLValorPrevisto.Visible = true;
            this.colBOLValorPrevisto.VisibleIndex = 5;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 6;
            this.gridColumn1.Width = 26;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gridControl3);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupControl3.Location = new System.Drawing.Point(950, 225);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(377, 557);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Novos Boletos";
            // 
            // gridControl3
            // 
            this.gridControl3.DataMember = null;
            this.gridControl3.DataSource = this.aCOrdosNovosBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(2, 20);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit2});
            this.gridControl3.Size = new System.Drawing.Size(373, 535);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // aCOrdosNovosBindingSource
            // 
            this.aCOrdosNovosBindingSource.DataMember = "ACOrdos_Novos";
            this.aCOrdosNovosBindingSource.DataSource = this.aCOrdosBindingSource;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL1,
            this.colBOLEmissao1,
            this.colBOLVencto1,
            this.colBOLPagamento1,
            this.colBOLValorPrevisto1,
            this.gridColumn2});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView3_RowStyle);
            // 
            // colBOL1
            // 
            this.colBOL1.Caption = "Boleto";
            this.colBOL1.FieldName = "BOL";
            this.colBOL1.Name = "colBOL1";
            this.colBOL1.OptionsColumn.ReadOnly = true;
            this.colBOL1.Visible = true;
            this.colBOL1.VisibleIndex = 0;
            this.colBOL1.Width = 76;
            // 
            // colBOLEmissao1
            // 
            this.colBOLEmissao1.Caption = "Emiss�o";
            this.colBOLEmissao1.FieldName = "BOLEmissao";
            this.colBOLEmissao1.Name = "colBOLEmissao1";
            this.colBOLEmissao1.OptionsColumn.ReadOnly = true;
            this.colBOLEmissao1.Width = 69;
            // 
            // colBOLVencto1
            // 
            this.colBOLVencto1.Caption = "Vencto";
            this.colBOLVencto1.FieldName = "BOLVencto";
            this.colBOLVencto1.Name = "colBOLVencto1";
            this.colBOLVencto1.OptionsColumn.ReadOnly = true;
            this.colBOLVencto1.Visible = true;
            this.colBOLVencto1.VisibleIndex = 1;
            this.colBOLVencto1.Width = 64;
            // 
            // colBOLPagamento1
            // 
            this.colBOLPagamento1.Caption = "Pagamento";
            this.colBOLPagamento1.FieldName = "BOLPagamento";
            this.colBOLPagamento1.Name = "colBOLPagamento1";
            this.colBOLPagamento1.OptionsColumn.ReadOnly = true;
            this.colBOLPagamento1.Visible = true;
            this.colBOLPagamento1.VisibleIndex = 3;
            this.colBOLPagamento1.Width = 79;
            // 
            // colBOLValorPrevisto1
            // 
            this.colBOLValorPrevisto1.Caption = "Valor";
            this.colBOLValorPrevisto1.DisplayFormat.FormatString = "n2";
            this.colBOLValorPrevisto1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBOLValorPrevisto1.FieldName = "BOLValorPrevisto";
            this.colBOLValorPrevisto1.Name = "colBOLValorPrevisto1";
            this.colBOLValorPrevisto1.OptionsColumn.ReadOnly = true;
            this.colBOLValorPrevisto1.Visible = true;
            this.colBOLValorPrevisto1.VisibleIndex = 2;
            this.colBOLValorPrevisto1.Width = 80;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 4;
            this.gridColumn2.Width = 26;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // bOLetosBindingSource
            // 
            this.bOLetosBindingSource.DataMember = "BOLetos";
            this.bOLetosBindingSource.DataSource = this.dAcordo;
            // 
            // cAcordo
            // 
            this.BindingSourcePrincipal = this.aCOrdosBindingSource;
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cAcordo";
            this.Size = new System.Drawing.Size(1327, 807);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControl3, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAcordo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpADV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORNECEDORESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chDetalhado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOriginais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosOriginaisBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editorPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosNovosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLetosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl gridOriginais;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private BoletosProc.Acordo.dAcordo dAcordo;
        private DevExpress.XtraGrid.Columns.GridColumn colACO;
        private DevExpress.XtraGrid.Columns.GridColumn colACOStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colACODATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colACODATAA;
        private System.Windows.Forms.BindingSource aCOrdosOriginaisBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto;
        private System.Windows.Forms.BindingSource aCOrdosNovosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLEmissao1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento1;
        private DevExpress.XtraEditors.SimpleButton BNAcordo;
        private System.Windows.Forms.BindingSource bOLetosBindingSource;
        private System.Windows.Forms.BindingSource aCOrdosBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox EditorStatus;
        private DevExpress.XtraEditors.SimpleButton BCancela;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox editorPI1;
        private DevExpress.XtraGrid.Columns.GridColumn colACOJudicial;
        private DevExpress.XtraGrid.Columns.GridColumn colACOJuridico;
        private DevExpress.XtraGrid.Columns.GridColumn colACOADV_FRN;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpADV;
        private System.Windows.Forms.BindingSource fORNECEDORESBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.SimpleButton BotTrocaADV;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpAcordo;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPrevisto;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPrevisto1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn colACOTermino;
        private DevExpress.XtraEditors.CheckEdit chDetalhado;
    }
}
