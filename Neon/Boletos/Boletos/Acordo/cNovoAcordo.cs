using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AbstratosNeon;
using BoletosProc.Acordo;
using VirEnumeracoesNeon;

namespace Boletos.Acordo
{
    /// <summary>
    /// Novo Acordo
    /// </summary>
    public partial class cNovoAcordo : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// Acordo
        /// </summary>
        public Acordo Acordo;

        /// <summary>
        /// Construtor padr�o para o Desiner, N�O USAR
        /// </summary>
        public cNovoAcordo()
        {
            InitializeComponent();                        
        }

        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup1;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Acordo"></param>
        public cNovoAcordo(Acordo Acordo)
        {
            nivel = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("ACORDO");
            InitializeComponent();
            this.Acordo = Acordo;
            imageComboBoxEdit1.EditValue = Acordo.Proprietario;
            checkEdit1.Checked = Acordo.Judicial;
            //gridOriginais.DataSource = Acordo.dAcordo.BOLetos;
            gridControl1.DataSource = Acordo.dAcordo.GeraNovos;
            bOLetosDataTableBindingSource.DataSource = Acordo.dAcordo.BOLetos;
            //fORNECEDORESBindingSource.DataSource = Framework.datasets.dFornecedores.GetdFornecedoresST("ADV");
            dFornecedoresLookup1 = new CadastrosProc.Fornecedores.dFornecedoresLookup();
            fORNECEDORESBindingSource.DataSource = dFornecedoresLookup1;
            dFornecedoresLookup1.FRNLookupTableAdapter.Fill(dFornecedoresLookup1.FRNLookup);
            TotalPelosOriginais();
            calcHonorarios.EditValue = 0.0m;
            //spinParcelas.Enabled = false;
            if (nivel < 3)
            {
                colHonorarios.Visible = false;
                colTotal.Visible = false;
                gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None;
                if (nivel < 2)
                {                    
                    spinTotal.Enabled = false;                    
                    gridView1.OptionsBehavior.Editable = false;
                }
            };
            ABS_Apartamento Apartamento = ABS_Apartamento.GetApartamento(Acordo.APT);
                
            LabelDestinatario.Visible = imageComboBoxEdit1.Visible = Apartamento.Alugado;
            int FRN = -1;
            if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select APTJur_FRN from AParTamentos where APT = @P1", out FRN, Apartamento.APT))
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONEscrit_FRN from condominios where CON = @P1", out FRN, Apartamento.CON);
            if(FRN > 0)
            {
                Acordo.FRN = FRN;
                lookUpEdit1.EditValue = FRN;
            }
            
        }


        /// <summary>
        /// Update
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            foreach (BoletosProc.Acordo.dAcordo.GeraNovosRow rowG in Acordo.dAcordo.GeraNovos)
            {
                if (rowG.Honorarios >= rowG.Valor)
                    return false;
                if ((rowG.Honorarios < 0) || (rowG.Valor <= 0))
                    return false;
            };
            if (spinTotal.ErrorText != "")
                if (MessageBox.Show("Confirma a gera��o de acordo com valor inferior ao valor original?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return false;
            bool gerado = Acordo.Gerar(dllImpresso.Botoes.Botao.imprimir);            
            return gerado;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Dividir();            
        }

        private void Dividir() {
            try
            {
                decimal Parcelas = (decimal)spinParcelas.EditValue;
                decimal ParcelasH;
                decimal ValHonorarios = 0;
                if (Acordo.Juridico)
                {
                    ParcelasH = (decimal)spinParacH.EditValue;
                    if(calcHonorarios.EditValue != null)
                        if (calcHonorarios.EditValue is decimal)
                        {
                            ValHonorarios = (decimal)calcHonorarios.EditValue;
                        }
                        else
                        {
                            ValHonorarios = (int)calcHonorarios.EditValue;
                        }
                }
                else
                    ParcelasH = 0;
                Dividir((int)Parcelas, (decimal)spinTotal.EditValue, dateEditINI.DateTime,(int)ParcelasH,ValHonorarios);
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        bool ParcelaImpostas = false;

        private int nivel=1;
        decimal TotalOrig = 0;
        decimal TotalCorrigido = 0;
        decimal TotalHonorarios = 0;
        decimal TotalcomMulta = 0;
        int n = 0;

        private void TotalPelosOriginais() {
            TotalOrig = 0;
            TotalCorrigido = 0;
            TotalHonorarios = 0;
            TotalcomMulta = 0;
            n = 0;
            foreach (dAcordo.BOLetosRow rowBoletos in Acordo.dAcordo.BOLetos)
                if (rowBoletos.Incluir)
                {
                    TotalOrig += rowBoletos.BOLValorPrevisto;
                    TotalCorrigido += rowBoletos.ValorCorrigido;
                    TotalHonorarios += rowBoletos.Honorarios;
                    TotalcomMulta += rowBoletos.SubTotal;
                    rowBoletos.incValOriginal = rowBoletos.BOLValorPrevisto;
                    rowBoletos.incEncargos = rowBoletos.SubTotal - rowBoletos.BOLValorPrevisto;
                    rowBoletos.incTotal = rowBoletos.SubTotal;
                    n++;
                }
                else 
                {
                    rowBoletos.SetincValOriginalNull();
                    rowBoletos.SetincEncargosNull();
                    rowBoletos.SetincTotalNull();
                };
            BlocDividir = true;
            if (nivel < 3)
                spinParcelas.Properties.MaxValue = n;
            if (!ParcelaImpostas)
            {
                
                ParcelaImpostas = false;
                
                spinParcelas.EditValue = Math.Min(3, n);
            }
            /*
            if (nivel < 3)
            {
                spinTotal MinValue = TotalOrig;
                spinTotal.Properties.MaxValue = 1000000;
            };
             */
            decimal honorarios = 0;
            if (chJuridico.Checked)
                honorarios = calcHonorarios.Value;
            spinTotal.EditValue = TotalcomMulta + honorarios;
            spinTotal.ErrorText = "";
            if (dateEditINI.DateTime == DateTime.MinValue)
                dateEditINI.DateTime = DateTime.Today.AddDays(5);
            BlocDividir = false;
            Dividir();
        }

        private void Dividir(int Parcelas,decimal Total,DateTime Datainicial,int ParcelasH, decimal TotalH) { 
            Acordo.dAcordo.GeraNovos.Clear();
            if (ParcelasH > Parcelas)
                ParcelasH = Parcelas;
            if (Total == 0)
                return;
            int minParcelasH = (int)System.Math.Truncate((TotalH * Parcelas / Total) + 1);
            if(minParcelasH > ParcelasH)
                ParcelasH = minParcelasH;
            if (Parcelas > 0)
            {
                decimal ValorParcela = Math.Round(Total / Parcelas, 2);
                decimal ValorParcelaH = (ParcelasH == 0)?0: Math.Round(TotalH / ParcelasH, 2);
                for (int i = 1; i <= Parcelas; i++)
                {
                    decimal Valor = (i != Parcelas) ? ValorParcela : (Total - ValorParcela * (Parcelas - 1));
                    decimal ValorH;
                    if (i < ParcelasH)
                        ValorH = ValorParcelaH;
                    else
                        if (i == ParcelasH)
                            ValorH = (TotalH - ValorParcelaH * (ParcelasH - 1));
                        else
                            ValorH = 0;
                    dAcordo.GeraNovosRow NovaLinha = Acordo.dAcordo.GeraNovos.NewGeraNovosRow();
                    NovaLinha.Data = Datainicial.AddMonths(i - 1);
                    NovaLinha.Valor = Valor;
                    NovaLinha.Honorarios = ValorH;
                    Acordo.dAcordo.GeraNovos.AddGeraNovosRow(NovaLinha);

                }
            }
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            TotalPelosOriginais();
        }

        private void spinParcelas_Properties_EditValueChanged(object sender, EventArgs e)
        {
            ParcelaImpostas = true;
        }

        private void repositoryItemCheckEdit1_CheckedChanged(object sender, EventArgs e)
        {
            gridView2.CloseEditor();
            gridView2.UpdateCurrentRow();
        }

        private bool BlocDividir = false;

        private void spinTotal_EditValueChanged(object sender, EventArgs e)
        {
            
            if (!BlocDividir)
            {
                ValidaMinimo();
                Dividir();
            }
        }

        private void spinParcelas_EditValueChanged(object sender, EventArgs e)
        {
            if (!BlocDividir)
               Dividir();
        }

        private void dateEditINI_EditValueChanged(object sender, EventArgs e)
        {
            if (!BlocDividir)
               Dividir();
        }

        private void ValidaMinimo() 
        {
            decimal honorarios = 0;
            if (chJuridico.Checked)
                honorarios = calcHonorarios.Value;
            if (!Acordo.dAcordo.DistribuiEncargos(spinTotal.Value - honorarios, false))
            {
                spinTotal.ErrorText = "Valor acordado � menor do que valor original\r\nA quita��o dos boletos ir� acarretar 'descontos' no balancete";
                //e.Cancel = true;
            }
            else
                spinTotal.ErrorText = "";
        }

        private void spinTotal_Validating(object sender, CancelEventArgs e)
        {

            if ((nivel < 3) && ((decimal)spinTotal.EditValue < TotalOrig))
            {
                spinTotal.EditValue = TotalOrig;
            }
            else
            {
                spinTotal.EditValue = Math.Round((decimal)spinTotal.EditValue, 2);
                ValidaMinimo();                
            }
        }

        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            
            if (gridView1.FocusedColumn == colValor)
            {
                e.Value = Math.Round((decimal)e.Value, 2);
            }
        }

        private void imageComboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {
            Acordo.Proprietario = (bool)imageComboBoxEdit1.EditValue;
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            Acordo.Judicial = checkEdit1.Checked;
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            if (chJuridico.Checked)
                colHonorarios1.Visible = Acordo.Juridico = panelJuridico.Visible = true;
            else
            {
                colHonorarios1.Visible = Acordo.Judicial = Acordo.Juridico = panelJuridico.Visible = false;
                calcHonorarios.Value = 0;
            }
            if (!BlocDividir)
                Dividir();
        }

        private void gridView2_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView2.GetRow(e.RowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return;
            BoletosProc.Acordo.dAcordo.BOLetosRow linha = ((BoletosProc.Acordo.dAcordo.BOLetosRow)DRV.Row);
            
                if (linha.BOLVencto < DateTime.Today.AddDays(-3))
                    e.Appearance.BackColor = Color.FromArgb(254, 200, 200);//vermelho
                else
                    if (linha.BOLVencto < DateTime.Today)
                        e.Appearance.BackColor = Color.Yellow;
                    else
                        e.Appearance.BackColor = Color.Silver;
        }

        private void calcHonorarios_EditValueChanged(object sender, EventArgs e)
        {
            ValidaMinimo();
            if (!BlocDividir)
                Dividir();
        }

        private void spinParacH_EditValueChanged(object sender, EventArgs e)
        {
            if (!BlocDividir)
                Dividir();
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {           
            bool gerado = Acordo.Gerar(cBotaoImpBol1.BotaoClicado);
            if (gerado)
                this.FechaTela(DialogResult.OK);
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (lookUpEdit1.EditValue != null)            
                Acordo.FRN = (int)lookUpEdit1.EditValue;                                           
            else
                Acordo.FRN = 0;
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            BlocDividir = true;
            decimal Total = 0;
            decimal TotalH = 0;
            foreach (dAcordo.GeraNovosRow rowG in Acordo.dAcordo.GeraNovos)
            {
                Total += rowG.Valor;
                TotalH += rowG.Honorarios;
                if (rowG.Valor < rowG.Honorarios)
                    rowG.RowError = "Honor�rio maior do que o valor da parcela";
            };
            spinTotal.Value = Total;
            calcHonorarios.Value = TotalH;
            ValidaMinimo();
            BlocDividir = false;
        }

        private void lookUpEdit1_Properties_PopupFilter(object sender, DevExpress.XtraEditors.Controls.PopupFilterEventArgs e)
        {
            e.Criteria = DevExpress.Data.Filtering.CriteriaOperator.Parse("RAV = ?", (int)RAVPadrao.EscritorioAdvogado);
        }
    }


}

