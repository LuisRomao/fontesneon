namespace Boletos
{
    partial class ImpAcordo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xrTituloUnidade = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.ACOrdosbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aCOrdosOriginaisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aCOrdosNovosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrjuridico = new DevExpress.XtraReports.UI.XRLabel();
            this.xrjudicial = new DevExpress.XtraReports.UI.XRLabel();
            this.xradvogado = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subImpNov1 = new Boletos.Acordo.SubImpNov();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subImpOrig2 = new Boletos.Acordo.SubImpOrig();
            ((System.ComponentModel.ISupportInitialize)(this.ACOrdosbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosOriginaisBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosNovosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subImpNov1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subImpOrig2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrTitulo
            // 
            this.xrTitulo.Text = "Relat�rio de Acordos";
            // 
            // PageHeader
            // 
            this.PageHeader.HeightF = 307F;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xradvogado,
            this.xrjudicial,
            this.xrjuridico,
            this.xrSubreport2,
            this.xrSubreport1,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel4});
            this.Detail.HeightF = 198F;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrTituloUnidade
            // 
            this.xrTituloUnidade.Dpi = 254F;
            this.xrTituloUnidade.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTituloUnidade.LocationFloat = new DevExpress.Utils.PointFloat(21F, 21F);
            this.xrTituloUnidade.Name = "xrTituloUnidade";
            this.xrTituloUnidade.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTituloUnidade.SizeF = new System.Drawing.SizeF(1757F, 64F);
            this.xrTituloUnidade.StylePriority.UseFont = false;
            this.xrTituloUnidade.Text = "Unidade: XXXXXXXXXXXXXXXXX";
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ACO")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(169F, 32F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(254F, 35F);
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 33F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(148F, 35F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Acordo:";
            // 
            // ACOrdosbindingSource
            // 
            this.ACOrdosbindingSource.DataMember = "ACOrdos";
            this.ACOrdosbindingSource.DataSource = typeof(BoletosProc.Acordo.dAcordo);
            // 
            // aCOrdosOriginaisBindingSource
            // 
            this.aCOrdosOriginaisBindingSource.DataMember = "ACOrdos_Originais";
            this.aCOrdosOriginaisBindingSource.DataSource = this.ACOrdosbindingSource;
            // 
            // aCOrdosNovosBindingSource
            // 
            this.aCOrdosNovosBindingSource.DataMember = "ACOrdos_Novos";
            this.aCOrdosNovosBindingSource.DataSource = this.ACOrdosbindingSource;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTituloUnidade,
            this.xrLine1});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("ACO_APT", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 93F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeader1_BeforePrint);
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 4;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1799F, 20F);
            this.xrLine1.Visible = false;
            // 
            // xrjuridico
            // 
            this.xrjuridico.Dpi = 254F;
            this.xrjuridico.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrjuridico.LocationFloat = new DevExpress.Utils.PointFloat(656F, 21F);
            this.xrjuridico.Name = "xrjuridico";
            this.xrjuridico.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrjuridico.SizeF = new System.Drawing.SizeF(148F, 42F);
            this.xrjuridico.StylePriority.UseFont = false;
            this.xrjuridico.Text = "Jur�dico";
            // 
            // xrjudicial
            // 
            this.xrjudicial.Dpi = 254F;
            this.xrjudicial.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrjudicial.LocationFloat = new DevExpress.Utils.PointFloat(656F, 64F);
            this.xrjudicial.Name = "xrjudicial";
            this.xrjudicial.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrjudicial.SizeF = new System.Drawing.SizeF(148F, 42F);
            this.xrjudicial.StylePriority.UseFont = false;
            this.xrjudicial.Text = "Judicial";
            // 
            // xradvogado
            // 
            this.xradvogado.Dpi = 254F;
            this.xradvogado.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xradvogado.LocationFloat = new DevExpress.Utils.PointFloat(826F, 42F);
            this.xradvogado.Name = "xradvogado";
            this.xradvogado.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xradvogado.SizeF = new System.Drawing.SizeF(952F, 42F);
            this.xradvogado.StylePriority.UseFont = false;
            this.xradvogado.Text = "NomeADV";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(635F, 34F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(677F, 106F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subImpNov1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(1101F, 85F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(21F, 106F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subImpOrig2;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(635F, 85F);
            // 
            // ImpAcordo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupHeader1});
            this.DataSource = this.ACOrdosbindingSource;
            this.Version = "10.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ImpAcordo_BeforePrint);
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ACOrdosbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosOriginaisBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCOrdosNovosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subImpNov1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subImpOrig2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel xrTituloUnidade;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource ACOrdosbindingSource;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;

        private System.Windows.Forms.BindingSource aCOrdosOriginaisBindingSource;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport2;
        private Boletos.Acordo.SubImpNov subImpNov1;
        private Boletos.Acordo.SubImpOrig subImpOrig2;
        private System.Windows.Forms.BindingSource aCOrdosNovosBindingSource;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xradvogado;
        private DevExpress.XtraReports.UI.XRLabel xrjudicial;
        private DevExpress.XtraReports.UI.XRLabel xrjuridico;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
    }
}