using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Boletos.Boleto;
using DevExpress.XtraReports.UI;
using AbstratosNeon;
using BoletosProc.Acordo;
using VirEnumeracoesNeon;

namespace Boletos.Acordo
{
    /// <summary>
    /// Acordo
    /// </summary>
    public partial class cAcordo : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// Apartamento
        /// </summary>
        public int APT;

        /// <summary>
        /// Construtor padrao
        /// </summary>
        public cAcordo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Refresh
        /// </summary>
        public override void Refresh()
        {
            CarregaDados();
        }

        private SortedList _GuardaBoletos;
        private SortedList GuardaBoletos { get { return _GuardaBoletos ?? (_GuardaBoletos = new SortedList()); } }

        Boleto.Boleto PegaBoleto(int BOL)
        {
            Boleto.Boleto Retorno;
            Retorno = (Boleto.Boleto)GuardaBoletos[BOL];
            if (Retorno == null) 
            {
                Retorno = new Boletos.Boleto.Boleto(BOL);
                GuardaBoletos.Add(BOL, Retorno);
            }
            return Retorno;
        }

        /*
        private void Correcao(int corAPT) 
        {
            if (corAPT == 0)
            {
                dAcordoTableAdapters.AcordoAntigosTableAdapter TA = new Boletos.Acordo.dAcordoTableAdapters.AcordoAntigosTableAdapter();
                TA.TrocarStringDeConexao();
                TA.Fill(dAcordo.AcordoAntigos);
            }
            //Verifica se tem acordo antigo para migrar
            string BuscaAcodosAntigoas;
            DataTable Antigos;
            if (corAPT != 0)
            {
                BuscaAcodosAntigoas =
            "SELECT   distinct  ACOrdos.ACO,ACOAntigo\r\n" +
            "FROM         ACOrdos INNER JOIN\r\n" +
            "                      ACOxBOL ON ACOrdos.ACO = ACOxBOL.ACO INNER JOIN\r\n" +
            "                      BOLetos ON ACOxBOL.BOL = BOLetos.BOL INNER JOIN\r\n" +
            "                      BOletoDetalhe ON BOLetos.BOL = BOletoDetalhe.BOD_BOL\r\n" +
            "WHERE     (ACOrdos.ACOStatus = 1) AND (BOletoDetalhe.BODAcordo_BOL IS NULL) AND (BOletoDetalhe.BOD_PLA = '111000') AND \r\n" +
            "                      (ACOxBOL.ACOBOLOriginal = 0) AND (ACOrdos.ACO_APT = @P1);";
                Antigos = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(BuscaAcodosAntigoas, corAPT);
            }
            else
            {
                BuscaAcodosAntigoas =
        "SELECT   distinct TOP (1000) ACOrdos.ACO,ACOAntigo\r\n" +
        "FROM         ACOrdos INNER JOIN\r\n" +
        "                      ACOxBOL ON ACOrdos.ACO = ACOxBOL.ACO INNER JOIN\r\n" +
        "                      BOLetos ON ACOxBOL.BOL = BOLetos.BOL INNER JOIN\r\n" +
        "                      BOletoDetalhe ON BOLetos.BOL = BOletoDetalhe.BOD_BOL\r\n" +
        "WHERE     (ACOrdos.ACOStatus = 1) AND (BOletoDetalhe.BODAcordo_BOL IS NULL) AND (BOletoDetalhe.BOD_PLA = '111000') AND \r\n" +
        "                      (ACOxBOL.ACOBOLOriginal = 0);";
                Antigos = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(BuscaAcodosAntigoas);
            }
            
            if (Antigos != null)
            {
                int mig = 0;
                int stat = 0;
                int falha = 0;
                progressBar1.Maximum = Antigos.Rows.Count;
                string modL1 = string.Format(" / {0:n0}", Antigos.Rows.Count);
                progressBar1.Value = 0;
                progressBar1.Visible = label1.Visible = true;
                
                DateTime mostrar = DateTime.Now.AddSeconds(3);
                foreach (DataRow row in Antigos.Rows)
                {

                    string BuscaAcordoAntigo = "SELECT ENCERRADO\r\n" +
                                                   "FROM         Acordo\r\n" +
                                                   "WHERE     ([N�mero Acordo] = @P1 )";
                    bool encerrado = false;
                    int novostatus = 1;
                    if (corAPT == 0)
                    {
                        dAcordo.AcordoAntigosRow rowAcoV = dAcordo.AcordoAntigos.FindByN�mero_Acordo((int)row[1]);
                        if (rowAcoV == null)
                            novostatus = 3;
                        else
                            if (rowAcoV.Encerrado)
                                novostatus = 2;
                    }
                    else
                    {
                        if (!VirOleDb.TableAdapter.STTableAdapter.BuscaEscalar(BuscaAcordoAntigo, out encerrado, (int)row[1]))
                            novostatus = 3;
                        if (encerrado)
                            novostatus = 2;
                    };
                    if (novostatus != 1)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update ACOrdos set ACOStatus = @P1 where ACO = @P2", novostatus, (int)row[0]);
                        stat++;
                    }
                    else
                    {
                        Acordo Ac = new Acordo((int)row[0]);
                        if (!Ac.Converte())
                        {
                            
                            falha++;
                        }
                        else
                            mig++;
                    }
                    progressBar1.Value = progressBar1.Value + 1;
                    if (mostrar < DateTime.Now)
                    {
                        Application.DoEvents();
                        label1.Text = string.Format("{0:n0}{1} falha {2} encerrado {3} migrado {4}", progressBar1.Value,modL1,falha,stat,mig);
                        mostrar = DateTime.Now.AddSeconds(3);
                    }
                };
                if (corAPT == 0)
                    MessageBox.Show("ok");
                progressBar1.Visible = false;
                label1.Visible = false;
            }
        }
        */
        private void CarregaDados()
        {
            //Correcao(APT);
                        
            if (_GuardaBoletos != null)
                _GuardaBoletos.Clear();
            _GuardaBoletos = null;
            BNAcordo.Enabled = true;
            BCancela.Enabled = false;

            bool Carregado = false;
            while (!Carregado)
            {
                Carregado = true;
                dAcordo.ACOrdosTableAdapter.FillByAPT(dAcordo.ACOrdos, APT);
                dAcordo.NovosTableAdapter.Fill(dAcordo.Novos, APT);
                dAcordo.OriginaisTableAdapter.Fill(dAcordo.Originais, APT);
                foreach (dAcordo.ACOrdosRow rowAco in dAcordo.ACOrdos)
                {
                    if ((StatusACO)rowAco.ACOStatus == StatusACO.Ativo)
                    {
                        Acordo Ac = new Acordo(rowAco.ACO);
                        if (Ac.Encerrar())
                            Carregado = false;
                    }
                        
                };
            }
            if (nivel < 3)
                BNAcordo.Enabled = false;
            //foreach (dAcordo.ACOrdosRow rowACO in dAcordo.ACOrdos)
                //if (rowACO.ACOStatus == 1)
                //{
                    
                  //  BCancela.Enabled = true;
                    

                //}
        }

        private int nivel;

        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup1;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_APT">APT</param>
        public cAcordo(int _APT)
        {
            InitializeComponent();
            dFornecedoresLookup1 = new CadastrosProc.Fornecedores.dFornecedoresLookup();
            fORNECEDORESBindingSource.DataSource = dFornecedoresLookup1;
            dFornecedoresLookup1.FRNLookupTableAdapter.Fill(dFornecedoresLookup1.FRNLookup);            
            APT = _APT;
            nivel = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("ACORDO");
            CarregaDados();
            if (nivel >= 3)
            {
                BotTrocaADV.Visible = lookUpEdit1.Visible = true;
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            return true;
        }

        private ABS_Apartamento _Ap;

        private ABS_Apartamento Ap
        {
            get
            {
                if (_Ap == null)
                    _Ap = ABS_Apartamento.GetApartamento(APT);
                return _Ap;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int status = 0;
            string comando =
"SELECT     CONAvisoAcordo, CONStatusAcordo\r\n" +
"FROM         CONDOMINIOS\r\n" +
"WHERE     (CON = @P1);";
            DataRow rowretorno = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(comando, Ap.CON);
            if ((rowretorno != null) && (rowretorno["CONStatusAcordo"] != DBNull.Value))
                status = (int)rowretorno["CONStatusAcordo"];
            switch (status)
            {
                case 1:
                    MessageBox.Show(rowretorno["CONAvisoAcordo"].ToString(), "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case 2:
                    MessageBox.Show(rowretorno["CONAvisoAcordo"].ToString(), "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    int nivel = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("ACORDO");
                    if (nivel < 3)
                        return;
                    break;
            }
            //string BuscaAcordoAntigo = "SELECT     [N�mero Acordo]\r\n" +
            //                           "FROM         Acordo\r\n" +
            //                           "WHERE     (Codcon = @P1 ) AND (Bloco = @P3 ) AND (Apartamento = @P2 )";
            bool Cancelar = false;
            //if (VirOleDb.TableAdapter.STTableAdapter.EstaCadastrado(BuscaAcordoAntigo, Ap.CONCodigo, Ap.BLOCodigo, Ap.APTNumero))
            int nAcordosAtivos = 0;

            if (nAcordosAtivos > 0)
            {
                if (nivel >= 4)
                {
                    Cancelar = MessageBox.Show("ESTE APARTAMENTO J� POSSUI ACORDO\r\nEfetuar um segundo?", "Segundo acordo", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No;
                }
                else
                {
                    MessageBox.Show("ESTE APARTAMENTO J� POSSUI ACORDO");
                    Cancelar = true;
                }
            }


            if (!Cancelar)
            {
                Acordo NovoAcordo = new Acordo(APT, false, false);
                                                
                //bOLetosTableAdapter.Fill(dAcordo.BOLetos, APT);

                cNovoAcordo Novo = new cNovoAcordo(NovoAcordo);
                //Novo.gridOriginais.DataSource = dAcordo.BOLetos;
                if (cNovoAcordo.ShowModuloSTA(CompontesBasicos.EstadosDosComponentes.PopUp, Novo) == DialogResult.OK)
                    this.FechaTela(DialogResult.OK);
            };
        }        

        private void BCancela_Click(object sender, EventArgs e)
        {
            if ((BoletosProc.Acordo.StatusACO)LinhaMae.ACOStatus != BoletosProc.Acordo.StatusACO.Ativo)
                return;

            Acordo AcordoCancelar = new Acordo(LinhaMae.ACO);
            string Justificativa = "";
            if (VirInput.Input.Execute("Justificativa:", ref Justificativa, true))
                if (AcordoCancelar.Cancelar(Justificativa))
                    this.FechaTela(DialogResult.OK);
                else
                    MessageBox.Show(string.Format("Acordo n�o cancelado\r\n\r\n{0}", AcordoCancelar.UltimoErro));
            return;
            /*
            try
            {
                VirMSSQL.TableAdapter.STTableAdapter.IniciaTrasacaoST(dAcordo.ACOrdosTableAdapter);
                VirOleDb.TableAdapter.STTableAdapter.IniciaTrasacao();
                decimal TotalPago = 0;
                foreach (dAcordo.NovosRow novo in LinhaMae.GetNovosRows())
                {
                    Boleto.Boleto NBoleto = new Boletos.Boleto.Boleto(novo.BOL);
                    if (!novo.IsBOLPagamentoNull())
                    {
                        throw new Exception("N�o foi implementado o cancelamento com boletos pagos");
                        TotalPago += novo.BOLValorPago;
                        NBoleto.GravaJustificativa("Este boleto j� estava pago no momento do cancelamento do acordo", "");
                    }
                    else
                        NBoleto.GravaJustificativa("Acordo cancelado", "");
                    NBoleto.Cancelar();

                };
                foreach (dAcordo.OriginaisRow Orig in LinhaMae.GetOriginaisRows())
                {
                    Boleto.Boleto OBoleto = new Boletos.Boleto.Boleto(Orig.BOL);

                    OBoleto.Reativa("Boleto restaurado pelo cancelamento do acordo");
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM  [Boleto - Acordo] WHERE ([N�mero Boleto] = @P1)", Orig.BOL);
                    //quitarparcial
                };
                LinhaMae.ACOStatus = 3;
                dAcordo.ACOrdosTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM Acordo WHERE (Acordo.[N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                VirOleDb.TableAdapter.STTableAdapter.Commit();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();

            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(null);
                VirOleDb.TableAdapter.STTableAdapter.Vircatch(ex);
                throw new Exception("Cancelando acordo:" + ex.Message, ex);
            }
            */ 
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            BNAcordo.Enabled = true;
        }

        dAcordo.ACOrdosRow LinhaMae
        {
            get
            {
                DataRowView DRV = (DataRowView)gridView1.GetRow(gridView1.FocusedRowHandle);
                if ((DRV == null) || (DRV.Row == null))
                    return null;
                else
                    return (dAcordo.ACOrdosRow)DRV.Row;
            }
        }

        private void simpleButton1_Click_2(object sender, EventArgs e)
        {
            if ((LinhaMae != null) && (lookUpEdit1.EditValue != null) && (MessageBox.Show("Confirma ?", "Confirma��o", MessageBoxButtons.YesNo) == DialogResult.Yes))
            {
                LinhaMae.ACOADV_FRN = (int)lookUpEdit1.EditValue;
                dAcordo.ACOrdosTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
            }
        }

        private void cBotaoImpAcordo_clicado(object sender, EventArgs e)
        {
            if (LinhaMae == null)
                return;
            using (ImpAcordo Impresso = new ImpAcordo(chDetalhado.Checked))
            {
                Impresso.CarregaDados(LinhaMae.ACO);

                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(Ap.CON);
                if (rowCON != null)
                    Impresso.SetNome(rowCON.CONNome);
                else
                    Impresso.SetNome(Ap.CONCodigo);

                switch (cBotaoImpAcordo.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.botao:
                    case dllImpresso.Botoes.Botao.imprimir:
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                        Impresso.Apontamento(Ap.CON, 71);
                        Impresso.CreateDocument();
                        Impresso.Print();
                        break;
                    case dllImpresso.Botoes.Botao.tela:
                        Impresso.CreateDocument();
                        Impresso.ShowPreviewDialog();
                        break;
                    case dllImpresso.Botoes.Botao.pdf:
                    case dllImpresso.Botoes.Botao.PDF_frente:
                        System.Windows.Forms.SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            Impresso.CreateDocument();
                            Impresso.ExportOptions.Pdf.Compressed = true;
                            Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                            Impresso.ExportToPdf(saveFileDialog1.FileName);
                        }
                        break;
                    case dllImpresso.Botoes.Botao.email:
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Acordo", Impresso, "Acordo", "Acordo"))
                            MessageBox.Show("E.mail enviado");
                        break;
                }
            }
        }

        private Font _FonteDestacado;

        private Font FonteDestacado(Font Modelo)
        {
            if (_FonteDestacado == null)
                _FonteDestacado = new Font(Modelo, FontStyle.Bold);
            return _FonteDestacado;
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dAcordo.OriginaisRow rowO = (dAcordo.OriginaisRow)gridView2.GetFocusedDataRow();
            if (rowO != null)
            {
                Boleto.Boleto BOLo = PegaBoleto(rowO.BOL);
                if (BOLo.Encontrado)
                {
                    Boletos.Boleto.cBoleto cBoleto = new cBoleto(BOLo.rowPrincipal, true);
                    cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
                }
            }
        }





        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dAcordo.NovosRow rowN = (dAcordo.NovosRow)gridView3.GetFocusedDataRow();
            if (rowN != null)
            {
                Boleto.Boleto BOLN = PegaBoleto(rowN.BOL);
                if (BOLN.Encontrado)
                {
                    Boletos.Boleto.cBoleto cBoleto = new cBoleto(BOLN.rowPrincipal, true);
                    cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
                }
            }
        }

        private void gridView2_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {

            dAcordo.OriginaisRow rowO = (dAcordo.OriginaisRow)gridView2.GetDataRow(e.RowHandle);
            if (rowO != null)
            {
                Boleto.Boleto BOLo = PegaBoleto(rowO.BOL);
                if (BOLo.Encontrado)
                    BOLo.AjustaAparencia(e.Appearance);
            }
        }

        private void gridView3_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dAcordo.NovosRow rowN = (dAcordo.NovosRow)gridView3.GetDataRow(e.RowHandle);
            if (rowN != null)
            {
                Boleto.Boleto BOLN = PegaBoleto(rowN.BOL);
                if (BOLN.Encontrado)
                    BOLN.AjustaAparencia(e.Appearance);
            }
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dAcordo.ACOrdosRow Linha = (dAcordo.ACOrdosRow)gridView1.GetDataRow(e.RowHandle);
            if(Linha != null)
                switch ((BoletosProc.Acordo.StatusACO)Linha.ACOStatus)
                {
                    case StatusACO.NaoGerado:
                        break;
                    case StatusACO.Ativo:
                        e.Appearance.BackColor = Color.Yellow;
                        break;
                    case StatusACO.Terminado:
                        e.Appearance.BackColor = Color.Silver;
                        break;
                    case StatusACO.Cancelado:
                        e.Appearance.BackColor = Color.FromArgb(254, 200, 200);//vermelho
                        break;
                    case StatusACO.Divide:
                        e.Appearance.BackColor = Color.Thistle;
                        break;
                    default:
                        break;
                }

        }

                
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (LinhaMae == null)
                return;
            BCancela.Enabled = ((BoletosProc.Acordo.StatusACO)LinhaMae.ACOStatus == StatusACO.Ativo);
        }

        private void lookUpEdit1_Properties_PopupFilter(object sender, DevExpress.XtraEditors.Controls.PopupFilterEventArgs e)
        {
            e.Criteria = DevExpress.Data.Filtering.CriteriaOperator.Parse("RAV = ?", (int)RAVPadrao.EscritorioAdvogado);
        }
    }

}

