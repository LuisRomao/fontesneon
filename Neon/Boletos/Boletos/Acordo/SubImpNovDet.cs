using System;
using DevExpress.XtraReports.UI;

namespace Boletos.Acordo
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SubImpNovDet : XtraReport
    {
        /// <summary>
        /// 
        /// </summary>
        public SubImpNovDet()
        {
            InitializeComponent();            
        }

        private void CantoRedondo(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.CantosRedondos(sender, e, 30);
        }

        private int i;

        private BoletosProc.Acordo.dAcordo.NovosRow LinhaMae
        {
            get
            {
                System.Data.DataRowView DRV = (System.Data.DataRowView)GetCurrentRow();
                if (DRV == null)
                    return null;
                else
                    return (BoletosProc.Acordo.dAcordo.NovosRow)DRV.Row;
            }
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            labNovParcela.Text = i.ToString();
            xrlinhasDet.Text = "";
            xrlinhasDetVal.Text = "";
            foreach (BoletosProc.Acordo.dAcordo.BOletoDetalheRow rowBOD in LinhaMae.GetBOletoDetalheRows())
            {
                xrlinhasDet.Text += string.Format("{0}{1}{2}",
                    xrlinhasDet.Text == "" ? "" : "\r\n",
                    rowBOD.IsBODAcordo_BOLNull() ? "" : string.Format("{0}: ", rowBOD.BODAcordo_BOL),
                    rowBOD.BODMensagem);
                xrlinhasDetVal.Text += string.Format("{0}{1:n2}",
                    xrlinhasDetVal.Text == "" ? "" : "\r\n",
                    rowBOD.BODValor);
            }
        }

        private void SubImpNovDet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }

    }
}
