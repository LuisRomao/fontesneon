//05/05/2014 LH - Seguro no acordo 13.2.8.41
//06/05/2014 LH - Acordo via internet da erro 13.2.8.42

using System;
using Boletos.Boleto;
using dllVirEnum;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using AbstratosNeon;
using CompontesBasicos.Performance;
using VirEnumeracoesNeon;
using BoletosProc.Acordo;

namespace Boletos.Acordo
{
    /// <summary>
    /// Classe representante do acordo
    /// </summary>
    public class Acordo:BoletosProc.Acordo.AcordoProc
    {
        
        private ABS_Apartamento apartamento;
        /// <summary>
        /// Apartamento
        /// </summary>
        public ABS_Apartamento Apartamento { get { return apartamento ?? (apartamento = ABS_Apartamento.GetApartamento(APT)); } }                               

        /// <summary>
        /// Linha mae do acordo
        /// </summary>
        public dAcordo.ACOrdosRow LinhaMae;

        
        /// <summary>
        /// Gera os redistros da divisao na tabela ACOrdo
        /// </summary>
        /// <param name="BOrigem"></param>
        /// <param name="Novos"></param>
        /// <returns>ACO</returns>
        public static int GeraDivisao(Boleto.Boleto BOrigem, params Boleto.Boleto[] Novos)
        {
            try
            {
                dAcordo dAcordo = new dAcordo();
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Acordo - 94",dAcordo.ACOrdosTableAdapter, dAcordo.ACOxBOLTableAdapter);
                dAcordo.ACOrdosRow novoACO = dAcordo.ACOrdos.NewACOrdosRow();                
                novoACO.ACO_APT = BOrigem.rowPrincipal.BOL_APT;
                novoACO.ACOI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                novoACO.ACODATAI = DateTime.Now;
                novoACO.ACOProprietario = BOrigem.rowPrincipal.BOLProprietario;
                novoACO.ACOStatus = (int)StatusACO.Divide;
                dAcordo.ACOrdos.AddACOrdosRow(novoACO);
                dAcordo.ACOrdosTableAdapter.Update(novoACO);
                novoACO.AcceptChanges();
                dAcordo.ACOxBOL.AddACOxBOLRow(novoACO, BOrigem.rowPrincipal.BOL, true);
                foreach (Boleto.Boleto BNovo in Novos)
                    dAcordo.ACOxBOL.AddACOxBOLRow(novoACO, BNovo.rowPrincipal.BOL, false);

                dAcordo.ACOxBOLTableAdapter.Update(dAcordo.ACOxBOL);

                dAcordo.ACOxBOL.AcceptChanges();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return novoACO.ACO;
            }
            catch (Exception e)
            {                
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro na divis�o: " + e.Message, e);
            }
        }

        /// <summary>
        /// Construtor para novos acordos
        /// </summary>
        /// <param name="_APT">APT</param> 
        /// <param name="JaCalculado">indica se o acordo j� est� calculado e nao necessita das informa��es de atualiza��o dos bolteos (juros, multa etc)</param>
        /// <param name="ComA">Com boletos de acordo</param>
        public Acordo(int _APT,bool JaCalculado,bool ComA):base(!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, _APT,JaCalculado,ComA)
        {                        
            //CarregaDados(JaCalculado, ComA);
        }

        /// <summary>
        /// Construtor para acordos existentes
        /// </summary>
        /// <param name="ACO"></param>
        public Acordo(int ACO):base(!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao) 
        {
            VirMSSQL.TableAdapter.EmbarcaEmTrans(dAcordo.ACOrdosTableAdapter, dAcordo.OriginaisTableAdapter, dAcordo.NovosTableAdapter);
            this.ACO = ACO;
            Gerado = (dAcordo.ACOrdosTableAdapter.FillByACO(dAcordo.ACOrdos, ACO) > 0);
            if (Gerado)
            {
                LinhaMae = dAcordo.ACOrdos[0];
                Proprietario = LinhaMae.ACOProprietario;
                Judicial = LinhaMae.IsACOJudicialNull() ? false : LinhaMae.ACOJudicial;
                Juridico = LinhaMae.IsACOJuridicoNull() ? false : LinhaMae.ACOJuridico;
                APT = LinhaMae.ACO_APT;
                if (!LinhaMae.IsACOADV_FRNNull())
                    FRN = LinhaMae.ACOADV_FRN;
                if (Juridico && (FRN == 0))
                {
                    string comando =
"SELECT     CONDOMINIOS.CONEscrit_FRN\r\n" +
"FROM         BLOCOS INNER JOIN\r\n" +
"                      CONDOMINIOS ON BLOCOS.BLO_CON = CONDOMINIOS.CON INNER JOIN\r\n" +
"                      APARTAMENTOS ON BLOCOS.BLO = APARTAMENTOS.APT_BLO\r\n" +
"WHERE     (APARTAMENTOS.APT = @P1);";
                    int novoFRN;
                    if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(comando, out novoFRN, LinhaMae.ACO_APT))
                        FRN = novoFRN;
                }
                dAcordo.OriginaisTableAdapter.FillByACO(dAcordo.Originais, ACO);
                dAcordo.NovosTableAdapter.FillByACO(dAcordo.Novos, ACO);
            }
        }

        /// <summary>
        /// Construtor para acordo j� existente
        /// </summary>
        /// <param name="Boleto">um boleto do acordo</param>
        /// <param name="Original"></param>
        /// <param name="Status"></param>
        public Acordo(Boleto.Boleto Boleto, bool Original, StatusACO? Status = null)
            : base(!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao) 
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Acordo embarca",true);
            VirMSSQL.TableAdapter.EmbarcaEmTrans(dAcordo.ACOrdosTableAdapter, dAcordo.OriginaisTableAdapter, dAcordo.NovosTableAdapter);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Acordo fill 1", false);
            if(Status.HasValue)
                Gerado = (dAcordo.ACOrdosTableAdapter.FillByBOLStatus(dAcordo.ACOrdos, Boleto.rowPrincipal.BOL, Original,(int)Status) > 0);
            else
                Gerado = (dAcordo.ACOrdosTableAdapter.FillByBOL(dAcordo.ACOrdos, Boleto.rowPrincipal.BOL,Original) > 0);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Acordo marca", false);
            if (Gerado)
            {
                LinhaMae = dAcordo.ACOrdos[0];
                ACO = LinhaMae.ACO;
                this.Proprietario = LinhaMae.ACOProprietario;
                if (!LinhaMae.IsACOJudicialNull())
                    this.Judicial = LinhaMae.ACOJudicial;
                if (!LinhaMae.IsACOJuridicoNull())
                    this.Juridico = LinhaMae.ACOJuridico;
                this.APT = LinhaMae.ACO_APT;
                if (!LinhaMae.IsACOADV_FRNNull())
                    this.FRN = LinhaMae.ACOADV_FRN;
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Acordo fill 2", false);
                dAcordo.OriginaisTableAdapter.FillByACO(dAcordo.Originais, ACO);
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Acordo fill 3", false);
                dAcordo.NovosTableAdapter.FillByACO(dAcordo.Novos, ACO);
            }
            CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
        }
 
        /*
        /// <summary>
        /// Construtor para acordo existente
        /// </summary>
        /// <param name="ACO">ACO</param>
        public Acordo(int ACO)
        {
        }
        */

        /// <summary>
        /// inclui o novo boleto dos honor�rios ao acordo
        /// </summary>
        /// <param name="BOL"></param>
        internal void IncluirBoletoHonorarios(int BOL)
        {
            IncluirBoleto(BOL,true);
        }

        internal void IncluirBoleto(int BOL, bool Original)
        {
            dAcordo.ACOxBOLRow novarow = dAcordo.ACOxBOL.NewACOxBOLRow();
            novarow.ACO = ACO;
            novarow.BOL = BOL;
            novarow.ACOBOLOriginal = Original;
            dAcordo.ACOxBOL.AddACOxBOLRow(novarow);
            dAcordo.ACOxBOLTableAdapter.EmbarcaEmTransST();
            dAcordo.ACOxBOLTableAdapter.Update(novarow);
            novarow.AcceptChanges();
        }

        

        private SortedList<int, Boleto.Boleto> _BoletosOrig;

        private SortedList<int,Boleto.Boleto> BoletosOrig
        { get { return _BoletosOrig ?? (_BoletosOrig = new SortedList<int, Boleto.Boleto>()); } }

        /// <summary>
        /// Para cadastrar os boletos originais em acordos da internet
        /// </summary>
        /// <param name="BOL"></param>
        public void CadastraBoletoOrig(int BOL)
        {
            Boleto.Boleto BOr = new Boleto.Boleto(BOL);
            BoletosOrig.Add(BOL, BOr);
        }

        private List<int> BolsRemoverSeguro;

        /// <summary>
        /// Calcula os encargos dos boletos
        /// </summary>
        /// <param name="Data">Date de c�lculo</param>
        protected override void AtualizaValores(DateTime Data)
        {
            BoletosOrig.Clear();
            BolsRemoverSeguro = null;
            foreach (dAcordo.BOLetosRow rowBOL in dAcordo.BOLetos)
            {
                Boleto.Boleto Bol = new Boletos.Boleto.Boleto(rowBOL.BOL);
                //Bol.RemoveSeguro(Boleto.Boleto.TipoRemoveSeguro.remove_SeguroVencido);                
                if (Bol.ComSeguro())
                {
                    Bol.Valorpara(Data, true);
                    if (BolsRemoverSeguro == null)
                        BolsRemoverSeguro = new List<int>();
                    BolsRemoverSeguro.Add(rowBOL.BOL);
                }
                else
                    Bol.Valorpara(Data, false);
                rowBOL.ValorCorrigido = Bol.valorcorrigido;
                rowBOL.Multa = Bol.multa;
                rowBOL.Juros = Bol.juros;
                rowBOL.SubTotal = Bol.valorfinal;
                rowBOL.Honorarios = Bol.honor;
                rowBOL.Total = Bol.valorfinal + Bol.honor;
                rowBOL.Incluir = (Bol.rowPrincipal.BOLProprietario == Proprietario);
                BoletosOrig.Add(Bol.rowPrincipal.BOL, Bol);
            }
        }

        private String VerificaIntegridade()
        {
            String Retorno = "";
            /*
            //Verifica se o apartamento est� cadastrado
            dLocal1.ApartamentoTableAdapter.Fill(dLocal1.Apartamento, retacordirow.codcon,retacordirow.bloco,retacordirow.apartamento);
            if (dLocal1.Apartamento.Rows.Count == 0)
            {
                Retorno = "Apartamento n�o Cadastrado";
            }
            else
                if (dLocal1.Apartamento.Rows.Count > 1)
                    Retorno = "Mais de um apartamento cadastrado";
                else
                    Apatarow = (dLocal.ApartamentoRow)dLocal1.Apartamento.Rows[0];

            if (Retorno == "")
            {
                //O acordo � para o inquilino mas n�o tem inquilino?                
                if((retacordirow.DESTINATARIO.ToUpper() == "I")
                        &&
                   (dLocal1.ApartamentoTableAdapter.TemInquilino(retacordirow.codcon, retacordirow.bloco, retacordirow.apartamento) != null))
                    Retorno = "Acordo cancelado, Acordo n�o pode ser realizado pois apto n�o tem inquilino";                    
            };

            if (Retorno == "") {
                //j� tem acordo?                  
                if (dLocal1.ApartamentoTableAdapter.JatemAcordo(retacordirow.codcon, retacordirow.apartamento, retacordirow.bloco) != null)                
                    Retorno = "Acordo cancelado, apartamento j� possui acordo no sistema";
            };

            if ((retacordirow.IsJuridicoNull()) || (!retacordirow.Juridico))
              if (Retorno == "") {
                if (dLocal1.ApartamentoTableAdapter.EstaNoJuridico(retacordirow.codcon,retacordirow.apartamento, retacordirow.bloco) != null)                
                    Retorno = "Acordo cancelado, cobran�a no jur�dico";
              };

            if (Retorno == "")
            {
                //Verificar parcelas originais
                foreach (dIntenet.RetOriginaisRow Origrow in retacordirow.GetRetOriginaisRows()) { 
                    //Verifica se a parcela original existe e se esta paga
                    object retornoPago = dLocal1.ApartamentoTableAdapter.ParcelaOriginalPaga(Origrow.numero);
                    if ((retornoPago == null) || (retornoPago == DBNull.Value))
                        Retorno = "Acordo cancelado, pois boleto original n�o foi encontrado (" + Origrow.numero.ToString() + ")";
                    else
                        if ((Boolean)retornoPago)
                            Retorno = "Acordo cancelado, pois boleto original est� pago (" + Origrow.numero.ToString() + ")";
                    if (Retorno != "")
                        break;
                };
            }
            */
            return Retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        public string UltimoErro="";

        private System.Collections.SortedList NovosBoletosGerados;

        class retParte 
        {
            public decimal ValorEfetivo;
            public string Descricao;
            public int BOL;
            public bool Proprietario;
        }

        int iPegaParte;
        Boleto.Boleto[] Bsaldo;
        decimal[] ValoresEfetivos;
        decimal[] Porcs;
        int ProximoBSaldo;
        void ZerarPegaParte() 
        {
            iPegaParte = 0;
            Bsaldo = null;           
        }
        


        private retParte PegaParteDaOrigem(decimal Teto,int k)
        {
            Performance.PerformanceST.Registra("PegaParteDaOrigem 1");
            retParte retorno = new retParte();
            if (Bsaldo != null)
            {
                Performance.PerformanceST.Registra("PegaParteDaOrigem 2");
                retorno.Descricao = string.Format("Boleto {0} vencimento {1:dd/MM/yyyy} ({2:n0}%)", dAcordo.BOLetos[iPegaParte].BOL, dAcordo.BOLetos[iPegaParte].BOLVencto, Porcs[ProximoBSaldo]*100);
                retorno.ValorEfetivo = ValoresEfetivos[ProximoBSaldo];
                retorno.BOL = Bsaldo[ProximoBSaldo].rowPrincipal.BOL;
                retorno.Proprietario = Bsaldo[ProximoBSaldo].rowPrincipal.BOLProprietario;
                ProximoBSaldo++;
                if (ProximoBSaldo == Bsaldo.Length)
                {
                    Bsaldo = null;
                    iPegaParte++;
                }
            }
            else
            {
                Performance.PerformanceST.Registra("PegaParteDaOrigem 3");
                while (!dAcordo.BOLetos[iPegaParte].Incluir)
                    iPegaParte++;                
                if (dAcordo.BOLetos[iPegaParte].incTotal > Teto)
                {
                    System.Collections.ArrayList aValoresEfetivos = new System.Collections.ArrayList();
                    aValoresEfetivos.Add(Teto);
                    decimal SaldoBoletoDividindo = dAcordo.BOLetos[iPegaParte].incTotal - Teto;
                    k++;
                    Performance.PerformanceST.Registra("PegaParteDaOrigem W");
                    while(SaldoBoletoDividindo > 0)
                        if ((dAcordo.GeraNovos[k].Valor - dAcordo.GeraNovos[k].Honorarios) < SaldoBoletoDividindo)
                        {
                            aValoresEfetivos.Add((dAcordo.GeraNovos[k].Valor - dAcordo.GeraNovos[k].Honorarios));
                            SaldoBoletoDividindo -= (dAcordo.GeraNovos[k].Valor - dAcordo.GeraNovos[k].Honorarios);
                            k++;
                        }
                        else
                        {
                            aValoresEfetivos.Add(SaldoBoletoDividindo);
                            SaldoBoletoDividindo = 0;
                        }
                    ValoresEfetivos = (decimal[])aValoresEfetivos.ToArray(typeof(decimal));
                    Porcs = new decimal[ValoresEfetivos.Length];
                    //decimal Verifica = 1;
                    Performance.PerformanceST.Registra("PegaParteDaOrigem FOR");
                    for (int w = 0; w < ValoresEfetivos.Length; w++)
                    {
                        Porcs[w] = ValoresEfetivos[w] / dAcordo.BOLetos[iPegaParte].incTotal;
                        //Verifica -= Porcs[w];
                    };
                    //if (Verifica != 0)
                    //    System.Windows.Forms.MessageBox.Show("Erro:"+Verifica.ToString("n8"));                                        

                    Boleto.Boleto BO = BoletosOrig[dAcordo.BOLetos[iPegaParte].BOL];
                    if (BO == null)
                    {
                        Performance.PerformanceST.Registra("PegaParteDaOrigem NEW BO");
                        BO = new Boleto.Boleto(dAcordo.BOLetos[iPegaParte].BOL);
                    }
                    Performance.PerformanceST.Registra("DIVIDIR",true);
                    Bsaldo = BO.Dividir(ACO, Porcs);
                    Performance.PerformanceST.StackPop();
                    retorno.BOL = Bsaldo[0].rowPrincipal.BOL;
                    retorno.Proprietario = dAcordo.BOLetos[iPegaParte].BOLProprietario;
                    retorno.Descricao = string.Format("Boleto {0} vencimento {1:dd/MM/yyyy} ({2:n0}%)", dAcordo.BOLetos[iPegaParte].BOL, dAcordo.BOLetos[iPegaParte].BOLVencto, Porcs[0]*100);
                    retorno.ValorEfetivo = Teto;
                    ProximoBSaldo = 1;
                    
                }
                else
                {
                    Performance.PerformanceST.Registra("PegaParteDaOrigem ELSE");
                    retorno.BOL = dAcordo.BOLetos[iPegaParte].BOL;
                    retorno.Proprietario = dAcordo.BOLetos[iPegaParte].BOLProprietario;
                    retorno.Descricao = string.Format("Boleto {0} vencimento {1:dd/MM/yyyy} (integral)", dAcordo.BOLetos[iPegaParte].BOL, dAcordo.BOLetos[iPegaParte].BOLVencto);
                    retorno.ValorEfetivo = dAcordo.BOLetos[iPegaParte].incTotal;
                    iPegaParte++;
                }                
            }
            return retorno;
        }

        private class DadosAPT
        {
            public string CONCodigo;
            public string BLOCodigo;
            public string APTNumero;
            public string CONNome;
            public int CON;

            public DadosAPT(int APT) 
            {
                string comandoDadosAPT = "SELECT CONCodigo, BLOCodigo, APTNumero, CONNome,CON\r\n" +
                                         "FROM         APARTAMENTOS INNER JOIN\r\n" +
                                         "BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO INNER JOIN\r\n" +
                                         "CONDOMINIOS ON BLOCOS.BLO_CON = CONDOMINIOS.CON\r\n" +
                                         "WHERE     (APARTAMENTOS.APT = @P1)";
                System.Collections.ArrayList DadosAPT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLLinha(comandoDadosAPT, APT);
                CONCodigo = DadosAPT[0].ToString();
                BLOCodigo = DadosAPT[1].ToString();
                APTNumero = DadosAPT[2].ToString();
                CONNome = DadosAPT[3].ToString();
                CON = (int)DadosAPT[4];
            }
        }

        private DadosAPT _dadosAPT;

        private DadosAPT dadosAPT {
            get { return _dadosAPT ?? (_dadosAPT = new DadosAPT(APT)); }
        }

        /// <summary>
        /// Gera efetivamente o acordo
        /// </summary>
        /// <param name="Botao"></param>
        /// <returns></returns>
        public bool Gerar(dllImpresso.Botoes.Botao Botao)
        {
            Performance.PerformanceST.Zerar();
            Performance.PerformanceST.Registra("Geral");
            Boleto.BoletosGrade.RegistraAlteracao(APT);
            string CorpoEmail = "";
            string CorpoEmailJ = "";            
            try
            {
                Performance.PerformanceST.Registra("Parte 1");
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Acordo - 449");
                if (BolsRemoverSeguro != null)
                    foreach (int nBOLSeg in BolsRemoverSeguro)                    
                        BoletosOrig[nBOLSeg].RemoveSeguro(Boleto.Boleto.TipoRemoveSeguro.soremove);                    
                CorpoEmail = string.Format("Condom�nio: {0}\r\n",dadosAPT.CONNome);
                CorpoEmail +=string.Format("Bloco: {0}\r\n",dadosAPT.BLOCodigo);
                CorpoEmail +=string.Format("Apartamento: {0}\r\n\r\n",dadosAPT.APTNumero);                
                //Cria acordo MSSQL
                const string ComandoCriaAcordo = "INSERT INTO ACOrdos (ACO_APT, ACOStatus, ACODATAI, ACOProprietario, ACOI_USU, ACOAntigo,ACOJuridico,ACOJudicial,ACOADV_FRN) VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9)";
                object oFRN;
                if ((Juridico) && (FRN != 0) && (FRN != -1))
                    oFRN = FRN;
                else
                    oFRN = DBNull.Value;
                Performance.PerformanceST.Registra("ACO");
                ACO = VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc(ComandoCriaAcordo, APT, (int)StatusACO.Ativo, DateTime.Now, Proprietario, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, 0, Juridico, Judicial, oFRN);
                if (ACO == -1)
                    throw new Exception("Erro na grava��o do acordo");
                
                //Originais
                //string comandoIncluirACOBOL = "INSERT INTO ACOxBOL  (ACO, BOL, ACOBOLOriginal) VALUES (@P1,@P2,@P3)";
                CorpoEmail += "Boletos originais:\r\n\r\n";
                CorpoEmail += "N�mero\tVencimento\tValor\r\n";
                Performance.PerformanceST.Registra("Originais");
                foreach (dAcordo.BOLetosRow rowBoletos in dAcordo.BOLetos){
                    if (rowBoletos.Incluir) {
                        BoletosOrig[rowBoletos.BOL].IncluirEmAcordo(ACO,true);                        
                        CorpoEmail += string.Format("{0}\t{1:dd/MM/yyyy}\t{2:n2}\r\n", rowBoletos.BOL, rowBoletos.BOLVencto, rowBoletos.BOLValorPrevisto);                                               
                    };
                };
                Performance.PerformanceST.Registra("Novos");
                //NOVOS
                int prestacao = 1;                
                CorpoEmailJ = CorpoEmail;
                CorpoEmail += "\r\n\r\nNovos Boletos\r\n\r\nN�mero\tVencimento\tValor\tLinha digit�vel\r\n";
                CorpoEmailJ += "\r\n\r\nNovos Boletos\r\n\r\nN�mero\tVencimento\tValor\tHonor�rios\tLinha digit�vel\r\n";
                NovosBoletosGerados = new System.Collections.SortedList();
                ZerarPegaParte();
                Performance.PerformanceST.Registra("Honorarios");
                //conta parcecelas com honorarios
                int nHonorarios = 0;
                for(int k = 0;k<dAcordo.GeraNovos.Count;k++)
                {
                    if(dAcordo.GeraNovos[k].Honorarios > 0)
                        nHonorarios++;
                };

                Performance.PerformanceST.Registra("Parte for");
                for(int k = 0;k<dAcordo.GeraNovos.Count;k++)
                {
                    Performance.PerformanceST.Registra("Parte for");
                    dAcordo.GeraNovosRow rowGeraNovo = dAcordo.GeraNovos[k];
                    Performance.PerformanceST.Registra("Competencia",true);
                    Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(rowGeraNovo.Data.Month,rowGeraNovo.Data.Year);
                    Performance.PerformanceST.StackPop();
                    //GERACAO DO BOLETO                                                                                 
                    System.Collections.ArrayList BODs = new System.Collections.ArrayList();
                    decimal Saldo = rowGeraNovo.Valor;
                    //honorario
                    if (!rowGeraNovo.IsHonorariosNull() && (rowGeraNovo.Honorarios > 0))
                    {
                        BoletosProc.Boleto.dBoletos.BOletoDetalheRow novaBODHrow = Boleto.Boleto.GerarBOD(dadosAPT.CON,
                                                                                              APT,Proprietario,
                                                                                              "119000",
                                                                                              false,
                                                                                              Comp,
                                                                                              rowGeraNovo.Data,
                                                                                              string.Format("Honor�rios: unidade {0}{1} Parc {2}/{3}", dadosAPT.BLOCodigo == "SB" ? "" : dadosAPT.BLOCodigo + "-", dadosAPT.APTNumero, prestacao, nHonorarios),
                                                                                              rowGeraNovo.Honorarios,
                                                                                              false);                        
                        if (novaBODHrow == null)
                            throw new Exception("Falha na grava��o do acordo!!!!");
                        BODs.Add(novaBODHrow.BOD);
                        Saldo -= rowGeraNovo.Honorarios;                        
                    }                    
                    while (Saldo > 0)
                    {
                        Performance.PerformanceST.Registra("PegaParteDaOrigem");
                        retParte BIncuir = PegaParteDaOrigem(Saldo,k);
                        Saldo -= BIncuir.ValorEfetivo;
                        Performance.PerformanceST.Registra("GeraBod");
                        BoletosProc.Boleto.dBoletos.BOletoDetalheRow novaBODrow = Boleto.Boleto.GerarBOD(dadosAPT.CON, APT, BIncuir.Proprietario, "111000", false, Comp, rowGeraNovo.Data, BIncuir.Descricao, BIncuir.ValorEfetivo, false, null, BIncuir.BOL);                        
                        if (novaBODrow == null)
                            throw new Exception("Falha na grava��o do acordo!!!!");
                        BODs.Add(novaBODrow.BOD);
                    };
                    //Cria Boleto
                    int NovoBOL = 0;
                    if (!rowGeraNovo.IsBOLNull())
                        NovoBOL = rowGeraNovo.BOL;
                    //bool ProibirRegistro = (dAcordo.GeraNovos.Count > 3);
                    Performance.PerformanceST.Registra("new Boleto",true);
                    Boletos.Boleto.Boleto NovoBoleto = new Boletos.Boleto.Boleto(Proprietario, Apartamento, "Acordo", Comp, rowGeraNovo.Data, "A",NovoBOL,StatusBoleto.Acordo,false, (int[])BODs.ToArray(typeof(int)));
                    Performance.PerformanceST.StackPop();
                    Performance.PerformanceST.Registra("corpo Email");
                    CorpoEmail += string.Format("{0}\t{1:dd/MM/yyyy}\t{2:n2}\t{3}\r\n", NovoBoleto.rowPrincipal.BOL, NovoBoleto.rowPrincipal.BOLVencto, NovoBoleto.rowPrincipal.BOLValorPrevisto, NovoBoleto.LinhaDigitavel);
                    CorpoEmailJ += string.Format("{0}\t{1:dd/MM/yyyy}\t{2:n2}\t{3:n2}\t{4}\r\n", NovoBoleto.rowPrincipal.BOL, NovoBoleto.rowPrincipal.BOLVencto, NovoBoleto.rowPrincipal.BOLValorPrevisto, rowGeraNovo.Honorarios, NovoBoleto.LinhaDigitavel);
                    
                    if (NovoBoleto.Encontrado == false)
                        throw new Exception("Erro ao criar boleto:\r\n" + NovoBoleto.UltimoErro);
                    NovosBoletosGerados.Add(NovoBoleto.rowPrincipal.BOL, NovoBoleto);
                    Performance.PerformanceST.Registra("incluir em acordo");
                    NovoBoleto.IncluirEmAcordo(ACO, false);
                    //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoIncluirACOBOL, ACO, NovoBoleto.rowPrincipal.BOL, false);
                    prestacao++;
                };
                Performance.PerformanceST.Registra("Comit");
                VirMSSQL.TableAdapter.STTableAdapter.Commit();                
            }
            catch (Exception e) {                
                VirMSSQL.TableAdapter.VircatchSQL(e);                
                throw new Exception("Erro no acordo: "+e.Message ,e);
                
            };
            int CON = -1;
            Performance.PerformanceST.Registra("Impresso 1");
            dllImpresso.ImpBoletoAcordo Impresso = new dllImpresso.ImpBoletoAcordo();
            if (NovosBoletosGerados != null)
                foreach (System.Collections.DictionaryEntry DE in NovosBoletosGerados)
                {
                    Boletos.Boleto.Boleto NovoBoleto = (Boletos.Boleto.Boleto)DE.Value;                    
                    if(CON == -1)
                        CON = NovoBoleto.CON;
                    NovoBoleto.CarregaLinha(Impresso, true,false);                    
                };
            if ((Botao == dllImpresso.Botoes.Botao.imprimir_frente)
                ||
                (Botao == dllImpresso.Botoes.Botao.PDF_frente)
                ||
                (Botao == dllImpresso.Botoes.Botao.email))
                Impresso.ComRemetente = false;

            Performance.PerformanceST.Registra("Impresso 2");
            switch (Botao) { 
                case dllImpresso.Botoes.Botao.email:
                    Impresso.CreateDocument();
                    if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Acordo " + ACO.ToString(), Impresso, CorpoEmail, "Acordo " + ACO.ToString()))
                        System.Windows.Forms.MessageBox.Show("E.mail enviado");
                    else
                    {
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)                        
                            System.Windows.Forms.MessageBox.Show("Falha no envio");                                                    
                        else                        
                            System.Windows.Forms.MessageBox.Show("Cancelado o envio");                        
                    };                    
                    break;
                case dllImpresso.Botoes.Botao.imprimir_frente:
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.botao:
                    Impresso.Apontamento(CON, 71);
                    Impresso.CreateDocument();
                    Impresso.Print();
                    break;                
                case dllImpresso.Botoes.Botao.pdf:
                case dllImpresso.Botoes.Botao.PDF_frente:
                    Impresso.CreateDocument();
                    System.Windows.Forms.SaveFileDialog saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
                    saveFileDialog1.DefaultExt = "pdf";
                    if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {                        
                        Impresso.ExportOptions.Pdf.Compressed = true;
                        Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                        Impresso.ExportToPdf(saveFileDialog1.FileName);
                    }
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    Impresso.CreateDocument();
                    Impresso.ShowPreviewDialog();
                    break;                    
            };
            if ((Juridico) && (FRNEmail != null) && (FRNEmail != "") && (CorpoEmailJ != ""))
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(FRNEmail, "Boletos Acordo " + ACO.ToString(), Impresso, CorpoEmailJ, "Acordo Gerado");
            
            NovosBoletosGerados = null;
            Performance.PerformanceST.Relatorio(true, true);
            return true;                    
        }
     

        StatusACO Status 
        {
            get 
            {
                if ((!Gerado) || (LinhaMae == null))
                    return StatusACO.NaoGerado;
                return (StatusACO)LinhaMae.ACOStatus;
            }
        }

        /// <summary>
        /// Cancela um acordo
        /// </summary>
        /// <param name="Motivo">Motivo do cancelamento</param>
        /// <returns></returns>
        public bool Cancelar(string Motivo)
        {
            Boleto.BoletosGrade.RegistraAlteracao(APT);
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Acordo - 1073", dAcordo.ACOrdosTableAdapter);
                //VirOleDb.TableAdapter.STTableAdapter.AbreTrasacaoST();
                if (_Cancelar(Motivo))
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    //VirOleDb.TableAdapter.STTableAdapter.Commit();
                    return true;
                }
                else  
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(new Exception("Erro no cancelamento de acordo:"+UltimoErro));
                    //VirMSSQL.TableAdapter.STTableAdapter.RollBack("Erro no cancelamento de acordo:"+UltimoErro);
                    //VirOleDb.TableAdapter.STTableAdapter.RollBack("Erro no cancelamento de acordo:" + UltimoErro);
                    return false;
                }
            }
            catch (Exception e)
            {
                //VirOleDb.TableAdapter.STTableAdapter.Vircatch(null);
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                UltimoErro = e.Message;
                return false;
            }
            //return retono;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Reabrir()
        {
            if (Status != StatusACO.Terminado)
                return false;
            try 
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Acordo - 1104",dAcordo.ACOrdosTableAdapter);
                LinhaMae.ACOStatus = (int)StatusACO.Ativo;
                LinhaMae.SetACOTerminoNull();
                dAcordo.ACOrdosTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return true;
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Encerrar()
        {
            if (!Gerado)
            {
                UltimoErro = "Acordo n�o gerado";
                return false;
            };
            //if (dAcordo.Novos.Count == 0)
            //    return false;
            DateTime UltimoPagamento = LinhaMae.ACODATAI;
            foreach (dAcordo.NovosRow rownovos in dAcordo.Novos)
                if (rownovos.IsBOLPagamentoNull())
                {
                    UltimoErro = "Acordo com parcelas em aberto";
                    dAcordo.ACOrdosTableAdapter.MarcaInternet(LinhaMae.ACO);
                    return false;
                }
                else
                    if (rownovos.BOLPagamento > UltimoPagamento)
                        UltimoPagamento = rownovos.BOLPagamento;
            LinhaMae.ACOStatus = (int)StatusACO.Terminado;
            LinhaMae.ACOTermino = UltimoPagamento;
            dAcordo.ACOrdosTableAdapter.Update(LinhaMae);
            LinhaMae.AcceptChanges();
            /*
            foreach (dAcordo.OriginaisRow rowOrig in dAcordo.Originais)
            {
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM   [Boleto - Pagamentos Acordo] WHERE ([N�mero Boleto] = @P1)", rowOrig.BOL);
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM  [Boleto - Acordo] WHERE ([N�mero Boleto] = @P1)", rowOrig.BOL);
            };
            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Dividas] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Parcelas] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Honor�rios] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM Acordo WHERE (Acordo.[N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
            */
            if ((Judicial) && (FRNEmail != null) && (FRNEmail != ""))
            {
                ABS_Apartamento Apartamento = ABS_Apartamento.GetApartamento(APT);
                string CorpoEmail = string.Format("Acordo Judicial encerrado\r\n\r\nCondom�nio: {0}\r\nBloco: {1}\r\nApartamento: {2}\r\n\r\n", Apartamento.CONCodigo, Apartamento.BLOCodigo, Apartamento.APTNumero);
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(FRNEmail, CorpoEmail, "Acordo Judicial encerrado: " + ACO.ToString());
            }
            return true;

        }

        /// <summary>
        /// 
        /// </summary>
        public void CorrigirErroConversa()
        {                                
                    
                    
                    foreach (dAcordo.NovosRow rowNovo in dAcordo.Novos)
                    {
                        Boleto.Boleto NBoleto = new Boletos.Boleto.Boleto(rowNovo.BOL);
                        if (!NBoleto.rowPrincipal.IsBOLPagamentoNull())
                            continue;
                        if (!NBoleto.Encontrado)
                        {
                            UltimoErro = string.Format("Boleto n�o encontrado:{0}", rowNovo.BOL);
                            
                        }
                        if ((NBoleto.BOLStatus != StatusBoleto.Acordo) && (NBoleto.BOLStatus != StatusBoleto.Antigo))
                        {
                            UltimoErro = string.Format("Status de Boleto incorreto.Boleto: {0} Status: {1}", rowNovo.BOL, NBoleto.BOLStatus);
                            
                        }
                        NBoleto.Cancelar();
                        
                            //NBoleto.BOLStatus = StatusBoleto.Cancelado;
                            //NBoleto.SalvarAlteracoes(string.Format("Boleto de Acordo cancelado.\r\nMotivo: {0}\r\nBoletos restaurados:\r\n{1}{2}", Motivo, Restaurados, Honorarios));
                        //NBoleto.Cancelar();
                        
                    };
                    LinhaMae.ACOStatus = (int)StatusACO.Cancelado;
                    LinhaMae.ACOTermino = DateTime.Now;
                    dAcordo.ACOrdosTableAdapter.Update(LinhaMae);
                    LinhaMae.AcceptChanges();

            /*
                    foreach (dAcordo.OriginaisRow rowOrig in dAcordo.Originais)
                    {
                        VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM   [Boleto - Pagamentos Acordo] WHERE ([N�mero Boleto] = @P1)", rowOrig.BOL);
                        VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM  [Boleto - Acordo] WHERE ([N�mero Boleto] = @P1)", rowOrig.BOL);
                    };
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Dividas] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Parcelas] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Honor�rios] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM Acordo WHERE (Acordo.[N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                    */
                    
                    
                
        }


        private bool _Cancelar(string Motivo) 
        {            
            if (!Gerado)
            {
                UltimoErro = "Acordo n�o gerado";
                return false;
            }
            switch (Status)
            {
                case StatusACO.Ativo:
                    string CorpoEmail = "";
                    System.Collections.ArrayList ListaACODivisao = new System.Collections.ArrayList();
                    CorpoEmail = string.Format("Condom�nio: {0}\r\n", dadosAPT.CONNome);
                    CorpoEmail += string.Format("Bloco: {0}\r\n", dadosAPT.BLOCodigo);
                    CorpoEmail += string.Format("Apartamento: {0}\r\n\r\n", dadosAPT.APTNumero);
                    //CorpoEmail = "Condom�nio:" + LinhaMae.c
                    //CorpoEmail += "Bloco:" + DadosAPT[1].ToString() + "\r\n";
                    //CorpoEmail += "Apartamento:" + DadosAPT[2].ToString() + "\r\n\r\n";
                    foreach (dAcordo.NovosRow rowNovo in dAcordo.Novos)
                    {
                        Boleto.Boleto NBoleto = new Boletos.Boleto.Boleto(rowNovo.BOL);
                        if (!NBoleto.Encontrado)
                        {
                            UltimoErro = string.Format("Boleto n�o encontrado:{0}",rowNovo.BOL);
                            return false;
                        }

                        if (NBoleto.BOLStatus == StatusBoleto.Valido)
                            continue;

                        if ((NBoleto.BOLStatus != StatusBoleto.Acordo) && (NBoleto.BOLStatus != StatusBoleto.Antigo))
                        {
                            UltimoErro = string.Format("Status de Boleto incorreto.Boleto: {0} Status: {1}",rowNovo.BOL, NBoleto.BOLStatus);
                            return false;
                        }
                        CorpoEmail += string.Format("Boleto: {0} Vencimento: {1:dd/MM/yyyy}", NBoleto.rowPrincipal.BOL, NBoleto.rowPrincipal.BOLVencto);
                        if (NBoleto.rowPrincipal.IsBOLPagamentoNull())
                        {
                            CorpoEmail += string.Format(" ABERTO:\r\n");
                            string Restaurados = "";
                            string Honorarios = "";
                            BoletosProc.Boleto.dBoletos.BOletoDetalheRow[] rowBODs = NBoleto.rowPrincipal.GetBOletoDetalheRows();
                            foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in rowBODs)
                            {
                                if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.honorarios))
                                {
                                    if (Honorarios == "")
                                        Honorarios = "\r\nHonorarios n�o recebidos:";
                                    Honorarios += string.Format("\r\n     {0}: {1:n2}", rowBOD.BODMensagem, rowBOD.BODValor);
                                    CorpoEmail += string.Format("   Honorarios n�o recebidos: {0}: {1:n2}\r\n\r\n", rowBOD.BODMensagem, rowBOD.BODValor);
                                }
                                else
                                    if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.acordo))
                                    {
                                        Boletos.Boleto.Boleto BoletoOrig = new Boletos.Boleto.Boleto(rowBOD.BODAcordo_BOL);
                                        if (!BoletoOrig.Encontrado)
                                        {
                                            UltimoErro = string.Format("Boleto original n�o encontrado:{0}", rowBOD.BODAcordo_BOL);
                                            return false;
                                        };
                                        Restaurados += string.Format("\r\n     Boleto: {0} Valor Original: {1:n2} Valor Acordado {2:n2}", BoletoOrig.rowPrincipal.BOL, BoletoOrig.rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9), rowBOD.BODValor.ToString("n2").PadLeft(9));
                                        switch (BoletoOrig.BOLStatus)
                                        {
                                            case StatusBoleto.Erro:
                                            case StatusBoleto.Cancelado:
                                            case StatusBoleto.Acordo:
                                            case StatusBoleto.OriginalDividido:
                                            case StatusBoleto.Parcial:
                                            case StatusBoleto.Valido:
                                            default:
                                                UltimoErro = string.Format("boleto original com status inv�lido.\r\nBoleto {0} status {1}", BoletoOrig.rowPrincipal.BOL, BoletoOrig.BOLStatus);
                                                break;
                                            case StatusBoleto.Antigo:
                                            case StatusBoleto.OriginalAcordo:
                                                BoletoOrig.BOLStatus = StatusBoleto.Valido;
                                                break;
                                            case StatusBoleto.ParcialAcordo:
                                                BoletoOrig.BOLStatus = StatusBoleto.Parcial;
                                                int ACODivisao = 0;
                                                string ComandoBusca = string.Format(
"SELECT     ACOrdos.ACO, ACOrdos.ACOStatus\r\n" +
"FROM         ACOrdos INNER JOIN\r\n" +
"                      ACOxBOL ON ACOrdos.ACO = ACOxBOL.ACO\r\n" +
"WHERE     (ACOxBOL.ACOBOLOriginal = 0) AND (ACOxBOL.BOL = @P1) AND (ACOrdos.ACOStatus = {0});", (int)StatusACO.Divide);
                                                if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(ComandoBusca, out ACODivisao, BoletoOrig.rowPrincipal.BOL))
                                                    if (!ListaACODivisao.Contains(ACODivisao))
                                                        ListaACODivisao.Add(ACODivisao);
                                                break;
                                        }
                                        BoletoOrig.rowPrincipal.BOLCancelado = false;
                                        BoletoOrig.rowPrincipal.BOLExportar = true;
                                        BoletoOrig.SalvarAlteracoes("Boleto restaurado no cancelamento do acordo");
                                        //BoletoOrig.ExportaAcces();

                                    };

                            }
                            NBoleto.BOLStatus = StatusBoleto.Cancelado;
                            NBoleto.SalvarAlteracoes(string.Format("Boleto de Acordo cancelado.\r\nMotivo: {0}\r\nBoletos restaurados:\r\n{1}{2}", Motivo, Restaurados, Honorarios));
                            NBoleto.Cancelar();
                        }
                        else
                        {
                            CorpoEmail += string.Format(" PAGO: {0:dd/MM/yyyy} - {1:n2}\r\n", NBoleto.rowPrincipal.BOLPagamento, NBoleto.rowPrincipal.BOLValorPago);
                            BoletosProc.Boleto.dBoletos.BOletoDetalheRow[] rowBODs = NBoleto.rowPrincipal.GetBOletoDetalheRows();
                            foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in rowBODs)
                            {
                                if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.acordo))
                                {
                                    Boletos.Boleto.Boleto BoletoOrig = new Boletos.Boleto.Boleto(rowBOD.BODAcordo_BOL);
                                    if (!BoletoOrig.Encontrado)
                                    {
                                        UltimoErro = string.Format("Boleto original n�o encontrado:{0}", rowBOD.BODAcordo_BOL);
                                        return false;
                                    };
                                    if (BoletoOrig.rowPrincipal.IsBOLPagamentoNull())
                                    {
                                        BoletoOrig.rowPrincipal.BOLPagamento = NBoleto.rowPrincipal.BOLPagamento;                                        
                                        BoletoOrig.SalvarAlteracoes("Boleto intermediario pago");                                       
                                    }
                                }
                            }
                        }
                    };
                    LinhaMae.ACOStatus = (int)StatusACO.Cancelado;
                    LinhaMae.ACOTermino = DateTime.Now;
                    dAcordo.ACOrdosTableAdapter.Update(LinhaMae);
                    LinhaMae.AcceptChanges();
                    /*
                    foreach (dAcordo.OriginaisRow rowOrig in dAcordo.Originais)
                    {
                        VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM   [Boleto - Pagamentos Acordo] WHERE ([N�mero Boleto] = @P1)", rowOrig.BOL);
                        VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM  [Boleto - Acordo] WHERE ([N�mero Boleto] = @P1)", rowOrig.BOL);
                    };
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Dividas] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Parcelas] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM [Acordo - Honor�rios] WHERE ([N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM Acordo WHERE (Acordo.[N�mero Acordo] = @P1)", LinhaMae.ACOAntigo);
                    */
                    if ((Juridico) && (FRNEmail != null) && (FRNEmail != "") && (CorpoEmail != ""))
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(FRNEmail,CorpoEmail, "CANCELAMENTO de Acordo " + ACO.ToString());
                    foreach (int ACOorig in ListaACODivisao)
                    {
                        Acordo Acodiv = new Acordo(ACOorig);
                        Acodiv._Cancelar("Acordo cancelado: Boleto original reconstitu�do");
                    }
                    return true;
                case StatusACO.Divide:
                    System.Collections.ArrayList NBoletos = new System.Collections.ArrayList();
                    foreach (dAcordo.NovosRow rowNovo in dAcordo.Novos)
                    {
                        Boleto.Boleto NBoleto = new Boletos.Boleto.Boleto(rowNovo.BOL);
                        if (!NBoleto.rowPrincipal.IsBOLPagamentoNull())
                            return false;
                        NBoletos.Add(NBoleto);
                    }
                    foreach (Boleto.Boleto NBoleto in NBoletos)
                    {                        
                        NBoleto.BOLStatus = StatusBoleto.Cancelado;
                        NBoleto.SalvarAlteracoes(string.Format("Boleto cancelado. Reintegrado ao original em cancelamento de acordo"));
                        NBoleto.Cancelar();
                    }
                    foreach (dAcordo.OriginaisRow rowOrig in dAcordo.Originais)
                    {
                        Boleto.Boleto OBoleto = new Boletos.Boleto.Boleto(rowOrig.BOL);
                        OBoleto.BOLStatus = StatusBoleto.Valido;
                        OBoleto.rowPrincipal.BOLCancelado = false;
                        OBoleto.rowPrincipal.BOLExportar = true;
                        OBoleto.SalvarAlteracoes(string.Format("Boleto reconstitu�do em cancelamento de acordo"));
                        //OBoleto.ExportaAcces();
                    }
                    return true;
                case StatusACO.Terminado:                   
                case StatusACO.Cancelado:                                    
                default:
                    return false;                    
            }
        }

        private static VirEnum virEnumStatusACO;

        /// <summary>
        /// VirEnum para StatusACO
        /// </summary>
        public static VirEnum VirEnumStatusACO
        {
            get
            {
                if (virEnumStatusACO == null)
                {
                    virEnumStatusACO = new VirEnum(typeof(StatusACO));
                    virEnumStatusACO.GravaNomes(StatusACO.Ativo, "Acordo em andamento");
                    virEnumStatusACO.GravaNomes(StatusACO.Cancelado, "Acordo cancelado");
                    virEnumStatusACO.GravaNomes(StatusACO.Terminado, "Acordo terminado");
                    virEnumStatusACO.GravaNomes(StatusACO.Divide, "Divis�o de boletos");
                }
                return virEnumStatusACO;
            }
        }
        
    }

    
}
