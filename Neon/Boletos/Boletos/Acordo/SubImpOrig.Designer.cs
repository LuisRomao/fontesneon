namespace Boletos.Acordo
{
    partial class SubImpOrig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.labOrigValor = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigVencimento = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigComp = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigTipo = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigNumero = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.QuadroBoletosOriginais = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel37 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.OriginaisbindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.OriginaisbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.labOrigValor,
            this.labOrigVencimento,
            this.labOrigComp,
            this.labOrigTipo,
            this.labOrigNumero});
            this.Detail.Height = 11;
            this.Detail.KeepTogether = true;
            this.Detail.KeepTogetherWithDetailReports = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // labOrigValor
            // 
            this.labOrigValor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigValor.CanGrow = false;
            this.labOrigValor.CanShrink = true;
            this.labOrigValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:n2}")});
            this.labOrigValor.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigValor.Location = new System.Drawing.Point(183, 0);
            this.labOrigValor.Multiline = true;
            this.labOrigValor.Name = "labOrigValor";
            this.labOrigValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labOrigValor.Size = new System.Drawing.Size(50, 10);
            this.labOrigValor.StylePriority.UseBorders = false;
            this.labOrigValor.Text = "R$ 999.999.99";
            this.labOrigValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labOrigVencimento
            // 
            this.labOrigVencimento.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigVencimento.CanGrow = false;
            this.labOrigVencimento.CanShrink = true;
            this.labOrigVencimento.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yyyy}")});
            this.labOrigVencimento.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigVencimento.Location = new System.Drawing.Point(125, 0);
            this.labOrigVencimento.Multiline = true;
            this.labOrigVencimento.Name = "labOrigVencimento";
            this.labOrigVencimento.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labOrigVencimento.Size = new System.Drawing.Size(54, 10);
            this.labOrigVencimento.StylePriority.UseBorders = false;
            this.labOrigVencimento.Text = "R$ 999.999.99";
            this.labOrigVencimento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labOrigComp
            // 
            this.labOrigComp.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigComp.CanGrow = false;
            this.labOrigComp.CanShrink = true;
            this.labOrigComp.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Competencia", "")});
            this.labOrigComp.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigComp.Location = new System.Drawing.Point(75, 0);
            this.labOrigComp.Multiline = true;
            this.labOrigComp.Name = "labOrigComp";
            this.labOrigComp.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labOrigComp.Size = new System.Drawing.Size(47, 10);
            this.labOrigComp.StylePriority.UseBorders = false;
            this.labOrigComp.Text = "labOrigComp";
            this.labOrigComp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // labOrigTipo
            // 
            this.labOrigTipo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigTipo.CanGrow = false;
            this.labOrigTipo.CanShrink = true;
            this.labOrigTipo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLTipoCRAI", "")});
            this.labOrigTipo.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigTipo.Location = new System.Drawing.Point(58, 0);
            this.labOrigTipo.Multiline = true;
            this.labOrigTipo.Name = "labOrigTipo";
            this.labOrigTipo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labOrigTipo.Size = new System.Drawing.Size(13, 10);
            this.labOrigTipo.StylePriority.UseBorders = false;
            this.labOrigTipo.Text = "CONDOM�NIO";
            // 
            // labOrigNumero
            // 
            this.labOrigNumero.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigNumero.CanGrow = false;
            this.labOrigNumero.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL", "")});
            this.labOrigNumero.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigNumero.Location = new System.Drawing.Point(0, 0);
            this.labOrigNumero.Multiline = true;
            this.labOrigNumero.Name = "labOrigNumero";
            this.labOrigNumero.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labOrigNumero.Size = new System.Drawing.Size(55, 10);
            this.labOrigNumero.StylePriority.UseBorders = false;
            this.labOrigNumero.Text = "labOrigNumero";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroBoletosOriginais});
            this.PageHeader.Height = 27;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QuadroBoletosOriginais
            // 
            this.QuadroBoletosOriginais.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.QuadroBoletosOriginais.BorderColor = System.Drawing.Color.Silver;
            this.QuadroBoletosOriginais.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.QuadroBoletosOriginais.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel76,
            this.xrPanel37});
            this.QuadroBoletosOriginais.Location = new System.Drawing.Point(0, 0);
            this.QuadroBoletosOriginais.Name = "QuadroBoletosOriginais";
            this.QuadroBoletosOriginais.Size = new System.Drawing.Size(249, 27);
            this.QuadroBoletosOriginais.StylePriority.UseBorders = false;
            // 
            // xrLabel76
            // 
            this.xrLabel76.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel76.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel76.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel76.Location = new System.Drawing.Point(0, 0);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.Size = new System.Drawing.Size(245, 15);
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.Text = "Boletos Originais";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel76.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel76_Draw);
            // 
            // xrPanel37
            // 
            this.xrPanel37.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLabel37});
            this.xrPanel37.Location = new System.Drawing.Point(0, 15);
            this.xrPanel37.Name = "xrPanel37";
            this.xrPanel37.Size = new System.Drawing.Size(245, 11);
            this.xrPanel37.StylePriority.UseBorders = false;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.ForeColor = System.Drawing.Color.Black;
            this.xrLabel40.Location = new System.Drawing.Point(199, 0);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.Size = new System.Drawing.Size(30, 10);
            this.xrLabel40.Text = "Valor";
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel39.ForeColor = System.Drawing.Color.Black;
            this.xrLabel39.Location = new System.Drawing.Point(133, 0);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.Size = new System.Drawing.Size(48, 10);
            this.xrLabel39.Text = "Vencimento";
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.ForeColor = System.Drawing.Color.Black;
            this.xrLabel38.Location = new System.Drawing.Point(74, 0);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.Size = new System.Drawing.Size(53, 10);
            this.xrLabel38.Text = "Compet�ncia";
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel37.ForeColor = System.Drawing.Color.Black;
            this.xrLabel37.Location = new System.Drawing.Point(8, 0);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.Size = new System.Drawing.Size(37, 10);
            this.xrLabel37.Text = "N�mero";
            // 
            // OriginaisbindingSource
            // 
            this.OriginaisbindingSource.DataSource = typeof(BoletosProc.Acordo.dAcordo.OriginaisDataTable);
            // 
            // SubImpOrig
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader});
            this.DataSource = this.OriginaisbindingSource;
            this.SnappingMode = DevExpress.XtraReports.UI.SnappingMode.None;
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(this.OriginaisbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource OriginaisbindingSource;
        private DevExpress.XtraReports.UI.XRLabel labOrigValor;
        private DevExpress.XtraReports.UI.XRLabel labOrigVencimento;
        private DevExpress.XtraReports.UI.XRLabel labOrigComp;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel labOrigTipo;
        private DevExpress.XtraReports.UI.XRLabel labOrigNumero;
        private DevExpress.XtraReports.UI.XRPanel QuadroBoletosOriginais;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRPanel xrPanel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
    }
}
