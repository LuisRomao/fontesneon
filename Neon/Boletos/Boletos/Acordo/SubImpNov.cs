using System;
using DevExpress.XtraReports.UI;

namespace Boletos.Acordo
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SubImpNov : XtraReport
    {
        private BoletosProc.Acordo.dAcordo.NovosRow LinhaMae {
            get { 
                System.Data.DataRowView DRV = (System.Data.DataRowView)GetCurrentRow();
                if (DRV == null)
                    return null;
                else
                    return (BoletosProc.Acordo.dAcordo.NovosRow)DRV.Row;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public SubImpNov()
        {
            InitializeComponent();
        }

        private int i = 1;
        

        private string ComandobuscaH = 
"SELECT     SUM(BODValor) AS Honorarios\r\n" + 
"FROM         BOletoDetalhe\r\n" + 
"WHERE     (BOD_PLA = '119000') AND (BOD_BOL = @P1);";


/*
"SELECT     [Acordo - Honor�rios].[Valor Presta��o], [Acordo - Honor�rios].Pago\r\n" + 
"FROM         ([Acordo - Honor�rios] INNER JOIN\r\n" + 
"                      [Acordo - Parcelas] ON [Acordo - Honor�rios].[N�mero presta��o] = [Acordo - Parcelas].[N�mero presta��o] AND \r\n" + 
"                      [Acordo - Honor�rios].[N�mero Acordo] = [Acordo - Parcelas].[N�mero Acordo])\r\n" + 
"WHERE     ([Acordo - Parcelas].[N�mero do Boleto] = @P1);";
*/        
        


        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            labNovParcela.Text = i.ToString();
            decimal Honorarios = 0;
            if (LinhaMae == null)
            {
                return;
            }
            VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(ComandobuscaH, out Honorarios, LinhaMae.BOL);
            if (Honorarios > 0)
            {
                xrH1.Visible = true;
                xrH1.Text = Honorarios.ToString("n2");
                xrH2.Visible = !LinhaMae.IsBOLPagamentoNull();
                
            }
            else
            {
                xrH1.Visible = xrH2.Visible = false;
            }
            i++;

            /*
            System.Data.DataRow rowH = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLRow(ComandobuscaH, LinhaMae.BOL);
            if (rowH != null)
            {
                
                xrH1.Visible = true;
                xrH1.Text = ((decimal)rowH[0]).ToString("n2");
                xrH2.Visible = ((Boolean)rowH[1]);

            }
            else {
                xrH1.Visible = false;
                
                xrH2.Visible = false;
            }
            i++;
            */
        }

        private void SubImpNov_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 1;
            
        }

        private void xrLabel19_Draw(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.CantosRedondos(sender, e, 30);
        }

        private void xrLabel10_Draw(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.CantosRedondos(sender, e, 30);
        }

    }
}
