namespace Boletos.Acordo
{
    partial class cNovoAcordo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cNovoAcordo));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridOriginais = new DevExpress.XtraGrid.GridControl();
            this.bOLetosDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOLTipoCRAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPrevisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorCorrigido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMulta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJuros = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHonorarios = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncluir = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colBOLProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.editorPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.colincValOriginal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colincEncargos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colincTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.geraNovosDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colHonorarios1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.chJuridico = new DevExpress.XtraEditors.CheckEdit();
            this.panelJuridico = new DevExpress.XtraEditors.PanelControl();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.fORNECEDORESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spinParacH = new DevExpress.XtraEditors.SpinEdit();
            this.calcHonorarios = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.LabelDestinatario = new DevExpress.XtraEditors.LabelControl();
            this.spinTotal = new DevExpress.XtraEditors.CalcEdit();
            this.dateEditINI = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.spinParcelas = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOriginais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLetosDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editorPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geraNovosDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chJuridico.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelJuridico)).BeginInit();
            this.panelJuridico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORNECEDORESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinParacH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcHonorarios.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditINI.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditINI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinParcelas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridOriginais);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 35);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1327, 538);
            this.groupControl2.TabIndex = 4;
            this.groupControl2.Text = "Boletos Originais";
            // 
            // gridOriginais
            // 
            this.gridOriginais.DataSource = this.bOLetosDataTableBindingSource;
            this.gridOriginais.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridOriginais.Location = new System.Drawing.Point(2, 20);
            this.gridOriginais.MainView = this.gridView2;
            this.gridOriginais.Name = "gridOriginais";
            this.gridOriginais.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.editorPI,
            this.repositoryItemCheckEdit1});
            this.gridOriginais.Size = new System.Drawing.Size(1323, 516);
            this.gridOriginais.TabIndex = 0;
            this.gridOriginais.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // bOLetosDataTableBindingSource
            // 
            this.bOLetosDataTableBindingSource.DataSource = typeof(BoletosProc.Acordo.dAcordo.BOLetosDataTable);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOLTipoCRAI,
            this.colBOLVencto,
            this.colBOL,
            this.colBOLValorPrevisto,
            this.colValorCorrigido,
            this.colMulta,
            this.colJuros,
            this.colSubTotal,
            this.colTotal,
            this.colHonorarios,
            this.colIncluir,
            this.colBOLProprietario,
            this.colincValOriginal,
            this.colincEncargos,
            this.colincTotal});
            this.gridView2.GridControl = this.gridOriginais;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOLVencto, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView2_RowStyle);
            this.gridView2.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView2_RowUpdated);
            // 
            // colBOLTipoCRAI
            // 
            this.colBOLTipoCRAI.Caption = "Tipo";
            this.colBOLTipoCRAI.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI.Name = "colBOLTipoCRAI";
            this.colBOLTipoCRAI.OptionsColumn.ReadOnly = true;
            this.colBOLTipoCRAI.Visible = true;
            this.colBOLTipoCRAI.VisibleIndex = 0;
            this.colBOLTipoCRAI.Width = 37;
            // 
            // colBOLVencto
            // 
            this.colBOLVencto.Caption = "Vencto";
            this.colBOLVencto.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colBOLVencto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBOLVencto.FieldName = "BOLVencto";
            this.colBOLVencto.Name = "colBOLVencto";
            this.colBOLVencto.OptionsColumn.ReadOnly = true;
            this.colBOLVencto.Visible = true;
            this.colBOLVencto.VisibleIndex = 1;
            this.colBOLVencto.Width = 66;
            // 
            // colBOL
            // 
            this.colBOL.Caption = "BOL";
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.ReadOnly = true;
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 2;
            // 
            // colBOLValorPrevisto
            // 
            this.colBOLValorPrevisto.Caption = "Valor orig";
            this.colBOLValorPrevisto.DisplayFormat.FormatString = "n2";
            this.colBOLValorPrevisto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBOLValorPrevisto.FieldName = "BOLValorPrevisto";
            this.colBOLValorPrevisto.Name = "colBOLValorPrevisto";
            this.colBOLValorPrevisto.OptionsColumn.ReadOnly = true;
            this.colBOLValorPrevisto.Visible = true;
            this.colBOLValorPrevisto.VisibleIndex = 3;
            // 
            // colValorCorrigido
            // 
            this.colValorCorrigido.Caption = "Valor Corr";
            this.colValorCorrigido.DisplayFormat.FormatString = "n2";
            this.colValorCorrigido.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorCorrigido.FieldName = "ValorCorrigido";
            this.colValorCorrigido.Name = "colValorCorrigido";
            this.colValorCorrigido.OptionsColumn.ReadOnly = true;
            // 
            // colMulta
            // 
            this.colMulta.Caption = "Multa";
            this.colMulta.DisplayFormat.FormatString = "n2";
            this.colMulta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMulta.FieldName = "Multa";
            this.colMulta.Name = "colMulta";
            this.colMulta.OptionsColumn.ReadOnly = true;
            // 
            // colJuros
            // 
            this.colJuros.Caption = "Juros";
            this.colJuros.DisplayFormat.FormatString = "n2";
            this.colJuros.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colJuros.FieldName = "Juros";
            this.colJuros.Name = "colJuros";
            this.colJuros.OptionsColumn.ReadOnly = true;
            // 
            // colSubTotal
            // 
            this.colSubTotal.Caption = "SubTotal";
            this.colSubTotal.DisplayFormat.FormatString = "n2";
            this.colSubTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSubTotal.FieldName = "SubTotal";
            this.colSubTotal.Name = "colSubTotal";
            this.colSubTotal.OptionsColumn.ReadOnly = true;
            this.colSubTotal.Visible = true;
            this.colSubTotal.VisibleIndex = 4;
            // 
            // colTotal
            // 
            this.colTotal.Caption = "Total";
            this.colTotal.DisplayFormat.FormatString = "n2";
            this.colTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.ReadOnly = true;
            // 
            // colHonorarios
            // 
            this.colHonorarios.Caption = "Honor.";
            this.colHonorarios.DisplayFormat.FormatString = "n2";
            this.colHonorarios.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colHonorarios.FieldName = "Honorarios";
            this.colHonorarios.Name = "colHonorarios";
            this.colHonorarios.OptionsColumn.ReadOnly = true;
            this.colHonorarios.Width = 67;
            // 
            // colIncluir
            // 
            this.colIncluir.Caption = "Incluir";
            this.colIncluir.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIncluir.FieldName = "Incluir";
            this.colIncluir.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colIncluir.Name = "colIncluir";
            this.colIncluir.Visible = true;
            this.colIncluir.VisibleIndex = 6;
            this.colIncluir.Width = 41;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEdit1_CheckedChanged);
            // 
            // colBOLProprietario
            // 
            this.colBOLProprietario.Caption = "Dest.";
            this.colBOLProprietario.ColumnEdit = this.editorPI;
            this.colBOLProprietario.FieldName = "BOLProprietario";
            this.colBOLProprietario.Name = "colBOLProprietario";
            this.colBOLProprietario.Visible = true;
            this.colBOLProprietario.VisibleIndex = 5;
            this.colBOLProprietario.Width = 31;
            // 
            // editorPI
            // 
            this.editorPI.AutoHeight = false;
            this.editorPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.editorPI.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.editorPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Proprietário", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 1)});
            this.editorPI.Name = "editorPI";
            this.editorPI.SmallImages = this.imagensPI1;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // colincValOriginal
            // 
            this.colincValOriginal.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colincValOriginal.AppearanceCell.Options.UseBackColor = true;
            this.colincValOriginal.Caption = "Original";
            this.colincValOriginal.DisplayFormat.FormatString = "n2";
            this.colincValOriginal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colincValOriginal.FieldName = "incValOriginal";
            this.colincValOriginal.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colincValOriginal.Name = "colincValOriginal";
            this.colincValOriginal.OptionsColumn.ReadOnly = true;
            this.colincValOriginal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incValOriginal", "{0:n2}")});
            this.colincValOriginal.Visible = true;
            this.colincValOriginal.VisibleIndex = 7;
            // 
            // colincEncargos
            // 
            this.colincEncargos.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colincEncargos.AppearanceCell.Options.UseBackColor = true;
            this.colincEncargos.Caption = "Encargos";
            this.colincEncargos.DisplayFormat.FormatString = "n2";
            this.colincEncargos.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colincEncargos.FieldName = "incEncargos";
            this.colincEncargos.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colincEncargos.Name = "colincEncargos";
            this.colincEncargos.OptionsColumn.ReadOnly = true;
            this.colincEncargos.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incEncargos", "{0:n2}")});
            this.colincEncargos.Visible = true;
            this.colincEncargos.VisibleIndex = 8;
            // 
            // colincTotal
            // 
            this.colincTotal.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colincTotal.AppearanceCell.Options.UseBackColor = true;
            this.colincTotal.Caption = "Total";
            this.colincTotal.DisplayFormat.FormatString = "n2";
            this.colincTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colincTotal.FieldName = "incTotal";
            this.colincTotal.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colincTotal.Name = "colincTotal";
            this.colincTotal.OptionsColumn.ReadOnly = true;
            this.colincTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incTotal", "{0:n2}")});
            this.colincTotal.Visible = true;
            this.colincTotal.VisibleIndex = 9;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(334, 206);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Novos Boletos";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.geraNovosDataTableBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(2, 20);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1});
            this.gridControl1.Size = new System.Drawing.Size(330, 184);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // geraNovosDataTableBindingSource
            // 
            this.geraNovosDataTableBindingSource.DataSource = typeof(BoletosProc.Acordo.dAcordo.GeraNovosDataTable);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colData,
            this.colValor,
            this.colHonorarios1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.NewItemRowText = "Novo Boleto";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colData, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colData
            // 
            this.colData.Caption = "Data";
            this.colData.DisplayFormat.FormatString = "d";
            this.colData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colData.FieldName = "Data";
            this.colData.Name = "colData";
            this.colData.Visible = true;
            this.colData.VisibleIndex = 0;
            this.colData.Width = 94;
            // 
            // colValor
            // 
            this.colValor.Caption = "Valor";
            this.colValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colValor.DisplayFormat.FormatString = "n2";
            this.colValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValor.FieldName = "Valor";
            this.colValor.Name = "colValor";
            this.colValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:n2}")});
            this.colValor.Visible = true;
            this.colValor.VisibleIndex = 1;
            this.colValor.Width = 88;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            this.repositoryItemCalcEdit1.Precision = 2;
            // 
            // colHonorarios1
            // 
            this.colHonorarios1.Caption = "Honorários";
            this.colHonorarios1.DisplayFormat.FormatString = "n2";
            this.colHonorarios1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colHonorarios1.FieldName = "Honorarios";
            this.colHonorarios1.Name = "colHonorarios1";
            this.colHonorarios1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Honorarios", "{0:n2}")});
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.cBotaoImpBol1);
            this.groupControl3.Controls.Add(this.chJuridico);
            this.groupControl3.Controls.Add(this.panelJuridico);
            this.groupControl3.Controls.Add(this.imageComboBoxEdit1);
            this.groupControl3.Controls.Add(this.LabelDestinatario);
            this.groupControl3.Controls.Add(this.spinTotal);
            this.groupControl3.Controls.Add(this.dateEditINI);
            this.groupControl3.Controls.Add(this.labelControl3);
            this.groupControl3.Controls.Add(this.labelControl2);
            this.groupControl3.Controls.Add(this.spinParcelas);
            this.groupControl3.Controls.Add(this.labelControl1);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(336, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(989, 206);
            this.groupControl3.TabIndex = 6;
            this.groupControl3.Text = "Novos Boletos";
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(7, 153);
            this.cBotaoImpBol1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(167, 29);
            this.cBotaoImpBol1.TabIndex = 14;
            this.cBotaoImpBol1.Titulo = "Gerar Acordo";
            this.cBotaoImpBol1.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // chJuridico
            // 
            this.chJuridico.Location = new System.Drawing.Point(251, 19);
            this.chJuridico.Name = "chJuridico";
            this.chJuridico.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chJuridico.Properties.Appearance.Options.UseFont = true;
            this.chJuridico.Properties.Caption = "JURÍDICO";
            this.chJuridico.Size = new System.Drawing.Size(116, 23);
            this.chJuridico.TabIndex = 13;
            this.chJuridico.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // panelJuridico
            // 
            this.panelJuridico.Controls.Add(this.lookUpEdit1);
            this.panelJuridico.Controls.Add(this.spinParacH);
            this.panelJuridico.Controls.Add(this.calcHonorarios);
            this.panelJuridico.Controls.Add(this.labelControl5);
            this.panelJuridico.Controls.Add(this.labelControl4);
            this.panelJuridico.Controls.Add(this.checkEdit1);
            this.panelJuridico.Location = new System.Drawing.Point(180, 44);
            this.panelJuridico.Name = "panelJuridico";
            this.panelJuridico.Size = new System.Drawing.Size(208, 138);
            this.panelJuridico.TabIndex = 12;
            this.panelJuridico.Visible = false;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Location = new System.Drawing.Point(5, 109);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNEmail", "Name2")});
            this.lookUpEdit1.Properties.DataSource = this.fORNECEDORESBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "FRNNome";
            this.lookUpEdit1.Properties.ShowHeader = false;
            this.lookUpEdit1.Properties.ValueMember = "FRN";
            this.lookUpEdit1.Properties.PopupFilter += new DevExpress.XtraEditors.Controls.PopupFilterEventHandler(this.lookUpEdit1_Properties_PopupFilter);
            this.lookUpEdit1.Size = new System.Drawing.Size(193, 20);
            this.lookUpEdit1.TabIndex = 16;
            this.lookUpEdit1.EditValueChanged += new System.EventHandler(this.lookUpEdit1_EditValueChanged);
            // 
            // fORNECEDORESBindingSource
            // 
            this.fORNECEDORESBindingSource.DataMember = "FRNLookup";
            this.fORNECEDORESBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // spinParacH
            // 
            this.spinParacH.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinParacH.Location = new System.Drawing.Point(106, 74);
            this.spinParacH.Name = "spinParacH";
            this.spinParacH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinParacH.Properties.Appearance.Options.UseFont = true;
            this.spinParacH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinParacH.Properties.IsFloatValue = false;
            this.spinParacH.Properties.Mask.EditMask = "N00";
            this.spinParacH.Properties.MaxValue = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.spinParacH.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinParacH.Properties.EditValueChanged += new System.EventHandler(this.spinParcelas_Properties_EditValueChanged);
            this.spinParacH.Size = new System.Drawing.Size(92, 26);
            this.spinParacH.TabIndex = 15;
            this.spinParacH.EditValueChanged += new System.EventHandler(this.spinParacH_EditValueChanged);
            // 
            // calcHonorarios
            // 
            this.calcHonorarios.Location = new System.Drawing.Point(106, 42);
            this.calcHonorarios.Name = "calcHonorarios";
            this.calcHonorarios.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcHonorarios.Properties.Appearance.Options.UseFont = true;
            this.calcHonorarios.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcHonorarios.Properties.DisplayFormat.FormatString = "n2";
            this.calcHonorarios.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcHonorarios.Properties.EditFormat.FormatString = "n2";
            this.calcHonorarios.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcHonorarios.Properties.Mask.EditMask = "n2";
            this.calcHonorarios.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcHonorarios.Properties.Precision = 2;
            this.calcHonorarios.Size = new System.Drawing.Size(92, 26);
            this.calcHonorarios.TabIndex = 14;
            this.calcHonorarios.EditValueChanged += new System.EventHandler(this.calcHonorarios_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(5, 75);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(75, 19);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Parcelas:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 45);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(95, 19);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Honorários:";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(4, 8);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Caption = "JUDICIAL";
            this.checkEdit1.Size = new System.Drawing.Size(116, 23);
            this.checkEdit1.TabIndex = 11;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(117, 24);
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Proprietário", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 1)});
            this.imageComboBoxEdit1.Properties.SmallImages = this.imagensPI1;
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(111, 20);
            this.imageComboBoxEdit1.TabIndex = 10;
            this.imageComboBoxEdit1.EditValueChanged += new System.EventHandler(this.imageComboBoxEdit1_EditValueChanged);
            // 
            // LabelDestinatario
            // 
            this.LabelDestinatario.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDestinatario.Appearance.Options.UseFont = true;
            this.LabelDestinatario.Location = new System.Drawing.Point(4, 22);
            this.LabelDestinatario.Name = "LabelDestinatario";
            this.LabelDestinatario.Size = new System.Drawing.Size(107, 19);
            this.LabelDestinatario.TabIndex = 9;
            this.LabelDestinatario.Text = "Destinatário:";
            // 
            // spinTotal
            // 
            this.spinTotal.Location = new System.Drawing.Point(61, 50);
            this.spinTotal.Name = "spinTotal";
            this.spinTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinTotal.Properties.Appearance.Options.UseFont = true;
            this.spinTotal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinTotal.Properties.DisplayFormat.FormatString = "n2";
            this.spinTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinTotal.Properties.EditFormat.FormatString = "n2";
            this.spinTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinTotal.Properties.Mask.EditMask = "n2";
            this.spinTotal.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinTotal.Properties.Precision = 2;
            this.spinTotal.Size = new System.Drawing.Size(117, 26);
            this.spinTotal.TabIndex = 8;
            this.spinTotal.EditValueChanged += new System.EventHandler(this.spinTotal_EditValueChanged);
            this.spinTotal.Validating += new System.ComponentModel.CancelEventHandler(this.spinTotal_Validating);
            // 
            // dateEditINI
            // 
            this.dateEditINI.EditValue = null;
            this.dateEditINI.Location = new System.Drawing.Point(61, 116);
            this.dateEditINI.Name = "dateEditINI";
            this.dateEditINI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditINI.Properties.Appearance.Options.UseFont = true;
            this.dateEditINI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditINI.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditINI.Size = new System.Drawing.Size(117, 26);
            this.dateEditINI.TabIndex = 7;
            this.dateEditINI.EditValueChanged += new System.EventHandler(this.dateEditINI_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(6, 119);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(45, 19);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Data:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(6, 89);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(47, 19);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Parc.:";
            // 
            // spinParcelas
            // 
            this.spinParcelas.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinParcelas.Location = new System.Drawing.Point(61, 84);
            this.spinParcelas.Name = "spinParcelas";
            this.spinParcelas.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinParcelas.Properties.Appearance.Options.UseFont = true;
            this.spinParcelas.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinParcelas.Properties.IsFloatValue = false;
            this.spinParcelas.Properties.Mask.EditMask = "N00";
            this.spinParcelas.Properties.MaxValue = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.spinParcelas.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinParcelas.Properties.EditValueChanged += new System.EventHandler(this.spinParcelas_Properties_EditValueChanged);
            this.spinParcelas.Size = new System.Drawing.Size(117, 26);
            this.spinParcelas.TabIndex = 4;
            this.spinParcelas.EditValueChanged += new System.EventHandler(this.spinParcelas_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(6, 53);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(49, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Total:";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 573);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1327, 210);
            this.panelControl1.TabIndex = 7;
            // 
            // cNovoAcordo
            // 
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cNovoAcordo";
            this.Size = new System.Drawing.Size(1327, 808);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridOriginais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLetosDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editorPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geraNovosDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chJuridico.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelJuridico)).EndInit();
            this.panelJuridico.ResumeLayout(false);
            this.panelJuridico.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORNECEDORESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinParacH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcHonorarios.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditINI.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditINI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinParcelas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPrevisto;
        private DevExpress.XtraGrid.Columns.GridColumn colValorCorrigido;
        private DevExpress.XtraGrid.Columns.GridColumn colMulta;
        private DevExpress.XtraGrid.Columns.GridColumn colJuros;
        private DevExpress.XtraGrid.Columns.GridColumn colSubTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colHonorarios;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraGrid.GridControl gridOriginais;
        private DevExpress.XtraGrid.Columns.GridColumn colIncluir;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource geraNovosDataTableBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colData;
        private DevExpress.XtraGrid.Columns.GridColumn colValor;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource bOLetosDataTableBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLProprietario;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox editorPI;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditINI;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit spinParcelas;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.CalcEdit spinTotal;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl LabelDestinatario;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit chJuridico;
        private DevExpress.XtraEditors.PanelControl panelJuridico;
        private DevExpress.XtraEditors.SpinEdit spinParacH;
        private DevExpress.XtraEditors.CalcEdit calcHonorarios;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn colHonorarios1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.BindingSource fORNECEDORESBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colincValOriginal;
        private DevExpress.XtraGrid.Columns.GridColumn colincEncargos;
        private DevExpress.XtraGrid.Columns.GridColumn colincTotal;
    }
}
