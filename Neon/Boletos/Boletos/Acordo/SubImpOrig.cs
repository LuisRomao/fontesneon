using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Boletos.Acordo
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SubImpOrig : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// 
        /// </summary>
        public SubImpOrig()
        {
            InitializeComponent();
        }

        private void xrLabel76_Draw(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.CantosRedondos(sender, e, 30);
        }

    }
}
