namespace Boletos.Acordo
{
    partial class SubImpNovDet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.labNovParcela = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovNumero = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovVencimento = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovValor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlinhasDet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlinhasDetVal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.NovosbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.NovosbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel3});
            this.Detail.HeightF = 12.00002F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrPanel3
            // 
            this.xrPanel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrPanel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.labNovParcela,
            this.labNovNumero,
            this.labNovVencimento,
            this.labNovValor,
            this.xrlinhasDet,
            this.xrlinhasDetVal,
            this.xrLabel1});
            this.xrPanel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.119276E-05F);
            this.xrPanel3.Name = "xrPanel3";
            this.xrPanel3.SizeF = new System.Drawing.SizeF(747.9167F, 12F);
            this.xrPanel3.StylePriority.UseBorders = false;
            // 
            // labNovParcela
            // 
            this.labNovParcela.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovParcela.CanGrow = false;
            this.labNovParcela.CanShrink = true;
            this.labNovParcela.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovParcela.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.labNovParcela.Multiline = true;
            this.labNovParcela.Name = "labNovParcela";
            this.labNovParcela.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labNovParcela.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.labNovParcela.StylePriority.UseBorders = false;
            this.labNovParcela.StylePriority.UseTextAlignment = false;
            this.labNovParcela.Text = "00";
            this.labNovParcela.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labNovNumero
            // 
            this.labNovNumero.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovNumero.CanGrow = false;
            this.labNovNumero.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.labNovNumero.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovNumero.LocationFloat = new DevExpress.Utils.PointFloat(15F, 0F);
            this.labNovNumero.Multiline = true;
            this.labNovNumero.Name = "labNovNumero";
            this.labNovNumero.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labNovNumero.SizeF = new System.Drawing.SizeF(50F, 10.00001F);
            this.labNovNumero.StylePriority.UseBorders = false;
            this.labNovNumero.StylePriority.UseTextAlignment = false;
            this.labNovNumero.Text = "PROPRIET�RIO";
            this.labNovNumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labNovVencimento
            // 
            this.labNovVencimento.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovVencimento.CanGrow = false;
            this.labNovVencimento.CanShrink = true;
            this.labNovVencimento.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yyyy}")});
            this.labNovVencimento.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovVencimento.LocationFloat = new DevExpress.Utils.PointFloat(65.00001F, 1.818989E-12F);
            this.labNovVencimento.Multiline = true;
            this.labNovVencimento.Name = "labNovVencimento";
            this.labNovVencimento.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labNovVencimento.SizeF = new System.Drawing.SizeF(72.15971F, 10.00002F);
            this.labNovVencimento.StylePriority.UseBorders = false;
            this.labNovVencimento.Text = "R$ 999.999.99";
            this.labNovVencimento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labNovValor
            // 
            this.labNovValor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovValor.CanGrow = false;
            this.labNovValor.CanShrink = true;
            this.labNovValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:n2}")});
            this.labNovValor.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovValor.LocationFloat = new DevExpress.Utils.PointFloat(137.1597F, 0F);
            this.labNovValor.Multiline = true;
            this.labNovValor.Name = "labNovValor";
            this.labNovValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labNovValor.SizeF = new System.Drawing.SizeF(58.11112F, 9.999996F);
            this.labNovValor.StylePriority.UseBorders = false;
            this.labNovValor.Text = "R$ 999.999.99";
            this.labNovValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrlinhasDet
            // 
            this.xrlinhasDet.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlinhasDet.Font = new System.Drawing.Font("Tahoma", 7F);
            this.xrlinhasDet.LocationFloat = new DevExpress.Utils.PointFloat(206.1805F, 0F);
            this.xrlinhasDet.Multiline = true;
            this.xrlinhasDet.Name = "xrlinhasDet";
            this.xrlinhasDet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlinhasDet.SizeF = new System.Drawing.SizeF(371.257F, 10.00001F);
            this.xrlinhasDet.StylePriority.UseBorders = false;
            this.xrlinhasDet.StylePriority.UseTextAlignment = false;
            this.xrlinhasDet.Text = "CONDOM�NIO";
            this.xrlinhasDet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrlinhasDetVal
            // 
            this.xrlinhasDetVal.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlinhasDetVal.Font = new System.Drawing.Font("Tahoma", 7F);
            this.xrlinhasDetVal.LocationFloat = new DevExpress.Utils.PointFloat(583.0868F, 0F);
            this.xrlinhasDetVal.Multiline = true;
            this.xrlinhasDetVal.Name = "xrlinhasDetVal";
            this.xrlinhasDetVal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlinhasDetVal.SizeF = new System.Drawing.SizeF(70.63196F, 10.00002F);
            this.xrlinhasDetVal.StylePriority.UseBorders = false;
            this.xrlinhasDetVal.StylePriority.UseTextAlignment = false;
            this.xrlinhasDetVal.Text = "CONDOM�NIO";
            this.xrlinhasDetVal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.CanGrow = false;
            this.xrLabel1.CanShrink = true;
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLPagamento", "{0:dd/MM/yyyy}")});
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 7F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(677.2849F, 0F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(70.63188F, 10.00001F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TopMargin
            // 
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NovosbindingSource
            // 
            this.NovosbindingSource.DataSource = typeof(BoletosProc.Acordo.dAcordo.NovosDataTable);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.PageHeader.HeightF = 27F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrPanel1
            // 
            this.xrPanel1.BorderColor = System.Drawing.Color.Silver;
            this.xrPanel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel2,
            this.xrLabel10});
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(747.9167F, 27F);
            this.xrPanel1.StylePriority.UseBorders = false;
            // 
            // xrPanel2
            // 
            this.xrPanel2.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel2,
            this.xrLabel5,
            this.xrLabel7,
            this.xrLabel8,
            this.xrLabel9});
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 14.99998F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(747.9167F, 11F);
            this.xrPanel2.StylePriority.UseBorders = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.ForeColor = System.Drawing.Color.Black;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(206.1805F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(220.9722F, 10F);
            this.xrLabel4.Text = "Conte�do do boleto";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Black;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(612.9479F, 2.861023E-06F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(40.77081F, 9.999996F);
            this.xrLabel2.Text = "Valor";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.ForeColor = System.Drawing.Color.Black;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(677.2848F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(70.63188F, 9.999996F);
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Pagamento";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.Black;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(154.868F, 0F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(30F, 10F);
            this.xrLabel7.Text = "Valor";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.ForeColor = System.Drawing.Color.Black;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(95.61111F, 1.000002F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(26.74998F, 9.999991F);
            this.xrLabel8.Text = "Venc.";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.ForeColor = System.Drawing.Color.Black;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(26.04167F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(37F, 10F);
            this.xrLabel9.Text = "N�mero";
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(747.9167F, 14.99999F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.Text = "Novos Boletos";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel10.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // SubImpNovDet
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader});
            this.DataSource = this.NovosbindingSource;
            this.Landscape = true;
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.SnappingMode = DevExpress.XtraReports.UI.SnappingMode.None;
            this.Version = "10.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SubImpNovDet_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.NovosbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource NovosbindingSource;
        private DevExpress.XtraReports.UI.XRLabel labNovParcela;
        private DevExpress.XtraReports.UI.XRLabel labNovValor;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel labNovNumero;
        private DevExpress.XtraReports.UI.XRLabel labNovVencimento;
        private DevExpress.XtraReports.UI.XRLabel xrlinhasDet;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrlinhasDetVal;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRPanel xrPanel3;
    }
}
