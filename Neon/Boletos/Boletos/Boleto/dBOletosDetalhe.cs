﻿using System.Collections;
using System.Data;
using CompontesBasicos.Performance;

namespace Boletos.Boleto {


    partial class dBOletosDetalhe
    {
        private dBOletosDetalheTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalhe
        /// </summary>
        public dBOletosDetalheTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dBOletosDetalheTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheTableAdapter;
            }
        }

        private ArrayList _CamposNaoCopiar;
        private ArrayList CamposNaoCopiar
        {
            get{ return _CamposNaoCopiar ?? (_CamposNaoCopiar = new ArrayList(new string[] { "BOD", "BOD_BOL", "BODValor" }));}
        }

        /*
        public int[] Dividir(int BODorigem, decimal Valor1) 
        {
            int[] Retorno = new int[2];
            BOletoDetalheTableAdapter.FillByBOD(BOletoDetalhe, BODorigem);
            BOletoDetalheRow rowOrig = BOletoDetalhe[0];
            BOletoDetalheRow rowB1;
            BOletoDetalheRow rowB2;
            if (Valor1 != 0)
            {
                rowB1 = BOletoDetalhe.NewBOletoDetalheRow();
                foreach (DataColumn DC in BOletoDetalhe.Columns)
                    if (!CamposNaoCopiar.Contains(DC.ColumnName))
                        rowB1[DC] = rowOrig[DC];
                rowB1.BODValor = Valor1;
                if ((Valor1 != rowOrig.BODValor) && (rowB1.BODMensagem.Length < 30))
                    rowB1.BODMensagem += " (Pagamento Parcial)";
                else
                    if ((Valor1 != rowOrig.BODValor) && (rowB1.BODMensagem.Length < 40))
                        rowB1.BODMensagem += " (Parcial)";
                BOletoDetalhe.AddBOletoDetalheRow(rowB1);
                bOletoDetalheTableAdapter.Update(rowB1);
                rowB1.AcceptChanges();
                Retorno[0] = rowB1.BOD;
            };
            if (Valor1 != rowOrig.BODValor)
            {
                rowB2 = BOletoDetalhe.NewBOletoDetalheRow();
                foreach (DataColumn DC in BOletoDetalhe.Columns)
                    if (!CamposNaoCopiar.Contains(DC.ColumnName))
                        rowB2[DC] = rowOrig[DC];
                rowB2.BODValor = rowOrig.BODValor - Valor1;
                if ((Valor1 != 0) && (rowB2.BODMensagem.Length < 30))
                    rowB2.BODMensagem += " (Pagamento Parcial)";
                else
                    if ((Valor1 != 0) && (rowB2.BODMensagem.Length < 40))
                        rowB2.BODMensagem += " (Parcial)";
                BOletoDetalhe.AddBOletoDetalheRow(rowB2);
                bOletoDetalheTableAdapter.Update(rowB2);
                rowB2.AcceptChanges();
                Retorno[1] = rowB2.BOD;
            };
            rowOrig.SetBOD_EVENull();
            rowOrig.SetBOD_GASNull();       
            rowOrig.SetBOD_PBONull();
            rowOrig.SetBOD_PGENull();
            rowOrig.SetBOD_RADNull();
            bOletoDetalheTableAdapter.Update(rowOrig);
            rowOrig.AcceptChanges();
            return Retorno;
        }*/        

        /// <summary>
        /// Dividir BOD gerando novos que substituem o original que continua existindo
        /// </summary>
        /// <param name="BODorigem"></param>
        /// <param name="Valorx"></param>
        /// <param name="ManterOriginal">Mantem o original e gera novos ou altera o original sendo o primeiro do vetor de retorno</param>
        /// <param name="MarcarParcialNaDescricao">Coloca 'parcial' na descrição</param>
        /// <returns>o BOD dos registros gerados que vão substituir o original</returns>
        /// <remarks>os valores zero no Valorx resultam em null no retorno</remarks>
        public int[] DividirBOD(int BODorigem,bool ManterOriginal,bool MarcarParcialNaDescricao, params decimal[] Valorx)
        {
            Performance.PerformanceST.Registra("DividirBOD");
            if (Valorx.Length == 0)
                throw new System.ArgumentException("Nenhum Valor", "Valorx");
            if(!ManterOriginal && (Valorx[0] == 0))
                throw new System.ArgumentException("O primeiro valor não pode ser zero se não vai manter o original", "Valorx");
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos dBoletosDetalhe - 140", BOletoDetalheTableAdapter);
                int[] Retorno = new int[Valorx.Length];
                Performance.PerformanceST.Registra("DividirBOD FILL", true);
                BOletoDetalheTableAdapter.FillByBOD(BOletoDetalhe, BODorigem);
                Performance.PerformanceST.StackPop();
                BOletoDetalheRow rowOrig = BOletoDetalhe[0];
                BOletoDetalheRow rowB;
                int i = 0;
                if (rowOrig.BODMensagem.Contains("(integral)"))
                    rowOrig.BODMensagem = rowOrig.BODMensagem.Replace("(integral)", "").Trim();
                if (!ManterOriginal)
                {
                    rowOrig.BODValor = Valorx[0];
                    if ((MarcarParcialNaDescricao) && (!rowOrig.BODMensagem.Contains("Parcial")))
                    {
                        if ((rowOrig.BODMensagem.Length < 30))
                            rowOrig.BODMensagem += " (Pagamento Parcial)";
                        else if (rowOrig.BODMensagem.Length < 40)
                            rowOrig.BODMensagem += " (Parcial)";
                    };
                    Retorno[0] = rowOrig.BOD;
                    i = 1;
                }
                Performance.PerformanceST.Registra("DividirBOD RAS", true);
                int? BODRAS = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select BODOrigem from BODxBOD where BODxBOD.BODDestino = @P1", rowOrig.BOD);
                Performance.PerformanceST.StackPop();
                for (; i < Valorx.Length; i++)
                    if (Valorx[i] != 0)
                    {
                        rowB = BOletoDetalhe.NewBOletoDetalheRow();
                        foreach (DataColumn DC in BOletoDetalhe.Columns)
                            if (!CamposNaoCopiar.Contains(DC.ColumnName))
                                rowB[DC] = rowOrig[DC];
                        rowB.BODValor = Valorx[i];
                        if (MarcarParcialNaDescricao && (Valorx[i] != rowOrig.BODValor))
                        {
                            if ((rowB.BODMensagem.Length < 30))
                                rowB.BODMensagem += " (Pagamento Parcial)";
                            else if (rowB.BODMensagem.Length < 40)
                                rowB.BODMensagem += " (Parcial)";
                        }
                        BOletoDetalhe.AddBOletoDetalheRow(rowB);
                        Performance.PerformanceST.Registra("DividirBOD UPDATE", true);
                        bOletoDetalheTableAdapter.Update(rowB);
                        Performance.PerformanceST.StackPop();
                        rowB.AcceptChanges();
                        Retorno[i] = rowB.BOD;
                        if (BODRAS.HasValue)
                        {
                            Performance.PerformanceST.Registra("DividirBOD INSERT", true);
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("INSERT INTO BODxBOD (BODOrigem, BODDestino) VALUES (@P1,@P2)", BODRAS.Value, rowB.BOD);
                            Performance.PerformanceST.StackPop();
                        }
                    }
                //rowOrig.SetBOD_EVENull();
                //rowOrig.SetBOD_GASNull();
                //rowOrig.SetBOD_PBONull();
                //rowOrig.SetBOD_PGENull();
                //rowOrig.SetBOD_RADNull();
                Performance.PerformanceST.Registra("DividirBOD UPDATE 2", true);
                BOletoDetalheTableAdapter.Update(rowOrig);
                Performance.PerformanceST.StackPop();
                rowOrig.AcceptChanges();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return Retorno;
            }
            catch (System.Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                return null;
            }
        }

    }
}
