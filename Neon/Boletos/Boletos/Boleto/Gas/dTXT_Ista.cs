﻿namespace Boletos.Boleto.Gas {


    partial class dTXT_Ista
    {
        private dTXT_IstaTableAdapters.APTxIGATableAdapter aPTxIGATableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APTxIGA
        /// </summary>
        public dTXT_IstaTableAdapters.APTxIGATableAdapter APTxIGATableAdapter
        {
            get
            {
                if (aPTxIGATableAdapter == null)
                {
                    aPTxIGATableAdapter = new dTXT_IstaTableAdapters.APTxIGATableAdapter();
                    aPTxIGATableAdapter.TrocarStringDeConexao();
                };
                return aPTxIGATableAdapter;
            }
        }

        private dTXT_IstaTableAdapters.ImportaGasTableAdapter importaGasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ImportaGas
        /// </summary>
        public dTXT_IstaTableAdapters.ImportaGasTableAdapter ImportaGasTableAdapter
        {
            get
            {
                if (importaGasTableAdapter == null)
                {
                    importaGasTableAdapter = new dTXT_IstaTableAdapters.ImportaGasTableAdapter();
                    importaGasTableAdapter.TrocarStringDeConexao();
                };
                return importaGasTableAdapter;
            }
        }
    }
}


