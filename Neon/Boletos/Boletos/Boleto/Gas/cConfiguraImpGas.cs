using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace Boletos.Boleto.Gas
{
    /// <summary>
    /// cConfiguraImpGas
    /// </summary>
    public partial class cConfiguraImpGas : CompontesBasicos.ComponenteBase
    {
        private dTXT_Ista DS;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_DS"></param>
        public cConfiguraImpGas(dTXT_Ista _DS)
        {
            InitializeComponent();
            DS = _DS;
            bindingSource1.DataSource = DS;
            AjustaSegue();
        }

        private GridHitInfo downHitInfo = null;
        private GridHitInfo downHitInfo2 = null;

       

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None)
                return;
            if (e.Button == MouseButtons.Left && hitInfo.RowHandle >= 0)
                downHitInfo = hitInfo;
        }

        private void gridView2_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfo.HitPoint.X - dragSize.Width / 2,
                    downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    dTXT_Ista.NaoCadastradosRow row = (dTXT_Ista.NaoCadastradosRow)view.GetDataRow(downHitInfo.RowHandle);
                    view.GridControl.DoDragDrop(row, DragDropEffects.Move);
                    downHitInfo = null;
                    DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = true;
                }
            }
        }

        

        private void gridControl1_DragOver(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent(typeof(dTXT_Ista.NaoCadastradosRow)))
            {                
                GridHitInfo hitInfoMove = bandedGridView1.CalcHitInfo(gridControl1.PointToClient(new Point(e.X, e.Y)));
                if (hitInfoMove.RowHandle >= 0)
                {
                    bandedGridView1.FocusedRowHandle = hitInfoMove.RowHandle;
                    dTXT_Ista.APTxIGARow candidato = (dTXT_Ista.APTxIGARow)bandedGridView1.GetDataRow(hitInfoMove.RowHandle);
                    if((candidato == null) || (!candidato.IsIGAApartamentoNull()))
                        e.Effect = DragDropEffects.None;
                    else
                        e.Effect = DragDropEffects.Move;
                }
                else
                    e.Effect = DragDropEffects.None;
                
            }
            else
                e.Effect = DragDropEffects.None;
        }

        private void gridControl1_DragDrop(object sender, DragEventArgs e)
        {
            GridControl grid = sender as GridControl;
            //Referencia OneNote: convers�o de coordenadas   em cConfiguraImpGas.cs   boletos.dll 11.1.7.9
            //GridHitInfo hitInfoDestino = bandedGridView1.CalcHitInfo(new Point(e.X, e.Y));
            GridHitInfo hitInfoDestino = bandedGridView1.CalcHitInfo(gridControl1.PointToClient(new Point(e.X, e.Y)));
            if (hitInfoDestino.RowHandle < 0)
                return;
            dTXT_Ista.APTxIGARow rowDestino = (dTXT_Ista.APTxIGARow)bandedGridView1.GetFocusedDataRow();
            if (rowDestino == null)
                return;


            dTXT_Ista.NaoCadastradosRow row = e.Data.GetData(typeof(dTXT_Ista.NaoCadastradosRow)) as dTXT_Ista.NaoCadastradosRow;
            


            if (row != null)
            {
                rowDestino.IGABLOCO = row.Bloco;
                rowDestino.IGAApartamento = row.APT;
                
                dTXT_Ista.ImportaGasRow rowIGA;
                if (rowDestino.IsIGANull())
                {
                    rowIGA = DS.ImportaGas.NewImportaGasRow();
                    rowIGA.IGA_APT = rowDestino.APT;                    
                    rowIGA.IGAFornecedor = 0;                   
                }
                else
                    rowIGA = DS.ImportaGas.FindByIGA(rowDestino.IGA);
                rowIGA.IGAApartamento = row.APT;
                rowIGA.IGABLOCO = row.Bloco;
                if (rowIGA.RowState == DataRowState.Detached)
                    DS.ImportaGas.AddImportaGasRow(rowIGA);
                DS.ImportaGasTableAdapter.Update(rowIGA);
                rowIGA.AcceptChanges();
                rowDestino.IGA = rowIGA.IGA;
                row.Delete();
            }
            AjustaSegue();
        }

        

        private void pictureEdit1_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(dTXT_Ista.APTxIGARow)))
                e.Effect = DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
        }

        private void bandedGridView1_MouseDown_1(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            downHitInfo2 = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None)
                return;
            if (e.Button == MouseButtons.Left && hitInfo.RowHandle >= 0)
            {
                dTXT_Ista.APTxIGARow candidato = (dTXT_Ista.APTxIGARow)bandedGridView1.GetDataRow(hitInfo.RowHandle);
                if ((candidato != null) || (!candidato.IsIGANull()))
                {
                    downHitInfo2 = hitInfo;
                }
            }
        }

        private void bandedGridView1_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo2 != null)
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfo2.HitPoint.X - dragSize.Width / 2,
                    downHitInfo2.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    dTXT_Ista.APTxIGARow row = (dTXT_Ista.APTxIGARow)view.GetDataRow(downHitInfo2.RowHandle);
                    view.GridControl.DoDragDrop(row, DragDropEffects.Move);
                    downHitInfo2 = null;
                    DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = true;
                }
            }            
        }

        private void pictureEdit1_DragDrop(object sender, DragEventArgs e)
        {
            
            dTXT_Ista.APTxIGARow row = e.Data.GetData(typeof(dTXT_Ista.APTxIGARow)) as dTXT_Ista.APTxIGARow;
            if((row.IsIGABLOCONull()) || (row.IsIGAApartamentoNull()))
                return;
            dTXT_Ista.NaoCadastradosRow rowNao = DS.NaoCadastrados.FindByBlocoAPT(row.IGABLOCO,row.IGAApartamento);
            if (rowNao == null)
                DS.NaoCadastrados.AddNaoCadastradosRow(row.IGABLOCO, row.IGAApartamento);
            
            dTXT_Ista.ImportaGasRow rowIGA = DS.ImportaGas.FindByIGA(row.IGA);
            if (rowIGA != null)
            {
                rowIGA.Delete();
                DS.ImportaGasTableAdapter.Update(DS.ImportaGas);
                DS.ImportaGas.AcceptChanges();
            };
            row.SetIGAApartamentoNull();
            row.SetIGABLOCONull();
            row.SetIGANull();            
            AjustaSegue();           
        }

        private void AjustaSegue()
        {
            BTsegue.Enabled = DS.NaoCadastrados.Count == 0;
        }

        private void BTsegue_Click(object sender, EventArgs e)
        {
            if (DS.NaoCadastrados.Count == 0)
                FechaTela(DialogResult.OK);
        }


    }
}
