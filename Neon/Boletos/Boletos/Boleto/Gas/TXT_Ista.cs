using System;
using System.Collections.Generic;
using System.Text;
using ArquivosTXT;

namespace Boletos.Boleto.Gas
{
    /// <summary>
    /// 
    /// </summary>
    public class TXT_Ista: cTabelaTXT
    {
        /// <summary>
        /// 
        /// </summary>
        public dTXT_Ista DS;

        /// <summary>
        /// 
        /// </summary>
        public dTXT_Ista.LeituraDataTable Leitura
        {
            get 
            {
                return DS.Leitura;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Bloco"></param>
        /// <param name="Apartamento"></param>
        /// <returns></returns>
        public dTXT_Ista.APTxIGARow BustaCadastrado(string Bloco,string Apartamento)
        {
            foreach (dTXT_Ista.APTxIGARow rowBusca in DS.APTxIGA)
                if ((!rowBusca.IsIGABLOCONull()) && (!rowBusca.IsIGAApartamentoNull()) && (Bloco == rowBusca.IGABLOCO) && (Apartamento == rowBusca.IGAApartamento))
                    return rowBusca;
            return null;
        }

        private dTXT_Ista.APTxIGARow Cadastra(string Bloco, string Apartamento, string BlocoReal, string ApartamentoReal)
        {
            dTXT_Ista.APTxIGARow Encontrado = null;
            foreach (dTXT_Ista.APTxIGARow rowBusca in DS.APTxIGA)
            {
                if ((Bloco.Trim().ToUpper() == rowBusca.BLOCodigo.Trim().ToUpper()) && (Apartamento.Trim().ToUpper() == rowBusca.APTNumero.Trim().ToUpper()))
                {
                    Encontrado = rowBusca;
                    Encontrado.IGABLOCO = BlocoReal;
                    Encontrado.IGAApartamento = ApartamentoReal;
                    dTXT_Ista.ImportaGasRow rowIGA = DS.ImportaGas.NewImportaGasRow();
                    rowIGA.IGA_APT = Encontrado.APT;
                    rowIGA.IGAApartamento = ApartamentoReal;
                    rowIGA.IGABLOCO = BlocoReal;
                    rowIGA.IGAFornecedor = 0;
                    DS.ImportaGas.AddImportaGasRow(rowIGA);
                    DS.ImportaGasTableAdapter.Update(rowIGA);
                    rowIGA.AcceptChanges();
                    Encontrado.IGA = rowIGA.IGA;
                    break;
                }                
            }

            return Encontrado;
        }

        private dTXT_Ista.APTxIGARow Cadastra(string Bloco, string Apartamento)
        {
            dTXT_Ista.APTxIGARow Encontrado;
            int nAPT = 0;
            if(Bloco.Trim() == "")
            {
                Encontrado = Cadastra("SB", Apartamento, Bloco, Apartamento);
                if (Encontrado != null)
                    return Encontrado;
                if (int.TryParse(Apartamento.Trim().Replace(" ", ""), out nAPT))
                {
                    Encontrado = Cadastra("SB", nAPT.ToString("0000"), Bloco, Apartamento);
                    if (Encontrado != null)
                        return Encontrado;
                    Encontrado = Cadastra("SB", nAPT.ToString("000"), Bloco, Apartamento);
                    if (Encontrado != null)
                        return Encontrado;
                }
            };
            Encontrado = Cadastra(Bloco, Apartamento, Bloco, Apartamento);
            if (Encontrado != null)
                return Encontrado;
            
            if (int.TryParse(Apartamento.Trim().Replace(" ", ""), out nAPT))
            {
                Encontrado = Cadastra(Bloco, nAPT.ToString("0000"), Bloco, Apartamento);
                if (Encontrado != null)
                    return Encontrado;
                Encontrado = Cadastra(Bloco, nAPT.ToString("000"), Bloco, Apartamento);
            }
            return Encontrado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <param name="CON"></param>
        public TXT_Ista(string NomeArquivo,int CON)
        {
            Modelo = ModelosTXT.layout;
            DS = new dTXT_Ista();
            DataTable = DS.Leitura;
            Formatodata = FormatoData.dd_MM_YYYY;
            RegistraMapa("Tipo", 1, 10);
            RegistraMapa("Versao", 11, 2);
            RegistraMapa("CodCond", 13, 4);
            RegistraMapa("Bloco", 17, 3);
            RegistraMapa("APT", 20, 7);
            RegistraMapa("data",27 , 10);
            RegistraMapa("mes",37 , 2);
            RegistraMapa("ano", 39, 4);
            RegistraMapa("LeituraAnt", 43, 14);
            RegistraMapa("LeituraAtual", 57, 14);
            RegistraMapa("Consumo", 71, 14);
            RegistraMapa("Valor", 85, 14);
            Carrega(NomeArquivo,"");
            DS.NaoCadastrados.Clear();
            DS.APTxIGATableAdapter.Fill(DS.APTxIGA, CON);
            DS.ImportaGasTableAdapter.FillByCON(DS.ImportaGas,CON);
            dTXT_Ista.APTxIGARow rowResposta;
            foreach (dTXT_Ista.LeituraRow rowLida in DS.Leitura)
            {
                rowResposta = BustaCadastrado(rowLida.Bloco, rowLida.APT);
                if (rowResposta == null)
                {
                    rowResposta = Cadastra(rowLida.Bloco, rowLida.APT);
                    if (rowResposta == null)
                    {
                        if (DS.NaoCadastrados.FindByBlocoAPT(rowLida.Bloco, rowLida.APT) == null)
                            DS.NaoCadastrados.AddNaoCadastradosRow(rowLida.Bloco, rowLida.APT);
                    }
                }
                
            }
        }
        
    }

}
