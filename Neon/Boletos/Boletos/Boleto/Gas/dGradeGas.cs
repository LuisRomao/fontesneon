﻿namespace Boletos.Boleto.Gas {


    partial class dGradeGas
    {
        partial class GASDataTable
        {
        }
    
        private dGradeGasTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalhe
        /// </summary>
        public dGradeGasTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dGradeGasTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheTableAdapter;
            }
        }

        private dGradeGasTableAdapters.UnidadesTableAdapter unidadesTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Unidades
        /// </summary>
        public dGradeGasTableAdapters.UnidadesTableAdapter UnidadesTableAdapter
        {
            get {
                if (unidadesTableAdapter == null) {
                    unidadesTableAdapter = new Boletos.Boleto.Gas.dGradeGasTableAdapters.UnidadesTableAdapter();
                    unidadesTableAdapter.TrocarStringDeConexao();
                };
                return unidadesTableAdapter; 
            }
            
        }

        private dGradeGasTableAdapters.GASTableAdapter gASTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: GASTableAdapter
        /// </summary>
        public dGradeGasTableAdapters.GASTableAdapter GASTableAdapter {
            get {
                if (gASTableAdapter == null) {
                    gASTableAdapter = new Boletos.Boleto.Gas.dGradeGasTableAdapters.GASTableAdapter();
                    gASTableAdapter.TrocarStringDeConexao();
                };
                return gASTableAdapter;
            }
        }

        private dGradeGasTableAdapters.CompementoBOLTableAdapter compementoBOLTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CompementoBOL
        /// </summary>
        public dGradeGasTableAdapters.CompementoBOLTableAdapter CompementoBOLTableAdapter
        {
            get
            {
                if (compementoBOLTableAdapter == null)
                {
                    compementoBOLTableAdapter = new dGradeGasTableAdapters.CompementoBOLTableAdapter();
                    compementoBOLTableAdapter.TrocarStringDeConexao();
                };
                return compementoBOLTableAdapter;
            }
        }
    }
}
