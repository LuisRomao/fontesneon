namespace Boletos.Boleto.Gas
{
    partial class cGradeGas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label11;
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cGradeGas));
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonBoleto = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dGradeGas = new Boletos.Boleto.Gas.dGradeGas();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.BotGerar = new DevExpress.XtraEditors.SimpleButton();
            this.ValGas = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpCTL = new DevExpress.XtraEditors.LookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkEditcomCondominio = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.BotImp = new DevExpress.XtraEditors.SimpleButton();
            this.radioTipo = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBoleto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGradeGas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpCTL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditcomCondominio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "Unidades";
            this.BindingSource_F.DataSource = this.dGradeGas;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(597, 69);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(36, 13);
            label11.TabIndex = 46;
            label11.Text = "Conta";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = null;
            this.gridControl1.DataSource = this.BindingSource_F;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "FK_Unidades_GAS";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 91);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonBoleto});
            this.gridControl1.Size = new System.Drawing.Size(1403, 708);
            this.gridControl1.TabIndex = 6;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBLOCodigo,
            this.colAPTNumero,
            this.colBLONome});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsView.AllowCellMerge = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBLOCodigo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAPTNumero, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "Bloco";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.OptionsColumn.ReadOnly = true;
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 0;
            this.colBLOCodigo.Width = 59;
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "N�nero";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.OptionsColumn.ReadOnly = true;
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 2;
            // 
            // colBLONome
            // 
            this.colBLONome.Caption = "BLONome";
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.OptionsColumn.ReadOnly = true;
            this.colBLONome.OptionsColumn.ShowCaption = false;
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 1;
            // 
            // repositoryItemButtonBoleto
            // 
            this.repositoryItemButtonBoleto.AutoHeight = false;
            this.repositoryItemButtonBoleto.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Boletos.Properties.Resources.Bboleto, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonBoleto.Mask.EditMask = "n2";
            this.repositoryItemButtonBoleto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemButtonBoleto.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemButtonBoleto.Name = "repositoryItemButtonBoleto";
            this.repositoryItemButtonBoleto.ReadOnly = true;
            this.repositoryItemButtonBoleto.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonBoleto_ButtonClick);
            // 
            // dGradeGas
            // 
            this.dGradeGas.DataSetName = "dGradeGas";
            this.dGradeGas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(5, 5);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(368, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 1;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            this.lookupCondominio_F1.alterado += new System.EventHandler(this.lookupCondominio_F1_alterado);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(379, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(69, 23);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "Carregar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // BotGerar
            // 
            this.BotGerar.Location = new System.Drawing.Point(1027, 24);
            this.BotGerar.Name = "BotGerar";
            this.BotGerar.Size = new System.Drawing.Size(90, 23);
            this.BotGerar.TabIndex = 3;
            this.BotGerar.Text = "Gerar";
            this.BotGerar.Visible = false;
            this.BotGerar.Click += new System.EventHandler(this.BotGerar_Click);
            // 
            // ValGas
            // 
            this.ValGas.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValGas.Appearance.ForeColor = System.Drawing.Color.Red;
            this.ValGas.Location = new System.Drawing.Point(226, 65);
            this.ValGas.Name = "ValGas";
            this.ValGas.Size = new System.Drawing.Size(109, 13);
            this.ValGas.TabIndex = 4;
            this.ValGas.Text = "Valor do G�s = 0,00";
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.Red;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.Location = new System.Drawing.Point(285, 31);
            this.panelControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(19, 16);
            toolTipTitleItem1.Text = "Fora da toler�ncia";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "O valor da consumo em m� est� fora da toler�ncia em rela��o ao m�s anterior";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.ToolTipController_F.SetSuperTip(this.panelControl2, superToolTip1);
            this.panelControl2.TabIndex = 5;
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.Violet;
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.Location = new System.Drawing.Point(429, 31);
            this.panelControl3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(19, 16);
            toolTipTitleItem2.Text = "Leitura errada";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "O leitura n�o confere com o valor lan�ado no boleto\r\nIsso pode ocorrer quando o m" +
    "edidor � substitu�do";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.ToolTipController_F.SetSuperTip(this.panelControl3, superToolTip2);
            this.panelControl3.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.lookUpCTL);
            this.panelControl1.Controls.Add(label11);
            this.panelControl1.Controls.Add(this.checkEditcomCondominio);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.BotImp);
            this.panelControl1.Controls.Add(this.radioTipo);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.spinEdit1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.ValGas);
            this.panelControl1.Controls.Add(this.BotGerar);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1403, 91);
            this.panelControl1.TabIndex = 7;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(716, 3);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(141, 22);
            this.simpleButton3.TabIndex = 48;
            this.simpleButton3.Text = "Exportar";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // lookUpCTL
            // 
            this.lookUpCTL.Location = new System.Drawing.Point(638, 66);
            this.lookUpCTL.Name = "lookUpCTL";
            this.lookUpCTL.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Selecionar", null, null, false),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Nenhuma", null, null, false)});
            this.lookUpCTL.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpCTL.Properties.DataSource = this.conTasLogicasBindingSource;
            this.lookUpCTL.Properties.DisplayMember = "CTLTitulo";
            this.lookUpCTL.Properties.NullText = "-- Padr�o --";
            this.lookUpCTL.Properties.ValueMember = "CTL";
            this.lookUpCTL.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit5_Properties_ButtonClick);
            this.lookUpCTL.Size = new System.Drawing.Size(182, 20);
            this.lookUpCTL.TabIndex = 47;
            this.lookUpCTL.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpCTL_ButtonClick);
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = typeof(FrameworkProc.datasets.dContasLogicas);
            // 
            // checkEditcomCondominio
            // 
            this.checkEditcomCondominio.EditValue = true;
            this.checkEditcomCondominio.Location = new System.Drawing.Point(341, 63);
            this.checkEditcomCondominio.Name = "checkEditcomCondominio";
            this.checkEditcomCondominio.Properties.Caption = "Com condom�nio";
            this.checkEditcomCondominio.Size = new System.Drawing.Size(140, 19);
            this.checkEditcomCondominio.TabIndex = 45;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(7, 65);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(90, 13);
            this.labelControl5.TabIndex = 44;
            this.labelControl5.Text = "Compet�ncia inicial";
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(103, 58);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 43;
            this.cCompet1.Titulo = null;
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Enabled = false;
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(600, 2);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(109, 23);
            this.simpleButton2.TabIndex = 15;
            this.simpleButton2.Text = "Importar TXT";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click_1);
            // 
            // BotImp
            // 
            this.BotImp.Enabled = false;
            this.BotImp.Image = ((System.Drawing.Image)(resources.GetObject("BotImp.Image")));
            this.BotImp.Location = new System.Drawing.Point(600, 31);
            this.BotImp.Name = "BotImp";
            this.BotImp.Size = new System.Drawing.Size(109, 23);
            this.BotImp.TabIndex = 14;
            this.BotImp.Text = "Importar";
            this.BotImp.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // radioTipo
            // 
            this.radioTipo.EditValue = 1;
            this.radioTipo.Location = new System.Drawing.Point(529, 3);
            this.radioTipo.Name = "radioTipo";
            this.radioTipo.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "G�s"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "�gua"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Luz")});
            this.radioTipo.Size = new System.Drawing.Size(65, 83);
            this.radioTipo.TabIndex = 13;
            this.radioTipo.EditValueChanged += new System.EventHandler(this.radioTipo_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(455, 34);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(68, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Leitura errada";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(311, 34);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(87, 13);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Fora da toler�ncia";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(201, 34);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(59, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "J� no boleto";
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(73, 31);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.DisplayFormat.FormatString = "{0}%";
            this.spinEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(78, 20);
            this.spinEdit1.TabIndex = 9;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 13);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Toler�ncia:";
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(100)))));
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.Location = new System.Drawing.Point(175, 31);
            this.panelControl4.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(19, 16);
            toolTipTitleItem3.Text = "J� no boleto";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "O consumo do g�s j� est� em um boleto. Caso o valor da leitura seja alterado o co" +
    "nsumo N�O ser� alterado";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.ToolTipController_F.SetSuperTip(this.panelControl4, superToolTip3);
            this.panelControl4.TabIndex = 7;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "arquivo txt|*.txt|todos|*.*";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xls";
            this.saveFileDialog1.Filter = "*.xls|*.xls";
            // 
            // cGradeGas
            // 
            this.BindingSourcePrincipal = this.BindingSource_F;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGradeGas";
            this.Size = new System.Drawing.Size(1403, 799);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBoleto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGradeGas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpCTL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditcomCondominio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton BotGerar;
        private DevExpress.XtraEditors.LabelControl ValGas;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private dGradeGas dGradeGas;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraEditors.RadioGroup radioTipo;
        private DevExpress.XtraEditors.SimpleButton BotImp;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonBoleto;
        private DevExpress.XtraEditors.CheckEdit checkEditcomCondominio;
        private DevExpress.XtraEditors.LookUpEdit lookUpCTL;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}
