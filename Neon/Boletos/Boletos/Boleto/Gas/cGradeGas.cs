using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Framework.objetosNeon;
using DevExpress.XtraGrid.Columns;

namespace Boletos.Boleto.Gas
{
    /// <summary>
    /// Componete para digita��o do g�s/�gua
    /// </summary>
    public partial class cGradeGas : CompontesBasicos.ComponenteBaseBindingSource
    {
        private enum TipoApontamento
        {
            TipoGas = 1,
            TipoAgua = 2,   
            TipoLuz = 3
        }

        private TipoApontamento Tipo
        {
            get
            {                
                return (TipoApontamento)radioTipo.EditValue;
            }
        }

        private string PlanoDeContas
        {
            get
            {
                switch (Tipo)
                {
                    case TipoApontamento.TipoAgua:
                        return "129000";
                    case TipoApontamento.TipoGas:
                        return "102000";
                    case TipoApontamento.TipoLuz:
                        return "129001";
                    default:
                        throw new Exception(string.Format("Enumera��o n�o tratada em cGradeGas.cs 46. Valor = {0}",Tipo));
                }                
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cGradeGas()
        {
            InitializeComponent();
            cCompet1.Comp = new Competencia().Add(-3);
            cCompet1.AjustaTela();
            CompFinal = new Competencia();
            CompFinal++;
            CompFinal++;
            CriaColunas();
        }

        /// <summary>
        /// Carregar os dados novamente
        /// </summary>
        public override void RefreshDados()
        {
            base.RefreshDados();
            Carregar();
        }

        private Competencia CompFinal;
        private List<DataColumn> NovasColunasTabela;
        private List<GridColumn> NovasColunasGrid;        

        private void CriaColunas()
        {
            LimpaTabelas();            
            RemoveColunasDinamicas();
            for (Competencia comp = cCompet1.Comp; comp <= CompFinal; comp++)
                CriaColuna(comp);
        }

        private void RemoveColunasDinamicas()
        {
            if (NovasColunasTabela == null)
            {
                NovasColunasTabela = new List<DataColumn>();
                NovasColunasGrid = new List<GridColumn>();
            }
            else
            {
                foreach (DataColumn ColunaT in NovasColunasTabela)
                    dGradeGas.Unidades.Columns.Remove(ColunaT);
                foreach (GridColumn ColunaG in NovasColunasGrid)
                    gridView1.Columns.Remove(ColunaG);
                NovasColunasTabela.Clear();
                NovasColunasGrid.Clear();
            }
        }

        private void LimpaTabelas()
        {
            dGradeGas.BOletoDetalhe.Clear();
            dGradeGas.GAS.Clear();
            dGradeGas.Unidades.Clear();
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            CriaColunas();
        }

        private void CriaColuna(Competencia Comp)
        {
            DataColumn novaColuna;
            GridColumn DvnovaColuna = null;
            novaColuna = dGradeGas.Unidades.Columns.Add(Comp.ToString(), typeof(decimal));
            NovasColunasTabela.Add(novaColuna);            
            novaColuna.Caption = Comp.ToString();
            DvnovaColuna = gridView1.Columns.Add();
            NovasColunasGrid.Add(DvnovaColuna);
            DvnovaColuna.FieldName = Comp.ToString();
            DvnovaColuna.Visible = true;
            DvnovaColuna.Width = 60;
            DvnovaColuna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            DvnovaColuna.DisplayFormat.FormatString = "n4";
            DvnovaColuna.OptionsColumn.AllowIncrementalSearch = false;
            DvnovaColuna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;


            novaColuna = dGradeGas.Unidades.Columns.Add("m3" + Comp.ToString(), typeof(decimal));
            NovasColunasTabela.Add(novaColuna);
            DvnovaColuna = gridView1.Columns.Add();
            NovasColunasGrid.Add(DvnovaColuna);
            DvnovaColuna.FieldName = "m3" + Comp.ToString();
            DvnovaColuna.Visible = true;
            DvnovaColuna.Width = 60;
            DvnovaColuna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            DvnovaColuna.DisplayFormat.FormatString = "n4";
            DvnovaColuna.OptionsColumn.ReadOnly = ((Tipo == TipoApontamento.TipoGas) && (PrecoDoGas != 0));
            DvnovaColuna.Caption = "m3 / kWh";
            DvnovaColuna.AppearanceCell.BackColor = Color.FromArgb(255, 200, 200);
            DvnovaColuna.Visible = true;
            DvnovaColuna.OptionsColumn.AllowIncrementalSearch = false;
            DvnovaColuna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;

            novaColuna = dGradeGas.Unidades.Columns.Add("R" + Comp.ToString(), typeof(decimal));
            NovasColunasTabela.Add(novaColuna);
            DvnovaColuna = gridView1.Columns.Add();
            NovasColunasGrid.Add(DvnovaColuna);
            DvnovaColuna.FieldName = "R" + Comp.ToString();
            DvnovaColuna.Visible = true;
            DvnovaColuna.Width = 60;
            DvnovaColuna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            DvnovaColuna.DisplayFormat.FormatString = "n2";
            DvnovaColuna.OptionsColumn.ReadOnly = ((Tipo == TipoApontamento.TipoGas) && (PrecoDoGas != 0));
            DvnovaColuna.Caption = "R$";
            DvnovaColuna.AppearanceCell.BackColor = Color.FromArgb(200, 200, 255);
            DvnovaColuna.Visible = true;
            DvnovaColuna.OptionsColumn.AllowIncrementalSearch = false;
            DvnovaColuna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;

            novaColuna = dGradeGas.Unidades.Columns.Add("Travado" + Comp.ToString(), typeof(bool));
            NovasColunasTabela.Add(novaColuna);
            novaColuna = dGradeGas.Unidades.Columns.Add("GAS" + Comp.ToString(), typeof(int));
            NovasColunasTabela.Add(novaColuna);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Carregar();
        }

        private void Carregar()
        {
            int? pos = null; 
            GridColumn posColuna = null;
            if(dGradeGas.Unidades.Count != 0)
            {
                pos = gridView1.FocusedRowHandle;
                posColuna = gridView1.FocusedColumn;
            }
            LimpaTabelas();
            if (lookupCondominio_F1.CON_sel > 0)
            {
                string Comando1 = "SELECT CONLeituraGas FROM CONDOMINIOS WHERE (CON = @P1)";
                string Comando2 = "SELECT CONValorGas FROM CONDOMINIOS WHERE (CON = @P1)";
                bool Cancelar;
                switch (Tipo)
                {
                    case TipoApontamento.TipoGas:

                        if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(Comando1, out Cancelar, lookupCondominio_F1.CON_sel))
                            MessageBox.Show("Este condom�nio n�o cobra g�s!!");
                        else
                            if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(Comando2, out PrecoDoGas, lookupCondominio_F1.CON_sel))
                                MessageBox.Show("Valor do g�s n�o cadastrado");
                            else
                            {
                                if (PrecoDoGas != 0)
                                {
                                    ValGas.Text = "Valor do G�s = " + PrecoDoGas.ToString("n2") + " R$/Kg";
                                    PrecoDoGas *= 2.3M;
                                }
                                else
                                    ValGas.Text = "C�lculo desligado (Pre�o = 0,00)";

                            };
                        break;
                    case TipoApontamento.TipoAgua:
                    case TipoApontamento.TipoLuz:
                        ValGas.Text = "";
                        break;
                    default:
                        throw new Exception();
                }
                CarregaDados(pos,posColuna);
                simpleButton2.Enabled = BotImp.Enabled = true;
            }
        }

        private Competencia UltimaCompDados;

        private void CarregaDados(int? pos = null,GridColumn posColuna = null)
        {
            cCompet1.Comp.CON = lookupCondominio_F1.CON_sel;
            try
            {
                gridView1.BeginDataUpdate();
                if (lookupCondominio_F1.CON_sel == -1)
                    return;
                //int pos = gridView1.FocusedRowHandle;                        
                //GridColumn DvnovaColuna = null;
                dGradeGas.GAS.Clear();
                dGradeGas.UnidadesTableAdapter.Fill(dGradeGas.Unidades, lookupCondominio_F1.CON_sel);
                dGradeGas.GASTableAdapter.FillByCompetencia(dGradeGas.GAS, lookupCondominio_F1.CON_sel, cCompet1.Comp.CompetenciaBind, (int)Tipo);
                dGradeGas.BOletoDetalheTableAdapter.FillByCompetencia(dGradeGas.BOletoDetalhe, lookupCondominio_F1.CON_sel, cCompet1.Comp.CompetenciaBind, (int)Tipo);
                UltimaCompDados = cCompet1.Comp.CloneCompet();
                using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                {
                    Esp.Espere("Carregando");
                    Esp.AtivaGauge(dGradeGas.Unidades.Count);
                    foreach (dGradeGas.UnidadesRow Urow in dGradeGas.Unidades)
                    {
                        Esp.Gauge();
                        foreach (dGradeGas.GASRow rowGAS in Urow.GetGASRows())
                        {
                            Competencia complinha = new Competencia(rowGAS.GASCompetencia);
                            if (complinha > UltimaCompDados)
                                UltimaCompDados = complinha.CloneCompet();
                            string Campo = string.Format("{0}", complinha);
                            Urow[Campo] = rowGAS.GASLeitura;
                            Urow["GAS" + Campo] = rowGAS.GAS;
                            Urow["m3" + Campo] = rowGAS.GASM3;
                            Urow["Travado" + Campo] = false;
                            dGradeGas.BOletoDetalheRow[] BODs = rowGAS.GetBOletoDetalheRows();
                            if (BODs != null)
                            {
                                decimal Total = 0;
                                foreach (dGradeGas.BOletoDetalheRow BODrow in BODs)
                                {
                                    Total += BODrow.BODValor;
                                    if (!BODrow.IsBOLStatusNull())
                                        Urow["Travado" + Campo] = true;
                                }
                                Urow["R" + Campo] = Total;
                            }
                        }
                    }
                };                
                dGradeGas.Unidades.AcceptChanges();
            }
            finally
            {
                gridView1.EndDataUpdate();
                if (posColuna != null)
                    gridView1.FocusedColumn = posColuna;
                if (pos.HasValue)
                    gridView1.FocusedRowHandle = pos.Value;
            }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {            
            try
            {
                dGradeGas.UnidadesRow row = (dGradeGas.UnidadesRow)gridView1.GetDataRow(e.RowHandle);
                if (row == null)
                    return;
                if ((e.Column.Fixed == DevExpress.XtraGrid.Columns.FixedStyle.None) && (e.Column.FieldName.StartsWith("m3")))
                {
                    decimal Tol = spinEdit1.Value / 100;                    
                    
                    Competencia Comp = Competencia.Parse(e.Column.FieldName.Substring(2, 7));
                    Competencia CompAnterior = Comp.CloneCompet(-1);

                    if (CompAnterior >= cCompet1.Comp)
                    {
                        decimal ValAnterior = 0;
                        decimal ValCorrente = 0;
                        if ((row["R" + Comp.ToString()] != DBNull.Value))
                            ValCorrente = (decimal)row["R" + Comp.ToString()];
                        if ((row["R" + CompAnterior.ToString()] != DBNull.Value))
                            ValAnterior = (decimal)row["R" + CompAnterior.ToString()];
                        if (Math.Abs(ValAnterior - ValCorrente) > ValAnterior * Tol)
                        {
                            e.Appearance.BackColor = Color.FromArgb(255, 0, 0);
                            e.Appearance.ForeColor = Color.White;
                        }
                    };
                    
                }

                if ((e.Column.Fixed == DevExpress.XtraGrid.Columns.FixedStyle.None) && (e.Column.FieldName.Substring(2, 1) == "/"))
                {                    
                    if ((row["Travado" + e.Column.FieldName] != DBNull.Value) && (bool)row["Travado" + e.Column.FieldName])
                    {
                        e.Appearance.BackColor = Color.FromArgb(255, 255, 100);
                        if (Tipo == TipoApontamento.TipoGas)
                        {
                            Competencia Comp = Competencia.Parse(e.Column.FieldName);
                            Competencia CompAnterior = Comp.CloneCompet(-1);
                            if ((row[CompAnterior.ToString()] != DBNull.Value) && (row[Comp.ToString()] != DBNull.Value) && (row["m3" + Comp.ToString()]) != DBNull.Value)
                            {
                                decimal Anterior = (decimal)row[CompAnterior.ToString()];
                                decimal Corrente = (decimal)row[Comp.ToString()];
                                decimal M3 = (decimal)row["m3" + Comp.ToString()];
                                if (Math.Abs(Corrente - Anterior - M3) > 0.00009M)
                                    //e.Appearance.BackColor = Color.FromArgb(255, 100, 100);
                                    e.Appearance.BackColor = Color.Violet;
                            };
                        };
                    }
                }
            }
            catch { }
            
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if ((e.Column.Fixed == DevExpress.XtraGrid.Columns.FixedStyle.None) && (e.Column.FieldName.StartsWith("R")))
            {
                dGradeGas.UnidadesRow row = (dGradeGas.UnidadesRow)gridView1.GetDataRow(e.RowHandle);
                if (row == null)
                    return;
                string ColunaTrava = e.Column.FieldName.Replace("R","Travado");
                if ((row[ColunaTrava] != DBNull.Value) && (bool)row[ColunaTrava])
                {
                    e.RepositoryItem = repositoryItemButtonBoleto;
                }
            }
        }

        private void repositoryItemButtonBoleto_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dGradeGas.UnidadesRow row = (dGradeGas.UnidadesRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            string ColunaGAS = gridView1.FocusedColumn.FieldName.Replace("R", "GAS");
            dGradeGas.GASRow GASrow = dGradeGas.GAS.FindByGAS((int)row[ColunaGAS]);
            dGradeGas.BOletoDetalheRow[] BODrows = GASrow.GetBOletoDetalheRows();
            if ((BODrows.Length == 1) && (!BODrows[0].IsBOLNull()))
            {
                Boletos.Boleto.cBoleto cBoleto = new Boletos.Boleto.cBoleto(new Boleto(BODrows[0].BOL), false);
                if (cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                    CarregaDados(gridView1.FocusedRowHandle, gridView1.FocusedColumn);
            }
            else
            {
                string pergunta = string.Format("Aten��o: este valor est� dividido em {0} boletos.\r\nAbrir em abas independentes ?",BODrows.Length);
                if (MessageBox.Show(pergunta, "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    foreach (dGradeGas.BOletoDetalheRow BODrow in BODrows)
                    {
                        Boletos.Boleto.cBoleto cBoleto = new Boletos.Boleto.cBoleto(new Boleto(BODrow.BOL), false);
                        cBoleto.Titulo = string.Format("Boleto {0}",BODrow.BOL);
                        cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                    }
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            dGradeGas.UnidadesRow LinhaEd = (dGradeGas.UnidadesRow)DRV.Row;
            CalculaGravaRow(LinhaEd);
        }

        private void CalculaGravaRow(dGradeGas.UnidadesRow LinhaEd)
        {
            bool EditandoLinha = false;
            Competencia Comp;
            //pula as colunas sem altera��o
            for (Comp = cCompet1.Comp.CloneCompet(); Comp <= CompFinal; Comp.Add(1))
            {

                if (diferente(LinhaEd["m3" + Comp.ToString(), DataRowVersion.Original], LinhaEd["m3" + Comp.ToString(), DataRowVersion.Current]))
                    break;
                if (diferente(LinhaEd["R" + Comp.ToString(), DataRowVersion.Original], LinhaEd["R" + Comp.ToString(), DataRowVersion.Current]))
                    break;
                if (diferente(LinhaEd[Comp.ToString(), DataRowVersion.Original], LinhaEd[Comp.ToString(), DataRowVersion.Current]))
                    break;
            }
            //vai calculando da coluna alterada at� o final
            for (; Comp <= CompFinal; Comp.Add(1))
            {
                decimal Original = 0;
                decimal Corrente = 0;
                bool semLeitura = (LinhaEd[Comp.ToString(), DataRowVersion.Current] == DBNull.Value);
                EditandoLinha = false;
                if (LinhaEd[Comp.ToString(), DataRowVersion.Original] != DBNull.Value)
                    Original = (decimal)(LinhaEd[Comp.ToString(), DataRowVersion.Original]);
                if (LinhaEd[Comp.ToString(), DataRowVersion.Current] != DBNull.Value)
                    Corrente = (decimal)(LinhaEd[Comp.ToString(), DataRowVersion.Current]);
                if (Math.Abs(Original - Corrente) >= 0.00005M)
                    EditandoLinha = true;

                decimal OriginalR = 0;
                decimal CorrenteR = 0;
                if (LinhaEd["R" + Comp.ToString(), DataRowVersion.Original] != DBNull.Value)
                    OriginalR = (decimal)(LinhaEd["R" + Comp.ToString(), DataRowVersion.Original]);
                if (LinhaEd["R" + Comp.ToString(), DataRowVersion.Current] != DBNull.Value)
                    CorrenteR = (decimal)(LinhaEd["R" + Comp.ToString(), DataRowVersion.Current]);
                if (Math.Abs(OriginalR - CorrenteR) >= 0.00005M)
                    EditandoLinha = true;
                  
                if ((Tipo != TipoApontamento.TipoGas) || (PrecoDoGas == 0))
                    if ((LinhaEd["Travado" + Comp.ToString()] != DBNull.Value) && (bool)LinhaEd["Travado" + Comp.ToString()])
                        EditandoLinha = false;

                if (EditandoLinha)
                {
                    //EditandoLinha = true;
                    decimal M3 = 0;
                    decimal Valor = 0;
                    decimal Anterior = 0;

                    if ((Tipo != TipoApontamento.TipoGas) || (PrecoDoGas == 0))
                    {
                        if ((LinhaEd["Travado" + Comp.ToString()] == DBNull.Value) || !(bool)LinhaEd["Travado" + Comp.ToString()])
                        {
                            if (LinhaEd["R" + Comp.ToString()] != DBNull.Value)
                                Valor = (decimal)LinhaEd["R" + Comp.ToString()];
                            if (LinhaEd["m3" + Comp.ToString()] != DBNull.Value)
                                M3 = (decimal)LinhaEd["m3" + Comp.ToString()];

                            Competencia CompAnterior = Comp.CloneCompet().Add(-1);
                            if (CompAnterior >= cCompet1.Comp)
                            {
                                if (LinhaEd[CompAnterior.ToString()] != DBNull.Value)
                                {
                                    Anterior = (decimal)LinhaEd[CompAnterior.ToString()];
                                    if ((M3 == 0) && (Corrente > Anterior))
                                    {
                                        M3 = Corrente - Anterior;
                                        LinhaEd["m3" + Comp.ToString()] = M3;
                                    }
                                }
                            }
                            else
                                Anterior = Corrente = 0;

                        }
                        else
                        {
                            LinhaEd.RejectChanges();
                            return;
                        };
                    }

                    else if (Tipo == TipoApontamento.TipoGas)
                    {
                        if (((LinhaEd["Travado" + Comp.ToString()] == DBNull.Value) || !(bool)LinhaEd["Travado" + Comp.ToString()]) && (!semLeitura))
                        {
                            Competencia CompAnterior = Comp.CloneCompet().Add(-1);
                            if (CompAnterior >= cCompet1.Comp)
                            {
                                if (LinhaEd[CompAnterior.ToString()] != DBNull.Value)
                                    Anterior = (decimal)LinhaEd[CompAnterior.ToString()];
                                if (Corrente < Anterior)
                                {
                                    MessageBox.Show("Leitura incorreta! Leitura Anterior = " + Anterior.ToString("n2") + " Leitura Atual = " + Corrente.ToString("n2"));
                                    Corrente = Anterior;
                                    LinhaEd[Comp.ToString()] = Corrente;
                                }
                                M3 = Corrente - Anterior;
                                Valor = Math.Round(M3 * PrecoDoGas, 2);
                            };
                            LinhaEd["m3" + Comp.ToString()] = M3;
                            LinhaEd["R" + Comp.ToString()] = Valor;
                        }
                        else
                        {
                            if (LinhaEd["R" + Comp.ToString()] != DBNull.Value)
                                Valor = (decimal)LinhaEd["R" + Comp.ToString()];
                            if (LinhaEd["m3" + Comp.ToString()] != DBNull.Value)
                                M3 = (decimal)LinhaEd["m3" + Comp.ToString()];
                        };
                    }
                    
                        
                    
                    //Parametros[8] = lookupCondominio_F1.CON_sel;
                    string descricao = "";
                    switch (Tipo)
                    {
                        case TipoApontamento.TipoGas:
                            descricao = string.Format("G�s {0:n4}m� {1:n4}Kg (Leit.{2:n4}-{3:n4})", M3, M3 * 2.3M, Anterior, Corrente);
                            if (descricao.Length > 50)
                            {
                                descricao = string.Format("G�s {0:n3}m� {1:n2}Kg (Leit.{2:n4}-{3:n4})", M3, M3 * 2.3M, Anterior, Corrente);
                                if (descricao.Length > 50)
                                {
                                    descricao = string.Format("G�s {0:n3}m� {1:n2}Kg", M3, M3 * 2.3M);
                                    if (descricao.Length > 50)
                                    {
                                        descricao = "G�s";
                                    }
                                }
                            }
                            break;
                        case TipoApontamento.TipoAgua:
                            descricao = string.Format("�gua {0:n4}m�", M3);

                            if (M3 == (Corrente - Anterior))
                            {
                                string Proposta = string.Format("{0} (Leit.{1:n4}-{2:n4})", descricao, Anterior, Corrente);
                                if (Proposta.Length <= 50)
                                    descricao = Proposta;
                            }
                            break;
                        case TipoApontamento.TipoLuz:
                            if (M3 != 0)
                                descricao = string.Format("Eletropaulo {0:n0} kWh", M3);
                            else
                                descricao = string.Format("Eletropaulo");

                            /*if (M3 == (Corrente - Anterior))
                            {
                                string Proposta = string.Format("{0} (Leit.{1:n4}-{2:n4})", descricao, Anterior, Corrente);
                                if (Proposta.Length <= 50)
                                    descricao = Proposta;
                            }*/
                            break;
                        default:
                            throw new Exception();
                    }
                    dGradeGas.GASRow GASRow = null;
                    if(LinhaEd[string.Format("GAS{0}", Comp)] != DBNull.Value)
                        GASRow = dGradeGas.GAS.FindByGAS((int)LinhaEd[string.Format("GAS{0}", Comp)]);
                    if (GASRow == null)
                    {
                        GASRow = dGradeGas.GAS.NewGASRow();
                    }
                    GASRow.GASLeitura = Corrente;
                    GASRow.GASDataA = DateTime.Now;
                    GASRow.GASA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                    GASRow.GASM3 = M3;
                    GASRow.GAS_APT = LinhaEd.APT;
                    GASRow.GASCompetencia = Comp.CompetenciaBind;
                    GASRow.GASTipo = ValorTipo(Tipo);
                    if (GASRow.RowState == DataRowState.Detached)
                        dGradeGas.GAS.AddGASRow(GASRow);
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos cGradeGas - 536",dGradeGas.GASTableAdapter, dGradeGas.BOletoDetalheTableAdapter);
                        dGradeGas.GASTableAdapter.Update(GASRow);
                        GASRow.AcceptChanges();
                        LinhaEd[string.Format("GAS{0}", Comp)] = GASRow.GAS;
                        if ((LinhaEd["Travado" + Comp.ToString()] == DBNull.Value) || !(bool)LinhaEd["Travado" + Comp.ToString()])
                        {
                            dGradeGas.BOletoDetalheRow BODrow = null;
                            dGradeGas.BOletoDetalheRow[] BODrows = GASRow.GetBOletoDetalheRows();
                            if (BODrows.Length > 1)
                                throw new Exception(string.Format("Erro em cGradeGas.cs GAS = {0}", GASRow.GAS));
                            else if (BODrows.Length == 1)
                            {
                                BODrow = BODrows[0];
                                if (!BODrow.IsBOLNull())
                                    throw new Exception(string.Format("Erro em cGradeGas.cs GAS = {0}", GASRow.GAS));
                            }
                            if (Valor == 0)
                            {
                                if (BODrow != null)
                                {
                                    BODrow.Delete();
                                    dGradeGas.BOletoDetalheTableAdapter.Update(dGradeGas.BOletoDetalhe);
                                    dGradeGas.BOletoDetalhe.AcceptChanges();
                                }
                            }

                            else
                            {
                                if (BODrow == null)
                                {
                                    BODrow = dGradeGas.BOletoDetalhe.NewBOletoDetalheRow();
                                    BODrow.BOD_APT = LinhaEd.APT;
                                    BODrow.BOD_CON = lookupCondominio_F1.CON_sel;
                                    BODrow.BOD_GAS = GASRow.GAS;
                                    BODrow.BOD_PLA = PlanoDeContas;
                                    BODrow.BODCompetencia = Comp.CompetenciaBind;
                                    BODrow.BODData = Comp.CalculaVencimento();
                                    BODrow.BODPrevisto = true;
                                    BODrow.BODProprietario = !LinhaEd.APTPagaConsumo;
                                }
                                BODrow.BODComCondominio = checkEditcomCondominio.Checked;
                                BODrow.BODMensagem = descricao;
                                BODrow.BODValor = Valor;
                                if (lookUpCTL.EditValue != null)
                                    BODrow.BOD_CTL = (int)lookUpCTL.EditValue;
                                if (BODrow.RowState == DataRowState.Detached)
                                    dGradeGas.BOletoDetalhe.AddBOletoDetalheRow(BODrow);
                                dGradeGas.BOletoDetalheTableAdapter.Update(BODrow);
                                BODrow.AcceptChanges();
                            };
                        }
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    }
                    catch (Exception E)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(E);
                        throw new Exception("Erro na grava��o: Processo cancelado\r\n" + E.Message.ToString(), E);
                    }
                };

            };

            LinhaEd.AcceptChanges();

        }

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            LimpaTela();
            DadosDoCondominio();
        }

        private void DadosDoCondominio()
        {
            conTasLogicasBindingSource.DataSource = FrameworkProc.datasets.dContasLogicas.GetdContasLogicas(lookupCondominio_F1.CON_sel, true);
            lookUpCTL.EditValue = null;
            checkEditcomCondominio.Checked = true;
        }       

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (lookupCondominio_F1.CON_sel == -1)
                return;
            
            FInputCompetencia FInputCompetencia = new FInputCompetencia();
            FInputCompetencia.cCompet1.Comp = CompFinal.CloneCompet();
            FInputCompetencia.cCompet1.AjustaTela();
            if (FInputCompetencia.ShowDialog() != DialogResult.OK)
                return;
            Competencia CompImput = FInputCompetencia.cCompet1.Comp;
            if ((CompImput < cCompet1.Comp) || (CompImput > CompFinal))
            {
                MessageBox.Show("Compet�ncia inv�lida");
                return;
            }
            Framework.Importador.cImportador Imp = new Framework.Importador.cImportador(lookupCondominio_F1.CON_sel);
            Imp.AddColunaImp("Sem Usu", "Sem Usu", typeof(string));
            Imp.AddColunaImp("Leitura", "Leitura", typeof(decimal), 99999.9999M, 0M);
            if (Tipo == TipoApontamento.TipoLuz)
            {
                Imp.AddColunaImp("M3", "kWh", typeof(decimal), 9999.9999M, 0M);
                Imp.AddColunaImp("Valor", "Valor", typeof(decimal), 9999.9999M, 0M);
            }
            else if ((Tipo != TipoApontamento.TipoGas) || (PrecoDoGas == 0))
            {
                Imp.AddColunaImp("M3", "M3", typeof(decimal), 9999.9999M, 0M);
                Imp.AddColunaImp("Valor", "Valor", typeof(decimal), 9999.9999M, 0M);
            };
            if (Imp.Carrega())
            {
                if (Imp.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    int APT;
                    decimal Leitura;
                    using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                    {
                        ESP.AtivaGauge(Imp._DataTable.Rows.Count);
                        foreach (DataRow DR in Imp._DataTable.Rows)
                        {
                            ESP.Gauge();
                            //if ((bool)DR["OK"] && (DR["Leitura"] != DBNull.Value))
                            if ((bool)DR["OK"])
                            {
                                APT = (int)DR["APT"];
                                if (DR["Leitura"] != DBNull.Value)
                                    Leitura = (decimal)DR["Leitura"];
                                else
                                    Leitura = 0;
                                dGradeGas.UnidadesRow rowUnidade = dGradeGas.Unidades.FindByAPT(APT);
                                rowUnidade[CompImput.ToString()] = Math.Round(Leitura, 2);
                                /*
                                if (Tipo != TipoApontamento.TipoLuz)
                                {
                                    if ((DR["M3"] != null) && (DR["M3"] != DBNull.Value))
                                        rowUnidade["m3" + CompImput.ToString()] = (decimal)DR["M3"];
                                    if ((DR["Valor"] != null) && (DR["Valor"] != DBNull.Value))
                                        rowUnidade["R" + CompImput.ToString()] = (decimal)DR["Valor"];
                                }
                                else*/
                                if ((Tipo != TipoApontamento.TipoGas) || (PrecoDoGas == 0))
                                {
                                    if ((DR["M3"] != null) && (DR["M3"] != DBNull.Value))
                                        rowUnidade["m3" + CompImput.ToString()] = (decimal)DR["M3"];
                                    if ((DR["Valor"] != null) && (DR["Valor"] != DBNull.Value))
                                        rowUnidade["R" + CompImput.ToString()] = (decimal)DR["Valor"];
                                };
                                CalculaGravaRow(rowUnidade);
                                rowUnidade.AcceptChanges();
                            }
                        }
                    }
                }
            }
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            if (lookupCondominio_F1.CON_sel < 0)
                return;
            FInputCompetencia FInputCompetencia = new FInputCompetencia();
            if(UltimaCompDados.CompetenciaBind + 1 < CompFinal.CompetenciaBind)
                FInputCompetencia.cCompet1.Comp = UltimaCompDados.CloneCompet(1);
            else
                FInputCompetencia.cCompet1.Comp = CompFinal.CloneCompet();
            FInputCompetencia.cCompet1.AjustaTela();
            if (FInputCompetencia.ShowDialog() != DialogResult.OK)
                return;
            Competencia CompImput = FInputCompetencia.cCompet1.Comp;
            if ((CompImput < cCompet1.Comp) || (CompImput > CompFinal))
            {
                MessageBox.Show("Compet�ncia inv�lida");
                return;
            }
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                TXT_Ista arquivoLido = new TXT_Ista(openFileDialog1.FileName, lookupCondominio_F1.CON_sel);
                cConfiguraImpGas cConf = new cConfiguraImpGas(arquivoLido.DS);
                if (cConf.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    foreach (dTXT_Ista.LeituraRow rowLeitura in arquivoLido.DS.Leitura)
                    {
                        if ((rowLeitura.Tipo.Trim().ToUpper() == "AGUA") && (Tipo == TipoApontamento.TipoGas))
                            continue;
                        if ((rowLeitura.Tipo.Trim().ToUpper() == "GAS") && (Tipo == TipoApontamento.TipoAgua))
                            continue;
                        if ((rowLeitura.Tipo.Trim().ToUpper() == "LUZ") && (Tipo == TipoApontamento.TipoLuz))
                            continue;
                        int APT = arquivoLido.BustaCadastrado(rowLeitura.Bloco, rowLeitura.APT).APT;
                        decimal Leitura = rowLeitura.LeituraAtual / 100;
                        dGradeGas.UnidadesRow rowUnidade = dGradeGas.Unidades.FindByAPT(APT);
                        rowUnidade[CompImput.ToString()] = Math.Round(Leitura, 2);
                        if ((Tipo != TipoApontamento.TipoGas) || (PrecoDoGas == 0))
                        {
                            if (!rowLeitura.IsConsumoNull())

                                rowUnidade["m3" + CompImput.ToString()] = Math.Round(rowLeitura.Consumo / 100, 2);
                            if (!rowLeitura.IsValorNull())
                                rowUnidade["R" + CompImput.ToString()] = rowLeitura.Valor;
                        };
                        CalculaGravaRow(rowUnidade);
                        rowUnidade.AcceptChanges();
                    }
                }                
            };

        }

        


        

        decimal PrecoDoGas = 0;


        

      


        private bool diferente(object O1, object O2) { 
            decimal D1=0;
            decimal D2=0;
            if (O1 is decimal)
                D1 = (decimal)O1;
            if (O2 is decimal)
                D2 = (decimal)O2;
            return D1 != D2;
        }
        
        
        

        

        private void BotGerar_Click(object sender, EventArgs e)
        {
            CompFinal.Add(1);
            CriaColuna(CompFinal);
            BotGerar.Text = "Gerar " + CompFinal.CloneCompet().Add(1);
        }

        

        private void spinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            //gridView1.Invalidate();
        }

        

        private void LimpaTela() {
            dGradeGas.GAS.Clear();
            dGradeGas.Unidades.Clear();
            simpleButton2.Enabled = BotImp.Enabled = false;
        }

        

        private void radioTipo_EditValueChanged(object sender, EventArgs e)
        {
            LimpaTela();
        }

        

        private int ValorTipo(TipoApontamento Tipo) {
            switch (Tipo) {
                case TipoApontamento.TipoGas: return 1;
                case TipoApontamento.TipoAgua: return 2;
                case TipoApontamento.TipoLuz: return 3;
            };
            return 0;
        }

        

        private void lookUpEdit5_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

        }

        private void lookUpCTL_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                lookUpCTL.EditValue = null;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                gridView1.ExportToXls(saveFileDialog1.FileName);
        }

        

        

               

    }

    
}

