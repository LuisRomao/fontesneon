using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Boletos.Boleto.Impresso
{
    /// <summary>
    /// Relat�rio Vencidos
    /// </summary>
    public partial class impVencidos : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public impVencidos()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataCorte"></param>
        /// <param name="NomeCondominio"></param>
        public void SetaDados(DateTime DataCorte,string NomeCondominio){
            bindingSource1.Sort = "BLOCodigo,APTNumero,BOLVencto";
            BoletosVencidosAte.Text = "Boletos Vencidos at� " + DataCorte.ToString("dd/MM/yyyy");
            this.NomeCondominio.Text = NomeCondominio;
            if (DateTime.Now > DataCorte.AddDays(3))
                mensagem.Visible = false;
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
          //  System.Data.DataRowView DRV = (System.Data.DataRowView)bindingSource1.Current;
            //Boletos.Boleto.dBoletos.BOLetosRow row = (Boletos.Boleto.dBoletos.BOLetosRow)DRV.Row;
             
            
        }

        private void xrLabel38_SummaryCalculated(object sender, TextFormatEventArgs e)
        {
  //          if((int)e.Value == 1)
    //            GroupFooter1.Visible = false;
      //      else
        //        GroupFooter1.Visible = true;
        }
    }
}
