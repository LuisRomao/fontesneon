namespace Boletos.Boleto.Impresso
{
    partial class ImpBoletosColunas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            this.dBoletosColunas1 = new Boletos.Boleto.Impresso.dBoletosColunas();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrNumeroBolTit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRespTit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrVencTit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrX0Tit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTotalTitulo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrNumeroBol = new DevExpress.XtraReports.UI.XRLabel();
            this.xrResp = new DevExpress.XtraReports.UI.XRLabel();
            this.xrVenc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrX0 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.compizza = new DevExpress.XtraReports.UI.FormattingRule();
            this.sempizza = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrmarcador = new DevExpress.XtraReports.UI.XRLabel();
            this.ComMarcador = new DevExpress.XtraReports.UI.FormattingRule();
            this.SemMarcadores = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrTotalG = new DevExpress.XtraReports.UI.XRLabel();
            this.xrX0Tot = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSubTotalSum = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrAgrupa = new DevExpress.XtraReports.UI.XRLabel();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrSubTotalTit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrX0SutTot = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSubTotalG = new DevExpress.XtraReports.UI.XRLabel();            
            this.NumeroBoleto = new DevExpress.XtraReports.Parameters.Parameter();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.dBoletosColunas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.HeightF = 319.6588F;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrNumeroBol,
            this.xrResp,
            this.xrVenc,
            this.xrX0,
            this.xrtotal});
            this.Detail.HeightF = 47F;
            this.Detail.StylePriority.UseBorderColor = false;
            this.Detail.StylePriority.UseBorders = false;
            // 
            // dBoletosColunas1
            // 
            this.dBoletosColunas1.DataSetName = "dBoletosColunas";
            this.dBoletosColunas1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(4.000057F, 87.87163F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(128F, 44F);
            this.xrLabel34.Text = "Apto";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrNumeroBolTit
            // 
            this.xrNumeroBolTit.Dpi = 254F;
            this.xrNumeroBolTit.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrNumeroBolTit.LocationFloat = new DevExpress.Utils.PointFloat(146F, 87.87163F);
            this.xrNumeroBolTit.Name = "xrNumeroBolTit";
            this.xrNumeroBolTit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrNumeroBolTit.SizeF = new System.Drawing.SizeF(107F, 41F);
            this.xrNumeroBolTit.Text = "Boleto";
            this.xrNumeroBolTit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrRespTit
            // 
            this.xrRespTit.Dpi = 254F;
            this.xrRespTit.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRespTit.LocationFloat = new DevExpress.Utils.PointFloat(264F, 87.87163F);
            this.xrRespTit.Name = "xrRespTit";
            this.xrRespTit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrRespTit.SizeF = new System.Drawing.SizeF(84F, 41F);
            this.xrRespTit.Text = "Resp";
            this.xrRespTit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrVencTit
            // 
            this.xrVencTit.Dpi = 254F;
            this.xrVencTit.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrVencTit.LocationFloat = new DevExpress.Utils.PointFloat(373F, 87.87163F);
            this.xrVencTit.Name = "xrVencTit";
            this.xrVencTit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrVencTit.SizeF = new System.Drawing.SizeF(101F, 44F);
            this.xrVencTit.Text = "Venc.";
            this.xrVencTit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrX0Tit
            // 
            this.xrX0Tit.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrX0Tit.Dpi = 254F;
            this.xrX0Tit.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrX0Tit.LocationFloat = new DevExpress.Utils.PointFloat(499F, 84F);
            this.xrX0Tit.Multiline = true;
            this.xrX0Tit.Name = "xrX0Tit";
            this.xrX0Tit.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.xrX0Tit.SizeF = new System.Drawing.SizeF(154F, 49.87164F);
            this.xrX0Tit.StylePriority.UseBorders = false;
            this.xrX0Tit.StylePriority.UseFont = false;
            this.xrX0Tit.StylePriority.UsePadding = false;
            this.xrX0Tit.StylePriority.UseTextAlignment = false;
            this.xrX0Tit.Text = "Condom�nio";
            this.xrX0Tit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrX0Tit.WordWrap = false;
            // 
            // xrTotalTitulo
            // 
            this.xrTotalTitulo.Dpi = 254F;
            this.xrTotalTitulo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTotalTitulo.LocationFloat = new DevExpress.Utils.PointFloat(1614F, 87.87163F);
            this.xrTotalTitulo.Name = "xrTotalTitulo";
            this.xrTotalTitulo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalTitulo.SizeF = new System.Drawing.SizeF(184F, 44F);
            this.xrTotalTitulo.StylePriority.UseTextAlignment = false;
            this.xrTotalTitulo.Text = "Total";
            this.xrTotalTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOAPT")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(140F, 42F);
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel3.WordWrap = false;
            // 
            // xrNumeroBol
            // 
            this.xrNumeroBol.BackColor = System.Drawing.Color.Transparent;
            this.xrNumeroBol.CanGrow = false;
            this.xrNumeroBol.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.xrNumeroBol.Dpi = 254F;
            this.xrNumeroBol.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrNumeroBol.LocationFloat = new DevExpress.Utils.PointFloat(144F, 0F);
            this.xrNumeroBol.Name = "xrNumeroBol";
            this.xrNumeroBol.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrNumeroBol.SizeF = new System.Drawing.SizeF(134F, 42F);
            this.xrNumeroBol.StylePriority.UseBackColor = false;
            this.xrNumeroBol.StylePriority.UseFont = false;
            this.xrNumeroBol.StylePriority.UsePadding = false;
            this.xrNumeroBol.Text = "xrNumeroBol";
            this.xrNumeroBol.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrNumeroBol.WordWrap = false;
            // 
            // xrResp
            // 
            this.xrResp.BackColor = System.Drawing.Color.Transparent;
            this.xrResp.CanGrow = false;
            this.xrResp.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PI")});
            this.xrResp.Dpi = 254F;
            this.xrResp.LocationFloat = new DevExpress.Utils.PointFloat(286F, 0F);
            this.xrResp.Name = "xrResp";
            this.xrResp.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrResp.SizeF = new System.Drawing.SizeF(46F, 42F);
            this.xrResp.StylePriority.UseBackColor = false;
            this.xrResp.StylePriority.UseTextAlignment = false;
            this.xrResp.Text = "xrResp";
            this.xrResp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrResp.WordWrap = false;
            // 
            // xrVenc
            // 
            this.xrVenc.BackColor = System.Drawing.Color.Transparent;
            this.xrVenc.CanGrow = false;
            this.xrVenc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yyyy}")});
            this.xrVenc.Dpi = 254F;
            this.xrVenc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrVenc.LocationFloat = new DevExpress.Utils.PointFloat(339F, 0F);
            this.xrVenc.Name = "xrVenc";
            this.xrVenc.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.xrVenc.SizeF = new System.Drawing.SizeF(157F, 42F);
            this.xrVenc.StylePriority.UseBackColor = false;
            this.xrVenc.StylePriority.UseFont = false;
            this.xrVenc.StylePriority.UsePadding = false;
            this.xrVenc.Text = "xrVenc";
            this.xrVenc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrVenc.WordWrap = false;
            // 
            // xrX0
            // 
            this.xrX0.BackColor = System.Drawing.Color.Transparent;
            this.xrX0.CanGrow = false;
            this.xrX0.Dpi = 254F;
            this.xrX0.LocationFloat = new DevExpress.Utils.PointFloat(499F, 0F);
            this.xrX0.Name = "xrX0";
            this.xrX0.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrX0.SizeF = new System.Drawing.SizeF(154F, 42F);
            this.xrX0.StylePriority.UseBackColor = false;
            this.xrX0.Text = "xrX0";
            this.xrX0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrX0.WordWrap = false;
            // 
            // xrtotal
            // 
            this.xrtotal.BackColor = System.Drawing.Color.Gainsboro;
            this.xrtotal.CanGrow = false;
            this.xrtotal.Dpi = 254F;
            this.xrtotal.LocationFloat = new DevExpress.Utils.PointFloat(1614F, 0F);
            this.xrtotal.Name = "xrtotal";
            this.xrtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrtotal.SizeF = new System.Drawing.SizeF(185F, 42F);
            this.xrtotal.Text = "xrtotal";
            this.xrtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrtotal.WordWrap = false;
            // 
            // compizza
            // 
            this.compizza.Condition = "[Parameters.Pizza] == true";
            // 
            // 
            // 
            this.compizza.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.compizza.Name = "compizza";
            // 
            // sempizza
            // 
            this.sempizza.Condition = "[Parameters.Pizza] == False";
            // 
            // 
            // 
            this.sempizza.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.sempizza.Name = "sempizza";
            // 
            // xrmarcador
            // 
            this.xrmarcador.Dpi = 254F;
            this.xrmarcador.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrmarcador.FormattingRules.Add(this.ComMarcador);
            this.xrmarcador.FormattingRules.Add(this.SemMarcadores);
            this.xrmarcador.LocationFloat = new DevExpress.Utils.PointFloat(1576.964F, 33.58F);
            this.xrmarcador.Name = "xrmarcador";
            this.xrmarcador.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrmarcador.SizeF = new System.Drawing.SizeF(39.03601F, 33.42F);
            this.xrmarcador.StylePriority.UseFont = false;
            this.xrmarcador.Text = "(1)";
            // 
            // ComMarcador
            // 
            this.ComMarcador.Condition = "[Parameters.Marcadores] == True";
            // 
            // 
            // 
            this.ComMarcador.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.ComMarcador.Name = "ComMarcador";
            // 
            // SemMarcadores
            // 
            this.SemMarcadores.Condition = "[Parameters.Marcadores]  == False";
            // 
            // 
            // 
            this.SemMarcadores.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.SemMarcadores.Name = "SemMarcadores";
            // 
            // xrTotalG
            // 
            this.xrTotalG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.dBoletosColunas1, "Boletos.BOLValorPago")});
            this.xrTotalG.Dpi = 254F;
            this.xrTotalG.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTotalG.LocationFloat = new DevExpress.Utils.PointFloat(1616F, 22.99995F);
            this.xrTotalG.Name = "xrTotalG";
            this.xrTotalG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalG.SizeF = new System.Drawing.SizeF(182.0001F, 44F);
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTotalG.Summary = xrSummary1;
            this.xrTotalG.Text = "xrTotalG";
            this.xrTotalG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTotalG.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrTotalG_Draw);
            // 
            // xrX0Tot
            // 
            this.xrX0Tot.BackColor = System.Drawing.Color.Transparent;
            this.xrX0Tot.CanGrow = false;
            this.xrX0Tot.Dpi = 254F;
            this.xrX0Tot.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrX0Tot.LocationFloat = new DevExpress.Utils.PointFloat(499.0001F, 25.00001F);
            this.xrX0Tot.Name = "xrX0Tot";
            this.xrX0Tot.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrX0Tot.SizeF = new System.Drawing.SizeF(154F, 42F);
            this.xrX0Tot.StylePriority.UseBackColor = false;
            this.xrX0Tot.StylePriority.UseFont = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrX0Tot.Summary = xrSummary2;
            this.xrX0Tot.Text = "xrX0Tot";
            this.xrX0Tot.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrX0Tot.WordWrap = false;
            // 
            // xrSubTotalSum
            // 
            this.xrSubTotalSum.BackColor = System.Drawing.Color.Transparent;
            this.xrSubTotalSum.CanGrow = false;
            this.xrSubTotalSum.Dpi = 254F;
            this.xrSubTotalSum.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSubTotalSum.LocationFloat = new DevExpress.Utils.PointFloat(101.0626F, 25.00001F);
            this.xrSubTotalSum.Name = "xrSubTotalSum";
            this.xrSubTotalSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSubTotalSum.SizeF = new System.Drawing.SizeF(312F, 42F);
            this.xrSubTotalSum.StylePriority.UseBackColor = false;
            this.xrSubTotalSum.StylePriority.UseFont = false;
            xrSummary3.FormatString = "{0:n2}";
            this.xrSubTotalSum.Summary = xrSummary3;
            this.xrSubTotalSum.Text = "TOTAL GERAL";
            this.xrSubTotalSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrSubTotalSum.WordWrap = false;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1,
            this.xrX0Tit,
            this.xrNumeroBolTit,
            this.xrRespTit,
            this.xrVencTit,
            this.xrLabel34,
            this.xrTotalTitulo});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Ordem", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 134.3412F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.StylePriority.UseBackColor = false;
            this.GroupHeader1.StylePriority.UseBorders = false;
            // 
            // xrPanel1
            // 
            this.xrPanel1.BackColor = System.Drawing.Color.Silver;
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrAgrupa});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1786F, 77F);
            this.xrPanel1.StylePriority.UseBackColor = false;
            // 
            // xrAgrupa
            // 
            this.xrAgrupa.AutoWidth = true;
            this.xrAgrupa.BackColor = System.Drawing.Color.Transparent;
            this.xrAgrupa.CanGrow = false;
            this.xrAgrupa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Agrupamento")});
            this.xrAgrupa.Dpi = 254F;
            this.xrAgrupa.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrAgrupa.ForeColor = System.Drawing.Color.White;
            this.xrAgrupa.LocationFloat = new DevExpress.Utils.PointFloat(0F, 13F);
            this.xrAgrupa.Name = "xrAgrupa";
            this.xrAgrupa.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrAgrupa.SizeF = new System.Drawing.SizeF(1730F, 53F);
            this.xrAgrupa.StylePriority.UseBorderColor = false;
            this.xrAgrupa.StylePriority.UseFont = false;
            this.xrAgrupa.StylePriority.UseForeColor = false;
            this.xrAgrupa.StylePriority.UseTextAlignment = false;
            this.xrAgrupa.Text = "xrAgrupa";
            this.xrAgrupa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrAgrupa.WordWrap = false;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "Boletos";
            this.bindingSource1.DataSource = this.dBoletosColunas1;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubTotalTit,
            this.xrX0SutTot,
            this.xrSubTotalG});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 71F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrSubTotalTit
            // 
            this.xrSubTotalTit.BackColor = System.Drawing.Color.Transparent;
            this.xrSubTotalTit.CanGrow = false;
            this.xrSubTotalTit.Dpi = 254F;
            this.xrSubTotalTit.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSubTotalTit.LocationFloat = new DevExpress.Utils.PointFloat(206F, 0F);
            this.xrSubTotalTit.Name = "xrSubTotalTit";
            this.xrSubTotalTit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSubTotalTit.SizeF = new System.Drawing.SizeF(256F, 42F);
            this.xrSubTotalTit.StylePriority.UseBackColor = false;
            this.xrSubTotalTit.StylePriority.UseFont = false;
            xrSummary4.FormatString = "{0:n2}";
            this.xrSubTotalTit.Summary = xrSummary4;
            this.xrSubTotalTit.Text = "Sub TOTAL";
            this.xrSubTotalTit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrSubTotalTit.WordWrap = false;
            // 
            // xrX0SutTot
            // 
            this.xrX0SutTot.BackColor = System.Drawing.Color.Transparent;
            this.xrX0SutTot.CanGrow = false;
            this.xrX0SutTot.Dpi = 254F;
            this.xrX0SutTot.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrX0SutTot.LocationFloat = new DevExpress.Utils.PointFloat(499F, 0F);
            this.xrX0SutTot.Name = "xrX0SutTot";
            this.xrX0SutTot.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrX0SutTot.SizeF = new System.Drawing.SizeF(154F, 42F);
            this.xrX0SutTot.StylePriority.UseBackColor = false;
            this.xrX0SutTot.StylePriority.UseFont = false;
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrX0SutTot.Summary = xrSummary5;
            this.xrX0SutTot.Text = "xrLabel2";
            this.xrX0SutTot.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrX0SutTot.WordWrap = false;
            // 
            // xrSubTotalG
            // 
            this.xrSubTotalG.BackColor = System.Drawing.Color.Transparent;
            this.xrSubTotalG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPago")});
            this.xrSubTotalG.Dpi = 254F;
            this.xrSubTotalG.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSubTotalG.LocationFloat = new DevExpress.Utils.PointFloat(1614F, 0F);
            this.xrSubTotalG.Name = "xrSubTotalG";
            this.xrSubTotalG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSubTotalG.SizeF = new System.Drawing.SizeF(185F, 44F);
            this.xrSubTotalG.StylePriority.UseBackColor = false;
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrSubTotalG.Summary = xrSummary6;
            this.xrSubTotalG.Text = "xrSubTotalG";
            this.xrSubTotalG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // bottomMarginBand1
            //             
            this.bottomMarginBand1.HeightF = 254F;            
            // 
            // NumeroBoleto
            // 
            this.NumeroBoleto.Description = "N�mero do boleto";
            this.NumeroBoleto.Name = "NumeroBoleto";
            this.NumeroBoleto.Type = typeof(bool);
            this.NumeroBoleto.ValueInfo = "True";
            this.NumeroBoleto.Visible = false;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader2.HeightF = 23.8125F;
            this.GroupHeader2.Level = 1;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubTotalSum,
            this.xrX0Tot,
            this.xrTotalG,
            this.xrmarcador});
            this.GroupFooter2.Dpi = 254F;
            this.GroupFooter2.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter2.HeightF = 76.72916F;
            this.GroupFooter2.Level = 1;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // ImpBoletosColunas
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.GroupHeader2,
            this.GroupFooter2});
            this.DataSource = this.bindingSource1;
            this.DrawGrid = false;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.compizza,
            this.ComMarcador,
            this.SemMarcadores,
            this.sempizza});
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.NumeroBoleto});
            this.SnappingMode = DevExpress.XtraReports.UI.SnappingMode.None;
            this.Version = "14.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ImpBoletosColunas_BeforePrint);
            this.Controls.SetChildIndex(this.GroupFooter2, 0);
            this.Controls.SetChildIndex(this.GroupHeader2, 0);
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dBoletosColunas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrNumeroBolTit;
        private DevExpress.XtraReports.UI.XRLabel xrRespTit;
        private DevExpress.XtraReports.UI.XRLabel xrVencTit;
        private DevExpress.XtraReports.UI.XRLabel xrX0Tit;
        private DevExpress.XtraReports.UI.XRLabel xrTotalTitulo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrNumeroBol;
        private DevExpress.XtraReports.UI.XRLabel xrResp;
        private DevExpress.XtraReports.UI.XRLabel xrVenc;
        private DevExpress.XtraReports.UI.XRLabel xrX0;
        private DevExpress.XtraReports.UI.XRLabel xrtotal;
        private DevExpress.XtraReports.UI.XRLabel xrTotalG;
        private DevExpress.XtraReports.UI.XRLabel xrX0Tot;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrAgrupa;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrSubTotalSum;
        private DevExpress.XtraReports.UI.XRLabel xrSubTotalTit;
        private DevExpress.XtraReports.UI.XRLabel xrX0SutTot;
        private DevExpress.XtraReports.UI.XRLabel xrSubTotalG;
        
        
        /// <summary>
        /// 
        /// </summary>
        public dBoletosColunas dBoletosColunas1;
        private DevExpress.XtraReports.Parameters.Parameter NumeroBoleto;
        private DevExpress.XtraReports.UI.FormattingRule compizza;
        private DevExpress.XtraReports.UI.XRLabel xrmarcador;
        private DevExpress.XtraReports.UI.FormattingRule ComMarcador;
        private DevExpress.XtraReports.UI.FormattingRule SemMarcadores;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.FormattingRule sempizza;

    }
}
