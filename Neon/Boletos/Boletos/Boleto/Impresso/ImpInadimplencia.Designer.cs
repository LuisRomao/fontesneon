namespace Boletos.Boleto.Impresso
{
    partial class ImpInadimplencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.RuleCorrecao1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.RuleCorrecao2 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.RuleMulta1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.RuleMulta2 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.RuleHonorarios1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.RuleHonorarios2 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.RuleProtesto1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.RuleProtesto2 = new DevExpress.XtraReports.UI.FormattingRule();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.mensagem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.parHonorarios = new DevExpress.XtraReports.Parameters.Parameter();
            this.parMultaJuros = new DevExpress.XtraReports.Parameters.Parameter();
            this.parCorrecao = new DevExpress.XtraReports.Parameters.Parameter();
            this.parQuitados = new DevExpress.XtraReports.Parameters.Parameter();
            this.parBoletosFuturos = new DevExpress.XtraReports.Parameters.Parameter();
            this.parDataCorte = new DevExpress.XtraReports.Parameters.Parameter();
            this.parDadosProtesto = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.calCorJur = new DevExpress.XtraReports.UI.CalculatedField();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelPrazo = new DevExpress.XtraReports.UI.XRLabel();
            this.parCODCON = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel48,
            this.xrLine4,
            this.xrLabel45,
            this.xrLabel43,
            this.xrLabel41,
            this.xrLabel39,
            this.xrLine1,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel25,
            this.xrLabel26,
            this.xrLabel27,
            this.xrLabel28,
            this.xrLabel30,
            this.xrLabel31,
            this.xrLabel32,
            this.xrLabel33,
            this.xrLabel34,
            this.xrLabel6});
            this.PageHeader.HeightF = 356F;
            this.PageHeader.Controls.SetChildIndex(this.xrLabel6, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel34, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel33, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel32, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel31, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel30, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel28, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel27, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel26, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel25, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel24, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel23, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLine1, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrTitulo, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel39, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel41, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel43, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel45, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLine4, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel48, 0);
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelPrazo,
            this.xrLine3,
            this.xrLabel46,
            this.xrLabel44,
            this.xrLabel42,
            this.xrLabel40,
            this.xrLine2,
            this.xrLabel5,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel4,
            this.xrLabel3});
            this.Detail.HeightF = 49.00001F;
            this.Detail.KeepTogether = true;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(BoletosProc.Boleto.dBoletos.BOLetosDataTable);
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 296F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(53F, 43F);
            this.xrLabel6.Text = "Bl.";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(64F, 296F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(107F, 43F);
            this.xrLabel34.Text = "Apto";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Dpi = 254F;
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(180F, 296F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(107F, 41F);
            this.xrLabel33.Text = "Boleto";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Dpi = 254F;
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(296F, 296F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(84F, 41F);
            this.xrLabel32.Text = "Resp";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(407F, 296F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(201F, 43F);
            this.xrLabel31.Text = "Vencimento";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Dpi = 254F;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(614F, 296F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(163F, 43F);
            this.xrLabel30.Text = "Original";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.FormattingRules.Add(this.RuleCorrecao1);
            this.xrLabel28.FormattingRules.Add(this.RuleCorrecao2);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(794F, 296F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(163F, 43F);
            this.xrLabel28.Text = "Corrigido";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // RuleCorrecao1
            // 
            this.RuleCorrecao1.Condition = "[Parameters.parCorrecao] == False";
            // 
            // 
            // 
            this.RuleCorrecao1.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.RuleCorrecao1.Name = "RuleCorrecao1";
            // 
            // RuleCorrecao2
            // 
            this.RuleCorrecao2.Condition = "[Parameters.parCorrecao] == True";
            // 
            // 
            // 
            this.RuleCorrecao2.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.RuleCorrecao2.Name = "RuleCorrecao2";
            // 
            // xrLabel27
            // 
            this.xrLabel27.Dpi = 254F;
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel27.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(958F, 296F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(159F, 43F);
            this.xrLabel27.Text = "Multa";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // RuleMulta1
            // 
            this.RuleMulta1.Condition = "[Parameters.parCorrecao] == False Or [Parameters.parMultaJuros] == False";
            // 
            // 
            // 
            this.RuleMulta1.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.RuleMulta1.Name = "RuleMulta1";
            // 
            // RuleMulta2
            // 
            this.RuleMulta2.Condition = "[Parameters.parCorrecao] == True and [Parameters.parMultaJuros] == True";
            // 
            // 
            // 
            this.RuleMulta2.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.RuleMulta2.Name = "RuleMulta2";
            // 
            // xrLabel26
            // 
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel26.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(1143F, 296F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(95F, 43F);
            this.xrLabel26.Text = "Juros";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel25.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(1259F, 296F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(170F, 43F);
            this.xrLabel25.Text = "Sub-Total";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Dpi = 254F;
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.FormattingRules.Add(this.RuleHonorarios1);
            this.xrLabel24.FormattingRules.Add(this.RuleHonorarios2);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(1455F, 296F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(180F, 43F);
            this.xrLabel24.Text = "Honor�rios";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // RuleHonorarios1
            // 
            this.RuleHonorarios1.Condition = "[Parameters.parCorrecao] == False Or [Parameters.parMultaJuros] == False Or [Para" +
    "meters.parHonorarios] == False";
            // 
            // 
            // 
            this.RuleHonorarios1.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.RuleHonorarios1.Name = "RuleHonorarios1";
            // 
            // RuleHonorarios2
            // 
            this.RuleHonorarios2.Condition = "[Parameters.parCorrecao] == True and [Parameters.parMultaJuros] == True And [Para" +
    "meters.parHonorarios] == True";
            // 
            // 
            // 
            this.RuleHonorarios2.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.RuleHonorarios2.Name = "RuleHonorarios2";
            // 
            // xrLabel23
            // 
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.FormattingRules.Add(this.RuleHonorarios1);
            this.xrLabel23.FormattingRules.Add(this.RuleHonorarios2);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(1667F, 296F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(127F, 43F);
            this.xrLabel23.Text = "Total";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("BLOCodigo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("APTNumero", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCodigo")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(64F, 43F);
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(69F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(84F, 43F);
            this.xrLabel4.Text = "xrLabel4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(159F, 0F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(157F, 43F);
            this.xrLabel7.Text = "xrLabel7";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel8
            // 
            this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Destinatario")});
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(323F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(79F, 42F);
            this.xrLabel8.Text = "xrLabel8";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yyyy}")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(407F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(170F, 43F);
            this.xrLabel9.Text = "xrLabel9";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.Gainsboro;
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:n2}")});
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(582F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(196F, 42F);
            this.xrLabel10.StylePriority.UseBackColor = false;
            this.xrLabel10.Text = "xrLabel10";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel10.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel10_Draw);
            // 
            // xrLabel11
            // 
            this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorCorrigido", "{0:n2}")});
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.FormattingRules.Add(this.RuleCorrecao1);
            this.xrLabel11.FormattingRules.Add(this.RuleCorrecao2);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(788F, 0F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(169F, 43F);
            this.xrLabel11.Text = "xrLabel11";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel12
            // 
            this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Multa", "{0:n2}")});
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel12.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(958F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(159F, 43F);
            this.xrLabel12.Text = "xrLabel12";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel13
            // 
            this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Juros", "{0:n2}")});
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel13.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(1122F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(138F, 44F);
            this.xrLabel13.Text = "xrLabel13";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel14
            // 
            this.xrLabel14.BackColor = System.Drawing.Color.Gainsboro;
            this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubTotal", "{0:n2}")});
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel14.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(1270F, 0F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(169F, 44F);
            this.xrLabel14.Text = "xrLabel14";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel14.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel14_Draw);
            // 
            // xrLabel15
            // 
            this.xrLabel15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Honorarios", "{0:n2}")});
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.FormattingRules.Add(this.RuleHonorarios1);
            this.xrLabel15.FormattingRules.Add(this.RuleHonorarios2);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(1445F, 0F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(159F, 43F);
            this.xrLabel15.Text = "xrLabel15";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.Gainsboro;
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Total", "{0:n2}")});
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.FormattingRules.Add(this.RuleHonorarios1);
            this.xrLabel5.FormattingRules.Add(this.RuleHonorarios2);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(1614F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(185F, 44F);
            this.xrLabel5.Text = "xrLabel5";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel5.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel5_Draw);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel2,
            this.xrLabel35,
            this.xrLabel38,
            this.xrLabel47});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 61.00001F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto")});
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(566F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrLabel18.StylePriority.UseBackColor = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel18.Summary = xrSummary1;
            this.xrLabel18.Text = "xrLabel18";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 254F;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(450F, 0F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(116F, 44F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.Text = "Boletos";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Total")});
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.FormattingRules.Add(this.RuleHonorarios1);
            this.xrLabel16.FormattingRules.Add(this.RuleHonorarios2);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(1603F, 0F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(190F, 43F);
            this.xrLabel16.StylePriority.UseBackColor = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel16.Summary = xrSummary2;
            this.xrLabel16.Text = "xrLabel1";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubTotal")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel2.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(1265F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(169F, 44F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel2.Summary = xrSummary3;
            this.xrLabel2.Text = "xrLabel2";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Dpi = 254F;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(79F, 0F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(274F, 43F);
            this.xrLabel35.Text = "Total da unidade:";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(360F, 0F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(85F, 44F);
            xrSummary4.Func = DevExpress.XtraReports.UI.SummaryFunc.Count;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel38.Summary = xrSummary4;
            this.xrLabel38.Text = "xrLabel38";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel47
            // 
            this.xrLabel47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubTotal")});
            this.xrLabel47.Dpi = 254F;
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel47.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel47.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(2352.999F, 0F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(169F, 44F);
            this.xrLabel47.StylePriority.UseBackColor = false;
            this.xrLabel47.StylePriority.UseFont = false;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel47.Summary = xrSummary5;
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // RuleProtesto1
            // 
            this.RuleProtesto1.Condition = "[Parameters.parDadosProtesto] == True";
            // 
            // 
            // 
            this.RuleProtesto1.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.RuleProtesto1.Name = "RuleProtesto1";
            // 
            // RuleProtesto2
            // 
            this.RuleProtesto2.Condition = "[Parameters.parDadosProtesto] == False";
            // 
            // 
            // 
            this.RuleProtesto2.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.RuleProtesto2.Name = "RuleProtesto2";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel37,
            this.xrLabel36,
            this.xrLabel29,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLabel19,
            this.mensagem});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 107F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel37
            // 
            this.xrLabel37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Total")});
            this.xrLabel37.Dpi = 254F;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.FormattingRules.Add(this.RuleHonorarios1);
            this.xrLabel37.FormattingRules.Add(this.RuleHonorarios2);
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(1614F, 21F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(185F, 44F);
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel37.Summary = xrSummary6;
            this.xrLabel37.Text = "xrLabel22";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel36
            // 
            this.xrLabel36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Honorarios")});
            this.xrLabel36.Dpi = 254F;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.FormattingRules.Add(this.RuleHonorarios1);
            this.xrLabel36.FormattingRules.Add(this.RuleHonorarios2);
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(1460F, 21F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(153F, 44F);
            xrSummary7.FormatString = "{0:n2}";
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel36.Summary = xrSummary7;
            this.xrLabel36.Text = "xrLabel21";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel29
            // 
            this.xrLabel29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubTotal")});
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel29.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(1286F, 21F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(169F, 44F);
            xrSummary8.FormatString = "{0:n2}";
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel29.Summary = xrSummary8;
            this.xrLabel29.Text = "xrLabel20";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel22
            // 
            this.xrLabel22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Juros")});
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel22.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(1117F, 21F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(169F, 44F);
            xrSummary9.FormatString = "{0:n2}";
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel22.Summary = xrSummary9;
            this.xrLabel22.Text = "xrLabel19";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel21
            // 
            this.xrLabel21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Multa")});
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.FormattingRules.Add(this.RuleMulta1);
            this.xrLabel21.FormattingRules.Add(this.RuleMulta2);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(958F, 21F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(153F, 44F);
            xrSummary10.FormatString = "{0:n2}";
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel21.Summary = xrSummary10;
            this.xrLabel21.Text = "xrLabel18";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel20
            // 
            this.xrLabel20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorCorrigido")});
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.FormattingRules.Add(this.RuleCorrecao1);
            this.xrLabel20.FormattingRules.Add(this.RuleCorrecao2);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(788F, 21F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(169F, 44F);
            xrSummary11.FormatString = "{0:n2}";
            xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel20.Summary = xrSummary11;
            this.xrLabel20.Text = "xrLabel17";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel19
            // 
            this.xrLabel19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto")});
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(497F, 21F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(280F, 44F);
            xrSummary12.FormatString = "{0:n2}";
            xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel19.Summary = xrSummary12;
            this.xrLabel19.Text = "xrLabel16";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // mensagem
            // 
            this.mensagem.Dpi = 254F;
            this.mensagem.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mensagem.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64F);
            this.mensagem.Name = "mensagem";
            this.mensagem.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.mensagem.SizeF = new System.Drawing.SizeF(413F, 42F);
            this.mensagem.Text = "Dados sujeitos a altera��es";
            this.mensagem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LineWidth = 3;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 44F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine2.SizeF = new System.Drawing.SizeF(1800F, 5F);
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 339F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(1800F, 5F);
            // 
            // parHonorarios
            // 
            this.parHonorarios.Description = "Honor�rios";
            this.parHonorarios.Name = "parHonorarios";
            this.parHonorarios.Type = typeof(bool);
            this.parHonorarios.ValueInfo = "False";
            // 
            // parMultaJuros
            // 
            this.parMultaJuros.Description = "Incluir multa/juros";
            this.parMultaJuros.Name = "parMultaJuros";
            this.parMultaJuros.Type = typeof(bool);
            this.parMultaJuros.ValueInfo = "False";
            // 
            // parCorrecao
            // 
            this.parCorrecao.Description = "Incluir corre��o";
            this.parCorrecao.Name = "parCorrecao";
            this.parCorrecao.Type = typeof(bool);
            this.parCorrecao.ValueInfo = "False";
            // 
            // parQuitados
            // 
            this.parQuitados.Description = "Incluir boletos quitados";
            this.parQuitados.Name = "parQuitados";
            this.parQuitados.Type = typeof(bool);
            this.parQuitados.ValueInfo = "True";
            // 
            // parBoletosFuturos
            // 
            this.parBoletosFuturos.Description = "Boletos n�o vencidos";
            this.parBoletosFuturos.Name = "parBoletosFuturos";
            this.parBoletosFuturos.Type = typeof(bool);
            this.parBoletosFuturos.ValueInfo = "False";
            // 
            // parDataCorte
            // 
            this.parDataCorte.Description = "Data de corte";
            this.parDataCorte.Name = "parDataCorte";
            this.parDataCorte.Type = typeof(System.DateTime);
            this.parDataCorte.ValueInfo = "08/13/2013 09:27:58";
            this.parDataCorte.Visible = false;
            // 
            // parDadosProtesto
            // 
            this.parDadosProtesto.Description = "Dados Protesto";
            this.parDadosProtesto.Name = "parDadosProtesto";
            this.parDadosProtesto.Type = typeof(bool);
            this.parDadosProtesto.ValueInfo = "False";
            // 
            // xrLabel39
            // 
            this.xrLabel39.Dpi = 254F;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel39.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(1833.959F, 294F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(152.7434F, 43F);
            this.xrLabel39.Text = "Emiss�o";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLEmissao", "{0:dd/MM/yyyy}")});
            this.xrLabel40.Dpi = 254F;
            this.xrLabel40.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel40.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(1833.959F, 0F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(170F, 43F);
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Dpi = 254F;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel41.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(2003.959F, 257.3852F);
            this.xrLabel41.Multiline = true;
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(163.4692F, 81.61487F);
            this.xrLabel41.Text = "Corre��o + Juros";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // calCorJur
            // 
            this.calCorJur.Expression = "[ValorCorrigido] - [BOLValorPrevisto] + [Juros]";
            this.calCorJur.Name = "calCorJur";
            // 
            // xrLabel42
            // 
            this.xrLabel42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calCorJur", "{0:n2}")});
            this.xrLabel42.Dpi = 254F;
            this.xrLabel42.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel42.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(2015.484F, 0F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(169F, 43F);
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel43.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(2195.308F, 296.0001F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(134.688F, 43F);
            this.xrLabel43.Text = "Multa";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Multa", "{0:n2}")});
            this.xrLabel44.Dpi = 254F;
            this.xrLabel44.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel44.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(2195.308F, 0F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(129.0261F, 43F);
            this.xrLabel44.Text = "xrLabel12";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Dpi = 254F;
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel45.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(2374.124F, 296.0001F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(127F, 43F);
            this.xrLabel45.Text = "Total";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.BackColor = System.Drawing.Color.Gainsboro;
            this.xrLabel46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubTotal", "{0:n2}")});
            this.xrLabel46.Dpi = 254F;
            this.xrLabel46.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel46.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(2352.999F, 0F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(169F, 44F);
            this.xrLabel46.Text = "xrLabel14";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel46.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel46_Draw);
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 254F;
            this.xrLine3.FormattingRules.Add(this.RuleProtesto1);
            this.xrLine3.FormattingRules.Add(this.RuleProtesto2);
            this.xrLine3.LineWidth = 3;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(1833.895F, 44.00001F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine3.SizeF = new System.Drawing.SizeF(792.7548F, 5F);
            // 
            // xrLine4
            // 
            this.xrLine4.Dpi = 254F;
            this.xrLine4.FormattingRules.Add(this.RuleProtesto1);
            this.xrLine4.FormattingRules.Add(this.RuleProtesto2);
            this.xrLine4.LineWidth = 3;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(1833.959F, 339.0001F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine4.SizeF = new System.Drawing.SizeF(792.691F, 5F);
            // 
            // xrLabel48
            // 
            this.xrLabel48.Dpi = 254F;
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabel48.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(2521.999F, 294F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(104.5198F, 43F);
            this.xrLabel48.Text = "Prazo";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabelPrazo
            // 
            this.xrLabelPrazo.Dpi = 254F;
            this.xrLabelPrazo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelPrazo.FormattingRules.Add(this.RuleProtesto1);
            this.xrLabelPrazo.FormattingRules.Add(this.RuleProtesto2);
            this.xrLabelPrazo.LocationFloat = new DevExpress.Utils.PointFloat(2545.769F, 0.9999887F);
            this.xrLabelPrazo.Name = "xrLabelPrazo";
            this.xrLabelPrazo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelPrazo.SizeF = new System.Drawing.SizeF(80.85034F, 43F);
            this.xrLabelPrazo.Text = "Prazo";
            this.xrLabelPrazo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parCODCON
            // 
            this.parCODCON.Description = "CODCON";
            this.parCODCON.Name = "parCODCON";
            this.parCODCON.Type = typeof(bool);
            this.parCODCON.ValueInfo = "False";
            // 
            // ImpInadimplencia
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calCorJur});
            this.DataSource = this.bindingSource1;
            this.FilterString = "[BOLPagamento] Is Null";
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.RuleCorrecao1,
            this.RuleCorrecao2,
            this.RuleMulta1,
            this.RuleMulta2,
            this.RuleHonorarios1,
            this.RuleHonorarios2,
            this.RuleProtesto1,
            this.RuleProtesto2});
            this.Landscape = true;
            this.PageHeight = 2100;
            this.PageWidth = 2970;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.parQuitados,
            this.parCorrecao,
            this.parMultaJuros,
            this.parHonorarios,
            this.parBoletosFuturos,
            this.parDataCorte,
            this.parDadosProtesto,
            this.parCODCON});
            this.Version = "16.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ImpInadimplencia_BeforePrint);
            this.Controls.SetChildIndex(this.ReportFooter, 0);
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel mensagem;
        /// <summary>
        /// bindingSource
        /// </summary>
        public System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.FormattingRule RuleCorrecao1;
        private DevExpress.XtraReports.UI.FormattingRule RuleCorrecao2;
        private DevExpress.XtraReports.UI.FormattingRule RuleMulta1;
        private DevExpress.XtraReports.UI.FormattingRule RuleMulta2;
        private DevExpress.XtraReports.UI.FormattingRule RuleHonorarios1;
        private DevExpress.XtraReports.UI.FormattingRule RuleHonorarios2;
        private DevExpress.XtraReports.Parameters.Parameter parBoletosFuturos;
        private DevExpress.XtraReports.Parameters.Parameter parDataCorte;
        /// <summary>
        /// Honor�rios
        /// </summary>
        public DevExpress.XtraReports.Parameters.Parameter parHonorarios;
        /// <summary>
        /// Multa e juros
        /// </summary>
        public DevExpress.XtraReports.Parameters.Parameter parMultaJuros;
        /// <summary>
        /// Corre��o
        /// </summary>
        public DevExpress.XtraReports.Parameters.Parameter parCorrecao;
        private DevExpress.XtraReports.Parameters.Parameter parQuitados;
        private DevExpress.XtraReports.Parameters.Parameter parDadosProtesto;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.FormattingRule RuleProtesto1;
        private DevExpress.XtraReports.UI.FormattingRule RuleProtesto2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.CalculatedField calCorJur;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabelPrazo;
        private DevExpress.XtraReports.Parameters.Parameter parCODCON;
    }
}