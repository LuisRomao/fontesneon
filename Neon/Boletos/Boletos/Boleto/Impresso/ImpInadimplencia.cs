using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Boletos.Boleto.Impresso
{
    /// <summary>
    /// Inadimplencia balancete antigo
    /// </summary>
    public partial class ImpInadimplencia : dllImpresso.ImpLogoCond
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ImpInadimplencia()
        {
            InitializeComponent();
        }

        private string _NomeCondominio;

        /// <summary>
        /// Seta os dados
        /// </summary>
        /// <param name="DataCorte"></param>
        /// <param name="NomeCondominio"></param>
        /// <param name="Quitados"></param>        
        public void SetaDados(DateTime DataCorte, string NomeCondominio,bool Quitados = true)
        {
            bindingSource1.Sort = "BLOCodigo,APTNumero,BOLVencto";
            _NomeCondominio = NomeCondominio;
            SetNome(NomeCondominio);
            xrTitulo.Text = "Boletos Vencidos at� " + DataCorte.ToString("dd/MM/yyyy");
            if (DateTime.Now > DataCorte.AddDays(3))
                mensagem.Visible = false;
            parQuitados.Value = Quitados;            
        }

        private void xrLabel5_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        private void xrLabel14_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        private int? _PrazoProtesto;

        private int? PrazoProtesto
        {
            get
            {
                if (!_PrazoProtesto.HasValue)
                {
                    if (bindingSource1.Count != 0)
                    {
                        System.Data.DataRowView DRV = (System.Data.DataRowView)bindingSource1[0];
                        _PrazoProtesto = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("Select CONProtesto from CONdominios where CON = @P1", DRV["BOL_CON"]);
                    }
                }
                return _PrazoProtesto.GetValueOrDefault(0);
            }
        }

        private string _CODCON;

        private string CODCON
        {
            get
            {
                if (_CODCON == null)
                {
                    if (bindingSource1.Count != 0)
                    {
                        System.Data.DataRowView DRV = (System.Data.DataRowView)bindingSource1[0];
                        _CODCON = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_string("Select CONCodigo from CONdominios where CON = @P1", DRV["BOL_CON"]);
                        _CODCON += " - ";                        
                    }
                }
                return _CODCON;
            }
        }

        private void ImpInadimplencia_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Landscape = (bool)parDadosProtesto.Value;
            if ((bool)parDadosProtesto.Value)                            
                xrLabelPrazo.Text = PrazoProtesto.ToString();
            
            if ((bool)parQuitados.Value)
                FilterString = "";
            else
                FilterString = "[BOLPagamento] Is Null";
            SetNome(string.Format("{0}{1}", ((bool)parCODCON.Value || (bool)parDadosProtesto.Value) ? CODCON : "",  _NomeCondominio));
            if (!(bool)parBoletosFuturos.Value)
            {
                parDataCorte.Value = DateTime.Today;
                string filtro2 = string.Format("[BOLVencto] <= [Parameters.parDataCorte]");
                if (FilterString == "")
                    FilterString = filtro2;
                else
                    FilterString = string.Format("({0}) and ({1})",FilterString, filtro2);
            }

        }

        private void xrLabel10_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        private void xrLabel46_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }
    }
}

