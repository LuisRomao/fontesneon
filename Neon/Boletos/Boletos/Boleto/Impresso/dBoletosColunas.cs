﻿namespace Boletos.Boleto.Impresso {


    partial class dBoletosColunas
    {
        private static dBoletosColunasTableAdapters.DadosCompTableAdapter dadosCompTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DadosComp
        /// </summary>
        public static dBoletosColunasTableAdapters.DadosCompTableAdapter DadosCompTableAdapter
        {
            get
            {
                if (dadosCompTableAdapter == null)
                {
                    dadosCompTableAdapter = new dBoletosColunasTableAdapters.DadosCompTableAdapter();
                    dadosCompTableAdapter.TrocarStringDeConexao();
                };
                return dadosCompTableAdapter;
            }
        }

        private static dBoletosColunasTableAdapters.BoletosTableAdapter boletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Boletos
        /// </summary>
        public  static dBoletosColunasTableAdapters.BoletosTableAdapter BoletosTableAdapter
        {
            get
            {
                if (boletosTableAdapter == null)
                {
                    boletosTableAdapter = new dBoletosColunasTableAdapters.BoletosTableAdapter();
                    boletosTableAdapter.TrocarStringDeConexao();
                };
                return boletosTableAdapter;
            }
        }

        private dBoletosColunasTableAdapters.BoletosColunasTableAdapter boletosColunasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BoletosColunas
        /// </summary>
        public dBoletosColunasTableAdapters.BoletosColunasTableAdapter BoletosColunasTableAdapter
        {
            get
            {
                if (boletosColunasTableAdapter == null)
                {
                    boletosColunasTableAdapter = new dBoletosColunasTableAdapters.BoletosColunasTableAdapter();
                    boletosColunasTableAdapter.TrocarStringDeConexao();
                };
                return boletosColunasTableAdapter;
            }
        }
    }
}
