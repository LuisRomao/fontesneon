﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;

namespace Boletos.Boleto.Impresso
{
    public partial class ImpInadUnidades : dllImpresso.ImpLogoCond
    {
        public ImpInadUnidades()
        {
            InitializeComponent();
        }

        public ImpInadUnidades(AbstratosNeon.ABS_Condominio Condominio, DateTime DataCorte):this()
        {
            SetNome(Condominio.Nome);
            xrTitulo.Text = string.Format("Boletos Vencidos até {0:dd/MM/yyyy}", DataCorte);
            xrBloco.Text = Condominio.CONNomeBloco;
            xrApartamento.Text = Condominio.CONNomeApto;
            BoletosProc.Boleto.dBoletos dBoletosL = new BoletosProc.Boleto.dBoletos();
            dBoletosL.BOLetosTableAdapter.FillByInad(dBoletosL.BOLetos, DataCorte, Condominio.CON);
            SortedList<int,AbstratosNeon.ABS_Apartamento> Apartamentos = AbstratosNeon.ABS_Apartamento.ABSGetApartamentos(Condominio.CON);
            foreach (BoletosProc.Boleto.dBoletos.BOLetosRow rowBOL in dBoletosL.BOLetos)
            {
                dImpInadimplencia.UnidadesRow row = dImpInadimplencia1.Unidades.FindByAPT(rowBOL.BOL_APT);
                if (row == null)
                    row = dImpInadimplencia1.Unidades.AddUnidadesRow(Apartamentos[rowBOL.BOL_APT].BLONome, Apartamentos[rowBOL.BOL_APT].APTNumero, 0, 0, rowBOL.BOL_APT);
                row.Boletos++;
                row.Valor += rowBOL.BOLValorPrevisto;
            }
            
        }

        private void xrLabel5_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }
    }
}
