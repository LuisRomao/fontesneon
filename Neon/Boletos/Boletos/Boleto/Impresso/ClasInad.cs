/*
MR - 01/04/2016 19:00 -           - Novo tipo de agrupamento (5) com lista em colunas e total por bloco (Altera��es indicadas por *** MRC - INICIO (01/04/2016 19:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace Boletos.Boleto.Impresso
{
    /// <summary>
    /// Conte�do para o quadro inadimpl�ncia
    /// </summary>
    public class RetornoInad
    {
        /// <summary>
        /// Titulo do quadro
        /// </summary>
        public string Titulo;

        /// <summary>
        /// Conteudo
        /// </summary>
        public string Conteudo;

        /// <summary>
        /// CalculaUnidadesInadimplenes
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="_Competencia"></param>
        /// <param name="DataBalancete"></param>
        /// <param name="MaxCarar"></param>
        /// <returns></returns>
        public static RetornoInad CalculaUnidadesInadimplenes(int CON, Framework.objetosNeon.Competencia _Competencia, DateTime DataBalancete, int MaxCarar)
        {
            Framework.objetosNeon.Competencia Competencia = _Competencia.CloneCompet();
            int RefData; //0=Emissao 1=Balancete 2=Prompt
            int Periodo; //Dias
            int Agrupamento; //1=Lista 2=Bloco 3=Unidades 
            bool ValorOriginal;
            bool ValorCorrigido;
            bool NumeroDeUnidades;
            string CONCodigo;
            int DestacarUnidadesInadDoMes;//0=Nao Destacar 1=Valor 2=Numero de unidades a mais de 30 dias e do mes
            string Comando = "select CONInaDataRef, CONInaPeriodo, CONInaAgrupamento, CONInaValorOrig, CONInaValorCor, CONInaNumero, CONInaDestacaMes, CONCodigo from CONdominios where CON = @P1";
            System.Collections.ArrayList Dados = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLLinha(Comando, CON);

            if (Dados.Count != 8)
                return null;
            else
            {
                if (Dados[0] == DBNull.Value)
                    RefData = 0;
                else
                    RefData = (int)Dados[0];
                if (Dados[1] == DBNull.Value)
                    Periodo = 0;
                else
                    Periodo = (int)Dados[1];
                if (Dados[2] == DBNull.Value)
                    Agrupamento = 0;
                else
                    Agrupamento = (int)Dados[2];
                if (Dados[3] == DBNull.Value)
                    ValorOriginal = false;
                else
                    ValorOriginal = (bool)Dados[3];
                if (Dados[4] == DBNull.Value)
                    ValorCorrigido = false;
                else
                    ValorCorrigido = (bool)Dados[4];
                if (Dados[5] == DBNull.Value)
                    NumeroDeUnidades = false;
                else
                    NumeroDeUnidades = (bool)Dados[5];
                if (Dados[6] == DBNull.Value)
                    DestacarUnidadesInadDoMes = 0;
                else
                    DestacarUnidadesInadDoMes = (int)Dados[6];
                CONCodigo = Dados[7].ToString();
            };

            return CalculaUnidadesInadimplenes(RefData, Periodo, Agrupamento, ValorOriginal, ValorCorrigido, NumeroDeUnidades, DestacarUnidadesInadDoMes, MaxCarar, DataBalancete, CON, Competencia);
        }

        //*** MRC - INICIO (01/04/2016 19:00) ***
        private class ItemInadimplente
        {
            public int col { get; set; }
            public int row { get; set; }
            public string bloco { get; set; }
            public string linha { get; set; }
        };
        //*** MRC - Termino (01/04/2016 19:00) ***

        /// <summary>
        /// Faz o c�lculo
        /// </summary>
        /// <param name="RefData"></param>
        /// <param name="Periodo"></param>
        /// <param name="Agrupamento"></param>
        /// <param name="ValorOriginal"></param>
        /// <param name="ValorCorrigido"></param>
        /// <param name="NumeroDeUnidades"></param>
        /// <param name="DestacarUnidadesInadDoMes"></param>
        /// <param name="MaxCarar"></param>
        /// <param name="DataBalancete"></param>
        /// <param name="CON"></param>
        /// <param name="_CompetAnterior"></param>
        /// <returns></returns>
        public static RetornoInad CalculaUnidadesInadimplenes(int RefData, int Periodo, int Agrupamento, bool ValorOriginal, bool ValorCorrigido, bool NumeroDeUnidades, int DestacarUnidadesInadDoMes, int MaxCarar, DateTime DataBalancete, int CON, Framework.objetosNeon.Competencia _CompetAnterior)
        {
            Framework.objetosNeon.Competencia CompetAnterior = _CompetAnterior.CloneCompet();
            RetornoInad Retorno = new RetornoInad();

            if (Agrupamento == 0)
                return null;

            DateTime Database = DateTime.Today;

            switch (RefData)
            {
                case 0:
                    Database = DateTime.Today.AddDays(-Periodo);
                    Retorno.Titulo = "UNIDADES INADIMPLENTES a mais de " + Periodo.ToString() + " dias (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
                case 1:
                    Database = DataBalancete.AddDays(-Periodo);
                    Retorno.Titulo = "UNIDADES INADIMPLENTES - Data base: " + Database.ToString("dd/MM/yyyy") + " (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
                case 2:
                    DateTime? novadata = DateTime.Today;
                    if ((!VirInput.Input.Execute("Data de Refer�ncia", ref novadata, null, null)) || !novadata.HasValue)
                        return null;

                    //DateTime novadata = DateTime.Today;
                    //if (!VirInput.Input.Execute("Data de Refer�ncia", ref novadata))
                    //    return null;

                    Database = novadata.Value.AddDays(-Periodo);
                    Retorno.Titulo = "UNIDADES INADIMPLENTES - Data base: " + Database.ToString("dd/MM/yyyy") + " (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
            };

            BoletosProc.Boleto.dBoletos dBoletosInad = new BoletosProc.Boleto.dBoletos();
            dBoletosInad.BOLetosTableAdapter.FillByInad(dBoletosInad.BOLetos, Database, CON);
            dBoletosInad.CarregaAPTs(CON);
            /*
            string comando =
"SELECT     BOL\r\n" +
"FROM         BOLetos\r\n" +
"WHERE     \r\n" +
"(BOL_CON = @P1) \r\n" +
"AND \r\n" +
"(BOLVencto <= @P2) \r\n" +
"AND \r\n" +
"(BOLCancelado = 0) \r\n" +
"AND \r\n" +
"(BOLetos.BOLPagamento IS NULL) \r\n" +
"AND \r\n" +
"(BOLStatus IN (-2,0,2,5));";

            System.Data.DataTable TabBol = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comando,CON,Database);
            */
            SortedList<string, Boleto> BoletosInad = new SortedList<string, Boleto>();


            //foreach (System.Data.DataRow DR in TabBol.Rows)

            foreach (BoletosProc.Boleto.dBoletos.BOLetosRow rowBOL in dBoletosInad.BOLetos)
            {
                Boleto BoletoX = new Boleto(rowBOL);
                if (!BoletoX.APT.HasValue)
                {
                    BoletoX.Cancelar();
                    continue;
                }
                string Titulo = string.Format("{0}-{1}-{2}", (BoletoX.Apartamento.BLOCodigo.ToUpper() == "SB") ? "" : BoletoX.Apartamento.BLOCodigo,
                                                              BoletoX.Apartamento.APTNumero,
                                                              BoletoX.rowPrincipal.BOL);
                BoletosInad.Add(Titulo, BoletoX);
            }

            string TextoUnidadesI = "";
            //*** MRC - INICIO (01/04/2016 19:00) ***
            bool booTipoRTF = false;
            System.Windows.Forms.RichTextBox TextoUnidadesI_RTF = new System.Windows.Forms.RichTextBox();                    
            //*** MRC - TERMINO (01/04/2016 19:00) ***

            ConjuntoLinhasInad Conj = new ConjuntoLinhasInad();

            //CompetAnterior.Add(-1);

            //int i = 0;
            //foreach (Boleto Bol in BoletosInad.Values)
            foreach (string chave in BoletosInad.Keys)
            {
                Boleto Bol = BoletosInad[chave];
                if (ValorCorrigido)
                    Bol.Valorpara(DateTime.Today);
                string Titulo = (Bol.Apartamento.BLOCodigo.ToUpper() == "SB") ? "" : Bol.Apartamento.BLOCodigo + "-";
                Titulo += Bol.Apartamento.APTNumero;

                Conj.Add(Titulo, Bol.rowPrincipal.BOLValorPrevisto, Bol.valorcorrigido, Bol.Apartamento.BLOCodigo, (Bol.Competencia == CompetAnterior));
            };




            int CompUltimalinha = 0;

            switch (Agrupamento)
            {
                case 1://Lista
                    string strValor = " Valor =";
                    if(Conj.Count > 96)
                        strValor = "";
                    foreach (LinhaQuadroInadimplente Linha in Conj)
                    {
                        string novogrupo = Linha.Texto;
                        if (NumeroDeUnidades)
                            novogrupo += " Boletos = " + Linha.Quantidade.ToString("00");
                        if (ValorOriginal)
                            novogrupo += strValor + " R$" + Linha.ValorOriginal.ToString("n2").PadLeft(9);
                        if (ValorCorrigido)
                            novogrupo += " Corrigido = R$" + Linha.ValorCorrigido.ToString("n2").PadLeft(9);
                        novogrupo += "; ";

                        if ((CompUltimalinha + novogrupo.Length) > MaxCarar)
                        {
                            TextoUnidadesI += "\r\n";
                            CompUltimalinha = novogrupo.Length;
                        }
                        else
                            CompUltimalinha += novogrupo.Length;
                        //ordenado.Add(novogrupo, novogrupo);
                        TextoUnidadesI += novogrupo;

                    };
                    //foreach(string ng in ordenado.Keys)
                    //    TextoUnidadesI += ng;
                    break;
                case 2://Blocos
                    ConjuntoLinhaBloco ConjB = new ConjuntoLinhaBloco();

                    foreach (LinhaQuadroInadimplente Linha in Conj)
                        ConjB.Add(Linha.ValorOriginal, Linha.ValorCorrigido, Linha.Bloco);
                    TextoUnidadesI = "";
                    foreach (LinhaBloco LB in ConjB)
                    {
                        TextoUnidadesI += "Bloco " + LB.Bloco + ": ";
                        if (NumeroDeUnidades)
                            TextoUnidadesI += "Unidades: " + LB.Quantidade.ToString("00") + " ";
                        if (ValorOriginal)
                            TextoUnidadesI += "Val.: R$ " + LB.ValorOriginal.ToString("n2").PadLeft(9) + " ";
                        if (ValorCorrigido)
                            TextoUnidadesI += "com encargos: R$ " + LB.ValorCorrigido.ToString("n2").PadLeft(9);
                        TextoUnidadesI += "\r\n";
                    };
                    break;
                case 3://Numero
                    decimal TotalO = 0;
                    decimal TotalC = 0;
                    if (ValorOriginal || ValorCorrigido)
                        foreach (LinhaQuadroInadimplente Linha in Conj)
                        {
                            TotalO += Linha.ValorOriginal;
                            TotalC += Linha.ValorCorrigido;
                        };
                    TextoUnidadesI = "";
                    if (NumeroDeUnidades)
                        TextoUnidadesI += "Unidades: " + Conj.Count.ToString("00") + "\r\n";
                    if (ValorOriginal)
                        TextoUnidadesI += "Valor original: R$ " + TotalO.ToString("n2") + "\r\n";
                    if (ValorCorrigido)
                        TextoUnidadesI += "Valor original + encargos: R$ " + TotalC.ToString("n2");
                    break;
                case 4:
                    foreach (Boleto Bol in BoletosInad.Values)
                        TextoUnidadesI += string.Format("{0} ", Bol.rowPrincipal.BOL);
                    break;
                //*** MRC - INICIO (01/04/2016 19:00) ***
                case 5:
                    booTipoRTF = true;
                    List<ItemInadimplente> tabelaIna = new List<ItemInadimplente>();
                    ConjuntoLinhaBloco ConjBlocos = new ConjuntoLinhaBloco();
                    int x = 0;
                    int y = 1;
                    int y_max = 1;
                    string linha = "";
                    foreach (LinhaQuadroInadimplente Linha in Conj)
                    {
                        if (!tabelaIna.Exists(b => b.bloco == Linha.Bloco))
                        {
                            x++;
                            y = 1;
                        }
                        else
                        {
                            y++;
                            if (y_max < y) y_max = y;
                        }
                        linha = Linha.Texto.Trim();
                        if (ValorOriginal)
                            linha += " Valor = R$" + Linha.ValorOriginal.ToString("n2").PadLeft(9);
                        else if (ValorCorrigido)
                            linha += " Corrigido = R$" + Linha.ValorCorrigido.ToString("n2").PadLeft(9);
                        tabelaIna.Add(new ItemInadimplente() { col = x, row = y, bloco = Linha.Bloco, linha = linha });
                        ConjBlocos.Add(Linha.ValorOriginal, Linha.ValorCorrigido, Linha.Bloco);
                    }
                    string linhavazia = (" ").PadLeft(linha.Length, ' ');
                    TextoUnidadesI_RTF.SelectionTabs = new int[] { 165, 330, 495 };
                    for (int i = 1; i <= y_max + 1; i++)
                    {
                        for (int j = 1; j <= 4; j++)
                        {
                            if (i <= y_max)
                            {
                                ItemInadimplente item = tabelaIna.Find(tab => tab.row == i && tab.col == j);
                                if (item != null)
                                    TextoUnidadesI_RTF.SelectedText += item.linha + (j < 4 ? "\t" : "");
                                else
                                    TextoUnidadesI_RTF.SelectedText += linhavazia + (j < 4 ? "\t" : "");
                            }
                            else
                            {
                                ItemInadimplente item = tabelaIna.Find(tab => tab.row == 1 && tab.col == j);
                                string strTotal = "";
                                if (item != null)
                                {                                 
                                    foreach (LinhaBloco bloco in ConjBlocos)
                                        if (bloco.Bloco == item.bloco)
                                        {
                                            if (ValorOriginal)
                                                strTotal += ("Total " + bloco.Bloco + " = R$" + bloco.ValorOriginal.ToString("n2").PadLeft(10)).PadRight(linhavazia.Length);
                                            else if (ValorCorrigido)
                                                strTotal += ("Total " + bloco.Bloco + " = R$" + bloco.ValorCorrigido.ToString("n2").PadLeft(10)).PadRight(linhavazia.Length);
                                        }
                                }
                                TextoUnidadesI_RTF.SelectionFont = new System.Drawing.Font(TextoUnidadesI_RTF.Font, System.Drawing.FontStyle.Bold);
                                if (strTotal != "")
                                    TextoUnidadesI_RTF.SelectedText += strTotal.ToUpper() + (j < 4 ? "\t" : "");
                                else
                                    TextoUnidadesI_RTF.SelectedText += linhavazia + (j < 4 ? "\t" : "");
                            }
                        }
                        TextoUnidadesI_RTF.SelectedText += "\r\n";
                    }
                    break;
                //*** MRC - TERMINO (01/04/2016 19:00) ***
            }

            switch (DestacarUnidadesInadDoMes)
            {
                case 1:
                    decimal ValorMes = 0;
                    foreach (LinhaQuadroInadimplente Linha in Conj)
                        ValorMes += Linha.ValorDoMes;
                    if (ValorMes > 0)
                        //*** MRC - TERMINO (01/04/2016 19:00) ***
                        if (booTipoRTF) 
                        {
                            TextoUnidadesI_RTF.SelectionFont = new System.Drawing.Font(TextoUnidadesI_RTF.Font, System.Drawing.FontStyle.Bold);
                            TextoUnidadesI_RTF.SelectedText += "\r\nValor inadimpl�ncia do m�s: R$ " + ValorMes.ToString("n2");
                        }
                        else
                            TextoUnidadesI += "\r\nValor inadimpl�ncia do m�s: R$ " + ValorMes.ToString("n2");
                        //*** MRC - TERMINO (01/04/2016 19:00) ***
                    break;
                case 2:
                    int InadMes = 0;
                    foreach (LinhaQuadroInadimplente Linha in Conj)
                        if (Linha.ValorDoMes > 0)
                        //if (Linha.ValorDoMes == Linha.ValorOriginal)
                            InadMes++;
                    //*** MRC - TERMINO (01/04/2016 19:00) ***
                    if (booTipoRTF)
                    {
                        TextoUnidadesI_RTF.SelectionFont = new System.Drawing.Font(TextoUnidadesI_RTF.Font, System.Drawing.FontStyle.Bold);
                        TextoUnidadesI_RTF.SelectedText += string.Format("\r\n{0:00} unidades inadimpl�ntes (Total)", Conj.Count);
                        TextoUnidadesI_RTF.SelectedText += string.Format("\r\n{0:00} unidades inadimpl�ntes compet�ncia {1}", InadMes, CompetAnterior);
                    }
                    else
                    {
                        TextoUnidadesI += string.Format("\r\n{0:00} unidades inadimpl�ntes (Total)", Conj.Count);
                        TextoUnidadesI += string.Format("\r\n{0:00} unidades inadimpl�ntes compet�ncia {1}", InadMes, CompetAnterior);
                    }
                    //*** MRC - TERMINO (01/04/2016 19:00) ***
                    break;
            }

            //*** MRC - TERMINO (01/04/2016 19:00) ***
            if (booTipoRTF)
                Retorno.Conteudo = TextoUnidadesI_RTF.Rtf;
            else
                Retorno.Conteudo = TextoUnidadesI;
            //*** MRC - TERMINO (01/04/2016 19:00) ***

            return Retorno;
        }
    }


    /// <summary>
    /// Classe para o agrupamento dos boletos por unidade (Classe Grupo)
    /// </summary>
    public class ConjuntoLinhasInad : System.Collections.ArrayList
    {
        private LinhaQuadroInadimplente LinhaPeloTitulo(string Texto)
        {
            foreach (LinhaQuadroInadimplente Linha in this)
                if (Linha.Texto == Texto)
                    return Linha;
            return null;
        }

        /// <summary>
        /// add
        /// </summary>
        /// <param name="_Texto"></param>
        /// <param name="_ValorOriginal"></param>
        /// <param name="_ValorCorrigido"></param>
        /// <param name="_Bloco"></param>
        /// <param name="BoletoDoMes"></param>
        public void Add(string _Texto, decimal _ValorOriginal, decimal _ValorCorrigido, string _Bloco, bool BoletoDoMes)
        {
            //int pos = this.IndexOf(_Texto);
            LinhaQuadroInadimplente Linha = LinhaPeloTitulo(_Texto);
            if (Linha == null)
                Add(new LinhaQuadroInadimplente(_Texto, _ValorOriginal, _ValorCorrigido, _Bloco, BoletoDoMes));
            else
            {
                Linha.ValorOriginal += _ValorOriginal;
                Linha.ValorCorrigido += _ValorCorrigido;
                Linha.Quantidade++;
                if (BoletoDoMes)
                    Linha.ValorDoMes += _ValorOriginal;
            }
        }
    }

    /// <summary>
    /// Classe para o agrupamento dos boletos por unidade (classe detalhe)
    /// </summary>
    public class LinhaQuadroInadimplente : Object
    {
        /// <summary>
        /// 
        /// </summary>
        public string Texto;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorOriginal;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorCorrigido;
        /// <summary>
        /// 
        /// </summary>
        public string Bloco;
        /// <summary>
        /// 
        /// </summary>
        public int Quantidade;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorDoMes;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Texto"></param>
        /// <param name="_ValorOriginal"></param>
        /// <param name="_ValorCorrigido"></param>
        /// <param name="_Bloco"></param>
        /// <param name="BoletoDoMes"></param>
        public LinhaQuadroInadimplente(string _Texto, decimal _ValorOriginal, decimal _ValorCorrigido, string _Bloco, bool BoletoDoMes)
        {
            Texto = _Texto;
            ValorOriginal = _ValorOriginal;
            ValorCorrigido = _ValorCorrigido;
            Bloco = _Bloco;
            Quantidade = 1;
            if (BoletoDoMes)
                ValorDoMes = _ValorOriginal;
            else
                ValorDoMes = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Texto;
        }
    }

    /// <summary>
    /// Classe para o agrupamento das unidades em blocos (Classe Grupo)
    /// </summary>
    public class ConjuntoLinhaBloco : System.Collections.ArrayList
    {
        private LinhaBloco LinhaPeloBloco(string Bloco)
        {
            foreach (LinhaBloco Linha in this)
                if (Linha.Bloco == Bloco)
                    return Linha;
            return null;
        }

        /// <summary>
        /// add
        /// </summary>
        /// <param name="_ValorOriginal"></param>
        /// <param name="_ValorCorrigido"></param>
        /// <param name="_Bloco"></param>
        public void Add(decimal _ValorOriginal, decimal _ValorCorrigido, string _Bloco)
        {
            LinhaBloco Linha = LinhaPeloBloco(_Bloco);
            if (Linha == null)
                Add(new LinhaBloco(_Bloco, _ValorOriginal, _ValorCorrigido));
            else
            {
                Linha.ValorOriginal += _ValorOriginal;
                Linha.ValorCorrigido += _ValorCorrigido;
                Linha.Quantidade++;
            }
        }
    }

    /// <summary>
    /// Classe para o agrupamento das unidades em blocos (classe detalhe)
    /// </summary>
    public class LinhaBloco : Object
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorOriginal;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorCorrigido;
        /// <summary>
        /// 
        /// </summary>
        public string Bloco;
        /// <summary>
        /// 
        /// </summary>
        public int Quantidade;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Bloco"></param>
        /// <param name="_ValorOriginal"></param>
        /// <param name="_ValorCorrigido"></param>
        public LinhaBloco(string _Bloco, decimal _ValorOriginal, decimal _ValorCorrigido)
        {
            Bloco = _Bloco;
            ValorOriginal = _ValorOriginal;
            ValorCorrigido = _ValorCorrigido;
            Quantidade = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Bloco;
        }
    }

}
