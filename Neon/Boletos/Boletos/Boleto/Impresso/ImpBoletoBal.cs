using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.Data;
using System.Text;
using AccessImp.Utilitarios.MSAccess;

namespace Boletos.Boleto.Impresso
{
    

    public partial class ImpBoletoBal : DevExpress.XtraReports.UI.XtraReport
    {
        //public DataTable dt;
        //public DataTable dtDesp;
        public DataTable dtSaldos;
        public DataTable dtSaldosH;
        public DataTable dtDC;
        public DateTime d1;
        public DateTime d2;
        public Boolean Balancete;
        public String MensagemDoBoleto;
        public bool Reimpressao = false;
        //public DSBoletos.BoletoDataTable BoletoImp;
        public bool ImpressaoInternet = (Boleto.TipoPadrao == VirDB.Bancovirtual.TiposDeBanco.Internet2);

        /// <summary>
        /// Componente Boleto que gerou o Componente
        /// </summary>
        private Boletos.Boleto.Boleto BoletoMae;

        private void PrintingSystem_StartPrint(object sender, DevExpress.XtraPrinting.PrintDocumentEventArgs e)
        {
            e.PrintDocument.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Horizontal;           
        }

        /*public ImpBoletoBal()
        {
            InitializeComponent();
            this.PrintingSystem.StartPrint += new DevExpress.XtraPrinting.PrintDocumentEventHandler(PrintingSystem_StartPrint);            
        }*/

        private void Receitas(bool Ativar) {
            QuadroReceitas.Visible = Ativar;
            if(Ativar){
            
            }
        }

        
        public ImpBoletoBal(Boletos.Boleto.Boleto _BoletoMae) {
            InitializeComponent();
            if (!this.DesignMode)
            {
                this.PrintingSystem.StartPrint += new DevExpress.XtraPrinting.PrintDocumentEventHandler(PrintingSystem_StartPrint);
                BoletoMae = _BoletoMae;
                BOL = BoletoMae.rowPrincipal.BOL;
                eMPRESASTableAdapter.TrocarStringDeConexao(Boleto.TipoPadrao);
                eMPRESASTableAdapter.Fill(dImpBoletoBal.EMPRESAS, _BoletoMae.EMP);
                dataTable1TableAdapter.TrocarStringDeConexao(Boleto.TipoPadrao);
                if (BoletoMae.rowPrincipal.BOLProprietario)
                {
                    dataTable1TableAdapter.FilProp(dImpBoletoBal.DadosBoleto, BOL);
                    try
                    {
                        
                        dImpBoletoBal.DadosBoletoRow rowdBOL = (dImpBoletoBal.DadosBoletoRow)dImpBoletoBal.DadosBoleto.Rows[0];
                        if ((rowdBOL.IsIntUsuNull()) || (rowdBOL.IntUsu == ""))
                        {
                            string comando = "SELECT [usuario internet], [senha internet]\r\n" +
                                             "FROM Propriet�rio WHERE (Codcon = @P1) AND (Apartamento = @P2) AND (Bloco = @P3)";
                            string comando1 = "UPDATE APARTAMENTOS\r\n" +
                                              "SET APTUsuarioInternetProprietario = @P1, APTSenhaInternetProprietario = @P2\r\n" +
                                              "WHERE (APT = @P3)";
                            ArrayList DadosInternet = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLLinha(comando, BoletoMae.rowComplementar.CONCodigo, BoletoMae.rowComplementar.APTNumero, BoletoMae.rowComplementar.BLOCodigo);
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando1, DadosInternet[0], DadosInternet[1], BoletoMae.rowPrincipal.BOL_APT);
                            dataTable1TableAdapter.FilProp(dImpBoletoBal.DadosBoleto, BOL);
                        }
                    }
                    catch { }
                }
                else
                {
                    dataTable1TableAdapter.FilInq(dImpBoletoBal.DadosBoleto, BOL);
                    try
                    {
                        
                        dImpBoletoBal.DadosBoletoRow rowdBOL = (dImpBoletoBal.DadosBoletoRow)dImpBoletoBal.DadosBoleto.Rows[0];
                        if ((rowdBOL.IsIntUsuNull()) || (rowdBOL.IntUsu == ""))
                        {
                            string comando = "SELECT [usuario internet], [senha internet]\r\n" +
                                             "FROM Inquilino WHERE (Codcon = @P1) AND (Apartamento = @P2) AND (Bloco = @P3)";
                            string comando1 = "UPDATE APARTAMENTOS\r\n" +
                                              "SET APTUsuarioInternetInquilino = @P1, APTSenhaInternetInquilino = @P2\r\n" +
                                              "WHERE (APT = @P3)";
                            ArrayList DadosInternet = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLLinha(comando, BoletoMae.rowComplementar.CONCodigo, BoletoMae.rowComplementar.APTNumero, BoletoMae.rowComplementar.BLOCodigo);
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando1, DadosInternet[0], DadosInternet[1], BoletoMae.rowPrincipal.BOL_APT);
                            dataTable1TableAdapter.FilInq(dImpBoletoBal.DadosBoleto, BOL);
                        }
                    }
                    catch { }
                }
                bOletoDetalheTableAdapter.TrocarStringDeConexao(Boleto.TipoPadrao);
                bOletoDetalheTableAdapter.Fill(dImpBoletoBal.BOletoDetalhe, BOL);
                if (_BoletoMae.EMP == 3)
                    xrLabel132.Text = "4990-1394 FAX: 4437-3816";
                else
                    xrLabel132.Text = "3376-7900 FAX: 4121-1162";
            }
        }


        private void CantosRedondos(object sender, DrawEventArgs e,Int32 Raio)
        {
            /*
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            rect.Width++;            
            Rectangle rectD;
            Rectangle rectM2;
            Rectangle rectM;
            Rectangle rectE = rectD = rectM2 = rectM = rect;
            rectE.Width = rectD.Width = 2 * Raio;
            rectD.X = rect.Right - 2 * Raio;
            rectD.Height = rectE.Height = 2 * Raio;
            rectM.X = rect.X + Raio;
            rectM.Width = rect.Width - (2 * Raio);
            rectM.Height = Raio;
            rectM2.Y = rectM2.Y + Raio;
            rectM2.Height = rectM2.Height - Raio;
            XRControl control = e.Data.Control;
            Brush brush = new SolidBrush(e.Brick.BackColor);
            rect.Width++;
            if (e.UniGraphics is GdiGraphics)
            {
                
                GdiGraphics graph = (GdiGraphics)e.UniGraphics;                                
                graph.Graphics.FillRectangle(Brushes.White, rect);
                graph.Graphics.FillPie(brush, rectE, 180, 90);
                graph.Graphics.FillPie(brush, rectD, -90, 90);                
                graph.Graphics.FillRectangle(brush, rectM);
                graph.Graphics.FillRectangle(brush, rectM2);
                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    brush = new SolidBrush(control.ForeColor);
                    graph.DrawString(e.Brick.Text.ToString(), control.Font, brush, rect, e.Data.StringFormat);
                };

            }
            else { 
            
                
                DevExpress.XtraPrinting.Export.Pdf.PdfGraphics graph = (DevExpress.XtraPrinting.Export.Pdf.PdfGraphics)e.UniGraphics;                
                graph.FillRectangle(Brushes.White, rect);
                graph.FillEllipse(brush, rectE.X, rectE.Y, 2 * Raio, 2 * Raio);
                graph.FillEllipse(brush, rectD.X, rectD.Y, 2 * Raio, 2 * Raio);                 
                graph.FillRectangle(brush, rectM);
                graph.FillRectangle(brush, rectM2);
                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    brush = new SolidBrush(control.ForeColor);
                    graph.DrawString(e.Brick.Text.ToString(), control.Font, brush, rect, e.Data.StringFormat);
                };
            }
            */
        }
        private void Degrade(object sender, DrawEventArgs e)
        {
            /*
            Color C1 = Color.FromArgb(200, 200, 200);
            Color C2 = Color.FromArgb(255, 255, 255);
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            Brush brush;
            LinearGradientBrush Grad = new LinearGradientBrush(rect, C1, C2, 0.0);
            XRControl control = e.Data.Control;
            if (e.UniGraphics is GdiGraphics)
            {
                
                GdiGraphics graph = (GdiGraphics)e.UniGraphics;                
                graph.Graphics.FillRectangle(Grad, rect);               
                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    brush = new SolidBrush(control.ForeColor);
                    graph.DrawString(e.Data.Text.ToString(), control.Font, brush, rect, e.Data.StringFormat);
                }
            }
            else
            {
                DevExpress.XtraPrinting.Export.Pdf.PdfGraphics graph = (DevExpress.XtraPrinting.Export.Pdf.PdfGraphics)e.UniGraphics;
                brush = new SolidBrush(control.BackColor);
                //Grad.InterpolationColors = Grad.InterpolationColors;
                
                graph. FillRectangle(brush, rect);                
                
                if ((e.Data != null) && (e.Data.Text != null))
                {
                    brush = new SolidBrush(control.ForeColor);
                    graph.DrawString(e.Data.Text.ToString(), control.Font, brush, rect, e.Data.StringFormat);
                }
            }
            */ 
        }

        private char[] separadorQuebra = new char[2] {'\r', '\n' };

        private decimal maxBarr;
        private decimal maxBarr2;

        private void DrawBarras(DrawEventArgs e, decimal Maximo) {
            if (Maximo == 0)
                Maximo = 100;
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            if (e.UniGraphics is GdiGraphics)
            {

                GdiGraphics graph = (GdiGraphics)e.UniGraphics;
                graph.Graphics.FillRectangle(Brushes.White, rect);
                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    string[] linhas = e.Brick.Text.Split(separadorQuebra, StringSplitOptions.RemoveEmptyEntries);
                    int larg = (int)(rect.Height / linhas.Length);
                    for (int i = 0; i < linhas.Length; i++)
                    {
                        decimal Valor;
                        decimal.TryParse(linhas[i],out Valor);
                        int w = (i + 1) * (int)(Valor * rect.Width / Maximo);
                        Rectangle recti = new Rectangle(rect.X, rect.Y + i * larg + 4, w, larg - 13);
                        graph.Graphics.FillRectangle(Brushes.Silver, recti);
                    }
                }
            }
            else
            {
                DevExpress.XtraPrinting.Export.Pdf.PdfGraphics graph = (DevExpress.XtraPrinting.Export.Pdf.PdfGraphics)e.UniGraphics;
                graph.FillRectangle(Brushes.White, rect);

                if ((e.Brick != null) && (e.Brick.Text != null))
                {

                }
            }
        }

        private void DrawBarras(object sender, DrawEventArgs e)
        {
            DrawBarras(e, maxBarr);
            return;
            if (maxBarr == 0)
                maxBarr = 100;
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            if (e.UniGraphics is GdiGraphics)
            {

                GdiGraphics graph = (GdiGraphics)e.UniGraphics;
                graph.Graphics.FillRectangle(Brushes.Yellow, rect);                
                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    string[] linhas = e.Brick.Text.Split(separadorQuebra,StringSplitOptions.RemoveEmptyEntries);
                    int larg = (int)(rect.Height / linhas.Length);
                    for(int i = 0;i<linhas.Length;i++){
                        decimal Valor = decimal.Parse(linhas[i]);
                        int w = (i + 1) * (int)(Valor * rect.Width / maxBarr);
                        Rectangle recti = new Rectangle(rect.X, rect.Y + i * larg + 4, w, larg-13);
                        graph.Graphics.FillRectangle(Brushes.Silver, recti);
                    }
                }
            }
            else
            {
                DevExpress.XtraPrinting.Export.Pdf.PdfGraphics graph = (DevExpress.XtraPrinting.Export.Pdf.PdfGraphics)e.UniGraphics;                
                graph.FillRectangle(Brushes.White, rect);

                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    
                }
            }
        }

        private void xrLabel7_Draw(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender,e,30);
        }
        private void xrLabel8_Draw(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);            
        }
        private void xrLabel13_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender,e);
        }
        private void xrLabel12_Draw(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);
        }
        
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            /*
            QuadroDesp.Text = "DESPESAS " + d1.ToString("dd/MM/yy") + " � " + d2.ToString("dd/MM/yy");
            xrLabel12.Text = "RECEITAS " + d1.ToString("dd/MM/yy") + " � " + d2.ToString("dd/MM/yy");
            xrLabel76.Text = "Creditos n�o localizados em " + DateTime.Today.ToString("dd/MM/yy");
            //int linha = this.CurrentRowIndex;
            //DataRow DR = (DataRow)dsBoletos1.Tables["Boleto"].Rows[linha];
            DataRow DR = ((DataRowView)bindingSource1.Current).Row;
            NamePago.Visible = (bool)DR["PAGO"];
            CodigoDeBarras.Visible = !(bool)DR["PAGO"];
            if (DR["BLOCO"].ToString() == "SB")
            {
                LabBlo1.Visible = false;
                LabBlo2.Visible = false;
                LabBlo3.Visible = false;
                LabBlo4.Visible = false;                
                LabApt3.Left = LabBlo3.Left;
                LabApt4.Left = LabBlo4.Left;
            }
            else
            {
                LabBlo1.Visible = true;
                LabBlo2.Visible = true;
                LabBlo3.Visible = true;
                LabBlo4.Visible = true;
                LabApt3.Left = LabBlo3.Left+188;
                LabApt4.Left = LabBlo4.Left+188;
            };

            //
            //COMPOSICAO DO BOLETO
            //

            //Boleto.DSBoletosTableAdapters.Boleto___PagamentosTableAdapter Adapterit = new Boleto.DSBoletosTableAdapters.Boleto___PagamentosTableAdapter();
            //DataTable dtit = Adapterit.GetData(Convert.ToDecimal((double)DR["n�mero boleto"]));                        
            Lista11.Text = "";
            Lista12.Text = "";
            //Lista21.Text = DR["n�mero boleto"].ToString();
            double Total=0;
            foreach (DataRow DRit in dtit.Rows)
            {
                if (DRit["mensagem"] == DBNull.Value)
                {                    
                    Lista11.Text += "\r\n" + DRit["descbase"];
                }
                else
                {                    
                    Lista11.Text += "\r\n" + DRit["descbase"] + " " + DRit["mensagem"];
                };                
                Lista12.Text += "\r\n" + ((float)DRit["valor"]).ToString("#,##0.00");
                Total += (float)DRit["valor"];
            };
            Lista12.Text += "\r\n\r\n" + Total.ToString("#,##0.00");
            
            */
        }

        

        private void RegistralinhaDesp(XRLabel Quadro,XRLabel Quadroval, string nome, double valor)
        {
            if (nome != "")
            {
                if (Quadro.Text != "")
                {
                    Quadro.Text += "\r\n";
                    Quadroval.Text += "\r\n" ;
                    //deltaposicao += aumentoporlinha;
                };
                Quadro.Text += "    " + nome;
                Quadroval.Text += valor.ToString("#,##0.00");
            }
        }

        private void Registralinha(string nome, double[] colunas)
        {
            if (nome != "") {
                ReceitaNome.Text += "\r\n  " + nome ;
                ReceitaA.Text += "\r\n" + ((colunas[0] == 0) ? " " : colunas[0].ToString("#,##0.00"));
                ReceitaMes.Text += "\r\n" + ((colunas[1] == 0) ? " " : colunas[1].ToString("#,##0.00"));
                RecetaP.Text += "\r\n" + ((colunas[2] == 0) ? " " : colunas[2].ToString("#,##0.00"));
                ReceitaT.Text += "\r\n" + ((colunas[3] == 0) ? " " : colunas[3].ToString("#,##0.00"));               
            }
        }

        double RentabilidadeTotal = 0;


        //private decimal aumentoporlinha = 26.277M;
        //private decimal reducaoporquadro = 68.0M;
        //private decimal deltaposicao = 0;

        private void OcultaQuadrosDesp() {
            /*
            if((BoletoMae.Precalculado != null) && (BoletoMae.Precalculado.ContainsKey("EstouroDeQuadro"))){
                QuadroDespesas.Visible = false;
                QuadroContinuacao.Visible = false;
                return;
            };
            PDespG.Visible = (DespesaNome.Text != "");
            if (!PDespG.Visible)
            {
                QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                PDespH.Top = PDespB.Top;
                PDespB.Top = PAdministradora.Top;
                PAdministradora.Top = PDespADMA.Top;
                PDespADMA.Top = PdespMO.Top;
                PdespMO.Top = PDespG.Top;
            };

            PdespMO.Visible = (DespesaNome1.Text != "");
            if (!PdespMO.Visible)
            {
                QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                PDespH.Top = PDespB.Top;
                PDespB.Top = PAdministradora.Top;
                PAdministradora.Top = PDespADMA.Top;
                PDespADMA.Top = PdespMO.Top;
            };

            PDespADMA.Visible = (DespesaNome.Text != "");
            if (!PDespADMA.Visible)
            {
                QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                PDespH.Top = PDespB.Top;
                PDespB.Top = PAdministradora.Top;
                PAdministradora.Top = PDespADMA.Top;
            };

            PAdministradora.Visible = (DespesaNomeADM.Text != "");
            if (!PAdministradora.Visible)
            {
                QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                PDespH.Top = PDespB.Top;
                PDespB.Top = PAdministradora.Top;
            };

            PDespB.Visible = (DespesaNomeBANC.Text != "");
            if (!PDespB.Visible)
            {
                QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                PDespH.Top = PDespB.Top;
            };

            PDespH.Visible = (DespesaNomeADV.Text != "");
            if (!PDespH.Visible)
            {
                QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
            };*/
        }

        private void Despesas() {
            
            QuadroDesp.Text = "DESPESAS " + d1.ToString("dd/MM/yy") + " � " + d2.ToString("dd/MM/yy");
            double TotalLinha = 0;
            double[] SubTotais = new double[6];
            int indsub;
            for (indsub = 0; indsub < SubTotais.GetLength(0);indsub++ )
                SubTotais[indsub]=0;
            
            double TotalDesp = 0;
            string nome = "";


            DespesaNome.Text = DespesaValor.Text = DespesaNome1.Text = DespesaValor1.Text = "";
            DespesaNome2.Text = DespesaValor2.Text = DespesaNomeADM.Text = DespesaValorADM.Text = "";
            DespesaNomeADV.Text = DespesaValorADV.Text = DespesaNomeBANC.Text = DespesaValorBANC.Text = "";


            ////////////////////
            //////GERAIS////////
            ////////////////////
            int maioritem = indsub = 0;
            double maiorvalor;

            Impresso.dImpBoletoBalTableAdapters.HDespesasAccessTableAdapter HDespesasAccessTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.HDespesasAccessTableAdapter();
            HDespesasAccessTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
            HDespesasAccessTableAdapter.Fill(dImpBoletoBal.HDespesasAccess, linhamae.CONCodigo, d1, d2);


            foreach (Impresso.dImpBoletoBal.HDespesasAccessRow DR in dImpBoletoBal.HDespesasAccess.Rows)
                if ((Int32.Parse((string)DR["TipoPag"]) < 219999) 
                    || 
                    (Int32.Parse((string)DR["TipoPag"]) >= 250000)
                    ||
                    (Int32.Parse((string)DR["TipoPag"]) == 230009)
                    )
                {
                    if (nome != DR["descricao"].ToString())
                    {
                        RegistralinhaDesp(DespesaNome, DespesaValor, nome, TotalLinha);
                        nome = DR["descricao"].ToString();
                        TotalLinha = 0;
                    };
                    TotalLinha += (double)DR["valorpago"];
                    SubTotais[indsub] += (double)DR["valorpago"];
                    TotalDesp += (double)DR["valorpago"];
                };


            maiorvalor = indsub = 0; 
            RegistralinhaDesp(DespesaNome, DespesaValor, nome, TotalLinha);
            DespesaValor.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";     
            
            /////////////////////////
            //////M�O DE OBRA////////
            /////////////////////////
            nome = "";
            DespesaNome1.Text += "";
            DespesaValor1.Text += "";
            TotalLinha = 0;
            indsub++;




            foreach (Impresso.dImpBoletoBal.HDespesasAccessRow DR in dImpBoletoBal.HDespesasAccess.Rows)
                if ((Int32.Parse((string)DR["TipoPag"]) >= 220000) && (Int32.Parse((string)DR["TipoPag"]) < 230000))
                {
                    if (nome != DR["descricao"].ToString())
                    {
                        RegistralinhaDesp(DespesaNome1, DespesaValor1, nome, TotalLinha);
                        nome = DR["descricao"].ToString();
                        TotalLinha = 0;
                    };
                    TotalLinha += (double)DR["valorpago"];
                    SubTotais[indsub] += (double)DR["valorpago"];
                    TotalDesp += (double)DR["valorpago"];
                };
            if (SubTotais[indsub] > maiorvalor) {
                maiorvalor = SubTotais[indsub];
                maioritem = indsub;
            };
            
            RegistralinhaDesp(DespesaNome1, DespesaValor1, nome, TotalLinha);
            DespesaValor1.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";
            

            /////////////////////////////
            //////ADMINISTRATIVAS////////
            /////////////////////////////
            nome = "";
            DespesaNome2.Text += "";
            DespesaValor2.Text += "";
            TotalLinha = 0;
            indsub++;

            foreach (Impresso.dImpBoletoBal.HDespesasAccessRow DR in dImpBoletoBal.HDespesasAccess.Rows){
                Int32 Conta = Int32.Parse((string)DR["TipoPag"]);
                if ((Conta > 230002) 
                    && (Conta <= 250000)
                  //  && (Conta != 240001)
                  //  && (Conta != 240004)
                    && (Conta != 240005)
                    && (Conta != 240006)
                    && (Conta != 230009)
                  //  && (Conta != 240007)
                    )
                {
                    if (nome != DR["descricao"].ToString())
                    {                        
                        RegistralinhaDesp(DespesaNome2, DespesaValor2, nome, TotalLinha);
                        nome = DR["descricao"].ToString();
                        TotalLinha = 0;
                    };
                    TotalLinha += (double)DR["valorpago"];
                    SubTotais[indsub] += (double)DR["valorpago"];
                    TotalDesp += (double)DR["valorpago"];
                };
            };
            if (SubTotais[indsub] > maiorvalor)
            {
                maiorvalor = SubTotais[indsub];
                maioritem = indsub;
            };
            
            RegistralinhaDesp(DespesaNome2, DespesaValor2, nome, TotalLinha);
            DespesaValor2.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";

            xrTotalDesp.Text = TotalDesp.ToString("#,##0.00");
            

            /////////////////////////////
            //////ADMINISTRADORA/////////
            /////////////////////////////
            nome = "";
            DespesaNomeADM.Text += "";
            DespesaValorADM.Text += "";
            TotalLinha = 0;
            indsub++;

            foreach (Impresso.dImpBoletoBal.HDespesasAccessRow DR in dImpBoletoBal.HDespesasAccess.Rows)
                if (
                    ((Int32.Parse((string)DR["TipoPag"]) > 230000) && (Int32.Parse((string)DR["TipoPag"]) < 230003))
            //        ||
              //      (Int32.Parse((string)DR["TipoPag"]) == 240001)
                //    ||
                  //  (Int32.Parse((string)DR["TipoPag"]) == 240004)
                 //   ||
                   // (Int32.Parse((string)DR["TipoPag"]) == 240007)
                    )
                {
                    if (nome != DR["descricao"].ToString())
                    {
                        RegistralinhaDesp(DespesaNomeADM, DespesaValorADM, nome, TotalLinha);
                        nome = DR["descricao"].ToString();
                        TotalLinha = 0;
                    };
                    TotalLinha += (double)DR["valorpago"];
                    SubTotais[indsub] += (double)DR["valorpago"];
                    TotalDesp += (double)DR["valorpago"];
                };
            if (SubTotais[indsub] > maiorvalor)
            {
                maiorvalor = SubTotais[indsub];
                maioritem = indsub;
            };
            RegistralinhaDesp(DespesaNomeADM, DespesaValorADM, nome, TotalLinha);
            DespesaValorADM.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";

            xrTotalDesp.Text = TotalDesp.ToString("#,##0.00");
            
 
           

            /////////////////////////////
            ////////////Bacarias/////////
            /////////////////////////////
            nome = "";
            DespesaNomeBANC.Text += "";
            DespesaValorBANC.Text += "";
            TotalLinha = 0;
            indsub++;

            foreach (Impresso.dImpBoletoBal.HDespesasAccessRow DR in dImpBoletoBal.HDespesasAccess.Rows)
                if (                    
                    (Int32.Parse((string)DR["TipoPag"]) == 240005)
                    ||
                    (Int32.Parse((string)DR["TipoPag"]) == 240006)                                        
                    )
                {
                    if (nome != DR["descricao"].ToString())
                    {
                        RegistralinhaDesp(DespesaNomeBANC, DespesaValorBANC, nome, TotalLinha);
                        nome = DR["descricao"].ToString();
                        TotalLinha = 0;
                    };
                    TotalLinha += (double)DR["valorpago"];
                    SubTotais[indsub] += (double)DR["valorpago"];
                    TotalDesp += (double)DR["valorpago"];
                };
            if (SubTotais[indsub] > maiorvalor)
            {
                maiorvalor = SubTotais[indsub];
                maioritem = indsub;
            };
            
            RegistralinhaDesp(DespesaNomeBANC, DespesaValorBANC, nome, TotalLinha);
            DespesaValorBANC.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";

            xrTotalDesp.Text = TotalDesp.ToString("#,##0.00");
            

            /////////////////////////////
            ////////ADVOCATICEAS/////////
            /////////////////////////////
            nome = "";
            DespesaNomeADV.Text += "";
            DespesaValorADV.Text += "";
            TotalLinha = 0;
            indsub++;
             
            foreach (Impresso.dImpBoletoBal.HDespesasAccessRow DR in dImpBoletoBal.HDespesasAccess.Rows)
                if (Int32.Parse((string)DR["TipoPag"]) == 230000) 
                {                                                                                                        
                    TotalLinha += (double)DR["valorpago"];
                    SubTotais[indsub] += (double)DR["valorpago"];
                    TotalDesp += (double)DR["valorpago"];
                };
            if (SubTotais[indsub] > maiorvalor)
            {
                maiorvalor = SubTotais[indsub];
                maioritem = indsub;
            };

           


            if (SubTotais[indsub] != 0)
            {
                RegistralinhaDesp(DespesaNomeADV, DespesaValorADV, "Honor�rios", TotalLinha);
                DespesaValorADV.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";
            };

            
            
            

            xrTotalDesp.Text = TotalDesp.ToString("#,##0.00");

            XRLabel[] pors = new XRLabel[] {DGpor,MOpor,ADMpor,ADMApor,Bancpor,HORpor};
            XRLine[] Lines = new XRLine[] { DGLine, MOLine, ADMLine, ADMALine, BancLine, HORLine };
            XRLine[] Lines1 = new XRLine[] { DGLine1, MOLine1, ADMLine1, ADMALine1, BancLine1, HORLine1 };

            int arr=0;
            for (indsub = 0; indsub < SubTotais.GetLength(0); indsub++)
            {                
                    if (indsub != maioritem)
                    {
                        if (TotalDesp != 0)
                            arr += Convert.ToInt32((1000.0 * SubTotais[indsub] / TotalDesp));                        
                    }
                    
            };

            for (indsub = 0; indsub < SubTotais.GetLength(0); indsub++) {
                if (SubTotais[indsub] != 0)
                {
                    if (indsub != maioritem)
                        pors[indsub].Text = ((100.0 * SubTotais[indsub] / TotalDesp)).ToString("0.0") + "%";
                    else
                        pors[indsub].Text = ((1000.0 - arr) / 10).ToString("0.0") + "%";


                    Lines[indsub].Width = (Int16)(370 * SubTotais[indsub] / TotalDesp);

                    Lines1[indsub].Left = Lines[indsub].Width;
                    Lines1[indsub].Width = 370 - Lines1[indsub].Left;
                };
            };

            /*
            if (SubTotais[0] != 0)
            {
                DGpor.Text = ((100.0 * SubTotalDG / TotalDesp)).ToString("0.0") + "%";
                DGLine.Width = (Int16)(370 * SubTotalDG / TotalDesp);
            };
            if (SubTotais[1] != 0)
            {
                MOpor.Text = ((100.0 * SubTotalMO / TotalDesp)).ToString("0.0") + "%";
                MOLine.Width = (Int16)(370 * SubTotalMO / TotalDesp);
            };
            if (SubTotais[2] != 0)
            {
                ADMpor.Text = ((100.0 * SubTotalADM / TotalDesp)).ToString("0.0") + "%";
                ADMLine.Width = (Int16)(370 * SubTotalADM / TotalDesp);
            };
            if (SubTotais[3] != 0)
            {
                ADMApor.Text = ((100.0 * SubTotalADMA / TotalDesp)).ToString("0.0") + "%";
                ADMALine.Width = (Int16)(370 * SubTotalADMA / TotalDesp);
            };
            if (SubTotalBANC != 0)
            {
                Bancpor.Text = ((100.0 * SubTotalBANC / TotalDesp)).ToString("0.0") + "%";
                BancLine.Width = (Int16)(370 * SubTotalBANC / TotalDesp);
            };
            if (SubTotalHOR != 0)
            {
                HORpor.Text = ((100.0 * SubTotalHOR / TotalDesp)).ToString("0.0") + "%";
                HORLine.Width = (Int16)(370 * SubTotalHOR / TotalDesp);
            };*/
        }

        public int BOL=0;
        private dImpBoletoBal.DadosBoletoRow linhamae;

        /*
        private void DadosBanco(int Banco) {
            switch (Banco)
            {
                case 237:
                    xrLabelEspecie.Text = "";
                    xrLabelCarteira.Text = "6";
                    xrLabelUsoBanco.Text = "";
                    BANCONome.Text = "BRADESCO S/A";
                    BANCONumero.Text = "237-2";
                    BANCOLocal.Text = "At� o vencimento, em qualquer ag. da rede banc�ria, ap�s o vencimento, somente no BRADESCO";
                    xrAgencia.Text = linhamae.Calc_AgCedente;
                    break;
                case 341:
                    xrLabelEspecie.Text = "";
                    xrLabelCarteira.Text = "6";
                    xrLabelUsoBanco.Text = "";
                    BANCONome.Text = "Banco Ita� S.A.";
                    BANCONumero.Text = "341-7";
                    BANCOLocal.Text = "At� o vencimento,preferencialmente no Ita� ou Banerj e ap�s o vencimento, somente no Ita� ou Banerj";
                    xrAgencia.Text = linhamae.Calc_AgCedente;
                    break;
                case 409:
                    xrLabelEspecie.Text = "REC";
                    xrLabelCarteira.Text = "ESPECIAL";
                    xrLabelUsoBanco.Text = "CVT 7744-5";
                    BANCONome.Text = "UNIBANCO";
                    BANCONumero.Text = "409-0";
                    BANCOLocal.Text = "At� o vencimento, pag�vel em qualquer banco. Ap�s o vencimento, em qualquer ag�ncia do Unibanco mediante consulta no sistema VC";
                    xrAgencia.Text = linhamae.Calc_AgCedente;
                    break;
                case 399:
                    xrLabelEspecie.Text = "";
                    xrEspecie.Text = "09 - Real";
                    xrLabelCarteira.Text = "CNR";// "CNR";
                    xrLabelUsoBanco.Text = "";
                    BANCONome.Text = "HSBC";
                    BANCONumero.Text = "399-9";
                    BANCOLocal.Text = "At� o vencimento,preferencialmente no HSBC. Ap�s o vencimento, somente no HSBC";
                    xrAgencia.Text = "3693872";
                    break;
            }
        }
        */

        public static RetornoInad CalculaUnidadesInadimplenes(int CON, Framework.objetosNeon.Competencia Competencia, DateTime DataBalancete, int MaxCarar)
        {            
            int RefData; //0=Emissao 1=Balancete 2=Prompt
            int Periodo; //Dias
            int Agrupamento; //1=Lista 2=Bloco 3=Unidades 
            bool ValorOriginal;
            bool ValorCorrigido;
            bool NumeroDeUnidades;
            string CONCodigo;
            int DestacarUnidadesInadDoMes;//0=Nao Destacar 1=Valor 2=Numero de unidades a mais de 30 dias e do mes
            string Comando = "select CONInaDataRef, CONInaPeriodo, CONInaAgrupamento, CONInaValorOrig, CONInaValorCor, CONInaNumero, CONInaDestacaMes, CONCodigo from CONdominios where CON = @P1";
            ArrayList Dados = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLLinha(Comando, CON);

            if (Dados.Count != 8)
                return null;
            else {
                if (Dados[0] == DBNull.Value)
                    RefData = 0;
                else
                    RefData = (int)Dados[0];
                if (Dados[1] == DBNull.Value)
                    Periodo = 0;
                else
                    Periodo = (int)Dados[1];
                if (Dados[2] == DBNull.Value)
                    Agrupamento = 0;
                else
                    Agrupamento = (int)Dados[2];
                if (Dados[3] == DBNull.Value)
                    ValorOriginal = false;
                else
                    ValorOriginal = (bool)Dados[3];
                if (Dados[4] == DBNull.Value)
                    ValorCorrigido = false;
                else
                    ValorCorrigido = (bool)Dados[4];
                if (Dados[5] == DBNull.Value)
                    NumeroDeUnidades = false;
                else
                    NumeroDeUnidades = (bool)Dados[5];
                if (Dados[6] == DBNull.Value)
                    DestacarUnidadesInadDoMes = 0;
                else
                    DestacarUnidadesInadDoMes = (int)Dados[6];
                CONCodigo = Dados[7].ToString();
            };
           
            //if (TextoUnidadesInadimplentes.Font.Size == 7)
                //MaxCarar = (int)Math.Truncate(TextoUnidadesInadimplentes.Width / 14.6);
            //else
                //MaxCarar = (int)Math.Truncate(TextoUnidadesInadimplentes.Width / 13.0);
           
            return CalculaUnidadesInadimplenes(RefData, Periodo, Agrupamento, ValorOriginal, ValorCorrigido, NumeroDeUnidades, DestacarUnidadesInadDoMes, MaxCarar, DataBalancete,new dImpBoletoBal(), CONCodigo, Competencia);
        }

        /// <summary>
        /// Calula o conte�do da quadro unidades inadimplentes conforme o modelo no cadastro do condom�nio ESTATICO
        /// </summary>
        public static RetornoInad CalculaUnidadesInadimplenes(int RefData, int Periodo, int Agrupamento, bool ValorOriginal, bool ValorCorrigido, bool NumeroDeUnidades, int DestacarUnidadesInadDoMes, int MaxCarar, DateTime DataBalancete, dImpBoletoBal DataSet, string CODCON, Framework.objetosNeon.Competencia CompetAnterior)
        {
            RetornoInad Retorno = new RetornoInad();
            
            if (Agrupamento == 0)
                return null;

            DateTime Database = DateTime.Today;
                        
            switch (RefData)
            {
                case 0:
                    Database = DateTime.Today.AddDays(-Periodo);
                    Retorno.Titulo = "UNIDADES INADIMPLENTES a mais de " + Periodo.ToString() + " dias (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
                case 1:
                    Database = DataBalancete.AddDays(-Periodo);
                    Retorno.Titulo = "UNIDADES INADIMPLENTES - Data base: " + Database.ToString("dd/MM/yyyy") + " (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
                case 2:
                    DateTime novadata = DateTime.Today;
                    if (!VirInput.Input.Execute("Data de Refer�ncia", ref novadata))
                        return null;

                    Database = novadata.AddDays(-Periodo);
                    Retorno.Titulo = "UNIDADES INADIMPLENTES - Data base: " + Database.ToString("dd/MM/yyyy") + " (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
            };

            int NumeroDeBoeltos = DataSet.InadimplentesTableAdapter.Fill(DataSet.Inadimplentes, Database, CODCON);

            //Boleto[] Boletos = new Boleto[NumeroDeBoeltos];

            string TextoUnidadesI = "";

            ConjuntoLinhasInad Conj = new ConjuntoLinhasInad();
            //Framework.objetosNeon.Competencia CompetAnterior = BoletoMae.Competencia.CloneCompet();
            CompetAnterior.Add(-1);

            //int i = 0;
            foreach (dImpBoletoBal.InadimplentesRow nBol in DataSet.Inadimplentes)
            {
                Boleto Bol = new Boleto((int)nBol.N�mero_Boleto);
                if (!Bol.Encontrado)
                {
                    throw new Exception(string.Format("Boleto n�o encontrado: {0}",nBol.N�mero_Boleto));
                    //fImportFromMsAccess.ImportaBoleto(((int)nBol.N�mero_Boleto).ToString());
                    //Bol = new Boleto((int)nBol.N�mero_Boleto);
                };
                if (ValorCorrigido)
                    Bol.Valorpara(DateTime.Today);
                string Titulo = (Bol.rowComplementar.BLOCodigo.ToUpper() == "SB") ? "" : Bol.rowComplementar.BLOCodigo + "-";
                Titulo += Bol.rowComplementar.APTNumero;

                Conj.Add(Titulo, Bol.rowPrincipal.BOLValorPrevisto, Bol.valorcorrigido, Bol.rowComplementar.BLOCodigo, (Bol.Competencia == CompetAnterior));
            };




            int CompUltimalinha = 0;

            switch (Agrupamento)
            {
                case 1://Lista

                    foreach (LinhaQuadroInadimplente Linha in Conj)
                    {
                        string novogrupo = Linha.Texto;
                        if (NumeroDeUnidades)
                            novogrupo += " Boletos = " + Linha.Quantidade.ToString("00");
                        if (ValorOriginal)
                            novogrupo += " Valor = R$" + Linha.ValorOriginal.ToString("n2").PadLeft(9);
                        if (ValorCorrigido)
                            novogrupo += " Corrigido = R$" + Linha.ValorCorrigido.ToString("n2").PadLeft(9);
                        novogrupo += "; ";

                        if ((CompUltimalinha + novogrupo.Length) > MaxCarar)
                        {
                            TextoUnidadesI += "\r\n";
                            CompUltimalinha = novogrupo.Length;
                        }
                        else
                            CompUltimalinha += novogrupo.Length;
                        TextoUnidadesI += novogrupo;

                    };
                    break;
                case 2://Blocos
                    ConjuntoLinhaBloco ConjB = new ConjuntoLinhaBloco();

                    foreach (LinhaQuadroInadimplente Linha in Conj)
                        ConjB.Add(Linha.ValorOriginal, Linha.ValorCorrigido, Linha.Bloco);
                    TextoUnidadesI = "";
                    foreach (LinhaBloco LB in ConjB)
                    {
                        TextoUnidadesI += "Bloco " + LB.Bloco + ": ";
                        if (NumeroDeUnidades)
                            TextoUnidadesI += "Unidades: " + LB.Quantidade.ToString("00") + " ";
                        if (ValorOriginal)
                            TextoUnidadesI += "Val.: R$ " + LB.ValorOriginal.ToString("n2").PadLeft(9) + " ";
                        if (ValorCorrigido)
                            TextoUnidadesI += "com encargos: R$ " + LB.ValorCorrigido.ToString("n2").PadLeft(9);
                        TextoUnidadesI += "\r\n";
                    };


                    break;
                case 3://Numero
                    decimal TotalO = 0;
                    decimal TotalC = 0;
                    if (ValorOriginal || ValorCorrigido)
                        foreach (LinhaQuadroInadimplente Linha in Conj)
                        {
                            TotalO += Linha.ValorOriginal;
                            TotalC += Linha.ValorCorrigido;
                        };
                    TextoUnidadesI = "";
                    if (NumeroDeUnidades)
                        TextoUnidadesI += "Unidades: " + Conj.Count.ToString("00") + "\r\n";
                    if (ValorOriginal)
                        TextoUnidadesI += "Valor original: R$ " + TotalO.ToString("n2") + "\r\n";
                    if (ValorCorrigido)
                        TextoUnidadesI += "Valor original + encargos: R$ " + TotalC.ToString("n2");
                    break;
                case 4:
                    foreach (dImpBoletoBal.InadimplentesRow nBol in DataSet.Inadimplentes)
                        TextoUnidadesI += nBol.N�mero_Boleto.ToString() + " ";
                    break;
            }

            switch (DestacarUnidadesInadDoMes)
            {
                case 1:
                    decimal ValorMes = 0;
                    foreach (LinhaQuadroInadimplente Linha in Conj)
                        ValorMes += Linha.ValorDoMes;
                    if (ValorMes > 0)
                        TextoUnidadesI += "\r\nValor inadimpl�ncia do m�s: R$ " + ValorMes.ToString("n2");
                    break;
                case 2:
                    int InadMes = 0;
                    foreach (LinhaQuadroInadimplente Linha in Conj)
                        if (Linha.ValorDoMes > 0)
                            InadMes++;
                    TextoUnidadesI += "\r\n" + (Conj.Count - InadMes).ToString("00") + " unidades inadimpl�ntes a mais de 30 dias\r\n";
                    TextoUnidadesI += InadMes.ToString("00") + " unidades inadimpl�tes do m�s";
                    break;
            }


            Retorno.Conteudo = TextoUnidadesI;

            return Retorno; 
        }
        



        /// <summary>
        /// Calula o conte�do da quadro unidades inadimplentes conforme o modelo no cadastro do condom�nio
        /// </summary>
        private void CalculaUnidadesInadimplenes() {
            
            TextoUnidadesInadimplentes.Text = "";
            if (ImpressaoInternet || Reimpressao)
            {                
                return;
            };
            if (linhamae.BOLTipoCRAI != "C")
            {
                return;
            }
             
            //int Modelo;
            int RefData; //0=Emissao 1=Balancete 2=Prompt
            int Periodo; //Dias
            int Agrupamento; //1=Lista 2=Bloco 3=Unidades 
            bool ValorOriginal;
            bool ValorCorrigido;
            bool NumeroDeUnidades;
            int DestacarUnidadesInadDoMes;//0=Nao Destacar 1=Valor 2=Numero de unidades a mais de 30 dias e do mes
            string Comando = "select CONInaDataRef, CONInaPeriodo, CONInaAgrupamento, CONInaValorOrig, CONInaValorCor, CONInaNumero, CONInaDestacaMes from CONdominios where CON = @P1";
            ArrayList Dados = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLLinha(Comando, BoletoMae.rowComplementar.CON);

            if (Dados.Count != 7)
                return;
            else {
                if (Dados[0] == DBNull.Value)
                    RefData = 0;
                else
                    RefData = (int)Dados[0];
                if (Dados[1] == DBNull.Value)
                    Periodo = 0;
                else
                    Periodo = (int)Dados[1];
                if (Dados[2] == DBNull.Value)
                    Agrupamento = 0;
                else
                    Agrupamento = (int)Dados[2];
                if (Dados[3] == DBNull.Value)
                    ValorOriginal = false;
                else
                    ValorOriginal = (bool)Dados[3];
                if (Dados[4] == DBNull.Value)
                    ValorCorrigido = false;
                else
                    ValorCorrigido = (bool)Dados[4];
                if (Dados[5] == DBNull.Value)
                    NumeroDeUnidades = false;
                else
                    NumeroDeUnidades = (bool)Dados[5];
                if (Dados[6] == DBNull.Value)
                    DestacarUnidadesInadDoMes = 0;
                else
                    DestacarUnidadesInadDoMes = (int)Dados[6];
            };

            if (Agrupamento == 0)
                return;

           

            DateTime Database = DateTime.Today;
            
            int MaxCarar;
            if (TextoUnidadesInadimplentes.Font.Size == 7)
                MaxCarar = (int)Math.Truncate(TextoUnidadesInadimplentes.Width / 14.6);
            else
                MaxCarar = (int)Math.Truncate(TextoUnidadesInadimplentes.Width / 13.0);
            
            switch (RefData)
            {
                case 0:
                    Database = BoletoMae.rowPrincipal.BOLEmissao.AddDays(-Periodo);
                    TituloUnidadesInad.Text += " a mais de "+Periodo.ToString()+" dias (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
                case 1:
                    Database = d2.AddDays(-Periodo);                    
                    TituloUnidadesInad.Text += " - Data base: " + Database.ToString("dd/MM/yyyy") + " (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
                case 2:
                    DateTime novadata=DateTime.Today;
                    if (!VirInput.Input.Execute("Data de Refer�ncia", ref novadata))
                        return;

                    Database = novadata.AddDays(-Periodo);                   
                    TituloUnidadesInad.Text += " - Data base: " + Database.ToString("dd/MM/yyyy") + " (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
            };

            int NumeroDeBoeltos = dImpBoletoBal.InadimplentesTableAdapter.Fill(dImpBoletoBal.Inadimplentes, Database, linhamae.CONCodigo);

            //Boleto[] Boletos = new Boleto[NumeroDeBoeltos];

            string TextoUnidadesI = "";

            ConjuntoLinhasInad Conj = new ConjuntoLinhasInad();
            Framework.objetosNeon.Competencia CompetAnterior = BoletoMae.Competencia.CloneCompet();
            CompetAnterior.Add(-1);

            //int i = 0;
            foreach (dImpBoletoBal.InadimplentesRow nBol in dImpBoletoBal.Inadimplentes){
                Boleto Bol = new Boleto((int)nBol.N�mero_Boleto);
                if (!Bol.Encontrado)
                {
                    throw new Exception(string.Format("Boleto n�o encontrado: {0}", nBol.N�mero_Boleto));
                    //fImportFromMsAccess.ImportaBoleto(((int)nBol.N�mero_Boleto).ToString());
                    //Bol = new Boleto((int)nBol.N�mero_Boleto);
                };
                if (ValorCorrigido)
                    Bol.Valorpara(DateTime.Today);
                string Titulo = (Bol.rowComplementar.BLOCodigo.ToUpper() == "SB") ? "" : Bol.rowComplementar.BLOCodigo + "-";
                Titulo += Bol.rowComplementar.APTNumero;
                
                Conj.Add(Titulo, Bol.rowPrincipal.BOLValorPrevisto, Bol.valorcorrigido, Bol.rowComplementar.BLOCodigo,(Bol.Competencia==CompetAnterior));
            };




            int CompUltimalinha = 0;

            switch (Agrupamento)
            {
                case 1://Lista

                    foreach (LinhaQuadroInadimplente Linha in Conj)
                    {
                        string novogrupo = Linha.Texto;
                        if (NumeroDeUnidades)
                            novogrupo += " Boletos = " + Linha.Quantidade.ToString("00");
                        if (ValorOriginal)
                            novogrupo += " Valor = R$" + Linha.ValorOriginal.ToString("n2").PadLeft(9);
                        if (ValorCorrigido)
                            novogrupo += " Corrigido = R$" + Linha.ValorCorrigido.ToString("n2").PadLeft(9);
                        novogrupo += "; ";

                        if ((CompUltimalinha + novogrupo.Length) > MaxCarar)
                        {
                            TextoUnidadesI += "\r\n";
                            CompUltimalinha = novogrupo.Length;
                        }
                        else
                            CompUltimalinha += novogrupo.Length;
                        TextoUnidadesI += novogrupo;

                    };
                    break;
                case 2://Blocos
                    ConjuntoLinhaBloco ConjB = new ConjuntoLinhaBloco();
                    
                    foreach (LinhaQuadroInadimplente Linha in Conj)                        
                            ConjB.Add(Linha.ValorOriginal, Linha.ValorCorrigido, Linha.Bloco);
                    TextoUnidadesI = "";
                    foreach (LinhaBloco LB in ConjB){
                        TextoUnidadesI += "Bloco " + LB.Bloco + ": ";
                        if (NumeroDeUnidades)
                            TextoUnidadesI += "Unidades: " + LB.Quantidade.ToString("00") + " ";
                        if (ValorOriginal)
                            TextoUnidadesI += "Val.: R$ " + LB.ValorOriginal.ToString("n2").PadLeft(9) + " ";
                        if (ValorCorrigido)
                            TextoUnidadesI += "com encargos: R$ " + LB.ValorCorrigido.ToString("n2").PadLeft(9);
                        TextoUnidadesI += "\r\n";
                    };    
                    
                    
                    break;
                case 3://Numero
                    decimal TotalO = 0;
                    decimal TotalC = 0;
                    if (ValorOriginal || ValorCorrigido)                                            
                        foreach (LinhaQuadroInadimplente Linha in Conj)
                        {
                            TotalO += Linha.ValorOriginal;
                            TotalC += Linha.ValorCorrigido;
                        };
                    TextoUnidadesI = "";
                    if(NumeroDeUnidades)
                        TextoUnidadesI += "Unidades: " + Conj.Count.ToString("00")+"\r\n";
                    if (ValorOriginal)
                        TextoUnidadesI += "Valor original: R$ " + TotalO.ToString("n2") + "\r\n";
                    if (ValorCorrigido)
                        TextoUnidadesI += "Valor original + encargos: R$ " + TotalC.ToString("n2");
                    break;
            }

            switch (DestacarUnidadesInadDoMes)
            {
                case 1:
                    decimal ValorMes=0;
                    foreach (LinhaQuadroInadimplente Linha in Conj)
                        ValorMes += Linha.ValorDoMes;                    
                    if(ValorMes > 0)
                        TextoUnidadesI += "\r\nValor inadimpl�ncia do m�s: R$ " + ValorMes.ToString("n2");
                    break;
                case 2:
                    int InadMes = 0;
                    foreach (LinhaQuadroInadimplente Linha in Conj)
                        if(Linha.ValorDoMes > 0)
                            InadMes++;
                    TextoUnidadesI += "\r\n"+(Conj.Count-InadMes).ToString("00") + " unidades inadimpl�ntes a mais de 30 dias\r\n";
                    TextoUnidadesI += InadMes.ToString("00")+ " unidades inadimpl�tes do m�s";
                    break;
            }
            
            TextoUnidadesInadimplentes.Text = TextoUnidadesI;
            

            return;
        }
        
        private void CamposCalculados() {
            
            if (linhamae.BOLTipoCRAI == "A")
            {
                xrLabel10.Text = "Acordo";
                xrCompetencia1.Text = xrCompetencia.Text = "--";
            }
            else
                xrCompetencia1.Text = xrCompetencia.Text = linhamae.BOLCompetenciaMes.ToString("00") + " " + linhamae.BOLCompetenciaAno.ToString();
            linhamae.Calc_PI = (linhamae.BOLProprietario) ? "P" : "I";
            if (!linhamae.IsBOLMensagemImpNull())
                MensagemDoBoleto = linhamae.BOLMensagemImp;
            if (linhamae.BOLTipoCRAI == "C")
            {
                try
                {
                    object obj = bOletoDetalheTableAdapter.MemBolTipoC(linhamae.BOL);
                    if (obj == null)
                        MensagemDoBoleto = "";
                    else
                        MensagemDoBoleto = obj.ToString();

                    Balancete = ((!Reimpressao) &&  (!linhamae.IsCONImprimeBalanceteBoletoNull()) && (linhamae.CONImprimeBalanceteBoleto));
                }
                catch {
                    //Impressao pela Internet
                    ImpressaoInternet = true;
                    MensagemDoBoleto = "";
                    Balancete = false;
                }
            }
            else {
                if (linhamae.BOLTipoCRAI == "R")
                    MensagemDoBoleto = dImpBoletoBal.MensagemRateio(linhamae.BOL);
                Balancete = false;
            }
            xrMensagem.Text = MensagemDoBoleto;

        /*    string BOLCedente;
            switch (linhamae.BOL_BCO)
            {
                case 237:
                    BOLCedente = linhamae.BOLAgencia + "-" + linhamae.BOLDigitoAgencia + "/" + linhamae.BOLCC + "-" + linhamae.BOLDigitoCC;
                    break;
                case 341:
                    BOLCedente = linhamae.BOLAgencia + "-" + linhamae.BOLDigitoAgencia + "/" + CC.Substring(0, 5) + "-" + CC.Substring(5, 1);
                    break;
                case 409:
                    BOLCedente = linhamae.BOLAgencia + "-" + linhamae.BOLDigitoAgencia + "/" + CC.Substring(0, 6) + "-" + CC.Substring(6, 1);
                    break;
            }*/
        }

        private void ComposicaoDoBoleto() {
            Lista11.Text = "";
            Lista12.Text = "";            
            decimal Total = 0;
            foreach (dImpBoletoBal.BOletoDetalheRow DRit in dImpBoletoBal.BOletoDetalhe)
            {
                /*if (DRit. ["mensagem"] == DBNull.Value)
                {
                    Lista11.Text += "\r\n" + DRit["descbase"];
                }
                else
                {
                    Lista11.Text += "\r\n" + DRit["descbase"] + " " + DRit["mensagem"];
                };
                 */
                Lista11.Text += "\r\n" + DRit.BODMensagem;
                Lista12.Text += "\r\n" + DRit.BODValor.ToString("#,##0.00");
                Total += DRit.BODValor;
            };
            Lista12.Text += "\r\n\r\n" + Total.ToString("#,##0.00");
        }

        private void SetaCodigoDeBarras() {
            LinhaDigitavel.Text = BoletoMae.LinhaDigitavel;
            CodigoDeBarras.Text = BoletoMae.CodigoBarras;
            NNSTR.Text = BoletoMae.NossoNumeroStr;
            NNSTR2.Text = BoletoMae.NossoNumeroStr;
        }

        private Impresso.dImpBoletoBal.SaldosAccessRow SaldosAccessRow; 

        private void CarregaDados() {
            Impresso.dImpBoletoBalTableAdapters.SaldosAccessTableAdapter SaldosAccessTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.SaldosAccessTableAdapter();
            SaldosAccessTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
            SaldosAccessTableAdapter.Fill(dImpBoletoBal.SaldosAccess, linhamae.CONCodigo);
            if (dImpBoletoBal.SaldosAccess.Rows.Count == 1)
            {
                SaldosAccessRow = (Impresso.dImpBoletoBal.SaldosAccessRow)dImpBoletoBal.SaldosAccess.Rows[0];
                Impresso.dImpBoletoBalTableAdapters.ReceitasAccessTableAdapter ReceitasAccessTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.ReceitasAccessTableAdapter();
                d1 = SaldosAccessRow.Data_Inicial;
                d2 = SaldosAccessRow.Data_Final;
                //DateTime D2 = new DateTime(2006, 11, 1);
                ReceitasAccessTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                ReceitasAccessTableAdapter.Fill(dImpBoletoBal.ReceitasAccess, linhamae.CONCodigo, SaldosAccessRow.Data_Inicial, SaldosAccessRow.Data_Final);
            };
        }

        

        private void ImpBoletoBal_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            /*
            
            if (dImpBoletoBal.DadosBoleto.Rows.Count == 1)
            {
                linhamae = (dImpBoletoBal.DadosBoletoRow)dImpBoletoBal.DadosBoleto.Rows[0];
                
                NamePago.Visible = !linhamae.IsBOLPagamentoNull();
                CodigoDeBarras.Visible = !NamePago.Visible;

                CamposCalculados();
                DadosBanco(linhamae.BOL_BCO);
                
                ComposicaoDoBoleto();
                SetaCodigoDeBarras();
            }
            else
            {

                return;
            }

                        

            //AJUSTE
            //Balancete = true;
            //PDadosInternet.Visible = ((bool)dtSaldos.Rows[0]["INTERNET"]);            
            //QuadroDespesas.Visible = Balancete;
            QuadroReceitas.Visible = Balancete;
            //QuadroSaldo.Visible = Balancete;

            if (BoletoMae.Precalculado != null) { 
                             
                    foreach (System.Collections.DictionaryEntry Entrada in BoletoMae.Precalculado) {
                        object oElemento = FindControl(Entrada.Key.ToString(), false);
                        if (oElemento is DevExpress.XtraReports.UI.XRLabel)
                        {
                            DevExpress.XtraReports.UI.XRLabel Elemento = (DevExpress.XtraReports.UI.XRLabel)oElemento;
                            Elemento.Text = Entrada.Value.ToString();
                        }
                        else if (oElemento is DevExpress.XtraReports.UI.XRLine)
                        {
                            DevExpress.XtraReports.UI.XRLine Elemento = (DevExpress.XtraReports.UI.XRLine)oElemento;
                            int[] dadosL = (int[])Entrada.Value;
                            Elemento.Left = dadosL[0];
                            Elemento.Width = dadosL[1];
                        }
                    };
            }


            if (!Balancete)
            {
                QuadroMensagem.Parent = Detail;
                
                QuadroMensagem.Width = xrMensagem.Width = xrLabel118.Width = QuadroDesp.Width;
                
                QuadroMensagem.Top = QuadroEsquerda.Top;
                QuadroMensagem.Left = QuadroDespesas.Left;
                QuadroDespesas.Visible = false;
                QuadroSaldo.Visible = false;
              //  picNeon2.Visible = true;
            }
            else
            {
               // picNeon2.Visible = false;





                if (BoletoMae.Precalculado == null)
                {
                    
                    QuadoReceitas();

                    QuadroSaldos();
                    Despesas();
                };
                                                                                                    

            };
            OcultaQuadrosDesp();
            QuadroPendenciasUnidadeCalc();
            QuadroConsumoCalc();
            
            PosicaoQuadros();
            if (BoletoMae.Precalculado == null)
                CalculaUnidadesInadimplenes();
            //Se n�o existirem inadimplentes esconde o quadro
            QuadroUnidadeInadimplentes.Visible = (TextoUnidadesInadimplentes.Text != "");
            if (BoletoMae.Precalculado == null)
            {
                BoletoMae.Precalculado = new Boletos.Boleto.PreCalculo();
                BoletoMae.Precalculado.Guarda(TextoUnidadesInadimplentes, TituloUnidadesInad);
                BoletoMae.Precalculado.Guarda(QuadroDesp,DespesaNome,DespesaNome1, DespesaNome2, DespesaNomeADM, DespesaNomeADV, DespesaNomeBANC);               
                BoletoMae.Precalculado.Guarda(DespesaValor, DespesaValor1, DespesaValor2, DespesaValorADM, DespesaValorADV, DespesaValorBANC);
                BoletoMae.Precalculado.Guarda(xrTotalDesp);               
                BoletoMae.Precalculado.Guarda(xrLabel12,ReceitaNome,ReceitaT,TotR,xrLabel19,SaldoFinal);
                BoletoMae.Precalculado.Guarda(DGpor, MOpor, ADMpor, ADMApor, Bancpor, HORpor);
                BoletoMae.Precalculado.Guarda(DGLine, MOLine, ADMLine, ADMALine, BancLine, HORLine);
                BoletoMae.Precalculado.Guarda(DGLine1, MOLine1, ADMLine1, ADMALine1, BancLine1, HORLine1);
            };
        }

        private void QuadoReceitas() {
            double[] colunas = new double[4];
            string nome = "";
            int colunasel;
            ReceitaNome.Text = "";
            ReceitaA.Text = "";
            ReceitaMes.Text = "";
            RecetaP.Text = "";
            ReceitaT.Text = "";
            double TotalRec = 0;
            CarregaDados();
            xrLabel12.Text = "RECEITAS " + d1.ToString("dd/MM/yy") + " � " + d2.ToString("dd/MM/yy");

            foreach (dImpBoletoBal.ReceitasAccessRow DR in dImpBoletoBal.ReceitasAccess)
            {

                if (DR["descricao"].ToString() != nome)
                {
                    Registralinha(nome, colunas);
                    nome = DR["descricao"].ToString();
                    for (int i = 0; i < 4; i++)
                        colunas[i] = 0;
                };
                if (DR["vencto"] == DBNull.Value)
                    colunasel = 1;
                else if ((DateTime)DR["vencto"] < d1)
                    colunasel = 0;
                else if ((DateTime)DR["vencto"] > d2)
                    colunasel = 2;
                else
                    colunasel = 1;
                //valorarr = (float)DR["valorpago"];
                //valorarr = (float)Math.Round(valorarr, 2, MidpointRounding.AwayFromZero);
                colunas[colunasel] += (float)DR["valorpago"];
                colunas[3] += (float)DR["valorpago"];

                TotalRec += (float)DR["valorpago"];
            };
            Registralinha(nome, colunas);
            ReceitaT.Text += "\r\n\r\n" + TotalRec.ToString("#,##0.00");
            TotR.Text = (TotalRec + RentabilidadeTotal).ToString("#,##0.00");
            TotR.Text = TotalRec.ToString("#,##0.00");*/
        }

        private void QuadroSaldos()
        {
            /*
            double pc1, pc2, pc3, pc4, pc51, pc52, pc53, pc54, pc61, pc62, pc63, pc64, pc7, pc8;

            pc1 = pc2 = pc3 = pc4 = pc51 = pc52 = pc53 = pc54 = pc61 = pc62 = pc63 = pc64 = pc7 = pc8 = 0;

            foreach (DataRow DR in dtDC.Rows)
            {
                Int32 conta = Int32.Parse(DR["TipoPag"].ToString());
                if ((conta > 0) && (conta < 200000))
                    pc1 += (double)DR["valorpago"];
                else if ((conta >= 200000) && (conta < 300000))
                    pc2 += (double)DR["valorpago"];
                else if ((conta >= 300000) && (conta < 400000))
                    pc3 += (double)DR["valorpago"];

                else if ((conta >= 400000) && (conta < 500000))
                    pc4 += (double)DR["valorpago"];
                else if (conta == 500001)
                    pc51 += (double)DR["valorpago"];
                else if (conta == 500002)
                    pc52 += (double)DR["valorpago"];
                else if (conta == 500003)
                    pc53 += (double)DR["valorpago"];
                else if (conta == 500004)
                    pc54 += (double)DR["valorpago"];
                else if (conta == 600001)
                    pc61 += (double)DR["valorpago"];
                else if (conta == 600002)
                    pc62 += (double)DR["valorpago"];
                else if (conta == 600003)
                    pc63 += (double)DR["valorpago"];
                else if (conta == 600004)
                    pc64 += (double)DR["valorpago"];
                else if ((conta >= 700000) && (conta < 800000))
                    pc7 += (double)DR["valorpago"];
                else if ((conta >= 800000) && (conta < 900000))
                    pc8 += (double)DR["valorpago"];


            };

            */
            double[] SaldosA = new double[8];
            /*
            double[] Credito = new double[7];
            double[] Debitos = new double[7];
            double[] SaldosF = new double[7];            
            double[] Rentabi = new double[7];
            */
            /*
            SaldosA[0] = ((double)(dtSaldos.Rows[0]["SALDOant"]));
            SaldosA[1] = ((double)(dtSaldos.Rows[0]["saldoant"]));
            SaldosA[2] = ((double)(dtSaldos.Rows[0]["SALDOFRANT"]));
            SaldosA[3] = ((double)(dtSaldos.Rows[0]["SALDO13ANT"]));
            SaldosA[4] = ((double)(dtSaldos.Rows[0]["saldooant"]));
            SaldosA[5] = ((double)(dtSaldos.Rows[0]["saldosaant"]));
            SaldosA[6] = ((double)(dtSaldos.Rows[0]["saldoaant"]));
            SaldosA[7] = ((double)(dtSaldos.Rows[0]["saldoapant"]));*/

            SaldosA[0] = SaldosAccessRow.Saldo_Anterior;// ((double)(dtSaldosH.Rows[0]["SALDO anterior"]));
            SaldosA[1] = SaldosAccessRow.Saldo_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo fechamento"]));
            SaldosA[2] = SaldosAccessRow.Saldo_Poupan�a_FR_Fechamento;// ((double)(dtSaldosH.Rows[0]["SALDO Poupan�a FR fechamento"]));
            SaldosA[3] = SaldosAccessRow.Saldo_Poupan�a_13_Fechamento;// ((double)(dtSaldosH.Rows[0]["SALDO Poupan�a 13 fechamento"]));
            SaldosA[4] = SaldosAccessRow.Saldo_Aplica��o_Autom�tica_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo aplica��o autom�tica fechamento"]));
            SaldosA[5] = SaldosAccessRow.Saldo_Poupan�a_Obras_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo Poupan�a obras fechamento"]));
            SaldosA[6] = SaldosAccessRow.Saldo_Poupan�a_Sal�o_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo Poupan�a sal�o fechamento"]));
            SaldosA[7] = SaldosAccessRow.Saldo_Aplica��o_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo aplica��o fechamento"]));
            
            /*
            Credito[0] = pc2;
            Credito[1] = pc51;
            Credito[2] = pc52;
            Credito[3] = pc53;
            Credito[4] = pc54;
            Credito[5] = pc3;
            Credito[6] = pc7;

            Debitos[0] = pc1;
            Debitos[1] = pc61;
            Debitos[2] = pc62;
            Debitos[3] = pc63;
            Debitos[4] = pc64;
            Debitos[5] = pc4;
            Debitos[6] = pc8;

            
            SaldosF[0] = ((double)(dtSaldos.Rows[0]["saldoant"])) + pc1 + pc61 + pc62 + pc63 + pc64 + pc4 + pc8 - pc2 - pc51 - pc52 - pc53 - pc54 - pc3 - pc7;
            SaldosF[1] = ((double)(dtSaldos.Rows[0]["SALDOFRfec"]));
            SaldosF[2] = ((double)(dtSaldos.Rows[0]["SALDO13fec"]));
            SaldosF[3] = ((double)(dtSaldos.Rows[0]["saldoofec"]));
            SaldosF[4] = ((double)(dtSaldos.Rows[0]["saldosafec"]));
            SaldosF[5] = ((double)(dtSaldos.Rows[0]["saldoafec"]));
            SaldosF[6] = ((double)(dtSaldos.Rows[0]["saldoapfec"]));

            RentabilidadeTotal = 0;
            for (int i = 0; i < 7; i++)
            {
                Rentabi[i] = SaldosF[i] - SaldosA[i] - Credito[i] + Debitos[i];
                RentabilidadeTotal += Rentabi[i];
            };

            SubTotR.Text = RentabilidadeTotal.ToString("#,##0.00");

            
            
            SaldoA.Text =
                "\r\n" + SaldosA[0].ToString("#,##0.00") +
                "\r\n" + SaldosA[1].ToString("#,##0.00") +
                "\r\n" + SaldosA[2].ToString("#,##0.00") +
                "\r\n" + SaldosA[3].ToString("#,##0.00") +
                "\r\n" + SaldosA[4].ToString("#,##0.00") +
                "\r\n" + SaldosA[5].ToString("#,##0.00") +
                "\r\n" + SaldosA[6].ToString("#,##0.00");


            for (int i = 0; i < 7; i++)
                if (Rentabi[i] > 0)
                    Credito[i] += Rentabi[i];
                else
                    Debitos[i] -= Rentabi[i];


            SaldoCREDITO.Text =
                "\r\n" + Credito[0].ToString("#,##0.00") +
                "\r\n" + Credito[1].ToString("#,##0.00") +
                "\r\n" + Credito[2].ToString("#,##0.00") +
                "\r\n" + Credito[3].ToString("#,##0.00") +
                "\r\n" + Credito[4].ToString("#,##0.00") +
                "\r\n" + Credito[5].ToString("#,##0.00") +
                "\r\n" + Credito[6].ToString("#,##0.00");

            SaldoDEBTO.Text =
                "\r\n" + Debitos[0].ToString("#,##0.00") +
                "\r\n" + Debitos[1].ToString("#,##0.00") +
                "\r\n" + Debitos[2].ToString("#,##0.00") +
                "\r\n" + Debitos[3].ToString("#,##0.00") +
                "\r\n" + Debitos[4].ToString("#,##0.00") +
                "\r\n" + Debitos[5].ToString("#,##0.00") +
                "\r\n" + Debitos[6].ToString("#,##0.00");
            
            SaldoFinal.Text =
                "\r\n" + SaldosF[0].ToString("#,##0.00") +
                "\r\n" + SaldosF[1].ToString("#,##0.00") +
                "\r\n" + SaldosF[2].ToString("#,##0.00") +
                "\r\n" + SaldosF[3].ToString("#,##0.00") +
                "\r\n" + SaldosF[4].ToString("#,##0.00") +
                "\r\n" + SaldosF[5].ToString("#,##0.00") +
                //"\r\n" + SaldosF[6].ToString("#,##0.00") +
                "\r\n" + SaldosF[6].ToString("#,##0.00");
            */
            SaldoFinal.Text =
                SaldosA[0].ToString("#,##0.00") +
                "\r\n" + SaldosA[1].ToString("#,##0.00") +
                "\r\n" + SaldosA[2].ToString("#,##0.00") +
                "\r\n" + SaldosA[3].ToString("#,##0.00") +
                "\r\n" + SaldosA[4].ToString("#,##0.00") +
                "\r\n" + SaldosA[5].ToString("#,##0.00") +
                "\r\n" + SaldosA[6].ToString("#,##0.00") +
                "\r\n" + SaldosA[7].ToString("#,##0.00");
            

        }

        private void PosicaoQuadros() {
            //Posicionado Quadro Pendencias do Condominio
            if (!QuadroSaldo.Visible)
                QuadroPendenciasUnidade.Top = QuadroSaldo.Top;

            if (!QuadroReceitas.Visible)
                QuadroPendenciasUnidade.Top = QuadroReceitas.Top;

            //Verificando se existe mensagem no boleto senao esconde o quadro.

            if ((MensagemDoBoleto == null) || (MensagemDoBoleto == ""))
            {
                QuadroMensagem.Visible = false;
                int Delta = (QuadroPendenciasUnidade.Top - QuadroMensagem.Top);
                QuadroPendenciasUnidade.Top -= Delta;
                QuadroConsumo.Top -= Delta;
            };

            if (QuadroPendenciasUnidade.Visible == false)
            {                
                QuadroConsumo.Top = QuadroPendenciasUnidade.Top;
            };


            if ((!Balancete) && (!QuadroMensagem.Visible) && (QuadroPendenciasUnidade.Visible))
            {
                QuadroPendenciasUnidade.Parent = Detail;
                QuadroPendenciasUnidade.Top = QuadroEsquerda.Top;
                QuadroPendenciasUnidade.Left = QuadroDespesas.Left;
            };

            

            //passa o quadro mao-de-obra para a outra p�gina se o numero de linhas acumulado > 66

            int Acumulado = 0;
            int DistanciaQuadros = 79;
            int quadrostransferidos = 0;
            if (!PDespG.Visible)
                Acumulado -= 3;
            else
                Acumulado += DespesaNome.Lines.Length;

            if (!PdespMO.Visible)
                Acumulado -= 3;
            else
            {
                if ((Acumulado + DespesaNome1.Lines.Length) > 66)
                {
                    if(quadrostransferidos == 0)
                      QuadroContinua.Top = PdespMO.Top;
                    PdespMO.Parent = QuadroContinuacao;
                    PdespMO.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                }
                Acumulado += DespesaNome1.Lines.Length;
            };
                

            
            
                //passa o quadro administrativa para a outra p�gina se o numero de linhas acumulado > 63
            if (!PDespADMA.Visible)
                Acumulado -= 3;
            else{                                                            
                if ((Acumulado + DespesaNome2.Lines.Length) > 63)
                {
                    if (quadrostransferidos == 0)
                      QuadroContinua.Top = PDespADMA.Top;
                    PDespADMA.Parent = QuadroContinuacao;
                    PDespADMA.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                };
                Acumulado += DespesaNome2.Lines.Length;
            };
                
            //passa o quadro administradora para a outra p�gina se o numero de linhas acumulado

            if (!PAdministradora.Visible)
                Acumulado -= 3;
            else{                                            

                    if ((Acumulado + DespesaNomeADM.Lines.Length) > 59)
                    {
                        if (quadrostransferidos == 0)
                          QuadroContinua.Top = PAdministradora.Top;
                        PAdministradora.Parent = QuadroContinuacao;
                        PAdministradora.Top = 37 + DistanciaQuadros * quadrostransferidos;
                        quadrostransferidos++;
                    };
                    Acumulado += DespesaNomeADM.Lines.Length;
            }

            if (!PDespB.Visible)
                Acumulado -= 3;
            else
            {




                if ((Acumulado + DespesaNomeBANC.Lines.Length) > 56)
                {
                    if (quadrostransferidos == 0)
                      QuadroContinua.Top = PDespB.Top;
                    PDespB.Parent = QuadroContinuacao;
                    PDespB.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                };
                Acumulado += DespesaNomeBANC.Lines.Length;
            };


            if (PDespH.Visible)
            {

                if ((Acumulado + DespesaNomeADV.Lines.Length) > 53)
                {
                    if (quadrostransferidos == 0)
                        QuadroContinua.Top = PDespH.Top;
                    PDespH.Parent = QuadroContinuacao;
                    PDespH.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                };

            }
                
            
            
            if (quadrostransferidos > 0)
            {
                QuadroContinuacao.Visible = true;
                QuadroContinua.Visible = true;
                QuadroTotais.Parent = QuadroContinuacao;
                QuadroTotais.Top = 37 + DistanciaQuadros * quadrostransferidos - 10;
                
            }
            else {
                QuadroUnidadeInadimplentes.Width += 903;
                TituloUnidadesInad.Width = TextoUnidadesInadimplentes.Width = 1778;
            }
            
        }

        private void GravaGasAgua(Framework.objetosNeon.Competencia Comp)
        {
            if (Comp > BoletoMae.Competencia)
                return;
            if (Comp <= BoletoMae.Competencia.CloneCompet(-4))
                return;
            if (ConsMes.Text != "")
            {
                ConsMes.Text += "\r\n";
                ConsLeitGas.Text += "\r\n";
                Consm3Gas.Text += "\r\n";
                ConsKgGas.Text += "\r\n";
                ConsValGas.Text += "\r\n";                
                ConsLeitAgua.Text += "\r\n";
                Consm3Agua.Text += "\r\n";                
                ConsValAgua.Text += "\r\n";
            };            
            if (Comp == BoletoMae.Competencia)
                ConsMes.Text += "*";
            ConsMes.Text += Comp.ToString();

            if (leituragas != 0)
            {
                TemGas = true;
                ConsLeitGas.Text += leituragas.ToString("n3");
                Consm3Gas.Text += m3gas.ToString("n3");
                ConsKgGas.Text += (m3gas * 2.3M).ToString("n2");
                ConsValGas.Text += Valgas.ToString("n2");
            }
            else {
                ConsLeitGas.Text += " - ";
                Consm3Gas.Text += " - ";
                ConsKgGas.Text += " - ";
                ConsValGas.Text += " - ";
            }

            if (leituraagua != 0)
            {
                TemAgua = true;
                ConsLeitAgua.Text += leituraagua.ToString("n3");
                Consm3Agua.Text += m3agua.ToString("n0");
                ConsValAgua.Text += Valagua.ToString("n2");
            }
            else {
                ConsLeitAgua.Text += " - ";
                Consm3Agua.Text += " - ";
                ConsValAgua.Text += " - ";
            };
            m3gas = Valgas = leituragas = m3agua = Valagua = leituraagua = 0;
            Comp = null;
        }

        decimal m3gas;       
        decimal Valgas;
        decimal leituragas;
        decimal m3agua;
        decimal Valagua;
        decimal leituraagua;
        bool TemAgua;
        bool TemGas;

        private void QuadroConsumoCalc(){
            if (ImpressaoInternet || Reimpressao)
            {
                QuadroConsumo.Visible = false;
                return;
            };
            if (linhamae.BOLTipoCRAI != "C")
            {
                QuadroConsumo.Visible = false;
                return;
            };
            dImpBoletoBal.ConsumoTableAdapter.Fill(dImpBoletoBal.Consumo, BoletoMae.rowPrincipal.BOL_APT);
            ConsMes.Text = ConsLeitGas.Text = Consm3Gas.Text = ConsKgGas.Text = ConsValGas.Text = "";
            ConsLeitAgua.Text = Consm3Agua.Text = ConsValAgua.Text = "";
            Framework.objetosNeon.Competencia Comp = null;
            m3gas = Valgas = leituragas = m3agua = Valagua = leituraagua = 0;
            TemAgua = TemGas = false;
            foreach (dImpBoletoBal.ConsumoRow rowGAS in dImpBoletoBal.Consumo) {
                Framework.objetosNeon.Competencia NovaComp = new Framework.objetosNeon.Competencia(rowGAS.GASCompetenciaMes, rowGAS.GASCompetenciaAno);
                if ((Comp != null) && (Comp != NovaComp))
                    GravaGasAgua(Comp);
                Comp = NovaComp;
                if (rowGAS.GASTipo == 1)
                {
                    m3gas = rowGAS.GASM3;
                    Valgas = (rowGAS.IsBODValorNull() ? 0 : rowGAS.BODValor);
                    leituragas = rowGAS.GASLeitura;                       
                    maxBarr = Math.Max(m3gas, maxBarr);
                }
                else
                {
                    m3agua = rowGAS.GASM3;
                    Valagua = (rowGAS.IsBODValorNull() ? 0 : rowGAS.BODValor);
                    leituraagua = rowGAS.GASLeitura;
                    maxBarr2 = Math.Max(m3gas, maxBarr2);
                }
            };
            if (Comp != null)
                GravaGasAgua(Comp);
            ConsPorcGas.Text = Consm3Gas.Text;
            ConsPorcAgua.Text = Consm3Agua.Text;
            if (ConsMes.Text != "")
            {
                QuadroConsumo.Visible = true;
                if (!TemGas)
                {
                    SubQuadroGas.Visible = ConsLeitGas.Visible = Consm3Gas.Visible = ConsKgGas.Visible = ConsValGas.Visible = ConsPorcGas.Visible = false;
                    int delta = SubQuadroAgua.Left - SubQuadroGas.Left;
                    SubQuadroAgua.Left -= delta;
                    SubQuadroAgua.Width += delta;
                    ConsLeitAgua.Left -= delta;
                    Consm3Agua.Left -= delta;
                    ConsValAgua.Left -= delta;
                    ConsPorcAgua.Left -= delta;
                }
                else{
                    decimal PrecoGas=0;
                    if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("SELECT CONValorGas FROM CONDOMINIOS WHERE CON = @P1", out PrecoGas,BoletoMae.rowComplementar.CON))                    
                        TituloGas.Text = "G�s ("+PrecoGas.ToString("n2")+" R$/Kg)";
                };
                if (!TemAgua)
                {
                    SubQuadroAgua.Visible = ConsLeitAgua.Visible = Consm3Agua.Visible = ConsValAgua.Visible = ConsPorcAgua.Visible = false;
                    int delta = SubQuadroAgua.Right - SubQuadroGas.Right;
                    SubQuadroGas.Width += delta;
                };
            }

        }

        private void QuadroPendenciasUnidadeCalc() {
            if (ImpressaoInternet || Reimpressao) {
                QuadroPendenciasUnidade.Visible = false;
                return;
            };
            if (linhamae.BOLTipoCRAI != "C")
            {
                QuadroPendenciasUnidade.Visible = false;
                return;
            }

            

            Impresso.dImpBoletoBalTableAdapters.CobrancaAccessTableAdapter CobrancaAccessTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.CobrancaAccessTableAdapter();
            CobrancaAccessTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Access);
            CobrancaAccessTableAdapter.Fill(dImpBoletoBal.CobrancaAccess, linhamae.CONCodigo,linhamae.BLOCodigo,linhamae.APTNumero);            
            //Boleto.DSBoletosTableAdapters.BoletosEmAbertoTableAdapter _ADPBoletosEmAbertos = new Boleto.DSBoletosTableAdapters.BoletosEmAbertoTableAdapter();
            //Boleto.DSBoletosTableAdapters.Cobran�aTableAdapter TBCob = new Boleto.DSBoletosTableAdapters.Cobran�aTableAdapter();
            //DataTable VerificaJuridico = TBCob.GetData(DR["CODCON"].ToString(),
              //                                                 DR["BLOCO"].ToString(),
                //                                               DR["APARTAMENTO"].ToString());
            
            if ((dImpBoletoBal.CobrancaAccess.Rows.Count > 0) && (dImpBoletoBal.CobrancaAccess.Rows[0]["Status"].ToString() == "J")){
                labRespBA.Text = "******";
                labValorBA.Text = "Juridico ";
                labVenctoBA.Text = "******";
                labCategoriaBA.Text = "******";
                QuadroPendenciasUnidade.Visible = true;
            }
            else{

               Impresso.dImpBoletoBalTableAdapters.BoletosAbertoAccessTableAdapter BoletosAbertoAccessTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.BoletosAbertoAccessTableAdapter();
               BoletosAbertoAccessTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Access);
               BoletosAbertoAccessTableAdapter.Fill(dImpBoletoBal.BoletosAbertoAccess, linhamae.CONCodigo,linhamae.BLOCodigo,linhamae.APTNumero,DateTime.Today);            


              // _DTBBoletosEmAberto = _ADPBoletosEmAbertos.GetData(DR["CODCON"].ToString(),
                //                                               DR["BLOCO"].ToString(),
                  //                                             DR["APARTAMENTO"].ToString(),
                    //                                           DateTime.Today);

               labRespBA.Text = "";
               labValorBA.Text = "";
               labVenctoBA.Text = "";
               labCategoriaBA.Text = "";

               bool mostrar = false;

               foreach (Impresso.dImpBoletoBal.BoletosAbertoAccessRow BoletosAbertoAccessRow in dImpBoletoBal.BoletosAbertoAccess.Rows)
               {
                   DateTime Vencimento = BoletosAbertoAccessRow.Data_Vencimento;
                   if ((Vencimento < DateTime.Today.AddDays(-3))
                       &&
                       ((linhamae.BOLProprietario) || (BoletosAbertoAccessRow.Destinat�rio.ToUpper() == "I"))
                       )
                       mostrar = true;
               }

               


               if (mostrar && (dImpBoletoBal.BoletosAbertoAccess.Rows.Count >10) )
               {
                   mostrar = false;
               };

               if (mostrar)
               {
                

                QuadroPendenciasUnidade.Visible = true;
                

                foreach (Impresso.dImpBoletoBal.BoletosAbertoAccessRow _DTR in dImpBoletoBal.BoletosAbertoAccess.Rows)
                {
                    if ((!linhamae.BOLProprietario) 
                        &&
                        _DTR["Destinat�rio"].ToString().ToUpper() == "P")
                        continue;
                    if (_DTR["Destinat�rio"].ToString().ToUpper() == "P")
                        labRespBA.Text += "PROPRIET�RIO\r\n";
                    else
                        labRespBA.Text += "INQUILINO\r\n";

                    labValorBA.Text += Convert.ToDecimal(_DTR["Valor Previsto"]).ToString("n2") + "\r\n";

                    labVenctoBA.Text += Convert.ToDateTime(_DTR["Data Vencimento"]).ToString("dd/MM/yyyy") + "\r\n";

                    if (_DTR["Condominio"].ToString().ToUpper() == "C")
                        labCategoriaBA.Text += "CONDOM�NIO" + "\r\n";
                    else
                        if (_DTR["Condominio"].ToString().ToUpper() == "R")
                            labCategoriaBA.Text += "RATEIO" + "\r\n";
                        else
                            if (_DTR["Condominio"].ToString().ToUpper() == "I")
                                labCategoriaBA.Text += "   " + "\r\n";
                            else
                                if (_DTR["Condominio"].ToString().ToUpper() == "A")
                                    labCategoriaBA.Text += "ACORDO" + "\r\n";
                                else
                                    labCategoriaBA.Text += _DTR["Condominio"].ToString().ToUpper() + "\r\n";
                }
               }
               else{
                QuadroPendenciasUnidade.Visible = false;                
               }
            };
            
        }

        private void xrLabel115_Draw(object sender, DrawEventArgs e)
        {                        
           /* 
            int largura = 3;
            String Manobra;
            String NUM1,NUM2;
            String Saida;
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            //rect.Y += 40;
            //XRLabel control = (XRLabel)e.Data.Control;
            String numero = e.Data.Text;
            foreach (char c in numero)
                if (!char.IsDigit(c))
                    return;
            StringBuilder Retorno = new StringBuilder();
            string[] a = new string[10];
            a[0]="00110";
            a[1]="10001";
            a[2]="01001";
            a[3]="11000";
            a[4]="00101";
            a[5]="10100";
            a[6]="01100";
            a[7]="00011";
            a[8]="10010";
            a[9]="01010";

//            if (e.UniGraphics is GdiGraphics)
            {
          IGraphics graph = e.UniGraphics;
          graph.FillRectangle(Brushes.White, rect);
                
                   

          

          Manobra=numero.Trim();
          if ((Manobra.Length % 2) != 0)
              Manobra="0"+Manobra;

          Saida="0000";

          Int32 indice1;
          Int32 indice2;
          char base0 = Convert.ToChar("0");
          for (int c = 1; c <= (Manobra.Length / 2);c++){
              indice1 = Manobra[2 * (c - 1) + 1 - 1] - base0;
              indice2 = Manobra[2 * (c - 1) + 2-1]- base0;
              NUM1 = a[indice1];
              NUM2 = a[indice2];
              for (int b = 0 ;b < 5;b++)
                  Saida+=(NUM1.Substring(b,1)+NUM2.Substring(b,1));
          };
          Saida+="100";

          for(int m = 1; m <= Saida.Length;m++){
              if ((m % 2) != 0)
              { //impar
                  if (Saida[m - 1] == Convert.ToChar("1"))
                      rect.Width = 3 * largura;
                  else
                      rect.Width = 1 * largura;
                  graph.FillRectangle(Brushes.Black, rect);
                  rect.X += rect.Width;
              }
              //par
              else
                  if (Saida[m - 1] == Convert.ToChar("1"))
                      rect.X += 3 * largura;
                  else
                      rect.X += 1 * largura;
          };

          //rect = Rectangle.Truncate(e.Bounds);
          //rect.Y += rect.Height / 2;
          //graph.Graphics.FillRectangle(Brushes.White, rect);
          //graph.DrawString(numero, control.Font, Brushes.Black, rect, e.Data.StringFormat);
            }
            */
        }

        private void ConsPorcAgua_Draw(object sender, DrawEventArgs e)
        {
            DrawBarras(e, maxBarr2);
        }

        public bool EstouroDeQuadro = false;
        private float maxQuadroDesp = 0;
        private float maxQuadroContinuacao = 0;

        //int teste = 0;

        private void QuadroTotais_Draw(object sender, DrawEventArgs e)
        {
            /*
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            rect.X += teste;
            teste += 100;
            rect.Width = 90;
            GdiGraphics graph = (GdiGraphics)e.UniGraphics;
            graph.Graphics.FillRectangle(Brushes.Red, rect);
             */ 

            DevExpress.XtraReports.UI.XRPanel Sender = (DevExpress.XtraReports.UI.XRPanel)sender;
            if(Sender.Parent == QuadroDespesas)
                maxQuadroDesp = Math.Max(maxQuadroDesp,e.Bounds.Bottom);
            else
                maxQuadroContinuacao = Math.Max(maxQuadroContinuacao,e.Bounds.Bottom);                                       
        }

        private void QuadroDespesas_Draw(object sender, DrawEventArgs e)
        {
            /*
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            rect.X += teste;
            teste += 100;
            rect.Width = 50;
            GdiGraphics graph = (GdiGraphics)e.UniGraphics;
            graph.Graphics.FillRectangle(Brushes.Blue, rect);
            rect.X += 50;
            rect.Y -= 10;
            rect.Width = 40;
            graph.Graphics.FillRectangle(Brushes.Green, rect);

             */
 
            if (maxQuadroDesp > e.Bounds.Bottom) {
                //System.Windows.Forms.MessageBox.Show("estouro");
                EstouroDeQuadro = true;
            }
            
        }

        private void QuadroContinuacao_Draw(object sender, DrawEventArgs e)
        {
            /*
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            rect.X += teste;
            teste += 100;
            rect.Width = 50;
            GdiGraphics graph = (GdiGraphics)e.UniGraphics;
            graph.Graphics.FillRectangle(Brushes.Green, rect);            
            rect.X += 50;            
            rect.Width = 40;
            rect.Y -= 10;
            graph.Graphics.FillRectangle(Brushes.Blue, rect);
            */

            if (maxQuadroContinuacao > e.Bounds.Bottom)
            {
                //System.Windows.Forms.MessageBox.Show("estouro");
                EstouroDeQuadro = true;
            }
        }

        

        

                
    }      
}



