namespace Boletos.Boleto.Impresso
{
    partial class ImpBoletoBal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpBoletoBal));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PainelSegunaPagina = new DevExpress.XtraReports.UI.XRPanel();
            this.QuadroContinuacao = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroUnidadeInadimplentes = new DevExpress.XtraReports.UI.XRPanel();
            this.TextoUnidadesInadimplentes = new DevExpress.XtraReports.UI.XRLabel();
            this.TituloUnidadesInad = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroEndereco = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel20 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox7 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.QuadroRemetente = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel26 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel35 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel34 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel33 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel32 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel31 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel30 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel29 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel28 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel27 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel21 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.bindingSourceEmpresa = new System.Windows.Forms.BindingSource(this.components);
            this.dImpBoletoBal = new Boletos.Boleto.Impresso.dImpBoletoBal();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel7 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel36 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel25 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPictureBox6 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPanel24 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUsoBanco = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCarteira = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel106 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrEspecie = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel23 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabelEspecie = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel19 = new DevExpress.XtraReports.UI.XRPanel();
            this.NNSTR2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel18 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel17 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel16 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel15 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel14 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel13 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel12 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrAgencia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel11 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrProprietario = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel123 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabBlo2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabBlo1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCompetencia1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel10 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel22 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel8 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.BANCOLocal = new DevExpress.XtraReports.UI.XRLabel();
            this.PDadosInternet = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel128 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel129 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel130 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox4 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox5 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.QuadroEsquerda = new DevExpress.XtraReports.UI.XRPanel();
            this.QuadroConsumo = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel45 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel139 = new DevExpress.XtraReports.UI.XRLabel();
            this.Consm3Agua = new DevExpress.XtraReports.UI.XRLabel();
            this.SubQuadroAgua = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel135 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel137 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsPorcAgua = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsPorcGas = new DevExpress.XtraReports.UI.XRLabel();
            this.SubQuadroGas = new DevExpress.XtraReports.UI.XRPanel();
            this.TituloGas = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsValAgua = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsLeitAgua = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsKgGas = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsValGas = new DevExpress.XtraReports.UI.XRLabel();
            this.Consm3Gas = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsMes = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsLeitGas = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroReceitas = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel37 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.RecetaP = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaMes = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaA = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaNome = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.SubTotR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.TotR = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroSaldo = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel38 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.SaldoFinal = new DevExpress.XtraReports.UI.XRLabel();
            this.SaldoCREDITO = new DevExpress.XtraReports.UI.XRLabel();
            this.SaldoDEBTO = new DevExpress.XtraReports.UI.XRLabel();
            this.SaldoA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroComposicaoBoleto = new DevExpress.XtraReports.UI.XRPanel();
            this.Lista11 = new DevExpress.XtraReports.UI.XRLabel();
            this.Lista12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroPendenciasUnidade = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.labRespBA = new DevExpress.XtraReports.UI.XRLabel();
            this.labValorBA = new DevExpress.XtraReports.UI.XRLabel();
            this.labVenctoBA = new DevExpress.XtraReports.UI.XRLabel();
            this.labCategoriaBA = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroMensagem = new DevExpress.XtraReports.UI.XRPanel();
            this.xrMensagem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this.NamePago = new DevExpress.XtraReports.UI.XRLabel();
            this.CodigoDeBarras = new DevExpress.XtraReports.UI.XRLabel();
            this.LinhaDigitavel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.BANCONumero = new DevExpress.XtraReports.UI.XRLabel();
            this.BANCONome = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel5 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.QuadroDespesas = new DevExpress.XtraReports.UI.XRPanel();
            this.QuadroContinua = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroTotais = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTotalDesp = new DevExpress.XtraReports.UI.XRLabel();
            this.PDespADMA = new DevExpress.XtraReports.UI.XRPanel();
            this.DespesaValor2 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNome2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.ADMpor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel39 = new DevExpress.XtraReports.UI.XRPanel();
            this.ADMLine = new DevExpress.XtraReports.UI.XRLine();
            this.ADMLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.PAdministradora = new DevExpress.XtraReports.UI.XRPanel();
            this.DespesaValorADM = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNomeADM = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.ADMApor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel40 = new DevExpress.XtraReports.UI.XRPanel();
            this.ADMALine = new DevExpress.XtraReports.UI.XRLine();
            this.ADMALine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroDesp = new DevExpress.XtraReports.UI.XRLabel();
            this.PDespG = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel6 = new DevExpress.XtraReports.UI.XRPanel();
            this.DGLine = new DevExpress.XtraReports.UI.XRLine();
            this.DGLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.DGpor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaValor = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNome = new DevExpress.XtraReports.UI.XRLabel();
            this.PdespMO = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel9 = new DevExpress.XtraReports.UI.XRPanel();
            this.MOLine = new DevExpress.XtraReports.UI.XRLine();
            this.MOLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.MOpor = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaValor1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNome1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PDespB = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel41 = new DevExpress.XtraReports.UI.XRPanel();
            this.BancLine = new DevExpress.XtraReports.UI.XRLine();
            this.BancLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.Bancpor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNomeBANC = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaValorBANC = new DevExpress.XtraReports.UI.XRLabel();
            this.PDespH = new DevExpress.XtraReports.UI.XRPanel();
            this.DespesaNomeADV = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel42 = new DevExpress.XtraReports.UI.XRPanel();
            this.HORLine = new DevExpress.XtraReports.UI.XRLine();
            this.HORLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.HORpor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaValorADV = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCompetencia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.NNSTR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabApt3 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabBlo3 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabApt4 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabBlo4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.dataTable1TableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.DadosBoletoTableAdapter();
            this.bOletoDetalheTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.BOletoDetalheTableAdapter();
            this.eMPRESASTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.EMPRESASTableAdapter();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.PainelSegunaPagina,
            this.QuadroEndereco,
            this.QuadroRemetente,
            this.xrPanel7,
            this.PDadosInternet,
            this.xrPanel4,
            this.QuadroEsquerda,
            this.NamePago,
            this.CodigoDeBarras,
            this.LinhaDigitavel,
            this.xrLabel33,
            this.xrLabel32,
            this.BANCONumero,
            this.BANCONome,
            this.xrPanel5,
            this.xrPictureBox1,
            this.QuadroDespesas,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel8,
            this.xrLabel9,
            this.xrLabel10,
            this.xrCompetencia,
            this.xrPanel2,
            this.xrLine1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 5620F;
            this.Detail.KeepTogether = true;
            this.Detail.MultiColumn.ColumnWidth = 880F;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // PainelSegunaPagina
            // 
            this.PainelSegunaPagina.CanGrow = false;
            this.PainelSegunaPagina.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroContinuacao,
            this.QuadroUnidadeInadimplentes});
            this.PainelSegunaPagina.Dpi = 254F;
            this.PainelSegunaPagina.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4895F);
            this.PainelSegunaPagina.Name = "PainelSegunaPagina";
            this.PainelSegunaPagina.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PainelSegunaPagina.SizeF = new System.Drawing.SizeF(1799F, 725F);
            // 
            // QuadroContinuacao
            // 
            this.QuadroContinuacao.CanGrow = false;
            this.QuadroContinuacao.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel41});
            this.QuadroContinuacao.Dpi = 254F;
            this.QuadroContinuacao.LocationFloat = new DevExpress.Utils.PointFloat(894F, 0F);
            this.QuadroContinuacao.Name = "QuadroContinuacao";
            this.QuadroContinuacao.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroContinuacao.SizeF = new System.Drawing.SizeF(905F, 720F);
            this.QuadroContinuacao.Visible = false;
            this.QuadroContinuacao.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroContinuacao_Draw);
            // 
            // xrLabel41
            // 
            this.xrLabel41.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel41.Dpi = 254F;
            this.xrLabel41.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(900F, 37F);
            this.xrLabel41.Text = "DESPESAS (Continua��o)";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel41.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // QuadroUnidadeInadimplentes
            // 
            this.QuadroUnidadeInadimplentes.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TextoUnidadesInadimplentes,
            this.TituloUnidadesInad});
            this.QuadroUnidadeInadimplentes.Dpi = 254F;
            this.QuadroUnidadeInadimplentes.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.QuadroUnidadeInadimplentes.Name = "QuadroUnidadeInadimplentes";
            this.QuadroUnidadeInadimplentes.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroUnidadeInadimplentes.SizeF = new System.Drawing.SizeF(889F, 90F);
            // 
            // TextoUnidadesInadimplentes
            // 
            this.TextoUnidadesInadimplentes.BackColor = System.Drawing.Color.Transparent;
            this.TextoUnidadesInadimplentes.BorderColor = System.Drawing.Color.Silver;
            this.TextoUnidadesInadimplentes.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TextoUnidadesInadimplentes.Dpi = 254F;
            this.TextoUnidadesInadimplentes.Font = new System.Drawing.Font("Courier New", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextoUnidadesInadimplentes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.TextoUnidadesInadimplentes.LocationFloat = new DevExpress.Utils.PointFloat(6F, 37F);
            this.TextoUnidadesInadimplentes.Multiline = true;
            this.TextoUnidadesInadimplentes.Name = "TextoUnidadesInadimplentes";
            this.TextoUnidadesInadimplentes.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TextoUnidadesInadimplentes.SizeF = new System.Drawing.SizeF(878F, 50F);
            this.TextoUnidadesInadimplentes.Text = "TextoUnidadesInadimplentes";
            this.TextoUnidadesInadimplentes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TituloUnidadesInad
            // 
            this.TituloUnidadesInad.BackColor = System.Drawing.Color.DarkGray;
            this.TituloUnidadesInad.CanShrink = true;
            this.TituloUnidadesInad.Dpi = 254F;
            this.TituloUnidadesInad.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TituloUnidadesInad.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TituloUnidadesInad.LocationFloat = new DevExpress.Utils.PointFloat(6F, 0F);
            this.TituloUnidadesInad.Name = "TituloUnidadesInad";
            this.TituloUnidadesInad.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TituloUnidadesInad.SizeF = new System.Drawing.SizeF(878F, 37F);
            this.TituloUnidadesInad.Text = "UNIDADES INADIMPLENTES";
            this.TituloUnidadesInad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.TituloUnidadesInad.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel7_Draw);
            // 
            // QuadroEndereco
            // 
            this.QuadroEndereco.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel72,
            this.xrPanel20,
            this.xrPictureBox7});
            this.QuadroEndereco.Dpi = 254F;
            this.QuadroEndereco.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3823F);
            this.QuadroEndereco.Name = "QuadroEndereco";
            this.QuadroEndereco.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroEndereco.SizeF = new System.Drawing.SizeF(1593F, 836F);
            // 
            // xrPanel20
            // 
            this.xrPanel20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel95,
            this.xrLabel94,
            this.xrLabel93,
            this.xrLabel92,
            this.xrLabel91,
            this.xrLabel90,
            this.xrLabel89,
            this.xrLabel88,
            this.xrLabel75,
            this.xrLabel74});
            this.xrPanel20.Dpi = 254F;
            this.xrPanel20.LocationFloat = new DevExpress.Utils.PointFloat(26.00003F, 418F);
            this.xrPanel20.Name = "xrPanel20";
            this.xrPanel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel20.SizeF = new System.Drawing.SizeF(1376F, 341.3125F);
            // 
            // xrLabel95
            // 
            this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLEndereco")});
            this.xrLabel95.Dpi = 254F;
            this.xrLabel95.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(339F, 265F);
            this.xrLabel95.Multiline = true;
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(1026F, 58F);
            this.xrLabel95.Text = "xrLabel95";
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel94.Dpi = 254F;
            this.xrLabel94.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(32F, 265F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(296F, 58F);
            this.xrLabel94.Text = "Endere�o:";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel93.Dpi = 254F;
            this.xrLabel93.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(32F, 180F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(296F, 58F);
            this.xrLabel93.Text = "Nome:";
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel92.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLNome")});
            this.xrLabel92.Dpi = 254F;
            this.xrLabel92.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(339F, 180F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(1026F, 58F);
            this.xrLabel92.Text = "xrLabel92";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel91
            // 
            this.xrLabel91.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel91.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.xrLabel91.Dpi = 254F;
            this.xrLabel91.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(773F, 111F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(233F, 58F);
            this.xrLabel91.Text = "xrLabel91";
            this.xrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel90
            // 
            this.xrLabel90.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel90.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCodigo")});
            this.xrLabel90.Dpi = 254F;
            this.xrLabel90.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(339F, 106F);
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(233F, 58F);
            this.xrLabel90.Text = "xrLabel90";
            this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel89
            // 
            this.xrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel89.Dpi = 254F;
            this.xrLabel89.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(603F, 111F);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(154F, 58F);
            this.xrLabel89.Text = "Apto:";
            this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel88.Dpi = 254F;
            this.xrLabel88.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(37F, 106F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(296F, 58F);
            this.xrLabel88.Text = "Bloco:";
            this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrLabel75.Dpi = 254F;
            this.xrLabel75.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(339F, 32F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(1026F, 58F);
            this.xrLabel75.Text = "xrLabel75";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel74
            // 
            this.xrLabel74.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel74.Dpi = 254F;
            this.xrLabel74.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(32F, 32F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(296F, 58F);
            this.xrLabel74.Text = "Condom�nio:";
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox7
            // 
            this.xrPictureBox7.Dpi = 254F;
            this.xrPictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox7.Image")));
            this.xrPictureBox7.LocationFloat = new DevExpress.Utils.PointFloat(16F, 32F);
            this.xrPictureBox7.Name = "xrPictureBox7";
            this.xrPictureBox7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPictureBox7.SizeF = new System.Drawing.SizeF(592F, 329F);
            this.xrPictureBox7.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // QuadroRemetente
            // 
            this.QuadroRemetente.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel26,
            this.xrPanel1});
            this.QuadroRemetente.Dpi = 254F;
            this.QuadroRemetente.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2953F);
            this.QuadroRemetente.Name = "QuadroRemetente";
            this.QuadroRemetente.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroRemetente.SizeF = new System.Drawing.SizeF(1794F, 825F);
            // 
            // xrPanel26
            // 
            this.xrPanel26.BorderColor = System.Drawing.Color.DarkGray;
            this.xrPanel26.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel97,
            this.xrPanel35,
            this.xrPanel34,
            this.xrPanel33,
            this.xrPanel32,
            this.xrPanel31,
            this.xrPanel30,
            this.xrPanel29,
            this.xrPanel28,
            this.xrPanel27,
            this.xrPanel21,
            this.xrLabel96,
            this.xrLabel87,
            this.xrLabel86});
            this.xrPanel26.Dpi = 254F;
            this.xrPanel26.LocationFloat = new DevExpress.Utils.PointFloat(1074F, 29F);
            this.xrPanel26.Name = "xrPanel26";
            this.xrPanel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel26.SizeF = new System.Drawing.SizeF(667F, 418F);
            // 
            // xrLabel11
            // 
            this.xrLabel11.Angle = 180F;
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(16F, 101F);
            this.xrLabel11.Multiline = true;
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(566F, 108F);
            this.xrLabel11.Text = "N�o existe o N� Indicado\r\n\r\nInforma��o escrita pelo porteiro ou zelador";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel97
            // 
            this.xrLabel97.Angle = 180F;
            this.xrLabel97.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel97.Dpi = 254F;
            this.xrLabel97.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel97.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(29F, 246F);
            this.xrLabel97.Multiline = true;
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel97.SizeF = new System.Drawing.SizeF(204F, 108F);
            this.xrLabel97.Text = "Falecido\r\nAusente\r\nN�o Procurado";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel35
            // 
            this.xrPanel35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel35.Dpi = 254F;
            this.xrPanel35.LocationFloat = new DevExpress.Utils.PointFloat(243F, 249F);
            this.xrPanel35.Name = "xrPanel35";
            this.xrPanel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel35.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel34
            // 
            this.xrPanel34.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel34.Dpi = 254F;
            this.xrPanel34.LocationFloat = new DevExpress.Utils.PointFloat(243F, 283F);
            this.xrPanel34.Name = "xrPanel34";
            this.xrPanel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel34.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel33
            // 
            this.xrPanel33.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel33.Dpi = 254F;
            this.xrPanel33.LocationFloat = new DevExpress.Utils.PointFloat(243F, 320F);
            this.xrPanel33.Name = "xrPanel33";
            this.xrPanel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel33.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel32
            // 
            this.xrPanel32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel32.Dpi = 254F;
            this.xrPanel32.LocationFloat = new DevExpress.Utils.PointFloat(587F, 106F);
            this.xrPanel32.Name = "xrPanel32";
            this.xrPanel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel32.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel31
            // 
            this.xrPanel31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel31.Dpi = 254F;
            this.xrPanel31.LocationFloat = new DevExpress.Utils.PointFloat(587F, 140F);
            this.xrPanel31.Name = "xrPanel31";
            this.xrPanel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel31.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel30
            // 
            this.xrPanel30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel30.Dpi = 254F;
            this.xrPanel30.LocationFloat = new DevExpress.Utils.PointFloat(587F, 175F);
            this.xrPanel30.Name = "xrPanel30";
            this.xrPanel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel30.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel29
            // 
            this.xrPanel29.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel29.Dpi = 254F;
            this.xrPanel29.LocationFloat = new DevExpress.Utils.PointFloat(587F, 212F);
            this.xrPanel29.Name = "xrPanel29";
            this.xrPanel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel29.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel28
            // 
            this.xrPanel28.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel28.Dpi = 254F;
            this.xrPanel28.LocationFloat = new DevExpress.Utils.PointFloat(587F, 249F);
            this.xrPanel28.Name = "xrPanel28";
            this.xrPanel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel28.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel27
            // 
            this.xrPanel27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel27.Dpi = 254F;
            this.xrPanel27.LocationFloat = new DevExpress.Utils.PointFloat(587F, 283F);
            this.xrPanel27.Name = "xrPanel27";
            this.xrPanel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel27.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel21
            // 
            this.xrPanel21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel21.Dpi = 254F;
            this.xrPanel21.LocationFloat = new DevExpress.Utils.PointFloat(587F, 320F);
            this.xrPanel21.Name = "xrPanel21";
            this.xrPanel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel21.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrLabel96
            // 
            this.xrLabel96.Angle = 180F;
            this.xrLabel96.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel96.Dpi = 254F;
            this.xrLabel96.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel96.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(349F, 212F);
            this.xrLabel96.Multiline = true;
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(233F, 148F);
            this.xrLabel96.Text = "Mudou-se\r\nDesconhecido\r\nRecusado\r\nEnd. Insuficiente";
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel87
            // 
            this.xrLabel87.Angle = 180F;
            this.xrLabel87.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel87.Dpi = 254F;
            this.xrLabel87.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel87.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(164F, 5F);
            this.xrLabel87.Multiline = true;
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(497F, 90F);
            this.xrLabel87.Text = "Reintegrado ao Servi�o Postal em __/__/__\r\nEm __/__/__   ________________________" +
                "\r\n                     Assinatura e n� do entregador";
            this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Angle = 180F;
            this.xrLabel86.Dpi = 254F;
            this.xrLabel86.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel86.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(0F, 365F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(667F, 53F);
            this.xrLabel86.Text = "PARA USO DOS CORREIOS";
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPanel1
            // 
            this.xrPanel1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrPanel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel1.BorderWidth = 1;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel46,
            this.xrLabel42,
            this.xrLabel55,
            this.xrLabel68,
            this.xrLine10,
            this.xrLabel122,
            this.xrLabel53,
            this.xrLabel57,
            this.xrLabel115,
            this.xrLabel117,
            this.xrLabel25});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(21F, 21F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1032F, 312F);
            // 
            // xrLabel46
            // 
            this.xrLabel46.Angle = 180F;
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSourceEmpresa, "EndBairro")});
            this.xrLabel46.Dpi = 254F;
            this.xrLabel46.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(21F, 132F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(990F, 48F);
            this.xrLabel46.Text = "xrLabel46";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // bindingSourceEmpresa
            // 
            this.bindingSourceEmpresa.DataMember = "EMPRESAS";
            this.bindingSourceEmpresa.DataSource = this.dImpBoletoBal;
            // 
            // dImpBoletoBal
            // 
            this.dImpBoletoBal.DataSetName = "dImpBoletoBal";
            this.dImpBoletoBal.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Angle = 180F;
            this.xrLabel42.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.BorderWidth = 1;
            this.xrLabel42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSourceEmpresa, "EMPCidade")});
            this.xrLabel42.Dpi = 254F;
            this.xrLabel42.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel42.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(413F, 75F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(598F, 42F);
            this.xrLabel42.Text = "xrLabel42";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Angle = 180F;
            this.xrLabel55.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.BorderWidth = 1;
            this.xrLabel55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSourceEmpresa, "EMPUF")});
            this.xrLabel55.Dpi = 254F;
            this.xrLabel55.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(296F, 75F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(101F, 42F);
            this.xrLabel55.Text = "xrLabel55";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Angle = 180F;
            this.xrLabel68.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.BorderWidth = 1;
            this.xrLabel68.Dpi = 254F;
            this.xrLabel68.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(381F, 265F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(254F, 37F);
            this.xrLabel68.Text = "Remetente";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine10
            // 
            this.xrLine10.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLine10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine10.BorderWidth = 1;
            this.xrLine10.Dpi = 254F;
            this.xrLine10.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLine10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 249F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine10.SizeF = new System.Drawing.SizeF(1027F, 11F);
            // 
            // xrLabel122
            // 
            this.xrLabel122.Angle = 180F;
            this.xrLabel122.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel122.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel122.BorderWidth = 1;
            this.xrLabel122.CanGrow = false;
            this.xrLabel122.Dpi = 254F;
            this.xrLabel122.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xrLabel122.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(376F, 15F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(106F, 42F);
            this.xrLabel122.Text = "Fax: ";
            this.xrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Angle = 180F;
            this.xrLabel53.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.BorderWidth = 1;
            this.xrLabel53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSourceEmpresa, "EMPCEP")});
            this.xrLabel53.Dpi = 254F;
            this.xrLabel53.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(21F, 75F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(259F, 43F);
            this.xrLabel53.Text = "xrLabel53";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Angle = 180F;
            this.xrLabel57.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel57.BorderWidth = 1;
            this.xrLabel57.CanGrow = false;
            this.xrLabel57.Dpi = 254F;
            this.xrLabel57.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xrLabel57.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(889F, 15F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(121F, 42F);
            this.xrLabel57.Text = "Fone: ";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Angle = 180F;
            this.xrLabel115.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel115.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel115.BorderWidth = 1;
            this.xrLabel115.CanGrow = false;
            this.xrLabel115.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSourceEmpresa, "EMPFone")});
            this.xrLabel115.Dpi = 254F;
            this.xrLabel115.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel115.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(540F, 15F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(339F, 47F);
            this.xrLabel115.Text = "xrLabel115";
            this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel117
            // 
            this.xrLabel117.Angle = 180F;
            this.xrLabel117.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel117.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel117.BorderWidth = 1;
            this.xrLabel117.CanGrow = false;
            this.xrLabel117.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSourceEmpresa, "EMPFax")});
            this.xrLabel117.Dpi = 254F;
            this.xrLabel117.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel117.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(26F, 15F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(339F, 47F);
            this.xrLabel117.Text = "xrLabel117";
            this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Angle = 180F;
            this.xrLabel25.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.BorderWidth = 1;
            this.xrLabel25.CanGrow = false;
            this.xrLabel25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSourceEmpresa, "EMPRazao")});
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(21F, 195F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(990F, 42F);
            this.xrLabel25.Text = "xrLabel25";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrPanel7
            // 
            this.xrPanel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel7.CanGrow = false;
            this.xrPanel7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel36,
            this.xrPanel25,
            this.xrPanel24,
            this.xrPanel23,
            this.xrPanel19,
            this.xrPanel18,
            this.xrPanel17,
            this.xrPanel16,
            this.xrPanel15,
            this.xrPanel14,
            this.xrPanel13,
            this.xrPanel12,
            this.xrPanel11,
            this.xrPanel10,
            this.xrPanel22,
            this.xrPanel8});
            this.xrPanel7.Dpi = 254F;
            this.xrPanel7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2058F);
            this.xrPanel7.Name = "xrPanel7";
            this.xrPanel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel7.SizeF = new System.Drawing.SizeF(1799F, 661F);
            // 
            // xrPanel36
            // 
            this.xrPanel36.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel36.CanGrow = false;
            this.xrPanel36.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel85,
            this.xrLabel84});
            this.xrPanel36.Dpi = 254F;
            this.xrPanel36.LocationFloat = new DevExpress.Utils.PointFloat(380F, 250F);
            this.xrPanel36.Name = "xrPanel36";
            this.xrPanel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel36.SizeF = new System.Drawing.SizeF(820F, 250F);
            // 
            // xrLabel85
            // 
            this.xrLabel85.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel85.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLMensagem")});
            this.xrLabel85.Dpi = 254F;
            this.xrLabel85.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.xrLabel85.Multiline = true;
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel85.SizeF = new System.Drawing.SizeF(815F, 229F);
            this.xrLabel85.Text = "xrLabel85";
            this.xrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.Dpi = 254F;
            this.xrLabel84.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(693F, 21F);
            this.xrLabel84.Text = "Instru��es (Texto de responsabilidade do Cedente)";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel25
            // 
            this.xrPanel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel25.CanGrow = false;
            this.xrPanel25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox6});
            this.xrPanel25.Dpi = 254F;
            this.xrPanel25.LocationFloat = new DevExpress.Utils.PointFloat(0F, 250F);
            this.xrPanel25.Name = "xrPanel25";
            this.xrPanel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel25.SizeF = new System.Drawing.SizeF(379F, 250F);
            // 
            // xrPictureBox6
            // 
            this.xrPictureBox6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox6.Dpi = 254F;
            this.xrPictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox6.Image")));
            this.xrPictureBox6.LocationFloat = new DevExpress.Utils.PointFloat(53F, 32F);
            this.xrPictureBox6.Name = "xrPictureBox6";
            this.xrPictureBox6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPictureBox6.SizeF = new System.Drawing.SizeF(275F, 180F);
            this.xrPictureBox6.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrPanel24
            // 
            this.xrPanel24.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel24.CanGrow = false;
            this.xrPanel24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel103,
            this.xrLabelUsoBanco,
            this.xrLabelCarteira,
            this.xrLine9,
            this.xrLine8,
            this.xrLine7,
            this.xrLine6,
            this.xrLabel108,
            this.xrLabel107,
            this.xrLabel106,
            this.xrEspecie,
            this.xrLabel104,
            this.xrLabel82});
            this.xrPanel24.Dpi = 254F;
            this.xrPanel24.LocationFloat = new DevExpress.Utils.PointFloat(0F, 200F);
            this.xrPanel24.Name = "xrPanel24";
            this.xrPanel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel24.SizeF = new System.Drawing.SizeF(1200F, 50F);
            // 
            // xrLabel103
            // 
            this.xrLabel103.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel103.CanGrow = false;
            this.xrLabel103.Dpi = 254F;
            this.xrLabel103.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(254F, 0F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(85F, 21F);
            this.xrLabel103.Text = "Carteira";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabelUsoBanco
            // 
            this.xrLabelUsoBanco.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelUsoBanco.Dpi = 254F;
            this.xrLabelUsoBanco.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelUsoBanco.LocationFloat = new DevExpress.Utils.PointFloat(11F, 18F);
            this.xrLabelUsoBanco.Name = "xrLabelUsoBanco";
            this.xrLabelUsoBanco.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelUsoBanco.SizeF = new System.Drawing.SizeF(222F, 21F);
            this.xrLabelUsoBanco.Text = "CVT 7744-5";
            this.xrLabelUsoBanco.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelCarteira
            // 
            this.xrLabelCarteira.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelCarteira.CanGrow = false;
            this.xrLabelCarteira.Dpi = 254F;
            this.xrLabelCarteira.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelCarteira.LocationFloat = new DevExpress.Utils.PointFloat(344F, 18F);
            this.xrLabelCarteira.Name = "xrLabelCarteira";
            this.xrLabelCarteira.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCarteira.SizeF = new System.Drawing.SizeF(96F, 21F);
            this.xrLabelCarteira.Text = "6";
            this.xrLabelCarteira.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine9
            // 
            this.xrLine9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine9.Dpi = 254F;
            this.xrLine9.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine9.LineWidth = 3;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(897F, 0F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine9.SizeF = new System.Drawing.SizeF(11F, 19F);
            // 
            // xrLine8
            // 
            this.xrLine8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine8.Dpi = 254F;
            this.xrLine8.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine8.LineWidth = 3;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(675F, 0F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine8.SizeF = new System.Drawing.SizeF(11F, 50F);
            // 
            // xrLine7
            // 
            this.xrLine7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine7.Dpi = 254F;
            this.xrLine7.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine7.LineWidth = 3;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(444F, 0F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine7.SizeF = new System.Drawing.SizeF(11F, 50F);
            // 
            // xrLine6
            // 
            this.xrLine6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine6.Dpi = 254F;
            this.xrLine6.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine6.LineWidth = 3;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(241F, 0F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine6.SizeF = new System.Drawing.SizeF(11F, 50F);
            // 
            // xrLabel108
            // 
            this.xrLabel108.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel108.CanGrow = false;
            this.xrLabel108.Dpi = 254F;
            this.xrLabel108.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(888F, 19F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(35F, 31F);
            this.xrLabel108.Text = "x";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomJustify;
            // 
            // xrLabel107
            // 
            this.xrLabel107.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel107.Dpi = 254F;
            this.xrLabel107.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(955F, 0F);
            this.xrLabel107.Name = "xrLabel107";
            this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel107.SizeF = new System.Drawing.SizeF(74F, 26F);
            this.xrLabel107.Text = "Valor";
            this.xrLabel107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel106
            // 
            this.xrLabel106.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel106.CanGrow = false;
            this.xrLabel106.Dpi = 254F;
            this.xrLabel106.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel106.LocationFloat = new DevExpress.Utils.PointFloat(688F, 0F);
            this.xrLabel106.Name = "xrLabel106";
            this.xrLabel106.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel106.SizeF = new System.Drawing.SizeF(127F, 26F);
            this.xrLabel106.Text = "Quantidade";
            this.xrLabel106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrEspecie
            // 
            this.xrEspecie.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrEspecie.Dpi = 254F;
            this.xrEspecie.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrEspecie.LocationFloat = new DevExpress.Utils.PointFloat(550F, 18F);
            this.xrEspecie.Name = "xrEspecie";
            this.xrEspecie.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrEspecie.SizeF = new System.Drawing.SizeF(116F, 21F);
            this.xrEspecie.Text = "R$";
            this.xrEspecie.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel104
            // 
            this.xrLabel104.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel104.CanGrow = false;
            this.xrLabel104.Dpi = 254F;
            this.xrLabel104.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(460F, 0F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(80F, 21F);
            this.xrLabel104.Text = "Esp�cie";
            this.xrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel82.CanGrow = false;
            this.xrLabel82.Dpi = 254F;
            this.xrLabel82.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(138F, 18F);
            this.xrLabel82.Text = "Uso do Banco";
            this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel23
            // 
            this.xrPanel23.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel23.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPanel23.BorderWidth = 1;
            this.xrPanel23.CanGrow = false;
            this.xrPanel23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelEspecie,
            this.xrLabel81,
            this.xrLine5,
            this.xrLine4,
            this.xrLine3,
            this.xrLine2,
            this.xrLabel102,
            this.xrLabel101,
            this.xrLabel100,
            this.xrLabel99,
            this.xrLabel98,
            this.xrLabel79,
            this.xrLabel80});
            this.xrPanel23.Dpi = 254F;
            this.xrPanel23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 150F);
            this.xrPanel23.Name = "xrPanel23";
            this.xrPanel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel23.SizeF = new System.Drawing.SizeF(1200F, 50F);
            // 
            // xrLabelEspecie
            // 
            this.xrLabelEspecie.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelEspecie.Dpi = 254F;
            this.xrLabelEspecie.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelEspecie.LocationFloat = new DevExpress.Utils.PointFloat(566F, 18F);
            this.xrLabelEspecie.Name = "xrLabelEspecie";
            this.xrLabelEspecie.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelEspecie.SizeF = new System.Drawing.SizeF(143F, 21F);
            this.xrLabelEspecie.Text = "N";
            this.xrLabelEspecie.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel81
            // 
            this.xrLabel81.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel81.Dpi = 254F;
            this.xrLabel81.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(820F, 18F);
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel81.SizeF = new System.Drawing.SizeF(58F, 20F);
            this.xrLabel81.Text = "N";
            this.xrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine5
            // 
            this.xrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine5.Dpi = 254F;
            this.xrLine5.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine5.LineWidth = 3;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(233F, 0F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine5.SizeF = new System.Drawing.SizeF(11F, 50F);
            // 
            // xrLine4
            // 
            this.xrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine4.Dpi = 254F;
            this.xrLine4.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine4.LineWidth = 3;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(508F, 0F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine4.SizeF = new System.Drawing.SizeF(11F, 50F);
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine3.Dpi = 254F;
            this.xrLine3.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine3.LineWidth = 3;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(765F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine3.SizeF = new System.Drawing.SizeF(11F, 50F);
            // 
            // xrLine2
            // 
            this.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine2.LineWidth = 3;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(921F, 0F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine2.SizeF = new System.Drawing.SizeF(11F, 50F);
            // 
            // xrLabel102
            // 
            this.xrLabel102.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel102.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLEmissao", "{0:dd/MM/yyyy}")});
            this.xrLabel102.Dpi = 254F;
            this.xrLabel102.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(963F, 18F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(212F, 21F);
            this.xrLabel102.Text = "xrLabel102";
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel101
            // 
            this.xrLabel101.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel101.CanGrow = false;
            this.xrLabel101.Dpi = 254F;
            this.xrLabel101.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(939F, 0F);
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(249F, 18F);
            this.xrLabel101.Text = "Data do Processamento";
            this.xrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel100
            // 
            this.xrLabel100.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel100.CanGrow = false;
            this.xrLabel100.Dpi = 254F;
            this.xrLabel100.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(778F, 0F);
            this.xrLabel100.Name = "xrLabel100";
            this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel100.SizeF = new System.Drawing.SizeF(69F, 18F);
            this.xrLabel100.Text = "Aceite";
            this.xrLabel100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel99
            // 
            this.xrLabel99.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel99.CanGrow = false;
            this.xrLabel99.Dpi = 254F;
            this.xrLabel99.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(524F, 0F);
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(222F, 18F);
            this.xrLabel99.Text = "Especie do Documento";
            this.xrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel98
            // 
            this.xrLabel98.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel98.CanGrow = false;
            this.xrLabel98.Dpi = 254F;
            this.xrLabel98.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(249F, 0F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(227F, 26F);
            this.xrLabel98.Text = "N�mero do Documento";
            this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel79.CanGrow = false;
            this.xrLabel79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLEmissao", "{0:dd/MM/yyyy}")});
            this.xrLabel79.Dpi = 254F;
            this.xrLabel79.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(13F, 18F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(212F, 21F);
            this.xrLabel79.Text = "xrLabel79";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.CanGrow = false;
            this.xrLabel80.Dpi = 254F;
            this.xrLabel80.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(201F, 18F);
            this.xrLabel80.Text = "Data do Documento";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel19
            // 
            this.xrPanel19.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.NNSTR2,
            this.xrLabel73});
            this.xrPanel19.Dpi = 254F;
            this.xrPanel19.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 150F);
            this.xrPanel19.Name = "xrPanel19";
            this.xrPanel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel19.SizeF = new System.Drawing.SizeF(598F, 50F);
            // 
            // NNSTR2
            // 
            this.NNSTR2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.NNSTR2.Dpi = 254F;
            this.NNSTR2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NNSTR2.LocationFloat = new DevExpress.Utils.PointFloat(202F, 0F);
            this.NNSTR2.Name = "NNSTR2";
            this.NNSTR2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.NNSTR2.SizeF = new System.Drawing.SizeF(375F, 42F);
            this.NNSTR2.Text = "NNSTR2";
            this.NNSTR2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.Dpi = 254F;
            this.xrLabel73.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(170F, 32F);
            this.xrLabel73.Text = "Nosso N�mero";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel18
            // 
            this.xrPanel18.BackColor = System.Drawing.Color.LightGray;
            this.xrPanel18.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel18.CanGrow = false;
            this.xrPanel18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel71,
            this.xrLabel70});
            this.xrPanel18.Dpi = 254F;
            this.xrPanel18.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 200F);
            this.xrPanel18.Name = "xrPanel18";
            this.xrPanel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel18.SizeF = new System.Drawing.SizeF(598F, 50F);
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.Dpi = 254F;
            this.xrLabel71.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(243F, 26F);
            this.xrLabel71.Text = "(=) Valor do Documento";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel70
            // 
            this.xrLabel70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel70.CanGrow = false;
            this.xrLabel70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:#,##0.00}")});
            this.xrLabel70.Dpi = 254F;
            this.xrLabel70.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(282F, 0F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(300F, 48F);
            this.xrLabel70.Text = "xrLabel70";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrPanel17
            // 
            this.xrPanel17.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel69});
            this.xrPanel17.Dpi = 254F;
            this.xrPanel17.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 250F);
            this.xrPanel17.Name = "xrPanel17";
            this.xrPanel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel17.SizeF = new System.Drawing.SizeF(598F, 50F);
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.Dpi = 254F;
            this.xrLabel69.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(291F, 32F);
            this.xrLabel69.Text = "(-) Desconto/Abatimento";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel16
            // 
            this.xrPanel16.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel16.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel67});
            this.xrPanel16.Dpi = 254F;
            this.xrPanel16.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 400F);
            this.xrPanel16.Name = "xrPanel16";
            this.xrPanel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel16.SizeF = new System.Drawing.SizeF(598F, 50F);
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.Dpi = 254F;
            this.xrLabel67.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(275F, 32F);
            this.xrLabel67.Text = "(+) Outros Acr�sciomos";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel15
            // 
            this.xrPanel15.BackColor = System.Drawing.Color.LightGray;
            this.xrPanel15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrPanel15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel65});
            this.xrPanel15.Dpi = 254F;
            this.xrPanel15.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 450F);
            this.xrPanel15.Name = "xrPanel15";
            this.xrPanel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel15.SizeF = new System.Drawing.SizeF(598F, 50F);
            // 
            // xrLabel65
            // 
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel65.Dpi = 254F;
            this.xrLabel65.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(254F, 32F);
            this.xrLabel65.Text = "(=) Valor Cobrado";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel14
            // 
            this.xrPanel14.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel14.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel63});
            this.xrPanel14.Dpi = 254F;
            this.xrPanel14.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 300F);
            this.xrPanel14.Name = "xrPanel14";
            this.xrPanel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel14.SizeF = new System.Drawing.SizeF(598F, 50F);
            // 
            // xrLabel63
            // 
            this.xrLabel63.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel63.Dpi = 254F;
            this.xrLabel63.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(254F, 32F);
            this.xrLabel63.Text = "(-) Outras Dedu��es";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel13
            // 
            this.xrPanel13.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel61});
            this.xrPanel13.Dpi = 254F;
            this.xrPanel13.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 350F);
            this.xrPanel13.Name = "xrPanel13";
            this.xrPanel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel13.SizeF = new System.Drawing.SizeF(598F, 50F);
            // 
            // xrLabel61
            // 
            this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel61.Dpi = 254F;
            this.xrLabel61.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(254F, 32F);
            this.xrLabel61.Text = "(+) Mora/Multa";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel12
            // 
            this.xrPanel12.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel12.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrAgencia,
            this.xrLabel59});
            this.xrPanel12.Dpi = 254F;
            this.xrPanel12.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 100F);
            this.xrPanel12.Name = "xrPanel12";
            this.xrPanel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel12.SizeF = new System.Drawing.SizeF(598F, 50F);
            // 
            // xrAgencia
            // 
            this.xrAgencia.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrAgencia.Dpi = 254F;
            this.xrAgencia.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrAgencia.LocationFloat = new DevExpress.Utils.PointFloat(282F, 0F);
            this.xrAgencia.Name = "xrAgencia";
            this.xrAgencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrAgencia.SizeF = new System.Drawing.SizeF(300F, 49F);
            this.xrAgencia.Text = "xrAgencia";
            this.xrAgencia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.Dpi = 254F;
            this.xrLabel59.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(276F, 26F);
            this.xrLabel59.Text = "Ag�ncia/C�digo do Cedente";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel11
            // 
            this.xrPanel11.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPanel11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel131,
            this.xrProprietario,
            this.xrLabel124,
            this.xrLabel123,
            this.LabBlo2,
            this.xrLabel119,
            this.LabBlo1,
            this.xrCompetencia1,
            this.xrLabel113,
            this.xrLabel112,
            this.xrLabel111,
            this.xrLabel110,
            this.xrLabel109});
            this.xrPanel11.Dpi = 254F;
            this.xrPanel11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 500F);
            this.xrPanel11.Name = "xrPanel11";
            this.xrPanel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel11.SizeF = new System.Drawing.SizeF(1799F, 161F);
            // 
            // xrLabel131
            // 
            this.xrLabel131.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel131.Dpi = 254F;
            this.xrLabel131.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(1492F, 13F);
            this.xrLabel131.Name = "xrLabel131";
            this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel131.SizeF = new System.Drawing.SizeF(90F, 35F);
            this.xrLabel131.Text = "N� Neon";
            this.xrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrProprietario
            // 
            this.xrProprietario.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrProprietario.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Calc_PI")});
            this.xrProprietario.Dpi = 254F;
            this.xrProprietario.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrProprietario.LocationFloat = new DevExpress.Utils.PointFloat(1749F, 3F);
            this.xrProprietario.Name = "xrProprietario";
            this.xrProprietario.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrProprietario.SizeF = new System.Drawing.SizeF(37F, 45F);
            this.xrProprietario.Text = "xrProprietario";
            this.xrProprietario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel124
            // 
            this.xrLabel124.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel124.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.xrLabel124.Dpi = 254F;
            this.xrLabel124.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(1582F, 3F);
            this.xrLabel124.Name = "xrLabel124";
            this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel124.SizeF = new System.Drawing.SizeF(154F, 45F);
            this.xrLabel124.Text = "xrLabel124";
            this.xrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel123
            // 
            this.xrLabel123.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel123.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.xrLabel123.Dpi = 254F;
            this.xrLabel123.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel123.LocationFloat = new DevExpress.Utils.PointFloat(1492F, 51F);
            this.xrLabel123.Name = "xrLabel123";
            this.xrLabel123.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel123.SizeF = new System.Drawing.SizeF(116F, 45F);
            this.xrLabel123.Text = "xrLabel123";
            this.xrLabel123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LabBlo2
            // 
            this.LabBlo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.LabBlo2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCodigo")});
            this.LabBlo2.Dpi = 254F;
            this.LabBlo2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabBlo2.LocationFloat = new DevExpress.Utils.PointFloat(1281F, 50F);
            this.LabBlo2.Name = "LabBlo2";
            this.LabBlo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabBlo2.SizeF = new System.Drawing.SizeF(106F, 45F);
            this.LabBlo2.Text = "LabBlo2";
            this.LabBlo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel119
            // 
            this.xrLabel119.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel119.Dpi = 254F;
            this.xrLabel119.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(1416F, 56F);
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(68F, 34F);
            this.xrLabel119.Text = "Apto";
            this.xrLabel119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // LabBlo1
            // 
            this.LabBlo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.LabBlo1.Dpi = 254F;
            this.LabBlo1.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabBlo1.LocationFloat = new DevExpress.Utils.PointFloat(1193F, 56F);
            this.LabBlo1.Name = "LabBlo1";
            this.LabBlo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabBlo1.SizeF = new System.Drawing.SizeF(82F, 34F);
            this.LabBlo1.Text = "Bloco";
            this.LabBlo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCompetencia1
            // 
            this.xrCompetencia1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrCompetencia1.Dpi = 254F;
            this.xrCompetencia1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCompetencia1.LocationFloat = new DevExpress.Utils.PointFloat(1331F, 0F);
            this.xrCompetencia1.Name = "xrCompetencia1";
            this.xrCompetencia1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCompetencia1.SizeF = new System.Drawing.SizeF(156F, 45F);
            this.xrCompetencia1.Text = "00 0000";
            this.xrCompetencia1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel113
            // 
            this.xrLabel113.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel113.Dpi = 254F;
            this.xrLabel113.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(1193F, 13F);
            this.xrLabel113.Name = "xrLabel113";
            this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel113.SizeF = new System.Drawing.SizeF(132F, 34F);
            this.xrLabel113.Text = "Compet�ncia";
            this.xrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel112
            // 
            this.xrLabel112.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel112.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLEndereco")});
            this.xrLabel112.Dpi = 254F;
            this.xrLabel112.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(127F, 108F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(1640F, 45F);
            this.xrLabel112.Text = "xrLabel112";
            this.xrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel111
            // 
            this.xrLabel111.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel111.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrLabel111.Dpi = 254F;
            this.xrLabel111.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(127F, 54F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(1050F, 53F);
            this.xrLabel111.Text = "xrLabel111";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel110
            // 
            this.xrLabel110.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel110.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLNome")});
            this.xrLabel110.Dpi = 254F;
            this.xrLabel110.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(127F, 0F);
            this.xrLabel110.Name = "xrLabel110";
            this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel110.SizeF = new System.Drawing.SizeF(1050F, 53F);
            this.xrLabel110.Text = "xrLabel110";
            this.xrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel109
            // 
            this.xrLabel109.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel109.Dpi = 254F;
            this.xrLabel109.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(116F, 26F);
            this.xrLabel109.Text = "Sacado";
            this.xrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel10
            // 
            this.xrPanel10.BackColor = System.Drawing.Color.LightGray;
            this.xrPanel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel36,
            this.xrLabel35});
            this.xrPanel10.Dpi = 254F;
            this.xrPanel10.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 0F);
            this.xrPanel10.Name = "xrPanel10";
            this.xrPanel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel10.SizeF = new System.Drawing.SizeF(598F, 100F);
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yyyy}")});
            this.xrLabel36.Dpi = 254F;
            this.xrLabel36.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(16F, 21F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(561F, 53F);
            this.xrLabel36.Text = "xrLabel36";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.Dpi = 254F;
            this.xrLabel35.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(127F, 21F);
            this.xrLabel35.Text = "Vencimento";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel22
            // 
            this.xrPanel22.BackColor = System.Drawing.Color.Transparent;
            this.xrPanel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel77,
            this.xrLabel78});
            this.xrPanel22.Dpi = 254F;
            this.xrPanel22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 100F);
            this.xrPanel22.Name = "xrPanel22";
            this.xrPanel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel22.SizeF = new System.Drawing.SizeF(1200F, 50F);
            // 
            // xrLabel77
            // 
            this.xrLabel77.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel77.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrLabel77.Dpi = 254F;
            this.xrLabel77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(116F, 2F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(1080F, 47F);
            this.xrLabel77.Text = "xrLabel77";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.Dpi = 254F;
            this.xrLabel78.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(106F, 26F);
            this.xrLabel78.Text = "Cedente";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel8
            // 
            this.xrPanel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrPanel8.CanGrow = false;
            this.xrPanel8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel34,
            this.BANCOLocal});
            this.xrPanel8.Dpi = 254F;
            this.xrPanel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel8.Name = "xrPanel8";
            this.xrPanel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel8.SizeF = new System.Drawing.SizeF(1200F, 100F);
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.CanGrow = false;
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(275F, 18F);
            this.xrLabel34.Text = "Local de Pagamento";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BANCOLocal
            // 
            this.BANCOLocal.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.BANCOLocal.Dpi = 254F;
            this.BANCOLocal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BANCOLocal.LocationFloat = new DevExpress.Utils.PointFloat(4F, 18F);
            this.BANCOLocal.Multiline = true;
            this.BANCOLocal.Name = "BANCOLocal";
            this.BANCOLocal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.BANCOLocal.SizeF = new System.Drawing.SizeF(1191F, 82F);
            this.BANCOLocal.Text = "AT� O VENCIMENTO, EM QUALQUER AG. DA REDE BANC�RIA";
            this.BANCOLocal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PDadosInternet
            // 
            this.PDadosInternet.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel128,
            this.xrLabel127,
            this.xrLabel129,
            this.xrLabel130});
            this.PDadosInternet.Dpi = 254F;
            this.PDadosInternet.LocationFloat = new DevExpress.Utils.PointFloat(587F, 1873F);
            this.PDadosInternet.Name = "PDadosInternet";
            this.PDadosInternet.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PDadosInternet.SizeF = new System.Drawing.SizeF(281F, 85F);
            // 
            // xrLabel128
            // 
            this.xrLabel128.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IntSenha")});
            this.xrLabel128.Dpi = 254F;
            this.xrLabel128.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel128.LocationFloat = new DevExpress.Utils.PointFloat(85F, 41F);
            this.xrLabel128.Name = "xrLabel128";
            this.xrLabel128.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel128.SizeF = new System.Drawing.SizeF(190F, 40F);
            this.xrLabel128.Text = "xrLabel128";
            this.xrLabel128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel127
            // 
            this.xrLabel127.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IntUsu")});
            this.xrLabel127.Dpi = 254F;
            this.xrLabel127.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(85F, 0F);
            this.xrLabel127.Name = "xrLabel127";
            this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel127.SizeF = new System.Drawing.SizeF(189F, 41F);
            this.xrLabel127.Text = "xrLabel127";
            this.xrLabel127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel129
            // 
            this.xrLabel129.Dpi = 254F;
            this.xrLabel129.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel129.LocationFloat = new DevExpress.Utils.PointFloat(0F, 8F);
            this.xrLabel129.Name = "xrLabel129";
            this.xrLabel129.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel129.SizeF = new System.Drawing.SizeF(85F, 32F);
            this.xrLabel129.Text = "Usu�rio";
            this.xrLabel129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel130
            // 
            this.xrLabel130.Dpi = 254F;
            this.xrLabel130.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel130.LocationFloat = new DevExpress.Utils.PointFloat(0F, 48F);
            this.xrLabel130.Name = "xrLabel130";
            this.xrLabel130.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel130.SizeF = new System.Drawing.SizeF(69F, 21F);
            this.xrLabel130.Text = "Senha";
            this.xrLabel130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel4
            // 
            this.xrPanel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel132,
            this.xrLabel24,
            this.xrPictureBox4,
            this.xrPictureBox5});
            this.xrPanel4.Dpi = 254F;
            this.xrPanel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1871F);
            this.xrPanel4.Name = "xrPanel4";
            this.xrPanel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel4.SizeF = new System.Drawing.SizeF(587F, 87F);
            // 
            // xrLabel132
            // 
            this.xrLabel132.CanGrow = false;
            this.xrLabel132.Dpi = 254F;
            this.xrLabel132.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(48F, 5F);
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(498F, 37F);
            this.xrLabel132.Text = "4125-6400 Fax: 4121-1162";
            this.xrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Dpi = 254F;
            this.xrLabel24.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(48F, 46F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(534F, 37F);
            this.xrLabel24.Text = "WWW.neonimoveis.com.br";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox4
            // 
            this.xrPictureBox4.Dpi = 254F;
            this.xrPictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox4.Image")));
            this.xrPictureBox4.LocationFloat = new DevExpress.Utils.PointFloat(5F, 5F);
            this.xrPictureBox4.Name = "xrPictureBox4";
            this.xrPictureBox4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPictureBox4.SizeF = new System.Drawing.SizeF(37.04167F, 39.6875F);
            this.xrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize;
            // 
            // xrPictureBox5
            // 
            this.xrPictureBox5.Dpi = 254F;
            this.xrPictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox5.Image")));
            this.xrPictureBox5.LocationFloat = new DevExpress.Utils.PointFloat(5F, 46F);
            this.xrPictureBox5.Name = "xrPictureBox5";
            this.xrPictureBox5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPictureBox5.SizeF = new System.Drawing.SizeF(39.6875F, 39.6875F);
            this.xrPictureBox5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize;
            // 
            // QuadroEsquerda
            // 
            this.QuadroEsquerda.CanGrow = false;
            this.QuadroEsquerda.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroConsumo,
            this.QuadroReceitas,
            this.QuadroSaldo,
            this.QuadroComposicaoBoleto,
            this.QuadroPendenciasUnidade,
            this.QuadroMensagem});
            this.QuadroEsquerda.Dpi = 254F;
            this.QuadroEsquerda.LocationFloat = new DevExpress.Utils.PointFloat(0F, 164F);
            this.QuadroEsquerda.Name = "QuadroEsquerda";
            this.QuadroEsquerda.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroEsquerda.SizeF = new System.Drawing.SizeF(873F, 1704F);
            // 
            // QuadroConsumo
            // 
            this.QuadroConsumo.BorderColor = System.Drawing.Color.Silver;
            this.QuadroConsumo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.QuadroConsumo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel45,
            this.Consm3Agua,
            this.SubQuadroAgua,
            this.ConsPorcAgua,
            this.ConsPorcGas,
            this.SubQuadroGas,
            this.ConsValAgua,
            this.ConsLeitAgua,
            this.ConsKgGas,
            this.ConsValGas,
            this.Consm3Gas,
            this.xrLabel58,
            this.ConsMes,
            this.ConsLeitGas});
            this.QuadroConsumo.Dpi = 254F;
            this.QuadroConsumo.LocationFloat = new DevExpress.Utils.PointFloat(11F, 940F);
            this.QuadroConsumo.Name = "QuadroConsumo";
            this.QuadroConsumo.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroConsumo.SizeF = new System.Drawing.SizeF(857F, 108F);
            this.QuadroConsumo.Visible = false;
            // 
            // xrPanel45
            // 
            this.xrPanel45.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel45.CanGrow = false;
            this.xrPanel45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel139});
            this.xrPanel45.Dpi = 254F;
            this.xrPanel45.LocationFloat = new DevExpress.Utils.PointFloat(0F, 32F);
            this.xrPanel45.Name = "xrPanel45";
            this.xrPanel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel45.SizeF = new System.Drawing.SizeF(95F, 46F);
            // 
            // xrLabel139
            // 
            this.xrLabel139.Dpi = 254F;
            this.xrLabel139.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel139.ForeColor = System.Drawing.Color.Black;
            this.xrLabel139.LocationFloat = new DevExpress.Utils.PointFloat(5F, 11F);
            this.xrLabel139.Name = "xrLabel139";
            this.xrLabel139.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel139.SizeF = new System.Drawing.SizeF(81F, 21F);
            this.xrLabel139.Text = "M�s/Ano";
            this.xrLabel139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Consm3Agua
            // 
            this.Consm3Agua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Consm3Agua.CanShrink = true;
            this.Consm3Agua.Dpi = 254F;
            this.Consm3Agua.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consm3Agua.LocationFloat = new DevExpress.Utils.PointFloat(619F, 80F);
            this.Consm3Agua.Multiline = true;
            this.Consm3Agua.Name = "Consm3Agua";
            this.Consm3Agua.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Consm3Agua.SizeF = new System.Drawing.SizeF(69F, 25F);
            this.Consm3Agua.Text = "100.0000";
            this.Consm3Agua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // SubQuadroAgua
            // 
            this.SubQuadroAgua.BackColor = System.Drawing.Color.DarkGray;
            this.SubQuadroAgua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.SubQuadroAgua.CanGrow = false;
            this.SubQuadroAgua.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel134,
            this.xrLabel135,
            this.xrLabel137,
            this.xrLabel138});
            this.SubQuadroAgua.Dpi = 254F;
            this.SubQuadroAgua.LocationFloat = new DevExpress.Utils.PointFloat(519F, 32F);
            this.SubQuadroAgua.Name = "SubQuadroAgua";
            this.SubQuadroAgua.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.SubQuadroAgua.SizeF = new System.Drawing.SizeF(327F, 46F);
            // 
            // xrLabel134
            // 
            this.xrLabel134.BorderColor = System.Drawing.Color.Black;
            this.xrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel134.Dpi = 254F;
            this.xrLabel134.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel134.ForeColor = System.Drawing.Color.Black;
            this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel134.Name = "xrLabel134";
            this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel134.SizeF = new System.Drawing.SizeF(327F, 20F);
            this.xrLabel134.Text = "�gua";
            this.xrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel135
            // 
            this.xrLabel135.Dpi = 254F;
            this.xrLabel135.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel135.ForeColor = System.Drawing.Color.Black;
            this.xrLabel135.LocationFloat = new DevExpress.Utils.PointFloat(200F, 21F);
            this.xrLabel135.Name = "xrLabel135";
            this.xrLabel135.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel135.SizeF = new System.Drawing.SizeF(37F, 21F);
            this.xrLabel135.Text = "R$";
            this.xrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel137
            // 
            this.xrLabel137.Dpi = 254F;
            this.xrLabel137.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel137.ForeColor = System.Drawing.Color.Black;
            this.xrLabel137.LocationFloat = new DevExpress.Utils.PointFloat(126F, 21F);
            this.xrLabel137.Name = "xrLabel137";
            this.xrLabel137.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel137.SizeF = new System.Drawing.SizeF(42F, 21F);
            this.xrLabel137.Text = "m�";
            this.xrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel138
            // 
            this.xrLabel138.Dpi = 254F;
            this.xrLabel138.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel138.ForeColor = System.Drawing.Color.Black;
            this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(10F, 21F);
            this.xrLabel138.Name = "xrLabel138";
            this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel138.SizeF = new System.Drawing.SizeF(85F, 21F);
            this.xrLabel138.Text = "Leitura";
            this.xrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ConsPorcAgua
            // 
            this.ConsPorcAgua.BackColor = System.Drawing.Color.Transparent;
            this.ConsPorcAgua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsPorcAgua.CanShrink = true;
            this.ConsPorcAgua.Dpi = 254F;
            this.ConsPorcAgua.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsPorcAgua.LocationFloat = new DevExpress.Utils.PointFloat(777F, 80F);
            this.ConsPorcAgua.Multiline = true;
            this.ConsPorcAgua.Name = "ConsPorcAgua";
            this.ConsPorcAgua.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsPorcAgua.SizeF = new System.Drawing.SizeF(65F, 25F);
            this.ConsPorcAgua.Text = "xxxx";
            this.ConsPorcAgua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsPorcAgua.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.ConsPorcAgua_Draw);
            // 
            // ConsPorcGas
            // 
            this.ConsPorcGas.BackColor = System.Drawing.Color.Transparent;
            this.ConsPorcGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsPorcGas.CanShrink = true;
            this.ConsPorcGas.Dpi = 254F;
            this.ConsPorcGas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsPorcGas.LocationFloat = new DevExpress.Utils.PointFloat(430F, 80F);
            this.ConsPorcGas.Multiline = true;
            this.ConsPorcGas.Name = "ConsPorcGas";
            this.ConsPorcGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsPorcGas.SizeF = new System.Drawing.SizeF(92F, 25F);
            this.ConsPorcGas.Text = "xxxx";
            this.ConsPorcGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsPorcGas.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DrawBarras);
            // 
            // SubQuadroGas
            // 
            this.SubQuadroGas.BackColor = System.Drawing.Color.DarkGray;
            this.SubQuadroGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.SubQuadroGas.CanGrow = false;
            this.SubQuadroGas.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TituloGas,
            this.xrLabel66,
            this.xrLabel64,
            this.xrLabel62,
            this.xrLabel60});
            this.SubQuadroGas.Dpi = 254F;
            this.SubQuadroGas.LocationFloat = new DevExpress.Utils.PointFloat(101F, 32F);
            this.SubQuadroGas.Name = "SubQuadroGas";
            this.SubQuadroGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.SubQuadroGas.SizeF = new System.Drawing.SizeF(412F, 46F);
            // 
            // TituloGas
            // 
            this.TituloGas.BorderColor = System.Drawing.Color.Black;
            this.TituloGas.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.TituloGas.Dpi = 254F;
            this.TituloGas.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TituloGas.ForeColor = System.Drawing.Color.Black;
            this.TituloGas.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.TituloGas.Name = "TituloGas";
            this.TituloGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TituloGas.SizeF = new System.Drawing.SizeF(411F, 20F);
            this.TituloGas.Text = "G�s";
            this.TituloGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Dpi = 254F;
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.ForeColor = System.Drawing.Color.Black;
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(269F, 21F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(37F, 21F);
            this.xrLabel66.Text = "R$";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Dpi = 254F;
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.ForeColor = System.Drawing.Color.Black;
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(195F, 21F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(37F, 21F);
            this.xrLabel64.Text = "Kg";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Dpi = 254F;
            this.xrLabel62.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel62.ForeColor = System.Drawing.Color.Black;
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(116F, 21F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(32F, 21F);
            this.xrLabel62.Text = "m�";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Dpi = 254F;
            this.xrLabel60.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel60.ForeColor = System.Drawing.Color.Black;
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(5F, 21F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(74F, 18F);
            this.xrLabel60.Text = "Leitura";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // ConsValAgua
            // 
            this.ConsValAgua.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ConsValAgua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsValAgua.CanShrink = true;
            this.ConsValAgua.Dpi = 254F;
            this.ConsValAgua.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsValAgua.LocationFloat = new DevExpress.Utils.PointFloat(693F, 80F);
            this.ConsValAgua.Multiline = true;
            this.ConsValAgua.Name = "ConsValAgua";
            this.ConsValAgua.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsValAgua.SizeF = new System.Drawing.SizeF(79F, 25F);
            this.ConsValAgua.Text = "100.00";
            this.ConsValAgua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsValAgua.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.Degrade);
            // 
            // ConsLeitAgua
            // 
            this.ConsLeitAgua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsLeitAgua.CanShrink = true;
            this.ConsLeitAgua.Dpi = 254F;
            this.ConsLeitAgua.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsLeitAgua.LocationFloat = new DevExpress.Utils.PointFloat(524F, 80F);
            this.ConsLeitAgua.Multiline = true;
            this.ConsLeitAgua.Name = "ConsLeitAgua";
            this.ConsLeitAgua.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsLeitAgua.SizeF = new System.Drawing.SizeF(90F, 25F);
            this.ConsLeitAgua.Text = "100.0000";
            this.ConsLeitAgua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ConsKgGas
            // 
            this.ConsKgGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsKgGas.CanShrink = true;
            this.ConsKgGas.Dpi = 254F;
            this.ConsKgGas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsKgGas.LocationFloat = new DevExpress.Utils.PointFloat(279F, 80F);
            this.ConsKgGas.Multiline = true;
            this.ConsKgGas.Name = "ConsKgGas";
            this.ConsKgGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsKgGas.SizeF = new System.Drawing.SizeF(69F, 25F);
            this.ConsKgGas.Text = "100.00";
            this.ConsKgGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ConsValGas
            // 
            this.ConsValGas.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ConsValGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsValGas.CanShrink = true;
            this.ConsValGas.Dpi = 254F;
            this.ConsValGas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsValGas.LocationFloat = new DevExpress.Utils.PointFloat(349F, 80F);
            this.ConsValGas.Multiline = true;
            this.ConsValGas.Name = "ConsValGas";
            this.ConsValGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsValGas.SizeF = new System.Drawing.SizeF(79F, 25F);
            this.ConsValGas.Text = "1000,00\r\n1000,00\r\n300,00";
            this.ConsValGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsValGas.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.Degrade);
            // 
            // Consm3Gas
            // 
            this.Consm3Gas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Consm3Gas.CanShrink = true;
            this.Consm3Gas.Dpi = 254F;
            this.Consm3Gas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consm3Gas.LocationFloat = new DevExpress.Utils.PointFloat(186F, 80F);
            this.Consm3Gas.Multiline = true;
            this.Consm3Gas.Name = "Consm3Gas";
            this.Consm3Gas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Consm3Gas.SizeF = new System.Drawing.SizeF(92F, 25F);
            this.Consm3Gas.Text = "100.0000";
            this.Consm3Gas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel58
            // 
            this.xrLabel58.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel58.Dpi = 254F;
            this.xrLabel58.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(847F, 32F);
            this.xrLabel58.Text = "HIST�RICO DE CONSUMO";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel58.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // ConsMes
            // 
            this.ConsMes.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.ConsMes.Dpi = 254F;
            this.ConsMes.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsMes.LocationFloat = new DevExpress.Utils.PointFloat(0F, 80F);
            this.ConsMes.Multiline = true;
            this.ConsMes.Name = "ConsMes";
            this.ConsMes.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsMes.SizeF = new System.Drawing.SizeF(94F, 25F);
            this.ConsMes.Text = "*01/2000";
            this.ConsMes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ConsLeitGas
            // 
            this.ConsLeitGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsLeitGas.CanShrink = true;
            this.ConsLeitGas.Dpi = 254F;
            this.ConsLeitGas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsLeitGas.LocationFloat = new DevExpress.Utils.PointFloat(95F, 80F);
            this.ConsLeitGas.Multiline = true;
            this.ConsLeitGas.Name = "ConsLeitGas";
            this.ConsLeitGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsLeitGas.SizeF = new System.Drawing.SizeF(90F, 25F);
            this.ConsLeitGas.Text = "100.000";
            this.ConsLeitGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QuadroReceitas
            // 
            this.QuadroReceitas.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel3,
            this.xrLabel22,
            this.SubTotR,
            this.xrLabel54,
            this.TotR});
            this.QuadroReceitas.Dpi = 254F;
            this.QuadroReceitas.LocationFloat = new DevExpress.Utils.PointFloat(0F, 143F);
            this.QuadroReceitas.Name = "QuadroReceitas";
            this.QuadroReceitas.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroReceitas.SizeF = new System.Drawing.SizeF(868F, 269F);
            // 
            // xrPanel3
            // 
            this.xrPanel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel37,
            this.RecetaP,
            this.ReceitaMes,
            this.ReceitaA,
            this.ReceitaT,
            this.xrLabel12,
            this.xrLabel28,
            this.ReceitaNome});
            this.xrPanel3.Dpi = 254F;
            this.xrPanel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel3.Name = "xrPanel3";
            this.xrPanel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel3.SizeF = new System.Drawing.SizeF(863F, 164F);
            // 
            // xrPanel37
            // 
            this.xrPanel37.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLabel37});
            this.xrPanel37.Dpi = 254F;
            this.xrPanel37.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.xrPanel37.Name = "xrPanel37";
            this.xrPanel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel37.SizeF = new System.Drawing.SizeF(859F, 27F);
            // 
            // xrLabel40
            // 
            this.xrLabel40.Dpi = 254F;
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel40.ForeColor = System.Drawing.Color.Black;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(746F, 0F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(95F, 26F);
            this.xrLabel40.Text = "TOTAL";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Dpi = 254F;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel39.ForeColor = System.Drawing.Color.Black;
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(630F, 0F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(116F, 25F);
            this.xrLabel39.Text = "Antecipado";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel39.Visible = false;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.ForeColor = System.Drawing.Color.Black;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(508F, 0F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(111F, 25F);
            this.xrLabel38.Text = "Do per�odo";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel38.Visible = false;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Dpi = 254F;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel37.ForeColor = System.Drawing.Color.Black;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(402F, 0F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(95F, 25F);
            this.xrLabel37.Text = "Anterior";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel37.Visible = false;
            // 
            // RecetaP
            // 
            this.RecetaP.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RecetaP.Dpi = 254F;
            this.RecetaP.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RecetaP.LocationFloat = new DevExpress.Utils.PointFloat(730F, 64F);
            this.RecetaP.Multiline = true;
            this.RecetaP.Name = "RecetaP";
            this.RecetaP.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.RecetaP.SizeF = new System.Drawing.SizeF(10F, 24F);
            this.RecetaP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.RecetaP.Visible = false;
            this.RecetaP.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // ReceitaMes
            // 
            this.ReceitaMes.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaMes.Dpi = 254F;
            this.ReceitaMes.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaMes.LocationFloat = new DevExpress.Utils.PointFloat(714F, 64F);
            this.ReceitaMes.Multiline = true;
            this.ReceitaMes.Name = "ReceitaMes";
            this.ReceitaMes.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaMes.SizeF = new System.Drawing.SizeF(11F, 24F);
            this.ReceitaMes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaMes.Visible = false;
            this.ReceitaMes.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // ReceitaA
            // 
            this.ReceitaA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaA.Dpi = 254F;
            this.ReceitaA.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaA.LocationFloat = new DevExpress.Utils.PointFloat(698F, 64F);
            this.ReceitaA.Multiline = true;
            this.ReceitaA.Name = "ReceitaA";
            this.ReceitaA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaA.SizeF = new System.Drawing.SizeF(11F, 27F);
            this.ReceitaA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaA.Visible = false;
            this.ReceitaA.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // ReceitaT
            // 
            this.ReceitaT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaT.Dpi = 254F;
            this.ReceitaT.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaT.LocationFloat = new DevExpress.Utils.PointFloat(741F, 64F);
            this.ReceitaT.Multiline = true;
            this.ReceitaT.Name = "ReceitaT";
            this.ReceitaT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaT.SizeF = new System.Drawing.SizeF(116F, 89F);
            this.ReceitaT.Text = "1";
            this.ReceitaT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaT.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(858F, 37F);
            this.xrLabel12.Text = "RECEITAS";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel12.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // xrLabel28
            // 
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(572F, 122F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(134F, 27F);
            this.xrLabel28.Text = "Sub-Total";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ReceitaNome
            // 
            this.ReceitaNome.BackColor = System.Drawing.Color.Transparent;
            this.ReceitaNome.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaNome.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ReceitaNome.Dpi = 254F;
            this.ReceitaNome.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaNome.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64F);
            this.ReceitaNome.Multiline = true;
            this.ReceitaNome.Name = "ReceitaNome";
            this.ReceitaNome.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaNome.SizeF = new System.Drawing.SizeF(698F, 44F);
            this.ReceitaNome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.ReceitaNome.WordWrap = false;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(463F, 169F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(234F, 34F);
            this.xrLabel22.Text = "Rentabilidade";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel22.Visible = false;
            // 
            // SubTotR
            // 
            this.SubTotR.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SubTotR.Dpi = 254F;
            this.SubTotR.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubTotR.LocationFloat = new DevExpress.Utils.PointFloat(722F, 169F);
            this.SubTotR.Multiline = true;
            this.SubTotR.Name = "SubTotR";
            this.SubTotR.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.SubTotR.SizeF = new System.Drawing.SizeF(130F, 34F);
            this.SubTotR.Text = "1";
            this.SubTotR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.SubTotR.Visible = false;
            this.SubTotR.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // xrLabel54
            // 
            this.xrLabel54.Dpi = 254F;
            this.xrLabel54.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(511F, 212F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(185F, 42F);
            this.xrLabel54.Text = "TOTAL GERAL";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TotR
            // 
            this.TotR.Dpi = 254F;
            this.TotR.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotR.LocationFloat = new DevExpress.Utils.PointFloat(714F, 212F);
            this.TotR.Name = "TotR";
            this.TotR.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TotR.SizeF = new System.Drawing.SizeF(143F, 45F);
            this.TotR.Text = "TOTAL";
            this.TotR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QuadroSaldo
            // 
            this.QuadroSaldo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel38,
            this.SaldoFinal,
            this.SaldoCREDITO,
            this.SaldoDEBTO,
            this.SaldoA,
            this.xrLabel19,
            this.xrLabel18});
            this.QuadroSaldo.Dpi = 254F;
            this.QuadroSaldo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 423F);
            this.QuadroSaldo.Name = "QuadroSaldo";
            this.QuadroSaldo.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroSaldo.SizeF = new System.Drawing.SizeF(863F, 296F);
            // 
            // xrPanel38
            // 
            this.xrPanel38.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel51,
            this.xrLabel50,
            this.xrLabel49,
            this.xrLabel29});
            this.xrPanel38.Dpi = 254F;
            this.xrPanel38.LocationFloat = new DevExpress.Utils.PointFloat(5F, 32F);
            this.xrPanel38.Name = "xrPanel38";
            this.xrPanel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel38.SizeF = new System.Drawing.SizeF(847F, 26F);
            // 
            // xrLabel51
            // 
            this.xrLabel51.Dpi = 254F;
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel51.ForeColor = System.Drawing.Color.Black;
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(725F, 0F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(106F, 26F);
            this.xrLabel51.Text = "Saldo";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Dpi = 254F;
            this.xrLabel50.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel50.ForeColor = System.Drawing.Color.Black;
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(455F, 5F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(80F, 11F);
            this.xrLabel50.Text = "Cr�dito";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel50.Visible = false;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Dpi = 254F;
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel49.ForeColor = System.Drawing.Color.Black;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(386F, 5F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(53F, 11F);
            this.xrLabel49.Text = "D�bito";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel49.Visible = false;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel29.ForeColor = System.Drawing.Color.Black;
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(243F, 5F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(64F, 11F);
            this.xrLabel29.Text = "Anterior";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel29.Visible = false;
            // 
            // SaldoFinal
            // 
            this.SaldoFinal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SaldoFinal.Dpi = 254F;
            this.SaldoFinal.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaldoFinal.LocationFloat = new DevExpress.Utils.PointFloat(722F, 58F);
            this.SaldoFinal.Multiline = true;
            this.SaldoFinal.Name = "SaldoFinal";
            this.SaldoFinal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.SaldoFinal.SizeF = new System.Drawing.SizeF(130F, 233F);
            this.SaldoFinal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.SaldoFinal.WordWrap = false;
            this.SaldoFinal.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // SaldoCREDITO
            // 
            this.SaldoCREDITO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SaldoCREDITO.Dpi = 254F;
            this.SaldoCREDITO.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaldoCREDITO.LocationFloat = new DevExpress.Utils.PointFloat(698F, 58F);
            this.SaldoCREDITO.Multiline = true;
            this.SaldoCREDITO.Name = "SaldoCREDITO";
            this.SaldoCREDITO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.SaldoCREDITO.SizeF = new System.Drawing.SizeF(16F, 191F);
            this.SaldoCREDITO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.SaldoCREDITO.Visible = false;
            this.SaldoCREDITO.WordWrap = false;
            this.SaldoCREDITO.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // SaldoDEBTO
            // 
            this.SaldoDEBTO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SaldoDEBTO.Dpi = 254F;
            this.SaldoDEBTO.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaldoDEBTO.LocationFloat = new DevExpress.Utils.PointFloat(677F, 69F);
            this.SaldoDEBTO.Multiline = true;
            this.SaldoDEBTO.Name = "SaldoDEBTO";
            this.SaldoDEBTO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.SaldoDEBTO.SizeF = new System.Drawing.SizeF(16F, 185F);
            this.SaldoDEBTO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.SaldoDEBTO.Visible = false;
            this.SaldoDEBTO.WordWrap = false;
            this.SaldoDEBTO.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // SaldoA
            // 
            this.SaldoA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SaldoA.Dpi = 254F;
            this.SaldoA.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaldoA.LocationFloat = new DevExpress.Utils.PointFloat(661F, 64F);
            this.SaldoA.Multiline = true;
            this.SaldoA.Name = "SaldoA";
            this.SaldoA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.SaldoA.SizeF = new System.Drawing.SizeF(16F, 180F);
            this.SaldoA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.SaldoA.Visible = false;
            this.SaldoA.WordWrap = false;
            this.SaldoA.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // xrLabel19
            // 
            this.xrLabel19.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel19.BorderColor = System.Drawing.Color.Silver;
            this.xrLabel19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(5F, 58F);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(651F, 233F);
            this.xrLabel19.Text = " Saldo Anterior de Conta Corrente\r\n Saldo Atual de Conta Corrente\r\n Fundo de Rese" +
                "rva\r\n Poupan�a 13�\r\n Aplica��o de Curto Prazo\r\n Poupan�a Obras\r\n Pupan�a S.Festa" +
                "s/Churras.\r\n Aplica��o Financeira";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(847F, 32F);
            this.xrLabel18.Text = "SALDOS";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel18.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // QuadroComposicaoBoleto
            // 
            this.QuadroComposicaoBoleto.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Lista11,
            this.Lista12,
            this.xrLabel120,
            this.xrLabel116});
            this.QuadroComposicaoBoleto.Dpi = 254F;
            this.QuadroComposicaoBoleto.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.QuadroComposicaoBoleto.Name = "QuadroComposicaoBoleto";
            this.QuadroComposicaoBoleto.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroComposicaoBoleto.SizeF = new System.Drawing.SizeF(863F, 132F);
            // 
            // Lista11
            // 
            this.Lista11.BackColor = System.Drawing.Color.Transparent;
            this.Lista11.BorderColor = System.Drawing.Color.Silver;
            this.Lista11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Lista11.Dpi = 254F;
            this.Lista11.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lista11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Lista11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.Lista11.Multiline = true;
            this.Lista11.Name = "Lista11";
            this.Lista11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Lista11.SizeF = new System.Drawing.SizeF(730F, 59F);
            this.Lista11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Lista12
            // 
            this.Lista12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lista12.Dpi = 254F;
            this.Lista12.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lista12.LocationFloat = new DevExpress.Utils.PointFloat(730F, 37F);
            this.Lista12.Multiline = true;
            this.Lista12.Name = "Lista12";
            this.Lista12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Lista12.SizeF = new System.Drawing.SizeF(126F, 91F);
            this.Lista12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.Lista12.WordWrap = false;
            this.Lista12.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // xrLabel120
            // 
            this.xrLabel120.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel120.Dpi = 254F;
            this.xrLabel120.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel120.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(857F, 37F);
            this.xrLabel120.Text = "COMPOSI��O DO BOLETO";
            this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel120.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // xrLabel116
            // 
            this.xrLabel116.Dpi = 254F;
            this.xrLabel116.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(587F, 101F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(143F, 26F);
            this.xrLabel116.Text = "TOTAL";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QuadroPendenciasUnidade
            // 
            this.QuadroPendenciasUnidade.BorderColor = System.Drawing.Color.Silver;
            this.QuadroPendenciasUnidade.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.QuadroPendenciasUnidade.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel76,
            this.labRespBA,
            this.labValorBA,
            this.labVenctoBA,
            this.labCategoriaBA});
            this.QuadroPendenciasUnidade.Dpi = 254F;
            this.QuadroPendenciasUnidade.LocationFloat = new DevExpress.Utils.PointFloat(11F, 852F);
            this.QuadroPendenciasUnidade.Name = "QuadroPendenciasUnidade";
            this.QuadroPendenciasUnidade.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroPendenciasUnidade.SizeF = new System.Drawing.SizeF(857F, 68F);
            // 
            // xrLabel76
            // 
            this.xrLabel76.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel76.Dpi = 254F;
            this.xrLabel76.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel76.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(847F, 37F);
            this.xrLabel76.Text = "PEND�NCIAS DA UNIDADE";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel76.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // labRespBA
            // 
            this.labRespBA.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.labRespBA.Dpi = 254F;
            this.labRespBA.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labRespBA.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.labRespBA.Multiline = true;
            this.labRespBA.Name = "labRespBA";
            this.labRespBA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labRespBA.SizeF = new System.Drawing.SizeF(201F, 26F);
            this.labRespBA.Text = "PROPRIET�RIO";
            this.labRespBA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // labValorBA
            // 
            this.labValorBA.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labValorBA.CanShrink = true;
            this.labValorBA.Dpi = 254F;
            this.labValorBA.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labValorBA.LocationFloat = new DevExpress.Utils.PointFloat(661F, 37F);
            this.labValorBA.Multiline = true;
            this.labValorBA.Name = "labValorBA";
            this.labValorBA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labValorBA.SizeF = new System.Drawing.SizeF(185F, 26F);
            this.labValorBA.Text = "R$ 999.999.99";
            this.labValorBA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.labValorBA.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // labVenctoBA
            // 
            this.labVenctoBA.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labVenctoBA.CanShrink = true;
            this.labVenctoBA.Dpi = 254F;
            this.labVenctoBA.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labVenctoBA.LocationFloat = new DevExpress.Utils.PointFloat(429F, 37F);
            this.labVenctoBA.Multiline = true;
            this.labVenctoBA.Name = "labVenctoBA";
            this.labVenctoBA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labVenctoBA.SizeF = new System.Drawing.SizeF(185F, 26F);
            this.labVenctoBA.Text = "10/10/2000";
            this.labVenctoBA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // labCategoriaBA
            // 
            this.labCategoriaBA.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labCategoriaBA.CanShrink = true;
            this.labCategoriaBA.Dpi = 254F;
            this.labCategoriaBA.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labCategoriaBA.LocationFloat = new DevExpress.Utils.PointFloat(217F, 37F);
            this.labCategoriaBA.Multiline = true;
            this.labCategoriaBA.Name = "labCategoriaBA";
            this.labCategoriaBA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labCategoriaBA.SizeF = new System.Drawing.SizeF(211F, 27F);
            this.labCategoriaBA.Text = "CONDOM�NIO";
            this.labCategoriaBA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QuadroMensagem
            // 
            this.QuadroMensagem.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrMensagem,
            this.xrLabel118});
            this.QuadroMensagem.Dpi = 254F;
            this.QuadroMensagem.LocationFloat = new DevExpress.Utils.PointFloat(5F, 730F);
            this.QuadroMensagem.Name = "QuadroMensagem";
            this.QuadroMensagem.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroMensagem.SizeF = new System.Drawing.SizeF(863F, 106F);
            // 
            // xrMensagem
            // 
            this.xrMensagem.BackColor = System.Drawing.Color.Transparent;
            this.xrMensagem.BorderColor = System.Drawing.Color.Silver;
            this.xrMensagem.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrMensagem.Dpi = 254F;
            this.xrMensagem.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrMensagem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrMensagem.LocationFloat = new DevExpress.Utils.PointFloat(5F, 56F);
            this.xrMensagem.Multiline = true;
            this.xrMensagem.Name = "xrMensagem";
            this.xrMensagem.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrMensagem.SizeF = new System.Drawing.SizeF(847F, 50F);
            this.xrMensagem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrMensagem.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // xrLabel118
            // 
            this.xrLabel118.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel118.CanShrink = true;
            this.xrLabel118.Dpi = 254F;
            this.xrLabel118.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel118.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(847F, 53F);
            this.xrLabel118.Text = "MENSAGEM IMPORTANTE";
            this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel118.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // NamePago
            // 
            this.NamePago.Dpi = 254F;
            this.NamePago.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NamePago.ForeColor = System.Drawing.Color.Gainsboro;
            this.NamePago.LocationFloat = new DevExpress.Utils.PointFloat(16F, 2762F);
            this.NamePago.Name = "NamePago";
            this.NamePago.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.NamePago.SizeF = new System.Drawing.SizeF(1053F, 69F);
            this.NamePago.Text = "PAGO";
            this.NamePago.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.NamePago.Visible = false;
            // 
            // CodigoDeBarras
            // 
            this.CodigoDeBarras.Dpi = 254F;
            this.CodigoDeBarras.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2720F);
            this.CodigoDeBarras.Name = "CodigoDeBarras";
            this.CodigoDeBarras.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.CodigoDeBarras.SizeF = new System.Drawing.SizeF(1090F, 130F);
            this.CodigoDeBarras.Text = "CodigoDeBarras";
            this.CodigoDeBarras.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.CodigoDeBarras.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel115_Draw);
            // 
            // LinhaDigitavel
            // 
            this.LinhaDigitavel.Dpi = 254F;
            this.LinhaDigitavel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LinhaDigitavel.LocationFloat = new DevExpress.Utils.PointFloat(582F, 1995F);
            this.LinhaDigitavel.Name = "LinhaDigitavel";
            this.LinhaDigitavel.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LinhaDigitavel.SizeF = new System.Drawing.SizeF(1212F, 58F);
            this.LinhaDigitavel.Text = "23790.00000  00000.000000  18000.000000  0 31000000000000";
            this.LinhaDigitavel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Dpi = 254F;
            this.xrLabel33.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(1482F, 2719F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(304F, 35F);
            this.xrLabel33.Text = "Ficha de Compensa��o";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Dpi = 254F;
            this.xrLabel32.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(1101F, 2719F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(275F, 35F);
            this.xrLabel32.Text = "Autentica��o Mec�nica";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BANCONumero
            // 
            this.BANCONumero.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.BANCONumero.Dpi = 254F;
            this.BANCONumero.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BANCONumero.LocationFloat = new DevExpress.Utils.PointFloat(394F, 1974F);
            this.BANCONumero.Name = "BANCONumero";
            this.BANCONumero.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.BANCONumero.SizeF = new System.Drawing.SizeF(188F, 82F);
            this.BANCONumero.Text = "237-2";
            this.BANCONumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BANCONome
            // 
            this.BANCONome.Dpi = 254F;
            this.BANCONome.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BANCONome.LocationFloat = new DevExpress.Utils.PointFloat(5F, 1990F);
            this.BANCONome.Name = "BANCONome";
            this.BANCONome.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.BANCONome.SizeF = new System.Drawing.SizeF(381F, 55F);
            this.BANCONome.Text = "BRADESCO S/A";
            this.BANCONome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPanel5
            // 
            this.xrPanel5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrPanel5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.xrLabel4});
            this.xrPanel5.Dpi = 254F;
            this.xrPanel5.LocationFloat = new DevExpress.Utils.PointFloat(868F, 1873F);
            this.xrPanel5.Name = "xrPanel5";
            this.xrPanel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel5.SizeF = new System.Drawing.SizeF(931F, 74F);
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel5.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.BorderWidth = 1;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(651F, 5F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(238F, 42F);
            this.xrLabel5.Text = "Recibo do Sacado";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel4.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.BorderWidth = 1;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(19F, 5F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(267F, 27F);
            this.xrLabel4.Text = "Autentica��o Mec�nica";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(339F, 148F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // QuadroDespesas
            // 
            this.QuadroDespesas.CanGrow = false;
            this.QuadroDespesas.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroContinua,
            this.QuadroTotais,
            this.PDespADMA,
            this.PAdministradora,
            this.QuadroDesp,
            this.PDespG,
            this.PdespMO,
            this.PDespB,
            this.PDespH});
            this.QuadroDespesas.Dpi = 254F;
            this.QuadroDespesas.LocationFloat = new DevExpress.Utils.PointFloat(894F, 164F);
            this.QuadroDespesas.Name = "QuadroDespesas";
            this.QuadroDespesas.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroDespesas.SizeF = new System.Drawing.SizeF(903F, 1704F);
            this.QuadroDespesas.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroDespesas_Draw);
            // 
            // QuadroContinua
            // 
            this.QuadroContinua.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel31});
            this.QuadroContinua.Dpi = 254F;
            this.QuadroContinua.LocationFloat = new DevExpress.Utils.PointFloat(5F, 444F);
            this.QuadroContinua.Name = "QuadroContinua";
            this.QuadroContinua.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroContinua.SizeF = new System.Drawing.SizeF(254F, 32F);
            this.QuadroContinua.Visible = false;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(233F, 29F);
            this.xrLabel31.Text = "Continua no verso";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QuadroTotais
            // 
            this.QuadroTotais.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel26,
            this.xrTotalDesp});
            this.QuadroTotais.Dpi = 254F;
            this.QuadroTotais.LocationFloat = new DevExpress.Utils.PointFloat(365F, 444F);
            this.QuadroTotais.Name = "QuadroTotais";
            this.QuadroTotais.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroTotais.SizeF = new System.Drawing.SizeF(535F, 29F);
            this.QuadroTotais.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroTotais_Draw);
            // 
            // xrLabel26
            // 
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(156F, 0F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(185F, 29F);
            this.xrLabel26.Text = "TOTAL GERAL";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTotalDesp
            // 
            this.xrTotalDesp.Dpi = 254F;
            this.xrTotalDesp.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTotalDesp.LocationFloat = new DevExpress.Utils.PointFloat(347F, 0F);
            this.xrTotalDesp.Name = "xrTotalDesp";
            this.xrTotalDesp.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalDesp.SizeF = new System.Drawing.SizeF(187F, 29F);
            this.xrTotalDesp.Text = "TOTAL";
            this.xrTotalDesp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // PDespADMA
            // 
            this.PDespADMA.BorderColor = System.Drawing.Color.Silver;
            this.PDespADMA.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PDespADMA.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.DespesaValor2,
            this.DespesaNome2,
            this.xrLabel15,
            this.ADMpor,
            this.xrPanel39,
            this.xrLabel16});
            this.PDespADMA.Dpi = 254F;
            this.PDespADMA.LocationFloat = new DevExpress.Utils.PointFloat(0F, 169F);
            this.PDespADMA.Name = "PDespADMA";
            this.PDespADMA.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PDespADMA.SizeF = new System.Drawing.SizeF(900F, 64F);
            this.PDespADMA.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroTotais_Draw);
            // 
            // DespesaValor2
            // 
            this.DespesaValor2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValor2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValor2.Dpi = 254F;
            this.DespesaValor2.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValor2.LocationFloat = new DevExpress.Utils.PointFloat(762F, 26F);
            this.DespesaValor2.Multiline = true;
            this.DespesaValor2.Name = "DespesaValor2";
            this.DespesaValor2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValor2.SizeF = new System.Drawing.SizeF(138F, 37F);
            this.DespesaValor2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValor2.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // DespesaNome2
            // 
            this.DespesaNome2.BackColor = System.Drawing.Color.Transparent;
            this.DespesaNome2.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNome2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNome2.Dpi = 254F;
            this.DespesaNome2.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNome2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26F);
            this.DespesaNome2.Multiline = true;
            this.DespesaNome2.Name = "DespesaNome2";
            this.DespesaNome2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNome2.SizeF = new System.Drawing.SizeF(762F, 16F);
            this.DespesaNome2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.DespesaNome2.WordWrap = false;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(217F, 26F);
            this.xrLabel15.Text = "Administrativas";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ADMpor
            // 
            this.ADMpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ADMpor.Dpi = 254F;
            this.ADMpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ADMpor.LocationFloat = new DevExpress.Utils.PointFloat(262F, 3F);
            this.ADMpor.Name = "ADMpor";
            this.ADMpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ADMpor.SizeF = new System.Drawing.SizeF(93F, 21F);
            this.ADMpor.Text = "100%";
            this.ADMpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel39
            // 
            this.xrPanel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ADMLine,
            this.ADMLine1});
            this.xrPanel39.Dpi = 254F;
            this.xrPanel39.LocationFloat = new DevExpress.Utils.PointFloat(360F, 8F);
            this.xrPanel39.Name = "xrPanel39";
            this.xrPanel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel39.SizeF = new System.Drawing.SizeF(375F, 11F);
            // 
            // ADMLine
            // 
            this.ADMLine.Dpi = 254F;
            this.ADMLine.ForeColor = System.Drawing.Color.Silver;
            this.ADMLine.LineWidth = 10;
            this.ADMLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.ADMLine.Name = "ADMLine";
            this.ADMLine.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ADMLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // ADMLine1
            // 
            this.ADMLine1.Dpi = 254F;
            this.ADMLine1.ForeColor = System.Drawing.Color.LightGray;
            this.ADMLine1.LineWidth = 3;
            this.ADMLine1.LocationFloat = new DevExpress.Utils.PointFloat(132F, 3F);
            this.ADMLine1.Name = "ADMLine1";
            this.ADMLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ADMLine1.SizeF = new System.Drawing.SizeF(238F, 5F);
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(656F, 45F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel16.Text = "Sub Total";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // PAdministradora
            // 
            this.PAdministradora.BorderColor = System.Drawing.Color.Silver;
            this.PAdministradora.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PAdministradora.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.DespesaValorADM,
            this.DespesaNomeADM,
            this.xrLabel20,
            this.ADMApor,
            this.xrPanel40,
            this.xrLabel17});
            this.PAdministradora.Dpi = 254F;
            this.PAdministradora.LocationFloat = new DevExpress.Utils.PointFloat(0F, 238F);
            this.PAdministradora.Name = "PAdministradora";
            this.PAdministradora.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PAdministradora.SizeF = new System.Drawing.SizeF(900F, 69F);
            this.PAdministradora.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroTotais_Draw);
            // 
            // DespesaValorADM
            // 
            this.DespesaValorADM.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValorADM.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValorADM.Dpi = 254F;
            this.DespesaValorADM.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValorADM.LocationFloat = new DevExpress.Utils.PointFloat(762F, 26F);
            this.DespesaValorADM.Multiline = true;
            this.DespesaValorADM.Name = "DespesaValorADM";
            this.DespesaValorADM.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValorADM.SizeF = new System.Drawing.SizeF(138F, 37F);
            this.DespesaValorADM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValorADM.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // DespesaNomeADM
            // 
            this.DespesaNomeADM.BackColor = System.Drawing.Color.Transparent;
            this.DespesaNomeADM.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNomeADM.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNomeADM.Dpi = 254F;
            this.DespesaNomeADM.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNomeADM.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26F);
            this.DespesaNomeADM.Multiline = true;
            this.DespesaNomeADM.Name = "DespesaNomeADM";
            this.DespesaNomeADM.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNomeADM.SizeF = new System.Drawing.SizeF(762F, 20F);
            this.DespesaNomeADM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.DespesaNomeADM.WordWrap = false;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(222F, 26F);
            this.xrLabel20.Text = "Administradora";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ADMApor
            // 
            this.ADMApor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ADMApor.Dpi = 254F;
            this.ADMApor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ADMApor.LocationFloat = new DevExpress.Utils.PointFloat(262F, 3F);
            this.ADMApor.Name = "ADMApor";
            this.ADMApor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ADMApor.SizeF = new System.Drawing.SizeF(93F, 21F);
            this.ADMApor.Text = "100%";
            this.ADMApor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel40
            // 
            this.xrPanel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ADMALine,
            this.ADMALine1});
            this.xrPanel40.Dpi = 254F;
            this.xrPanel40.LocationFloat = new DevExpress.Utils.PointFloat(360F, 8F);
            this.xrPanel40.Name = "xrPanel40";
            this.xrPanel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel40.SizeF = new System.Drawing.SizeF(375F, 11F);
            // 
            // ADMALine
            // 
            this.ADMALine.Dpi = 254F;
            this.ADMALine.ForeColor = System.Drawing.Color.Silver;
            this.ADMALine.LineWidth = 10;
            this.ADMALine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.ADMALine.Name = "ADMALine";
            this.ADMALine.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ADMALine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // ADMALine1
            // 
            this.ADMALine1.Dpi = 254F;
            this.ADMALine1.ForeColor = System.Drawing.Color.LightGray;
            this.ADMALine1.LineWidth = 3;
            this.ADMALine1.LocationFloat = new DevExpress.Utils.PointFloat(132F, 3F);
            this.ADMALine1.Name = "ADMALine1";
            this.ADMALine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ADMALine1.SizeF = new System.Drawing.SizeF(222F, 6F);
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Dpi = 254F;
            this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(656F, 48F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel17.Text = "Sub Total";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QuadroDesp
            // 
            this.QuadroDesp.BackColor = System.Drawing.Color.DarkGray;
            this.QuadroDesp.Dpi = 254F;
            this.QuadroDesp.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuadroDesp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.QuadroDesp.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.QuadroDesp.Name = "QuadroDesp";
            this.QuadroDesp.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QuadroDesp.SizeF = new System.Drawing.SizeF(900F, 37F);
            this.QuadroDesp.Text = "DESPESAS";
            this.QuadroDesp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.QuadroDesp.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // PDespG
            // 
            this.PDespG.BorderColor = System.Drawing.Color.Silver;
            this.PDespG.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PDespG.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrPanel6,
            this.DGpor,
            this.xrLabel13,
            this.DespesaValor,
            this.DespesaNome});
            this.PDespG.Dpi = 254F;
            this.PDespG.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.PDespG.Name = "PDespG";
            this.PDespG.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PDespG.SizeF = new System.Drawing.SizeF(900F, 58F);
            this.PDespG.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroTotais_Draw);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(656F, 42F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel2.Text = "Sub Total";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel6
            // 
            this.xrPanel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.DGLine,
            this.DGLine1});
            this.xrPanel6.Dpi = 254F;
            this.xrPanel6.LocationFloat = new DevExpress.Utils.PointFloat(360F, 8F);
            this.xrPanel6.Name = "xrPanel6";
            this.xrPanel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel6.SizeF = new System.Drawing.SizeF(375F, 11F);
            // 
            // DGLine
            // 
            this.DGLine.Dpi = 254F;
            this.DGLine.ForeColor = System.Drawing.Color.Silver;
            this.DGLine.LineWidth = 10;
            this.DGLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.DGLine.Name = "DGLine";
            this.DGLine.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.DGLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // DGLine1
            // 
            this.DGLine1.Dpi = 254F;
            this.DGLine1.ForeColor = System.Drawing.Color.LightGray;
            this.DGLine1.LineWidth = 3;
            this.DGLine1.LocationFloat = new DevExpress.Utils.PointFloat(185F, 3F);
            this.DGLine1.Name = "DGLine1";
            this.DGLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.DGLine1.SizeF = new System.Drawing.SizeF(185F, 5F);
            // 
            // DGpor
            // 
            this.DGpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DGpor.Dpi = 254F;
            this.DGpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DGpor.LocationFloat = new DevExpress.Utils.PointFloat(262F, 5F);
            this.DGpor.Name = "DGpor";
            this.DGpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DGpor.SizeF = new System.Drawing.SizeF(93F, 21F);
            this.DGpor.Text = "100%";
            this.DGpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(95F, 26F);
            this.xrLabel13.Text = "Gerais";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DespesaValor
            // 
            this.DespesaValor.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValor.Dpi = 254F;
            this.DespesaValor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValor.LocationFloat = new DevExpress.Utils.PointFloat(762F, 26F);
            this.DespesaValor.Multiline = true;
            this.DespesaValor.Name = "DespesaValor";
            this.DespesaValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValor.SizeF = new System.Drawing.SizeF(138F, 32F);
            this.DespesaValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValor.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // DespesaNome
            // 
            this.DespesaNome.BackColor = System.Drawing.Color.Transparent;
            this.DespesaNome.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNome.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNome.Dpi = 254F;
            this.DespesaNome.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNome.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26F);
            this.DespesaNome.Multiline = true;
            this.DespesaNome.Name = "DespesaNome";
            this.DespesaNome.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNome.SizeF = new System.Drawing.SizeF(762F, 16F);
            this.DespesaNome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.DespesaNome.WordWrap = false;
            // 
            // PdespMO
            // 
            this.PdespMO.BorderColor = System.Drawing.Color.Silver;
            this.PdespMO.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PdespMO.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrPanel9,
            this.MOpor,
            this.DespesaValor1,
            this.xrLabel14,
            this.DespesaNome1});
            this.PdespMO.Dpi = 254F;
            this.PdespMO.LocationFloat = new DevExpress.Utils.PointFloat(0F, 101F);
            this.PdespMO.Name = "PdespMO";
            this.PdespMO.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PdespMO.SizeF = new System.Drawing.SizeF(900F, 61F);
            this.PdespMO.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroTotais_Draw);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(656F, 42F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel3.Text = "Sub Total";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel9
            // 
            this.xrPanel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.MOLine,
            this.MOLine1});
            this.xrPanel9.Dpi = 254F;
            this.xrPanel9.LocationFloat = new DevExpress.Utils.PointFloat(360F, 8F);
            this.xrPanel9.Name = "xrPanel9";
            this.xrPanel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel9.SizeF = new System.Drawing.SizeF(375F, 11F);
            // 
            // MOLine
            // 
            this.MOLine.Dpi = 254F;
            this.MOLine.ForeColor = System.Drawing.Color.Silver;
            this.MOLine.LineWidth = 10;
            this.MOLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.MOLine.Name = "MOLine";
            this.MOLine.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.MOLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // MOLine1
            // 
            this.MOLine1.Dpi = 254F;
            this.MOLine1.ForeColor = System.Drawing.Color.LightGray;
            this.MOLine1.LineWidth = 3;
            this.MOLine1.LocationFloat = new DevExpress.Utils.PointFloat(143F, 3F);
            this.MOLine1.Name = "MOLine1";
            this.MOLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.MOLine1.SizeF = new System.Drawing.SizeF(217F, 6F);
            // 
            // MOpor
            // 
            this.MOpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MOpor.Dpi = 254F;
            this.MOpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MOpor.LocationFloat = new DevExpress.Utils.PointFloat(262F, 3F);
            this.MOpor.Name = "MOpor";
            this.MOpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.MOpor.SizeF = new System.Drawing.SizeF(93F, 21F);
            this.MOpor.Text = "100%";
            this.MOpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DespesaValor1
            // 
            this.DespesaValor1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValor1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValor1.Dpi = 254F;
            this.DespesaValor1.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValor1.LocationFloat = new DevExpress.Utils.PointFloat(762F, 26F);
            this.DespesaValor1.Multiline = true;
            this.DespesaValor1.Name = "DespesaValor1";
            this.DespesaValor1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValor1.SizeF = new System.Drawing.SizeF(138F, 30F);
            this.DespesaValor1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValor1.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(212F, 26F);
            this.xrLabel14.Text = "M�o de Obra";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DespesaNome1
            // 
            this.DespesaNome1.BackColor = System.Drawing.Color.Transparent;
            this.DespesaNome1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.DespesaNome1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNome1.Dpi = 254F;
            this.DespesaNome1.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNome1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26F);
            this.DespesaNome1.Multiline = true;
            this.DespesaNome1.Name = "DespesaNome1";
            this.DespesaNome1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNome1.SizeF = new System.Drawing.SizeF(762F, 16F);
            this.DespesaNome1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.DespesaNome1.WordWrap = false;
            // 
            // PDespB
            // 
            this.PDespB.BorderColor = System.Drawing.Color.Silver;
            this.PDespB.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PDespB.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel21,
            this.xrPanel41,
            this.Bancpor,
            this.xrLabel52,
            this.DespesaNomeBANC,
            this.DespesaValorBANC});
            this.PDespB.Dpi = 254F;
            this.PDespB.LocationFloat = new DevExpress.Utils.PointFloat(0F, 312F);
            this.PDespB.Name = "PDespB";
            this.PDespB.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PDespB.SizeF = new System.Drawing.SizeF(900F, 58F);
            this.PDespB.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroTotais_Draw);
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(656F, 38F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel21.Text = "Sub Total";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel41
            // 
            this.xrPanel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.BancLine,
            this.BancLine1});
            this.xrPanel41.Dpi = 254F;
            this.xrPanel41.LocationFloat = new DevExpress.Utils.PointFloat(360F, 8F);
            this.xrPanel41.Name = "xrPanel41";
            this.xrPanel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel41.SizeF = new System.Drawing.SizeF(375F, 11F);
            // 
            // BancLine
            // 
            this.BancLine.Dpi = 254F;
            this.BancLine.ForeColor = System.Drawing.Color.Silver;
            this.BancLine.LineWidth = 10;
            this.BancLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.BancLine.Name = "BancLine";
            this.BancLine.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BancLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // BancLine1
            // 
            this.BancLine1.Dpi = 254F;
            this.BancLine1.ForeColor = System.Drawing.Color.LightGray;
            this.BancLine1.LineWidth = 3;
            this.BancLine1.LocationFloat = new DevExpress.Utils.PointFloat(138F, 3F);
            this.BancLine1.Name = "BancLine1";
            this.BancLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BancLine1.SizeF = new System.Drawing.SizeF(185F, 6F);
            // 
            // Bancpor
            // 
            this.Bancpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Bancpor.Dpi = 254F;
            this.Bancpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Bancpor.LocationFloat = new DevExpress.Utils.PointFloat(262F, 3F);
            this.Bancpor.Name = "Bancpor";
            this.Bancpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Bancpor.SizeF = new System.Drawing.SizeF(93F, 21F);
            this.Bancpor.Text = "100%";
            this.Bancpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.Dpi = 254F;
            this.xrLabel52.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(159F, 26F);
            this.xrLabel52.Text = "Banc�rias";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DespesaNomeBANC
            // 
            this.DespesaNomeBANC.BackColor = System.Drawing.Color.Transparent;
            this.DespesaNomeBANC.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNomeBANC.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNomeBANC.Dpi = 254F;
            this.DespesaNomeBANC.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNomeBANC.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26F);
            this.DespesaNomeBANC.Multiline = true;
            this.DespesaNomeBANC.Name = "DespesaNomeBANC";
            this.DespesaNomeBANC.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNomeBANC.SizeF = new System.Drawing.SizeF(762F, 11F);
            this.DespesaNomeBANC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.DespesaNomeBANC.WordWrap = false;
            // 
            // DespesaValorBANC
            // 
            this.DespesaValorBANC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValorBANC.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValorBANC.Dpi = 254F;
            this.DespesaValorBANC.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValorBANC.LocationFloat = new DevExpress.Utils.PointFloat(762F, 26F);
            this.DespesaValorBANC.Multiline = true;
            this.DespesaValorBANC.Name = "DespesaValorBANC";
            this.DespesaValorBANC.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValorBANC.SizeF = new System.Drawing.SizeF(138F, 27F);
            this.DespesaValorBANC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValorBANC.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // PDespH
            // 
            this.PDespH.BorderColor = System.Drawing.Color.Silver;
            this.PDespH.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PDespH.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.DespesaNomeADV,
            this.xrLabel23,
            this.xrPanel42,
            this.HORpor,
            this.xrLabel56,
            this.DespesaValorADV});
            this.PDespH.Dpi = 254F;
            this.PDespH.LocationFloat = new DevExpress.Utils.PointFloat(0F, 376F);
            this.PDespH.Name = "PDespH";
            this.PDespH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PDespH.SizeF = new System.Drawing.SizeF(900F, 61F);
            this.PDespH.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.QuadroTotais_Draw);
            // 
            // DespesaNomeADV
            // 
            this.DespesaNomeADV.BackColor = System.Drawing.Color.Transparent;
            this.DespesaNomeADV.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNomeADV.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNomeADV.Dpi = 254F;
            this.DespesaNomeADV.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNomeADV.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26F);
            this.DespesaNomeADV.Multiline = true;
            this.DespesaNomeADV.Name = "DespesaNomeADV";
            this.DespesaNomeADV.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNomeADV.SizeF = new System.Drawing.SizeF(751F, 16F);
            this.DespesaNomeADV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.DespesaNomeADV.WordWrap = false;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(656F, 42F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel23.Text = "Sub Total";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel42
            // 
            this.xrPanel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel42.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.HORLine,
            this.HORLine1});
            this.xrPanel42.Dpi = 254F;
            this.xrPanel42.LocationFloat = new DevExpress.Utils.PointFloat(364F, 8F);
            this.xrPanel42.Name = "xrPanel42";
            this.xrPanel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel42.SizeF = new System.Drawing.SizeF(386F, 11F);
            // 
            // HORLine
            // 
            this.HORLine.Dpi = 254F;
            this.HORLine.ForeColor = System.Drawing.Color.Silver;
            this.HORLine.LineWidth = 10;
            this.HORLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.HORLine.Name = "HORLine";
            this.HORLine.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.HORLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // HORLine1
            // 
            this.HORLine1.Dpi = 254F;
            this.HORLine1.ForeColor = System.Drawing.Color.LightGray;
            this.HORLine1.LineWidth = 3;
            this.HORLine1.LocationFloat = new DevExpress.Utils.PointFloat(106F, 3F);
            this.HORLine1.Name = "HORLine1";
            this.HORLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.HORLine1.SizeF = new System.Drawing.SizeF(185F, 5F);
            // 
            // HORpor
            // 
            this.HORpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.HORpor.Dpi = 254F;
            this.HORpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HORpor.LocationFloat = new DevExpress.Utils.PointFloat(262F, 3F);
            this.HORpor.Name = "HORpor";
            this.HORpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.HORpor.SizeF = new System.Drawing.SizeF(93F, 21F);
            this.HORpor.Text = "100%";
            this.HORpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel56.Dpi = 254F;
            this.xrLabel56.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(169F, 26F);
            this.xrLabel56.Text = "Honor�rios";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DespesaValorADV
            // 
            this.DespesaValorADV.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValorADV.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValorADV.Dpi = 254F;
            this.DespesaValorADV.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValorADV.LocationFloat = new DevExpress.Utils.PointFloat(762F, 26F);
            this.DespesaValorADV.Multiline = true;
            this.DespesaValorADV.Name = "DespesaValorADV";
            this.DespesaValorADV.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValorADV.SizeF = new System.Drawing.SizeF(138F, 32F);
            this.DespesaValorADV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValorADV.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel13_Draw);
            // 
            // xrLabel7
            // 
            this.xrLabel7.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(1503F, 0F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(294F, 53F);
            this.xrLabel7.Text = "VENCIMENTO";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel7.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel7_Draw);
            // 
            // xrLabel6
            // 
            this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yy}")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(1503F, 57F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(294F, 90F);
            this.xrLabel6.Text = "xrLabel6";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(1209F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(289F, 53F);
            this.xrLabel8.Text = "VALOR";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel8.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel8_Draw);
            // 
            // xrLabel9
            // 
            this.xrLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:#,##0.00}")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(1209F, 57F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(289F, 90F);
            this.xrLabel9.Text = "xrLabel9";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(894F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(307F, 53F);
            this.xrLabel10.Text = "COMPET�NCIA";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel10.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel8_Draw);
            // 
            // xrCompetencia
            // 
            this.xrCompetencia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrCompetencia.Dpi = 254F;
            this.xrCompetencia.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCompetencia.LocationFloat = new DevExpress.Utils.PointFloat(894F, 57F);
            this.xrCompetencia.Name = "xrCompetencia";
            this.xrCompetencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCompetencia.SizeF = new System.Drawing.SizeF(307F, 90F);
            this.xrCompetencia.Text = "00 0000";
            this.xrCompetencia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrPanel2
            // 
            this.xrPanel2.BorderColor = System.Drawing.Color.Silver;
            this.xrPanel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel2.CanGrow = false;
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel48,
            this.xrLabel47,
            this.NNSTR,
            this.xrLabel45,
            this.xrLabel44,
            this.xrLabel43,
            this.LabApt3,
            this.LabBlo3,
            this.LabApt4,
            this.LabBlo4,
            this.xrLabel1});
            this.xrPanel2.Dpi = 254F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(341F, 0F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel2.SizeF = new System.Drawing.SizeF(540F, 148F);
            // 
            // xrLabel48
            // 
            this.xrLabel48.CanGrow = false;
            this.xrLabel48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Calc_AgCedente")});
            this.xrLabel48.Dpi = 254F;
            this.xrLabel48.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(143F, 116F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(390F, 32F);
            this.xrLabel48.Text = "xrLabel48";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.CanGrow = false;
            this.xrLabel47.Dpi = 254F;
            this.xrLabel47.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(0F, 116F);
            this.xrLabel47.Multiline = true;
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(132F, 30F);
            this.xrLabel47.Text = "Ag/Cedente:";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NNSTR
            // 
            this.NNSTR.CanGrow = false;
            this.NNSTR.Dpi = 254F;
            this.NNSTR.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NNSTR.LocationFloat = new DevExpress.Utils.PointFloat(197F, 90F);
            this.NNSTR.Name = "NNSTR";
            this.NNSTR.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.NNSTR.SizeF = new System.Drawing.SizeF(336F, 26F);
            this.NNSTR.Text = "NNSTR";
            this.NNSTR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.CanGrow = false;
            this.xrLabel45.Dpi = 254F;
            this.xrLabel45.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(0F, 90F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(159F, 26F);
            this.xrLabel45.Text = "Nosso N�mero:";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLNome")});
            this.xrLabel44.Dpi = 254F;
            this.xrLabel44.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(81F, 61F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(449F, 26F);
            this.xrLabel44.Text = "xrLabel44";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.CanGrow = false;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(79F, 26F);
            this.xrLabel43.Text = "Nome:";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // LabApt3
            // 
            this.LabApt3.CanGrow = false;
            this.LabApt3.Dpi = 254F;
            this.LabApt3.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabApt3.LocationFloat = new DevExpress.Utils.PointFloat(188F, 37F);
            this.LabApt3.Name = "LabApt3";
            this.LabApt3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabApt3.SizeF = new System.Drawing.SizeF(79F, 21F);
            this.LabApt3.Text = "Apto:";
            this.LabApt3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // LabBlo3
            // 
            this.LabBlo3.CanGrow = false;
            this.LabBlo3.Dpi = 254F;
            this.LabBlo3.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabBlo3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.LabBlo3.Name = "LabBlo3";
            this.LabBlo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabBlo3.SizeF = new System.Drawing.SizeF(74F, 27F);
            this.LabBlo3.Text = "Bloco:";
            this.LabBlo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // LabApt4
            // 
            this.LabApt4.CanGrow = false;
            this.LabApt4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.LabApt4.Dpi = 254F;
            this.LabApt4.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabApt4.LocationFloat = new DevExpress.Utils.PointFloat(275F, 32F);
            this.LabApt4.Name = "LabApt4";
            this.LabApt4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabApt4.SizeF = new System.Drawing.SizeF(135F, 28F);
            this.LabApt4.Text = "LabApt4";
            this.LabApt4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // LabBlo4
            // 
            this.LabBlo4.CanGrow = false;
            this.LabBlo4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCodigo")});
            this.LabBlo4.Dpi = 254F;
            this.LabBlo4.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabBlo4.LocationFloat = new DevExpress.Utils.PointFloat(85F, 32F);
            this.LabBlo4.Name = "LabBlo4";
            this.LabBlo4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabBlo4.SizeF = new System.Drawing.SizeF(82F, 28F);
            this.LabBlo4.Text = "LabBlo4";
            this.LabBlo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.CanGrow = false;
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(537F, 28F);
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LineWidth = 5;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(3F, 1958F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(1791F, 13F);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "DadosBoleto";
            this.bindingSource1.DataSource = this.dImpBoletoBal;
            // 
            // xrLabel30
            // 
            this.xrLabel30.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel30.Dpi = 254F;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(505F, 706F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(318F, 58F);
            this.xrLabel30.Text = "D�bitos";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel30.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // xrLabel27
            // 
            this.xrLabel27.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel27.Dpi = 254F;
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(50F, 773F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(508F, 58F);
            this.xrLabel27.Text = "Hist�rico";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel27.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel12_Draw);
            // 
            // xrLabel83
            // 
            this.xrLabel83.CanGrow = false;
            this.xrLabel83.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Boleto.Codigo Ag�ncia")});
            this.xrLabel83.Dpi = 254F;
            this.xrLabel83.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(11F, 265F);
            this.xrLabel83.Multiline = true;
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel83.SizeF = new System.Drawing.SizeF(1047F, 232F);
            this.xrLabel83.Text = "xrLabel83";
            // 
            // xrLabel126
            // 
            this.xrLabel126.Dpi = 254F;
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(646F, 1704F);
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(254F, 58F);
            this.xrLabel126.Text = "xrLabel126";
            // 
            // dataTable1TableAdapter
            // 
            this.dataTable1TableAdapter.ClearBeforeFill = true;
            this.dataTable1TableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("dataTable1TableAdapter.GetNovosDados")));
            this.dataTable1TableAdapter.TabelaDataTable = null;
            // 
            // bOletoDetalheTableAdapter
            // 
            this.bOletoDetalheTableAdapter.ClearBeforeFill = true;
            this.bOletoDetalheTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("bOletoDetalheTableAdapter.GetNovosDados")));
            this.bOletoDetalheTableAdapter.TabelaDataTable = null;
            // 
            // eMPRESASTableAdapter
            // 
            this.eMPRESASTableAdapter.ClearBeforeFill = true;
            this.eMPRESASTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("eMPRESASTableAdapter.GetNovosDados")));
            this.eMPRESASTableAdapter.TabelaDataTable = null;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 254F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 254F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrLabel72
            // 
            this.xrLabel72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONCodigo")});
            this.xrLabel72.Dpi = 254F;
            this.xrLabel72.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(26.99997F, 778.0002F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(1026F, 58F);
            this.xrLabel72.StylePriority.UseFont = false;
            this.xrLabel72.Text = "xrLabel72";
            this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ImpBoletoBal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataAdapter = this.bOletoDetalheTableAdapter;
            this.DataSource = this.bindingSource1;
            this.Dpi = 254F;
            this.ExportOptions.PrintPreview.ShowOptionsBeforeExport = false;
            this.Margins = new System.Drawing.Printing.Margins(150, 150, 254, 254);
            this.PageHeight = 2969;
            this.PageWidth = 2101;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ImpBoletoBal_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel DespesaNome;
        private DevExpress.XtraReports.UI.XRLabel DespesaValor;
        private DevExpress.XtraReports.UI.XRLabel QuadroDesp;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrCompetencia;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel NNSTR;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel LabApt3;
        private DevExpress.XtraReports.UI.XRLabel LabBlo3;
        private DevExpress.XtraReports.UI.XRLabel LabApt4;
        private DevExpress.XtraReports.UI.XRLabel LabBlo4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel DespesaValor2;
        private DevExpress.XtraReports.UI.XRLabel DespesaValor1;
        private DevExpress.XtraReports.UI.XRLabel DespesaNome2;
        private DevExpress.XtraReports.UI.XRLabel DespesaNome1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrTotalDesp;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRPanel PDespG;
        private DevExpress.XtraReports.UI.XRPanel QuadroDespesas;
        private DevExpress.XtraReports.UI.XRPanel PDespADMA;
        private DevExpress.XtraReports.UI.XRPanel PdespMO;
        private DevExpress.XtraReports.UI.XRPanel PAdministradora;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel DespesaNomeADM;
        private DevExpress.XtraReports.UI.XRLabel DespesaValorADM;
        private DevExpress.XtraReports.UI.XRPanel PDespH;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel DespesaNomeADV;
        private DevExpress.XtraReports.UI.XRLabel DespesaValorADV;
        private DevExpress.XtraReports.UI.XRPanel PDespB;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel DespesaNomeBANC;
        private DevExpress.XtraReports.UI.XRLabel DespesaValorBANC;
        private DevExpress.XtraReports.UI.XRPanel xrPanel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRPanel xrPanel7;
        private DevExpress.XtraReports.UI.XRPanel xrPanel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRPanel xrPanel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRPanel xrPanel11;
        private DevExpress.XtraReports.UI.XRPanel xrPanel12;
        private DevExpress.XtraReports.UI.XRLabel xrAgencia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRPanel xrPanel19;
        private DevExpress.XtraReports.UI.XRLabel NNSTR2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRPanel xrPanel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRPanel xrPanel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRPanel xrPanel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRPanel xrPanel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRPanel xrPanel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRPanel xrPanel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRPanel xrPanel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRPanel xrPanel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRPanel xrPanel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRPanel xrPanel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel LinhaDigitavel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRPanel xrPanel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel85;
        private DevExpress.XtraReports.UI.XRPanel xrPanel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRPanel xrPanel31;
        private DevExpress.XtraReports.UI.XRPanel xrPanel30;
        private DevExpress.XtraReports.UI.XRPanel xrPanel29;
        private DevExpress.XtraReports.UI.XRPanel xrPanel28;
        private DevExpress.XtraReports.UI.XRPanel xrPanel27;
        private DevExpress.XtraReports.UI.XRPanel xrPanel21;
        private DevExpress.XtraReports.UI.XRPanel xrPanel35;
        private DevExpress.XtraReports.UI.XRPanel xrPanel34;
        private DevExpress.XtraReports.UI.XRPanel xrPanel33;
        private DevExpress.XtraReports.UI.XRPanel xrPanel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel97;
        private DevExpress.XtraReports.UI.XRLabel xrLabel99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel101;
        private DevExpress.XtraReports.UI.XRLabel xrLabel100;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel xrLabel107;
        private DevExpress.XtraReports.UI.XRLabel xrLabel106;
        private DevExpress.XtraReports.UI.XRLabel xrEspecie;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel109;
        private DevExpress.XtraReports.UI.XRLabel xrLabel112;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel xrLabel110;
        private DevExpress.XtraReports.UI.XRLabel xrLabel113;
        private DevExpress.XtraReports.UI.XRLabel xrCompetencia1;
        private DevExpress.XtraReports.UI.XRLabel CodigoDeBarras;
        private DevExpress.XtraReports.UI.XRPanel xrPanel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel123;
        private DevExpress.XtraReports.UI.XRLabel LabBlo2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel119;
        private DevExpress.XtraReports.UI.XRLabel LabBlo1;
        private DevExpress.XtraReports.UI.XRLabel xrProprietario;
        private DevExpress.XtraReports.UI.XRLabel xrLabel124;
        private DevExpress.XtraReports.UI.XRLabel xrLabel128;
        private DevExpress.XtraReports.UI.XRLabel xrLabel127;
        private DevExpress.XtraReports.UI.XRLabel xrLabel126;
        private DevExpress.XtraReports.UI.XRLabel xrLabel129;
        private DevExpress.XtraReports.UI.XRLabel xrLabel130;
        private DevExpress.XtraReports.UI.XRLabel xrLabel131;
        private DevExpress.XtraReports.UI.XRPanel QuadroTotais;
        private DevExpress.XtraReports.UI.XRPanel PDadosInternet;
        public DevExpress.XtraReports.UI.XRLabel BANCONome;
        public DevExpress.XtraReports.UI.XRLabel BANCONumero;
        public DevExpress.XtraReports.UI.XRLabel BANCOLocal;
        private DevExpress.XtraReports.UI.XRLabel DGpor;
        private DevExpress.XtraReports.UI.XRLabel MOpor;
        private DevExpress.XtraReports.UI.XRLabel ADMpor;
        private DevExpress.XtraReports.UI.XRLabel ADMApor;
        private DevExpress.XtraReports.UI.XRLabel Bancpor;
        private DevExpress.XtraReports.UI.XRLabel HORpor;
        private DevExpress.XtraReports.UI.XRPanel xrPanel6;
        private DevExpress.XtraReports.UI.XRLine DGLine;
        private DevExpress.XtraReports.UI.XRLine DGLine1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel9;
        private DevExpress.XtraReports.UI.XRLine MOLine;
        private DevExpress.XtraReports.UI.XRLine MOLine1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel39;
        private DevExpress.XtraReports.UI.XRLine ADMLine;
        private DevExpress.XtraReports.UI.XRLine ADMLine1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel40;
        private DevExpress.XtraReports.UI.XRLine ADMALine;
        private DevExpress.XtraReports.UI.XRLine ADMALine1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel41;
        private DevExpress.XtraReports.UI.XRLine BancLine;
        private DevExpress.XtraReports.UI.XRLine BancLine1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel42;
        private DevExpress.XtraReports.UI.XRLine HORLine;
        private DevExpress.XtraReports.UI.XRLine HORLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox6;
        public System.Windows.Forms.BindingSource bindingSource1;
        public System.Windows.Forms.BindingSource bindingSourceEmpresa;
        private DevExpress.XtraReports.UI.XRLabel NamePago;
        public DevExpress.XtraReports.UI.XRLabel xrLabelCarteira;
        public DevExpress.XtraReports.UI.XRLabel xrLabelUsoBanco;
        private DevExpress.XtraReports.UI.XRLabel xrLabel81;
        public DevExpress.XtraReports.UI.XRLabel xrLabelEspecie;
        private Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.DadosBoletoTableAdapter dataTable1TableAdapter;
        private Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        private DevExpress.XtraReports.UI.XRPanel xrPanel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel132;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox4;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox5;
        private DevExpress.XtraReports.UI.XRPanel QuadroEsquerda;
        private DevExpress.XtraReports.UI.XRPanel QuadroMensagem;
        private DevExpress.XtraReports.UI.XRLabel xrLabel118;
        private DevExpress.XtraReports.UI.XRLabel xrMensagem;
        private DevExpress.XtraReports.UI.XRPanel QuadroPendenciasUnidade;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel labRespBA;
        private DevExpress.XtraReports.UI.XRLabel labValorBA;
        private DevExpress.XtraReports.UI.XRLabel labVenctoBA;
        private DevExpress.XtraReports.UI.XRLabel labCategoriaBA;
        private DevExpress.XtraReports.UI.XRPanel QuadroReceitas;
        private DevExpress.XtraReports.UI.XRPanel xrPanel3;
        private DevExpress.XtraReports.UI.XRLabel ReceitaNome;
        private DevExpress.XtraReports.UI.XRLabel RecetaP;
        private DevExpress.XtraReports.UI.XRLabel ReceitaMes;
        private DevExpress.XtraReports.UI.XRLabel ReceitaA;
        private DevExpress.XtraReports.UI.XRLabel ReceitaT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel SubTotR;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel TotR;
        private DevExpress.XtraReports.UI.XRPanel QuadroComposicaoBoleto;
        private DevExpress.XtraReports.UI.XRLabel Lista11;
        private DevExpress.XtraReports.UI.XRLabel Lista12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        private DevExpress.XtraReports.UI.XRPanel QuadroSaldo;
        private DevExpress.XtraReports.UI.XRLabel SaldoFinal;
        private DevExpress.XtraReports.UI.XRLabel SaldoCREDITO;
        private DevExpress.XtraReports.UI.XRLabel SaldoDEBTO;
        private DevExpress.XtraReports.UI.XRLabel SaldoA;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.EMPRESASTableAdapter eMPRESASTableAdapter;
        private DevExpress.XtraReports.UI.XRPanel xrPanel37;
        private DevExpress.XtraReports.UI.XRPanel QuadroRemetente;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRPanel QuadroEndereco;
        private DevExpress.XtraReports.UI.XRPanel xrPanel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        public dImpBoletoBal dImpBoletoBal;
        private DevExpress.XtraReports.UI.XRPanel PainelSegunaPagina;
        private DevExpress.XtraReports.UI.XRPanel QuadroUnidadeInadimplentes;
        private DevExpress.XtraReports.UI.XRLabel TextoUnidadesInadimplentes;
        private DevExpress.XtraReports.UI.XRLabel TituloUnidadesInad;
        private DevExpress.XtraReports.UI.XRPanel QuadroContinua;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRPanel QuadroContinuacao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRPanel QuadroConsumo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel ConsMes;
        private DevExpress.XtraReports.UI.XRLabel ConsLeitGas;
        private DevExpress.XtraReports.UI.XRLabel ConsKgGas;
        private DevExpress.XtraReports.UI.XRLabel ConsValGas;
        private DevExpress.XtraReports.UI.XRLabel Consm3Gas;
        private DevExpress.XtraReports.UI.XRLabel ConsValAgua;
        private DevExpress.XtraReports.UI.XRLabel ConsLeitAgua;
        private DevExpress.XtraReports.UI.XRPanel SubQuadroGas;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel ConsPorcGas;
        private DevExpress.XtraReports.UI.XRLabel ConsPorcAgua;
        private DevExpress.XtraReports.UI.XRPanel SubQuadroAgua;
        private DevExpress.XtraReports.UI.XRLabel xrLabel135;
        private DevExpress.XtraReports.UI.XRLabel xrLabel137;
        private DevExpress.XtraReports.UI.XRLabel xrLabel138;
        private DevExpress.XtraReports.UI.XRLabel TituloGas;
        private DevExpress.XtraReports.UI.XRLabel Consm3Agua;
        private DevExpress.XtraReports.UI.XRLabel xrLabel134;
        private DevExpress.XtraReports.UI.XRPanel xrPanel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel139;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
    }
}
