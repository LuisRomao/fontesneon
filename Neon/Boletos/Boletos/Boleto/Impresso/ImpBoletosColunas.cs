using System;
using System.Drawing;
using DevExpress.XtraReports.UI;
using System.Collections;
using DevExpress.XtraCharts;

namespace Boletos.Boleto.Impresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpBoletosColunas : dllImpresso.ImpLogoCond
    {
        /// <summary>
        /// Constutor padrao nao usar
        /// </summary>
        public ImpBoletosColunas() 
        {
            InitializeComponent();
            
        }

       

        private ArrayList NovasColunas;

        private class NovaColuna
        {
            public string TituloColuna;
            public string TituloImpresso;
            public Type TipoDado;

            public NovaColuna(string _TituloColuna, string _TituloImpresso, Type _TipoDado)
            {
                TituloColuna = _TituloColuna;
                TituloImpresso = _TituloImpresso;
                TipoDado = _TipoDado;
            }
        }

        private void CriaColunaNoImpresso(string TituloColuna, string TituloImpresso, Type TipoDado)
        {
            NovasColunas.Add(new NovaColuna(TituloColuna,TituloImpresso,TipoDado));
        }

        /// <summary>
        /// 
        /// </summary>
        public int testacolunas = 0;

        private void CriaColunaNoImpressoEfetivo(bool ComLinhas)
        {
            
            XRLabel clone;
            XRLabel cloneT;
            XRLabel cloneTot;
            XRLabel cloneSubTot;

            
            
            int deltaAcumulado = 0;
            int larguraColunas = xrX0.Width;
            

            bool NumeroBol = (bool)NumeroBoleto.Value;

            int ColunasInc = 0;

            if (!NumeroBol)
            {
                xrNumeroBol.Visible = xrNumeroBolTit.Visible = false;
                xrResp.Location = new Point(xrResp.Location.X - larguraColunas, xrResp.Location.Y);
                xrRespTit.Location = new Point(xrRespTit.Location.X - larguraColunas, xrRespTit.Location.Y);
                xrVenc.Location = new Point(xrVenc.Location.X - larguraColunas, xrVenc.Location.Y);
                xrVencTit.Location = new Point(xrVencTit.Location.X - larguraColunas, xrVencTit.Location.Y);
                xrSubTotalTit.Location = new Point(xrSubTotalTit.Location.X - larguraColunas, xrSubTotalTit.Location.Y);
                xrSubTotalSum.Location = new Point(xrSubTotalSum.Location.X - larguraColunas, xrSubTotalSum.Location.Y);
                xrX0.Location = new Point(xrX0.Location.X - larguraColunas, xrX0.Location.Y);
                xrX0Tit.Location = new Point(xrX0Tit.Location.X - larguraColunas, xrX0Tit.Location.Y);
                xrX0SutTot.Location = new Point(xrX0SutTot.Location.X - larguraColunas, xrX0SutTot.Location.Y);
                xrX0Tot.Location = new Point(xrX0Tot.Location.X - larguraColunas, xrX0Tot.Location.Y);
                ColunasInc--;
                //deltaAcumulado -= larguraColunas;
            }

            int posicao0 = xrX0.Location.X;

            Font fonte = xrX0.Font;
            Font fonteT;
            XRLabel ModeloDetalhe = xrX0;
            XRLabel ModeloDetalheTot = xrX0Tot;
            XRLabel ModeloTitulo = xrX0Tit;
            XRLabel ModeloSubTot = xrX0SutTot;
            clone = ModeloDetalhe;
            cloneT = ModeloTitulo;
            cloneTot = ModeloDetalheTot;
            cloneSubTot = ModeloSubTot;
            if (testacolunas != 0)
            {
                while (NovasColunas.Count > testacolunas)
                    NovasColunas.RemoveAt(0);
                while (NovasColunas.Count < testacolunas)
                    NovasColunas.Add((NovaColuna)NovasColunas[0]);
            }

            ColunasInc += NovasColunas.Count;

            int W30 = 17;

            if (ColunasInc > 7)                        
                this.Landscape = true;


            if (ColunasInc > 20)
            {
                larguraColunas = larguraColunas - 70;
                fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                ModeloTitulo.Angle = 30;
                ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                ModeloTitulo.Band.Height += W30;
                Font f = ModeloTitulo.Font;
                ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
            }
            else if (ColunasInc > 17)
            {
                larguraColunas = larguraColunas - 55;
                fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                ModeloTitulo.Angle = 30;
                ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                ModeloTitulo.Band.Height += W30;
                Font f = ModeloTitulo.Font;
                ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
            }
            else if (ColunasInc > 16)
            {
                larguraColunas = larguraColunas - 40;
                fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                ModeloTitulo.Angle = 30;
                ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                ModeloTitulo.Band.Height += W30;
                Font f = ModeloTitulo.Font;
                ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
            }
            else
                if (ColunasInc > 14) 
                {
                    larguraColunas = larguraColunas - 30;
                    fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                    ModeloTitulo.Angle = 30;
                    ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                    ModeloTitulo.Band.Height += W30;
                    Font f = ModeloTitulo.Font;
                    ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
                }
                else
                    if (ColunasInc > 12)
                    {
                        larguraColunas = larguraColunas - 15;
                        fonte = new Font(fonte.FontFamily, fonte.Size - 2, FontStyle.Bold);
                        ModeloTitulo.Angle = 30;
                        ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                        ModeloTitulo.Band.Height += W30;
                        Font f = ModeloTitulo.Font;
                        ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
                    }

                    //else if (ColunasInc < 10)
                    //{
                    //    larguraColunas = larguraColunas + 15;                        
                    //}
            fonteT = new Font(fonte.FontFamily, fonte.Size - 1, FontStyle.Bold);
            foreach (NovaColuna NC in NovasColunas)
            {                
                string TituloColuna = NC.TituloColuna; 
                string TituloImpresso = NC.TituloImpresso; 
                Type TipoDado = NC.TipoDado;
                if (deltaAcumulado != 0)                
                {
                    clone = new XRLabel();
                    clone.TextAlignment = ModeloDetalhe.TextAlignment;
                    clone.Size = new Size(0, 0);
                    clone.CanGrow = ModeloDetalhe.CanGrow;
                    clone.WordWrap = ModeloDetalhe.WordWrap;
                    Bands[BandKind.Detail].Controls.Add(clone);                                                                                                            
                    Bands[BandKind.Detail].Height = 40;
                    cloneTot = new XRLabel();
                    //cloneTot.BackColor = Color.Red;
                    GroupFooter2.Controls.Add(cloneTot);
                    //Bands[BandKind.ReportFooter].Controls.Add(cloneTot);
                    cloneTot.TextAlignment = ModeloDetalheTot.TextAlignment;
                    
                    cloneTot.Summary.FormatString = ModeloDetalheTot.Summary.FormatString;
                    cloneTot.Summary.Func = ModeloDetalheTot.Summary.Func;
                    cloneTot.Summary.Running = ModeloDetalheTot.Summary.Running;

                    cloneSubTot = new XRLabel();
                    Bands[BandKind.GroupFooter].Controls.Add(cloneSubTot);
                    cloneSubTot.TextAlignment = ModeloSubTot.TextAlignment;
                    
                    cloneSubTot.Summary.FormatString = ModeloSubTot.Summary.FormatString;
                    cloneSubTot.Summary.Func = ModeloSubTot.Summary.Func;
                    cloneSubTot.Summary.Running = ModeloSubTot.Summary.Running;

                    cloneT = new XRLabel();
                    //Bands[BandKind.PageHeader].Controls.Add(cloneT);
                    ModeloTitulo.Band.Controls.Add(cloneT);
                    cloneT.TextAlignment = ModeloTitulo.TextAlignment;
                    cloneT.Angle = ModeloTitulo.Angle;
                    cloneT.Multiline = true;
                    cloneT.WordWrap = true;
                    cloneT.Font = ModeloTitulo.Font;

                }
                clone.Draw += Degrade;
                clone.Size = new Size(larguraColunas, ModeloDetalhe.Height);
                clone.Location = new Point(posicao0 + deltaAcumulado, ModeloDetalhe.Location.Y);
                clone.Font = fonte;

                cloneTot.Location = new Point(posicao0 + deltaAcumulado, ModeloDetalheTot.Location.Y);
                cloneTot.Size = new Size(larguraColunas, ModeloDetalheTot.Height);
                cloneTot.Font = fonteT;

                cloneSubTot.Location = new Point(posicao0 + deltaAcumulado, ModeloSubTot.Location.Y);
                cloneSubTot.Size = new Size(larguraColunas, ModeloSubTot.Height);
                cloneSubTot.Font = fonteT;

                cloneT.Location = new Point(posicao0 + deltaAcumulado, ModeloTitulo.Location.Y);
                cloneT.Size = new Size(larguraColunas, ModeloTitulo.Height);
                
                string Mascara = "{0:n2}";
                if (TipoDado == typeof(DateTime))
                    Mascara = "{0:dd/MM}";
                clone.DataBindings.Add("Text", bindingSource1, TituloColuna, Mascara);
                if (TipoDado == typeof(decimal))
                {
                    cloneTot.DataBindings.Add("Text", bindingSource1, TituloColuna, Mascara);
                    cloneSubTot.DataBindings.Add("Text", bindingSource1, TituloColuna, Mascara);
                }
                else
                    cloneSubTot.Visible = cloneTot.Visible = false;
                if (TituloImpresso == "")
                    TituloImpresso = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(TituloColuna).PLADescricaoRes;
                cloneT.Text = TituloImpresso;
                deltaAcumulado += larguraColunas;
            };
            xrtotal.Location = new Point(posicao0 + deltaAcumulado, ModeloDetalhe.Location.Y);
            //if ((bool)Marcadores.Value)
            //{
            //    xrTotalG.Location = new Point(posicao0 + deltaAcumulado, xrTotalG.Location.Y);
            //    xrmarcador.Location = new Point(xrTotalG.Location.X + xrTotalG.Width, xrmarcador.Location.Y);
            //}
            //else
            xrTotalG.Location = new Point(posicao0 + deltaAcumulado, xrTotalG.Location.Y);
            xrSubTotalG.Location = new Point(posicao0 + deltaAcumulado, xrSubTotalG.Location.Y);
            xrTotalTitulo.Location = new Point(posicao0 + deltaAcumulado, xrTotalTitulo.Location.Y);
            
            if (ComLinhas)
            {
                XRLine Novalinha = new XRLine();
                Novalinha.Size = new Size(10, 5);
                Novalinha.Width = 1;
                Bands[BandKind.Detail].Controls.Add(Novalinha);
                Novalinha.Location = new Point(0, 42);
                Bands[BandKind.Detail].Height = 42;
                Novalinha.Width = xrtotal.Location.X + xrtotal.Width;
            };
            xrPanel1.Width = xrtotal.Location.X + xrtotal.Width;
            xrAgrupa.Width = xrPanel1.Width;
            if (Tipo == TipoRelatorio.Emissao)
            {
                //GroupFooter1.Visible = GroupHeader1.Visible = false;
                GroupFooter1.Visible = xrPanel1.Visible = false;
            }
        }

        private void GravaNaColuna(dBoletosColunas.BoletosRow rowBOL,string TituloColuna,string TituloImpresso,decimal Valor)
        {            
            if (!dBoletosColunas1.Boletos.Columns.Contains(TituloColuna))
            {
                dBoletosColunas1.Boletos.Columns.Add(new System.Data.DataColumn(TituloColuna, typeof(decimal)));
                if(TituloImpresso == "")
                     CriaColunaNoImpresso(TituloColuna, "",typeof(decimal));                
            };
            if (rowBOL[TituloColuna] == DBNull.Value)
                rowBOL[TituloColuna] = Valor;
            else
                rowBOL[TituloColuna] = (decimal)rowBOL[TituloColuna] + Valor;
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TipoRelatorio 
        {
            /// <summary>
            /// 
            /// </summary>
            Emissao,
            /// <summary>
            /// 
            /// </summary>
            Recebimento,
            /// <summary>
            /// 
            /// </summary>
            DetalhamentoCredito
        }

        private TipoRelatorio Tipo;
        private bool preparado;
        private int CON;
        private DateTime DI;
        private DateTime DF;
        private bool ComLinhas;

        /// <summary>
        /// 
        /// </summary>
        public enum ClassePizza
        {
            /// <summary>
            /// 
            /// </summary>
            Prazo = 0,
            /// <summary>
            /// 
            /// </summary>
            Atrasado = 1,
            /// <summary>
            /// 
            /// </summary>
            Acordo = 2,
            /// <summary>
            /// 
            /// </summary>
            Juridico = 3,
            /// <summary>
            /// 
            /// </summary>
            Rentabilidade = 4,
            /// <summary>
            /// 
            /// </summary>
            Outros = 5,
            /// <summary>
            /// 
            /// </summary>
            NIdentificado = 6
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Classe"></param>
        /// <param name="Valor"></param>
        public void AcumulaPizza(ClassePizza Classe,decimal Valor)
        {
            if (dBoletosColunas1.Grafico.Count == 0)
            {
                //Pizza.Value = true;
                dBoletosColunas1.Grafico.AddGraficoRow("Boletos no prazo", 0);
                dBoletosColunas1.Grafico.AddGraficoRow("Boletos atrasados", 0);
                dBoletosColunas1.Grafico.AddGraficoRow("Acordos Neon", 0);
                dBoletosColunas1.Grafico.AddGraficoRow("Acordos jur�dico", 0);
                //dBoletosColunas1.Grafico.AddGraficoRow("Cobran�a judicial", 0);
                dBoletosColunas1.Grafico.AddGraficoRow("Rentabilidade", 0);
                dBoletosColunas1.Grafico.AddGraficoRow("Outros", 0);
                dBoletosColunas1.Grafico.AddGraficoRow("N�o identificado", 0);
            }
            dBoletosColunas1.Grafico[(int)Classe].Valor += Valor;
        }

        /// <summary>
        /// 
        /// </summary>
        public enum Tratadados 
        {
            /// <summary>
            /// 
            /// </summary>
            limpar,
            /// <summary>
            /// 
            /// </summary>
            completar
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TrDados"></param>
        public void Prepara(Tratadados TrDados)
        {
            /*
            if (!pizza)
            {
                ReportFooter.HeightF = 48;
                
            }
            else
            {
                if (TrDados == Tratadados.limpar)
                    dBoletosColunas1.Grafico.Clear();
            }*/
            
            preparado = true;
            NovasColunas = new ArrayList();
            SetNome(FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(CON).CONNome);


            bool TemMulta = false;
            bool TemDesconto = false;

            switch (Tipo)
            {
                case TipoRelatorio.Emissao:
                    xrTitulo.Text = string.Format("Boletos emitidos de {0:dd/MM/yy} a {1:dd/MM/yy} ", DI, DF);
                    dBoletosColunas.BoletosTableAdapter.FillByEimssao(dBoletosColunas1.Boletos, CON, DI, DF);
                    dBoletosColunas1.BoletosColunasTableAdapter.FillByEmissao(dBoletosColunas1.BoletosColunas, CON, DI, DF);
                    xrtotal.DataBindings.Add("Text", bindingSource1, "BOLValorPrevisto", "{0:n2}");
                    xrTotalG.DataBindings.Add("Text", bindingSource1, "BOLValorPrevisto", "{0:n2}");
                    break;
                case TipoRelatorio.Recebimento:
                case TipoRelatorio.DetalhamentoCredito:
                    if (Tipo != TipoRelatorio.Emissao)
                        xrTitulo.Text = string.Format("Boletos recebidos de {0:dd/MM/yy} a {1:dd/MM/yy} ", DI, DF);
                    else
                        xrTitulo.Text = string.Format("Detalhamento de cr�ditos {0:dd/MM/yy} a {1:dd/MM/yy} ", DI, DF);
                    try
                    {
                        if (TrDados == Tratadados.completar)
                        {
                            dBoletosColunas.BoletosTableAdapter.ClearBeforeFill = false;
                            dBoletosColunas1.BoletosColunasTableAdapter.ClearBeforeFill = false;
                        }
                        dBoletosColunas.BoletosTableAdapter.FillBy(dBoletosColunas1.Boletos, DI, DF, CON);
                        dBoletosColunas.DadosCompTableAdapter.Fill(dBoletosColunas1.DadosComp, CON);
                        foreach (dBoletosColunas.BoletosRow rowBol in dBoletosColunas1.Boletos)
                        {
                            if (!rowBol.IsBOL_APTNull())
                            {
                                dBoletosColunas.DadosCompRow rowDComp = dBoletosColunas1.DadosComp.FindByAPT(rowBol.BOL_APT);
                                if (rowDComp != null)
                                {
                                    rowBol.BLOCodigo = rowDComp.BLOCodigo;
                                    rowBol.APTNumero = rowDComp.APTNumero;
                                }
                            }
                        };
                        dBoletosColunas1.BoletosColunasTableAdapter.Fill(dBoletosColunas1.BoletosColunas, DI, DF, CON);
                    }
                    finally
                    {
                        dBoletosColunas.BoletosTableAdapter.ClearBeforeFill = true;
                        dBoletosColunas1.BoletosColunasTableAdapter.ClearBeforeFill = true;
                    }
                    CriaColunaNoImpresso("BOLPagamento", "Pagamento", typeof(DateTime));
                    xrtotal.DataBindings.Add("Text", bindingSource1, "BOLValorPago", "{0:n2}");
                    xrTotalG.DataBindings.Add("Text", bindingSource1, "BOLValorPago", "{0:n2}");
                    break;                                                        
            }
        

            foreach (dBoletosColunas.BoletosRow rowBOL in dBoletosColunas1.Boletos)
            {
                
                decimal MultaDesconto = 0;
                if (!rowBOL.IsBOLValorPagoNull())
                    MultaDesconto = rowBOL.BOLValorPago;
                foreach (dBoletosColunas.BoletosColunasRow rowDet in rowBOL.GetBoletosColunasRows())
                {
                    GravaNaColuna(rowBOL, rowDet.BOD_PLA, "", rowDet.BODValor);
                    MultaDesconto -= rowDet.BODValor;
                };

                if (Tipo != TipoRelatorio.Emissao)
                {
                    /*
                    if (pizza)
                    {
                        if (rowBOL.BOL < 0)
                        {
                            foreach (dBoletosColunas.BoletosColunasRow BolCol in rowBOL.GetBoletosColunasRows())
                            {
                                if (BolCol.BOD_PLA == "910002")
                                    AcumulaPizza(ClassePizza.NIdentificado, rowBOL.BOLValorPago);
                                else if (BolCol.BOD_PLA == "170002")
                                    AcumulaPizza(ClassePizza.Rentabilidade, rowBOL.BOLValorPago);
                                else
                                    AcumulaPizza(ClassePizza.Outros, rowBOL.BOLValorPago);
                            }
                        }
                        else
                        {
                            
                            //if (!commulta)//(rowBOL.BOLPagamento <= rowBOL.BOLVencto)
                            if (rowBOL.BOLValorPrevisto == rowBOL.BOLValorPago)
                                AcumulaPizza(ClassePizza.Prazo, rowBOL.BOLValorPago);
                            else
                            {
                                Boleto BOLe = new Boleto(rowBOL.BOL);
                                Acordo.Acordo Acordo = new Boletos.Acordo.Acordo(BOLe, true);
                                if (Acordo.Gerado)
                                {
                                    if (Acordo.Judicial || Acordo.Juridico) 
                                        //AcumulaPizza(ClassePizza.Judicial, rowBOL.BOLValorPago);
                                    //else if (Acordo.Juridico)
                                        AcumulaPizza(ClassePizza.Juridico, rowBOL.BOLValorPago);
                                    else
                                        AcumulaPizza(ClassePizza.Acordo, rowBOL.BOLValorPago);
                                }
                                else
                                    AcumulaPizza(ClassePizza.Atrasado, rowBOL.BOLValorPago);
                            }
                        }
                    };
                    */
                    if (MultaDesconto > 0)
                    {
                        GravaNaColuna(rowBOL, "MULTA", "Multa por atraso", MultaDesconto);
                        TemMulta = true;
                    }
                    else if (MultaDesconto < 0)
                    {
                        GravaNaColuna(rowBOL, "DESCONTO", "Desconto", MultaDesconto);
                        TemDesconto = true;
                    }
                };
                if (rowBOL.IsBLOCodigoNull() || (rowBOL.BLOCodigo == "-"))
                {
                    rowBOL.BLOAPT = "";
                    rowBOL.PI = "";
                }
                else
                {
                    rowBOL.BLOAPT = rowBOL.BLOCodigo == "SB" ? "" : rowBOL.BLOCodigo + " ";
                    rowBOL.BLOAPT += rowBOL.APTNumero;
                    rowBOL.PI = rowBOL.BOLProprietario ? "P" : "I";
                }
                if (Tipo != TipoRelatorio.Emissao)
                {
                    if (!rowBOL.IsAgrupamentoNull())
                        continue;
                    if (rowBOL.BOLVencto < DI)
                    {
                        rowBOL.Agrupamento = string.Format("Atrasados (Vencidos antes de {0:dd/MM/yyyy})", DI);
                        rowBOL.Ordem = 1;
                    }
                    else
                        if (rowBOL.BOLVencto > DF)
                        {
                            rowBOL.Agrupamento = string.Format("Adiantados (Vencidos ap�s de {0:dd/MM/yyyy})", DF);
                            rowBOL.Ordem = 3;
                        }
                        else
                        {
                            rowBOL.Agrupamento = string.Format("No per�odo ({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", DI, DF);
                            rowBOL.Ordem = 2;
                        }
                }
                else
                {
                    rowBOL.Ordem = 0;
                }
            }
            bindingSource1.Sort = "Ordem,BLOCodigo,APTNumero";
            if (Tipo == TipoRelatorio.Recebimento)
            {
                if (TemDesconto)
                    CriaColunaNoImpresso("DESCONTO", "Desconto", typeof(decimal));
                if (TemMulta)
                    CriaColunaNoImpresso("MULTA", "Multa por atraso", typeof(decimal));
            }


            CriaColunaNoImpressoEfetivo(ComLinhas);
            /*
            if (pizza)
            {
                System.Collections.Generic.List<dBoletosColunas.GraficoRow> apagar = new System.Collections.Generic.List<dBoletosColunas.GraficoRow>();
                foreach (dBoletosColunas.GraficoRow grrow in dBoletosColunas1.Grafico)
                    if (grrow.Valor == 0)
                        apagar.Add(grrow);
                foreach (dBoletosColunas.GraficoRow grrow in apagar)
                    grrow.Delete();
                dBoletosColunas1.Grafico.AcceptChanges();
            }*/
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        /// <param name="Tipo"></param>
        /// <param name="ComNumeroBol"></param>
        /// <param name="ComLinhas"></param>
        /// <param name="teste"></param>
        public ImpBoletosColunas(int CON, DateTime DI, DateTime DF, TipoRelatorio Tipo, bool ComNumeroBol, bool ComLinhas, int teste)
        {
            ConstrutorComplemento(CON, DI, DF, Tipo, ComLinhas, ComNumeroBol,teste);            
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        /// <param name="Tipo"></param>
        /// <param name="ComNumeroBol"></param>
        /// <param name="ComLinhas"></param>
        public ImpBoletosColunas(int CON, DateTime DI, DateTime DF, TipoRelatorio Tipo,bool ComNumeroBol, bool ComLinhas)
        {
            ConstrutorComplemento(CON, DI, DF, Tipo, ComLinhas, ComNumeroBol,0);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        /// <param name="Tipo"></param>
        /// <param name="ComLinhas"></param>
        public ImpBoletosColunas(int CON, DateTime DI, DateTime DF, TipoRelatorio Tipo, bool ComLinhas)
        {
            ConstrutorComplemento(CON, DI, DF, Tipo, ComLinhas, true, 0);
        }

        private void ConstrutorComplemento(int CON, DateTime DI, DateTime DF, TipoRelatorio Tipo, bool ComLinhas,bool ComNumeroBol, int teste)
        {
            InitializeComponent();
            //((PieSeriesLabel)xrChart1.Series[0].Label).Position = PieSeriesLabelPosition.TwoColumns;           
            testacolunas = teste;
            this.CON = CON;
            this.DI = DI;
            this.DF = DF;
            this.ComLinhas = ComLinhas;
            this.Tipo = Tipo;
            NumeroBoleto.Value = ComNumeroBol;
        }

        private void ImpBoletosColunas_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (!preparado)
                Prepara(Tratadados.limpar);
            
        }

        private void xrTotalG_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }
    }
}
