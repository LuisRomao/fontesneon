using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Boletos.Boleto.CadCheques;

namespace Boletos.Boleto.CadCheques
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cCadCheques : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public cCadCheques()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            gridView1.CloseEditor();
            return (gridView1.UpdateCurrentRow());
        }
    }
}

