﻿/*
MR - 25/02/2016 11:00           - Inclusão do campo de boleto com registro (CONBoletoComRegistro) no DadosCONDOMINIOSBOLTableAdapter
*/

namespace Boletos.Boleto {

    /// <summary>
    /// dBoletos
    /// </summary>
    partial class dBoletos
    {
        internal System.Collections.Generic.SortedList<int,AbstratosNeon.ABS_Apartamento> Apartamentos;

        /// <summary>
        /// Carrega apartamentos no dataset para agilizar
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="Forcar">Forca a recarga</param>
        /// <returns></returns>
        public int CarregaAPTs(int CON,bool Forcar = false)
        {
            if ((Apartamentos == null) || Forcar)
                Apartamentos = AbstratosNeon.ABS_Apartamento.ABSGetApartamentos(CON);
            return Apartamentos.Count;
        }

        private static dBoletos _dBoletosSt;

        /// <summary>
        /// dataset estático:dBoletos
        /// </summary>
        public static dBoletos dBoletosSt
        {
            get
            {
                if (_dBoletosSt == null)
                    _dBoletosSt = new dBoletos();
                return _dBoletosSt;
            }
        }        

        private dBoletosTableAdapters.ExtraPendenteTableAdapter extraPendenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ExtraPendente
        /// </summary>
        public dBoletosTableAdapters.ExtraPendenteTableAdapter ExtraPendenteTableAdapter
        {
            get
            {
                if (extraPendenteTableAdapter == null)
                {
                    extraPendenteTableAdapter = new dBoletosTableAdapters.ExtraPendenteTableAdapter();
                    extraPendenteTableAdapter.TrocarStringDeConexao();
                };
                return extraPendenteTableAdapter;
            }
        }

        private dBoletosTableAdapters.BODxBODTableAdapter bODxBODTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BODxBOD
        /// </summary>
        public dBoletosTableAdapters.BODxBODTableAdapter BODxBODTableAdapter
        {
            get
            {
                if (bODxBODTableAdapter == null)
                {
                    bODxBODTableAdapter = new dBoletosTableAdapters.BODxBODTableAdapter();
                    bODxBODTableAdapter.TrocarStringDeConexao();
                };
                return bODxBODTableAdapter;
            }
        }

        private dBoletosTableAdapters.BODxPAGTableAdapter bODxPAGTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BODxPAG
        /// </summary>
        public dBoletosTableAdapters.BODxPAGTableAdapter BODxPAGTableAdapter
        {
            get
            {
                if (bODxPAGTableAdapter == null)
                {
                    bODxPAGTableAdapter = new dBoletosTableAdapters.BODxPAGTableAdapter();
                    bODxPAGTableAdapter.TrocarStringDeConexao();
                };
                return bODxPAGTableAdapter;
            }
        }

        private dBoletosTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dBoletosTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dBoletosTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao();
                };
                return conTasLogicasTableAdapter;
            }
        }

        private dBoletosTableAdapters.CTLxCCTTableAdapter cTLxCCTTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CTLxCCT
        /// </summary>
        public dBoletosTableAdapters.CTLxCCTTableAdapter CTLxCCTTableAdapter
        {
            get
            {
                if (cTLxCCTTableAdapter == null)
                {
                    cTLxCCTTableAdapter = new dBoletosTableAdapters.CTLxCCTTableAdapter();
                    cTLxCCTTableAdapter.TrocarStringDeConexao();
                };
                return cTLxCCTTableAdapter;
            }
        }
    
        private dBoletosTableAdapters.DescontoAPartamentoTableAdapter descontoAPartamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DescontoAPartamento
        /// </summary>
        public dBoletosTableAdapters.DescontoAPartamentoTableAdapter DescontoAPartamentoTableAdapter
        {
            get
            {
                if (descontoAPartamentoTableAdapter == null)
                {
                    descontoAPartamentoTableAdapter = new dBoletosTableAdapters.DescontoAPartamentoTableAdapter();
                    descontoAPartamentoTableAdapter.TrocarStringDeConexao();
                };
                return descontoAPartamentoTableAdapter;
            }
        }

        private dBoletosTableAdapters.BoletoDEscontoTableAdapter boletoDEscontoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BoletoDEsconto
        /// </summary>
        public dBoletosTableAdapters.BoletoDEscontoTableAdapter BoletoDEscontoTableAdapter
        {
            get
            {
                if (boletoDEscontoTableAdapter == null)
                {
                    boletoDEscontoTableAdapter = new dBoletosTableAdapters.BoletoDEscontoTableAdapter();
                    boletoDEscontoTableAdapter.TrocarStringDeConexao();
                };
                return boletoDEscontoTableAdapter;
            }
        }

        private dBoletosTableAdapters.SuperBOletoTableAdapter superBOletoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SuperBOleto
        /// </summary>
        public dBoletosTableAdapters.SuperBOletoTableAdapter SuperBOletoTableAdapter
        {
            get
            {
                if (superBOletoTableAdapter == null)
                {
                    superBOletoTableAdapter = new dBoletosTableAdapters.SuperBOletoTableAdapter();
                    superBOletoTableAdapter.TrocarStringDeConexao();
                };
                return superBOletoTableAdapter;
            }
        }

        private dBoletosTableAdapters.SuperBoletoDetalheTableAdapter superBoletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SuperBoletoDetalhe
        /// </summary>
        public dBoletosTableAdapters.SuperBoletoDetalheTableAdapter SuperBoletoDetalheTableAdapter
        {
            get
            {
                if (superBoletoDetalheTableAdapter == null)
                {
                    superBoletoDetalheTableAdapter = new dBoletosTableAdapters.SuperBoletoDetalheTableAdapter();
                    superBoletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return superBoletoDetalheTableAdapter;
            }
        }



        private static dBoletosTableAdapters.BOLetosTableAdapter stBOLetosTableAdapter;
        private static dBoletosTableAdapters.BOletoDetalheTableAdapter stBOletoDetalheTableAdapter;



        private dBoletosTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        //private dBoletosTableAdapters.BoletoAccessTableAdapter boletoAccessTableAdapter;
        private dBoletosTableAdapters.UsuarioSenhaTableAdapter usuarioSenhaTableAdapter;

        /// <summary>
        /// ??
        /// </summary>
        public dBoletosTableAdapters.UsuarioSenhaTableAdapter UsuarioSenhaTableAdapter {
            get {
                if (usuarioSenhaTableAdapter == null) {
                    usuarioSenhaTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.UsuarioSenhaTableAdapter();
                    usuarioSenhaTableAdapter.TrocarStringDeConexao();
                };
                return usuarioSenhaTableAdapter;
            }
        }

        /*
        public dBoletosTableAdapters.BoletoAccessTableAdapter BoletoAccessTableAdapter
        {
            get
            {
                if (boletoAccessTableAdapter == null) {
                    boletoAccessTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.BoletoAccessTableAdapter();
                    boletoAccessTableAdapter.TrocarStringDeConexao();
                };
                return boletoAccessTableAdapter;
            }
        }*/
        
        /// <summary>
        /// CONDOMINIOSTableAdapter
        /// </summary>
        public dBoletosTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter{
            get { 
               if(cONDOMINIOSTableAdapter == null){
                   cONDOMINIOSTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.CONDOMINIOSTableAdapter();
                   cONDOMINIOSTableAdapter.TrocarStringDeConexao();
               };
               return cONDOMINIOSTableAdapter;
            }
        }

        /// <summary>
        /// STBOLetosTableAdapter
        /// </summary>
        public static dBoletosTableAdapters.BOLetosTableAdapter STBOLetosTableAdapter
        {
            get
            {
                if (stBOLetosTableAdapter == null)
                {
                    stBOLetosTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.BOLetosTableAdapter();
                    stBOLetosTableAdapter.TrocarStringDeConexao(Boleto.TipoPadrao);
                };
                return stBOLetosTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static dBoletosTableAdapters.BOletoDetalheTableAdapter STBOletoDetalheTableAdapter
        {
            get
            {
                if (stBOletoDetalheTableAdapter == null)
                {
                    stBOletoDetalheTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.BOletoDetalheTableAdapter();
                    stBOletoDetalheTableAdapter.TrocarStringDeConexao(Boleto.TipoPadrao);
                };
                return stBOletoDetalheTableAdapter;
            }
        }

        private dBoletosTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;
        private dBoletosTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        private dBoletosTableAdapters.DadosAPTTableAdapter dadosAPTTableAdapter;
        private dBoletosTableAdapters.DadosCONDOMINIOSBOLTableAdapter dadosCONDOMINIOSBOLTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public dBoletosTableAdapters.DadosCONDOMINIOSBOLTableAdapter DadosCONDOMINIOSBOLTableAdapter {
            get {
                if (dadosCONDOMINIOSBOLTableAdapter == null) {
                    dadosCONDOMINIOSBOLTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.DadosCONDOMINIOSBOLTableAdapter();
                    dadosCONDOMINIOSBOLTableAdapter.TrocarStringDeConexao();
                }
                return dadosCONDOMINIOSBOLTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dBoletosTableAdapters.DadosAPTTableAdapter DadosAPTTableAdapter {
            get {
                if (dadosAPTTableAdapter == null) {
                    dadosAPTTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.DadosAPTTableAdapter();
                    dadosAPTTableAdapter.TrocarStringDeConexao();
                }
                return dadosAPTTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dBoletosTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get {
                if (bOLetosTableAdapter == null) {
                    bOLetosTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao(Boleto.TipoPadrao);
                };
                return bOLetosTableAdapter; 
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public dBoletosTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get {
                if (bOletoDetalheTableAdapter == null) {
                    bOletoDetalheTableAdapter = new Boletos.Boleto.dBoletosTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao(Boleto.TipoPadrao);
                };
                return bOletoDetalheTableAdapter; 
            }           
        }

        /*
        /// <summary>
        /// Calculas os campos complementares
        /// </summary>
        /// <param name="Data">Data de cálculo</param>
        /// <returns>Indica se algum boleto foi alterado durante o processo de cálculo dos dados complemetares</returns>
        public bool CamposComplementares(System.DateTime Data) {
            return CamposComplementares(Data, true);
        }*/
        
        /// <summary>
        /// Calculas os campos complementares
        /// </summary>
        /// <param name="Data">Data de cálculo</param>
        /// <param name="ocultarZeros">Oculta os zeros colocando null</param>
        /// <param name="ESP">Espera</param>
        /// <returns>Indica se algum boleto foi alterado durante o processo de cálculo dos dados complemetares</returns>
        public bool CamposComplementares(System.DateTime Data, bool ocultarZeros = true, CompontesBasicos.Espera.cEspera ESP = null)
        {
            bool CamposComplementares_Recalcular = false;
            if (ESP != null)
                ESP.AtivaGauge(BOLetos.Count);
            System.DateTime AjGauge = System.DateTime.Now.AddSeconds(3);
            int i = 0;
            foreach (dBoletos.BOLetosRow rowBOL in BOLetos)
            {
                i++;
                if ((ESP != null) && (System.DateTime.Now > AjGauge))
                {
                    ESP.Gauge(i);
                    AjGauge = System.DateTime.Now.AddSeconds(3);
                }
                Boletos.Boleto.Boleto Bol = new Boletos.Boleto.Boleto(rowBOL);
                //Boletos.Boleto.Boleto Bol = new Boletos.Boleto.Boleto(rowBOL.BOL);
                if (Bol.RemoveSeguro(Boleto.TipoRemoveSeguro.remove_SeguroVencido))
                    CamposComplementares_Recalcular = true;
                if (Bol.Apartamento != null)
                {
                    rowBOL.BLOCodigo = Bol.Apartamento.BLOCodigo;
                    rowBOL.APTNumero = Bol.Apartamento.APTNumero;
                }
                if(rowBOL.IsBOLProprietarioNull())
                    rowBOL.Destinatario = "";
                else
                    rowBOL.Destinatario = rowBOL.BOLProprietario ? "Prop" : "Inq.";
                if ((rowBOL.IsBOLPagamentoNull()) || (rowBOL.BOLPagamento > Data))
                {
                    Bol.Valorpara(Data);
                    rowBOL.ValorCorrigido = Bol.valorcorrigido;
                    if (Bol.multa > 0)
                        rowBOL.Multa = Bol.multa;
                    else
                        rowBOL.SetMultaNull();
                    if (Bol.juros > 0)
                        rowBOL.Juros = Bol.juros;
                    else
                        rowBOL.SetJurosNull();
                    rowBOL.SubTotal = Bol.valorfinal;
                    rowBOL.Honorarios = Bol.honor;
                    rowBOL.Total = Bol.valorfinal + Bol.honor;
                }
                else{
                    if (!ocultarZeros) {
                        rowBOL.Total = rowBOL.SubTotal = Bol.valorfinal;                                                
                    }
                }
                rowBOL.AcceptChanges();
            }
            return CamposComplementares_Recalcular;
        }

        /// <summary>
        /// Inclui os boletos de acodos feitos apos a data de corte
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="CON"></param>
        public void RecuperaOsBolDoAcordo(System.DateTime Data,int CON) {
            string comandoRec = "SELECT     BOLetos.BOL\r\n"+
                                "FROM         ACOrdos INNER JOIN\r\n"+
                                "ACOxBOL ON ACOrdos.ACO = ACOxBOL.ACO INNER JOIN\r\n"+
                                "BOLetos ON ACOxBOL.BOL = BOLetos.BOL INNER JOIN\r\n"+
                                "APARTAMENTOS ON ACOrdos.ACO_APT = APARTAMENTOS.APT INNER JOIN\r\n"+
                                "BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO\r\n" +
                                "WHERE   (ACOStatus in (1,2)) and  (ACOrdos.ACODATAI > @P1) AND (BOLetos.BOLVencto <= @P2) AND (BLOCOS.BLO_CON = @P3)";
            System.Data.DataTable BolRec = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comandoRec, Data.AddDays(1).AddSeconds(-1), Data, CON);
            if (BolRec.Rows.Count > 0)                
                    try
                    {
                        BOLetosTableAdapter.ClearBeforeFill = false;
                        foreach (System.Data.DataRow rowB in BolRec.Rows)
                            if (this.BOLetos.FindByBOL((int)rowB[0]) == null)
                                BOLetosTableAdapter.FillByBOL(this.BOLetos, (int)rowB[0]);
                    }
                    finally
                    {
                        BOLetosTableAdapter.ClearBeforeFill = true;
                    };
                        
        }

        #region Velocidade
        private System.Collections.Generic.SortedList<int, string> BOLMensagemImps;

        private int CON;

        internal string BOLMensagemImp(int _CON, Framework.objetosNeon.Competencia comp)
        {
            if (CON != _CON)
            {
                CON = _CON;
                BOLMensagemImps = null;
            }
            if (BOLMensagemImps == null)
                BOLMensagemImps = new System.Collections.Generic.SortedList<int, string>();
            if (!BOLMensagemImps.ContainsKey(comp.CompetenciaBind))
            {
                string PBOMensagemBalancete = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_string("SELECT PBOMensagemBalancete FROM PrevisaoBOletos WHERE (PBO_CON = @P1) AND (PBOCompetencia = @P2)", CON, comp.CompetenciaBind);
                BOLMensagemImps.Add(comp.CompetenciaBind, PBOMensagemBalancete);
            }
            return BOLMensagemImps[comp.CompetenciaBind];
        } 
        #endregion


    }
}




