﻿namespace Boletos.Boleto {
    
    
    public partial class dRasCredito 
    {
        private dRasCreditoTableAdapters.ChequesDesTableAdapter chequesDesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ChequesDes
        /// </summary>
        public dRasCreditoTableAdapters.ChequesDesTableAdapter ChequesDesTableAdapter
        {
            get
            {
                if (chequesDesTableAdapter == null)
                {
                    chequesDesTableAdapter = new dRasCreditoTableAdapters.ChequesDesTableAdapter();
                    chequesDesTableAdapter.TrocarStringDeConexao();
                };
                return chequesDesTableAdapter;
            }
        }

        private dRasCreditoTableAdapters.BoletosDesTableAdapter boletosDesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BoletosDes
        /// </summary>
        public dRasCreditoTableAdapters.BoletosDesTableAdapter BoletosDesTableAdapter
        {
            get
            {
                if (boletosDesTableAdapter == null)
                {
                    boletosDesTableAdapter = new dRasCreditoTableAdapters.BoletosDesTableAdapter();
                    boletosDesTableAdapter.TrocarStringDeConexao();
                };
                return boletosDesTableAdapter;
            }
        }
    }
}
