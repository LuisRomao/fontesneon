﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using Framework;
using Framework.datasets;
using Framework.objetosNeon;
using VirEnumeracoesNeon;
using FrameworkProc;

namespace Boletos.Boleto
{
    /// <summary>
    /// Rastreador de crédito
    /// </summary>
    public partial class RasCredito : ComponenteBaseDialog
    {
        private int BOD;

        /// <summary>
        /// Rastreável
        /// </summary>
        public bool Rastreavel=false;

        private decimal Disponivel = 0;

        private int CON;

        private int? APT;

        private DateTime DataCredito;        

        /// <summary>
        /// Construtor principal
        /// </summary>
        public RasCredito()
        {            
            InitializeComponent();            
        }

        private List<dRasCredito.BoletosDesRow> rowBODsDesconto;

        /// <summary>
        /// Construtor a ser usado (chama o principal)
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="BOD"></param>
        /// <param name="APT"></param>
        /// <param name="DataCredito"></param>
        /// <param name="Valor"></param>
        public RasCredito(int CON,int BOD,int? APT,DateTime DataCredito, decimal Valor):this()
        {            
            this.BOD = BOD;
            this.CON = CON;
            this.APT = APT;
            this.DataCredito = DataCredito;
            textEdit1.EditValue = Valor;
            CarregaDados();
            Boleto.VirEnumStatusBoleto.CarregaEditorDaGrid(colBOLStatus);
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
        }

        private void CarregaDados()
        {
            Disponivel = 0;
            BotGeraCheque.Enabled = BotDesconta.Enabled = false;
            if (dRasCredito.BoletosDesTableAdapter.Fill(dRasCredito.BoletosDes, BOD) > 0)
            {
                Rastreavel = true;                
                rowBODsDesconto = new List<dRasCredito.BoletosDesRow>();
                foreach (dRasCredito.BoletosDesRow row in dRasCredito.BoletosDes)
                    if (row.IsBOLNull())
                    {
                        BotGeraCheque.Enabled = true;
                        BotDesconta.Enabled = true;
                        Disponivel += row.BODValor;
                        rowBODsDesconto.Add(row);
                    };
                
            }
            if (dRasCredito.ChequesDesTableAdapter.Fill(dRasCredito.ChequesDes, BOD) > 0)            
                Rastreavel = true;                
            
            textEditDisp.EditValue = -Disponivel;
            textEditDisp.Visible = labelDisp.Visible = (Disponivel != 0);
        }

        private void BotGeraCheque_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma a emissão de um cheque ?", "CONFIRMAÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;
            try
            {
                BoletosProc.Boleto.dBoletos dBoletos1 = new BoletosProc.Boleto.dBoletos();
                Framework.objetosNeon.Competencia Compet = new Framework.objetosNeon.Competencia();
                int CTL = dPLAnocontas.dPLAnocontasSt.CTLPadrao(CON, CTLTipo.CreditosAnteriores);
                string PLA = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD);
                int? SPL = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select SPL from SubPLano where SPL_PLA = @P1", PLA);
                if (!SPL.HasValue)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("INSERT INTO SubPLano (SPL_PLA, SPLCodigo, SPLDescricao, SPLServico, SPLNoCondominio, SPLReterISS, SPLValorISS) VALUES (@P1,0,'NÃO É SERVIÇO PRESTADO',1,1,1,2)",PLA);
                    SPL = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select SPL from SubPLano where SPL_PLA = @P1", PLA);
                }
                string Servico = string.Format("Devolução de pag. em duplicidade em {0:dd/MM/yyyy}", DataCredito);
                malote mal = malote.PrimeiroMaloteApos(CON, DateTime.Today);
                //malote de volta
                mal++;
                DateTime VencimentoCheque = mal.DataRetorno.SomaDiasUteis(1);
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllBoletos Boletos RasCredito.cs - 87", dBoletos1.BODxPAGTableAdapter);                
                ContaPagar.Nota Nota = new ContaPagar.Nota(CON, Compet, NOATipo.Avulsa, CTL);
                if (!Nota.Encontrada())
                    Nota = new ContaPagar.Nota(CON,
                                               null,
                                               CTL,
                                               DateTime.Today,
                                               0,
                                               NOATipo.Avulsa,
                                               PLA,
                                               SPL.Value,
                                               Servico,
                                               Compet,
                                               "",
                                               "",
                                               PAGTipo.cheque,
                                               NOAStatus.Cadastrada);
                int PAG = Nota.addPAG(PAGTipo.cheque, VencimentoCheque, -Disponivel, true, false, "", false);
                BoletosProc.Boleto.dBoletos.BODxPAGRow BODxPAGRow = dBoletos1.BODxPAG.NewBODxPAGRow();
                BODxPAGRow.BOD = BOD;
                BODxPAGRow.PAG = PAG;
                dBoletos1.BODxPAG.AddBODxPAGRow(BODxPAGRow);
                dBoletos1.BODxPAGTableAdapter.Update(BODxPAGRow);
                BODxPAGRow.AcceptChanges();
                foreach (dRasCredito.BoletosDesRow row in rowBODsDesconto)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from BODxBOD where BODDestino = @P1", row.BOD);
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from BOletoDetalhe where BOD = @P1", row.BOD);
                }
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
                throw ex;
            }
            CarregaDados();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {            
            int BOL;
            if(VirInput.Input.Execute("Número do boleto",out BOL))
            {
                Boleto BolDescontar = new Boleto(BOL);
                if (!BolDescontar.Encontrado)
                    MessageBox.Show("Boleto não encontrado", "Erro");
                else
                {
                    if (!BolDescontar.rowPrincipal.IsBOLPagamentoNull())
                    {
                        MessageBox.Show("Boleto já quitado", "Erro");
                        return;
                    }
                    if (BolDescontar.rowPrincipal.BOLCancelado)
                    {
                        MessageBox.Show("Boleto cancelado", "Erro");
                        return;
                    }
                    if (CON != BolDescontar.Apartamento.Condominio.CON)
                        MessageBox.Show("Boleto não pertence ao condomínio", "Erro");
                    else
                    {
                        if((APT.HasValue) && (APT != BolDescontar.Apartamento.APT))
                        {
                            string Mens = string.Format("Confirma a utilização do crédito para quitação do boleto da unidade {0}?", BolDescontar.Unidade);
                            if (MessageBox.Show(Mens, "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                return;
                        }
                        SortedList<int, decimal> BODsDesconto = new SortedList<int,decimal>();
                        foreach (dRasCredito.BoletosDesRow rowBODDesconto in rowBODsDesconto)
                        {
                                if (!rowBODDesconto.IsBOLNull())
                                    continue;
                            BODsDesconto.Add(rowBODDesconto.BOD,rowBODDesconto.BODValor);
                        }
                        BolDescontar.QuitarComExtra(DataCredito,Disponivel, BODsDesconto);
                        /*
                        BolDescontar.Valorpara(DataCredito);
                        decimal saldo = BolDescontar.valorfinal;
                        decimal juros = saldo - BolDescontar.rowPrincipal.BOLValorPrevisto;                        
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos RasCredito.cs - 217", BolDescontar.dBoletos.BOletoDetalheTableAdapter);
                            if (BolDescontar.ValorPrevisto > -Disponivel)
                            {
                                //se tem juros usa divisão proporcional, se não é por valor
                                if (juros == 0)
                                    BolDescontar = BolDescontar.Dividir("Boleto dividido para quitação parcial com crédito", -Disponivel)[0];
                                else
                                {
                                    decimal Propor = (-Disponivel) / saldo;
                                    juros = Math.Round(juros * Propor, 2,MidpointRounding.AwayFromZero);
                                    BolDescontar = BolDescontar.Dividir("Boleto dividido para quitação parcial com crédito", -Disponivel-juros)[0];
                                }
                                BOL = BolDescontar.BOL;
                            }
                            
                            foreach (dRasCredito.BoletosDesRow rowBODDesconto in rowBODsDesconto)
                            {
                                if (!rowBODDesconto.IsBOLNull())
                                    continue;
                                if ((-rowBODDesconto.BODValor) > saldo)
                                {
                                    Boleto.DivideBOD(rowBODDesconto.BOD, -saldo);
                                    saldo = 0;
                                }
                                else
                                    saldo += rowBODDesconto.BODValor;                                
                                BolDescontar.dBoletos.BOletoDetalheTableAdapter.PrendeBOD(BOL, rowBODDesconto.BOD);
                                
                                if (saldo <= 0)
                                    break;
                            }
                            BolDescontar = new Boleto(BOL);
                            BolDescontar.SalvarAlteracoes("Utilização de crédito");
                            //if (saldo <= juros)                            
                            BolDescontar.Baixa("Crédito", DataCredito, 0, juros);
                            VirMSSQL.TableAdapter.CommitSQL();
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.VircatchSQL(ex);
                            throw ex;
                        }
                        */
                        CarregaDados();
                    }
                }
            }
        }
    }
}
