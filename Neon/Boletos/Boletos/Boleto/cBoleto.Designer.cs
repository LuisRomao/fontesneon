namespace Boletos.Boleto
{
    partial class cBoleto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cBoleto));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.bOLTipoCRAILabel = new System.Windows.Forms.Label();
            this.bOLEmissaoLabel = new System.Windows.Forms.Label();
            this.bOLVenctoLabel = new System.Windows.Forms.Label();
            this.bOLValorPrevistoLabel = new System.Windows.Forms.Label();
            this.bOLCompetenciaAnoLabel = new System.Windows.Forms.Label();
            this.bOLEnderecoLabel = new System.Windows.Forms.Label();
            this.bOLNomeLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bOLetosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bOLTipoCRAITextEdit = new DevExpress.XtraEditors.TextEdit();
            this.bOLEmissaoDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.bOLVenctoDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.bOLValorPrevistoSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.bOLEnderecoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.bOLNomeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.bOletoDetalheBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bOletoDetalheGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOD_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PlalookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.PLAnocontasbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumnPLADesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PLALookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBODMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODAcordo_BOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOD_CTL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpCTL = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.BotAcumular = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.imageStatusRegistro = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.cBotaoSegundaVia = new dllImpresso.Botoes.cBotaoImpBol();
            this.BCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.BQuitar = new DevExpress.XtraEditors.SimpleButton();
            this.imageStatus = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.imagePI = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.BotDividir = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonQuita = new DevExpress.XtraEditors.SimpleButton();
            this.BotObs = new DevExpress.XtraEditors.SimpleButton();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.fKBoletoDEscontoBOLetosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBDEData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBDEDesconto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelRodape = new DevExpress.XtraEditors.PanelControl();
            this.panelJurosAlterados = new DevExpress.XtraEditors.PanelControl();
            this.labelValAc = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLetosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLTipoCRAITextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEmissaoDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEmissaoDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLVenctoDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLVenctoDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLValorPrevistoSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEnderecoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLNomeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOletoDetalheBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOletoDetalheGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlalookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PLAnocontasbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PLALookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpCTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageStatusRegistro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagePI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBoletoDEscontoBOLetosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelRodape)).BeginInit();
            this.panelRodape.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelJurosAlterados)).BeginInit();
            this.panelJurosAlterados.SuspendLayout();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.Enabled = false;
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // bOLTipoCRAILabel
            // 
            this.bOLTipoCRAILabel.AutoSize = true;
            this.bOLTipoCRAILabel.Location = new System.Drawing.Point(222, 34);
            this.bOLTipoCRAILabel.Name = "bOLTipoCRAILabel";
            this.bOLTipoCRAILabel.Size = new System.Drawing.Size(31, 13);
            this.bOLTipoCRAILabel.TabIndex = 8;
            this.bOLTipoCRAILabel.Text = "Tipo:";
            // 
            // bOLEmissaoLabel
            // 
            this.bOLEmissaoLabel.AutoSize = true;
            this.bOLEmissaoLabel.Location = new System.Drawing.Point(7, 34);
            this.bOLEmissaoLabel.Name = "bOLEmissaoLabel";
            this.bOLEmissaoLabel.Size = new System.Drawing.Size(49, 13);
            this.bOLEmissaoLabel.TabIndex = 10;
            this.bOLEmissaoLabel.Text = "Emissao:";
            // 
            // bOLVenctoLabel
            // 
            this.bOLVenctoLabel.AutoSize = true;
            this.bOLVenctoLabel.Location = new System.Drawing.Point(213, 110);
            this.bOLVenctoLabel.Name = "bOLVenctoLabel";
            this.bOLVenctoLabel.Size = new System.Drawing.Size(30, 13);
            this.bOLVenctoLabel.TabIndex = 12;
            this.bOLVenctoLabel.Text = "Venc";
            // 
            // bOLValorPrevistoLabel
            // 
            this.bOLValorPrevistoLabel.AutoSize = true;
            this.bOLValorPrevistoLabel.Location = new System.Drawing.Point(7, 110);
            this.bOLValorPrevistoLabel.Name = "bOLValorPrevistoLabel";
            this.bOLValorPrevistoLabel.Size = new System.Drawing.Size(35, 13);
            this.bOLValorPrevistoLabel.TabIndex = 18;
            this.bOLValorPrevistoLabel.Text = "Valor:";
            // 
            // bOLCompetenciaAnoLabel
            // 
            this.bOLCompetenciaAnoLabel.AutoSize = true;
            this.bOLCompetenciaAnoLabel.Location = new System.Drawing.Point(319, 34);
            this.bOLCompetenciaAnoLabel.Name = "bOLCompetenciaAnoLabel";
            this.bOLCompetenciaAnoLabel.Size = new System.Drawing.Size(73, 13);
            this.bOLCompetenciaAnoLabel.TabIndex = 22;
            this.bOLCompetenciaAnoLabel.Text = "Competencia:";
            // 
            // bOLEnderecoLabel
            // 
            this.bOLEnderecoLabel.AutoSize = true;
            this.bOLEnderecoLabel.Location = new System.Drawing.Point(7, 84);
            this.bOLEnderecoLabel.Name = "bOLEnderecoLabel";
            this.bOLEnderecoLabel.Size = new System.Drawing.Size(56, 13);
            this.bOLEnderecoLabel.TabIndex = 26;
            this.bOLEnderecoLabel.Text = "Endere�o:";
            // 
            // bOLNomeLabel
            // 
            this.bOLNomeLabel.AutoSize = true;
            this.bOLNomeLabel.Location = new System.Drawing.Point(7, 58);
            this.bOLNomeLabel.Name = "bOLNomeLabel";
            this.bOLNomeLabel.Size = new System.Drawing.Size(38, 13);
            this.bOLNomeLabel.TabIndex = 28;
            this.bOLNomeLabel.Text = "Nome:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(375, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 64;
            this.label1.Text = "Pag:";
            // 
            // bOLetosBindingSource
            // 
            this.bOLetosBindingSource.DataMember = "BOLetos";
            this.bOLetosBindingSource.DataSource = typeof(BoletosProc.Boleto.dBoletos);
            // 
            // bOLTipoCRAITextEdit
            // 
            this.bOLTipoCRAITextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLTipoCRAI", true));
            this.bOLTipoCRAITextEdit.Location = new System.Drawing.Point(284, 34);
            this.bOLTipoCRAITextEdit.Name = "bOLTipoCRAITextEdit";
            this.bOLTipoCRAITextEdit.Properties.ReadOnly = true;
            this.bOLTipoCRAITextEdit.Size = new System.Drawing.Size(29, 20);
            this.bOLTipoCRAITextEdit.TabIndex = 9;
            // 
            // bOLEmissaoDateEdit
            // 
            this.bOLEmissaoDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLEmissao", true));
            this.bOLEmissaoDateEdit.EditValue = null;
            this.bOLEmissaoDateEdit.Location = new System.Drawing.Point(116, 33);
            this.bOLEmissaoDateEdit.Name = "bOLEmissaoDateEdit";
            this.bOLEmissaoDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.bOLEmissaoDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.bOLEmissaoDateEdit.Properties.ReadOnly = true;
            this.bOLEmissaoDateEdit.Size = new System.Drawing.Size(100, 20);
            this.bOLEmissaoDateEdit.TabIndex = 11;
            // 
            // bOLVenctoDateEdit
            // 
            this.bOLVenctoDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLVencto", true));
            this.bOLVenctoDateEdit.EditValue = null;
            this.bOLVenctoDateEdit.Location = new System.Drawing.Point(273, 109);
            this.bOLVenctoDateEdit.Name = "bOLVenctoDateEdit";
            this.bOLVenctoDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.bOLVenctoDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.bOLVenctoDateEdit.Size = new System.Drawing.Size(96, 20);
            this.bOLVenctoDateEdit.TabIndex = 13;
            this.bOLVenctoDateEdit.EditValueChanged += new System.EventHandler(this.bOLVenctoDateEdit_EditValueChanged);
            // 
            // bOLValorPrevistoSpinEdit
            // 
            this.bOLValorPrevistoSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLValorPrevisto", true));
            this.bOLValorPrevistoSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.bOLValorPrevistoSpinEdit.Location = new System.Drawing.Point(116, 109);
            this.bOLValorPrevistoSpinEdit.Name = "bOLValorPrevistoSpinEdit";
            this.bOLValorPrevistoSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.bOLValorPrevistoSpinEdit.Properties.DisplayFormat.FormatString = "n2";
            this.bOLValorPrevistoSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bOLValorPrevistoSpinEdit.Properties.ReadOnly = true;
            this.bOLValorPrevistoSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.bOLValorPrevistoSpinEdit.TabIndex = 19;
            // 
            // bOLEnderecoTextEdit
            // 
            this.bOLEnderecoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLEndereco", true));
            this.bOLEnderecoTextEdit.Location = new System.Drawing.Point(115, 83);
            this.bOLEnderecoTextEdit.Name = "bOLEnderecoTextEdit";
            this.bOLEnderecoTextEdit.Properties.MaxLength = 100;
            this.bOLEnderecoTextEdit.Size = new System.Drawing.Size(620, 20);
            this.bOLEnderecoTextEdit.TabIndex = 27;
            this.bOLEnderecoTextEdit.EditValueChanged += new System.EventHandler(this.bOLEnderecoTextEdit_EditValueChanged);
            // 
            // bOLNomeTextEdit
            // 
            this.bOLNomeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLNome", true));
            this.bOLNomeTextEdit.Location = new System.Drawing.Point(116, 57);
            this.bOLNomeTextEdit.Name = "bOLNomeTextEdit";
            this.bOLNomeTextEdit.Properties.MaxLength = 40;
            this.bOLNomeTextEdit.Size = new System.Drawing.Size(619, 20);
            this.bOLNomeTextEdit.TabIndex = 29;
            this.bOLNomeTextEdit.EditValueChanged += new System.EventHandler(this.bOLNomeTextEdit_EditValueChanged);
            // 
            // bOletoDetalheBindingSource
            // 
            this.bOletoDetalheBindingSource.DataMember = "FK_BOletoDetalhe_BOLetos1";
            this.bOletoDetalheBindingSource.DataSource = this.bOLetosBindingSource;
            // 
            // bOletoDetalheGridControl
            // 
            this.bOletoDetalheGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.bOletoDetalheGridControl.DataSource = this.bOletoDetalheBindingSource;
            this.bOletoDetalheGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bOletoDetalheGridControl.Location = new System.Drawing.Point(0, 295);
            this.bOletoDetalheGridControl.MainView = this.gridView1;
            this.bOletoDetalheGridControl.Name = "bOletoDetalheGridControl";
            this.bOletoDetalheGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.PLALookUpEdit1,
            this.PlalookUpEdit2,
            this.repositoryItemLookUpCTL,
            this.repositoryItemCalcEdit1});
            this.bOletoDetalheGridControl.Size = new System.Drawing.Size(740, 228);
            this.bOletoDetalheGridControl.TabIndex = 50;
            this.bOletoDetalheGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOD_PLA,
            this.colBODValor,
            this.gridColumn1,
            this.gridColumnPLADesc,
            this.colBODMensagem,
            this.colBODAcordo_BOL,
            this.colBOD_CTL});
            this.gridView1.GridControl = this.bOletoDetalheGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.NewItemRowText = "Incluir linha";
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.ShownEditor += new System.EventHandler(this.gridView1_ShownEditor);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            // 
            // colBOD_PLA
            // 
            this.colBOD_PLA.Caption = "Conta";
            this.colBOD_PLA.ColumnEdit = this.PlalookUpEdit2;
            this.colBOD_PLA.FieldName = "BOD_PLA";
            this.colBOD_PLA.Name = "colBOD_PLA";
            this.colBOD_PLA.OptionsColumn.FixedWidth = true;
            this.colBOD_PLA.Visible = true;
            this.colBOD_PLA.VisibleIndex = 0;
            this.colBOD_PLA.Width = 68;
            // 
            // PlalookUpEdit2
            // 
            this.PlalookUpEdit2.AutoHeight = false;
            this.PlalookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PlalookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "Cod", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descri��o", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.PlalookUpEdit2.DataSource = this.PLAnocontasbindingSource;
            this.PlalookUpEdit2.DisplayMember = "PLA";
            this.PlalookUpEdit2.Name = "PlalookUpEdit2";
            this.PlalookUpEdit2.NullText = "";
            this.PlalookUpEdit2.ValueMember = "PLA";
            this.PlalookUpEdit2.PopupFilter += new DevExpress.XtraEditors.Controls.PopupFilterEventHandler(this.PlalookUpEdit2_PopupFilter);
            this.PlalookUpEdit2.EditValueChanged += new System.EventHandler(this.AjustarBOLMensagen);
            // 
            // PLAnocontasbindingSource
            // 
            this.PLAnocontasbindingSource.DataMember = "PLAnocontas";
            this.PLAnocontasbindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colBODValor.DisplayFormat.FormatString = "n2";
            this.colBODValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.OptionsColumn.FixedWidth = true;
            this.colBODValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", "{0:n2}")});
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 3;
            this.colBODValor.Width = 83;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Bot�es";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 5;
            this.gridColumn1.Width = 27;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // gridColumnPLADesc
            // 
            this.gridColumnPLADesc.Caption = "Des. Conta";
            this.gridColumnPLADesc.ColumnEdit = this.PLALookUpEdit1;
            this.gridColumnPLADesc.FieldName = "BOD_PLA";
            this.gridColumnPLADesc.Name = "gridColumnPLADesc";
            this.gridColumnPLADesc.Visible = true;
            this.gridColumnPLADesc.VisibleIndex = 1;
            this.gridColumnPLADesc.Width = 105;
            // 
            // PLALookUpEdit1
            // 
            this.PLALookUpEdit1.AutoHeight = false;
            this.PLALookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PLALookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descri��o", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "Cod.")});
            this.PLALookUpEdit1.DataSource = this.PLAnocontasbindingSource;
            this.PLALookUpEdit1.DisplayMember = "PLADescricao";
            this.PLALookUpEdit1.Name = "PLALookUpEdit1";
            this.PLALookUpEdit1.NullText = "";
            this.PLALookUpEdit1.ShowHeader = false;
            this.PLALookUpEdit1.ValueMember = "PLA";
            this.PLALookUpEdit1.EditValueChanged += new System.EventHandler(this.AjustarBOLMensagen);
            // 
            // colBODMensagem
            // 
            this.colBODMensagem.Caption = "Mensagem";
            this.colBODMensagem.FieldName = "BODMensagem";
            this.colBODMensagem.Name = "colBODMensagem";
            this.colBODMensagem.Visible = true;
            this.colBODMensagem.VisibleIndex = 2;
            this.colBODMensagem.Width = 272;
            // 
            // colBODAcordo_BOL
            // 
            this.colBODAcordo_BOL.Caption = "Boleto Origem";
            this.colBODAcordo_BOL.FieldName = "BODAcordo_BOL";
            this.colBODAcordo_BOL.Name = "colBODAcordo_BOL";
            this.colBODAcordo_BOL.OptionsColumn.ReadOnly = true;
            // 
            // colBOD_CTL
            // 
            this.colBOD_CTL.Caption = "Conta L�gica";
            this.colBOD_CTL.ColumnEdit = this.repositoryItemLookUpCTL;
            this.colBOD_CTL.FieldName = "BOD_CTL";
            this.colBOD_CTL.Name = "colBOD_CTL";
            this.colBOD_CTL.Visible = true;
            this.colBOD_CTL.VisibleIndex = 4;
            // 
            // repositoryItemLookUpCTL
            // 
            this.repositoryItemLookUpCTL.AutoHeight = false;
            this.repositoryItemLookUpCTL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpCTL.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpCTL.DataSource = this.conTasLogicasBindingSource;
            this.repositoryItemLookUpCTL.DisplayMember = "CTLTitulo";
            this.repositoryItemLookUpCTL.Name = "repositoryItemLookUpCTL";
            this.repositoryItemLookUpCTL.NullText = "- Padr�o -";
            this.repositoryItemLookUpCTL.ShowHeader = false;
            this.repositoryItemLookUpCTL.ValueMember = "CTL";
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = typeof(FrameworkProc.datasets.dContasLogicas);
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLJustificativa", true));
            this.memoEdit1.EditValue = "";
            this.memoEdit1.Location = new System.Drawing.Point(116, 135);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Size = new System.Drawing.Size(619, 113);
            this.memoEdit1.TabIndex = 51;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.BotAcumular);
            this.panelControl1.Controls.Add(this.dateEdit1);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.imageStatusRegistro);
            this.panelControl1.Controls.Add(this.textEdit1);
            this.panelControl1.Controls.Add(this.cBotaoSegundaVia);
            this.panelControl1.Controls.Add(this.BCancelar);
            this.panelControl1.Controls.Add(this.BQuitar);
            this.panelControl1.Controls.Add(this.imageStatus);
            this.panelControl1.Controls.Add(this.imagePI);
            this.panelControl1.Controls.Add(this.BotDividir);
            this.panelControl1.Controls.Add(this.simpleButtonQuita);
            this.panelControl1.Controls.Add(this.BotObs);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Controls.Add(this.memoEdit1);
            this.panelControl1.Controls.Add(this.bOLTipoCRAITextEdit);
            this.panelControl1.Controls.Add(this.bOLTipoCRAILabel);
            this.panelControl1.Controls.Add(this.bOLEmissaoDateEdit);
            this.panelControl1.Controls.Add(this.bOLNomeLabel);
            this.panelControl1.Controls.Add(this.bOLEmissaoLabel);
            this.panelControl1.Controls.Add(this.bOLNomeTextEdit);
            this.panelControl1.Controls.Add(this.bOLVenctoDateEdit);
            this.panelControl1.Controls.Add(this.bOLEnderecoLabel);
            this.panelControl1.Controls.Add(this.bOLVenctoLabel);
            this.panelControl1.Controls.Add(this.bOLEnderecoTextEdit);
            this.panelControl1.Controls.Add(this.bOLValorPrevistoSpinEdit);
            this.panelControl1.Controls.Add(this.bOLValorPrevistoLabel);
            this.panelControl1.Controls.Add(this.bOLCompetenciaAnoLabel);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(740, 260);
            this.panelControl1.TabIndex = 52;
            // 
            // BotAcumular
            // 
            this.BotAcumular.Location = new System.Drawing.Point(3, 225);
            this.BotAcumular.Name = "BotAcumular";
            this.BotAcumular.Size = new System.Drawing.Size(107, 22);
            this.BotAcumular.TabIndex = 67;
            this.BotAcumular.Text = "Acumular";
            this.BotAcumular.Visible = false;
            this.BotAcumular.Click += new System.EventHandler(this.BotAcumular_Click);
            // 
            // dateEdit1
            // 
            this.dateEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLPagamento", true));
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(414, 109);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(96, 20);
            this.dateEdit1.TabIndex = 65;
            // 
            // imageStatusRegistro
            // 
            this.imageStatusRegistro.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLStatusRegistro", true));
            this.imageStatusRegistro.Location = new System.Drawing.Point(514, 5);
            this.imageStatusRegistro.MenuManager = this.BarManager_F;
            this.imageStatusRegistro.Name = "imageStatusRegistro";
            this.imageStatusRegistro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions1)});
            this.imageStatusRegistro.Properties.ReadOnly = true;
            this.imageStatusRegistro.Size = new System.Drawing.Size(160, 20);
            this.imageStatusRegistro.TabIndex = 63;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOL", true));
            this.textEdit1.EditValue = "BOL";
            this.textEdit1.Location = new System.Drawing.Point(10, 0);
            this.textEdit1.MenuManager = this.BarManager_F;
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(206, 28);
            this.textEdit1.TabIndex = 62;
            // 
            // cBotaoSegundaVia
            // 
            this.cBotaoSegundaVia.ItemComprovante = true;
            this.cBotaoSegundaVia.Location = new System.Drawing.Point(587, 31);
            this.cBotaoSegundaVia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoSegundaVia.Name = "cBotaoSegundaVia";
            this.cBotaoSegundaVia.Size = new System.Drawing.Size(148, 23);
            this.cBotaoSegundaVia.TabIndex = 61;
            this.cBotaoSegundaVia.Titulo = "Segunda via";
            this.cBotaoSegundaVia.clicado += new System.EventHandler(this.cBotaoSegundaVia_clicado);
            // 
            // BCancelar
            // 
            this.BCancelar.Location = new System.Drawing.Point(3, 195);
            this.BCancelar.Name = "BCancelar";
            this.BCancelar.Size = new System.Drawing.Size(107, 24);
            this.BCancelar.TabIndex = 60;
            this.BCancelar.Text = "Cancelar";
            this.BCancelar.Visible = false;
            this.BCancelar.Click += new System.EventHandler(this.BCancelar_Click);
            // 
            // BQuitar
            // 
            this.BQuitar.Location = new System.Drawing.Point(3, 165);
            this.BQuitar.Name = "BQuitar";
            this.BQuitar.Size = new System.Drawing.Size(107, 24);
            this.BQuitar.TabIndex = 59;
            this.BQuitar.Text = "Quitar";
            this.BQuitar.Visible = false;
            this.BQuitar.Click += new System.EventHandler(this.BQuitar_Click);
            // 
            // imageStatus
            // 
            this.imageStatus.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLStatus", true));
            this.imageStatus.Location = new System.Drawing.Point(346, 5);
            this.imageStatus.MenuManager = this.BarManager_F;
            this.imageStatus.Name = "imageStatus";
            this.imageStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions2)});
            this.imageStatus.Properties.ReadOnly = true;
            this.imageStatus.Size = new System.Drawing.Size(164, 20);
            this.imageStatus.TabIndex = 58;
            // 
            // imagePI
            // 
            this.imagePI.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bOLetosBindingSource, "BOLProprietario", true));
            this.imagePI.Location = new System.Drawing.Point(237, 5);
            this.imagePI.MenuManager = this.BarManager_F;
            this.imagePI.Name = "imagePI";
            this.imagePI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions3)});
            this.imagePI.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Propriet�rio", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 1)});
            this.imagePI.Properties.SmallImages = this.imagensPI1;
            this.imagePI.Size = new System.Drawing.Size(100, 20);
            this.imagePI.TabIndex = 57;
            this.imagePI.EditValueChanged += new System.EventHandler(this.imagePI_EditValueChanged);
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // BotDividir
            // 
            this.BotDividir.Location = new System.Drawing.Point(3, 137);
            this.BotDividir.Name = "BotDividir";
            this.BotDividir.Size = new System.Drawing.Size(107, 22);
            this.BotDividir.TabIndex = 55;
            this.BotDividir.Text = "Dividir";
            this.BotDividir.Visible = false;
            this.BotDividir.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButtonQuita
            // 
            this.simpleButtonQuita.Location = new System.Drawing.Point(624, 109);
            this.simpleButtonQuita.Name = "simpleButtonQuita";
            this.simpleButtonQuita.Size = new System.Drawing.Size(111, 19);
            this.simpleButtonQuita.TabIndex = 54;
            this.simpleButtonQuita.Text = "Cancelar Quita��o";
            this.simpleButtonQuita.Visible = false;
            this.simpleButtonQuita.Click += new System.EventHandler(this.simpleButtonQuita_Click);
            // 
            // BotObs
            // 
            this.BotObs.Location = new System.Drawing.Point(516, 109);
            this.BotObs.Name = "BotObs";
            this.BotObs.Size = new System.Drawing.Size(102, 19);
            this.BotObs.TabIndex = 53;
            this.BotObs.Text = "Incluir Obs.";
            this.BotObs.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(464, 29);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 27);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 52;
            this.cCompet1.Titulo = null;
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.DataSource = this.fKBoletoDEscontoBOLetosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(256, 86);
            this.gridControl1.TabIndex = 53;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gridControl1.Visible = false;
            // 
            // fKBoletoDEscontoBOLetosBindingSource
            // 
            this.fKBoletoDEscontoBOLetosBindingSource.DataMember = "FK_BoletoDEsconto_BOLetos";
            this.fKBoletoDEscontoBOLetosBindingSource.DataSource = this.bOLetosBindingSource;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.gridView2.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.gridView2.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBDEData,
            this.colBDEDesconto});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBDEData, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colBDEData
            // 
            this.colBDEData.Caption = "At�";
            this.colBDEData.FieldName = "BDEData";
            this.colBDEData.Name = "colBDEData";
            this.colBDEData.Visible = true;
            this.colBDEData.VisibleIndex = 0;
            this.colBDEData.Width = 114;
            // 
            // colBDEDesconto
            // 
            this.colBDEDesconto.Caption = "Desconto";
            this.colBDEDesconto.DisplayFormat.FormatString = "n2";
            this.colBDEDesconto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBDEDesconto.FieldName = "BDEDesconto";
            this.colBDEDesconto.Name = "colBDEDesconto";
            this.colBDEDesconto.Visible = true;
            this.colBDEDesconto.VisibleIndex = 1;
            this.colBDEDesconto.Width = 88;
            // 
            // panelRodape
            // 
            this.panelRodape.Controls.Add(this.panelJurosAlterados);
            this.panelRodape.Controls.Add(this.gridControl1);
            this.panelRodape.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelRodape.Location = new System.Drawing.Point(0, 523);
            this.panelRodape.Name = "panelRodape";
            this.panelRodape.Size = new System.Drawing.Size(740, 90);
            this.panelRodape.TabIndex = 54;
            // 
            // panelJurosAlterados
            // 
            this.panelJurosAlterados.Controls.Add(this.labelValAc);
            this.panelJurosAlterados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelJurosAlterados.Location = new System.Drawing.Point(258, 2);
            this.panelJurosAlterados.Name = "panelJurosAlterados";
            this.panelJurosAlterados.Size = new System.Drawing.Size(480, 86);
            this.panelJurosAlterados.TabIndex = 54;
            this.panelJurosAlterados.Visible = false;
            // 
            // labelValAc
            // 
            this.labelValAc.AutoSize = true;
            this.labelValAc.ForeColor = System.Drawing.Color.Red;
            this.labelValAc.Location = new System.Drawing.Point(5, 11);
            this.labelValAc.Name = "labelValAc";
            this.labelValAc.Size = new System.Drawing.Size(83, 13);
            this.labelValAc.TabIndex = 27;
            this.labelValAc.Text = "Valor de x at� x";
            // 
            // cBoleto
            // 
            this.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.BindingSourcePrincipal = this.bOLetosBindingSource;
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.bOletoDetalheGridControl);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelRodape);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cBoleto";
            this.Size = new System.Drawing.Size(740, 638);
            this.Load += new System.EventHandler(this.cBoleto_Load);
            this.Controls.SetChildIndex(this.panelRodape, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.bOletoDetalheGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLetosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLTipoCRAITextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEmissaoDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEmissaoDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLVenctoDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLVenctoDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLValorPrevistoSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEnderecoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLNomeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOletoDetalheBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOletoDetalheGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlalookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PLAnocontasbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PLALookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpCTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageStatusRegistro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagePI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBoletoDEscontoBOLetosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelRodape)).EndInit();
            this.panelRodape.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelJurosAlterados)).EndInit();
            this.panelJurosAlterados.ResumeLayout(false);
            this.panelJurosAlterados.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bOLetosBindingSource;
        private DevExpress.XtraEditors.TextEdit bOLTipoCRAITextEdit;
        private DevExpress.XtraEditors.DateEdit bOLEmissaoDateEdit;
        private DevExpress.XtraEditors.DateEdit bOLVenctoDateEdit;
        private DevExpress.XtraEditors.SpinEdit bOLValorPrevistoSpinEdit;
        private DevExpress.XtraEditors.TextEdit bOLEnderecoTextEdit;
        private DevExpress.XtraEditors.TextEdit bOLNomeTextEdit;
        private System.Windows.Forms.BindingSource bOletoDetalheBindingSource;
        private DevExpress.XtraGrid.GridControl bOletoDetalheGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private System.Windows.Forms.BindingSource PLAnocontasbindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPLADesc;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit PLALookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit PlalookUpEdit2;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.SimpleButton BotObs;
        private DevExpress.XtraGrid.Columns.GridColumn colBODMensagem;
        private DevExpress.XtraEditors.SimpleButton simpleButtonQuita;
        private DevExpress.XtraEditors.SimpleButton BotDividir;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource fKBoletoDEscontoBOLetosBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colBDEData;
        private DevExpress.XtraGrid.Columns.GridColumn colBDEDesconto;
        private DevExpress.XtraEditors.ImageComboBoxEdit imagePI;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageStatus;
        private DevExpress.XtraEditors.SimpleButton BQuitar;
        private DevExpress.XtraGrid.Columns.GridColumn colBODAcordo_BOL;
        private DevExpress.XtraEditors.SimpleButton BCancelar;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoSegundaVia;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageStatusRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_CTL;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpCTL;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private System.Windows.Forms.Label bOLTipoCRAILabel;
        private System.Windows.Forms.Label bOLEmissaoLabel;
        private System.Windows.Forms.Label bOLVenctoLabel;
        private System.Windows.Forms.Label bOLValorPrevistoLabel;
        private System.Windows.Forms.Label bOLCompetenciaAnoLabel;
        private System.Windows.Forms.Label bOLEnderecoLabel;
        private System.Windows.Forms.Label bOLNomeLabel;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PanelControl panelRodape;
        private DevExpress.XtraEditors.PanelControl panelJurosAlterados;
        private System.Windows.Forms.Label labelValAc;
        private DevExpress.XtraEditors.SimpleButton BotAcumular;
    }
}
