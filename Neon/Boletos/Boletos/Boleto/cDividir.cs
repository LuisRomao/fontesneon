using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using DevExpress.XtraEditors;
using Boletos.Boleto;
using System.Collections;

namespace Boletos.Boleto
{
    /// <summary>
    /// Divide boleto
    /// </summary>
    public partial class cDividir : ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cDividir()
        {
            InitializeComponent();            
        }

        /// <summary>
        /// Inicia os dados
        /// </summary>
        /// <param name="BOriginal"></param>
        /// <returns></returns>
        public bool Iniciar(Boleto BOriginal) 
        {
            if (!BOriginal.Encontrado)
                return false;
            if (!BOriginal.rowPrincipal.IsBOLPagamentoNull())
                return false;
            if (!Boletos.Boleto.Boleto.StatusBoletoDivisivel.Contains(BOriginal.BOLStatus))
                return false;
            listBoxControl1.Items.Clear();
            TotalOriginal.Text = BOriginal.rowPrincipal.BOLValorPrevisto.ToString("n2");
            Total1.Text = Total2.Text = "0,00";
            listBoxControl2.Items.Clear();
            listBoxControl3.Items.Clear();
            imagePI.EditValue = imageComboBoxEdit1.EditValue = BOriginal.rowPrincipal.BOLProprietario;
            bool ProprietarioNoSegundo = !BOriginal.rowPrincipal.BOLProprietario;
            
            if (!ProprietarioNoSegundo)
                if (!BOriginal.Apartamento.Alugado)
                {
                    ProprietarioNoSegundo = true;
                    imageComboBoxEdit1.Properties.ReadOnly = imageComboBoxEdit2.Properties.ReadOnly = true;
                }
            imageComboBoxEdit2.EditValue = ProprietarioNoSegundo;
            foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow BODrow in BOriginal.rowPrincipal.GetBOletoDetalheRows())
            {
                listBoxControl1.Items.Add(new PortaItem(BODrow.BODMensagem,BODrow.BOD_PLA,BODrow.BODValor,BODrow.BOD));
            }
            return true;
        }

        private int indexOfItemUnderMouseToDrag;       
        private Rectangle dragBoxFromMouseDown;      

        private void listBoxControl1_MouseDown(object sender, MouseEventArgs e)
        {
            ListBoxControl chamador = (ListBoxControl)sender;
            
            indexOfItemUnderMouseToDrag = chamador.IndexFromPoint(e.Location);
            
            if (indexOfItemUnderMouseToDrag != ListBox.NoMatches)
            {
                Size dragSize = SystemInformation.DragSize;
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)), dragSize);
            }
            else                
                dragBoxFromMouseDown = Rectangle.Empty;

        }      

        private void listBoxControl3_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(PortaItem)))
            {
                PortaItem PI = (PortaItem)e.Data.GetData(typeof(PortaItem));
                if ((PI.Sender == null) || (PI.Sender != sender))
                    e.Effect = DragDropEffects.Move;
                else
                    e.Effect = DragDropEffects.None;
                if((sender == panelControl1) && (PI.Sender != listBoxControl1))
                    e.Effect = DragDropEffects.None;
            }
            else
                e.Effect = DragDropEffects.None;
        }
       
        private void listBoxControl1_MouseUp(object sender, MouseEventArgs e)
        {
            dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void listBoxControl1_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                ListBoxControl ListDragSource = (ListBoxControl)sender;
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {

                    PortaItem PI = (PortaItem)ListDragSource.Items[indexOfItemUnderMouseToDrag];
                    PI.Sender = sender;
                    //DragDropEffects dropEffect = ListDragSource.DoDragDrop(PI, DragDropEffects.All | DragDropEffects.Link);                        
                    //CHAMADA EFETIVA
                    DragDropEffects dropEffect = ListDragSource.DoDragDrop(PI, DragDropEffects.Move);                        
                    if (dropEffect == DragDropEffects.Move)
                    {
                            ListDragSource.Items.RemoveAt(indexOfItemUnderMouseToDrag);

                            // Selects the previous item in the list as long as the list has an item.
                            if (indexOfItemUnderMouseToDrag > 0)
                                ListDragSource.SelectedIndex = indexOfItemUnderMouseToDrag - 1;

                            else if (ListDragSource.Items.Count > 0)
                                // Selects the first item.
                                ListDragSource.SelectedIndex = 0;
                            AjustaTotal();
                            
                    }
                       
                    
                }
            }

        }

        private bool AjustaTotal() 
        {
            decimal Saldo = AjustaTotal(listBoxControl1, TotalOriginal);
            AjustaTotal(listBoxControl2, Total1);
            AjustaTotal(listBoxControl3, Total2);
            return (btnConfirmar_F.Enabled = (Saldo == 0));
        }

        private void listBoxControl3_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void listBoxControl3_DragDrop(object sender, DragEventArgs e)
        {            
            if (e.Data.GetDataPresent(typeof(PortaItem))) 
            {
                AddPortaItem((ListBoxControl)sender, (PortaItem)e.Data.GetData((typeof(PortaItem))));
                //ListBoxControl ListDragTarget = (ListBoxControl)sender;                
                //ListDragTarget.Items.Add(e.Data.GetData((typeof(PortaItem))));                
            }
            
        }

        private void AddPortaItem(ListBoxControl Lista, PortaItem novoPoI) 
        {
            foreach (PortaItem PoI in Lista.Items)
                if (PoI.BOD == novoPoI.BOD) {
                    PoI.Valor += novoPoI.Valor;
                    return;
                }
            Lista.Items.Add(novoPoI);
        }

        private void panelControl1_DragDrop(object sender, DragEventArgs e)
        {            
            if (e.Data.GetDataPresent(typeof(PortaItem)))
            {               
                PortaItem item = (PortaItem)e.Data.GetData((typeof(PortaItem)));
                PortaItem item2 = new PortaItem(item);
                decimal original = item.Valor;
                item.Valor = Math.Round(original * spinEdit1.Value / 100, 2);
                item2.Valor = original - item.Valor;
                AddPortaItem(listBoxControl2, item);
                AddPortaItem(listBoxControl3, item2);
                //listBoxControl2.Items.Add(item);
                //listBoxControl3.Items.Add(item2);
            }
        }

        private bool TravPor; 

        private void spinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (!TravPor) 
            {
                TravPor = true;
                if (sender == spinEdit1)
                    spinEdit2.Value = 100 - spinEdit1.Value;
                else
                    spinEdit1.Value = 100 - spinEdit2.Value;
                TravPor = false;
            }
        }

        private decimal AjustaTotal(ListBoxControl Lista,LabelControl LabTotal) 
        { 
            decimal calcTotal = 0;
            foreach (PortaItem PoI in Lista.Items)
                calcTotal += PoI.Valor;
            LabTotal.Text = calcTotal.ToString("n2");
            return calcTotal;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            decimal ValorMaximo;
            decimal Valor = ValorMaximo = AjustaTotal(listBoxControl1, TotalOriginal);
            if (ValorMaximo <= 0)
                return;
            if(VirInput.Input.Execute("Valor (boleto 1)",ref Valor,2))
                if((ValorMaximo <= Valor) || (Valor <= 0))
                    MessageBox.Show("Valor incorreto");
                else
                {
                    decimal porc = Valor / ValorMaximo;
                    int n = listBoxControl1.Items.Count;
                    decimal Saldo = Valor;
                    foreach (PortaItem item in listBoxControl1.Items)
                    {                        
                        decimal original = item.Valor;
                        if (n > 1)
                        {
                            item.Valor = Math.Round(original * porc, 2);
                            Saldo -= item.Valor;
                        }
                        else
                            item.Valor = Saldo;
                        listBoxControl2.Items.Add(item);
                        if (original > item.Valor)
                        {
                            PortaItem item2 = new PortaItem(item);
                            item2.Valor = original - item.Valor;                            
                            listBoxControl3.Items.Add(item2);
                        }
                        n--;
                    }
                    listBoxControl1.Items.Clear();
                    AjustaTotal();
                }
        }

        /// <summary>
        /// Itens
        /// </summary>
        /// <returns></returns>
        public SortedList<int, PortaItem> ItensB1() 
        {
            SortedList<int, PortaItem> retorno = new SortedList<int, PortaItem>();
            foreach (PortaItem PoI in listBoxControl2.Items)
                retorno.Add(PoI.BOD,PoI);
            return retorno;
        }
    }

    

}
