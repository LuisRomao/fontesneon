/*
LH - 22/04/2014       - 13.2.8.27 - Valor Original
LH - 25/04/2014       - 13.2.8.34 - Simulador        
LH - 05/05/2014       - 13.2.8.41 - Seguro no acordo
LH - 09/05/2014       - 13.2.8.47 - Registro do envio de e-mails no boleto
LH - 13/06/2014       - 13.2.9.13 - Arrecadado - duplicidade - altera��o nos honor�rios
LH - 18/07/2014       - 14.1.4.5  - inclus�o da carteira 16
LH - 21/10/2014       - 14.1.4.70 - boleto tipo saldinho
MR - 20/05/2015 17:00 -           - Transf. de Valor Arrecadado com pagamento eletr�nico para condominios habilitados (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***)
MR - 10/06/2015 08:20 -           - Ajuste da descri��o dos dados de dep�sito (dados da conta + CCT) para Valor Arrecadado (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***)
MR - 25/02/2016 11:00 -           - Arquivo de Cobran�a Itau (Altera��es indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
MR - 15/03/2016 10:00 -           - Limpeza de C�digo (Altera��es indicadas por *** MRC - INICIO - REMESSA (15/03/2016 10:00) ***)
                                    Tratamento do tamanho de fonte para Quadro Inadimplente (Altera��es indicadas por *** MRC - INICIO (15/03/2016 10:00) ***)
MR - 21/03/2016 20:00 -           - Inclus�o no boleto de CNPJ do Condom�nio apenas se cadastrado (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
                                    C�digo de Empresa para remessa Bradesco diferenciado entre SA e SBC        
                                    Tratamento de altera��o de vencimento e concess�o de abatimento para Cobran�a Itau
MR - 08/06/2016 11:00 -           - Permite altera��o de boleto com registro DDA na funcao AlteracaoNoRegistroBoleto (Altera��es indicadas por *** MRC - INICIO (08/06/2016 11:00) ***)
MR - 26/06/2016 14:00 -           - No caso de altera��o de resgitro por abatimento, o valor enviado para o t�tulo continua sendo o valor original (Altera��es indicadas por *** MRC - INICIO (26/06/2016 14:00) ***)
*/

using AbstratosNeon;
using BoletosCalc;
using BoletosProc.Boleto;
using CompontesBasicos;
using CompontesBasicosProc;
using DevExpress.XtraReports.UI;
using dllImpresso;
using dllVirEnum;
using EDIBoletos;
using Framework;
using FrameworkProc;
using Framework.datasets;
using Framework.GrupoEnumeracoes;
using Framework.objetosNeon;
using FrameworkProc.datasets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using VirEnumeracoesNeon;
using VirEnumeracoes;
using CadastrosProc;

namespace Boletos.Boleto
{
    /// <summary>
    /// Objeto que representa um boleto
    /// </summary>
    public class Boleto : BoletoProc, IEDIBoletos
    {








        private const string comandoLanca = "INSERT INTO [Lancamentos Bradesco] \r\n" +
                                          "(Codcon, Apartamento, Bloco, [N�mero Documento], [Valor Pago], Cr�dito, [Data de Leitura], [Data de Lan�amento], \r\n" +
                                          "Tipopag, Descri��o,[M�s Compet�ncia],CCD) \r\n" +
                                          "VALUES     (@P1,@P2,@P3,@P4,@P5,1,@P6,@P7,@P8,@P9,@P10,@P11)";



        private static dBoletos _dBoletosSt;



        private static ArrayList _RADjagerados;

        /*
        private ABS_Apartamento apartamento;

        /// <summary>
        /// Apartamento do boleto
        /// </summary>
        public ABS_Apartamento Apartamento
        {
            get 
            {
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Buscando APT", true);
                if ((apartamento == null) && (APT.HasValue))
                {
                    if ((dBoletos.Apartamentos != null) && (dBoletos.Apartamentos.ContainsKey(APT.Value)))
                        apartamento = dBoletos.Apartamentos[APT.Value];
                    else
                    {
                        apartamento = ABS_Apartamento.GetApartamento(rowPrincipal.BOL_APT);
                        if (dBoletos.Apartamentos == null)
                            dBoletos.Apartamentos = new SortedList<int, ABS_Apartamento>();
                        dBoletos.Apartamentos.Add(APT.Value, apartamento);
                    }
                }
                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                return apartamento;
            }
            set
            {
                apartamento = value;
            }
        }
        */
        private static VirEnum virEnumStatusBoleto;
        private static VirEnum virEnumStatusRegistroBoleto;

        private bool _BoletoNovencimento = true;

        private DocBacarios.CPFCNPJ _CPF;

        private dBOletosDetalhe _dBOletosDetalhe;

        private ABS_balancete balancete = null;

        private BoletoCalc boletoCalculado;





        /// <summary>
        /// Dados do boleto na primeira impressao
        /// </summary>
        private List<dImpBoletoBase.BOLETOSIMPRow> BOLETOSIMPRow1;

        #region Rotidas para CalculaLinhaDigitavel






        //private string CalculaLinhaDigitavel() {
        //    if (_BoletoNovencimento)
        //      return CalculaLinhaDigitavel(rowPrincipal.BOLVencto, rowPrincipal.BOLValorPrevisto);
        //else
        //  return CalculaLinhaDigitavel(DataDeCalculo, valorfinal);
        //}

        bool carteiraRegistrada = false;



        #endregion


        /*
        private string CalculaLinhaDigitavel(DateTime DtVencimento,decimal Valor)
        {
            string[] CAMPOs = new string[5];
            DateTime DataRef = new DateTime(1997, 10, 7);
            if (DtVencimento < DataRef)
                return "";
            Int32 Fatorvenc = ((TimeSpan)(DtVencimento - DataRef)).Days;
            string numero = "";
            string DigitoNossoNumero = CalculaNossoNumero();
            switch (rowComplementar.CON_BCO)
            {
                case 104:
                 //   bool carteiraRegistrada = true;
                    int codigocedente;
                    if (rowComplementar.IsCONCodigoCNRNull())
                        throw new Exception("Codigo de cobran�a CNR n�o cadastrado");
                    else
                        codigocedente = int.Parse(rowComplementar.CONCodigoCNR);
                    string verificadorcodigocliente = BoletoCalc.Modulo11_2_9(codigocedente.ToString("000000"),true);
                    string livre = string.Format("{0:000000}{1}{2}{3}{4}4{5}", codigocedente, verificadorcodigocliente, NossoNumeroStrXXX.Substring(2, 3),carteiraRegistrada ? "1" : "2", NossoNumeroStrXXX.Substring(5, 3), NossoNumeroStrXXX.Substring(8,9));
                    livre += BoletoCalc.Modulo11_2_9(livre,true);
                    numero = String.Format("1049{0:0000}{1:0000000000}{2}", Fatorvenc, (double)(100 * Valor),livre);
                    numero = numero.Insert(4, BoletoCalc.Modulo11_2_9(numero, false));
                    CAMPOs[0] = String.Format("1049{0}", livre.Substring(0,5));
                    CAMPOs[1] = livre.Substring(5, 10);
                    CAMPOs[2] = livre.Substring(15, 10);
                    CAMPOs[3] = numero.Substring(4, 1);
                    CAMPOs[4] = numero.Substring(5, 14);
                    break;
                case 237:
                    numero = String.Format("2379{0:0000}{1:0000000000}{2}06{3}0{4}0", Fatorvenc, (double)(100 * Valor), rowComplementar.CONAgencia, NossoNumeroStrXXX.Substring(2, 11), rowComplementar.CONConta);
                    numero = numero.Insert(4, BoletoCalc.Modulo11_2_9(numero, false));
                    CAMPOs[0] = String.Format("2379{0}0", rowComplementar.CONAgencia);
                    CAMPOs[1] = numero.Substring(24, 10);
                    CAMPOs[2] = numero.Substring(34, 10);
                    CAMPOs[3] = numero.Substring(4, 1);
                    CAMPOs[4] = numero.Substring(5, 14);
                    break;
                case 341:
                    numero = "3419" + Fatorvenc.ToString("0000") + ((double)(100 * Valor)).ToString("0000000000") +
                                          "175" + NN.ToString("00000000") + DigitoNossoNumero.ToString() +
                                          rowComplementar.CONAgencia.ToString() + rowComplementar.CONConta + rowComplementar.CONDigitoConta.ToString() + "000";
                    numero = numero.Insert(4, BoletoCalc.Modulo11_2_9(numero, false));
                    CAMPOs[0] = "3419175" + numero.Substring(22, 2);
                    CAMPOs[1] = numero.Substring(24, 6) + DigitoNossoNumero + rowComplementar.CONAgencia.Substring(0, 3);
                    CAMPOs[2] = rowComplementar.CONAgencia.Substring(3, 1) + rowComplementar.CONConta.ToString().Substring(0, 5)+rowComplementar.CONDigitoConta.ToString().Substring(0,1) + "000";
                    CAMPOs[3] = numero.Substring(4, 1);
                    CAMPOs[4] = numero.Substring(5, 14);
                    break;
                case 399:
                    //1-3(3) codigo do banco 399
                    //4-4(1) Real 9 
                    //5-5(1) Verificador que sera colocado depois (pular)
                    //6-9(4) fator de vencimento
                    //10-19(10) valor
                    //20-26(7) Codigo do cedente
                    //27-39(13) Numero do boleto BOL
                    //40-43(4) Data em formato juliano DDDY
                    //44-44(1) numero 2
                    numero = "3999" + Fatorvenc.ToString("0000") + ((double)(100 * Valor)).ToString("0000000000") +
                             "3693872"+
                                          "000"+NN.ToString("0000000000") +
                                          DtVencimento.DayOfYear.ToString("000") + DtVencimento.Year.ToString().Substring(3, 1)
                                           + "2";
                    numero = numero.Insert(4, BoletoCalc.Modulo11_2_9(numero, false));
                    CAMPOs[0] = numero.Substring(0, 4)+ numero.Substring(19, 5);
                    CAMPOs[1] = numero.Substring(24, 10) ;
                    CAMPOs[2] = numero.Substring(34, 10);
                    CAMPOs[3] = numero.Substring(4, 1);
                    CAMPOs[4] = numero.Substring(5, 14);
                    break;
                case 409:
                    int codigodocliente;                    
                    if (rowComplementar.IsCONCodigoCNRNull())
                        throw new Exception("Codigo de cobran�a CNR n�o cadastrado");
                    else
                        codigodocliente = int.Parse(rowComplementar.CONCodigoCNR);                    
                     numero = "4099" + Fatorvenc.ToString("0000") + ((double)(100 * Valor)).ToString("0000000000") + "5" +
                                           codigodocliente.ToString("0000000") + "00" + NN.ToString("00000000000000") + DigitoNossoNumero;
                    //};
                     numero = numero.Insert(4, BoletoCalc.Modulo11_2_9(numero, false));

                    CAMPOs[0] = "40995" + codigodocliente.ToString("0000000").Substring(0, 4);
                    CAMPOs[1] = codigodocliente.ToString("0000000").Substring(4, 3) + "00" + numero.Substring(29, 5);
                    CAMPOs[2] = numero.Substring(34, 10);
                    CAMPOs[3] = numero.Substring(4, 1);
                    CAMPOs[4] = numero.Substring(5, 14);
                    break;

            };
            codigoBarras = numero;
            //   CalculaBarras(numero);
            for (int i = 0; i <= 2; i++)
                CAMPOs[i] += BoletoCalc.Modulo10(CAMPOs[i]);
            return CAMPOs[0].Insert(5, ".") + "  " +
                   CAMPOs[1].Insert(5, ".") + "  " +
                   CAMPOs[2].Insert(5, ".") + "  " +
                   CAMPOs[3].Substring(0, 1) + "  " +
                   CAMPOs[4];
        }

        */        /*
        public static void ExportaAccesPendente() {
            System.Data.DataTable BolParaExp = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela("select BOL from Boletos where BOLExportar = 1");
            foreach (System.Data.DataRow rowBOL in BolParaExp.Rows) {
                Boleto ExpBol = new Boleto((int)rowBOL["BOL"]);
                if (ExpBol.Encontrado)
                    ExpBol.ExportaAcces();
            }
        }
        */

        

        /*
        private dBoletos.BOletoDetalheRow IncluiBOD(string PLA, string Mensagem, decimal Valor,bool GravarJustificativa,bool Previsto, int? PAG,int? CTL,int? BODAcordo_BOL = null)
        {            
            dBoletos DataSet = (dBoletos)rowPrincipal.Table.DataSet;

            //evitar erro se os bods n�o estiverem carregados
            if((rowPrincipal.BOLValorPrevisto != 0) && (rowPrincipal.GetBOletoDetalheRows().Length == 0))
            {
                dBoletos.BOletoDetalheTableAdapter.EmbarcaEmTransST();
                dBoletos.BOletoDetalheTableAdapter.FillBOL(DataSet.BOletoDetalhe,rowPrincipal.BOL);
            }

            dBoletos.BOletoDetalheRow rowBOD = DataSet.BOletoDetalhe.NewBOletoDetalheRow();
            rowBOD.BOD_CON = rowPrincipal.BOL_CON;
            if (!rowPrincipal.IsBOL_APTNull())
            {
                rowBOD.BOD_APT = rowPrincipal.BOL_APT;
                rowBOD.BODProprietario = rowPrincipal.BOLProprietario;
            }
            rowBOD.BOD_BOL = rowPrincipal.BOL;
            rowBOD.BOD_PLA = PLA;
            rowBOD.BODComCondominio = (rowPrincipal.BOLTipoCRAI == "C");
            rowBOD.BODCompetencia = rowPrincipal.BOLCompetenciaAno * 100 + rowPrincipal.BOLCompetenciaMes;
            rowBOD.BODPrevisto = Previsto;
            rowBOD.BODData = (rowPrincipal.BOLVencto);
            rowBOD.BODMensagem = Mensagem;
            rowBOD.BODValor = Valor;
            if (CTL.HasValue)
                rowBOD.BOD_CTL = CTL.Value;
            if(BODAcordo_BOL.HasValue)
                rowBOD.BODAcordo_BOL = BODAcordo_BOL.Value;
            DataSet.BOletoDetalhe.AddBOletoDetalheRow(rowBOD);           
            
            if (rowPrincipal.IsBOLPagamentoNull())
            {
                rowPrincipal.BOLValorPrevisto = 0;
                rowPrincipal.BOLValorOriginal = 0;
                foreach (dBoletos.BOletoDetalheRow rowBODdet in rowPrincipal.GetBOletoDetalheRows())
                {
                    if(rowBODdet.BOD_PLA == Framework.PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD))
                        continue;
                    rowPrincipal.BOLValorPrevisto += rowBODdet.BODValor;
                    if ((rowBODdet.IsBODPrevistoNull()) || (rowBODdet.BODPrevisto))
                        rowPrincipal.BOLValorOriginal += rowBODdet.BODValor;
                }
            }
            rowPrincipal.BOLExportar = true;            
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 837",dBoletos.BOletoDetalheTableAdapter);
                dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                if (GravarJustificativa)
                {
                    if (PLA == "120002")
                        GravaJustificativa("Inclu�do valor de segunda via (" + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome + ")", "");
                    else
                        if (rowPrincipal.IsBOLPagamentoNull())
                            GravaJustificativa(String.Format("Inclu�do item no boleto {0}-{1} ({2})", PLA, Mensagem, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome), "");
                }
                else
                    SalvarAlteracoesSemRegistrar();
                    //SalvarAlteracoes("");
                
                rowBOD.AcceptChanges();
                if (PAG.HasValue)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("INSERT INTO BODxPAG (BOD, PAG) VALUES (@P1, @P2)",rowBOD.BOD,PAG.Value);
                }
                //exporta na pr�pria thread
                if (rowPrincipal.IsBOLPagamentoNull())
                {
                    //ReExportaTh();
                    GuardaBODsOriginal();
                }
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro: "+ e.Message, e);
            }
            return rowBOD;
        }
        */

        private bool DescontoAPartamentoCarregado = false;





        private int eMP;

        private bool ForcarBalancete;


        /*
        //private bool PegadadosAPT(bool Proprietario, Int32 APT)
        private bool PegadadosAPT()
        {
            

            if (rowPrincipal.IsBOL_APTNull())
                return false;
            if (_rowAPT != null)
                return true;
            int encontrados;
            if (rowPrincipal.BOLProprietario)
                encontrados = dBoletos.DadosAPTTableAdapter.FillProprietario(dBoletos.DadosAPT, rowPrincipal.BOL_APT);
            else
            {
                encontrados = dBoletos.DadosAPTTableAdapter.FillInquilino(dBoletos.DadosAPT, rowPrincipal.BOL_APT);
                if(encontrados == 0)
                    encontrados = dBoletos.DadosAPTTableAdapter.FillProprietario(dBoletos.DadosAPT, rowPrincipal.BOL_APT);
            }
            if (encontrados != 1)
                return false;
            else
            {
                _rowAPT = (dBoletos.DadosAPTRow)dBoletos.DadosAPT.Rows[0];
                if (rowAPT["FPG"] == DBNull.Value)
                    _rowAPT.FPG = 1;
                return true;
            }
             
        }
        */

        private dBoletos.DadosCONDOMINIOSBOLRow rowDadosCON;

        internal Competencia TravaCompetenciaDesconto;

        /// <summary>
        /// Faz sair o endere�o mesmo nao sendo correio.
        /// </summary>
        public bool ComprovanteDeEndereco = false;

        /// <summary>
        /// PBO para o simulador
        /// </summary>
        public int? PBOsimulador;



        /// <summary>
        /// UltimaException
        /// </summary>
        public Exception UltimaException;

        //public string geraErro;

        /// <summary>
        /// Carrega os dados do boleto
        /// </summary>
        /// <param name="_BOL">Numero do boleto</param>
        /// <returns>Registra na propriedade "Encontrado"</returns>
        private bool CarregaDadosDoBoleto(int _BOL)
        {
            //geraErro = geraErro.Substring(2);
            
            dBoletos.BOLetosTableAdapter.FillByBOL(dBoletos.BOLetos, _BOL);//6.4 %
            if (dBoletos.BOLetos.Rows.Count != 1)
                Encontrado = Super_CarregaSeSuper(_BOL);
            else
            {
                Encontrado = true;
                rowPrincipal = dBoletos.BOLetos.FindByBOL(_BOL);
                
                dBoletos.BOletoDetalheTableAdapter.FillBOL(dBoletos.BOletoDetalhe, _BOL); // 6,22 %
                
                dBoletos.BoletoDEscontoTableAdapter.Fill(dBoletos.BoletoDEsconto, _BOL); // 5,9 %                
                GuardaBODsOriginal();
                IniciaCampos(); // 6 %
            };
            
            return Encontrado;
        }

        /// <summary>
        /// Esta fun��o cria uma nova linha BOD no momento da cria��o do boleto. Deve ser revisado caso venha a ser usada em outro lugar
        /// </summary>
        /// <param name="PLA"></param>
        /// <param name="BODMensagem"></param>
        /// <param name="Valor"></param>
        private void GeraBOD(string PLA, string BODMensagem, decimal Valor/*, bool Previsto*/)
        {
            dBoletos.BOletoDetalheRow NovoBod = dBoletos.BOletoDetalhe.NewBOletoDetalheRow();
            NovoBod.BOD_BOL = rowPrincipal.BOL;
            if (!rowPrincipal.IsBOL_APTNull())
                NovoBod.BOD_APT = rowPrincipal.BOL_APT;
            NovoBod.BOD_PLA = PLA;
            NovoBod.BODComCondominio = (rowPrincipal.BOLTipoCRAI == "C");
            NovoBod.BODCompetencia = rowPrincipal.BOLCompetenciaAno * 100 + rowPrincipal.BOLCompetenciaMes;
            NovoBod.BODData = rowPrincipal.BOLVencto;
            NovoBod.BODPrevisto = true;
            NovoBod.BODMensagem = BODMensagem;
            NovoBod.BODProprietario = rowPrincipal.BOLProprietario;
            NovoBod.BODValor = Valor;
            NovoBod.BOD_CON = rowPrincipal.BOL_CON;
            dBoletos.BOletoDetalhe.AddBOletoDetalheRow(NovoBod);
            dBoletos.BOletoDetalheTableAdapter.Update(NovoBod);
            //j� � gravado no banco de dados para que o BOD seja tratado de forma homogenea com os demais que est�o sendo inclu�do no boleto.
            rowPrincipal.BOLValorPrevisto += Valor;
            rowPrincipal.BOLValorOriginal += Valor;
            NovoBod.AcceptChanges();
        }

        private string GeraEndereco()
        {
            FormaPagamento Forma = (FormaPagamento)rowPrincipal.BOL_FPG;
            if (ComprovanteDeEndereco)
                Forma = FormaPagamento.Correio;
            if (Apartamento != null)
                return Apartamento.PESEndereco(Forma, Proprietario, false);
            else if (Fornecedor != null)
                return Fornecedor.Endereco(ModeloEndereco.TresLinhas);
            else
                return "Entrega Pessoal";
        }

        private string GeraInstrucoes()
        {
            if (rowDadosCON.IsCONMenBoletoNull())
                return string.Empty;
            String Manobra = rowDadosCON.CONMenBoleto;
            if (Manobra.Contains("@M"))
                if (rowDadosCON.IsCONValorMultaNull())
                {
                    if (!VariaveisGlobais.OcultarMessageBox)
                        MessageBox.Show("ATEN��O: Foi inclu�do o marcador @M na mensagem por�m n�o foi definido um valor para multa!");
                }
                else
                    Manobra = Manobra.Replace("@M", rowDadosCON.CONValorMulta.ToString("0"));
            if (Manobra.Contains("@J"))
                if (rowDadosCON.IsCONValorJurosNull())
                {
                    if (!VariaveisGlobais.OcultarMessageBox)
                        MessageBox.Show("ATEN��O: Foi inclu�do o marcador @J na mensagem por�m n�o foi definido um valor para o juros!");
                }
                else
                    Manobra = Manobra.Replace("@J", rowDadosCON.CONValorJuros.ToString("0"));
            return Manobra;
        }

        /*
        private bool GravaAccess()
        {
            try
            {
                object[] Parametros = new object[24];
                string ApartamentoNumero = Apartamento.APTNumero;
                string Banco = Conta.BCO.ToString();
                string Bloco = Apartamento.BLOCodigo;
                string CC = Conta.NumeroConta.ToString() + Conta.NumeroDg;
                string Codcon = Apartamento.Condominio.CONCodigo;
                string Codigo_Agencia = Conta.strAgencia;
                string Condominio = rowPrincipal.BOLTipoCRAI;
                DateTime Data_emissao = rowPrincipal.BOLEmissao;
                DateTime Data_Vencimento = rowPrincipal.BOLVencto;
                const string Endereco = ".";
                bool Impresso = true;
                string Mes_Competencia = Competencia.ToStringN();
                double Multa = (double)rowPrincipal.BOLMulta;
                string Nome = rowPrincipal.BOLNome;
                double Nosso_Numero = (double)BOL;
                double Numero_Boleto = (double)BOL;
                bool Pago = (!rowPrincipal.IsBOLPagamentoNull());
                string Tipo_de_Multa = rowPrincipal.BOLTipoMulta;
                float Valor_Previsto = (float)rowPrincipal.BOLValorPrevisto;
                float Valor_Pago = ((!rowPrincipal.IsBOLValorPagoNull()) ? (float)rowPrincipal.BOLValorPago : 0);
                object Data_Pagamento;
                if (!rowPrincipal.IsBOLPagamentoNull())
                    Data_Pagamento = rowPrincipal.BOLPagamento.Date;
                else
                    Data_Pagamento = DBNull.Value;
                string Destinatario = (rowPrincipal.BOLProprietario ? "P" : "I");
                bool Correio = (rowPrincipal.BOL_FPG <= 2);
                for (int i = 0; i < 24; i++)
                    Parametros[i] = DBNull.Value;

                Parametros[0] = rowPrincipal.BOL;//[N�mero Boleto]                
                Parametros[1] = rowPrincipal.BOL;//[Nosso N�mero],                 
                Parametros[2] = Apartamento.Condominio.CONCodigo;//Codcon 
                Parametros[3] = Apartamento.APTNumero;//Apartamento 
                Parametros[4] = Bloco;//Bloco
                Parametros[5] = Destinatario;//Destinat�rio
                Parametros[6] = Condominio;//Condominio(CRAI) 
                Parametros[7] = Data_emissao.Date;//[Data emiss�o
                Parametros[8] = Data_Vencimento.Date;//[Data Vencimento]
                Parametros[9] = Multa;//Multa
                Parametros[10] = Data_Pagamento;//[Data Pagamento]                 
                Parametros[11] = Pago;//Pago 
                Parametros[12] = Valor_Previsto;//[Valor Previsto] 
                Parametros[13] = Valor_Pago;//[Valor Pago] 
                if (Condominio == "A")
                    Parametros[14] = "0101";//[M�s Compet�ncia]
                else
                    Parametros[14] = Mes_Competencia;//[M�s Compet�ncia]
                Parametros[15] = Correio;//Correio
                Parametros[16] = Endereco;//Endere�o
                Parametros[17] = Nome;//Nome
                Parametros[18] = " ";//Mensagem
                Parametros[19] = Impresso;//Impresso
                Parametros[20] = Tipo_de_Multa;//[Tipo de Multa]
                Parametros[21] = CC;//CC, 
                Parametros[22] = Codigo_Agencia;//[Codigo Ag�ncia], 
                Parametros[23] = Banco;//Banco
                //CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("Gravando no banco");
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(CoGravaBoleto, Parametros);
                rowPrincipal.BOLExportar = false;
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                return false;
            };
            return true;
        }
        */

        /// <summary>
        /// Incluir Descontos
        /// </summary>
        private void IncluirDescontos()
        {
            if (!APT.HasValue)
                return;
            if (!Contemlinha(PadraoPLA.PLAPadraoCodigo(PLAPadrao.Condominio)))
                return;
            if (!DescontoAPartamentoCarregado)
            {
                dBoletos.DescontoAPartamentoTableAdapter.FillByCON(dBoletos.DescontoAPartamento, CON);
                DescontoAPartamentoCarregado = true;
            }
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 880", dBoletos.BoletoDEscontoTableAdapter);
                foreach (dBoletos.DescontoAPartamentoRow rowDAP in rowPrincipal.GetDescontoAPartamentoRows())
                {
                    dBoletos.BoletoDEscontoRow BDErow = dBoletos.BoletoDEsconto.NewBoletoDEscontoRow();
                    DateTime DataDesconto;
                    int DiaDesconto = rowDAP.DAPdia;
                    if (DiaDesconto > DateTime.DaysInMonth(rowPrincipal.BOLVencto.Year, rowPrincipal.BOLVencto.Month))
                        DiaDesconto = DateTime.DaysInMonth(rowPrincipal.BOLVencto.Year, rowPrincipal.BOLVencto.Month);
                    if (rowPrincipal.BOLVencto.Day >= DiaDesconto)
                        DataDesconto = new DateTime(rowPrincipal.BOLVencto.Year, rowPrincipal.BOLVencto.Month, DiaDesconto);
                    else
                    {
                        DateTime DataManobra = rowPrincipal.BOLVencto.AddMonths(-1);
                        DataDesconto = new DateTime(DataManobra.Year, DataManobra.Month, DiaDesconto);
                    }
                    BDErow.BDEData = DataDesconto.SomaDiasUteis(0);
                    BDErow.BDEDesconto = rowDAP.DAPValor;
                    BDErow.BDE_BOL = rowPrincipal.BOL;
                    dBoletos.BoletoDEsconto.AddBoletoDEscontoRow(BDErow);
                    dBoletos.BoletoDEscontoTableAdapter.Update(BDErow);
                    BDErow.AcceptChanges();
                };
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw;
            }
        }


        private bool PegadadosCondominio_APT(int APT)
        {
            if (rowDadosCON != null)
                return true;
            else
            {
                rowDadosCON = dBoletos.DadosCONDOMINIOSBOL.FindByCON(CON);
                if (rowDadosCON != null)
                    return true;
                if (dBoletos.DadosCONDOMINIOSBOLTableAdapter.FillByAPT(dBoletos.DadosCONDOMINIOSBOL, APT) != 1)
                    return false;
                else
                {
                    rowDadosCON = (dBoletos.DadosCONDOMINIOSBOLRow)dBoletos.DadosCONDOMINIOSBOL.Rows[0];
                    return true;
                }
            }
        }

        private bool PegadadosCondominio1(int CON)
        {
            if (rowDadosCON != null)
                return true;
            else
            {
                rowDadosCON = dBoletos.DadosCONDOMINIOSBOL.FindByCON(CON);
                if (rowDadosCON != null)
                    return true;
                if (dBoletos.DadosCONDOMINIOSBOLTableAdapter.Fill(dBoletos.DadosCONDOMINIOSBOL, CON) != 1)
                    return false;
                else
                {
                    rowDadosCON = (dBoletos.DadosCONDOMINIOSBOLRow)dBoletos.DadosCONDOMINIOSBOL.Rows[0];
                    return true;
                }
            }
        }


        private bool RegistraBOLExtra()
        {
            int CTL = dPLAnocontas.dPLAnocontasSt.CTLPadrao(CON, CTLTipo.CreditosAnteriores);
            //ARLrow.ARLSTATUS = Tipo == TipoExtra.Duplicidade ? "Duplicidade Autom�tico" : "Extra";
            Boleto BoletoExtra = new Boleto(CON,
                                                                          Apartamento,
                                                                          Proprietario,
                                                                          Competencia,
                                                                          rowPrincipal.BOLPagamento,
                                                                          "E",
                                                                          StatusBoleto.Extra);
            string Descritivo1 = string.Format("Cr�dito boleto {0}. Unidade {1}", BOL, Unidade);
            string Historico = "Boleto gerado pelo cancelamento da emiss�o com boleto j� quitado";
            string PLAC = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraC);
            string PLAD = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD);
            dBoletos.BOletoDetalheRow rowBODO = BoletoExtra.IncluiBOD(PLAC, Descritivo1, rowPrincipal.BOLValorPago, false, null, CTL);
            int? CCD = null;
            int? ARL = null;
            if (!rowPrincipal.IsBOL_CCDNull())
                CCD = rowPrincipal.BOL_CCD;
            if (!rowPrincipal.IsBOL_ARLNull())
                ARL = rowPrincipal.BOL_ARL;
            BoletoExtra.TravaBaixaAutomaticaExtra = true;
            BoletoExtra.TravaCompetenciaDesconto = Competencia;
            BoletoExtra.Baixa(Historico, rowPrincipal.BOLPagamento, rowPrincipal.BOLValorPago, 0, CCD, ARL);
            return true;
        }

        private bool RegistraCobrancaAtiva(DateTime Pagamento, dBoletos.BOletoDetalheRow rowBOD)
        {
            int SPL = DSCentral.EMP == 1 ? 562 : 232;
            int FRN = DSCentral.EMP == 1 ? 2942 : 211;
            dFornecedores.FORNECEDORESRow rowFRN = dFornecedores.GetdFornecedoresST("FSR").FORNECEDORES.FindByFRN(FRN);
            if (rowFRN == null)
            {
                dFornecedores.RefreshDados("FSR");
                rowFRN = dFornecedores.GetdFornecedoresST("FSR").FORNECEDORES.FindByFRN(FRN);
            }
            DateTime DataCh = DateTime.Today.AddDays(7);
            while (DataCh.DayOfWeek != DayOfWeek.Friday)
                DataCh = DataCh.AddDays(1);

            ContaPagarProc.dNOtAs.dNOtAsSt.IncluiNotaSimples_Efetivo(0,
                                                             DataCh,
                                                             DataCh,
                                                             new Competencia(DataCh),
                                                             NOATipo.Avulsa,
                                                             rowFRN.FRNNome,
                                                             CON,
                                                             PAGTipo.cheque,
                                                             "",
                                                             true,
                                                             PadraoPLA.PLAPadraoCodigo(PLAPadrao.CobrancaAtivaD),
                                                             SPL,
                                                             rowBOD.BODMensagem,
                                                             rowBOD.BODValor,
                                                             FRN,
                                                             false,
                                                             ContaPagarProc.dNOtAs.TipoChave.BOL,
                                                             rowBOD.BOD);
            return true;
        }

        private bool RegistraHonorario(DateTime Pagamento, dBoletos.BOletoDetalheRow rowBOD, Acordo.Acordo AcordoAtual)
        {
            if (AcordoAtual.FRN <= 0)
            {
                string erro = string.Format("Erro: Acordo sem fornecedor: ACO: {0} BOL: {1}", AcordoAtual.LinhaMae.ACO, rowPrincipal.BOL);
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", erro, "erro do honorario");
                throw new Exception(erro);
            }
            string Nominal = string.Empty;
            dllImpostos.SolicitaRetencao[] Retencoes = null;
            string FRNEmail = string.Empty;
            string Retencao = string.Empty;
            int SPL = 206;
            if (DSCentral.EMP == 3)
                SPL = 18;
            if (AcordoAtual.FRN > 0)
            {
                dFornecedores.FORNECEDORESRow rowFRN = Framework.datasets.dFornecedores.GetdFornecedoresST("ADV").FORNECEDORES.FindByFRN(AcordoAtual.FRN);
                if (rowFRN == null)
                {
                    dFornecedores.RefreshDados("ADV");
                    rowFRN = Framework.datasets.dFornecedores.GetdFornecedoresST("ADV").FORNECEDORES.FindByFRN(AcordoAtual.FRN);
                    if (rowFRN == null)
                    {
                        string erro = string.Format("Erro: Fornecedor nao encontrado: ACO: {0} BOL: {1} FRN:{2}", AcordoAtual.LinhaMae.ACO, rowPrincipal.BOL, AcordoAtual.FRN);
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb", erro, "erro do honorario");
                    }
                }
                else
                {
                    Nominal = rowFRN.FRNNome;
                    if (!rowFRN.IsFRNCnpjNull())
                    {
                        if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                            FRNEmail = rowFRN.FRNEmail;
                        else
                            FRNEmail = "luis@virweb.com.br";
                        DocBacarios.CPFCNPJ cCNPJ = new DocBacarios.CPFCNPJ(rowFRN.FRNCnpj);
                        if (cCNPJ.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                        {
                            dllImpostos.SolicitaRetencao RetISS = null;
                            if (rowFRN.FRNRetemISS)
                            {
                                decimal ValISS = Math.Round(rowBOD.BODValor * dllImpostos.Imposto.AliquotaBase(Tipocidade) + 0.004999M, 2);
                                RetISS = new dllImpostos.SolicitaRetencao(Tipocidade, ValISS, SimNao.Sim);
                                Retencao += string.Format("   ISS:{0:n2}\r\n", ValISS);
                            }
                            if (DSCentral.EMP == 1)
                                SPL = 39;
                            else
                                SPL = 18;
                            dllImpostos.SolicitaRetencao RetCOF;
                            bool simples = (!rowFRN.IsFRNSimplesNull() && rowFRN.FRNSimples);
                            if ((!simples) && (rowBOD.BODValor >= dllImpostos.Imposto.PisoBaseCalculo(TipoImposto.PIS_COFINS_CSLL)))
                            {
                                decimal ValCOF = Math.Round(rowBOD.BODValor * dllImpostos.Imposto.AliquotaBase(TipoImposto.PIS_COFINS_CSLL) + 0.004999M, 2);
                                RetCOF = new dllImpostos.SolicitaRetencao(TipoImposto.PIS_COFINS_CSLL, ValCOF, SimNao.Sim);
                                Retencao += string.Format("   PIS/COFINS/CSLL:{0:n2}\r\n", ValCOF);
                                Retencoes = new dllImpostos.SolicitaRetencao[] { RetISS, RetCOF };
                            }
                            else
                            {
                                Retencao += "   PIS/COFINS/CSLL:0 (SIMPLES)\r\n";
                                Retencoes = new dllImpostos.SolicitaRetencao[] { RetISS };
                            }
                        }
                        else if (cCNPJ.Tipo == DocBacarios.TipoCpfCnpj.CPF)
                            if (cCNPJ.Valor != 0)
                            {
                                dllImpostos.SolicitaRetencao RetISS = null;
                                dllImpostos.SolicitaRetencao RetINSSP = null;
                                dllImpostos.SolicitaRetencao RetINSST = null;
                                if (rowFRN.FRNRetemISS)
                                {
                                    decimal ValISS = Math.Round(rowBOD.BODValor * dllImpostos.Imposto.AliquotaBase(Tipocidade) + 0.004999M, 2);
                                    Retencao += string.Format("   ISS:{0:n2}\r\n", ValISS);
                                    RetISS = new dllImpostos.SolicitaRetencao(Tipocidade, ValISS, SimNao.Padrao);
                                }
                                if (rowFRN.FRNRetemINSSPrestador)
                                {
                                    decimal ValINSSP = Math.Round(rowBOD.BODValor * dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfRet) + 0.004999M, 2);
                                    Retencao += string.Format("   INSS:{0:n2}\r\n", ValINSSP);
                                    RetINSSP = new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, ValINSSP, SimNao.Padrao);
                                }
                                if (rowFRN.FRNRetemINSSTomador)
                                {
                                    decimal ValINSST = Math.Round(rowBOD.BODValor * dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfEmp) + 0.004999M, 2);
                                    RetINSST = new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, ValINSST, SimNao.Padrao);
                                }
                                if (DSCentral.EMP == 1)
                                    SPL = 39;
                                else
                                    SPL = 18;
                                Retencoes = new dllImpostos.SolicitaRetencao[] { RetISS, RetINSSP, RetINSST };
                            }
                    }
                }
            }

            DateTime DataCh = DateTime.Today.AddDays(7);
            while (DataCh.DayOfWeek != DayOfWeek.Friday)
                DataCh = DataCh.AddDays(1);

            ContaPagarProc.dNOtAs.dNOtAsSt.IncluiNotaSimples_Efetivo(0, 
                                                                     DataCh, 
                                                                     DataCh, 
                                                                     new Competencia(DataCh), 
                                                                     NOATipo.Avulsa, 
                                                                     Nominal, 
                                                                     CON, 
                                                                     PAGTipo.Honorarios,
                                                                     "",                                                                      
                                                                     true, 
                                                                     "230000", 
                                                                     SPL, 
                                                                     rowBOD.BODMensagem, 
                                                                     rowBOD.BODValor, 
                                                                     AcordoAtual.FRN, 
                                                                     false, 
                                                                     ContaPagarProc.dNOtAs.TipoChave.BOL, 
                                                                     rowBOD.BOD,
                                                                     null,null,null,null,NOAStatus.Cadastrada, 
                                                                     Retencoes);
            if (FRNEmail != string.Empty)
            {
                string CorpoEmais = "Pagamento de honor�rios cadastrado. Favor providenciar a nota e a guia do ISS\r\rDados:\r\n";
                string CNPJ = string.Empty;
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONCNPJ from condominios where CON = @P1", out CNPJ, CON);
                CorpoEmais += string.Format("Condominio: {2} - {0} - {1}\r\n", Condominio.CONCodigo, Apartamento.Condominio.Nome, CNPJ);
                CorpoEmais += string.Format("Data do pagamento:{0:dd/MM/yyyy}\r\nDescritivo:{1}\r\nValor:{2:n2}\r\n", DataCh, rowBOD.BODMensagem, rowBOD.BODValor);
                if (Retencao != string.Empty)
                    CorpoEmais += ("Reten��es:\r\n" + Retencao);
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(FRNEmail, CorpoEmais, "Honor�rios cadastrados");
            };
            return true;
        }

        private BoletoCalc BoletoCalculado
        {
            get
            {
                if (boletoCalculado == null)
                {
                    string CodigoCNR = Conta.strCNR;
                    //int Carteira = ComRegistro ? 9 : 6;
                    //if (CodigoCNR.Trim() == "16")
                    //    Carteira = 16;
                    //if ((rowComplementar.CON_BCO == 341) && (!CompontesBasicos.FormPrincipalBase.strEmProducao))
                    //{
                    //    Carteira = 115;
                    //}
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                    int Carteira = CarteiraBanco;
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
                    boletoCalculado = new BoletoCalc(Conta.BCO, BOL, Conta.strAgencia, Conta.strNumeroConta, Conta.NumeroDg, CodigoCNR, Carteira);
                    if (_BoletoNovencimento)
                        if ((rowPrincipal.BOLTipoCRAI == "S") && (!rowPrincipal.IsBOLValorOriginalNull()))
                            boletoCalculado.CalculaLinhaDigitavel(rowPrincipal.BOLVencto, rowPrincipal.BOLValorOriginal);
                        else
                            boletoCalculado.CalculaLinhaDigitavel(rowPrincipal.BOLVencto, rowPrincipal.BOLValorPrevisto);
                    else
                        boletoCalculado.CalculaLinhaDigitavel(DataDeCalculo, valorfinal);
                }
                return boletoCalculado;
            }
        }

        private DocBacarios.CPFCNPJ CPF
        {
            get
            {                
                if (_CPF == null)
                    if (Apartamento != null)
                        _CPF = Apartamento.PESCpfCnpj(Proprietario);
                    else if (Fornecedor != null)
                        _CPF = Fornecedor.CNPJ;                
                return _CPF;
            }
        }

        private dBOletosDetalhe dBOletosDetalhe
        {
            get { return _dBOletosDetalhe ?? (_dBOletosDetalhe = new dBOletosDetalhe()); }
        }

        private static ArrayList RADjagerados
        {
            get { return _RADjagerados == null ? (_RADjagerados = new ArrayList()) : _RADjagerados; }
        }


        /*
        /// <summary>
        /// Informa�oes complementares (Banco conta etc)
        /// </summary>
        [Obsolete("Usar o objeto Apartamento e Condominio - cuidado na troca")]
        public override dBoletos.CONDOMINIOSRow rowComplementar
        {
            get {
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Busca rowComplementar", true);
                if ((_rowComplementar == null) && (Encontrado))
                {
                    //dBoletos.CONDOMINIOSDataTable DT = dBoletos.CONDOMINIOSTableAdapter.GetData(rowPrincipal.BOL);
                    dBoletos.CONDOMINIOSDataTable DT;
                    dBoletos.CONDOMINIOSTableAdapter.EmbarcaEmTransST();
                    //if(!Externo)
                        if (rowPrincipal.IsBOL_APTNull())
                            DT = dBoletos.CONDOMINIOSTableAdapter.GetDataByCON(rowPrincipal.BOL_CON);
                        else
                        {
                            //DT = dBoletos.CONDOMINIOSTableAdapter.GetDataByAPT(rowPrincipal.BOL_APT);
                            DT = dBoletos.CONDOMINIOSTableAdapter.GetData(rowPrincipal.BOL);
                            if(DT.Count == 0)
                                DT = dBoletos.CONDOMINIOSTableAdapter.GetDataByAPT(rowPrincipal.BOL_APT);
                        }
                    //else
                    //   DT = dBoletos.CONDOMINIOSTableAdapter.GetDataByAPTNET(rowPrincipal.BOL_APT,EMP);
                    if (DT.Rows.Count == 1)                                            
                        _rowComplementar = (dBoletos.CONDOMINIOSRow)DT.Rows[0];                    
                }
                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                return _rowComplementar;
            }
        }*/



        /*
        /// <summary>
        /// Competencia do boleto
        /// </summary>
        public Competencia Competencia;*/

        private static TipoImposto Tipocidade
        {
            get { return DSCentral.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA; }
        }

        private decimal ValorBanco
        {
            get
            {
                decimal retorno = 0;
                int CON = (Apartamento == null) ? rowPrincipal.BOL_CON : Apartamento.CON;
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONValorBoleto from condominios where CON = @P1", out retorno, CON);
                return retorno;
            }
        }
        private decimal ValorCorreio
        {
            get
            {
                decimal retorno = 0;
                int CON = (Apartamento == null) ? rowPrincipal.BOL_CON : Apartamento.CON;
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONValorCartaSimples from condominios where CON = @P1", out retorno, CON);
                return retorno;
            }
        }

        /// <summary>
        /// Cancela a emiss�o do boleto. os pagamentos retornam para serem gerados novamente.
        /// </summary>
        /// <param name="Justificativa"></param>
        /// <returns></returns>
        internal bool CancelaEmissao(string Justificativa)
        {
            if ((BOLStatus != StatusBoleto.Antigo) && (BOLStatus != StatusBoleto.Valido) && (BOLStatus != StatusBoleto.EmProducao))
                return false;


            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 3839",
                                                                       dBoletos.BOLetosTableAdapter,
                                                                       dBoletos.BOletoDetalheTableAdapter);

                if ((!rowPrincipal.IsBOLPagamentoNull()) && (rowPrincipal.BOLValorPrevisto != 0))
                    RegistraBOLExtra();
                rowPrincipal.BOLCancelado = true;
                string Original = string.Empty;
                dBoletos.BOletoDetalheRow[] rowBODs = rowPrincipal.GetBOletoDetalheRows();
                foreach (dBoletos.BOletoDetalheRow rowBOD in rowBODs)
                {
                    Original += string.Format("    {0} {1,30}:{2,6:n2}\r\n", rowBOD.BOD_PLA, rowBOD.BODMensagem, rowBOD.BODValor);
                    rowBOD.SetBOD_BOLNull();
                };

                rowPrincipal.BOLJustificativa = string.Format("{0}{4:dd/MM/yyyy HH:mm:ss} - Emiss�o do boleto cancelada por {1}\r\nJustificativa:\r\n{2}\r\n\r\nOriginal:\r\n{3}\r\n",
                    rowPrincipal.IsBOLJustificativaNull() ? string.Empty : rowPrincipal.BOLJustificativa + Environment.NewLine, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
                    Justificativa, Original, DateTime.Now);
                //apaga boleto original (boletos - pagamentos) Access
                //string ComandoApaga = "delete from [boleto - pagamentos] where [N�mero Boleto] = @P1;";
                //VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL);
                //ComandoApaga = "delete from boleto where [N�mero Boleto] = @P1;";
                //VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL);
                dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                dBoletos.BOletoDetalheTableAdapter.Update(rowBODs);
                VirMSSQL.TableAdapter.CommitSQL();


                Encontrado = false;
                rowPrincipal = null;
                return true;
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;

                VirMSSQL.TableAdapter.VircatchSQL(e);
                throw new Exception("Erro em cancelar emiss�o: " + e.Message, e);
            }
        }

        internal void IncluirEmAcordo(int ACO, bool Original)
        {
            string comandoIncluirACOBOL = "INSERT INTO ACOxBOL  (ACO, BOL, ACOBOLOriginal) VALUES (@P1,@P2,@P3)";
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoIncluirACOBOL, ACO, rowPrincipal.BOL, Original);
            if (Original)
            {
                rowPrincipal.BOLCancelado = true;
                BOLStatus = StatusBoleto.OriginalAcordo;
                GravaJustificativa(string.Format("Boleto inclu�do no acordo {0}", ACO));
            }
        }

        /// <summary>
        /// Ajusta a cor da linha pelo status do boleto
        /// </summary>
        /// <param name="Appearance"></param>
        public void AjustaAparencia(DevExpress.Utils.AppearanceObject Appearance)
        {
            AjustaAparencia(Appearance, rowPrincipal);
        }



        /// <summary>
        /// Ajusta a cor da linha pelo status do boleto
        /// </summary>
        /// <param name="Appearance"></param>
        /// <param name="linha"></param>
        public static void AjustaAparencia(DevExpress.Utils.AppearanceObject Appearance, dBoletos.BOLetosRow linha)
        {
            StatusBoleto Status = (StatusBoleto)linha.BOLStatus;
            StatusRegistroBoleto StatusR = (StatusRegistroBoleto)linha.BOLStatus;
            if (Status == StatusBoleto.EmProducao)
                Appearance.BackColor = Color.Orange;
            else if (linha.BOLCancelado)
                Appearance.BackColor = Color.Wheat;
            else if (linha.BOLProtestado)
                if (!linha.IsBOLPagamentoNull())
                    Appearance.BackColor = Color.FromArgb(0, 192, 0); //verde
                else
                    Appearance.BackColor = Color.Red;
            //else if ((Status == StatusBoleto.Valido) && ((StatusR == StatusRegistroBoleto.BoletoRegistrando) || (StatusR == StatusRegistroBoleto.BoletoAguardandoRetorno)))
            //    Appearance.BackColor = Color.Violet;
            else if (!linha.IsBOLPagamentoNull())
                if ((linha.BOLTipoCRAI == "E") && (linha.GetExtraPendenteRows().Length > 0))
                    Appearance.BackColor = Color.Cyan;
                else
                    Appearance.BackColor = Color.FromArgb(200, 254, 200); //verde
            else if ((Status == StatusBoleto.Saldinho) || (Status == StatusBoleto.Acumulado))
                Appearance.BackColor = Color.DarkGoldenrod;//Marrom
            else if (linha.BOLVencto < DateTime.Today.AddDays(-3))
                Appearance.BackColor = Color.FromArgb(254, 200, 200);//vermelho            
            else if (linha.BOLVencto < DateTime.Today)
                Appearance.BackColor = Color.Yellow;
            else
                Appearance.BackColor = Color.Silver;
            Appearance.ForeColor = Color.Black;
        }

        /// <summary>
        /// Cria uma liga��o entre registro BODs
        /// </summary>
        /// <param name="BODO">Origem</param>
        /// <param name="BODDs">Destino</param>
        public static void AmarraBODs(int BODO, params int[] BODDs)
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos boleto.cs - 527", dBoletosSt.BODxBODTableAdapter);
                foreach (int BODD in BODDs)
                {
                    dBoletos.BODxBODRow BODxBODrow = dBoletosSt.BODxBOD.NewBODxBODRow();
                    BODxBODrow.BODOrigem = BODO;
                    BODxBODrow.BODDestino = BODD;
                    dBoletosSt.BODxBOD.AddBODxBODRow(BODxBODrow);
                    dBoletosSt.BODxBODTableAdapter.Update(BODxBODrow);
                    BODxBODrow.AcceptChanges();
                }
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
            }
        }

        /// <summary>
        /// Apaga o boleto
        /// </summary>
        /// <param name="ApagarBOD">apaga os itens BOD ou libera para novo boleto</param>       
        public void Apagar(bool ApagarBOD)
        {

            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 3753",
                    dBoletos.BOLetosTableAdapter);
                if (ApagarBOD)
                    dBoletos.BOLetosTableAdapter.DeleteBOD(rowPrincipal.BOL);
                else
                    //TODO APAGAR BOD autom�tico - Impresso/correio/banco
                    dBoletos.BOLetosTableAdapter.UpdateLiberaBOD(rowPrincipal.BOL);
                dBoletos.BOLetosTableAdapter.DeleteBOL(rowPrincipal.BOL);

                //apaga boleto original (boletos - pagamentos) Access
                //string ComandoApaga = "delete from [boleto - pagamentos] where [N�mero Boleto] = @P1;";
                //if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL) == 0)
                //    throw new Exception("Falha na grava��o do acordo!!!!");
                VirMSSQL.TableAdapter.STTableAdapter.Commit();

                Encontrado = false;
                rowPrincipal = null;
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro em apaga: ", e);
            }
        }

        /// <summary>
        /// Apaga linha do impresso
        /// </summary>
        public void ApagarBOLETOSIMPRow()
        {
            if (BOLETOSIMPRow1 != null)
            {
                foreach (dImpBoletoBase.BOLETOSIMPRow rowdel in BOLETOSIMPRow1)
                    rowdel.Delete();
                BOLETOSIMPRow1 = null;
            }
        }

        /// <summary>
        /// cancela boleto. (no access) nao afeta o status
        /// </summary>        
        public void Cancelar()
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 3887");
                //VirOleDb.TableAdapter.STTableAdapter.AbreTrasacaoST();
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update boletos set BOLCancelado=1 where BOL = @P1", rowPrincipal.BOL);

                //apaga boleto original (boletos - pagamentos) Access
                //string ComandoApaga = "delete from [boleto - pagamentos] where [N�mero Boleto] = @P1;";
                //VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL);                 
                //ComandoApaga = "delete from boleto where [N�mero Boleto] = @P1;";
                //VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL);

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                //VirOleDb.TableAdapter.STTableAdapter.Commit();

                Encontrado = false;
                rowPrincipal = null;
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                //VirOleDb.TableAdapter.STTableAdapter.Vircatch(null);
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro em cancelar: " + e.Message, e);
            }
        }

        /// <summary>
        /// Carregas os dados do boleto no impresso
        /// </summary>
        /// <param name="Impresso">impresso</param>
        /// <param name="Vencimento">se a data deve ser a de vencimento</param>
        /// <param name="SegundaVia">Se � senda via</param>
        public void CarregaLinha(ImpBoletoBase Impresso, bool Vencimento, bool SegundaVia)
        {
            
            BoletoNovencimento = Vencimento;
            if (Impresso.dImpBoletoBase1.BOLETOSIMP.Rows.Count == 0)
            {
                //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                int cart = CarteiraBanco; //ComRegistro ? 9 : 6;
                //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
                //*** MRC - INICIO - REMESSA (15/03/2016 10:00) ***
                //if ((rowCON.CON_BCO == 341) && (!CompontesBasicos.FormPrincipalBase.strEmProducao))
                //    cart = 115;
                //*** MRC - TERMINO - REMESSA (15/03/2016 10:00) ***
                Impresso.DadosBanco(rowCON.CON_BCO, cart);
                if (Impresso.GetType().IsSubclassOf(typeof(ImpBoletoBal)) || (Impresso.GetType() == typeof(ImpBoletoBal)))
                {
                    ImpBoletoBal ImpressoBal = (ImpBoletoBal)Impresso;

                    //*** MRC - INICIO (15/03/2016 10:00) ***
                    //int MaxCar = ImpressoBal.PreparaH(CON, Competencia, ForcarBalancete);
                    int MaxCar = ImpressoBal.PreparaH(CON, Competencia, ForcarBalancete, rowCON.CONInaTamanhoFonte);
                    //*** MRC - TERMINO (15/03/2016 10:00) ***
                    Boletos.Boleto.Impresso.RetornoInad RetornoInad1 = null;
                    Competencia Compbal;
                    if (ImpressoBal.balancete != null)
                    {
                        Compbal = new Competencia(ImpressoBal.balancete.ABS_comp.CompetenciaBind);
                        RetornoInad1 = Boletos.Boleto.Impresso.RetornoInad.CalculaUnidadesInadimplenes(CON, Compbal, ImpressoBal.balancete.DataF, MaxCar);
                    }
                    //else if (ImpressoBal.SaldosAccessRow != null)
                    //{
                    //    Compbal = Competencia.CloneCompet(-1);
                    //    RetornoInad1 = Boletos.Boleto.Impresso.RetornoInad.CalculaUnidadesInadimplenes(CON, Competencia, ImpressoBal.SaldosAccessRow.Data_Final, MaxCar);
                    //}
                    if (RetornoInad1 == null)
                        ImpressoBal.SetaquadroInad(string.Empty, string.Empty);
                    else
                        ImpressoBal.SetaquadroInad(RetornoInad1.Titulo, RetornoInad1.Conteudo);
                };
            };

            

            dImpBoletoBase.BOLETOSIMPRow BOLETOSIMPRow = Impresso.dImpBoletoBase1.BOLETOSIMP.NewBOLETOSIMPRow();
            BOLETOSIMPRow.CONNomeBloco = Condominio.IdentificacaoBloco;
            BOLETOSIMPRow.CONNomeApto = Condominio.IdentificacaoApartamento;
            if (Apartamento != null)
            {
                BOLETOSIMPRow.APTNumero = Apartamento.APTNumero;
                BOLETOSIMPRow.BLOCodigo = Apartamento.BLOCodigo;
                BOLETOSIMPRow.APTStatusCobranca = (int)Apartamento.StatusCobranca;
                BOLETOSIMPRow.APTSeguro = Apartamento.APTSeguro;
            }
            else
            {
                BOLETOSIMPRow.APTNumero = string.Empty;
                BOLETOSIMPRow.BLOCodigo = string.Empty;
                BOLETOSIMPRow.APTStatusCobranca = (int)APTStatusCobranca.SemCobranca;
            }
            BOLETOSIMPRow.CONOcultaSenha = Condominio.OcultaSenhaBoleto;
            if (Condominio.CodigoSeguroConteudo.HasValue)
                BOLETOSIMPRow.CON_PSC = Condominio.CodigoSeguroConteudo.Value;
            BOLETOSIMPRow.BOL = rowPrincipal.BOL;
            BOLETOSIMPRow.BOL_BCO = Conta.BCO;
            BOLETOSIMPRow.BOLCodBar = CodigoBarras;
            BOLETOSIMPRow.BOLCodbarstr = LinhaDigitavel;
            BOLETOSIMPRow.BOLStatusRegistro = rowPrincipal.BOLStatusRegistro;
            BOLETOSIMPRow.BOLEmissao = rowPrincipal.BOLEmissao;
            if ((rowPrincipal.BOLEndereco == "Entrega Pessoal") && ComprovanteDeEndereco)
                BOLETOSIMPRow.BOLEndereco = GeraEndereco();
            else
                BOLETOSIMPRow.BOLEndereco = rowPrincipal.BOLEndereco;
            BOLETOSIMPRow.BOLMensagem = rowPrincipal.BOLMensagem;

            //Console.WriteLine(rowPrincipal.GetBoletoDEscontoRows().Length);
            //Console.WriteLine(dBoletos.BoletoDEsconto.Count);
            foreach (dBoletos.BoletoDEscontoRow BDErow in rowPrincipal.GetBoletoDEscontoRows())
                BOLETOSIMPRow.BOLMensagem += string.Format("\r\nConceder desconto de R${0:n2} para pagamento at� {1:dd/MM/yyyy} (Total R${2:n2})", BDErow.BDEDesconto, BDErow.BDEData, rowPrincipal.BOLValorPrevisto - BDErow.BDEDesconto);
            //foreach (dBoletos.BoletoDEscontoRow BDErow in dBoletos.BoletoDEscontoTableAdapter.GetData(rowPrincipal.BOL))
            //{
            //  BOLETOSIMPRow.BOLMensagem += string.Format("\r\nConceder desconto de R${0:n2} para pagamento at� {1:dd/MM/yyyy} (Total R${2:n2})", BDErow.BDEDesconto, BDErow.BDEData, rowPrincipal.BOLValorPrevisto - BDErow.BDEDesconto);
            //}

            BOLETOSIMPRow.BOLNome = rowPrincipal.BOLNome.Trim();

            

            if (CPF != null)
                switch (CPF.Tipo)
                {
                    case DocBacarios.TipoCpfCnpj.CPF:
                        BOLETOSIMPRow.NomeCPFPagador = string.Format("{0} CPF: {1}", BOLETOSIMPRow.BOLNome, CPF);
                        break;
                    case DocBacarios.TipoCpfCnpj.CNPJ:
                        BOLETOSIMPRow.NomeCPFPagador = string.Format("{0} CNPJ: {1}", BOLETOSIMPRow.BOLNome, CPF);
                        break;
                    default:
                        BOLETOSIMPRow.NomeCPFPagador = string.Format("{0}", BOLETOSIMPRow.BOLNome);
                        break;
                }
            else
                BOLETOSIMPRow.NomeCPFPagador = string.Format("{0}", BOLETOSIMPRow.BOLNome);
            
            if (CPF != null)
                BOLETOSIMPRow.CPF = CPF.Valor;
            if (Vencimento)
            {
                if ((rowPrincipal.BOLTipoCRAI == "S") && (!rowPrincipal.IsBOLValorOriginalNull()))
                    BOLETOSIMPRow.BOLValorPrevisto = rowPrincipal.BOLValorOriginal;
                else
                    BOLETOSIMPRow.BOLValorPrevisto = rowPrincipal.BOLValorPrevisto;
                BOLETOSIMPRow.BOLVencto = rowPrincipal.BOLVencto;
                if (BOLETOSIMPRow.BOL_BCO == 104)
                    BOLETOSIMPRow.BOLMensagem = string.Format("-Ap�s o vencimento, pag�vel somente nas ag�ncias da Caixa Econ�mica\r\n{0}", BOLETOSIMPRow.BOLMensagem);
            }
            else
            {
                BOLETOSIMPRow.BOLValorPrevisto = valorfinal;
                BOLETOSIMPRow.BOLVencto = DataDeCalculo;
                BOLETOSIMPRow.BOLMensagem = "N�o receber ap�s o vencimento";
            };

            

            if (Impresso is ImpBoletoReciboSV)
                BOLETOSIMPRow.BOLMensagem = "N�o cobrar multa nem juros";
            if (Conta.BCO == 104)
                BOLETOSIMPRow.Calc_AgCedente = string.Format("{0}/{1}-{2}", Conta.strAgencia, Conta.CNR.Value, BoletoCalc.Modulo11_2_9(Conta.CNR.Value.ToString("000000"), true));
            else
                BOLETOSIMPRow.Calc_AgCedente = string.Format("{0}-{1}/{2}-{3}", Conta.strAgencia, Conta.AgenciaDg, Conta.strNumeroConta, Conta.NumeroDg);

            

            if (rowPrincipal.BOLTipoCRAI == "G")
                BOLETOSIMPRow.Calc_PI = "G";
            else
                if (rowPrincipal.IsBOLProprietarioNull())
                BOLETOSIMPRow.Calc_PI = string.Empty;
            else
                BOLETOSIMPRow.Calc_PI = rowPrincipal.BOLProprietario ? "P" : "I";
            BOLETOSIMPRow.Competencia = String.Format("{0:00} {1:0000}", rowPrincipal.BOLCompetenciaMes, rowPrincipal.BOLCompetenciaAno);
            BOLETOSIMPRow.CompAno = rowPrincipal.BOLCompetenciaAno;
            BOLETOSIMPRow.CompMes = rowPrincipal.BOLCompetenciaMes;
            if (!rowPrincipal.IsBOL_APTNull())
                BOLETOSIMPRow.APT = rowPrincipal.BOL_APT;
            BOLETOSIMPRow.CON = rowPrincipal.BOL_CON;
            BOLETOSIMPRow.CONCodigo = Condominio.CONCodigo;
            
            BOLETOSIMPRow.CONEndereco = string.Format("{0} CEP {1}", rowCON.CONEndereco.Trim(), rowCON.CONCep);
            BOLETOSIMPRow.CONEnderecoComple = string.Format("{0}{1}{2}", rowCON.CONBairro.Trim(),
                                                                         (!(rowCON.IsCIDNomeNull()) ? " " + rowCON.CIDNome.Trim() : string.Empty),
                                                                         (!(rowCON.IsCIDUfNull()) ? "/" + rowCON.CIDUf : string.Empty));
            BOLETOSIMPRow.CONNome = Condominio.Nome;
            BOLETOSIMPRow.CONCNPJ = (!rowCON.IsCONCnpjNull() ? ("CNPJ: " + rowCON.CONCnpj) : string.Empty);
            
            //*** MRC - INICIO (21/03/2016 20:00) ***
            BOLETOSIMPRow.Cedente = string.Format("{0}{1} CEP:{2}\r\n{3} - {4}{5}{6}",
                                                  rowCON.CONNome,
                                                  (!rowCON.IsCONCnpjNull() ? (" - CNPJ: " + rowCON.CONCnpj) : string.Empty),
                                                  rowCON.CONCep,
                                                  rowCON.CONEndereco.Trim(),
                                                  rowCON.CONBairro.Trim(),
                                                  (!(rowCON.IsCIDNomeNull()) ? " " + rowCON.CIDNome : string.Empty),
                                                  (!(rowCON.IsCIDUfNull()) ? "/" + rowCON.CIDUf : string.Empty));
            //*** MRC - TERMINO (21/03/2016 20:00) ***
            
            if (Condominio.CONCodigoFolha1.HasValue)
                BOLETOSIMPRow.CONCodigoFolha1 = Condominio.CONCodigoFolha1.Value;
            string Mens = string.Empty;

            if (rowPrincipal.BOLTipoCRAI == "S")
            {
                
                BOLETOSIMPRow.CONNome = Apartamento.Condominio.Nome;                
                BOLETOSIMPRow.CONEndereco = Apartamento.Condominio.Endereco(ModeloEndereco.UmaLinha_End_CEP);                
                BOLETOSIMPRow.CONCodigo = Apartamento.Condominio.CONCodigo;                
                if (Apartamento.Condominio.CONCodigoFolha1.HasValue)
                    BOLETOSIMPRow.CONCodigoFolha1 = Apartamento.Condominio.CONCodigoFolha1.Value;                
                BOLETOSIMPRow.CONCNPJ = Apartamento.Condominio.Endereco(ModeloEndereco.UmaLinha_End);                
            }

            

            if (Impresso.GetType() == typeof(ImpBoletoAcordo))
                if (BoletosProc.Acordo.dAcordo.DAcordoST.ACOrdosTableAdapter.FillByBOL(BoletosProc.Acordo.dAcordo.DAcordoST.ACOrdos, rowPrincipal.BOL, false) == 1)
                {
                    BoletosProc.Acordo.dAcordo.ACOrdosRow rowACO = BoletosProc.Acordo.dAcordo.DAcordoST.ACOrdos[0];
                    BoletosProc.Acordo.dAcordo.DAcordoST.OriginaisTableAdapter.FillByACO(BoletosProc.Acordo.dAcordo.DAcordoST.Originais, rowACO.ACO);

                    BOLETOSIMPRow.A1 = string.Empty;
                    BOLETOSIMPRow.A2 = string.Empty;
                    BOLETOSIMPRow.A3 = string.Empty;
                    BOLETOSIMPRow.A4 = string.Empty;
                    BOLETOSIMPRow.A5 = BOLETOSIMPRow.A6 = BOLETOSIMPRow.A7 = BOLETOSIMPRow.A8 = BOLETOSIMPRow.A9 = string.Empty;
                    string Quebra = string.Empty;
                    foreach (BoletosProc.Acordo.dAcordo.OriginaisRow rowORG in BoletosProc.Acordo.dAcordo.DAcordoST.Originais)
                    {
                        Competencia Comp = new Competencia(rowORG.BOLCompetenciaMes, rowORG.BOLCompetenciaAno);
                        BOLETOSIMPRow.A1 += Quebra + rowORG.BOL;
                        BOLETOSIMPRow.A3 += Quebra + Comp;
                        BOLETOSIMPRow.A2 += Quebra + rowORG.BOLTipoCRAI;
                        BOLETOSIMPRow.A4 += Quebra + rowORG.BOLVencto.ToString("dd/MM/yyyy");
                        BOLETOSIMPRow.A5 += Quebra + rowORG.BOLValorPrevisto.ToString("n2");
                        Quebra = Environment.NewLine;
                    };
                    BoletosProc.Acordo.dAcordo.DAcordoST.NovosTableAdapter.FillByACO(BoletosProc.Acordo.dAcordo.DAcordoST.Novos, rowACO.ACO);
                    Quebra = string.Empty;
                    int n = 1;

                    foreach (BoletosProc.Acordo.dAcordo.NovosRow rowNov in BoletosProc.Acordo.dAcordo.DAcordoST.Novos)
                    {

                        BOLETOSIMPRow.A6 += Quebra + (rowPrincipal.BOL == rowNov.BOL ? "**  " : string.Empty) + n;
                        BOLETOSIMPRow.A7 += Quebra + rowNov.BOL;
                        BOLETOSIMPRow.A8 += Quebra + rowNov.BOLVencto.ToString("dd/MM/yyyy");
                        BOLETOSIMPRow.A9 += Quebra + rowNov.BOLValorPrevisto.ToString("n2");
                        Quebra = Environment.NewLine;
                        if (rowPrincipal.BOL == rowNov.BOL)
                            BOLETOSIMPRow.Competencia = String.Format("{0}/{1}", n, BoletosProc.Acordo.dAcordo.DAcordoST.Novos.Rows.Count);
                        n++;
                    };
                    if (n > 42)
                        ((ImpBoletoAcordo)Impresso).AjustarTamanho();
                    if (n > 53)
                        ((ImpBoletoAcordo)Impresso).AjustarFonte();
                }

            

            if (Impresso.GetType() == typeof(ImpBoletoReciboAc))
                if (BoletosProc.Acordo.dAcordo.DAcordoST.ACOrdosTableAdapter.FillByBOL(BoletosProc.Acordo.dAcordo.DAcordoST.ACOrdos, rowPrincipal.BOL, false) == 1)
                {
                    BoletosProc.Acordo.dAcordo.ACOrdosRow rowACO = BoletosProc.Acordo.dAcordo.DAcordoST.ACOrdos[0];

                    int n = 1;

                    foreach (BoletosProc.Acordo.dAcordo.NovosRow rowNov in BoletosProc.Acordo.dAcordo.DAcordoST.Novos)
                    {
                        if (rowPrincipal.BOL == rowNov.BOL)
                            BOLETOSIMPRow.Competencia = string.Format("{0}/{1}", n, BoletosProc.Acordo.dAcordo.DAcordoST.Novos.Rows.Count);
                        n++;
                    };

                }

            

            if ((Impresso is ImpBoletoReciboSV) || (Impresso.GetType().IsSubclassOf(typeof(ImpBoletoReciboSV))))
            {
                ImpBoletoReciboSV ImpRec = (ImpBoletoReciboSV)Impresso;

                Boletos.Boleto.CadCheques.cCadCheques CadCh = new Boletos.Boleto.CadCheques.cCadCheques();
                int Contador = 0;
                if (CadCh.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    BOLETOSIMPRow.A1 = BOLETOSIMPRow.A2 = BOLETOSIMPRow.A3 = BOLETOSIMPRow.A4 = BOLETOSIMPRow.A5 = string.Empty;
                    string Quebra = string.Empty;
                    foreach (Boletos.Boleto.CadCheques.dCheques.ChequesRow CHrow in CadCh.dCheques.Cheques)
                    {
                        Contador++;
                        BOLETOSIMPRow.A1 += Quebra + CHrow.Banco;
                        BOLETOSIMPRow.A2 += Quebra + CHrow.Agencia;
                        BOLETOSIMPRow.A3 += Quebra + CHrow.Conta;
                        BOLETOSIMPRow.A4 += Quebra + CHrow.Numero;
                        BOLETOSIMPRow.A5 += Quebra + CHrow.Valor.ToString("n2");
                        Quebra = "\r\n";
                    };


                };
                if (Contador == 0)
                    ImpRec.QuadroCheques.Visible = ImpRec.QuadroCheques2.Visible = false;
                else
                {
                    ImpRec.QuadroCheques.Visible = ImpRec.QuadroCheques2.Visible = true;
                    ImpRec.xrTitulo.Text = (Contador == 1) ? "V�lido como comprovante somente ap�s a compensa��o do cheque" : "V�lido como comprovante somente ap�s a compensa��o dos cheques";
                    ImpRec.LabelCheques.Text = (Contador == 1) ? "Cheque:" : "Cheques:";
                };
            }


            if (rowPrincipal.BOLTipoCRAI == "C")
            {
                string Comandobusca = string.Empty;
                if (PBOsimulador.HasValue)
                {
                    Comandobusca = "SELECT PrevisaoBOletos.PBOMensagemBalancete\r\n" +
                                              "FROM PrevisaoBOletos \r\n" +
                                              "WHERE     (PrevisaoBOletos.PBO = @P1)";
                    VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(Comandobusca, out Mens, PBOsimulador.Value);
                }
                else
                    //Comandobusca = "SELECT DISTINCT PrevisaoBOletos.PBOMensagemBalancete\r\n" +
                    //                    "FROM BOLetos INNER JOIN\r\n" +
                    //                  "BOletoDetalhe ON BOLetos.BOL = BOletoDetalhe.BOD_BOL INNER JOIN\r\n" +
                    //                "PrevisaoBOletos ON BOletoDetalhe.BOD_PBO = PrevisaoBOletos.PBO\r\n" +
                    //              "WHERE     (BOLetos.BOL = @P1)";

                    //VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(Comandobusca, out Mens, rowPrincipal.BOL);

                    Mens = _dBoletos.BOLMensagemImp(CON, Competencia);
                //if(!Externo)

                BOLETOSIMPRow.BOLMensagemImp = Mens;
            }
            else
                if (rowPrincipal.BOLTipoCRAI == "R")
            {
                string ComandobuscaR = "SELECT     RATeios.RATHistorico\r\n" +
                                       "FROM         BOletoDetalhe INNER JOIN\r\n" +
                                       "RAteioDetalhes ON BOletoDetalhe.BOD_RAD = RAteioDetalhes.RAD INNER JOIN\r\n" +
                                       "RATeios ON RAteioDetalhes.RAD_RAT = RATeios.RAT\r\n" +
                                       "WHERE     (BOletoDetalhe.BOD_BOL = @P1)";
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(ComandobuscaR, out Mens, rowPrincipal.BOL);
                BOLETOSIMPRow.BOLMensagemImp = Mens;
            }
            else
                if (!rowPrincipal.IsBOLMensagemImpNull())
                BOLETOSIMPRow.BOLMensagemImp = rowPrincipal.BOLMensagemImp;
            else
                BOLETOSIMPRow.BOLMensagemImp = string.Empty;

            


            if (!Vencimento)
            {
                BOLETOSIMPRow.BOLMensagemImp = " Vencimento original: " + rowPrincipal.BOLVencto.ToString("dd/MM/yyyy");
                if (valorfinal > rowPrincipal.BOLValorPrevisto)
                    BOLETOSIMPRow.BOLMensagemImp += string.Format("\r\n Encargos inclusos: R$ {0:n2}", valorfinal - rowPrincipal.BOLValorPrevisto);
                else
                    if (valorfinal < rowPrincipal.BOLValorPrevisto)
                    BOLETOSIMPRow.BOLMensagemImp += string.Format("\r\n Desconto: R$ {0:n2}", rowPrincipal.BOLValorPrevisto - valorfinal);
            }

            BOLETOSIMPRow.IntSenha = string.Empty;
            BOLETOSIMPRow.IntUsu = string.Empty;


            //para nao colocar usu�rio e senha nos boletos de homologa��o
            if ((!rowPrincipal.IsBOL_APTNull()) && (rowPrincipal.BOLNome != "NOME DO SACADO"))
            {

                BOLETOSIMPRow.IntUsu = Apartamento.APTUsuarioInternet(Proprietario);
                BOLETOSIMPRow.IntSenha = Apartamento.APTSenhaInternet(Proprietario);

                /*
                if (dBoletos.UsuarioSenhaTableAdapter.Fill(dBoletos.UsuarioSenha, rowPrincipal.BOL_APT) == 1)
                {
                    dBoletos.UsuarioSenhaRow rowUsu = (dBoletos.UsuarioSenhaRow)dBoletos.UsuarioSenha[0];
                    if (rowPrincipal.BOLProprietario)
                    {

                        if (!rowUsu.IsAPTUsuarioInternetProprietarioNull())
                            BOLETOSIMPRow.IntUsu = rowUsu.APTUsuarioInternetProprietario;
                        if (!rowUsu.IsAPTSenhaInternetProprietarioNull())
                            BOLETOSIMPRow.IntSenha = rowUsu.APTSenhaInternetProprietario;
                    }
                    else
                    {
                        if (!rowUsu.IsAPTUsuarioInternetInquilinoNull())
                            BOLETOSIMPRow.IntUsu = rowUsu.APTUsuarioInternetInquilino;
                        if (!rowUsu.IsAPTSenhaInternetInquilinoNull())
                            BOLETOSIMPRow.IntSenha = rowUsu.APTSenhaInternetInquilino;
                    }
                };*/
            }

            

            BOLETOSIMPRow.NNStr2 = NossoNumeroStr;
            if (!rowPrincipal.IsBOLPagamentoNull())
                BOLETOSIMPRow.BOLPagamento = rowPrincipal.BOLPagamento;
            string Composicao = string.Empty;
            string ComposicaoTotal = string.Empty;
            decimal Total = 0;
            foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
            {
                Composicao += Environment.NewLine + rowBOD.BODMensagem;
                ComposicaoTotal += Environment.NewLine + rowBOD.BODValor.ToString("#,##0.00");
                Total += rowBOD.BODValor;
            };
            ComposicaoTotal += "\r\n\r\n" + Total.ToString("#,##0.00");


            BOLETOSIMPRow.Composicao = Composicao;
            BOLETOSIMPRow.ComposicaoTotal = ComposicaoTotal;


            Impresso.dImpBoletoBase1.BOLETOSIMP.AddBOLETOSIMPRow(BOLETOSIMPRow);
            if (BOLETOSIMPRow1 == null)
                BOLETOSIMPRow1 = new List<dImpBoletoBase.BOLETOSIMPRow>();
            BOLETOSIMPRow1.Add(BOLETOSIMPRow);            
        }
        /// <summary>
        /// Corre��o multa.        
        /// </summary>
        /// <returns></returns>
        public bool CorrecaoMulta()
        {
            if (rowPrincipal.IsBOLPagamentoNull())
                return false;
            decimal Total = 0;
            foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                Total += rowBOD.BODValor;
            decimal MultaPaga = rowPrincipal.BOLValorPago - Total;
            if (MultaPaga == 0)
                return false;
            if (MultaPaga > 0.009m)
                IncluiBOD("120001", "Multa por atraso", MultaPaga, false);
            else
                if (MultaPaga < -0.009m)
                IncluiBOD("101000", "Desconto", MultaPaga, false);
            return true;
        }
        /// <summary>
        /// Destrava
        /// </summary>
        /// <returns></returns>
        public bool Destrava()
        {
            if (BOLStatus != StatusBoleto.EmProducao)
                return false;
            else
            {
                rowPrincipal.BOLStatus = (int)StatusBoleto.Valido;
                rowPrincipal.BOLCancelado = false;
                if (BOLStatusRegistro == StatusRegistroBoleto.ComregistroNaoRegistrado)
                    BOLStatusRegistro = DebitoAutomaticoCadastrado() ? StatusRegistroBoleto.BoletoRegistrandoDA : StatusRegistroBoleto.BoletoRegistrando;
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 212", dBoletos.BOLetosTableAdapter);
                    GravaJustificativa("Boleto Liberado", string.Empty);
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                }
                return true;
            }
        }

        /// <summary>
        /// Divide o registro BOD em 2
        /// </summary>
        /// <param name="BOD">BOD</param>
        /// <param name="Valor">Valor da primeira parte</param>
        /// <param name="meses">meses a avan�ar a compet�ncia</param>
        /// <returns></returns>
        public static int? DivideBOD(int BOD, decimal Valor, int meses)
        {
            int? nCompetenciaAtual = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select BODCompetencia from BOletoDetalhe where BOD = @P1", BOD);
            if (nCompetenciaAtual.HasValue)
            {
                Competencia comp = new Competencia(nCompetenciaAtual.Value);
                comp.Add(meses);
                return DivideBOD(BOD, Valor, comp);
            }
            else
                return null;
        }

        /// <summary>
        /// Divide o registro BOD em 2
        /// </summary>
        /// <param name="BOD">BOD</param>
        /// <param name="Valor">Valor da primeira parte</param>
        /// <param name="comp">Compet�ncia da segunda parte</param>
        /// <returns>BOD novo</returns>
        public static int? DivideBOD(int BOD, decimal Valor, Competencia comp = null)
        {
            if (dBoletosSt.BOletoDetalheTableAdapter.FillByBOD(dBoletosSt.BOletoDetalhe, BOD) != 1)
                return null;
            dBoletos.BOletoDetalheRow rowBODOriginal = dBoletosSt.BOletoDetalhe[0];
            return DivideBOD(rowBODOriginal, Valor, comp);

            /*
            if (Framework.PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoC) == rowBODOriginal.BOD_PLA)
                throw new Exception("Boleto com seguro n�o pode ser dividido");
            dBoletos.BOletoDetalheRow rowBODNovo = dBoletosSt.BOletoDetalhe.NewBOletoDetalheRow();
            if (!rowBODOriginal.BODMensagem.Contains("Parc."))
            {
                string BODMensagem = string.Format("{0} Parc.", rowBODOriginal.BODMensagem);
                if (BODMensagem.Length > dBoletosSt.BOletoDetalhe.Columns["BODMensagem"].MaxLength)
                {
                    int Erro = BODMensagem.Length - dBoletosSt.BOletoDetalhe.Columns["BODMensagem"].MaxLength;
                    BODMensagem = string.Format("{0} Parc.", rowBODOriginal.BODMensagem.Substring(0, rowBODOriginal.BODMensagem.Length - Erro));                    
                }
                rowBODOriginal.BODMensagem = BODMensagem;
            }        
            foreach (DataColumn DC in dBoletosSt.BOletoDetalhe.Columns)            
                if (!DC.AutoIncrement)                
                    rowBODNovo[DC] = rowBODOriginal[DC];                            
            if (comp != null)
                rowBODNovo.BODCompetencia = comp.CompetenciaBind;
            rowBODNovo.BODValor = rowBODOriginal.BODValor - Valor;
            rowBODOriginal.BODValor = Valor;
            dBoletosSt.BOletoDetalhe.AddBOletoDetalheRow(rowBODNovo);            
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto - Boleto.cs - 1183", dBoletosSt.BOletoDetalheTableAdapter);
                dBoletosSt.BOletoDetalheTableAdapter.Update(rowBODNovo);
                dBoletosSt.BOletoDetalheTableAdapter.Update(rowBODOriginal);
                rowBODNovo.AcceptChanges();
                rowBODOriginal.AcceptChanges();
                if (dBoletosSt.BODxBODTableAdapter.FillbyBODD(dBoletosSt.BODxBOD, rowBODOriginal.BOD) > 0)
                    AmarraBODs(dBoletosSt.BODxBOD[0].BODOrigem, rowBODNovo.BOD);
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                return null;
            }
            return rowBODNovo.BOD;*/
        }

        /// <summary>
        /// Divide o registro BOD em 2
        /// </summary>
        /// <param name="rowBODOriginal"></param>
        /// <param name="Valor"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public static int? DivideBOD(dBoletos.BOletoDetalheRow rowBODOriginal, decimal Valor, Competencia comp = null)
        {
            dBoletos dBoletos1 = (dBoletos)rowBODOriginal.Table.DataSet;
            if (PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoC) == rowBODOriginal.BOD_PLA)
                throw new Exception("Boleto com seguro n�o pode ser dividido");
            dBoletos.BOletoDetalheRow rowBODNovo = dBoletos1.BOletoDetalhe.NewBOletoDetalheRow();
            if (!rowBODOriginal.BODMensagem.Contains("Parc."))
            {
                string BODMensagem = string.Format("{0} Parc.", rowBODOriginal.BODMensagem);
                if (BODMensagem.Length > dBoletos1.BOletoDetalhe.Columns["BODMensagem"].MaxLength)
                {
                    int Erro = BODMensagem.Length - dBoletos1.BOletoDetalhe.Columns["BODMensagem"].MaxLength;
                    BODMensagem = string.Format("{0} Parc.", rowBODOriginal.BODMensagem.Substring(0, rowBODOriginal.BODMensagem.Length - Erro));
                }
                rowBODOriginal.BODMensagem = BODMensagem;
            }
            foreach (DataColumn DC in dBoletos1.BOletoDetalhe.Columns)
                if (!DC.AutoIncrement)
                    rowBODNovo[DC] = rowBODOriginal[DC];
            if (comp != null)
                rowBODNovo.BODCompetencia = comp.CompetenciaBind;
            rowBODNovo.BODValor = rowBODOriginal.BODValor - Valor;
            rowBODOriginal.BODValor = Valor;
            dBoletos1.BOletoDetalhe.AddBOletoDetalheRow(rowBODNovo);
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto - Boleto.cs - 1183", dBoletos1.BOletoDetalheTableAdapter);
                dBoletos1.BOletoDetalheTableAdapter.Update(rowBODNovo);
                dBoletos1.BOletoDetalheTableAdapter.Update(rowBODOriginal);
                rowBODNovo.AcceptChanges();
                rowBODOriginal.AcceptChanges();
                if (dBoletos1.BODxBODTableAdapter.FillbyBODD(dBoletos1.BODxBOD, rowBODOriginal.BOD) > 0)
                    AmarraBODs(dBoletos1.BODxBOD[0].BODOrigem, rowBODNovo.BOD);
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                return null;
            }
            return rowBODNovo.BOD;
        }

        /*
        /// <summary>
        /// Exporta o boleto para o access
        /// </summary>
        /// <remarks>Nao atualiza</remarks>
        /// <returns></returns>        
        public bool ExportaAccesNovo()
        {
            try
            {
                if (!Encontrado)
                    return false;
                //VirOleDb.TableAdapter VirOleDbLocal = VirOleDb.TableAdapter.STTableAdapter;                
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from Boleto where Boleto.[N�mero Boleto] = @P1", rowPrincipal.BOL);
                if ((!rowPrincipal.IsBOL_APTNull())
                      &&
                     (!rowPrincipal.BOLTipoCRAI.EstaNoGrupo("S", "E"))
                      &&
                     (!rowPrincipal.BOLCancelado)
                      &&
                     (BOLStatus != StatusBoleto.Saldinho)
                      &&
                     (BOLStatus != StatusBoleto.Acumulado)
                   )
                    return GravaAccess();
                else
                    return true;
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                return false;
            }
        }
        */

        /// <summary>
        /// Gera BOD e grava no banco de dados
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="APT"></param>
        /// <param name="Proprietario"></param>
        /// <param name="PLA"></param>
        /// <param name="ComCondominio"></param>
        /// <param name="compet"></param>
        /// <param name="Vencimento"></param>
        /// <param name="Mensagem"></param>
        /// <param name="Valor"></param>
        /// <param name="Previsto">se o valor era previsto</param>
        /// <param name="CTL"></param>
        /// <param name="Acordo_BOL"></param>
        /// <returns></returns>
        public static dBoletos.BOletoDetalheRow GerarBOD(int CON, int? APT, bool Proprietario, string PLA, bool ComCondominio, Competencia compet,
                                                   DateTime Vencimento, string Mensagem, decimal Valor, bool Previsto, int? CTL = null, int? Acordo_BOL = null)
        {

            dBoletos.BOletoDetalheRow rowBOD = dBoletosSt.BOletoDetalhe.NewBOletoDetalheRow();
            rowBOD.BOD_CON = CON;
            if (APT.HasValue)
                rowBOD.BOD_APT = APT.Value;

            rowBOD.BODProprietario = Proprietario;
            rowBOD.BOD_PLA = PLA;
            rowBOD.BODComCondominio = ComCondominio;
            rowBOD.BODCompetencia = compet.CompetenciaBind;
            rowBOD.BODPrevisto = Previsto;
            rowBOD.BODData = Vencimento;
            rowBOD.BODMensagem = Mensagem;
            rowBOD.BODValor = Valor;
            if (CTL.HasValue)
                rowBOD.BOD_CTL = CTL.Value;
            if (Acordo_BOL.HasValue)
                rowBOD.BODAcordo_BOL = Acordo_BOL.Value;
            dBoletosSt.BOletoDetalhe.AddBOletoDetalheRow(rowBOD);
            dBoletosSt.BOletoDetalheTableAdapter.EmbarcaEmTransST();
            dBoletosSt.BOletoDetalheTableAdapter.Update(rowBOD);
            rowBOD.AcceptChanges();
            return rowBOD;
        }

        /// <summary>
        /// Imprimir
        /// </summary>
        /// <param name="Destino"></param>
        /// <param name="segundaVia"></param>
        public bool Imprimir(dllImpresso.Botoes.Botao Destino, bool segundaVia = true)
        {
            ImpBoletoBal ImpSB_BAL = null;
            if (rowSBO != null)
                rowPrincipal.BOLMensagemImp = string.Empty;
            string DestinatarioDefault = string.Empty;
            ImpBoletoBase ImpSB;
            if (rowPrincipal.BOLTipoCRAI == "A")
                ImpSB = new ImpBoletoAcordo();
            else if (rowPrincipal.BOLTipoCRAI == "S")
            {
                ImpSegundaVia ImpSBs = new ImpSegundaVia();
                ImpSBs.xrTitulo.Text = "Seguro Conte�do";
                ImpSB = ImpSBs;
            }

            else if ((rowPrincipal.BOLTipoCRAI == "C") && (!segundaVia))
            {
                ImpSB_BAL = new ImpBoletoBal();
                ImpSB_BAL.balancete = Balancete;
                ImpSB = ImpSB_BAL;
            }
            else
            {
                ImpSB = new ImpSegundaVia();
                if (!segundaVia)
                    ((ImpSegundaVia)ImpSB).xrTitulo.Visible = false;
            }
            if ((Destino == dllImpresso.Botoes.Botao.email_comprovante) || (Destino == dllImpresso.Botoes.Botao.imprimir_comprovante) || (Destino == dllImpresso.Botoes.Botao.pdf_comprovante))
                ComprovanteDeEndereco = true;
            if (rowPrincipal.BOLProtestado)
            {
                MessageBox.Show("N�o � permitida a impress�o de boletos protestados!!!\r\nEntrar em contato com a Dra BLANCA advogada respons�vel!!");
                return false;
            };
            if (DestinatarioDefault == string.Empty)
                DestinatarioDefault = EmailResponsavel;
            if (segundaVia)
                IncluicustoSegundaVia(true);
            CarregaLinha(ImpSB, true, true);
            ImpSB.bindingSourcePrincipal.Sort = "Correio,BLOCodigo,APTNumero";
            if ((Destino == dllImpresso.Botoes.Botao.imprimir_frente)
                ||
                (Destino == dllImpresso.Botoes.Botao.PDF_frente)
                ||
                (Destino == dllImpresso.Botoes.Botao.email))
                ImpSB.ComRemetente = false;
            switch (Destino)
            {
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.imprimir_frente:
                case dllImpresso.Botoes.Botao.imprimir_comprovante:
                    ImpSB.Apontamento(CON, 2);
                    ImpSB.CreateDocument();
                    ImpSB.Print();
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    ImpSB.CreateDocument();
                    ImpSB.ShowPreview();
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                case dllImpresso.Botoes.Botao.PDF_frente:
                case dllImpresso.Botoes.Botao.pdf_comprovante:
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Title = "PDF";
                    saveFileDialog1.DefaultExt = "pdf";
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        ImpSB.CreateDocument();
                        ImpSB.ExportOptions.Pdf.Compressed = true;
                        ImpSB.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                        ImpSB.ExportToPdf(saveFileDialog1.FileName);
                    }
                    break;
                case dllImpresso.Botoes.Botao.email:
                case dllImpresso.Botoes.Botao.email_comprovante:
                    string NomeArquivo = string.Empty;
                    string CBs = string.Empty;
                    foreach (dImpBoletoBase.BOLETOSIMPRow rowImp in ImpSB.dImpBoletoBase1.BOLETOSIMP)
                    {
                        NomeArquivo += ((NomeArquivo == string.Empty ? string.Empty : "-") + rowImp.BOL);
                        if (rowImp.IsBOLPagamentoNull())
                            CBs += (CBs == string.Empty ? string.Empty : "\r\n\r\n\r\n") + "Boleto:" + rowImp.BOL.ToString() + "\r\nVencimento: " + rowImp.BOLVencto.ToString("dd/MM/yyyy") + "\r\nValor: " + rowImp.BOLValorPrevisto.ToString("n2") + "\r\nLinha digit�vel: " + rowImp.BOLCodbarstr;
                        else
                            CBs += (CBs == string.Empty ? string.Empty : "\r\n\r\n\r\n") + "Boleto:" + rowImp.BOL.ToString() + "\r\nVencimento: " + rowImp.BOLVencto.ToString("dd/MM/yyyy") + "\r\nValor: " + rowImp.BOLValorPrevisto.ToString("n2") + "\r\nData do pagamento: " + rowImp.BOLPagamento.ToString("dd/MM/yyyy");
                    };
                    if (NomeArquivo != string.Empty)

                        if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(string.Empty, NomeArquivo, ImpSB, "Em anexo os boletos:\r\n\r\n" + CBs, "Boletos: " + NomeArquivo, DestinatarioDefault))
                        {
                            if (rowSBO == null)
                                GravaJustificativa("Enviado para:" + VirEmailNeon.EmailDiretoNeon.EmalST.UltimoEndereco, string.Empty);
                            MessageBox.Show("E.mail enviado");
                        }
                        else
                            if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                        {
                            MessageBox.Show("Falha no envio");
                            Exception eX = VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro;
                            while (eX != null)
                            {
                                MessageBox.Show(eX.Message);
                                eX = eX.InnerException;
                            };
                        }
                        else
                            MessageBox.Show("Cancelado");
                    break;
            }
            if ((ImpSB_BAL != null) && !ImpSB_BAL.Cabe)
                return false;
            else
                return true;
        }


        /*
        /// <summary>
        /// Incluir um BOD em um boleto existente
        /// </summary>
        /// <param name="PLA"></param>
        /// <param name="Mensagem"></param>
        /// <param name="Valor"></param>
        /// <param name="Previsto">se o valor era previsto</param>
        /// <param name="BODAcordo_BOL"></param>
        /// <returns></returns>
        public dBoletos.BOletoDetalheRow IncluiBOD(string PLA, string Mensagem, decimal Valor, bool Previsto, int? BODAcordo_BOL = null)
        {
            return IncluiBOD(PLA, Mensagem, Valor, true, Previsto, null, null, BODAcordo_BOL);
        }
        */

        /// <summary>
        /// Inlcuir BOD amarrado a um PAG
        /// </summary>
        /// <param name="PLA"></param>
        /// <param name="Mensagem"></param>
        /// <param name="Valor"></param>
        /// <param name="Previsto">se o valor era previsto</param>
        /// <param name="PAG"></param>
        /// <param name="CTL"></param>
        /// <returns></returns>
        public dBoletos.BOletoDetalheRow IncluiBOD(string PLA, string Mensagem, decimal Valor, bool Previsto, int? PAG, int? CTL)
        {
            return IncluiBOD(PLA, Mensagem, Valor, true, Previsto, PAG, CTL, null);
        }

        /// <summary>
        /// Reagenda o BOD
        /// </summary>
        /// <param name="BOD">BOD</param>
        /// <param name="meses">Quantos m�ses avan�ar</param>
        /// <returns>Se deu certo</returns>
        public static bool ReagendaBOD(int BOD, int meses = 1)
        {
            int? nCompetenciaAtual = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select BODCompetencia from BOletoDetalhe where BOD = @P1", BOD);
            if (nCompetenciaAtual.HasValue)
            {
                Competencia comp = new Competencia(nCompetenciaAtual.Value);
                comp.Add(meses);
                return ReagendaBOD(BOD, comp);
            }
            else
                return false;
        }

        /// <summary>
        /// Reagenda o BOD
        /// </summary>
        /// <param name="BOD">BOD</param>
        /// <param name="comp">Compet�ncia</param>
        /// <returns>Se deu certor</returns>
        public static bool ReagendaBOD(int BOD, Competencia comp)
        {
            if (dBoletosSt.BOletoDetalheTableAdapter.FillByBOD(dBoletosSt.BOletoDetalhe, BOD) != 1)
                return false;
            dBoletos.BOletoDetalheRow rowBOD = dBoletosSt.BOletoDetalhe[0];
            rowBOD.BODCompetencia = comp.CompetenciaBind;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto - Boleto.cs - 1133", dBoletosSt.BOletoDetalheTableAdapter);
                dBoletosSt.BOletoDetalheTableAdapter.Update(rowBOD);
                rowBOD.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                return false;
            }
            return true;
        }


        /// <summary>
        /// Reativa o boleto que estava cancelado
        /// </summary>
        public void Reativa(string Justificativa)
        {
            if (!rowPrincipal.BOLCancelado)
                return;
            rowPrincipal.BOLCancelado = false;
            rowPrincipal.BOLExportar = true;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 3657", dBoletos.BOLetosTableAdapter);
                dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                rowPrincipal.AcceptChanges();
                //ExportaAcces();                
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro em reativa: " + e.Message, e);
            }
        }


        /// <summary>
        /// Cheque para seguro conte�do
        /// </summary>
        /// <returns></returns>
        public bool RegistraChSeguro()
        {
            if (rowPrincipal.BOLTipoCRAI == "S")
                return true;
            dBoletos.BOletoDetalheRow rowBODSeguro = null;
            foreach (dBoletos.BOletoDetalheRow RowBOD in rowPrincipal.GetBOletoDetalheRows())
                if (RowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoC))
                    rowBODSeguro = RowBOD;
            if (rowBODSeguro == null)
                return false;

            int? PAG = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT PAG FROM BODxPAG WHERE BOD = @P1", rowBODSeguro.BOD);
            if (PAG.HasValue)
                return true;

            int FRNNeon = (DSCentral.EMP == 1) ? 1119 : 172;
            string Favorecido;
            VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("SELECT FRNNome FROM FORNECEDORES WHERE (FRN = @P1)", out Favorecido, FRNNeon);
            dllImpostos.SolicitaRetencao[] Retencoes = null;
            DateTime DataCh = new DateTime(rowPrincipal.BOLVencto.Year, rowPrincipal.BOLVencto.Month, 1).AddMonths(1).AddDays(15);

            string PLA = PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoD);
            string descricao = string.Format("{0} {1} {2}", Apartamento.BLOCodigo, Apartamento.APTNumero, rowBODSeguro.BODMensagem);
            int SPL = (DSCentral.EMP == 1) ? 398 : 144;
            return ContaPagarProc.dNOtAs.dNOtAsSt.IncluiNotaSimples_Efetivo(0, DataCh, DataCh, new Competencia(DataCh), NOATipo.Avulsa, Favorecido, Condominio.CON,
                                                         PAGTipo.cheque,"", true, PLA, SPL, descricao, rowBODSeguro.BODValor, FRNNeon,
                                                         false, ContaPagarProc.dNOtAs.TipoChave.BOL, rowBODSeguro.BOD,null,null,null,null,NOAStatus.Cadastrada, Retencoes).HasValue;

        }

        /// <summary>
        /// Remove BOD
        /// </summary>
        /// <param name="BOD"></param>
        /// <returns></returns>
        public bool RemoverBOD(int BOD)
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 742", dBoletos.BOLetosTableAdapter, dBoletos.BOletoDetalheTableAdapter);
                dBoletos.BOletoDetalheRow rowBOD = dBoletos.BOletoDetalhe.FindByBOD(BOD);
                if (rowBOD == null)
                    return false;
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete BODxPAG where BOD = @P1", BOD);
                rowBOD.Delete();
                dBoletos.BOletoDetalheTableAdapter.Update(dBoletos.BOletoDetalhe);
                dBoletos.BOletoDetalhe.AcceptChanges();
                rowPrincipal.BOLValorPrevisto = 0;
                rowPrincipal.BOLValorOriginal = 0;
                foreach (dBoletos.BOletoDetalheRow rowBODdet in rowPrincipal.GetBOletoDetalheRows())
                    rowPrincipal.BOLValorPrevisto += rowBODdet.BODValor;
                    //if(rowBODdet.IsBODPrevistoNull() || rowBODdet.BODPrevisto)
                    //    rowPrincipal.BOLValorOriginal += rowBODdet.BODValor;
                dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                rowPrincipal.AcceptChanges();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                return true;
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro: " + e.Message, e);
            }
        }

        //private string NossoNumeroStrXXX;
        //private Boletos.Boleto.dExpImpBol dExpImpBol;
        //private Boletos.Boleto.dExpImpBolTableAdapters.ExpBoletoTableAdapter ExpBoletoTableAdapter;
        //private Boletos.Boleto.dExpImpBolTableAdapters.DadosComplementaresTableAdapter DadosComplementaresTableAdapter;
        //private Boletos.Boleto.dExpImpBolTableAdapters.BoletoPagamentosTableAdapter BoletoPagamentosTableAdapter;


        /*
        private void IniciaExp(){
            if (dExpImpBol == null) {
                dExpImpBol = new dExpImpBol();
                ExpBoletoTableAdapter = new Boletos.Boleto.dExpImpBolTableAdapters.ExpBoletoTableAdapter();
                ExpBoletoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Access);
                BoletoPagamentosTableAdapter = new Boletos.Boleto.dExpImpBolTableAdapters.BoletoPagamentosTableAdapter();
                BoletoPagamentosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Access);
                DadosComplementaresTableAdapter = new Boletos.Boleto.dExpImpBolTableAdapters.DadosComplementaresTableAdapter();
                DadosComplementaresTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                
            }
        }
        */

        /*
        /// <summary>
        /// Grava Justificativa para altera��o do boleto
        /// </summary>
        /// <param name="Justificativa">justificativa</param>
        /// <param name="ValorAcordado">Valor acertado</param>
        /// <param name="DataLimite">Data Acordada</param>
        /// <param name="EmailJuridico">Email para notifica��o ao jur�dico</param>
        public bool GravaJustificativa(string Justificativa, decimal? ValorAcordado=null, DateTime? DataLimite=null, string EmailJuridico="")
        {
            bool retorno = true;
            if (rowSBO == null)
            {
                string Historico = (rowPrincipal.IsBOLJustificativaNull() ? "" : rowPrincipal.BOLJustificativa + "\r\n\r\n");
                Justificativa = String.Format("{0} - {1}", DateTime.Now, Justificativa);
                rowPrincipal.BOLJustificativa = Historico + Justificativa;
                if (ValorAcordado.HasValue)
                {
                    rowPrincipal.BOLValorAcordado = ValorAcordado.Value;
                    rowPrincipal.BOLDataLimiteJust = DataLimite.Value;
                    //AlteracaoNoRegistroBoleto(); Ao reativar lembrar que nao pode ser chamado aqui porque pode estar dentro de uma transa��o                      
                };
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos boleto GravaJustificativa - 473", dBoletos.BOLetosTableAdapter);
                    dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                    rowPrincipal.AcceptChanges();
                    VirMSSQL.TableAdapter.CommitSQL();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                }
                if (EmailJuridico != "")
                {
                    string Conteudo = "Condom�nio: " + Apartamento.Condominio.Nome + Environment.NewLine;
                    Conteudo += "Bloco: " + Apartamento.BLOCodigo + Environment.NewLine;
                    Conteudo += "N�mero: " + Apartamento.APTNumero + "\r\n\r\n";
                    Conteudo += "N�mero do Boleto: " + rowPrincipal.BOL.ToString() + Environment.NewLine;
                    Conteudo += "Compet�ncia: " + Competencia.ToString() + Environment.NewLine;
                    Conteudo += "Vencimeto: " + rowPrincipal.BOLVencto.ToString("dd/MM/yyyy") + Environment.NewLine;
                    Conteudo += "Valor original: " + rowPrincipal.BOLValorPrevisto.ToString("n2") + Environment.NewLine;
                    Conteudo += "Tipo: " + rowPrincipal.BOLTipoCRAI.ToString();
                    Conteudo += "\r\n\r\n\r\n**** Evento:\r\n";
                    Conteudo += Justificativa;
                    if (Historico != "")
                        Conteudo += "\r\n\r\n\r\n**** Hist�rico:\r\n" + Historico;
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(EmailJuridico, Conteudo, "Aviso de evento em boleto");
                }
            }
            return retorno;
        }
        */

        /*
        /// <summary>
        /// Grava Justificativa para altera��o do boleto
        /// </summary>
        /// <param name="Justificativa">justificativa</param>
        /// <param name="ValorAcordado">Valor acertado</param>
        /// <param name="DataLimite">Data Acordada</param>
        /// <param name="EmailJuridico">Email para notifica��o ao jur�dico</param>
        public void GravaJustificativa(string Justificativa, decimal ValorAcordado, DateTime DataLimite, string EmailJuridico) 
        {
            GravaJustificativaGenerico(Justificativa, ValorAcordado, DataLimite, EmailJuridico);
        }
        */



        /*
        /// <summary>
        /// Grava Observa��o
        /// </summary>
        /// <param name="Obs">Observa��o</param>        
        /// <param name="EmailJuridico">Email para notifica��o ao jur�dico</param>
        public override void GravaJustificativa(string Obs, string EmailJuridico)
        {
            GravaJustificativa(Obs, null, null, EmailJuridico);            
        }
        */


        /*
        /// <summary>
        /// Salva as latera��es sem registrar nada. Usar somente na cria��o de um boleto onde s�o feitas altera��es em um try que contenha a cria��o e a altera��o.
        /// </summary>
        /// <returns></returns>
        public void SalvarAlteracoesSemRegistrar()
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos boleto.cs - 817",dBoletos.BOLetosTableAdapter, dBoletos.BOletoDetalheTableAdapter);                
                if (!dBoletos.BOLetosTableAdapter.STTransAtiva())
                    throw new Exception("Uso incorreto da fun��o");
                dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                rowPrincipal.AcceptChanges();
                foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                {
                    dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                    rowBOD.AcceptChanges();
                }
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                throw e;
            }
        }
        */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Justificativa"></param>
        /// <returns></returns>
        public bool SalvarAlteracoes(string Justificativa)
        {
            return SalvarAlteracoes(Justificativa, false);
        }
        /// <summary>
        /// Salva as altera��es registrando o coment�rio
        /// </summary>
        /// <param name="Justificativa">justificativa</param>
        /// <param name="Silencioso"></param>
        /// <returns>se foi gravado</returns>
        public bool SalvarAlteracoes(string Justificativa, bool Silencioso)
        {
            List<dBoletos.BOletoDetalheRow> BODsAlterados = new List<dBoletos.BOletoDetalheRow>();
            List<dBoletos.BOletoDetalheRow> RowsRemover = new List<dBoletos.BOletoDetalheRow>();
            decimal Total = 0;
            decimal TotalPrevisto = 0;
            List<PLAPadrao> PLAExtraNominal = new List<PLAPadrao>(new PLAPadrao[] { PLAPadrao.MultasAtraso, PLAPadrao.PagamentoExtraD, PLAPadrao.Saldinho });
            foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
            {
                if (rowBOD.BODValor != decimal.Round(rowBOD.BODValor, 2))
                    rowBOD.BODValor = decimal.Round(rowBOD.BODValor, 2);
                if (rowBOD.RowState != DataRowState.Unchanged)
                    BODsAlterados.Add(rowBOD);
                //if (rowPrincipal.IsBOLPagamentoNull() || (rowBOD.BOD_PLA != Framework.PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho)))
                Total += rowBOD.BODValor;
                if (rowBOD.IsBODPrevistoNull())
                    if ((rowBOD.BOD_PLA == string.Empty) || (rowBOD.BOD_PLA == string.Empty))
                        rowBOD.BODPrevisto = false;
                    else
                        rowBOD.BODPrevisto = true;
                if (rowPrincipal.IsBOLPagamentoNull())
                {
                    if (rowBOD.BODPrevisto)
                        TotalPrevisto += rowBOD.BODValor;
                }
                else
                    if (!PLAExtraNominal.Contains(Framework.datasets.dPLAnocontas.PlaPadrao(rowBOD.BOD_PLA)))
                    TotalPrevisto += rowBOD.BODValor;
            };

            foreach (dBoletos.BOletoDetalheRow rowBOD in BODrowsOriginal)
                if (rowBOD.RowState.EstaNoGrupo(DataRowState.Deleted, DataRowState.Detached))
                    BODsAlterados.Add(rowBOD);
                else
                    if (rowBOD.IsBOD_BOLNull())
                    RowsRemover.Add(rowBOD);
            ;

            if (rowPrincipal.IsBOLPagamentoNull())
            {
                if (rowPrincipal.BOLValorPrevisto != Total)
                    rowPrincipal.BOLValorPrevisto = Total;
            }
            else
                if (rowPrincipal.BOLValorPrevisto != TotalPrevisto)
                rowPrincipal.BOLValorPrevisto = TotalPrevisto;

            if (rowPrincipal.IsBOLValorOriginalNull())
                rowPrincipal.BOLValorOriginal = TotalPrevisto;
            SortedList CamposAlterados = VirDB.VirtualTableAdapter.CamposEfetivamenteAlterados(rowPrincipal);

            if ((CamposAlterados.Count > 0) || (BODsAlterados.Count > 0) || (Justificativa != string.Empty) || (RowsRemover.Count > 0))
            {
                if (Justificativa == string.Empty)
                    if (!VirInput.Input.Execute("Justificativa", ref Justificativa, true))
                        return false;
                ;
                string Just = String.Format("{0} - Boleto alterado por {1}\r\nJustificativa:{2}", DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome, Justificativa);
                rowPrincipal.BOLJustificativa = (rowPrincipal.IsBOLJustificativaNull() ? string.Empty : rowPrincipal.BOLJustificativa + "\r\n\r\n") + Just;
                //listar alteracoes
                if (CamposAlterados.ContainsKey("BOLValorPrevisto"))
                    rowPrincipal.BOLJustificativa += String.Format("\r\nValor: {0:n2} - > {1:n2}", (decimal)rowPrincipal["BOLValorPrevisto", DataRowVersion.Original], rowPrincipal.BOLValorPrevisto);
                if (CamposAlterados.ContainsKey("BOLVencto"))
                    rowPrincipal.BOLJustificativa += String.Format("\r\nVencimento: {0:dd/MM/yyyy} - > {1:dd/MM/yyyy}", (DateTime)rowPrincipal["BOLVencto", DataRowVersion.Original], rowPrincipal.BOLVencto);
                if (CamposAlterados.ContainsKey("BOLNome"))
                    rowPrincipal.BOLJustificativa += String.Format("\r\nDestinat�rio: {0} - > {1}", rowPrincipal["BOLNome", DataRowVersion.Original], rowPrincipal.BOLNome);
                if (CamposAlterados.ContainsKey("BOLEndereco"))
                    rowPrincipal.BOLJustificativa += String.Format("\r\nEndere�o: {0} - > {1}", rowPrincipal["BOLEndereco", DataRowVersion.Original], rowPrincipal.BOLEndereco);
                if ((CamposAlterados.ContainsKey("BOLCompetenciaMes")) || (CamposAlterados.ContainsKey("BOLCompetenciaAno")))
                {
                    Competencia compOrig = new Competencia((Int16)rowPrincipal["BOLCompetenciaMes", DataRowVersion.Original], (Int16)rowPrincipal["BOLCompetenciaAno", DataRowVersion.Original]);
                    Competencia compNova = new Competencia((Int16)rowPrincipal["BOLCompetenciaMes", DataRowVersion.Current], (Int16)rowPrincipal["BOLCompetenciaAno", DataRowVersion.Current]);
                    rowPrincipal.BOLJustificativa += String.Format("\r\nCompet�ncia: {0} - > {1}", compOrig, compNova);
                };
                if (CamposAlterados.ContainsKey("BOLProprietario"))
                    if (rowPrincipal.BOLProprietario)
                        rowPrincipal.BOLJustificativa += "\r\nRespons�vel: Inquilino - > Propriet�rio";
                    else
                        rowPrincipal.BOLJustificativa += "\r\nRespons�vel: Propriet�rio - > Inquilino";
                if ((BODsAlterados.Count > 0) || (RowsRemover.Count > 0))
                {
                    rowPrincipal.BOLJustificativa += "\r\n  Itens alterados: ";
                    foreach (dBoletos.BOletoDetalheRow rowBOD in RowsRemover)
                        rowPrincipal.BOLJustificativa += "\r\n  Removido: " + DadosLinhasOriginais[rowBOD.GetHashCode()];
                    foreach (dBoletos.BOletoDetalheRow rowBOD in BODsAlterados)
                        switch (rowBOD.RowState)
                        {
                            case DataRowState.Added:
                                rowPrincipal.BOLJustificativa += String.Format("\r\n  Inclu�do: {0}  +{1:n2}", rowBOD.BODMensagem, rowBOD.BODValor);
                                break;
                            case DataRowState.Deleted:
                                rowPrincipal.BOLJustificativa += "\r\n  Exclu�do: " + DadosLinhasOriginais[rowBOD.GetHashCode()];
                                break;
                            case DataRowState.Detached:
                                break;
                            case DataRowState.Modified:
                                decimal ValorOriginal = (decimal)rowBOD["BODValor", DataRowVersion.Original];
                                if (ValorOriginal != rowBOD.BODValor)
                                    rowPrincipal.BOLJustificativa += String.Format("\r\n  {0}: {1:n2} - > {2:n2}", rowBOD.BODMensagem, ValorOriginal, rowBOD.BODValor);
                                else
                                    rowPrincipal.BOLJustificativa += String.Format("\r\n  {0} - > {1}: {2:n2}", DadosLinhasOriginais[rowBOD.GetHashCode()], rowBOD.BODMensagem, rowBOD.BODValor);
                                break;
                            case DataRowState.Unchanged:
                                break;
                            default:
                                break;
                        };
                };
                rowPrincipal.BOLA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                rowPrincipal.BOLDATAA = DateTime.Now;

                rowPrincipal.BOLExportar = (StatusBoletoEditavel.Contains(BOLStatus));
                rowPrincipal.SetBOLDataLimiteJustNull();
                rowPrincipal.SetBOLValorAcordadoNull();
                if (rowPrincipal.IsBOLValorOriginalNull())
                    rowPrincipal.BOLValorOriginal = (decimal)rowPrincipal["BOLValorPrevisto", DataRowVersion.Original];
                if (!Silencioso)
                    if ((CamposAlterados.ContainsKey("BOLValorPrevisto")) && (rowPrincipal.BOLValorOriginal > rowPrincipal.BOLValorPrevisto))
                    {
                        if (!AlteracaoNoRegistroBoleto(StatusRegistroBoleto.AlterarRegistroValor))
                            return false;
                    }
                    else if (CamposAlterados.ContainsKey("BOLVencto"))
                        if (!AlteracaoNoRegistroBoleto(StatusRegistroBoleto.AlterarRegistroData))
                            return false;
                try
                {

                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 638", dBoletos.BOLetosTableAdapter, dBoletos.BOletoDetalheTableAdapter);
                    dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                    dBoletos dBoletosX = (dBoletos)rowPrincipal.Table.DataSet;
                    dBoletos.BOletoDetalheTableAdapter.Update(dBoletosX.BOletoDetalhe);
                    /*
                    foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                        if (rowBOD.RowState.EstaNoGrupo( DataRowState.Modified, DataRowState.Added))
                            dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                    
                    foreach (dBoletos.BOletoDetalheRow rowBOD in RowsRemover)
                        dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                    */
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    rowPrincipal.AcceptChanges();
                    //foreach (dBoletos.BOletoDetalheRow rowBOD in BODsAlterados)
                    //    if ((rowBOD.RowState != DataRowState.Deleted) && (rowBOD.RowState != DataRowState.Detached))
                    //        rowBOD.AcceptChanges();

                    dBoletosX.BOletoDetalhe.AcceptChanges();
                    /*
                    if (rowPrincipal.BOLExportar)
                        ReExportaTh();
                    else 
                    {
                        string ComandoApaga = "delete from [boleto - pagamentos] where [N�mero Boleto] = @P1;";
                        VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL);
                        ComandoApaga = "delete from boleto where [N�mero Boleto] = @P1;";
                        VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowPrincipal.BOL); 
                    }
                    */
                    return true;
                }
                catch (Exception e)
                {
                    UltimoErro = e.Message;
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                    rowPrincipal.RejectChanges();
                    foreach (dBoletos.BOletoDetalheRow rowBOD in BODsAlterados)
                        if ((rowBOD.RowState != DataRowState.Deleted) && (rowBOD.RowState != DataRowState.Detached))
                            rowBOD.RejectChanges();
                    throw new Exception(UltimoErro, e);
                    //return false;
                }

            }
            else
                return true;
        }

        /// <summary>
        /// Balancet a ser aplicado no impresso
        /// </summary>
        public ABS_balancete Balancete
        {
            get { return balancete; }
            set { balancete = value; }
        }

        /*
        /// <summary>
        /// Linha da tabela de boletos
        /// </summary>
        public dBoletos.BOLetosRow rowPrincipal;*/

        /// <summary>
        /// BOL
        /// </summary>
        public int BOL
        {
            get { return rowPrincipal.BOL; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool BoletoNovencimento
        {
            set
            {
                if (value != _BoletoNovencimento)
                {
                    _BoletoNovencimento = value;
                    boletoCalculado = null;
                }
            }
        }

        /// <summary>
        /// Status do Boleto (rowPrincipal.BOLSTatus)
        /// </summary>
        public StatusBoleto BOLStatus
        {
            get
            {
                if (!Encontrado)
                    return StatusBoleto.Erro;
                return (StatusBoleto)rowPrincipal.BOLStatus;
            }
            set
            {
                rowPrincipal.BOLStatus = (int)value;
            }
        }

        /// <summary>
        /// Status do registro do Boleto (rowPrincipal.BOLSTatusRegistro)
        /// </summary>
        public StatusRegistroBoleto BOLStatusRegistro
        {
            get
            {
                if (!Encontrado)
                    return StatusRegistroBoleto.BoletoSemRegistro;
                return (StatusRegistroBoleto)rowPrincipal.BOLStatusRegistro;
            }
            set
            {
                rowPrincipal.BOLStatusRegistro = (int)value;
            }
        }

        /// <summary>
        /// C�digo de barras
        /// </summary>
        public string CodigoBarras
        {
            get
            {
                return BoletoCalculado.codigoBarras;
            }
        }

        /// <summary>
        /// dBoletosSt
        /// </summary>
        public static dBoletos dBoletosSt
        {
            get
            {
                if (_dBoletosSt == null)
                    _dBoletosSt = new dBoletos();
                return _dBoletosSt;
            }
        }

        /// <summary>
        /// Retorna o e.mail do respons�vel pelo apartamento
        /// </summary>
        /// <returns>e.mail</returns>
        /// <remarks>Use o m�todo QuerEmail para saber se deve ser enviado email automaticamente</remarks>
        public string EmailResponsavel
        {
            get
            {
                if (Apartamento == null)
                    return string.Empty;
                return Apartamento.PESEmail(Proprietario);
            }
        }

        /// <summary>
        /// Empresa do boleto
        /// </summary>
        public int EMP
        {
            get
            {
                if (eMP == 0)
                    eMP = DSCentral.EMP;
                return eMP;
            }
            set { eMP = value; }
        }

        /// <summary>
        /// Forma de pagamento deste boleto
        /// </summary>
        /// <returns></returns>
        public FormaPagamento FormaPagamento
        {
            get
            {
                if (rowPrincipal == null)
                    return FormaPagamento.EntreigaPessoal;
                else
                    return (FormaPagamento)rowPrincipal.BOL_FPG;
            }
        }

        /// <summary>
        /// Linha digit�vel
        /// </summary>
        public string LinhaDigitavel
        {
            get
            {
                return BoletoCalculado.linhadigitavel;
            }
        }



        /// <summary>
        /// Nosso n�mero
        /// </summary>
        public string NossoNumeroStr
        {
            get
            {
                if (BoletoCalculado == null)
                    throw new Exception("******** VALIDACAO 3");
                //if(Validar)
                //  if(BoletoCalculado.NossoNumeroStr != NossoNumeroStrXXX)
                //    throw new Exception("******** VALIDACAO 4");
                return BoletoCalculado.NossoNumeroStr;
            }
        }

        /*
        private AbstratosNeon.ABS_Apartamento _BOLApartamento;

        /// <summary>
        /// Apartamento do boleto
        /// </summary>
        public AbstratosNeon.ABS_Apartamento BOLApartamento
        {
            get 
            {
                if ((_BOLApartamento == null) && APT.HasValue)
                    _BOLApartamento = AbstratosNeon.ABS_Apartamento.GetApartamento(APT.Value);
                return _BOLApartamento;
            }
        }
        */

        /*
        private dBoletos.DadosAPTRow _rowAPT;

        /// <summary>
        /// Dados do apartamento
        /// </summary>
        public dBoletos.DadosAPTRow rowAPT
        {
            get 
            {                
                if (_rowAPT == null)
                    PegadadosAPT();
                return _rowAPT; 
            }
            set 
            {                
                _rowAPT = value; 
            }
        }*/
        /*
        /// <summary>
        /// Condom�nio
        /// </summary>
        public override int CON
        {
            get { return rowPrincipal.BOL_CON; }
        }*/

        private bool _Proprietario;

        /// <summary>
        /// Propriet�rio ou inquilino
        /// </summary>
        public bool Proprietario
        {
            get
            {
                if (rowPrincipal != null)
                    _Proprietario = rowPrincipal.BOLProprietario;
                return _Proprietario;
            }
            set
            {
                _Proprietario = value;
            }
        }

        /*
        private string ConteudoEmail =
            "\r\n" +
            "Prezado Cliente," + "\r\n" +
            "\r\n" +
            "Para sua maior seguran�a e comodidade, a Neon Online, passa a enviar a partir de agora o Boleto Banc�rio do %C por email." + "\r\n" +
            "" + "\r\n" +
            "O arquivo (pdf) do Boleto Banc�rio est� anexo. Voc� poder� imprimi-lo e efetuar o pagamento em qualquer banco integrado ao sistema de compensa��o ou via internet." + "\r\n" +
            "" + "\r\n" +
            "Para pagamento via internet disponibilizamos tamb�m o n�mero do C�digo de barras:" + "\r\n" +
            "" + "\r\n" +
            " %B " + "\r\n" +
            "" + "\r\n" +
            "que identificar� este boleto no site de seu banco." + "\r\n" +
            "" + "\r\n" +
            "Neon Online, facilitando sua vida." + "\r\n" +
            "" + "\r\n" +
            "Caso v�ce n�o queira receber os boletos por e-mail basta desmarcar esta op��o no sou cadastro no Neon Online." + "\r\n" +
            "" + "\r\n" +
            "Att," + "\r\n" +
            "" + "\r\n" +
            "Neon Im�veis" + "\r\n" +
            "\r\n";

        
        public bool AjustaBoletoPeloAccess(bool Forcar)
        {
            
            if ((!rowPrincipal.IsBOLExportarNull()) && (rowPrincipal.BOLExportar))
            {
                //ExportaAcces();
                ReExportaTh();
                return true;
            };
            string Comandodadosaccess = "SELECT [Valor Previsto],Pago, [Data Vencimento],[Valor Pago],[Data Pagamento] FROM boleto WHERE [N�mero Boleto] = @P1";
            string comandoUpdate = "UPDATE BOLetos SET BOLValorPago = @P1, BOLPagamento =@P2, BOLVencto = @P3 WHERE (BOL = @P4)";
            DataRow rowAccess = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLRow(Comandodadosaccess, rowPrincipal.BOL);
            if (rowAccess == null)
                return false;
            if (rowPrincipal.BOLCancelado)
                dBoletos.BOLetosTableAdapter.RetornaCancelado(rowPrincipal.BOL);
            bool reimportar = false;
             
            if (Math.Abs((float)rowAccess["Valor Previsto"] - (float)rowPrincipal.BOLValorPrevisto) >= 0.01)
                reimportar = true;
            decimal TotBOD = 0;
            foreach (Boletos.Boleto.dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())            
                TotBOD += rowBOD.BODValor;
            if (Math.Abs(TotBOD - rowPrincipal.BOLValorPrevisto) >= 0.01m)
                reimportar = true;
            if(!reimportar)
            {
                if ((!(Boolean)rowAccess["Pago"]) && (rowPrincipal.BOLVencto == (DateTime)rowAccess["Data Vencimento"]))
                    return true;
                else
                    if (!rowPrincipal.IsBOLPagamentoNull())                        
                        return true;
                    else
                    {
                        
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoUpdate, rowAccess["Valor Pago"], rowAccess["Data Pagamento"], rowAccess["Data Vencimento"], rowPrincipal.BOL);
                        return true;
                    }
            }
            else
            {
                if (Forcar)
                
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from boletodetalhe where BOD_BOL = @P1", rowPrincipal.BOL);
                fImportFromMsAccess.ImportaBoleto(rowPrincipal.BOL.ToString(), Forcar);
                
                return true;
               
            };
            
            /*

            //SqlCommand ChecaExistencia = new SqlCommand("SELECT BOL FROM Boletos WHERE(BOL = " + _oleDR["N�mero Boleto"].ToString() + ");", _connSQL);
            String _strInsertSQL;
            bool Novo;
            //    SqlCommand _sqlIncluir = new SqlCommand("", _connSQL);
             
            //    SqlCommand _sqlVerificaBOD = new SqlCommand("SELECT BOD FROM BOletoDetalhe WHERE (BOD_BOL = @BOL) AND (BOD_PLA = @PLA) AND (BODValor = @Valor)", _connSQL);
            double BOLNovo = (double)rowAccess["N�mero Boleto"];
                        
            
            //Int32 APT = fImportFromMsAccess.GetSQLApartamentoPK(_oleDR["CODCON"].ToString(), _oleDR["BLOCO"].ToString(), _oleDR["Apartamento"].ToString());
            //    if (APT == 0)
            //        return false;
            //    SqlCommand PegaDig = new SqlCommand("SELECT CONDigitoAgencia FROM CONDOMINIOS WHERE(CONCodigo = '" + _oleDR["CODCON"].ToString() + "')", _connSQL);
            //    string digito = PegaDig.ExecuteScalar().ToString();
            DateTime DataVencto = (DateTime)rowAccess["Data Vencimento"];
            //    string Mes;
            //    string Ano;
            //    if (_oleDR["M�s Compet�ncia"].ToString() == "0101")
            //    {
            //        Mes = "01";
            //        Ano = "01";
            //     }
            //    else
            //    {
            //        Mes = _oleDR["M�s Compet�ncia"].ToString().Substring(0, 2);
            //        Ano = _oleDR["M�s Compet�ncia"].ToString().Substring(2, 4);
            //    }
            //    object retorno = ChecaExistencia.ExecuteScalar();
                if (retorno == null)
                {
                    _strInsertSQL =
                          "SET IDENTITY_INSERT boletos ON;" +
                          "INSERT INTO BOLetos " +
                          "(BOL,BOLNNAntigo,BOL_APT, BOLProprietario, BOLTipoCRAI, BOLEmissao, BOLVencto, BOLPagamento, BOLMulta, BOLValorPrevisto,  " +
                          "BOLValorPago, BOLCompetenciaAno, BOLCompetenciaMes, BOLEndereco, BOLNome, BOLMensagem, BOLImpressao, BOLTipoMulta, BOLCC,  " +
                          "BOLAgencia, BOL_BCO, BOLDigitoCC, BOLDigitoAgencia, BOL_FPG, BOLDATAI, BOLDATAA) " +
                          "VALUES " +
                          "(@BOL,@BOLNNAntigo,@BOL_APT,@BOLProprietario,@BOLTipoCRAI,@BOLEmissao,@BOLVencto,@BOLPagamento,@BOLMulta,@BOLValorPrevisto," +
                          "@BOLValorPago,@BOLCompetenciaAno,@BOLCompetenciaMes,@BOLEndereco,@BOLNome,@BOLMensagem,@BOLImpressao," +
                          "@BOLTipoMulta,@BOLCC,@BOLAgencia,@BOL_BCO,@BOLDigitoCC,@BOLDigitoAgencia,@BOL_FPG,@BOLDATAI,@BOLDATAA);" +
                          "SET IDENTITY_INSERT boletos OFF;";
                    Novo = true;





                    _sqlIncluir.Parameters.AddWithValue("@BOL", BOLNovo);
                    _sqlIncluir.Parameters.AddWithValue("@BOLNNAntigo", (double)_oleDR["Nosso N�mero"]);

                    _sqlIncluir.Parameters.AddWithValue("@BOL_APT", APT);
                    _sqlIncluir.Parameters.AddWithValue("@BOLProprietario", (bool)(_oleDR["Destinat�rio"].ToString() == "P"));
                    _sqlIncluir.Parameters.AddWithValue("@BOLTipoCRAI", _oleDR["Condominio"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLEmissao", (DateTime)_oleDR["Data emiss�o"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLVencto", DataVencto);
                    if ((_oleDR["Data Pagamento"] != null) && (_oleDR["Data Pagamento"] != DBNull.Value))
                        _sqlIncluir.Parameters.AddWithValue("@BOLPagamento", (DateTime)_oleDR["Data Pagamento"]);
                    else
                    {
                        System.Data.SqlClient.SqlParameter Par1 = new SqlParameter("@BOLPagamento", SqlDbType.DateTime);
                        _sqlIncluir.Parameters.Add(Par1);
                        Par1.Value = DBNull.Value;
                    };

                    _sqlIncluir.Parameters.AddWithValue("@BOLMulta", (double)_oleDR["Multa"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLValorPrevisto", Math.Round((float)_oleDR["Valor Previsto"], 2));
                    _sqlIncluir.Parameters.AddWithValue("@BOLValorPago", Math.Round((float)_oleDR["Valor Pago"], 2));
                    _sqlIncluir.Parameters.AddWithValue("@BOLCompetenciaAno", Ano);
                    _sqlIncluir.Parameters.AddWithValue("@BOLCompetenciaMes", Mes);
                    //string strend = _oleDR["Endere�o"].ToString();
                    _sqlIncluir.Parameters.AddWithValue("@BOLEndereco", _oleDR["Endere�o"].ToString());//(strend == "") ? "" : strend);
                    _sqlIncluir.Parameters.AddWithValue("@BOLNome", _oleDR["Nome"].ToString());
                    _sqlIncluir.Parameters.AddWithValue("@BOLMensagem", _oleDR["Mensagem"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLImpressao", (bool)_oleDR["Impresso"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLTipoMulta", _oleDR["Tipo de Multa"]);
                    string strCC = _oleDR["CC"].ToString();
                    if (strCC.Length > 6)
                    {
                        _sqlIncluir.Parameters.AddWithValue("@BOLCC", strCC.Substring(0, 6));
                        _sqlIncluir.Parameters.AddWithValue("@BOLDigitoCC", _oleDR["CC"].ToString().Substring(6, 1));

                    }
                    else
                    {
                        PegaDig.CommandText = "SELECT CONConta FROM CONDOMINIOS WHERE(CONCodigo = '" + _oleDR["CODCON"].ToString() + "')";
                        string conta = PegaDig.ExecuteScalar().ToString();
                        PegaDig.CommandText = "SELECT CONDigitoConta FROM CONDOMINIOS WHERE(CONCodigo = '" + _oleDR["CODCON"].ToString() + "')";
                        string digitoconta = PegaDig.ExecuteScalar().ToString();

                        _sqlIncluir.Parameters.AddWithValue("@BOLCC", conta);
                        _sqlIncluir.Parameters.AddWithValue("@BOLDigitoCC", digitoconta);

                    };

                    if (_oleDR["Codigo Ag�ncia"] != null)
                    {
                        _sqlIncluir.Parameters.AddWithValue("@BOLAgencia", _oleDR["Codigo Ag�ncia"].ToString());
                    }
                    else
                    {
                        PegaDig.CommandText = "SELECT CONAgencia FROM CONDOMINIOS WHERE(CONCodigo = '" + _oleDR["CODCON"].ToString() + "')";
                        string agencia = PegaDig.ExecuteScalar().ToString();
                        _sqlIncluir.Parameters.AddWithValue("@BOLAgencia", agencia);
                    }

                    if ((_oleDR["Banco"].ToString() != "341")
                        &&
                        (_oleDR["Banco"].ToString() != "237")
                        &&
                        (_oleDR["Banco"].ToString() != "409")
                        )
                    {
                        PegaDig.CommandText = "SELECT CON_BCO FROM CONDOMINIOS WHERE(CONCodigo = '" + _oleDR["CODCON"].ToString() + "')";
                        int Banco = (int)PegaDig.ExecuteScalar();
                        _sqlIncluir.Parameters.AddWithValue("@BOL_BCO", Banco);
                    }
                    else
                        _sqlIncluir.Parameters.AddWithValue("@BOL_BCO", _oleDR["Banco"].ToString());

                    _sqlIncluir.Parameters.AddWithValue("@BOLDigitoAgencia", digito);
                    _sqlIncluir.Parameters.AddWithValue("@BOL_FPG", ((bool)_oleDR["Correio"]) ? 2 : 4);
                    _sqlIncluir.Parameters.AddWithValue("@BOLDATAI", DateTime.Now);
                    _sqlIncluir.Parameters.AddWithValue("@BOLDATAA", DateTime.Now);


                }
                else
                {
                    _strInsertSQL =
                      "UPDATE BOLetos " +
                      "SET " +
                      "BOLPagamento = @BOLPagamento, " +
                      "BOLValorPago = @BOLValorPago, " +
                      "BOLVencto = @BOLVencto, " +
                      "BOLValorPrevisto = @BOLValorPrevisto " +
                      "WHERE     (BOL = @BOL)";
                    Novo = false;
                    _sqlIncluir.Parameters.AddWithValue("@BOL", _oleDR["N�mero Boleto"].ToString());
                    if ((_oleDR["Data Pagamento"] != null) && (_oleDR["Data Pagamento"] != DBNull.Value))
                        _sqlIncluir.Parameters.AddWithValue("@BOLPagamento", (DateTime)_oleDR["Data Pagamento"]);
                    else
                    {
                        System.Data.SqlClient.SqlParameter Par1 = new SqlParameter("@BOLPagamento", SqlDbType.DateTime);
                        _sqlIncluir.Parameters.Add(Par1);
                        Par1.Value = DBNull.Value;
                    };
                    _sqlIncluir.Parameters.AddWithValue("@BOLValorPago", Math.Round((float)_oleDR["Valor Pago"], 2));
                    _sqlIncluir.Parameters.AddWithValue("@BOLVencto", DataVencto);
                    _sqlIncluir.Parameters.AddWithValue("@BOLValorPrevisto", Math.Round((float)_oleDR["Valor Previsto"], 2));
                };


                _sqlIncluir.CommandText = _strInsertSQL;

                try
                {
                    //MessageBox.Show(CODCON + " - " + BLOCO + " - " + APARTAMENTO + " - " + _oleDR["NOME"].ToString());



                    _sqlIncluir.ExecuteNonQuery();

                    _oleDR.Close();
                    ///// ITENS DOS BOLETOS
                    // if (Novo)
                    //{
                    _oleSelectCommand.CommandText = "SELECT [Tipo Pagamento], Valor, Destinat�rio, Mensagem " +
                                                    "FROM [Boleto - Pagamentos] " +
                                                    "WHERE ([N�mero Boleto] = @NumeroBoleto)";

                    _oleSelectCommand.Parameters.AddWithValue("@NumeroBoleto", NumeroNeon);

                    _oleDR = _oleSelectCommand.ExecuteReader();

                    ///////////////////////////////////////

                    _sqlIncluir.CommandText = "INSERT INTO BOletoDetalhe " +
                                              "(BOD_BOL, BOD_PLA, BODValor, BODMensagem, BOD_APT, BODProprietario,BODFracaoEfetiva,BODData," +
                                              "BODCompetenciaMes,BODCompetenciaAno,BODComCondominio)" +
                                              "VALUES " +
                                              "(@BOD_BOL,@BOD_PLA,@BODValor,@BODMensagem,@BOD_APT,@BODProprietario,1,@BODData," +
                                              "@BODCompetenciaMes,@BODCompetenciaAno,0);";
                    SqlCommand MenPLA = new SqlCommand("", _connSQL);
                    while (_oleDR.Read())
                    {
                        ///////////////////////

                        _sqlVerificaBOD.Parameters.Clear();
                        _sqlVerificaBOD.Parameters.AddWithValue("@BOL", BOLNovo);
                        _sqlVerificaBOD.Parameters.AddWithValue("@PLA", _oleDR["Tipo Pagamento"]);
                        _sqlVerificaBOD.Parameters.AddWithValue("@Valor", _oleDR["Valor"]);
                        object oBOD = _sqlVerificaBOD.ExecuteScalar();
                        if (oBOD != null)
                            continue;

                        string PLA = _oleDR["Tipo Pagamento"].ToString();
                        DSCentral.PLAnocontasRow rowPLA = DSCentral.DCentral.PLAnocontas.FindByPLA(PLA);
                        if (rowPLA == null)
                            rowPLA = DSCentral.DCentral.ImportaPLA(PLA);

                        string MenBase = "";
                        if (rowPLA != null)
                            MenBase = rowPLA.PLADescricaoRes;

                        _sqlIncluir.Parameters.Clear();


                        _sqlIncluir.Parameters.AddWithValue("@BOD_BOL", BOLNovo);
                        _sqlIncluir.Parameters.AddWithValue("@BOD_PLA", _oleDR["Tipo Pagamento"]);
                        _sqlIncluir.Parameters.AddWithValue("@BODValor", Math.Round((float)_oleDR["Valor"], 2));
                        if (_oleDR["Mensagem"] != DBNull.Value)
                            MenBase = (string)_oleDR["Mensagem"];

                        _sqlIncluir.Parameters.AddWithValue("@BODMensagem", MenBase);
                        _sqlIncluir.Parameters.AddWithValue("@BOD_APT", APT);
                        _sqlIncluir.Parameters.AddWithValue("@BODProprietario", (bool)(_oleDR["Destinat�rio"].ToString() == "P"));
                        _sqlIncluir.Parameters.AddWithValue("@BODData", DataVencto);
                        _sqlIncluir.Parameters.AddWithValue("@BODCompetenciaAno", Ano);
                        _sqlIncluir.Parameters.AddWithValue("@BODCompetenciaMes", Mes);
                        _sqlIncluir.ExecuteNonQuery();


                    };

                    //};
                    /////
                    _connSQL.Close();


                    _connACCESS.Close();
                    return true;

                }
                catch (Exception Erro)
                {
                    String _Str = "";
                    foreach (SqlParameter _Parametro in _sqlIncluir.Parameters)
                        if (_Parametro.Value == null)
                            _Str += _Parametro.ToString() + " - NULO \r\n\r\n";
                        else

                            _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n\r\n";

                    MessageBox.Show("Ocorreu um erro ao tentar importar o boleto do MS Access.\r\n\r\n Erro: " + Erro.Message + "\r\n\r\n" + _Str,
                                    "Importar Propriet�rio do MS Access", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    _connSQL.Close();

                    _oleDR.Close();
                    _connACCESS.Close();

                    return false;
                }





            }
            else
            {
                _oleDR.Close();
                _connACCESS.Close();
                return false;
            }
              
        }

        */

        /// <summary>
        /// Retorna se o Respons�vel pelo boleto quer e.mail autom�tico
        /// </summary>
        /// <returns></returns>
        /// <remarks>Caso n�o exista um email cadastrado o retorno ser� false independente da solicita��o de email pelo cliente</remarks>
        public bool QuerEmail
        {
            get
            {
                if (Apartamento == null)
                    return false;
                else
                    return Apartamento.QuerEmail(FormaPagamento, Proprietario);
            }
        }

        /// <summary>
        /// Se deve imprimir
        /// </summary>
        public bool QuerImpresso
        {
            get
            {
                if (Apartamento == null)
                    return true;
                else
                    return Apartamento.QuerImpresso(FormaPagamento, Proprietario);
            }
        }

        /// <summary>
        /// Dados do condom�nio (poder� ser substituido por um objeto
        /// </summary>
        public dCondominiosAtivos.CONDOMINIOSRow rowCON { get { return dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(CON); } }

        /// <summary>
        /// Tipo do boleto
        /// </summary>
        public BOLTipoCRAI TipoCRAIE
        {
            get
            {
                return virEnumBOLTipoCRAI.Parse(rowPrincipal.BOLTipoCRAI);
            }
            set
            {
                rowPrincipal.BOLTipoCRAI = virEnumBOLTipoCRAI.Valor(value);
            }
        }

        private ContaCorrenteNeon ContaCondominio => (ContaCorrenteNeon)Condominio.ABSConta;

        /// <summary>
        /// Indica se o Propriet�rio / inquilino est� configurado para D�bito Autom�tico
        /// </summary>
        /// <returns></returns>
        internal bool DebitoAutomaticoCadastrado()
        {
            if (rowSBO != null)
                return false;            
            if (
                (ContaCondominio.DebitoAutomatico)
                  &&
                (Apartamento != null)
                  &&
                (Apartamento.ContaDebito(Proprietario) != null)
               )
                return true;
            else
                return false;
        }

        #region Registro

        /// <summary>
        /// Registra Boleto
        /// </summary>
        /// <param name="Remessa"></param>
        public bool Registrar(BoletoRemessaBase Remessa)
        {
            if ((Apartamento == null) && ((Fornecedor == null)))
                return false;
            if (!rowPrincipal.IsBOLPagamentoNull())
            {
                AlteraStatusRegistro(StatusRegistroBoleto.BoletoQuitado);
                return false;
            }
            if ((BOLStatus == StatusBoleto.Saldinho) || (BOLStatus == StatusBoleto.Acumulado))
            {
                AlteraStatusRegistro(StatusRegistroBoleto.ComregistroNaoRegistrado);
                return false;
            }
            if ((rowPrincipal.BOLVencto < DateTime.Today) && (BOLStatusRegistro.EstaNoGrupo(StatusRegistroBoleto.BoletoRegistrando, StatusRegistroBoleto.BoletoRegistrandoDA)))
            {
                AlteraStatusRegistro(StatusRegistroBoleto.ComregistroNaoRegistrado);
                return false;
            }

            bool SoRegistra = (Remessa != null);

            if (!SoRegistra) //Se a remessa ainda n�o foi configurada
                if (rowCON.CON_BCO == 237)
                {
                    Remessa = new BoletoRemessaBra();
                    Remessa.IniciaRemessa();
                    Remessa.CodEmpresa = DSCentral.EMP == 1 ? 602270 : 4854082;
                    Remessa.NomeEmpresa = "Neon Imoveis";
                    Remessa.Carteira = "9";
                    Remessa.SequencialRemessa = 1;
                }
                else
                {
                    Remessa = new BoletoRemessaItau();
                    Remessa.IniciaRemessa();
                    Remessa.AgenciaMae = int.Parse(rowCON.CONAgencia);
                    Remessa.ContaMae = int.Parse(rowCON.CONConta);
                    Remessa.ContaMaeDg = rowCON.CONDigitoConta;
                    Remessa.NomeEmpresa = rowCON.CONNome;
                    Remessa.Carteira = CarteiraBanco.ToString();
                    Remessa.SequencialRemessa = 1;
                }

            BoletoReg boletoreg = new BoletoReg()
            {
                Agencia = rowCON.CONAgencia,
                Conta = rowCON.CONConta,
                DigConta = rowCON.CONDigitoConta,
                CNPJ = rowCON.IsCONCnpjNull() ? null : new DocBacarios.CPFCNPJ(rowCON.CONCnpj),
                NomeEmpresa = rowCON.CONNome,
                CobrarMulta = true,
                PercMulta = rowPrincipal.BOLMulta,
                NossoNumero = rowPrincipal.BOL,
                EmissaoBoleto = (int)BoletoRemessaBra.TiposEmissaoBoleto.Cliente
            };
            try
            {
                boletoreg.Convenio = int.Parse(rowCON.CONCodigoCNR);
            }
            catch { };



            if (
                 (BOLStatusRegistro == StatusRegistroBoleto.BoletoRegistrandoDA)
                  &&
                 ((!ContaCondominio.DebitoAutomatico)
                      ||
                    (Apartamento == null)
                      ||
                    (Apartamento.ContaDebito(Proprietario) == null)
                 )
               )
                BOLStatusRegistro = StatusRegistroBoleto.BoletoRegistrando;

            switch (BOLStatusRegistro)
            {
                case StatusRegistroBoleto.BoletoRegistrando:
                    boletoreg.IDOcorrencia = (rowCON.CON_BCO == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.Remessa : (int)BoletoRemessaItau.TiposOcorrencia.Remessa;
                    boletoreg.Vencimento = rowPrincipal.BOLVencto;
                    break;
                case StatusRegistroBoleto.BoletoRegistrandoDA:
                    boletoreg.IDOcorrencia = (rowCON.CON_BCO == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.Remessa : (int)BoletoRemessaItau.TiposOcorrencia.Remessa;
                    boletoreg.Vencimento = rowPrincipal.BOLVencto;
                    boletoreg.DAAgencia = Apartamento.ContaDebito(Proprietario).Agencia.ToString();
                    boletoreg.DADigAgencia = Apartamento.ContaDebito(Proprietario).AgenciaDg.Value.ToString();
                    boletoreg.DANumConta = Apartamento.ContaDebito(Proprietario).NumeroConta.ToString();
                    boletoreg.DADigNumConta = Apartamento.ContaDebito(Proprietario).NumeroDg;
                    boletoreg.DARazaoConta = 07050;
                    boletoreg.RegistrarDA = true;
                    break;
                case StatusRegistroBoleto.AlterarRegistroData:
                    boletoreg.IDOcorrencia = (rowCON.CON_BCO == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.AlteracaoVencimento : (int)BoletoRemessaItau.TiposOcorrencia.AlteracaoVencimento;
                    if (rowPrincipal.IsBOLDataLimiteJustNull() || (rowPrincipal.BOLDataLimiteJust < rowPrincipal.BOLVencto))
                        boletoreg.Vencimento = rowPrincipal.BOLVencto;
                    else
                        boletoreg.Vencimento = rowPrincipal.BOLDataLimiteJust;
                    break;
                case StatusRegistroBoleto.AlterarRegistroValor:
                    boletoreg.IDOcorrencia = (rowCON.CON_BCO == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.ConcessaoAbatimento : (int)BoletoRemessaItau.TiposOcorrencia.ConcessaoAbatimento;
                    boletoreg.Vencimento = rowPrincipal.BOLVencto;
                    boletoreg.Abatimento = rowPrincipal.BOLValorOriginal - rowPrincipal.BOLValorPrevisto;
                    if (boletoreg.Abatimento < 0)
                    {
                        AlteraStatusRegistro(StatusRegistroBoleto.BoletoRegistrado);
                        return false;
                    }
                    break;
                case StatusRegistroBoleto.ErroNoRegistro:
                    break;
                case StatusRegistroBoleto.BoletoSemRegistro:
                case StatusRegistroBoleto.BoletoAguardandoRetorno:
                case StatusRegistroBoleto.BoletoRegistrado:
                case StatusRegistroBoleto.BoletoRegistradoDDA:
                case StatusRegistroBoleto.ComregistroNaoRegistrado:
                default:
                    break;
            }
            if (boletoreg.IDOcorrencia == ((rowCON.CON_BCO == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.ConcessaoAbatimento : (int)BoletoRemessaItau.TiposOcorrencia.ConcessaoAbatimento))
                boletoreg.Valor = rowPrincipal.BOLValorOriginal;
            else
                boletoreg.Valor = rowPrincipal.BOLValorPrevisto;
            boletoreg.NumDocumento = rowPrincipal.BOL.ToString();
            boletoreg.EspecieTitulo = (rowCON.CON_BCO == 237) ? (int)BoletoRemessaBra.TiposEspecieTitulo.Outros : (int)BoletoRemessaItau.TiposEspecieTitulo.Condominio;
            boletoreg.Emissao = rowPrincipal.BOLEmissao;
            boletoreg.Instrucao1 = (int)BoletoRemessaBra.TiposInstrucao.BaixaDecursoPrazo;
            boletoreg.Instrucao2 = rowCON.CONDiasExpiraRegistro;
            boletoreg.CPF = CPF;
            boletoreg.NomeSacado = NomeSacado;
            boletoreg.EnderecoSacado = rowCON.CONEndereco.Trim();
            boletoreg.CEPSacado = (!rowCON.IsCONCepNull() && (rowCON.CONCep != string.Empty)) ? rowCON.CONCep : "0";
            boletoreg.BairroSacado = rowCON.CONBairro.Trim();
            boletoreg.CidadeSacado = (!rowCON.IsCIDNomeNull()) ? rowCON.CIDNome : string.Empty;
            boletoreg.EstadoSacado = (!rowCON.IsCIDUfNull()) ? rowCON.CIDUf : string.Empty;


            //Rateio de Cr�dito TESTE
            if ((rowCON.CON == 397) && (ValorPrevisto == 10.01M))
            {
                boletoreg.RateioCreditos.Add(new RateioCredito(109, 0, 255718, "5", 2.01M, "NEON IMOVEIS"));
                boletoreg.RateioCreditos.Add(new RateioCredito(109, 0, 304610, "9", 2.02M, "CONJUNTO RES. YRAJA GARDEN I"));
                boletoreg.RateioCreditos.Add(new RateioCredito(109, 0, 302254, "4", 2.03M, "COND. EDIF. VIVENDA SANT AGATA"));
                boletoreg.RateioCreditos.Add(new RateioCredito(109, 0, 363651, "8", 2.04M, "CONDOMINIO RESIDENCIAL VIVANCE"));
            };
            //if ((rowCON.CON == 397) && (ValorPrevisto == 10.03M))
            //{
            //    boletoreg.RateioCreditos.Add(new RateioCredito(109, 0, 264876, "8", 8.01M, "COND. EDIF. MISSURI"));                
            //}
            //Debito automatico TESTE
            /*
            if ((rowCON.CON == 397) && (ValorPrevisto == 10.02M))
            {
                boletoreg.DAAgencia = "0109";
                boletoreg.DADigAgencia = "0";
                boletoreg.DANumConta = "255718";
                boletoreg.DADigNumConta = "5";
                boletoreg.DARazaoConta = 07050;
                boletoreg.RegistrarDA = true;
                boletoreg.RateioCreditos.Add(new RateioCredito(109, 0, 255718, "5", 2.01M, "NEON IMOVEIS"));
            }*/


            //desconto
            dBoletos.BoletoDEscontoRow[] rowDES = rowPrincipal.GetBoletoDEscontoRows();
            if (rowDES.Length == 0)
            {
                bool ClearBeforeFillOrig = dBoletos.BoletoDEscontoTableAdapter.ClearBeforeFill;
                try
                {
                    dBoletos.BoletoDEscontoTableAdapter.ClearBeforeFill = false;
                    dBoletos.BoletoDEscontoTableAdapter.Fill(dBoletos.BoletoDEsconto, BOL);
                }
                finally
                {
                    dBoletos.BoletoDEscontoTableAdapter.ClearBeforeFill = ClearBeforeFillOrig;
                }
            }
            if (rowDES.Length > 0)
            {
                boletoreg.Desconto = rowDES[0].BDEDesconto;
                boletoreg.LimDesconto = rowDES[0].BDEData;
            }
            //*** MRC - TERMINO - REMESSA (15/03/2016 10:00) ***

            Remessa.Boletos.Add(boletoreg);

            if (!SoRegistra)
                throw new NotImplementedException("ajustar a path");
                //Remessa.GravaArquivo(@"C:\Temp\Neon\ArquivosBradesco\");
            return true;
        }

        private string NomeSacado
        {
            get
            {
                if (Apartamento != null)
                    return Apartamento.PESNome(Proprietario);
                else if (Fornecedor != null)
                    return Fornecedor.FRNNome;
                else
                    return rowPrincipal.BOLNome;
            }
        }

        /// <summary>
        /// Endere�o para onde o boleto � efetivamente enviado
        /// </summary>
        /// <returns></returns>
        internal string EnderecoEnvioEfetivo()
        {
            if (FormaPagamento.EstaNoGrupo(FormaPagamento.Correio, FormaPagamento.CorreioEemail, FormaPagamento.email_Correio))
                return Apartamento.PESEndereco(FormaPagamento, Proprietario, false);
            else
                return rowCON.CONEndereco.Trim();
        }

        /// <summary>
        /// CEP para onde o boleto � efetivamente enviado
        /// </summary>
        /// <returns></returns>
        public string CEPEnvioEfetivo()
        {
            if (Apartamento == null)
                return "0";
            return Apartamento.PESCEP(FormaPagamento, Proprietario);
        }


        /// <summary>
        /// Altera o status de registro do boleto
        /// </summary>
        public void AlteraStatusRegistro(StatusRegistroBoleto intStatusRegistro, string Informacao = "")
        {
            if(rowSBO == null)
            //Altera Status do Registro
            {
                rowPrincipal.BOLStatusRegistro = (int)intStatusRegistro;

                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 184", dBoletos.BOLetosTableAdapter);
                    GravaJustificativa(string.Format("Status Registro: {0} {1}",
                                                      Boleto.VirEnumStatusRegistroBoleto.Descritivo(intStatusRegistro),
                                                      Informacao == string.Empty ? string.Empty : "\r\n ****" + Informacao + "****"
                                                    )
                                      );
                    //dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    rowPrincipal.AcceptChanges();
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                    throw new Exception("Erro ao alterar status de registro do boleto: " + e.Message, e);
                }
            }
            else
            {
                rowSBO.SBOStatusRegistro = (int)intStatusRegistro;
                VirMSSQL.TableAdapter.EmbarcaEmTrans(dBoletos.SuperBOletoTableAdapter);
               dBoletos.SuperBOletoTableAdapter.Update(rowSBO);
            }
        }


        private bool AlteracaoNoRegistroBoleto(StatusRegistroBoleto Tipo)
        {
            if (!rowPrincipal.IsBOLPagamentoNull())
                return true;
            //*** MRC - INICIO (08/06/2016 11:00) ***
            //if ((BOLStatusRegistro == StatusRegistroBoleto.BoletoRegistrado) && (Boletos.Boleto.Boleto.StatusBoletoRegistravel.Contains(BOLStatus)))
            if ((BOLStatusRegistro == StatusRegistroBoleto.BoletoRegistrado || BOLStatusRegistro == StatusRegistroBoleto.BoletoRegistradoDDA) && (Boleto.StatusBoletoRegistravel.Contains(BOLStatus)))
                //*** MRC - TERMINO (08/06/2016 11:00) ***

                switch (MessageBox.Show("Confirma a altera��o do registro do boleto?\r\n\r\nEsta opera��o acarreta custo!", "CONFIRMA��O", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3))
                {
                    case DialogResult.Yes:
                        rowPrincipal.BOLStatusRegistro = (int)Tipo;
                        rowPrincipal.BOLJustificativa += "\r\nSolicitada altera��o do registro";
                        //rowPrincipal.BOLJustificativa += String.Format("\r\n** N�O solicitada altera��o do registro **");
                        break;
                    case DialogResult.No:
                        rowPrincipal.BOLJustificativa += "\r\n** N�O solicitada altera��o do registro **";
                        break;
                    case DialogResult.Cancel:
                        UltimoErro = "Cancelado pelo usu�rio";
                        return false;
                }
            return true;
        }

        #endregion 



        #region Baixa

        /// <summary>
        /// Ajusta a data do acordo no caso de baixa anterior ao inicico do acordo: deposito ou implanta��o
        /// </summary>
        private void AjustaDataAcordo(DateTime Pagamento)
        {
            if (BOLStatus == StatusBoleto.Acordo)
            {
                DateTime DataMinima = Pagamento.Date.AddDays(-1);
                string ComandoAjuste =
"UPDATE    ACOrdos\r\n" +
"SET              ACODATAI = @P1\r\n" +
"FROM         ACOrdos INNER JOIN\r\n" +
"                      ACOxBOL ON ACOrdos.ACO = ACOxBOL.ACO\r\n" +
"WHERE     (ACOxBOL.BOL = @P2) AND (ACOxBOL.ACOBOLOriginal = 0) AND (ACOrdos.ACODATAI > @P3);";
                if (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoAjuste, DataMinima, rowPrincipal.BOL, DataMinima) > 0)
                    GravaJustificativa(string.Format("Data do acordo ajustada para:{0}", DataMinima), string.Empty);
            }
        }

        /// <summary>
        /// Baixa na implanta��o
        /// </summary>
        /// <param name="DataPag"></param>
        /// <returns></returns>
        [Obsolete("Descontinuado - usar baixa normal", true)]
        public bool BaixaImplantacao(DateTime DataPag)
        {
            try
            {

                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 1898");
                AjustaDataAcordo(DataPag);
                rowPrincipal.BOLPagamento = DataPag;
                rowPrincipal.BOLValorPago = rowPrincipal.BOLValorPrevisto;

                GravaJustificativa("Marcado como pago manualmente por " + Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome, string.Empty);
                /*
                string PagoAccess =
    "UPDATE    Boleto\r\n" +
    "SET              [Data Pagamento] = @P1, Pago = 1, [Valor Pago] =  [Valor Previsto]\r\n" +
    "WHERE     (Boleto.[N�mero Boleto] = @P2);";
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(PagoAccess, DataPag, rowPrincipal.BOL);
                PagoAccess =
    "UPDATE    [Acordo - Parcelas]\r\n" +
    "SET              Pago = 1, [Data de Pagamento] = @P1, [Valor Pago] = @P2 \r\n" +
    "WHERE     ([Acordo - Parcelas].[N�mero do Boleto] = @P3);";
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(PagoAccess, DataPag, (double)rowPrincipal.BOLValorPrevisto, rowPrincipal.BOL);
                VirOleDb.TableAdapter.STTableAdapter.Commit();*/
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);                
                return false;
            }


            return true;
        }

        private bool CancelaBaixaAcordo(string Justificativa)
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 1934");
                Acordo.Acordo AcordoAtual = new Boletos.Acordo.Acordo(this, false);
                if (!AcordoAtual.Gerado)
                    throw new Exception("Acordo n�o encontrado");
                AcordoAtual.Reabrir();
                foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                    if (!rowBOD.IsBODAcordo_BOLNull())
                    {
                        Boleto BolOrig = new Boleto(rowBOD.BODAcordo_BOL);
                        if (!BolOrig.CancelaBaixa(TipoCancelaBaixa.acordo, Justificativa))
                            throw new Exception(BolOrig.UltimoErro);
                    }
                rowPrincipal.SetBOLPagamentoNull();
                rowPrincipal.SetBOLValorPagoNull();
                rowPrincipal.BOLCancelado = false;
                SalvarAlteracoes("Cancelamento da quita��o\r\n" + Justificativa);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                return false;
            }
            return true;
        }

        private enum TipoCancelaBaixa { direto, acordo }

        /// <summary>
        /// Cancela Baixa
        /// </summary>
        /// <param name="Justificativa"></param>
        /// <returns></returns>
        public bool CancelaBaixa(string Justificativa)
        {
            return CancelaBaixa(TipoCancelaBaixa.direto, Justificativa);
        }

        private bool CancelaBaixa(TipoCancelaBaixa TipoCan, string Justificativa)
        {
            if (!Encontrado)
            {
                UltimoErro = "Boleto n�o encontrado";
                return false;
            }



            switch (BOLStatus)
            {
                case StatusBoleto.Erro:
                case StatusBoleto.Antigo:
                    UltimoErro = "Boleto com erro";
                    return false;
                case StatusBoleto.Cancelado:
                    UltimoErro = "Boleto cancelado";
                    return false;
                case StatusBoleto.ParcialAcordo:
                case StatusBoleto.OriginalAcordo:
                    if (TipoCan == TipoCancelaBaixa.direto)
                    {
                        UltimoErro = "este Boleto faz parte de um acordo e sua quita��o s� pode ser cancela atrav�s do boleto de acordo";
                        return false;
                    }
                    break;
                case StatusBoleto.Acordo:
                    return CancelaBaixaAcordo(Justificativa);
                case StatusBoleto.OriginalDividido:
                case StatusBoleto.Parcial:
                case StatusBoleto.Valido:
                case StatusBoleto.Saldinho:
                case StatusBoleto.Acumulado:
                default:
                    break;
            }

            if (rowPrincipal.BOLCancelado)
            {
                UltimoErro = "Boleto cancelado";
                return false;
            }

            if (rowPrincipal.IsBOLPagamentoNull())
            {
                UltimoErro = "Boleto nao quitado";
                return false;
            }

            //checar se � acordo
            //checar se � super boleto

            bool retorno = false;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto Boleto - 2146");
                rowPrincipal.SetBOLPagamentoNull();
                rowPrincipal.SetBOLValorPagoNull();
                rowPrincipal.SetBOL_ARLNull();
                rowPrincipal.SetBOL_CCDNull();
                if (TipoCan == TipoCancelaBaixa.acordo)
                    rowPrincipal.BOLCancelado = true;
                //APAGANDO JUROS


                BODrowsOriginal = rowPrincipal.GetBOletoDetalheRows();
                decimal ValorOrigialCalculado = 0;
                foreach (dBoletos.BOletoDetalheRow BODrow in BODrowsOriginal)
                    if (BODrow.BODPrevisto)
                        ValorOrigialCalculado += BODrow.BODValor;
                if (rowPrincipal.BOLValorOriginal == ValorOrigialCalculado)
                {
                    foreach (dBoletos.BOletoDetalheRow BODrow in BODrowsOriginal)
                        if ((!BODrow.BODPrevisto) && (!BODrow.BOD_PLA.EstaNoGrupo("120002", "920002")))
                            BODrow.Delete();
                    dBoletos.BOletoDetalheTableAdapter.Update(dBoletos.BOletoDetalhe);
                }
                retorno = SalvarAlteracoes("Cancelamento da quita��o\r\n" + Justificativa);

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                //VirOleDb.TableAdapter.STTableAdapter.Vircatch(null);
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
            }

            return retorno;
        }





        /// <summary>
        /// Remove o conte�do se passou a validade
        /// </summary>
        /// <param name="Pagamento"></param>        
        /// <returns></returns>
        public bool AjustaSeguro(DateTime Pagamento/*,decimal ValorPago*/)
        {
            DateTime DataseguroI = new DateTime(rowPrincipal.BOLVencto.Year, rowPrincipal.BOLVencto.Month, 1).AddMonths(1);
            if (Pagamento > DataseguroI)
                return RemoveSeguro(TipoRemoveSeguro.remove_SeguroVencido);
            else
                return false;

        }





        /*
        /// <summary>
        /// Esta fun��o � para corrigir um erro ser� desativada completamente com o balancete novo
        /// </summary>
        /// <returns></returns>
        public bool ExportaBODparaBalancete()
        {
            if (rowPrincipal.IsBOL_APTNull())
                return false;
            if (rowPrincipal.BOLTipoCRAI == "S")
                return false;
            if (!_expotaBalanceteAccess)
                return false;
            if (rowPrincipal.IsBOLPagamentoNull())
                return false;
            string ComandoVerifica = "SELECT 1 FROM [Lancamentos Bradesco] WHERE (Codcon = @P1) AND (Apartamento = @P2) AND (Bloco = @P3) AND ([N�mero Documento] = @P4)";
            if (VirOleDb.TableAdapter.STTableAdapter.EstaCadastrado(ComandoVerifica, Condominio.CONCodigo, Apartamento.APTNumero, Apartamento.BLOCodigo, rowPrincipal.BOL))
                return false;
            try
            {
                VirOleDb.TableAdapter.AbreTrasacaoOLEDB("Boletos Boleto Boleto - ExportaBODparaBalancete 2576");
                foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                {
                    DSCentral.PLAnocontasRow rowPLA = DSCentral.DCentral.PlanoDeContas.FindByPLA(rowBOD.BOD_PLA);
                    object[] Parametros = new object[11];
                    Parametros[0] = Condominio.CONCodigo;
                    Parametros[1] = Apartamento.APTNumero;
                    Parametros[2] = Apartamento.BLOCodigo;
                    Parametros[3] = rowPrincipal.BOL;
                    Parametros[4] = (double)rowBOD.BODValor;
                    Parametros[5] = DateTime.Today;
                    Parametros[6] = rowPrincipal.BOLPagamento;
                    Parametros[7] = rowBOD.BOD_PLA;
                    if (rowPLA.PLADescricaoLivre)
                    {
                        string manobra = rowBOD.BODMensagem;
                        if (manobra.Contains(" (Pagamento Parcial)"))
                            manobra = manobra.Replace(" (Pagamento Parcial)", string.Empty).Trim();
                        Parametros[8] = manobra;
                    }
                    else
                        Parametros[8] = rowPLA.PLADescricao;


                    Parametros[9] = rowPrincipal.BOLCompetenciaMes.ToString("00") + rowPrincipal.BOLCompetenciaAno.ToString("0000");
                    if (!rowPrincipal.IsBOL_CCDNull())
                        Parametros[10] = rowPrincipal.BOL_CCD;
                    else
                        Parametros[10] = DBNull.Value;
                    if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLanca, Parametros) != 1)
                        throw new Exception("Erro no lancamento");
                };
                VirOleDb.TableAdapter.STTableAdapter.Commit();
                return true;
            }
            catch (Exception e)
            {
                VirOleDb.TableAdapter.STTableAdapter.Vircatch(e);
                return false;
            }
        }*/

        

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Limpar"></param>
        public void GravaNoBalanceteVelho(bool Limpar)
        {
            string ComandoVerifica = "SELECT 1 FROM [Lancamentos Bradesco] WHERE (Codcon = @P1) AND (Apartamento = @P2) AND (Bloco = @P3) AND ([N�mero Documento] = @P4)";
            string ComandoApaga = "DELETE FROM [Lancamentos Bradesco] WHERE (Codcon = @P1) AND (Apartamento = @P2) AND (Bloco = @P3) AND ([N�mero Documento] = @P4)";
            //decimal Total = 0;
            if (Limpar && (VirOleDb.TableAdapter.STTableAdapter.EstaCadastrado(ComandoVerifica, Condominio.CONCodigo, Apartamento.APTNumero, Apartamento.BLOCodigo, rowPrincipal.BOL)))
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, Condominio.CONCodigo, Apartamento.APTNumero, Apartamento.BLOCodigo, rowPrincipal.BOL);
            if (rowPrincipal.BOLCancelado)
                return;
            foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
            {
                DSCentral.PLAnocontasRow rowPLA = DSCentral.DCentral.PlanoDeContas.FindByPLA(rowBOD.BOD_PLA);
                //Total += rowBOD.BODValor;
                object[] Parametros = new object[11];
                Parametros[0] = Condominio.CONCodigo;
                Parametros[1] = Apartamento.APTNumero;
                Parametros[2] = Apartamento.BLOCodigo;
                Parametros[3] = rowPrincipal.BOL;
                Parametros[4] = (double)rowBOD.BODValor;
                Parametros[5] = DateTime.Today;
                Parametros[6] = rowPrincipal.BOLPagamento;
                Parametros[7] = rowBOD.BOD_PLA;
                if (rowPLA.PLADescricaoLivre)
                {
                    string manobra = rowBOD.BODMensagem;
                    if (manobra.Contains(" (Pagamento Parcial)"))
                        manobra = manobra.Replace(" (Pagamento Parcial)", string.Empty).Trim();
                    Parametros[8] = manobra;
                }
                else
                    Parametros[8] = rowPLA.PLADescricao;
                Parametros[9] = rowPrincipal.BOLCompetenciaMes.ToString("00") + rowPrincipal.BOLCompetenciaAno.ToString("0000");
                //if (CCD.HasValue)
                //    Parametros[10] = CCD.Value;
                //else
                Parametros[10] = DBNull.Value;
                if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLanca, Parametros) != 1)
                    throw new Exception("Erro no lancamento");
            }
        }*/

        internal string QuitarComCredAnterior = null;

        private int? _CCT;

        /// <summary>
        /// conta de cr�dito do boleto
        /// </summary>
        public int? CCT
        {
            get
            {
                if (!_CCT.HasValue)
                    if (!rowPrincipal.IsBOL_CCDNull())
                        _CCT = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT SCC_CCT FROM SaldoContaCorrente INNER JOIN ContaCorrenteDetalhe ON SaldoContaCorrente.SCC = ContaCorrenteDetalhe.CCD_SCC WHERE (CCD = @P1)", rowPrincipal.BOL_CCD);
                    else if (!rowPrincipal.IsBOL_ARLNull())
                        _CCT = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT ARL_CCT FROM ARquivoLido WHERE (ARL = @P1)", rowPrincipal.BOL_ARL);
                return _CCT;
            }
        }

        /// <summary>
        /// Dados da conta cr�dito do boleto
        /// </summary>
        public dContasLogicas.ContaCorrenTeRow rowCCT
        {
            get
            {
                if (CCT.HasValue)
                    return dContasLogicas.dContasLogicasSt.ContaCorrenTe.FindByCCT(CCT.Value);
                else
                    return null;
            }
        }

        /// <summary>
        /// Status de registro
        /// </summary>
        public StatusRegistroBoleto StatusRegistro
        {
            get
            {
                return (StatusRegistroBoleto)rowPrincipal.BOLStatusRegistro;
            }
            set
            {
                rowPrincipal.BOLStatusRegistro = (int)value;
            }
        }

        private bool _Baixa_nao_Acordo(string TipoBaixa, DateTime Pagamento, decimal ValorPago, decimal MultaPaga, int? CCD, int? ARL, int? ACO)
        {
            
            
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto Boleto - _Baixa_nao_Acordo 2346", dBoletos.BOletoDetalheTableAdapter);
                //if (expotaBalanceteAccess)
                //    VirOleDb.TableAdapter.AbreTrasacaoOLEDB("Boletos Boleto Boleto - _Baixa_nao_Acordo 2347");
                rowPrincipal.BOLValorPago = ValorPago;
                rowPrincipal.BOLPagamento = Pagamento;
                StatusRegistro = StatusRegistroBoleto.BoletoQuitado;
                if (ARL.HasValue)
                    rowPrincipal.BOL_ARL = ARL.Value;
                if (CCD.HasValue)
                    rowPrincipal.BOL_CCD = CCD.Value;

                //SalvarAlteracoes("Boleto Quitado");

                if (rowPrincipal.BOLTipoCRAI == "E")
                {
                    dBoletos.BOletoDetalheRow[] rowBODs = rowPrincipal.GetBOletoDetalheRows();
                    if ((rowBODs.Length != 1) || (rowBODs[0].BOD_PLA != PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraC)))
                        throw new Exception("Boleto tipo E fora do padr�o");
                    if (rowBODs[0].BODValor != ValorPago)
                    {
                        rowBODs[0].BODValor = ValorPago;
                        dBoletos.BOletoDetalheTableAdapter.Update(rowBODs[0]);
                        rowBODs[0].AcceptChanges();
                        MultaPaga = 0;
                    }
                    string PLAD = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD);
                    Competencia proxComp;
                    string Descritivo2;
                    if (TravaCompetenciaDesconto == null)
                    {
                        proxComp = Framework.objetosNeon.Competencia.Ultima(CON).Add(1);
                        Descritivo2 = string.Format("Desconto pagamento antecipado em {0:dd/MM/yyyy}", Pagamento);
                    }
                    else
                    {
                        proxComp = TravaCompetenciaDesconto;
                        Descritivo2 = string.Format("Desconto pag. boleto cancelado ({0:dd/MM/yyyy})", Pagamento);
                    }
                    int CTL = dPLAnocontas.dPLAnocontasSt.CTLPadrao(CON, CTLTipo.CreditosAnteriores);
                    dBoletos.BOletoDetalheRow rowBODD = Boleto.GerarBOD(CON,
                                                                                       APT,
                                                                                       Proprietario,
                                                                                       PLAD,
                                                                                       true,
                                                                                       proxComp,
                                                                                       proxComp.CalculaVencimento(),
                                                                                       Descritivo2,
                                                                                       -ValorPago,
                                                                                       false,
                                                                                       CTL);
                    AmarraBODs(rowBODs[0].BOD, rowBODD.BOD);
                    SalvarAlteracoes(string.Format("Boleto E Quitado - {0}", TipoBaixa));
                }
                else if (rowPrincipal.BOLTipoCRAI == "S")
                {
                    SalvarAlteracoes("Boleto Quitado");
                    string PLAseguro = PadraoPLA.PLAPadraoCodigo(PLAPadrao.segurocosnteudoC);
                    if (ValorPago >= (2 * rowPrincipal.BOLValorPrevisto))
                    {
                        int parcelas = (int)Math.Truncate(ValorPago / rowPrincipal.BOLValorPrevisto);
                        Competencia comp = new Competencia(rowPrincipal.BOLCompetenciaMes, rowPrincipal.BOLCompetenciaAno);
                        DateTime Vencimento = rowPrincipal.BOLVencto;
                        for (int i = 1; i < parcelas; i++)
                        {
                            comp++;
                            Vencimento = Vencimento.AddMonths(1);
                            DateTime dataVI = new DateTime(Vencimento.Year, Vencimento.Month, 1).AddMonths(1);
                            DateTime dataVF = dataVI.AddMonths(1).AddDays(-1);
                            string Mensagem = string.Format("Seguro conte�do ({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", dataVI, dataVF);
                            dBoletos.BOletoDetalheRow novoBOD = Boleto.GerarBOD(rowPrincipal.BOL_CON, rowPrincipal.BOL_APT, rowPrincipal.BOLProprietario,
                                                               PLAseguro, false, comp, Vencimento, Mensagem, rowPrincipal.BOLValorPrevisto, true);

                            Boleto novoBol = new Boleto(rowPrincipal.BOL_CON, Apartamento, rowPrincipal.BOLProprietario, comp, Vencimento, "S",
                                                                      StatusBoleto.Valido, novoBOD.BOD);
                            novoBol.Baixa(TipoBaixa, Vencimento, rowPrincipal.BOLValorPrevisto, 0, -1, -1);
                        }
                        rowPrincipal.BOLValorPrevisto = rowPrincipal.BOLValorPago - (parcelas - 1) * rowPrincipal.BOLValorPrevisto;
                        //dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                        //rowPrincipal.AcceptChanges();
                        SalvarAlteracoes(string.Format("Boleto quitado para {0} meses", parcelas));
                    }
                }
                else
                {
                    
                    decimal Total = 0;

                    /*
                    if (expotaBalanceteAccess)
                        if (VirOleDb.TableAdapter.STTableAdapter.EstaCadastrado(ComandoVerifica, Condominio.CONCodigo, Apartamento.APTNumero, Apartamento.BLOCodigo, rowPrincipal.BOL))
                            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, Condominio.CONCodigo, Apartamento.APTNumero, Apartamento.BLOCodigo, rowPrincipal.BOL);
                    */
                    foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                    {
                        DSCentral.PLAnocontasRow rowPLA = DSCentral.DCentral.PlanoDeContas.FindByPLA(rowBOD.BOD_PLA);
                        if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho))
                        {
                            Boleto BolSaldinho = new Boleto(rowBOD.BODAcordo_BOL);
                            BolSaldinho.rowPrincipal.SetBOLPagamentoNull();
                            decimal MultaBoletoSaldinho = 0;
                            BolSaldinho.AjustaBoletoParaQuitacao(Pagamento, rowBOD.BODValor, ref MultaBoletoSaldinho);
                            string strTipoBaixa = string.Format("Baixa de saldo pelo boleto {0}", BOL);
                            BolSaldinho.rowPrincipal.BOLCancelado = false;
                            BolSaldinho.BOLStatus = ((BolSaldinho.BOLStatus == StatusBoleto.Acumulado) ? StatusBoleto.Valido : StatusBoleto.Parcial);
                            //BolSaldinho.Baixa(strTipoBaixa, Pagamento, rowBOD.BODValor, rowBOD.BODValor - BolSaldinho.rowPrincipal.BOLValorPrevisto, CCD, ARL, ACO);
                            BolSaldinho.Baixa(strTipoBaixa, Pagamento, rowBOD.BODValor, MultaBoletoSaldinho, CCD, ARL, ACO);
                            ValorPago -= rowBOD.BODValor;
                            rowPrincipal.BOLValorPago = ValorPago;
                            rowPrincipal.BOLValorPrevisto = rowPrincipal.BOLValorPrevisto - rowBOD.BODValor;
                            continue;
                        }
                        Total += rowBOD.BODValor;

                        //if (!PLAExtraNominal.Contains(Framework.datasets.dPLAnocontas.PlaPadrao(rowBOD.BOD_PLA)))
                        //    ValorNominal += rowBOD.BODValor;
                        /*
                        if (expotaBalanceteAccess)
                        {
                            object[] Parametros = new object[11];
                            Parametros[0] = Condominio.CONCodigo;
                            Parametros[1] = Apartamento.APTNumero;
                            Parametros[2] = Apartamento.BLOCodigo;
                            Parametros[3] = rowPrincipal.BOL;
                            Parametros[4] = (double)rowBOD.BODValor;
                            Parametros[5] = DateTime.Today;
                            Parametros[6] = Pagamento;
                            Parametros[7] = rowBOD.BOD_PLA;
                            if (rowPLA.PLADescricaoLivre)
                            {
                                string manobra = rowBOD.BODMensagem;
                                if (manobra.Contains(" (Pagamento Parcial)"))
                                    manobra = manobra.Replace(" (Pagamento Parcial)", string.Empty).Trim();
                                Parametros[8] = manobra;
                            }
                            else
                                Parametros[8] = rowPLA.PLADescricao;
                            Parametros[9] = rowPrincipal.BOLCompetenciaMes.ToString("00") + rowPrincipal.BOLCompetenciaAno.ToString("0000");
                            if (CCD.HasValue)
                                Parametros[10] = CCD.Value;
                            else
                                Parametros[10] = DBNull.Value;
                            //Parametros[10] = oCCD_LANCAMENTOSBRADESCO;
                            if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLanca, Parametros) != 1)
                                throw new Exception("Erro no lan�amento");
                        }*/
                        if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.honorarios))
                            if (ACO.GetValueOrDefault(0) != 0)
                            {
                                Acordo.Acordo AcordoAtual = new Boletos.Acordo.Acordo(ACO.Value);
                                if (AcordoAtual.FRN > 0)
                                {
                                    if (!RegistraHonorario(Pagamento, rowBOD, AcordoAtual))
                                        throw new Exception("Erro no registro dos honor�rios");
                                }
                                else
                                {
                                    string erro = string.Format("Acordo: ACO={0}", ACO);
                                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", erro, "erro do honor�rio");
                                }
                            }
;
                        if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.CobrancaAtivaC))
                            if (!RegistraCobrancaAtiva(Pagamento, rowBOD))
                                throw new Exception("Erro no registro da cobran�a Ativa");
                        ;
                        if (rowBOD.IsBOD_CTLNull())
                        {
                            int? CTL = null;
                            if (rowBOD.BOD_PLA == "110000")
                                CTL = BuscaCTL(CTLTipo.Fundo);
                            else
                                CTL = BuscaCTL(CTLTipo.Caixa);
                            if (CTL.HasValue)
                            {
                                rowBOD.BOD_CTL = CTL.Value;
                                //Quando o rowBOD.BOD_PLA = 0 ele � falso (super) e n�o pode ser salvo
                                if (rowBOD.BOD_PLA != "0")
                                    dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                                else
                                    rowBOD.AcceptChanges();
                            }
                        }
                        TransfereArrecadado(rowBOD);
                    };

                    if (Math.Abs((Total + MultaPaga) - ValorPago) > 0.01M)
                        throw new Exception(string.Format("{0} Itens diferentes do total {1:n2} - {2:n2}", rowPrincipal.BOL, ValorPago, Total));
                    //if (ValorPrevisto != ValorNominal)
                    //  ValorPrevisto = ValorNominal;
                    SalvarAlteracoes(string.Format("Boleto Quitado - {0}", TipoBaixa));
                    MultaPaga = Math.Round(MultaPaga, 2);
                    if (MultaPaga > 0.009m)
                    {
                        IncluiBOD(PadraoPLA.PLAPadraoCodigo(PLAPadrao.MultasAtraso), "Multas", MultaPaga, false, null, BuscaCTL(CTLTipo.Caixa));
                        //Referencia ONENote : Acerto do boleto
                        /*
                        if (expotaBalanceteAccess)
                        {
                            //Incluir multa                            
                            object[] Parametros = new object[11];
                            Parametros[0] = Condominio.CONCodigo;
                            Parametros[1] = Apartamento.APTNumero;
                            Parametros[2] = Apartamento.BLOCodigo;
                            Parametros[3] = rowPrincipal.BOL;
                            Parametros[4] = (double)MultaPaga;
                            Parametros[5] = DateTime.Today;
                            Parametros[6] = Pagamento;
                            Parametros[7] = PadraoPLA.PLAPadraoCodigo(PLAPadrao.MultasAtraso);
                            Parametros[8] = "Multas";
                            Parametros[9] = rowPrincipal.BOLCompetenciaMes.ToString("00") + rowPrincipal.BOLCompetenciaAno.ToString("0000");
                            if (CCD.HasValue)
                                Parametros[10] = CCD.Value;
                            else
                                Parametros[10] = DBNull.Value;
                            if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLanca, Parametros) != 1)
                                throw new Exception("Erro no lan�amento da multa");

                        }*/


                    }
                    else
                        if (MultaPaga < -0.009m)
                    {
                        //Desconta no condom�nio
                        if (QuitarComCredAnterior != null)
                            IncluiBOD(PadraoPLA.PLAPadraoCodigo(PLAPadrao.CreditoNaoIdentificado), QuitarComCredAnterior, MultaPaga, false, null, BuscaCTL(CTLTipo.CreditosAnteriores));
                        else
                            IncluiBOD(PadraoPLA.PLAPadraoCodigo(PLAPadrao.Condominio), "Desconto", MultaPaga, false, null, BuscaCTL(CTLTipo.Caixa));

                        //Incluir multa
                        //LOG += " Cadastrando Multa\r\n";
                        /*
                        if (expotaBalanceteAccess)
                        {
                            object[] Parametros = new object[11];
                            Parametros[0] = Condominio.CONCodigo;
                            Parametros[1] = Apartamento.APTNumero;
                            Parametros[2] = Apartamento.BLOCodigo;
                            //Refer�ncia ONENOTE Troca para BOL na busca
                            //if ((!rowPrincipal.IsBOLNNAntigoNull()) && (rowPrincipal.BOLNNAntigo != rowPrincipal.BOL))
                            //    Parametros[3] = rowPrincipal.BOLNNAntigo;
                            //else
                            Parametros[3] = rowPrincipal.BOL;
                            Parametros[4] = (double)MultaPaga;
                            Parametros[5] = DateTime.Today;
                            Parametros[6] = Pagamento;
                            Parametros[7] = PadraoPLA.PLAPadraoCodigo(PLAPadrao.Condominio);
                            Parametros[8] = "Desconto";
                            Parametros[9] = rowPrincipal.BOLCompetenciaMes.ToString("00") + rowPrincipal.BOLCompetenciaAno.ToString("0000");
                            if (CCD.HasValue)
                                Parametros[10] = CCD.Value;
                            else
                                Parametros[10] = DBNull.Value;
                            if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLanca, Parametros) != 1)
                                throw new Exception("Erro no lancamento do desconto");
                        }
                        //LOG += " DESCONTO P3->" + Parametros[3].ToString() + " BOLNNAntigo->" + rowPrincipal.BOLNNAntigo.ToString() + " NN->" + this.NN.ToString() + " - " + MultaPaga.ToString() + "\r\n";
                        */
                    }

                }


                VirMSSQL.TableAdapter.CommitSQL();
                //if (expotaBalanceteAccess)
                //    VirOleDb.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                //if (expotaBalanceteAccess)
                //    VirOleDb.TableAdapter.STTableAdapter.Vircatch(null);
                VirMSSQL.TableAdapter.VircatchSQL(e);

                UltimaException = e;
                UltimoErro = e.Message;
                return false;
            }
            return true;
        }

        private DataView _DV_ConTasLogicas_CTLTipo;

        private DataView DV_ConTasLogicas_CTLTipo
        {
            get
            {
                if (_DV_ConTasLogicas_CTLTipo == null)
                {
                    _DV_ConTasLogicas_CTLTipo = new DataView(dBoletos.ConTasLogicas);
                    _DV_ConTasLogicas_CTLTipo.Sort = "CTLTipo";
                }
                return _DV_ConTasLogicas_CTLTipo;
            }
        }

        private void CarregaDadosCTL()
        {
            dBoletos.ConTasLogicasTableAdapter.EmbarcaEmTransST();
            dBoletos.CTLxCCTTableAdapter.EmbarcaEmTransST();
            dBoletos.BODxPAGTableAdapter.EmbarcaEmTransST();
            if (dBoletos.ConTasLogicas.Count == 0)
                dBoletos.ConTasLogicasTableAdapter.Fill(dBoletos.ConTasLogicas, CON);
            if (dBoletos.CTLxCCT.Count == 0)
                dBoletos.CTLxCCTTableAdapter.FillByCON(dBoletos.CTLxCCT, CON);
            if (dBoletos.BODxPAG.Count == 0)
                dBoletos.BODxPAGTableAdapter.FillByBOL(dBoletos.BODxPAG, BOL);
        }

        private int? BuscaCTL(CTLTipo Tipo)
        {

            //return dPLAnocontas.dPLAnocontasSt.CTLPadrao(CON, Tipo);

            CarregaDadosCTL();
            int ind = DV_ConTasLogicas_CTLTipo.Find((int)Tipo);
            if (ind >= 0)
                return ((dBoletos.ConTasLogicasRow)DV_ConTasLogicas_CTLTipo[ind].Row).CTL;
            else
                return null;

        }

        private static int? SPL500000 = null;

        /// <summary>
        /// Transfere o arrecadado
        /// </summary>
        /// <returns></returns>
        public bool TransfereArrecadado()
        {
            bool retorno = false;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllboletos boletos Bolto.cs - 2857");
                foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                    if (TransfereArrecadado(rowBOD))
                        retorno = true;
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
                return false;
            }
            return retorno;
        }

        private bool TransfereArrecadado(dBoletos.BOletoDetalheRow rowBOD)
        {            
            if (rowBOD.IsBOD_CTLNull())
            {
                dBoletos.BOletoDetalheTableAdapter.EmbarcaEmTransST();
                int? CTL = null;
                if (rowBOD.BOD_PLA == "110000")
                    CTL = BuscaCTL(CTLTipo.Fundo);
                else
                    CTL = BuscaCTL(CTLTipo.Caixa);
                if (CTL.HasValue)
                {
                    rowBOD.BOD_CTL = CTL.Value;
                    dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                    rowBOD.AcceptChanges();
                }
            }
            if (rowBOD.IsBOD_CTLNull())
                return false;
            CarregaDadosCTL();
            //estranhamente rowBOD.ConTasLogicasRow vem como null !!!
            //dBoletos.CTLxCCTRow[] CTLxCCTRows = rowBOD.ConTasLogicasRow.GetCTLxCCTRows();
            dBoletos.ConTasLogicasRow rowCTL = dBoletos.ConTasLogicas.FindByCTL(rowBOD.BOD_CTL);
            if (rowCTL == null)
                return false;
            dBoletos.CTLxCCTRow[] CTLxCCTRows = rowCTL.GetCTLxCCTRows();
            if (CTLxCCTRows.Length != 1)
                return false;
            if (rowBOD.GetBODxPAGRows().Length != 0)
                return false;
            Competencia Compet = new Competencia(rowPrincipal.BOLPagamento, CON);
            CTLCCTTranArrecadado TipoTranArrecadado = (CTLCCTTranArrecadado)CTLxCCTRows[0].CTLCCTTranArrecadado;
            malote mal;
            DateTime VencimentoCheque;
            bool saldo = false;
            if (TipoTranArrecadado == CTLCCTTranArrecadado.NoPeriodo)
            {
                //VencimentoCheque = dFERiados.DiasUteis(Compet.DataFimBalancete, 2, Framework.datasets.dFERiados.Sentido.retorna);
                VencimentoCheque = Compet.DataFimBalancete.SomaDiasUteis(2, Sentido.Traz);
                //malote de volta
                mal = malote.MalotePara(CON, VencimentoCheque);
                //malote de ida
                mal--;
                if (mal.DataEnvio < DateTime.Now)
                    saldo = true;                
            }
            if (saldo || (TipoTranArrecadado == CTLCCTTranArrecadado.NoProximoPeriodo))
            {
                DateTime EmissaoCheque = Compet.DataFimBalancete.SomaDiasUteis(3, Sentido.Frente);
                if (EmissaoCheque < DateTime.Now)
                    EmissaoCheque = DateTime.Now;
                //malote de ida
                mal = malote.PrimeiroMaloteApos(CON, EmissaoCheque);
                //malote de volta
                mal++;
                VencimentoCheque = mal.DataRetorno.SomaDiasUteis(1, Sentido.Frente);
            }
            else
                return false;
            string PLA = "500000";
            if (!SPL500000.HasValue)
                SPL500000 = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select SPL from SubPLano where SPL_PLA = @P1", PLA);
            string Servico = string.Format("Apli {0} ref: {1}{2}", rowCTL.CTLTitulo, Compet, saldo ? " saldo" : string.Empty);
            if (Servico.Length > 50)
            {
                Servico = string.Format("Ap {0} {1}{2}", rowCTL.CTLTitulo, Compet, saldo ? " saldo" : string.Empty);
                if (Servico.Length > 50)
                {
                    int delta = Servico.Length - 50;
                    Servico = string.Format("Ap {0} {1}{2}", rowCTL.CTLTitulo.Substring(0, rowCTL.CTLTitulo.Length - delta), Compet, saldo ? " saldo" : string.Empty);
                }
            }
            string Favorecido = Apartamento.Condominio.Nome;

            string DadosDep = string.Format("{0} Ag {1}-{2} {3} {4}-{5}", CTLxCCTRows[0].BCONome,
                                                                          CTLxCCTRows[0].CCTAgencia,
                                                                          CTLxCCTRows[0].CCTAgenciaDg,
                                                                          CTLxCCTRows[0].CCTTipo == 0 ? "CC" : "Poupan�a",
                                                                          CTLxCCTRows[0].CCTConta,
                                                                          CTLxCCTRows[0].CCTContaDg);
            //CTL � usado no campo Numero
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllBoletos Boletos Boleto.cs - 2894", dBoletos.BODxPAGTableAdapter);
                ContaPagar.Nota Nota = new ContaPagar.Nota(CON, Compet, NOATipo.TransferenciaF, rowCTL.CTL);
                if (!Nota.Encontrada())
                    Nota = new ContaPagar.Nota(CON,
                                               null,
                                               rowCTL.CTL,
                                               DateTime.Today,
                                               0,
                                               NOATipo.TransferenciaF,
                                               PLA,
                                               SPL500000.Value,
                                               Servico,
                                               Compet,
                                               "",
                                               "",
                                               PAGTipo.TrafArrecadado,
                                               NOAStatus.Cadastrada,string.Empty,
                                               DadosDep);
                int PAG;
                
                if (rowCON.IsCONCodigoComunicacaoNull() || rowCON.CONCodigoComunicacao == string.Empty || !ContaCondominio.PagamentoEletronico)
                    PAG = Nota.addPAG(PAGTipo.TrafArrecadado, VencimentoCheque, rowBOD.BODValor, true, true, Favorecido, true, DadosDep);
                else
                {                                     
                    DadosDep = string.Format("{0} - CCT = {1}", DadosDep, CTLxCCTRows[0].CCT);                    
                    PAG = Nota.addPAG(PAGTipo.PECreditoConta, VencimentoCheque, rowBOD.BODValor, true, true, Favorecido, true, DadosDep, CCT);
                }                
                dBoletos.BODxPAGRow BODxPAGRow = dBoletos.BODxPAG.NewBODxPAGRow();
                BODxPAGRow.BOD = rowBOD.BOD;
                BODxPAGRow.PAG = PAG;
                dBoletos.BODxPAG.AddBODxPAGRow(BODxPAGRow);
                dBoletos.BODxPAGTableAdapter.Update(BODxPAGRow);
                BODxPAGRow.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                //System.Console.WriteLine(string.Format("erro BOD = {0}", rowBOD.BOD));
                VirMSSQL.TableAdapter.VircatchSQL(e);
                throw e;
            }
            return true;
        }






        /// <summary>
        /// Unidade - j� com o bloco se for o caso
        /// </summary>
        public string Unidade
        {
            get
            {
                if (APT.HasValue)
                    if (Apartamento.BLOCodigo.ToUpper() != "SB")
                        return string.Format("{0} {1}", Apartamento.BLOCodigo, Apartamento.APTNumero);
                    else
                        return Apartamento.APTNumero;
                else
                    return "-";
            }
        }

        internal class Antecipar
        {
            public dBoletos.BOletoDetalheRow row;
            public decimal ValorPendente;

            public Antecipar(dBoletos.BOletoDetalheRow _row)
            {
                row = _row;
                ValorPendente = row.BODValor;
            }
        }

        private List<Antecipar> BODsAntecipados;

        private void GuardaAntecipado(dBoletos.BOletoDetalheRow row)
        {
            if (BODsAntecipados == null)
                BODsAntecipados = new List<Antecipar>();
            BODsAntecipados.Add(new Antecipar(row));
        }

        private decimal TranfereAntecipado(Boleto BolDescontar, decimal Teto)
        {
            if ((BODsAntecipados == null) || (BODsAntecipados.Count == 0))
                return 0;
            decimal retorno = 0;
            while ((BODsAntecipados.Count > 0) && (Teto > 0))
            {
                Antecipar Antecipar1 = BODsAntecipados[0];
                decimal ValorTranferir;
                if (Teto + Antecipar1.ValorPendente >= 0)
                {
                    ValorTranferir = Antecipar1.ValorPendente;
                    BODsAntecipados.Remove(Antecipar1);
                }
                else
                {
                    ValorTranferir = -Teto;
                    Antecipar1.ValorPendente += Teto;
                };
                Teto += ValorTranferir;
                dBoletos.BOletoDetalheRow novorowBOD = BolDescontar.IncluiBOD(Antecipar1.row.BOD_PLA, Antecipar1.row.BODMensagem, ValorTranferir, false, false, null, Antecipar1.row.BOD_CTL);
                int? BODorigem = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select BODOrigem from BODxBOD where BODDestino = @P1", Antecipar1.row.BOD);
                if (BODorigem.HasValue)
                    AmarraBODs(BODorigem.Value, novorowBOD.BOD);
                retorno += ValorTranferir;
            }
            if (!BolDescontar.rowPrincipal.IsBOLValorPagoNull())
            {
                BolDescontar.rowPrincipal.BOLValorPago += retorno;
                BolDescontar.SalvarAlteracoesSemRegistrar();
            }
            return retorno;
        }

        /// <summary>
        /// Utiliza este boleto para quita��o de boletos em aberto da unidade
        /// </summary>
        internal void QuitacaoAutomaticaBolExtra()
        {
            if (TipoCRAIE != BOLTipoCRAI.Extra)
                return;
            dBoletos dBoletosAbertos = new dBoletos();
            dBoletosAbertos.BOLetosTableAdapter.EmbarcaEmTransST();
            dBoletosAbertos.BOletoDetalheTableAdapter.EmbarcaEmTransST();
            dBoletosAbertos.BOLetosTableAdapter.FillByAPTCorte(dBoletosAbertos.BOLetos, APT, DateTime.Today);
            dBoletosAbertos.BOletoDetalheTableAdapter.FillByAPT1(dBoletosAbertos.BOletoDetalhe, APT);
            dBoletos.BOletoDetalheRow[] rowDets = rowPrincipal.GetBOletoDetalheRows();
            if (rowDets.Length != 1)
                return;
            dRasCredito dRasCredito1 = new dRasCredito();
            dRasCredito1.BoletosDesTableAdapter.EmbarcaEmTransST();
            dRasCredito1.BoletosDesTableAdapter.Fill(dRasCredito1.BoletosDes, rowDets[0].BOD);
            SortedList<int, decimal> Descontar = new SortedList<int, decimal>();
            foreach (dRasCredito.BoletosDesRow rowBODDesconto in dRasCredito1.BoletosDes)
            {
                if (!rowBODDesconto.IsBOLNull())
                    continue;
                Descontar.Add(rowBODDesconto.BOD, rowBODDesconto.BODValor);
            }
            //Descontar.Add(rowDets[0].BOD,-rowDets[0].BODValor);
            foreach (dBoletos.BOLetosRow BOLrow in dBoletosAbertos.BOLetos)
            {
                Boleto BolCandidato = new Boleto(BOLrow);
                if ((
                     (BolCandidato.TipoCRAIE == BOLTipoCRAI.condominio)
                     ||
                     (BolCandidato.TipoCRAIE == BOLTipoCRAI.Rateio)
                    )
                    &&
                    (BolCandidato.rowPrincipal.IsBOLPagamentoNull())
                  )
                {
                    Descontar = BolCandidato.QuitarComExtra(rowPrincipal.BOLPagamento, Descontar.Values[0], Descontar);
                    if (Descontar == null)
                        break;
                }
            }
        }


        /// <summary>
        /// Quita o boleto com os cr�ditos de um boleto extra
        /// </summary>
        /// <param name="DataCredito">Data do cr�dito usada para calclulo do boleto</param>
        /// <param name="Disponivel">Valor total disponivel para</param>
        /// <param name="BODsDesconto">lista dos bods de cr�dito dispon�veis</param>
        /// <returns>Bod saldo usado no caso de quita��o autom�tica</returns>
        internal SortedList<int, decimal> QuitarComExtra(DateTime DataCredito, decimal Disponivel, SortedList<int, decimal> BODsDesconto)
        {
            Boleto BolDescontar = this;
            int BOLDescontar = BOL;
            Valorpara(DataCredito);
            decimal saldo = valorfinal;
            decimal juros = saldo - rowPrincipal.BOLValorPrevisto;
            SortedList<int, decimal> BodSaldo = null;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto.cs QuitarComExtra - 3554", dBoletos.BOletoDetalheTableAdapter);
                if (ValorPrevisto > -Disponivel)
                {
                    //se tem juros usa divis�o proporcional, se n�o � por valor
                    if (juros == 0)
                        BolDescontar = BolDescontar.Dividir("Boleto dividido para quita��o parcial com cr�dito", -Disponivel)[0];
                    else
                    {
                        //tem um caso onde os juros s�o negativos por isso esta nova checagem contra o saldo e nao o previsto.
                        if (-Disponivel < saldo)
                        {
                            decimal Propor = (-Disponivel) / saldo;
                            juros = Math.Round(juros * Propor, 2, MidpointRounding.AwayFromZero);
                            BolDescontar = BolDescontar.Dividir("Boleto dividido para quita��o parcial com cr�dito", -Disponivel - juros)[0];
                        }
                    }
                    BOLDescontar = BolDescontar.BOL;
                }

                foreach (int BODdescont in BODsDesconto.Keys)
                {
                    //if (!rowBODDesconto.IsBOLNull())
                    //    continue;
                    if ((-BODsDesconto[BODdescont]) > saldo)
                    {
                        int? BODSaldoBOL = Boleto.DivideBOD(BODdescont, -saldo);
                        BodSaldo = new SortedList<int, decimal>();
                        BodSaldo.Add(BODSaldoBOL.Value, BODsDesconto[BODdescont] + saldo);
                        saldo = 0;
                    }
                    else
                        saldo += BODsDesconto[BODdescont];
                    BolDescontar.dBoletos.BOletoDetalheTableAdapter.PrendeBOD(BOLDescontar, BODdescont);

                    if (saldo <= 0)
                        break;
                }
                BolDescontar = new Boleto(BOLDescontar);
                BolDescontar.SalvarAlteracoes("Utiliza��o de cr�dito", true);
                //if (saldo <= juros)                            
                BolDescontar.Baixa("Cr�dito", DataCredito, 0, juros);
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
                throw ex;
            }
            return BodSaldo;
        }

        private bool _Baixa(string TipoBaixa, DateTime Pagamento, decimal ValorPago, decimal MultaPaga, int? CCD, int? ARL, int? ACO)
        {
            if (BOLStatus == StatusBoleto.Acordo)
            {
                
                AjustaDataAcordo(Pagamento);
                Acordo.Acordo AcordoAtual = new Boletos.Acordo.Acordo(this, false);
                if (!AcordoAtual.Gerado)
                    throw new Exception("Acordo n�o encontrado");
                dBoletos.BOletoDetalheRow[] rowBODs = rowPrincipal.GetBOletoDetalheRows();
                dBoletos.BOletoDetalheRow rowBOD;
                BODsAntecipados = null;
                List<dBoletos.BOletoDetalheRow> BODsParaTransferir = new List<dBoletos.BOletoDetalheRow>();
                decimal[] Sobremultas = new decimal[rowBODs.Length];
                decimal MultaDoBoletoH = 0;
                if (MultaPaga != 0)
                {
                    decimal Total = 0;
                    int iMaior = -1;
                    for (int i = 0; i < Sobremultas.Length; i++)
                    {
                        rowBOD = rowBODs[i];
                        if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.acordo))
                        {
                            Total += rowBOD.BODValor;
                            if (iMaior == -1)
                                iMaior = i;
                            else
                                if (rowBOD.BODValor > rowBODs[iMaior].BODValor)
                                iMaior = i;
                        }
                    }
                    if (Total == 0)
                        MultaDoBoletoH = MultaPaga;
                    else
                    {
                        decimal erro = MultaPaga;
                        for (int i = 0; i < Sobremultas.Length; i++)
                        {
                            rowBOD = rowBODs[i];
                            if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.acordo))
                            {
                                Sobremultas[i] = Math.Round(rowBOD.BODValor * MultaPaga / Total, 2);
                                erro -= Sobremultas[i];
                            };
                        };
                        Sobremultas[iMaior] += erro;
                    }
                }

                Boleto BoletoH_Novo = null;
                for (int i = 0; i < Sobremultas.Length; i++)
                {
                    rowBOD = rowBODs[i];
                    if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD))
                        GuardaAntecipado(rowBOD);
                    else
                        if (rowBODs[i].BOD_PLA != PadraoPLA.PLAPadraoCodigo(PLAPadrao.acordo))
                    {
                        if ((BoletoH_Novo == null) && (rowBODs[i].BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.honorarios)))
                        {
                            BoletoH_Novo = new Boleto(CON, Apartamento, rowPrincipal.BOLProprietario, Competencia, Pagamento, "H", StatusBoleto.Valido);
                            BoletoH_Novo.SalvarAlteracoes(string.Format("Custos Acordo {0}\r\nBoleto Quitado = {1}", AcordoAtual.ACO, BOL));
                            if (rowBODs[i].IsBODAcordo_BOLNull())
                            {
                                rowBODs[i].BODAcordo_BOL = BoletoH_Novo.BOL;
                                dBoletos.BOletoDetalheTableAdapter.Update(rowBODs[i]);
                                rowBODs[i].AcceptChanges();
                            }
                        }
                        BODsParaTransferir.Add(rowBODs[i]);
                    }
                }
                if (BoletoH_Novo != null)
                {
                    decimal ValorDoBoletoH = 0;
                    foreach (dBoletos.BOletoDetalheRow BODTran in BODsParaTransferir)
                    {
                        int? CTL = null;
                        if (!BODTran.IsBOD_CTLNull())
                            CTL = BODTran.BOD_CTL;
                        dBoletos.BOletoDetalheRow rowBODSub = BoletoH_Novo.IncluiBOD(BODTran.BOD_PLA, BODTran.BODMensagem, BODTran.BODValor, false, false, null, CTL);
                        ValorDoBoletoH += BODTran.BODValor;
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BODxBOD set BODOrigem = @P1 where BODOrigem = @P2", rowBODSub.BOD, BODTran.BOD);
                    };
                    BODsParaTransferir.Clear();
                    AcordoAtual.IncluirBoletoHonorarios(BoletoH_Novo.BOL);
                    if (QuitarComCredAnterior != null)
                    {
                        BoletoH_Novo.QuitarComCredAnterior = QuitarComCredAnterior;
                        if (!BoletoH_Novo.Baixa(TipoBaixa, Pagamento, 0, -ValorDoBoletoH, CCD, ARL, AcordoAtual.ACO))
                        {
                            UltimoErro = "Erro na baixa do boleto de honor�rios " + BoletoH_Novo.UltimoErro;
                            return false;
                        };
                    }
                    else
                    {
                        if (!BoletoH_Novo.Baixa(TipoBaixa, Pagamento, ValorDoBoletoH + MultaDoBoletoH, MultaDoBoletoH, CCD, ARL, AcordoAtual.ACO))
                        {
                            UltimoErro = "Erro na baixa do boleto de honor�rios " + BoletoH_Novo.UltimoErro;
                            return false;
                        };
                        TranfereAntecipado(BoletoH_Novo, ValorDoBoletoH + MultaDoBoletoH);
                    }
                }
                for (int i = 0; i < Sobremultas.Length; i++)
                {
                    rowBOD = rowBODs[i];
                    if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.acordo))
                    {
                        Boleto BolOriginal = new Boleto(rowBOD.BODAcordo_BOL);
                        if (!BolOriginal.Encontrado)
                        {
                            UltimoErro = "Boleto original n�o encontrado: " + rowBOD.BODAcordo_BOL.ToString();
                            return false;
                        };
                        decimal ValorTransferido = 0;
                        foreach (dBoletos.BOletoDetalheRow BODTran in BODsParaTransferir)
                        {
                            int? CTL = null;
                            if (!BODTran.IsBOD_CTLNull())
                                CTL = BODTran.BOD_CTL;
                            BolOriginal.IncluiBOD(BODTran.BOD_PLA, BODTran.BODMensagem, BODTran.BODValor, false, false, null, CTL);
                            ValorTransferido += BODTran.BODValor;
                        };
                        BODsParaTransferir.Clear();
                        BolOriginal.rowPrincipal.BOLCancelado = false;
                        BolOriginal.SalvarAlteracoes(string.Format("Boleto original restaurado na quita��o do acordo {0}\r\nBoleto Quitado = {1}", ACO, BOL));
                        //decimal ValorAtecipado = TranfereAntecipado(BolOriginal, rowBOD.BODValor + Sobremultas[i] + ValorTransferido);
                        if (QuitarComCredAnterior != null)
                        {
                            decimal ValorNominal = BolOriginal.ValorPrevisto;
                            BolOriginal.QuitarComCredAnterior = QuitarComCredAnterior;
                            BolOriginal.IncluiBOD(PadraoPLA.PLAPadraoCodigo(PLAPadrao.CreditoNaoIdentificado), QuitarComCredAnterior, -rowBOD.BODValor, false, null, BuscaCTL(CTLTipo.CreditosAnteriores));
                            if (!BolOriginal.Baixa(TipoBaixa, Pagamento, 0, -BolOriginal.rowPrincipal.BOLValorPrevisto, CCD, ARL, AcordoAtual.ACO))
                            {
                                UltimoErro = "Erro na baixa do boleto original " + BolOriginal.UltimoErro;
                                return false;
                            }
                            else
                            {
                                BolOriginal.ValorPrevisto = ValorNominal;
                                BolOriginal.GravaJustificativa("Ajuste do valor nominal");
                            }
                        }
                        else
                        {
                            if (!BolOriginal.Baixa(TipoBaixa, Pagamento, rowBOD.BODValor + Sobremultas[i] + ValorTransferido, rowBOD.BODValor + ValorTransferido - BolOriginal.rowPrincipal.BOLValorPrevisto + Sobremultas[i], CCD, ARL, AcordoAtual.ACO))
                            {
                                UltimoErro = "Erro na baixa do boleto original " + BolOriginal.UltimoErro;
                                return false;
                            };
                            TranfereAntecipado(BolOriginal, rowBOD.BODValor + Sobremultas[i] + ValorTransferido);
                        }

                        //BolOriginal.rowPrincipal.BOLValorPago -= ValorAtecipado;
                    };
                }
                if ((BODsAntecipados != null) && (BODsAntecipados.Count != 0))
                    throw new Exception(string.Format("Valor antecipado maior do que o total"));
                //else
                //  foreach (Antecipar BODsAntecipado in BODsAntecipados)
                //    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from BODxBOD where BODDestino = @P1", BODsAntecipado.row.BOD);
                rowPrincipal.BOLValorPago = ValorPago;
                rowPrincipal.BOLPagamento = Pagamento;
                if (ARL.HasValue)
                    rowPrincipal.BOL_ARL = ARL.Value;
                if (CCD.HasValue)
                    rowPrincipal.BOL_CCD = CCD.Value;
                rowPrincipal.BOLCancelado = true;
                SalvarAlteracoes("Boleto de acordo quitado");
                AcordoAtual = new Boletos.Acordo.Acordo(AcordoAtual.ACO);
                AcordoAtual.Encerrar();
                return true;
            }
            else
                return _Baixa_nao_Acordo(TipoBaixa, Pagamento, ValorPago, MultaPaga, CCD, ARL, ACO);
        }

        /*
        /// <summary>
        /// Efetua a baixa do boleto com os desdobramentos
        /// </summary>
        /// <param name="Pagamento"></param>
        /// <param name="ValorPago"></param>
        /// <param name="MultaPaga"></param>
        /// <param name="CCD"></param>
        /// <param name="ARL"></param>
        /// <returns></returns>
        public bool Baixa(DateTime Pagamento, decimal ValorPago, decimal MultaPaga, int CCD, int ARL)
        {
            return Baixa(Pagamento, ValorPago, MultaPaga, CCD, ARL, 0);
        }
        */
        /*
        /// <summary>
        /// N�o usar
        /// </summary>
        /// <param name="Pagamento"></param>
        /// <param name="ValorPago"></param>
        /// <param name="MultaPaga"></param>
        /// <param name="CCD"></param>
        /// <param name="ARL"></param>
        /// <param name="ACO"></param>
        /// <returns></returns>
        [Obsolete("usar int? no ACO",true)]
        public bool Baixa(DateTime Pagamento, decimal ValorPago, decimal MultaPaga, int CCD, int ARL, int ACO)
        {
            int? novoACO = ACO;
            return Baixa(Pagamento, ValorPago, MultaPaga, CCD, ARL, novoACO);
        }
        */


        internal bool _expotaBalanceteAccess
        {
            get { return Condominio.CONDivideSaldos; }
        }

        /// <summary>
        /// Valor nominal do boleto
        /// </summary>
        public decimal ValorPrevisto
        {
            get { return rowPrincipal.BOLValorPrevisto; }
            set { rowPrincipal.BOLValorPrevisto = value; }
        }

        /// <summary>
        /// Ajusta o boleto para quitacao
        /// </summary>
        /// <param name="Pagamento">Data do pagamento - somente para incluir nas descri��es</param>
        /// <param name="ValorPago">Valor total creditado nesta quita��o</param>
        /// <param name="Multapaga">Multa paga nesta quita��o</param>
        /// <returns>Boleto a ser efetivamente quitado - nulo se for o pr�prio</returns>
        /// <remarks>Esta fun��o ajusta o boleto para a quita��o</remarks>
        /// <remarks>1) remove o seguro se perdeu a validade</remarks>
        /// <remarks>2) recalcula o valorprevisto com base nos itens para eliminar eventuais erros</remarks>
        /// <remarks>3) se o pagamento liq for maior que o previsto: incluir o valor a maior e gera o desconto</remarks>
        /// <remarks>4) se o pagamento for menor que o previsto: abate a diferen�a dos juros e cobra no poximo mes</remarks>
        /// <remarks>5) se o erro for maior que os juros ent�o divide o boleto de tal forma que a primeira parte com os juros seja o valor creditado - ajusta os juros</remarks>        
        /// <remarks>Note que a Multapaga ser� incluida nos BODs como multa/desconto na quita��o</remarks>
        private Boleto AjustaBoletoParaQuitacao(DateTime Pagamento, decimal ValorPago, ref decimal Multapaga)
        {
            //Leia com aten��o o remarks
            //passo 1 - remover o seguro vencido
            if (ComSeguro())
                AjustaSeguro(Pagamento);

            //passo 2 - corrigir eventual erro
            decimal Total = 0;
            foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                Total += rowBOD.BODValor;
            if (Total != rowPrincipal.BOLValorPrevisto)
            {
                string Erro = string.Format("Valor incorreto do boleto BOL = {0} Valor = {1:n2} Itens = {2:n2}", BOL, rowPrincipal.BOLValorPrevisto, Total);
                rowPrincipal.BOLValorPrevisto = Total;
                SalvarAlteracoes("Erro");
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", Erro, "Erro em BOLETO");
            }

            //verifica se vai para o 3 4 ou 5
            decimal DifValor = Math.Round(ValorPago - Multapaga - ValorPrevisto, 2, MidpointRounding.AwayFromZero);
            if (DifValor == 0)
                return null; //sem corre��es, � s� quitar.           
            else
            {
                Competencia proxComp = Competencia.Ultima(CON);
                proxComp++;
                if (DifValor > 0) //passo 3 - incluir no boleto e descontar do pr�ximo
                {
                    int CTL = dPLAnocontas.dPLAnocontasSt.CTLPadrao(CON, CTLTipo.CreditosAnteriores);
                    string PLAC = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraC);
                    string PLAD = PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD);
                    string Descritivo1 = string.Format("Pagamento a Maior. Unidade {0}", Unidade);
                    string Descritivo2 = string.Format("Desconto pagamento a Maior em {0:dd/MM/yyyy}", Pagamento);
                    dBoletos.BOletoDetalheRow rowBODO = IncluiBOD(PLAC, Descritivo1, DifValor, false, null, CTL);
                    dBoletos.BOletoDetalheRow rowBODD = Boleto.GerarBOD(CON,
                                                                                       APT,
                                                                                       Proprietario,
                                                                                       PLAD,
                                                                                       true,
                                                                                       proxComp,
                                                                                       proxComp.CalculaVencimento(),
                                                                                       Descritivo2,
                                                                                       -DifValor,
                                                                                       false,
                                                                                       CTL);
                    Boleto.AmarraBODs(rowBODO.BOD, rowBODD.BOD);
                    return null; // o boleto a ser quitado � o pr�prio      
                }
                else if (Multapaga + DifValor >= 0)
                {
                    Multapaga += DifValor;
                    //N�o precisa abater dos BOD porque j� vai tirar do juros a ser inclu�do, � s� cobrar do proximo na conta de juros diretamente
                    dBoletos.BOletoDetalheRow rowBODD = Boleto.GerarBOD(CON,
                                                                                       APT,
                                                                                       Proprietario,
                                                                                       PadraoPLA.PLAPadraoCodigo(PLAPadrao.MultasAtraso),
                                                                                       true,
                                                                                       proxComp,
                                                                                       proxComp.CalculaVencimento(),
                                                                                       string.Format("Diferen�a refer�nte ao vencimento {0:dd/MM/yyyy}", rowPrincipal.BOLVencto),
                                                                                       -DifValor,
                                                                                       false,
                                                                                       dPLAnocontas.dPLAnocontasSt.CTLPadrao(CON, CTLTipo.Caixa));
                    return null;
                }
                else
                {
                    Boleto[] Partes;
                    if (Multapaga == 0)
                        Partes = Dividir("Boleto dividido na quita��o (valor pago a menor)", ValorPago, null, null, true);
                    else
                    {
                        //(Total + Multapaga) � quanto deveria ter sido pago ( 100 % )
                        decimal porc = ValorPago / (Total + Multapaga);
                        Partes = DividirPorc("Boleto dividido na quita��o (valor pago a menor com multa)", porc);
                        Multapaga = (ValorPago - Partes[0].ValorPrevisto);
                    }
                    if (Partes[1].rowPrincipal.BOLValorPrevisto < 20.0M)
                    {
                        Partes[1].BOLStatus = ((Partes[1].BOLStatus == StatusBoleto.Valido) ? StatusBoleto.Acumulado : StatusBoleto.Saldinho);
                        Partes[1].SalvarAlteracoes(string.Format("O boleto vai ser cobrado junto com o pr�ximo Valor inferior a R$ 20,00"));

                    }
                    return Partes[0];
                }
            }

        }

        /// <summary>
        /// Evita que a quita��o do boleto extra baixe automaticamente outros boletos
        /// </summary>
        public bool TravaBaixaAutomaticaExtra = false;

        /// <summary>
        /// Efetua a baixa do boleto com os desdobramentos
        /// </summary>
        /// <param name="TipoBaixa">Descritivo do tipo de baixa para o hist�rico do boleto</param>
        /// <param name="Pagamento"></param>
        /// <param name="ValorPago">Valor total creditado</param>
        /// <param name="MultaPaga">Multa paga</param>
        /// <param name="CCD"></param>
        /// <param name="ARL"></param>
        /// <param name="ACO"></param>
        /// <returns></returns>
        /// <remarks>a diferen�a entre o (valorprevisto + multapaga) e ValorPago ser� tratada como pagamento a menor ou maior</remarks>
        public bool Baixa(string TipoBaixa, DateTime Pagamento, decimal ValorPago, decimal MultaPaga, int? CCD = null, int? ARL = null, int? ACO = null)
        {
            //Legado
            if (ARL.HasValue && (ARL == -1))
                ARL = null;
            if (CCD.HasValue && (CCD == -1))
                CCD = null;

            bool Retorno = false;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto Baixa - 3427", dBoletos.BOLetosTableAdapter, dBoletos.BOletoDetalheTableAdapter);                                
                Boleto BoletoSubstitutoQuitar = null;
                if (TipoCRAIE != BOLTipoCRAI.Extra)
                {
                    
                    BoletoSubstitutoQuitar = AjustaBoletoParaQuitacao(Pagamento, ValorPago, ref MultaPaga);
                    
                }
                if (BoletoSubstitutoQuitar != null)
                {
                    
                    Retorno = BoletoSubstitutoQuitar._Baixa(TipoBaixa, Pagamento, ValorPago, MultaPaga, CCD, ARL, ACO);
                }
                else
                {
                    
                    Retorno = _Baixa(TipoBaixa, Pagamento, ValorPago, MultaPaga, CCD, ARL, ACO);
                    
                }
                if (Retorno && (TipoCRAIE == BOLTipoCRAI.Extra) && !TravaBaixaAutomaticaExtra)
                    QuitacaoAutomaticaBolExtra();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                UltimaException = e;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                Retorno = false;
            }
            return Retorno;
        }

        #endregion



        #region SuperBoletos

        //foi transferido para o boletosProc
        //public dBoletos.SuperBOletoRow rowSBO = null;

        /// <summary>
        /// Carrega os dados do boleto modelo na ram
        /// </summary>
        /// <param name="Modelo"></param>
        private void Super_CarregaDados(Boleto Modelo)
        {
            rowPrincipal = dBoletos.BOLetos.NewBOLetosRow();
            Encontrado = true; //simula um boleto v�lido
            Competencia = Modelo.Competencia;
            for (int i = 0; i < rowPrincipal.ItemArray.Length; i++)
                rowPrincipal[i] = Modelo.rowPrincipal[i];
            rowPrincipal.BOLJustificativa = "SUPER BOLETO";
            rowPrincipal.BOLMensagem = "N�o receber ap�s o vencimento";
            rowPrincipal.BOL = rowSBO.SBO;
            rowPrincipal.BOLValorPrevisto = 0;
            rowPrincipal.BOLVencto = rowSBO.SBOVencto;
            rowPrincipal.BOLStatus = (int)StatusBoleto.Valido;
            rowPrincipal.BOLTipoCRAI = "I";
            rowPrincipal.BOLCompetenciaAno = 1;
            rowPrincipal.BOLCompetenciaMes = 1;
            if (Apartamento == null)
                throw new Exception("Dados do Apartamento n�o encontrados");
            if (!PegadadosCondominio_APT(rowPrincipal.BOL_APT))
                throw new Exception("Dados do Conom�nio n�o encontrados");/*
            Conta = new ContaCorrente(ContaCorrente.TipoConta.CC, 
                                      rowDadosCON.CON_BCO, 
                                      int.Parse(rowDadosCON.CONAgencia), 
                                      int.Parse(rowDadosCON.CONDigitoAgencia), 
                                      int.Parse(rowDadosCON.CONConta), 
                                      rowDadosCON.CONDigitoConta, 
                                      Condominio.Conta(null).ComRegistro,
                                      rowDadosCON.IsCONCodigoCNRNull() ? (int?)null : int.Parse(rowDadosCON.CONCodigoCNR));   */
            dBoletos.BOLetos.AddBOLetosRow(rowPrincipal);
            rowPrincipal.AcceptChanges();
            boletoCalculado = null;
            IniciaCampos();
        }






        /// <summary>
        /// Carrega os dados do boleto entrando na ram
        /// </summary>
        /// <param name="Entrando"></param>
        private void Super_CarregaDadosBOD(Boleto Entrando)
        {

            boletoCalculado = null;
            dBoletos.BOletoDetalheRow novoBOD;
            novoBOD = dBoletos.BOletoDetalhe.NewBOletoDetalheRow();
            novoBOD.BOD_APT = 0;
            novoBOD.BOD_BOL = rowPrincipal.BOL;
            novoBOD.BOD_PLA = "0";
            novoBOD.BODComCondominio = true;
            novoBOD.BODCompetencia = 1;
            novoBOD.BODData = DateTime.Today;
            novoBOD.BODPrevisto = true;
            novoBOD.BODMensagem = string.Format("BOLETO {0} - {1}{2}",
                                    Entrando.rowPrincipal.BOL,
                                    (Entrando.Apartamento.BLOCodigo != "SB") ? Entrando.Apartamento.BLOCodigo + " " : string.Empty,
                                    Entrando.Apartamento.APTNumero);
            novoBOD.BODProprietario = true;
            Entrando.Valorpara(rowPrincipal.BOLVencto);
            novoBOD.BODValor = Entrando.valorfinal;
            //novoBOD.BODValor = Entrando.rowPrincipal.BOLValorPrevisto;
            novoBOD.BOD_CON = rowPrincipal.BOL_CON;
            novoBOD.BODPrevisto = true;
            dBoletos.BOletoDetalhe.AddBOletoDetalheRow(novoBOD);
            rowPrincipal.BOLValorPrevisto += Entrando.valorfinal;
        }

        /// <summary>
        /// Inclui um boleto no super
        /// </summary>
        /// <param name="Entrando"></param>
        public void addBolentonoSuper(Boleto Entrando)
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 3154", dBoletos.SuperBOletoTableAdapter, dBoletos.SuperBoletoDetalheTableAdapter);
                Entrando.Valorpara(rowPrincipal.BOLVencto);
                if (rowSBO == null)
                    return;
                //rowPrincipal.BOLValorPrevisto += Entrando.rowPrincipal.BOLValorPrevisto;
                dBoletos.SuperBoletoDetalhe.AddSuperBoletoDetalheRow(rowSBO, Entrando.rowPrincipal, Entrando.valorfinal);
                dBoletos.SuperBoletoDetalheTableAdapter.Update(dBoletos.SuperBoletoDetalhe);
                dBoletos.SuperBoletoDetalhe.AcceptChanges();
                rowSBO.SBOValorPrevisto += Entrando.valorfinal;
                dBoletos.SuperBOletoTableAdapter.Update(rowSBO);
                rowSBO.AcceptChanges();
                Super_CarregaDadosBOD(Entrando);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw (e);
            }
        }

        /// <summary>
        /// Carrega um super boleto na ram
        /// </summary>
        /// <param name="SBO"></param>
        /// <returns></returns>
        public bool Super_CarregaSeSuper(int SBO)
        {
            //esta fun��o deve ser destivada passando a usar o objeto SuperBoleto no lugar
            dBoletos.SuperBoletoDetalhe.Clear();
            if (dBoletos.SuperBOletoTableAdapter.FillBySBO(dBoletos.SuperBOleto, SBO) != 1)
                return false;
            else
            {
                rowSBO = dBoletos.SuperBOleto[0];
                dBoletos.SuperBoletoDetalheTableAdapter.FillBySBO(dBoletos.SuperBoletoDetalhe, SBO);
                bool primeiro = true;
                //bool Valido = rowSBO.SBOVencto >= DateTime.Today;
                bool Valido = true;
                foreach (dBoletos.SuperBoletoDetalheRow rowSBD in dBoletos.SuperBoletoDetalhe)
                {
                    Boleto Entrando = new Boleto(rowSBD.SBD_BOL);
                    if ((Entrando.rowPrincipal.BOLCancelado) || !Entrando.rowPrincipal.IsBOLPagamentoNull())
                        Valido = false;
                    if (!Entrando.Encontrado)
                        return false;
                    if (primeiro)
                        Super_CarregaDados(Entrando);
                    Super_CarregaDadosBOD(Entrando);
                    primeiro = false;
                }
                if (!Valido)
                {
                    rowPrincipal.BOLCancelado = true;
                    rowPrincipal.BOLJustificativa += string.Format("\r\nBoleto sem validade");
                    rowPrincipal.BOLStatus = (int)StatusBoleto.Cancelado;
                }
            }
            return true;
        }

        #endregion

        #region Construtores        

        /// <summary>
        /// Cria um super boleto (registra no banco de dados)
        /// </summary>
        /// <param name="Modelo"></param>
        /// <param name="Vencimento"></param>
        public Boleto(Boleto Modelo, DateTime Vencimento)
        {
            rowSBO = dBoletos.SuperBOleto.NewSuperBOletoRow();
            rowSBO.SBOValorPrevisto = 0;
            //rowSBO.SBOVencto = Modelo.rowPrincipal.BOLVencto;
            rowSBO.SBOVencto = Vencimento;
            Condominio = Modelo.Condominio;
            Apartamento = Modelo.Apartamento;
            Proprietario = Modelo.Proprietario;
            rowSBO.SBOStatusRegistro = (int)(DebitoAutomaticoCadastrado() ? StatusRegistroBoleto.BoletoRegistrandoDA : StatusRegistroBoleto.BoletoRegistrando);
            rowSBO.SBODataI = DateTime.Now;
            dBoletos.SuperBOleto.AddSuperBOletoRow(rowSBO);
            dBoletos.SuperBOletoTableAdapter.Update(rowSBO);
            rowSBO.AcceptChanges();
            Super_CarregaDados(Modelo);
            addBolentonoSuper(Modelo);
        }

        /// <summary>
        /// Cria um objeto boleto de teste na memoria;
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="APT"></param>
        /// <param name="numeroBOL"></param>
        /// <param name="Valor"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CarteriraRegistrada"></param>  
        /// <param name="TipoCRAI"></param>
        /// <param name="_ForcarBalancete">For�a um balancete no impresso</param>
        public Boleto(int? CON, int? APT, int numeroBOL, decimal Valor, DateTime Vencimento, bool CarteriraRegistrada = false, string TipoCRAI = null, bool _ForcarBalancete = false)
        {
            ForcarBalancete = _ForcarBalancete;
            carteiraRegistrada = CarteriraRegistrada;
            rowPrincipal = dBoletos.BOLetos.NewBOLetosRow();
            Encontrado = true; //simula um boleto v�lido
            Competencia = new Competencia(DateTime.Today.Month, DateTime.Today.Year);
            if (CON.HasValue)
            {
                PegadadosCondominio1(CON.Value);
                APT = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT TOP 1 APARTAMENTOS.APT, BLOCOS.BLO_CON FROM APARTAMENTOS INNER JOIN BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO WHERE (BLOCOS.BLO_CON = @P1)", CON.Value);
            }
            else
                if (APT.HasValue)
            {
                if (!PegadadosCondominio_APT(APT.Value))
                    throw new Exception("Dados do Conom�nio n�o encontrados");
            }
            else
                throw new Exception("Dados incorretos. CON = null e APT = null");
            rowPrincipal.BOL_APT = APT.Value;
            rowPrincipal.BOLProprietario = true;
            rowPrincipal.BOL_CON = rowDadosCON.CON;
            if (Apartamento == null)
                throw new Exception("Dados do Apartamento n�o encontrados");


            rowPrincipal.BOLI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
            rowPrincipal.BOLDATAI = DateTime.Now;
            rowPrincipal.BOLValorPrevisto = Valor;
            rowPrincipal.BOLMensagemImp = "TESTE";
            rowPrincipal.BOLCompetenciaAno = (Int16)Competencia.Ano;
            rowPrincipal.BOLCompetenciaMes = (Int16)Competencia.Mes;
            rowPrincipal.BOL_FPG = (int)Apartamento.FormaPagamentoEfetivo(null, true, false);
            /*
            Conta = new ContaCorrente(ContaCorrente.TipoConta.CC,
                                      rowDadosCON.CON_BCO,
                                      int.Parse(rowDadosCON.CONAgencia),
                                      rowDadosCON.IsCONDigitoAgenciaNull() ? (int?)null : int.Parse(rowDadosCON.CONDigitoAgencia),
                                      int.Parse(rowDadosCON.CONConta),
                                      rowDadosCON.CONDigitoConta,
                                      rowDadosCON.IsCONCodigoCNRNull() ? (int?)null : int.Parse(rowDadosCON.CONCodigoCNR),"",
                                      rowDadosCON.CON_BCO == 104 ? "" : "");            */
            rowPrincipal.BOLEmissao = DateTime.Now;
            rowPrincipal.BOLEndereco = GeraEndereco();
            rowPrincipal.BOLMensagem = GeraInstrucoes();
            rowPrincipal.BOLMulta = rowDadosCON.CONValorMulta;
            rowPrincipal.BOLNome = "Jo�o da Silva";
            rowPrincipal.BOLProprietario = true;
            if (TipoCRAI != null)
                rowPrincipal.BOLTipoCRAI = TipoCRAI;
            else
                rowPrincipal.BOLTipoCRAI = "T";
            rowPrincipal.BOLTipoMulta = rowDadosCON.CONTipoMulta;
            rowPrincipal.BOLVencto = Vencimento;
            //rowPrincipal.BOLVencto = new DateTime(2006, 8, 23);
            rowPrincipal.BOLExportar = false;
            rowPrincipal.BOLCancelado = false;
            rowPrincipal.BOL = numeroBOL;
            rowPrincipal.BOLStatus = 0;
            rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.BoletoSemRegistro;
            dBoletos.BOLetos.AddBOLetosRow(rowPrincipal);
            IniciaCampos();
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <param name="APT"></param>
        /// <param name="MensagemImportante"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="Status"></param>
        /// <param name="IncluirCorreio"></param>
        /// <param name="IncluirBanco"></param>
        /// <param name="BODs"></param>
        [Obsolete("usar ABS_Apartamento e nao APT")]
        public Boleto(bool Proprietario, int APT, string MensagemImportante, Competencia Comp, DateTime Vencimento, String CRAI, StatusBoleto Status, bool IncluirCorreio, bool IncluirBanco, params int[] BODs)
        {
            ABS_Apartamento _Apartamento = ABS_Apartamento.GetApartamento(APT);
            CriaNovoBoleto(Proprietario, _Apartamento, MensagemImportante, Comp, Vencimento, CRAI, null, Status, IncluirCorreio, IncluirBanco, false, BODs);            
        }
        */

        /// <summary>
        /// Cria um objeto boleto criando os registros no banco de dados;
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <param name="_Apartamento"></param>
        /// <param name="MensagemImportante"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="Status"></param>
        /// <param name="IncluirCorreio"></param>
        /// <param name="IncluirBanco"></param>
        /// <param name="BODs"></param>
        public Boleto(bool Proprietario, ABS_Apartamento _Apartamento, string MensagemImportante, Competencia Comp, DateTime Vencimento, String CRAI, StatusBoleto Status, bool IncluirCorreio, bool IncluirBanco, params int[] BODs)
        {
            CriaNovoBoleto(Proprietario, false, _Apartamento, MensagemImportante, Comp, Vencimento, CRAI, null, Status, IncluirCorreio, IncluirBanco, false, BODs);
        }

        /// <summary>
        /// Cria um objeto boleto criando os registros no banco de dados;
        /// </summary>
        /// <param name="_dBoletos1"></param>
        /// <param name="ComDesconto"></param>
        /// <param name="Proprietario"></param>
        /// <param name="_Apartamento"></param>
        /// <param name="MensagemImportante"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="Status"></param>
        /// <param name="IncluirCorreio"></param>
        /// <param name="IncluirBanco"></param>
        /// <param name="BODs"></param>
        public Boleto(dBoletos _dBoletos1, bool ComDesconto, bool Proprietario, ABS_Apartamento _Apartamento, string MensagemImportante, Competencia Comp, DateTime Vencimento, String CRAI, StatusBoleto Status, bool IncluirCorreio, bool IncluirBanco, params int[] BODs)
        {
            dBoletos = _dBoletos1;
            CriaNovoBoleto(Proprietario, ComDesconto, _Apartamento, MensagemImportante, Comp, Vencimento, CRAI, null, Status, IncluirCorreio, IncluirBanco, false, BODs);
        }

        /// <summary>
        /// Cria um objeto boleto criando os registros no banco de dados;
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="MensagemImportante"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="Status"></param>
        /// <param name="IncluirCorreio"></param>
        /// <param name="IncluirBanco"></param>
        /// <param name="Fornecedor"></param>
        /// <param name="BODs"></param>
        public Boleto(int CON, string MensagemImportante, Competencia Comp, DateTime Vencimento, String CRAI, StatusBoleto Status, bool IncluirCorreio, bool IncluirBanco, ABS_Fornecedor Fornecedor, params int[] BODs)
        {
            CriaNovoBoleto(CON, true, false, null, MensagemImportante, Comp, Vencimento, CRAI, null, Status, IncluirCorreio, IncluirBanco, string.Empty, false, Fornecedor, BODs);
        }

        /// <summary>
        /// Cria um objeto boleto criando os registros no banco de dados;
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="MensagemImportante"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="Status"></param>
        /// <param name="IncluirCorreio"></param>
        /// <param name="IncluirBanco"></param>
        /// <param name="Destinatario"></param>
        /// <param name="BODs"></param>
        public Boleto(int CON, string MensagemImportante, Competencia Comp, DateTime Vencimento, String CRAI, StatusBoleto Status, bool IncluirCorreio, bool IncluirBanco, string Destinatario, params int[] BODs)
        {
            CriaNovoBoleto(CON, true, false, null, MensagemImportante, Comp, Vencimento, CRAI, null, Status, IncluirCorreio, IncluirBanco, Destinatario, false, null, BODs);
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="APT"></param>
        /// <param name="Proprietario"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="Status"></param>
        /// <param name="BODs"></param>
        [Obsolete("usar objeto ABS_Apartamento no lugar de int? APT")]
        public Boleto(int CON, int? APT, bool Proprietario, Competencia Comp, DateTime Vencimento, String CRAI, StatusBoleto Status, params int[] BODs)
        {
            ABS_Apartamento _Apartamento = null;
            if (APT.HasValue)
                _Apartamento = ABS_Apartamento.GetApartamento(APT.Value);
            CriaNovoBoleto(CON, Proprietario, false, _Apartamento, string.Empty, Comp, Vencimento, CRAI, null, Status, false, false, null, false, null, BODs);
        }


        /// <summary>
        /// Cria um objeto boleto criando os registros no banco de dados;
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="_Apartamento"></param>
        /// <param name="Proprietario"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="Status"></param>
        /// <param name="BODs"></param>
        public Boleto(int CON, ABS_Apartamento _Apartamento, bool Proprietario, Competencia Comp, DateTime Vencimento, String CRAI, StatusBoleto Status, params int[] BODs)
        {
            CriaNovoBoleto(CON, Proprietario, false, _Apartamento, string.Empty, Comp, Vencimento, CRAI, null, Status, false, false, null, false, null, BODs);
        }

        /*
        private void CriaNovoBoleto(bool Proprietario, int APT, string MensagemImportante, Competencia Comp, DateTime Vencimento, String CRAI, int? BOL, StatusBoleto Status, bool IncluirCorreio, bool IncluirBanco, bool ProibirRegistro, params int[] BODs)
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("new APT", true);
            Apartamento = ABS_Apartamento.GetApartamento(APT);
            CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
            CriaNovoBoleto(Proprietario, Apartamento, MensagemImportante, Comp, Vencimento, CRAI, BOL, Status, IncluirCorreio, IncluirBanco, ProibirRegistro, BODs);            
        }
        */

        private void CriaNovoBoleto(bool Proprietario, bool ComDesconto, ABS_Apartamento _Apartamento, string MensagemImportante, Competencia Comp, DateTime Vencimento, String CRAI, int? BOL, StatusBoleto Status, bool IncluirCorreio, bool IncluirBanco, bool ProibirRegistro, params int[] BODs)
        {
            CriaNovoBoleto(_Apartamento.CON, Proprietario, ComDesconto, _Apartamento, MensagemImportante, Comp, Vencimento, CRAI, BOL, Status, IncluirCorreio, IncluirBanco, null, ProibirRegistro, null, BODs);
        }




        /*
        private bool ComRegistro
        {
            get
            {
                PegadadosCondominio1(rowPrincipal.BOL_CON);
                //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                switch (rowDadosCON.CON_BCO)
                {
                    case 237:
                        if (!rowDadosCON.IsCONCodigoCNRNull())
                            if (rowDadosCON.CONCodigoCNR != "" && rowDadosCON.CONCodigoCNR.Trim() != "16")
                                return rowDadosCON.CONBoletoComRegistro;
                        return false;
                    default:
                        return rowDadosCON.CONBoletoComRegistro;
                }
                //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
            }
        }*/

        internal void IncluirSaldoAnterior(Boleto BolSaldo)
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boleto IncluirSaldoAnterior - 4216");
                BolSaldo.Valorpara(rowPrincipal.BOLVencto);
                IncluiBOD(PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho),
                          string.Format("Saldo vencimento {0:dd/MM/yyyy}",
                          BolSaldo.rowPrincipal.BOLVencto),
                          BolSaldo.valorfinal,
                          false, BolSaldo.BOL);
                GravaJustificativa(string.Format("Inclus�o do boleto {0}", BolSaldo.BOL));
                BolSaldo.rowPrincipal.BOLCancelado = true;
                BolSaldo.GravaJustificativa(string.Format("inclu�do no boleto {0}", BOL), string.Empty);
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
            }
        }

        private void CriaNovoBoleto(int CON,
                                    bool? Proprietario,
                                    bool ComDesconto,
                                    ABS_Apartamento _Apartamento,
                                    string MensagemImportante,
                                    Competencia Comp,
                                    DateTime Vencimento,
                                    String CRAI,
                                    int? BOL,
                                    StatusBoleto Status,
                                    bool IncluirCorreio,
                                    bool IncluirBanco,
                                    string Nome,
                                    bool ProibirRegistro,
                                    ABS_Fornecedor Fornecedor,
                                    params int[] BODs)
        {
            if (_Apartamento != null)
                Apartamento = _Apartamento;
            if (_Fornecedor != null)
                Fornecedor = _Fornecedor;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 3347", dBoletos.BOLetosTableAdapter,
                                                                               dBoletos.BOletoDetalheTableAdapter);
                if ((Status == StatusBoleto.Saldinho) || (Status == StatusBoleto.Acumulado))
                    ProibirRegistro = true;
                Competencia = Comp.CloneCompet();
                rowPrincipal = dBoletos.BOLetos.NewBOLetosRow();
                rowPrincipal.BOL_CON = CON;
                rowPrincipal.BOLStatus = (int)Status;
                rowPrincipal.BOLCancelado = !Boleto.StatusBoletoExportavel.Contains(Status);
                /*
                if (CRAI != "S")
                {
                    if ((rowPrincipal.BOLCancelado) || (BOLStatus == StatusBoleto.EmProducao))
                        rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.ComregistroNaoRegistrado;
                    else
                    {
                        if ((Vencimento >= DateTime.Today) && (!ProibirRegistro))
                            rowPrincipal.BOLStatusRegistro = (int)(DebitoAutomaticoCadastrado() ? StatusRegistroBoleto.BoletoRegistrandoDA : StatusRegistroBoleto.BoletoRegistrando;
                        else
                            rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.ComregistroNaoRegistrado;
                    }
                }
                else
                    rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.BoletoSemRegistro;
                */
                if (Proprietario.HasValue)
                    rowPrincipal.BOLProprietario = Proprietario.Value;
                if (Apartamento != null)
                    rowPrincipal.BOL_APT = Apartamento.APT;
                if (Fornecedor != null)
                    rowPrincipal.BOL_FRN = Fornecedor.FRN;
                if (!PegadadosCondominio1(CON))
                    throw new Exception("Dados do Conom�nio n�o encontrados");
                rowPrincipal.BOLI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                rowPrincipal.BOLDATAI = DateTime.Now;
                rowPrincipal.BOLValorPrevisto = 0; //ser� ajustada depois, na inclusao dos BODs
                rowPrincipal.BOLValorOriginal = 0;
                if ((MensagemImportante != null) && (MensagemImportante != string.Empty))
                    rowPrincipal.BOLMensagemImp = MensagemImportante;
                rowPrincipal.BOLProtestado = false;
                rowPrincipal.BOLCompetenciaAno = (Int16)Comp.Ano;
                rowPrincipal.BOLCompetenciaMes = (Int16)Comp.Mes;
                rowPrincipal.BOL_FPG = (int)((Apartamento == null) ? FormaPagamento.EntreigaPessoal : Apartamento.FormaPagamentoEfetivo(null, Proprietario.Value, false));

                if (Vencimento < DateTime.Today)
                    rowPrincipal.BOLEmissao = Vencimento;
                else
                    rowPrincipal.BOLEmissao = DateTime.Now;
                rowPrincipal.BOLEndereco = GeraEndereco();
                rowPrincipal.BOLMensagem = GeraInstrucoes();
                rowPrincipal.BOLMulta = rowDadosCON.CONValorMulta;
                if ((Nome != null) && (Nome != string.Empty))
                    rowPrincipal.BOLNome = Nome;
                else if (Apartamento != null)
                    rowPrincipal.BOLNome = Apartamento.PESNome(Proprietario.Value);
                else if (Fornecedor != null)
                    rowPrincipal.BOLNome = Fornecedor.FRNNome;
                rowPrincipal.BOLTipoCRAI = CRAI;
                rowPrincipal.BOLTipoMulta = rowDadosCON.CONTipoMulta;
                rowPrincipal.BOLVencto = Vencimento;
                if (CRAI != "S")
                    if ((rowPrincipal.BOLCancelado) || (BOLStatus == StatusBoleto.EmProducao))
                        rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.ComregistroNaoRegistrado;
                    else
                        if ((Vencimento >= DateTime.Today) && (!ProibirRegistro))
                        rowPrincipal.BOLStatusRegistro = (int)(DebitoAutomaticoCadastrado() ? StatusRegistroBoleto.BoletoRegistrandoDA : StatusRegistroBoleto.BoletoRegistrando);
                    else
                        rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.ComregistroNaoRegistrado;
                else
                {
                    rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.BoletoSemRegistro;
                    rowPrincipal.BOLExportar = false;
                    rowPrincipal.BOLCancelado = false;
                }
                dBoletos.BOLetos.AddBOLetosRow(rowPrincipal);
                Encontrado = true;
                if ((BOL.HasValue) && (BOL.Value != 0))
                {
                    dBoletos.BOLetos.Columns["BOL"].ReadOnly = false;
                    rowPrincipal.BOL = BOL.Value;
                    VirMSSQL.TableAdapter.STTableAdapter.Insert(rowPrincipal, true, false);
                }
                else
                    dBoletos.BOLetosTableAdapter.Update(rowPrincipal);

                rowPrincipal.AcceptChanges();
                bool RecalcularBOLValor = false;
                bool ClearBeforeFill = true;
                try
                {
                    ClearBeforeFill = dBoletos.BOletoDetalheTableAdapter.ClearBeforeFill;
                    dBoletos.BOletoDetalheTableAdapter.ClearBeforeFill = false;
                    for (int i = 0; i < BODs.Length; i++)
                    {
                        if (BODs[i] <= 0)
                            continue;

                        dBoletos.BOletoDetalheRow novoBOD = dBoletos.BOletoDetalhe.FindByBOD(BODs[i]);
                        if (novoBOD == null)
                        {
                            dBoletos.BOletoDetalheTableAdapter.FillByBOD(dBoletos.BOletoDetalhe, BODs[i]);
                            novoBOD = dBoletos.BOletoDetalhe.FindByBOD(BODs[i]);
                        };
                        novoBOD.BOD_BOL = rowPrincipal.BOL;
                        RecalcularBOLValor = true;
                    };
                    dBoletos.BOletoDetalheTableAdapter.Update(dBoletos.BOletoDetalhe);
                    dBoletos.BOletoDetalhe.AcceptChanges();
                }
                finally
                {
                    dBoletos.BOletoDetalheTableAdapter.ClearBeforeFill = ClearBeforeFill;
                }

                bool Zerado = false;
                if (RecalcularBOLValor)
                {
                    object retorno = dBoletos.BOLetosTableAdapter.GetTotal(rowPrincipal.BOL);
                    decimal Total = (decimal)(retorno ?? 0);
                    rowPrincipal.BOLValorPrevisto = rowPrincipal.BOLValorOriginal = Total;
                    if (Total == 0)
                    {
                        rowPrincipal.BOLPagamento = rowPrincipal.BOLVencto;
                        rowPrincipal.BOLValorPago = 0;
                        Encontrado = true; //Para evitar que o BOLStaus venha com erro
                        if (rowPrincipal.BOLStatusRegistro.EstaNoGrupo((int)StatusRegistroBoleto.BoletoRegistrando, (int)StatusRegistroBoleto.BoletoRegistrandoDA))
                            rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.BoletoSemRegistro;
                        dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                        rowPrincipal.AcceptChanges();
                        Zerado = true;
                        RecalcularBOLValor = false;
                    }
                }
                if (!Zerado)
                {
                    if ((IncluirCorreio) && (FormaPagamento.EstaNoGrupo(FormaPagamento.Correio, FormaPagamento.CorreioEemail)))
                        GeraBOD("109000", "Correio", ValorCorreio);

                    if (IncluirBanco)
                        GeraBOD("125000", "Despesas banc�rias", ValorBanco);
                }
                if (rowPrincipal.RowState == DataRowState.Modified)
                {
                    dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                    rowPrincipal.AcceptChanges();
                }

                decimal TotalBOLValorPrevisto = 0;
                decimal TotalOriginal = 0;

                foreach (dBoletos.BOletoDetalheRow BOletoDetalheRow in rowPrincipal.GetBOletoDetalheRows())
                {

                    if (BOletoDetalheRow.IsBODPrevistoNull())
                    {
                        BOletoDetalheRow.BODPrevisto = true;
                        dBoletos.BOletoDetalheTableAdapter.Update(BOletoDetalheRow);
                        BOletoDetalheRow.AcceptChanges();
                    }
                    TotalBOLValorPrevisto += BOletoDetalheRow.BODValor;
                    if (BOletoDetalheRow.BODPrevisto)
                        TotalOriginal += BOletoDetalheRow.BODValor;
                    if (!BOletoDetalheRow.IsBOD_RADNull())
                        if (!RADjagerados.Contains(BOletoDetalheRow.BOD_RAD))
                        {
                            RADjagerados.Add(BOletoDetalheRow.BOD_RAD);
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update RAteioDetalhes set RADGerado = 1 where RAD = @P1", BOletoDetalheRow.BOD_RAD);
                        }
                    dllImpostos.SolicitaRetencao[] SolicitaRetencaos;
                    if (!BOletoDetalheRow.IsBODRetencaoNull())
                    {
                        INSSSindico CDRINSS = (INSSSindico)BOletoDetalheRow.BODRetencao;
                        if (CDRINSS != INSSSindico.NaoRecolhe)
                        {
                            SolicitaRetencaos = new dllImpostos.SolicitaRetencao[CDRINSS == INSSSindico.ParteCond ? 1 : 2];
                            SolicitaRetencaos[0] = new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, -BOletoDetalheRow.BODValor * 0.2M, SimNao.Nao);
                            decimal TotImposto = SolicitaRetencaos[0].Valor;
                            if (CDRINSS == INSSSindico.ParteCondSindico)
                            {
                                SolicitaRetencaos[1] = new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, -BOletoDetalheRow.BODValor * 0.11M, SimNao.Nao);
                                TotImposto += SolicitaRetencaos[1].Valor;
                            }
                            int PES = Apartamento.PES_INSS;
                            ContaPagarProc.dNOtAs.dNOtAsSt.IncluiNotaSimples_Efetivo(0, rowPrincipal.BOLVencto, rowPrincipal.BOLVencto, 
                                                                                     new Competencia(rowPrincipal.BOLVencto), NOATipo.Avulsa, string.Empty, CON, PAGTipo.cheque,"",
                                                                                     true, "220016", 24, "INSS - Corpo diretivo", TotImposto, 0, true, ContaPagarProc.dNOtAs.TipoChave.PES, 
                                                                                     PES,null,null,null,null,NOAStatus.Cadastrada, SolicitaRetencaos);
                        }
                    }
                }

                if ((rowPrincipal.BOLValorOriginal != TotalOriginal) || (rowPrincipal.BOLValorPrevisto != TotalBOLValorPrevisto))
                {
                    rowPrincipal.BOLValorOriginal = TotalOriginal;
                    rowPrincipal.BOLValorPrevisto = TotalBOLValorPrevisto;
                }
                if (rowPrincipal.RowState == DataRowState.Modified)
                {
                    dBoletos.BOLetosTableAdapter.Update(rowPrincipal);
                    rowPrincipal.AcceptChanges();
                }
                if (ComDesconto)
                    IncluirDescontos();

                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                GuardaBODsOriginal();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                Encontrado = false;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro ao criar boleto: " + UltimoErro, e);

            }
        }


        /// <summary>
        /// Construtor: Cria um objeto boleto criando os registros no banco de dados;
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <param name="_Apartamento"></param>
        /// <param name="MensagemImportante"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="BOL"></param>
        /// <param name="Status"></param>
        /// <param name="ProibirRegistro"></param>
        /// <param name="BODs"></param>
        public Boleto(bool Proprietario, ABS_Apartamento _Apartamento, string MensagemImportante, Competencia Comp, DateTime Vencimento, String CRAI, int BOL, StatusBoleto Status, bool ProibirRegistro, params int[] BODs)
        {
            CriaNovoBoleto(Proprietario, false, _Apartamento, MensagemImportante, Comp, Vencimento, CRAI, BOL, Status, false, false, ProibirRegistro, BODs);
        }

        /// <summary>
        /// Construtor: Cria um objeto boleto criando os registros no banco de dados;
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="MensagemImportante"></param>
        /// <param name="Comp"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CRAI"></param>
        /// <param name="Status"></param>
        /// <param name="Nome"></param>
        /// <param name="BODs"></param>
        public Boleto(int CON, string MensagemImportante, Competencia Comp, DateTime Vencimento, BOLTipoCRAI CRAI, StatusBoleto Status, string Nome, params int[] BODs)
        {
            CriaNovoBoleto(CON, null, false, null, MensagemImportante, Comp, Vencimento, virEnumBOLTipoCRAI.Valor(CRAI), null, Status, false, false, Nome, false, null, BODs);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rowPrincipal">Linha do boleto</param>        
        public Boleto(dBoletos.BOLetosRow rowPrincipal)
            : base(rowPrincipal)
        {
            
        }

        /*
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rowPrincipal">Linha do boleto</param>
        /// <param name="_EMP">Empresa</param>
        public Boleto(dBoletos.BOLetosRow rowPrincipal,int _EMP)
        {
            EMP = _EMP;
            Externo = true;
            Encontrado = true;
            this.rowPrincipal = rowPrincipal;
            GuardaBODsOriginal();
            IniciaCampos();
        }
        */


        /// <summary>
        /// Contrutor
        /// </summary>
        /// <param name="_BOL">N�mero do boleto</param>
        /// <remarks>Registra na propriedade "Encontrado"</remarks>
        public Boleto(int _BOL)
        {
            VirMSSQL.TableAdapter.EmbarcaEmTrans(dBoletos.BOLetosTableAdapter, dBoletos.BOletoDetalheTableAdapter, dBoletos.BoletoDEscontoTableAdapter);
            CarregaDadosDoBoleto(_BOL);
        }


        #endregion


        #region Dividir

        /*
        /// <summary>
        /// divide o boleto em 2
        /// </summary>
        /// <param name="ValorBol1"></param>
        /// <returns></returns>
        public Boleto[] Dividir(string Justificativa, decimal ValorBol1)
        {
            return Dividir(Justificativa, ValorBol1, rowPrincipal.BOLProprietario, rowPrincipal.BOLProprietario);
        }
          
        public Boleto[] DividirPorc(string Justificativa, decimal porc)
        {
            return DividirPorc(Justificativa, porc, rowPrincipal.BOLProprietario, rowPrincipal.BOLProprietario);
        } 
         
        */

        private bool UnidadeAcumula(decimal Saldo, int indice, dBoletos.BOletoDetalheRow[] rowBODs, List<int> Grupo)
        {
            if (indice >= rowBODs.Length)
                return false;
            if (Saldo == rowBODs[indice].BODValor)
            {
                Grupo.Add(indice);
                return true;
            }
            if (UnidadeAcumula(Saldo, indice + 1, rowBODs, Grupo))
                return true;
            else
            {
                if (Saldo < rowBODs[indice].BODValor)
                    return false;
                if (UnidadeAcumula(Saldo - rowBODs[indice].BODValor, indice + 1, rowBODs, Grupo))
                {
                    Grupo.Add(indice);
                    return true;
                }
                else
                    return false;
            }
        }

        private SortedList<int, PortaItem> GrupoPorValor(decimal ValorBol1, dBoletos.BOletoDetalheRow[] rowBODs)
        {
            SortedList<int, PortaItem> Retorno = new SortedList<int, PortaItem>();
            List<int> Grupo = new List<int>();
            if (UnidadeAcumula(ValorBol1, 0, rowBODs, Grupo))
                foreach (int i in Grupo)
                    Retorno.Add(rowBODs[i].BOD, new PortaItem(rowBODs[i].BODMensagem, rowBODs[i].BOD_PLA, rowBODs[i].BODValor, rowBODs[i].BOD));
            return Retorno;
        }

        /// <summary>
        /// divide o boleto em 2
        /// </summary>
        /// <param name="Justificativa">Justificativa</param>
        /// <param name="ValorBol1">Valor da primeira metade</param>
        /// <param name="Proprietario1">Destinat�rio do primeiro</param>
        /// <param name="Proprietario2">Destinat�rio do segundo</param>
        /// <param name="PodeRemoverItem">Se um conjunto de itens baterem o valor ele sapara pelos itens</param>
        /// <returns></returns>
        public Boleto[] Dividir(string Justificativa, decimal ValorBol1, bool? Proprietario1 = null, bool? Proprietario2 = null, bool PodeRemoverItem = false)
        {
            if (ValorBol1 <= 0)
                throw new ArgumentException("Valor negativo ou zero na divis�o do boleto", "ValorBol1");
            if (ValorBol1 >= ValorPrevisto)
                throw new ArgumentException("Valor da parcela maior ou igual ao total do boleto", "ValorBol1");
            switch (BOLStatus)
            {
                case StatusBoleto.Erro:
                case StatusBoleto.Antigo:
                case StatusBoleto.Cancelado:
                case StatusBoleto.OriginalDividido:
                case StatusBoleto.Extra:
                default:
                    throw new Exception(string.Format("Boleto tipo '{0}' n�o pode ser dividido", BOLStatus));
                case StatusBoleto.Valido:
                case StatusBoleto.EmProducao:
                case StatusBoleto.Parcial:
                case StatusBoleto.ParcialAcordo:
                case StatusBoleto.OriginalAcordo:
                case StatusBoleto.Saldinho:
                case StatusBoleto.Acumulado:
                    decimal porc = ValorBol1 / rowPrincipal.BOLValorPrevisto;
                    //SortedList<int, PortaItem> Itens = new SortedList<int, PortaItem>();
                    SortedList<int, PortaItem> Itens = PodeRemoverItem ? GrupoPorValor(ValorBol1, rowPrincipal.GetBOletoDetalheRows()) : new SortedList<int, PortaItem>();
                    if (Itens.Count == 0)
                    {
                        decimal Saldo = ValorBol1;
                        dBoletos.BOletoDetalheRow[] rowBODs = rowPrincipal.GetBOletoDetalheRows();

                        //caso tenha saldos eles devem ficar todos do mesmo lado e sem quebrar
                        decimal TotalSaldos = 0;
                        foreach (dBoletos.BOletoDetalheRow rowBOD in rowBODs)
                            if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho))
                                TotalSaldos += rowBOD.BODValor;
                        if (TotalSaldos < ValorBol1) //cabe tudo na primeira parte
                            foreach (dBoletos.BOletoDetalheRow rowBOD in rowBODs)
                                if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho))
                                {
                                    Itens.Add(rowBOD.BOD, new PortaItem(rowBOD.BODMensagem, rowBOD.BOD_PLA, rowBOD.BODValor, rowBOD.BOD));
                                    Saldo -= rowBOD.BODValor;
                                }


                        dBoletos.BOletoDetalheRow maiorrow = null;
                        foreach (dBoletos.BOletoDetalheRow rowBOD in rowBODs)
                        {
                            if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho))
                                continue;
                            decimal ValorIt = Math.Round(rowBOD.BODValor * porc, 2);
                            Saldo -= ValorIt;
                            Itens.Add(rowBOD.BOD, new PortaItem(rowBOD.BODMensagem, rowBOD.BOD_PLA, ValorIt, rowBOD.BOD));
                            if ((maiorrow == null) || (maiorrow.BODValor < rowBOD.BODValor))
                                maiorrow = rowBOD;
                        }
                        if (Saldo != 0)
                            Itens[maiorrow.BOD].Valor += Saldo;
                    }
                    return Dividir(Justificativa, Itens, Proprietario1, Proprietario2);
                case StatusBoleto.Acordo:
                    return DividirBoletoAcordo(Justificativa, ValorBol1);
            }
        }

        /// <summary>
        /// divide o boleto em 2
        /// </summary>
        /// <param name="Justificativa"></param>
        /// <param name="porc">porcentagem do primeiro</param>
        /// <param name="Proprietario1">Destinat�rio do primeiro</param>
        /// <param name="Proprietario2">Destinat�rio do segundo</param>
        /// <returns></returns>
        public Boleto[] DividirPorc(string Justificativa, decimal porc, bool? Proprietario1 = null, bool? Proprietario2 = null)
        {
            switch (BOLStatus)
            {
                case StatusBoleto.Erro:
                case StatusBoleto.Antigo:
                case StatusBoleto.Cancelado:
                case StatusBoleto.OriginalDividido:
                case StatusBoleto.Extra:
                default:
                    throw new Exception(string.Format("Boleto tipo '{0}' n�o pode ser dividido", BOLStatus));
                case StatusBoleto.Valido:
                case StatusBoleto.EmProducao:
                case StatusBoleto.Parcial:
                case StatusBoleto.ParcialAcordo:
                case StatusBoleto.OriginalAcordo:
                case StatusBoleto.Saldinho:
                case StatusBoleto.Acumulado:
                    SortedList<int, PortaItem> Itens = new SortedList<int, PortaItem>();
                    foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                        Itens.Add(rowBOD.BOD, new PortaItem(rowBOD.BODMensagem, rowBOD.BOD_PLA, Math.Round(rowBOD.BODValor * porc, 2), rowBOD.BOD));
                    return Dividir(Justificativa, Itens, Proprietario1, Proprietario2);
                case StatusBoleto.Acordo:
                    return DividirBoletoAcordo(Justificativa, Math.Round(ValorPrevisto * porc, 2, MidpointRounding.AwayFromZero));
            }
        }

        private Boleto[] DividirBoletoAcordo(string Justificativa, decimal ValorBol1)
        {
            Boleto BGerado;
            decimal ValorOriginal = ValorPrevisto;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 5875", dBoletos.BOLetosTableAdapter,
                                                                               dBoletos.BOletoDetalheTableAdapter,
                                                                               dBoletos.BoletoDEscontoTableAdapter);
                List<int> ABODsNovo = new List<int>();
                decimal Saldo = ValorPrevisto - ValorBol1;
                foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                {
                    if (Saldo == 0)
                        break;
                    if (rowBOD.IsBODAcordo_BOLNull())
                        continue;
                    if (Saldo >= rowBOD.BODValor)
                    {
                        //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BoletoDetalhe set BOD_BOL = NULL where BOD = @P1", rowBOD.BOD);
                        rowBOD.SetBOD_BOLNull();
                        dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                        rowBOD.AcceptChanges();
                        ABODsNovo.Add(rowBOD.BOD);
                        Saldo -= rowBOD.BODValor;
                    }
                    else
                    {
                        Boleto BolDividir = new Boleto(rowBOD.BODAcordo_BOL);
                        Boleto[] NovosBoletos = BolDividir.DividirPorc(Justificativa, (rowBOD.BODValor - Saldo) / rowBOD.BODValor);
                        if (rowBOD.BODMensagem.Contains("(integral)"))
                            rowBOD.BODMensagem = rowBOD.BODMensagem.Replace("(integral)", string.Empty).Trim();
                        dBoletos.BOletoDetalheRow rowBODNova = Clona_BOletoDetalheRow(rowBOD);
                        ABODsNovo.Add(rowBODNova.BOD);
                        rowBODNova.BODAcordo_BOL = NovosBoletos[1].BOL;
                        rowBODNova.BODValor = Saldo;
                        rowBOD.BODValor = rowBOD.BODValor - Saldo;
                        rowBOD.BODAcordo_BOL = NovosBoletos[0].BOL;
                        Saldo = 0;
                    }
                };
                foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
                {
                    if (Saldo == 0)
                        break;
                    if (!rowBOD.IsBODAcordo_BOLNull())
                        continue;
                    if (Saldo >= rowBOD.BODValor)
                    {
                        ABODsNovo.Add(rowBOD.BOD);
                        Saldo -= rowBOD.BODValor;
                    }
                    else
                    {
                        dBoletos.BOletoDetalheRow rowBODNova = Clona_BOletoDetalheRow(rowBOD);
                        ABODsNovo.Add(rowBODNova.BOD);
                        rowBOD.BODValor = rowBOD.BODValor - Saldo;
                        rowBODNova.BODValor = Saldo;
                        Saldo = 0;
                    }
                };
                dBoletos.BOletoDetalheTableAdapter.Update(dBoletos.BOletoDetalhe);
                dBoletos.BOletoDetalhe.AcceptChanges();
                string BOLMensagemImp = rowPrincipal.IsBOLMensagemImpNull() ? string.Empty : rowPrincipal.BOLMensagemImp;
                BGerado = new Boleto(rowPrincipal.BOLProprietario, Apartamento, BOLMensagemImp, Competencia, rowPrincipal.BOLVencto, rowPrincipal.BOLTipoCRAI, StatusBoleto.Acordo, false, false, ABODsNovo.ToArray());
                Acordo.Acordo Ac = new Acordo.Acordo(this, false);
                Ac.IncluirBoleto(BGerado.BOL, false);
                string[] Just = new string[] { Justificativa, Justificativa };
                for (int i = 0; i < 2; i++)
                    Just[i] = string.Format("{0}\r\n" +
                                            " {1} Original     : {3} - {5,9:n2}\r\n" +
                                            " {1} Remanecente  : {3} - {6,9:n2}\r\n" +
                                            " {2} Desmembrado  : {4} - {7,9:n2}\r\n",
                                              Justificativa,
                                              i == 0 ? "->" : "  ",
                                              i == 1 ? "->" : "  ",
                                              BOL, BGerado.BOL,
                                              ValorOriginal, ValorOriginal - BGerado.ValorPrevisto, BGerado.ValorPrevisto);

                SalvarAlteracoes(Just[0]);
                BGerado.GravaJustificativa(Just[1]);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro Dividir: " + e.Message, e);
            }
            return new Boleto[] { new Boleto(BOL), BGerado };
        }

        private dBoletos.BOletoDetalheRow Clona_BOletoDetalheRow(dBoletos.BOletoDetalheRow rowModelo)
        {
            dBoletos.BOletoDetalheRow rowBODNova = dBoletos.BOletoDetalhe.NewBOletoDetalheRow();
            foreach (DataColumn DC in dBoletos.BOletoDetalhe.Columns)
                if ((!DC.ReadOnly) && (DC.ColumnName != "BOD_BOL"))
                    rowBODNova[DC.ColumnName] = rowModelo[DC.ColumnName];
            dBoletos.BOletoDetalhe.AddBOletoDetalheRow(rowBODNova);
            dBoletos.BOletoDetalheTableAdapter.Update(rowBODNova);
            rowBODNova.AcceptChanges();
            return rowBODNova;
        }

        /// <summary>
        /// divide o boleto em 2
        /// </summary>
        /// <param name="Itens">Cole��o que mostra como devem ficar os itens do primeiro boleto</param>
        /// <param name="Proprietario1"></param>
        /// <param name="Proprietario2"></param>
        /// <param name="Justificativa"></param>
        /// <returns></returns>
        public Boleto[] Dividir(string Justificativa, SortedList<int, PortaItem> Itens, bool? Proprietario1 = null, bool? Proprietario2 = null)
        {
            switch (BOLStatus)
            {
                case StatusBoleto.Erro:
                case StatusBoleto.Antigo:
                case StatusBoleto.Cancelado:
                case StatusBoleto.Extra:
                case StatusBoleto.OriginalDividido:
                case StatusBoleto.Acordo:
                default:
                    throw new Exception(string.Format("Boleto tipo '{0}' n�o pode ser dividido", BOLStatus));
                case StatusBoleto.Valido:
                case StatusBoleto.OriginalAcordo:
                case StatusBoleto.Acumulado:
                    return DividirEfetivoGera2(Justificativa, Itens, Proprietario1.GetValueOrDefault(rowPrincipal.BOLProprietario), Proprietario2.GetValueOrDefault(rowPrincipal.BOLProprietario));
                case StatusBoleto.EmProducao:
                case StatusBoleto.Parcial:
                case StatusBoleto.ParcialAcordo:
                case StatusBoleto.Saldinho:
                    return DividirEfetivoGera1(Justificativa, Itens, Proprietario1.GetValueOrDefault(rowPrincipal.BOLProprietario), Proprietario2.GetValueOrDefault(rowPrincipal.BOLProprietario));
            }
        }

        /// <summary>
        /// Substitui o boleto por outros 2
        /// </summary>
        /// <param name="Justificativa"></param>
        /// <param name="Itens"></param>
        /// <param name="Proprietario1"></param>
        /// <param name="Proprietario2"></param>
        /// <returns></returns>
        private Boleto[] DividirEfetivoGera2(string Justificativa, SortedList<int, PortaItem> Itens, bool Proprietario1, bool Proprietario2)
        {
            Boleto B1;
            Boleto B2;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 5886", dBoletos.BOLetosTableAdapter);
                StatusBoleto NovoStatusGerado;
                switch (BOLStatus)
                {
                    case StatusBoleto.Valido:
                        NovoStatusGerado = StatusBoleto.Parcial;
                        BOLStatus = StatusBoleto.OriginalDividido;
                        break;
                    case StatusBoleto.OriginalAcordo:
                        NovoStatusGerado = StatusBoleto.ParcialAcordo;
                        BOLStatus = StatusBoleto.OriginalDividido;
                        break;
                    default:
                        throw new NotImplementedException("n�o implementado"); ;
                };


                rowPrincipal.BOLStatusRegistro = (int)StatusRegistroBoleto.BoletoSemRegistro;
                rowPrincipal.BOLCancelado = true;

                //Gera os BODS nos novos boletos e armazena nos Lists
                List<int> ABODs1 = new List<int>();
                List<int> ABODs2 = new List<int>();

                foreach (dBoletos.BOletoDetalheRow rowBODoriginal in rowPrincipal.GetBOletoDetalheRows())
                {
                    int[] RetDiv;
                    if (Itens.ContainsKey(rowBODoriginal.BOD))
                    {
                        PortaItem PorI = Itens[rowBODoriginal.BOD];
                        RetDiv = dBOletosDetalhe.DividirBOD(rowBODoriginal.BOD, true, true, PorI.Valor, rowBODoriginal.BODValor - PorI.Valor);
                    }
                    else
                        RetDiv = dBOletosDetalhe.DividirBOD(rowBODoriginal.BOD, true, true, 0, rowBODoriginal.BODValor);

                    if (RetDiv[0] != 0)
                        ABODs1.Add(RetDiv[0]);
                    if (RetDiv[1] != 0)
                        ABODs2.Add(RetDiv[1]);
                }

                string BOLMensagemImp = rowPrincipal.IsBOLMensagemImpNull() ? string.Empty : rowPrincipal.BOLMensagemImp;

                //Gera os novos boletos                
                B1 = new Boleto(Proprietario1, Apartamento, BOLMensagemImp, Competencia, rowPrincipal.BOLVencto, rowPrincipal.BOLTipoCRAI, NovoStatusGerado, false, false, ABODs1.ToArray());
                B2 = new Boleto(Proprietario2, Apartamento, BOLMensagemImp, Competencia, rowPrincipal.BOLVencto, rowPrincipal.BOLTipoCRAI, NovoStatusGerado, false, false, ABODs2.ToArray());

                //Coloca informa��o nas obs dos 3 boletos
                string[] Just = new string[3];
                for (int i = 0; i < 3; i++)
                    Just[i] = string.Format("{0}\r\n" +
                                            " {1} Original : {4} - {7,9:n2}\r\n" +
                                            " {2} Parte 1  : {5} - {8,9:n2}\r\n" +
                                            " {3} Parte 2  : {6} - {9,9:n2}\r\n",
                                              Justificativa,
                                              i == 0 ? "->" : "  ",
                                              i == 1 ? "->" : "  ",
                                              i == 2 ? "->" : "  ",
                                              BOL, B1.BOL, B2.BOL,
                                              ValorPrevisto, B1.ValorPrevisto, B2.ValorPrevisto);

                GravaJustificativa(Just[0], null);
                B1.GravaJustificativa(Just[1], null);
                B2.GravaJustificativa(Just[2], null);

                //Registra a opera��o em um acordo tipo divis�o
                Acordo.Acordo.GeraDivisao(this, B1, B2);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro Dividir: " + e.Message, e);
            }
            return new Boleto[] { B1, B2 };
        }

        /// <summary>
        /// Separa uma parte do boleto como outro ativo
        /// </summary>
        /// <param name="Justificativa"></param>
        /// <param name="Itens"></param>
        /// <param name="Proprietario1"></param>
        /// <param name="Proprietario2"></param>
        /// <returns></returns>
        private Boleto[] DividirEfetivoGera1(string Justificativa, SortedList<int, PortaItem> Itens, bool Proprietario1, bool Proprietario2)
        {
            Boleto BGerado;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 5954", dBoletos.BOLetosTableAdapter,
                                                                               dBoletos.BOletoDetalheTableAdapter,
                                                                               dBoletos.BoletoDEscontoTableAdapter);
                //Gera os BODS para o novo boleto e armazena no List
                List<int> ABODsNovo = new List<int>();

                decimal ValorRemanecente = 0;

                foreach (dBoletos.BOletoDetalheRow rowBODoriginal in rowPrincipal.GetBOletoDetalheRows())
                    if (Itens.ContainsKey(rowBODoriginal.BOD))
                    {
                        PortaItem PorI = Itens[rowBODoriginal.BOD];
                        ValorRemanecente += PorI.Valor;
                        if (PorI.Valor != rowBODoriginal.BODValor)
                        {
                            int[] RetDiv = dBOletosDetalhe.DividirBOD(rowBODoriginal.BOD, false, false, PorI.Valor, rowBODoriginal.BODValor - PorI.Valor);
                            ABODsNovo.Add(RetDiv[1]);
                        }
                    }
                    else
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BoletoDetalhe set BOD_BOL = NULL where BOD = @P1", rowBODoriginal.BOD);
                        ABODsNovo.Add(rowBODoriginal.BOD);
                    }

                string BOLMensagemImp = rowPrincipal.IsBOLMensagemImpNull() ? string.Empty : rowPrincipal.BOLMensagemImp;

                //Gera o novo boleto
                BGerado = new Boleto(Proprietario2, Apartamento, BOLMensagemImp, Competencia, rowPrincipal.BOLVencto, rowPrincipal.BOLTipoCRAI, BOLStatus, false, false, ABODsNovo.ToArray());
                decimal ValorOriginal = ValorPrevisto;
                CarregaDadosDoBoleto(BOL);
                if (rowPrincipal.BOLProprietario != Proprietario1)
                    rowPrincipal.BOLProprietario = Proprietario1;
                string[] Just = new string[] { Justificativa, Justificativa };
                if (BOLStatus != StatusBoleto.EmProducao)
                    for (int i = 0; i < 2; i++)
                        Just[i] = string.Format("{0}\r\n" +
                                                " {1} Original     : {3} - {5,9:n2}\r\n" +
                                                " {1} Remanecente  : {3} - {6,9:n2}\r\n" +
                                                " {2} Desmembrado  : {4} - {7,9:n2}\r\n",
                                                  Justificativa,
                                                  i == 0 ? "->" : "  ",
                                                  i == 1 ? "->" : "  ",
                                                  BOL, BGerado.BOL,
                                                  ValorOriginal, ValorRemanecente, BGerado.ValorPrevisto);
                SalvarAlteracoes(Just[0]);
                BGerado.GravaJustificativa(Just[1]);
                if (BOLStatus != StatusBoleto.EmProducao)
                {
                    Acordo.Acordo AcDivide = new Acordo.Acordo(this, false, BoletosProc.Acordo.StatusACO.Divide);
                    if (!AcDivide.Gerado)
                        throw new Exception("Acordo n�o encontrado");
                    AcDivide.IncluirBoleto(BGerado.BOL, false);
                    /*
                    if (BOLStatus == StatusBoleto.ParcialAcordo)
                    {
                        Acordo.Acordo AcAcordo = new Acordo.Acordo(this, true, Acordo.StatusACO.Ativo);
                        if (!AcAcordo.Gerado)
                            throw new Exception("Acordo n�o encontrado");
                        AcAcordo.IncluirBoleto(BGerado.BOL, true);
                    }
                    */
                }
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro Dividir: " + e.Message, e);
            }
            return new Boleto[] { new Boleto(BOL), BGerado };
        }

        /// <summary>
        /// Divide o boleto para acordo em v�rias partes
        /// </summary>
        /// <param name="ACO">Acordo</param>
        /// <param name="porcs"></param>
        /// <returns></returns>
        internal Boleto[] Dividir(int ACO, params decimal[] porcs)
        {
            Boleto[] Bn;
            try
            {
                Bn = new Boleto[porcs.Length];
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos Boleto - 5077", dBoletos.BOLetosTableAdapter);

                ArrayList[] ABODsn = new ArrayList[porcs.Length];
                decimal[] Valores = new decimal[porcs.Length];
                for (int i = 0; i < porcs.Length; i++)
                    ABODsn[i] = new ArrayList();

                foreach (dBoletos.BOletoDetalheRow rowBODoriginal in rowPrincipal.GetBOletoDetalheRows())
                {
                    decimal Saldo = rowBODoriginal.BODValor;
                    for (int i = 0; i < porcs.Length - 1; i++)
                        Saldo -= Valores[i] = Math.Round(rowBODoriginal.BODValor * porcs[i], 2);
                    Valores[porcs.Length - 1] = Saldo;

                    int[] RetDiv = dBOletosDetalhe.DividirBOD(rowBODoriginal.BOD, true, true, Valores);

                    for (int i = 0; i < porcs.Length; i++)
                        if (RetDiv[i] != 0)
                            ABODsn[i].Add(RetDiv[i]);
                }


                string BOLMensagemImp = rowPrincipal.IsBOLMensagemImpNull() ? string.Empty : rowPrincipal.BOLMensagemImp;

                for (int i = 0; i < porcs.Length; i++)
                {
                    
                    Bn[i] = new Boleto(Apartamento.Alugado ? rowPrincipal.BOLProprietario : true, Apartamento, BOLMensagemImp, Competencia, rowPrincipal.BOLVencto, rowPrincipal.BOLTipoCRAI, StatusBoleto.ParcialAcordo, false, false, (int[])ABODsn[i].ToArray(typeof(int)));
                    
                }
                
                Acordo.Acordo.GeraDivisao(this, Bn);
                
                string Obsoriginal = string.Empty;
                if (!rowPrincipal.IsBOLJustificativaNull())
                    Obsoriginal = rowPrincipal.BOLJustificativa + "\r\n\r\n";
                Obsoriginal += string.Format("{0} - Boleto dividido para inclus�o no acordo {1}", DateTime.Now, ACO);
                Obsoriginal += string.Format("\r\n       -> Original :{0} - {1}", rowPrincipal.BOL, rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                string ObsFilho;
                for (int i = 0; i < porcs.Length; i++)
                {
                    ObsFilho = string.Format("Boleto gerado por divis�o para inclus�o no acordo {0}", ACO);
                    ObsFilho += string.Format("\r\n          Original :{0} - {1}", rowPrincipal.BOL, rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                    Obsoriginal += string.Format("\r\n          Parcela {0}:{1} - {2}", i + 1, Bn[i].rowPrincipal.BOL, Bn[i].rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                    for (int j = 0; j < porcs.Length; j++)
                        ObsFilho += string.Format("\r\n      {0}  Parcela {1}:{2} - {3}", (i == j) ? "->" : "  ", j + 1, Bn[j].rowPrincipal.BOL, Bn[j].rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                    
                    Bn[i].SalvarAlteracoes(ObsFilho);
                    
                }

                if (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update Boletos set BOLCancelado = 1,BOLStatus = @P1,BOLExportar = 0,BOLJustificativa = @P2 where BOL = @P3", (int)StatusBoleto.OriginalDividido, Obsoriginal, rowPrincipal.BOL) < 1)
                    throw new Exception("Erro na divisao do boleto");




                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                UltimoErro = e.Message;
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                throw new Exception("Erro Dividir: " + e.Message, e);
            }
            return Bn;
        }

        #endregion

        #region Est�ticos
        /// <summary>
        /// VirEnum para StatusBoleto
        /// </summary>
        public static VirEnum VirEnumStatusBoleto
        {
            get
            {
                if (virEnumStatusBoleto == null)
                {
                    virEnumStatusBoleto = new VirEnum(typeof(StatusBoleto));
                    virEnumStatusBoleto.GravaNomes(StatusBoleto.Antigo, "Antigo", Color.Red);
                    virEnumStatusBoleto.GravaNomes(StatusBoleto.Cancelado, "Boleto cancelado", Color.Red);
                    virEnumStatusBoleto.GravaNomes(StatusBoleto.Valido, "Boleto v�lido", Color.FromArgb(200, 254, 200));//verde
                    virEnumStatusBoleto.GravaNomes(StatusBoleto.OriginalDividido, "Boleto dividido", Color.Wheat);
                    virEnumStatusBoleto.GravaNomes(StatusBoleto.Parcial, "Boleto Parcial", Color.Silver);
                    virEnumStatusBoleto.GravaNomes(StatusBoleto.OriginalAcordo, "Boleto Original (Acordo)", Color.Wheat);
                    virEnumStatusBoleto.GravaNomes(StatusBoleto.ParcialAcordo, "Boleto parical (Acordo)", Color.Silver);
                    virEnumStatusBoleto.GravaNomes(StatusBoleto.Acordo, "Boleto de Acordo", Color.FromArgb(200, 254, 200));
                    VirEnumStatusBoleto.GravaNomes(StatusBoleto.EmProducao, "Em emiss�o", Color.Yellow);
                    VirEnumStatusBoleto.GravaNomes(StatusBoleto.Saldinho, "Saldo", Color.DarkGoldenrod);
                    VirEnumStatusBoleto.GravaNomes(StatusBoleto.Acumulado, "Acumulado no pr�ximo", Color.DarkGoldenrod);
                }
                return virEnumStatusBoleto;
            }
        }

        /// <summary>
        /// VirEnum para StatusRegistroBoleto
        /// </summary>
        public static VirEnum VirEnumStatusRegistroBoleto
        {
            get
            {
                if (virEnumStatusRegistroBoleto == null)
                {
                    virEnumStatusRegistroBoleto = new VirEnum(typeof(StatusRegistroBoleto));
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoSemRegistro, "Sem registro", Color.White);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoRegistrando, "Aguarda Registro", Color.Yellow);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoAguardandoRetorno, "Aguarda Retorno", Color.Violet);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoRegistrado, "Registrado", Color.Lime);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoRegistradoDDA, "Registrado DDA", Color.Lime);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.ComregistroNaoRegistrado, "N�o registrado", Color.White);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.AlterarRegistroData, "Alterando Data", Color.Yellow);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.AlterarRegistroValor, "Alterando Valor", Color.Yellow);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.ErroNoRegistro, "Erro no registro", Color.Red);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoAguardandoRetornoDA, "Ag. Ret. D�bito", Color.Violet);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoQuitado, " - ", Color.White);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoRegistradoDA, "D�bito Autom�tico", Color.Aqua);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoRegistrandoDA, "Ag. Registro D�bito", Color.Yellow);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoRegistroVencido, "Registro Vencido", Color.Pink);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoQuitado, "Quitado", Color.Gainsboro);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoQuitadoDDA, "Quitado DDA", Color.Gainsboro);
                    virEnumStatusRegistroBoleto.GravaNomes(StatusRegistroBoleto.BoletoQuitadoDA, "Quitado DA", Color.Gainsboro);
                }
                return virEnumStatusRegistroBoleto;
            }
        }

        static private List<StatusBoleto> _StatusBoletoDivisivel;
        /// <summary>
        /// Conjunto de statusBoleto que podem ser divididos
        /// </summary>
        static public List<StatusBoleto> StatusBoletoDivisivel
        {
            get
            {
                return _StatusBoletoDivisivel ?? (_StatusBoletoDivisivel = new List<StatusBoleto>(new StatusBoleto[] { StatusBoleto.Antigo,
                                                                                                                StatusBoleto.Parcial,
                                                                                                                StatusBoleto.Valido,
                                                                                                                StatusBoleto.EmProducao}));
            }
        }

        static private List<StatusBoleto> _StatusBoletoEditavel;
        /// <summary>
        /// Conjunto de statusBoleto que podem ser Editados
        /// </summary>
        static public List<StatusBoleto> StatusBoletoEditavel
        {
            get
            {
                return _StatusBoletoEditavel ?? (_StatusBoletoEditavel = new List<StatusBoleto>(new StatusBoleto[] { StatusBoleto.Antigo,
                                                                                                              StatusBoleto.Parcial,
                                                                                                              StatusBoleto.Valido,
                                                                                                              StatusBoleto.Acordo,
                                                                                                              StatusBoleto.EmProducao,
                                                                                                              StatusBoleto.Acumulado,
                                                                                                              StatusBoleto.Saldinho}));
            }
        }

        static private List<StatusBoleto> _StatusBoletoRegistravel;
        /// <summary>
        /// Conjunto de statusBoleto que podem ser Editados
        /// </summary>
        static public List<StatusBoleto> StatusBoletoRegistravel
        {
            get
            {
                return _StatusBoletoRegistravel ?? (_StatusBoletoRegistravel = new List<StatusBoleto>(new StatusBoleto[] { StatusBoleto.Parcial,
                                                                                                                           StatusBoleto.Valido,
                                                                                                                           StatusBoleto.Acordo}));
            }
        }

        static private ArrayList _StatusBoletoExportavel;
        /// <summary>
        /// Conjunto de statusBoleto que podem ser Exportavel
        /// </summary>
        static public ArrayList StatusBoletoExportavel
        {
            get
            {
                return _StatusBoletoExportavel ?? (_StatusBoletoExportavel = new ArrayList(new StatusBoleto[] { StatusBoleto.Antigo,
                                                                                                                  StatusBoleto.Parcial,
                                                                                                                  StatusBoleto.Valido,
                                                                                                                  StatusBoleto.Acordo,
                                                                                                                  StatusBoleto.Extra
                                                                                                                 }));
            }
        }

        static private List<StatusRegistroBoleto> _StatusRegistroAguarda;

        /// <summary>
        /// Conjunto dos status que ainda vao evoluir
        /// </summary>
        static public List<StatusRegistroBoleto> StatusRegistroAguarda
        {
            get
            {
                return _StatusRegistroAguarda ?? (_StatusRegistroAguarda = new List<StatusRegistroBoleto>(new StatusRegistroBoleto[]
                         { StatusRegistroBoleto.AlterarRegistroData,
                             StatusRegistroBoleto.AlterarRegistroValor,
                             StatusRegistroBoleto.BoletoAguardandoRetorno,
                             StatusRegistroBoleto.BoletoRegistrando,
                             StatusRegistroBoleto.ErroNoRegistro,
                             StatusRegistroBoleto.BoletoRegistradoDA,
                         }));
            }
        }

        #endregion

    }

    



    /// <summary>
    /// Classe porta item
    /// </summary>
    public class PortaItem
    {
        /// <summary>
        /// Item
        /// </summary>
        public int BOD;
        /// <summary>
        /// Descricao
        /// </summary>
        public string Descricao;
        /// <summary>
        /// PLA
        /// </summary>
        public string PLA;
        /// <summary>
        /// Chamador
        /// </summary>
        public object Sender;
        /// <summary>
        /// Valor
        /// </summary>
        public decimal Valor;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Modelo"></param>
        public PortaItem(PortaItem Modelo)
        {
            Descricao = Modelo.Descricao;
            PLA = Modelo.PLA;
            Valor = Modelo.Valor;
            BOD = Modelo.BOD;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Descricao"></param>
        /// <param name="_PLA"></param>
        /// <param name="_Valor"></param>
        /// <param name="_BOD"></param>
        public PortaItem(string _Descricao, string _PLA, decimal _Valor, int _BOD)
        {
            Descricao = _Descricao;
            PLA = _PLA;
            Valor = _Valor;
            BOD = _BOD;
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", PLA, Descricao.PadRight(40), Valor.ToString("n2").PadLeft(9));
        }

    }

}
