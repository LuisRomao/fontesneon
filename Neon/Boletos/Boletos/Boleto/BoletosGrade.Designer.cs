namespace Boletos.Boleto
{
    partial class BoletosGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BoletosGrade));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOD_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLupa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dBoletos = new BoletosProc.Boleto.dBoletos();
            this.bOLetosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bOLetosTableAdapter = new BoletosProc.Boleto.dBoletosTableAdapters.BOLetosTableAdapter();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EditorPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.colBOLTipoCRAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLMulta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPrevisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaAno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLImpressao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL_FPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.BtCorrecao = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.cmdLimpaBoletos = new DevExpress.XtraEditors.SimpleButton();
            this.cmdGeraRemessa = new DevExpress.XtraEditors.SimpleButton();
            this.cmdSeparaBoletos = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.BotCancelaProt = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoOriginal = new dllImpresso.Botoes.cBotaoImpBol();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.cGeraSuper = new dllImpresso.Botoes.cBotaoImpBol();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.colBOLEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.BCorItau1 = new DevExpress.XtraEditors.SimpleButton();
            this.BCorItau = new DevExpress.XtraEditors.SimpleButton();
            this.SBInquilino = new DevExpress.XtraEditors.SimpleButton();
            this.SBProprietario = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.ch6M = new DevExpress.XtraEditors.CheckEdit();
            this.BotExtrato = new VirBotaoNeon.cBotaoImpNeon();
            this.BotNadaConsta = new VirBotaoNeon.cBotaoImpNeon();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cBotaoImpRecibo = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpJuros = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpPara = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoSegundaVia = new dllImpresso.Botoes.cBotaoImpBol();
            this.panelJuridico = new DevExpress.XtraEditors.PanelControl();
            this.labelEscritorio = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditVencimento = new DevExpress.XtraEditors.DateEdit();
            this.checkVencidos = new DevExpress.XtraEditors.CheckEdit();
            this.BotAcordo = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cloAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorCorrigido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMulta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJuros = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHonorarios = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.colBOLJustificativa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colBOLStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookSuper = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.SuperBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.timerCalcular = new System.Windows.Forms.Timer(this.components);
            this.BotCreditos = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colBOLStatusRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBoletos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLetosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch6M.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelJuridico)).BeginInit();
            this.panelJuridico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVencimento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVencimento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkVencidos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookSuper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SuperBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotCreditos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Location = new System.Drawing.Point(0, 201);
            this.groupControl1.Size = new System.Drawing.Size(1330, 84);
            // 
            // LayoutPesquisa_F
            // 
            this.LayoutPesquisa_F.Size = new System.Drawing.Size(1326, 84);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            this.BtnIncluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            this.BtnExcluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridControl_F.DataSource = this.bOLetosBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            gridLevelNode1.LevelTemplate = this.gridView1;
            gridLevelNode1.RelationName = "FK_BOletoDetalhe_BOLetos1";
            this.GridControl_F.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.GridControl_F.Location = new System.Drawing.Point(0, 285);
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.EditorPI,
            this.repositoryItemMemoEdit1,
            this.repositoryItemImageComboBox1,
            this.repositoryItemLookSuper,
            this.repositoryItemButtonEdit1,
            this.BotCreditos,
            this.repositoryItemImageComboBox2});
            this.GridControl_F.ShowOnlyPredefinedDetails = true;
            this.GridControl_F.Size = new System.Drawing.Size(1330, 362);
            this.GridControl_F.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.PaleTurquoise;
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.Row.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL,
            this.colBOLProprietario,
            this.colBOLTipoCRAI,
            this.colBOLEmissao,
            this.colBOLVencto,
            this.colBOLPagamento,
            this.colBOLMulta,
            this.colBOLValorPrevisto,
            this.colBOLValorPago,
            this.colBOLCompetenciaAno,
            this.colBOLCompetenciaMes,
            this.colBOLNome,
            this.colBOLImpressao,
            this.colBOL_FPG,
            this.colBOLEndereco,
            this.colBLOCodigo,
            this.cloAPTNumero,
            this.colValorCorrigido,
            this.colMulta,
            this.colJuros,
            this.colSubTotal,
            this.colHonorarios,
            this.colTotal,
            this.colBOLJustificativa,
            this.colBOLStatus,
            this.gridColumn1,
            this.gridColumn2,
            this.colBOLStatusRegistro});
            this.GridView_F.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsDetail.ShowDetailTabs = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GridView_F.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsSelection.MultiSelect = true;
            this.GridView_F.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOLVencto, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridView_F_RowCellStyle);
            this.GridView_F.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.GridView_F_RowStyle);
            this.GridView_F.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOD_PLA,
            this.colBODMensagem,
            this.colBODValor,
            this.colLupa});
            this.gridView1.GridControl = this.GridControl_F;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            // 
            // colBOD_PLA
            // 
            this.colBOD_PLA.Caption = "Conta";
            this.colBOD_PLA.FieldName = "BOD_PLA";
            this.colBOD_PLA.Name = "colBOD_PLA";
            this.colBOD_PLA.OptionsColumn.ReadOnly = true;
            this.colBOD_PLA.Visible = true;
            this.colBOD_PLA.VisibleIndex = 0;
            // 
            // colBODMensagem
            // 
            this.colBODMensagem.Caption = "Descri��o";
            this.colBODMensagem.FieldName = "BODMensagem";
            this.colBODMensagem.Name = "colBODMensagem";
            this.colBODMensagem.OptionsColumn.ReadOnly = true;
            this.colBODMensagem.Visible = true;
            this.colBODMensagem.VisibleIndex = 1;
            this.colBODMensagem.Width = 311;
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.DisplayFormat.FormatString = "n2";
            this.colBODValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.OptionsColumn.ReadOnly = true;
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 2;
            this.colBODValor.Width = 88;
            // 
            // colLupa
            // 
            this.colLupa.Caption = "gridColumn3";
            this.colLupa.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colLupa.Name = "colLupa";
            this.colLupa.OptionsColumn.ShowCaption = false;
            this.colLupa.Visible = true;
            this.colLupa.VisibleIndex = 3;
            this.colLupa.Width = 30;
            // 
            // dBoletos
            // 
            this.dBoletos.DataSetName = "dBoletos";
            this.dBoletos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bOLetosBindingSource
            // 
            this.bOLetosBindingSource.DataMember = "BOLetos";
            this.bOLetosBindingSource.DataSource = this.dBoletos;
            // 
            // bOLetosTableAdapter
            // 
            this.bOLetosTableAdapter.ClearBeforeFill = true;
            this.bOLetosTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("bOLetosTableAdapter.GetNovosDados")));
            this.bOLetosTableAdapter.TabelaDataTable = null;
            // 
            // colBOL
            // 
            this.colBOL.Caption = "N. Bol";
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "BOL", "Boletos:{0:n0}")});
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 3;
            this.colBOL.Width = 70;
            // 
            // colBOLProprietario
            // 
            this.colBOLProprietario.Caption = "Dest.";
            this.colBOLProprietario.ColumnEdit = this.EditorPI;
            this.colBOLProprietario.FieldName = "BOLProprietario";
            this.colBOLProprietario.Name = "colBOLProprietario";
            this.colBOLProprietario.OptionsColumn.AllowEdit = false;
            this.colBOLProprietario.OptionsColumn.ReadOnly = true;
            this.colBOLProprietario.Visible = true;
            this.colBOLProprietario.VisibleIndex = 4;
            this.colBOLProprietario.Width = 34;
            // 
            // EditorPI
            // 
            this.EditorPI.AutoHeight = false;
            this.EditorPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EditorPI.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EditorPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Propriet�rio", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 1)});
            this.EditorPI.Name = "EditorPI";
            this.EditorPI.SmallImages = this.imagensPI1;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // colBOLTipoCRAI
            // 
            this.colBOLTipoCRAI.Caption = "Tipo";
            this.colBOLTipoCRAI.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI.Name = "colBOLTipoCRAI";
            this.colBOLTipoCRAI.OptionsColumn.AllowEdit = false;
            this.colBOLTipoCRAI.OptionsColumn.ReadOnly = true;
            this.colBOLTipoCRAI.Visible = true;
            this.colBOLTipoCRAI.VisibleIndex = 5;
            this.colBOLTipoCRAI.Width = 32;
            // 
            // colBOLEmissao
            // 
            this.colBOLEmissao.Caption = "Emiss�o";
            this.colBOLEmissao.FieldName = "BOLEmissao";
            this.colBOLEmissao.Name = "colBOLEmissao";
            this.colBOLEmissao.OptionsColumn.ReadOnly = true;
            // 
            // colBOLVencto
            // 
            this.colBOLVencto.Caption = "Vencto";
            this.colBOLVencto.FieldName = "BOLVencto";
            this.colBOLVencto.Name = "colBOLVencto";
            this.colBOLVencto.OptionsColumn.ReadOnly = true;
            this.colBOLVencto.Visible = true;
            this.colBOLVencto.VisibleIndex = 6;
            this.colBOLVencto.Width = 66;
            // 
            // colBOLPagamento
            // 
            this.colBOLPagamento.Caption = "Pagamento";
            this.colBOLPagamento.FieldName = "BOLPagamento";
            this.colBOLPagamento.Name = "colBOLPagamento";
            this.colBOLPagamento.OptionsColumn.ReadOnly = true;
            this.colBOLPagamento.Visible = true;
            this.colBOLPagamento.VisibleIndex = 7;
            this.colBOLPagamento.Width = 70;
            // 
            // colBOLMulta
            // 
            this.colBOLMulta.Caption = "Multa";
            this.colBOLMulta.FieldName = "BOLMulta";
            this.colBOLMulta.Name = "colBOLMulta";
            this.colBOLMulta.OptionsColumn.AllowEdit = false;
            this.colBOLMulta.OptionsColumn.ReadOnly = true;
            // 
            // colBOLValorPrevisto
            // 
            this.colBOLValorPrevisto.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colBOLValorPrevisto.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colBOLValorPrevisto.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colBOLValorPrevisto.AppearanceCell.Options.UseBackColor = true;
            this.colBOLValorPrevisto.AppearanceCell.Options.UseFont = true;
            this.colBOLValorPrevisto.AppearanceCell.Options.UseForeColor = true;
            this.colBOLValorPrevisto.Caption = "Valor";
            this.colBOLValorPrevisto.DisplayFormat.FormatString = "n2";
            this.colBOLValorPrevisto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBOLValorPrevisto.FieldName = "BOLValorPrevisto";
            this.colBOLValorPrevisto.Name = "colBOLValorPrevisto";
            this.colBOLValorPrevisto.OptionsColumn.ReadOnly = true;
            this.colBOLValorPrevisto.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BOLValorPrevisto", "{0:n2}")});
            this.colBOLValorPrevisto.Visible = true;
            this.colBOLValorPrevisto.VisibleIndex = 10;
            // 
            // colBOLValorPago
            // 
            this.colBOLValorPago.Caption = "Valor Pago";
            this.colBOLValorPago.DisplayFormat.FormatString = "n2";
            this.colBOLValorPago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBOLValorPago.FieldName = "BOLValorPago";
            this.colBOLValorPago.Name = "colBOLValorPago";
            this.colBOLValorPago.OptionsColumn.ReadOnly = true;
            // 
            // colBOLCompetenciaAno
            // 
            this.colBOLCompetenciaAno.Caption = "Ano";
            this.colBOLCompetenciaAno.FieldName = "BOLCompetenciaAno";
            this.colBOLCompetenciaAno.Name = "colBOLCompetenciaAno";
            this.colBOLCompetenciaAno.OptionsColumn.AllowEdit = false;
            this.colBOLCompetenciaAno.OptionsColumn.ReadOnly = true;
            this.colBOLCompetenciaAno.Visible = true;
            this.colBOLCompetenciaAno.VisibleIndex = 8;
            this.colBOLCompetenciaAno.Width = 36;
            // 
            // colBOLCompetenciaMes
            // 
            this.colBOLCompetenciaMes.Caption = "M�s";
            this.colBOLCompetenciaMes.FieldName = "BOLCompetenciaMes";
            this.colBOLCompetenciaMes.Name = "colBOLCompetenciaMes";
            this.colBOLCompetenciaMes.OptionsColumn.AllowEdit = false;
            this.colBOLCompetenciaMes.OptionsColumn.ReadOnly = true;
            this.colBOLCompetenciaMes.Visible = true;
            this.colBOLCompetenciaMes.VisibleIndex = 9;
            this.colBOLCompetenciaMes.Width = 27;
            // 
            // colBOLNome
            // 
            this.colBOLNome.Caption = "Nome Cedente";
            this.colBOLNome.FieldName = "BOLNome";
            this.colBOLNome.Name = "colBOLNome";
            this.colBOLNome.OptionsColumn.ReadOnly = true;
            this.colBOLNome.Width = 92;
            // 
            // colBOLImpressao
            // 
            this.colBOLImpressao.Caption = "Impresso";
            this.colBOLImpressao.FieldName = "BOLImpressao";
            this.colBOLImpressao.Name = "colBOLImpressao";
            this.colBOLImpressao.OptionsColumn.AllowEdit = false;
            this.colBOLImpressao.OptionsColumn.ReadOnly = true;
            this.colBOLImpressao.Width = 92;
            // 
            // colBOL_FPG
            // 
            this.colBOL_FPG.Caption = "Forma de pagamento";
            this.colBOL_FPG.FieldName = "BOL_FPG";
            this.colBOL_FPG.Name = "colBOL_FPG";
            this.colBOL_FPG.OptionsColumn.AllowEdit = false;
            this.colBOL_FPG.OptionsColumn.ReadOnly = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.BtCorrecao);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.cmdLimpaBoletos);
            this.panelControl1.Controls.Add(this.cmdGeraRemessa);
            this.panelControl1.Controls.Add(this.cmdSeparaBoletos);
            this.panelControl1.Controls.Add(this.simpleButton7);
            this.panelControl1.Controls.Add(this.BotCancelaProt);
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.cBotaoOriginal);
            this.panelControl1.Controls.Add(this.simpleButton10);
            this.panelControl1.Controls.Add(this.simpleButton8);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 647);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1330, 100);
            this.panelControl1.TabIndex = 6;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(9, 65);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(128, 23);
            this.simpleButton4.TabIndex = 23;
            this.simpleButton4.Text = "Reimpress�o / e-mails";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // BtCorrecao
            // 
            this.BtCorrecao.Location = new System.Drawing.Point(1000, 6);
            this.BtCorrecao.Name = "BtCorrecao";
            this.BtCorrecao.Size = new System.Drawing.Size(180, 52);
            this.BtCorrecao.TabIndex = 22;
            this.BtCorrecao.Text = "Corre��o boleto menor";
            this.BtCorrecao.Visible = false;
            this.BtCorrecao.Click += new System.EventHandler(this.BtCorrecao_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(782, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(100, 23);
            this.simpleButton2.TabIndex = 20;
            this.simpleButton2.Text = "Exporta";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // cmdLimpaBoletos
            // 
            this.cmdLimpaBoletos.Location = new System.Drawing.Point(676, 65);
            this.cmdLimpaBoletos.Name = "cmdLimpaBoletos";
            this.cmdLimpaBoletos.Size = new System.Drawing.Size(100, 23);
            this.cmdLimpaBoletos.TabIndex = 19;
            this.cmdLimpaBoletos.Text = "Limpa Boletos";
            this.cmdLimpaBoletos.Click += new System.EventHandler(this.cmdLimpaBoletos_Click);
            // 
            // cmdGeraRemessa
            // 
            this.cmdGeraRemessa.Location = new System.Drawing.Point(676, 35);
            this.cmdGeraRemessa.Name = "cmdGeraRemessa";
            this.cmdGeraRemessa.Size = new System.Drawing.Size(100, 23);
            this.cmdGeraRemessa.TabIndex = 18;
            this.cmdGeraRemessa.Text = "Gera Remessa";
            this.cmdGeraRemessa.Click += new System.EventHandler(this.cmdGeraRemessa_Click);
            // 
            // cmdSeparaBoletos
            // 
            this.cmdSeparaBoletos.Location = new System.Drawing.Point(676, 6);
            this.cmdSeparaBoletos.Name = "cmdSeparaBoletos";
            this.cmdSeparaBoletos.Size = new System.Drawing.Size(100, 23);
            this.cmdSeparaBoletos.TabIndex = 17;
            this.cmdSeparaBoletos.Text = "Separa Boletos";
            this.cmdSeparaBoletos.Click += new System.EventHandler(this.cmdSeparaBoletos_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(383, 65);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(256, 23);
            this.simpleButton7.TabIndex = 16;
            this.simpleButton7.Text = "Exportar";
            this.simpleButton7.Visible = false;
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click_1);
            // 
            // BotCancelaProt
            // 
            this.BotCancelaProt.Location = new System.Drawing.Point(383, 35);
            this.BotCancelaProt.Name = "BotCancelaProt";
            this.BotCancelaProt.Size = new System.Drawing.Size(256, 23);
            this.BotCancelaProt.TabIndex = 15;
            this.BotCancelaProt.Text = "Cancelar protesto";
            this.BotCancelaProt.Visible = false;
            this.BotCancelaProt.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(381, 6);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(258, 23);
            this.simpleButton5.TabIndex = 14;
            this.simpleButton5.Text = "Marcar Boleto como pago (implanta��o)";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // cBotaoOriginal
            // 
            this.cBotaoOriginal.ItemComprovante = true;
            this.cBotaoOriginal.Location = new System.Drawing.Point(9, 35);
            this.cBotaoOriginal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoOriginal.Name = "cBotaoOriginal";
            this.cBotaoOriginal.Size = new System.Drawing.Size(130, 23);
            this.cBotaoOriginal.TabIndex = 11;
            this.cBotaoOriginal.Titulo = "Imprimir";
            this.cBotaoOriginal.clicado += new System.EventHandler(this.cBotaoImpBol2_clicado);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(153, 35);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(222, 23);
            this.simpleButton10.TabIndex = 9;
            this.simpleButton10.Text = "Boletos de teste";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click_3);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(1000, 65);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(180, 23);
            this.simpleButton8.TabIndex = 4;
            this.simpleButton8.Text = "Recupera cancelados";
            this.simpleButton8.Visible = false;
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(9, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(130, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Protocolo";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(157, 6);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(218, 23);
            this.simpleButton3.TabIndex = 8;
            this.simpleButton3.Text = "Cancelar EMISS�O de Boleto";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click_1);
            // 
            // cGeraSuper
            // 
            this.cGeraSuper.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.cGeraSuper.Appearance.Options.UseBackColor = true;
            this.cGeraSuper.Enabled = false;
            this.cGeraSuper.ItemComprovante = true;
            this.cGeraSuper.Location = new System.Drawing.Point(647, 5);
            this.cGeraSuper.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cGeraSuper.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cGeraSuper.Name = "cGeraSuper";
            this.cGeraSuper.Size = new System.Drawing.Size(129, 23);
            this.cGeraSuper.TabIndex = 27;
            this.cGeraSuper.Titulo = "Criar Super Boleto";
            this.cGeraSuper.clicado += new System.EventHandler(this.cBotaoImpBol2_clicado_1);
            // 
            // calcEdit1
            // 
            this.calcEdit1.Location = new System.Drawing.Point(74, 10);
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Size = new System.Drawing.Size(116, 22);
            this.calcEdit1.TabIndex = 4;
            // 
            // colBOLEndereco
            // 
            this.colBOLEndereco.Caption = "Endere�o";
            this.colBOLEndereco.FieldName = "BOLEndereco";
            this.colBOLEndereco.Name = "colBOLEndereco";
            this.colBOLEndereco.OptionsColumn.AllowEdit = false;
            this.colBOLEndereco.OptionsColumn.ReadOnly = true;
            this.colBOLEndereco.Width = 113;
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.panelControl2.Appearance.BackColor2 = System.Drawing.Color.LightCyan;
            this.panelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.Controls.Add(this.BCorItau1);
            this.panelControl2.Controls.Add(this.BCorItau);
            this.panelControl2.Controls.Add(this.SBInquilino);
            this.panelControl2.Controls.Add(this.SBProprietario);
            this.panelControl2.Controls.Add(this.panelControl13);
            this.panelControl2.Controls.Add(this.panelControl12);
            this.panelControl2.Controls.Add(this.panelControl11);
            this.panelControl2.Controls.Add(this.panelControl10);
            this.panelControl2.Controls.Add(this.panelControl9);
            this.panelControl2.Controls.Add(this.ch6M);
            this.panelControl2.Controls.Add(this.BotExtrato);
            this.panelControl2.Controls.Add(this.cGeraSuper);
            this.panelControl2.Controls.Add(this.BotNadaConsta);
            this.panelControl2.Controls.Add(this.panelControl8);
            this.panelControl2.Controls.Add(this.panelControl7);
            this.panelControl2.Controls.Add(this.cBotaoImpRecibo);
            this.panelControl2.Controls.Add(this.cBotaoImpJuros);
            this.panelControl2.Controls.Add(this.cBotaoImpPara);
            this.panelControl2.Controls.Add(this.cBotaoSegundaVia);
            this.panelControl2.Controls.Add(this.panelJuridico);
            this.panelControl2.Controls.Add(this.dateEdit1);
            this.panelControl2.Controls.Add(this.panelControl6);
            this.panelControl2.Controls.Add(this.panelControl5);
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.dateEditVencimento);
            this.panelControl2.Controls.Add(this.checkVencidos);
            this.panelControl2.Controls.Add(this.BotAcordo);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.calcEdit1);
            this.panelControl2.Controls.Add(this.simpleButton6);
            this.panelControl2.Controls.Add(this.checkEdit1);
            this.panelControl2.Controls.Add(this.lookupBlocosAptos_F1);
            this.panelControl2.Controls.Add(this.cCompet1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.LookAndFeel.SkinName = "Office 2010 Blue";
            this.panelControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1330, 201);
            this.panelControl2.TabIndex = 7;
            // 
            // BCorItau1
            // 
            this.BCorItau1.Location = new System.Drawing.Point(1168, 35);
            this.BCorItau1.Name = "BCorItau1";
            this.BCorItau1.Size = new System.Drawing.Size(162, 96);
            this.BCorItau1.TabIndex = 46;
            this.BCorItau1.Text = "Registrar super";
            this.BCorItau1.Visible = false;
            this.BCorItau1.Click += new System.EventHandler(this.BCorItau1_Click);
            // 
            // BCorItau
            // 
            this.BCorItau.Location = new System.Drawing.Point(1000, 35);
            this.BCorItau.Name = "BCorItau";
            this.BCorItau.Size = new System.Drawing.Size(162, 96);
            this.BCorItau.TabIndex = 45;
            this.BCorItau.Text = "Corre��o Ita�";
            this.BCorItau.Visible = false;
            this.BCorItau.Click += new System.EventHandler(this.BCorItau_Click);
            // 
            // SBInquilino
            // 
            this.SBInquilino.Location = new System.Drawing.Point(697, 173);
            this.SBInquilino.Name = "SBInquilino";
            this.SBInquilino.Size = new System.Drawing.Size(79, 22);
            this.SBInquilino.TabIndex = 44;
            this.SBInquilino.Text = "Inquilino";
            this.SBInquilino.Visible = false;
            this.SBInquilino.Click += new System.EventHandler(this.SBInquilino_Click);
            // 
            // SBProprietario
            // 
            this.SBProprietario.Location = new System.Drawing.Point(608, 172);
            this.SBProprietario.Name = "SBProprietario";
            this.SBProprietario.Size = new System.Drawing.Size(75, 23);
            this.SBProprietario.TabIndex = 42;
            this.SBProprietario.Text = "Propriet�rio";
            this.SBProprietario.Visible = false;
            this.SBProprietario.Click += new System.EventHandler(this.SBProprietario_Click);
            // 
            // panelControl13
            // 
            this.panelControl13.Appearance.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panelControl13.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl13.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl13.Appearance.Options.UseBackColor = true;
            this.panelControl13.Controls.Add(this.labelControl13);
            this.panelControl13.Location = new System.Drawing.Point(62, 171);
            this.panelControl13.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl13.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(75, 24);
            this.panelControl13.TabIndex = 41;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseTextOptions = true;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl13.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl13.Location = new System.Drawing.Point(2, 2);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(71, 20);
            this.labelControl13.TabIndex = 14;
            this.labelControl13.Text = "Acumular";
            // 
            // panelControl12
            // 
            this.panelControl12.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.panelControl12.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl12.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl12.Appearance.Options.UseBackColor = true;
            this.panelControl12.Controls.Add(this.labelControl12);
            this.panelControl12.Location = new System.Drawing.Point(6, 171);
            this.panelControl12.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl12.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(48, 24);
            this.panelControl12.TabIndex = 40;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseTextOptions = true;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl12.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl12.Location = new System.Drawing.Point(2, 2);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(44, 20);
            this.labelControl12.TabIndex = 14;
            this.labelControl12.Text = "Extra";
            // 
            // panelControl11
            // 
            this.panelControl11.Appearance.BackColor = System.Drawing.Color.Violet;
            this.panelControl11.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl11.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl11.Appearance.Options.UseBackColor = true;
            this.panelControl11.Controls.Add(this.labelControl11);
            this.panelControl11.Location = new System.Drawing.Point(225, 171);
            this.panelControl11.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl11.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(89, 24);
            this.panelControl11.TabIndex = 39;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseTextOptions = true;
            this.labelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl11.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl11.Location = new System.Drawing.Point(2, 2);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(85, 20);
            this.labelControl11.TabIndex = 14;
            this.labelControl11.Text = "Registrando";
            // 
            // panelControl10
            // 
            this.panelControl10.Appearance.BackColor = System.Drawing.Color.Orange;
            this.panelControl10.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl10.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl10.Appearance.Options.UseBackColor = true;
            this.panelControl10.Controls.Add(this.labelControl10);
            this.panelControl10.Location = new System.Drawing.Point(145, 171);
            this.panelControl10.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl10.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(72, 24);
            this.panelControl10.TabIndex = 38;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseTextOptions = true;
            this.labelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl10.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl10.Location = new System.Drawing.Point(2, 2);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(68, 20);
            this.labelControl10.TabIndex = 14;
            this.labelControl10.Text = "Aguardo";
            // 
            // panelControl9
            // 
            this.panelControl9.Appearance.BackColor = System.Drawing.Color.Wheat;
            this.panelControl9.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl9.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl9.Appearance.Options.UseBackColor = true;
            this.panelControl9.Controls.Add(this.labelControl9);
            this.panelControl9.Location = new System.Drawing.Point(511, 141);
            this.panelControl9.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl9.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(91, 24);
            this.panelControl9.TabIndex = 37;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl9.Location = new System.Drawing.Point(2, 2);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(87, 20);
            this.labelControl9.TabIndex = 14;
            this.labelControl9.Text = "Cancelado";
            // 
            // ch6M
            // 
            this.ch6M.EditValue = true;
            this.ch6M.Location = new System.Drawing.Point(647, 61);
            this.ch6M.Name = "ch6M";
            this.ch6M.Properties.Caption = "�ltimos 6 meses";
            this.ch6M.Size = new System.Drawing.Size(129, 17);
            this.ch6M.TabIndex = 36;
            // 
            // BotExtrato
            // 
            this.BotExtrato.AbrirArquivoExportado = true;
            this.BotExtrato.AcaoBotao = dllBotao.Botao.imprimir;
            this.BotExtrato.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.BotExtrato.Appearance.Options.UseBackColor = true;
            this.BotExtrato.AssuntoEmail = "Extrato Unidade";
            this.BotExtrato.BotaoEmail = true;
            this.BotExtrato.BotaoImprimir = true;
            this.BotExtrato.BotaoPDF = true;
            this.BotExtrato.BotaoTela = true;
            this.BotExtrato.CorpoEmail = "Extrato de unidade anexo";
            this.BotExtrato.CreateDocAutomatico = true;
            this.BotExtrato.Impresso = null;
            this.BotExtrato.Location = new System.Drawing.Point(647, 32);
            this.BotExtrato.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BotExtrato.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BotExtrato.Name = "BotExtrato";
            this.BotExtrato.NaoUsarxlsX = true;
            this.BotExtrato.NomeArquivoAnexo = "Extrato";
            this.BotExtrato.PriComp = null;
            this.BotExtrato.Size = new System.Drawing.Size(129, 23);
            this.BotExtrato.TabIndex = 35;
            this.BotExtrato.Titulo = "Extrato";
            this.BotExtrato.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImpNeon1_clicado);
            this.BotExtrato.NotificaErro += new dllBotao.ErroBotaoHandler(this.BotNadaConsta_NotificaErro);
            // 
            // BotNadaConsta
            // 
            this.BotNadaConsta.AbrirArquivoExportado = false;
            this.BotNadaConsta.AcaoBotao = dllBotao.Botao.imprimir;
            this.BotNadaConsta.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.BotNadaConsta.Appearance.Options.UseBackColor = true;
            this.BotNadaConsta.AssuntoEmail = "Nada Consta";
            this.BotNadaConsta.BotaoEmail = true;
            this.BotNadaConsta.BotaoImprimir = true;
            this.BotNadaConsta.BotaoPDF = true;
            this.BotNadaConsta.BotaoTela = true;
            this.BotNadaConsta.CorpoEmail = "Nada consta anexo";
            this.BotNadaConsta.CreateDocAutomatico = true;
            this.BotNadaConsta.Enabled = false;
            this.BotNadaConsta.Impresso = null;
            this.BotNadaConsta.Location = new System.Drawing.Point(540, 5);
            this.BotNadaConsta.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BotNadaConsta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BotNadaConsta.Name = "BotNadaConsta";
            this.BotNadaConsta.NaoUsarxlsX = true;
            this.BotNadaConsta.NomeArquivoAnexo = "NadaConsta";
            this.BotNadaConsta.PriComp = null;
            this.BotNadaConsta.Size = new System.Drawing.Size(101, 23);
            this.BotNadaConsta.TabIndex = 34;
            this.BotNadaConsta.Titulo = "Nada Consta";
            this.BotNadaConsta.clicado += new dllBotao.BotaoEventHandler(this.BotNadaConsta_clicado);
            this.BotNadaConsta.NotificaErro += new dllBotao.ErroBotaoHandler(this.BotNadaConsta_NotificaErro);
            // 
            // panelControl8
            // 
            this.panelControl8.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.panelControl8.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl8.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl8.Appearance.Options.UseBackColor = true;
            this.panelControl8.Controls.Add(this.labelControl2);
            this.panelControl8.Location = new System.Drawing.Point(60, 141);
            this.panelControl8.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl8.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(79, 24);
            this.panelControl8.TabIndex = 32;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(2, 2);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(75, 20);
            this.labelControl2.TabIndex = 14;
            this.labelControl2.Text = "PAGO\r\nProtestado";
            // 
            // panelControl7
            // 
            this.panelControl7.Appearance.BackColor = System.Drawing.Color.Red;
            this.panelControl7.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl7.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl7.Appearance.Options.UseBackColor = true;
            this.panelControl7.Controls.Add(this.labelControl8);
            this.panelControl7.Location = new System.Drawing.Point(417, 141);
            this.panelControl7.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl7.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(88, 24);
            this.panelControl7.TabIndex = 31;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl8.Location = new System.Drawing.Point(2, 2);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(84, 20);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "PROTESTO";
            // 
            // cBotaoImpRecibo
            // 
            this.cBotaoImpRecibo.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.cBotaoImpRecibo.Appearance.Options.UseBackColor = true;
            this.cBotaoImpRecibo.Enabled = false;
            this.cBotaoImpRecibo.ItemComprovante = false;
            this.cBotaoImpRecibo.Location = new System.Drawing.Point(435, 61);
            this.cBotaoImpRecibo.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoImpRecibo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpRecibo.Name = "cBotaoImpRecibo";
            this.cBotaoImpRecibo.Size = new System.Drawing.Size(99, 23);
            this.cBotaoImpRecibo.TabIndex = 29;
            this.cBotaoImpRecibo.Titulo = "Recibo";
            this.cBotaoImpRecibo.clicado += new System.EventHandler(this.cBotaoImpBol3_clicado_1);
            // 
            // cBotaoImpJuros
            // 
            this.cBotaoImpJuros.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.cBotaoImpJuros.Appearance.Options.UseBackColor = true;
            this.cBotaoImpJuros.Enabled = false;
            this.cBotaoImpJuros.ItemComprovante = false;
            this.cBotaoImpJuros.Location = new System.Drawing.Point(435, 32);
            this.cBotaoImpJuros.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoImpJuros.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpJuros.Name = "cBotaoImpJuros";
            this.cBotaoImpJuros.Size = new System.Drawing.Size(99, 23);
            this.cBotaoImpJuros.TabIndex = 28;
            this.cBotaoImpJuros.Titulo = "Alterar Juros";
            this.cBotaoImpJuros.clicado += new System.EventHandler(this.cBotaoImpJuros_clicado);
            // 
            // cBotaoImpPara
            // 
            this.cBotaoImpPara.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.cBotaoImpPara.Appearance.Options.UseBackColor = true;
            this.cBotaoImpPara.Enabled = false;
            this.cBotaoImpPara.ItemComprovante = false;
            this.cBotaoImpPara.Location = new System.Drawing.Point(435, 5);
            this.cBotaoImpPara.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoImpPara.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpPara.Name = "cBotaoImpPara";
            this.cBotaoImpPara.Size = new System.Drawing.Size(99, 23);
            this.cBotaoImpPara.TabIndex = 27;
            this.cBotaoImpPara.Titulo = "Nova Data";
            this.cBotaoImpPara.clicado += new System.EventHandler(this.cBotaoImpPara_clicado);
            // 
            // cBotaoSegundaVia
            // 
            this.cBotaoSegundaVia.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.cBotaoSegundaVia.Appearance.Options.UseBackColor = true;
            this.cBotaoSegundaVia.ItemComprovante = true;
            this.cBotaoSegundaVia.Location = new System.Drawing.Point(300, 5);
            this.cBotaoSegundaVia.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoSegundaVia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoSegundaVia.Name = "cBotaoSegundaVia";
            this.cBotaoSegundaVia.Size = new System.Drawing.Size(129, 23);
            this.cBotaoSegundaVia.TabIndex = 26;
            this.cBotaoSegundaVia.Titulo = "Segunda via";
            this.cBotaoSegundaVia.clicado += new System.EventHandler(this.cBotaoImpBol3_clicado);
            // 
            // panelJuridico
            // 
            this.panelJuridico.Appearance.BackColor = System.Drawing.Color.Red;
            this.panelJuridico.Appearance.Options.UseBackColor = true;
            this.panelJuridico.Controls.Add(this.labelEscritorio);
            this.panelJuridico.Controls.Add(this.labelControl7);
            this.panelJuridico.Location = new System.Drawing.Point(608, 90);
            this.panelJuridico.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelJuridico.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelJuridico.Name = "panelJuridico";
            this.panelJuridico.Size = new System.Drawing.Size(206, 75);
            this.panelJuridico.TabIndex = 21;
            this.panelJuridico.Visible = false;
            // 
            // labelEscritorio
            // 
            this.labelEscritorio.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelEscritorio.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelEscritorio.Appearance.Options.UseFont = true;
            this.labelEscritorio.Appearance.Options.UseForeColor = true;
            this.labelEscritorio.Location = new System.Drawing.Point(5, 41);
            this.labelEscritorio.Name = "labelEscritorio";
            this.labelEscritorio.Size = new System.Drawing.Size(54, 13);
            this.labelEscritorio.TabIndex = 16;
            this.labelEscritorio.Text = "escrit�rio";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl7.Location = new System.Drawing.Point(2, 2);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(202, 57);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "JUR�DICO";
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(540, 37);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(101, 22);
            this.dateEdit1.TabIndex = 19;
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // panelControl6
            // 
            this.panelControl6.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.panelControl6.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl6.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl6.Appearance.Options.UseBackColor = true;
            this.panelControl6.Controls.Add(this.labelControl6);
            this.panelControl6.Location = new System.Drawing.Point(320, 141);
            this.panelControl6.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl6.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(91, 24);
            this.panelControl6.TabIndex = 17;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl6.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl6.Location = new System.Drawing.Point(2, 2);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(87, 20);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "VENCIDO";
            // 
            // panelControl5
            // 
            this.panelControl5.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.panelControl5.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl5.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl5.Appearance.Options.UseBackColor = true;
            this.panelControl5.Appearance.Options.UseTextOptions = true;
            this.panelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.panelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.panelControl5.Controls.Add(this.labelControl5);
            this.panelControl5.Location = new System.Drawing.Point(223, 141);
            this.panelControl5.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl5.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(91, 24);
            this.panelControl5.TabIndex = 16;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl5.Location = new System.Drawing.Point(2, 2);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(87, 20);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "CAR�NCIA";
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.Silver;
            this.panelControl4.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl4.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.Controls.Add(this.labelControl4);
            this.panelControl4.Location = new System.Drawing.Point(145, 141);
            this.panelControl4.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(72, 24);
            this.panelControl4.TabIndex = 15;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl4.Location = new System.Drawing.Point(2, 2);
            this.labelControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(68, 20);
            this.labelControl4.TabIndex = 14;
            this.labelControl4.Text = "ABERTO";
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(254)))), ((int)(((byte)(200)))));
            this.panelControl3.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl3.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Location = new System.Drawing.Point(6, 141);
            this.panelControl3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(48, 24);
            this.panelControl3.TabIndex = 14;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(2, 2);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(44, 20);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "PAGO";
            // 
            // dateEditVencimento
            // 
            this.dateEditVencimento.EditValue = null;
            this.dateEditVencimento.Enabled = false;
            this.dateEditVencimento.Location = new System.Drawing.Point(344, 113);
            this.dateEditVencimento.Name = "dateEditVencimento";
            this.dateEditVencimento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVencimento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditVencimento.Size = new System.Drawing.Size(119, 22);
            this.dateEditVencimento.TabIndex = 12;
            this.dateEditVencimento.EditValueChanged += new System.EventHandler(this.dateEditVencimento_EditValueChanged);
            // 
            // checkVencidos
            // 
            this.checkVencidos.Location = new System.Drawing.Point(257, 114);
            this.checkVencidos.Name = "checkVencidos";
            this.checkVencidos.Properties.Caption = "Vencimento";
            this.checkVencidos.Size = new System.Drawing.Size(81, 17);
            this.checkVencidos.TabIndex = 11;
            this.checkVencidos.CheckedChanged += new System.EventHandler(this.checkVencidos_CheckedChanged);
            // 
            // BotAcordo
            // 
            this.BotAcordo.Enabled = false;
            this.BotAcordo.Location = new System.Drawing.Point(540, 61);
            this.BotAcordo.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BotAcordo.Name = "BotAcordo";
            this.BotAcordo.Size = new System.Drawing.Size(101, 23);
            this.BotAcordo.TabIndex = 9;
            this.BotAcordo.Text = "Acordo";
            this.BotAcordo.Click += new System.EventHandler(this.BotAcordo_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 16);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(41, 13);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "N�mero:";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(196, 5);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(98, 23);
            this.simpleButton6.TabIndex = 5;
            this.simpleButton6.Text = "Calcular";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(3, 114);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Compet�ncia";
            this.checkEdit1.Size = new System.Drawing.Size(106, 17);
            this.checkEdit1.TabIndex = 2;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupBlocosAptos_F1.Appearance.Options.UseBackColor = true;
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.CON_selrow = null;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(5, 40);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.NivelCONOculto = 2;
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(424, 66);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 1;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = null;
            this.lookupBlocosAptos_F1.alteradoAPT += new System.EventHandler(this.lookupBlocosAptos_F1_alterado);
            this.lookupBlocosAptos_F1.alterado += new System.EventHandler(this.lookupBlocosAptos_F1_alterado);
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Enabled = false;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(122, 107);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(128, 27);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 0;
            this.cCompet1.Titulo = null;
            this.cCompet1.Click += new System.EventHandler(this.cCompet1_Click);
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "Bloco";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.OptionsColumn.ReadOnly = true;
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 1;
            this.colBLOCodigo.Width = 39;
            // 
            // cloAPTNumero
            // 
            this.cloAPTNumero.Caption = "N�mero";
            this.cloAPTNumero.FieldName = "APTNumero";
            this.cloAPTNumero.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.cloAPTNumero.Name = "cloAPTNumero";
            this.cloAPTNumero.OptionsColumn.ReadOnly = true;
            this.cloAPTNumero.Visible = true;
            this.cloAPTNumero.VisibleIndex = 2;
            this.cloAPTNumero.Width = 49;
            // 
            // colValorCorrigido
            // 
            this.colValorCorrigido.Caption = "Corrigido";
            this.colValorCorrigido.DisplayFormat.FormatString = "n2";
            this.colValorCorrigido.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorCorrigido.FieldName = "ValorCorrigido";
            this.colValorCorrigido.Name = "colValorCorrigido";
            this.colValorCorrigido.OptionsColumn.ReadOnly = true;
            this.colValorCorrigido.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorCorrigido", "{0:n2}")});
            this.colValorCorrigido.Visible = true;
            this.colValorCorrigido.VisibleIndex = 11;
            this.colValorCorrigido.Width = 72;
            // 
            // colMulta
            // 
            this.colMulta.Caption = "Multa";
            this.colMulta.DisplayFormat.FormatString = "n2";
            this.colMulta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMulta.FieldName = "Multa";
            this.colMulta.Name = "colMulta";
            this.colMulta.OptionsColumn.ReadOnly = true;
            this.colMulta.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Multa", "{0:n2}")});
            this.colMulta.Visible = true;
            this.colMulta.VisibleIndex = 12;
            this.colMulta.Width = 54;
            // 
            // colJuros
            // 
            this.colJuros.Caption = "Juros";
            this.colJuros.DisplayFormat.FormatString = "n2";
            this.colJuros.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colJuros.FieldName = "Juros";
            this.colJuros.Name = "colJuros";
            this.colJuros.OptionsColumn.ReadOnly = true;
            this.colJuros.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Juros", "{0:n2}")});
            this.colJuros.Visible = true;
            this.colJuros.VisibleIndex = 13;
            this.colJuros.Width = 55;
            // 
            // colSubTotal
            // 
            this.colSubTotal.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colSubTotal.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colSubTotal.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSubTotal.AppearanceCell.Options.UseBackColor = true;
            this.colSubTotal.AppearanceCell.Options.UseFont = true;
            this.colSubTotal.AppearanceCell.Options.UseForeColor = true;
            this.colSubTotal.Caption = "Sub Total";
            this.colSubTotal.DisplayFormat.FormatString = "n2";
            this.colSubTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSubTotal.FieldName = "SubTotal";
            this.colSubTotal.Name = "colSubTotal";
            this.colSubTotal.OptionsColumn.ReadOnly = true;
            this.colSubTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SubTotal", "{0:n2}")});
            this.colSubTotal.Visible = true;
            this.colSubTotal.VisibleIndex = 14;
            // 
            // colHonorarios
            // 
            this.colHonorarios.Caption = "Honor�rios";
            this.colHonorarios.DisplayFormat.FormatString = "n2";
            this.colHonorarios.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colHonorarios.FieldName = "Honorarios";
            this.colHonorarios.Name = "colHonorarios";
            this.colHonorarios.OptionsColumn.AllowEdit = false;
            this.colHonorarios.OptionsColumn.ReadOnly = true;
            this.colHonorarios.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Honorarios", "{0:n2}")});
            this.colHonorarios.Visible = true;
            this.colHonorarios.VisibleIndex = 15;
            // 
            // colTotal
            // 
            this.colTotal.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colTotal.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colTotal.AppearanceCell.Options.UseBackColor = true;
            this.colTotal.AppearanceCell.Options.UseFont = true;
            this.colTotal.Caption = "Total";
            this.colTotal.DisplayFormat.FormatString = "n2";
            this.colTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.ReadOnly = true;
            this.colTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:n2}")});
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 16;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "pdf";
            this.saveFileDialog1.Title = "PDF";
            // 
            // colBOLJustificativa
            // 
            this.colBOLJustificativa.Caption = "OBS";
            this.colBOLJustificativa.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colBOLJustificativa.FieldName = "BOLJustificativa";
            this.colBOLJustificativa.Name = "colBOLJustificativa";
            this.colBOLJustificativa.OptionsColumn.AllowEdit = false;
            this.colBOLJustificativa.OptionsColumn.ReadOnly = true;
            this.colBOLJustificativa.Visible = true;
            this.colBOLJustificativa.VisibleIndex = 19;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            this.repositoryItemMemoEdit1.ReadOnly = true;
            // 
            // colBOLStatus
            // 
            this.colBOLStatus.Caption = "Status";
            this.colBOLStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colBOLStatus.FieldName = "BOLStatus";
            this.colBOLStatus.Name = "colBOLStatus";
            this.colBOLStatus.OptionsColumn.AllowEdit = false;
            this.colBOLStatus.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Super";
            this.gridColumn1.ColumnEdit = this.repositoryItemLookSuper;
            this.gridColumn1.FieldName = "BOL";
            this.gridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 17;
            // 
            // repositoryItemLookSuper
            // 
            this.repositoryItemLookSuper.AutoHeight = false;
            this.repositoryItemLookSuper.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions1),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemLookSuper.DataSource = this.SuperBindingSource;
            this.repositoryItemLookSuper.DisplayMember = "SBD_SBO";
            this.repositoryItemLookSuper.Name = "repositoryItemLookSuper";
            this.repositoryItemLookSuper.ValueMember = "SBD_BOL";
            this.repositoryItemLookSuper.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemLookSuper_ButtonClick);
            // 
            // SuperBindingSource
            // 
            this.SuperBindingSource.DataMember = "SuperBoletoDetalhe";
            this.SuperBindingSource.DataSource = this.dBoletos;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowSize = false;
            this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 20;
            this.gridColumn2.Width = 30;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions2.Image = global::Boletos.Properties.Resources.Bboleto;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions2, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // timerCalcular
            // 
            this.timerCalcular.Tick += new System.EventHandler(this.timerCalcular_Tick);
            // 
            // BotCreditos
            // 
            this.BotCreditos.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.BotCreditos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions3, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.BotCreditos.Name = "BotCreditos";
            this.BotCreditos.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotCreditos.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotCreditos_ButtonClick);
            // 
            // colBOLStatusRegistro
            // 
            this.colBOLStatusRegistro.Caption = "Reg";
            this.colBOLStatusRegistro.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colBOLStatusRegistro.FieldName = "BOLStatusRegistro";
            this.colBOLStatusRegistro.Name = "colBOLStatusRegistro";
            this.colBOLStatusRegistro.Visible = true;
            this.colBOLStatusRegistro.VisibleIndex = 18;
            this.colBOLStatusRegistro.Width = 92;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // BoletosGrade
            // 
            this.Autofill = false;
            this.BindingSourcePrincipal = this.bOLetosBindingSource;
            this.BotaoExcluir = false;
            this.BotaoIncluir = false;
            this.CampoTab = "BOL";
            this.ComponenteCampos = typeof(Boletos.Boleto.cBoleto);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.dataLayout = new System.DateTime(2017, 3, 29, 0, 0, 0, 0);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "BoletosGrade";
            this.PrefixoTab = "Boleto:";
            this.Size = new System.Drawing.Size(1330, 807);
            this.Titulo = "Boletos";
            this.cargaFinal += new System.EventHandler(this.BoletosGrade_cargaFinal);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBoletos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLetosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch6M.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelJuridico)).EndInit();
            this.panelJuridico.ResumeLayout(false);
            this.panelJuridico.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVencimento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVencimento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkVencidos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookSuper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SuperBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotCreditos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bOLetosBindingSource;
        private BoletosProc.Boleto.dBoletos dBoletos;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLMulta;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPrevisto;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPago;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaAno;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaMes;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLNome;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLImpressao;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL_FPG;
        private BoletosProc.Boleto.dBoletosTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLEndereco;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private Framework.Lookup.LookupBlocosAptos_F lookupBlocosAptos_F1;
        private Framework.objetosNeon.cCompet cCompet1;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colBODMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox EditorPI;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton BotAcordo;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.DateEdit dateEditVencimento;
        private DevExpress.XtraEditors.CheckEdit checkVencidos;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn cloAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colValorCorrigido;
        private DevExpress.XtraGrid.Columns.GridColumn colMulta;
        private DevExpress.XtraGrid.Columns.GridColumn colJuros;
        private DevExpress.XtraGrid.Columns.GridColumn colSubTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colHonorarios;
        private DevExpress.XtraGrid.Columns.GridColumn colTotal;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.PanelControl panelJuridico;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoOriginal;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoSegundaVia;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpPara;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpJuros;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpRecibo;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLJustificativa;
        private DevExpress.XtraEditors.LabelControl labelEscritorio;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton BotCancelaProt;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLStatus;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private VirBotaoNeon.cBotaoImpNeon BotNadaConsta;
        private dllImpresso.Botoes.cBotaoImpBol cGeraSuper;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookSuper;
        private System.Windows.Forms.BindingSource SuperBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton cmdSeparaBoletos;
        private DevExpress.XtraEditors.SimpleButton cmdGeraRemessa;
        private DevExpress.XtraEditors.SimpleButton cmdLimpaBoletos;
        private VirBotaoNeon.cBotaoImpNeon BotExtrato;
        private DevExpress.XtraEditors.CheckEdit ch6M;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.Timer timerCalcular;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotCreditos;
        private DevExpress.XtraGrid.Columns.GridColumn colLupa;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.SimpleButton BtCorrecao;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLStatusRegistro;
        private DevExpress.XtraEditors.SimpleButton SBInquilino;
        private DevExpress.XtraEditors.SimpleButton SBProprietario;
        private DevExpress.XtraEditors.SimpleButton BCorItau;
        private DevExpress.XtraEditors.SimpleButton BCorItau1;
    }
}
