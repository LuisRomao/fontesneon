/*
03/02/2015 LH 14.2.4.14 - Reativa��o da quita��o manual
*/

using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;
using BoletosProc.Boleto;
using FrameworkProc.datasets;
using VirEnumeracoesNeon;

namespace Boletos.Boleto
{
    /// <summary>
    /// Componente visivel que representa um boleto
    /// </summary>
    /// <remarks>Contem um objeto "Boleto"</remarks>
    public partial class cBoleto : CompontesBasicos.ComponenteCamposBase
    {
        private Boleto Boleto = null;

        private void AjusteIniciais()
        {
            //if ((!CompontesBasicos.FormPrincipalBase.strEmProducao) || (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU == 30))
            //    BTeste.Visible = true;
            conTasLogicasBindingSource.DataSource = dContasLogicas.GetdContasLogicas(rowPrincipal.BOL_CON, Editavel);
            cBotaoSegundaVia.Visible = false;
            panelRodape.Visible = gridControl1.Visible = panelJurosAlterados.Visible = false;
            Boleto.VirEnumStatusBoleto.CarregaEditorDaGrid(imageStatus);
            Boleto.VirEnumStatusRegistroBoleto.CarregaEditorDaGrid(imageStatusRegistro);            
            colunaAlteracao = rowPrincipal.Table.Columns["BOLA_USU"];
            colunaDataAlteracao = rowPrincipal.Table.Columns["BOLDATAA"];
            colunaInclusao = rowPrincipal.Table.Columns["BOLI_USU"];
            colunaDataInclusao = rowPrincipal.Table.Columns["BOLDATAI"];
            bOLNomeTextEdit.Properties.ReadOnly = somenteleitura;
            imagePI.Properties.ReadOnly = somenteleitura;
            if ((!Boleto.rowPrincipal.IsBOL_APTNull()) && !Boleto.Apartamento.Alugado && Boleto.rowPrincipal.BOLProprietario)
                imagePI.Properties.ReadOnly = true;
            bOLEnderecoTextEdit.Properties.ReadOnly = somenteleitura;
            bOLVenctoDateEdit.Properties.ReadOnly = somenteleitura;
            BotDividir.Visible = (!somenteleitura && Boleto.StatusBoletoDivisivel.Contains(Boleto.BOLStatus));
            PLAnocontasbindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt;
            gridView1.OptionsBehavior.Editable = !somenteleitura;
            cCompet1.Comp.Mes = rowPrincipal.BOLCompetenciaMes;
            cCompet1.Comp.Ano = rowPrincipal.BOLCompetenciaAno;
            cCompet1.AjustaTela();
            cCompet1.ReadOnly = somenteleitura;
            BotObs.Visible = !somenteleitura;
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("EDITABOLETO") <= 1)
                gridView1.OptionsBehavior.Editable = false;

            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("EDITABOLETO") <= 2)
                simpleButtonQuita.Visible = false;
            else
                simpleButtonQuita.Visible = !rowPrincipal.IsBOLPagamentoNull();
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("EDITABOLETO") >= 3)
                BQuitar.Visible = BCancelar.Visible = true;
            dBoletos Dataset = (dBoletos)rowPrincipal.Table.DataSet;
            if (rowPrincipal.GetBoletoDEscontoRows().Length == 0)
            {
                bool original = Dataset.BoletoDEscontoTableAdapter.ClearBeforeFill;
                if (Dataset.BoletoDEscontoTableAdapter.Fill(Dataset.BoletoDEsconto, rowPrincipal.BOL) > 0)
                {
                    panelRodape.Visible = true;
                    gridControl1.Visible = true;
                }
                Dataset.BoletoDEscontoTableAdapter.ClearBeforeFill = original;
            }
            else
                gridControl1.Visible = true;
            BotAcumular.Visible = false;
            if((!rowPrincipal.BOLCancelado) && ((Boleto.BOLStatus == StatusBoleto.Parcial) || (Boleto.BOLStatus == StatusBoleto.Valido)))
            {
                BotAcumular.Visible = true;
                foreach(dBoletos.BOletoDetalheRow rowBod in rowPrincipal.GetBOletoDetalheRows())
                    if(rowBod.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho))
                        BotAcumular.Visible = false;
            }
            
            switch (Boleto.BOLStatus)
            {
                case StatusBoleto.Saldinho:
                case StatusBoleto.Acumulado:
                    if (!rowPrincipal.BOLCancelado)
                    {
                        BotAcumular.Visible = true;
                        BotAcumular.Text = "N�o Acumular";
                    }
                    break;
                case StatusBoleto.Erro:
                case StatusBoleto.Antigo:
                case StatusBoleto.Valido:
                case StatusBoleto.OriginalDividido:
                case StatusBoleto.Parcial:                
                    cBotaoSegundaVia.Visible = !rowPrincipal.BOLCancelado;
                    if (!rowPrincipal.BOLCancelado && !rowPrincipal.IsBOLValorAcordadoNull() && !rowPrincipal.IsBOLDataLimiteJustNull())
                    {
                        panelRodape.Visible = panelJurosAlterados.Visible = true;
                        labelValAc.Text = string.Format("Valor de {0:n2} para pagamento at� {1:dd/MM/yyyy}",rowPrincipal.BOLValorAcordado,rowPrincipal.BOLDataLimiteJust);
                    }
                    break;
                case StatusBoleto.Cancelado:
                    BQuitar.Visible = BotDividir.Visible = BCancelar.Visible = simpleButtonQuita.Visible = false;
                    break;
                case StatusBoleto.OriginalAcordo:
                case StatusBoleto.ParcialAcordo:
                    BQuitar.Visible = BotDividir.Visible = BCancelar.Visible = simpleButtonQuita.Visible = false;
                    break;
                case StatusBoleto.Acordo:
                    BotDividir.Visible = BCancelar.Visible = false;
                    cBotaoSegundaVia.Visible = true;
                    break;
                default:
                    break;
            }
            if (!Boleto.rowPrincipal.IsBOLPagamentoNull())
            {
                BQuitar.Visible = BotDividir.Visible = BCancelar.Visible = false;
            }
            if (Boleto.rowSBO != null)
            {
                cCompet1.Visible = false;
                gridView1.OptionsBehavior.Editable = false;
                simpleButtonQuita.Visible = false;
                BQuitar.Visible = BCancelar.Visible = false;                
                gridControl1.Visible = false;
            }
            if (Boleto.rowPrincipal.BOLTipoCRAI == "E")
            {
                BotDividir.Visible = simpleButtonQuita.Visible = false;
            }
        }


        /// <summary>
        /// Consturtor
        /// </summary>
        /// <param name="rowBoleto">Linha mae</param>
        /// <param name="SomenteLeitura">Somente leitura</param>
        public cBoleto(dBoletos.BOLetosRow rowBoleto, bool SomenteLeitura)
        {
            InitializeComponent();
            somenteleitura = SomenteLeitura;
            SetaBoleto(rowBoleto);
            AjusteIniciais();            

        }

        /// <summary>
        /// Consturtor
        /// </summary>
        /// <param name="_Boleto">Boleto</param>
        /// <param name="SomenteLeitura"></param>
        public cBoleto(Boleto _Boleto, bool SomenteLeitura)
        {
            InitializeComponent();
            somenteleitura = SomenteLeitura;            
            Boleto = _Boleto;
            rowPrincipal = Boleto.rowPrincipal;
            bOLetosBindingSource.DataSource = rowPrincipal.Table;            
            AjusteIniciais();

        }

        private dBoletos.BOLetosRow rowPrincipal;

        private void SetaBoleto(dBoletos.BOLetosRow rowBoleto) {
            rowPrincipal = rowBoleto;
            Boleto = new Boleto(rowBoleto);
            bOLetosBindingSource.DataSource = rowBoleto.Table;
            bOLetosBindingSource.Position = bOLetosBindingSource.Find("BOL", rowBoleto.BOL);            
        }

        /// <summary>
        /// Construtor padr�o 
        /// </summary>
        /// <remarks>� usado para a chamada generica do quadro de inadimplencia</remarks>
        public cBoleto()
        {
            InitializeComponent();
             
        }

        /// <summary>
        /// Chamada Generica - QuadroInad
        /// </summary>
        /// <param name="args">10 parametros</param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            if ((args.Length > 0) && (args[0] is string) && ((string)args[0] == "QuadroInad"))
            {
                int CON = (int)args[1];
                int RefData = (int)args[2];
                int Carencia = (int)args[3];
                int Agrupamento = (int)args[4];
                bool ValorOriginal = (bool)args[5];
                bool ValorCorrigido = (bool)args[6];
                bool NumeroDeUnidades = (bool)args[7];
                int DestacarUnidadesInadDoMes = (int)args[8];
                DateTime DataBalancete = (DateTime)args[9];
                //string CODCON = (string)args[10];
                //Boletos.Boleto.Impresso.dImpBoletoBal DataSet = new Boletos.Boleto.Impresso.dImpBoletoBal();
                Framework.objetosNeon.Competencia Compet = new Framework.objetosNeon.Competencia(DateTime.Today, CON);
                Compet.Add(-1);
                Boletos.Boleto.Impresso.RetornoInad Ret = Boletos.Boleto.Impresso.RetornoInad.CalculaUnidadesInadimplenes(RefData, Carencia, Agrupamento, ValorOriginal, 
                                                              ValorCorrigido, NumeroDeUnidades, DestacarUnidadesInadDoMes, 100, DataBalancete, CON, Compet);
                if (Ret != null)
                    return new object[] { Ret.Titulo, Ret.Conteudo };
                else
                    return new object[] { "", "" };
            }
            else
                return base.ChamadaGenerica(args);
        }


        private bool Alterado = false;

        /// <summary>
        /// Comanda a grava��o das alteracoes
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            BoletosGrade.RegistraAlteracao(Boleto.APT.GetValueOrDefault(0));
            bOLetosBindingSource.EndEdit();
            if (!Validate(true))
                return false;            
            if (VirDB.VirtualTableAdapter.EfetivamenteAlterados(rowPrincipal))
                Alterado = true;
            decimal Total = 0;
            foreach (dBoletos.BOletoDetalheRow BODRow in rowPrincipal.GetBOletoDetalheRows())
            {
                if (BODRow.RowState != DataRowState.Deleted)
                    Total += BODRow.BODValor;
                if (VirDB.VirtualTableAdapter.EfetivamenteAlterados(BODRow))
                    Alterado = true;
            }

            if (!Alterado)
                return true;
            else
                if (base.Update_F())
                {
                    string Justivicativa = "";
                    if (!VirInput.Input.Execute("Justificativa", ref Justivicativa,true))
                        return false;
                    
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("cBoleto Update_F - 236");
                        if (Boleto.SalvarAlteracoes(Justivicativa))
                        {
                            if (Total == 0)
                                Boleto.Baixa("Baixa de boleto zerado", DateTime.Today, 0, 0);                            
                        }
                        else
                        {
                            throw new Exception("Erro ao gravar.\r\nAltera��o cancelada\r\nErro:" + Boleto.UltimoErro);                            
                        }
                        VirMSSQL.TableAdapter.CommitSQL();
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(ex);
                    }
                    return true;
                }
                else
                    return false;            
            
        }



        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

            dBoletos.BOletoDetalheRow rowDeletar = (dBoletos.BOletoDetalheRow)gridView1.GetDataRow(gridView1.FocusedRowHandle);
            if (rowDeletar != null)
            {
                if (rowDeletar.IsBODAcordo_BOLNull())
                {
                    if (rowDeletar.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD))
                        rowDeletar.SetBOD_BOLNull();
                    else
                        rowDeletar.Delete();
                    Alterado = true;
                    Editando();
                }
                else
                    if (rowDeletar.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho))
                {
                    string pergunta = string.Format("Confirma a exclus�o do boleto {0} deste boleto", rowDeletar.BODAcordo_BOL);
                    if (MessageBox.Show(pergunta, "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("cBoleto repositoryItemButtonEdit1_ButtonClick - 258");
                            Boleto BolSaldinho = new Boleto(rowDeletar.BODAcordo_BOL);
                            BolSaldinho.rowPrincipal.BOLCancelado = false;
                            BolSaldinho.GravaJustificativa(string.Format("Removido do boleto {0} e restaurado", Boleto.BOL), "");
                            rowDeletar.Delete();
                            Boleto.SalvarAlteracoes(string.Format("Boleto {0} removido", BolSaldinho.BOL));
                            VirMSSQL.TableAdapter.CommitSQL();
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.VircatchSQL(ex);
                        }
                    }
                }
                else
                    MessageBox.Show("Este item n�o pode ser removido");
            }


        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            dBoletos.BOletoDetalheRow BODrow = (dBoletos.BOletoDetalheRow)DRV.Row;
            BODrow.BOD_APT = rowPrincipal.BOL_APT;
            BODrow.BODComCondominio = (rowPrincipal.BOLTipoCRAI == "C");
            BODrow.BODCompetencia = rowPrincipal.BOLCompetenciaAno * 100 + rowPrincipal.BOLCompetenciaMes;            
            BODrow.BODData = rowPrincipal.BOLVencto;
            BODrow.BOD_CON = rowPrincipal.BOL_CON;
            BODrow.BODPrevisto = true;
            BODrow.BODProprietario = rowPrincipal.BOLProprietario;
            Editando();
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            if (rowPrincipal.BOLCompetenciaMes != cCompet1.Comp.Mes)
                rowPrincipal.BOLCompetenciaMes = (Int16)cCompet1.Comp.Mes;
            if (rowPrincipal.BOLCompetenciaMes != cCompet1.Comp.Mes)
                rowPrincipal.BOLCompetenciaAno = (Int16)cCompet1.Comp.Ano;
            Editando();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string Justificativa = "";
            if (!VirInput.Input.Execute("Observa��o", ref Justificativa, true))
                    return;

            //string Just = string.Format("\r\n{0} - Observa��o feita por {1}\r\nObs:{2}", 
            //                             DateTime.Now,
            //                             CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
            //                             Justificativa);
            Boleto.GravaJustificativa(Justificativa,"");
            //memoEdit1.EditValue += Just;
            //bOLetosBindingSource.EndEdit();
            //Editando();
        }

        private void AjustarBOLMensagen(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit Chamador = (DevExpress.XtraEditors.LookUpEdit)sender;
            string PLA = Chamador.EditValue.ToString();
            gridView1.SetFocusedRowCellValue(colBODMensagem, Framework.datasets.dPLAnocontas.dPLAnocontasSt.GetDescricao(PLA));
        }

        private void simpleButtonQuita_Click(object sender, EventArgs e)
        {
            if (Boleto.rowPrincipal.BOLTipoCRAI == "E")
                return;
            string Justificativa = "";
            if (!VirInput.Input.Execute("Justificativa", ref Justificativa, true))
                return;
            if (Boleto.CancelaBaixa(Justificativa))
                FechaTela(DialogResult.OK);
            else
                MessageBox.Show(string.Format("Erro no cancelamento da quita��o:\r\n{0}",Boleto.UltimoErro));
            /*
            rowPrincipal.SetBOLPagamentoNull();
            rowPrincipal.SetBOLValorPagoNull();
            if (Boleto.SalvarAlteracoes("Cancelamento da quita��o"))
                FechaTela(DialogResult.OK);
            */
        }                

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (Boleto.rowPrincipal.BOLTipoCRAI == "E")
                return;
            string Justificativa = "";
            if (!VirInput.Input.Execute("Justificativa", ref Justificativa, true))
                return;
            cDividir NovoDivisor = new cDividir();
            if (NovoDivisor.Iniciar(Boleto))
                if(NovoDivisor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    string Just = string.Format("Boleto dividido manualmente por {0}:\r\n {1}", CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome, Justificativa);                 
                    Boleto[] B = Boleto.Dividir(Just,NovoDivisor.ItensB1(), (bool)NovoDivisor.imageComboBoxEdit1.EditValue, (bool)NovoDivisor.imageComboBoxEdit2.EditValue);
                    /*
                    //string Justdet;
                    //string Justdet1;
                    Justdet = Justdet1 = "Boleto gerado por divis�o feita por " + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome + "\r\nJustificativa:" + Justificativa;
                    Just += string.Format    ("\r\n       -> Original :{0} - {1}", rowPrincipal.BOL, rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                    Justdet += string.Format ("\r\n          Original :{0} - {1}", rowPrincipal.BOL, rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                    Justdet1 += string.Format("\r\n          Original :{0} - {1}", rowPrincipal.BOL, rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                    for (int i = 0; i < 2; i++)
                    {
                        Just += string.Format    ("\r\n          Parcela {0}:{1} - {2}", i + 1, B[i].rowPrincipal.BOL, B[i].rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                        Justdet += string.Format ("\r\n      {0}  Parcela {1}:{2} - {3}", (i == 0) ? "->":"  "  ,i + 1, B[i].rowPrincipal.BOL, B[i].rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                        Justdet1 += string.Format("\r\n      {0}  Parcela {1}:{2} - {3}", (i == 1) ? "->" : "  ", i + 1, B[i].rowPrincipal.BOL, B[i].rowPrincipal.BOLValorPrevisto.ToString("n2").PadLeft(9));
                    }
                    B[0].GravaJustificativa(Justdet, "");
                    B[1].GravaJustificativa(Justdet1, "");
                    */
                    bOLetosBindingSource.EndEdit();
                    Boleto.SalvarAlteracoes(Just);
                    FechaTela(DialogResult.OK);
                };            
        }

        private void imagePI_EditValueChanged(object sender, EventArgs e)
        {
            Editando();
        }

        /// <summary>
        /// Informa se o boleto foi editado
        /// </summary>
        public bool BoletoEditado = false;

        private void Editando() 
        {
            if (!TravaEditando)
            {
                BoletoEditado = true;
                btnConfirmar_F.Enabled = true;
                BotDividir.Enabled = false;
                BotObs.Visible = false;
                BQuitar.Enabled = false;
                BCancelar.Enabled = false;
                simpleButtonQuita.Enabled = false;
            }
        }

        private bool TravaEditando = true;

        private void cBoleto_Load(object sender, EventArgs e)
        {
            TravaEditando = false;
        }

        private void bOLNomeTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            Editando();
        }

        private void bOLEnderecoTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            Editando();
        }

        private void bOLVenctoDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            Editando();
        }

        /// <summary>
        /// Cancela_F
        /// </summary>
        /// <param name="TesteCanClose"></param>
        /// <returns></returns>
        public override bool Cancela_F(bool TesteCanClose)
        {
            if (BoletoEditado)
                return base.Cancela_F(TesteCanClose);
            else
            {
                FechaTela(DialogResult.Cancel);
                return true;
            }
            
        }

        private enum Tarefa { CreditoAnterior, Caixinha, Cancelar };       

        private void BQuitar_Click(object sender, EventArgs e)
        {
            if ((Boleto.TipoCRAIE == BOLTipoCRAI.Extra) && (CompontesBasicos.FormPrincipalBase.USULogado != 30))
                return;
            DateTime? DataBaixa = DateTime.Today;
            if (Boleto.ValorPrevisto == 0)
            {
                if (!VirInput.Input.Execute("Data da baixa", ref DataBaixa, null, DateTime.Today))
                    return;
                Boleto.Baixa("Boleto zerado", DataBaixa.Value, 0, 0);
            }
            else
            {
                SortedList Alternativas = new SortedList();
                Alternativas.Add(Tarefa.CreditoAnterior, "Quitar boleto com cr�dito de balancete anterior.");
                Alternativas.Add(Tarefa.Caixinha, "Quitar boleto com caixinha.");
                Alternativas.Add(Tarefa.Cancelar, "Cancelar.");
                object oResposta = Tarefa.Cancelar;

                VirInput.Input.Execute("O que voc� quer fazer ?", Alternativas, out oResposta, VirInput.Formularios.cRadioCh.TipoSelecao.radio);
                Tarefa Resposta = (Tarefa)(oResposta ?? Tarefa.Cancelar);

                switch (Resposta)
                {
                    case Tarefa.CreditoAnterior:
                        if (!VirInput.Input.Execute("Data da baixa", ref DataBaixa, null, DateTime.Today))
                            return;
                        string Justificativa = "";
                        if (!VirInput.Input.Execute("Identifica��o do cr�dito:", ref Justificativa, false, 50))
                            return;
                        try
                        {
                            Boleto.QuitarComCredAnterior = Justificativa;
                            int BOLreserva = rowPrincipal.BOL;
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos cBoleto - 520");
                            //if (Boleto._expotaBalanceteAccess)
                            //    VirOleDb.TableAdapter.AbreTrasacaoOLEDB("Boletos cBoleto - 520");
                            if (Boleto.rowPrincipal.IsBOLPagamentoNull() && Boleto.ComSeguro())
                                Boleto.AjustaSeguro(DataBaixa.Value);
                            bool ok = true;
                            if (!Boleto.rowPrincipal.BOLCancelado)
                            {
                                if (Boleto.Baixa("Cr�dito balancete anterior", DataBaixa.Value, 0, -Boleto.ValorPrevisto, null, null, null))
                                {
                                    //Carregar o boleto novamente
                                    Boleto = new Boleto(BOLreserva);
                                    if (!Boleto.Encontrado)
                                        throw new Exception("Erro na opera��o. Quita��o n�o efetuada - Boleto n�o encontrdo");
                                    Boleto.GravaJustificativa(string.Format("Cr�dito balancete anterior feita por {0}\r\nJustificativa:\r\n{1}\r\n", CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome, Justificativa), "");

                                }
                                else
                                {
                                    //MessageBox.Show("Erro enviado:" + Boleto.UltimoErro);
                                    throw Boleto.UltimaException;
                                    //    VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(Boleto.UltimaException);                        
                                    //Res = DialogResult.Cancel;
                                }
                            }
                            else
                                ok = false;
                            //geraErro = geraErro.Substring(5, 5);
                            VirMSSQL.TableAdapter.STTableAdapter.Commit();
                            

                            MessageBox.Show(ok ? "Ok" : "Boleto cancelado");
                        }
                        catch (Exception ex)
                        {
                            
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                            throw new Exception("Erro na opera��o. Quita��o n�o efetuada", ex);
                        };
                        FechaTela(DialogResult.OK);
                        break;
                    case Tarefa.Caixinha:
                        return;
                    case Tarefa.Cancelar:
                        if (CompontesBasicos.FormPrincipalBase.USULogado == 30)
                        {
                                //if (Boleto.rowPrincipal.IsBOLPagamentoNull() && Boleto.ComSeguro())
                                //    Boleto.AjustaSeguro(new DateTime(2013,1,5));
                            if (MessageBox.Show("Esta opera��o ir� criar lan�amentos no balancete! Confirma?", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                            {
                                //Travar na data do balancete fechado
                                decimal ValorPago = Boleto.rowPrincipal.BOLValorPrevisto;
                                if (!VirInput.Input.Execute("Valor pago", ref ValorPago))
                                    return;
                                Boleto.Baixa("Teste", DateTime.Today, ValorPago, 0);
                            }

                    
                                }
                        return;
                    default:
                        throw new NotImplementedException(string.Format("Tarefa n�o implementada: {0}", Resposta));
                }

                
            }
        }

        private void BCancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma o CANCELAMENTO definitivo deste boleto ?", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                DialogResult Res = DialogResult.OK;
                string Justivicativa = "";
                if (!VirInput.Input.Execute("Justificativa", ref Justivicativa,true))
                    return;                                
                Boleto.BOLStatus = StatusBoleto.Cancelado;
                Boleto.rowPrincipal.BOLCancelado = true;
                Boleto.SalvarAlteracoes("Boleto CANCELADO: " + Justivicativa);
                //Boleto.Cancelar();
                FechaTela(Res);
            }
        }

        private void cBotaoSegundaVia_clicado(object sender, EventArgs e)
        {
            Boleto.Imprimir(cBotaoSegundaVia.BotaoClicado);
        }

     

      

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dBoletos.BOletoDetalheRow row = (dBoletos.BOletoDetalheRow)gridView1.GetDataRow(e.FocusedRowHandle);
            if (row == null)
                return;
            System.Collections.Generic.List<string> NaoEditaveis = new System.Collections.Generic.List<string>(new string[] {PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD),
                                                                                                                             PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho)});
            gridColumnPLADesc.OptionsColumn.ReadOnly = colBOD_PLA.OptionsColumn.ReadOnly = colBODValor.OptionsColumn.ReadOnly = (NaoEditaveis.Contains(row.BOD_PLA));
           
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dBoletos.BOletoDetalheRow row = (dBoletos.BOletoDetalheRow)gridView1.GetDataRow(e.RowHandle);            

            if ((row == null) || (row.RowState == DataRowState.Detached))
                return;            

            if (row.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD))
            {                
                e.Appearance.BackColor = System.Drawing.Color.Aqua;
                e.Appearance.ForeColor = System.Drawing.Color.Black;
                e.HighPriority = true;
            }
            else if (row.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.Saldinho))
            {
                e.Appearance.BackColor = System.Drawing.Color.DarkGoldenrod;
                e.Appearance.ForeColor = System.Drawing.Color.Black;
                e.HighPriority = true;
            }

            
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            Editando();
        }

        private void BotAcumular_Click(object sender, EventArgs e)
        {
            if ((Boleto.BOLStatus == StatusBoleto.Saldinho) || (Boleto.BOLStatus == StatusBoleto.Acumulado))
            {
                if (MessageBox.Show("Confirma que este boleto N�O ser� cobrado junto com o pr�ximo?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    string Justivicativa = "";
                    if (!VirInput.Input.Execute("Justificativa", ref Justivicativa, true))
                        return;
                    Boleto.BOLStatus = ((Boleto.BOLStatus==StatusBoleto.Acumulado) ? StatusBoleto.Valido : StatusBoleto.Parcial);
                    Boleto.SalvarAlteracoes(string.Format("O boleto N�O vai mais ser cobrado junto com o pr�ximo\r\n{0}", Justivicativa));
                    FechaTela(DialogResult.Yes);
                }
            }
            else
                if (MessageBox.Show("Confirma que este boleto deve ser cobrado junto com o pr�ximo?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    string Justivicativa = "";
                    if (!VirInput.Input.Execute("Justificativa", ref Justivicativa, true))
                        return;
                    Boleto.BOLStatus = ((Boleto.BOLStatus == StatusBoleto.Valido) ? StatusBoleto.Acumulado : StatusBoleto.Saldinho); 
                    Boleto.SalvarAlteracoes(string.Format("O boleto vai ser cobrado junto com o pr�ximo\r\n{0}", Justivicativa));
                    FechaTela(DialogResult.Yes);
                }
        }

        private void PlalookUpEdit2_PopupFilter(object sender, DevExpress.XtraEditors.Controls.PopupFilterEventArgs e)
        {
            e.Criteria = DevExpress.Data.Filtering.CriteriaOperator.Parse("PLA != '111000'");            
        }
    }
}

