/*
LH - 09/05/2014       - 13.2.8.47 - S� recalcular se for uma unidade
LH - 07/07/2014       - 13.2.9.24 - cor diferenciada para E e H
MR - 25/02/2016 11:00 -           - Arquivo de Cobran�a Itau (Altera��es indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
MR - 15/03/2016 10:00 -           - Limpeza de C�digo (Altera��es indicadas por *** MRC - INICIO - REMESSA (15/03/2016 10:00) ***)
MR - 21/03/2016 20:00 -           - C�digo de Empresa para remessa Bradesco diferenciado entre SA e SBC (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
MR - 01/04/2016 19:00 -           - Corre��o na gera��o de arquivo remessa itau incluindo a finaliza��o do arquivo que para o itau � feito com fun��o espec�fica (Altera��es indicadas por *** MRC - INICIO (01/04/2016 19:00) ***)
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using dllImpresso;
using EDIBoletos;
using AbstratosNeon;
using BoletosProc.Boleto;
using CompontesBasicosProc;
using dllCNAB;
using CompontesBasicos;
using VirEnumeracoes;
using VirEnumeracoesNeon;

namespace Boletos.Boleto
{
    /// <summary>
    /// Grade de boletos
    /// </summary>
    public partial class BoletosGrade : CompontesBasicos.ComponenteGradeNavegadorPesquisa
    {

        private bool editarJuros;
        private List<Boleto> _BoletosRemessa;
        private List<Boleto> BoletosRemessa
        {
            get
            {
                if (_BoletosRemessa == null)
                    _BoletosRemessa = new List<Boleto>();
                return _BoletosRemessa;
            }
        }


        /// <summary>
        /// Construtor principal
        /// </summary>
        public BoletosGrade()
        {
            InitializeComponent();

            if (!DesignMode)
            {
                panelControl1.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") > 0);
                simpleButton3.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") > 0);
                BotCancelaProt.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("PROTESTO") > 2);
                BtnImprimir_F.Enabled = true;
                BtnImprimir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                Boleto.VirEnumStatusBoleto.CarregaEditorDaGrid(colBOLStatus);
                Boleto.VirEnumStatusRegistroBoleto.CarregaEditorDaGrid(colBOLStatusRegistro);
                simpleButton7.Visible = true;
                // Setagem manual porque o do desiger n�o est� sendo respeitado!!
                GridView_F.OptionsSelection.InvertSelection = false;
                //GridView_F.Columns["DX$CheckboxSelectorColumn"].Width = 15;
                DevExpress.XtraGrid.Columns.GridColumn column;
                System.Reflection.FieldInfo myPropertyInfo1 = GridView_F.GetType().GetField("checkboxSelectorColumn", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                if (myPropertyInfo1 != null)
                {
                    column = myPropertyInfo1.GetValue(GridView_F) as DevExpress.XtraGrid.Columns.GridColumn;
                    if (column != null)
                        column.Width = 50;// 30 � pequento    300 � grande 50?
                }
            }
            GridView_F.OptionsBehavior.Editable = true;
            if (Framework.DSCentral.USU == 30)
            {
                BtCorrecao.Visible = true;
                BCorItau.Visible = true;
                BCorItau1.Visible = true;
            }
        }

        DateTime DataReferencia
        {
            get
            {
                if (checkVencidos.Checked)
                    return dateEditVencimento.DateTime;
                else
                    return DateTime.Today;
            }
        }


        /// <summary>
        /// Abre o boleto
        /// </summary>
        protected override void Alterar()
        {
            bool SLeitura;
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("EDITABOLETO") <= 0)
                SLeitura = true;
            else
                SLeitura = false;
            AbrecBoleto(SLeitura);
        }

        /// <summary>
        /// Abre o boleto
        /// </summary>
        protected override void Visualizar()
        {
            AbrecBoleto(true);
        }

        private void AbrecBoleto(bool SLeitura)
        {
            if (LinhaMae != null)
            {
                if (!LinhaMae.IsBOLPagamentoNull())
                    SLeitura = true;
                if (!Boleto.StatusBoletoEditavel.Contains((StatusBoleto)LinhaMae.BOLStatus))
                    SLeitura = true;
                using (cBoleto cBoleto = new cBoleto(LinhaMae, SLeitura))
                {
                    if ((cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK) || (cBoleto.BoletoEditado))
                    {
                        int BOLAnteriro = LinhaMae.BOL;
                        Calcular();
                        int novaPos = bOLetosBindingSource.Find("BOL", BOLAnteriro);
                        if (novaPos != -1)
                            bOLetosBindingSource.Position = novaPos;
                    }
                }

            };
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ImpProtocolo Protocolo = ImpProtocolo.ImpProtocoloSt;
            Protocolo.SetNome(lookupBlocosAptos_F1.CON_selrow.CONNome);
            Protocolo.dImpProtocolo.DadosProt.Clear();
            if (GridView_F.GetSelectedRows().Length > 0)
                foreach (int i in GridView_F.GetSelectedRows())
                {
                    DataRowView DRV = (DataRowView)GridView_F.GetRow(i);
                    Boleto BoletosImp = new Boleto((dBoletos.BOLetosRow)DRV.Row);

                    Protocolo.dImpProtocolo.DadosProt.AddDadosProtRow(BoletosImp.Apartamento.APTNumero,
                                                                      BoletosImp.rowPrincipal.BOLNome,
                                                                      BoletosImp.rowPrincipal.BOLEndereco,
                                                                      BoletosImp.Apartamento.BLOCodigo,
                                                                      BoletosImp.rowPrincipal.BOLProprietario ? "P" : "I",
                                                                      "",
                                                                      BoletosImp.Apartamento.BLONome,
                                                                      (int)BoletosImp.FormaPagamento,
                                                                      BoletosImp.DebitoAutomaticoCadastrado() ? 237 : 0);
                }
            else
                if (LinhaMae != null)
            {
                Boleto BoletosImp = new Boleto(LinhaMae);
                Protocolo.dImpProtocolo.DadosProt.AddDadosProtRow(BoletosImp.Apartamento.APTNumero,
                                                                  BoletosImp.rowPrincipal.BOLNome,
                                                                  BoletosImp.rowPrincipal.BOLEndereco,
                                                                  BoletosImp.Apartamento.BLOCodigo,
                                                                  BoletosImp.rowPrincipal.BOLProprietario ? "P" : "I",
                                                                  "",
                                                                  BoletosImp.Apartamento.BLONome,
                                                                  (int)BoletosImp.FormaPagamento,
                                                                  BoletosImp.DebitoAutomaticoCadastrado() ? 237 : 0);
            };
            Protocolo.bindingSource1.Sort = "BLOCO,APTNumero";
            if (checkEdit1.Checked)
                Protocolo.SetDocumento("Boleto compet�ncia " + cCompet1.Comp);
            else
                Protocolo.SetDocumento("Boletos");
            Protocolo.Apontamento(lookupBlocosAptos_F1.CON_sel, 1);
            Protocolo.CreateDocument();
            Protocolo.Apontamento(-1, -1);
            Protocolo.Print();

        }

        private void lookupBlocosAptos_F1_alterado(object sender, EventArgs e)
        {
            if (!blocloock)
                Fecha();
        }

        private string Emailjuridico = "";

        private void EstaNoJuridico()
        {
            bool NoJuridico;
            Emailjuridico = "";
            //verifica no access
            /*
            const string ComandoVerificaJuridico = "SELECT Status, Data, Cobrar, Observacoes FROM Cobran�a WHERE (Codcon = @P1) AND (Bloco = @P2) AND (Apartamento = @P3)";
            string ComandoH =
"SELECT     Codcon, Bloco, Apartamento, [Data do Registro], Hist�rico\r\n" +
"FROM         [Cobran�a - Hist�rico]\r\n" +
"WHERE     (Codcon = @P1) AND (Bloco = @P2) AND (Apartamento = @P3)\r\n" +
"ORDER BY Apartamento, [Data do Registro];";
            */
            //Framework.objetosNeon.Apartamento Ap = new Framework.objetosNeon.Apartamento(lookupBlocosAptos_F1.APT_Sel);
            ABS_Apartamento Ap = ABS_Apartamento.GetApartamento(lookupBlocosAptos_F1.APT_Sel);
            if (Ap.StatusCobranca == APTStatusCobranca.juridico)
            {
                NoJuridico = true;
                CobrancaProc.LinhaCobrancaProc LinhaCobrancaProc1 = new CobrancaProc.LinhaCobrancaProc(Ap,true);                
                string MemCob = LinhaCobrancaProc1.RelatorioLog;
                MessageBox.Show(MemCob, "Observa��o da cobran�a");                
                /*
                try
                {
                    //DataRow rowCobranca = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoVerificaJuridico, Ap.CONCodigo, Ap.BLOCodigo, Ap.APTNumero);
                    //if (rowCobranca != null)
                    //{
                    //    if (rowCobranca["Observacoes"] != DBNull.Value)
                    //    {
                    //        DataTable Historico = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLTabela(ComandoH, Ap.CONCodigo, Ap.BLOCodigo, Ap.APTNumero);
                    //        foreach (DataRow rowH in Historico.Rows)
                    //            MemCob += ((DateTime)rowH["Data do Registro"]).ToString("dd/MM/yyyy") + " - " + rowH["Hist�rico"].ToString() + "\r\n";
                    //        MemCob += "\r\n\r\n" + rowCobranca["Observacoes"].ToString() + "\r\n\r\nStatus: " + rowCobranca["Status"].ToString();
                    //        
                    //    }
                    //}
                    //MessageBox.Show(MemCob, "Observa��o da cobran�a");
                }
                catch (System.Data.OleDb.OleDbException)
                {
                }
                catch (Exception e)
                {
                    throw (e);
                }*/
            }
            else
                NoJuridico = false;
            //bool NoJuridico = VirOleDb.TableAdapter.STTableAdapter.EstaCadastrado(ComandoVerificaJuridico, Ap.CONCodigo, Ap.BLOCodigo, Ap.APTNumero);
            panelJuridico.Visible = NoJuridico;

            string ComandoAPT = "SELECT FRNFone1, FRNEmail, FRNFantasia, FRNNome\r\n" +
                                "FROM AParTamentos INNER JOIN FORNECEDORES ON APTJur_FRN = FORNECEDORES.FRN\r\n" +
                                "where (APT = @P1)";

            string ComandoACO = "SELECT FRNFone1, FRNEmail, FRNFantasia, FRNNome\r\n" +
                                "FROM ACOrdos INNER JOIN FORNECEDORES ON ACOADV_FRN = FRN \r\n" +
                                "WHERE (ACO_APT = @P1) and (ACOStatus = 1)";
            string Comando = "SELECT FRNFone1, FRNEmail, FRNFantasia, FRNNome\r\n" +
                             "FROM CONDOMINIOS INNER JOIN FORNECEDORES ON CONDOMINIOS.CONEscrit_FRN = FORNECEDORES.FRN\r\n" +
                             "where (CON = @P1)";

            DataRow rowFRN;
            if (NoJuridico)
            {
                try
                {
                    rowFRN = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoAPT, lookupBlocosAptos_F1.APT_Sel);
                    if (rowFRN == null)
                        rowFRN = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoACO, lookupBlocosAptos_F1.APT_Sel);
                    if (rowFRN == null)
                        rowFRN = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(Comando, lookupBlocosAptos_F1.CON_sel);

                    if (rowFRN == null)
                        labelEscritorio.Text = "";
                    else
                    {
                        string nome = "";
                        string fone = "";
                        if (rowFRN["FRNEmail"] != DBNull.Value)
                            Emailjuridico = rowFRN["FRNEmail"].ToString();
                        if (rowFRN["FRNFantasia"] != DBNull.Value)
                            nome = rowFRN["FRNFantasia"].ToString();
                        if (nome == "")
                            if (rowFRN["FRNNome"] != DBNull.Value)
                                nome = rowFRN["FRNNome"].ToString();
                        if (rowFRN["FRNFone1"] != DBNull.Value)
                            fone = rowFRN["FRNFone1"].ToString();
                        labelEscritorio.Text = nome + " - " + fone;
                    }
                }
                catch (System.Data.OleDb.OleDbException)
                {                    
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }
        }

        private bool blocloock = false;

        private string BuscaInternet =
"SELECT codcon, bloco, apartamento, data, juridico, judicial, RetNovosB.vencto, RetNovosB.valor, \r\n" +
"                      RetNovosB.Honorarios\r\n" +
"FROM         RetNovosB INNER JOIN\r\n" +
"                      RetAcordo ON RetNovosB.Acordo = RetAcordo.id\r\n" +
"WHERE     (RetNovosB.nn = @P1);";

        private bool ReentradaCalcular = false;

        private bool NadaConstaAtivo()
        {
            bool temProblema = false;
            foreach (dBoletos.BOLetosRow rowBOL in dBoletos.BOLetos)
            {
                if ((rowBOL.BOLTipoCRAI == "A") && (rowBOL.IsBOLPagamentoNull()))
                {
                    temProblema = true;
                    break;
                };
                if ((rowBOL.BOLVencto <= DateTime.Today) && rowBOL.IsBOLPagamentoNull())
                {
                    temProblema = true;
                    break;
                }
            }
            return !temProblema;
        }

        private void SetarCPF(bool P, int PES, string Nome)
        {
            string Texto = string.Format("O {0} '{1}' n�o tem o CPF cadastrardo. Cadastrar agora?",P ? "propriet�rio":"inquilino",Nome);
            if (MessageBox.Show(Texto, "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                DocBacarios.CPFCNPJ CPF_CNPJ = null;
                while ((CPF_CNPJ == null) || (CPF_CNPJ.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO))
                {
                    long CPF = 0;
                    if (VirInput.Input.Execute("CPF", out CPF))                    
                        CPF_CNPJ = new DocBacarios.CPFCNPJ(CPF);                    
                    else
                        break;
                }
                if ((CPF_CNPJ != null) && (CPF_CNPJ.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO))
                {
                    string comandoUpdate = "update PESSOAS set PESCpfCnpj = @P1 where PES = @P2";
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoUpdate, CPF_CNPJ.ToString(), PES);
                }
            }
        }

        private void ChecarCPF(int APT)
        {
            string comandoBusca =
"SELECT        APARTAMENTOS.APTProprietario_PES, APARTAMENTOS.APTInquilino_PES, PESSOAS.PESCpfCnpj AS CPFP, PESSOAS_1.PESCpfCnpj AS CPFI,PESSOAS.PESNome as PESNomeP,PESSOAS_1.PESNome as PESNomeI\r\n" +
"FROM            APARTAMENTOS LEFT OUTER JOIN\r\n" +
"                         PESSOAS AS PESSOAS_1 ON APARTAMENTOS.APTInquilino_PES = PESSOAS_1.PES LEFT OUTER JOIN\r\n" +
"                         PESSOAS ON APARTAMENTOS.APTProprietario_PES = PESSOAS.PES\r\n" +
"WHERE        (APARTAMENTOS.APT = @P1);";
            DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(comandoBusca, APT);
            if (DR != null)
            {
                if ((DR["APTProprietario_PES"] != DBNull.Value) && (DR["CPFP"] == DBNull.Value))                
                    SetarCPF(true, (int)DR["APTProprietario_PES"], DR["PESNomeP"].ToString());                                    
                if ((DR["APTInquilino_PES"] != DBNull.Value) && (DR["CPFI"] == DBNull.Value))
                    SetarCPF(true, (int)DR["APTInquilino_PES"], DR["PESNomeI"].ToString());
            }
        }

        private ABS_Apartamento apartamento;

        private ABS_Apartamento Apartamento
        {
            get
            {
                if (lookupBlocosAptos_F1.APT_Sel == -1)
                    return null;
                if ((apartamento == null) || (apartamento.APT != lookupBlocosAptos_F1.APT_Sel))
                    apartamento = ABS_Apartamento.GetApartamento(lookupBlocosAptos_F1.APT_Sel);
                return apartamento;
            }
        }

        private void Calcular()
        {
            SBProprietario.Visible = SBInquilino.Visible = false;
            if (ReentradaCalcular)
                return;
            try
            {
                ReentradaCalcular = true;
                int? BOLFoco = null;
                dBoletos.BOLetosRow rowFoco = (dBoletos.BOLetosRow)GridView_F.GetFocusedDataRow();
                if (rowFoco != null)
                    BOLFoco = rowFoco.BOL;
                bool Recalcular = true;
                if ((dBoletos != null) && (dBoletos.Apartamentos != null))                
                    dBoletos.Apartamentos = null;                
                int Tentativas = 0;
                while (Recalcular && (Tentativas < 2))
                {
                    Recalcular = false;
                    Tentativas++;
                    using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                    {
                        ESP.Titulo = "Carregando dados";
                        ESP.Espere("");
                        Application.DoEvents();
                        BotNadaConsta.Enabled = false;
                        //BotExtrato.Enabled = false;
                        editarJuros = false;
                        BotAcordo.Enabled = false;
                        panelJuridico.Visible = false;
                        dBoletos.SuperBoletoDetalhe.Clear();
                        dBoletos.BOletoDetalhe.Clear();
                        dBoletos.ExtraPendente.Clear();
                        dBoletos.BOLetos.Clear();
                        GridView_F.CollapseAllDetails();

                        //numero do boleto
                        if ((calcEdit1.EditValue != null) && ((decimal)calcEdit1.EditValue != 0))
                        {
                            if ((decimal)calcEdit1.EditValue > int.MaxValue)
                            {
                                MessageBox.Show(string.Format("Valor inv�lido: {0:n0}", calcEdit1.EditValue));
                            }
                            int BOL = (int)((decimal)calcEdit1.EditValue);
                            int R1 = bOLetosTableAdapter.FillByBOL(dBoletos.BOLetos, (int)BOL);

                            if (R1 != 0)
                            {
                                dBoletos.SuperBoletoDetalheTableAdapter.FillByBOL(dBoletos.SuperBoletoDetalhe, DateTime.Today.AddDays(-1), BOL);
                            }
                            else
                            {
                                if ((BOL > 10000000) && (BOL < 20000000))
                                {
                                    DataRow DR = VirMSSQL.TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Internet1).BuscaSQLRow(BuscaInternet, BOL);
                                    MessageBox.Show(string.Format("Boleto de acordo Internet\r\nCondominio: {0}\r\nBloco: {1}\r\nApartamento: {2}\r\n" +
                                                                  "Data do Acordo: {3}\r\n{4} {5}\r\nVencimento: {6}\r\nValor: {7:n2}\r\nHonorarios: {8:n2}",
                                        DR["codcon"] == DBNull.Value ? "" : DR["codcon"],
                                        DR["bloco"] == DBNull.Value ? "" : DR["bloco"],
                                        DR["apartamento"] == DBNull.Value ? "" : DR["apartamento"],
                                        DR["data"] == DBNull.Value ? "" : DR["data"],
                                        DR["juridico"] == DBNull.Value ? "" : ((bool)DR["juridico"] ? "juridico" : ""),
                                        DR["judicial"] == DBNull.Value ? "" : ((bool)DR["judicial"] ? "judicial" : ""),
                                        DR["vencto"] == DBNull.Value ? "" : DR["vencto"],
                                        DR["valor"] == DBNull.Value ? 0 : DR["valor"],
                                        DR["Honorarios"] == DBNull.Value ? 0 : DR["Honorarios"]
                                        ));
                                }
                                else
                                {
                                    Boleto Super = new Boleto(BOL);
                                    if ((Super.Encontrado && Super.rowSBO != null))
                                    {
                                        using (cBoleto cBoleto = new cBoleto(Super, true))
                                        {
                                            cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
                                        }
                                        calcEdit1.EditValue = null;
                                    }
                                    else
                                        MessageBox.Show("Boleto n�o encontrado");
                                }
                                return;
                            };


                            dBoletos.BOletoDetalheTableAdapter.FillBOL(dBoletos.BOletoDetalhe, BOL);
                            dBoletos.BoletoDEscontoTableAdapter.Fill(dBoletos.BoletoDEsconto, BOL);
                            calcEdit1.EditValue = null;
                            if (LinhaMae != null)
                            {
                                cCompet1.Comp.Ano = LinhaMae.BOLCompetenciaAno;
                                cCompet1.Comp.Mes = LinhaMae.BOLCompetenciaMes;
                                cCompet1.AjustaTela();
                                blocloock = true;
                                if (!LinhaMae.IsBOL_APTNull())
                                    lookupBlocosAptos_F1.APT_Sel = LinhaMae.BOL_APT;
                                else
                                {
                                    lookupBlocosAptos_F1.APT_Sel = -1;
                                    lookupBlocosAptos_F1.CON_sel = LinhaMae.BOL_CON;
                                }
                                blocloock = false;
                            }
                            if (dBoletos.CamposComplementares(DataReferencia))
                                Recalcular = true;
                        }
                        else
                        {
                            //apartamento
                            if (lookupBlocosAptos_F1.APT_Sel != -1)
                            {
                                BotAcordo.Enabled = true;
                                
                                EstaNoJuridico();
                                
                                if (checkEdit1.Checked)
                                {
                                    bOLetosTableAdapter.FillByAPTComp(dBoletos.BOLetos, lookupBlocosAptos_F1.APT_Sel, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes);
                                    dBoletos.SuperBoletoDetalheTableAdapter.FillByAPT(dBoletos.SuperBoletoDetalhe, DateTime.Today.AddDays(-1), lookupBlocosAptos_F1.APT_Sel);
                                }
                                else
                                    if (!checkVencidos.Checked)
                                {
                                    if (ch6M.Checked)
                                        bOLetosTableAdapter.FillByAPTCorte(dBoletos.BOLetos, lookupBlocosAptos_F1.APT_Sel, DateTime.Today.AddMonths(-6));
                                    else
                                        bOLetosTableAdapter.FillByAPT1(dBoletos.BOLetos, lookupBlocosAptos_F1.APT_Sel);
                                    dBoletos.SuperBoletoDetalheTableAdapter.FillByAPT(dBoletos.SuperBoletoDetalhe, DateTime.Today.AddDays(-1), lookupBlocosAptos_F1.APT_Sel);                                    
                                    BotNadaConsta.Enabled = NadaConstaAtivo();
                                }
                                else
                                    bOLetosTableAdapter.FillVencidosAPT(dBoletos.BOLetos, dateEditVencimento.DateTime, dateEditVencimento.DateTime, lookupBlocosAptos_F1.APT_Sel);

                                dBoletos.BOletoDetalheTableAdapter.FillByAPT1(dBoletos.BOletoDetalhe, lookupBlocosAptos_F1.APT_Sel);
                                dBoletos.BoletoDEscontoTableAdapter.FillByAPT(dBoletos.BoletoDEsconto, lookupBlocosAptos_F1.APT_Sel,DateTime.Today);
                                if (dBoletos.CamposComplementares(DataReferencia))
                                    Recalcular = true;
                                ChecarCPF(lookupBlocosAptos_F1.APT_Sel);
                                if (Apartamento != null)
                                {
                                    SBProprietario.Visible = true;
                                    SBInquilino.Visible = Apartamento.Alugado;
                                }
                            }
                            else
                            {
                                //condominio
                                if (lookupBlocosAptos_F1.CON_sel != -1)
                                {
                                    //if (Abortar)
                                    //  return;

                                    if (checkEdit1.Checked)
                                    {
                                        ESP.Espere("Lendo boletos");
                                        Application.DoEvents();
                                        bOLetosTableAdapter.FillByCONComp(dBoletos.BOLetos, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes, lookupBlocosAptos_F1.CON_sel);
                                        ESP.Espere("Lendo detalhes boletos");
                                        Application.DoEvents();
                                        dBoletos.BOletoDetalheTableAdapter.FillByCONComp(dBoletos.BOletoDetalhe, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes, lookupBlocosAptos_F1.CON_sel);
                                        dBoletos.BoletoDEscontoTableAdapter.FillByCONComp(dBoletos.BoletoDEsconto, lookupBlocosAptos_F1.CON_sel, (short)cCompet1.Comp.Mes, (short)cCompet1.Comp.Ano);
                                    }
                                    else if (checkVencidos.Checked)
                                    {
                                        ESP.Espere("Lendo boletos");
                                        Application.DoEvents();
                                        bOLetosTableAdapter.FillVencidosAte(dBoletos.BOLetos, dateEditVencimento.DateTime, dateEditVencimento.DateTime, lookupBlocosAptos_F1.CON_sel);
                                        ESP.Espere("Lendo detalhes boletos");
                                        Application.DoEvents();
                                        dBoletos.BOletoDetalheTableAdapter.FillByCON(dBoletos.BOletoDetalhe, lookupBlocosAptos_F1.CON_sel);
                                    }
                                    else
                                    {
                                        ESP.Espere("Lendo boletos");
                                        Application.DoEvents();
                                        if (ch6M.Checked)
                                            bOLetosTableAdapter.FillByCONCorte(dBoletos.BOLetos, lookupBlocosAptos_F1.CON_sel, DateTime.Today.AddMonths(-6));
                                        else
                                            bOLetosTableAdapter.FillByCON(dBoletos.BOLetos, lookupBlocosAptos_F1.CON_sel);
                                        ESP.Espere("Lendo detalhes boletos");
                                        Application.DoEvents();
                                        dBoletos.BOletoDetalheTableAdapter.FillByCON(dBoletos.BOletoDetalhe, lookupBlocosAptos_F1.CON_sel);
                                    };
                                    ESP.Espere("Lendo Super boletos");
                                    Application.DoEvents();
                                    dBoletos.SuperBoletoDetalheTableAdapter.FillByCON(dBoletos.SuperBoletoDetalhe, DateTime.Today.AddDays(-1), lookupBlocosAptos_F1.CON_sel);
                                    ESP.Espere("Lendo dados complementares");
                                    Application.DoEvents();
                                    dBoletos.CarregaAPTs(CON, true);
                                    //if (BoletosProc.Boleto.dBoletos.CamposComplementares(DataReferencia,true,ESP))
                                    if (dBoletos.CamposComplementares(DataReferencia, true))
                                        Recalcular = true;
                                }
                            }
                        };
                        dBoletos.ExtraPendenteTableAdapter.Fill(dBoletos.ExtraPendente, CON);
                        if (GridView_F.RowCount > 1)
                            GridView_F.UnselectRow(0);
                    }
                }
                if (BOLFoco.HasValue)
                {
                    int Pos = GridView_F.LocateByValue("BOL", BOLFoco.Value);
                    if (Pos >= 0)
                        GridView_F.FocusedRowHandle = Pos;
                }
            }
            finally
            {
                ReentradaCalcular = false;
            }
        }

        private int CON
        {
            get
            {
                return lookupBlocosAptos_F1.CON_sel;
            }
        }

        private dBoletos.BOLetosRow LinhaMae
        {
            get
            {
                return (dBoletos.BOLetosRow)LinhaMae_F;
            }
        }

        #region Impressos  

        ImpBoletoBase[] Impressos;

        enum TipoImpresso
        {
            Seguro = 0,
            Acordo = 1,
            Recibo = 2,
            ReciboAcordo = 3,
            SegundaVia = 4,
            Balancete = 5,
            Simplificado = 6
        }

        private ImpBoletoBase GetImpresso(TipoImpresso Tipo)
        {

            if (Impressos[(int)Tipo] == null)
                switch (Tipo)
                {
                    case TipoImpresso.Seguro:
                        Impressos[(int)Tipo] = new ImpBoletoSeguro();
                        break;
                    case TipoImpresso.Acordo:
                        Impressos[(int)Tipo] = new ImpBoletoAcordo();
                        break;
                    case TipoImpresso.Recibo:
                        Impressos[(int)Tipo] = new ImpBoletoReciboSV();
                        break;
                    case TipoImpresso.ReciboAcordo:
                        Impressos[(int)Tipo] = new ImpBoletoReciboAc();
                        break;
                    case TipoImpresso.SegundaVia:
                        Impressos[(int)Tipo] = new ImpSegundaVia();
                        break;
                    case TipoImpresso.Balancete:

                        Impressos[(int)Tipo] = new ImpBoletoBal();
                        break;
                    case TipoImpresso.Simplificado:
                        ImpSegundaVia novoImp = new ImpSegundaVia();
                        novoImp.xrTitulo.Visible = false;
                        Impressos[(int)Tipo] = novoImp;
                        break;
                }
            return Impressos[(int)Tipo];

        }

        #endregion

        private List<StatusBoleto> NaoImprimiveis;

        private void Imprime(bool SegundaVia, dllImpresso.Botoes.Botao Destino, bool Vencimento, bool Recibo)
        {
            string DestinatarioDefault = "";
            Impressos = new ImpBoletoBase[7];
            ImpBoletoBase ImpSB = null;
            List<int> Selecionadas = new List<int>(GridView_F.GetSelectedRows());
            if (Selecionadas.Count == 0)
            {
                MessageBox.Show("Nenhuma linha selecionada!!");
                return;
            }
            if ((GridView_F.FocusedRowHandle >= 0) && (!Selecionadas.Contains(GridView_F.FocusedRowHandle)))
            {
                MessageBox.Show("Linha em foco n�o est� selecionada!!");
                return;
            }

            if (NaoImprimiveis == null)
                NaoImprimiveis = new List<StatusBoleto>(new StatusBoleto[] { StatusBoleto.Acumulado, StatusBoleto.Cancelado, StatusBoleto.EmProducao, StatusBoleto.Erro, StatusBoleto.Saldinho });

            System.Collections.ArrayList BoletosImpressos = new System.Collections.ArrayList();
            foreach (int i in Selecionadas)
            {
                DataRowView DRV = (DataRowView)GridView_F.GetRow(i);
                Boleto BolImp = new Boleto((dBoletos.BOLetosRow)DRV.Row);
                if (NaoImprimiveis.Contains(BolImp.BOLStatus))
                    continue;
                if (!Vencimento && (BolImp.Conta.BCO.EstaNoGrupo (341,104)) && (dateEdit1.DateTime > BolImp.rowPrincipal.BOLVencto.AddMonths(1)))
                {
                    MessageBox.Show("Boletos com registro no Ita� e Caixa n�o podem ser pagos apos 30 dias");
                    continue;
                }
                if (Recibo)
                    switch (BolImp.rowPrincipal.BOLTipoCRAI)
                    {
                        case "S":
                        case "C":
                        case "I":
                        case "R":
                            ImpSB = GetImpresso(TipoImpresso.Recibo);
                            break;
                        case "A":
                            if (VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("SELECT ACO FROM ACOxBOL where BOL = @P1", BolImp.rowPrincipal.BOL))
                                ImpSB = GetImpresso(TipoImpresso.ReciboAcordo);
                            else
                                ImpSB = GetImpresso(TipoImpresso.Recibo);
                            break;
                    }
                else if (SegundaVia)
                {
                    if ((BolImp.rowPrincipal.BOLTipoCRAI == "A") && (VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("SELECT ACO FROM ACOxBOL where BOL = @P1", BolImp.rowPrincipal.BOL)))
                        ImpSB = GetImpresso(TipoImpresso.Acordo);
                    else
                        ImpSB = GetImpresso(TipoImpresso.SegundaVia);
                }
                else
                    switch (BolImp.rowPrincipal.BOLTipoCRAI)
                    {
                        case "S":
                            ImpSB = GetImpresso(TipoImpresso.Seguro);
                            break;
                        case "C":
                            ImpSB = GetImpresso(TipoImpresso.Balancete);
                            if (checkEdit1.Checked)
                                ((ImpBoletoBal)ImpSB).PrecargaConsumo(CON, cCompet1.Comp);
                            break;
                        case "A":
                            if (VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("SELECT ACO FROM ACOxBOL where BOL = @P1", BolImp.rowPrincipal.BOL))
                                ImpSB = GetImpresso(TipoImpresso.Acordo);
                            else
                                ImpSB = GetImpresso(TipoImpresso.Simplificado);
                            break;
                        case "I":
                        case "R":
                            ImpSB = GetImpresso(TipoImpresso.Simplificado);
                            break;
                    }

                if ((Destino == dllImpresso.Botoes.Botao.email_comprovante) || (Destino == dllImpresso.Botoes.Botao.imprimir_comprovante) || (Destino == dllImpresso.Botoes.Botao.pdf_comprovante))
                    BolImp.ComprovanteDeEndereco = true;
                if (BolImp.rowPrincipal.BOLProtestado)
                {
                    MessageBox.Show("N�o � permitida a impress�o de boletos protestados!!!\r\nEntrar em contato com a Dra BLANCA advogada respons�vel!!");
                    return;
                };
                BoletosImpressos.Add(BolImp);
                if (DestinatarioDefault == "")
                    DestinatarioDefault = BolImp.EmailResponsavel;
                if (SegundaVia)
                    BolImp.IncluicustoSegundaVia(true);
                if (!Vencimento)
                {
                    string Justificativa = "";
                    if (Emailjuridico != "")
                        if (MessageBox.Show("ATEN��O:Este apartamento est� no jur�dico e este evento ter� de ser notificado para " + labelEscritorio.Text + "\r\nContinuar ?", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                            return;
                    BolImp.Valorpara((DateTime)dateEdit1.EditValue);
                    if (editarJuros)
                    {
                        decimal Juros = BolImp.valorfinal - BolImp.rowPrincipal.BOLValorPrevisto;
                        decimal JurosOrig = Juros;
                        if (!VirInput.Input.Execute("Juros", ref Juros))
                            return;
                        if ((Juros != JurosOrig) || (Emailjuridico != ""))
                            if (!VirInput.Input.Execute("Justificativa", ref Justificativa, true))
                                return;
                        if (Juros != JurosOrig)
                        {
                            BolImp.valorfinal = BolImp.rowPrincipal.BOLValorPrevisto + Juros;
                            if (BolImp.BOLStatusRegistro == StatusRegistroBoleto.BoletoRegistrado)
                                BolImp.BOLStatusRegistro = StatusRegistroBoleto.AlterarRegistroData;
                            string Jus = string.Format("Juros alterados de {0:n2} para {1:n2} para pagamento at� {2:dd/MM/yyyy} por {3}\r\nJustificativa:{4}", JurosOrig, Juros, dateEdit1.EditValue, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome, Justificativa);
                            if (!BolImp.GravaJustificativa(Jus, BolImp.rowPrincipal.BOLValorPrevisto + Juros, (DateTime)dateEdit1.EditValue, Emailjuridico))
                            {
                                Calcular();
                                return;
                            }
                        }
                        else
                            BolImp.GravaJustificativa("Boleto re-impresso para " + dateEdit1.DateTime.ToString("dd/MM/yyyy") + " com valor de " + BolImp.valorfinal.ToString("n2") + " por " + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome + "\r\n" + ((Justificativa == "") ? "" : "\r\nJustificativa:" + Justificativa), Emailjuridico);
                    }
                    else
                    {
                        if ((Emailjuridico != "") && (Justificativa == ""))
                            if (!VirInput.Input.Execute("Justificativa", ref Justificativa, true))
                                return;
                        BolImp.GravaJustificativa("Boleto re-impresso para " + dateEdit1.DateTime.ToString("dd/MM/yyyy") + " com valor de " + BolImp.valorfinal.ToString("n2") + " por " + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome + "\r\n" + ((Justificativa == "") ? "" : "\r\nJustificativa:" + Justificativa), Emailjuridico);
                    }

                };
                if (Recibo)
                {
                    ((ImpBoletoReciboSV)ImpSB).SetaDataPag(dateEdit1.DateTime);
                    BolImp.DataDeCalculo = (dateEdit1.DateTime > DateTime.Today) ? dateEdit1.DateTime.AddDays(15) : DateTime.Today.AddDays(15);
                }

                BolImp.CarregaLinha(ImpSB, Vencimento, SegundaVia);
            };

            for (int i = 0; i < Impressos.Length; i++)
                if (Impressos[i] != null)
                {
                    ImpSB = Impressos[i];
                    ImpSB.bindingSourcePrincipal.Sort = "Correio,BLOCodigo,APTNumero";

                    if ((Destino == dllImpresso.Botoes.Botao.imprimir_frente)
                        ||
                        (Destino == dllImpresso.Botoes.Botao.PDF_frente)
                        ||
                        (Destino == dllImpresso.Botoes.Botao.email))
                        ImpSB.ComRemetente = false;
                    switch (Destino)
                    {
                        case dllImpresso.Botoes.Botao.botao:
                        case dllImpresso.Botoes.Botao.imprimir:
                        case dllImpresso.Botoes.Botao.imprimir_frente:
                        case dllImpresso.Botoes.Botao.imprimir_comprovante:
                            if (lookupBlocosAptos_F1.CON_sel != -1)
                                ImpSB.Apontamento(lookupBlocosAptos_F1.CON_sel, 2);
                            else
                                MessageBox.Show("Apontamento n�o efetuado");
                            if (PassoAPasso)
                                MessageBox.Show("Criando documento");

                            ImpSB.CreateDocument();
                            if (PassoAPasso)
                                MessageBox.Show("Imprimindo");
                            ImpSB.Print();
                            break;
                        case dllImpresso.Botoes.Botao.tela:
                            ImpSB.CreateDocument();
                            ImpSB.ShowPreview();
                            break;
                        case dllImpresso.Botoes.Botao.pdf:
                        case dllImpresso.Botoes.Botao.PDF_frente:
                        case dllImpresso.Botoes.Botao.pdf_comprovante:
                            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                ImpSB.CreateDocument();
                                ImpSB.ExportOptions.Pdf.Compressed = true;
                                ImpSB.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                                ImpSB.ExportToPdf(saveFileDialog1.FileName);
                            }
                            break;
                        case dllImpresso.Botoes.Botao.email:
                        case dllImpresso.Botoes.Botao.email_comprovante:
                            string NomeArquivo = "";
                            string CBs = "";
                            foreach (dImpBoletoBase.BOLETOSIMPRow rowImp in ImpSB.dImpBoletoBase1.BOLETOSIMP)
                            {
                                NomeArquivo += ((NomeArquivo == "" ? "" : "-") + rowImp.BOL);
                                if (rowImp.IsBOLPagamentoNull())
                                    CBs += (CBs == "" ? "" : "\r\n\r\n\r\n") + "Boleto:" + rowImp.BOL.ToString() + "\r\nVencimento: " + rowImp.BOLVencto.ToString("dd/MM/yyyy") + "\r\nValor: " + rowImp.BOLValorPrevisto.ToString("n2") + "\r\nLinha digit�vel: " + rowImp.BOLCodbarstr;
                                else
                                    CBs += (CBs == "" ? "" : "\r\n\r\n\r\n") + "Boleto:" + rowImp.BOL.ToString() + "\r\nVencimento: " + rowImp.BOLVencto.ToString("dd/MM/yyyy") + "\r\nValor: " + rowImp.BOLValorPrevisto.ToString("n2") + "\r\nData do pagamento: " + rowImp.BOLPagamento.ToString("dd/MM/yyyy");
                            };
                            if (NomeArquivo != "")
                            {

                                if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", NomeArquivo, ImpSB, "Em anexo os boletos:\r\n\r\n" + CBs, "Boletos: " + NomeArquivo, DestinatarioDefault))
                                {
                                    foreach (Boleto BolEnv in BoletosImpressos)
                                        BolEnv.GravaJustificativa("Enviado para:" + VirEmailNeon.EmailDiretoNeon.EmalST.UltimoEndereco, "");
                                    MessageBox.Show("E.mail enviado");
                                }
                                else
                                {
                                    if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                                    {
                                        foreach (Boleto BolEnv in BoletosImpressos)
                                            BolEnv.GravaJustificativa(string.Format("Enviado para: {0} (fila)", VirEmailNeon.EmailDiretoNeon.EmalST.UltimoEndereco), "");
                                        MessageBox.Show("E.mail N�O enviado (na fila)");
                                    }
                                    else
                                        MessageBox.Show("Cancelado");
                                }
                            }
                            break;
                    }
                }

        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            cCompet1.Enabled = checkEdit1.Checked;
            if (!AlterandoCH)
            {
                AlterandoCH = true;

                if (checkEdit1.Checked)
                    checkVencidos.Checked = false;
                Fecha();
                AlterandoCH = false;
            }
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            Calcular();
        }

        private void GridView_F_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            dBoletos.BOLetosRow linha = ((dBoletos.BOLetosRow)GridView_F.GetDataRow(e.RowHandle));
            if (linha == null)
                return;
            Boleto.AjustaAparencia(e.Appearance, linha);
            e.HighPriority = true;
            return;
        }

        private Font _FonteDestacado;

        private Font FonteDestacado(Font Modelo)
        {
            if (_FonteDestacado == null)
                _FonteDestacado = new Font(Modelo, FontStyle.Bold);
            return _FonteDestacado;
        }

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;            
            dBoletos.BOLetosRow linha = ((dBoletos.BOLetosRow)GridView_F.GetDataRow(e.RowHandle));
            if (linha == null)
                return;


            if ((e.Column.FieldName == "BOL") && (!linha.IsBOLJustificativaNull()))
            {
                e.Appearance.ForeColor = linha.BOLProtestado ? Color.White : Color.Red;
                e.Appearance.Font = FonteDestacado(e.Appearance.Font);
            }
            if(e.Column.Equals(colBOLStatusRegistro))
            {
                Boleto.VirEnumStatusRegistroBoleto.CellStyle((StatusRegistroBoleto)linha.BOLStatusRegistro, e);
            }
        }




        private void simpleButton3_Click_1(object sender, EventArgs e)
        {
            bool Alterado = false;
            if ((GridView_F.SelectedRowsCount == 0) && (LinhaMae == null))
                return;
            string Justificativa = "";
            if (VirInput.Input.Execute("Justificativa:", ref Justificativa, true))
            {
                if (GridView_F.SelectedRowsCount == 0)
                    Alterado = Alterado || new Boleto(LinhaMae.BOL).CancelaEmissao(Justificativa);
                else
                    foreach (int iSel in GridView_F.GetSelectedRows())
                        if ((new Boleto((dBoletos.BOLetosRow)GridView_F.GetDataRow(iSel))).CancelaEmissao(Justificativa))
                            Alterado = true;

            }
            if (Alterado)
                Calcular();
        }


        /// <summary>
        /// Recalcular
        /// </summary>
        public override void Refresh()
        {
            Calcular();
        }

        private void BotAcordo_Click(object sender, EventArgs e)
        {

            Boletos.Acordo.cAcordo Novo = new Boletos.Acordo.cAcordo(lookupBlocosAptos_F1.APT_Sel);
            CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludointerno(Novo, "Acordos ", CompontesBasicos.EstadosDosComponentes.JanelasAtivas, false);
            //Novo.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);

        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {

            if (lookupBlocosAptos_F1.CON_sel != -1)
            {

                if (checkEdit1.Checked)
                {
                    bOLetosTableAdapter.RecuperaCancelados(cCompet1.Comp.Ano, cCompet1.Comp.Mes, lookupBlocosAptos_F1.CON_sel);
                    bOLetosTableAdapter.FillByCONComp(dBoletos.BOLetos, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes, lookupBlocosAptos_F1.CON_sel);
                    dBoletos.BOletoDetalheTableAdapter.FillByCONComp(dBoletos.BOletoDetalhe, (short)cCompet1.Comp.Ano, (short)cCompet1.Comp.Mes, lookupBlocosAptos_F1.CON_sel);
                }

            }

        }



        private bool AlterandoCH = false;



        private void checkVencidos_CheckedChanged(object sender, EventArgs e)
        {
            dateEditVencimento.Enabled = checkVencidos.Checked;
            if (!AlterandoCH)
            {
                AlterandoCH = true;

                if (checkVencidos.Checked)
                {
                    checkEdit1.Checked = false;
                    if (dateEditVencimento.DateTime == DateTime.MinValue)
                        dateEditVencimento.DateTime = DateTime.Now;
                };
                Fecha();
                AlterandoCH = false;
            }
        }

        private void Fecha()
        {
            dBoletos.BOletoDetalhe.Clear();
            dBoletos.BOLetos.Clear();
            BotAcordo.Enabled = false;
            //BotExtrato.Enabled = false;
        }

        private void dateEditVencimento_EditValueChanged(object sender, EventArgs e)
        {
            Fecha();
        }

        private void cCompet1_Click(object sender, EventArgs e)
        {
            Fecha();
        }


        /// <summary>
        /// Imprimir relat�rio
        /// </summary>
        public override void Imprimir_F()
        {
            //dBoletos.CamposComplementares(DateTime.Today, false);
            Impresso.ImpInadimplencia ImpVencidos = new Boletos.Boleto.Impresso.ImpInadimplencia();
            ImpVencidos.bindingSource1.DataSource = dBoletos.BOLetos;
            ImpVencidos.SetaDados(DateTime.Today, lookupBlocosAptos_F1.CON_selrow.CONNome);
            ImpVencidos.xrTitulo.Text = string.Format("Boletos ({0:dd/MM/yyyy})", DateTime.Today);
            ImpVencidos.ShowPreview();
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            cBotaoImpPara.Enabled = cBotaoImpJuros.Enabled = cBotaoImpRecibo.Enabled = false;

            if (dateEdit1.EditValue != null)
            {
                DateTime DataNova = (DateTime)dateEdit1.EditValue;
                if (DataNova >= DateTime.Today)
                {
                    cBotaoImpPara.Enabled = cBotaoImpJuros.Enabled = cBotaoImpRecibo.Enabled = true;
                    //cGeraSuper.Enabled = true;
                }
            }
            AtuaBotaoSuper();
        }

        private void simpleButton10_Click_3(object sender, EventArgs e)
        {
            if (lookupBlocosAptos_F1.APT_Sel == -1)
            {
                MessageBox.Show("Selecione um apartamento");
                return;
            };
            Boleto BolTeste;
            ImpBoletoBase ImpTeste = new ImpBoletoBase();
            //BolTeste = new Boleto(lookupBlocosAptos_F1.APT_Sel, 19, 321.12M, new DateTime(2006, 8, 24), true);
            //BolTeste.CarregaLinha(ImpTeste, true, false);
            //string teste = "";
            FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(lookupBlocosAptos_F1.CON_sel);
            switch (rowCON.CON_BCO)
            {
                case 104:
                    DateTime data;
                    for (int i = 1; i <= 9; i++)
                    {
                        data = DateTime.Today.AddDays(10);
                        do
                        {
                            BolTeste = new Boleto(CON, lookupBlocosAptos_F1.APT_Sel, i, 10, data, true);
                            BolTeste.BOLStatusRegistro = StatusRegistroBoleto.BoletoRegistrando;
                            data = data.AddDays(1);
                        } while (BolTeste.LinhaDigitavel.Substring(41, 1) != i.ToString());
                        BolTeste.CarregaLinha(ImpTeste, true, false);
                        BoletosRemessa.Add(BolTeste);
                    };
                    int nn = 10;
                    for (int Veri = 0; Veri <= 9; Veri++)
                    {
                        data = DateTime.Today.AddDays(10);
                        do
                        {
                            BolTeste = new Boleto(CON, lookupBlocosAptos_F1.APT_Sel, nn, 10, data, true);
                            BolTeste.BOLStatusRegistro = StatusRegistroBoleto.BoletoRegistrando;
                            nn++;
                        } while (BolTeste.LinhaDigitavel.Substring(37, 1) != Veri.ToString());
                        BolTeste.CarregaLinha(ImpTeste, true, false);
                        BoletosRemessa.Add(BolTeste);
                    };
                    break;

                case 341:
                    for (int i = 1; i <= 30; i++)
                    {
                        BolTeste = new Boleto(null, lookupBlocosAptos_F1.APT_Sel, i, 10, DateTime.Today, false);
                        BolTeste.CarregaLinha(ImpTeste, true, false);

                        //BolTeste = new Boleto(lookupBlocosAptos_F1.APT_Sel, i, 10, DateTime.Today, false);
                        //BolTeste.CarregaLinha(ImpTeste, true, false);

                    };
                    break;
                case 33:
                    for (int i = 1; i <= 30; i++)
                    {
                        BolTeste = new Boleto(CON, lookupBlocosAptos_F1.APT_Sel, i, 10, DateTime.Today.AddDays(i), true);
                        BolTeste.BOLStatusRegistro = StatusRegistroBoleto.BoletoRegistrando;
                        BolTeste.CarregaLinha(ImpTeste, true, false);
                        BoletosRemessa.Add(BolTeste);
                        //BolTeste = new Boleto(lookupBlocosAptos_F1.APT_Sel, i, 10, DateTime.Today, false);
                        //BolTeste.CarregaLinha(ImpTeste, true, false);

                    };
                    break;
                default:
                    throw new NotImplementedException(string.Format("BoletosGrade - N�o implementado para banco = {0}", rowCON.CON_BCO));
            }

            ImpTeste.ComRemetente = false;
            ImpTeste.CreateDocument();
            ImpTeste.ShowPreviewDialog();

        }

        private bool PassoAPasso = false;

        private void cBotaoImpBol2_clicado(object sender, EventArgs e)
        {
            Imprime(false, cBotaoOriginal.BotaoClicado, true, false);
        }

        private void cBotaoImpBol3_clicado(object sender, EventArgs e)
        {
            Imprime(true, cBotaoSegundaVia.BotaoClicado, true, false);
        }

        private void cBotaoImpPara_clicado(object sender, EventArgs e)
        {
            Imprime(true, cBotaoImpPara.BotaoClicado, false, false);
        }

        private void cBotaoImpBol3_clicado_1(object sender, EventArgs e)
        {
            editarJuros = true;
            Imprime(true, cBotaoImpRecibo.BotaoClicado, false, true);
            editarJuros = false;
        }

        private void cBotaoImpJuros_clicado(object sender, EventArgs e)
        {
            editarJuros = true;
            Imprime(true, cBotaoImpJuros.BotaoClicado, false, false);
            editarJuros = false;
        }





        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (LinhaMae != null)
            {
                if (MessageBox.Show("Esta opera��o s� dever ser usada em implanta��o pois ir� criar problemas no balancete!!!\r\nConfirma?", "ATEN��O", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                {
                    DateTime? DataPag = LinhaMae.BOLVencto;
                    if (!VirInput.Input.Execute("Data de pagamento", ref DataPag, null, LinhaMae.BOLVencto))
                        return;
                    if (DataPag.Value > DateTime.Today)
                        MessageBox.Show("Data inv�lida");
                    else
                    {
                        Boleto Bol = new Boleto(LinhaMae);
                        if (!Bol.Baixa("Manual 1", DataPag.Value, LinhaMae.BOLValorPrevisto, 0))
                            MessageBox.Show("Erro na baixa:" + Bol.UltimoErro);

                        //if (!Bol.BaixaImplantacao(DataPag.Value))
                        //    MessageBox.Show("Erro na baixa:" + Bol.UltimoErro);
                    }
                }
            }
        }

        private VirMSSQL.TableAdapter _TableAdapterInternet2;

        private VirMSSQL.TableAdapter TableAdapterInternet2
        {
            get
            {
                if (_TableAdapterInternet2 == null)
                    _TableAdapterInternet2 = new VirMSSQL.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                return _TableAdapterInternet2;
            }
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (LinhaMae != null)
            {
                Boleto Bol = new Boleto(LinhaMae);
                if (!Bol.rowPrincipal.BOLProtestado)
                    MessageBox.Show("Boleto n�o protestado");
                else
                {
                    string Justificativa = "";
                    if (!VirInput.Input.Execute("Justificativa", ref Justificativa, true))
                        return;
                    TableAdapterInternet2.ExecutarSQLNonQuery("delete from Protesto where BOL = @P1", Bol.rowPrincipal.BOL);
                    Bol.rowPrincipal.BOLProtestado = false;
                    Bol.GravaJustificativa("Cancelado o protesto\r\nJustificativa:\r\n" + Justificativa, "blanca@advocaciamendes.com.br");
                }
            };
        }

        //bool Liberado;

        /*
        private void CorrecaoAcVelha()
        {

            int BOLBase = 0;
            int BOLTeto = 0;
            int Cor = 0;
            int Duvida = 0;
            VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select max(BOL) from boletos", out BOLTeto);
            while (BOLBase < BOLTeto)
            {
                DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(
"SELECT     BOL\r\n" +
"FROM         BOLetos\r\n" +
"WHERE     (BOLTipoCRAI = 'A') AND (BOLPagamento > CONVERT(DATETIME, '2009-09-01 00:00:00', 102)) AND (BOLCancelado = 0)\r\n" +
"and \r\n" +
"(bol in \r\n" +
"(select top(10000) bol from boletos where bol > @P1\r\n" +
"ORDER BY BOL)\r\n" +
");", BOLBase);
                if (DT.Rows.Count == 0)
                    BOLBase += 10000;
                else
                {
                    BOLBase = (int)DT.Rows[DT.Rows.Count - 1][0];
                    foreach (DataRow DR in DT.Rows)
                    {
                        bool Pago = false;
                        if (VirOleDb.TableAdapter.STTableAdapter.BuscaEscalar(
"SELECT     Pago\r\n" +
"FROM         [Acordo - Parcelas]\r\n" +
"WHERE     ([N�mero do Boleto] = @P1);", out Pago, DR[0]))
                        {
                            if (!Pago)
                            {
                                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update [Acordo - Parcelas] set pago = 1 where ([N�mero do Boleto] = @P1)", DR[0]);
                                Cor++;
                            }
                            else
                                Duvida++;
                        }
                    }
                }
            }
            MessageBox.Show(string.Format("Corrigidos:{0} Verificar:{1}", Cor, Duvida));

        }
        */

        //int ultimoACO;

        /*
        private void verificaAcordos()
        {
            DataTable TM = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela("SELECT ACO, ACO_APT, ACOAntigo FROM ACOrdos WHERE (ACOStatus = 1) order by ACO");
            //DataTable 
            foreach (DataRow DRM in TM.Rows)
            {
                break;
                if ((int)DRM["ACO"] < ultimoACO)
                    continue;
                ultimoACO = (int)DRM["ACO"];
                if (!VirOleDb.TableAdapter.STTableAdapter.EstaCadastrado("SELECT [N�mero Acordo]  FROM Acordo where [N�mero Acordo] = @P1", DRM["ACOAntigo"]))
                {
                    lookupBlocosAptos_F1.APT_Sel = (int)DRM["ACO_APT"];
                    break;
                    //MessageBox.Show(DRM["ACO"].ToString());
                }
            }
            MessageBox.Show("FIM");
            TM = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLTabela("SELECT [N�mero Acordo]  FROM Acordo");
            //DataTable 
            foreach (DataRow DRM in TM.Rows)
            {
            //    if ((int)DRM["[N�mero Acordo]"] < ultimoACO)
            //        continue;
            //    ultimoACO = (int)DRM["ACO"];
                if (!VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("SELECT ACO FROM ACOrdos WHERE ACOAntigo = @P1", DRM["N�mero Acordo"]))
                {

                    MessageBox.Show(DRM["N�mero Acordo"].ToString());
                }
            }
            MessageBox.Show("FIM2");
        }
        */

        private void setValor(SortedList SL, DataRow DR, string Nome, string Campo)
        {
            object obj = DR[Campo];
            SL.Add(Nome, obj == DBNull.Value ? "" : obj.ToString());
        }


        private void BotNadaConsta_clicado(object sender, dllBotao.BotaoArgs args)
        {
            if (!NadaConstaAtivo())
                return;
            /*
            bool temProblema = false;
            foreach (dBoletos.BOLetosRow rowBOL in dBoletos.BOLetos)
            {
                if ((rowBOL.BOLTipoCRAI == "A") && (rowBOL.IsBOLPagamentoNull()))
                {
                    temProblema = true;
                    break;
                };
                if ((rowBOL.BOLVencto <= DateTime.Today) && rowBOL.IsBOLPagamentoNull())
                {
                    temProblema = true;
                    break;
                }
            }
            if (temProblema)
                return;
            */
            string ComandoBuscaDados =
"SELECT     CONCodigo, CONNome, CONBairro, CIDNome, CIDUf, \r\n" +
"                      CONCep, CONCnpj, CONEndereco2, CONNumero, BLOCodigo, BLONome, \r\n" +
"                      APTNumero, PESSOAS.PESNome, PESSOAS.PESEmail, \r\n" +
"                      PESSOAS_S.PESNome as Sindico \r\n" +
"FROM         CONDOMINIOS INNER JOIN\r\n" +
"                      BLOCOS ON CONDOMINIOS.CON = BLOCOS.BLO_CON INNER JOIN\r\n" +
"                      APARTAMENTOS ON BLOCOS.BLO = APARTAMENTOS.APT_BLO INNER JOIN\r\n" +
"                      PESSOAS ON APARTAMENTOS.APTProprietario_PES = PESSOAS.PES INNER JOIN\r\n" +
"                      CIDADES ON CONDOMINIOS.CON_CID = CIDADES.CID LEFT OUTER JOIN\r\n" +
"                      View_SINDICOS ON CONDOMINIOS.CON = View_SINDICOS.CDR_CON LEFT OUTER JOIN\r\n" +
"                      PESSOAS AS PESSOAS_S ON PESSOAS_S.PES = View_SINDICOS.CDR_PES\r\n" +
"WHERE     (APARTAMENTOS.APT = @P1);";
            DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoBuscaDados, lookupBlocosAptos_F1.APT_Sel);
            if (DR == null)
                return;
            bool apontar = (args.Botao == dllBotao.Botao.imprimir);
            ImpRTFLogoSimples Impresso = new ImpRTFLogoSimples("", apontar ? lookupBlocosAptos_F1.CON_sel : -1, 77);
            SortedList SL = new System.Collections.SortedList();

            setValor(SL, DR, "CODCON", "CONCodigo");
            setValor(SL, DR, "Condom�nio", "CONNome");
            setValor(SL, DR, "Bairro", "CONBairro");
            setValor(SL, DR, "Cidade", "CIDNome");
            setValor(SL, DR, "UF", "CIDUf");
            setValor(SL, DR, "CEP", "CONCep");
            setValor(SL, DR, "CNPJ", "CONCnpj");
            setValor(SL, DR, "Endere�o", "CONEndereco2");
            setValor(SL, DR, "N�mero", "CONNumero");
            setValor(SL, DR, "Bloco", "BLOCodigo");
            setValor(SL, DR, "Nome Bloco", "BLONome");
            setValor(SL, DR, "Apartamento", "APTNumero");
            setValor(SL, DR, "Propriet�rio", "PESNome");
            setValor(SL, DR, "S�ndico", "Sindico");
            SortedList Alternativas = new SortedList();
            Alternativas.Add(0, "NEON");
            Alternativas.Add(1, "Sindico");
            int escolhido = 0;
            if (!VirInput.Input.Execute("Assinado", Alternativas, out escolhido))
            {
                BotNadaConsta.Impresso = null;
                return;
            }
            Impresso.CarregarImpresso(escolhido == 0 ? "Nada consta" : "NADA CONSTA S", SL);
            BotNadaConsta.Impresso = Impresso;
            if (DR["PESEmail"] != DBNull.Value)
                BotNadaConsta.EmailDefault = DR["PESEmail"].ToString();
            return;
        }

        private void BotNadaConsta_NotificaErro(object sender, Exception e)
        {
            MessageBox.Show(e.Message);
        }

        /*
        private const string comandoLanca = "INSERT INTO [Lancamentos Bradesco] \r\n" +
                                          "(Codcon, Apartamento, Bloco, [N�mero Documento], [Valor Pago], Cr�dito, [Data de Leitura], [Data de Lan�amento], \r\n" +
                                          "Tipopag, Descri��o,[M�s Compet�ncia],CCD) \r\n" +
                                          "VALUES     (@P1,@P2,@P3,@P4,@P5,1,@P6,@P7,@P8,@P9,@P10,@P11)";

        private bool RegravaBaixaAccess(Boleto BOLETO)
        {
            try
            {
                if (BOLETO.rowPrincipal.IsBOLPagamentoNull())
                    return false;
                object[] Parametros;
                //rowPrincipal.BOLPagamento = Pagamento;
                //rowPrincipal.BOLValorPago = ValorPago;                    

                string ComandoVerifica = "SELECT 1 FROM [Lancamentos Bradesco] WHERE (Codcon = @P1) AND (Apartamento = @P2) AND (Bloco = @P3) AND ([N�mero Documento] = @P4)";

                decimal Total = 0;

                if (VirOleDb.TableAdapter.STTableAdapter.EstaCadastrado(ComandoVerifica, BOLETO.Condominio.CONCodigo, BOLETO.Apartamento.APTNumero, BOLETO.Apartamento.BLOCodigo, BOLETO.rowPrincipal.BOL))
                {
                    return false;
                };

                //object tiraDuvida = rowPrincipal.GetBOletoDetalheRows();
                //dBoletos.BOletoDetalheRow rowBOD = tiraDuvida

                foreach (dBoletos.BOletoDetalheRow rowBOD in BOLETO.rowPrincipal.GetBOletoDetalheRows())
                {
                    Framework.DSCentral.PLAnocontasRow rowPLA = Framework.DSCentral.DCentral.PlanoDeContas.FindByPLA(rowBOD.BOD_PLA);
                    Total += rowBOD.BODValor;
                    Parametros = new object[11];
                    Parametros[0] = BOLETO.Condominio.CONCodigo;
                    Parametros[1] = BOLETO.Apartamento.APTNumero;
                    Parametros[2] = BOLETO.Apartamento.BLOCodigo;
                    Parametros[3] = BOLETO.rowPrincipal.BOL;
                    Parametros[4] = (double)rowBOD.BODValor;
                    Parametros[5] = DateTime.Today;
                    Parametros[6] = BOLETO.rowPrincipal.BOLPagamento;
                    Parametros[7] = rowBOD.BOD_PLA;
                    if (rowPLA.PLADescricaoLivre)
                    {
                        string manobra = rowBOD.BODMensagem;
                        if (manobra.Contains(" (Pagamento Parcial)"))
                            manobra = manobra.Replace(" (Pagamento Parcial)", "").Trim();
                        Parametros[8] = manobra;
                    }
                    else
                        Parametros[8] = rowPLA.PLADescricao;


                    Parametros[9] = BOLETO.rowPrincipal.BOLCompetenciaMes.ToString("00") + BOLETO.rowPrincipal.BOLCompetenciaAno.ToString("0000");
                    Parametros[10] = BOLETO.rowPrincipal.BOL_CCD;
                    if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLanca, Parametros) != 1)
                    {
                        return false;
                    }

                    
                };


                decimal MultaPaga = Math.Round(BOLETO.rowPrincipal.BOLValorPago - Total, 2);
                if (MultaPaga > 0.009m)
                {
                    //Referencia ONENote : Aceto do boleto
                    //Incluir multa
                    //LOG += " Cadastrando Multa\r\n";
                    Parametros = new object[11];
                    Parametros[0] = BOLETO.Condominio.CONCodigo;
                    Parametros[1] = BOLETO.Apartamento.APTNumero;
                    Parametros[2] = BOLETO.Apartamento.BLOCodigo;

                    Parametros[3] = BOLETO.rowPrincipal.BOL;
                    Parametros[4] = (double)MultaPaga;
                    Parametros[5] = DateTime.Today;
                    Parametros[6] = BOLETO.rowPrincipal.BOLPagamento;
                    Parametros[7] = "120001";
                    Parametros[8] = "Multas";
                    Parametros[9] = BOLETO.rowPrincipal.BOLCompetenciaMes.ToString("00") + BOLETO.rowPrincipal.BOLCompetenciaAno.ToString("0000");
                    Parametros[10] = BOLETO.rowPrincipal.BOL_CCD;
                    if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLanca, Parametros) != 1)
                    {
                        return false;
                    };
                    //LOG += " P3->" + Parametros[3].ToString() + " BOLNNAntigo->" + rowPrincipal.BOLNNAntigo.ToString() + " NN->" + this.NN.ToString() + " - " + MultaPaga.ToString() + "\r\n";

                }
                else
                    if (MultaPaga < -0.009m)
                {
                    //Incluir multa
                    //LOG += " Cadastrando Multa\r\n";
                    Parametros = new object[11];
                    Parametros[0] = BOLETO.Condominio.CONCodigo;
                    Parametros[1] = BOLETO.Apartamento.APTNumero;
                    Parametros[2] = BOLETO.Apartamento.BLOCodigo;

                    Parametros[3] = BOLETO.rowPrincipal.BOL;
                    Parametros[4] = (double)MultaPaga;
                    Parametros[5] = DateTime.Today;
                    Parametros[6] = BOLETO.rowPrincipal.BOLPagamento;
                    Parametros[7] = "101000";
                    Parametros[8] = "Desconto";
                    Parametros[9] = BOLETO.rowPrincipal.BOLCompetenciaMes.ToString("00") + BOLETO.rowPrincipal.BOLCompetenciaAno.ToString("0000");
                    Parametros[10] = BOLETO.rowPrincipal.BOL_CCD;
                    if (VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLanca, Parametros) != 1)
                    {
                        return false;
                    };


                }





                return true;
            }
            catch
            {

                return false;
            }
        }
        */

        private void simpleButton7_Click_1(object sender, EventArgs e)
        {
            
        }

        private Boleto Super = null;

        private bool AddNoSuper(Boleto Novo)
        {
            if (Super == null)
                Super = new Boleto(Novo, dateEdit1.DateTime);
            else
                Super.addBolentonoSuper(Novo);
            return true;
        }


        private void cBotaoImpBol2_clicado_1(object sender, EventArgs e)
        {
            if (GridView_F.SelectedRowsCount < 1)
                return;
            List<Boleto> Agrupar = new List<Boleto>();
            foreach (int iSel in GridView_F.GetSelectedRows())
            {
                Boleto BolCandidato = new Boleto(((dBoletos.BOLetosRow)GridView_F.GetDataRow(iSel)).BOL);
                if (!BolCandidato.Encontrado)
                {
                    MessageBox.Show(string.Format("Erro: Boleto {0} n�o encontrado", BolCandidato.rowPrincipal.BOL));
                    return;
                }
                if (BolCandidato.rowPrincipal.BOLProtestado)
                {
                    MessageBox.Show(string.Format("Erro: Boleto {0} protestado", BolCandidato.rowPrincipal.BOL));
                    return;
                }
                if (!BolCandidato.rowPrincipal.IsBOLPagamentoNull())
                {
                    MessageBox.Show(string.Format("Erro: Boleto {0} quitado", BolCandidato.rowPrincipal.BOL));
                    return;
                }
                if (BolCandidato.rowPrincipal.BOLCancelado)
                {
                    MessageBox.Show(string.Format("Erro: Boleto {0} cancelado", BolCandidato.rowPrincipal.BOL));
                    return;
                }
                Agrupar.Add(BolCandidato);
            }
            Super = null;
            foreach (Boleto B in Agrupar)
            {
                AddNoSuper(B);
                B.GravaJustificativa(string.Format("Boleto inclu�do no super-boleto {0} com vencimento para {1:dd/MM/yyyy} com o valor de {2:n2}",
                                                    Super.rowPrincipal.BOL,
                                                    Super.rowPrincipal.BOLVencto,
                                                    B.valorfinal), "");
            }
            VirEmailNeon.EmailDiretoNeon.EmalST.UltimoEndereco = "";
            Super.Imprimir(cGeraSuper.BotaoClicado);
            if ((cGeraSuper.BotaoClicado == dllImpresso.Botoes.Botao.email) || (cGeraSuper.BotaoClicado == dllImpresso.Botoes.Botao.email_comprovante))
                if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoEndereco != "")
                    foreach (Boleto B in Agrupar)
                    {
                        B.GravaJustificativa("Enviado para:" + VirEmailNeon.EmailDiretoNeon.EmalST.UltimoEndereco, "");
                    }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            AtuaBotaoSuper();
        }


        private void AtuaBotaoSuper()
        {
            cGeraSuper.Enabled = false;
            if ((dateEdit1.EditValue != null) && (GridView_F.SelectedRowsCount >= 1))
            {
                DateTime DataNova = (DateTime)dateEdit1.EditValue;
                if (DataNova >= DateTime.Today)
                {
                    cGeraSuper.Enabled = true;
                }
            }
        }

        private void repositoryItemLookSuper_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if ((LinhaMae == null) || (LinhaMae.GetSuperBoletoDetalheRows().Length == 0))
                return;
            Boleto Super = new Boleto(LinhaMae.GetSuperBoletoDetalheRows()[0].SBD_SBO);
            if ((Super.Encontrado && Super.rowSBO != null))
            {
                using (cBoleto cBoleto = new cBoleto(Super, true))
                {
                    cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
                }
                calcEdit1.EditValue = null;
            }
            else
                MessageBox.Show("Boleto n�o encontrado");
        }

        private void BoletosGrade_Load(object sender, EventArgs e)
        {
            //cCompet1.Comp = new Framework.objetosNeon.Competencia(DateTime.Today);
            cCompet1.AjustaTela();
        }

        #region Registro
        private void cmdSeparaBoletos_Click(object sender, EventArgs e)
        {
            // if (BoletosRemessa == null) { BoletosRemessa = new List<Boleto>(); }

            if (GridView_F.GetSelectedRows().Length == 0)
            {
                GridView_F.SelectRow(GridView_F.FocusedRowHandle);
            };
            if (GridView_F.GetSelectedRows().Length > 0)
            {
                foreach (int i in GridView_F.GetSelectedRows())
                {
                    DataRowView DRV = (DataRowView)GridView_F.GetRow(i);
                    Boleto BolImp = new Boleto(((dBoletos.BOLetosRow)DRV.Row).BOL);
                    BoletosRemessa.Add(BolImp);
                };
            };

            MessageBox.Show("Finalizado");
        }

        private void cmdGeraRemessa_Click(object sender, EventArgs e)
        {            
            if ((BoletosRemessa == null) || (BoletosRemessa.Count == 0))
                return;
            string strNomeArquivo = "";
            BoletoRemessaBase BoletoRemessa = null;
            switch (BoletosRemessa[0].rowCON.CON_BCO)
            {
                case 237:
                    BoletoRemessa = new BoletoRemessaBra();
                    break;
                case 341:
                    BoletoRemessa = new BoletoRemessaItau();
                    break;
                case 33:
                    BoletoRemessa = new BoletoRemessaCNAB_SAN();
                    break;
                case 104:
                    BoletoRemessa = new BoletoRemessaCNAB_CX();
                    break;
                default:
                    throw new NotImplementedException(string.Format("Remessa: Banco {0} n�o configurado para registro de boleto\r\n", BoletosRemessa[0].rowCON.CON_BCO));
            }
            BoletoRemessa.IniciaRemessa();
            switch (BoletosRemessa[0].rowCON.CON_BCO)
            {
                case 237:
                    BoletoRemessa.Configura(int.Parse(BoletosRemessa[0].rowCON.CONAgencia),
                                            int.Parse(BoletosRemessa[0].rowCON.CONConta),
                                            BoletosRemessa[0].rowCON.CONDigitoConta,
                                            "Neon Imoveis",
                                            new DocBacarios.CPFCNPJ(BoletosRemessa[0].rowCON.CONCnpj),
                                            BoletosRemessa[0].CarteiraBanco.ToString(),
                                            Framework.DSCentral.EMP == 1 ? 602270 : 4854082,
                                            1);                                        
                    //Seta e insere boleto
                    foreach (Boleto bol in BoletosRemessa)
                        bol.Registrar(BoletoRemessa);                    
                    strNomeArquivo = BoletoRemessa.GravaArquivo(@"C:\Temp\Neon\ArquivosBradesco\Real\");
                    if (strNomeArquivo == "")
                        MessageBox.Show("Erro ao gerar arquivo: " + BoletoRemessa.UltimoErro.InnerException.Message);
                    else
                        MessageBox.Show("Arquivo Remessa gerado em: " + strNomeArquivo);
                    break;

                case 341:                    
                case 104:
                case 33:

                    BoletoRemessa.Configura( int.Parse(BoletosRemessa[0].rowCON.CONAgencia),
                                             int.Parse(BoletosRemessa[0].rowCON.CONConta),
                                             BoletosRemessa[0].rowCON.CONDigitoConta,
                                             BoletosRemessa[0].rowCON.CONNome,
                                             new DocBacarios.CPFCNPJ(BoletosRemessa[0].rowCON.CONCnpj),
                                             BoletosRemessa[0].CarteiraBanco.ToString(),
                                             int.Parse(BoletosRemessa[0].rowCON.CONCodigoCNR.Trim()),
                                             1);                                        
                    foreach (Boleto bol in BoletosRemessa)                    
                        bol.Registrar(BoletoRemessa);                                        
                    string Caminho = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\TMP\\";
                    if (!System.IO.Directory.Exists(Caminho))
                        System.IO.Directory.CreateDirectory(Caminho);
                    strNomeArquivo = BoletoRemessa.GravaArquivo(Caminho);
                    if (strNomeArquivo == "")
                    {
                        string Erro = "Sem nome\r\n";
                        Exception Ex = BoletoRemessa.UltimoErro;
                        while (Ex != null)
                        {
                            Erro += string.Format("{0}\r\n", Ex.Message);
                            Ex = Ex.InnerException;
                        }
                        MessageBox.Show(Erro);
                    }
                    else
                        MessageBox.Show("Arquivo Remessa gerado em: " + strNomeArquivo);
                    break;                    
                default:
                    throw new NotImplementedException(string.Format("BoletoGrade - N�o implementado para banco = {0}", BoletosRemessa[0].rowCON.CON_BCO));
            }
        }

        private void cmdLimpaBoletos_Click(object sender, EventArgs e)
        {
            _BoletosRemessa = null;
        }
        #endregion


        /// <summary>
        /// Retorna o e.mail do respons�vel pela unidade
        /// </summary>
        /// <returns>e.mail</returns>        
        public string EmailResponsavel
        {
            get
            {
                if (lookupBlocosAptos_F1.APT_Sel == -1)
                    return "";
                if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    return "virtual@virweb.com.br";
                ABS_Apartamento Apartamento = ABS_Apartamento.GetApartamento(lookupBlocosAptos_F1.APT_Sel);
                if (Apartamento.encontrado)
                    return Apartamento.EmailResponsavel;
                else
                    return "virtual@virweb.com.br";
            }
        }

        private void cBotaoImpNeon1_clicado(object sender, dllBotao.BotaoArgs args)
        {
            Impresso.ImpInadimplencia ImpVencidos = new Boletos.Boleto.Impresso.ImpInadimplencia();
            ImpVencidos.bindingSource1.DataSource = dBoletos.BOLetos;
            ImpVencidos.SetaDados(DateTime.Today, lookupBlocosAptos_F1.CON_selrow.CONNome, false);
            ImpVencidos.xrTitulo.Text = "Boletos (" + DateTime.Today.ToString("dd/MM/yyyy") + ")";
            BotExtrato.Impresso = ImpVencidos;
            BotExtrato.EmailDefault = EmailResponsavel;
            return;
        }

        private void BoletosGrade_cargaFinal(object sender, EventArgs e)
        {
            cCompet1.Comp = new Framework.objetosNeon.Competencia(DateTime.Today);
            cCompet1.AjustaTela();
        }



        /// <summary>
        /// Recalcula os dados
        /// </summary>
        public override void RefreshDados()
        {
            timerCalcular.Enabled = true;
        }

        private void timerCalcular_Tick(object sender, EventArgs e)
        {
            timerCalcular.Enabled = false;
            if (APTs_Alterados != null)
            {
                if (APTs_Alterados.Contains(lookupBlocosAptos_F1.APT_Sel))
                    Calcular();
                APTs_Alterados.Clear();
            }

        }

        private static List<int> APTs_Alterados;

        /// <summary>
        /// Registra a altera��o de boletos em uma unidade para que seja feito o re-calculo
        /// </summary>
        /// <param name="APT"></param>
        public static void RegistraAlteracao(int APT)
        {
            if (APTs_Alterados == null)
                APTs_Alterados = new List<int>();
            APTs_Alterados.Add(APT);
        }

        private void BotCreditos_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView Grid = (DevExpress.XtraGrid.Views.Grid.GridView)GridControl_F.FocusedView;
            if (Grid == null)
                return;
            dBoletos.BOletoDetalheRow rowBOD = (dBoletos.BOletoDetalheRow)Grid.GetFocusedDataRow();
            int? APTras = null;
            if (!rowBOD.IsBOD_APTNull())
                APTras = rowBOD.BOD_APT;
            string ComandoBusca =
"SELECT        ARquivoLido.ARL_Data\r\n" +
"FROM            BOLetos INNER JOIN\r\n" +
"                         ARquivoLido ON BOLetos.BOL_ARL = ARquivoLido.ARL\r\n" +
"WHERE        (BOLetos.BOL = @P1);";
            DateTime? DataDoPagamento = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_DateTime(ComandoBusca, rowBOD.BOD_BOL);

            //RasCredito Ras = new RasCredito(rowBOD.BOD_CON, rowBOD.BOD, APTras,rowBOD.BOLetosRow.BOLPagamento, rowBOD.BODValor);
            RasCredito Ras = new RasCredito(rowBOD.BOD_CON, rowBOD.BOD, APTras, DataDoPagamento.GetValueOrDefault(rowBOD.BOLetosRow.BOLPagamento), rowBOD.BODValor);
            if (!Ras.Rastreavel)
                return;
            if (Ras.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                Calcular();
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.Name == colLupa.Name)
            {
                DevExpress.XtraGrid.Views.Grid.GridView Grid = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                dBoletos.BOletoDetalheRow rowBOD = (dBoletos.BOletoDetalheRow)Grid.GetDataRow(e.RowHandle);
                if ((rowBOD == null) || rowBOD.BOLetosRow.IsBOLPagamentoNull())
                    return;
                if (rowBOD.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraC))
                {
                    e.RepositoryItem = BotCreditos;
                }
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Alterar();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
           
        }

        private void BtCorrecao_Click(object sender, EventArgs e)
        {
            const string ComandoBusca =
"SELECT        dbo.BOLetos.BOL, dbo.BOLetos.BOLValorPrevisto, dbo.BOLetos.BOLValorPago, dbo.BOLetos.BOLValorOriginal, dbo.BOletoDetalhe.BOD_PLA, dbo.BOletoDetalhe.BODValor, \r\n" +
"              dbo.BOletoDetalhe.BOD, BOletoDetalhe_1.BOD AS BODDesconto, BOletoDetalhe_1.BOD_BOL, BOLetos_1.BOLPagamento, BOLetos_1.BOLValorPrevisto AS BOLValorPrevistoDescontado\r\n" +
"FROM            dbo.BOLetos INNER JOIN\r\n" +
"                         dbo.BOletoDetalhe ON dbo.BOLetos.BOL = dbo.BOletoDetalhe.BOD_BOL INNER JOIN\r\n" +
"                         dbo.BODxBOD ON dbo.BOletoDetalhe.BOD = dbo.BODxBOD.BODOrigem INNER JOIN\r\n" +
"                         dbo.BOletoDetalhe AS BOletoDetalhe_1 ON dbo.BODxBOD.BODDestino = BOletoDetalhe_1.BOD LEFT OUTER JOIN\r\n" +
"                         dbo.BOLetos AS BOLetos_1 ON BOletoDetalhe_1.BOD_BOL = BOLetos_1.BOL\r\n" +
"WHERE        (dbo.BOLetos.BOL = @P1);";
            const string ComandoCorrecaoBoleto = "UPDATE BOLetos SET BOLValorPago = @P1,BOLValorPrevisto = BOLValorOriginal WHERE (BOL = @P2) AND (BOLValorPago = @P3)";
            const string ComandoDel1 = "delete from BODxBOD where BODOrigem = @P1";
            const string ComandoDel2 = "delete from BOletoDetalhe where BOD = @P1";
            const string ComandoCorrecaoBoletoDescontado = "update boletos set BOLValorPrevisto = @P1, BOLValorOriginal = @P2 where BOL = @P3 and BOLValorPrevisto = @P4";
            System.Data.DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoBusca, LinhaMae.BOL);
            if (DR == null)
            {
                MessageBox.Show("N�o encontrado");
                return;
            }
            decimal ValorReal_Erro = (decimal)DR["BOLValorPrevisto"];
            decimal BOLValorOriginal = (decimal)DR["BOLValorOriginal"];
            decimal Erro = ValorReal_Erro - BOLValorOriginal;
            decimal ValorExtra = (decimal)DR["BODValor"];
            decimal ValorTotal = (decimal)DR["BOLValorPago"];
            int BODErrado1 = (int)DR["BOD"];
            int BODErrado2 = (int)DR["BODDesconto"];
            int? BOLDescontado = null;
            if (DR["BOD_BOL"] != DBNull.Value)
                BOLDescontado = (int)DR["BOD_BOL"];
            if (Erro != ValorExtra)
            {
                MessageBox.Show("Valor incorreto");
                return;
            }
            bool tirardesconto = true;
            if(BOLDescontado.HasValue && (DR["BOLPagamento"] != DBNull.Value))
            {
                MessageBox.Show("Boleto com desconto quitado");
                tirardesconto = false;
            }
            VirMSSQL.TableAdapter.AbreTrasacaoSQL("Corre��o");
            try
            {
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoCorrecaoBoleto, ValorTotal - Erro, LinhaMae.BOL, ValorTotal);
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoDel1, BODErrado1);
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoDel2, BODErrado1);
                if (tirardesconto)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoDel2, BODErrado2);
                    if (BOLDescontado.HasValue)
                    {
                        decimal ValorPrevistoComErro = (decimal)DR["BOLValorPrevistoDescontado"];
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoCorrecaoBoletoDescontado, ValorPrevistoComErro - Erro, ValorPrevistoComErro - Erro, BOLDescontado.Value, ValorPrevistoComErro);
                    }
                }
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
                throw;
            }

        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            List<int> Selecionadas = new List<int>(GridView_F.GetSelectedRows());
            if (Selecionadas.Count == 0)
            {
                MessageBox.Show("Nenhuma linha selecionada!!");
                return;
            }
            if ((GridView_F.FocusedRowHandle >= 0) && (!Selecionadas.Contains(GridView_F.FocusedRowHandle)))
            {
                MessageBox.Show("Linha em foco n�o est� selecionada!!");
                return;
            }

            GeraBoleto.GeraBol GeraBol1 = new GeraBoleto.GeraBol(this);
            SortedList<int, Boleto> BoletosGerados = new SortedList<int, Boleto>();

            //if (NaoImprimiveis == null)
            //    NaoImprimiveis = new List<StatusBoleto>(new StatusBoleto[] { StatusBoleto.Acumulado, StatusBoleto.Cancelado, StatusBoleto.EmProducao, StatusBoleto.Erro, StatusBoleto.Saldinho });

            //ArrayList BoletosImpressos = new ArrayList();
            Framework.objetosNeon.Competencia comp = null;
            bool? ComCondominio = null;
            foreach (int i in Selecionadas)
            {
                DataRowView DRV = (DataRowView)GridView_F.GetRow(i);
                Boleto BolImp = new Boleto((dBoletos.BOLetosRow)DRV.Row);
                if (BolImp.BOLStatus != StatusBoleto.Valido)                    
                    continue;
                if (BolImp.rowPrincipal.BOLVencto < DateTime.Today)
                    continue;
                if (comp == null)
                    comp = BolImp.Competencia;
                else
                    if (comp != BolImp.Competencia)
                       {
                           MessageBox.Show("Boletos de compet�ncias diferentes");
                           return;
                       };
                if (!ComCondominio.HasValue)
                    ComCondominio = (BolImp.TipoCRAIE == BOLTipoCRAI.condominio);
                else
                    if (ComCondominio.Value != (BolImp.TipoCRAIE == BOLTipoCRAI.condominio))
                        {
                            MessageBox.Show("Boletos de tipos diferentes");
                            return;
                        }
                BoletosGerados.Add(BolImp.BOL, BolImp);
            };
            
            if (BoletosGerados.Count > 0)                            
                GeraBol1.EmissaoEfetiva(CON, comp, ComCondominio.Value, BoletosGerados);                         
        }

        private void SBProprietario_Click(object sender, EventArgs e)
        {
            if (Apartamento != null)
                Apartamento.AbrecApartamento(true);
        }

        private void SBInquilino_Click(object sender, EventArgs e)
        {
            if (Apartamento != null)
                Apartamento.AbrecApartamento(false);
        }

        private void BCorItau_Click(object sender, EventArgs e)
        {
            if (Framework.DSCentral.USU != 30)
                return;
            dBoletos.BOLetosRow row = (dBoletos.BOLetosRow)GridView_F.GetFocusedDataRow();
            decimal taxa = 0;
            if (!VirInput.Input.Execute("taxa", ref taxa))
                return;
            string Comando1 =
"SELECT        dbo.BOletoDetalhe.BOD, dbo.BOletoDetalhe.BODValor, dbo.BODxBOD.BODOrigem, dbo.BODxBOD.BODDestino, BOletoDetalhe_1.BOD_PLA, BOletoDetalhe_1.BODValor AS Expr1, dbo.BOLetos.BOL, \r\n" +
"                         dbo.BOLetos.BOLPagamento, dbo.BOLetos.BOLValorPrevisto, dbo.BOLetos.BOLValorPago\r\n" +
"FROM            dbo.BOletoDetalhe LEFT OUTER JOIN\r\n" +
"                         dbo.BODxBOD ON dbo.BOletoDetalhe.BOD = dbo.BODxBOD.BODOrigem LEFT OUTER JOIN\r\n" +
"                         dbo.BOletoDetalhe AS BOletoDetalhe_1 ON dbo.BODxBOD.BODDestino = BOletoDetalhe_1.BOD LEFT OUTER JOIN\r\n" +
"                         dbo.BOLetos ON BOletoDetalhe_1.BOD_BOL = dbo.BOLetos.BOL\r\n" +
"WHERE        (dbo.BOletoDetalhe.BOD_BOL = @P1) AND (dbo.BOletoDetalhe.BOD_PLA = '910002');";

            DataTable Dados = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(Comando1,row.BOL);
            if (Dados.Rows.Count != 1)
            {
                MessageBox.Show("Erro 1 linhas");
                return;
            }
            if (Dados.Rows[0]["BOLValorPago"] != DBNull.Value)
            {
                MessageBox.Show("Erro Segundo Boleto Pago");
                return;
            }
            decimal ValorErrado = (decimal)Dados.Rows[0]["BODValor"];
            int BOD = (int)Dados.Rows[0]["BOD"];
            int BODDestino = (Dados.Rows[0]["BODDestino"] == DBNull.Value) ? 0 :  (int)Dados.Rows[0]["BODDestino"];
            decimal BOLValorPrevistoBoleto2 = (Dados.Rows[0]["BOLValorPrevisto"] == DBNull.Value) ? 0 : (decimal)Dados.Rows[0]["BOLValorPrevisto"];
            int BOL2 = Dados.Rows[0]["BOL"] == DBNull.Value ? 0 : (int)Dados.Rows[0]["BOL"];
            if (ValorErrado == taxa)
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("");
                    if (BODDestino != 0)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete BODxBOD where BODOrigem = @P1", BOD);
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete BOletoDetalhe where BOD = @P1", BODDestino);
                    }
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete BOletoDetalhe where BOD = @P1", BOD);                    
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOletos set BOLValorPago = @P1 where BOL = @P2", row.BOLValorPago - taxa, row.BOL);
                    if (BOL2 != 0)
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOletos set BOLValorPrevisto = @P1 where BOL = @P2", BOLValorPrevistoBoleto2 + taxa, BOL2);
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update Arquivolido set ARLValor = @P1 where ARL = @P2", row.BOLValorPago - (2*taxa), row.BOL_ARL);
                    VirMSSQL.TableAdapter.CommitSQL();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                }
            }
            else
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("");
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOletoDetalhe set BODValor = @P1 where BOD = @P2",ValorErrado - taxa, BOD);
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOletoDetalhe set BODValor = @P1 where BOD = @P2", - ValorErrado + taxa, BODDestino);
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOletos set BOLValorPago = @P1 where BOL = @P2", row.BOLValorPago - taxa, row.BOL);
                    if (BOL2 != 0)
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOletos set BOLValorPrevisto = @P1 where BOL = @P2", BOLValorPrevistoBoleto2 + taxa, BOL2);
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update Arquivolido set ARLValor = @P1 where ARL = @P2", row.BOLValorPago - (2 * taxa), row.BOL_ARL);
                    VirMSSQL.TableAdapter.CommitSQL();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                }
            }
        }

        private void BCorItau1_Click(object sender, EventArgs e)
        {
            int BOL = 0;
            if (VirInput.Input.Execute("N�mero", out BOL))
            {
                BoletoRemessaBase BoletoRemessa = new BoletoRemessaItau();
                BoletoRemessa.IniciaRemessa();
                Boleto BolTeste = new Boleto(BOL);
                BoletoRemessa.Configura(int.Parse(BolTeste.rowCON.CONAgencia),
                                                             int.Parse(BolTeste.rowCON.CONConta),
                                                             BolTeste.rowCON.CONDigitoConta,
                                                             BolTeste.rowCON.CONNome,
                                                             new DocBacarios.CPFCNPJ(BolTeste.rowCON.CONCnpj),
                                                             BolTeste.CarteiraBanco.ToString(),
                                                             int.Parse(BolTeste.rowCON.CONCodigoCNR.Trim()),
                                                             1);
                BolTeste.Registrar(BoletoRemessa);
                string Caminho = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\TMP\\";
                if (!System.IO.Directory.Exists(Caminho))
                    System.IO.Directory.CreateDirectory(Caminho);
                string strNomeArquivo = BoletoRemessa.GravaArquivo(Caminho);
                if (strNomeArquivo == "")
                {
                    string Erro = "Sem nome\r\n";
                    Exception Ex = BoletoRemessa.UltimoErro;
                    while (Ex != null)
                    {
                        Erro += string.Format("{0}\r\n", Ex.Message);
                        Ex = Ex.InnerException;
                    }
                    MessageBox.Show(Erro);
                }
                else
                    MessageBox.Show("Arquivo Remessa gerado em: " + strNomeArquivo);                
            }
        }
    }
    
}

