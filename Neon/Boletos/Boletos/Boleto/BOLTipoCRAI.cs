﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boletos.Boleto
{
    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum BOLTipoCRAI
    {
        /// <summary>
        /// C
        /// </summary>
        condominio = 0,
        /// <summary>
        /// A
        /// </summary>
        acordo = 1,
        /// <summary>
        /// I
        /// </summary>
        incluido = 2,
        /// <summary>
        /// Não usar
        /// </summary>
        [Obsolete("Antigo T",true)]
        Transferencia = 3,
        /// <summary>
        /// 
        /// </summary>
        [Obsolete("Antigo G",true)]
        geral = 4,
        /// <summary>
        /// S
        /// </summary>
        Seguro = 5,
        /// <summary>
        /// E
        /// </summary>
        Extra = 6,
        /// <summary>
        /// R
        /// </summary>
        Rateio = 7,
    }

    

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumBOLTipoCRAI : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumBOLTipoCRAI(Type Tipo) :
            base(Tipo)
        {
        }

        /// <summary>
        /// Tipo do bolote
        /// </summary>
        /// <param name="CRAI"></param>
        /// <returns></returns>
        public static BOLTipoCRAI Parse(string CRAI)
        {
            switch (CRAI)
            {
                case "A":
                    return BOLTipoCRAI.acordo;
                case "C":
                    return BOLTipoCRAI.condominio;
                case "E":
                    return BOLTipoCRAI.Extra;                
                case  "R":
                    return BOLTipoCRAI.Rateio;
                case "S":
                    return BOLTipoCRAI.Seguro;
                case "I":
                default:
                    return BOLTipoCRAI.incluido;
                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_BOLTipoCRAI"></param>
        /// <returns></returns>
        public static string Valor(BOLTipoCRAI _BOLTipoCRAI)
        {
            switch (_BOLTipoCRAI)
            {
                case BOLTipoCRAI.condominio:
                    return "C";                    
                case BOLTipoCRAI.acordo:
                    return "A";                    
                case BOLTipoCRAI.incluido:
                    return "I";                    
                case BOLTipoCRAI.Seguro:
                    return "S";
                case BOLTipoCRAI.Rateio:
                    return "R";
                case BOLTipoCRAI.Extra:
                    return "E";
                default:
                    return "I";
            }
        }

        private static virEnumBOLTipoCRAI _virEnumBOLTipoCRAISt;

        /// <summary>
        /// VirEnum estático para campo BOLTipoCRAI
        /// </summary>
        public static virEnumBOLTipoCRAI virEnumBOLTipoCRAISt
        {
            get
            {
                if (_virEnumBOLTipoCRAISt == null)
                {
                    _virEnumBOLTipoCRAISt = new virEnumBOLTipoCRAI(typeof(BOLTipoCRAI));
                    _virEnumBOLTipoCRAISt.GravaNomes(BOLTipoCRAI.condominio, "Condomínio");
                    _virEnumBOLTipoCRAISt.GravaNomes(BOLTipoCRAI.acordo, "Acordo");
                    _virEnumBOLTipoCRAISt.GravaNomes(BOLTipoCRAI.incluido, "Incluído");
                    _virEnumBOLTipoCRAISt.GravaNomes(BOLTipoCRAI.Seguro, "Seguro conteúdo");
                    _virEnumBOLTipoCRAISt.GravaNomes(BOLTipoCRAI.Rateio, "Rateio");
                    _virEnumBOLTipoCRAISt.GravaNomes(BOLTipoCRAI.Extra, "Boleto Extra");
                }
                return _virEnumBOLTipoCRAISt;
            }
        }
    }
}
