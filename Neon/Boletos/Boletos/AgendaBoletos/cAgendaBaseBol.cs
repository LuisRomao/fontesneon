using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Boletos.AgendaBoletos
{

    public partial class cAgendaBaseBol : Calendario.Agenda.cAgenda
    {
        public cAgendaBaseBol()
        {
            InitializeComponent();
        }

        protected override void Atualizar()
        {
            //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from agendabase");
            Calendario.Agenda.dAGendaBase.dAGendaBaseSt.FillAGendaBase(1);            
            AgendaBoletos.AgendaBoletosSt.Atualizar(this);
        }
    }
}

