using System;
using System.Collections.Generic;
using System.Text;

namespace Boletos.AgendaBoletos
{
    /// <summary>
    /// Classe de inteligencia para AgendaBoletos
    /// </summary>
    public class AgendaBoletos
    {
        private static AgendaBoletos _AgendaBoletosSt;

        /// <summary>
        /// Instancia estática de AgendaBoletos
        /// </summary>
        public static AgendaBoletos AgendaBoletosSt
        {
            get
            {
                if (_AgendaBoletosSt == null)
                    _AgendaBoletosSt = new AgendaBoletos();
                return _AgendaBoletosSt;
            }
        }

        public int Antecedencia = 16;
        public int duracao = 30;
        

        public void Atualizar(System.Windows.Forms.Control Chamador) {
            CompontesBasicos.Espera.cEspera TelaEspera=null;
            Calendario.Agenda.dAGendaBase.dAGendaBaseSt.FillAGendaBase(1);
            if (Chamador != null)
            {
                TelaEspera = new CompontesBasicos.Espera.cEspera(Chamador);
                TelaEspera.Espere("Atualizando");
                TelaEspera.AtivaGauge(Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.Count);
            }
            foreach (Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow CONrow in Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS)
            {
                
                if (Chamador != null)
                {
                    TelaEspera.Gauge();
                    if (TelaEspera.Abortar)
                    {
                        TelaEspera.Visible = false;
                        TelaEspera = null;
                        return;
                    }
                }
                Calendario.Agenda.dAGendaBase.AGendaBaseRow AGBrow;
                AGBrow = null;
                foreach (Calendario.Agenda.dAGendaBase.AGendaBaseRow XAGBrow in Calendario.Agenda.dAGendaBase.dAGendaBaseSt.AGendaBase)
                    if (XAGBrow.AGB_CON == CONrow.CON)
                    {
                        AGBrow = XAGBrow;
                        break;
                    }
                if (CONrow.CONStatus == 1)
                {
                    Framework.objetosNeon.Competencia Compet = Framework.objetosNeon.Competencia.Proxima(CONrow.CON);
                    
                    if ((AGBrow == null) || ((AGBrow.AGBCompetenciaAno != Compet.Ano) || (AGBrow.AGBCompetenciaMes != Compet.Mes)))
                    {
                        try
                        {
                            if (AGBrow == null)
                                 AGBrow = Calendario.Agenda.dAGendaBase.dAGendaBaseSt.AGendaBase.NewAGendaBaseRow();
                            DateTime DatataVenco = Compet.VencimentoPadrao;
                            AGBrow.AGB_TAG = 1;
                            AGBrow.AGB_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                            AGBrow.AGB_CON = CONrow.CON;
                            AGBrow.AGBCompetenciaAno = Compet.Ano;
                            AGBrow.AGBCompetenciaMes = Compet.Mes;

                            AGBrow.AGBDataInicio = DatataVenco.AddDays(-Antecedencia);
                            if (AGBrow.AGBDataInicio < DateTime.Today)                            
                                AGBrow.AGBDataInicio = DateTime.Today;

                            //AGBrow.AGBDataLimite = DatataVenco;
                            AGBrow.AGBDataTermino = AGBrow.AGBDataInicio.AddDays(1);
                            AGBrow.AGBDiaTodo = true;





                            if (DatataVenco < DateTime.Today)
                            {
                                AGBrow.AGBDataInicio = DatataVenco;
                                AGBrow.AGBDataTermino = DateTime.Today.AddDays(1);
                            }
                           
                            
                            
                            
                            AGBrow.AGBDescricao = CONrow.CONCodigo + " - " + CONrow.CONNome;
                            AGBrow.AGBSubDescricao = Compet.ToString();
                            
                            if(AGBrow.RowState == System.Data.DataRowState.Detached)
                                 Calendario.Agenda.dAGendaBase.dAGendaBaseSt.AGendaBase.AddAGendaBaseRow(AGBrow);
                            Calendario.Agenda.dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.Update(AGBrow);
                            AGBrow.AcceptChanges();
                        }
                        catch (Exception e){
                            System.Windows.Forms.MessageBox.Show("Condomínio: "+CONrow.CONCodigo+"\r\n"+e.Message);
                        };
                    }
                    
                }
                else { 
                    if (AGBrow != null){
                        AGBrow.Delete();
                        dAGendaBoletos.dAGendaBoletosSt.AGendaBoletosTableAdapter.Update(AGBrow);
                    }
                }
            };
            if (Chamador != null)
            {
                TelaEspera.Visible = false;
                TelaEspera = null;
            }

        }
    }
}
