using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Boletos.AgendaBoletos
{
    /// <summary>
    /// Componete visual para agendaboletos
    /// </summary>
    public partial class cAgendaBoletos : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// construtor padrao
        /// </summary>
        public cAgendaBoletos()
        {
            InitializeComponent();
            Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CarregaCondominios();
            CondominiosbindingSource.DataSource = Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST;
            AGendaBoletosbindingSource.DataSource = Calendario.Agenda.dAGendaBase.dAGendaBaseSt;
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            AgendaBoletos.AgendaBoletosSt.Atualizar(this);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from agendabase");
            Calendario.Agenda.dAGendaBase.dAGendaBaseSt.FillAGendaBase(1);
            
            AgendaBoletos.AgendaBoletosSt.Atualizar(this);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from agendabase");
            Calendario.Agenda.dAGendaBase.dAGendaBaseSt.FillAGendaBase(1);
            
            AgendaBoletos.AgendaBoletosSt.Atualizar(this);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from agendabase");
            Calendario.Agenda.dAGendaBase.dAGendaBaseSt.FillAGendaBase(1);
            
            AgendaBoletos.AgendaBoletosSt.Atualizar(this);
        }
    }
}

