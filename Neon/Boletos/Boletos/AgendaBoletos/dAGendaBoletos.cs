﻿namespace Boletos.AgendaBoletos {

    /// <summary>
    /// DataSet principal para AGendaBoletos
    /// </summary>
    partial class dAGendaBoletos
    {
        private static dAGendaBoletos _dAGendaBoletosSt;

        /// <summary>
        /// dataset estático:dAGendaBoletos
        /// </summary>
        public static dAGendaBoletos dAGendaBoletosSt
        {
            get
            {
                if (_dAGendaBoletosSt == null)
                {
                    _dAGendaBoletosSt = new dAGendaBoletos();
                    //_dAGendaBoletosSt.AGendaBoletosTableAdapter.Fill(_dAGendaBoletosSt.AGendaBoletos);
                }
                return _dAGendaBoletosSt;
            }
        }

        private dAGendaBoletosTableAdapters.AGendaBoletosTableAdapter aGendaBoletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: AGendaBoletos
        /// </summary>
        public dAGendaBoletosTableAdapters.AGendaBoletosTableAdapter AGendaBoletosTableAdapter
        {
            get
            {
                if (aGendaBoletosTableAdapter == null)
                {
                    aGendaBoletosTableAdapter = new dAGendaBoletosTableAdapters.AGendaBoletosTableAdapter();
                    aGendaBoletosTableAdapter.TrocarStringDeConexao();
                };
                return aGendaBoletosTableAdapter;
            }
        }
    }
}
