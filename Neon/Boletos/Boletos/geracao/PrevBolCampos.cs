/*
29/04/2014 LH e-mail opcional - cancelamento de emiss�o parcial 13.2.8.34
30/04/2014 LH e-mail opcional - cancelamento de emiss�o parcial 13.2.8.38
13/05/2014 13.2.8.48 LH reativa��o do XML
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Framework.objetosNeon;
using AbstratosNeon;
using VirEnumeracoes;
using VirEnumeracoesNeon;


namespace Boletos.geracao
{
    /// <summary>
    /// Previsao de boletos
    /// </summary>
    public partial class PrevBolCampos : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// Linha m�e
        /// </summary>
        public Boletos.geracao.dPrevBolCampos.PrevisaoBOletosRow linhamae = null;

        /// <summary>
        /// Compet�ncia
        /// </summary>
        public Competencia compet = null;

        private dPrevBolCampos.CONDOMINIOSRow Condrow = null;
        /// <summary>
        /// 
        /// </summary>
        public bool impressaopermitida = true;        

        /// <summary>
        /// Carrega os dados
        /// </summary>
        public void carregadados(){                        
            linhamae = (Boletos.geracao.dPrevBolCampos.PrevisaoBOletosRow)(dPrevBolCampos.PrevisaoBOletos.Rows[0]);
            

            Condrow = (dPrevBolCampos.CONDOMINIOSRow)dPrevBolCampos.CONDOMINIOS.Rows[0];
            dPrevBolCampos.BOletoDetalheTableAdapter.FillRegistro(dPrevBolCampos.BOletoDetalhe, linhamae.PBO);
            dPrevBolCampos.APTLoockupTableAdapter.Fill(dPrevBolCampos.APTLoockup, Condrow.CON);
            dPrevBolCampos.ListaPaGamentosEspecificosTableAdapter.Fill(dPrevBolCampos.ListaPaGamentosEspecificos, Condrow.CON);
            somenteleitura = false;

            /*
            foreach (dPrevBolCampos.BOletoDetalheRow rowBOD in dPrevBolCampos.BOletoDetalhe)
            {
                if (!rowBOD.IsBOD_BOLNull())
                {
                    somenteleitura = true;
                    break;
                }
            };
            */

            foreach (dPrevBolCampos.BOletoDetalheRow rowBOD in dPrevBolCampos.BOletoDetalhe)
            {
                if (!rowBOD.IsBOLStatusRegistroNull())
                {
                    if (rowBOD.BOLStatusRegistro == (int)StatusRegistroBoleto.ErroNoRegistro)
                        BotRegNov.Visible = true;
                    if ((rowBOD.BOLStatusRegistro ==  (int)StatusRegistroBoleto.BoletoRegistrando)                    
                        ||
                        (rowBOD.BOLStatusRegistro ==  (int)StatusRegistroBoleto.BoletoAguardandoRetorno)
                        )
                    {
                        impressaopermitida = false;
                       
                    }
                }
            };

            //if (linhamae.PBOGerado && !somenteleitura)
            //    linhamae.PBOGerado = false;
            //if (!linhamae.PBOGerado && somenteleitura)
            //    linhamae.PBOGerado = true;
            if (VerificacaoDeDados())
            {
                switch (Condrow.CONTipoFR) {
                    case "P": labelFundoReserva.Text = Condrow.CONValorFR.ToString("0.##") + " % dos boletos";
                              ValorFundo.Enabled = false;
                        break;
                    case "F": labelFundoReserva.Text = "Valor fixo por unidade: "+Condrow.CONValorFR.ToString("0.00");
                        break;
                    default: labelFundoReserva.Text = Condrow.CONTipoFR;
                        break;
                }
            }
            else
                labelFundoReserva.Text = "";
            
            pBOVencimentoDateEdit.Properties.MinValue = DateTime.Today.AddDays(3);
            //pBOVencimentoDateEdit.Properties.MaxValue = compet.CalculaVencimento(Condrow.CON).AddDays(30);
            pBOVencimentoDateEdit.Properties.MaxValue = compet.VencimentoPadrao.AddDays(30);
            if (dPrevBolCampos.BOletoDetalhe.Rows.Count == 0)
                btnConfirmar_F.Enabled = false;
            cBotaoImpNeon1.Enabled = false;
            cBotaoImpNeon1.Enabled = true;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public PrevBolCampos()
        {
            InitializeComponent();
            dPlanosSeguroConteudoBindingSource.DataSource = Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt;
            Boleto.Boleto.VirEnumStatusRegistroBoleto.CarregaEditorDaGrid(colBOL_StatusRegistro);
            Boleto.Boleto.VirEnumStatusBoleto.CarregaEditorDaGrid(colBOLStatus);
            richEditControl1.Document.Sections[0].Margins.Left = 0;
            richEditControl1.Document.Sections[0].Margins.Top = 0;
            //if (CompontesBasicos.FormPrincipalBase.USULogado == 30)
              //  button1.Visible = true;
        }

        /// <summary>
        /// Grava as altera��es
        /// </summary>
        /// <returns>Se foi gravado</returns>
        /// <remarks>Tem pendencias (Todo)</remarks>
        public override bool Update_F()
        {            
            bool retorno = false;
            try
            {
                retorno = base.Update_F();                
            }
            catch
            {
                //Todo: este catch na verdade nao funciona pos o componente captura o erro e nao reginstr em lugar nenhum sua ocorencia !!!
                MessageBox.Show("ERRO: j� foi criada uma previs�o para esta compet�ncia.\r\nVerifique se n�o existe um segundo formul�rio aberto!!");
            }
            if (retorno)
            {                
                    System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
                    foreach (dPrevBolCampos.BOletoDetalheRow rowdet in dPrevBolCampos.BOletoDetalhe.Rows)
                        if (rowdet.BODValor == 0)
                            Apagar.Add(rowdet);
                    foreach (dPrevBolCampos.BOletoDetalheRow rowdet in Apagar)
                        rowdet.Delete();
                    dPrevBolCampos.PrevisaoBOletosTableAdapter.Update(dPrevBolCampos.PrevisaoBOletos);
                    dPrevBolCampos.PrevisaoBOletos.AcceptChanges();                    
                    dPrevBolCampos.BOletoDetalheTableAdapter.Update(dPrevBolCampos.BOletoDetalhe);
                    dPrevBolCampos.BOletoDetalhe.AcceptChanges();                    
            }            
            return retorno;
        }

        private Rateios.dRateios.ListaFracaoDataTable TabLista=null;
              
        private decimal _ValorBrutoCalc = 0;

        private decimal ValorBrutoCalc 
        {
            get 
            {
                if (_ValorBrutoCalc == 0)
                    _ValorBrutoCalc = CalculaValorBruto();
                return _ValorBrutoCalc;
            }
        }

        //private bool ModeloAntigo = false;

        private decimal CalculaValorBruto(){
            decimal ValorLiquido = linhamae.PBOValorTotal;            
            //Lista de fracao ideal sem desconto do sindico porem corrigida para 100% (Bruta) PG 44                     
            TabLista = Rateios.dRateios.ListaFracaoTableAdapter.GetData(linhamae.PBO_CON);
            Rateios.dRateios.AjustaDescontos(Boletos.Rateios.dRateios.TiposDesconto.Condominio, TabLista);
            dRateios1.CalculaFracaoCorrigida(Condrow.CONFracaoIdeal ? Rateios.dRateios.TipoSimula.ComFracao : Rateios.dRateios.TipoSimula.SemFracao, TabLista, linhamae.PBOValorTotal);
            ValorBruto.Text = string.Format("Valor Nominal = R$ {0:n2}",dRateios1.Nominal);
            return dRateios1.Nominal;
            /*
            decimal SomatoriaDescontos = 0;
            //ModeloAntigo = false;
            foreach (Boletos.Rateios.dRateios.ListaFracaoRow linhaLista in TabLista)
            {
                if(linhaLista.IsCDRDescontoCondominioNull())
                    continue;

                if (linhaLista.CDRDescontoCondominio == -1)
                    ModeloAntigo = true;

                if (ModeloAntigo)
                {
                    if (!linhaLista.IsCDR_CGONull())
                    {
                        if (linhaLista.CDR_CGO == 1)
                            if (Condrow.CONCondominioSindico < 100)
                                SomatoriaDescontos += (linhaLista.FracaoCorrigida * ((100M - Condrow.CONCondominioSindico) / 100));
                        if (linhaLista.CDR_CGO == 2)
                            if (Condrow.CONCondominioSubSindico < 100)
                                SomatoriaDescontos += (linhaLista.FracaoCorrigida * ((100M - Condrow.CONCondominioSubSindico) / 100));
                    };
                }
                else
                {
                    if (linhaLista.CDRDescontoCondominio > 0)
                        if (!linhaLista.CDRDescontoCondominioValor)
                            //SomatoriaDescontos -= linhaLista.CDRDescontoCondominio;
                            //else
                            SomatoriaDescontos += (linhaLista.FracaoCorrigida * (linhaLista.CDRDescontoCondominio / 100));
                }                 
            };
            decimal Retorno = ValorLiquido / (1 - SomatoriaDescontos);
            //ValorBruto.Text = "Valor Real = " + Retorno.ToString("0.00");
            return Retorno;
            */
        }        

        private bool VerificacaoDeDados() {
            if (Condrow.IsCONFracaoIdealNull())
            {
                MessageBox.Show("Erro no cadastro: 'Utiliza Fra��o ideal ?'");
                return false;
            };
            if (Condrow.IsCONTipoFRNull())
            {
                MessageBox.Show("Erro no cadastro: 'Tipo de Fundo do Reserva ?'");
                return false;
            };
            if (Condrow.IsCONValorFRNull())
            {
                MessageBox.Show("Erro no cadastro: 'Valor do Fundo de Reserva ?'");
                return false;
            };


            //if (Condrow.IsCONSindicoPagaFRNull())
            //{
            //    MessageBox.Show("Erro no cadastro: 'S�ndico paga Fundo de Reserva ?'");
            //    return false;
            //};
                

            return true;
        }

        private bool SemCTL = false;

        private int? CTL(PLAPadrao PLA)
        {
            if (SemCTL)
                return null;
            if (dPrevBolCampos.ConTasLogicas.Count == 0)
            {
                if (dPrevBolCampos.ConTasLogicasTableAdapter.Fill(dPrevBolCampos.ConTasLogicas, linhamae.PBO_CON) == 0)
                {
                    SemCTL = true;
                    return null;
                }                
            }
            foreach (dPrevBolCampos.ConTasLogicasRow rowTeste in dPrevBolCampos.ConTasLogicas)            
                if ( 
                    ((rowTeste.CTLTipo == (int)Framework.CTLTipo.Caixa) && (PLA == PLAPadrao.Condominio))
                    ||
                    ((rowTeste.CTLTipo == (int)Framework.CTLTipo.Fundo) && (PLA == PLAPadrao.FundoReserva))
                    ||
                    ((rowTeste.CTLTipo == (int)Framework.CTLTipo.Caixa) && (PLA == PLAPadrao.segurocosnteudoC))
                   )
                    return rowTeste.CTL;
            
            return null;
        }

        private dPrevBolCampos.BOletoDetalheRow novoBOD(int APT, PLAPadrao PLA, decimal FracaoCorrigida, decimal ValorNominal, DateTime Vencimento, ref decimal Total, bool Proprietario, string Mensagem, INSSSindico? Retencao)
        {
            dPrevBolCampos.BOletoDetalheRow novalinha = dPrevBolCampos.BOletoDetalhe.NewBOletoDetalheRow();
            novalinha.BOD_PBO = linhamae.PBO;
            novalinha.BOD_CON = linhamae.PBO_CON;
            novalinha.BOD_APT = APT;
            novalinha.BOD_PLA = PadraoPLA.PLAPadraoCodigo(PLA);
            novalinha.BODComCondominio = true;
            novalinha.BODPrevisto = true;
            novalinha.BODValor = ValorNominal;
            novalinha.BODData = Vencimento; //compet.DataVencimenot(APT);
            novalinha.BODCompetencia = linhamae.PBOCompetencia;
            Total += novalinha.BODValor;
            novalinha.BODMensagem = Mensagem;
            novalinha.BODProprietario = Proprietario;
            if (Retencao.HasValue)
                novalinha.BODRetencao = (int)(Retencao.Value);
            int? BOD_CTL = CTL(PLA);
            if(BOD_CTL.HasValue)
                novalinha.BOD_CTL = BOD_CTL.Value;                
            novalinha.EndEdit();
            dPrevBolCampos.BOletoDetalhe.AddBOletoDetalheRow(novalinha);
            return novalinha;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("simpleButton1_Click 1");
            apartamentos = null;
            _ValorBrutoCalc = 0;
            if (!VerificacaoDeDados())
                return;
            if (dPrevBolCampos.BOletoDetalhe.Rows.Count > 0)
            {
                foreach (dPrevBolCampos.BOletoDetalheRow BOletoDetalheRow in dPrevBolCampos.BOletoDetalhe)
                    if (!BOletoDetalheRow.IsBOD_BOLNull())
                    {
                        MessageBox.Show("J� foram emitidos Boletos!!");
                        return;
                    };
                dPrevBolCampos.BOletoDetalheTableAdapter.ApagarBOD(linhamae.PBO);
                dPrevBolCampos.BOletoDetalhe.Clear();
            };            
            
            decimal Total = 0;
            decimal TotalFR = 0;
            compet.VencimentoPadrao = linhamae.PBOVencimento;
            
            Competencia comp = new Competencia(linhamae.PBOCompetencia);
            //Condominio e FR proporcional;
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("simpleButton1_Click 2");
            foreach (Boletos.Rateios.dRateios.ListaFracaoRow linhaLista in TabLista)
            {
                bool Proprietario = true;
                DateTime Vencimento = Apartamento(linhaLista.APT).DataVencimento(compet);//.DataVencimenot(linhaLista.APT);
                if (!linhaLista.IsAPTInquilino_PESNull())                
                    if (linhaLista.IsAPTInquilinoPagaCondominioNull() || linhaLista.APTInquilinoPagaCondominio)
                        Proprietario = false;
                string Mensagem = string.Format("Condom�nio {0}", comp);                               
                dPrevBolCampos.BOletoDetalheRow rowBODcondominio = novoBOD(linhaLista.APT, PLAPadrao.Condominio, linhaLista.FracaoCorrigida, linhaLista.ValorNominal,Vencimento, ref Total, Proprietario,Mensagem,null);                
                if ((!linhaLista.IsCDRDescontoCondominioNull()) && (linhaLista.CDRDescontoCondominio > 0))
                {
                    Mensagem = string.Format("Desconto Condom�nio ({0})", linhaLista.CGONome);
                    novoBOD(linhaLista.APT, PLAPadrao.Condominio, 0, linhaLista.ValorCorrigido - linhaLista.ValorNominal, Vencimento, ref Total, Proprietario, Mensagem, (INSSSindico)linhaLista.CDRINSS);                    
                }
                
                // FUNDO DE RESERVA
                decimal ValorFR;
                bool APTInquilinoPagaFR = false;
                if (!linhaLista.IsAPTInquilinoPagaFRNull())
                    APTInquilinoPagaFR = linhaLista.APTInquilinoPagaFR;                
                if (Condrow.CONTipoFR == "P")                
                    ValorFR = decimal.Round(linhaLista.ValorNominal * Condrow.CONValorFR / 100M, 2);                                                   
                else 
                    ValorFR = Condrow.CONValorFR;                                    
                //TotalFR += GravaLinhaFR(linhaLista, 0, ValorFR, APTInquilinoPagaFR);
                Mensagem = string.Format("{0} {1}", TxtFundoR.Text.Trim(),comp);
                Proprietario = !APTInquilinoPagaFR;
                novoBOD(linhaLista.APT, PLAPadrao.FundoReserva , 0, ValorFR, Vencimento, ref TotalFR, Proprietario, Mensagem,null);
                if ((!linhaLista.IsCDRDescontoFRNull()) && (linhaLista.CDRDescontoFR > 0))
                {
                    decimal ValorDescontoFR = 0;
                    if (linhaLista.CDRDescontoFRValor)
                    {
                        if (ValorFR < linhaLista.CDRDescontoFR)
                            ValorDescontoFR = -ValorFR;
                        else
                            ValorDescontoFR = -linhaLista.CDRDescontoFR;
                    }
                    else
                        ValorDescontoFR = -decimal.Round(linhaLista.CDRDescontoFR / 100M * ValorFR, 2);
                    Mensagem = string.Format("Desconto F.R. ({0})", linhaLista.CGONome);
                    novoBOD(linhaLista.APT, PLAPadrao.FundoReserva, 0, ValorDescontoFR, Vencimento, ref TotalFR, Proprietario, Mensagem, (INSSSindico)linhaLista.CDRINSS);
                }

                // SEGURO CONTEUDO

                decimal TotalSeguro = 0;
                if (!Condrow.IsCON_PSCNull())
                {
                    DateTime DataseguroI = new DateTime(Vencimento.Year, Vencimento.Month, 1).AddMonths(1);
                    DateTime DataseguroF = DataseguroI.AddMonths(1).AddDays(-1);
                    Competencia compSeguro = new Competencia(DataseguroI);
                    Mensagem = string.Format("Seguro conteudo ({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", DataseguroI, DataseguroF);
                    if (Apartamento(linhaLista.APT).APTSeguro)
                        if (linhaLista.IsCDR_CGONull() || (linhaLista.CDR_CGO != 1))
                        {
                            string ComandoVerificaS = "SELECT BOL FROM BOLetos WHERE (BOLTipoCRAI = 'S') AND (BOL_APT = @P1) AND (BOLCompetenciaAno = @P2) AND (BOLCompetenciaMes = @P3)";
                            if (!VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado(ComandoVerificaS, linhaLista.APT, compSeguro.Ano, compSeguro.Mes))
                                novoBOD(linhaLista.APT, PLAPadrao.segurocosnteudoC, 0, Condrow.PSCValor, Vencimento, ref TotalSeguro, false, Mensagem, null);
                        }
                }

                
                //Aplica

                /*
                 * if ((!linhaLista.IsCDRDescontoFRNull()) && (linhaLista.CDRDescontoFR > 0))
            {
                dPrevBolCampos.BOletoDetalheRow novalinhaDesc = dPrevBolCampos.BOletoDetalhe.NewBOletoDetalheRow();
                novalinhaDesc.BOD_PBO = novalinha.BOD_PBO;
                novalinhaDesc.BOD_APT = novalinha.BOD_APT;
                novalinhaDesc.BOD_PLA = novalinha.BOD_PLA;
                novalinhaDesc.BODComCondominio = novalinha.BODComCondominio;
                novalinhaDesc.BODData = novalinha.BODData;
                novalinhaDesc.BODCompetenciaAno = novalinha.BODCompetenciaAno;
                novalinhaDesc.BODCompetenciaMes = novalinha.BODCompetenciaMes;
                novalinhaDesc.BODFracaoEfetiva = 0;
                novalinhaDesc.BODProprietario = novalinha.BODProprietario;
                if (linhaLista.CDRDescontoFRValor)
                {
                    if (Valor < linhaLista.CDRDescontoFR)
                        novalinhaDesc.BODValor = -Valor;
                    else
                        novalinhaDesc.BODValor = -linhaLista.CDRDescontoFR;
                    //evitar boleto negativo
                }
                else
                    novalinhaDesc.BODValor = -decimal.Round(linhaLista.CDRDescontoFR / 100M * Valor, 2);
                Retorno += novalinhaDesc.BODValor;
                novalinhaDesc.BODMensagem = string.Format("Desconto F.R. ({0})", linhaLista.CGONome);
                novalinhaDesc.BODRetencao = linhaLista.CDRINSS;
                dPrevBolCampos.BOletoDetalhe.AddBOletoDetalheRow(novalinhaDesc);
            }
                 */



            };
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("simpleButton1_Click 2");
            linhamae.PBOValorTotal = Total;

            //if (Condrow.CONTipoFR == "P")
            linhamae.PBOValorFR = TotalFR;
            //else
            //    FundoDeReserva();

            PagamentosEspecificos();

            linhamae.EndEdit();
            btnConfirmar_F.Enabled = true;
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("simpleButton1_Click 4");
            AjustaTela();
            
            //if (CompontesBasicos.FormPrincipalBase.USULogado == 30)
            //{
            //    string Relatorio = CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();
            //    Clipboard.SetText(Relatorio);
            //    MessageBox.Show(Relatorio);
            //}
        }

        private SortedList<int, ABS_Apartamento> apartamentos;

        /// <summary>
        /// Apartamento do boleto
        /// </summary>
        private ABS_Apartamento Apartamento(int APT)
        {
            if (apartamentos == null)
                apartamentos = ABS_Apartamento.ABSGetApartamentos(linhamae.PBO_CON);
            if (!apartamentos.ContainsKey(APT))
                apartamentos.Add(APT, ABS_Apartamento.GetApartamento(APT));
            return apartamentos[APT];
        }

        private void PrevBolCampos_cargaFinal(object sender, EventArgs e)
        {
            linhamae = (Boletos.geracao.dPrevBolCampos.PrevisaoBOletosRow)(((DataRowView)previsaoBOletosBindingSource.Current).Row);
            dPrevBolCampos.BOletoDetalheTableAdapter.FillRegistro(dPrevBolCampos.BOletoDetalhe, linhamae.PBO);
        }                      

        private void PagamentosEspecificos()
        {
            dPrevBolCampos.PagamentosEspecificosTableAdapter.Fill(dPrevBolCampos.PagamentosEspecificos, Condrow.CON);
            
            foreach (geracao.dPrevBolCampos.PagamentosEspecificosRow rowPGE in dPrevBolCampos.PagamentosEspecificos)
            {

                dPrevBolCampos.BOletoDetalheRow novalinha = dPrevBolCampos.BOletoDetalhe.NewBOletoDetalheRow();
                novalinha.BOD_PBO = linhamae.PBO;
                novalinha.BOD_CON = linhamae.PBO_CON;
                novalinha.BOD_APT = rowPGE.APT;
                novalinha.BOD_PGE = rowPGE.PGE;
                novalinha.BOD_PLA = rowPGE.PGE_PLA;
                novalinha.BODComCondominio = true;
                novalinha.BODPrevisto = true;
                novalinha.BODValor = rowPGE.PGEValor;
                if (!rowPGE.IsPGE_CTLNull())
                    novalinha.BOD_CTL = rowPGE.PGE_CTL;
                novalinha.BODData = Apartamento(rowPGE.APT).DataVencimento(compet);
                novalinha.BODCompetencia = linhamae.PBOCompetencia;

                Competencia comp = new Competencia(linhamae.PBOCompetencia);
                novalinha.BODMensagem = string.Format("{0} {1}",rowPGE.PGEMensagem, comp);
                novalinha.BODProprietario = rowPGE.APTPGEProprietario;

                novalinha.EndEdit();
                dPrevBolCampos.BOletoDetalhe.AddBOletoDetalheRow(novalinha);
            };
            
        }

        private void DesligaCaixaAlta(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
        }

        private void LigaCaixaAlta(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = true;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Caderno.cCadernoEletronico cCadernoEletronico = new Boletos.Caderno.cCadernoEletronico();
            cCadernoEletronico.Fill(CompontesBasicos.TipoDeCarga.pk, Condrow.CON, false);
            cCadernoEletronico.cADernoEletronicoBindingSource.MoveLast();
            ShowModulo(cCadernoEletronico);
        }

        /// <summary>
        /// Refresh
        /// </summary>
        public override void RefreshDados()
        {
            base.RefreshDados();            
            AjustaTela();            
        }

        private void AjustaTela()
        {
            _ValorBrutoCalc = CalculaValorBruto();            
        }        

        private void textEdit3_Validated(object sender, EventArgs e)
        {
            AjustaTela();
            dPrevBolCampos.BOletoDetalheTableAdapter.ApagarBOD(linhamae.PBO);
            dPrevBolCampos.BOletoDetalhe.Clear();
        }

        private void PrevBolCampos_Load(object sender, EventArgs e)
        {
            cBotaoImpNeon1.Enabled = false;
            //BCancelaEmi.Enabled = true;
            if (somenteleitura)
            {
                BCancelaEmi.Visible = true;
                //BCancelaEmi.Enabled = true;
                BCancelaEmi.Left = simpleButton1.Left;
                cBotaoImpNeon1.Visible = true;
                cBotaoImpNeon1.Enabled = impressaopermitida;
                cBotaoImpNeon1.Left = simpleButton1.Left;
                simpleButton1.Visible = simpleButton2.Visible = false;
            }
        }

        private void BCancelaEmi_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Todos os boletos ser�o cancelados.\r\n\r\nConfirma ?", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                MessageBox.Show("As reten��es de INSS dever�o ser canceladas manualmente.", "ATEN��O");
                string Justificativa = "";
                string Erro = "";
                //bool Cancelar = false;
                if (VirInput.Input.Execute("Justificativa", ref Justificativa,true))
                {
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Boletos PrevBolCampos - 625",dPrevBolCampos.PrevisaoBOletosTableAdapter);
                        System.Collections.ArrayList BOLS = new System.Collections.ArrayList();
                        foreach (dPrevBolCampos.BOletoDetalheRow rowBOD in dPrevBolCampos.BOletoDetalhe)
                        {
                            if ((!rowBOD.IsBOD_BOLNull()) && (!BOLS.Contains(rowBOD.BOD_BOL)))
                            {
                                BOLS.Add(rowBOD.BOD_BOL);
                                Boleto.Boleto Bol = new Boletos.Boleto.Boleto(rowBOD.BOD_BOL);
                                if (Bol.Encontrado)
                                    if (!Bol.CancelaEmissao(Justificativa))
                                    {
                                        Erro += string.Format("Erro: Boleto {0} n�o pode ser cancelado!\r\n", rowBOD.BOD_BOL);
                                        //Cancelar = true;
                                        //break;
                                    }
                            }
                        }
                        dPrevBolCampos.PrevisaoBOletosTableAdapter.Update(linhamae);
                        dPrevBolCampos.PrevisaoBOletos.AcceptChanges();                        
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();                        
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                        MessageBox.Show(string.Format("Erro ao cancelar boleto:\r\n{0}",ex.Message));
                        
                    };
                    if (Erro != "")
                    {
                        Clipboard.SetText(Erro);
                        MessageBox.Show(string.Format("Relat�rio na �rea de tranfer�ncia.\r\n\r\nErros:\r\n{0}", Erro));
                    }
                    FechaTela(DialogResult.OK);
                };
                
            }
        }

        private void cBotaoImp1_clicado(object sender, dllBotao.BotaoArgs args)
        {
            SortedList<int, Boleto.Boleto> Boletos = new SortedList<int, Boleto.Boleto>();
            foreach (dPrevBolCampos.BOletoDetalheRow rowBOD in linhamae.GetBOletoDetalheRows())
                if (!rowBOD.IsBOD_BOLNull())
                    if (!Boletos.ContainsKey(rowBOD.BOD_BOL))
                        Boletos.Add(rowBOD.BOD_BOL, new Boleto.Boleto(rowBOD.BOD_BOL));
            if (Boletos.Count == 0)
                return;


            switch (args.Botao)
            {

                case dllBotao.Botao.botao:
                case dllBotao.Botao.imprimir:
                case dllBotao.Botao.tela:
                case dllBotao.Botao.pdf:
                    dllImpresso.ImpBoletoBal impresso = new dllImpresso.ImpBoletoBal();
                    foreach (Boleto.Boleto BoletoImp in Boletos.Values)
                        BoletoImp.CarregaLinha(impresso, true, false);
                    impresso.CreateDocument();
                    cBotaoImpNeon1.Impresso = impresso;
                    //if(linhamae.PBORegistrando)
                    //    MessageBox.Show(Email(Boletos));
                    break;
                case dllBotao.Botao.email:
                    MessageBox.Show(Email(Boletos));
                    break;

            }

            //linhamae.PBORegistrando = false;
            dPrevBolCampos.PrevisaoBOletosTableAdapter.Update(linhamae);
            linhamae.AcceptChanges();

            //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update PrevisaoBOletos set PBORegistrando = 0 where PBO = @P1", linhamae.PBO);
        }

        private string Email(SortedList<int, Boleto.Boleto> Boletos)
        {
            string ConteudoEmail =
            "\r\n" +
            "Prezado Cliente," + "\r\n" +
            "\r\n" +
            "Para sua maior seguran�a e comodidade, a Neon Online, passa a enviar a partir de agora o Boleto Banc�rio do %C por email." + "\r\n" +
            "" + "\r\n" +
            "O arquivo (pdf) do Boleto Banc�rio est� anexo. Voc� poder� imprimi-lo e efetuar o pagamento em qualquer banco integrado ao sistema de compensa��o ou via internet." + "\r\n" +
            "" + "\r\n" +
            "Para pagamento via internet disponibilizamos tamb�m o n�mero do C�digo de barras:" + "\r\n" +
            "" + "\r\n" +
            " %B " + "\r\n" +
            "" + "\r\n" +
            "que identificar� este boleto no site de seu banco." + "\r\n" +
            "" + "\r\n" +
            "Neon Online, facilitando sua vida." + "\r\n" +
            "" + "\r\n" +
            "Caso v�ce n�o queira receber os boletos por e-mail basta desmarcar esta op��o no seu cadastro no Neon Online." + "\r\n" +
            "" + "\r\n" +
            "Att," + "\r\n" +
            "" + "\r\n" +
            "Neon Im�veis" + "\r\n" +
            "\r\n";
            string Relatorio = "";
            dllImpresso.ImpBoletoBal ImpSB = new dllImpresso.ImpBoletoBal();
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.AtivaGauge(Boletos.Count);
                Esp.Espere("E.mail");
                ImpSB.ComRemetente = false;
                foreach (Boleto.Boleto BoletosE in Boletos.Values)
                {
                    if (Esp.Abortar)
                        break;                    

                    if (BoletosE.QuerEmail)
                    {
                        ImpSB.Apontamento(-1, -1);
                        BoletosE.CarregaLinha(ImpSB, true, false);
                        ImpSB.bindingSourcePrincipal.Filter = "BOL = " + BoletosE.BOL.ToString();
                        ImpSB.CreateDocument();
                        string Corpo = ConteudoEmail.Replace("%C", BoletosE.rowCON.CONNome).Replace("%B", BoletosE.LinhaDigitavel);
                        try
                        {
                            VirEmailNeon.EmailDiretoNeon.EmalSTSemRetorno.Enviar(BoletosE.EmailResponsavel, BoletosE.BOL.ToString(), ImpSB, Corpo, "Boleto de condom�nio");
                            Relatorio += "ok: " + BoletosE.Apartamento.BLOCodigo + " " + BoletosE.Apartamento.APTNumero + " " + BoletosE.EmailResponsavel + "\r\n";
                        }
                        catch
                        {
                            Relatorio += "E.mail n�o enviado: " + BoletosE.Apartamento.BLOCodigo + " " + BoletosE.Apartamento.APTNumero + " " + BoletosE.EmailResponsavel + "\r\n";
                            Relatorio += "\r\n" + VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro + "\r\n";
                        };
                        Esp.Gauge();
                        Application.DoEvents();
                    };
                };
            };

            return Relatorio;

        }

        private void cONSeguradora_FRNLookUpEdit_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.Equals(colBotao))
            {
                dPrevBolCampos.BOletoDetalheRow row = (dPrevBolCampos.BOletoDetalheRow)gridView1.GetDataRow(e.RowHandle);
                if ((row != null) && (!row.IsBOLStatusNull()))
                    e.RepositoryItem = repositoryBotao;
                else
                    e.RepositoryItem = null;
            }
        }

        private void repositoryBotao_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dPrevBolCampos.BOletoDetalheRow row = (dPrevBolCampos.BOletoDetalheRow)gridView1.GetFocusedDataRow();
            if ((row == null) || (row.IsBOD_BOLNull()))
                return;
            Boleto.cBoleto cBoleto = new Boleto.cBoleto(PegaBol(row.BOD_BOL), false);
            cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);

        }

        private SortedList<int, Boleto.Boleto> EstoqueBoletos;

        private Boleto.Boleto PegaBol(int BOL)
        {
            if (EstoqueBoletos == null)
                EstoqueBoletos = new SortedList<int, Boleto.Boleto>();
            if (!EstoqueBoletos.ContainsKey(BOL))
                EstoqueBoletos.Add(BOL, new Boleto.Boleto(BOL));
            return EstoqueBoletos[BOL];
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dPrevBolCampos.BOletoDetalheRow row = (dPrevBolCampos.BOletoDetalheRow)gridView1.GetDataRow(e.RowHandle);
            if ((row == null) || (row.IsBOD_BOLNull()))
                return;
            if (e.Column.Equals(colBOLStatus))
            {
                
                Boleto.Boleto.VirEnumStatusBoleto.CellStyle((StatusBoleto)row.BOLStatus, e);
                //PegaBol(row.BOD_BOL).AjustaAparencia(e.Appearance);
            }
            if (e.Column.Equals(colBOL_StatusRegistro))
                Boleto.Boleto.VirEnumStatusRegistroBoleto.CellStyle((StatusRegistroBoleto)row.BOLStatusRegistro, e);
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dPrevBolCampos.BOletoDetalheRow row = (dPrevBolCampos.BOletoDetalheRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            colBODValor.OptionsColumn.ReadOnly = colBODMensagem.OptionsColumn.ReadOnly = colBODData.OptionsColumn.ReadOnly = !row.IsBOD_BOLNull();
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dPrevBolCampos.BOletoDetalheRow row = (dPrevBolCampos.BOletoDetalheRow)gridView1.GetDataRow(e.RowHandle);
            if ((row == null) || (!row.IsBOD_BOLNull()))
                return;
            if (row.RowState == DataRowState.Modified)
            {
                dPrevBolCampos.BOletoDetalheTableAdapter.Update(row);
                row.AcceptChanges();
            }
        }

        private void BotRegNov_Click(object sender, EventArgs e)
        {            
            foreach (dPrevBolCampos.BOletoDetalheRow rowBOD in dPrevBolCampos.BOletoDetalhe)
            {
                if (!rowBOD.IsBOLStatusRegistroNull() && (rowBOD.BOLStatusRegistro == (int)StatusRegistroBoleto.ErroNoRegistro))
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOLetos set BOLStatusRegistro = @P1 where BOL = @P2",
                                                                             (int)StatusRegistroBoleto.BoletoRegistrando,
                                                                             rowBOD.BOD_BOL);
                BotRegNov.Visible = false;                
            };
            carregadados();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!this.Validate(true))
                return;
            previsaoBOletosBindingSource.EndEdit();
            if (linhamae.RowState != DataRowState.Unchanged)
            {
                dPrevBolCampos.PrevisaoBOletosTableAdapter.Update(linhamae);
                linhamae.AcceptChanges();
            };
            int? CONBalBoleto = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CONBalBoleto from condominios where con = @P1", linhamae.PBO_CON);
            if (CONBalBoleto.HasValue)
            {
                Competencia compBol = new Competencia(linhamae.PBOCompetencia);
                Competencia compBalancete = compBol.CloneCompet(CONBalBoleto.Value);
                AbstratosNeon.ABS_balancete balancete = AbstratosNeon.ABS_balancete.GetBalancete(linhamae.PBO_CON, compBalancete,TipoBalancete.Boleto);
                if (!balancete.simulaBoleto(linhamae.PBO))                
                    MessageBox.Show(string.Format("Balancete n�o dispon�vel ({0})", compBalancete));
            }
        }

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            string comando =
"SELECT        BOL\r\n" +
"FROM            BOLetos\r\n" +
"WHERE        (BOLEmissao >= CONVERT(DATETIME, '2015-04-10 00:00:00', 102)) AND (BOLNome = '')\r\n" +
";";
            DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comando);
            if (MessageBox.Show(string.Format("corrigir:{0}", DT.Rows.Count),"Corre��o",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                foreach (DataRow DR in DT.Rows)
                {
                    Boleto.Boleto BoletoX = new Boleto.Boleto((int)DR[0]);
                    BoletoX.correcao();
                }
                MessageBox.Show("ok");
            }
        }
        */       
    }

    

}

