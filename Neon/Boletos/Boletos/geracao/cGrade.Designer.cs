namespace Boletos.geracao
{
    partial class cGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            this.previsaoBOletosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dPrevBol = new Boletos.geracao.dPrevBol();
            this.dRateios = new Boletos.Rateios.dRateios();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONAuditor1_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpusuario = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colProximoVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONEmiteBoletos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonBLOQ = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonOk = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonNovo = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonPreparado = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonRegistrando = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonAg = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.legenda1 = new dllVirEnum.Legenda.Legenda();
            this.lookupGerente1 = new Framework.Lookup.LookupGerente();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previsaoBOletosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPrevBol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRateios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpusuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBLOQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonNovo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonPreparado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonRegistrando)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonAg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // previsaoBOletosBindingSource
            // 
            this.previsaoBOletosBindingSource.DataMember = "PrevisaoBOletos";
            this.previsaoBOletosBindingSource.DataSource = this.dPrevBol;
            // 
            // dPrevBol
            // 
            this.dPrevBol.DataSetName = "dPrevBol";
            this.dPrevBol.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dRateios
            // 
            this.dRateios.DataSetName = "dRateios";
            this.dRateios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.cONDOMINIOSBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 63);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpusuario,
            this.repositoryItemButtonBLOQ,
            this.repositoryItemButtonOk,
            this.repositoryItemButtonNovo,
            this.repositoryItemButtonPreparado,
            this.repositoryItemButtonRegistrando,
            this.repositoryItemButtonAg});
            this.gridControl1.Size = new System.Drawing.Size(1476, 719);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dPrevBol;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCodigo,
            this.colCONNome,
            this.colCONAuditor1_USU,
            this.colProximoVencimento,
            this.colCONEmiteBoletos});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONEmiteBoletos, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colProximoVencimento, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCodigo, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.FixedWidth = true;
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "CONCodigo", "{0:n0}")});
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            this.colCONCodigo.Width = 81;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Condom�nio";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.FixedWidth = true;
            this.colCONNome.OptionsColumn.ReadOnly = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 1;
            this.colCONNome.Width = 212;
            // 
            // colCONAuditor1_USU
            // 
            this.colCONAuditor1_USU.Caption = "gerente";
            this.colCONAuditor1_USU.ColumnEdit = this.LookUpusuario;
            this.colCONAuditor1_USU.FieldName = "CONAuditor1_USU";
            this.colCONAuditor1_USU.Name = "colCONAuditor1_USU";
            this.colCONAuditor1_USU.OptionsColumn.ReadOnly = true;
            // 
            // LookUpusuario
            // 
            this.LookUpusuario.AutoHeight = false;
            this.LookUpusuario.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpusuario.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USU Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpusuario.DataSource = this.uSUariosBindingSource;
            this.LookUpusuario.DisplayMember = "USUNome";
            this.LookUpusuario.Name = "LookUpusuario";
            this.LookUpusuario.ShowHeader = false;
            this.LookUpusuario.ValueMember = "USU";
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colProximoVencimento
            // 
            this.colProximoVencimento.Caption = "Pr�ximo";
            this.colProximoVencimento.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colProximoVencimento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colProximoVencimento.FieldName = "ProximoVencimento";
            this.colProximoVencimento.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colProximoVencimento.Name = "colProximoVencimento";
            this.colProximoVencimento.OptionsColumn.ReadOnly = true;
            this.colProximoVencimento.Visible = true;
            this.colProximoVencimento.VisibleIndex = 2;
            this.colProximoVencimento.Width = 82;
            // 
            // colCONEmiteBoletos
            // 
            this.colCONEmiteBoletos.Caption = "Emite";
            this.colCONEmiteBoletos.FieldName = "CONEmiteBoletos";
            this.colCONEmiteBoletos.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colCONEmiteBoletos.Name = "colCONEmiteBoletos";
            this.colCONEmiteBoletos.Visible = true;
            this.colCONEmiteBoletos.VisibleIndex = 3;
            this.colCONEmiteBoletos.Width = 52;
            // 
            // repositoryItemButtonBLOQ
            // 
            this.repositoryItemButtonBLOQ.Appearance.Options.UseTextOptions = true;
            this.repositoryItemButtonBLOQ.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemButtonBLOQ.AutoHeight = false;
            this.repositoryItemButtonBLOQ.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Boletos.Properties.Resources.cadeado, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "Cadeado", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonBLOQ.Name = "repositoryItemButtonBLOQ";
            this.repositoryItemButtonBLOQ.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonBLOQ.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            this.repositoryItemButtonBLOQ.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            // 
            // repositoryItemButtonOk
            // 
            this.repositoryItemButtonOk.Appearance.Options.UseTextOptions = true;
            this.repositoryItemButtonOk.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemButtonOk.AutoHeight = false;
            this.repositoryItemButtonOk.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Boletos.Properties.Resources.BtnImprimir_F_Glyph, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "ReImprime", null, true)});
            this.repositoryItemButtonOk.Name = "repositoryItemButtonOk";
            this.repositoryItemButtonOk.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonOk.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            this.repositoryItemButtonOk.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            // 
            // repositoryItemButtonNovo
            // 
            this.repositoryItemButtonNovo.AutoHeight = false;
            this.repositoryItemButtonNovo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Boletos.Properties.Resources.BtnIncluir_F_Glyph, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", "Novo", null, true)});
            this.repositoryItemButtonNovo.Name = "repositoryItemButtonNovo";
            this.repositoryItemButtonNovo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonNovo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // repositoryItemButtonPreparado
            // 
            this.repositoryItemButtonPreparado.AutoHeight = false;
            this.repositoryItemButtonPreparado.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonPreparado.Name = "repositoryItemButtonPreparado";
            this.repositoryItemButtonPreparado.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonPreparado.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            this.repositoryItemButtonPreparado.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            // 
            // repositoryItemButtonRegistrando
            // 
            this.repositoryItemButtonRegistrando.AutoHeight = false;
            this.repositoryItemButtonRegistrando.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Boletos.Properties.Resources.BtnImprimir_F_Glyph, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", "ReImprime", null, true)});
            this.repositoryItemButtonRegistrando.Name = "repositoryItemButtonRegistrando";
            this.repositoryItemButtonRegistrando.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonRegistrando.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // repositoryItemButtonAg
            // 
            this.repositoryItemButtonAg.AutoHeight = false;
            this.repositoryItemButtonAg.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Boletos.Properties.Resources.BtnImprimir_F_Glyph, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", "ReImprime", null, true)});
            this.repositoryItemButtonAg.Name = "repositoryItemButtonAg";
            this.repositoryItemButtonAg.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonAg.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem6});
            this.barManager1.MaxItemId = 7;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 4";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Custom 4";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Detalhes";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Atualizar";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Gerar Boletos";
            this.barButtonItem6.Id = 5;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1476, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 782);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1476, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 782);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1476, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 782);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Controls.Add(this.legenda1);
            this.panelControl1.Controls.Add(this.lookupGerente1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1476, 63);
            this.panelControl1.TabIndex = 10;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(302, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 13);
            this.labelControl1.TabIndex = 42;
            this.labelControl1.Text = "Compet�ncia inicial";
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(397, 1);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 41;
            this.cCompet1.Titulo = null;
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // legenda1
            // 
            this.legenda1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.legenda1.Location = new System.Drawing.Point(5, 27);
            this.legenda1.Margem = 6;
            this.legenda1.Name = "legenda1";
            this.legenda1.Size = new System.Drawing.Size(545, 33);
            this.legenda1.TabIndex = 40;
            // 
            // lookupGerente1
            // 
            this.lookupGerente1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente1.Appearance.Options.UseBackColor = true;
            this.lookupGerente1.CaixaAltaGeral = true;
            this.lookupGerente1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente1.Location = new System.Drawing.Point(5, 5);
            this.lookupGerente1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupGerente1.Name = "lookupGerente1";
            this.lookupGerente1.Size = new System.Drawing.Size(290, 20);
            this.lookupGerente1.somenteleitura = false;
            this.lookupGerente1.TabIndex = 0;
            this.lookupGerente1.Titulo = null;
            this.lookupGerente1.USUSel = -1;
            this.lookupGerente1.alterado += new System.EventHandler(this.lookupGerente1_alterado);
            // 
            // cGrade
            // 
            this.AutoScroll = true;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGrade";
            this.Size = new System.Drawing.Size(1476, 807);
            this.Load += new System.EventHandler(this.cGrade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previsaoBOletosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPrevBol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRateios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpusuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBLOQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonNovo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonPreparado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonRegistrando)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonAg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private dPrevBol dPrevBol;
        private System.Windows.Forms.BindingSource previsaoBOletosBindingSource;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private Boletos.Rateios.dRateios dRateios;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private Framework.Lookup.LookupGerente lookupGerente1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAuditor1_USU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpusuario;
        private System.Windows.Forms.BindingSource uSUariosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colProximoVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colCONEmiteBoletos;
        private dllVirEnum.Legenda.Legenda legenda1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonBLOQ;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonOk;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonNovo;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonPreparado;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonRegistrando;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonAg;
    }
}
