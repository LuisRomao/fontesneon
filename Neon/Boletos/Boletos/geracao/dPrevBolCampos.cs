﻿namespace Boletos.geracao {


    partial class dPrevBolCampos
    {
        private dPrevBolCamposTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dPrevBolCamposTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dPrevBolCamposTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao();
                };
                return conTasLogicasTableAdapter;
            }
        }

        private static dPrevBolCamposTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalhe
        /// </summary>
        public static dPrevBolCamposTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dPrevBolCamposTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheTableAdapter;
            }
        }

        private static dPrevBolCamposTableAdapters.APTLoockupTableAdapter aPTLoockupTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APTLoockup
        /// </summary>
        public static dPrevBolCamposTableAdapters.APTLoockupTableAdapter APTLoockupTableAdapter
        {
            get
            {
                if (aPTLoockupTableAdapter == null)
                {
                    aPTLoockupTableAdapter = new dPrevBolCamposTableAdapters.APTLoockupTableAdapter();
                    aPTLoockupTableAdapter.TrocarStringDeConexao();
                };
                return aPTLoockupTableAdapter;
            }
        }

        private static dPrevBolCamposTableAdapters.ListaPaGamentosEspecificosTableAdapter listaPaGamentosEspecificosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ListaPaGamentosEspecificos
        /// </summary>
        public static dPrevBolCamposTableAdapters.ListaPaGamentosEspecificosTableAdapter ListaPaGamentosEspecificosTableAdapter
        {
            get
            {
                if (listaPaGamentosEspecificosTableAdapter == null)
                {
                    listaPaGamentosEspecificosTableAdapter = new dPrevBolCamposTableAdapters.ListaPaGamentosEspecificosTableAdapter();
                    listaPaGamentosEspecificosTableAdapter.TrocarStringDeConexao();
                };
                return listaPaGamentosEspecificosTableAdapter;
            }
        }

        private static dPrevBolCamposTableAdapters.PrevisaoBOletosTableAdapter previsaoBOletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PrevisaoBOletos
        /// </summary>
        public static dPrevBolCamposTableAdapters.PrevisaoBOletosTableAdapter PrevisaoBOletosTableAdapter
        {
            get
            {
                if (previsaoBOletosTableAdapter == null)
                {
                    previsaoBOletosTableAdapter = new dPrevBolCamposTableAdapters.PrevisaoBOletosTableAdapter();
                    previsaoBOletosTableAdapter.TrocarStringDeConexao();
                };
                return previsaoBOletosTableAdapter;
            }
        }

        private static dPrevBolCamposTableAdapters.PagamentosEspecificosTableAdapter pagamentosEspecificosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PagamentosEspecificos
        /// </summary>
        public static dPrevBolCamposTableAdapters.PagamentosEspecificosTableAdapter PagamentosEspecificosTableAdapter
        {
            get
            {
                if (pagamentosEspecificosTableAdapter == null)
                {
                    pagamentosEspecificosTableAdapter = new dPrevBolCamposTableAdapters.PagamentosEspecificosTableAdapter();
                    pagamentosEspecificosTableAdapter.TrocarStringDeConexao();
                };
                return pagamentosEspecificosTableAdapter;
            }
        }

        private static dPrevBolCamposTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public static dPrevBolCamposTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dPrevBolCamposTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }
    }
}

namespace Boletos.geracao.dPrevBolCamposTableAdapters {
    
    
    public partial class BOletoDetalheTableAdapter {
    }
}
