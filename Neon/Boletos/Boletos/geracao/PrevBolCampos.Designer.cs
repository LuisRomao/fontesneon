namespace Boletos.geracao
{
    partial class PrevBolCampos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrevBolCampos));
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.pBOCompetenciaanoLabel = new System.Windows.Forms.Label();
            this.pBOVencimentoLabel = new System.Windows.Forms.Label();
            this.pBOValorTotalLabel = new System.Windows.Forms.Label();
            this.cONCodigoLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.previsaoBOletosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dPrevBolCampos = new Boletos.geracao.dPrevBolCampos();
            this.pBOVencimentoDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONCodigoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONNomeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.button2 = new System.Windows.Forms.Button();
            this.BotRegNov = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoImpNeon1 = new VirBotaoNeon.cBotaoImpNeon();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.BCancelaEmi = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.ValorBruto = new System.Windows.Forms.Label();
            this.labelFundoReserva = new System.Windows.Forms.Label();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.ValorFundo = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.richEditControl1 = new DevExpress.XtraRichEdit.RichEditControl();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.dPlanosSeguroConteudoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TxtFundoR = new DevExpress.XtraEditors.TextEdit();
            this.listaPaGamentosEspecificosGridControl = new DevExpress.XtraGrid.GridControl();
            this.listaPaGamentosEspecificosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPGEValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGEMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.fKBOletoDetalhePrevisaoBOletosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOD_BOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colBODMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOD_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit1Bloco = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.aPTLoockupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colBODProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.colBODData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCopiaBOD_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditAPT = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colCopiaBOD_APT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditCargo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBOL_StatusRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colBOLStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colBotao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryBotao = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dRateios1 = new Boletos.Rateios.dRateios();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previsaoBOletosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPrevBolCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBOVencimentoDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBOVencimentoDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONNomeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValorFundo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPlanosSeguroConteudoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFundoR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaPaGamentosEspecificosGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaPaGamentosEspecificosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBOletoDetalhePrevisaoBOletosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit1Bloco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTLoockupBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditAPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditCargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBotao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRateios1)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // pBOCompetenciaanoLabel
            // 
            this.pBOCompetenciaanoLabel.AutoSize = true;
            this.pBOCompetenciaanoLabel.Location = new System.Drawing.Point(10, 54);
            this.pBOCompetenciaanoLabel.Name = "pBOCompetenciaanoLabel";
            this.pBOCompetenciaanoLabel.Size = new System.Drawing.Size(73, 13);
            this.pBOCompetenciaanoLabel.TabIndex = 4;
            this.pBOCompetenciaanoLabel.Text = "Compet�ncia:";
            // 
            // pBOVencimentoLabel
            // 
            this.pBOVencimentoLabel.AutoSize = true;
            this.pBOVencimentoLabel.Location = new System.Drawing.Point(10, 82);
            this.pBOVencimentoLabel.Name = "pBOVencimentoLabel";
            this.pBOVencimentoLabel.Size = new System.Drawing.Size(66, 13);
            this.pBOVencimentoLabel.TabIndex = 10;
            this.pBOVencimentoLabel.Text = "Vencimento:";
            // 
            // pBOValorTotalLabel
            // 
            this.pBOValorTotalLabel.AutoSize = true;
            this.pBOValorTotalLabel.Location = new System.Drawing.Point(269, 54);
            this.pBOValorTotalLabel.Name = "pBOValorTotalLabel";
            this.pBOValorTotalLabel.Size = new System.Drawing.Size(68, 13);
            this.pBOValorTotalLabel.TabIndex = 16;
            this.pBOValorTotalLabel.Text = "Valor l�quido:";
            // 
            // cONCodigoLabel
            // 
            this.cONCodigoLabel.AutoSize = true;
            this.cONCodigoLabel.Location = new System.Drawing.Point(10, 27);
            this.cONCodigoLabel.Name = "cONCodigoLabel";
            this.cONCodigoLabel.Size = new System.Drawing.Size(55, 13);
            this.cONCodigoLabel.TabIndex = 25;
            this.cONCodigoLabel.Text = "CODCON:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(269, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "F. de Reserva:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(921, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Fundo de reserva:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(921, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Seguro conte�do:";
            // 
            // previsaoBOletosBindingSource
            // 
            this.previsaoBOletosBindingSource.DataMember = "PrevisaoBOletos";
            this.previsaoBOletosBindingSource.DataSource = this.dPrevBolCampos;
            // 
            // dPrevBolCampos
            // 
            this.dPrevBolCampos.DataSetName = "dPrevBolCampos";
            this.dPrevBolCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pBOVencimentoDateEdit
            // 
            this.pBOVencimentoDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.previsaoBOletosBindingSource, "PBOVencimento", true));
            this.pBOVencimentoDateEdit.EditValue = null;
            this.pBOVencimentoDateEdit.Location = new System.Drawing.Point(93, 80);
            this.pBOVencimentoDateEdit.Name = "pBOVencimentoDateEdit";
            this.pBOVencimentoDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pBOVencimentoDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.pBOVencimentoDateEdit.Size = new System.Drawing.Size(151, 20);
            this.pBOVencimentoDateEdit.TabIndex = 11;
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.previsaoBOletosBindingSource, "PBOValorTotal", true));
            this.textEdit3.Location = new System.Drawing.Point(354, 49);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit3.Properties.DisplayFormat.FormatString = "C02";
            this.textEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEdit3.Properties.EditFormat.FormatString = "N02";
            this.textEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEdit3.Size = new System.Drawing.Size(109, 22);
            this.textEdit3.TabIndex = 22;
            this.textEdit3.Validated += new System.EventHandler(this.textEdit3_Validated);
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dPrevBolCampos;
            // 
            // cONCodigoTextEdit
            // 
            this.cONCodigoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigo", true));
            this.cONCodigoTextEdit.Enabled = false;
            this.cONCodigoTextEdit.Location = new System.Drawing.Point(93, 23);
            this.cONCodigoTextEdit.Name = "cONCodigoTextEdit";
            this.cONCodigoTextEdit.Size = new System.Drawing.Size(98, 20);
            this.cONCodigoTextEdit.TabIndex = 26;
            // 
            // cONNomeTextEdit
            // 
            this.cONNomeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONNome", true));
            this.cONNomeTextEdit.Enabled = false;
            this.cONNomeTextEdit.Location = new System.Drawing.Point(197, 23);
            this.cONNomeTextEdit.Name = "cONNomeTextEdit";
            this.cONNomeTextEdit.Size = new System.Drawing.Size(341, 20);
            this.cONNomeTextEdit.TabIndex = 27;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.button2);
            this.groupControl2.Controls.Add(this.BotRegNov);
            this.groupControl2.Controls.Add(this.cBotaoImpNeon1);
            this.groupControl2.Controls.Add(this.cCompet1);
            this.groupControl2.Controls.Add(this.BCancelaEmi);
            this.groupControl2.Controls.Add(this.simpleButton2);
            this.groupControl2.Controls.Add(this.ValorBruto);
            this.groupControl2.Controls.Add(this.labelFundoReserva);
            this.groupControl2.Controls.Add(this.checkEdit1);
            this.groupControl2.Controls.Add(this.ValorFundo);
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Controls.Add(this.simpleButton1);
            this.groupControl2.Controls.Add(this.cONCodigoTextEdit);
            this.groupControl2.Controls.Add(this.cONNomeTextEdit);
            this.groupControl2.Controls.Add(this.textEdit3);
            this.groupControl2.Controls.Add(this.pBOValorTotalLabel);
            this.groupControl2.Controls.Add(this.cONCodigoLabel);
            this.groupControl2.Controls.Add(this.pBOCompetenciaanoLabel);
            this.groupControl2.Controls.Add(this.pBOVencimentoDateEdit);
            this.groupControl2.Controls.Add(this.pBOVencimentoLabel);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 35);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1568, 107);
            this.groupControl2.TabIndex = 28;
            this.groupControl2.Text = "Condom�nio";
            // 
            // button2
            // 
            this.button2.Image = global::Boletos.Properties.Resources.Bboleto;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(944, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 32);
            this.button2.TabIndex = 44;
            this.button2.Text = "Simular";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BotRegNov
            // 
            this.BotRegNov.Location = new System.Drawing.Point(943, 27);
            this.BotRegNov.Name = "BotRegNov";
            this.BotRegNov.Size = new System.Drawing.Size(147, 32);
            this.BotRegNov.TabIndex = 43;
            this.BotRegNov.Text = "Registrar novamente";
            this.BotRegNov.Visible = false;
            this.BotRegNov.Click += new System.EventHandler(this.BotRegNov_Click);
            // 
            // cBotaoImpNeon1
            // 
            this.cBotaoImpNeon1.AbrirArquivoExportado = true;
            this.cBotaoImpNeon1.AcaoBotao = dllBotao.Botao.imprimir;
            this.cBotaoImpNeon1.AssuntoEmail = null;
            this.cBotaoImpNeon1.BotaoEmail = true;
            this.cBotaoImpNeon1.BotaoImprimir = true;
            this.cBotaoImpNeon1.BotaoPDF = true;
            this.cBotaoImpNeon1.BotaoTela = true;
            this.cBotaoImpNeon1.CorpoEmail = null;
            this.cBotaoImpNeon1.CreateDocAutomatico = true;
            this.cBotaoImpNeon1.Impresso = null;
            this.cBotaoImpNeon1.Location = new System.Drawing.Point(790, 70);
            this.cBotaoImpNeon1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpNeon1.Name = "cBotaoImpNeon1";
            this.cBotaoImpNeon1.NaoUsarxlsX = false;
            this.cBotaoImpNeon1.NomeArquivoAnexo = null;
            this.cBotaoImpNeon1.PriComp = null;
            this.cBotaoImpNeon1.Size = new System.Drawing.Size(147, 32);
            this.cBotaoImpNeon1.TabIndex = 42;
            this.cBotaoImpNeon1.Titulo = "Imprimir Boletos";
            this.cBotaoImpNeon1.Visible = false;
            this.cBotaoImpNeon1.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImp1_clicado);
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompet1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.Appearance.Options.UseFont = true;
            this.cCompet1.Appearance.Options.UseForeColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.previsaoBOletosBindingSource, "PBOCompetencia", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(93, 47);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = true;
            this.cCompet1.Size = new System.Drawing.Size(98, 27);
            this.cCompet1.somenteleitura = true;
            this.cCompet1.TabIndex = 38;
            this.cCompet1.Titulo = null;
            // 
            // BCancelaEmi
            // 
            this.BCancelaEmi.Location = new System.Drawing.Point(790, 27);
            this.BCancelaEmi.Name = "BCancelaEmi";
            this.BCancelaEmi.Size = new System.Drawing.Size(147, 32);
            this.BCancelaEmi.TabIndex = 37;
            this.BCancelaEmi.Text = "Cancelar a emiss�o";
            this.BCancelaEmi.Click += new System.EventHandler(this.BCancelaEmi_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(747, 26);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(37, 76);
            this.simpleButton2.TabIndex = 36;
            this.simpleButton2.Text = "OBS";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // ValorBruto
            // 
            this.ValorBruto.AutoSize = true;
            this.ValorBruto.Location = new System.Drawing.Point(471, 54);
            this.ValorBruto.Name = "ValorBruto";
            this.ValorBruto.Size = new System.Drawing.Size(58, 13);
            toolTipTitleItem3.Text = "Valor efetivamente rateado\r\n";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Este valor � a base de calculo para o\r\n Fundo de reserva";
            toolTipTitleItem4.LeftIndent = 6;
            toolTipTitleItem4.Text = "Obs: o valor l�quido � este valor \r\nmenos os descontos do corpo diretivo";
            superToolTip2.Items.Add(toolTipTitleItem3);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip2.Items.Add(toolTipTitleItem4);
            this.ToolTipController_F.SetSuperTip(this.ValorBruto, superToolTip2);
            this.ValorBruto.TabIndex = 35;
            this.ValorBruto.Text = "Valor Total";
            // 
            // labelFundoReserva
            // 
            this.labelFundoReserva.AutoSize = true;
            this.labelFundoReserva.Location = new System.Drawing.Point(471, 82);
            this.labelFundoReserva.Name = "labelFundoReserva";
            this.labelFundoReserva.Size = new System.Drawing.Size(133, 13);
            this.labelFundoReserva.TabIndex = 33;
            this.labelFundoReserva.Text = "Tipo de Fundo de Reserva";
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONFracaoIdeal", true));
            this.checkEdit1.Enabled = false;
            this.checkEdit1.Location = new System.Drawing.Point(553, 24);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Fra��o ideal";
            this.checkEdit1.Size = new System.Drawing.Size(80, 19);
            this.checkEdit1.TabIndex = 31;
            // 
            // ValorFundo
            // 
            this.ValorFundo.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.previsaoBOletosBindingSource, "PBOValorFR", true));
            this.ValorFundo.Location = new System.Drawing.Point(354, 78);
            this.ValorFundo.Name = "ValorFundo";
            this.ValorFundo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValorFundo.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.ValorFundo.Properties.Appearance.Options.UseFont = true;
            this.ValorFundo.Properties.Appearance.Options.UseForeColor = true;
            this.ValorFundo.Properties.DisplayFormat.FormatString = "C02";
            this.ValorFundo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ValorFundo.Properties.EditFormat.FormatString = "N02";
            this.ValorFundo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ValorFundo.Size = new System.Drawing.Size(109, 22);
            this.ValorFundo.TabIndex = 30;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(639, 26);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(102, 76);
            this.simpleButton1.TabIndex = 28;
            this.simpleButton1.Text = "Gerar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.richEditControl1);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.lookUpEdit4);
            this.groupControl1.Controls.Add(this.TxtFundoR);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.listaPaGamentosEspecificosGridControl);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 142);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1568, 181);
            this.groupControl1.TabIndex = 29;
            this.groupControl1.Text = "Memsagens";
            // 
            // richEditControl1
            // 
            this.richEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControl1.DataBindings.Add(new System.Windows.Forms.Binding("RtfText", this.previsaoBOletosBindingSource, "PBOMensagemBalancete", true));
            this.richEditControl1.Location = new System.Drawing.Point(5, 25);
            this.richEditControl1.MenuManager = this.BarManager_F;
            this.richEditControl1.Name = "richEditControl1";
            this.richEditControl1.Options.AutoCorrect.UseSpellCheckerSuggestions = true;
            this.richEditControl1.Size = new System.Drawing.Size(458, 148);
            this.richEditControl1.TabIndex = 28;
            this.richEditControl1.Text = "richEditControl1";
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_PSC", true));
            this.lookUpEdit4.Location = new System.Drawing.Point(924, 51);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PSCDescricao", "Descri��o", 78, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PSCValor", "Valor", 56, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.lookUpEdit4.Properties.DataSource = this.dPlanosSeguroConteudoBindingSource;
            this.lookUpEdit4.Properties.DisplayMember = "PSCDescricao";
            this.lookUpEdit4.Properties.ReadOnly = true;
            this.lookUpEdit4.Properties.ValueMember = "PSC";
            this.lookUpEdit4.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cONSeguradora_FRNLookUpEdit_Properties_ButtonClick);
            this.lookUpEdit4.Size = new System.Drawing.Size(147, 20);
            this.lookUpEdit4.TabIndex = 26;
            // 
            // dPlanosSeguroConteudoBindingSource
            // 
            this.dPlanosSeguroConteudoBindingSource.DataMember = "PlanosSeguroConteudo";
            this.dPlanosSeguroConteudoBindingSource.DataSource = typeof(Framework.datasets.dPlanosSeguroConteudo);
            // 
            // TxtFundoR
            // 
            this.TxtFundoR.EditValue = "Fundo de reserva";
            this.TxtFundoR.Location = new System.Drawing.Point(924, 151);
            this.TxtFundoR.Name = "TxtFundoR";
            this.TxtFundoR.Properties.MaxLength = 42;
            this.TxtFundoR.Size = new System.Drawing.Size(228, 20);
            this.TxtFundoR.TabIndex = 24;
            // 
            // listaPaGamentosEspecificosGridControl
            // 
            this.listaPaGamentosEspecificosGridControl.DataSource = this.listaPaGamentosEspecificosBindingSource;
            this.listaPaGamentosEspecificosGridControl.Location = new System.Drawing.Point(472, 31);
            this.listaPaGamentosEspecificosGridControl.MainView = this.gridView2;
            this.listaPaGamentosEspecificosGridControl.Name = "listaPaGamentosEspecificosGridControl";
            this.listaPaGamentosEspecificosGridControl.Size = new System.Drawing.Size(443, 142);
            this.listaPaGamentosEspecificosGridControl.TabIndex = 24;
            this.listaPaGamentosEspecificosGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // listaPaGamentosEspecificosBindingSource
            // 
            this.listaPaGamentosEspecificosBindingSource.DataMember = "ListaPaGamentosEspecificos";
            this.listaPaGamentosEspecificosBindingSource.DataSource = this.dPrevBolCampos;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPGEValor,
            this.colPGEMensagem});
            this.gridView2.GridControl = this.listaPaGamentosEspecificosGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colPGEValor
            // 
            this.colPGEValor.Caption = "Valor";
            this.colPGEValor.DisplayFormat.FormatString = "n2";
            this.colPGEValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPGEValor.FieldName = "PGEValor";
            this.colPGEValor.Name = "colPGEValor";
            this.colPGEValor.OptionsColumn.AllowEdit = false;
            this.colPGEValor.OptionsColumn.FixedWidth = true;
            this.colPGEValor.Visible = true;
            this.colPGEValor.VisibleIndex = 1;
            this.colPGEValor.Width = 140;
            // 
            // colPGEMensagem
            // 
            this.colPGEMensagem.Caption = "Mensagem";
            this.colPGEMensagem.FieldName = "PGEMensagem";
            this.colPGEMensagem.Name = "colPGEMensagem";
            this.colPGEMensagem.OptionsColumn.AllowEdit = false;
            this.colPGEMensagem.Visible = true;
            this.colPGEMensagem.VisibleIndex = 0;
            this.colPGEMensagem.Width = 948;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gridControl1);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 323);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1568, 253);
            this.groupControl3.TabIndex = 30;
            this.groupControl3.Text = "Boletos";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.fKBOletoDetalhePrevisaoBOletosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 20);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEdit1Bloco,
            this.LookUpEditAPT,
            this.LookUpEditCargo,
            this.repositoryItemImageComboBox1,
            this.repositoryItemImageComboBox2,
            this.repositoryItemImageComboBox3,
            this.repositoryBotao,
            this.repositoryItemCalcEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1564, 231);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // fKBOletoDetalhePrevisaoBOletosBindingSource
            // 
            this.fKBOletoDetalhePrevisaoBOletosBindingSource.DataMember = "FK_BOletoDetalhe_PrevisaoBOletos";
            this.fKBOletoDetalhePrevisaoBOletosBindingSource.DataSource = this.previsaoBOletosBindingSource;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(188)))), ((int)(((byte)(237)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(188)))), ((int)(((byte)(237)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(205)))), ((int)(((byte)(241)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(188)))), ((int)(((byte)(237)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(188)))), ((int)(((byte)(237)))));
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite;
            this.gridView1.Appearance.EvenRow.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseFont = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(8)))), ((int)(((byte)(107)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(78)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(128)))), ((int)(((byte)(227)))));
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(178)))), ((int)(((byte)(227)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(178)))), ((int)(((byte)(227)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(78)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.gridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(213)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.OddRow.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseFont = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(228)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(78)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(188)))), ((int)(((byte)(237)))));
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(187)))));
            this.gridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(168)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOD_BOL,
            this.colBODValor,
            this.colBODMensagem,
            this.colBOD_APT,
            this.colBODProprietario,
            this.colBODData,
            this.colCopiaBOD_APT,
            this.colCopiaBOD_APT1,
            this.colBOL_StatusRegistro,
            this.colBOLStatus,
            this.colBotao});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOD_APT, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCopiaBOD_APT, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colBOD_BOL
            // 
            this.colBOD_BOL.Caption = "Boleto";
            this.colBOD_BOL.FieldName = "BOD_BOL";
            this.colBOD_BOL.Name = "colBOD_BOL";
            this.colBOD_BOL.OptionsColumn.FixedWidth = true;
            this.colBOD_BOL.OptionsColumn.ReadOnly = true;
            this.colBOD_BOL.Visible = true;
            this.colBOD_BOL.VisibleIndex = 7;
            this.colBOD_BOL.Width = 83;
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colBODValor.DisplayFormat.FormatString = "n2";
            this.colBODValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.OptionsColumn.FixedWidth = true;
            this.colBODValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", "{0:n2}")});
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 5;
            this.colBODValor.Width = 78;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colBODMensagem
            // 
            this.colBODMensagem.Caption = "Mensagem";
            this.colBODMensagem.FieldName = "BODMensagem";
            this.colBODMensagem.MinWidth = 60;
            this.colBODMensagem.Name = "colBODMensagem";
            this.colBODMensagem.Visible = true;
            this.colBODMensagem.VisibleIndex = 4;
            this.colBODMensagem.Width = 238;
            // 
            // colBOD_APT
            // 
            this.colBOD_APT.Caption = "Bloco";
            this.colBOD_APT.ColumnEdit = this.LookUpEdit1Bloco;
            this.colBOD_APT.FieldName = "BOD_APT";
            this.colBOD_APT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colBOD_APT.Name = "colBOD_APT";
            this.colBOD_APT.OptionsColumn.FixedWidth = true;
            this.colBOD_APT.OptionsColumn.ReadOnly = true;
            this.colBOD_APT.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.colBOD_APT.Visible = true;
            this.colBOD_APT.VisibleIndex = 0;
            this.colBOD_APT.Width = 68;
            // 
            // LookUpEdit1Bloco
            // 
            this.LookUpEdit1Bloco.AutoHeight = false;
            this.LookUpEdit1Bloco.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit1Bloco.DataSource = this.aPTLoockupBindingSource;
            this.LookUpEdit1Bloco.DisplayMember = "BLOCO";
            this.LookUpEdit1Bloco.Name = "LookUpEdit1Bloco";
            this.LookUpEdit1Bloco.ValueMember = "APT";
            // 
            // aPTLoockupBindingSource
            // 
            this.aPTLoockupBindingSource.DataMember = "APTLoockup";
            this.aPTLoockupBindingSource.DataSource = this.dPrevBolCampos;
            // 
            // colBODProprietario
            // 
            this.colBODProprietario.Caption = "P/I";
            this.colBODProprietario.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colBODProprietario.FieldName = "BODProprietario";
            this.colBODProprietario.Name = "colBODProprietario";
            this.colBODProprietario.OptionsColumn.AllowEdit = false;
            this.colBODProprietario.OptionsColumn.FixedWidth = true;
            this.colBODProprietario.ToolTip = "Padr�o";
            this.colBODProprietario.Visible = true;
            this.colBODProprietario.VisibleIndex = 2;
            this.colBODProprietario.Width = 33;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Propriet�rio", true, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Padr�o", null, -1)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.NullText = "Padr�o";
            this.repositoryItemImageComboBox1.SmallImages = this.imagensPI1;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // colBODData
            // 
            this.colBODData.Caption = "Vencimento";
            this.colBODData.FieldName = "BODData";
            this.colBODData.Name = "colBODData";
            this.colBODData.OptionsColumn.FixedWidth = true;
            this.colBODData.Visible = true;
            this.colBODData.VisibleIndex = 6;
            this.colBODData.Width = 84;
            // 
            // colCopiaBOD_APT
            // 
            this.colCopiaBOD_APT.Caption = "APT";
            this.colCopiaBOD_APT.ColumnEdit = this.LookUpEditAPT;
            this.colCopiaBOD_APT.FieldName = "CopiaBOD_APT";
            this.colCopiaBOD_APT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCopiaBOD_APT.Name = "colCopiaBOD_APT";
            this.colCopiaBOD_APT.OptionsColumn.FixedWidth = true;
            this.colCopiaBOD_APT.OptionsColumn.ReadOnly = true;
            this.colCopiaBOD_APT.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.colCopiaBOD_APT.Visible = true;
            this.colCopiaBOD_APT.VisibleIndex = 1;
            this.colCopiaBOD_APT.Width = 47;
            // 
            // LookUpEditAPT
            // 
            this.LookUpEditAPT.AutoHeight = false;
            this.LookUpEditAPT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditAPT.DataSource = this.aPTLoockupBindingSource;
            this.LookUpEditAPT.DisplayMember = "APTNumero";
            this.LookUpEditAPT.Name = "LookUpEditAPT";
            this.LookUpEditAPT.ValueMember = "APT";
            // 
            // colCopiaBOD_APT1
            // 
            this.colCopiaBOD_APT1.Caption = "Cargo";
            this.colCopiaBOD_APT1.ColumnEdit = this.LookUpEditCargo;
            this.colCopiaBOD_APT1.FieldName = "CopiaBOD_APT1";
            this.colCopiaBOD_APT1.Name = "colCopiaBOD_APT1";
            this.colCopiaBOD_APT1.OptionsColumn.FixedWidth = true;
            this.colCopiaBOD_APT1.OptionsColumn.ReadOnly = true;
            this.colCopiaBOD_APT1.Visible = true;
            this.colCopiaBOD_APT1.VisibleIndex = 3;
            this.colCopiaBOD_APT1.Width = 99;
            // 
            // LookUpEditCargo
            // 
            this.LookUpEditCargo.AutoHeight = false;
            this.LookUpEditCargo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditCargo.DataSource = this.aPTLoockupBindingSource;
            this.LookUpEditCargo.DisplayMember = "CGONome";
            this.LookUpEditCargo.Name = "LookUpEditCargo";
            this.LookUpEditCargo.NullText = " -";
            this.LookUpEditCargo.ValueMember = "APT";
            // 
            // colBOL_StatusRegistro
            // 
            this.colBOL_StatusRegistro.Caption = "Registro";
            this.colBOL_StatusRegistro.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colBOL_StatusRegistro.FieldName = "BOLStatusRegistro";
            this.colBOL_StatusRegistro.Name = "colBOL_StatusRegistro";
            this.colBOL_StatusRegistro.OptionsColumn.FixedWidth = true;
            this.colBOL_StatusRegistro.OptionsColumn.ReadOnly = true;
            this.colBOL_StatusRegistro.Visible = true;
            this.colBOL_StatusRegistro.VisibleIndex = 9;
            this.colBOL_StatusRegistro.Width = 109;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colBOLStatus
            // 
            this.colBOLStatus.Caption = "Status";
            this.colBOLStatus.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colBOLStatus.FieldName = "BOLStatus";
            this.colBOLStatus.Name = "colBOLStatus";
            this.colBOLStatus.OptionsColumn.ReadOnly = true;
            this.colBOLStatus.Visible = true;
            this.colBOLStatus.VisibleIndex = 8;
            this.colBOLStatus.Width = 96;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colBotao
            // 
            this.colBotao.Caption = "gridColumn1";
            this.colBotao.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBotao.Name = "colBotao";
            this.colBotao.OptionsColumn.FixedWidth = true;
            this.colBotao.OptionsColumn.ShowCaption = false;
            this.colBotao.Visible = true;
            this.colBotao.VisibleIndex = 10;
            this.colBotao.Width = 25;
            // 
            // repositoryBotao
            // 
            this.repositoryBotao.AutoHeight = false;
            this.repositoryBotao.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::Boletos.Properties.Resources.Bboleto, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryBotao.Name = "repositoryBotao";
            this.repositoryBotao.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryBotao.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryBotao_ButtonClick);
            // 
            // dRateios1
            // 
            this.dRateios1.DataSetName = "dRateios";
            this.dRateios1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PrevBolCampos
            // 
            this.Autofill = false;
            this.AutoScroll = true;
            this.BindingSourcePrincipal = this.previsaoBOletosBindingSource;
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl2);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "PrevBolCampos";
            this.Size = new System.Drawing.Size(1568, 601);
            this.Titulo = "Previs�o de Boletos";
            this.Load += new System.EventHandler(this.PrevBolCampos_Load);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControl3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previsaoBOletosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPrevBolCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBOVencimentoDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBOVencimentoDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONNomeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValorFundo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPlanosSeguroConteudoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFundoR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaPaGamentosEspecificosGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaPaGamentosEspecificosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBOletoDetalhePrevisaoBOletosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit1Bloco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTLoockupBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditAPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditCargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBotao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRateios1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DateEdit pBOVencimentoDateEdit;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource previsaoBOletosBindingSource;
        /// <summary>
        /// 
        /// </summary>
        public dPrevBolCampos dPrevBolCampos;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.TextEdit cONCodigoTextEdit;
        private DevExpress.XtraEditors.TextEdit cONNomeTextEdit;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource fKBOletoDetalhePrevisaoBOletosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_BOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraGrid.Columns.GridColumn colBODMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_APT;
        private DevExpress.XtraGrid.Columns.GridColumn colBODProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colBODData;
        private Boletos.Rateios.dRateios dRateios1;
        private DevExpress.XtraGrid.Columns.GridColumn colCopiaBOD_APT;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit1Bloco;
        private DevExpress.XtraGrid.Columns.GridColumn colCopiaBOD_APT1;
        private System.Windows.Forms.BindingSource aPTLoockupBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditAPT;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditCargo;
        private DevExpress.XtraEditors.TextEdit ValorFundo;
        private System.Windows.Forms.BindingSource listaPaGamentosEspecificosBindingSource;
        private DevExpress.XtraGrid.GridControl listaPaGamentosEspecificosGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEMensagem;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.Label labelFundoReserva;
        private System.Windows.Forms.Label ValorBruto;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.TextEdit TxtFundoR;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private Framework.objetosNeon.cCompet cCompet1;
        private VirBotaoNeon.cBotaoImpNeon cBotaoImpNeon1;
        private DevExpress.XtraEditors.SimpleButton BCancelaEmi;
        private System.Windows.Forms.BindingSource dPlanosSeguroConteudoBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL_StatusRegistro;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraGrid.Columns.GridColumn colBotao;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryBotao;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraRichEdit.RichEditControl richEditControl1;
        private DevExpress.XtraEditors.SimpleButton BotRegNov;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label pBOCompetenciaanoLabel;
        private System.Windows.Forms.Label pBOVencimentoLabel;
        private System.Windows.Forms.Label pBOValorTotalLabel;
        private System.Windows.Forms.Label cONCodigoLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Framework.objetosNeon.ImagensPI imagensPI1;
    }
}
