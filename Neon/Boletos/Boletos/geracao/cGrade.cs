using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Framework.objetosNeon;
using System.Collections;
using Framework;
using VirEnumeracoesNeon;
using DevExpress.XtraGrid.Columns;
using VirEnumeracoes;

namespace Boletos.geracao
{
    /// <summary>
    /// Gerenciador
    /// </summary>
    public partial class cGrade : CompontesBasicos.ComponenteBaseBindingSource
    {
        private ArrayList NovasColunasGrid;
        private ArrayList NovosCamposTabela;
        

        /// <summary>
        /// Construtor
        /// </summary>
        public cGrade()
        {            
            InitializeComponent();
            NovasColunasGrid = new ArrayList();
            NovosCamposTabela = new ArrayList();
            uSUariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosStTodos;
            Enumeracoes.virEnumPBOStatus.virEnumPBOStatusSt.GeraLegenda(legenda1);
            cCompet1.Comp = new Competencia().Add(-6);
            cCompet1.AjustaTela();
            repositoryItemButtonOk.Appearance.BackColor = Enumeracoes.virEnumPBOStatus.virEnumPBOStatusSt.GetCor(PBOStatus.ok);
            repositoryItemButtonBLOQ.Appearance.BackColor = Enumeracoes.virEnumPBOStatus.virEnumPBOStatusSt.GetCor(PBOStatus.Bloqueado);
            repositoryItemButtonAg.Appearance.BackColor = Enumeracoes.virEnumPBOStatus.virEnumPBOStatusSt.GetCor(PBOStatus.aguardaRetorno);
            //repositoryItemButtonNovo
            repositoryItemButtonPreparado.Appearance.BackColor = Enumeracoes.virEnumPBOStatus.virEnumPBOStatusSt.GetCor(PBOStatus.Preparado);
            repositoryItemButtonRegistrando.Appearance.BackColor = Enumeracoes.virEnumPBOStatus.virEnumPBOStatusSt.GetCor(PBOStatus.Registrando);
        }
             
        private dPrevBol.CONDOMINIOSRow Linha;

        private dPrevBol.CONDOMINIOSRow PegaLinha()
        {
            Linha = (dPrevBol.CONDOMINIOSRow)gridView1.GetDataRow(gridView1.FocusedRowHandle);
            return Linha;
        }        

        private PBOStatus? GetPBOStatus(dPrevBol.CONDOMINIOSRow Linha,string Campo)
        {
            if (Linha["PBOStatus" + Campo] == DBNull.Value)
                return null;
            else
                return (PBOStatus)Linha["PBOStatus" + Campo];
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            string ChaveAnterior = CompontesBasicos.Performance.Performance.PerformanceST.ChaveAnterior ?? "Geral";
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("RowCellStyle");
            try
            {
                if (e.RowHandle < 0)
                    return;
                if (NovasColunasGrid == null)
                    return;                
                dPrevBol.CONDOMINIOSRow LinhaAt = (dPrevBol.CONDOMINIOSRow)gridView1.GetDataRow(e.RowHandle);
                if (LinhaAt == null)
                    return;
                if (NovasColunasGrid.Contains(e.Column))
                {
                    PBOStatus? NStatus = GetPBOStatus(LinhaAt, e.Column.FieldName);
                    if (NStatus.HasValue)
                        Enumeracoes.virEnumPBOStatus.virEnumPBOStatusSt.CellStyle(NStatus.Value, e);                    
                }
                else
                    if (e.Column == colProximoVencimento)
                    {
                        if (LinhaAt.CONEmiteBoletos)
                        {
                            if (LinhaAt.IsProximoVencimentoNull())
                                e.Appearance.BackColor = Color.Red;
                            else
                                if (LinhaAt.ProximoVencimento <= DateTime.Today.AddDays(5))
                                    e.Appearance.BackColor = Color.Red;
                                else
                                    if (LinhaAt.ProximoVencimento <= DateTime.Today.AddDays(8))
                                        e.Appearance.BackColor = Color.Yellow;
                                    else
                                        e.Appearance.BackColor = Color.LightGreen;
                        }
                    }
                    else
                        e.Appearance.BackColor = Color.Silver;
                //    stQ = statusQuadrinho.Padrao;
                //e.Appearance.BackColor = CoresCelulas[stQ];
            }
            catch
            {
                e.Appearance.BackColor = Color.Black;
            };
            CompontesBasicos.Performance.Performance.PerformanceST.Registra(ChaveAnterior);
        }                               

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditaPrevisao();            
        }

        private void EditaPrevisao()
        {
        if ((gridView1.FocusedColumn.AbsoluteIndex >= 2) && (gridView1.FocusedValue != DBNull.Value))
            {
                PegaLinha();
                PrevBolCampos Detalhes = new PrevBolCampos();
                Competencia comp = Competencia.Parse(gridView1.FocusedColumn.FieldName, Linha.CON);
                if (comp == null)
                    return;                
                dPrevBolCampos.PrevisaoBOletosTableAdapter.Fill(Detalhes.dPrevBolCampos.PrevisaoBOletos, Linha.CON, comp.CompetenciaBind);
                dPrevBolCampos.CONDOMINIOSTableAdapter.Fill(Detalhes.dPrevBolCampos.CONDOMINIOS, Linha.CON);
                Detalhes.compet = comp;                
                Detalhes.carregadados();
                //  ?? Detalhes.AjustaSomeneLeitura((bool)Linha["Gerado" + gridView1.FocusedColumn.FieldName]);  
                Detalhes.Titulo = string.Format("{0} ({1})", Linha.CONCodigo, gridView1.FocusedColumn.FieldName);
                Detalhes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
  

                //ForPai().ShowMoludointerno(Detalhes, Linha.CONCodigo + "(" + gridView1.FocusedColumn.FieldName + ")", CompontesBasicos.EstadosDosComponentes.JanelasAtivas, Detalhes.somenteleitura);
                //Detalhes.compet = new Competencia(mes, ano);
                //Detalhes.compet.CON = Linha.CON;
                //Detalhes.carregadados();
                //ShowModulo(Detalhes);
            };
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            EditaPrevisao();
        }

        private void LimpaColunasDinamicas()
        {
            foreach (GridColumn colDel in NovasColunasGrid)
                gridView1.Columns.Remove(colDel);
            NovasColunasGrid.Clear();
            foreach (DataColumn CampoDel in NovosCamposTabela)
                dPrevBol.CONDOMINIOS.Columns.Remove(CampoDel);
            NovosCamposTabela.Clear();
        }

        private SortedList<PBOStatus, int> SeqStatus;

        private PBOStatus MenorStatus(PBOStatus st1, PBOStatus st2)
        {
            if (SeqStatus == null)
            {
                SeqStatus = new SortedList<PBOStatus, int>();
                SeqStatus.Add(PBOStatus.Bloqueado, 0);
                SeqStatus.Add(PBOStatus.Preparado, 1);                
                SeqStatus.Add(PBOStatus.Registrando, 2);
                SeqStatus.Add(PBOStatus.aguardaRetorno, 3);
                SeqStatus.Add(PBOStatus.ok, 4);
            }
            return (SeqStatus[st1] < SeqStatus[st2]) ? st1 : st2;
        }

        private GridColumn DvnovaColuna = null;

        private void Atualiza()
        {
            //CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("In�cio");
            int pos = gridView1.FocusedRowHandle;
            LimpaColunasDinamicas();
            DataColumn novaColuna;
            DvnovaColuna = null;
            dPrevBol.SomaIt.Clear();
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Fill");
            dPrevBol.PrevisaoBOletosTableAdapter.FillComp(dPrevBol.PrevisaoBOletos, cCompet1.Comp.CompetenciaBind);
            dPrevBol.SomaItTableAdapter.Fill(dPrevBol.SomaIt, cCompet1.Comp.CompetenciaBind);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Cria colunas");

            //Criando as colunas
            if (dPrevBol.PrevisaoBOletos.Rows.Count > 0)
            {
                dPrevBol.PrevisaoBOletosRow PrimeiraRow = dPrevBol.PrevisaoBOletos[0];
                dPrevBol.PrevisaoBOletosRow UltimaRow = dPrevBol.PrevisaoBOletos[dPrevBol.PrevisaoBOletos.Rows.Count - 1];
                Competencia compI = new Competencia(PrimeiraRow.PBOCompetencia);
                Competencia compF = new Competencia(UltimaRow.PBOCompetencia);
                compF++;
                for (Competencia comp = compI.CloneCompet(); comp <= compF; comp++)
                {
                    //campo na tabela (ram)
                    novaColuna = dPrevBol.CONDOMINIOS.Columns.Add(string.Format("{0}", comp), typeof(string));
                    novaColuna.Caption = string.Format("{0}", comp);
                    NovosCamposTabela.Add(novaColuna);
                    //coluna na grid
                    DvnovaColuna = gridView1.Columns.Add();
                    DvnovaColuna.FieldName = string.Format("{0}", comp);
                    DvnovaColuna.Visible = true;
                    DvnovaColuna.Width = 65;
                    DvnovaColuna.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    DvnovaColuna.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    NovasColunasGrid.Add(DvnovaColuna);
                    novaColuna = dPrevBol.CONDOMINIOS.Columns.Add(string.Format("PBOStatus{0}", comp), typeof(int));
                    NovosCamposTabela.Add(novaColuna);
                };
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular fill");
            //popular
            dPrevBol.CONDOMINIOSTableAdapter.Fill(dPrevBol.CONDOMINIOS, DSCentral.EMP);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular");
            foreach (dPrevBol.CONDOMINIOSRow ConRow in dPrevBol.CONDOMINIOS)
            {
                Competencia Proxcomp = null;
                foreach (dPrevBol.PrevisaoBOletosRow PvRow1 in ConRow.GetPrevisaoBOletosRows())
                {

                    Competencia comp = new Competencia(PvRow1.PBOCompetencia, PvRow1.PBO_CON, null);
                    ConRow[comp.ToString()] = string.Format("{0:dd/MM}", PvRow1.PBOVencimento);                    
                    PBOStatus StatusCompetencia = PBOStatus.ok;
                    foreach (dPrevBol.SomaItRow rowSoma in PvRow1.GetSomaItRows())
                    {
                        PBOStatus StatusSoma = PBOStatus.ok;
                        if (rowSoma.BODComCondominio)
                        {
                            if (rowSoma.IsBOLStatusNull())
                                StatusSoma = PBOStatus.Preparado;
                            else
                            {
                                StatusBoleto StatusBol = (StatusBoleto)rowSoma.BOLStatus;
                                StatusRegistroBoleto StatusReg = (StatusRegistroBoleto)rowSoma.BOLStatusRegistro;
                                if (StatusBol == StatusBoleto.EmProducao)
                                    StatusSoma = PBOStatus.Bloqueado;
                                else
                                //else if (StatusBol == Boleto.StatusBoleto.AguardandoRegistro)
                                //else if (!StatusReg.EstaNoGrupo(StatusRegistroBoleto.BoletoSemRegistro,
                                //                                StatusRegistroBoleto.AlterarRegistroData,
                                //                                StatusRegistroBoleto.AlterarRegistroValor,
                                //                                StatusRegistroBoleto.BoletoQuitado))
                                {
                                    switch (StatusReg)
                                    {
                                        case StatusRegistroBoleto.BoletoAguardandoRetornoDA:
                                        case StatusRegistroBoleto.BoletoAguardandoRetorno:
                                            StatusSoma = PBOStatus.aguardaRetorno;
                                            break;
                                        case StatusRegistroBoleto.BoletoRegistrando:
                                        case StatusRegistroBoleto.BoletoRegistrandoDA:
                                            StatusSoma = PBOStatus.Registrando;
                                            break;
                                        //case StatusRegistroBoleto.BoletoRegistrado:
                                        //case StatusRegistroBoleto.BoletoRegistradoDDA:
                                        //    //StatusSoma = PBOStatus.Imprimir;
                                        //    break;
                                    }
                                };


                            }
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Menor Status");
                            StatusCompetencia = MenorStatus(StatusCompetencia, StatusSoma);
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular");
                        }
                    }
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Gravando");
                    if ((PBOStatus)PvRow1.PBOStatus != StatusCompetencia)
                    {
                        PvRow1.PBOStatus = (int)StatusCompetencia;
                        dPrevBol.PrevisaoBOletosTableAdapter.Update(PvRow1);
                        PvRow1.AcceptChanges();
                    };
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular Fim");
                    ConRow[string.Format("PBOStatus{0}", comp)] = PvRow1.PBOStatus;
                    PBOStatus Status = (PBOStatus)PvRow1.PBOStatus;
                    if (Status == PBOStatus.ok)
                        Proxcomp = comp.CloneCompet(1);
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular");
                };
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular Null");
                if (Proxcomp != null)
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular Null A");
                    ConRow.ProximaCompetencia = Proxcomp.CompetenciaBind;
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular Null B");
                    ConRow.ProximoVencimento = Proxcomp.VencimentoPadrao;
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular Null");
                }
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Popular");
            }

            if (DvnovaColuna != null)
                gridView1.FocusedColumn = DvnovaColuna;
            gridView1.FocusedRowHandle = pos;
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("FIM");
            //string relatorio = CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();
            //MessageBox.Show(relatorio);
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Atualiza();            
        }
        
        private void cGrade_Load(object sender, EventArgs e)
        {
            Atualiza();            
        }

        
        /// <summary>
        /// Atualiza dados
        /// </summary>
        public override void RefreshDados() 
        {
            Atualiza();
        }
             
        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") <= 0)
                return;
            if (PegaLinha() == null)
                return;
            Rateios.Rateio.StatusRegerar Status = Boletos.Rateios.Rateio.StatusRegerar.Identico;
            //foreach (System.Collections.DictionaryEntry DE in Rateios.Rateio.RevisarRateios(Linha.CON))
            foreach (Rateios.Rateio.StatusRegerar StatusRat in Rateios.Rateio.RevisarRateios(Linha.CON).Values)
            {
                if (StatusRat > Status)
                    Status = StatusRat;
            };
            if (Status == Rateios.Rateio.StatusRegerar.Diferente)
            {
                if (MessageBox.Show("Existem rateios a serem verificados!\r\nIgnorar ?", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return;
            }
            GeraBoleto.cGeraBol cGeraBol = new GeraBoleto.cGeraBol();
            cGeraBol.alteraCondominio( Linha.CON);
            CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludointerno(cGeraBol, string.Format("{0} - {1}", Linha.CONCodigo, Linha.CONNome), CompontesBasicos.EstadosDosComponentes.JanelasAtivas, false);
        }             

        private void lookupGerente1_alterado(object sender, EventArgs e)
        {
            if (lookupGerente1.USUSel == -1)
                gridView1.ActiveFilterString = "";
            else
                gridView1.ActiveFilterString = string.Format("[CONAuditor1_USU] = {0}", lookupGerente1.USUSel);
        }


        /// <summary>
        /// Chamada gen�rica
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            int USUg = (int)args[0];
            lookupGerente1.USUSel = USUg;
            if(USUg == -1)
                gridView1.ActiveFilterString = string.Format("[ProximoVencimento] <= #{0:yyyy-MM-dd}# and [CONEmiteBoletos] = true", DateTime.Today.AddDays(5));
            else
                gridView1.ActiveFilterString = string.Format("[ProximoVencimento] <= #{0:yyyy-MM-dd}# and [CONAuditor1_USU] = {1} and [CONEmiteBoletos] = true", DateTime.Today.AddDays(5), lookupGerente1.USUSel);
            return null;
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            Atualiza();
        }

        private void ReImprime()
        {
            if (PegaLinha() == null)
                return;
            Competencia comp = Competencia.Parse(gridView1.FocusedColumn.FieldName, Linha.CON);
            GeraBoleto.GeraBol GeraBol1 = new GeraBoleto.GeraBol(this);
            SortedList<int, Boleto.Boleto> BoletosGerados = new SortedList<int, Boleto.Boleto>();
            BoletosProc.Boleto.dBoletos dBoletos1 = new BoletosProc.Boleto.dBoletos();
            dBoletos1.BOLetosTableAdapter.FillByCONComp(dBoletos1.BOLetos, (short)comp.Ano, (short)comp.Mes, Linha.CON);
            dBoletos1.BOletoDetalheTableAdapter.FillByCONComp(dBoletos1.BOletoDetalhe, (short)comp.Ano, (short)comp.Mes, Linha.CON);
            dBoletos1.BoletoDEscontoTableAdapter.FillByCONComp(dBoletos1.BoletoDEsconto, Linha.CON, (short)comp.Mes, (short)comp.Ano);
            dBoletos1.CarregaAPTs(Linha.CON);
            foreach (BoletosProc.Boleto.dBoletos.BOLetosRow rowBOL in dBoletos1.BOLetos)
                if (((StatusBoleto)rowBOL.BOLStatus == StatusBoleto.Valido) && (rowBOL.BOLTipoCRAI == "C"))
                {                    
                    //Boleto.Boleto NovoBoleto = new Boleto.Boleto(rowBOL.BOL);
                    Boleto.Boleto NovoBoleto = new Boleto.Boleto(rowBOL);
                    BoletosGerados.Add(NovoBoleto.BOL,NovoBoleto);
                }
            if (BoletosGerados.Count > 0)
            {
                /*
                bool Super = false;
                switch (MessageBox.Show("Usar SuperBoletos ?", "Super Boletos", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                { 
                    case DialogResult.Yes:
                        Super = true;
                        break;
                    case DialogResult.No:
                        Super = false;
                        break;
                    case DialogResult.Cancel:
                    default:
                        return;
                }*/
                GeraBol1.EmissaoEfetiva(Linha.CON, comp, true, BoletosGerados);
            }
        }

        private void Desbloquear()
        {
            if (PegaLinha() == null)
                return;
            /*
            bool Super = false;
            switch (MessageBox.Show("Usar SuperBoletos ?", "Super Boletos", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {
                case DialogResult.Yes:
                    Super = true;
                    break;
                case DialogResult.No:
                    Super = false;
                    break;
                case DialogResult.Cancel:
                default:
                    return;
            };*/


            Competencia comp = Competencia.Parse(gridView1.FocusedColumn.FieldName, Linha.CON);
            GeraBoleto.GeraBol GeraBol1 = new GeraBoleto.GeraBol(this);
            SortedList<int, Boleto.Boleto> BoletosGerados = new SortedList<int, Boleto.Boleto>();
            BoletosProc.Boleto.dBoletos dBoletos1 = new BoletosProc.Boleto.dBoletos();
            dBoletos1.BOLetosTableAdapter.FillByRetidos(dBoletos1.BOLetos,(short)comp.Ano, (short)comp.Mes,Linha.CON);
            dBoletos1.BOletoDetalheTableAdapter.FillByCONComp(dBoletos1.BOletoDetalhe,(short)comp.Ano, (short)comp.Mes,Linha.CON);
            foreach (BoletosProc.Boleto.dBoletos.BOLetosRow rowBOL in dBoletos1.BOLetos)
            {
                Boleto.Boleto BolDestravar = new Boleto.Boleto(rowBOL);
                BolDestravar.Destrava();
                BoletosGerados.Add(BolDestravar.BOL, BolDestravar);
                //rowBOL.BOLStatus = (int)StatusBoleto.Valido;
                //rowBOL.BOLCancelado = false;
                /*
                if ((CompontesBasicosProc.StatusRegistroBoleto)rowBOL.BOLStatusRegistro == Boleto.StatusRegistroBoleto.BoletoSemRegistro)
                {
                    BoletosGerados.Add(new Boleto.Boleto(rowBOL.BOL));
                    rowBOL.BOLStatus = (int)StatusBoleto.Valido;
                    rowBOL.BOLCancelado = false;
                }
                else if ((CompontesBasicosProc.StatusRegistroBoleto)rowBOL.BOLStatusRegistro == Boleto.StatusRegistroBoleto.BoletoRegistrando)
                {
                    rowBOL.BOLStatus = (int)StatusBoleto.Valido;
                    rowBOL.BOLStatusRegistro = (int)CompontesBasicosProc.StatusRegistroBoleto.BoletoRegistrando;
                }


                else if ((StatusBoleto)rowBOL.BOLStatus == Boleto.StatusBoleto.EmProducaoDivididoSemRegistro)
                {
                    BoletosGerados.Add(new Boleto.Boleto(rowBOL.BOL));
                    rowBOL.BOLStatus = (int)StatusBoleto.Parcial;
                    rowBOL.BOLCancelado = false;
                }
 */                
                //dBoletos1.BOLetosTableAdapter.Update(dBoletos1.BOLetos);
                
            }
            if (BoletosGerados.Count > 0)
            {
                
                GeraBol1.EmissaoEfetiva(Linha.CON, comp, true, BoletosGerados);
                Atualiza();
            }
        }

        private void CriaNovaPrevisao()
        {
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") <= 0)
                return;
            if(PegaLinha() == null)
                return;
            Competencia comp = Competencia.Parse(gridView1.FocusedColumn.FieldName, Linha.CON);
            if (comp == null)
                return;
            if (!Linha.IsProximaCompetenciaNull() && (Linha.ProximaCompetencia != comp.CompetenciaBind))
            {
                string Aviso = string.Format("Compet�ncia esperada: {0} - Compet�ncia solicitada {1}\r\n\r\nConfirma a gera��o ?",                                                
                                              new Competencia(Linha.ProximaCompetencia),
                                              comp);
                if (MessageBox.Show(Aviso, "ATEN��O",MessageBoxButtons.YesNo,MessageBoxIcon.Warning,MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;
            }


            string TituloTab = string.Format("NOVO {0} ({1})", Linha.CONCodigo, comp);
            foreach (DevExpress.XtraTab.XtraTabPage Tab in CompontesBasicos.FormPrincipalBase.FormPrincipal.TabControl_F.TabPages)
                if (Tab.Text == TituloTab)
                {
                    CompontesBasicos.FormPrincipalBase.FormPrincipal.TabControl_F.SelectedTabPage = Tab;
                    return;
                };
            PrevBolCampos Detalhes = new PrevBolCampos();
            dPrevBolCampos.CONDOMINIOSTableAdapter.Fill(Detalhes.dPrevBolCampos.CONDOMINIOS, Linha.CON);
            if (Detalhes.dPrevBolCampos.CONDOMINIOS.Count != 1)
            {
                MessageBox.Show("Falha: Condom�nio n�o encontrado!!");
                return;
            };
            dPrevBolCampos.CONDOMINIOSRow Condrow = Detalhes.dPrevBolCampos.CONDOMINIOS[0];
            Detalhes.somenteleitura = false;
            dPrevBolCampos.PrevisaoBOletosRow NovaLinha = Detalhes.dPrevBolCampos.PrevisaoBOletos.NewPrevisaoBOletosRow();
            NovaLinha.PBO_CON = Linha.CON;
            NovaLinha.PBOCompetencia = comp.CompetenciaBind;
            NovaLinha.PBOStatus = (int)PBOStatus.Preparado;

            if (!Condrow.IsCONMenBoletoNull())
            {
                String Manobra = Condrow.CONMenBoleto;
                if (Manobra.Contains("@M"))
                    if (Condrow.IsCONValorMultaNull())
                        MessageBox.Show("ATEN��O: Foi inclu�do o marcador @M na mensagem por�m n�o foi definido um valor para multa!");
                    else
                        Manobra = Manobra.Replace("@M", Condrow.CONValorMulta.ToString("0"));
                if (Manobra.Contains("@J"))
                    if (Condrow.IsCONValorJurosNull())
                        MessageBox.Show("ATEN��O: Foi inclu�do o marcador @J na mensagem por�m n�o foi definido um valor para o juros!");
                    else
                        Manobra = Manobra.Replace("@J", Condrow.CONValorJuros.ToString("0"));
                NovaLinha.PBOMensagem = Manobra;
            }
            if (Condrow.IsCONValorCondominioNull())
                NovaLinha.PBOValorTotal = 0;
            else
                NovaLinha.PBOValorTotal = Condrow.CONValorCondominio;
            if (Condrow.CONTipoFR == "F")
                NovaLinha.PBOValorFR = 0;
            else
            {
                if (Condrow.IsCONValorFRNull())
                    Condrow.CONValorFR = 0;
                NovaLinha.PBOValorFR = NovaLinha.PBOValorTotal * Condrow.CONValorFR / 100;
            }
            //NovaLinha.PBOVencimento = ultimo;
            //NovaLinha.PBOVencimento = CalculaVencimento(ano, mes, Condrow.CONReferenciaVencimento, Condrow.CONCompetenciaVencida, Condrow.CONTipoVencimento, Condrow.CONDiaVencimento, Condrow.CONPostergaFeriado);
            //NovaLinha.PBOVencimento = Competencia.CalculaVencimento(ano, mes, Condrow.CONReferenciaVencimento, Condrow.CONCompetenciaVencida, Condrow.CONTipoVencimento, Condrow.CONDiaVencimento, Condrow.CONPostergaFeriado);
            NovaLinha.PBOVencimento = comp.VencimentoPadrao;

            Detalhes.dPrevBolCampos.PrevisaoBOletos.AddPrevisaoBOletosRow(NovaLinha);
            Detalhes.compet = comp;
            Detalhes.carregadados();

            Detalhes.Titulo = TituloTab;
            Detalhes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);

            //ForPai().ShowMoludointerno(Detalhes, TituloTab , CompontesBasicos.EstadosDosComponentes.JanelasAtivas, false);

            Caderno.cCadernoEletronico cCadernoEletronico = new Boletos.Caderno.cCadernoEletronico();

            int Carregados = cCadernoEletronico.cADernoEletronicoTableAdapter.FillByComp(cCadernoEletronico.dCaderno.CADernoEletronico, Condrow.CON, (short)comp.Mes, (short)comp.Ano);

            Caderno.dCaderno.CADernoEletronicoRow CADernoEletronicoRow;

            bool Cancela = false;
            if (Carregados == 0)
            {
                CADernoEletronicoRow = cCadernoEletronico.dCaderno.CADernoEletronico.NewCADernoEletronicoRow();
                CADernoEletronicoRow.CAD_CON = Condrow.CON;
                CADernoEletronicoRow.CADCompetenciaAno = (Int16)comp.Ano;
                CADernoEletronicoRow.CADCompetenciaMes = (Int16)comp.Mes;
                CADernoEletronicoRow.CADTexto = "";
            }
            else
            {
                CADernoEletronicoRow = cCadernoEletronico.dCaderno.CADernoEletronico[0];
                if (CADernoEletronicoRow.CADTexto.Contains("!**** PREPARA��O"))
                    Cancela = true;
            };


            if (!Cancela)
            {
                Competencia compAnt = comp.CloneCompet(-1);
                object obj = cCadernoEletronico.cADernoEletronicoTableAdapter.buscaTexto(Condrow.CON, compAnt.Mes, compAnt.Ano);
                string Texto = null;
                if (obj != null)
                    Texto = obj.ToString();
                else
                    Texto = "";
                if (Texto.Contains("****!"))
                    Texto = Texto.Substring(Texto.IndexOf("****!") + 7);
                CADernoEletronicoRow.CADTexto = string.Format("{0}\r\n!**** PREPARA��O: {1}****!\r\n{2}\r\n{3}", CADernoEletronicoRow.CADTexto, DateTime.Now, Texto, CADernoEletronicoRow.CADTexto);
                if (CADernoEletronicoRow.RowState == DataRowState.Detached)
                    cCadernoEletronico.dCaderno.CADernoEletronico.AddCADernoEletronicoRow(CADernoEletronicoRow);
                cCadernoEletronico.cADernoEletronicoTableAdapter.Update(cCadernoEletronico.dCaderno.CADernoEletronico);
            };


        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Right:
                    ReImprime();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    EditaPrevisao();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph:
                    if ((string)e.Button.Tag == "Cadeado")
                        Desbloquear();
                    else if ((string)e.Button.Tag == "Novo")
                        CriaNovaPrevisao();
                    else if ((string)e.Button.Tag == "ReImprime")
                        ReImprime();
                    break;
            }            
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            string ChaveAnterior = CompontesBasicos.Performance.Performance.PerformanceST.ChaveAnterior ?? "Geral";
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CustomRowCellEdit");
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") <= 0)
                return;
            if (e.RowHandle < 0)
                return;
            if (NovasColunasGrid == null)
                return;
            
            dPrevBol.CONDOMINIOSRow LinhaAt = (dPrevBol.CONDOMINIOSRow)gridView1.GetDataRow(e.RowHandle);
            if (LinhaAt == null)
                return;
            
            if (NovasColunasGrid.Contains(e.Column))
            {
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("GetPBOStatus");
                PBOStatus? NStatus = GetPBOStatus(LinhaAt, e.Column.FieldName);
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("CustomRowCellEdit");
                if (NStatus.HasValue)
                    switch (NStatus.Value)
                    {
                        case PBOStatus.Bloqueado:
                            e.RepositoryItem = repositoryItemButtonBLOQ;
                            break;
                        case PBOStatus.Registrando:   
                         e.RepositoryItem = repositoryItemButtonRegistrando;
                            break;                        
                        case PBOStatus.Preparado:
                            e.RepositoryItem = repositoryItemButtonPreparado;
                            break;                        
                        case PBOStatus.ok:
                            e.RepositoryItem = repositoryItemButtonOk;
                            break;
                        case PBOStatus.aguardaRetorno:
                            e.RepositoryItem = repositoryItemButtonAg;
                            break;
                        default:
                            break;
                    }
                else
                    e.RepositoryItem = repositoryItemButtonNovo;
            }

            CompontesBasicos.Performance.Performance.PerformanceST.Registra(ChaveAnterior);
            
            
        }

                            
    }
}

