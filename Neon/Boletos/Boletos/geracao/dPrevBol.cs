﻿namespace Boletos.geracao
{


    partial class dPrevBol
    {


        private dPrevBolTableAdapters.SomaItTableAdapter somaItTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SomaIt
        /// </summary>
        public dPrevBolTableAdapters.SomaItTableAdapter SomaItTableAdapter
        {
            get
            {
                if (somaItTableAdapter == null)
                {
                    somaItTableAdapter = new dPrevBolTableAdapters.SomaItTableAdapter();
                    somaItTableAdapter.TrocarStringDeConexao();
                };
                return somaItTableAdapter;
            }
        }

        private dPrevBolTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dPrevBolTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dPrevBolTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dPrevBolTableAdapters.PrevisaoBOletosTableAdapter previsaoBOletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PrevisaoBOletos
        /// </summary>
        public dPrevBolTableAdapters.PrevisaoBOletosTableAdapter PrevisaoBOletosTableAdapter
        {
            get
            {
                if (previsaoBOletosTableAdapter == null)
                {
                    previsaoBOletosTableAdapter = new dPrevBolTableAdapters.PrevisaoBOletosTableAdapter();
                    previsaoBOletosTableAdapter.TrocarStringDeConexao();
                };
                return previsaoBOletosTableAdapter;
            }
        }
    }
}
