using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Boletos
{
    public partial class TesteDeErro : CompontesBasicos.ComponenteCamposBase
    {
        public TesteDeErro()
        {
            InitializeComponent();
        }

        public object oerro;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(oerro.ToString());
        }
    }
}

