/*
13/05/2014 13.2.8.48 LH reativa��o do XML
25/11/2014 14.1.4.90 LH incorpora��o do saldinho
23/01/2015 14.2.4.1  LH Altera��o na busta dos dados do APT para melhoria de performance
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using CompontesBasicos.Espera;
using Framework.objetosNeon;
using CompontesBasicosProc;
using AbstratosNeon;
using VirEnumeracoesNeon;

namespace Boletos.GeraBoleto
{
    /// <summary>
    /// Gerador de boletos
    /// </summary>
    public partial class cGeraBol : CompontesBasicos.ComponenteBaseBindingSource
    {
        
        private string DefCRAI = "C";        
        private decimal Correio;
        private decimal DespBan;

        private SortedList<int, ABS_Apartamento> Apartamentos;

        /// <summary>
        /// Construtor
        /// </summary>
        public cGeraBol()
        {
            InitializeComponent();
        }

        private bool VerificaCadastro() { 
            if(rowDadosCON == null)
               return false;
           if ((rowDadosCON.IsCONMenBoletoNull()) || (rowDadosCON.CONMenBoleto == ""))
            {
                MessageBox.Show("Erro no cadastros: 'Mensagem do boleto n�o cadastrada'");
                return false;
            }
            if(rowDadosCON.IsCON_BCONull()){
                MessageBox.Show("Erro no cadastros: 'Banco n�o Cadastrado para o condom�nio'");
                return false;
            };
            if (rowDadosCON.IsCONAgenciaNull())
            {
                MessageBox.Show("Erro no cadastros: 'Ag�ncia n�o cadastrada'");
                return false;
            }
            else {
                if (rowDadosCON.CONAgencia.Length != 4)
                {
                    MessageBox.Show("Erro no cadastros: 'Ag�ncia deve conter 4 digitos'");
                    return false;
                }
            };
            if (rowDadosCON.IsCONContaNull())
            {
                MessageBox.Show("Erro no cadastros: 'Conta n�o cadastrada'");
                return false;
            }
            else
            {
                if (rowDadosCON.CONConta.Length < 5)
                {
                    MessageBox.Show("Erro no cadastros: 'Conta corrente deve conter 6 digitos'");
                    return false;
                }
            };
            if (rowDadosCON.IsCONDigitoContaNull())
            {
                MessageBox.Show("Erro no cadastros: 'Digito da conta n�o cadastrado'");
                return false;
            };
            if (rowDadosCON.IsCONDigitoAgenciaNull())
            {
                MessageBox.Show("Erro no cadastros: 'Digito da ag�ncia n�o cadastrado'");
                return false;
            };
            return true;
        }

        private void lookupCondominio_F1_alterado(object sender, EventArgs e){
            alteraCondominio();
        }

        /// <summary>
        /// Condominio
        /// </summary>
        public int CON;

        /// <summary>
        /// Altera o condom�nio
        /// </summary>
        /// <param name="_CON"></param>
        public void alteraCondominio(Int32 _CON) {
            CON = _CON;
            lookupCondominio_F1.CON_sel = _CON;           
            lookupCondominio_F1.Enabled = false;
            alteraCondominio();
        }        

        private void alteraCondominio()
        {            
            competenciasTableAdapter.TrocarStringDeConexao();
            //competenciasTableAdapter.Fill(dGeraBol.Competencias, chComCondominio.Checked, lookupCondominio_F1.CON_sel);
            competenciasTableAdapter.Fill(dGeraBol.Competencias, lookupCondominio_F1.CON_sel);
            List<dGeraBol.CompetenciasRow> apagar = new List<dGeraBol.CompetenciasRow>();
            foreach (dGeraBol.CompetenciasRow row in dGeraBol.Competencias)
                if (row.BODComCondominio != chComCondominio.Checked)
                    apagar.Add(row);

            foreach (dGeraBol.CompetenciasRow row in apagar)
                row.Delete();
            dGeraBol.Competencias.AcceptChanges();

            radioGroup1.Properties.Items.Clear();
            checkedListBoxControl1.Items.Clear();
            radioGroup1.SelectedIndex = -1;
            this.dGeraBol.IntensParaBoleto.Clear();
            foreach (dGeraBol.CompetenciasRow Comp in dGeraBol.Competencias)
            {
                Framework.objetosNeon.Competencia _Comp = new Competencia(Comp.BODCompetencia, lookupCondominio_F1.CON_sel,null);
                radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(_Comp, _Comp.ToString()));
            };
            if (radioGroup1.Properties.Items.Count > 15)
            {
                Int16 colunas = (Int16)(1 + radioGroup1.Properties.Items.Count / 2);
                radioGroup1.Width = 60 * colunas;
                radioGroup1.Height = 50;
            }
            else
            {                
                radioGroup1.Width = 300;
                radioGroup1.Height = 67;
            };
            LevantaSaldinhos();
            
        }

        /// <summary>
        /// Compet�ncia
        /// </summary>
        public Framework.objetosNeon.Competencia Comp;

        private void BuscaDadosCondominio(int CON) 
        {
            dadosCONDOMINIOSBOLTableAdapter1.Fill(dGeraBol.DadosCONDOMINIOSBOL, CON);
            Apartamentos = ABS_Apartamento.ABSGetApartamentos(CON);
            chDescontos.Checked = chDescontos.Enabled = ((int)dBoletos.DescontoAPartamentoTableAdapter.TotalporCON(CON) > 0);            
            if (dGeraBol.DadosCONDOMINIOSBOL.Rows.Count != 1)
            {
                MessageBox.Show("Condom�nio n�o encontrado");
                return;
            };
            rowDadosCON = (dGeraBol.DadosCONDOMINIOSBOLRow)dGeraBol.DadosCONDOMINIOSBOL.Rows[0];
            if (!rowDadosCON.IsCONValorBoletoNull())
            {
                CHBanco.Enabled = true;
                DespBan = rowDadosCON.CONValorBoleto;
            }
            else
                CHBanco.Enabled = false;
            if (!rowDadosCON.IsCONRepassaDespBancNull() && !rowDadosCON.IsCONValorBoletoNull() && rowDadosCON.CONRepassaDespBanc)
                CHBanco.Checked = true;
            else
                CHBanco.Checked = false;

            Correio = 0;
            CHCorreio.Enabled = false;
            CHCorreio.Checked = false;

            if (!rowDadosCON.IsCONCorreioBoletoNull())
            {
                CHCorreio.Enabled = true;
                if (rowDadosCON.CONCorreioBoleto == "CS")
                    if (!rowDadosCON.IsCONValorCartaSimplesNull())
                        Correio = rowDadosCON.CONValorCartaSimples;
                    else
                        Correio = 0;
                else
                    if (!rowDadosCON.IsCONValorARNull())
                        Correio = rowDadosCON.CONValorAR;
                    else
                        Correio = 0;

            };

            if (!rowDadosCON.IsCONRepassaCorreioNull() && !rowDadosCON.IsCONCorreioBoletoNull() && rowDadosCON.CONRepassaCorreio)
            {
                if (rowDadosCON.CONCorreioBoleto == "CS")
                    if (!rowDadosCON.IsCONValorCartaSimplesNull())
                        Correio = rowDadosCON.CONValorCartaSimples;
                    else
                        Correio = 0;
                else
                    if (!rowDadosCON.IsCONValorARNull())
                        Correio = rowDadosCON.CONValorAR;
                    else
                        Correio = 0;
                CHCorreio.Checked = true;
            }
            else
                CHCorreio.Checked = false;            
            
        }    



        private dGeraBol.PrevisaoBOletosRow LinhaPBO;

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            //CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral");
            if(radioGroup1.EditValue != null){

                //MessageBox.Show("1");
                Lista.Clear();
                //MessageBox.Show("2");
                Comp = (Framework.objetosNeon.Competencia)radioGroup1.EditValue;
                //MessageBox.Show("3");
                if (chComCondominio.Checked)
                {
                    //MessageBox.Show("4");
                    if (1 == dGeraBol.PrevisaoBOletosTableAdapter.Fill(dGeraBol.PrevisaoBOletos, Comp.CON, Comp.CompetenciaBind))
                    {
                        //MessageBox.Show("5");
                        LinhaPBO = (dGeraBol.PrevisaoBOletosRow)dGeraBol.PrevisaoBOletos.Rows[0];
                    }
                    else
                        LinhaPBO = null;

                }
                
                if ((rowDadosCON == null) || (rowDadosCON.CON != lookupCondominio_F1.CON_sel))                                   
                    BuscaDadosCondominio(lookupCondominio_F1.CON_sel);


                this.rateiosDaCompetenciaTableAdapter.Fill(this.dGeraBol.RateiosDaCompetencia, chComCondominio.Checked, lookupCondominio_F1.CON_sel, Comp.CompetenciaBind);             
                checkedListBoxControl1.Items.Clear();
                
                itenBOD it;
                bool TemGas = false;
                bool TemAgua = false;
                foreach (dGeraBol.RateiosDaCompetenciaRow Ratrow in dGeraBol.RateiosDaCompetencia)
                {
                    if (Ratrow.BOD_PLA == "102000")
                    {
                        if (!TemGas)
                        {
                            it = new itenBOD("G�s", "102000");
                            TemGas = true;
                            Lista.Add("102000");
                        }
                        else
                            continue;
                    }
                    else if (Ratrow.BOD_PLA == "129000")
                    {
                        if (!TemAgua)
                        {
                            it = new itenBOD("�gua", "129000");
                            TemAgua = true;
                            Lista.Add("129000");
                        }
                        else
                            continue;
                    }
                    else
                    {
                        it = new itenBOD(Ratrow.BODMensagem, "");
                        Lista.Add(Ratrow.BODMensagem);
                    }
                    checkedListBoxControl1.Items.Add(it,true);                    
                };                
                intensParaBoletoBindingSource.Filter = "";
                //o fill abaixo parece sem sentido porque o CalculaItens come�a com um CLEAR
                //this.intensParaBoletoTableAdapter.Fill(this.dGeraBol.IntensParaBoleto, chComCondominio.Checked, lookupCondominio_F1.CON_sel, Comp.CompetenciaBind);                
                CalculaItens();                
            }
            //string Relat = CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();
            //Console.Write(Relat);
            //Clipboard.SetText(Relat);
            //MessageBox.Show(Relat);
        }

        private System.Collections.Generic.List<string> Lista = new List<string>();

        private void CalculaItens() {
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("CalculaItens");
            if ((radioGroup1.SelectedIndex == -1) ||(Comp == null))
                return;            
            this.dGeraBol.IntensParaBoleto.Clear();
            
            DevExpress.XtraEditors.BaseCheckedListBoxControl.CheckedItemCollection Selecionados = checkedListBoxControl1.CheckedItems;
            intensParaBoletoBindingSource.Filter = "BODMensagem='Correio' or BODMensagem='Despesas Banc�rias' or BOD_PLA = '800001'";            

            Lista.Clear();
                        
            foreach (DevExpress.XtraEditors.Controls.CheckedListBoxItem ii in Selecionados)
            {
                
                itenBOD iii = (itenBOD)ii.Value;
                
                    if (iii.BODPLA == "")
                    {
                        intensParaBoletoBindingSource.Filter += " or BODMensagem = '" + iii.BODMensagem.ToString() + "'";
                        Lista.Add(iii.BODMensagem);
                    }
                    else
                    {
                        intensParaBoletoBindingSource.Filter += " or BOD_PLA = '" + iii.BODPLA.ToString() + "'";
                        Lista.Add(iii.BODPLA);
                    }                
            };            
            this.intensParaBoletoTableAdapter.Fill(this.dGeraBol.IntensParaBoleto, chComCondominio.Checked, lookupCondominio_F1.CON_sel,Comp.CompetenciaBind);            
            incluirCorreioCustoBoleto();            
        }

        private void checkedListBoxControl1_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {            
            CalculaItens();            
        }

        
        /*
        private dGeraBol.DadosAPTDataTable ATPProp;
        private dGeraBol.DadosAPTDataTable ATPInc;

        private dGeraBol.DadosAPTRow PegadadosAPT(bool Proprietario, Int32 APT)
        {
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("PegadadosAPT");
            if (ATPProp == null)
            {
                ATPProp = dGeraBol.DadosAPTTableAdapter.GetDataProp(CON);
                ATPInc = dGeraBol.DadosAPTTableAdapter.GetDataInq(CON);
            }
            dGeraBol.DadosAPTDataTable ATPx = Proprietario ? ATPProp : ATPInc;
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral");
            return ATPx.FindByAPT(APT);            
        }
        */

        private int Simulado;

        private int SimulaBOD()
        {
            return Simulado--;
        }

        private class PortaSaldinhos
        {
            public List<Boleto.Boleto> Bols;

            public decimal Total(DateTime DataCalculo)
            {
                decimal retorno = 0;
                foreach (Boleto.Boleto curBoleto in Bols)
                {
                    curBoleto.Valorpara(DataCalculo);
                    retorno += curBoleto.valorfinal;
                }
                return retorno;
            }

            public PortaSaldinhos(Boleto.Boleto BoletoEntra)
            {                                
                Bols = new List<Boleto.Boleto>();
                Bols.Add(BoletoEntra);
            }

            public void Add(Boleto.Boleto BoletoEntra)
            {                
                Bols.Add(BoletoEntra);                
            }
        }

        private SortedList<string, PortaSaldinhos> Saldinhos;

        private bool LevantaSaldinhos()
        {
            string Comando = "SELECT BOL FROM BOLetos WHERE (BOL_CON = @P1) AND (BOLStatus in (15,16) and (BOLCancelado = 0))";
            System.Data.DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(Comando, lookupCondominio_F1.CON_sel);
            if (DT != null)
            {
                Saldinhos = new SortedList<string, PortaSaldinhos>();
                foreach (System.Data.DataRow DR in DT.Rows)
                {
                    int BOL = (int)DR[0];
                    Boleto.Boleto BoletoSaldinho = new Boleto.Boleto(BOL);
                    string chave = string.Format("{0}{1}",BoletoSaldinho.APT,BoletoSaldinho.Proprietario ? "P":"I");
                    if (Saldinhos.ContainsKey(chave))                    
                        Saldinhos[chave].Add(BoletoSaldinho);                    
                    else                    
                        Saldinhos.Add(chave, new PortaSaldinhos(BoletoSaldinho));                    
                }
                return true;
            }
            else
                return false;
        }

        private void incluirCorreioCustoBoleto()
        {
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("incluirCorreioCustoBoleto");
            DateTime DataPadrao;
            if (chComCondominio.Checked)
                if (LinhaPBO != null)
                    DataPadrao = LinhaPBO.PBOVencimento;
                else
                    DataPadrao = Comp.VencimentoPadrao;
            else
                DataPadrao = Comp.VencimentoPadrao;
            //dGeraBol.DadosAPTRow rowAPT;
            
            foreach (dGeraBol.IntensParaBoletoRow rowIten in dGeraBol.IntensParaBoleto)
            {
                ABS_Apartamento Apartamento1 = Apartamentos[rowIten.APT];
                //caso do inquilino pagao
                //Rateio
                if (!Apartamento1.Alugado)
                    rowIten.BODProprietario = true;
                else
                {
                    if (!rowIten.IsBOD_RADNull())
                        switch (Apartamento1.InquilinoPagaRateio)
                        {
                            case TipoPagIP.NaoPaga:
                                rowIten.BODProprietario = true;
                                break;
                            case TipoPagIP.Paga:
                                rowIten.BODProprietario = false;
                                break;
                            case TipoPagIP.SeguePLA:                                
                                break;
                        };
                }
                    //if (rowIten.BODProprietario)//rateio de propriet�rio
                    //    rowIten.BODProprietario = Apartamento1.InquilinoPagaRateio.EstaNoGrupo(TipoPagIP.Paga,TipoPagIP.SeguePLA);
                    //{
                        //rowAPT = PegadadosAPT(false, rowIten.APT);
                        //if (rowAPT != null)//tem inquilino
                        //if (Apartamento1.Alugado)//tem inquilino
                        //{
                        //    if ((!rowAPT.IsAPTInquilinoPagaRateioNull()) && (rowAPT.APTInquilinoPagaRateio))//� pag�o: puxa para ele
                         //       rowIten.BODProprietario = false;
                       // }
                    //}
                   // else//rateio de inquilino
                     //   rowIten.BODProprietario = Apartamento1.InquilinoPagaRateio.EstaNoGrupo(TipoPagIP.NaoPaga, TipoPagIP.SeguePLA);
                    //{

                       // rowAPT = PegadadosAPT(false, rowIten.APT);
                       // if (rowAPT != null)//tem inquilino
                       // {
                       //     if ((!rowAPT.IsAPTInquilinoPagaRateioNull()) && (!rowAPT.APTInquilinoPagaRateio))//n�o paga nada: empurra para propriet�rio
                        //        rowIten.BODProprietario = true;
                       // }
                       // else
                       //     rowIten.BODProprietario = true;
                   // };
                

                //rowAPT = PegadadosAPT(rowIten.BODProprietario, rowIten.APT);
                //if (rowAPT == null)
                //{
                //    if (rowIten.BODProprietario)
                //    {
                //        MessageBox.Show("Erro no cadastros: 'Propriet�rio n�o Cadastrado' \r\nbloco/Ap: " + rowIten.BLOCodigo + "/" + rowIten.APTNumero);
                //    }
                //    else
                //    {
                //        rowIten.BODProprietario = true;
                //        rowAPT = PegadadosAPT(rowIten.BODProprietario, rowIten.APT);
                //        if (rowAPT == null)
                //        {
                //            MessageBox.Show("Erro no cadastros: 'Propriet�rio n�o Cadastrado' \r\nbloco/Ap: " + rowIten.BLOCodigo + "/" + rowIten.APTNumero);
                //        }
                //    }
                //};
                //ajusta data diferente

                if (chComCondominio.Checked)
                {
                    //if ((!rowAPT.IsAPTDiaDiferenteNull()) && (rowAPT.APTDiaDiferente))
                    if(Apartamento1.APTDiaDiferente)
                        rowIten.BODData = Apartamento1.DataVencimento(Comp);
                        //rowIten.BODData = Comp.DataVencimenot(rowIten.APT);

                    else
                        if (rowIten.BODData != DataPadrao)
                            rowIten.BODData = DataPadrao;
                }
            };
            if ((!CHCorreio.Checked) && (!CHBanco.Checked) && (Saldinhos == null))
                return;
            System.Collections.ArrayList novasrows = new System.Collections.ArrayList();
            dGeraBol.IntensParaBoletoRow novarow;
            //String boletoAnterior = "";
            System.Collections.SortedList novosBoletos = new SortedList();
            foreach (dGeraBol.IntensParaBoletoRow rowIten in dGeraBol.IntensParaBoleto)
            {

                if ((Lista.Count > 0) && (!Lista.Contains(rowIten.BODMensagem)) && (!Lista.Contains(rowIten.BOD_PLA)))
                    continue;                

                if (!novosBoletos.Contains(rowIten.BOLETO))
                {
                    //novosBoletos.Add(rowIten.BOLETO, rowIten);
                    novosBoletos.Add(rowIten.BOLETO, rowIten.BODValor);
                    //rowAPT = PegadadosAPT(rowIten.BODProprietario, rowIten.APT); 
                    //if (rowAPT == null)
                    //{
                    //    if (rowIten.BODProprietario)
                    //        MessageBox.Show("Erro no cadastros: 'Propriet�rio n�o Cadastrado' \r\nbloco/Ap: " + rowIten.BLOCodigo + "/" + rowIten.APTNumero);
                    //    else
                    //        MessageBox.Show("Erro no cadastros: 'Inquilino n�o Cadastrado' \r\nbloco/Ap: " + rowIten.BLOCodigo + "/" + rowIten.APTNumero);
                    //    break;
                    //};

                    //AbstratosNeon.FormaPagamento FormaPag = (AbstratosNeon.FormaPagamento)rowAPT.FPG;
                    //if ((rowAPT != null) && (rowAPT.FPG <= 2) && (CHCorreio.Checked))
                    ABS_Apartamento Apartamento1 = Apartamentos[rowIten.APT];
                    //if ((rowAPT != null) && (FormaPag.EstaNoGrupo(AbstratosNeon.FormaPagamento.Correio, AbstratosNeon.FormaPagamento.CorreioEemail)) && (CHCorreio.Checked))
                    if ((Apartamento1.FormaPagamentoEfetivo(null,rowIten.BODProprietario,false).EstaNoGrupo(FormaPagamento.Correio, FormaPagamento.CorreioEemail)) && (CHCorreio.Checked))
                    {
                        novarow = dGeraBol.IntensParaBoleto.NewIntensParaBoletoRow();
                        novarow.BOD = SimulaBOD();
                        novarow.APT = rowIten.APT;
                        novarow.APTNumero = rowIten.APTNumero;
                        novarow.BLO = rowIten.BLO;
                        novarow.BLOCodigo = rowIten.BLOCodigo;
                        novarow.BLONome = rowIten.BLONome;
                        novarow.BOD_PLA = "109000";

                        novarow.BODData = rowIten.BODData;
                        novarow.BODMensagem = "Correio";
                        novarow.BODProprietario = rowIten.BODProprietario;
                        novarow.BODValor = Correio;
                        novarow.BOLETO = rowIten.BOLETO;
                        novarow.BODCompetencia = rowIten.BODCompetencia;                        
                        novarow.EndEdit();
                        novasrows.Add(novarow);
                    };
                    if (CHBanco.Checked)
                    {
                        novarow = dGeraBol.IntensParaBoleto.NewIntensParaBoletoRow();
                        novarow.BOD = SimulaBOD();
                        novarow.APT = rowIten.APT;
                        novarow.APTNumero = rowIten.APTNumero;
                        novarow.BLO = rowIten.BLO;
                        novarow.BLOCodigo = rowIten.BLOCodigo;
                        novarow.BLONome = rowIten.BLONome;
                        novarow.BOD_PLA = "125000";
                        novarow.BODData = rowIten.BODData;
                        novarow.BODMensagem = "Despesas Banc�rias";
                        novarow.BODProprietario = rowIten.BODProprietario;
                        novarow.BODValor = DespBan;
                        novarow.BOLETO = rowIten.BOLETO;
                        novarow.BODCompetencia = rowIten.BODCompetencia;                        
                        novarow.EndEdit();
                        novasrows.Add(novarow);
                    }
                    if (Saldinhos != null)
                    {
                        string chave = string.Format("{0}{1}", rowIten.APT, rowIten.BODProprietario ? "P" : "I");
                        if(Saldinhos.ContainsKey(chave))
                        {
                            novarow = dGeraBol.IntensParaBoleto.NewIntensParaBoletoRow();
                            novarow.BOD = SimulaBOD();
                            novarow.APT = rowIten.APT;
                            novarow.APTNumero = rowIten.APTNumero;
                            novarow.BLO = rowIten.BLO;
                            novarow.BLOCodigo = rowIten.BLOCodigo;
                            novarow.BLONome = rowIten.BLONome;
                            novarow.BOD_PLA = "800001";
                            novarow.BODData = rowIten.BODData;
                            novarow.BODMensagem = "Saldo anterior";
                            novarow.BODProprietario = rowIten.BODProprietario;
                            novarow.BODValor = Saldinhos[chave].Total(rowIten.BODData);
                            novarow.BOLETO = rowIten.BOLETO;
                            novarow.BODCompetencia = rowIten.BODCompetencia;
                            novarow.EndEdit();
                            novasrows.Add(novarow);
                        }
                    }
                }
                else
                {
                    //DictionaryEntry DE = (DictionaryEntry)novosBoletos[rowIten.BOLETO];
                    //decimal TotalAcumulado = (decimal)novosBoletos[rowIten.BOLETO];
                    //TotalAcumulado += rowIten.BODValor;
                    //DE.Value = rowIten.BODValor = (decimal)DE.Value;
                    novosBoletos[rowIten.BOLETO] = (decimal)novosBoletos[rowIten.BOLETO] + rowIten.BODValor;
                }

            }
            foreach (dGeraBol.IntensParaBoletoRow rowIncluir in novasrows)
                if ((decimal)novosBoletos[rowIncluir.BOLETO] != 0)
                    dGeraBol.IntensParaBoleto.AddIntensParaBoletoRow(rowIncluir);
        }

        dGeraBol.DadosCONDOMINIOSBOLRow rowDadosCON;

        

        private BoletosProc.Boleto.dBoletos _dBoletos = null;

        /// <summary>
        /// dBoletos
        /// </summary>
        public BoletosProc.Boleto.dBoletos dBoletos
        {
            get
            {
                return _dBoletos ?? (_dBoletos = new BoletosProc.Boleto.dBoletos());                
            }            
        }

       

        

        

        private void simpleButton1_Click(object sender, EventArgs e)
        {            
            NovoGeraOsBoletos();            
        }

        private class ProtoBoleto 
        {
            public dGeraBol.IntensParaBoletoRow LinhaModelo;
            public List<int> BODs;
            public List<dGeraBol.IntensParaBoletoRow> rowBODs;
            public decimal Total;
            public string CRAI;
            public ProtoBoleto(dGeraBol.IntensParaBoletoRow _LinhaModelo,bool Condominio)
            {
                LinhaModelo = _LinhaModelo;
                BODs = new List<int>();
                BODs.Add(LinhaModelo.BOD);
                rowBODs = new List<dGeraBol.IntensParaBoletoRow>();
                rowBODs.Add(LinhaModelo);
                Total = LinhaModelo.BODValor;
                CRAI = Condominio ? "C" : "I";
                if (!_LinhaModelo.IsBOD_RADNull() && (CRAI != "C"))
                    CRAI = "R";
            }
            public void IncluiLinha(dGeraBol.IntensParaBoletoRow Linha)
            {
                BODs.Add(Linha.BOD);
                rowBODs.Add(Linha);
                Total += Linha.BODValor;
                if (!Linha.IsBOD_PBONull())
                    CRAI = "C";
                else
                    if (!Linha.IsBOD_RADNull() && (CRAI != "C"))
                        CRAI = "R";
            }
            public void RemoverLinha(dGeraBol.IntensParaBoletoRow Linha)
            {
                BODs.Remove(Linha.BOD);
                rowBODs.Remove(Linha);
                Total -= Linha.BODValor;                
            }
        }

        private GeraBol geraBol1;

        private GeraBol GeraBol1
        {
            get 
            { 
                if(geraBol1 == null)
                    geraBol1 = new GeraBol(this);
                return geraBol1;
            }
        }

        private enum Tarefa { Gerar , GerarBloqueado , Cancelar}

        private decimal ValorMinimo
        {
            get { return 20; }
        }

        private void NovoGeraOsBoletos(){           
            if (chComCondominio.Checked && (LinhaPBO == null)) {
                MessageBox.Show("Previs�o n�o gerada!!");
                return;
            };            
            try
            {
                if (rowDadosCON == null)
                    return;
                BoletosRecNCorreio = new SortedList<int, Boleto.Boleto>();
                this.Enabled = false;                
                
                if (!VerificaCadastro())
                    return;
                SortedList<string, ProtoBoleto> novosBoletos = new SortedList<string, ProtoBoleto>();               

                SortedList Alternativas = new SortedList();
                
                Alternativas.Add(Tarefa.Gerar,"Gerar e emitir os Boleto");
                Alternativas.Add(Tarefa.GerarBloqueado,"Gerar os boletos mas n�o emitir ainda.");
                object oResposta = Tarefa.Cancelar;
                //VirInput.Input.Execute("O que voc� quer fazer ?", Alternativas, out Resposta);
                VirInput.Input.Execute("O que voc� quer fazer ?", Alternativas, out oResposta,VirInput.Formularios.cRadioCh.TipoSelecao.radio);
                Tarefa Resposta = (Tarefa)(oResposta ?? Tarefa.Cancelar);
                if (Resposta != Tarefa.Cancelar)
                {
                    using (cEspera esp = new cEspera(this))
                    {
                        esp.Motivo.Text = "Agrupando Boletos";
                        esp.AtivaGauge(dGeraBol.IntensParaBoleto.Count);
                        Application.DoEvents();
                        foreach (dGeraBol.IntensParaBoletoRow rowIten in dGeraBol.IntensParaBoleto)
                        {
                            esp.Gauge();
                            if ((rowIten.BOD == 0))
                                continue;
                            if ((Lista.Count > 0) && (!Lista.Contains(rowIten.BODMensagem)) && (!Lista.Contains(rowIten.BOD_PLA)))
                                continue;                            
                            if (!novosBoletos.ContainsKey(rowIten.BOLETO))
                            {
                                //dGeraBol.DadosAPTRow rowAPT = PegadadosAPT(rowIten.BODProprietario, rowIten.APT);
                                //if (rowAPT == null)
                                //{
                                //    MessageBox.Show("Dados do Apartamento n�o encontrados:" + rowIten.BLONome + "-" + rowIten.APTNumero + " " + (rowIten.BODProprietario ? "P" : "I"));
                                //    return;
                                //};
                                novosBoletos.Add(rowIten.BOLETO, new ProtoBoleto(rowIten, chComCondominio.Checked));
                            }
                            else
                                (novosBoletos[rowIten.BOLETO]).IncluiLinha(rowIten);
                        };
                        esp.Motivo.Text = "Gravando Boletos";
                        CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral A");
                        esp.AtivaGauge(novosBoletos.Count);
                        Application.DoEvents();
                        StatusBoleto enumStatusBoleto;
                        enumStatusBoleto = Resposta == Tarefa.Gerar ? StatusBoleto.Valido : StatusBoleto.EmProducao;

                        string MenErros = "";
                        dBoletos.BOletoDetalheTableAdapter.EmbarcaEmTransST();
                        dBoletos.BOletoDetalheTableAdapter.FillPreCarga(dBoletos.BOletoDetalhe, CON);
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral B");
                        foreach (ProtoBoleto Prot in novosBoletos.Values)
                        {
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral Erro");
                            decimal erro = -Prot.Total;                            
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral J");
                            if (Prot.Total < 0)
                            {
                                List<dGeraBol.IntensParaBoletoRow> Apagar = new List<dGeraBol.IntensParaBoletoRow>();
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral C");
                                //primeiro os adiatamentos e depois os descontos
                                SortedList<int, dGeraBol.IntensParaBoletoRow> Ordenado = new SortedList<int, dGeraBol.IntensParaBoletoRow>();
                                int contador = 1;
                                foreach (dGeraBol.IntensParaBoletoRow rowIten in Prot.rowBODs)
                                {
                                    if (rowIten.BOD_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.PagamentoExtraD))
                                        Ordenado.Add(-contador, rowIten);
                                    else
                                        Ordenado.Add(contador, rowIten);
                                    contador++;
                                }
                                foreach(dGeraBol.IntensParaBoletoRow rowIten in Ordenado.Values)
                                {
                                    if(rowIten.BODValor < 0)
                                    {
                                        if (-rowIten.BODValor <= erro)
                                        {
                                            if(Boleto.Boleto.ReagendaBOD(rowIten.BOD,1))
                                            {
                                                Apagar.Add(rowIten);
                                                Prot.Total += erro;
                                                erro += rowIten.BODValor;                                                
                                            }
                                        }
                                        else
                                        {
                                            //divide
                                            BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBODOriginal = dBoletos.BOletoDetalhe.FindByBOD(rowIten.BOD);
                                            //if (Boleto.Boleto.DivideBOD(rowIten.BOD, rowIten.BODValor + erro, 1).HasValue)
                                            if (Boleto.Boleto.DivideBOD(rowBODOriginal, rowIten.BODValor + erro, Comp.CloneCompet(1)).HasValue)
                                            {
                                                rowIten.BODValor += erro;
                                                Prot.Total += erro;
                                                erro = 0;
                                            }                                            
                                        }
                                        if (erro <= 0)
                                            break;
                                    }
                                }
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Remove Linha",true);
                                foreach(dGeraBol.IntensParaBoletoRow rowIten in Apagar)
                                    Prot.RemoverLinha(rowIten);
                                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                            }
                            else
                                if (Prot.Total < ValorMinimo)
                                { 
                                }
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral K");
                            if (Prot.BODs.Count == 0)
                                continue;
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral L");
                            try
                            {
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral M");
                                VirMSSQL.TableAdapter.AbreTrasacaoSQL("cGeraBol NovoGeraOsBoletos - 764");
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral M1");
                                //if (Prot.LinhaModelo.APT == 47561)
                                //    MessageBox.Show("DEBUGAR");
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("new Boleto",true);
                                Boletos.Boleto.Boleto BoletoGerado = new Boletos.Boleto.Boleto(
                                                                                 dBoletos,
                                                                                 chDescontos.Checked,
                                                                                 Prot.LinhaModelo.BODProprietario,
                                                                                 Apartamentos[Prot.LinhaModelo.APT],
                                                                                 null,
                                                                                 Comp,
                                                                                 Prot.LinhaModelo.BODData,
                                                                                 Prot.CRAI,
                                                                                 enumStatusBoleto,
                                                                                 CHCorreio.Checked,
                                                                                 CHBanco.Checked,
                                                                                 Prot.BODs.ToArray());
                                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral D");
                                if (Saldinhos != null)
                                {
                                    string chave = string.Format("{0}{1}", BoletoGerado.APT, BoletoGerado.Proprietario ? "P" : "I");
                                    if (Saldinhos.ContainsKey(chave))
                                    {
                                        foreach (Boleto.Boleto BoletoSaldo in Saldinhos[chave].Bols)
                                            BoletoGerado.IncluirSaldoAnterior(BoletoSaldo);
                                    }
                                }
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral E");
                                BoletosRecNCorreio.Add(BoletoGerado.BOL, BoletoGerado);
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral F");
                                VirMSSQL.TableAdapter.CommitSQL();
                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral G");
                            }
                            catch (Exception e)
                            {
                                MenErros += e.Message + "\r\n";
                                VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(e);
                                VirMSSQL.TableAdapter.VircatchSQL(e);
                            }
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral H");
                            esp.Gauge();
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral I");
                        }
                        /*
                        if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU == 30)
                        {
                            string Relat = CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();
                            Console.Write(Relat);
                            Clipboard.SetText(Relat);
                            MessageBox.Show(Relat);
                        }*/
                        if (MenErros != "")
                            MessageBox.Show(string.Format("Erro na gera��o de boletos. BOLETOS N�O GERADOS:\r\n\r\n{0}",MenErros), "ATEN��O");
                    }
                    

                    //if (chComCondominio.Checked && (LinhaPBO != null))
                    //{
                    //    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update PrevisaoBOletos set PBOGerado = 1, PBORegistrando = @P2 where PBO = @P1", LinhaPBO.PBO, CHBoletoRegistro.Checked);
                    //};
                                                  

                    //ImpSB = null;

                    //if ((!CHBoletoRegistro.Checked) && (Resposta == 1))
                    if (Resposta == Tarefa.Gerar)
                    {                        
                        GeraBol1.EmissaoEfetiva(lookupCondominio_F1.CON_sel, Comp, chComCondominio.Checked, chSuper.Checked, BoletosRecNCorreio, !ch_email.Checked,false);                        
                    }
                    

                    if (estado == CompontesBasicos.EstadosDosComponentes.JanelasAtivas)
                    {
                        FechaTela(DialogResult.OK);
                    }                    
                };
                
            }
            finally {
                this.Enabled = true;
                BoletosRecNCorreio = null;
                //checkNaoVerificarRateios.Checked = false;
            }
        }

    

    

     

     
       

        private SortedList<int,Boleto.Boleto> BoletosRecNCorreio = null;        
        
        /*
        private void IncluiDescontos(cEspera esp) 
        {
            esp.Motivo.Text = "Incluindo descontos";
            esp.AtivaGauge(BoletosRecNCorreio.Count);
            foreach (Boleto.Boleto BoletoManobra in BoletosRecNCorreio.Values)
            {                                
                BoletoManobra.IncluirDescontos();                
            };
        }
        */
        

        
    

                        

        private string GeraInstrucoes()
        {
            if (rowDadosCON.IsCONMenBoletoNull())
                return "";
            String Manobra = rowDadosCON.CONMenBoleto;
            if (Manobra.Contains("@M"))
                if (rowDadosCON.IsCONValorMultaNull())
                    MessageBox.Show("ATEN��O: Foi inclu�do o marcador @M na mensagem por�m n�o foi definido um valor para multa!");
                else
                    Manobra = Manobra.Replace("@M", rowDadosCON.CONValorMulta.ToString("0"));
            if (Manobra.Contains("@J"))
                if (rowDadosCON.IsCONValorJurosNull())
                    MessageBox.Show("ATEN��O: Foi inclu�do o marcador @J na mensagem por�m n�o foi definido um valor para o juros!");
                else
                    Manobra = Manobra.Replace("@J", rowDadosCON.CONValorJuros.ToString("0"));
            return Manobra;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.Cancel);            
        }

       

        private void CHBanco_CheckedChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("CHB");
            CalculaItens();
        }

        private void CHCorreio_CheckedChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("CHC");
            CalculaItens();
        }

        private void chComCondominio_CheckedChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("CC");
            DefCRAI = (chComCondominio.Checked) ? "C" : "";
            alteraCondominio();
            if (chDescontos.Enabled)
                chDescontos.Checked = chComCondominio.Checked;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Comp == null)
                return;
            int? CONBalBoleto = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CONBalBoleto from condominios where con = @P1", Comp.CON);
            if (CONBalBoleto.HasValue)
            {
                Competencia compBalancete = Comp.CloneCompet(CONBalBoleto.Value);
                AbstratosNeon.ABS_balancete balancete = AbstratosNeon.ABS_balancete.GetBalancete(Comp.CON, compBalancete,TipoBalancete.Boleto);
                if (!balancete.simulaBoleto())               
                    MessageBox.Show(string.Format("Balancete n�o dispon�vel ({0})", compBalancete));
            }
        }

        

       

        




    }

    /// <summary>
    /// Itens do checkbox
    /// </summary>
    public class itenBOD {        
        /// <summary>
        /// 
        /// </summary>
        public string BODMensagem;

        /// <summary>
        /// 
        /// </summary>
        public string BODPLA;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_BODMensagem"></param>
        /// <param name="_BODPLA"></param>
        public itenBOD(string _BODMensagem, string _BODPLA)
        {
            BODPLA = _BODPLA;
            BODMensagem = _BODMensagem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return BODMensagem;
        }
    }

}

