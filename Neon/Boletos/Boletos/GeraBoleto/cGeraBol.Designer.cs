namespace Boletos.GeraBoleto
{
    partial class cGeraBol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cGeraBol));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ch_email = new DevExpress.XtraEditors.CheckEdit();
            this.button2 = new System.Windows.Forms.Button();
            this.chDescontos = new DevExpress.XtraEditors.CheckEdit();
            this.chSuper = new DevExpress.XtraEditors.CheckEdit();
            this.chComCondominio = new DevExpress.XtraEditors.CheckEdit();
            this.CHBanco = new DevExpress.XtraEditors.CheckEdit();
            this.CHCorreio = new DevExpress.XtraEditors.CheckEdit();
            this.button1 = new System.Windows.Forms.Button();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.checkedListBoxControl1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.dGeraBol = new Boletos.GeraBoleto.dGeraBol();
            this.intensParaBoletoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.intensParaBoletoTableAdapter = new Boletos.GeraBoleto.dGeraBolTableAdapters.IntensParaBoletoTableAdapter();
            this.intensParaBoletoGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOD_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLETO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.competenciasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.competenciasTableAdapter = new Boletos.GeraBoleto.dGeraBolTableAdapters.CompetenciasTableAdapter();
            this.rateiosDaCompetenciaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rateiosDaCompetenciaTableAdapter = new Boletos.GeraBoleto.dGeraBolTableAdapters.RateiosDaCompetenciaTableAdapter();
            this.bOletoDetalheTableAdapter = new Boletos.GeraBoleto.dGeraBolTableAdapters.BOletoDetalheTableAdapter();
            this.dadosCONDOMINIOSBOLTableAdapter1 = new Boletos.GeraBoleto.dGeraBolTableAdapters.DadosCONDOMINIOSBOLTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch_email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDescontos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSuper.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chComCondominio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHBanco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHCorreio.Properties)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGeraBol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensParaBoletoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensParaBoletoGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.competenciasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rateiosDaCompetenciaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.ch_email);
            this.panelControl1.Controls.Add(this.button2);
            this.panelControl1.Controls.Add(this.chDescontos);
            this.panelControl1.Controls.Add(this.chSuper);
            this.panelControl1.Controls.Add(this.chComCondominio);
            this.panelControl1.Controls.Add(this.CHBanco);
            this.panelControl1.Controls.Add(this.CHCorreio);
            this.panelControl1.Controls.Add(this.button1);
            this.panelControl1.Controls.Add(this.xtraScrollableControl1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.checkedListBoxControl1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1288, 174);
            this.panelControl1.TabIndex = 0;
            // 
            // ch_email
            // 
            this.ch_email.EditValue = true;
            this.ch_email.Location = new System.Drawing.Point(485, 148);
            this.ch_email.Name = "ch_email";
            this.ch_email.Properties.Caption = "Enviar e-mails";
            this.ch_email.Size = new System.Drawing.Size(118, 19);
            this.ch_email.TabIndex = 15;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Image = global::Boletos.Properties.Resources.Bboleto;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(1095, 106);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 25);
            this.button2.TabIndex = 14;
            this.button2.Text = "Simular";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chDescontos
            // 
            this.chDescontos.Location = new System.Drawing.Point(396, 149);
            this.chDescontos.Name = "chDescontos";
            this.chDescontos.Properties.Caption = "Descontos";
            this.chDescontos.Size = new System.Drawing.Size(83, 19);
            this.chDescontos.TabIndex = 13;
            // 
            // chSuper
            // 
            this.chSuper.Location = new System.Drawing.Point(264, 149);
            this.chSuper.Name = "chSuper";
            this.chSuper.Properties.Caption = "Criar Super-boletos";
            this.chSuper.Size = new System.Drawing.Size(126, 19);
            this.chSuper.TabIndex = 12;
            // 
            // chComCondominio
            // 
            this.chComCondominio.EditValue = true;
            this.chComCondominio.Location = new System.Drawing.Point(71, 31);
            this.chComCondominio.Name = "chComCondominio";
            this.chComCondominio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.chComCondominio.Properties.Appearance.Options.UseFont = true;
            this.chComCondominio.Properties.Caption = "Boletos de Condom�nio";
            this.chComCondominio.Size = new System.Drawing.Size(165, 19);
            this.chComCondominio.TabIndex = 11;
            this.chComCondominio.CheckedChanged += new System.EventHandler(this.chComCondominio_CheckedChanged);
            // 
            // CHBanco
            // 
            this.CHBanco.Location = new System.Drawing.Point(109, 149);
            this.CHBanco.Name = "CHBanco";
            this.CHBanco.Properties.Caption = "Cobrar despesa Banc�ria";
            this.CHBanco.Size = new System.Drawing.Size(149, 19);
            this.CHBanco.TabIndex = 9;
            this.CHBanco.CheckedChanged += new System.EventHandler(this.CHBanco_CheckedChanged);
            // 
            // CHCorreio
            // 
            this.CHCorreio.Location = new System.Drawing.Point(2, 148);
            this.CHCorreio.Name = "CHCorreio";
            this.CHCorreio.Properties.Caption = "Cobrar Correio";
            this.CHCorreio.Size = new System.Drawing.Size(101, 19);
            this.CHCorreio.TabIndex = 8;
            this.CHCorreio.CheckedChanged += new System.EventHandler(this.CHCorreio_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(1095, 75);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 25);
            this.button1.TabIndex = 6;
            this.button1.Text = "Cancela";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.radioGroup1);
            this.xtraScrollableControl1.Location = new System.Drawing.Point(73, 57);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(300, 85);
            this.xtraScrollableControl1.TabIndex = 5;
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(0, 0);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Size = new System.Drawing.Size(297, 82);
            this.radioGroup1.TabIndex = 1;
            this.radioGroup1.EditValueChanged += new System.EventHandler(this.radioGroup1_EditValueChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Location = new System.Drawing.Point(1094, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(147, 63);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "GERAR BOLETOS";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // checkedListBoxControl1
            // 
            this.checkedListBoxControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxControl1.CheckOnClick = true;
            this.checkedListBoxControl1.Location = new System.Drawing.Point(383, 5);
            this.checkedListBoxControl1.Name = "checkedListBoxControl1";
            this.checkedListBoxControl1.Size = new System.Drawing.Size(1286, 139);
            this.checkedListBoxControl1.TabIndex = 3;
            this.checkedListBoxControl1.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.checkedListBoxControl1_ItemCheck);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 59);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Compet�ncia";
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(5, 5);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(368, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 0;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            this.lookupCondominio_F1.alterado += new System.EventHandler(this.lookupCondominio_F1_alterado);
            // 
            // dGeraBol
            // 
            this.dGeraBol.DataSetName = "dGeraBol";
            this.dGeraBol.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // intensParaBoletoBindingSource
            // 
            this.intensParaBoletoBindingSource.DataMember = "IntensParaBoleto";
            this.intensParaBoletoBindingSource.DataSource = this.dGeraBol;
            // 
            // intensParaBoletoTableAdapter
            // 
            this.intensParaBoletoTableAdapter.ClearBeforeFill = true;
            this.intensParaBoletoTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("intensParaBoletoTableAdapter.GetNovosDados")));
            this.intensParaBoletoTableAdapter.TabelaDataTable = null;
            // 
            // intensParaBoletoGridControl
            // 
            this.intensParaBoletoGridControl.DataSource = this.intensParaBoletoBindingSource;
            this.intensParaBoletoGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.intensParaBoletoGridControl.Location = new System.Drawing.Point(0, 174);
            this.intensParaBoletoGridControl.MainView = this.gridView1;
            this.intensParaBoletoGridControl.Name = "intensParaBoletoGridControl";
            this.intensParaBoletoGridControl.Size = new System.Drawing.Size(1288, 621);
            this.intensParaBoletoGridControl.TabIndex = 2;
            this.intensParaBoletoGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOD_PLA,
            this.colBODValor,
            this.colBODMensagem,
            this.colBODProprietario,
            this.colBODData,
            this.colAPTNumero,
            this.colBLOCodigo,
            this.colBLONome,
            this.colBOLETO});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(236, 307, 208, 191);
            this.gridView1.GridControl = this.intensParaBoletoGridControl;
            this.gridView1.GroupCount = 2;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", this.colBODValor, "{0:0.00}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "BODValor", null, "Itens:{0}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBODData, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOLETO, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colBOD_PLA
            // 
            this.colBOD_PLA.Caption = "Conta";
            this.colBOD_PLA.FieldName = "BOD_PLA";
            this.colBOD_PLA.Name = "colBOD_PLA";
            this.colBOD_PLA.Visible = true;
            this.colBOD_PLA.VisibleIndex = 4;
            this.colBOD_PLA.Width = 138;
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.DisplayFormat.FormatString = "n2";
            this.colBODValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", "{0:n2}")});
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 5;
            this.colBODValor.Width = 140;
            // 
            // colBODMensagem
            // 
            this.colBODMensagem.Caption = "Mensagem";
            this.colBODMensagem.FieldName = "BODMensagem";
            this.colBODMensagem.Name = "colBODMensagem";
            this.colBODMensagem.Visible = true;
            this.colBODMensagem.VisibleIndex = 3;
            this.colBODMensagem.Width = 514;
            // 
            // colBODProprietario
            // 
            this.colBODProprietario.Caption = "Propriet�rio";
            this.colBODProprietario.FieldName = "BODProprietario";
            this.colBODProprietario.Name = "colBODProprietario";
            this.colBODProprietario.Visible = true;
            this.colBODProprietario.VisibleIndex = 2;
            this.colBODProprietario.Width = 82;
            // 
            // colBODData
            // 
            this.colBODData.Caption = "Data";
            this.colBODData.FieldName = "BODData";
            this.colBODData.Name = "colBODData";
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "N�mero";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 1;
            this.colAPTNumero.Width = 61;
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "Bloco";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            // 
            // colBLONome
            // 
            this.colBLONome.Caption = "Nome Bloco";
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 0;
            this.colBLONome.Width = 153;
            // 
            // colBOLETO
            // 
            this.colBOLETO.Caption = "BOLETO";
            this.colBOLETO.FieldName = "BOLETO";
            this.colBOLETO.Name = "colBOLETO";
            // 
            // competenciasBindingSource
            // 
            this.competenciasBindingSource.DataMember = "Competencias";
            this.competenciasBindingSource.DataSource = this.dGeraBol;
            // 
            // competenciasTableAdapter
            // 
            this.competenciasTableAdapter.ClearBeforeFill = true;
            this.competenciasTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("competenciasTableAdapter.GetNovosDados")));
            this.competenciasTableAdapter.TabelaDataTable = null;
            // 
            // rateiosDaCompetenciaBindingSource
            // 
            this.rateiosDaCompetenciaBindingSource.DataMember = "RateiosDaCompetencia";
            this.rateiosDaCompetenciaBindingSource.DataSource = this.dGeraBol;
            // 
            // rateiosDaCompetenciaTableAdapter
            // 
            this.rateiosDaCompetenciaTableAdapter.ClearBeforeFill = true;
            this.rateiosDaCompetenciaTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("rateiosDaCompetenciaTableAdapter.GetNovosDados")));
            this.rateiosDaCompetenciaTableAdapter.TabelaDataTable = null;
            // 
            // bOletoDetalheTableAdapter
            // 
            this.bOletoDetalheTableAdapter.ClearBeforeFill = true;
            this.bOletoDetalheTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("bOletoDetalheTableAdapter.GetNovosDados")));
            this.bOletoDetalheTableAdapter.TabelaDataTable = null;
            // 
            // dadosCONDOMINIOSBOLTableAdapter1
            // 
            this.dadosCONDOMINIOSBOLTableAdapter1.ClearBeforeFill = true;
            this.dadosCONDOMINIOSBOLTableAdapter1.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("dadosCONDOMINIOSBOLTableAdapter1.GetNovosDados")));
            this.dadosCONDOMINIOSBOLTableAdapter1.TabelaDataTable = null;
            // 
            // cGeraBol
            // 
            this.AutoScroll = true;
            this.Controls.Add(this.intensParaBoletoGridControl);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGeraBol";
            this.Size = new System.Drawing.Size(1288, 795);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch_email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDescontos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSuper.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chComCondominio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHBanco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHCorreio.Properties)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGeraBol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensParaBoletoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensParaBoletoGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.competenciasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rateiosDaCompetenciaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.BindingSource intensParaBoletoBindingSource;
        private Boletos.GeraBoleto.dGeraBolTableAdapters.IntensParaBoletoTableAdapter intensParaBoletoTableAdapter;
        private DevExpress.XtraGrid.GridControl intensParaBoletoGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraGrid.Columns.GridColumn colBODMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colBODProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colBODData;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLETO;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private System.Windows.Forms.BindingSource competenciasBindingSource;
        private Boletos.GeraBoleto.dGeraBolTableAdapters.CompetenciasTableAdapter competenciasTableAdapter;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckedListBoxControl checkedListBoxControl1;
        private System.Windows.Forms.BindingSource rateiosDaCompetenciaBindingSource;
        private Boletos.GeraBoleto.dGeraBolTableAdapters.RateiosDaCompetenciaTableAdapter rateiosDaCompetenciaTableAdapter;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private Boletos.GeraBoleto.dGeraBolTableAdapters.DadosCONDOMINIOSBOLTableAdapter dadosCONDOMINIOSBOLTableAdapter1;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private System.Windows.Forms.Button button1;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        /// <summary>
        /// dGeraBol
        /// </summary>
        private dGeraBol dGeraBol;
        private Boletos.GeraBoleto.dGeraBolTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        private DevExpress.XtraEditors.CheckEdit CHBanco;
        private DevExpress.XtraEditors.CheckEdit CHCorreio;
        private DevExpress.XtraEditors.CheckEdit chComCondominio;
        private DevExpress.XtraEditors.CheckEdit chSuper;
        private DevExpress.XtraEditors.CheckEdit chDescontos;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.CheckEdit ch_email;
    }
}
