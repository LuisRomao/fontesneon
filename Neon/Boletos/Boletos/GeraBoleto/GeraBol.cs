﻿/*
LH - 05/05/2014       - 13.2.8.47 - Registro do envio de e-mails no boleto
MR - 29/12/2014 12:00 -           - Alteração no texto do conteúdo do e-mail enviado com o boleto (Alterações indicadas por *** MRC - INICIO (29/12/2014 12:00) ***)

*/

using CompontesBasicosProc;
using CompontesBasicos.Espera;
using VirEnumeracoesNeon;
using DevExpress.XtraReports.UI;
using Framework.objetosNeon;
using System.Collections.Generic;
using System.Windows.Forms;
using VirEnumeracoes;


namespace Boletos.GeraBoleto
{
    /// <summary>
    /// Lógica para emissão de boletos
    /// </summary>
    public class GeraBol
    {
        private Control Chamador;
        //private Competencia Comp;
        private dllImpresso.ImpBoletoBase ImpSB;
        private dllImpresso.ImpBoletoBase ImpEmail;
        //private dllImpresso.ImpBoletoBal ImpEmail;
        private dllImpresso.ImpProtocolo Protocolo;
        private SortedList<int, Boleto.Boleto> SuperBoletos;
        private List<Boleto.Boleto> SuperBoletosInc;

        /// <summary>
        /// Último erro
        /// </summary>
        public string UltimoErro;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Chamador"></param>
        public GeraBol(Control _Chamador)
        {
            Chamador = _Chamador;
        }

        private string NomePDFbalancete = null;
        //private string NomePDFInad = null;

        /// <summary>
        /// prepara o report tanto para impressão como para e-mail
        /// </summary>
        /// <returns></returns>
        private bool PreparaReport(cEspera esp, int CON, bool ComCondominio, bool ComSuper, SortedList<int, Boleto.Boleto> BoletosGerados, Competencia Comp)
        {

            esp.Motivo.Text = "Preparando impressos";
            esp.AtivaGauge(BoletosGerados.Count);
            Application.DoEvents();
            Protocolo = dllImpresso.ImpProtocolo.ImpProtocoloSt;
            string manobra = CONNome(CON);
            Protocolo.SetNome(manobra);
            Protocolo.dImpProtocolo.DadosProt.Clear();

            if (!ComCondominio)
            {
                dllImpresso.ImpSegundaVia ImpSBsv = new dllImpresso.ImpSegundaVia();
                ImpSBsv.xrTitulo.Visible = false;
                ImpSB = ImpSBsv;
                dllImpresso.ImpSegundaVia ImpSBsvE = new dllImpresso.ImpSegundaVia();
                ImpSBsvE.xrTitulo.Visible = false;
                ImpSBsvE.ComRemetente = false;
                ImpEmail = ImpSBsvE;
            }
            else
            {
                bool? CONImprimeBalanceteBoleto = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_bool("Select CONImprimeBalanceteBoleto from condominios where CON = @P1", CON);
                if (CONImprimeBalanceteBoleto.GetValueOrDefault(false))
                {
                    if ((Framework.DSCentral.EMP != 1) || (!CON.EstaNoGrupo(732)))
                    {
                        NomePDFbalancete = AbstratosNeon.ABS_balancete.ImpressoGravado(CON, Comp, true);
                        /*
                        if (CON == 845)
                        {
                            AbstratosNeon.ABS_balancete Balancete = AbstratosNeon.ABS_balancete.GetBalancete(CON, Comp, TipoBalancete.Boleto);
                            Boleto.Impresso.ImpInadUnidades Impresso = new Boleto.Impresso.ImpInadUnidades(AbstratosNeon.ABS_Condominio.GetCondominio(845), Balancete.DataF);
                            string Caminho = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\TMP\\";
                            if (!System.IO.Directory.Exists(Caminho))
                                System.IO.Directory.CreateDirectory(Caminho);
                            NomePDFInad = Caminho + "Inadimplencia.pdf";
                            Impresso.ExportToPdf(NomePDFInad);
                        }
                        */
                    }
                }
                ImpSB = new dllImpresso.ImpBoletoBal();
                ((dllImpresso.ImpBoletoBal)ImpSB).PrecargaConsumo(CON, Comp);
                dllImpresso.ImpBoletoBal ImpEmailBal = new dllImpresso.ImpBoletoBal();
                ImpEmailBal.PrecargaConsumo(CON, Comp);
                ImpEmailBal.OcultarBalancete = (NomePDFbalancete != null);
                ImpEmailBal.ComRemetente = false;
                ImpEmail = ImpEmailBal;                
            }

            SortedList<string, Boleto.Boleto> Unificar = new SortedList<string, Boleto.Boleto>();
            SuperBoletos = new SortedList<int, Boleto.Boleto>();
            SuperBoletosInc = new List<Boleto.Boleto>();

            foreach (Boleto.Boleto BoletosImp in BoletosGerados.Values)
            {
                if (BoletosImp.QuerImpresso)
                    BoletosImp.CarregaLinha(ImpSB, true, false);
                if (BoletosImp.QuerEmail)
                    BoletosImp.CarregaLinha(ImpEmail, true, false);
                Boleto.Boleto Super = null;
                if (ComSuper)
                    if (!BoletosImp.DebitoAutomaticoCadastrado())

                    {
                        string Chave = BoletosImp.rowPrincipal.BOLNome + BoletosImp.rowPrincipal.BOLEndereco;
                        if (!Unificar.ContainsKey(Chave))
                            Unificar.Add(Chave, BoletosImp);
                        else
                        {
                            Boleto.Boleto BoletoSomar = Unificar[Chave];
                            //retira o segundo individual do impresso.
                            BoletosImp.ApagarBOLETOSIMPRow();
                            //Retira a linha do super que estava no impresso;
                            BoletoSomar.ApagarBOLETOSIMPRow();
                            SuperBoletosInc.Add(BoletosImp);
                            //Boleto.dBoletos.BOletoDetalheRow novoBOD;                        
                            if (BoletoSomar.rowSBO == null)
                            {
                                //O boleto que estava no acumular nao era super. ele dever ser incluido no super que vai ser gerado.
                                //retira o individual do impresso.                                
                                SuperBoletosInc.Add(BoletoSomar);
                                Super = new Boletos.Boleto.Boleto(BoletoSomar, BoletoSomar.rowPrincipal.BOLVencto);
                                Unificar[Chave] = Super;
                                SuperBoletos.Add(Super.BOL, Super);
                                if (BoletoSomar.BOLStatusRegistro == StatusRegistroBoleto.BoletoRegistrando)
                                    BoletoSomar.AlteraStatusRegistro(StatusRegistroBoleto.ComregistroNaoRegistrado);
                                BoletoSomar.GravaJustificativa(string.Format("Boleto incluido no super: {0}", Super.BOL));
                            }
                            else
                                Super = BoletoSomar;
                            Super.addBolentonoSuper(BoletosImp);
                            if (BoletosImp.BOLStatusRegistro == StatusRegistroBoleto.BoletoRegistrando)
                                BoletosImp.AlteraStatusRegistro(StatusRegistroBoleto.ComregistroNaoRegistrado);
                            BoletosImp.GravaJustificativa(string.Format("Boleto incluido no super: {0}", Super.BOL));
                            if (Super.QuerImpresso)
                                Super.CarregaLinha(ImpSB, true, false);
                            if (Super.QuerEmail)
                                Super.CarregaLinha(ImpEmail, true, false);
                            CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                        }
                    }
;
                if (Super == null)
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Prepara protocolo 1");
                    Protocolo.dImpProtocolo.DadosProt.AddDadosProtRow(BoletosImp.Apartamento.APTNumero,
                                                                      BoletosImp.rowPrincipal.BOLNome,
                                                                      BoletosImp.rowPrincipal.BOLEndereco,
                                                                      BoletosImp.Apartamento.BLOCodigo,
                                                                      BoletosImp.rowPrincipal.BOLProprietario ? "P" : "I",string.Empty,
                                                                      BoletosImp.Apartamento.BLONome,
                                                                      (int)BoletosImp.FormaPagamento,
                                                                      BoletosImp.DebitoAutomaticoCadastrado() ? 237 : 0);
                }
                else
                    Protocolo.dImpProtocolo.DadosProt.AddDadosProtRow(BoletosImp.Apartamento.APTNumero,
                                                                      BoletosImp.rowPrincipal.BOLNome,
                                                                      string.Format("Incluído no boleto: {0}", Super.Unidade),//,BoletosImp.rowPrincipal.BOLEndereco,
                                                                      BoletosImp.Apartamento.BLOCodigo,
                                                                      BoletosImp.rowPrincipal.BOLProprietario ? "P" : "I",string.Empty,
                                                                      BoletosImp.Apartamento.BLONome,
                                                                      (int)FormaPagamento.Dentro_DeSuper,
                                                                      BoletosImp.DebitoAutomaticoCadastrado() ? 237 : 0);
                esp.Gauge();
            };
            return true;
        }

        private string CONNome(int CON)
        {
            string retorno;
            FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow row = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(CON);
            retorno = row.CONNome;
            return retorno;
            //return Framework.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(CON).CONNome;
        }

        internal void EmissaoEfetiva(int CON, Competencia Comp, bool ComCondominio, SortedList<int, Boleto.Boleto> BoletosGerados)
        {
            System.Collections.SortedList Alternativas = new System.Collections.SortedList();
            Alternativas.Add(0, "usar Super-Boletos");
            Alternativas.Add(1, "imprimir");
            Alternativas.Add(2, "e-mail");
            List<int> Selecionados;
            VirInput.Input.Execute("Opções", "Opções", Alternativas, out Selecionados);
            EmissaoEfetiva(CON, Comp, ComCondominio, Selecionados.Contains(0), BoletosGerados, !Selecionados.Contains(2), !Selecionados.Contains(1));
        }

        /// <summary>
        /// Imprime , envia e-mail e ajusta status dos boletos
        /// </summary>
        internal void EmissaoEfetiva(int CON, Competencia Comp, bool ComCondominio, bool ComSuper, SortedList<int, Boleto.Boleto> BoletosGerados, bool TravarEmails, bool TravaImpressao)
        {
            using (cEspera esp = new cEspera(Chamador))
            {
                if (!PreparaReport(esp, CON, ComCondominio, ComSuper, BoletosGerados, Comp))
                    return;
                //if (!TravaImpressao.HasValue)
                //    TravaImpressao = (MessageBox.Show("Imprimir?", "Imprimir", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2, 0) == DialogResult.No);
                //if (MessageBox.Show("Imprimir?", "Imprimir", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2, 0) == DialogResult.Yes)
                if (!TravaImpressao)
                {
                    esp.Motivo.Text = "Imprimindo";
                    Application.DoEvents();
                    Imprimir(CON, Comp);
                }
                if (!TravarEmails)
                {
                    UltimoErro = string.Empty;
                    esp.Motivo.Text = "Preparando e-mail";
                    Application.DoEvents();
                    Email(esp, BoletosGerados);
                    Email(esp, SuperBoletos);
                }
            }
        }

        private bool Imprimir(int CON, Competencia Comp)
        {
            ImpSB.Apontamento(CON, 1);
            ImpSB.bindingSourcePrincipal.Sort = "Correio,BLOCodigo,APTNumero";
            ImpSB.CreateDocument();
            ImpSB.Print();
            //if (ImprimirProtocolo)
            //{
            Protocolo.bindingSource1.Sort = "BLOCO,APTNumero";
            Protocolo.SetDocumento("Boleto competência " + Comp.ToString());
            Protocolo.Apontamento(CON, 1);
            Protocolo.CreateDocument();
            Protocolo.Apontamento(CON, -1);
            Protocolo.Print();
            //};
            return true;

        }


        private string ConteudoEmail(DocBacarios.CPFCNPJ cpf)
        {
            if ((cpf != null) && (cpf.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO))
                return
            "\r\n" +
            "Prezado Cliente," + "\r\n" +
            "\r\n" +
            "Segue em anexo o arquivo (pdf) do Boleto Bancário do %C." + "\r\n" +
string.Empty + "\r\n" +
            "Você poderá imprimi-lo e efetuar o pagamento em qualquer banco integrado ao sistema de compensação ou via internet." + "\r\n" +
string.Empty + "\r\n" +
            "Para pagamento via internet disponibilizamos também o número do Código de Barras:" + "\r\n" +
string.Empty + "\r\n" +
            " %B " + "\r\n" +
string.Empty + "\r\n" +
            "que identificará este boleto no site de seu banco." + "\r\n" +
string.Empty + "\r\n" +
            "Caso você não queira receber os boletos por e-mail basta desmarcar esta opção no seu cadastro no Neon Online." + "\r\n" +
string.Empty + "\r\n" +
            "Att," + "\r\n" +
string.Empty + "\r\n" +
            "Neon Imóveis" + "\r\n" +
            "\r\n";
            else
                return
"\r\n" +
"Prezado Cliente," + "\r\n" +
"\r\n" +
"Conforme informado nos meses anteriores, a partir de janeiro de 2017 a nova regra da Febraban determina que os boletos só poderão ser emitidos com o CPF cadastrado.\r\n" +
string.Format("Portanto, para emissão do seu boleto, entre em contato pelo número {0} ou informe o CPF pelo site www.neonimoveis.com.br", Framework.DSCentral.EMP == 1 ? "3376.7900" : "4990-1394") +
"\r\n" +
"Segue em anexo o arquivo (pdf) do Demonstrativo do %C." + "\r\n" +
"\r\n" +
"Caso você não queira receber os boletos por e-mail basta desmarcar esta opção no seu cadastro no Neon Online." + "\r\n" +
"\r\n" +
"Att," + "\r\n" +
"\r\n" +
"Neon Imóveis" + "\r\n" +
"\r\n";
        }


        private bool Email(cEspera esp, SortedList<int, Boleto.Boleto> BoletosGerados)
        {
            esp.Motivo.Text = "E.mail";
            esp.AtivaGauge(BoletosGerados.Count);
            Application.DoEvents();
            dllImpresso.ImpBoletoBase ImpX = ImpEmail;
            ImpX.ComRemetente = false;
            foreach (Boleto.Boleto BoletosE in BoletosGerados.Values)
            {
                esp.Gauge();
                if (BoletosE.QuerEmail)
                {
                    if (SuperBoletosInc.Contains(BoletosE))
                        continue;
                    ImpX.Apontamento(-1, -1);
                    ImpX.bindingSourcePrincipal.Filter = "BOL = " + BoletosE.rowPrincipal.BOL.ToString();
                    ImpX.CreateDocument();
                    string Corpo = ConteudoEmail(BoletosE.Apartamento.PESCpfCnpj(BoletosE.Proprietario)).Replace("%C", BoletosE.Apartamento.Condominio.Nome).Replace("%B", BoletosE.LinhaDigitavel);
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Email 7");
                    try
                    {
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Email 8");
                        if (NomePDFbalancete == null)
                            VirEmailNeon.EmailDiretoNeon.EmalSTSemRetorno.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, BoletosE.EmailResponsavel, BoletosE.rowPrincipal.BOL.ToString(), ImpX, Corpo, "Boleto de condomínio", BoletosE.BOL, string.Empty);
                        else //if (NomePDFInad == null)
                            VirEmailNeon.EmailDiretoNeon.EmalSTSemRetorno.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, BoletosE.EmailResponsavel, BoletosE.rowPrincipal.BOL.ToString(), ImpX, Corpo, "Boleto de condomínio", BoletosE.BOL, string.Empty, NomePDFbalancete);
                        //else
                        //    VirEmailNeon.EmailDiretoNeon.EmalSTSemRetorno.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, BoletosE.EmailResponsavel, BoletosE.rowPrincipal.BOL.ToString(), ImpX, Corpo, "Boleto de condomínio", BoletosE.BOL, string.Empty, NomePDFbalancete, NomePDFInad);
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Email 9");
                        UltimoErro += "ok: " + BoletosE.Apartamento.BLOCodigo + " " + BoletosE.Apartamento.APTNumero + " " + BoletosE.EmailResponsavel + "\r\n";
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Email 10");
                        if (BoletosE.rowSBO == null)
                            BoletosE.GravaJustificativa(string.Format("Enviado para: {0}", BoletosE.EmailResponsavel));
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Email 11");
                    }
                    catch
                    {
                        UltimoErro += "E.mail não enviado: " + BoletosE.Apartamento.BLOCodigo + " " + BoletosE.Apartamento.APTNumero + " " + BoletosE.EmailResponsavel + "\r\n";
                        UltimoErro += "\r\n" + VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro + "\r\n";
                    };

                };
            };
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Email 12");
            esp.AtivaGauge(BoletosGerados.Count);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Email 13");
            /*
            foreach (Boleto.Boleto BoletosE in SuperBoletos.Values)
            {
                //if (Cespera.Abortar)
                //    return false;


                if (BoletosE.QuerEmail)
                {
                    if (SuperBoletosInc.Contains(BoletosE))
                        continue;
                    ImpSB.Apontamento(-1, -1);
                    ImpSB.bindingSourcePrincipal.Filter = "BOL = " + BoletosE.rowPrincipal.BOL.ToString();
                    ImpSB.CreateDocument();
                    string Corpo = ConteudoEmail.Replace("%C", BoletosE.Apartamento.Condominio.Nome).Replace("%B", BoletosE.LinhaDigitavel);
                    try
                    {
                        //VirEmailNeon.EmailDiretoNeon.EmalSTSemRetorno.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, BoletosE.EmailResponsavel, BoletosE.rowPrincipal.BOL.ToString(), ImpSB, Corpo, "Boleto de condomínio");
                        VirEmailNeon.EmailDiretoNeon.EmalSTSemRetorno.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, BoletosE.EmailResponsavel, BoletosE.rowPrincipal.BOL.ToString(), ImpSB, Corpo, "Boleto de condomínio",null);
                        UltimoErro += "ok: " + BoletosE.Apartamento.BLOCodigo + " " + BoletosE.rowComplementar.APTNumero + " " + BoletosE.EmailResponsavel + "\r\n";
                    }
                    catch
                    {
                        UltimoErro += "E.mail não enviado: " + BoletosE.Apartamento.BLOCodigo + " " + BoletosE.rowComplementar.APTNumero + " " + BoletosE.EmailResponsavel + "\r\n";
                        UltimoErro += "\r\n" + VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro + "\r\n";
                    };
                    esp.Gauge();

                };
            };
            */
            return true;

        }
    }
}
