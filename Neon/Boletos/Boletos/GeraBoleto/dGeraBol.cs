﻿namespace Boletos.GeraBoleto {


    partial class dGeraBol
    {
        private dGeraBolTableAdapters.DadosAPTTableAdapter dadosAPTTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DadosAPT
        /// </summary>
        public dGeraBolTableAdapters.DadosAPTTableAdapter DadosAPTTableAdapter
        {
            get
            {
                if (dadosAPTTableAdapter == null)
                {
                    dadosAPTTableAdapter = new dGeraBolTableAdapters.DadosAPTTableAdapter();
                    dadosAPTTableAdapter.TrocarStringDeConexao();
                };
                return dadosAPTTableAdapter;
            }
        }

        private dGeraBolTableAdapters.DadosCONDOMINIOSBOLTableAdapter dadosCONDOMINIOSBOLTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DadosCONDOMINIOSBOL
        /// </summary>
        public dGeraBolTableAdapters.DadosCONDOMINIOSBOLTableAdapter DadosCONDOMINIOSBOLTableAdapter
        {
            get
            {
                if (dadosCONDOMINIOSBOLTableAdapter == null)
                {
                    dadosCONDOMINIOSBOLTableAdapter = new dGeraBolTableAdapters.DadosCONDOMINIOSBOLTableAdapter();
                    dadosCONDOMINIOSBOLTableAdapter.TrocarStringDeConexao();
                };
                return dadosCONDOMINIOSBOLTableAdapter;
            }
        }

        private dGeraBolTableAdapters.PrevisaoBOletosTableAdapter previsaoBOletosTableAdapter;

        /// <summary>
        /// PrevisaoBOletosTableAdapter
        /// </summary>
        public dGeraBolTableAdapters.PrevisaoBOletosTableAdapter PrevisaoBOletosTableAdapter
        {
            get
            {
                if (previsaoBOletosTableAdapter == null)
                {
                    previsaoBOletosTableAdapter = new Boletos.GeraBoleto.dGeraBolTableAdapters.PrevisaoBOletosTableAdapter();
                    previsaoBOletosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return previsaoBOletosTableAdapter;
            }

        }

        
    }
}
