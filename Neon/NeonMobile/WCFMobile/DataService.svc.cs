using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Services;
using System.Data.Services.Common;
using System.Data.Services.Providers;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.ServiceModel.Web;
using System.Configuration;
using System.Reflection;
using System.ServiceModel;
using PortalUtil;
using BoletoUtil;
using BoletoUtil.Datasets;
using CadastroUtil.Datasets;
using FinanceiroUtil.Datasets;
using FuncionalidadeUtil.Datasets;

namespace WCFMobile
{
	[ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [JSONPSupportBehavior]
    public class DataService : DataService<NeonEntities>, IServiceProvider
    {
        //Conte�do: Produtos e Servi�os - CNT = 1
        private int intCNT = 1;

        private ArrayList UsuariosTipoCondomino
        {
            get
            {
                return new ArrayList(new Credencial.TipoUsuario[] { Credencial.TipoUsuario.Proprietario,
                                                                    Credencial.TipoUsuario.Inquilino,
                                                                    Credencial.TipoUsuario.ProprietarioSindico,
                                                                    Credencial.TipoUsuario.InquilinoSindico });
            }
        }

        public static void InitializeService(DataServiceConfiguration config)
        {
            config.SetEntitySetAccessRule("*", EntitySetRights.AllRead);
            config.SetServiceOperationAccessRule("*", ServiceOperationRights.AllRead);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
			config.UseVerboseErrors = true;
        }

        public object GetService(Type serviceType)
        {
            if (serviceType == typeof(IDataServiceStreamProvider))
            {
                // Return the stream provider to the data service.
                return new ImageStreamProvider();
            }
            return null;
        }

        protected override void HandleException(HandleExceptionArgs args)
        {
            // Handle exceptions raised in service operations. 
            if (args.Exception is TargetInvocationException && args.Exception.InnerException != null)
            {
                if (args.Exception.InnerException.GetType() == typeof(DataServiceException))
                {
                    //Unpack the DataServiceException. 
                    args.UseVerboseErrors = true;
                    args.Exception = args.Exception.InnerException;
                }
                else
                {
                    //Return a new DataServiceException as "400: bad request." 
                    args.UseVerboseErrors = true;
                    args.Exception = new DataServiceException(400, args.Exception.InnerException.Message);
                }
            }
        }

        [WebGet]
        public IQueryable<Usuario> Login(string Usuario, string Senha, String Token)
        {
            //Verifica token
            if (Token != ConfigurationManager.AppSettings["MobileKeyToken"])
                throw new DataServiceException(400, "Consulta n�o permitida.");

            //Inicia usuario
            var usuario = new List<Usuario>();
            Usuario usu = new WCFMobile.Usuario();

            //Verifica autenticacao
            Autenticacao Aut = new Autenticacao();
            Credencial Cred = Aut.Autentica(Usuario, Senha);
            if (Cred != null)
            {
                //Verifica se usuario e do tipo condomino
                if (UsuariosTipoCondomino.Contains(Cred.TUsuario))
                {
                    //Pega dados do usuario
                    dCadastro dCad = new dCadastro(Cred.EMP);
                    dCad.CondominoTableAdapter.Fill(dCad.Condomino, Cred.rowUsuario.APT, Cred.rowUsuario.PES);
                    dCadastro.CondominoRow rowPesApt = dCad.Condomino[0];

                    //Monta dados do usuario
                    usu.EMP = Cred.EMP;
                    usu.CON = Cred.CON;
                    usu.CONCodigo = Cred.rowUsuario.CONCodigo;
                    usu.CONNome = Cred.rowUsuario.CONNome;
                    usu.BLOCodigo = Cred.rowUsuario.BLOCodigo;
                    usu.APT = Cred.rowUsuario.APT;
                    usu.APTNumero = Cred.rowUsuario.APTNumero;
                    usu.PES = Cred.rowUsuario.PES;
                    usu.Nome = Cred.rowUsuario.Nome;
                    usu.Email = Cred.rowUsuario.Email;
                    usu.TipoUsuario = Cred.rowUsuario.TipoUsuario;
                    usu.Proprietario = Cred.rowUsuario.Proprietario;
                    usu.CpfCnpj = Cred.rowUsuario.CpfCnpj;
                    usu.APTSeguro = rowPesApt.APTSeguro;

                    //Retorna sucesso
                    usu.Ret_Sucesso = true;
                    usu.Ret_Mensagem = "Login efetuado com sucesso";
                }
                else
                {
                    //Retorna erro
                    usu.Ret_Sucesso = false;
                    usu.Ret_Mensagem = "Aplicativo dispon�vel apenas para cond�minos";
                }
            }
            else
            {
                //Retorna erro
                usu.Ret_Sucesso = false;
                usu.Ret_Mensagem = "Usu�rio ou Senha inv�lidos.\r\nOs dados de acesso est�o dispon�veis em seu boleto mensal.";
            }
            usuario.Add(usu);

            //Retorna
            return usuario.AsQueryable();
        }

        [WebGet]
        public IQueryable<Boleto> getBoletos(int EMP, int CON, int APT, String Token)
        {
            //Verifica token
            if (Token != ConfigurationManager.AppSettings["MobileKeyToken"])
                throw new DataServiceException(400, "Consulta n�o permitida.");

            //Retorna
            return MontaListaBoletos(EMP, CON, APT).AsQueryable();
        }

        [WebGet]
        public IQueryable<Boleto> getBoleto(int EMP, int CON, int APT, int BOL, String Token)
        {
            //Verifica token
            if (Token != ConfigurationManager.AppSettings["MobileKeyToken"])
                throw new DataServiceException(400, "Consulta n�o permitida.");

            //Retorna
            return MontaListaBoletos(EMP, CON, APT, BOL).AsQueryable();
        }

        private IQueryable<Boleto> MontaListaBoletos(int EMP, int CON, int APT, int BOL = 0)
        {
            //Inicia campos
            DateTime datCalculo = DateTime.Today;

            //Inicia datasets
            dBoleto dBol = new dBoleto(EMP);

            //Seta o condominio
            dBol.CONDOMINIOSTableAdapter.Fill(dBol.CONDOMINIOS, CON);
            dBoleto.CONDOMINIOSRow rowCON = dBol.CONDOMINIOS[0];

            //Seta o apartamento
            dBol.AParTamentoTableAdapter.Fill(dBol.AParTamento, APT);
            dBoleto.AParTamentoRow rowAPT = dBol.AParTamento[0];

            //Seleciona boletos
            dBol.BoletosTableAdapter.Fill(dBol.Boletos, DateTime.Today.AddMonths(-6), APT);

            //Inicia lista de boletos
            var boleto = new List<Boleto>();

            //Inicia boleto
            VirBoleto CurBoleto = new VirBoleto(dBol, true);
            VirBoleto CurBoletoLD;
            if ((CON == 732) && (EMP == 1))
                CurBoleto.jurosCompostos = false;

            //Monta lista (realiza calculos necessarios para valores e situacao)
            foreach (dBoleto.BoletosRow rowBOL in dBol.Boletos)
            {
                //Verifica se boleto especifico
                if (BOL > 0 && BOL != rowBOL.BOL) continue;

                //Verifica pagamento e competencia
                rowBOL.pago = !rowBOL.IsBOLPagamentoNull();
                rowBOL.competencia = string.Format("{0:00}{1:0000}", rowBOL.BOLCompetenciaMes, rowBOL.BOLCompetenciaAno);

                //Verifica no juridico
                bool booNoJuridico = (rowAPT.APTStatusCobranca == -1);

                //Pega o boleto e calcula valores
                CurBoleto.CarregaDados(rowBOL);
                CurBoleto.Calcula(datCalculo, false);

                //Verifica situacao (e cor da situacao) do boleto 
                string strSituacao = "";
                string strSituacaoCor = "Black";
                switch (CurBoleto.stparcela)
                {
                    case Statusparcela.Stparok:
                        strSituacao = "Pago";
                        strSituacaoCor = "#337ab7";
                        break;
                    case Statusparcela.Stparaberto:
                        strSituacao = "A Vencer";
                        strSituacaoCor = "#337ab7";
                        break;
                    case Statusparcela.Stparabertotolerancia:
                        strSituacao = "Aguardo";
                        strSituacaoCor = "#337ab7";
                        break;
                    case Statusparcela.Stparatrasadodentro:
                    case Statusparcela.Stparatrasadofora:
                        strSituacao = "Vencido";
                        strSituacaoCor = "#ea4c4c";
                        break;
                    default:
                        break;
                };

                //Verifica o tipo de boleto (legenda)
                string strTipoBoleto = "";
                switch (rowBOL.tipoboleto)
                {
                    case "A":
                        strTipoBoleto = "de Acordo";
                        break;
                    case "C":
                        strTipoBoleto = "de Condom�nio";
                        break;
                    case "R":
                        strTipoBoleto = "de Rateio";
                        break;
                    case "I":
                        strTipoBoleto = "de Implanta��o";
                        break;
                    default:
                        strTipoBoleto = rowBOL.tipoboleto;
                        break;
                };

                //Verifica impressao e boletos vencidos pagaveis
                bool booImprimir = false;
                if (rowBOL.pago || rowBOL.BOLProtestado)
                    booImprimir = false;
                else
                {
                    if (booNoJuridico && (CurBoleto.stparcela == Statusparcela.Stparatrasadofora))
                        booImprimir = false;
                    else
                    {
                        if ((rowCON.CON_BCO == 341 || rowCON.CON_BCO == 104) && (rowCON.CONBoletoComRegistro) && (rowBOL.dtvencto.AddMonths(1) < DateTime.Today))
                            booImprimir = false;
                        else
                            booImprimir = true;
                    }
                }

                //Adiciona boleto calculado na lista
                Boleto bol = new WCFMobile.Boleto();
                bol.BOL = rowBOL.BOL;
                bol.BLOCodigo = rowBOL.BLOCodigo;
                bol.APTNumero = rowBOL.APTNumero;
                bol.BOLTipoUsuario = rowBOL.BOLTipoUsuario;
                bol.BOLVencto = rowBOL.dtvencto.ToString("dd/MM/yyyy");
                bol.BOLVenctoDT = rowBOL.dtvencto;
                bol.BOLOriginal = CurBoleto.valororiginal.ToString("#0.00");
                bol.BOLCorrigido = CurBoleto.ValorCorrigidostr == "" ? "0,00" : Convert.ToDecimal(CurBoleto.ValorCorrigidostr).ToString("#0.00");
                bol.BOLMulta = CurBoleto.multa.ToString("#0.00");
                bol.BOLJuros = CurBoleto.juros.ToString("#0.00");
                bol.BOLTotal = CurBoleto.valorfinal.ToString("#0.00");
                bol.BOLSituacao = strSituacao;
                bol.BOLSituacaoCor = strSituacaoCor;
                bol.BOLTipo = strTipoBoleto;
                bol.BOLImprimir = booImprimir;
                bol.CONNome = rowCON.CONNome;

                //Verifica vencimento final para o boleto
                DateTime datVenctoFinal = rowBOL.dtvencto;
                if (rowBOL.dtvencto <= DateTime.Today.AddDays(5))
                {
                    //Verifica se tem opcoes de data de pagamento
                    SortedList Alternativas = new SortedList();
                    decimal ValorAlter = 0;
                    for (DateTime dataAlter = DateTime.Today.AddDays(7); dataAlter >= DateTime.Today; dataAlter = dataAlter.AddDays(-1))
                    {
                        if ((dataAlter.DayOfWeek == DayOfWeek.Saturday) || (dataAlter.DayOfWeek == DayOfWeek.Sunday))
                            continue;
                        decimal NovoValorAlter = CurBoleto.Calcula(dataAlter, false);
                        if (NovoValorAlter != ValorAlter)
                        {
                            ValorAlter = NovoValorAlter;
                            Alternativas.Add(dataAlter, ValorAlter);
                        };
                    }

                    //Pega a primeira data entre as alternativas
                    if (Alternativas.Count > 0)
                        datVenctoFinal = (DateTime)Alternativas.GetKey(0);
                }
                bol.BOLVenctoFinal = datVenctoFinal.ToString("dd/MM/yyyy");

                //Verifica linha digitavel no dia do vencimento final
                dBol.DadosBancariosTableAdapter.Fill(dBol.DadosBancarios, CON);
                CurBoletoLD = new VirBoleto(rowBOL, new TDadosBanco(dBol.DadosBancarios[0]));
                CurBoletoLD.Calcula(datVenctoFinal, false);
                bol.BOLLinhaDigitavel = CurBoletoLD.LinhaDigitavel();

                //Adiciona boleto na lista
                boleto.Add(bol);
            }

            //Orderna a lista por data de vencimento desc
            var boletoOrder = boleto.OrderByDescending(o => o.BOLVenctoDT).ToList();

            //Retorna
            return boletoOrder.AsQueryable();
        }

        [WebGet]
        public IQueryable<CorpoDiretivo> getCorpoDiretivo(int EMP, int CON, String Token)
        {
            //Verifica token
            if (Token != ConfigurationManager.AppSettings["MobileKeyToken"])
                throw new DataServiceException(400, "Consulta n�o permitida.");

            //Inicia datasets
            dCadastro dCad = new dCadastro(EMP);

            //Seleciona corpo diretivo
            dCad.CORPODIRETIVOTableAdapter.Fill(dCad.CORPODIRETIVO, CON);

            //Inicia lista de corpo diretivo
            var corpodiretivo = new List<CorpoDiretivo>();

            //Monta lista
            foreach (dCadastro.CORPODIRETIVORow rowCD in dCad.CORPODIRETIVO)
            {
                //Adiciona na lista
                CorpoDiretivo cd = new WCFMobile.CorpoDiretivo();
                cd.CDR = rowCD.CDR;
                cd.CDR_CON = rowCD.CDR_CON;
                cd.CGONome = rowCD.CGONome;
                cd.PESNome = rowCD.PESNome;
                if (!rowCD.IsPESFone1Null()) cd.PESFone1 = rowCD.PESFone1;
                if (!rowCD.IsPESEmailNull()) cd.PESEmail = rowCD.PESEmail;
                cd.APTNumero = rowCD.APTNumero;
                cd.BLOCodigo = rowCD.BLOCodigo;
                corpodiretivo.Add(cd);
            }

            //Retorna
            return corpodiretivo.AsQueryable();
        }

        [WebGet]
        public IQueryable<Documento> getDocumentos(int EMP, int CON, int Tipo, String Token)
        {
            //Verifica token
            if (Token != ConfigurationManager.AppSettings["MobileKeyToken"])
                throw new DataServiceException(400, "Consulta n�o permitida.");

            //Inicia datasets
            dCadastro dCad = new dCadastro(EMP);

            //Seleciona documentos
            dCad.PUBlicacaoTableAdapter.Fill(dCad.PUBlicacao, CON, Tipo);

            //Inicia lista de documentos
            var documentos = new List<Documento>();

            //Monta lista
            foreach (dCadastro.PUBlicacaoRow rowDoc in dCad.PUBlicacao)
            {
                //Adiciona na lista
                Documento doc = new WCFMobile.Documento();
                doc.ID = rowDoc.ID;
                if (!rowDoc.IsDataNull()) doc.Data = rowDoc.Data;
                if (!rowDoc.IsHorarioNull()) doc.Horario = rowDoc.Horario;
                doc.Descritivo = rowDoc.Descritivo;
                doc.Tipo = rowDoc.Tipo;
                if (!rowDoc.IsArquivoNull()) doc.Arquivo = String.Format("{0}{1}", ConfigurationManager.AppSettings["PublicacaoRootIn"], rowDoc.Arquivo);
                documentos.Add(doc);
            }

            //Retorna
            return documentos.AsQueryable();
        }

        [WebGet]
        public IQueryable<Aviso> getAvisos(int EMP, int CON, int APT, String Token)
        {
            //Verifica token
            if (Token != ConfigurationManager.AppSettings["MobileKeyToken"])
                throw new DataServiceException(400, "Consulta n�o permitida.");

            //Inicia datasets
            dFuncionalidade dFun = new dFuncionalidade(EMP);

            //Seleciona avisos ativos
            dFun.ST_AViSosTableAdapter.FillAtivos(dFun.ST_AViSos, CON, APT, DateTime.Now.ToShortDateString());

            //Inicia lista de avisos
            var avisos = new List<Aviso>();

            //Monta lista
            foreach (dFuncionalidade.ST_AViSosRow rowAvi in dFun.ST_AViSos)
            {
                //Adiciona na lista
                Aviso avi = new WCFMobile.Aviso();
                avi.AVS = rowAvi.AVS;
                avi.AVSTitulo = rowAvi.AVSTitulo.ToUpper();
                avi.AVSMensagem = rowAvi.AVSMensagem;
                avi.AVSDataCadastro = rowAvi.AVSDataCadastro.ToString("dd/MM/yyyy");
                avi.AVSBloco = rowAvi.AVSBloco;
                avi.AVSUnidade = rowAvi.AVSUnidade;
                avisos.Add(avi);
            }

            //Retorna
            return avisos.AsQueryable();
        }

        [WebGet]
        public IQueryable<Balancete> getBalancetes(int EMP, int CON, String Token)
        {
            //Verifica token
            if (Token != ConfigurationManager.AppSettings["MobileKeyToken"])
                throw new DataServiceException(400, "Consulta n�o permitida.");

            //Inicia datasets
            dFinanceiro dFin = new dFinanceiro(EMP);

            //Seta condominio
            dFin.CONDOMINIOSTableAdapter.Fill(dFin.CONDOMINIOS, CON);

            //Seleciona documentos
            dFin.CarregaTotais(CON);

            //Inicia lista de balancetes
            var balancetes = new List<Balancete>();

            //Vizualizacao grafica se configurada, link PowerBI
            Balancete bal = new WCFMobile.Balancete();
            if (!dFin.CONDOMINIOS[0].IsCONLinkBalanceteGraficoInternetNull())
            {
                bal.ID = -1;
                bal.Descritivo = "VISUALIZA��O GR�FICA";
                bal.Arquivo = dFin.CONDOMINIOS[0].CONLinkBalanceteGraficoInternet;
                balancetes.Add(bal);
            }

            //Acumulado 12 meses, nome do arquivo criptografado
            string strNomeArquivo = String.Format("{0}_{1}_h12.pdf", EMP, CON);
            bal = new WCFMobile.Balancete();
            bal.ID = 0;
            bal.Descritivo = "ACUMULADO 12 MESES";
            bal.Arquivo = String.Format(@"{0}ArquivoApp.aspx?ID={1}", ConfigurationManager.AppSettings["PublicacaoRootOut"], Criptografa(strNomeArquivo));
            balancetes.Add(bal);

            //Monta lista
            foreach (dFinanceiro.BALancetesRow rowBal in dFin.BALancetes)
            {
                //Procura e seta nome do arquivo criptografado (so adiciona na lista se existir)
                strNomeArquivo = String.Format(@"{2:0000}{3:00}/{0}_{1}_B{2:0000}{3:00}.pdf", EMP, CON, rowBal.BALCompet / 100, rowBal.BALCompet % 100);
                if (!System.IO.File.Exists(ConfigurationManager.AppSettings["PathArquivos"] + strNomeArquivo))
                    strNomeArquivo = String.Format(@"{0}_{1}_B{2:0000}{3:00}.pdf", EMP, CON, rowBal.BALCompet / 100, rowBal.BALCompet % 100);
                if (!System.IO.File.Exists(ConfigurationManager.AppSettings["PathArquivos"] + strNomeArquivo))
                    continue;
                
                //Adiciona na lista
                bal = new WCFMobile.Balancete();
                bal.ID = rowBal.BAL;
                bal.Descritivo = "BALANCETE - " + rowBal.BALCompet.ToString().Substring(4,2) + "/" + rowBal.BALCompet.ToString().Substring(0, 4);
                bal.Arquivo = String.Format(@"{0}ArquivoApp.aspx?ID={1}", ConfigurationManager.AppSettings["PublicacaoRootOut"], Criptografa(strNomeArquivo));
                balancetes.Add(bal);
            }

            //Retorna
            return balancetes.AsQueryable();
        }

        private string Criptografa(String strNomeArquivo)
        {
            strNomeArquivo = strNomeArquivo.Replace(".pdf", "");
            strNomeArquivo = strNomeArquivo.Replace("0", "N").Replace("1", "e").Replace("2", "O").Replace("3", "c").Replace("4", "I");
            strNomeArquivo = strNomeArquivo.Replace("5", "m").Replace("6", "D").Replace("7", "s").Replace("8", "R").Replace("9", "b");
            strNomeArquivo = strNomeArquivo.Replace("/", "F").Replace("_", "g");
            return strNomeArquivo;
        }

        [WebGet]
        public IQueryable<Retorno> setSeguroConteudo(String Condominio, String Nome, String Unidade, String Email, String Telefone, String Mensagem, String Token)
        {
            //Verifica token
            if (Token != ConfigurationManager.AppSettings["MobileKeyToken"])
                throw new DataServiceException(400, "Consulta n�o permitida");

            //Inicia retorno
            var retorno = new List<Retorno>();
            Retorno ret = new WCFMobile.Retorno();

            try
            {
                //Pega dados configurados para o conteudo
                string strEmailTo = "";
                string strEmailBcc = "";
                SqlDataReader drConteudo = Conteudo(ConfigurationManager.ConnectionStrings["SiteConnectionString"].ToString(), intCNT);
                if (drConteudo.HasRows)
                {
                    drConteudo.Read();
                    if (drConteudo["CNTEmailTo"] != DBNull.Value) strEmailTo = drConteudo["CNTEmailTo"].ToString();
                    if (drConteudo["CNTEmailBcc"] != DBNull.Value) strEmailBcc = drConteudo["CNTEmailBcc"].ToString();
                }
                drConteudo.Close();

                //Monta e envia e-mail
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();
                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //From
                    email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");

                    //To
                    email.To.Add(strEmailTo.Replace(";", ","));

                    //Bcc
                    if (strEmailBcc != "") email.Bcc.Add(strEmailBcc.Replace(";", ","));

                    //Corpo do email
                    email.Subject = "Solicita��o de Contato - Ades�o Seguro Conte�do (Aplicativo)";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head></head>\r\n" +
                                               "   <body style='font: 11pt Calibri'>\r\n" +
                                               "      <p style='font-size: 14pt'><b>SOLICITA��O DE CONTATO - ADES�O SEGURO CONTE�DO (APLICATIVO)</b></p>\r\n" +
                                               "      <p><b>Data:</b> {0}</p>\r\n" +
                                               "      <p><b>Condom�nio:</b> {1}</p>\r\n" +
                                               "      <p><b>Nome:</b> {2}</p>\r\n" +
                                               "      <p><b>Unidade:</b> {3}</p>\r\n" +
                                               "      <p><b>E-mail:</b> {4}</p>\r\n" +
                                               "      <p><b>Telefone:</b> {5}</p>\r\n" +
                                               "      <p><b>Mensagem:</b><br/>{6}</p>\r\n" +
                                               "   </body>\r\n" +
                                               "</html>",
                                               DateTime.Now, Condominio, Nome, Unidade, Email, Telefone, Mensagem.Replace("\n", "<br/>"));

                    //Envia email
                    SmtpClient1.Send(email);
                }

                //Retorna sucesso
                ret.Ret_Sucesso = true;
                ret.Ret_Mensagem = String.Format("Solicita��o de contato enviada com sucesso! Retornaremos o seu contato o mais breve poss�vel");
            }
            catch (Exception ex)
            {
                //Retorna erro
                ret.Ret_Sucesso = false;
                ret.Ret_Mensagem = String.Format("Ocorreu um erro ao enviar sua solicita��o de contato: {0}", ex.Message);
            }
            retorno.Add(ret);

            //Retorna
            return retorno.AsQueryable();
        }

        public SqlDataReader Conteudo(string connStr, int intCNT)
        {
            //Cria conexao
            SqlConnection myConnection = new SqlConnection(connStr);

            //Abre conexao
            myConnection.Open();

            //Seta Query
            string strQuery = string.Format("SELECT * FROM CoNTeudo WHERE CNT = {0}", intCNT);
            SqlCommand myCommand = new SqlCommand(strQuery, myConnection);

            //Executa
            SqlDataReader drReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            myCommand.Dispose();

            //Retorna o datareader
            return drReader;
        }
    }
}
