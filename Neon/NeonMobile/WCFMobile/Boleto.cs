//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WCFMobile
{
    using System;
    
    public partial class Boleto
    {
        public int BOL { get; set; }
        public string BOLTipoUsuario { get; set; }
        public string BOLTotal { get; set; }
        public string BOLSituacao { get; set; }
        public string BOLLinhaDigitavel { get; set; }
        public string BOLVencto { get; set; }
        public string APTNumero { get; set; }
        public string BLOCodigo { get; set; }
        public string CONNome { get; set; }
        public string BOLOriginal { get; set; }
        public string BOLCorrigido { get; set; }
        public string BOLMulta { get; set; }
        public string BOLJuros { get; set; }
        public string BOLSituacaoCor { get; set; }
        public string BOLTipo { get; set; }
        public bool BOLImprimir { get; set; }
        public string BOLVenctoFinal { get; set; }
        public System.DateTime BOLVenctoDT { get; set; }
    }
}
