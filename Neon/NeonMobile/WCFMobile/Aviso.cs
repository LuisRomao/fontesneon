//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WCFMobile
{
    using System;
    
    public partial class Aviso
    {
        public int AVS { get; set; }
        public int AVS_CON { get; set; }
        public string AVSTitulo { get; set; }
        public string AVSMensagem { get; set; }
        public bool AVSBloco { get; set; }
        public bool AVSUnidade { get; set; }
        public string AVSDataCadastro { get; set; }
    }
}
