﻿/*
 LH - 22/05/2014 14:00            - Notas só são duplicidade se for dentro de 1 ano
 LH - 25/12/2014 14:20  14.1.4.96 - Pagamento com boleto integrado na mesma tela de emissão de cheque
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using CompontesBasicos;
using DocBacarios;
using VirEnumeracoesNeon;
using Framework.objetosNeon;
using FrameworkProc.datasets;
using ContaPagarProc;

namespace ContaPagar
{
    /// <summary>
    /// Cadastra novas notas
    /// </summary>
    public partial class cNovaNota : ComponenteBaseDialog
    {

        //private Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresRow FRNrow;
        private dNOtAs.CONDOMINIOSRow CONrow;
        private Follow.PagamentoPeriodico PagamentoPeriodico = null;
        private int NOANumero;

        private NotasGrade chamador;

        private NOATipo TipoMeste;
        private bool NumeroLiberado;

        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookupLocal;

        private DadosNFe _DadosNFe;

        public cNovaNota(NotasGrade _chamador, DadosNFe cBuscaNota1):this(_chamador,NOATipo.ImportacaoXML,-1)
        {
            _DadosNFe = cBuscaNota1;
            ButImp.Visible = (_DadosNFe.ArquivoTMP != "");
            if (_DadosNFe.CNPJPrestador != null)
                SetaFornecedor(_DadosNFe.CNPJPrestador);
            if (_DadosNFe.CNPJTomador != null)
                setaTomador(_DadosNFe.CNPJTomador);
            spinEdit1.Value = _DadosNFe.NumeroNF;
            if (_DadosNFe.NumeroNF != 0)
                spinEdit1.ReadOnly = true;
            if (_DadosNFe.Emissao != DateTime.MinValue)
            {
                dateEdit1.DateTime = _DadosNFe.Emissao;
                dateEdit1.ReadOnly = true;
            }
            if (_DadosNFe.competencia != null)
            {
                cCompet1.Comp = _DadosNFe.competencia;
                cCompet1.ReadOnly = true;
            }
            calcEdit1.Value = _DadosNFe.Valor;
            if (_DadosNFe.Valor != 0)
                calcEdit1.ReadOnly = true;
            if (_DadosNFe.Descricao.Length > 50)
                comboBoxEdit1.Text = _DadosNFe.Descricao.Substring(0, 50);
            else
                comboBoxEdit1.Text = _DadosNFe.Descricao;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_chamador"></param>
        /// <param name="_TipoMeste"></param>
        /// <param name="CON"></param>
        public cNovaNota(NotasGrade _chamador,NOATipo _TipoMeste, int CON) 
        {           
            InitializeComponent();
            chamador = _chamador;
            TipoMeste = _TipoMeste;
            dPLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25;
            SubPLanoPLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25;
            dFornecedoresLookupLocal = new CadastrosProc.Fornecedores.dFornecedoresLookup();
            dFornecedoresLookupLocal.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookupLocal.FRNLookup);            
            fornecedoresBindingSource.DataSource = dFornecedoresLookupLocal;
            cCompet1.IsNull = true;
            dateEdit1.Properties.MinValue = DateTime.Today.AddMonths(-10);
            dateEdit1.Properties.MaxValue = DateTime.Today;
            xtraTabTipo.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            cCompet1.CompMaxima = new Competencia(DateTime.Today.AddMonths(3));
            switch (TipoMeste)
            {
                case NOATipo.ImportacaoXML:                    
                case NOATipo.Avulsa:
                case NOATipo.NotaParcial:
                case NOATipo.PagamentoPeriodico:
                    xtraTabTipo.SelectedTabPage = xtraTabPageNota;
                    lookUpFOR2.Properties.ReadOnly = true;
                    break;
                case NOATipo.Recibo:
                    NumeroLiberado = true;
                    xtraTabTipo.SelectedTabPage = xtraTabPageNota;
                    groupControlTomador.Enabled = true;
                    CNPJcond.Properties.ReadOnly = true;
                    if(dNOtAs.dNOtAsSt.CONDOMINIOSTableAdapter.FillByCON(dNOtAs.dNOtAsSt.CONDOMINIOS, CON) == 0)
                        throw new Exception(string.Format("Condomínio não encontrado:{0}", CON));
                    CONrow = dNOtAs.dNOtAsSt.CONDOMINIOS[0];
                    CNPJcond.Text = CONrow.CONCnpj;
                    textEditCondominio.Text = string.Format("{0} - {1}", CONrow.CONCodigo, CONrow.CONNome);
                    PopulaCTL(CONrow.CON);
                    break;
                default:
                    throw new Exception(string.Format("Tipo inválido:{0}",TipoMeste));
            }
        }        

        private bool ValidaCamposServico()
        {
            if (lookUpEditPLA1.EditValue == null)
            {
                lookUpEditPLA1.Focus();
                lookUpEditPLA1.ErrorText = "Campo obrigatório";
                return false;
            }
            if (lookUpEdit1.EditValue == null)
            {
                lookUpEdit1.Focus();
                lookUpEdit1.ErrorText = "Campo obrigatório";
                return false;
            }
            if (calcEdit1.Value == 0)
            {
                calcEdit1.Focus();
                calcEdit1.ErrorText = "Campo obrigatório";
                return false;
            }
            if (comboBoxEdit1.Text == "")
            {
                comboBoxEdit1.Focus();
                comboBoxEdit1.ErrorText = "Campo obrigatório";
                return false;
            }
            return true;
        }

        private static string GetComandoBusca25()
        {
            string ComandoBusca25 =
            "SELECT     NOtAs.NOADATAI,NOA\r\n" +
            "FROM         NOtAs\r\n" +
            "WHERE     \r\n" +
            "(NOtAs.NOA_FRN = @P1) \r\n" +            
            "AND \r\n" +
            "(NOtAs.NOA_CON = @P2) \r\n" +
            "AND \r\n" +
            "(NOATotal = @P3) \r\n" +
            "AND \r\n" +
            "(NOtAs.NOACompet = @P4);";
            return ComandoBusca25;
        }

        private static string GetComandoBusca()
        {
            string comandoBusca =
            "SELECT     NOtAs.NOADATAI, CONDOMINIOS.CONCodigo,NOA\r\n" +
            "FROM         NOtAs INNER JOIN\r\n" +
            "                      CONDOMINIOS ON NOtAs.NOA_CON = CONDOMINIOS.CON\r\n" +
            "WHERE     (NOtAs.NOANumero = @P1) AND (NOtAs.NOA_FRN = @P2) AND (NOtAs.NOADataEmissao > @P3);";
            return comandoBusca;
        }

        /// <summary>
        /// Can Closer
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            Validate();
            if (Fornecedor == null)
            {
                CNPJFormat.ErrorText = "Campo obrigatório";
                return false;
            }

            if (CONrow == null)
            {
                CNPJcond.ErrorText = "Campo obrigatório";
                return false;
            }

            //decimal

            if (spinEdit1.Value == 0)
            {
                if (TipoMeste == NOATipo.Avulsa)
                {
                    spinEdit1.ErrorText = "Campo obrigatório";
                    return false;
                }
            }
            else
            {
                NOANumero = (int)spinEdit1.Value;


                DataRow Ret = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(GetComandoBusca(), NOANumero, Fornecedor.FRN, DateTime.Today.AddYears(-1));
                if (Ret != null)
                {
                    DateTime DataInc = (DateTime)Ret["NOADATAI"];
                    if (MessageBox.Show(String.Format("Nota já cadastrada em :{0:dd/MM/yyyy hh:mm}\r\n\r\nNota: {1}\r\n\r\nCondomínio: {2}\r\n\r\nAbrir Nota?", DataInc, NOANumero, (string)Ret["CONCodigo"]), "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        cNotasCampos novocNotas = (cNotasCampos)CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(cNotasCampos), "Nota original", (int)Ret["NOA"], false, chamador, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);                        
                    }
                    else
                        spinEdit1.Focus();
                    return false;
                };




            }

            if (dateEdit1.EditValue == null)
            {
                dateEdit1.ErrorText = "Campo obrigatório";
                return false;
            };

            decimal Total = 0;

            if (!groupControlSevicos.Visible)
            {
                if (!ValidaCamposServico())
                    return false;
                Total = calcEdit1.Value;
            }
            else
            {
                gridView1.CloseEditor();
                foreach (dNovaNota.ServicosRow row in dNovaNota.Servicos)
                {
                    if ((row.Servico == null) || (row.Servico == ""))
                    {
                        //row.RowError = "campo obrigatório";
                        row.SetColumnError(colServico.FieldName, "campo obrigatório");
                        return false;
                    };
                    if (row.Valor == 0)
                    {
                        //row.RowError = "campo obrigatório";
                        row.SetColumnError(colValor.FieldName, "campo obrigatório");                        
                        return false;
                    }
                    Total += row.Valor;
                }
            }

            if (PagamentoPeriodico == null)
            {
                DataRow Ret1 = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(GetComandoBusca25(),
                                                          Fornecedor.FRN,
                                                          CONrow.CON,
                                                          Total,
                                                          cCompet1.Comp.CompetenciaBind);

                if (Ret1 != null)
                {
                    DateTime DataInc = (DateTime)Ret1["NOADATAI"];
                    if (MessageBox.Show(string.Format("ATENÇÃO\r\nJá existe uma nota desta competência cadastrada neste valor em {0}\r\n\r\nAbrir Nota?", DataInc), "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        int NOAAbrir = (int)Ret1["NOA"];
                        cNotasCampos novocNotas = (cNotasCampos)CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(cNotasCampos), "Nota original (Mesmo valor)", NOAAbrir, false, chamador, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                        return false;
                    };
                };
            }

            return base.CanClose();
        }

        private void ParaQuadroTomador()
        {            
            groupControlTomador.Enabled = true;
            CNPJcond.Focus();
        }

        private Cadastros.Fornecedores.Fornecedor Fornecedor;

        private void SetaFornecedor(CPFCNPJ cnpj)
        {
            CNPJFormat.Text = cnpj.ToString();
            Fornecedor = Cadastros.Fornecedores.Fornecedor.GetFornecedor(cnpj, true);
            if (Fornecedor == null)
            {
                CNPJFormat.Text = "";
                CNPJFormat.Focus();
            }
            else
            {
                lookUpFOR2.EditValue = Fornecedor.FRN;
                CNPJFormat.Properties.ReadOnly = true;
                lookUpFOR2.Properties.ReadOnly = true;
                dFornecedoresLookupLocal.FRNLookupTableAdapter.FillByFRN(dFornecedoresLookupLocal.FRNLookup, Fornecedor.FRN);
                switch (TipoMeste)
                {
                    case NOATipo.Avulsa:
                    case NOATipo.NotaParcial:
                    case NOATipo.PagamentoPeriodico:
                    case NOATipo.ImportacaoXML:
                        ParaQuadroTomador();
                        break;
                    case NOATipo.Recibo:
                        ParaQuadroNota();
                        break;
                    default:
                        throw new Exception(string.Format("Tipo inválido:{0}", TipoMeste));
                }

            }
        }

        private void CNPJFormat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;

                CPFCNPJ cnpj = new CPFCNPJ(CNPJFormat.Text);

                if (cnpj.Tipo != TipoCpfCnpj.INVALIDO)
                {                    
                    SetaFornecedor(cnpj);                    
                }
                else
                {
                    MessageBox.Show("CNPJ inválido");                   
                    CNPJFormat.Focus();
                }
            }
        }

        /*
        private void lookUpFOR1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                int FRN = (int)lookUpFOR2.EditValue;
                //CPFCNPJ cnpj = new CPFCNPJ(CNPJFormat.Text);
                //CNPJFormat.Text = cnpj.ToString();
                Fornecedor = new Cadastros.Fornecedores.Fornecedor(FRN);
                //FRNrow = Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fornecedores.FindByFRN(FRN);
                if (Fornecedor.CNPJ != null)
                    CNPJFormat.Text = Fornecedor.CNPJ.ToString();
                groupControlTomador.Enabled = true;
                CNPJcond.Focus();
                
                lookUpFOR2.Properties.ReadOnly = true;
            }
        }*/

        private cNovaNotaCond _cNovaNotaC;

        private cNovaNotaCond cNovaNotaC 
        { 
            get
            {
                if (_cNovaNotaC == null)
                {
                    _cNovaNotaC = new cNovaNotaCond();
                    _cNovaNotaC.dNOtAsBindingSource.DataSource = dNOtAs.dNOtAsSt;
                }
                return _cNovaNotaC; 
            } 
        }

        private void ParaQuadroNota()
        {
            groupControlNota.Enabled = true;
            spinEdit1.Focus();
        }

        private int? NOAProv;

        private void setaTomador(CPFCNPJ cnpj)
        {
            CNPJcond.Text = cnpj.ToString();
            int nCond = dNOtAs.dNOtAsSt.CONDOMINIOSTableAdapter.Fill(dNOtAs.dNOtAsSt.CONDOMINIOS, cnpj.ToString());

            if (nCond == 0)
                MessageBox.Show("Nenhum condomínio com este CNPJ!");
            else if (nCond == 1)
                CONrow = dNOtAs.dNOtAsSt.CONDOMINIOS[0];
            else
            {
                if (cNovaNotaC.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    CONrow = cNovaNotaC.CONrow;
                }
            };
            if (CONrow != null)
            {
                textEditCondominio.Text = string.Format("{0} - {1}", CONrow.CONCodigo, CONrow.CONNome);
                PopulaCTL(CONrow.CON);
                if (dNOtAs.dNOtAsSt.PaGamentosFixosTableAdapter.FillByFRN(dNOtAs.dNOtAsSt.PaGamentosFixos, CONrow.CON, Fornecedor.FRN) > 0)
                {
                    Follow.cContratos cContratos = new ContaPagar.Follow.cContratos();
                    cContratos.dNOtAsBindingSource.DataSource = dNOtAs.dNOtAsSt;
                    switch (cContratos.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp))
                    {
                        case DialogResult.Yes:
                        case DialogResult.OK:
                            PagamentoPeriodico = new ContaPagar.Follow.PagamentoPeriodico(cContratos.PGFrow.PGF);
                            NOAProv = PagamentoPeriodico.NotaProvisoria();
                            if (!NOAProv.HasValue)
                            {
                                PagamentoPeriodico.CadastrarProximo();
                                NOAProv = PagamentoPeriodico.NotaProvisoria();
                            }
                            if (!NOAProv.HasValue)
                            {
                                MessageBox.Show(string.Format("Erro: O pagamento da competência {0} ainda está pendente. Cancele se for o caso.", PagamentoPeriodico.ProximaCompetencia));
                                return;
                            }
                            TXTContrato.Visible = true;
                            cCompet1.IsNull = false;
                            cCompet1.Comp = PagamentoPeriodico.ProximaCompetencia;
                            cCompet1.AjustaTela();
                            cCompet1.ReadOnly = true;
                            lookUpEditPLA1.EditValue = PagamentoPeriodico.PGFrow.PGF_PLA;
                            lookUpEdit1.EditValue = PagamentoPeriodico.PGFrow.PGF_SPL;
                            calcEdit1.Value = PagamentoPeriodico.PGFrow.PGFValor;
                            comboBoxEdit1.Text = PagamentoPeriodico.PGFrow.PGFDescricao;
                            simpleButton1.Visible = false;
                            groupControlNota.Enabled = true;
                            lookUpEdit1.Properties.ReadOnly = lookUpEditPLA1.Properties.ReadOnly = lookUpEditPLA2.Properties.ReadOnly = true;
                            comboBoxEdit1.Properties.ReadOnly = true;
                            calcEdit1.Properties.ReadOnly = true;
                            break;
                        case DialogResult.No:
                            break;
                        case DialogResult.Cancel:
                            return;
                    }

                }
                ParaQuadroNota();
                CNPJcond.Properties.ReadOnly = true;

            }
        }

        private void textEdit4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;

                CPFCNPJ cnpj = new CPFCNPJ(CNPJcond.Text);

                if (cnpj.Tipo != TipoCpfCnpj.INVALIDO)
                {
                    setaTomador(cnpj);
                    
                }
                else
                {
                    MessageBox.Show("CNPJ inválido");
                    CNPJcond.Focus();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if (Resultado == DialogResult.OK)
            {
                if (PagamentoPeriodico != null) //contrato                
                {                    
                    if (NOAProv.HasValue)
                    {
                        cNotasCampos cNotasCampos1 = (cNotasCampos)FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(cNotasCampos), "Nota do contrato", NOAProv.Value, false, chamador, EstadosDosComponentes.JanelasAtivas);
                        cNotasCampos1.Destrava(NOANumero, dateEdit1.DateTime, PagamentoPeriodico,_DadosNFe != null ? _DadosNFe.ArquivoTMP : "");  
                    }
                    else                    
                        throw new Exception(string.Format("Nota provisória não encontrada\r\nPGF = {0}", PagamentoPeriodico.PGF));                    
                }
                else
                {
                    NOATipo TipoDeNota = TipoMeste;
                    if (groupControlSevicos.Visible)
                    {
                        switch (TipoMeste)
                        {
                            case NOATipo.Avulsa:
                            case NOATipo.ImportacaoXML:
                                TipoDeNota = NOATipo.NotaParcial;
                                break;
                            case NOATipo.Recibo:
                                TipoDeNota = NOATipo.ReciboParcial;
                                break;
                            default:
                                throw new Exception(string.Format("Tipo inválido:{0}", TipoMeste));
                        }
                    }
                    else
                    {
                        dNovaNota.Servicos.AddServicosRow(lookUpEditPLA1.EditValue.ToString(),
                                                          (int)lookUpEdit1.EditValue,
                                                          comboBoxEdit1.Text,
                                                          calcEdit1.Value);
                    }

                    if ((NOANumero == 0) && (dNovaNota.Servicos.Count > 1))
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("SELECT MAX(NOANumero) FROM NOtAs WHERE (NOA_FRN = @P1)", out NOANumero, Fornecedor.FRN);
                        NOANumero++;
                        if (NOANumero < 9000000)
                            NOANumero = 9000000;

                    }

                    //if (groupControlSevicos.Visible)
                    //{
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cNovaNota - 448");
                        System.Collections.Generic.List<Nota> NotasCriadas = new List<Nota>();
                        List<dllImpostos.SolicitaRetencao> Sol = null;
                        Sol = new List<dllImpostos.SolicitaRetencao>();
                        if (_DadosNFe != null)
                        {
                            if (_DadosNFe.RetencaISS != 0)
                                Sol.Add(new dllImpostos.SolicitaRetencao(Framework.DSCentral.EMP == 1 ? VirEnumeracoes.TipoImposto.ISSQN : VirEnumeracoes.TipoImposto.ISS_SA, _DadosNFe.RetencaISS, VirEnumeracoes.SimNao.Padrao));
                            decimal TotalCOFPISCSLL = _DadosNFe.RetencaoCOF + _DadosNFe.RetencaoCSLL + _DadosNFe.RetencaoPIS;
                            if (TotalCOFPISCSLL != 0)
                                Sol.Add(new dllImpostos.SolicitaRetencao(VirEnumeracoes.TipoImposto.PIS_COFINS_CSLL, TotalCOFPISCSLL, VirEnumeracoes.SimNao.Padrao));
                            if (_DadosNFe.RetencaoIR != 0)
                                Sol.Add(new dllImpostos.SolicitaRetencao(VirEnumeracoes.TipoImposto.IR, _DadosNFe.RetencaoIR, VirEnumeracoes.SimNao.Padrao));
                            if (_DadosNFe.CNPJPrestador.Tipo == TipoCpfCnpj.CPF)
                                Sol.Add(new dllImpostos.SolicitaRetencao(VirEnumeracoes.TipoImposto.INSSpfRet, Math.Round(_DadosNFe.Valor * 20 / 100, 2, MidpointRounding.AwayFromZero), VirEnumeracoes.SimNao.Padrao));
                            if (_DadosNFe.RetencaoINSS != 0)
                            {
                                if (_DadosNFe.CNPJPrestador.Tipo == TipoCpfCnpj.CNPJ)
                                    Sol.Add(new dllImpostos.SolicitaRetencao(VirEnumeracoes.TipoImposto.INSS, _DadosNFe.RetencaoINSS, VirEnumeracoes.SimNao.Padrao));
                                else                                                                    
                                    Sol.Add(new dllImpostos.SolicitaRetencao(VirEnumeracoes.TipoImposto.INSSpfRet, _DadosNFe.RetencaoINSS, VirEnumeracoes.SimNao.Padrao));                                
                            }

                        }
                        foreach (dNovaNota.ServicosRow row in dNovaNota.Servicos)
                        {                            
                            NotasCriadas.Add(new Nota(CONrow.CON,
                                                      Fornecedor.FRN,
                                                      NOANumero,
                                                      dateEdit1.DateTime,
                                                      row.Valor,
                                                      TipoDeNota,
                                                      row.PLA,
                                                      row.SPL,
                                                      row.Servico,
                                                      (cCompet1.IsNull ? null : cCompet1.Comp),
                                                      _DadosNFe != null ? _DadosNFe.ArquivoTMP : ""
                                                      ));
                        }
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                        int nTotal = NotasCriadas.Count;
                        int i = 0;
                        foreach (Nota NovaNota in NotasCriadas)
                            if (NovaNota.Encontrada())
                            {
                                i++;
                                string Titulo;
                                if (nTotal > 1)
                                    Titulo = string.Format("{0} - Nota:{1} {2}/{3}", CONrow.CONCodigo, NovaNota.LinhaMae.NOANumero, i, nTotal);
                                else
                                    Titulo = string.Format("{0} - Nota:{1}", CONrow.CONCodigo, NovaNota.LinhaMae.NOANumero);
                                cNotasCampos cNota = (cNotasCampos)CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(cNotasCampos), Titulo, NovaNota.LinhaMae.NOA, false, chamador, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                                if ((i==1) && (Sol != null) && (Sol.Count > 0))
                                    cNota.Nota.CadastraRetencaoNaRam(Sol);
                                
                                if (lookCTL.EditValue != null)
                                    cNota.SetCTL((int)lookCTL.EditValue);
                            }
                            else
                                throw new Exception("Erro ao cadastrar as notas\r\nNotas não cadastradas.");
                    }
                    catch (Exception e)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                        throw new Exception("Erro ao cadastrar as notas\r\nNotas não cadastradas.", e);
                    }
                    //}
                    //else
                    /*
                    {
                        Nota NovaNota = new Nota( CONrow.CON, 
                                                  FRNrow.FRN, 
                                                  NOANumero, 
                                                  dateEdit1.DateTime, 
                                                  calcEdit1.Value, 
                                                  NOATipo.Avulsa, 
                                                  lookUpEditPLA1.EditValue.ToString(), 
                                                  (int)lookUpEdit1.EditValue, 
                                                  comboBoxEdit1.Text, 
                                                  (cCompet1.IsNull ? null : cCompet1.Comp), 
                                                  "");
                        //NovaNota.abre
                        if (NovaNota.Encontrada())
                        {
                            string Titulo = string.Format("{0} - Nota:{1}", CONrow.CONCodigo, NovaNota.LinhaMae.NOANumero);
                            CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(cNotasCampos), Titulo, NovaNota.LinhaMae.NOA, false, chamador, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                        }
                        else
                            MessageBox.Show("Cancelado");
                    }
                    */
                }


            }
            base.FechaTela(Resultado);
        }        

        bool SemafaroPLA;

        private Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow;

        private void lookUpEditPLA1_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroPLA)
            {
                try
                {
                    SemafaroPLA = true;
                    if (sender == lookUpEditPLA1)
                        lookUpEditPLA2.EditValue = lookUpEditPLA1.EditValue;
                    if (sender == lookUpEditPLA2)
                        lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue;
                    if ((lookUpEditPLA2.EditValue == null) || (lookUpEditPLA2.EditValue == DBNull.Value))
                    {
                        lookUpEdit1.Enabled = false;
                        PLArow = null;
                    }
                    else
                    {
                        string PLA = lookUpEditPLA2.EditValue.ToString();
                        PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas.FindByPLA(PLA);
                        SubPLanoPLAnocontasBindingSource.Filter = string.Format("SPL_PLA = {0}", PLA);
                        lookUpEdit1.Enabled = true;
                        if (SubPLanoPLAnocontasBindingSource.Count == 0)
                            lookUpEdit1.EditValue = null;
                        else if (SubPLanoPLAnocontasBindingSource.Count == 1)
                        {
                            DataRowView DRV = (DataRowView)SubPLanoPLAnocontasBindingSource[0];
                            Framework.datasets.dPLAnocontas.SubPLanoRow rowsub = (Framework.datasets.dPLAnocontas.SubPLanoRow)DRV.Row;
                            lookUpEdit1.EditValue = rowsub.SPL;
                        }
                        else
                            lookUpEdit1.EditValue = null;
                    }
                    CarregaLista();
                }
                finally
                {
                    SemafaroPLA = false;
                }
            }
        }

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                if (PLArow == null)
                    return;
                cNovoSPL novo = new cNovoSPL(string.Format("{0} - {1}", PLArow.PLA, PLArow.PLADescricao));
                novo.Cadastre(PLArow.PLA);                
            }
        }

        private void comboBoxEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            int nPLA;
            if (int.TryParse(lookUpEditPLA1.EditValue.ToString(), out nPLA))
            {
                switch (e.Button.Kind)
                {
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Plus:
                        dSTRs.Add(nPLA, comboBoxEdit1.Text);
                        CarregaLista();
                        break;
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Minus:
                        dSTRs.Dell(nPLA, comboBoxEdit1.Text);
                        CarregaLista();
                        break;
                }
            };
        }

        private void CarregaLista()
        {
            comboBoxEdit1.Properties.Items.Clear();
            foreach (Framework.datasets.dSTRs.STRsRow rowSTR in dSTRs.STRs)
            {
                if (rowSTR.STRChave.ToString() == lookUpEditPLA1.EditValue.ToString())
                    comboBoxEdit1.Properties.Items.Add(rowSTR.STRTexto);
            };
        }

        private Framework.datasets.dSTRs _dSTRs;

        private Framework.datasets.dSTRs dSTRs
        {
            get
            {
                if (_dSTRs == null)
                {
                    _dSTRs = new Framework.datasets.dSTRs();
                    _dSTRs.Fill("PLA");
                };
                return _dSTRs;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (ValidaCamposServico())
            {
                groupControlSevicos.Visible = true;
                dNovaNota.ServicosRow novarow = dNovaNota.Servicos.NewServicosRow();
                novarow.PLA = lookUpEditPLA1.EditValue.ToString();
                novarow.Servico = comboBoxEdit1.Text;
                novarow.SPL = (int)lookUpEdit1.EditValue;
                novarow.Valor = calcEdit1.Value;
                dNovaNota.Servicos.AddServicosRow(novarow);
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dNovaNota.ServicosRow row = (dNovaNota.ServicosRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            row.Delete();
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (cCompet1.IsNull)
            {
                cCompet1.Comp = new Framework.objetosNeon.Competencia(dateEdit1.DateTime);
                cCompet1.IsNull = false;
                cCompet1.AjustaTela();
            }
            if ((spinEdit1.Value == 0) && (!NumeroLiberado))
                spinEdit1.Focus();
            else
            {
                groupControl2.Enabled = true;
                lookUpEditPLA1.Focus();
            }
        }

        private void comboBoxEdit1_Validating(object sender, CancelEventArgs e)
        {
            string Corrigido = "";
            bool primeiro = true;
            for (int i = 0; i < comboBoxEdit1.Text.Length; i++)
            {
                if (primeiro)
                    Corrigido += char.ToUpper(comboBoxEdit1.Text[i]);
                else
                    Corrigido += char.ToLower(comboBoxEdit1.Text[i]);
                if (comboBoxEdit1.Text[i] != ' ')
                    primeiro = false;
            }
            if (comboBoxEdit1.Text != Corrigido)
                comboBoxEdit1.Text = Corrigido;
        }        

        private void lookUpFOR2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (lookUpFOR2.EditValue != null)
                {
                    int FRN = (int)lookUpFOR2.EditValue;
                    Fornecedor = new Cadastros.Fornecedores.Fornecedor(FRN);
                    //FRNrow = Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fornecedores.FindByFRN(FRN);
                    if (Fornecedor.CNPJ != null)
                        CNPJFormat.Text = Fornecedor.CNPJ.ToString();
                    else
                        CNPJFormat.Text = "";
                    ParaQuadroNota();
                }
            }
        }

        private void spinEdit1_EditValueChanged(object sender, EventArgs e)
        {                        
            if (dateEdit1.EditValue != null)                            
                groupControl2.Enabled = true;                                     
        }

        private dContasLogicas dContasLogicas;

        private void PopulaCTL(int CON)
        {
            dContasLogicas = dContasLogicas.GetdContasLogicas(CON, true);
            conTasLogicasBindingSource.DataSource = dContasLogicas;
            foreach (dContasLogicas.ConTasLogicasRow rowCTL in dContasLogicas.ConTasLogicas)
            {
                switch ((Framework.CTLTipo)rowCTL.CTLTipo)
                {
                    case Framework.CTLTipo.Caixa:
                        lookCTL.EditValue = rowCTL.CTL;
                        break;
                    //case Framework.CTLTipo.Fundo:
                    //  CTLFUNDO = rowCTL.CTL;
                    //break;
                    //case Framework.CTLTipo.CreditosAnteriores:
                    //  _CTLCREDITOS_Anteriores = rowCTL.CTL;
                    //break;
                }
            }      
        }

        private void ButImp_Click(object sender, EventArgs e)
        {
            if ((_DadosNFe != null) && (_DadosNFe.ArquivoTMP != ""))
                System.Diagnostics.Process.Start(_DadosNFe.ArquivoTMP);
        }
    }
}
