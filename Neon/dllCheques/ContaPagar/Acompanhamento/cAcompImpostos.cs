using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using CompontesBasicos;
using VirEnumeracoes;
using dllImpostoNeon;
using VirEnumeracoesNeon;


namespace ContaPagar.Acompanhamento
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAcompImpostos : ComponenteBase
    {
        private TipoImposto[] TipoImpostos;

        /// <summary>
        /// 
        /// </summary>
        public cAcompImpostos()
        {
            InitializeComponent();
            ImpostoNeon.VirEnumTipoImposto.CarregaEditorDaGrid(colPAGIMP);            
            Framework.Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
            Framework.Enumeracoes.VirEnumTiposMalote.CarregaEditorDaGrid(colCONMalote);            
            TipoImpostos = new TipoImposto[] { TipoImposto.INSS, TipoImposto.INSSpfEmp, TipoImposto.INSSpfRet, TipoImposto.IR, TipoImposto.ISSQN, TipoImposto.PIS_COFINS_CSLL };
            ImpostoNeon.VirEnumTipoImposto.GravaCHs(TipoImpostos[0], checkEdit1);
            ImpostoNeon.VirEnumTipoImposto.GravaCHs(TipoImpostos[1], checkEdit2);
            ImpostoNeon.VirEnumTipoImposto.GravaCHs(TipoImpostos[2], checkEdit3);
            ImpostoNeon.VirEnumTipoImposto.GravaCHs(TipoImpostos[3], checkEdit4);
            ImpostoNeon.VirEnumTipoImposto.GravaCHs(TipoImpostos[4], checkEdit5);
            ImpostoNeon.VirEnumTipoImposto.GravaCHs(TipoImpostos[5], checkEdit6);
            foreach (TipoImposto TI in TipoImpostos)
                ImpostoNeon.VirEnumTipoImposto.GetCh(TI).Text = ImpostoNeon.VirEnumTipoImposto.Descritivo(TI);
            dateEdit1.DateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            dateEdit2.DateTime = dateEdit1.DateTime.AddMonths(1).AddDays(-1);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.AtivaGauge(TipoImpostos.Length + 1);
                Application.DoEvents();
                dsImpostos.ImpostosTableAdapter.ClearBeforeFill = false;
                dsImpostos.Impostos.Clear();
                foreach (TipoImposto TI in TipoImpostos)
                {
                    if (ImpostoNeon.VirEnumTipoImposto.GetCh(TI).Checked)
                        dsImpostos.ImpostosTableAdapter.FillByIMPPeriodo(dsImpostos.Impostos, (int)TI, dateEdit1.DateTime, dateEdit2.DateTime);
                    Esp.Gauge();
                    Application.DoEvents();
                }
                dsImpostos.CalculaDataLimite();
            }
        }

        private void repositoryDataQuia_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dsImpostos.ImpostosRow irow = (dsImpostos.ImpostosRow)gridView1.GetFocusedDataRow();
            if (irow == null)
                return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (irow.IsPAGDataGuiaNull())
                    return;
                if (MessageBox.Show("Confirma o cancelamento do guia?", "CANCELAMENTO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    dsImpostos.ImpostosTableAdapter.ApagaData(irow.PAG);
                    irow.SetPAGDataGuiaNull();
                    irow.AcceptChanges();
                }
            }
            else
                if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.OK)
                {                    
                        dsImpostos.ImpostosTableAdapter.GravaData(DateTime.Now,irow.PAG);
                        irow.PAGDataGuia = DateTime.Now;
                        irow.AcceptChanges();                    
                }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return;
            dsImpostos.ImpostosRow linha = ((dsImpostos.ImpostosRow)DRV.Row);
            if (e.Column == colPAGIMP)
            {
                e.Appearance.BackColor = ImpostoNeon.VirEnumTipoImposto.GetCor(linha.PAGIMP);
                e.Appearance.ForeColor = Color.Black;
            }
            else
            {

                DateTime Agora = DateTime.Now;
                DateTime Hoje = DateTime.Today;
                Hoje = Agora.Date;
                int Horadecorte = ((Agora.Hour > 10) ? 24 : 11);
                
                if ((e.Column == colCHEStatus) || (e.Column == colcDataLimite))
                {
                    if ((CHEStatus)linha.CHEStatus != CHEStatus.Cadastrado)
                        return;
                    if (linha.IscDataLimiteNull())
                        return;

                    if (linha.cDataLimite < Agora)
                    {
                        e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                    }
                    else
                        if (linha.cDataLimite <= Hoje.AddHours(Horadecorte))
                        {
                            e.Appearance.BackColor = Color.Yellow;
                            e.Appearance.ForeColor = Color.Black;
                        }

                }
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            dsImpostos.ImpostosRow row = (Acompanhamento.dsImpostos.ImpostosRow)DRV.Row;
            object senhaO = row[dsImpostos.Impostos.CONSenhaPrefColumn, DataRowVersion.Original];
            object senhaN = row[dsImpostos.Impostos.CONSenhaPrefColumn, DataRowVersion.Current];
            if ((senhaO == DBNull.Value) && (senhaN != DBNull.Value) && (senhaN != senhaO))
            {
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update condominios set CONSenhaPref = @P1 where CON = @P2", senhaN, row.CON);                
            };
            object usuO = row[dsImpostos.Impostos.CONUsuPrefColumn, DataRowVersion.Original];
            object usuN = row[dsImpostos.Impostos.CONUsuPrefColumn, DataRowVersion.Current];
            if ((usuO == DBNull.Value) && (usuN != DBNull.Value) && (usuN != usuO))
            {
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update condominios set CONUsuPref = @P1 where CON = @P2", usuN, row.CON);
            };
            row.AcceptChanges();
        }

        private void repositoryItemImageComboBox1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            
        }

        private void BuscaNota(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dsImpostos.ImpostosRow row = (Acompanhamento.dsImpostos.ImpostosRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(cNotasCampos), "NOTA", row.NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog FD = new SaveFileDialog();
            FD.Filter = "*.xls|*.xls";
            if(FD.ShowDialog() == DialogResult.OK)
            {
                DevExpress.XtraPrinting.XlsExportOptions Ops = new DevExpress.XtraPrinting.XlsExportOptions();
                Ops.SheetName = "Impostos";
                //Ops.ShowGridLines = false;
                Ops.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value;
                
                //gridView1.AppearancePrint.FooterPanel.BackColor = System.Drawing.Color.White;
//gridView1.AppearancePrint.FooterPanel.ForeColor = System.Drawing.Color.Black;
//gridView1.AppearancePrint.FooterPanel.Options.UseBackColor = true;
//gridView1.AppearancePrint.FooterPanel.Options.UseForeColor = true;
//gridView1.AppearancePrint.GroupRow.BackColor = System.Drawing.Color.White;
//gridView1.AppearancePrint.GroupRow.ForeColor = System.Drawing.Color.Black;
//gridView1.AppearancePrint.GroupRow.Options.UseBackColor = true;
//gridView1.AppearancePrint.GroupRow.Options.UseForeColor = true;
//gridView1.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.White;
//gridView1.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Black;
//gridView1.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
//gridView1.AppearancePrint.HeaderPanel.Options.UseForeColor = true;



                //gridView1.ExportToXls("c:\\gridcom.xls",Ops);
                //DevExpress.XtraGrid.StyleFormatConditionCollection col = new DevExpress.XtraGrid.StyleFormatConditionCollection(gridView1);
                //col.Assign(gridView1.FormatConditions);
                //gridView1.FormatConditions.Clear();

                //gridView1.ExportToXls("c:\\gridsem.xls", Ops);
                gridView1.ExportToXls(FD.FileName,Ops);

                //gridView1.FormatConditions.Assign(col);
                //Ops.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Text;
                //gridView1.ExportToXls("c:\\gridtxt.xls", Ops);

                
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            dsImpostos.Impostos.Clear();
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            dsImpostos.Impostos.Clear();
        }

        //private string Caminho;

        

        
        
    }
}
