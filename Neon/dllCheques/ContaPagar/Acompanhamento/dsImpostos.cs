﻿using VirEnumeracoesNeon;

namespace ContaPagar.Acompanhamento {


    partial class dsImpostos
    {
        private dsImpostosTableAdapters.ImpostosTableAdapter impostosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Impostos
        /// </summary>
        public dsImpostosTableAdapters.ImpostosTableAdapter ImpostosTableAdapter
        {
            get
            {
                if (impostosTableAdapter == null)
                {
                    impostosTableAdapter = new dsImpostosTableAdapters.ImpostosTableAdapter();
                    impostosTableAdapter.TrocarStringDeConexao();
                };
                return impostosTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CalculaDataLimite() 
        {
            foreach (dsImpostos.ImpostosRow irow in Impostos) 
            {                
                if ((CHEStatus)irow.CHEStatus == CHEStatus.Cadastrado)
                {
                    dllCheques.Cheque cheque = new dllCheques.Cheque(irow.CHE,dllCheques.Cheque.TipoChave.CHE);

                    cheque.CalculaDatas();
                    if (cheque.MaloteIda != null)
                        irow.cDataLimite = cheque.MaloteIda.DataEnvio;
                }
            }
        }

    }
}
