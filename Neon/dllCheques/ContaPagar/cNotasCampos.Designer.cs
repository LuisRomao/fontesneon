namespace ContaPagar
{
    partial class cNotasCampos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cNotasCampos));
            System.Windows.Forms.Label nOANumeroLabel;
            System.Windows.Forms.Label nOADataEmissaoLabel;
            System.Windows.Forms.Label nOATotalLabel;
            System.Windows.Forms.Label nOATipoLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelDadosDep = new System.Windows.Forms.Label();
            this.NIT_IM = new System.Windows.Forms.Label();
            this.dNOtAsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAGamentosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fornecedoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAGamentosGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colPAGVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPAGI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTipoPag = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPAGDOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBotao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHENumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGIMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryImpostos = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCHEFavorecido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboFavorecido = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colPAGAdicional = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGPermiteAgrupar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCHEStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryCHESTATUS = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colbotaoCheque = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_CTL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_CHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHE_CCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Botoes = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.BotaoCheque = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryTipoPagFull = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryTipoPagGuia = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.nOANumeroSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.nOADataEmissaoDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.nOATotalSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.nOAAguardaNotaCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.lookUpFOR2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpFOR1 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditPLA2 = new DevExpress.XtraEditors.LookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CNPJFormat = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEditPLA1 = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.chPagao = new DevExpress.XtraEditors.CheckEdit();
            this.ComboTipo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.DataLimite = new DevExpress.XtraEditors.DateEdit();
            this.grcForn = new DevExpress.XtraEditors.GroupControl();
            this.lblContaCredito = new System.Windows.Forms.Label();
            this.NIT_IMValor = new DevExpress.XtraEditors.TextEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.label5 = new System.Windows.Forms.Label();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.SubPLanoPLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.BotDestrava = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoIntegrador1 = new Framework.Integrador.cBotaoIntegrador();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.bAutoTeste = new System.Windows.Forms.Button();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.bAutoTeste1 = new System.Windows.Forms.Button();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.PControles = new DevExpress.XtraEditors.PanelControl();
            this.BotINSS = new DevExpress.XtraEditors.SimpleButton();
            this.BotISS = new DevExpress.XtraEditors.SimpleButton();
            this.BotPis = new DevExpress.XtraEditors.SimpleButton();
            this.BotIR = new DevExpress.XtraEditors.SimpleButton();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.ButImp = new DevExpress.XtraEditors.SimpleButton();
            this.BotRemove = new DevExpress.XtraEditors.SimpleButton();
            this.cmbContaCredito = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.chkDepProprioCon = new DevExpress.XtraEditors.CheckEdit();
            this.lookCTL = new DevExpress.XtraEditors.LookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbContaDebito = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lblContaDebito = new System.Windows.Forms.Label();
            this.textDadosDep = new DevExpress.XtraEditors.TextEdit();
            this.Antec = new System.Windows.Forms.Label();
            this.ComboPAGTipoDefault = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.ChProc = new DevExpress.XtraEditors.CheckEdit();
            this.comboMalotes1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            nOANumeroLabel = new System.Windows.Forms.Label();
            nOADataEmissaoLabel = new System.Windows.Forms.Label();
            nOATotalLabel = new System.Windows.Forms.Label();
            nOATipoLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNOtAsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGamentosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGamentosGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoPag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryImpostos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboFavorecido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCHESTATUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Botoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotaoCheque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoPagFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoPagGuia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOANumeroSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOADataEmissaoDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOADataEmissaoDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOATotalSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOAAguardaNotaCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CNPJFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chPagao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataLimite.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataLimite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcForn)).BeginInit();
            this.grcForn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NIT_IMValor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubPLanoPLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PControles)).BeginInit();
            this.PControles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbContaCredito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepProprioCon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookCTL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbContaDebito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDadosDep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboPAGTipoDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChProc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboMalotes1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // nOANumeroLabel
            // 
            nOANumeroLabel.AutoSize = true;
            nOANumeroLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nOANumeroLabel.Location = new System.Drawing.Point(7, 26);
            nOANumeroLabel.Name = "nOANumeroLabel";
            nOANumeroLabel.Size = new System.Drawing.Size(54, 13);
            nOANumeroLabel.TabIndex = 9;
            nOANumeroLabel.Text = "N�mero:";
            // 
            // nOADataEmissaoLabel
            // 
            nOADataEmissaoLabel.AutoSize = true;
            nOADataEmissaoLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nOADataEmissaoLabel.Location = new System.Drawing.Point(7, 56);
            nOADataEmissaoLabel.Name = "nOADataEmissaoLabel";
            nOADataEmissaoLabel.Size = new System.Drawing.Size(56, 13);
            nOADataEmissaoLabel.TabIndex = 11;
            nOADataEmissaoLabel.Text = "Emiss�o:";
            // 
            // nOATotalLabel
            // 
            nOATotalLabel.AutoSize = true;
            nOATotalLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nOATotalLabel.Location = new System.Drawing.Point(296, 56);
            nOATotalLabel.Name = "nOATotalLabel";
            nOATotalLabel.Size = new System.Drawing.Size(39, 13);
            nOATotalLabel.TabIndex = 15;
            nOATotalLabel.Text = "Total:";
            // 
            // nOATipoLabel
            // 
            nOATipoLabel.AutoSize = true;
            nOATipoLabel.Location = new System.Drawing.Point(660, 27);
            nOATipoLabel.Name = "nOATipoLabel";
            nOATipoLabel.Size = new System.Drawing.Size(31, 13);
            nOATipoLabel.TabIndex = 23;
            nOATipoLabel.Text = "Tipo:";
            nOATipoLabel.Visible = false;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(272, 26);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(63, 13);
            label1.TabIndex = 35;
            label1.Text = "CNPJ/CPF:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(8, 42);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(88, 13);
            label2.TabIndex = 18;
            label2.Text = "Primeira Data:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(8, 72);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(58, 13);
            label3.TabIndex = 19;
            label3.Text = "Parcelas:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(7, 25);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(87, 13);
            label4.TabIndex = 43;
            label4.Text = "F. pagamento:";
            // 
            // labelDadosDep
            // 
            this.labelDadosDep.AutoSize = true;
            this.labelDadosDep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDadosDep.Location = new System.Drawing.Point(5, 85);
            this.labelDadosDep.Name = "labelDadosDep";
            this.labelDadosDep.Size = new System.Drawing.Size(60, 13);
            this.labelDadosDep.TabIndex = 45;
            this.labelDadosDep.Text = "Dep�sito:";
            this.labelDadosDep.Visible = false;
            // 
            // NIT_IM
            // 
            this.NIT_IM.AutoSize = true;
            this.NIT_IM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NIT_IM.Location = new System.Drawing.Point(694, 26);
            this.NIT_IM.Name = "NIT_IM";
            this.NIT_IM.Size = new System.Drawing.Size(34, 13);
            this.NIT_IM.TabIndex = 24;
            this.NIT_IM.Text = "Tipo:";
            this.NIT_IM.Visible = false;
            // 
            // dNOtAsBindingSource
            // 
            this.dNOtAsBindingSource.DataMember = "NOtAs";
            this.dNOtAsBindingSource.DataSource = typeof(ContaPagarProc.dNOtAs);
            // 
            // pAGamentosBindingSource
            // 
            this.pAGamentosBindingSource.DataMember = "FK_PAGamentos_NOtAs";
            this.pAGamentosBindingSource.DataSource = this.dNOtAsBindingSource;
            // 
            // fornecedoresBindingSource
            // 
            this.fornecedoresBindingSource.DataMember = "FORNECEDORES";
            this.fornecedoresBindingSource.DataSource = typeof(ContaPagarProc.dNOtAs);
            // 
            // pAGamentosGridControl
            // 
            this.pAGamentosGridControl.DataSource = this.pAGamentosBindingSource;
            this.pAGamentosGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pAGamentosGridControl.Location = new System.Drawing.Point(2, 20);
            this.pAGamentosGridControl.MainView = this.gridView1;
            this.pAGamentosGridControl.Name = "pAGamentosGridControl";
            this.pAGamentosGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTipoPag,
            this.repositoryItemLookUpUSU,
            this.Botoes,
            this.repositoryItemCalcEdit1,
            this.repositoryImpostos,
            this.repositoryItemCheckEdit1,
            this.ComboFavorecido,
            this.repositoryCHESTATUS,
            this.BotaoCheque,
            this.repositoryTipoPagFull,
            this.repositoryTipoPagGuia});
            this.pAGamentosGridControl.Size = new System.Drawing.Size(1218, 323);
            this.pAGamentosGridControl.TabIndex = 0;
            this.pAGamentosGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn1,
            this.colPAGValor,
            this.colPAGVencimento,
            this.colPAGA_USU,
            this.colPAGI_USU,
            this.colPAGTipo,
            this.colPAGDOC,
            this.colBotao,
            this.colCHENumero,
            this.colPAGIMP,
            this.colCHEFavorecido,
            this.colPAGAdicional,
            this.colPAGPermiteAgrupar,
            this.colCHEStatus,
            this.colbotaoCheque,
            this.colPAG,
            this.colPAG_CTL,
            this.colBOD,
            this.colPAG_CHE,
            this.colCHE_CCT});
            this.gridView1.GridControl = this.pAGamentosGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPAGVencimento, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanging);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Data Inclus�o";
            this.gridColumn2.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm";
            this.gridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn2.FieldName = "PAGDATAI";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Data Altera��o";
            this.gridColumn1.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn1.FieldName = "PAGDATAA";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            // 
            // colPAGValor
            // 
            this.colPAGValor.Caption = "Valor";
            this.colPAGValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colPAGValor.DisplayFormat.FormatString = "n2";
            this.colPAGValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPAGValor.FieldName = "PAGValor";
            this.colPAGValor.Name = "colPAGValor";
            this.colPAGValor.OptionsColumn.FixedWidth = true;
            this.colPAGValor.Visible = true;
            this.colPAGValor.VisibleIndex = 1;
            this.colPAGValor.Width = 68;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.EditFormat.FormatString = "0.00";
            this.repositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            this.repositoryItemCalcEdit1.Precision = 2;
            // 
            // colPAGVencimento
            // 
            this.colPAGVencimento.Caption = "Vencto";
            this.colPAGVencimento.FieldName = "PAGVencimento";
            this.colPAGVencimento.Name = "colPAGVencimento";
            this.colPAGVencimento.OptionsColumn.FixedWidth = true;
            this.colPAGVencimento.Visible = true;
            this.colPAGVencimento.VisibleIndex = 0;
            this.colPAGVencimento.Width = 69;
            // 
            // colPAGA_USU
            // 
            this.colPAGA_USU.Caption = "Altera��o";
            this.colPAGA_USU.ColumnEdit = this.repositoryItemLookUpUSU;
            this.colPAGA_USU.FieldName = "PAGA_USU";
            this.colPAGA_USU.Name = "colPAGA_USU";
            this.colPAGA_USU.OptionsColumn.FixedWidth = true;
            // 
            // repositoryItemLookUpUSU
            // 
            this.repositoryItemLookUpUSU.AutoHeight = false;
            this.repositoryItemLookUpUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpUSU.DataSource = this.uSUariosBindingSource;
            this.repositoryItemLookUpUSU.DisplayMember = "USUNome";
            this.repositoryItemLookUpUSU.Name = "repositoryItemLookUpUSU";
            this.repositoryItemLookUpUSU.ValueMember = "USU";
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colPAGI_USU
            // 
            this.colPAGI_USU.Caption = "Inclus�o";
            this.colPAGI_USU.ColumnEdit = this.repositoryItemLookUpUSU;
            this.colPAGI_USU.FieldName = "PAGI_USU";
            this.colPAGI_USU.Name = "colPAGI_USU";
            this.colPAGI_USU.OptionsColumn.FixedWidth = true;
            // 
            // colPAGTipo
            // 
            this.colPAGTipo.Caption = "Tipo";
            this.colPAGTipo.ColumnEdit = this.repositoryTipoPag;
            this.colPAGTipo.FieldName = "PAGTipo";
            this.colPAGTipo.Name = "colPAGTipo";
            this.colPAGTipo.OptionsColumn.FixedWidth = true;
            this.colPAGTipo.Visible = true;
            this.colPAGTipo.VisibleIndex = 2;
            this.colPAGTipo.Width = 61;
            // 
            // repositoryTipoPag
            // 
            this.repositoryTipoPag.AutoHeight = false;
            this.repositoryTipoPag.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTipoPag.Name = "repositoryTipoPag";
            // 
            // colPAGDOC
            // 
            this.colPAGDOC.Caption = "Parcela";
            this.colPAGDOC.FieldName = "PAGN";
            this.colPAGDOC.Name = "colPAGDOC";
            this.colPAGDOC.OptionsColumn.FixedWidth = true;
            this.colPAGDOC.Visible = true;
            this.colPAGDOC.VisibleIndex = 3;
            this.colPAGDOC.Width = 52;
            // 
            // colBotao
            // 
            this.colBotao.Caption = "gridColumn3";
            this.colBotao.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBotao.Name = "colBotao";
            this.colBotao.OptionsColumn.FixedWidth = true;
            this.colBotao.OptionsColumn.ShowCaption = false;
            this.colBotao.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colBotao.Visible = true;
            this.colBotao.VisibleIndex = 10;
            this.colBotao.Width = 25;
            // 
            // colCHENumero
            // 
            this.colCHENumero.Caption = "Cheque";
            this.colCHENumero.FieldName = "CHENumero";
            this.colCHENumero.Name = "colCHENumero";
            this.colCHENumero.OptionsColumn.FixedWidth = true;
            this.colCHENumero.OptionsColumn.ReadOnly = true;
            this.colCHENumero.Visible = true;
            this.colCHENumero.VisibleIndex = 5;
            this.colCHENumero.Width = 67;
            // 
            // colPAGIMP
            // 
            this.colPAGIMP.Caption = "Imposto";
            this.colPAGIMP.ColumnEdit = this.repositoryImpostos;
            this.colPAGIMP.FieldName = "PAGIMP";
            this.colPAGIMP.Name = "colPAGIMP";
            this.colPAGIMP.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPAGIMP.OptionsColumn.FixedWidth = true;
            this.colPAGIMP.OptionsColumn.ReadOnly = true;
            this.colPAGIMP.Visible = true;
            this.colPAGIMP.VisibleIndex = 6;
            this.colPAGIMP.Width = 53;
            // 
            // repositoryImpostos
            // 
            this.repositoryImpostos.AutoHeight = false;
            this.repositoryImpostos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryImpostos.Name = "repositoryImpostos";
            // 
            // colCHEFavorecido
            // 
            this.colCHEFavorecido.Caption = "Favorecido";
            this.colCHEFavorecido.ColumnEdit = this.ComboFavorecido;
            this.colCHEFavorecido.FieldName = "CHEFavorecido";
            this.colCHEFavorecido.Name = "colCHEFavorecido";
            this.colCHEFavorecido.Visible = true;
            this.colCHEFavorecido.VisibleIndex = 7;
            this.colCHEFavorecido.Width = 223;
            // 
            // ComboFavorecido
            // 
            this.ComboFavorecido.AutoHeight = false;
            this.ComboFavorecido.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboFavorecido.Name = "ComboFavorecido";
            // 
            // colPAGAdicional
            // 
            this.colPAGAdicional.Caption = "Adicional";
            this.colPAGAdicional.FieldName = "PAGAdicional";
            this.colPAGAdicional.Name = "colPAGAdicional";
            this.colPAGAdicional.OptionsColumn.AllowEdit = false;
            this.colPAGAdicional.Visible = true;
            this.colPAGAdicional.VisibleIndex = 8;
            this.colPAGAdicional.Width = 100;
            // 
            // colPAGPermiteAgrupar
            // 
            this.colPAGPermiteAgrupar.Caption = "Ag.";
            this.colPAGPermiteAgrupar.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPAGPermiteAgrupar.CustomizationCaption = "Permite agrupar";
            this.colPAGPermiteAgrupar.FieldName = "PAGPermiteAgrupar";
            this.colPAGPermiteAgrupar.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colPAGPermiteAgrupar.Name = "colPAGPermiteAgrupar";
            this.colPAGPermiteAgrupar.OptionsColumn.AllowEdit = false;
            this.colPAGPermiteAgrupar.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colPAGPermiteAgrupar.OptionsColumn.AllowIncrementalSearch = false;
            this.colPAGPermiteAgrupar.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPAGPermiteAgrupar.OptionsColumn.FixedWidth = true;
            this.colPAGPermiteAgrupar.Visible = true;
            this.colPAGPermiteAgrupar.VisibleIndex = 9;
            this.colPAGPermiteAgrupar.Width = 27;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.Caption = "Status do cheque";
            this.colCHEStatus.ColumnEdit = this.repositoryCHESTATUS;
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.OptionsColumn.AllowEdit = false;
            this.colCHEStatus.OptionsColumn.AllowIncrementalSearch = false;
            this.colCHEStatus.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCHEStatus.OptionsColumn.FixedWidth = true;
            this.colCHEStatus.OptionsColumn.ReadOnly = true;
            this.colCHEStatus.OptionsFilter.AllowAutoFilter = false;
            this.colCHEStatus.OptionsFilter.AllowFilter = false;
            this.colCHEStatus.Visible = true;
            this.colCHEStatus.VisibleIndex = 4;
            this.colCHEStatus.Width = 96;
            // 
            // repositoryCHESTATUS
            // 
            this.repositoryCHESTATUS.AutoHeight = false;
            this.repositoryCHESTATUS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryCHESTATUS.Name = "repositoryCHESTATUS";
            // 
            // colbotaoCheque
            // 
            this.colbotaoCheque.Caption = "gridColumn3";
            this.colbotaoCheque.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colbotaoCheque.Name = "colbotaoCheque";
            this.colbotaoCheque.OptionsColumn.FixedWidth = true;
            this.colbotaoCheque.OptionsColumn.ShowCaption = false;
            this.colbotaoCheque.Visible = true;
            this.colbotaoCheque.VisibleIndex = 11;
            this.colbotaoCheque.Width = 25;
            // 
            // colPAG
            // 
            this.colPAG.FieldName = "PAG";
            this.colPAG.Name = "colPAG";
            this.colPAG.OptionsColumn.ReadOnly = true;
            // 
            // colPAG_CTL
            // 
            this.colPAG_CTL.FieldName = "PAG_CTL";
            this.colPAG_CTL.Name = "colPAG_CTL";
            this.colPAG_CTL.OptionsColumn.ReadOnly = true;
            // 
            // colBOD
            // 
            this.colBOD.FieldName = "BOD";
            this.colBOD.Name = "colBOD";
            // 
            // colPAG_CHE
            // 
            this.colPAG_CHE.FieldName = "PAG_CHE";
            this.colPAG_CHE.Name = "colPAG_CHE";
            // 
            // colCHE_CCT
            // 
            this.colCHE_CCT.FieldName = "CHE_CCT";
            this.colCHE_CCT.Name = "colCHE_CCT";
            // 
            // Botoes
            // 
            this.Botoes.AutoHeight = false;
            this.Botoes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.Botoes.Name = "Botoes";
            this.Botoes.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.Botoes.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.Botoes_ButtonClick);
            // 
            // BotaoCheque
            // 
            this.BotaoCheque.AutoHeight = false;
            editorButtonImageOptions1.Image = global::ContaPagar.Properties.Resources.impCheque1;
            this.BotaoCheque.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.BotaoCheque.Name = "BotaoCheque";
            this.BotaoCheque.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotaoCheque.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotaoCheque_ButtonClick);
            // 
            // repositoryTipoPagFull
            // 
            this.repositoryTipoPagFull.AutoHeight = false;
            this.repositoryTipoPagFull.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTipoPagFull.Name = "repositoryTipoPagFull";
            this.repositoryTipoPagFull.ReadOnly = true;
            // 
            // repositoryTipoPagGuia
            // 
            this.repositoryTipoPagGuia.AutoHeight = false;
            this.repositoryTipoPagGuia.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTipoPagGuia.Name = "repositoryTipoPagGuia";
            // 
            // nOANumeroSpinEdit
            // 
            this.nOANumeroSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOANumero", true));
            this.nOANumeroSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nOANumeroSpinEdit.Location = new System.Drawing.Point(69, 23);
            this.nOANumeroSpinEdit.Name = "nOANumeroSpinEdit";
            this.nOANumeroSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.nOANumeroSpinEdit.Properties.IsFloatValue = false;
            this.nOANumeroSpinEdit.Properties.Mask.EditMask = "n0";
            this.nOANumeroSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.nOANumeroSpinEdit.TabIndex = 0;
            // 
            // nOADataEmissaoDateEdit
            // 
            this.nOADataEmissaoDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOADataEmissao", true));
            this.nOADataEmissaoDateEdit.EditValue = null;
            this.nOADataEmissaoDateEdit.Location = new System.Drawing.Point(69, 53);
            this.nOADataEmissaoDateEdit.Name = "nOADataEmissaoDateEdit";
            this.nOADataEmissaoDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nOADataEmissaoDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.nOADataEmissaoDateEdit.Size = new System.Drawing.Size(100, 20);
            this.nOADataEmissaoDateEdit.TabIndex = 2;
            this.nOADataEmissaoDateEdit.EditValueChanged += new System.EventHandler(this.nOADataEmissaoDateEdit_EditValueChanged);
            // 
            // nOATotalSpinEdit
            // 
            this.nOATotalSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOATotal", true));
            this.nOATotalSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nOATotalSpinEdit.Location = new System.Drawing.Point(341, 51);
            this.nOATotalSpinEdit.Name = "nOATotalSpinEdit";
            this.nOATotalSpinEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(231)))), ((int)(((byte)(210)))));
            this.nOATotalSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nOATotalSpinEdit.Properties.Appearance.Options.UseBackColor = true;
            this.nOATotalSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.nOATotalSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.nOATotalSpinEdit.Properties.DisplayFormat.FormatString = "n2";
            this.nOATotalSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.nOATotalSpinEdit.Properties.EditFormat.FormatString = "n2";
            this.nOATotalSpinEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.nOATotalSpinEdit.Size = new System.Drawing.Size(128, 22);
            this.nOATotalSpinEdit.TabIndex = 3;
            this.nOATotalSpinEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.nOATotalSpinEdit_ButtonClick);
            this.nOATotalSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.nOATotalSpinEdit_Validating);
            // 
            // nOAAguardaNotaCheckEdit
            // 
            this.nOAAguardaNotaCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOAAguardaNota", true));
            this.nOAAguardaNotaCheckEdit.Location = new System.Drawing.Point(486, 24);
            this.nOAAguardaNotaCheckEdit.Name = "nOAAguardaNotaCheckEdit";
            this.nOAAguardaNotaCheckEdit.Properties.Caption = "Aguardando a nota";
            this.nOAAguardaNotaCheckEdit.Size = new System.Drawing.Size(118, 19);
            this.nOAAguardaNotaCheckEdit.TabIndex = 28;
            this.nOAAguardaNotaCheckEdit.TabStop = false;
            // 
            // lookUpFOR2
            // 
            this.lookUpFOR2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOA_FRN", true));
            this.lookUpFOR2.Location = new System.Drawing.Point(175, 23);
            this.lookUpFOR2.Name = "lookUpFOR2";
            this.lookUpFOR2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lookUpFOR2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "CNPJ"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpFOR2.Properties.DataSource = this.fornecedoresBindingSource;
            this.lookUpFOR2.Properties.DisplayMember = "FRNNome";
            this.lookUpFOR2.Properties.ValueMember = "FRN";
            this.lookUpFOR2.Size = new System.Drawing.Size(508, 20);
            this.lookUpFOR2.TabIndex = 29;
            this.lookUpFOR2.TabStop = false;
            this.lookUpFOR2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpFOR2_ButtonClick);
            this.lookUpFOR2.EditValueChanged += new System.EventHandler(this.lookUpFOR2_EditValueChanged);
            // 
            // lookUpFOR1
            // 
            this.lookUpFOR1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOA_FRN", true));
            this.lookUpFOR1.Location = new System.Drawing.Point(5, 23);
            this.lookUpFOR1.Name = "lookUpFOR1";
            this.lookUpFOR1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lookUpFOR1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "CNPJ"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome")});
            this.lookUpFOR1.Properties.DataSource = this.fornecedoresBindingSource;
            this.lookUpFOR1.Properties.DisplayMember = "FRNCnpj";
            this.lookUpFOR1.Properties.NullText = "CNPJ";
            this.lookUpFOR1.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.OnlyInPopup;
            this.lookUpFOR1.Properties.ValueMember = "FRN";
            this.lookUpFOR1.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpFOR1_Properties_ButtonClick);
            this.lookUpFOR1.Size = new System.Drawing.Size(155, 20);
            this.lookUpFOR1.TabIndex = 30;
            this.lookUpFOR1.TabStop = false;
            this.lookUpFOR1.EditValueChanged += new System.EventHandler(this.lookUpFOR2_EditValueChanged);
            // 
            // lookUpEditPLA2
            // 
            this.lookUpEditPLA2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOA_PLA", true));
            this.lookUpEditPLA2.Location = new System.Drawing.Point(184, 23);
            this.lookUpEditPLA2.Name = "lookUpEditPLA2";
            this.lookUpEditPLA2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditPLA2.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA2.Properties.DisplayMember = "PLADescricao";
            this.lookUpEditPLA2.Properties.ValueMember = "PLA";
            this.lookUpEditPLA2.Size = new System.Drawing.Size(499, 20);
            this.lookUpEditPLA2.TabIndex = 4;
            this.lookUpEditPLA2.TabStop = false;
            this.lookUpEditPLA2.EditValueChanged += new System.EventHandler(this.lookUpEditPLA2_EditValueChanged);
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // CNPJFormat
            // 
            this.CNPJFormat.Location = new System.Drawing.Point(341, 24);
            this.CNPJFormat.Name = "CNPJFormat";
            this.CNPJFormat.Size = new System.Drawing.Size(128, 20);
            this.CNPJFormat.TabIndex = 1;
            this.CNPJFormat.TextChanged += new System.EventHandler(this.CNPJFormat_TextChanged);
            this.CNPJFormat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CNPJFormat_KeyDown);
            // 
            // lookUpEditPLA1
            // 
            this.lookUpEditPLA1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOA_PLA", true));
            this.lookUpEditPLA1.Location = new System.Drawing.Point(5, 23);
            this.lookUpEditPLA1.Name = "lookUpEditPLA1";
            this.lookUpEditPLA1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditPLA1.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA1.Properties.DisplayMember = "PLA";
            this.lookUpEditPLA1.Properties.ValueMember = "PLA";
            this.lookUpEditPLA1.Size = new System.Drawing.Size(155, 20);
            this.lookUpEditPLA1.TabIndex = 0;
            this.lookUpEditPLA1.EditValueChanged += new System.EventHandler(this.lookUpEditPLA2_EditValueChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cCompet1);
            this.groupControl1.Controls.Add(this.chPagao);
            this.groupControl1.Controls.Add(this.ComboTipo);
            this.groupControl1.Controls.Add(nOANumeroLabel);
            this.groupControl1.Controls.Add(this.nOANumeroSpinEdit);
            this.groupControl1.Controls.Add(this.CNPJFormat);
            this.groupControl1.Controls.Add(label1);
            this.groupControl1.Controls.Add(this.nOAAguardaNotaCheckEdit);
            this.groupControl1.Controls.Add(nOATipoLabel);
            this.groupControl1.Controls.Add(nOADataEmissaoLabel);
            this.groupControl1.Controls.Add(this.nOADataEmissaoDateEdit);
            this.groupControl1.Controls.Add(nOATotalLabel);
            this.groupControl1.Controls.Add(this.nOATotalSpinEdit);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 146);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1452, 80);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "NOTA";
            this.groupControl1.Enter += new System.EventHandler(this.groupControl1_Enter);
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.dNOtAsBindingSource, "NOACompet", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(175, 49);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 45;
            this.cCompet1.TabStop = false;
            this.cCompet1.Titulo = "Framework.objetosNeon.cCompet - Framework.objetosNeon.cCompet - ";
            // 
            // chPagao
            // 
            this.chPagao.Location = new System.Drawing.Point(486, 50);
            this.chPagao.Name = "chPagao";
            this.chPagao.Properties.Caption = "Codom�nio paga os Impostos";
            this.chPagao.Size = new System.Drawing.Size(197, 19);
            this.chPagao.TabIndex = 44;
            this.chPagao.TabStop = false;
            // 
            // ComboTipo
            // 
            this.ComboTipo.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOATipo", true));
            this.ComboTipo.Location = new System.Drawing.Point(697, 24);
            this.ComboTipo.Name = "ComboTipo";
            this.ComboTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboTipo.Properties.ReadOnly = true;
            this.ComboTipo.Size = new System.Drawing.Size(161, 20);
            this.ComboTipo.TabIndex = 43;
            this.ComboTipo.TabStop = false;
            this.ComboTipo.Visible = false;
            // 
            // DataLimite
            // 
            this.DataLimite.EditValue = null;
            this.DataLimite.Location = new System.Drawing.Point(800, 24);
            this.DataLimite.Name = "DataLimite";
            this.DataLimite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataLimite.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.DataLimite.Properties.Appearance.Options.UseFont = true;
            this.DataLimite.Properties.Appearance.Options.UseForeColor = true;
            this.DataLimite.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataLimite.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DataLimite.Properties.ReadOnly = true;
            this.DataLimite.Size = new System.Drawing.Size(166, 26);
            this.DataLimite.TabIndex = 1;
            this.DataLimite.TabStop = false;
            // 
            // grcForn
            // 
            this.grcForn.Controls.Add(this.lblContaCredito);
            this.grcForn.Controls.Add(this.NIT_IMValor);
            this.grcForn.Controls.Add(this.NIT_IM);
            this.grcForn.Controls.Add(this.lookUpFOR1);
            this.grcForn.Controls.Add(this.lookUpFOR2);
            this.grcForn.Dock = System.Windows.Forms.DockStyle.Top;
            this.grcForn.Location = new System.Drawing.Point(0, 226);
            this.grcForn.Name = "grcForn";
            this.grcForn.Size = new System.Drawing.Size(1452, 51);
            this.grcForn.TabIndex = 1;
            this.grcForn.Text = "FORNECEDOR";
            // 
            // lblContaCredito
            // 
            this.lblContaCredito.AutoSize = true;
            this.lblContaCredito.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblContaCredito.ForeColor = System.Drawing.Color.Red;
            this.lblContaCredito.Location = new System.Drawing.Point(884, 26);
            this.lblContaCredito.Name = "lblContaCredito";
            this.lblContaCredito.Size = new System.Drawing.Size(333, 13);
            this.lblContaCredito.TabIndex = 34;
            this.lblContaCredito.Text = "Conta Corrente para Cr�dito:   Nenhuma conta cadastrada";
            this.lblContaCredito.Visible = false;
            // 
            // NIT_IMValor
            // 
            this.NIT_IMValor.Location = new System.Drawing.Point(750, 23);
            this.NIT_IMValor.MenuManager = this.BarManager_F;
            this.NIT_IMValor.Name = "NIT_IMValor";
            this.NIT_IMValor.Properties.ReadOnly = true;
            this.NIT_IMValor.Size = new System.Drawing.Size(128, 20);
            this.NIT_IMValor.TabIndex = 19;
            this.NIT_IMValor.TabStop = false;
            this.NIT_IMValor.Visible = false;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.label5);
            this.groupControl3.Controls.Add(this.lookUpEdit1);
            this.groupControl3.Controls.Add(this.comboBoxEdit1);
            this.groupControl3.Controls.Add(this.lookUpEditPLA1);
            this.groupControl3.Controls.Add(this.lookUpEditPLA2);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(0, 277);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1452, 73);
            this.groupControl3.TabIndex = 2;
            this.groupControl3.Text = "Classifica��o";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(694, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Conta:";
            this.label5.Visible = false;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOA_SPL", true));
            this.lookUpEdit1.Enabled = false;
            this.lookUpEdit1.Location = new System.Drawing.Point(5, 46);
            this.lookUpEdit1.MenuManager = this.BarManager_F;
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLCodigo", "C�digo", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLDescricao", "Descri��o", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLValorISS", "ISS (%)", 15, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEdit1.Properties.DataSource = this.SubPLanoPLAnocontasBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "Descritivo";
            this.lookUpEdit1.Properties.PopupWidth = 400;
            this.lookUpEdit1.Properties.ValueMember = "SPL";
            this.lookUpEdit1.Size = new System.Drawing.Size(173, 20);
            this.lookUpEdit1.TabIndex = 18;
            this.lookUpEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit1_ButtonClick);
            // 
            // SubPLanoPLAnocontasBindingSource
            // 
            this.SubPLanoPLAnocontasBindingSource.DataMember = "SubPLano";
            this.SubPLanoPLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOAServico", true));
            this.comboBoxEdit1.Location = new System.Drawing.Point(184, 46);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus)});
            this.comboBoxEdit1.Properties.Validating += new System.ComponentModel.CancelEventHandler(this.comboBoxEdit1_Properties_Validating);
            this.comboBoxEdit1.Size = new System.Drawing.Size(499, 20);
            this.comboBoxEdit1.TabIndex = 3;
            this.comboBoxEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.comboBoxEdit1_ButtonClick);
            // 
            // BotDestrava
            // 
            this.BotDestrava.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotDestrava.Appearance.Options.UseFont = true;
            this.BotDestrava.Location = new System.Drawing.Point(798, 65);
            this.BotDestrava.Name = "BotDestrava";
            this.BotDestrava.Size = new System.Drawing.Size(131, 41);
            this.BotDestrava.TabIndex = 24;
            this.BotDestrava.Text = "Destrava Nota";
            this.BotDestrava.Visible = false;
            this.BotDestrava.Click += new System.EventHandler(this.BotDestrava_Click);
            // 
            // cBotaoIntegrador1
            // 
            this.cBotaoIntegrador1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBotaoIntegrador1.Appearance.Options.UseFont = true;
            this.cBotaoIntegrador1.CaixaAltaGeral = true;
            this.cBotaoIntegrador1.Doc = System.Windows.Forms.DockStyle.None;
            this.cBotaoIntegrador1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cBotaoIntegrador1.Location = new System.Drawing.Point(937, 65);
            this.cBotaoIntegrador1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoIntegrador1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cBotaoIntegrador1.Name = "cBotaoIntegrador1";
            this.cBotaoIntegrador1.Size = new System.Drawing.Size(136, 41);
            this.cBotaoIntegrador1.somenteleitura = false;
            this.cBotaoIntegrador1.TabIndex = 20;
            this.cBotaoIntegrador1.Tipo = Framework.Integrador.TiposIntegra.Periodicos;
            this.cBotaoIntegrador1.Titulo = null;
            this.cBotaoIntegrador1.Visible = false;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.bAutoTeste);
            this.groupControl4.Controls.Add(this.memoEdit1);
            this.groupControl4.Controls.Add(this.checkEdit1);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(0, 350);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1452, 87);
            this.groupControl4.TabIndex = 3;
            this.groupControl4.Text = "Observa��o (uso interno)";
            // 
            // bAutoTeste
            // 
            this.bAutoTeste.Location = new System.Drawing.Point(697, 32);
            this.bAutoTeste.Name = "bAutoTeste";
            this.bAutoTeste.Size = new System.Drawing.Size(199, 23);
            this.bAutoTeste.TabIndex = 42;
            this.bAutoTeste.Text = "Auto teste Limite";
            this.bAutoTeste.UseVisualStyleBackColor = true;
            this.bAutoTeste.Visible = false;
            this.bAutoTeste.Click += new System.EventHandler(this.bAutoTeste_Click);
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOAObs", true));
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(2, 39);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(1448, 46);
            this.memoEdit1.TabIndex = 27;
            this.memoEdit1.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOAProblema", true));
            this.checkEdit1.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkEdit1.Location = new System.Drawing.Point(2, 20);
            this.checkEdit1.MenuManager = this.BarManager_F;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.checkEdit1.Properties.Caption = "Nota com problema";
            this.checkEdit1.Size = new System.Drawing.Size(1448, 19);
            this.checkEdit1.TabIndex = 44;
            // 
            // bAutoTeste1
            // 
            this.bAutoTeste1.Location = new System.Drawing.Point(1097, 136);
            this.bAutoTeste1.Name = "bAutoTeste1";
            this.bAutoTeste1.Size = new System.Drawing.Size(199, 23);
            this.bAutoTeste1.TabIndex = 43;
            this.bAutoTeste1.Text = "Auto teste Cheque";
            this.bAutoTeste1.UseVisualStyleBackColor = true;
            this.bAutoTeste1.Visible = false;
            this.bAutoTeste1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.bAutoTeste1);
            this.groupControl5.Controls.Add(this.pAGamentosGridControl);
            this.groupControl5.Controls.Add(this.PControles);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl5.Location = new System.Drawing.Point(0, 437);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(1452, 345);
            this.groupControl5.TabIndex = 4;
            this.groupControl5.Text = "PAGAMENTOS";
            this.groupControl5.Enter += new System.EventHandler(this.groupControl5_Enter);
            // 
            // PControles
            // 
            this.PControles.Controls.Add(this.BotINSS);
            this.PControles.Controls.Add(this.BotISS);
            this.PControles.Controls.Add(this.BotPis);
            this.PControles.Controls.Add(this.BotIR);
            this.PControles.Controls.Add(this.spinEdit1);
            this.PControles.Controls.Add(this.dateEdit1);
            this.PControles.Controls.Add(label3);
            this.PControles.Controls.Add(label2);
            this.PControles.Controls.Add(this.simpleButton1);
            this.PControles.Dock = System.Windows.Forms.DockStyle.Right;
            this.PControles.Location = new System.Drawing.Point(1220, 20);
            this.PControles.Name = "PControles";
            this.PControles.Size = new System.Drawing.Size(230, 323);
            this.PControles.TabIndex = 7;
            // 
            // BotINSS
            // 
            this.BotINSS.Location = new System.Drawing.Point(102, 144);
            this.BotINSS.Name = "BotINSS";
            this.BotINSS.Size = new System.Drawing.Size(118, 23);
            this.BotINSS.TabIndex = 26;
            this.BotINSS.Text = "INSS - M�o de obra";
            this.BotINSS.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // BotISS
            // 
            this.BotISS.Location = new System.Drawing.Point(9, 144);
            this.BotISS.Name = "BotISS";
            this.BotISS.Size = new System.Drawing.Size(87, 23);
            this.BotISS.TabIndex = 25;
            this.BotISS.Text = "ISSQN";
            this.BotISS.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // BotPis
            // 
            this.BotPis.Location = new System.Drawing.Point(102, 115);
            this.BotPis.Name = "BotPis";
            this.BotPis.Size = new System.Drawing.Size(118, 23);
            this.BotPis.TabIndex = 24;
            this.BotPis.Text = "PIS/COFINS/CSLL";
            this.BotPis.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // BotIR
            // 
            this.BotIR.Location = new System.Drawing.Point(9, 115);
            this.BotIR.Name = "BotIR";
            this.BotIR.Size = new System.Drawing.Size(87, 23);
            this.BotIR.TabIndex = 23;
            this.BotIR.Text = "IR Destacado";
            this.BotIR.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(100, 69);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(120, 20);
            this.spinEdit1.TabIndex = 21;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(100, 39);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(120, 20);
            this.dateEdit1.TabIndex = 20;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(11, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(209, 23);
            this.simpleButton1.TabIndex = 19;
            this.simpleButton1.Text = "Parcelar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.ButImp);
            this.groupControl6.Controls.Add(this.BotRemove);
            this.groupControl6.Controls.Add(this.cmbContaCredito);
            this.groupControl6.Controls.Add(this.chkDepProprioCon);
            this.groupControl6.Controls.Add(this.lookCTL);
            this.groupControl6.Controls.Add(this.BotDestrava);
            this.groupControl6.Controls.Add(this.cmbContaDebito);
            this.groupControl6.Controls.Add(this.cBotaoIntegrador1);
            this.groupControl6.Controls.Add(this.lblContaDebito);
            this.groupControl6.Controls.Add(this.labelDadosDep);
            this.groupControl6.Controls.Add(this.textDadosDep);
            this.groupControl6.Controls.Add(label4);
            this.groupControl6.Controls.Add(this.Antec);
            this.groupControl6.Controls.Add(this.ComboPAGTipoDefault);
            this.groupControl6.Controls.Add(this.ChProc);
            this.groupControl6.Controls.Add(this.comboMalotes1);
            this.groupControl6.Controls.Add(this.DataLimite);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl6.Location = new System.Drawing.Point(0, 35);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(1452, 111);
            this.groupControl6.TabIndex = 5;
            this.groupControl6.Text = "Data Limite para vencimento";
            // 
            // ButImp
            // 
            this.ButImp.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButImp.Appearance.Options.UseFont = true;
            this.ButImp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButImp.ImageOptions.Image")));
            this.ButImp.Location = new System.Drawing.Point(1081, 65);
            this.ButImp.Name = "ButImp";
            this.ButImp.Size = new System.Drawing.Size(139, 41);
            this.ButImp.TabIndex = 52;
            this.ButImp.Text = "Abrir PDF";
            this.ButImp.Visible = false;
            this.ButImp.Click += new System.EventHandler(this.ButImp_Click);
            // 
            // BotRemove
            // 
            this.BotRemove.Location = new System.Drawing.Point(1110, 27);
            this.BotRemove.Name = "BotRemove";
            this.BotRemove.Size = new System.Drawing.Size(186, 23);
            this.BotRemove.TabIndex = 51;
            this.BotRemove.Text = "Remove nota";
            this.BotRemove.Visible = false;
            this.BotRemove.Click += new System.EventHandler(this.BotRemove_Click);
            // 
            // cmbContaCredito
            // 
            this.cmbContaCredito.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOA_CCTCred", true));
            this.cmbContaCredito.Location = new System.Drawing.Point(292, 82);
            this.cmbContaCredito.Name = "cmbContaCredito";
            this.cmbContaCredito.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbContaCredito.Size = new System.Drawing.Size(484, 20);
            this.cmbContaCredito.TabIndex = 50;
            this.cmbContaCredito.Visible = false;
            // 
            // chkDepProprioCon
            // 
            this.chkDepProprioCon.Location = new System.Drawing.Point(100, 82);
            this.chkDepProprioCon.Name = "chkDepProprioCon";
            this.chkDepProprioCon.Properties.Caption = "Conta do pr�prio condom�nio";
            this.chkDepProprioCon.Size = new System.Drawing.Size(165, 19);
            this.chkDepProprioCon.TabIndex = 3;
            this.chkDepProprioCon.TabStop = false;
            this.chkDepProprioCon.CheckedChanged += new System.EventHandler(this.chkDepProprioCon_CheckedChanged);
            // 
            // lookCTL
            // 
            this.lookCTL.Location = new System.Drawing.Point(517, 24);
            this.lookCTL.MenuManager = this.BarManager_F;
            this.lookCTL.Name = "lookCTL";
            this.lookCTL.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookCTL.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookCTL.Properties.DataSource = this.conTasLogicasBindingSource;
            this.lookCTL.Properties.DisplayMember = "CTLTitulo";
            this.lookCTL.Properties.ShowHeader = false;
            this.lookCTL.Properties.ValueMember = "CTL";
            this.lookCTL.Size = new System.Drawing.Size(259, 20);
            this.lookCTL.TabIndex = 49;
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = typeof(FrameworkProc.datasets.dContasLogicas);
            // 
            // cmbContaDebito
            // 
            this.cmbContaDebito.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOA_CCT", true));
            this.cmbContaDebito.Location = new System.Drawing.Point(100, 53);
            this.cmbContaDebito.Name = "cmbContaDebito";
            this.cmbContaDebito.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbContaDebito.Size = new System.Drawing.Size(411, 20);
            this.cmbContaDebito.TabIndex = 2;
            this.cmbContaDebito.Visible = false;
            // 
            // lblContaDebito
            // 
            this.lblContaDebito.AutoSize = true;
            this.lblContaDebito.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContaDebito.Location = new System.Drawing.Point(5, 56);
            this.lblContaDebito.Name = "lblContaDebito";
            this.lblContaDebito.Size = new System.Drawing.Size(83, 13);
            this.lblContaDebito.TabIndex = 47;
            this.lblContaDebito.Text = "Conta D�bito:";
            this.lblContaDebito.Visible = false;
            // 
            // textDadosDep
            // 
            this.textDadosDep.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOADadosPag", true));
            this.textDadosDep.Location = new System.Drawing.Point(100, 82);
            this.textDadosDep.MenuManager = this.BarManager_F;
            this.textDadosDep.Name = "textDadosDep";
            this.textDadosDep.Size = new System.Drawing.Size(676, 20);
            this.textDadosDep.TabIndex = 5;
            this.textDadosDep.Visible = false;
            // 
            // Antec
            // 
            this.Antec.AutoSize = true;
            this.Antec.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Antec.ForeColor = System.Drawing.Color.Red;
            this.Antec.Location = new System.Drawing.Point(971, 27);
            this.Antec.Name = "Antec";
            this.Antec.Size = new System.Drawing.Size(102, 13);
            this.Antec.TabIndex = 42;
            this.Antec.Text = "* Pode antecipar";
            this.Antec.Visible = false;
            // 
            // ComboPAGTipoDefault
            // 
            this.ComboPAGTipoDefault.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dNOtAsBindingSource, "NOADefaultPAGTipo", true));
            this.ComboPAGTipoDefault.Location = new System.Drawing.Point(100, 24);
            this.ComboPAGTipoDefault.MenuManager = this.BarManager_F;
            this.ComboPAGTipoDefault.Name = "ComboPAGTipoDefault";
            this.ComboPAGTipoDefault.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboPAGTipoDefault.Size = new System.Drawing.Size(411, 20);
            this.ComboPAGTipoDefault.TabIndex = 0;
            this.ComboPAGTipoDefault.EditValueChanged += new System.EventHandler(this.ComboPAGTipoDefault_EditValueChanged);
            // 
            // ChProc
            // 
            this.ChProc.Location = new System.Drawing.Point(515, 57);
            this.ChProc.Name = "ChProc";
            this.ChProc.Properties.Caption = "Procura��o";
            this.ChProc.Properties.ReadOnly = true;
            this.ChProc.Size = new System.Drawing.Size(75, 19);
            this.ChProc.TabIndex = 3;
            // 
            // comboMalotes1
            // 
            this.comboMalotes1.Location = new System.Drawing.Point(596, 56);
            this.comboMalotes1.Name = "comboMalotes1";
            this.comboMalotes1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboMalotes1.Properties.ReadOnly = true;
            this.comboMalotes1.Size = new System.Drawing.Size(180, 20);
            this.comboMalotes1.TabIndex = 4;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cNotasCampos
            // 
            this.Autofill = false;
            this.AutoScroll = true;
            this.BindingSourcePrincipal = this.dNOtAsBindingSource;
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.grcForn);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl6);
            this.DesabilitarAllowDBNull = false;
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.Estado = CompontesBasicos.EstadosDosComponentes.JanelasAtivas;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cNotasCampos";
            this.Size = new System.Drawing.Size(1452, 807);
            this.Titulo = "Framework.ComponentesNeon.CamposCondominio - Framework.ComponentesNeon.CamposCond" +
    "ominio - Nota";
            this.cargaInicial += new System.EventHandler(this.cNotasCampos_cargaInicial);
            this.cargaFinal += new System.EventHandler(this.cNotasCampos_cargaFinal);
            this.Load += new System.EventHandler(this.cNotasCampos_Load);
            this.Controls.SetChildIndex(this.groupControl6, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.grcForn, 0);
            this.Controls.SetChildIndex(this.groupControl3, 0);
            this.Controls.SetChildIndex(this.groupControl4, 0);
            this.Controls.SetChildIndex(this.groupControl5, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNOtAsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGamentosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGamentosGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoPag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryImpostos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboFavorecido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCHESTATUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Botoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotaoCheque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoPagFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoPagGuia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOANumeroSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOADataEmissaoDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOADataEmissaoDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOATotalSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOAAguardaNotaCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CNPJFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chPagao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataLimite.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataLimite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcForn)).EndInit();
            this.grcForn.ResumeLayout(false);
            this.grcForn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NIT_IMValor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubPLanoPLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PControles)).EndInit();
            this.PControles.ResumeLayout(false);
            this.PControles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbContaCredito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepProprioCon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookCTL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbContaDebito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDadosDep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboPAGTipoDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChProc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboMalotes1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource dNOtAsBindingSource;
        private System.Windows.Forms.BindingSource pAGamentosBindingSource;
        private DevExpress.XtraGrid.GridControl pAGamentosGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGA_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGDOC;
        private DevExpress.XtraEditors.SpinEdit nOANumeroSpinEdit;
        private DevExpress.XtraEditors.DateEdit nOADataEmissaoDateEdit;
        private DevExpress.XtraEditors.SpinEdit nOATotalSpinEdit;
        private DevExpress.XtraEditors.CheckEdit nOAAguardaNotaCheckEdit;
        private DevExpress.XtraEditors.LookUpEdit lookUpFOR2;
        private System.Windows.Forms.BindingSource fornecedoresBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookUpFOR1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA2;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private DevExpress.XtraEditors.TextEdit CNPJFormat;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl grcForn;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryTipoPag;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.PanelControl PControles;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpUSU;
        private System.Windows.Forms.BindingSource uSUariosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBotao;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit Botoes;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.DateEdit DataLimite;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.CheckEdit ChProc;
        private DevExpress.XtraEditors.ImageComboBoxEdit comboMalotes1;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboPAGTipoDefault;
        private DevExpress.XtraGrid.Columns.GridColumn colCHENumero;
        private DevExpress.XtraEditors.SimpleButton BotIR;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGIMP;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryImpostos;
        private DevExpress.XtraEditors.SimpleButton BotPis;
        private DevExpress.XtraEditors.SimpleButton BotISS;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEFavorecido;
        private System.Windows.Forms.Button bAutoTeste;
        private System.Windows.Forms.Label Antec;
        private System.Windows.Forms.Button bAutoTeste1;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGPermiteAgrupar;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox ComboFavorecido;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryCHESTATUS;
        private DevExpress.XtraEditors.SimpleButton BotINSS;
        private DevExpress.XtraEditors.CheckEdit chPagao;
        private DevExpress.XtraEditors.TextEdit NIT_IMValor;
        private System.Windows.Forms.Label NIT_IM;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.TextEdit textDadosDep;
        private System.Windows.Forms.Label labelDadosDep;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.BindingSource SubPLanoPLAnocontasBindingSource;
        private Framework.Integrador.cBotaoIntegrador cBotaoIntegrador1;
        private DevExpress.XtraGrid.Columns.GridColumn colbotaoCheque;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotaoCheque;
        private System.Windows.Forms.Label lblContaDebito;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbContaDebito;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGAdicional;
        private System.Windows.Forms.Label lblContaCredito;
        private DevExpress.XtraEditors.SimpleButton BotDestrava;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookCTL;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_CTL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_CHE;
        private DevExpress.XtraEditors.CheckEdit chkDepProprioCon;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbContaCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colCHE_CCT;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryTipoPagFull;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryTipoPagGuia;
        private DevExpress.XtraEditors.SimpleButton BotRemove;
        private DevExpress.XtraEditors.SimpleButton ButImp;
    }
}
