/*
MR - 28/04/2014 09:45 13.2.8.34  - Carregamento do combo de tipos de pagamento para PE com os tipos �teis, correcao de uso de enumeracao (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 29/04/2014 15:25            - Atualiza automaticamente conta de credito do fornecedor quando cadastro for alterado (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***)
LH - 22/05/2014 14:00            - Notas s� s�o duplicidade se for dentro de 1 ano
MR - 20/05/2014 13:00 13.2.9.3   - Permite editar o grid de pagamento mesmo para formas de pagamento do tipo eletr�nico, setando e solicitando informa��es adicionais se necess�rio (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (20/05/2014 13:00) ***)
                                   Alterado o looping de parcelamento de pagamento para melhorar o tratamento de casos com linha digit�vel
                                   Criada uma fun��o para solicitar a linha digit�vel com novas consist�ncias para ser utilizada em v�rios pontos
                                   Tratamento de Double-click para alterar campos Adicional (linha digit�vel ou instru��o OP) e PermiteAgrupar (apenas os pagamentos n�o eletr�nicos)
MR - 01/07/2014 19:00 13.2.9.22  - Permite utilizar o leitor e converte para linha digitavel (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (01/07/2014 19:00) ***)
MR - 11/07/2014 10:00 14.1.4.0   - Recalcula dac do codigo de barras na Linha Digit�vel caso tenha alterado vencimento ou valor (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (11/07/2014 10:00) ***)
MR - 30/07/2014 16:00            - Tratamento do novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 05/08/2014 11:00            - Tratamento do tipo de conta do condominio ao verificar se condominio faz pagamento eletronico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (05/08/2014 11:00) ***)
MR - 06/08/2014 13:00            - Retirada a divis�o do valor total alto da nota nas parcelas no caso de pagamentos do tipo eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***)
MR - 08/08/2014 11:00            - Tratamento da divis�o do valor total alto da nota nas parcelas no caso de cheques de acordo com a configura��o do campo CONDivideCheque do condominio (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (08/08/2014 11:00) ***)
LH - 07/10/2014       14.1.4.60  - IR pessoa f�sica
LH - 17/12/2014       14.1.4.101 - Nota provis�rias
MR - 27/04/2015 12:00            - Pagamento de tributo eletronico com inclusao do "guia de recolhimento - eletr�nico" (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
MR - 20/05/2015 17:00            - Permite que Cr�dito em Conta (PECreditoConta) sejam agrup�vel (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***)
                                 - N�o deixa alterar o cheque quando � do tipo Guia ou Guia Eletr�nica (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***)
MR - 10/06/2015 08:20            - PECreditoConta entre contas do pr�prio condominio (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***)
                                 - Consist�ncia dos dados do Fornecedor para PECreditoConta de fornecedor
MR - 15/06/2015 11:30            - Pagamento de tributo eletronico n�o usa "guia de recolhimento - eletr�nico" para INSSpfRet, INSSpfEmp ou INSSpf que ser� convertido na SEFIP Folha (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (15/06/2015 11:30) ***)
MR - 08/07/2015 09:00            - Tratamento da informa��o adicional de pagamento de transf. entre contas do condominio quando inclui pagamento na m�o direto na lista (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (08/07/2015 09:00) ***)
MR - 14/01/2016 12:30            - Descri��o de conta para cr�dito no caso de transfer�ncia indicando corrente ou poupan�a (Altera��es indicadas por *** MRC - INICIO (14/01/2016 12:30) ***)
LH - 17/06/2016 10:18 15.2.9.49  - Travamento das op��es de pagametos.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Cadastros.Sindicos;
using ContaPagarProc;
using dllCheques;
using dllImpostoNeon;
using dllImpostos;
using Framework;
using Framework.objetosNeon;
using FrameworkProc;
using FrameworkProc.datasets;
using System.Threading;
using CompontesBasicosProc;
using CompontesBasicos;
using dllClienteServProc;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using AbstratosNeon;

namespace ContaPagar
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cNotasCampos : Framework.ComponentesNeon.CamposCondominio, IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        #region Construtores

        /// <summary>
        /// Construtor padr�o (N�O REMOVER)
        /// </summary>
        public cNotasCampos()
            : this(null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_EMPTProc"></param>
        public cNotasCampos(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
            else
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("construtor");
            InitializeComponent();
            dNOtAsLocal = new dNOtAs(EMPTProc1);
            dNOtAsLocal.FORNECEDORESTableAdapter.Fill(dNOtAsLocal.FORNECEDORES);
            if (!DesignMode)
            {
                dNOtAsBindingSource.DataSource = dNOtAsLocal;
                TableAdapterPrincipal = dNOtAsLocal.NOtAsTableAdapter;
                //fornecedoresBindingSource.DataSource = Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD;
                fornecedoresBindingSource.DataSource = dNOtAsLocal;
                pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25X(EMPTProc1.Tipo).PLAnocontas;
                SubPLanoPLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25X(EMPTProc1.Tipo);
                uSUariosBindingSource.DataSource = dUSUarios.dUSUariosStTodosX(EMPTProc1.Tipo);
                cCompet1.CompMaxima = new Competencia(DateTime.Today.AddMonths(3));
                Enumeracoes.VirEnumTiposMalote.CarregaEditorDaGrid(comboMalotes1);
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("FIM construtor");
            if (Framework.DSCentral.USU == 30)
                BotRemove.Visible = true;            
        }

        /// <summary>
        /// Construtor que abre a tela
        /// </summary>
        /// <param name="NOA"></param>
        /// <param name="_Titulo"></param>
        /// <param name="_EMPTProc"></param>
        public cNotasCampos(int NOA, string _Titulo, EMPTProc _EMPTProc = null)
            : this(_EMPTProc)
        {
            pk = NOA;
            Titulo = _Titulo;
            VirShowModulo(EstadosDosComponentes.JanelasAtivas);
        }
        #endregion

        private Follow.PagamentoPeriodico PagamentoPeriodicoDestravar;
        private string JustificativaDestrava = null;

        private Nota _Nota;

        public Nota Nota 
        {
            get 
            {
                if (_Nota == null)
                    _Nota = new Nota(LinhaMae,_EMPTProc1);
                return _Nota;
            }
        }

        private void AjustaImposto(ImpostoNeon Impx,dNOtAs.PAGamentosRow PAGrow)
        {
            if ((Impx == null) || (PAGrow == null))
                return;
            Impx.DataNota = LinhaMae.NOADataEmissao;
            if (PAGrow.PAGVencimento != Impx.VencimentoEfetivo)
            {
                if (Cheque.Editavel((CHEStatus)PAGrow.CHEStatus))
                    PAGrow.PAGVencimento = Impx.VencimentoEfetivo;
                else
                    MessageBox.Show("Aten��o altera��o na data de impostos j� pagos ou com pagamento programado");
            }
        }

        internal void Destrava(int? NOANumero, DateTime? NOADataEmissao, Follow.PagamentoPeriodico _PagamentoPeriodicoDestravar,string Justificativa = null)
        {
            if (NOANumero.HasValue)
                LinhaMae.NOANumero = NOANumero.Value;
            if (NOADataEmissao.HasValue)
                LinhaMae.NOADataEmissao = NOADataEmissao.Value;            
            PagamentoPeriodicoDestravar = _PagamentoPeriodicoDestravar;
            JustificativaDestrava = Justificativa;
            foreach (dNOtAs.PAGamentosRow rowPAG in LinhaMae.GetPAGamentosRows())
                if (rowPAG.CHEStatus == (int)CHEStatus.CadastradoBloqueado)                
                    rowPAG.CHEStatus = (int)CHEStatus.Cadastrado;
            AjustaImposto(ImpIR, ImpIRrow);
            AjustaImposto(ImpContr, ImpContrrow);
            AjustaImposto(ImpISS, ImpISSrow);
            AjustaImposto(ImpINSS, ImpINSSrow);
            AjustaImposto(ImpINSSTomador, ImpINSSTomadorrow);
            AjustaImposto(ImpINSSPrestador, ImpINSSPrestadorrow);
            /*
            if (ImpIR != null)
            {
                ImpIR.DataNota = LinhaMae.NOADataEmissao;
                if (ImpIRrow.PAGVencimento != ImpIR.VencimentoEfetivo)
                    ImpIRrow.PAGVencimento = ImpIR.VencimentoEfetivo;
            }
            if (ImpContr != null)
            {
                ImpContr.DataNota = LinhaMae.NOADataEmissao;
                if (ImpContrrow.PAGVencimento != ImpContr.VencimentoEfetivo)
                    ImpContrrow.PAGVencimento = ImpContr.VencimentoEfetivo;
            }
            if (ImpISS != null)
            {
                ImpISS.DataNota = LinhaMae.NOADataEmissao;
                if (ImpISSrow.PAGVencimento != ImpISS.VencimentoEfetivo)
                    ImpISSrow.PAGVencimento = ImpISS.VencimentoEfetivo;
            }
            if (ImpINSS != null)
            {
                ImpINSS.DataNota = LinhaMae.NOADataEmissao;
                if (ImpINSSrow.PAGVencimento != ImpINSS.VencimentoEfetivo)
                    ImpINSSrow.PAGVencimento = ImpINSS.VencimentoEfetivo;
            }
            if (ImpINSSTomador != null)
            {
                ImpINSSTomador.DataNota = LinhaMae.NOADataEmissao;
                if (ImpINSSTomadorrow.PAGVencimento != ImpINSSTomador.VencimentoEfetivo)
                    ImpINSSTomadorrow.PAGVencimento = ImpINSSTomador.VencimentoEfetivo;
            }
            if (ImpINSSPrestador != null)
            {
                ImpINSSPrestador.DataNota = LinhaMae.NOADataEmissao;
                if (ImpINSSPrestadorrow.PAGVencimento != ImpINSSPrestador.VencimentoEfetivo)
                    ImpINSSPrestadorrow.PAGVencimento = ImpINSSPrestador.VencimentoEfetivo;
            }
            */
        }

        /// <summary>
        ///indica que a nota foi cadastrada no banco imediatamene antes da abertura e deve ser apagada no caso de cancelamento 
        /// </summary>
        //public bool ApagarSeCancelar = false;
        private bool SemafaroFOR = true;
        private bool BlocCNPJFormat = true;
        private bool SemafaroDefaultPAGTipo;
        //*** MRC - INICIO - PAG-FOR (20/05/2014 13:00) ***
        //private bool SemaforoDefaultPAGTipoParcela;
        //*** MRC - TERMINO - PAG-FOR (20/05/2014 13:00) ***
        /// <summary>
        /// InibirVerificaDuplicidadeDeNota
        /// </summary>
        public bool InibirVerificaDuplicidadeDeNota;

        private int NOAAbrir;

        //private bool JaVerificouContratos = false;
        //private System.Collections.ArrayList ContratosTestados;

        private bool VerificaDuplicidadeDeNota()
        {
            
            if (LinhaMae.IsNOA_FRNNull())
                return false;
            string comandoBusca =
"SELECT     NOtAs.NOADATAI, CONDOMINIOS.CONCodigo,NOA\r\n" +
"FROM         NOtAs INNER JOIN\r\n" +
"                      CONDOMINIOS ON NOtAs.NOA_CON = CONDOMINIOS.CON\r\n" +
"WHERE     (NOtAs.NOANumero = @P1) AND (NOtAs.NOA_FRN = @P2) AND (NOtAs.NOA <> @P3) AND (NOtAs.NOADataEmissao > @P4);";
            
            if ((LinhaMae.NOANumero > 2) && (!InibirVerificaDuplicidadeDeNota))
            {
                DataRow Ret = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(comandoBusca, LinhaMae.NOANumero, LinhaMae.NOA_FRN, LinhaMae.NOA, DateTime.Today.AddYears(-1));
                //DataRow Ret = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(comandoBusca, LinhaMae.NOANumero, LinhaMae.NOA_FRN, LinhaMae.NOA);
                if (Ret != null)
                {
                    DateTime DataInc = (DateTime)Ret["NOADATAI"];
                    if (MessageBox.Show(String.Format("Nota j� cadastrada em :{0:dd/MM/yyyy hh:mm}\r\n\r\nNota: {1}\r\n\r\nCondom�nio: {2}\r\n\r\nAbrir Nota?", DataInc, nOANumeroSpinEdit.EditValue, (string)Ret["CONCodigo"]), "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        NOAAbrir = (int)Ret["NOA"];
                        timer1.Enabled = true;
                    }
                    return true;
                };
            }

            string ComandoBusca25 =
"SELECT     NOtAs.NOADATAI,NOA\r\n" +
"FROM         NOtAs\r\n" +
"WHERE     \r\n" +
"(NOtAs.NOA_FRN = @P1) \r\n" +
"AND \r\n" +
"(NOtAs.NOA <> @P2) \r\n" +
"AND \r\n" +
"(NOtAs.NOA_CON = @P3) \r\n" +
"AND \r\n" +
"(NOATotal = @P4) \r\n" +
"AND \r\n" +
"(NOtAs.NOACompet = @P5);";
            if (!LinhaMae.IsNOA_FRNNull())
            {
                object[] Parametros = new object[5];
                Parametros[0] = LinhaMae.NOA_FRN;
                Parametros[1] = LinhaMae.NOA;
                Parametros[2] = LinhaMae.NOA_CON;
                Parametros[3] = LinhaMae.NOATotal;
                Parametros[4] = LinhaMae.NOACompet;
                DataRow Ret = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoBusca25, Parametros);
                if (Ret != null)
                {
                    DateTime DataInc = (DateTime)Ret["NOADATAI"];
                    if (MessageBox.Show(string.Format("ATEN��O\r\nJ� existe uma nota desta compet�ncia cadastrada neste valor em {0}\r\n\r\nAbrir Nota?", DataInc), "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        NOAAbrir = (int)Ret["NOA"];
                        timer1.Enabled = true;
                        //CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludo(typeof(cNotasCampos), "VERIFICAR", CompontesBasicos.TipoDeCarga.pk, (int)Ret["NOA"], false);
                    };
                    return true;
                };
            }
            return false;
        }

        private dNOtAs.NOtAsRow LinhaMae {
            get 
            {
                return (LinhaMae_F == null) ? null : (dNOtAs.NOtAsRow)LinhaMae_F;
            }
        }

        private FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow _rowCON;

        private FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON {
            get 
            {
                if ((_rowCON == null) || (_rowCON.RowState != DataRowState.Unchanged))                
                    _rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(EMPTProc1.Tipo).CONDOMINIOS.FindByCON(LinhaMae.NOA_CON);
                return _rowCON;
            }
        }

        private dNOtAs.FORNECEDORESRow _rowFRN;

        private dNOtAs.FORNECEDORESRow rowFRN
        {
            get
            {                
                if ((LinhaMae == null) || (LinhaMae.IsNOA_FRNNull()))                
                    _rowFRN = null;                
                else                                
                    _rowFRN = dNOtAsLocal.FORNECEDORES.FindByFRN(LinhaMae.NOA_FRN);                
                return _rowFRN;
            }
        }
        
        private bool RetNoVencimento;

        private void AjustaDataLimite() 
        {
            if ((ComboPAGTipoDefault.EditValue == null) || (ComboPAGTipoDefault.EditValue == DBNull.Value))
                return;
            AjustaDataLimite(
                (PAGTipo)ComboPAGTipoDefault.EditValue,
                rowCON.IsCONProcuracaoNull() ? false : rowCON.CONProcuracao,
                (malote.TiposMalote)rowCON.CONMalote,
                DateTime.Now);
        }

        private void AjustaDataLimite(PAGTipo PAGTIPO, bool Procuracao,malote.TiposMalote TipoMalote,DateTime HoraCadastro)
        {
            RetNoVencimento = false;

            if (PAGTIPO == PAGTipo.sindico)
            {
                malote malotelimite = malote.MalotePara(TipoMalote, malote.SentidoMalote.ida, HoraCadastro, 0);
                DataLimite.DateTime = malotelimite.DataMalote.AddDays(1);                                    
            }
            else
            {
                if (Procuracao)
                {
                    if(HoraCadastro.Hour < 13)
                        DataLimite.DateTime = HoraCadastro.Date;
                    else
                        DataLimite.DateTime = HoraCadastro.Date.AddDays(1);
                    if (DataLimite.DateTime.DayOfWeek == DayOfWeek.Monday)
                        DataLimite.DateTime = DataLimite.DateTime.AddDays(-2);
                    else if (DataLimite.DateTime.DayOfWeek == DayOfWeek.Sunday)
                        DataLimite.DateTime = DataLimite.DateTime.AddDays(-1);
                }
                else
                {
                    malote malotelimite = malote.MalotePara(TipoMalote, malote.SentidoMalote.ida, HoraCadastro, 1);


                    if (malotelimite.DataRetorno.Hour > 12)
                    {
                        DataLimite.DateTime = malotelimite.DataRetorno.Date.AddDays(1);
                        RetNoVencimento = true;
                    }
                    else
                        DataLimite.DateTime = malotelimite.DataRetorno.Date;
                    if (DataLimite.DateTime.DayOfWeek == DayOfWeek.Monday)
                        DataLimite.DateTime = DataLimite.DateTime.AddDays(-2);
                };
            };            
            Antec.Visible = RetNoVencimento;
        }

        Cadastros.Condominios.Condominio _condominio;        

        private Cadastros.Condominios.Condominio condominio
        {
            get => _condominio ?? (_condominio = (Cadastros.Condominios.Condominio)ABS_Condominio.GetCondominio(LinhaMae.NOA_CON));
        }

        //private bool? _booPagamentoEletronico;

        private bool booPagamentoEletronico
        {
            get => condominio.Conta.PagamentoEletronico;

            /*{
                
                if (!_booPagamentoEletronico.HasValue)
                {
                    _booPagamentoEletronico = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_bool("SELECT TOP (1) CASE WHEN (CONCodigoComunicacao IS NOT NULL) AND (CONCodigoComunicacao <> '') AND (CCTPrincipal = 1) THEN CCTPagamentoEletronico ELSE 0 END AS PagamentoEletronico FROM CONDOMINIOS INNER JOIN ContaCorrenTe ON CONDOMINIOS.CON = ContaCorrenTe.CCT_CON WHERE (((SELECT CONTipoContaPE FROM CONDOMINIOS WHERE CON = @P1) IN (0,1)) AND (CON = @P1))OR (((SELECT CONTipoContaPE FROM CONDOMINIOS WHERE CON = @P1) IN (2)) AND (CCT_CON = 409)) ORDER BY CCTPrincipal DESC", rowCON.CON);
                    if (!_booPagamentoEletronico.HasValue)
                        _booPagamentoEletronico = false;
                }
                return _booPagamentoEletronico.Value;
            }*/
        }

        private void cNotasCampos_Load(object sender, EventArgs e)
        {            
            try
            {                
                if (rowCON == null)
                {
                    MessageBox.Show("Condom�nio inativo!");
                    FechaTela(DialogResult.Abort);
                    return;
                };
                if (rowCON.IsCONMaloteNull() || rowCON.IsCONProcuracaoNull())
                {
                    MessageBox.Show("Cadastro do condom�nio incompleto!\r\nVerifique o dia do malote e a procura��o");
                    FechaTela(DialogResult.Abort);
                    return;
                };               
                ComboFavorecido.Items.Add(rowCON.CONNome);            
                dSindicos.SindicosRow rowSindico = dSindicos.dSindicosSt.Sindicos.FindByCON(rowCON.CON);             
                if ((rowSindico != null) && (!rowSindico.IsPESNomeNull()))
                    ComboFavorecido.Items.Add(rowSindico.PESNome);             
                comboMalotes1.EditValue = rowCON.CONMalote;              
                ChProc.Checked = rowCON.CONProcuracao;              
                SemafaroFOR = BlocCNPJFormat = false;
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("ponto 1");                
                Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(repositoryTipoPagFull);
                Enumeracoes.VirEnumPAGTipoGUIAS.CarregaEditorDaGrid(repositoryTipoPagGuia);
                if (booPagamentoEletronico)
                {
                   //Todos                   
                   Enumeracoes.VirEnumPAGTipoUtil.CarregaEditorDaGrid(ComboPAGTipoDefault);
                   Enumeracoes.VirEnumPAGTipoUtil.CarregaEditorDaGrid(colPAGTipo);                   
                   
                   //Preenche combo com contas para debito
                   Cadastros.Condominios.PaginasCondominio.dConta dContas = new Cadastros.Condominios.PaginasCondominio.dConta();
                   Cadastros.Condominios.PaginasCondominio.dConta.ContaCorrenTeDataTable dCont = dContas.ContaCorrenTeTableAdapter.GetContaPgEletronicoByCON(rowCON.CON);
                   foreach (DataRow row in dCont.Rows)
                        cmbContaDebito.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(row["DescricaoContaCorrente"].ToString(), row["CCT"], 0));
                    if (dCont.Rows.Count == 1)
                    {
                        //if(!LinhaMae.IsNOA_CCTNull())
                        cmbContaDebito.Enabled = false;
                        if(LinhaMae.IsNOA_CCTNull())
                            LinhaMae.NOA_CCT = dCont[0].CCT;
                        //cmbContaDebito.EditValue = dCont[0].CCT;
                        //cmbContaDebito.Enabled = true;
                    }

                    //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                    //Preenche combo com contas para credito
                    dContas.ContaCorrenTeTableAdapter.Fill(dCont, rowCON.CON);
                   foreach (DataRow row in dCont.Rows)
                   { cmbContaCredito.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(row["DescricaoContaCorrente"].ToString(), row["CCT"], 0)); }
                   //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                }
                else
                {
                   //Sem pagamento eletronico
                   Enumeracoes.VirEnumPAGTipoSemPE.CarregaEditorDaGrid(ComboPAGTipoDefault);
                   Enumeracoes.VirEnumPAGTipoSemPE.CarregaEditorDaGrid(colPAGTipo);
                }
                //*** MRC - FIM - PAG-FOR (2) ***    
                //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                chkDepProprioCon.Checked = !LinhaMae.IsNOA_CCTCredNull();
                //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("ponto 2");
                if (LinhaMae != null)
                {
                    if (!LinhaMae.IsNOA_FRNNull())
                        AjustaCNPJ(LinhaMae.NOA_FRN);
                    if (LinhaMae.RowState == DataRowState.Detached)
                    {
                        LinhaMae.NOAStatus = (int)NOAStatus.Parcial;
                        LinhaMae.NOATipo = (int)NOATipo.Avulsa;
                        LinhaMae.NOAAguardaNota = false;
                        LinhaMae.NOADefaultPAGTipo = (int)PAGTipo.cheque;
                        LinhaMae.NOANumero = 0;
                        LinhaMae.NOAProblema = false;
                    }
                    if (!LinhaMae.IsNOA_PGFNull())
                    {
                        cBotaoIntegrador1.Visible = true;
                        cBotaoIntegrador1.Chave = LinhaMae.NOA_PGF;
                    }
                    ButImp.Visible = ((NOA > 0) && !LinhaMae.IsNOAPDFNull());
                };                
                AjustaDataLimite();               
                SemafaroDefaultPAGTipo = true;            
                dateEdit1.DateTime = DateTime.Today.AddMonths(1);             
                decimal Liquido = 0;
                decimal bruto = 0;
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("ponto 3");
                foreach (dNOtAs.PAGamentosRow rowPAG in LinhaMae.GetPAGamentosRows())
                {
                    if (!rowPAG.IsPAG_CTLNull())
                        lookCTL.EditValue = rowPAG.PAG_CTL;
                    if (!rowPAG.IsCHEStatusNull() && (rowPAG.CHEStatus == (int)CHEStatus.CadastradoBloqueado))
                        BotDestrava.Visible = true;
                    bruto += rowPAG.PAGValor;
                    if (!rowPAG.IsPAGIMPNull())
                        switch ((TipoImposto)rowPAG.PAGIMP)
                        {
                            case TipoImposto.IR:
                            case TipoImposto.IRPF:
                                ImpIR = new ImpostoNeon((TipoImposto)rowPAG.PAGIMP, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);
                                //ImpIR.DataNota = LinhaMae.NOADataEmissao;
                                //ImpIR.DataPagamento = MenorData;
                                //ImpIR.ValorBase = LinhaMae.NOATotal;
                                ImpIRrow = rowPAG;
                                break;
                            case TipoImposto.PIS_COFINS_CSLL:
                                ImpContr = new ImpostoNeon(TipoImposto.PIS_COFINS_CSLL, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);
                                //ImpContr.DataNota = LinhaMae.NOADataEmissao;
                                //ImpContr.DataPagamento = MenorData;
                                //ImpContr.ValorBase = LinhaMae.NOATotal;
                                ImpContrrow = rowPAG;
                                break;
                            case TipoImposto.ISS_SA:
                            case TipoImposto.ISSQN:
                                ImpISS = new ImpostoNeon(Tipocidade, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal, new Competencia(LinhaMae.NOACompet));
                                //ImpISS.DataNota = LinhaMae.NOADataEmissao;
                                //ImpISS.DataPagamento = MenorData;
                                //ImpISS.ValorBase = LinhaMae.NOATotal;
                                //ImpISS.Competencia = new Competencia(LinhaMae.NOACompet);
                                ImpISSrow = rowPAG;
                                break;
                            case TipoImposto.INSS:
                                ImpINSS = new ImpostoNeon(TipoImposto.INSS, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);
                                //ImpINSS.DataNota = LinhaMae.NOADataEmissao;
                                //ImpINSS.DataPagamento = MenorData;
                                //ImpINSS.ValorBase = LinhaMae.NOATotal;
                                ImpINSSrow = rowPAG;
                                break;
                            case TipoImposto.INSSpfEmp:
                                ImpINSSTomador = new ImpostoNeon(TipoImposto.INSSpfEmp, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);
                                //ImpINSSTomador.DataNota = LinhaMae.NOADataEmissao;
                                //ImpINSSTomador.DataPagamento = MenorData;
                                //ImpINSSTomador.ValorBase = LinhaMae.NOATotal;
                                ImpINSSTomadorrow = rowPAG;
                                break;
                            case TipoImposto.INSSpfRet:
                                ImpINSSPrestador = new ImpostoNeon(TipoImposto.INSSpfRet, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);
                                //ImpINSSPrestador.DataNota = LinhaMae.NOADataEmissao;
                                //ImpINSSPrestador.DataPagamento = MenorData;
                                //ImpINSSPrestador.ValorBase = LinhaMae.NOATotal;
                                ImpINSSPrestadorrow = rowPAG;
                                break;
                        }
                    else
                        Liquido += rowPAG.PAGValor;
                };
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("ponto 4");
                nOANumeroSpinEdit.Focus();
                chPagao.Checked = false;            
                if (LinhaMae["NOATotal"] != DBNull.Value)
                    if ((LinhaMae.NOATotal == Liquido) && (Liquido > 0) && (Liquido != bruto))
                        chPagao.Checked = true;           
                ajustaBotoesPFPJ();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("ponto 5");
                if (LinhaMae.NOAStatus == (int)NOAStatus.NotaProvisoria)
                    TravaTela(true); 
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Erro encontrado (1)."), ex);
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("indefinido fim load");
        }
        
        private dNOtAs dNOtAsLocal;        

        /// <summary>
        /// Carrega a linha pk
        /// </summary>
        protected override void FillAutomatico()
        {
            //MessageBox.Show("1");
            if (dNOtAsLocal.NOtAsTableAdapter.FillByNOA(dNOtAsLocal.NOtAs, pk) == 1)
            {
                LinhaMae_F = dNOtAsLocal.NOtAs[0];
                //MessageBox.Show(string.Format("pk = {0}",pk));
                dNOtAsLocal.PAGamentosTableAdapter.FillByNOA(dNOtAsLocal.PAGamentos, pk);
                //MessageBox.Show(string.Format("ok"));
            }
            else
                dNOtAsLocal.PAGamentos.Clear();
            //dNOtAsLocal.PaGamentosFixosTableAdapter.Fill(dNOtAsLocal.PaGamentosFixos, rowCON.CON);
            Ativado = true;            
        }

     

        private string Justificativa = "";

        private bool SemPendencias()
        {
            dNOtAsBindingSource.EndEdit();
            if (!Validate())
                return false;
            bool PrecisaDeConta = false;
            foreach (dNOtAs.PAGamentosRow rowPAG in dNOtAsLocal.PAGamentos)
            {
                if (rowPAG.RowState == DataRowState.Deleted)
                    continue;
                if (rowPAG.IsCHEStatusNull() || (((CHEStatus)rowPAG.CHEStatus).EstaNoGrupo(CHEStatus.Cadastrado, CHEStatus.CadastradoBloqueado)))
                    if (((PAGTipo)rowPAG.PAGTipo).EstaNoGrupo(PAGTipo.PECreditoConta, PAGTipo.PEOrdemPagamento, PAGTipo.PETituloBancario, PAGTipo.PETituloBancarioAgrupavel, PAGTipo.PETransferencia))
                        PrecisaDeConta = true;
                
            }
            //if (cmbContaDebito.Visible && cmbContaDebito.Text == "")
            if (PrecisaDeConta && LinhaMae.IsNOA_CCTNull())
            {
                lblContaDebito.Visible = cmbContaDebito.Visible = true;
                cmbContaDebito.Focus();
                MessageBox.Show("Informe uma Conta para D�bito");
                return false;
            }

            if (cmbContaCredito.Visible && cmbContaCredito.Text == "")
            {
                cmbContaCredito.Focus();
                MessageBox.Show("Informe uma Conta para Cr�dito, obrigat�rio para a forma de pagamento selecionada com cr�dito em conta do pr�prio condom�nio");
                return false;
            }
            if (cmbContaCredito.Visible && (cmbContaCredito.EditValue.Equals(cmbContaDebito.EditValue)))
            {
                cmbContaCredito.Focus();
                MessageBox.Show("As Contas para D�bito e Cr�dito do mesmo condom�nio n�o podem ser iguais");
                return false;
            };
            PAGTipo Tipo = (PAGTipo)ComboPAGTipoDefault.EditValue;
            if ((Tipo == PAGTipo.PECreditoConta || Tipo == PAGTipo.PETransferencia))
            {
                if (grcForn.Enabled)
                {
                    if (lookUpFOR1.Text == "")
                    {
                        lookUpFOR1.Focus();
                        MessageBox.Show("Informe o Fornecedor, obrigat�rio para a forma de pagamento selecionada");
                        return false;
                    }
                    if (rowFRN.IsFRNCnpjNull() || (new DocBacarios.CPFCNPJ(rowFRN.FRNCnpj).Tipo == DocBacarios.TipoCpfCnpj.INVALIDO))
                    {
                        lookUpFOR1.Focus();
                        MessageBox.Show("Fornecedor sem CPF/CNPJ v�lido");
                        return false;
                    }
                    if (lblContaCredito.ForeColor == Color.Red)
                    {
                        lookUpFOR1.Focus();
                        MessageBox.Show("Conta para Cr�dito n�o cadastrada ou incompleta para o Fornecedor, obrigat�ria para a forma de pagamento selecionada. Atualize o cadastro do Fornecedor");
                        return false;
                    }
                }
            };
            if (!CamposObrigatoriosOk(dNOtAsBindingSource))
            {
                MessageBox.Show("Campo obrigat�rio");
                foreach (DevExpress.XtraEditors.BaseEdit c in Controlesobrigatorios)
                    if (c.ErrorText != "")
                    {
                        c.Focus();
                        break;
                    }
                return false;
            };
            dNOtAsBindingSource.EndEdit();
            if (!Validate())
                return false;

            if (VerificaDuplicidadeDeNota())
                if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.VerificaFuncionalidade("DUPLICIDADE") <= 0)
                    return false;
            return true;
        }

        //private bool UsarServidorDeProcessos = true;        
        private int TentavasRecuperar = 10;

        private RetornoServidorProcessos Retorno = null;
        private SPeriodicoNota.PeriodicoNotaClient PeriodicoNotaClient1;
        private int? PGFPagamentoPeriodicoDestravar = null;               

        private void ThreadChamadaAoServidor()
        {
            try
            {
                Retorno = PeriodicoNotaClient1.dNOtAs_GravaNoBanco(VirCrip.VirCripWS.Crip(Framework.DSCentral.ChaveCriptografar(EMPTProc1.EMP,EMPTProc1.USU)),
                                                                   ClienteServProc.RemoveTimezoneDataSet(dNOtAsLocal), 
                                                                   Justificativa, 
                                                                   PGFPagamentoPeriodicoDestravar, 
                                                                   JustificativaDestrava);                
            }
            catch (Exception e)
            {
                Retorno = new RetornoServidorProcessos("Erro interno na chamada", string.Format("Erro interno na chamada:\r\n{0}" , e.Message));
                Retorno.ok = false;
                Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;                
            }
        }

        /*
        [Obsolete("unificar RH Notas")]
        public static bool AmbienteDesenvolvimento = false;

        [Obsolete("unificar RH Notas")]
        private static TiposServidorProc _TipoServidoSelecionado = TiposServidorProc.Indefinido;

        [Obsolete("unificar RH Notas")]
        private static TiposServidorProc TipoServidoSelecionado
        {
            get 
            {
                if (_TipoServidoSelecionado == TiposServidorProc.Indefinido)
                {
                    if (!AmbienteDesenvolvimento)
                        _TipoServidoSelecionado = FormPrincipalBase.strEmProducao ? TiposServidorProc.Producao : TiposServidorProc.QAS;
                    else
                    {
                        System.Collections.SortedList Alternativas = new SortedList();
                        Alternativas.Add(TiposServidorProc.Simulador, "Simulador");
                        Alternativas.Add(TiposServidorProc.Local, "Seridor local");
                        Alternativas.Add(TiposServidorProc.QAS, "Servidor QAS");
                        Alternativas.Add(TiposServidorProc.Producao, "(cuidado) Servidor Produ��o");
                        Alternativas.Add(TiposServidorProc.SemServidor, "Processo antigo");
                        object oSelecionado;
                        VirInput.Input.Execute("Tipo Sevidor de processos", Alternativas, out oSelecionado, VirInput.Formularios.cRadioCh.TipoSelecao.radio);
                        _TipoServidoSelecionado = (TiposServidorProc)oSelecionado;
                    }
                }
                return _TipoServidoSelecionado;
            }
        }

        /*
        [Obsolete("unificar RH Notas")]
        private enum TiposServidorProc
        {
            SemServidor = -1,
            Indefinido = 0,
            Simulador = 1,
            Local = 2,
            QAS = 3,
            Producao = 4
        }*/

        private string URLServidorProcessos
        {
            get
            {
                switch (ClienteServProc.TipoServidoSelecionado)
                {
                    case TiposServidorProc.Local:
                        return "WCFPeriodicoNotaL";
                    case TiposServidorProc.QAS:
                        return "WCFPeriodicoNotaH";
                    case TiposServidorProc.Producao:
                        return "WCFPeriodicoNotaP";
                    case TiposServidorProc.Indefinido:
                    case TiposServidorProc.Simulador:
                    default:
                        return "";
                }
            }
        }

        
        

        /// <summary>
        /// Update
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            
            
            if (!SemPendencias())
                return false;
                                     
            
            //if (VerificaContratos())
            //    return false;
            gridView1.CloseEditor();            
            pAGamentosBindingSource.EndEdit();
            
            decimal Total = 0;
            bool Quitada = true;
            bool Bloqueada = false;
            int Parcela = 1;
            dNOtAs.PAGamentosRow[] PAGs = LinhaMae.GetPAGamentosRows();
            int NParcelas = 0;

            System.Collections.SortedList ParaNumerar = new System.Collections.SortedList();
            int segundodiferencial = 1;
            foreach (dNOtAs.PAGamentosRow rowPAG in PAGs){
                ParaNumerar.Add(rowPAG.PAGVencimento.AddSeconds(segundodiferencial++), rowPAG);
                bool Cancelado = ((!rowPAG.IsPAG_CHENull()) && ((CHEStatus)rowPAG.CHEStatus == CHEStatus.Cancelado));
                
                if (rowPAG.IsPAGIMPNull() && (!Cancelado))                                    
                    NParcelas++;
                //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                //Forca "PAGPermiteAgrupar = false" para os casos de pagamento eletronico ainda cadastrados (por garantia), do tipo nao agrupaveis
                PAGTipo PAGTipo1 = (PAGTipo)rowPAG.PAGTipo;                
                if ((CHEStatus)rowPAG.CHEStatus == CHEStatus.Cadastrado
                   && ((PAGTipo1 == PAGTipo.PETransferencia) || (PAGTipo1 == PAGTipo.PEOrdemPagamento) || (PAGTipo1 == PAGTipo.PETituloBancario)))
                   rowPAG.PAGPermiteAgrupar = false;
                //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
            };
            
            
            foreach (dNOtAs.PAGamentosRow rowPAG in ParaNumerar.Values)
            {
                bool Cancelado = false;
                if(!rowPAG.IsPAG_CHENull())
                {
                    CHEStatus CheStatus = (CHEStatus)rowPAG.CHEStatus;
                    if(CheStatus == CHEStatus.Cancelado || CheStatus == CHEStatus.PagamentoCancelado)
                        Cancelado = true;                
                }
                if (Cancelado)
                    continue;
                if (rowPAG.PAGValor < 0) 
                {
                    MessageBox.Show("Parcela com valor negativo!!");
                    return false;
                };
                if (rowPAG.IsPAGDOCNull())
                    Quitada = false;
                if (rowPAG.CHEStatus == (int)CHEStatus.CadastradoBloqueado)
                    Bloqueada = true;
                bool somaNoTotaldaNota = true;
                if (!rowPAG.IsPAGIMPNull() && chPagao.Checked)
                    somaNoTotaldaNota = false;
                if (!rowPAG.IsPAGIMPNull() && ((TipoImposto)rowPAG.PAGIMP == TipoImposto.INSSpfEmp))
                    somaNoTotaldaNota = false;
                if (somaNoTotaldaNota)
                     Total += rowPAG.PAGValor;
                if (NParcelas > 1)
                {
                    if ((rowPAG.IsPAGIMPNull()) && !rowPAG.IsPAG_CHENull() && Cheque.Editavel((CHEStatus)rowPAG.CHEStatus))
                    {
                        string PAGN = String.Format("{0}/{1}", Parcela++, NParcelas);
                        if (rowPAG.IsPAGNNull() && rowPAG.IsCHENumeroNull())
                            rowPAG.PAGN = PAGN;
                    };
                }
            }
            if (Bloqueada)
                LinhaMae.NOAStatus = (int)NOAStatus.NotaProvisoria;
            else
            {
                if (LinhaMae.NOATotal != Total)
                {
                    LinhaMae.NOAStatus = (int)NOAStatus.Parcial;//parcial
                    MessageBox.Show(String.Format("Os pagamentos n�o conferem com o total da nota!!\r\nNota:{0:n2}\r\nPagamentos:{1:n2}", LinhaMae.NOATotal, Total));
                }
                else                
                    LinhaMae.NOAStatus = (int)(Quitada ? NOAStatus.Liquidada : NOAStatus.Cadastrada);
                
            }
            if (base.Update_F()) {
                SemafaroDefaultPAGTipo = false;
                if (dNOtAsLocal.NOtAs.Count != 1)
                {
                    string mensagem = string.Format("cNotasCampos.cs deveria ter somento uma linha no dNOtAsLocal.NOtAs.Count e tem {0}", dNOtAsLocal.NOtAs.Count);
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", mensagem, "CHECAR ERRO CONCEITUAL");
                    return false;
                }
                TiposServidorProc TipoS = dllClienteServProc.ClienteServProc.TipoServidoSelecionado;
                if (TipoS != TiposServidorProc.SemServidor)
                {                    
                    if(PagamentoPeriodicoDestravar != null)
                        PGFPagamentoPeriodicoDestravar = PagamentoPeriodicoDestravar.PGF;
                    else
                        PGFPagamentoPeriodicoDestravar = null;
                    using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this, true))
                    {                    
                        ESP.Espere("Gravando");
                        ESP.AtivaGauge(30);
                        System.Windows.Forms.Application.DoEvents();                        
                        Thread ThreadPeriodicoNota = null;
                        bool terminado = false;
                        int tentativas = 0;
                        while (!terminado && (tentativas < TentavasRecuperar))
                        {
                            tentativas++;
                            if (ClienteServProc.TipoServidoSelecionado == TiposServidorProc.Simulador)
                            {
                                ESP.Espere("ATEN��O: uso do simulador");                                
                                dNOtAs dNOtAsCopia = (dNOtAs)dNOtAsLocal.Copy();
                                ThreadPeriodicoNota = new Thread(() => Retorno = SimuladNOtAs_GravaNoBanco(EMPTProc1.EMP, EMPTProc1.USU, dNOtAsCopia, Justificativa, PGFPagamentoPeriodicoDestravar, JustificativaDestrava));
                            }
                            else
                            {
                                if (LinhaMae.IsNOA_FRNNull())
                                    dNOtAsLocal.FORNECEDORES.Clear();
                                else
                                    dNOtAsLocal.FORNECEDORESTableAdapter.FillByFRN(dNOtAsLocal.FORNECEDORES, LinhaMae.NOA_FRN);                                   
                                PeriodicoNotaClient1 = new SPeriodicoNota.PeriodicoNotaClient(URLServidorProcessos);                                
                                ThreadPeriodicoNota = new Thread(new ThreadStart(ThreadChamadaAoServidor));
                            }

                            ThreadPeriodicoNota.Start();
                            DateTime proxima = DateTime.Now.AddSeconds(3);
                            while (ThreadPeriodicoNota.IsAlive)
                            {
                                if (DateTime.Now > proxima)
                                {
                                    ESP.Gauge();
                                    proxima = DateTime.Now.AddSeconds(1);
                                }
                                System.Windows.Forms.Application.DoEvents();
                            }
                            if (Retorno.ok)
                            {
                                dNOtAsLocal.AcceptChanges();
                                terminado = true;
                            }
                            else
                            {
                                if (Retorno.TipoDeErro != VirExceptionProc.VirTipoDeErro.recuperavel)
                                    terminado = true;
                                else
                                {
                                    ESP.Espere(string.Format("Gravando (Tentativa {0})", tentativas));
                                    ESP.AtivaGauge(30);
                                    ESP.Gauge(0);
                                }
                            }
                        }
                        if (!Retorno.ok)
                        {
                            if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                            {
                                System.Windows.Forms.Clipboard.SetText(Retorno.Mensagem);
                                System.Windows.Forms.MessageBox.Show(Retorno.Mensagem);
                            }
                            switch(Retorno.TipoDeErro)
                            {
                                case VirExceptionProc.VirTipoDeErro.concorrencia:
                                    MessageBox.Show("Os dados que deveriam ser gravados foram alterados por outro usu�rio.\r\n\r\nDADOS N�O GRAVADOS!!","ATEN��O");
                                    return true;
                                case VirExceptionProc.VirTipoDeErro.efetivo_bug:                            
                                   MessageBox.Show("Dados n�o gravados!! Erro reportado");
                                   VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", Retorno.Mensagem,"ERRO NO SERVIDOR DE PROCESSOS");
                                break;                            
                                case VirExceptionProc.VirTipoDeErro.local:
                                   MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados n�o gravados!!", Retorno.MensagemSimplificada));
                                   return true;
                                case VirExceptionProc.VirTipoDeErro.recuperavel:
                                    MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados n�o gravados!!",Retorno.MensagemSimplificada));                                    
                                break;                                                                
                            }
                            return false;
                        }
                    }
                }
                else
                    if (!dNOtAsLocal.GravaNoBanco(Justificativa, PagamentoPeriodicoDestravar, JustificativaDestrava))
                        return false;                
                if (GradeChamadora != null)
                {
                    NotasGrade Chamador = (NotasGrade)GradeChamadora;
                    Chamador.RefreshDados(LinhaMae.NOA_CON);
                }
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// Simula o Servidor de processos para teste
        /// </summary>
        /// <param name="EMP"></param>
        /// <param name="USU"></param>
        /// <param name="DNOtAs"></param>
        /// <param name="Justificativa"></param>
        /// <param name="PGF"></param>
        /// <param name="JustificativaDestrava"></param>
        /// <returns></returns>
        internal RetornoServidorProcessos SimuladNOtAs_GravaNoBanco(int EMP, int USU, dNOtAs DNOtAs, string Justificativa, int? PGF, string JustificativaDestrava)
        {
            RetornoServidorProcessos Retorno = new RetornoServidorProcessos("OK","OK");
            try
            {
                FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(EMP == Framework.DSCentral.EMP ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial, USU);
                VirMSSQL.TableAdapter.Servidor_De_Processos = true;                
                //na simula��o � sempre local
                //FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Local , USU);
                PagamentoPeriodicoProc PagamentoPeriodicoDestravar = null;
                if (PGF.HasValue)
                    PagamentoPeriodicoDestravar = new PagamentoPeriodicoProc(PGF.Value, false, EMPTProc1);
                DNOtAs.EMPTProc1 = EMPTProc1;
                if (!DNOtAs.GravaNoBanco(Justificativa, PagamentoPeriodicoDestravar, JustificativaDestrava))                                   
                {
                    Retorno.ok = false;
                    Retorno.Mensagem = "Erro ao gravar: Checar o motivo";
                    Retorno.MensagemSimplificada = "Erro ao gravar: Checar o motivo";
                    Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
                }
                return Retorno;
            }
            catch (Exception e)
            {                
                Retorno.ok = false;
                Retorno.Mensagem = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, true, true, true);
                Retorno.Mensagem = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, false, false, false);
                Retorno.TipoDeErro = VirExceptionProc.VirExceptionProc.IdentificaTipoDeErro(e);                
                return Retorno;            
            }
            finally
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = false;
            }
        }        
        
        private void CNPJFormat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (DocBacarios.CPFCNPJ.IsValid(CNPJFormat.Text))
                {
                    CNPJValido();
                }
                else
                {
                    MessageBox.Show("CNPJ inv�lido");
                }
            }
        }

        private DocBacarios.CPFCNPJ cCNPJ;

        private void ajustaBotoesPFPJ() 
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("ajustaBotoesPFPJ");
            NIT_IM.Visible = NIT_IMValor.Visible = BotINSS.Visible = BotIR.Visible = BotPis.Visible = BotISS.Visible = false;
            if ((cCNPJ == null) || (rowFRN == null))
                return;
            BotIR.Visible = true;
            if (cCNPJ.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
            {
                BotISS.Visible = !rowFRN.IsFRNRegistroNull();
                BotPis.Visible = BotINSS.Visible = true;
                if (rowFRN.IsFRNRegistroNull())
                {
                    NIT_IM.Visible = true;
                    NIT_IM.Text = "Inscri��o Municipal n�o cadastrada";
                    NIT_IM.ForeColor = Color.Red;
                }
                else
                {
                    NIT_IM.ForeColor = Color.Black;
                    NIT_IMValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
                    NIT_IM.Text = "I.M.";
                    NIT_IM.Visible = NIT_IMValor.Visible = true;
                    NIT_IMValor.EditValue = rowFRN.FRNRegistro;
                }
            }
            else
                if (cCNPJ.Tipo == DocBacarios.TipoCpfCnpj.CPF)
                {
                    BotISS.Visible = true;
                    BotINSS.Visible = !rowFRN.IsFRNRegistroNull();
                    
                    if (rowFRN.IsFRNRegistroNull())
                    {
                        NIT_IM.Visible = true;
                        NIT_IM.Text = "NIT/PIS/PASEP n�o cadastrado";
                        NIT_IM.ForeColor = Color.Red;
                    }
                    else
                    {
                        NIT_IM.ForeColor = Color.Black;
                        NIT_IMValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                        NIT_IMValor.Properties.DisplayFormat.FormatString = @"000\.00000\.00-0";
                        NIT_IM.Text = "NIT:";
                        NIT_IM.Visible = NIT_IMValor.Visible = true;
                        NIT_IMValor.EditValue = rowFRN.FRNRegistro;
                    }
                }
        }

        private void CNPJValido()
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CNPJ v�lido");
            cCNPJ = new DocBacarios.CPFCNPJ(CNPJFormat.Text);
            bool BlocCNPJFormatAnt = BlocCNPJFormat;
            BlocCNPJFormat = true;
            CNPJFormat.Text = cCNPJ.ToString(true);
            BlocCNPJFormat = BlocCNPJFormatAnt;
            string CNPJAlvo = cCNPJ.ToString(true);
            int? FRN = -1;
            foreach (dNOtAs.FORNECEDORESRow Candidato in dNOtAsLocal.FORNECEDORES)
                if ((!Candidato.IsFRNCnpjNull()) && (Candidato.FRNCnpj == CNPJAlvo))
                {
                    FRN = Candidato.FRN;
                    break;
                }

            if (!FRN.HasValue)
            {
                dNOtAsLocal.FORNECEDORESTableAdapter.Fill(dNOtAsLocal.FORNECEDORES);
                foreach (dNOtAs.FORNECEDORESRow Candidato in dNOtAsLocal.FORNECEDORES)
                    if ((!Candidato.IsFRNCnpjNull()) && (Candidato.FRNCnpj == CNPJAlvo))
                    {
                        FRN = Candidato.FRN;
                        break;
                    }
                _rowFRN = null;
            }
            if (FRN.HasValue)
            {
                SemafaroFOR = true;
                lookUpFOR1.EditValue = lookUpFOR2.EditValue = FRN.Value;
                LinhaMae.NOA_FRN = FRN.Value;
                SemafaroFOR = false;
                nOADataEmissaoDateEdit.Focus();
                //NotaJaCadastrada();
                AjustaCNPJ(FRN.Value);
            }
            else
            {
                if (IncluiFRN(CNPJAlvo))
                    nOADataEmissaoDateEdit.Focus();
            };
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CNPJ v�lido FIM");        
        }

        private void CNPJFormat_TextChanged(object sender, EventArgs e)
        {
            if (BlocCNPJFormat)
                return;
            try
            {
                BlocCNPJFormat = true;
                if ((CNPJFormat.Text.Length > 13) && (DocBacarios.CPFCNPJ.IsValid(CNPJFormat.Text)))
                {
                    CNPJValido();                                                                
                }
                else {
                    if (CNPJFormat.Text.Length == 14)
                        MessageBox.Show("CNPJ inv�lido");
                }
            }
            finally
            {
                BlocCNPJFormat = false;
            };
        }
       
        private void AjustaCNPJ(int FRN) 
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("AjustaCNPJ");
            BlocCNPJFormat = true;
            if (FRN == -1)
                CNPJFormat.Text = "";
            else
            {
                LinhaMae.NOA_FRN = FRN;
                //rowFRN = Fornecedores.FindByFRN(FRN);
                if (rowFRN == null)
                {
                    dNOtAsLocal.FORNECEDORESTableAdapter.Fill(dNOtAsLocal.FORNECEDORES);
                    _rowFRN = null;
                    //Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fill();
                    /*
                    try
                    {
                        Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = false;
                        Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Sigla = "ADV";
                        Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fill();
                    }
                    finally
                    {
                        Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Sigla = "FSR";
                        Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = true;
                    }*/
                    //rowFRN = Fornecedores.FindByFRN(FRN);
                    if (rowFRN == null)
                        throw new Exception(string.Format("Fornecedor n�o encontrado : {0}",FRN));
                }
                CNPJFormat.Text = (rowFRN.IsFRNCnpjNull()) ? "" : rowFRN.FRNCnpj;
                cCNPJ = new DocBacarios.CPFCNPJ(CNPJFormat.Text);
                ajustaBotoesPFPJ();
            };
            BlocCNPJFormat = false;
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("AjustaCNPJ FIM");
        }

        private void lookUpFOR2_EditValueChanged(object sender, EventArgs e)
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("lookUpFOR2_EditValueChanged");
            if (!SemafaroFOR) 
            {                
                int FRN = -1;
                SemafaroFOR = true;
                if (sender == lookUpFOR1)
                {
                    if ((lookUpFOR1.EditValue == null) || (lookUpFOR1.EditValue == DBNull.Value))
                        return;
                    lookUpFOR2.EditValue = lookUpFOR1.EditValue;                    
                    FRN = (int)lookUpFOR1.EditValue;
                }
                if (sender == lookUpFOR2)
                {
                    if ((lookUpFOR2.EditValue == null) || (lookUpFOR2.EditValue == DBNull.Value))
                        return;
                    lookUpFOR1.EditValue = lookUpFOR2.EditValue;
                    FRN = (int)lookUpFOR2.EditValue;
                }
                
                AjustaCNPJ(FRN);
                //NotaJaCadastrada();

                //*** MRC - INICIO - PAG-FOR (2) ***
                lblContaCredito.Visible = false;
                if (ComboPAGTipoDefault.Text != "" && lookUpFOR1.ItemIndex != -1)
                {
                   //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
                   PAGTipo Tipo = (PAGTipo)ComboPAGTipoDefault.EditValue;
                   if (Tipo == PAGTipo.PECreditoConta || Tipo == PAGTipo.PETransferencia)
                   //*** MRC - TERMINO - PAG-FOR (28/04/2014 09:45) ***
                   {
                      lblContaCredito.Visible = true;
                      //Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESDataTable Forn = new Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESDataTable();
                      //Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESTableAdapter.Fill(Forn, (int)lookUpFOR1.EditValue);
                      string strContaCredito = string.Format("Banco {0} Ag�ncia {1}-{2} Conta {3}-{4}", rowFRN.IsFRNBancoCreditoNull() ? "" : rowFRN.FRNBancoCredito, rowFRN.IsFRNAgenciaCreditoNull() ? "" : rowFRN.FRNAgenciaCredito, rowFRN.IsFRNAgenciaDgCreditoNull() ? "" : rowFRN.FRNAgenciaDgCredito, rowFRN.IsFRNContaCreditoNull() ? "" : rowFRN.FRNContaCredito, rowFRN.IsFRNContaDgCreditoNull() ? "" : rowFRN.FRNContaDgCredito);
                      //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                      if (rowFRN.IsFRNBancoCreditoNull() || rowFRN.IsFRNAgenciaCreditoNull() || rowFRN.IsFRNAgenciaDgCreditoNull() || rowFRN.IsFRNContaCreditoNull() || rowFRN.IsFRNContaDgCreditoNull() ||
                          rowFRN.FRNBancoCredito.Trim() == "" || rowFRN.FRNAgenciaCredito.Trim() == "" || rowFRN.FRNAgenciaDgCredito.Trim() == "" || rowFRN.FRNContaCredito.Trim() == "" || rowFRN.FRNContaDgCredito.Trim() == "")
                      {
                          //*** MRC - INICIO (14/01/2016 12:30) ***
                         lblContaCredito.Text = "Conta para Cr�dito n�o cadastrada ou incompleta";
                         //*** MRC - TERMINO (14/01/2016 12:30) ***
                         lblContaCredito.ForeColor = System.Drawing.Color.Red;
                      }
                      //*** MRC - TERMINO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                      else
                      {
                         //*** MRC - INICIO (14/01/2016 12:30) ***
                         lblContaCredito.Text = string.Format("Conta {0} para Cr�dito:   {1}", (rowFRN.FRNContaTipoCredito == (int)FRNContaTipoCredito.ContaCorrete) ? "Corrente" : "Poupan�a", strContaCredito);
                         //*** MRC - TERMINO (14/01/2016 12:30) ***
                         lblContaCredito.ForeColor = System.Drawing.Color.Blue;
                      }
                   }
                }
                //*** MRC - FIM - PAG-FOR (2) ***

                SemafaroFOR = false;
            }
        }

        bool SemafaroPLA;

        private Framework.datasets.dSTRs _dSTRs;

        private Framework.datasets.dSTRs dSTRs
        {
            get {
                if (_dSTRs == null) {
                    _dSTRs = new Framework.datasets.dSTRs();
                    _dSTRs.Fill("PLA");
                };
                return _dSTRs;
            }
        }

        private Framework.datasets.dPLAnocontas.PLAnocontasRow _PLArow;
        private string _PLA;

        private Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow
        {
            set { 
                _PLArow = value;
                if (_PLArow != null)
                    _PLA = _PLArow.PLA;
            }
            get 
            {                
                if ((_PLArow != null) && (_PLArow.RowState == DataRowState.Detached))
                    _PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas.FindByPLA(_PLA);
                return _PLArow; 
            }
        }

        private void lookUpEditPLA2_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroPLA)
            {
                try
                {
                    SemafaroPLA = true;
                    if (sender == lookUpEditPLA1)
                        lookUpEditPLA2.EditValue = lookUpEditPLA1.EditValue;
                    if (sender == lookUpEditPLA2)
                        lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue;
                    if ((lookUpEditPLA2.EditValue == null) || (lookUpEditPLA2.EditValue == DBNull.Value))
                    {
                        lookUpEdit1.Enabled = false;
                        _PLArow = null;
                    }
                    else
                    {
                        string PLA = lookUpEditPLA2.EditValue.ToString();
                        PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas.FindByPLA(PLA);   
                        if(PLArow == null)
                            PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(PLA);   
                        SubPLanoPLAnocontasBindingSource.Filter = string.Format("SPL_PLA = {0}", PLA);
                        lookUpEdit1.Enabled = true;
                    }                    
                    CarregaLista();
                }
                finally
                {
                    SemafaroPLA = false;
                }
            }
        }

        private void CarregaLista() {            
            comboBoxEdit1.Properties.Items.Clear();
            foreach (Framework.datasets.dSTRs.STRsRow rowSTR in dSTRs.STRs)
            {
                if (rowSTR.STRChave.ToString() == lookUpEditPLA1.EditValue.ToString())
                    comboBoxEdit1.Properties.Items.Add(rowSTR.STRTexto);
            };
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
                dNOtAs.PAGamentosRow rowPAG = (dNOtAs.PAGamentosRow)DRV.Row;
                rowPAG.PAGA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                rowPAG.PAGDATAA = DateTime.Now;
                if (!rowPAG.IsPAGIMPNull())
                {
                    if ((ImpINSSPrestadorrow != null) && (ImpINSSTomadorrow != null) && (ImpINSSTomadorrow.RowState != DataRowState.Unchanged))                                            
                        ImpINSSPrestadorrow.PAGValor = CalculaINSSP(ImpINSSTomadorrow.PAGValor);                    
                    RevisarDataDosImpostos();
                }
            }
            else
                RevisarDataDosImpostos();
        }

        private void RevisarDataDosImpostos() {
            if ((ImpIR != null) && (ImpIR.DataPagamento != MenorData))
            {
                if ((CHEStatus)ImpIRrow.CHEStatus == CHEStatus.Cadastrado)
                {
                    ImpIR.DataPagamento = MenorData;
                    ImpIRrow.PAGVencimento = ImpIR.VencimentoEfetivo;
                }
            };
            if ((ImpContr != null) && (ImpContr.DataPagamento != MenorData))
            {
                if ((CHEStatus)ImpContrrow.CHEStatus == CHEStatus.Cadastrado)
                {
                    ImpContr.DataPagamento = MenorData;
                    ImpContrrow.PAGVencimento = ImpContr.VencimentoEfetivo;
                }
            }
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            dNOtAs.PAGamentosRow rowPAG = (dNOtAs.PAGamentosRow)DRV.Row;
            if (rowFRN != null)
                rowPAG.CHEFavorecido = rowFRN.FRNNome;
            rowPAG.PAGTipo = (int)ComboPAGTipoDefault.EditValue;
            rowPAG.PAGI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
            rowPAG.PAGDATAI = DateTime.Now;
            rowPAG.CHEStatus = (int)CHEStatus.Cadastrado;
            rowPAG.PAGPermiteAgrupar = true;
            if (lookCTL.EditValue != null)
                rowPAG.PAG_CTL = (int)lookCTL.EditValue;
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (08/07/2015 09:00) ***
            if (rowPAG.PAGTipo == (int)PAGTipo.PECreditoConta && !LinhaMae.IsNOA_CCTCredNull())
                rowPAG.PAGAdicional = string.Format("{0} - CCT = {1}", cmbContaCredito.Text, cmbContaCredito.EditValue.ToString());
            //*** MRC - TERMINO - PAGAMENTO ELETRONICO (08/07/2015 09:00) ***
        }

        //private bool Cancelando;

        /// <summary>
        /// Cancela o cadastramento
        /// </summary>
        /// <param name="TesteCanClose"></param>
        /// <returns></returns>
        public override bool Cancela_F(bool TesteCanClose)
        {
            SemafaroDefaultPAGTipo = false;
            //*** MRC - INICIO - PAG-FOR (20/05/2014 13:00) ***
            //SemaforoDefaultPAGTipoParcela = false;
            //*** MRC - TERMINO - PAG-FOR (20/05/2014 13:00) ***
            //Cancelando = true;
            //if ((ApagarSeCancelar) && (MessageBox.Show("Confima o cancelamento ?","Confirma��o",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2) == DialogResult.Yes))
            if ((false) && (MessageBox.Show("Confirma o cancelamento ?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cNotasCampos - 1119",
                                                                           dNOtAsLocal.PAGamentosTableAdapter, 
                                                                           dNOtAsLocal.NOtAsTableAdapter);
                    dNOtAs.PAGamentosRow[] Pagamentos = LinhaMae.GetPAGamentosRows();
                    foreach (dNOtAs.PAGamentosRow PAGrow in Pagamentos)
                    {
                        //RemoveParcelaDoCheque(PAGrow);
                        PAGrow.Delete();
                        Nota.AjustaParcelaNoCheque(PAGrow);
                    }
                    dNOtAsLocal.PAGamentosTableAdapter.Update(dNOtAsLocal.PAGamentos);
                    LinhaMae.Delete();
                    dNOtAsLocal.NOtAsTableAdapter.Update(dNOtAsLocal.NOtAs);
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    FechaTela(DialogResult.Cancel);
                    return true;
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                    return false;
                }
            }
            else
                return base.Cancela_F(TesteCanClose);
        }

        private void comboBoxEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            int nPLA;
            if (int.TryParse(lookUpEditPLA1.EditValue.ToString(), out nPLA))
            {
                switch (e.Button.Kind)
                {
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Plus:
                        dSTRs.Add(nPLA, comboBoxEdit1.Text);
                        CarregaLista();
                        break;
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Minus:
                        dSTRs.Dell(nPLA, comboBoxEdit1.Text);
                        CarregaLista();
                        break;
                }
            };
        }

        private System.Collections.ArrayList StatusEditavel = new System.Collections.ArrayList(new CHEStatus[] { CHEStatus.Cadastrado, CHEStatus.DebitoAutomatico, CHEStatus.Eletronico });

        /*
        Tranferido para o objeto cheque como m�todo est�tico para uso em outros lugares tamb�m
        private bool SolicitaLinhaDigitavel(string strParcela, ref string strPAGAdicional, ref DateTime datVencimento, ref decimal decValor)
        {
           while (true)
           {
              //Solicita linha digit�vel
              if (!VirInput.Input.Execute("Linha Digit�vel (digite apenas os n�meros) - Parcela " + strParcela + " :", ref strPAGAdicional, false, 47))
                 return false;
              if (strPAGAdicional == "")
                 return true;

              //Limpa linha digit�vel
              strPAGAdicional = strPAGAdicional.Replace(".", "").Replace(" ", "");
              
              //Verifica se tem apenas n�meros
              Int64 lngLinhaDigitavel;
              if (!Int64.TryParse(strPAGAdicional.Substring(0, 10), out lngLinhaDigitavel) || !Int64.TryParse(strPAGAdicional.Substring(10, 10), out lngLinhaDigitavel) || !Int64.TryParse(strPAGAdicional.Substring(20, 10), out lngLinhaDigitavel) || !Int64.TryParse(strPAGAdicional.Substring(30, 10), out lngLinhaDigitavel) || !Int64.TryParse(strPAGAdicional.Substring(40), out lngLinhaDigitavel))
                 MessageBox.Show(string.Format("Linha Digit�vel incorreta (n�o num�rica). Favor digitar novamente."));
              else
              {
                 //Confere linha digit�vel
                 if (strPAGAdicional.Length == 44)
                 {
                    //Possivelmente usou leitor com codigo de barras, entao converte em linha digitavel e pede confirmacao
                    strPAGAdicional = BoletoCalc.ConverteCodigoBarrasEmLinhaDigitavel(strPAGAdicional);
                    if (MessageBox.Show(string.Format("Confirma a Linha Digit�vel abaixo ?\n\n{0}", strPAGAdicional.Substring(0, 5) + "." + strPAGAdicional.Substring(5, 5) + " " + strPAGAdicional.Substring(10, 5) + "." + strPAGAdicional.Substring(15, 6) + " " + strPAGAdicional.Substring(21, 5) + "." + strPAGAdicional.Substring(26, 6) + " " + strPAGAdicional.Substring(32, 1) + " " + strPAGAdicional.Substring(33)), "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                       break;
                 }
                 else if (strPAGAdicional.Length < 47)
                    MessageBox.Show(string.Format("Linha Digit�vel incompleta. Favor digitar toda a linha digit�vel do t�tulo."));
                 else if (!BoletoCalc.VerificaLinhaDigitavel(strPAGAdicional))
                    MessageBox.Show(string.Format("Linha Digit�vel incorreta. Favor digitar novamente."));
                 else
                    break;
              }
              //*** MRC - TERMINO - PAG-FOR (01/07/2014 19:00) ***
           }

           //*** MRC - INICIO - PAG-FOR (11/07/2014 10:00) ***
           //Seta vencimento e valor de acordo com os dados da linha digitavel e do grid
           DateTime DataRef = new DateTime(1997, 10, 7);
           TimeSpan Fator = datVencimento.Date.Subtract(DataRef);
           bool booAjustaDACCodigoBarras = false;
           if (Convert.ToInt32(strPAGAdicional.Substring(33, 4)) != 0)
              datVencimento = DataRef.AddDays(Convert.ToInt32(strPAGAdicional.Substring(33, 4)));
           else
           {
              strPAGAdicional = strPAGAdicional.Substring(0,33) + Fator.ToString().Substring(0, 4) + strPAGAdicional.Substring(strPAGAdicional.Length-10,10);
              booAjustaDACCodigoBarras = true;
           }
           if (Convert.ToInt32(strPAGAdicional.Substring(strPAGAdicional.Length - 10, 10)) != 0)
              decValor = (Convert.ToDecimal(strPAGAdicional.Substring(strPAGAdicional.Length - 10, 10)) / 100);
           else
           {
              strPAGAdicional = strPAGAdicional.Substring(0,37) + decValor.ToString("0.00").Replace(",", "").PadLeft(10, '0');
              booAjustaDACCodigoBarras = true;
           }
           if (booAjustaDACCodigoBarras)
              strPAGAdicional = strPAGAdicional.Substring(0, 32) + BoletoCalc.Modulo11_2_9(strPAGAdicional.Substring(0, 4) + strPAGAdicional.Substring(33, 14) + strPAGAdicional.Substring(4, 5) + strPAGAdicional.Substring(10, 10) + strPAGAdicional.Substring(21, 10), false) + strPAGAdicional.Substring(33, 14);
           //*** MRC - Termino - PAG-FOR (11/07/2014 10:00) ***

           //Pede confirmacao para inserir exibindo vencimento e valor
           if (MessageBox.Show(string.Format("Confirma o cadastro do t�tulo abaixo ?\n\nVencimento - {0}\nValor - {1}", datVencimento.ToString("dd/MM/yyyy"), decValor.ToString("0.00")), "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
              return false;
           else
              return true;
        }
        */

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if ((nOATotalSpinEdit.EditValue == null) || (nOATotalSpinEdit.EditValue == DBNull.Value))
               return;
            decimal Valor = (decimal)nOATotalSpinEdit.EditValue;
            decimal Retido = 0;
            bool ParcelaRetencaoPaga = false;
            int Parcelas = 0;

            foreach (dNOtAs.PAGamentosRow rowPAG in LinhaMae.GetPAGamentosRows())
            {
               if (!rowPAG.IsPAG_CHENull() && (CHEStatus)rowPAG.CHEStatus == CHEStatus.Cancelado)
                  continue;
               if (!rowPAG.IsCHEStatusNull() && StatusEditavel.Contains((CHEStatus)rowPAG.CHEStatus) && (rowPAG.IsPAGIMPNull()))
                  rowPAG.Delete();
               else
               {
                  if (rowPAG.IsPAGIMPNull())
                  {
                     Parcelas++;
                     Valor -= (rowPAG.PAGValor + Retido);
                     Retido = 0;
                     ParcelaRetencaoPaga = true;
                  }
                  else
                  {
                     if (!chPagao.Checked && ((TipoImposto)rowPAG.PAGIMP != TipoImposto.INSSpfEmp))
                        Retido += rowPAG.PAGValor;
                  }
               }
            };
            DateTime data = dateEdit1.DateTime;
            if (Valor - Retido <= 0)
               return;
            decimal Ultima = Valor - Retido;

            //*** MRC - INICIO - PAG-FOR (20/05/2014 13:00) ***
            for (int i = Parcelas + 1; i <= spinEdit1.Value; i++, data = data.AddMonths(1))
            {
               //Calcula vencimento e valor da parcela
               DateTime datVencimento = data;
               decimal decValor = 0;
               if (i < spinEdit1.Value)
               {
                  decValor = decimal.Round(Valor / (spinEdit1.Value - Parcelas), 2);

                  if (!ParcelaRetencaoPaga)
                  {
                     ParcelaRetencaoPaga = true;
                     decValor -= Retido;
                  };
                  Ultima -= decValor;
               }
               else
                  decValor = Ultima;

               //Verifica se pede dados adicionais e seta se permite ou nao agrupar
               string strPAGAdicional = "";
               bool booPermiteAgrupar = true;
               int intPAGTipo = (int)ComboPAGTipoDefault.EditValue;
               if (intPAGTipo == (int)PAGTipo.PETituloBancario)
               {
                  //Solicita a linha digitavel                  
                  if (!Cheque.SolicitaLinhaDigitavel(i.ToString(), ref strPAGAdicional, ref datVencimento, ref decValor))
                     return;
                  booPermiteAgrupar = false;
               }
               else if (intPAGTipo == (int)PAGTipo.PEOrdemPagamento)
               {
                  //Solicita instrucao indicando o padrao
                  strPAGAdicional = textDadosDep.Text;
                  if (!VirInput.Input.Execute("Instru��o para Ordem de Pagamento - Parcela " + i.ToString() + " :", ref strPAGAdicional, false, 40))
                     return;
                  booPermiteAgrupar = false;
               }
               //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
               else if (intPAGTipo == (int)PAGTipo.PECreditoConta && !LinhaMae.IsNOA_CCTCredNull())
               {
                    //Seta conta de deposito do proprio condominio
                    strPAGAdicional = string.Format("{0} - CCT = {1}", cmbContaCredito.Text , cmbContaCredito.EditValue.ToString());  
               }
               //*** MRC - TERMINO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
               //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
               else if (intPAGTipo == (int)PAGTipo.PETransferencia)
               {
                   booPermiteAgrupar = false;
               }
               //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***

               //Seta os dados do pagamento
               dNOtAs.PAGamentosRow novaPAG = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
               novaPAG.PAG_NOA = LinhaMae.NOA;
               if (rowFRN != null)
                   novaPAG.CHEFavorecido = rowFRN.FRNNome;
               if (lookCTL.EditValue != null)
                   novaPAG.PAG_CTL = (int)lookCTL.EditValue;
               novaPAG.CHEStatus = (int)CHEStatus.Cadastrado;
               novaPAG.PAGTipo = intPAGTipo;
               if (strPAGAdicional != "")
                  novaPAG.PAGAdicional = strPAGAdicional;
               novaPAG.PAGPermiteAgrupar = booPermiteAgrupar;
               novaPAG.PAGValor = decValor;
               novaPAG.PAGVencimento = datVencimento;
               novaPAG.PAGN = String.Format("{0}/{1}", i, spinEdit1.Value);
               if (novaPAG.PAGN.Length > 5)                                 
                   novaPAG.PAGN = String.Format("{0}", i);
               dNOtAsLocal.PAGamentos.AddPAGamentosRow(novaPAG);                
               //*** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***
               if ((intPAGTipo != (int)PAGTipo.PECreditoConta) && (intPAGTipo != (int)PAGTipo.PEOrdemPagamento) && 
                   (intPAGTipo != (int)PAGTipo.PETituloBancario) && (intPAGTipo != (int)PAGTipo.PETituloBancarioAgrupavel) && 
                   (intPAGTipo != (int)PAGTipo.PETransferencia))
               //*** MRC - TERMINO - PAG-FOR (06/08/2014 13:00) ***
               //*** MRC - INICIO - PAG-FOR (08/08/2014 11:00) ***
               {
                   bool? booDivideCheque = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_bool("SELECT CONDivideCheque FROM CONDOMINIOS WHERE (CON = @P1)", rowCON.CON);
                   if (booDivideCheque.GetValueOrDefault(false))
                      //if (Framework.DSCentral.EMP == 1)
                      dividiremvaloremenores(novaPAG, 4999);
               }
               //*** MRC - TERMINO - PAG-FOR (08/08/2014 11:00) ***
            };
            //*** MRC - TERMINO - PAG-FOR (20/05/2014 13:00) ***
            RevisarDataDosImpostos();
        }

        private void dividiremvaloremenores(dNOtAs.PAGamentosRow Semente, decimal Teto) 
        {
            int parcelas = 1 + (int)Math.Truncate(Semente.PAGValor / Teto);
            if (parcelas > 1)
            {
                decimal Valordividido = Math.Round(Semente.PAGValor / parcelas, 2);
                decimal Saldo = Semente.PAGValor - Valordividido;
                Semente.PAGValor = Valordividido;
                //*** MRC - INICIO - PAG-FOR (2) ***
                //Semente.PermiteAgrupar = false;
                Semente.PAGPermiteAgrupar = false;
                //*** MRC - FIM - PAG-FOR (2) ***
                for (int i = 2; i <= parcelas; i++) 
                {
                    dNOtAs.PAGamentosRow novaPAG = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                    novaPAG.PAG_NOA = Semente.PAG_NOA;
                    if (lookCTL.EditValue != null)
                        novaPAG.PAG_CTL = (int)lookCTL.EditValue;
                    novaPAG.CHEStatus = Semente.CHEStatus;
                    novaPAG.PAGTipo = Semente.PAGTipo;
                    novaPAG.PAGValor = (i == parcelas) ? Saldo : Valordividido;
                    Saldo -= novaPAG.PAGValor;
                    novaPAG.PAGVencimento = Semente.PAGVencimento;
                    novaPAG.PAGN = Semente.PAGN;
                    //*** MRC - INICIO - PAG-FOR (2) ***
                    //novaPAG.PermiteAgrupar = false;
                    novaPAG.PAGPermiteAgrupar = false;
                    //*** MRC - FIM - PAG-FOR (2) ***
                    dNOtAsLocal.PAGamentos.AddPAGamentosRow(novaPAG);
                }
            }
        }

        private bool Deletavel(dNOtAs.PAGamentosRow rowDel)
        {
            if (rowDel.IsCHEStatusNull())
                return false;            
            if (!StatusEditavel.Contains((CHEStatus)rowDel.CHEStatus))
                return false;
            if ((!rowDel.IsPAGIMPNull()) && (rowDel.PAGIMP == (int)TipoImposto.INSSpfEmp) && (ImpINSSPrestador != null))
                return false;
            return true;
        }

        private void Botoes_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {            
            dNOtAs.PAGamentosRow rowDel = (dNOtAs.PAGamentosRow)gridView1.GetFocusedDataRow();
            if (rowDel == null)
                return;            
            if (!Deletavel(rowDel))
                return;
            if ((ImpIRrow != null) && (ImpIRrow == rowDel))
            {
                ImpIRrow = null;
                ImpIR = null;
            };
            if ((ImpContrrow != null) && (ImpContrrow == rowDel))
            {
                ImpContrrow = null;
                ImpContr = null;
            };
            if ((ImpINSSrow != null) && (ImpINSSrow == rowDel))
            {
                ImpINSSrow = null;
                ImpINSS = null;
            };
            if ((ImpISSrow != null) && (ImpISSrow == rowDel))
            {
                ImpISSrow = null;
                ImpISS = null;
            };
            if ((ImpINSSTomadorrow != null) && (ImpINSSTomadorrow == rowDel))
            {
                ImpINSSTomadorrow = null;
                ImpINSSTomador = null;
            };
            if ((ImpINSSPrestadorrow != null) && (ImpINSSPrestadorrow == rowDel))
            {
                ImpINSSPrestadorrow = null;
                ImpINSSPrestador = null;
            };
            if (!rowDel.IsPAGIMPNull())
                rowDel.Delete();
            else
            {
                if (!rowDel.IsPAGxBODNull())
                {
                    string MensagemCancelamento = "Este item n�o pode ser removido porque est� ligado a um cr�dito.";
                    MessageBox.Show(MensagemCancelamento, "Aviso");                        
                }

                else if (LinhaMae.IsNOA_PGFNull())
                {
                    if ((rowDel.IsPAG_CHENull()) || (!EMPTProc1.STTA.EstaCadastrado("select RHS from RHTarefaSubdetalhe where RHS_CHE = @P1", rowDel.PAG_CHE)))
                        rowDel.Delete();
                    else                    
                        MessageBox.Show("Este item n�o pode ser removido porque foi gerado pelo DP.\r\nSe for o caso cancele o cheque.");                    
                }
                else
                {
                    string MensagemCancelamento = "Este item n�o pode ser removido porque est� ligado a um peri�dico.\r\nSer� gerado um cheque cancelado.";

                    if (MessageBox.Show(MensagemCancelamento, "Confirma��o", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                        if ((VirInput.Input.Execute("Justificativa para o cancelamento:", ref Justificativa, true)) && (Justificativa != ""))
                        {
                            rowDel.CHEStatus = (int)CHEStatus.Cancelado;
                            //LinhaMae.NOATotal -= rowDel.PAGValor;
                            if (LinhaMae.IsNOAObsNull())
                                LinhaMae.NOAObs = Justificativa;
                            else
                                LinhaMae.NOAObs += (Environment.NewLine + Justificativa);
                        }
                }
            }
            RevisarDataDosImpostos();
        }

        //private List<dNOtAs.PAGamentosRow> LinhasCancelarCheques;

        private bool IncluiFRN(string CNPJAlvo)
        {
            int FRN = -1;
            System.Collections.SortedList CamposNovos = new System.Collections.SortedList();
            CamposNovos.Add("FRNCnpj", CNPJAlvo);
            CamposNovos.Add("FRNTipo", "FSR");
            FRN = ShowModuloAdd(typeof(Cadastros.Fornecedores.cFornecedoresCampos), CamposNovos);
            if (FRN > 0)
            {
                dNOtAsLocal.FORNECEDORESTableAdapter.Fill(dNOtAsLocal.FORNECEDORES);
                _rowFRN = null;
                if (dNOtAsLocal.FORNECEDORES.FindByFRN(FRN) == null)
                    throw new Exception("Fornecedor n�o encontrado!");
                SemafaroFOR = true;
                lookUpFOR1.EditValue = lookUpFOR2.EditValue = FRN;
                LinhaMae.NOA_FRN = FRN;
                SemafaroFOR = false;
                AjustaCNPJ(FRN);
                return true;
            }
            return false;
        }

        private void lookUpFOR1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
                IncluiFRN("");
        }

        /// <summary>
        /// Deveria estar no componente base
        /// Por falta de tempo foi feito manual
        /// </summary>
        private System.Collections.ArrayList _Controlesobrigatorios;

        /// <summary>
        /// Deveria estar no componente base
        /// Por falta de tempo foi feito manual
        /// </summary>
        private System.Collections.ArrayList Controlesobrigatorios {
            get {
                if (_Controlesobrigatorios == null) {
                    _Controlesobrigatorios = new System.Collections.ArrayList();
                    _Controlesobrigatorios.Add(nOANumeroSpinEdit);
                    _Controlesobrigatorios.Add(nOADataEmissaoDateEdit);
                    _Controlesobrigatorios.Add(nOATotalSpinEdit);
                    _Controlesobrigatorios.Add(lookUpFOR1);
                    _Controlesobrigatorios.Add(lookUpEditPLA1);
                    _Controlesobrigatorios.Add(comboBoxEdit1);
                };
                return _Controlesobrigatorios;
            }
        }

        /*
        private void ReterImpostos() {
            return;
            bool simples = false;
            if(rowFRN != null)
              if (!rowFRN.IsFRNSimplesNull())
                if (rowFRN.FRNSimples)
                    simples = true;
            if (!simples)
            {
                if ((rowFRN.FRNCnpj.Length > 14) && (LinhaMae.NOATotal > 5000M))
                {
                    if (ImpContr == null)
                    {
                        ImpContr = new ImpostoNeon(TipoImposto.PIS_COFINS_CSLL);
                        ImpContr.DataNota = LinhaMae.NOADataEmissao;
                        ImpContr.DataPagamento = MenorData;
                        ImpContr.ValorBase = LinhaMae.NOATotal;
                        ImpContrrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                        ImpContrrow.PAG_NOA = LinhaMae.NOA;
                        ImpContrrow.PAGTipo = (int)PAGTipo.cheque;
                        ImpContrrow.PAGIMP = (int)TipoImposto.PIS_COFINS_CSLL;
                        ImpContrrow.PAGValor = 0;
                        ImpContrrow.PAGVencimento = DateTime.MinValue;
                        dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpContrrow);
                    };

                    ImpContr.DataNota = LinhaMae.NOADataEmissao;
                    ImpContr.DataPagamento = MenorData;
                    ImpContr.ValorBase = LinhaMae.NOATotal;
                    ImpContrrow.PAGValor = ImpContr.ValorImposto;
                    ImpContrrow.PAGVencimento = ImpContr.VencimentoEfetivo;

                }
                else
                {
                    if (ImpContr != null)
                    {
                        ImpContr = null;
                        ImpContrrow.Delete();
                        ImpContrrow = null;

                    };
                };
            };
        }
        */

        private bool blockEnter;

        private void groupControl5_Enter(object sender, EventArgs e)
        {
            if (!blockEnter)
            {
                try
                {
                    blockEnter = true;
                    if (CamposObrigatoriosOk(dNOtAsBindingSource))
                    {                        
                        dNOtAsBindingSource.EndEdit();
                        VerificaDuplicidadeDeNota();
                        //VerificaContratos();
                    }
                    else
                    {
                        MessageBox.Show("Campo obrigat�rio");
                        foreach (DevExpress.XtraEditors.BaseEdit c in Controlesobrigatorios)
                            if (c.ErrorText != "")
                            {
                                c.Focus();
                                break;
                            }
                    }
                }
                finally
                {
                    blockEnter = false;
                }
            }
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            dNOtAs.PAGamentosRow rowPAG = (dNOtAs.PAGamentosRow)((DataRowView)e.Row).Row;            
            if (!rowPAG.IsPAG_CHENull() && !StatusEditavel.Contains((CHEStatus)rowPAG.CHEStatus))
            {
                MessageBox.Show("Cheque j� emitido");
                e.Valid = false;
            }
            if (rowPAG["PAGValor"] != DBNull.Value)
            {
                if (rowPAG.PAGValor != Math.Round(rowPAG.PAGValor, 2))
                    rowPAG.PAGValor = Math.Round(rowPAG.PAGValor, 2);
            }
            else
                rowPAG.PAGValor = 0;
        }
    
        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return;
            dNOtAs.PAGamentosRow linha = ((dNOtAs.PAGamentosRow)DRV.Row);
            if (e.Column == colPAGVencimento)
            {
                if (linha["PAGVencimento"] != DBNull.Value) {
                    if (linha.PAGVencimento < DataLimite.DateTime)
                    {
                        if ((RetNoVencimento) && (linha.PAGVencimento == DataLimite.DateTime.AddDays(-1)))
                            e.Appearance.BackColor = Color.Yellow;
                        else
                        {
                            e.Appearance.BackColor = Color.Red;
                            e.Appearance.ForeColor = Color.White;
                        }
                    };
                }
            }
        }

        //private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        //{
        //    AjustaDataLimite();
        //}

        private void cNotasCampos_cargaInicial(object sender, EventArgs e)
        {            
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(ComboTipo);            
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);            
            ImpostoNeon.VirEnumTipoImposto.CarregaEditorDaGrid(colPAGIMP);            
        }

        private void ComboPAGTipoDefault_EditValueChanged(object sender, EventArgs e)
        {
            if (SemafaroDefaultPAGTipo)
            {
                ComboPAGTipoDefault.DoValidate();
                AjustaDataLimite();
            }
            if ( ComboPAGTipoDefault.Text != "")
            {
                //*** MRC - INICIO - PAG-FOR (2) ***
                //labelDadosDep.Visible = textDadosDep.Visible = ((PAGTipo)ComboPAGTipoDefault.EditValue == PAGTipo.deposito);               
                PAGTipo Tipo = (PAGTipo)ComboPAGTipoDefault.EditValue;

                //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                if (Tipo == PAGTipo.deposito)
                {
                   labelDadosDep.Visible = textDadosDep.Visible = true;
                   chkDepProprioCon.Visible = false;
                   labelDadosDep.Text = "Dep�sito:";
                }
                else if (Tipo == PAGTipo.PEOrdemPagamento)
                {
                   labelDadosDep.Visible = textDadosDep.Visible = true;
                   chkDepProprioCon.Visible = false;
                   labelDadosDep.Text = "Instru��o:";
                }
                else if (Tipo == PAGTipo.PECreditoConta)
                {
                    labelDadosDep.Visible = chkDepProprioCon.Visible = true;
                    textDadosDep.Visible = false;
                    labelDadosDep.Text = "Conta Cr�dito:";
                }
                else
                {
                    labelDadosDep.Visible = textDadosDep.Visible = chkDepProprioCon.Visible = false; 
                }

                if (Tipo.EstaNoGrupo(PAGTipo.PECreditoConta, PAGTipo.PEOrdemPagamento, PAGTipo.PETituloBancario, PAGTipo.PETituloBancarioAgrupavel, PAGTipo.PETransferencia))
                {
                    lblContaDebito.Visible = cmbContaDebito.Visible = true;
                    if (cmbContaDebito.Properties.Items.Count == 1)
                        cmbContaDebito.SelectedIndex = 0;                    
                }
                else
                {
                    cmbContaDebito.SelectedIndex = -1;
                    lblContaDebito.Visible = cmbContaDebito.Visible = false;
                }

                lblContaCredito.Visible = false;
                if (lookUpFOR1.ItemIndex != -1)
                {
                   if (Tipo == PAGTipo.PECreditoConta || Tipo == PAGTipo.PETransferencia)
                   {
                      lblContaCredito.Visible = true;
                      //Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESDataTable Forn = new Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESDataTable();
                      //Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESTableAdapter.Fill(Forn, (int)lookUpFOR1.EditValue);
                      string strContaCredito = string.Format("Banco {0} Ag�ncia {1}-{2} Conta {3}-{4}", rowFRN.IsFRNBancoCreditoNull() ? "" : rowFRN.FRNBancoCredito, rowFRN.IsFRNAgenciaCreditoNull() ? "" : rowFRN.FRNAgenciaCredito, rowFRN.IsFRNAgenciaDgCreditoNull() ? "" : rowFRN.FRNAgenciaDgCredito, rowFRN.IsFRNContaCreditoNull() ? "" : rowFRN.FRNContaCredito, rowFRN.IsFRNContaDgCreditoNull() ? "" : rowFRN.FRNContaDgCredito);
                      //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                      if (rowFRN.IsFRNBancoCreditoNull() || rowFRN.IsFRNAgenciaCreditoNull() || rowFRN.IsFRNAgenciaDgCreditoNull() || rowFRN.IsFRNContaCreditoNull() || rowFRN.IsFRNContaDgCreditoNull() ||
                          rowFRN.FRNBancoCredito.Trim() == "" || rowFRN.FRNAgenciaCredito.Trim() == "" || rowFRN.FRNAgenciaDgCredito.Trim() == "" || rowFRN.FRNContaCredito.Trim() == "" || rowFRN.FRNContaDgCredito.Trim() == "")
                      {
                          //*** MRC - INICIO (14/01/2016 12:30) ***
                          lblContaCredito.Text = "Conta para Cr�dito n�o cadastrada ou incompleta";
                          //*** MRC - TERMINO (14/01/2016 12:30) ***
                          lblContaCredito.ForeColor = System.Drawing.Color.Red;
                      }
                      //*** MRC - TERMINO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                      else
                      {
                          //*** MRC - INICIO (14/01/2016 12:30) ***
                          lblContaCredito.Text = string.Format("Conta {0} para Cr�dito:   {1}", (rowFRN.FRNContaTipoCredito == (int)FRNContaTipoCredito.ContaCorrete) ? "Corrente" : "Poupan�a", strContaCredito);
                          //*** MRC - TERMINO (14/01/2016 12:30) ***
                          lblContaCredito.ForeColor = System.Drawing.Color.Blue;
                      }
                   }
                }
                //*** MRC - FIM - PAG-FOR (2) ***
            }
        }

        private List<DevExpress.XtraGrid.Columns.GridColumn> ColunasEditaveis;

        private void LinhaReadOnly(bool valor)
        {
            if (ColunasEditaveis == null)
            {
                ColunasEditaveis = new List<DevExpress.XtraGrid.Columns.GridColumn>();
                foreach (DevExpress.XtraGrid.Columns.GridColumn coluna in gridView1.Columns)
                    if (!coluna.OptionsColumn.ReadOnly)
                        if (coluna != colbotaoCheque)
                            ColunasEditaveis.Add(coluna);
            }
            foreach (DevExpress.XtraGrid.Columns.GridColumn coluna in ColunasEditaveis)
                coluna.OptionsColumn.ReadOnly = valor;                   
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.FocusedRowHandle);
            if ((DRV == null) || (DRV.Row == null))
            {
                LinhaReadOnly(false);
                gridView1.OptionsBehavior.Editable = true;
                return;
            }
            dNOtAs.PAGamentosRow linha = ((dNOtAs.PAGamentosRow)DRV.Row);
            bool LinhaEditavel = true;
            //*** MRC - INICIO - PAG-FOR (2) ***
            PAGTipo Tipo = (PAGTipo)linha.PAGTipo;
            if (!linha.IsCHEStatusNull())
                LinhaEditavel = (StatusEditavel.Contains((CHEStatus)linha.CHEStatus));
            else
                LinhaEditavel = ((Tipo == PAGTipo.Caixinha)
                              || (Tipo == PAGTipo.GuiaEletronica)
                              || (Tipo == PAGTipo.Acumular));
            
            LinhaReadOnly(!LinhaEditavel);
            if (!linha.IsPAGIMPNull() && linha.PAGIMP == (int)TipoImposto.INSSpfRet)
                colPAGValor.OptionsColumn.ReadOnly = true;
            //*** MRC - FIM - PAG-FOR (2) ***
           
            /*
            if (!linha.IsCHEStatusNull())
                gridView1.OptionsBehavior.Editable = (StatusEditavel.Contains((Framework.CHEStatus)linha.CHEStatus));
            else
                gridView1.OptionsBehavior.Editable = (((PAGTipo)linha.PAGTipo == PAGTipo.Caixinha) 
                                                    || ((PAGTipo)linha.PAGTipo == PAGTipo.GuiaEletronica)
                                                    || ((PAGTipo)linha.PAGTipo == PAGTipo.Acumular));
            */
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (ImpIRrow != null)                    
                  return;
            TipoImposto Tipo;
            if (cCNPJ.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                Tipo = TipoImposto.IR;
            else if (cCNPJ.Tipo == DocBacarios.TipoCpfCnpj.CPF)
                Tipo = TipoImposto.IRPF;
            else
                return;
            decimal IR = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(Tipo) + 0.004999M, 2);
            if (VirInput.Input.Execute("IR Destacado", ref IR,2,true)) {
                IR = Math.Round(IR, 2);
                if (IR <= 0)
                    return;
                ImpIR = new dllImpostoNeon.ImpostoNeon(Tipo, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);
                //ImpIR.DataNota = LinhaMae.NOADataEmissao;
                //ImpIR.DataPagamento = MenorData;
                //ImpIR.ValorBase = LinhaMae.NOATotal;
                //if (IR != ImpIR.ValorImposto)
                //    MessageBox.Show("Valor do IR n�o confere\r\nEsperado: " + ImpIR.ValorImposto.ToString("n2") + "\r\nInformado:" + IR.ToString("n2"), "ATEN��O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ImpIRrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                ImpIRrow.PAG_NOA = LinhaMae.NOA;
                if (lookCTL.EditValue != null)
                    ImpIRrow.PAG_CTL = (int)lookCTL.EditValue;
                //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                if (rowCON.IsCONGPSEletronicoNull() || !rowCON.CONGPSEletronico) 
                    ImpIRrow.PAGTipo = (int)PAGTipo.Guia;
                else
                    ImpIRrow.PAGTipo = (int)PAGTipo.PEGuia;
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                ImpIRrow.PAGIMP = (int)Tipo;
                ImpIRrow.PAGValor = IR;
                ImpIRrow.PAGVencimento = ImpIR.VencimentoEfetivo;
                ImpIRrow.CHEStatus = (int)CHEStatus.Cadastrado;
                ImpIRrow.CHEFavorecido = ImpIR.Nominal();
                ImpIRrow.PAGPermiteAgrupar = true;
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpIRrow);
            }
        }

        private ImpostoNeon ImpIR = null;
        private dNOtAs.PAGamentosRow ImpIRrow = null;
        private ImpostoNeon ImpContr = null;
        private dNOtAs.PAGamentosRow ImpContrrow = null;
        private ImpostoNeon ImpISS = null;
        private dNOtAs.PAGamentosRow ImpISSrow = null;
        private ImpostoNeon ImpINSS = null;
        private dNOtAs.PAGamentosRow ImpINSSrow = null;
        private ImpostoNeon ImpINSSTomador = null;
        private dNOtAs.PAGamentosRow ImpINSSTomadorrow = null;
        private ImpostoNeon ImpINSSPrestador = null;
        private dNOtAs.PAGamentosRow ImpINSSPrestadorrow = null;

        private DateTime MenorData{
            get {
                DateTime Retornar = DateTime.MaxValue;
                foreach (dNOtAs.PAGamentosRow rowPag in LinhaMae.GetPAGamentosRows())
                    if (rowPag.RowState != DataRowState.Deleted)
                        if (rowPag.IsPAGIMPNull())
                            if (rowPag.PAGVencimento < Retornar)
                                 Retornar = rowPag.PAGVencimento;
                if (Retornar == DateTime.MaxValue)
                    return LinhaMae.NOADataEmissao;
                else
                    return Retornar;
            }
        }

        private void nOATotalSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (nOATotalSpinEdit.EditValue != null)
            {
                nOATotalSpinEdit.EditValue = Math.Round((decimal)nOATotalSpinEdit.EditValue, 2);
                //NotaJaCadastrada();
            }
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
                if ((DRV != null) && (DRV.Row != null))
                {
                    dNOtAs.PAGamentosRow Linha = (dNOtAs.PAGamentosRow)DRV.Row;
                    if (Linha.IsCHEStatusNull())
                    {
                        //e.Appearance.BackColor = Color.AliceBlue;
                        e.Appearance.BackColor = Color.Pink;
                        e.Appearance.ForeColor = Color.Black;
                    }
                    else
                        if ((CHEStatus)Linha.CHEStatus == CHEStatus.Cancelado)
                        {
                            e.Appearance.BackColor = Color.Wheat;
                            e.Appearance.ForeColor = Color.Black;
                            e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Strikeout | FontStyle.Italic);
                        }
                        else
                            if ((CHEStatus)Linha.CHEStatus == CHEStatus.PagamentoCancelado)
                            {
                                e.Appearance.BackColor = Color.Silver;
                                e.Appearance.ForeColor = Color.Black;
                                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Strikeout | FontStyle.Italic);
                            }
                            else
                            {
                                if (!Linha.IsPAGIMPNull())
                                {
                                    e.Appearance.BackColor = Color.FromArgb(255, 255, 100);
                                    e.Appearance.ForeColor = Color.FromArgb(100, 100, 100);
                                }
                                else
                                {
                                    e.Appearance.BackColor = Color.FromArgb(100, 255, 255);
                                    e.Appearance.ForeColor = Color.Black;
                                }
                            }
                }
            }
        }

        private void comboBoxEdit1_Properties_Validating(object sender, CancelEventArgs e)
        {
            string Corrigido = "";
            bool primeiro = true;
            for (int i = 0; i < comboBoxEdit1.Text.Length; i++)
            {
                if (primeiro)
                    Corrigido += char.ToUpper(comboBoxEdit1.Text[i]);
                else
                    Corrigido += char.ToLower(comboBoxEdit1.Text[i]);
                if (comboBoxEdit1.Text[i] != ' ')
                    primeiro = false;
            }
            if (comboBoxEdit1.Text != Corrigido)
                comboBoxEdit1.Text = Corrigido;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (ImpContrrow != null)
                return;
            decimal ValCont = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(TipoImposto.PIS_COFINS_CSLL) + 0.004999M, 2);
            if (VirInput.Input.Execute("Val.Destacado", ref ValCont,2,true))
            {
                ValCont = Math.Round(ValCont, 2);
                if (ValCont <= 0)
                    return;
                ImpContr = new ImpostoNeon(TipoImposto.PIS_COFINS_CSLL, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);                
                ImpContrrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                ImpContrrow.PAG_NOA = LinhaMae.NOA;
                if (lookCTL.EditValue != null)
                    ImpContrrow.PAG_CTL = (int)lookCTL.EditValue;
                //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                if (rowCON.IsCONGPSEletronicoNull() || !rowCON.CONGPSEletronico)
                    ImpContrrow.PAGTipo = (int)PAGTipo.Guia;
                else
                    ImpContrrow.PAGTipo = (int)PAGTipo.PEGuia;
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                ImpContrrow.PAGIMP = (int)TipoImposto.PIS_COFINS_CSLL;
                ImpContrrow.PAGValor = ValCont;
                ImpContrrow.PAGVencimento = ImpContr.VencimentoEfetivo;
                ImpContrrow.CHEStatus = (int)CHEStatus.Cadastrado;
                ImpContrrow.CHEFavorecido = ImpContr.Nominal();
                ImpContrrow.PAGPermiteAgrupar = true;
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpContrrow);
            }
        }

        static TipoImposto Tipocidade
        {
            get { return Framework.DSCentral.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA; }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (ImpISS != null)
                return;            

            decimal ValCont = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(Tipocidade) + 0.004999M, 2);
            if (VirInput.Input.Execute("Val.Destacado", ref ValCont,2,true))
            {
                ValCont = Math.Round(ValCont, 2);
                if (ValCont <= 0)
                    return;
                ImpISS = new dllImpostoNeon.ImpostoNeon(Tipocidade, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal, new Competencia(LinhaMae.NOACompet));
                //ImpISS.DataNota = LinhaMae.NOADataEmissao;
                //ImpISS.DataPagamento = MenorData;
                //ImpISS.ValorBase = LinhaMae.NOATotal;
                //ImpISS.Competencia = new Competencia(LinhaMae.NOACompet);
                // if (ValCont != ImpContr.ValorImposto)
                //    MessageBox.Show("Valor destacado n�o confere\r\nEsperado: " + ImpContr.ValorImposto.ToString("n2") + "\r\nInformado:" + ValCont.ToString("n2"), "ATEN��O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ImpISSrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                ImpISSrow.PAG_NOA = LinhaMae.NOA;
                if (lookCTL.EditValue != null)
                    ImpISSrow.PAG_CTL = (int)lookCTL.EditValue;
                ImpISSrow.PAGTipo = (int)PAGTipo.Guia; //N�o faz eletronico porque nao tem convenio (deve ser guia de recolhimento - cheque)
                ImpISSrow.PAGIMP = (int)Tipocidade;
                ImpISSrow.PAGValor = ValCont;
                ImpISSrow.PAGVencimento = ImpISS.VencimentoEfetivo;                
                ImpISSrow.CHEFavorecido = ImpISS.Nominal();
                ImpISSrow.CHEStatus = (int)CHEStatus.Cadastrado;
                ImpISSrow.PAGPermiteAgrupar = true;
                //if (MessageBox.Show("Guia j� emitida?", "Guia", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                //    ImpISSrow.PAGDataGuia = DateTime.Today;
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpISSrow);
            }

        }

        private void bAutoTeste_Click(object sender, EventArgs e)
        {
            malote M1 = null;
            malote M2 = null;
            bool RetornaNaData;
            PAGTipo[] PAGTestar = new PAGTipo[] { PAGTipo.boleto, PAGTipo.sindico };
            memoEdit1.Text  = "************AUTO TESTE************\r\n\r\n";
            memoEdit1.Text += "-------Data Limite-------\r\n";
            for (bool procuracao = false; ; procuracao = true)
            {
                memoEdit1.Text += procuracao ? "\r\nProcuracao:\r\n" : "sem Procuracao:\r\n";
                foreach (PAGTipo PAGTIPO in PAGTestar)
                {
                    memoEdit1.Text += "\r\n    Tipo Pagamento :"+PAGTIPO.ToString();
                    foreach (malote.TiposMalote TipoMalote in Enum.GetValues(typeof(malote.TiposMalote)))
                    {
                        memoEdit1.Text += "\r\n\r\n     Tipo Malote :" + TipoMalote.ToString() + "\r\n";
                        for (DateTime Data = new DateTime(2009, 1, 5); Data < new DateTime(2009, 1, 13); Data = Data.AddDays(1))
                        {
                            foreach (int horas in new int[] { 10, 15 })
                            {
                                DateTime HoraCadastro = Data.AddHours(horas);
                                AjustaDataLimite(PAGTIPO, procuracao, TipoMalote, HoraCadastro);
                                memoEdit1.Text += String.Format("      Cadastro {0:dd/MM:HH(ddd)} -> Limite {1:dd/MM/yyyy (ddd)}{2}\r\n",
                                    HoraCadastro, 
                                    DataLimite.DateTime, 
                                    RetNoVencimento ? " Malote retorna no dia de Venct pela manha" : "");
                                foreach (DateTime DataT in new DateTime[] { DataLimite.DateTime.AddDays(-4), DataLimite.DateTime.AddDays(-3), DataLimite.DateTime.AddDays(-2), DataLimite.DateTime.AddDays(-1), DataLimite.DateTime })
                                {
                                    DateTime[] Datas = Cheque.CalculaDatas(PAGTIPO, procuracao, TipoMalote, DataT,HoraCadastro, ref M1, ref M2,out RetornaNaData);
                                    memoEdit1.Text += String.Format("                 Vencimento {0:dd/MM(ddd)}: Ida ({1}) -> Volta ({2}) {3} {4}\r\n",
                                        DataT,
                                        M1 == null ? Datas[0].ToString("dd/MM:HH(ddd)") : M1.ToString("ss"),
                                        M2 == null ? Datas[1].ToString("dd/MM:HH(ddd)") : M2.ToString("ss"),
                                        RetornaNaData ? "*":" ",
                                        (Datas[0] < HoraCadastro) ? "Falha" : "Ok   "
                                        );
                                }
                            }                            
                        }
                    }
                }
                if (procuracao)
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            PAGTipo[] PAGTestar = new PAGTipo[] { PAGTipo.boleto, PAGTipo.sindico };
            memoEdit1.Text = "************AUTO TESTE************\r\n\r\n";
            memoEdit1.Text += "-------Cheques-------\r\n";
            memoEdit1.Text += String.Format("Data Referencia:{0}\r\n",DateTime.Today);
            malote M1=null;
            malote M2=null;
            bool RetornaNaData;
            for (bool procuracao = false; ; procuracao = true)
            {
                memoEdit1.Text += procuracao ? "\r\nProcuracao:\r\n" : "sem Procuracao:\r\n";
                foreach (PAGTipo PAGTIPO in PAGTestar)
                {
                    memoEdit1.Text += "\r\n    Tipo Pagamento :" + PAGTIPO.ToString();
                    foreach (malote.TiposMalote TipoMalote in Enum.GetValues(typeof(malote.TiposMalote)))
                    {
                        memoEdit1.Text += String.Format("\r\n\r\n     Tipo Malote :{0}\r\n", TipoMalote);
                        for (DateTime Data = DateTime.Today.AddDays(-5); Data < DateTime.Today.AddDays(20); Data = Data.AddDays(1))
                        {
                            DateTime[] Datas = Cheque.CalculaDatas(PAGTIPO, procuracao, TipoMalote, Data, DateTime.Today, ref M1, ref M2, out RetornaNaData);
                            memoEdit1.Text += String.Format("      Vencimento {0:dd/MM(ddd)}: Ida ({1}) -> Volta ({2}) {3}\r\n",
                                Data,
                                M1 == null ? Datas[0].ToString("dd/MM:HH(ddd)") : M1.ToString("ss"),
                                M2 == null ? Datas[1].ToString("dd/MM:HH(ddd)") : M2.ToString("ss"),
                                RetornaNaData ? "*":" ");
                        }
                    }
                }
                if (procuracao)
                    break;
            }
        }

        private void INSSPJ() 
        {
            if (ImpINSSrow != null)
                return;
            decimal INSS = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(TipoImposto.INSS) + 0.004999M, 2);
            if (VirInput.Input.Execute("INSS m�o-de-obra", ref INSS, 2, true))
            {
                INSS = Math.Round(INSS, 2);
                if (INSS <= 0)
                    return;
                ImpINSS = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSS, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);
                //ImpINSS.DataNota = LinhaMae.NOADataEmissao;
                //ImpINSS.DataPagamento = MenorData;
                //ImpINSS.ValorBase = LinhaMae.NOATotal;
                //if (INSS != ImpINSS.ValorImposto)
                //    MessageBox.Show("Valor do INSS n�o confere\r\nEsperado: " + ImpINSS.ValorImposto.ToString("n2") + "\r\nInformado:" + INSS.ToString("n2"), "ATEN��O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ImpINSSrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                ImpINSSrow.PAG_NOA = LinhaMae.NOA;
                if (lookCTL.EditValue != null)
                    ImpINSSrow.PAG_CTL = (int)lookCTL.EditValue;
                //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                if (rowCON.IsCONGPSEletronicoNull() || !rowCON.CONGPSEletronico)
                    ImpINSSrow.PAGTipo = (int)PAGTipo.Guia;
                else
                    ImpINSSrow.PAGTipo = (int)PAGTipo.PEGuia;
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***                                
                ImpINSSrow.PAGIMP = (int)TipoImposto.INSS;
                ImpINSSrow.PAGValor = INSS;
                ImpINSSrow.PAGVencimento = ImpINSS.VencimentoEfetivo;
                ImpINSSrow.CHEStatus = (int)CHEStatus.Cadastrado;
                ImpINSSrow.CHEFavorecido = ImpINSS.Nominal();
                ImpINSSrow.PAGPermiteAgrupar = true;
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpINSSrow);
            }
        }

        private decimal CalculaINSSP(decimal INSST)
        {
            decimal INSSP = Math.Round(INSST / Imposto.AliquotaBase(TipoImposto.INSSpfEmp) * Imposto.AliquotaBase(TipoImposto.INSSpfRet) + 0.004999M, 2);
            decimal Teto = Imposto.Teto(TipoImposto.INSSpfRet);
            if (Teto < INSSP)
                INSSP = Teto;
            return INSSP;
        }

        private bool ChecaSEFIP(Competencia compet)
        {
            Framework.datasets.AGGStatus Status = Framework.datasets.AGGStatus.NaoIniciado;

            if (Framework.datasets.dAGendaGeral.dAGendaGeralSt.GetAGBCompetencia(Framework.datasets.AGGTipo.SEFIP) > compet)
                Status = Framework.datasets.AGGStatus.Terminado;                
            else if (Framework.datasets.dAGendaGeral.dAGendaGeralSt.GetAGBCompetencia(Framework.datasets.AGGTipo.SEFIP) == compet)
                Status = Framework.datasets.dAGendaGeral.dAGendaGeralSt.GetAGBStatus(Framework.datasets.AGGTipo.SEFIP);
            if (Status != Framework.datasets.AGGStatus.NaoIniciado)
                return avisoINSS.Avisar(Status == Framework.datasets.AGGStatus.Terminado);
            else
                return true;
        }

        private void INSSPF()
        {            
            if (ImpINSSTomadorrow == null)
            {
                decimal INSST = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(TipoImposto.INSSpfEmp) + 0.004999M, 2);
                if (VirInput.Input.Execute("INSS tomador", ref INSST, 2, true))
                {
                    INSST = Math.Round(INSST, 2);
                    if (INSST > 0)
                    {
                        ImpINSSTomador = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSSpfEmp, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);
                        if (!ChecaSEFIP(ImpINSSTomador.Competencia))
                            return;
                        ImpINSSTomadorrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                        ImpINSSTomadorrow.PAG_NOA = LinhaMae.NOA;
                        if (lookCTL.EditValue != null)
                            ImpINSSTomadorrow.PAG_CTL = (int)lookCTL.EditValue;
                        ImpINSSTomadorrow.PAGTipo = (int)PAGTipo.Guia;                        
                        ImpINSSTomadorrow.PAGIMP = (int)TipoImposto.INSSpfEmp;
                        ImpINSSTomadorrow.PAGValor = INSST;
                        ImpINSSTomadorrow.PAGVencimento = ImpINSSTomador.VencimentoEfetivo;
                        ImpINSSTomadorrow.CHEStatus = (int)CHEStatus.Cadastrado;
                        ImpINSSTomadorrow.CHEFavorecido = ImpINSSTomador.Nominal();
                        ImpINSSTomadorrow.PAGPermiteAgrupar = true;
                        dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpINSSTomadorrow);
                    }
                }
            }
            if ((ImpINSSTomadorrow != null) && (ImpINSSPrestadorrow == null))
            {
                decimal INSSP = CalculaINSSP(ImpINSSTomadorrow.PAGValor);
                //decimal INSSP = Math.Round(ImpINSSTomadorrow.PAGValor / Imposto.AliquotaBase(TipoImposto.INSSpfEmp) * Imposto.AliquotaBase(TipoImposto.INSSpfRet) + 0.004999M, 2);
                //decimal Teto = Imposto.Teto(TipoImposto.INSSpfRet);
                //if (Teto < INSSP)
                //    INSSP = Teto;
                if (MessageBox.Show(string.Format("Incluir INSS do Prestador ?\r\nValor: {0:n2}", INSSP), "INSS Prestador", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                { 
                


                //if (VirInput.Input.Execute("INSS Prestador", ref INSSP, 2, true))
                //{
                //    if (Teto < INSSP)
                  //  {
                    //    MessageBox.Show(string.Format("ATEN��O: Valor m�ximo do INSS = {0:n2}. Valor corrigido",Teto));
                      //  INSSP = Teto;
                 //   }
                   // else
                     //   INSSP = Math.Round(INSSP, 2);
                //    if (INSSP > 0)
                  //  {
                        ImpINSSPrestador = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSSpfRet, LinhaMae.NOADataEmissao, MenorData, LinhaMae.NOATotal);                        
                        ImpINSSPrestadorrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                        ImpINSSPrestadorrow.PAG_NOA = LinhaMae.NOA;
                        if (lookCTL.EditValue != null)
                            ImpINSSPrestadorrow.PAG_CTL = (int)lookCTL.EditValue;
                        ImpINSSPrestadorrow.PAGTipo = (int)PAGTipo.Guia;                        
                        ImpINSSPrestadorrow.PAGIMP = (int)TipoImposto.INSSpfRet;
                        ImpINSSPrestadorrow.PAGValor = INSSP;
                        ImpINSSPrestadorrow.PAGVencimento = ImpINSSPrestador.VencimentoEfetivo;
                        ImpINSSPrestadorrow.CHEStatus = (int)CHEStatus.Cadastrado;
                        ImpINSSPrestadorrow.CHEFavorecido = ImpINSSPrestador.Nominal();                        
                        ImpINSSPrestadorrow.PAGPermiteAgrupar = true;                        
                        dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpINSSPrestadorrow);
                 //   }
                }
            }
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            switch (cCNPJ.Tipo)
            {
                case DocBacarios.TipoCpfCnpj.CNPJ:
                    INSSPJ();
                    break;
                case DocBacarios.TipoCpfCnpj.CPF:
                    INSSPF();
                    break;
                case DocBacarios.TipoCpfCnpj.INVALIDO:                   
                default:
                    break;
            }                      
        }

        private void lookUpFOR2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
            {
                if (rowFRN == null)
                    return;
                int FRN = rowFRN.FRN;
                Cadastros.Fornecedores.cFornecedoresCampos EditorFRN = new Cadastros.Fornecedores.cFornecedoresCampos();               
                EditorFRN.Fill(CompontesBasicos.TipoDeCarga.pk, FRN, false);
                if (EditorFRN.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    dNOtAsLocal.FORNECEDORESTableAdapter.Fill(dNOtAsLocal.FORNECEDORES);
                    _rowFRN = null;
                    AjustaCNPJ(FRN);

                    //*** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***
                    lblContaCredito.Visible = false;
                    if (ComboPAGTipoDefault.Text != "" && lookUpFOR2.ItemIndex != -1)
                    {
                       PAGTipo Tipo = (PAGTipo)ComboPAGTipoDefault.EditValue;
                       if (Tipo == PAGTipo.PECreditoConta || Tipo == PAGTipo.PETransferencia)
                       {
                          lblContaCredito.Visible = true;
                          //Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESDataTable Forn = new Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESDataTable();
                          //Cadastros.Fornecedores.dFornecedoresCampos.FORNECEDORESTableAdapter.Fill(Forn, (int)lookUpFOR2.EditValue);
                          string strContaCredito = string.Format("Banco {0} Ag�ncia {1}-{2} Conta {3}-{4}", rowFRN.IsFRNBancoCreditoNull() ? "" : rowFRN.FRNBancoCredito, rowFRN.IsFRNAgenciaCreditoNull() ? "" : rowFRN.FRNAgenciaCredito, rowFRN.IsFRNAgenciaDgCreditoNull() ? "" : rowFRN.FRNAgenciaDgCredito, rowFRN.IsFRNContaCreditoNull() ? "" : rowFRN.FRNContaCredito, rowFRN.IsFRNContaDgCreditoNull() ? "" : rowFRN.FRNContaDgCredito);
                          //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                          if (rowFRN.IsFRNBancoCreditoNull() || rowFRN.IsFRNAgenciaCreditoNull() || rowFRN.IsFRNAgenciaDgCreditoNull() || rowFRN.IsFRNContaCreditoNull() || rowFRN.IsFRNContaDgCreditoNull() ||
                              rowFRN.FRNBancoCredito.Trim() == "" || rowFRN.FRNAgenciaCredito.Trim() == "" || rowFRN.FRNAgenciaDgCredito.Trim() == "" || rowFRN.FRNContaCredito.Trim() == "" || rowFRN.FRNContaDgCredito.Trim() == "")
                          {
                              //*** MRC - INICIO (14/01/2016 12:30) ***
                              lblContaCredito.Text = "Conta para Cr�dito n�o cadastrada ou incompleta";
                              //*** MRC - TERMINO (14/01/2016 12:30) ***
                              lblContaCredito.ForeColor = System.Drawing.Color.Red;
                          }
                          //*** MRC - TERMINO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                          else
                          {
                              //*** MRC - INICIO (14/01/2016 12:30) ***
                              lblContaCredito.Text = string.Format("Conta {0} para Cr�dito:   {1}", (rowFRN.FRNContaTipoCredito == (int)FRNContaTipoCredito.ContaCorrete) ? "Corrente" : "Poupan�a", strContaCredito);
                              //*** MRC - TERMINO (14/01/2016 12:30) ***
                              lblContaCredito.ForeColor = System.Drawing.Color.Blue;
                          }
                       }
                    }
                    //*** MRC - TERMINO - PAG-FOR (29/04/2014 15:25) ***
                }
            }
        }

        private void nOATotalSpinEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
            {
                nOATotalSpinEdit.DoValidate();
                if (!CamposObrigatoriosOk(dNOtAsBindingSource))
                {
                    MessageBox.Show("Campo obrigat�rio");
                    foreach (DevExpress.XtraEditors.BaseEdit c in Controlesobrigatorios)
                        if (c.ErrorText != "")
                        {
                            c.Focus();
                            break;
                        }
                }
                else
                {
                    if ((LinhaMae.NOANumero == 0) || (LinhaMae.IsNOA_FRNNull()))
                    {
                        MessageBox.Show("Este recurso s� est� dispon�vel para notas fiscais v�lidas (n�mero/CNPJ)", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    };

                    BindingSourcePrincipal.EndEdit();
                    NotaMae.cNotaMae cNotaMae;
                    //if (LinhaMae.NOA == -1)
                    //{
                        InibirVerificaDuplicidadeDeNota = true;
                        if (!Update_F())
                        {
                            InibirVerificaDuplicidadeDeNota = false;
                            return;
                        }
                    //};
                    InibirVerificaDuplicidadeDeNota = false;
                    cNotaMae = new ContaPagar.NotaMae.cNotaMae(LinhaMae.NOA_CON, LinhaMae.NOA_FRN, LinhaMae.NOANumero);
                    System.Collections.SortedList NovasNotas = new System.Collections.SortedList();
                    if (cNotaMae.ShowEmPopUp() == DialogResult.OK)
                    {
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cNotasCampos - 1809", dNOtAs.dNOtAsSt.NOtAsTableAdapter); 
                            foreach (NotaMae.dNotaMae.NOtAsRow rowFilhas in cNotaMae.dNotaMae.NOtAs)
                            {
                                if (rowFilhas.NOA < 0)
                                {
                                    dNOtAs.NOtAsRow novaRow = dNOtAs.dNOtAsSt.NOtAs.NewNOtAsRow();
                                    novaRow.NOA_CON = LinhaMae.NOA_CON;
                                    novaRow.NOA_FRN = LinhaMae.NOA_FRN;
                                    novaRow.NOA_PLA = rowFilhas.NOA_PLA;
                                    novaRow.NOAAguardaNota = LinhaMae.NOAAguardaNota;
                                    if (!LinhaMae.IsNOACodServicoNull())
                                        novaRow.NOACodServico = LinhaMae.NOACodServico;
                                    novaRow.NOADataEmissao = LinhaMae.NOADataEmissao;
                                    novaRow.NOACompet = LinhaMae.NOACompet;
                                    novaRow.NOADATAI = DateTime.Now;
                                    novaRow.NOADefaultPAGTipo = LinhaMae.NOADefaultPAGTipo;
                                    novaRow.NOAI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                                    novaRow.NOANumero = LinhaMae.NOANumero;
                                    if (!LinhaMae.IsNOAObsNull())
                                        novaRow.NOAObs = LinhaMae.NOAObs;
                                    novaRow.NOAServico = rowFilhas.NOAServico;
                                    novaRow.NOAStatus = (int)NOAStatus.Parcial;
                                    novaRow.NOATipo = LinhaMae.NOATipo;
                                    novaRow.NOATotal = rowFilhas.NOATotal;
                                    if (!LinhaMae.IsNOA_CCTNull())
                                        novaRow.NOA_CCT = LinhaMae.NOA_CCT;
                                    dNOtAs.dNOtAsSt.NOtAs.AddNOtAsRow(novaRow);
                                    dNOtAs.dNOtAsSt.NOtAsTableAdapter.Update(novaRow);
                                    novaRow.AcceptChanges();
                                    NovasNotas.Add(novaRow.NOA,string.Format("Nota:{0} - {1}", novaRow.NOANumero, novaRow.NOA_PLA));                                    
                                }
                            }
                            VirMSSQL.TableAdapter.STTableAdapter.Commit();
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                            MessageBox.Show("Erro ao Gravar");
                            return;
                        }
                        foreach (System.Collections.DictionaryEntry DE in NovasNotas)
                        {
                            cNotasCampos NovaTab = (cNotasCampos)CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludo(typeof(cNotasCampos), (string)DE.Value, CompontesBasicos.TipoDeCarga.pk, (int)DE.Key, false);
                            NovaTab.InibirVerificaDuplicidadeDeNota = true;
                        }
                        InibirVerificaDuplicidadeDeNota = true;
                    }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (NOAAbrir != 0)
            {
                cNotasCampos NovaTab = (cNotasCampos)CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludo(typeof(cNotasCampos), "VERIFICAR", CompontesBasicos.TipoDeCarga.pk, NOAAbrir, true);
                NovaTab.InibirVerificaDuplicidadeDeNota = true;
            }
            NOAAbrir = 0;
        }

        private void nOADataEmissaoDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            //o try � para evitar erros no cancelamento onde a linha fica �rfam
            try
            {
                if ((LinhaMae["NOACompet"] == DBNull.Value) || (LinhaMae.NOACompet < 201100))
                {
                    LinhaMae.NOACompet = nOADataEmissaoDateEdit.DateTime.Year * 100 + nOADataEmissaoDateEdit.DateTime.Month;
                    cCompet1.CompetenciaBind = LinhaMae.NOACompet;
                }
            }
            catch { }
        }

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                if (PLArow == null)
                    return;
                cNovoSPL novo = new cNovoSPL(string.Format("{0} - {1}", PLArow.PLA, PLArow.PLADescricao));
                novo.Cadastre(PLArow.PLA);
                /*
                //novo.Plano.Text = string.Format("{0} - {1}", PLArow.PLA, PLArow.PLADescricao);
                if (novo.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    try
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoLocal();
                        string Comando1 = "INSERT INTO SubPLano(SPL_PLA,SPLCodigo,SPLDescricao,SPLServico,SPLNoCondominio) VALUES (@P1,@P2,@P3,1,@P4)";
                        int SPL = VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc(Comando1, PLArow.PLA, novo.Codigo.Text, novo.Descricao.Text, novo.checkNoLocal.Checked);
                        //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update NOtAs set NOA_SPL = @P1 where NOA_PLA = @P2 and NOA_SPL is null", SPL, row.NOA_PLA);
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                        Framework.datasets.dPLAnocontas.dPLAnocontasSt25.SubPLanoTableAdapter.Fill25(Framework.datasets.dPLAnocontas.dPLAnocontasSt25.SubPLano);
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                    }
                   
                    
                }
                */
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if(e.Column.Equals(colBotao))
            {
                dNOtAs.PAGamentosRow rowDel = (dNOtAs.PAGamentosRow)gridView1.GetDataRow(e.RowHandle);                
                e.RepositoryItem = ((rowDel != null) && Deletavel(rowDel)) ? Botoes : null;
            }
            else if (e.Column.Equals(colbotaoCheque))
            {
                dNOtAs.PAGamentosRow rowDel = (dNOtAs.PAGamentosRow)gridView1.GetDataRow(e.RowHandle);
                e.RepositoryItem = ((rowDel != null) && !rowDel.IsPAG_CHENull()) ? BotaoCheque : null;
            }
            else if (e.Column.Equals(colPAGTipo))
            {
                dNOtAs.PAGamentosRow rowDel = (dNOtAs.PAGamentosRow)gridView1.GetDataRow(e.RowHandle);
                if (rowDel != null)
                    if (!rowDel.IsPAGIMPNull())
                    {
                        TipoImposto TipoImp = (TipoImposto)rowDel.PAGIMP;
                        if (!rowCON.IsCONGPSEletronicoNull() && rowCON.CONGPSEletronico && !(TipoImp.EstaNoGrupo(TipoImposto.INSSpf,
                                                                            TipoImposto.INSSpfEmp,
                                                                            TipoImposto.INSSpfRet,
                                                                            TipoImposto.ISSQN,
                                                                            TipoImposto.ISS_SA)))
                            e.RepositoryItem = repositoryTipoPagGuia;
                        else
                        {
                            PAGTipo TipoAtual = (PAGTipo)rowDel.PAGTipo;
                            //este ultimo if � para permitir mudar para cheque algum que estaja como eletronico em um condominio que nao pode
                            if (TipoAtual.EstaNoGrupo(PAGTipo.PEGuia))
                                e.RepositoryItem = repositoryTipoPagGuia;
                            else
                                e.RepositoryItem = repositoryTipoPagFull;
                        }
                    }
                    else
                        if (ComboPAGTipoDefault.Properties.Items.GetItem(rowDel.PAGTipo) == null)
                            e.RepositoryItem = repositoryTipoPagFull;
                        else
                            e.RepositoryItem = repositoryTipoPag; 
            }
        }

        /// <summary>
        /// Chave
        /// </summary>
        public int NOA
        {
            get { return LinhaMae.NOA; }
        }

        private void BotaoCheque_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dNOtAs.PAGamentosRow rowPAG = (dNOtAs.PAGamentosRow)gridView1.GetFocusedDataRow();
            if ((rowPAG == null) || rowPAG.IsPAG_CHENull())
                return;
            Cheque Cheque = new Cheque(rowPAG.PAG_CHE, Cheque.TipoChave.CHE);
            if (Cheque.Encontrado)            
            {               
               PAGTipo Tipo = (PAGTipo)rowPAG.PAGTipo;               
               if (Tipo.EstaNoGrupo(PAGTipo.PECreditoConta, PAGTipo.PEOrdemPagamento, PAGTipo.PETituloBancario, PAGTipo.PETituloBancarioAgrupavel, PAGTipo.PETransferencia, PAGTipo.PEGuia, PAGTipo.Guia))
                   Cheque.Editar(true);               
               else
                  Cheque.Editar(false);
                if (Cheque.Status == CHEStatus.Cancelado)
                {
                    cNotasCampos Nova = new cNotasCampos(NOA, Titulo, EMPTProc1);
                    FechaTela(DialogResult.OK);
                    //Nova.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
                }
            }            
        }

        private void groupControl1_Enter(object sender, EventArgs e)
        {
            bool contemImpostos = (ImpIR == null) && (ImpContr == null) && (ImpISS == null) && (ImpINSS == null) && (ImpINSSTomador == null) && (ImpINSSPrestador == null);
            nOADataEmissaoDateEdit.Properties.ReadOnly = !contemImpostos;
            cCompet1.ReadOnly = !contemImpostos;
            nOATotalSpinEdit.Properties.ReadOnly = !contemImpostos;
        }

        //*** MRC - INICIO - PAG-FOR (20/05/2014 13:00) ***
        private void VerificaTratamentoColunasAdicionais(dNOtAs.PAGamentosRow linha, PAGTipo Tipo, string strPAGAdicional, bool booSetaPermiteAgrupar)
        {
            //Faz tratamento especifio para determinados tipos de pagamento (especialmente PE com informacoes adicionais)
            string PAGN = linha.IsPAGNNull() ? "" : linha.PAGN;
            if (Tipo == PAGTipo.PETituloBancario)
            {
               //Solicita linha digitavel
               DateTime datVencimento = linha.PAGVencimento;
               decimal decValor = linha.PAGValor;
               
               if (!Cheque.SolicitaLinhaDigitavel(PAGN, ref strPAGAdicional, ref datVencimento, ref decValor))
                  strPAGAdicional = "";
               else
               {
                  linha.PAGVencimento = datVencimento;
                  linha.PAGValor = decValor;
               }

               //N�o permite agrupar
               if (booSetaPermiteAgrupar) linha.PAGPermiteAgrupar = false;
            }
            else if (Tipo == PAGTipo.PEOrdemPagamento)
            {
               //Solicita instrucao indicando o padrao
               if (strPAGAdicional == "") 
                  strPAGAdicional = textDadosDep.Text;
               VirInput.Input.Execute(string.Format("Instru��o para Ordem de Pagamento - Parcela {0}:", PAGN ), ref strPAGAdicional, false, 40);

               //N�o permite agrupar
               if (booSetaPermiteAgrupar) linha.PAGPermiteAgrupar = false;
            }
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
            else if (Tipo == PAGTipo.PETransferencia)
            {
                //N�o permite agrupar
                if (booSetaPermiteAgrupar) linha.PAGPermiteAgrupar = false;
            }
            //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (08/07/2015 09:00) ***
            else if (Tipo == PAGTipo.PECreditoConta && !LinhaMae.IsNOA_CCTCredNull())
            {
                //Seta conta de deposito do proprio condominio
                strPAGAdicional = string.Format("{0} - CCT = {1}", cmbContaCredito.Text, cmbContaCredito.EditValue.ToString());

                //Permite agrupar
                if (booSetaPermiteAgrupar) linha.PAGPermiteAgrupar = true;
            }
            //*** MRC - TERMINO - PAGAMENTO ELETRONICO (08/07/2015 09:00) ***
            else
            {
               //Permite agrupar
               if (booSetaPermiteAgrupar) linha.PAGPermiteAgrupar = true;
            }

            //Atualiza informa��o adicional
            linha.PAGAdicional = strPAGAdicional;
        }

        private void gridView1_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //Verifica se esta alterando o tipo de pagamento
            if (e.Column.Equals(colPAGTipo))
            {
               //Pega a linha
               DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
               if ((DRV == null) || (DRV.Row == null))
                  return;
               dNOtAs.PAGamentosRow linha = ((dNOtAs.PAGamentosRow)DRV.Row);

               //Faz tratamento especifio para determinados tipos de pagamento (especialmente PE com informacoes adicionais)
               VerificaTratamentoColunasAdicionais(linha, (PAGTipo)e.Value, "", true);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
           //Paga a informacao com double click
           DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
           DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(view.GridControl.PointToClient(Control.MousePosition));

           //Verifica se e celula valida
           if (info.InRow || info.InRowCell)
           {
              //Pega a linha
              DataRowView DRV = (DataRowView)gridView1.GetRow(info.RowHandle);
              if ((DRV == null) || (DRV.Row == null))
                 return;
              dNOtAs.PAGamentosRow linha = ((dNOtAs.PAGamentosRow)DRV.Row);

              //Verifica se e linha que nao pode ser alterada (nao cadastrada)
              if ((CHEStatus)linha.CHEStatus != CHEStatus.Cadastrado)
                 return;

              //Verifica qual campo esta clicando (PAGAdicional ou PAGPermiteAgrupar)
              if (info.Column.Equals(colPAGAdicional))
              {
                 //Faz tratamento especifio para determinados tipos de pagamento (especialmente PE com informacoes adicionais)              
                 VerificaTratamentoColunasAdicionais(linha, (PAGTipo)linha.PAGTipo, linha.IsPAGAdicionalNull() ? "" : linha.PAGAdicional, false);
              }
              else if (info.Column.Equals(colPAGPermiteAgrupar))
              {
                 //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                 //Verifica se esta tentando alterar qualquer tipo diferente de tipos PE nao agrupavel 
                 PAGTipo Tipo = (PAGTipo)linha.PAGTipo;
                 if ((Tipo != PAGTipo.PETransferencia) && (Tipo != PAGTipo.PEOrdemPagamento) && (Tipo != PAGTipo.PETituloBancario))
                    linha.PAGPermiteAgrupar = !linha.PAGPermiteAgrupar;
                 //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
              }
           }
        }

        private void BotDestrava_Click(object sender, EventArgs e)
        {
            int? Numero = null;            
            if (LinhaMae.NOANumero != 0)
                Numero = LinhaMae.NOANumero;
            cDestrava cDestrava1 = new cDestrava(Numero, LinhaMae.NOADataEmissao);
            if (cDestrava1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                TravaTela(false);
                Follow.PagamentoPeriodico PagamentoPeriodico = new Follow.PagamentoPeriodico(LinhaMae.NOA_PGF);
                Destrava((int)cDestrava1.spin_Numero.Value, cDestrava1.date_DataEmissao.DateTime, PagamentoPeriodico,cDestrava1.memo_Justificativa.Text);
            }
        }

        private void TravaTela(bool Travar)
        {
            BotDestrava.Visible = Travar;
            PControles.Visible = !Travar;
            gridView1.OptionsView.NewItemRowPosition = Travar ? DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None : DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
        }

        private dContasLogicas dContasLogicas;

        private void cNotasCampos_cargaFinal(object sender, EventArgs e)
        {
            dContasLogicas = dContasLogicas.GetdContasLogicas(LinhaMae.NOA_CON,true);
            conTasLogicasBindingSource.DataSource = dContasLogicas;
            foreach (dContasLogicas.ConTasLogicasRow rowCTL in dContasLogicas.ConTasLogicas)
            {
                if (lookCTL.EditValue != null)
                    break;
                switch ((Framework.CTLTipo)rowCTL.CTLTipo)
                {
                    case Framework.CTLTipo.Caixa:
                        lookCTL.EditValue = rowCTL.CTL;
                        break;
                    //case Framework.CTLTipo.Fundo:
                      //  CTLFUNDO = rowCTL.CTL;
                        //break;
                    //case Framework.CTLTipo.CreditosAnteriores:
                      //  _CTLCREDITOS_Anteriores = rowCTL.CTL;
                        //break;
                }                
            }
        }

        internal void SetCTL(int CTL)
        {
            lookCTL.EditValue = CTL;
        }
        //*** MRC - TERMINO - PAG-FOR (20/05/2014 13:00) ***

        //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
        private void chkDepProprioCon_CheckedChanged(object sender, EventArgs e)
        {
            //Habilita e desabilita dados de credito (condominio ou fornecedor)
            cmbContaCredito.Visible = chkDepProprioCon.Checked;
            if (!chkDepProprioCon.Checked) { cmbContaCredito.EditValue = null; }
            grcForn.Enabled = !chkDepProprioCon.Checked;             
        }

        private void BotRemove_Click(object sender, EventArgs e)
        {
            if (Framework.DSCentral.USU != 30)
                return;
            int conta = 0;
            foreach (dNOtAs.PAGamentosRow rowPAG in LinhaMae.GetPAGamentosRows())
            {
                rowPAG.Delete();
                conta++;
                Console.WriteLine(string.Format("APAGANDO {0}",conta));
            }
            MessageBox.Show(string.Format("NOA:{0} - conta",LinhaMae.NOA,conta));
            Clipboard.SetText(LinhaMae.NOA.ToString());
        }

        private void ButImp_Click(object sender, EventArgs e)
        {
            if (NOA > 0)
                Nota.MostraPDF();
        }
    }
}
