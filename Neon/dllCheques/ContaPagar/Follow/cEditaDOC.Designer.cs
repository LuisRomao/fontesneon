namespace ContaPagar.Follow
{
    partial class cEditaDOC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.XArquivo = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.XTipo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.XDescritivo = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XArquivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XDescritivo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
         
            // 
            // XArquivo
            // 
            this.XArquivo.Location = new System.Drawing.Point(132, 85);
            this.XArquivo.MenuManager = this.BarManager_F;
            this.XArquivo.Name = "XArquivo";
            this.XArquivo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XArquivo.Properties.Appearance.Options.UseFont = true;
            this.XArquivo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.XArquivo.Size = new System.Drawing.Size(421, 26);
            this.XArquivo.TabIndex = 14;
            this.XArquivo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.XArquivo_ButtonClick);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(21, 88);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(62, 19);
            this.labelControl4.TabIndex = 16;
            this.labelControl4.Text = "Arquivo";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(21, 56);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 19);
            this.labelControl3.TabIndex = 15;
            this.labelControl3.Text = "Tipo";
            // 
            // XTipo
            // 
            this.XTipo.Location = new System.Drawing.Point(132, 53);
            this.XTipo.MenuManager = this.BarManager_F;
            this.XTipo.Name = "XTipo";
            this.XTipo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XTipo.Properties.Appearance.Options.UseFont = true;
            this.XTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.XTipo.Size = new System.Drawing.Size(120, 26);
            this.XTipo.TabIndex = 13;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Todos|*.html;*.htm;*.pdf|HTML|*.html|HTM|*.htm|PDF|*.pdf";
            this.openFileDialog1.InitialDirectory = "S:\\";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(21, 121);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(81, 19);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Descritivo";
            // 
            // XDescritivo
            // 
            this.XDescritivo.Location = new System.Drawing.Point(132, 118);
            this.XDescritivo.MenuManager = this.BarManager_F;
            this.XDescritivo.Name = "XDescritivo";
            this.XDescritivo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XDescritivo.Properties.Appearance.Options.UseFont = true;
            this.XDescritivo.Properties.MaxLength = 50;
            
            this.XDescritivo.Size = new System.Drawing.Size(421, 26);
            
            this.XDescritivo.TabIndex = 18;
            // 
            // cEditaDOC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.XDescritivo);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.XArquivo);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.XTipo);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cEditaDOC";
            this.Size = new System.Drawing.Size(570, 161);
            this.Controls.SetChildIndex(this.XTipo, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.XArquivo, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.XDescritivo, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XArquivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XDescritivo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ButtonEdit XArquivo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ImageComboBoxEdit XTipo;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraEditors.LabelControl labelControl1;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit XDescritivo;
    }
}
