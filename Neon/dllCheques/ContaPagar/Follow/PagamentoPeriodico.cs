/*
MR - 16/04/2014 11:15           - Altera��es indicadas por *** MRC - INICIO - PAG-FOR (16/04/2014 11:15) ***
LH - 23/04/2014       13.2.8.29 - FIX erro nos par�metros da chamada - ContaPagar.dNOtAs.dNOtAsStX(EMPTipo).IncluiNotaSimples
MR - 30/07/2014 16:00           - Tratamento do novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
LH - 01/12/2014 14:00 14.1.4.94 - alter��o no funcionamento dos pagamentos peri�dicos com boleto
*/


using System;
using System.Collections.Generic;
using dllCheques;
using Framework;
using Framework.objetosNeon;
using VirMSSQL;
using CompontesBasicosProc;
using ContaPagarProc;
using FrameworkProc;
using ContaPagarProc.Follow;
using VirEnumeracoesNeon;
using VirEnumeracoes;


namespace ContaPagar.Follow
{
    

    /// <summary>
    /// Objeto PagementoPeriodico
    /// </summary>
    public class PagamentoPeriodico : PagamentoPeriodicoProc
    {
        /// <summary>
        /// 
        /// </summary>
        public string competenciaerrada = "ok";
        /// <summary>
        /// 
        /// </summary>
        public string agenda = "ok";

        

        /*
        private VirMSSQL.TableAdapter STTA()
        {
            return TableAdapter.ST(EMPTipo == Enumeracoes.EMPTipo.Local ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial);
        }

        private void Vircatch(Exception e)
        {
            STTA().Vircatch(e);
        }

        private bool Commit()
        {
            return EMPTipo.Commit();
        }*/

        #region Grupo override

        /// <summary>
        /// 
        /// </summary>
        protected override string NominalPagEletronico
        {
            get { return FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(PGFrow.PGF_CON).CONNome; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string DadosPagEl()
        {
            Cadastros.Condominios.PaginasCondominio.dConta dContas = new Cadastros.Condominios.PaginasCondominio.dConta();
            Cadastros.Condominios.PaginasCondominio.dConta.ContaCorrenTeDataTable dCont = dContas.ContaCorrenTeTableAdapter.GetContaPgEletronicoByCON(PGFrow.PGF_CON);
            dContas.ContaCorrenTeTableAdapter.Fill(dCont, PGFrow.PGF_CON);
            return string.Format("{0} - CCT = {1}", dCont.FindByCCT(PGFrow.PGF_CCTCred).DescricaoContaCorrente, PGFrow.PGF_CCTCred);
        }


        /*
        /// <summary>
        /// Fun��o que atualiza o cadastro verificando o proximo evento e cadastrando se necess�rio
        /// </summary>
        /// <returns></returns>
        public override bool CadastrarProximo()
        {
            //Competencia
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("1");
            TipoPagPeriodico Forma = (TipoPagPeriodico)PGFrow.PGFForma;
            //Remove da agenda.
            if (!FormasContraladasNaAgenda.Contains(Forma) && (rowAGB != null) && (rowAGB.AGBStatus != (int)Enumeracoes.AGBStatus.Ok))
            {
                rowAGB.AGBStatus = (int)Enumeracoes.AGBStatus.Ok;
                rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - Forma de pagamento n�o controlada na agenda", DateTime.Now);
                if (!PGFrow.IsAGBDataInicioNull())
                    rowAGB.AGBDataTermino = rowAGB.AGBDataInicio = PGFrow.AGBDataInicio;
                dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(rowAGB);
                rowAGB.AcceptChanges();
            }
            if ((StatusPagamentoPeriodico)PGFrow.PGFStatus == StatusPagamentoPeriodico.Encerrado)
            {
                if (rowAGB != null)
                {
                    rowAGB.AGBStatus = (int)Enumeracoes.AGBStatus.Ok;                    
                    rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - Terminado", DateTime.Now);                    
                    if (!PGFrow.IsAGBDataInicioNull())
                        rowAGB.AGBDataTermino = rowAGB.AGBDataInicio = PGFrow.AGBDataInicio;
                    dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(rowAGB);
                    rowAGB.AcceptChanges();
                }              
            }

            bool retorno = false;
            Terminado = false;
            AjustarAGBReajContrato();
            //Verificar se n�o terminou o per�odo caso tenha compet�ncia de t�rmino
            if (!PGFrow.IsPGFCompetFNull())
                if (PGFrow.PGFProximaCompetencia > PGFrow.PGFCompetF)
                {
                    PGFrow.PGFStatus = (int)StatusPagamentoPeriodico.Encerrado;
                    PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} �ltima compet�ncia gerada: encerrado\r\n", DateTime.Now);
                    dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                    PGFrow.AcceptChanges();
                    Terminado = true;
                }
            //temos 3 agendamentos 1-pagamento 2-contrato 3-documento
            //verificar contrato (2)


            CompontesBasicos.Performance.Performance.PerformanceST.Registra("2");
            //*** MRC - INICIO - PAG-FOR (2) ***
            switch (Forma)
            {
                case TipoPagPeriodico.Cheque:
                case TipoPagPeriodico.ChequeDeposito:
                case TipoPagPeriodico.ChequeSindico:
                case TipoPagPeriodico.PECreditoConta:
                case TipoPagPeriodico.PETransferencia:
                case TipoPagPeriodico.PEOrdemPagamento:
                    if (StatusAtivos.Contains((StatusPagamentoPeriodico)PGFrow.PGFStatus))
                        retorno = CadastrarProximoCheque(enumNotaCheque.chegou);
                    else
                        retorno = true;
                    break;
                case TipoPagPeriodico.Conta:
                case TipoPagPeriodico.Boleto:
                case TipoPagPeriodico.DebitoAutomatico:
                case TipoPagPeriodico.DebitoAutomaticoConta:
                case TipoPagPeriodico.Folha:
                case TipoPagPeriodico.Adiantamento:
                    //retorno = CadastraNaAgenda() && CadastrarProximoCheque(enumNotaCheque.aguarda);
                    retorno = CadastraProximaNota(enumNotaCheque.aguarda) && CadastraNaAgenda();
                    break;
                //case TipoPagPeriodico.Conta:
                //case TipoPagPeriodico.DebitoAutomatico:
                //case TipoPagPeriodico.DebitoAutomaticoConta:
                case TipoPagPeriodico.PETituloBancario:                
                case TipoPagPeriodico.PETituloBancarioAgrupavel:                
                    retorno = CadastraNaAgenda();
                    break;                
                case TipoPagPeriodico.FolhaFGTS:
                    retorno = false;
                    break;
                default:
                    throw new NotImplementedException(string.Format("N�o implementado tratamento para Forma = {0} em CadastrarProximo PagamentosPeriodico.cs", (TipoPagPeriodico)PGFrow.PGFForma));retorno = false;                    
            };
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("3");
            return retorno;
        }
        */

        /*
        private decimal ValorLiquido()
        {
            decimal Liquido = PGFrow.PGFValor;
            if (!PGFrow.IsPGFISSNull() && dllImpostos.Imposto.Desconta(TipoImposto.ISSQN))
                Liquido -= PGFrow.PGFISS;
            //tratar PJ//
            if (!PGFrow.IsPGFINSSPNull() && dllImpostos.Imposto.Desconta(TipoImposto.INSSpfRet))
                Liquido -= PGFrow.PGFINSSP;
            if (!PGFrow.IsPGFINSSTNull() && dllImpostos.Imposto.Desconta(TipoImposto.INSSpfEmp))
                Liquido -= PGFrow.PGFINSST;
            if (!PGFrow.IsPGFIRNull() && dllImpostos.Imposto.Desconta(TipoImposto.IR))
                Liquido -= PGFrow.PGFIR;
            if (!PGFrow.IsPGFPISCOFNull() && dllImpostos.Imposto.Desconta(TipoImposto.PIS_COFINS_CSLL))
                Liquido -= PGFrow.PGFPISCOF;
            return Liquido;
        }*/

        /*
        Original da fun��o CadastrarProximoDebitoAutomatico
        int deltaData = (DataPag - ProximaData).Days;
            int deltaMeses = (int)Math.Round(deltaData / 30M);
            Competencia CompServ = ProximaCompetencia.Add(deltaMeses);
            TipoPagPeriodico PGFForma = (TipoPagPeriodico)PGFrow.PGFForma;

            if ((PGFForma != TipoPagPeriodico.DebitoAutomatico)
                &&
                (PGFForma != TipoPagPeriodico.DebitoAutomaticoConta)
                &&
                (PGFForma != TipoPagPeriodico.Conta)
                )
            {

                Framework.Alarmes.Aviso Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, CCD);
                if (!Aviso.Encontrado)
                {
                    string DescricaoErro = string.Format("Ocorreu o d�bito autom�tico para um pagamento com forma de pagamento distinta\r\nDetalhes:\r\n  Forma prevista:{0}\r\n  Data:{1:dd/MM/yyyy}\r\n"
                                                , PGFForma, DataPag);
                    Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, PGFrow.PGF_CON, CCD, CompServ, "D�bito autom�tico n�o previsto.", DescricaoErro);
                }
                return false;
            }
            decimal Liquido = PGFrow.PGFValor;



            if (!PGFrow.IsPGFISSNull() && dllImpostos.Imposto.Desconta(TipoImposto.ISSQN))
                Liquido -= PGFrow.PGFISS;
            //tratar PJ//
            if (!PGFrow.IsPGFINSSPNull() && dllImpostos.Imposto.Desconta(TipoImposto.INSSpfRet))
                Liquido -= PGFrow.PGFINSSP;
            if (!PGFrow.IsPGFINSSTNull() && dllImpostos.Imposto.Desconta(TipoImposto.INSSpfEmp))
                Liquido -= PGFrow.PGFINSST;
            if (!PGFrow.IsPGFIRNull() && dllImpostos.Imposto.Desconta(TipoImposto.IR))
                Liquido -= PGFrow.PGFIR;
            if (!PGFrow.IsPGFPISCOFNull() && dllImpostos.Imposto.Desconta(TipoImposto.PIS_COFINS_CSLL))
                Liquido -= PGFrow.PGFPISCOF;
            if (ValorEfetivo <= 0)
                return false;
            decimal fator = ValorEfetivo / Liquido;


            //int deltaData = (DataPag - ProximaData).Days;
            //int deltaMeses = (int)Math.Round(deltaData / 30M);
            //Competencia CompServ = ProximaCompetencia.Add(deltaMeses);


            int FRN = PGFrow.IsPGF_FRNNull() ? 0 : PGFrow.PGF_FRN;
            System.Collections.ArrayList Retencoes = new System.Collections.ArrayList();
            if (!PGFrow.IsPGFISSNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoISS, fator * PGFrow.PGFISS, CompontesBasicosProc.SimNao.Padrao));
            if (!PGFrow.IsPGFINSSPNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, fator * PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
            if (!PGFrow.IsPGFINSSTNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, fator * PGFrow.PGFINSST, CompontesBasicosProc.SimNao.Padrao));
            if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
            {
                if (!PGFrow.IsPGFIRNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IR, fator * PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
            }
            else
            {
                if (!PGFrow.IsPGFIRNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IRPF, fator * PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
            }
            if (!PGFrow.IsPGFPISCOFNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.PIS_COFINS_CSLL, fator * PGFrow.PGFPISCOF, CompontesBasicosProc.SimNao.Padrao));
            string Nominal = PGFrow.IsPGFNominalNull() ? "" : PGFrow.PGFNominal;
            dPagamentosPeriodicos.PAGamentos.Clear();
            dPagamentosPeriodicos.NOtAsTableAdapter.FillByPGFComp(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
            dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
            bool reportarErro = true;
            try
            {
                TableAdapter.AbreTrasacaoSQL(string.Format("ContaPagar PagamentoPeriodico - 322 CCD = {0}",CCD), 
                                             dPagamentosPeriodicos.PaGamentosFixosTableAdapter,
                                             dPagamentosPeriodicos.NOtAsTableAdapter,
                                             dPagamentosPeriodicos.PAGamentosTableAdapter);
                //Verificar se est� programado
                dPagamentosPeriodicos.NOtAsRow ultimaNota = null;
                bool CadastrarNota = true;
                if (dPagamentosPeriodicos.NOtAs.Count > 0)
                {
                    int apontador = 0;
                    ultimaNota = dPagamentosPeriodicos.NOtAs[0];
                    while ((ultimaNota.NOACompet > CompServ.CompetenciaBind) && (dPagamentosPeriodicos.NOtAs.Count > (apontador + 1)))
                        ultimaNota = dPagamentosPeriodicos.NOtAs[++apontador];
                    dPagamentosPeriodicos.PAGamentosRow[] rowPags = ultimaNota.GetPAGamentosRows();
                    dPagamentosPeriodicos.PAGamentosRow rowPagCHE = null;
                    foreach (dPagamentosPeriodicos.PAGamentosRow rowCandidato in rowPags)
                    {
                        PAGTipo Tipo = (PAGTipo)rowCandidato.PAGTipo;
                        if ((Tipo == PAGTipo.DebitoAutomatico) && ((CHEStatus)rowCandidato.CHEStatus == CHEStatus.DebitoAutomatico))
                        {
                            rowPagCHE = rowCandidato;
                            break;
                        }
                    }
                    if (ultimaNota.NOACompet < CompServ.CloneCompet(-1).CompetenciaBind)
                    {
                        new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoPulado, PGFrow.PGF_CON, PGFrow.PGF, CompServ.CloneCompet(-1), "D�bito autom�tico pulado", string.Format("Verificar o d�bito autom�tico.\r\nCompet�ncia pulada: {0}", CompServ.CloneCompet(-1)));
                    }
                    else
                        if (ultimaNota.NOACompet == CompServ.CloneCompet(-1).CompetenciaBind)
                        {
                            new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoDocumento, PGFrow.PGF_CON, PGFrow.PGF, CompServ.CloneCompet(-1), "D�bito autom�tico n�o agendado", string.Format("Verificar a conta (documento) para o d�bito autom�tico.\r\nCompet�ncia pulada: {0}", CompServ));
                        }
                        else

                            if (ultimaNota.NOACompet == CompServ.CompetenciaBind)
                            {
                                if ((rowPagCHE != null) && ((CHEStatus)rowPagCHE.CHEStatus == CHEStatus.DebitoAutomatico))
                                {
                                    Cheque ChequePrevisto = new Cheque(rowPagCHE.PAG_CHE,Cheque.TipoChave.CHE);
                                    decimal ValogAgendado = ChequePrevisto.CHErow.CHEValor;
                                    if (ValogAgendado > ValorEfetivo)
                                    {
                                        //ContaPagar.dNOtAs.PAGamentosRow rowPag = ContaPagar.dNOtAs.dNOtAsStX(EMPTipo).PAGamentos.NewPAGamentosRow();
                                        Cheque[] ChequesParciais = ChequePrevisto.Dividir(ValorEfetivo);
                                        if (ChequesParciais == null)
                                        {
                                            reportarErro = false;
                                            throw new Exception("Cheque n�o pode ser dividido");
                                        }
                                        ChequesParciais[0].AmarrarCCD(CCD);
                                        string DescricaoErro = string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico com valor inferior ao previamente agendado.\r\n      Agendamento: {1} {2:dd/MM/yyyy} referente a {3}\r\n      Valor debitado:{4}",
                                                               DateTime.Now,
                                                               ValogAgendado,
                                                               DataPag,
                                                               CompServ,
                                                               ValorEfetivo);
                                        PGFrow.PGFHistorico += DescricaoErro;
                                        new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoComValorIncorreto, PGFrow.PGF_CON, CCD, CompServ, "D�bito autom�tico com valor inferior ao agendado", DescricaoErro);
                                        CadastrarNota = false;
                                    }
                                    else
                                    {
                                        if (ValogAgendado == ValorEfetivo)
                                            PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico previamente agendado: {1:dd/MM/yyyy} referente a {2}",
                                                  DateTime.Now,
                                                  DataPag,
                                                  CompServ);
                                        else
                                            PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico previamente agendado com diverg�ncia de valor:\r\n      Data agendada: {1:dd/MM/yyyy} referente a {2} \r\n      Valor agendado: {3:n2}\r\n      Valor efetivo:{4:n2}",
                                                  DateTime.Now,
                                                  DataPag,
                                                  CompServ,
                                                  ValogAgendado,
                                                  ValorEfetivo);

                                        if (ValogAgendado < ValorEfetivo)
                                        {
                                            string obs = string.Format("O Valor do d�bito autom�tico foi superior ao programado {0:n2} -> {1:n2} Diferen�a inclu�da", ValogAgendado, ValorEfetivo);
                                            if (!ChequePrevisto.AjustarValor(ValorEfetivo, obs))
                                            {
                                                reportarErro = false;
                                                throw new Exception("Erro no ajusto do cheque");
                                            }
                                            new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoComValorIncorreto, PGFrow.PGF_CON, CCD, CompServ, "D�bito autom�tico com valor superior ao agendado", obs);
                                        }

                                        ChequePrevisto.AmarrarCCD(CCD);

                                        CadastrarNota = false;

                                        //verificar se tem avisos para remover
                                        if (rowPags.Length > 1)
                                            foreach (dPagamentosPeriodicos.PAGamentosRow rowLimpar in rowPags)

                                                if (!rowLimpar.IsCHE_CCDNull())
                                                {
                                                    Framework.Alarmes.Aviso Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoComValorIncorreto, rowLimpar.CHE_CCD);
                                                    if (Aviso.Encontrado)
                                                        Aviso.Delete();
                                                }
                                    }                                    
                                }
                                else
                                {
                                    string DescricaoErro = string.Format("Debito autom�tico em duplicidade.\r\n     Compet�ncia:{0}", CompServ);
                                    new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoPulado, PGFrow.PGF_CON, PGFrow.PGF, CompServ, "D�bito autom�tico em duplicidade", DescricaoErro);

                                }
                            }
                }

                if (CadastrarNota)
                {
                    ContaPagar.dNOtAs.dNOtAsStX(EMPTipo).IncluiNotaSimples(0, DataPag, DataPag, CompServ, NOATipo.PagamentoPeriodico, "", PGFrow.PGF_CON,
                        PAGTipo.DebitoAutomatico, "", false, PGFrow.PGF_PLA, PGFrow.PGF_SPL, PGFrow.PGFDescricao,
                        Math.Round(fator * PGFrow.PGFValor, 2, MidpointRounding.AwayFromZero), FRN, false, dNOtAs.TipoChave.PGF, PGFrow.PGF, CCD, 0,
                        (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                    PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico n�o agendado em {1:dd/MM/yyyy} referente a {2}",
                                              DateTime.Now,
                                              DataPag,
                                              CompServ);
                };
                

                CompServ++;
                PGFrow.PGFProximaCompetencia = CompServ.CompetenciaBind;

                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                PGFrow.AcceptChanges();                
                dPagamentosPeriodicos.PAGamentos.Clear();
                dPagamentosPeriodicos.NOtAs.Clear();
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
                dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
                TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                TableAdapter.VircatchSQL(e);
                if (reportarErro)
                    throw e;
                return false;
            }
            //AjustarAGBContas();
            CadastrarProximo();
            //CadastrarProximoDebito(PGFForma);
            return true;
         * 
         * 
         * 
         * 
         * 
         * 
         * 
        */

        

        /// <summary>
        /// esta fun��o deve ser chamada quando for detectado o lan�amento na conta. Ela n�o cadastra mas da baixa no cheque
        /// </summary>
        /// <param name="DataPag"></param>
        /// <param name="ValorEfetivo"></param>
        /// <param name="CCD"></param>
        /// <param name="ABSCompServ"></param>
        /// <returns></returns>
        public override bool ConciliaDebitoAutomatico(DateTime DataPag, decimal ValorEfetivo, int CCD, Abstratos.ABS_Competencia ABSCompServ = null)
        {
            Competencia CompServ = (Competencia)ABSCompServ;
            if (ValorEfetivo <= 0)
                return false;

            if (CompServ == null)
            {
                int deltaData = (DataPag - ProximaData).Days;
                int deltaMeses = (int)Math.Round(deltaData / 30M);
                CompServ = ProximaCompetencia.Add(deltaMeses);
            }
            TipoPagPeriodico PGFForma = (TipoPagPeriodico)PGFrow.PGFForma;

            if ((PGFForma != TipoPagPeriodico.DebitoAutomatico)
                &&
                (PGFForma != TipoPagPeriodico.DebitoAutomaticoConta)
                //&&
                //(PGFForma != TipoPagPeriodico.Boleto)
                )
            {

                Framework.Alarmes.Aviso Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, CCD);
                if (!Aviso.Encontrado)
                {
                    string DescricaoErro = string.Format("Ocorreu o d�bito autom�tico para um pagamento com forma de pagamento distinta\r\nDetalhes:\r\n  Forma prevista:{0}\r\n  Data:{1:dd/MM/yyyy}\r\n"
                                                , PGFForma, DataPag);
                    Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, PGFrow.PGF_CON, CCD, CompServ, "D�bito autom�tico n�o previsto.", DescricaoErro);
                }
                return false;
            }
            
            dPagamentosPeriodicos.PAGamentos.Clear();
            if (dPagamentosPeriodicos.NOtAsTableAdapter.FillByPGFComp(dPagamentosPeriodicos.NOtAs, PGFrow.PGF, CompServ.CompetenciaBind) == 0)
            {
                string DescricaoErro = string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico n�o agendado.\r\n      Data: {1:dd/MM/yyyy} ({2})\r\n      Valor:{3:n2}",
                                                                   DateTime.Now,
                                                                   DataPag,
                                                                   CompServ,
                                                                   ValorEfetivo);
                PGFrow.PGFHistorico += DescricaoErro;
                new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoNaoPrevisto, PGFrow.PGF_CON, CCD, CompServ, "D�bito n�o previsto", DescricaoErro);
                return false;
            }
            dPagamentosPeriodicos.NOtAsRow Notasrow = dPagamentosPeriodicos.NOtAs[0];
            dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGFCompet(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF, CompServ.CompetenciaBind);
            bool reportarErro = true;
            bool notaAgendada = false;
            //dPagamentosPeriodicos.PAGamentosRow[] rowPags = Notasrow.GetPAGamentosRows();
            dPagamentosPeriodicos.PAGamentosRow rowPagCHE = null;
            foreach (dPagamentosPeriodicos.PAGamentosRow rowCandidato in dPagamentosPeriodicos.PAGamentos)
            {
                PAGTipo Tipo = (PAGTipo)rowCandidato.PAGTipo;
                if (Tipo == PAGTipo.DebitoAutomatico)
                {
                    if ((CHEStatus)rowCandidato.CHEStatus == CHEStatus.DebitoAutomatico)
                    {
                        notaAgendada = true;
                        rowPagCHE = rowCandidato;
                        break;
                    }
                    else if ((CHEStatus)rowCandidato.CHEStatus == CHEStatus.CadastradoBloqueado)
                    {
                        rowPagCHE = rowCandidato;
                        break;
                    }
                }
            }
            if (rowPagCHE == null)
                return false;
            try
            {
                TableAdapter.AbreTrasacaoSQL(string.Format("ContaPagar PagamentoPeriodico - 322 CCD = {0}", CCD),
                                             dPagamentosPeriodicos.PaGamentosFixosTableAdapter,
                                             dPagamentosPeriodicos.NOtAsTableAdapter,
                                             dPagamentosPeriodicos.PAGamentosTableAdapter);               
                Cheque ChequePrevisto = new Cheque(rowPagCHE.PAG_CHE, Cheque.TipoChave.CHE);
                decimal ValogAgendado = ChequePrevisto.CHErow.CHEValor;
                if (notaAgendada)
                {
                    if (ValogAgendado > ValorEfetivo)
                    {
                        Cheque[] ChequesParciais = ChequePrevisto.Dividir(ValorEfetivo);
                        if (ChequesParciais == null)
                        {
                            reportarErro = false;
                            throw new Exception("Cheque n�o pode ser dividido");
                        }
                        ChequesParciais[0].AmarrarCCD(CCD);
                        string DescricaoErro = string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico com valor inferior ao previamente agendado.\r\n      Agendamento: {1} {2:dd/MM/yyyy} referente a {3}\r\n      Valor debitado:{4}",
                                               DateTime.Now,
                                               ValogAgendado,
                                               DataPag,
                                               CompServ,
                                               ValorEfetivo);
                        PGFrow.PGFHistorico += DescricaoErro;
                        new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoComValorIncorreto, PGFrow.PGF_CON, CCD, CompServ, "D�bito autom�tico com valor inferior ao agendado", DescricaoErro);
                    }
                    else
                    {
                        if (ValogAgendado == ValorEfetivo)
                            PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico previamente agendado: {1:dd/MM/yyyy} referente a {2}",
                                  DateTime.Now,
                                  DataPag,
                                  CompServ);
                        else
                        {
                            PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico previamente agendado com diverg�ncia de valor:\r\n      Data agendada: {1:dd/MM/yyyy} referente a {2} \r\n      Valor agendado: {3:n2}\r\n      Valor efetivo:{4:n2}",
                                  DateTime.Now,
                                  DataPag,
                                  CompServ,
                                  ValogAgendado,
                                  ValorEfetivo);
                            string obs = string.Format("O Valor do d�bito autom�tico foi superior ao programado {0:n2} -> {1:n2} Diferen�a inclu�da", ValogAgendado, ValorEfetivo);
                            if (!ChequePrevisto.AjustarValor(ValorEfetivo, obs))
                            {
                                reportarErro = false;
                                throw new Exception("Erro no ajusto do cheque");
                            }
                            new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoComValorIncorreto, PGFrow.PGF_CON, CCD, CompServ, "D�bito autom�tico com valor superior ao agendado", obs);
                        }

                        ChequePrevisto.AmarrarCCD(CCD);

                        //verificar se tem avisos para remover
                        //if (rowPags.Length > 1)
                        foreach (dPagamentosPeriodicos.PAGamentosRow rowLimpar in dPagamentosPeriodicos.PAGamentos)

                            if (!rowLimpar.IsCHE_CCDNull())
                            {
                                Framework.Alarmes.Aviso Aviso = new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoComValorIncorreto, rowLimpar.CHE_CCD);
                                if (Aviso.Encontrado)
                                    Aviso.Delete();
                            }
                    }
                }
                else
                {
                    if (ValogAgendado != ValorEfetivo)
                        ChequePrevisto.AjustarValor(ValorEfetivo, "Valor ajustado pelo d�bito\r\n");
                    PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} D�bito autom�tico SEM agendamento: {1:dd/MM/yyyy} referente a {2}",
                                  DateTime.Now,
                                  DataPag,
                                  CompServ);
                    ChequePrevisto.AmarrarCCD(CCD);
                    Nota NotaBloc = new Nota(Notasrow.NOA);
                    NotaBloc.Destrava(DataPag, DataPag, ValorEfetivo);
                }

                //                              }
                //else
                //{
                //    string DescricaoErro = string.Format("Debito autom�tico em duplicidade.\r\n     Compet�ncia:{0}", CompServ);
                //    new Framework.Alarmes.Aviso(Framework.datasets.ATVTipo.DebitoAutomaticoPulado, PGFrow.PGF_CON, PGFrow.PGF, CompServ, "D�bito autom�tico em duplicidade", DescricaoErro);

                //                                }




                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                PGFrow.AcceptChanges();
                dPagamentosPeriodicos.PAGamentos.Clear();
                dPagamentosPeriodicos.NOtAs.Clear();
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
                dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
                TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                TableAdapter.VircatchSQL(e);
                if (reportarErro)
                    throw e;
                return false;
            }
            //AjustarAGBContas();
            CadastrarProximo();
            //CadastrarProximoDebito(PGFForma);
            return true;

        }

        /// <summary>
        /// ValorProximoDebitoProgramado
        /// </summary>
        /// <returns></returns>
        public override decimal ValorProximoDebitoProgramado()
        {
            dPagamentosPeriodicos.PAGamentos.Clear();
            dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
            dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
            dPagamentosPeriodicos.NOtAsRow ultimaNota = null;
            dPagamentosPeriodicos.PAGamentosRow rowPagCHE = null;
            int iNotasChecar = Math.Min(dPagamentosPeriodicos.NOtAs.Count - 1, 1);
            while (iNotasChecar >= 0)
            {
                ultimaNota = dPagamentosPeriodicos.NOtAs[iNotasChecar];
                dPagamentosPeriodicos.PAGamentosRow[] rowPags = ultimaNota.GetPAGamentosRows();                
                foreach (dPagamentosPeriodicos.PAGamentosRow rowCandidato in rowPags)
                {
                    PAGTipo Tipo = (PAGTipo)rowCandidato.PAGTipo;
                    if ( 
                        (Tipo == PAGTipo.DebitoAutomatico) 
                          && 
                        ((CHEStatus)rowCandidato.CHEStatus == CHEStatus.DebitoAutomatico)
                       )
                    {
                        rowPagCHE = rowCandidato;
                        break;
                    }
                }
                if (rowPagCHE != null)
                    break;
                else
                    iNotasChecar--;
            }
            if (rowPagCHE != null)
                return rowPagCHE.PAGValor;
            return 0;
        }

        
        

        #endregion

        /*
        /// <summary>
        /// Linguagem
        /// </summary>
        public static LinguagemCodBar _LinguagemCB = LinguagemCodBar.Indefinida;

        /// <summary>
        /// 
        /// </summary>
        public static LinguagemCodBar LinguagemCB
        {
            get 
            {
                if (_LinguagemCB == LinguagemCodBar.Indefinida)
                {
                    try
                    {
                        _LinguagemCB = (LinguagemCodBar)Enum.Parse(typeof(CodBar.LinguagemCodBar), CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Linguagem codbar"));
                    }
                    catch
                    {
                        _LinguagemCB = CodBar.LinguagemCodBar.EPL2;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Linguagem codbar", LinguagemCB.ToString());
                    }
                }
                return _LinguagemCB;
            }
        }
        */        

        /// <summary>
        /// 
        /// </summary>
        public string ErroAgendamento;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="Valor"></param>
        /// <returns></returns>
        public bool Atualizar(int dia, decimal Valor)
        {
            if (!Encontrado)
                return false;
            else
            {
                if ((PGFrow.PGFValor != Valor) || (PGFrow.PGFDia != dia))
                {
                    PGFrow.PGFValor = Valor;
                    PGFrow.PGFDia = dia;
                    Salvar(string.Format("Recalculado na Folha: Dia: {0} Valor: {1:n2}", dia, Valor));
                    return true;
                }
                return false;
            }
        }
        

        /// <summary>
        /// Salva as altera��es
        /// </summary>
        /// <param name="justificativa"></param>
        /// <returns></returns>
        public override bool Salvar(String justificativa)
        {                        
            PGFrow.PGFHistorico = string.Format("{0}{1:dd/MM/yyyy HH:mm:ss} {2} Altera��o de cadastro\r\n                    Motivo:{3}\r\n                    Pr�xima compet�ncia: {4} Vencimento: {5:dd/MM/yyyy} ({6})",
                                                 PGFrow.IsPGFHistoricoNull() ? "": PGFrow.PGFHistorico+"\r\n",
                                                 DateTime.Now,
                                                 Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                 justificativa,
                                                 ProximaCompetencia,
                                                 ProximaData,
                                                 (Tipo == TipoPagPeriodico.DebitoAutomaticoConta) ? "d�bito autom�tico" : "conta");
            dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
            PGFrow.AcceptChanges();
            if (dPagamentosPeriodicos.NotasCarregadas || !dPagamentosPeriodicos.Coletivo)
                CadastrarProximo();
            return true;
        }

        /// <summary>
        /// Inclui uma obs no hist�rico
        /// </summary>
        /// <param name="Obs"></param>
        /// <param name="IdentificarComoObs"></param>
        public void GravaHistorico(string Obs,bool IdentificarComoObs = true)
        {
            string mascara = IdentificarComoObs ? "\r\n{0:dd/MM/yyyy HH:mm:ss} {1} Obs:\r\n     {2}" : "\r\n{0:dd/MM/yyyy HH:mm:ss} - {1} - {2}";            
            PGFrow.PGFHistorico += string.Format(mascara
                                                 , DateTime.Now,
                                                 Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                 Obs);
            dPagamentosPeriodicos.PaGamentosFixosTableAdapter.EmbarcaEmTransST();
            dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
            PGFrow.AcceptChanges();                     
        }

        /*
        /// <summary>
        /// Grava no hist�rico e na agenda
        /// </summary>
        /// <param name="MenH"></param>
        /// <param name="MenA">Mensagem da Agenda, se null usa a mesma do hist�rico</param>
        internal void GravaHistorico_Agenda(string MenH,string MenA = null)
        {            
            PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} {2}"
                                                 , DateTime.Now,
                                                 Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                 MenH);
            dPagamentosPeriodicos.PaGamentosFixosTableAdapter.EmbarcaEmTransST();
            dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
            PGFrow.AcceptChanges();
            dPagamentosPeriodicos.AGendaBaseTableAdapter.EmbarcaEmTransST();
            rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} {2}"
                                                 , DateTime.Now,
                                                 Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                 MenA ?? MenH);
            dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(rowAGB);
            rowAGB.AcceptChanges();            
        }
        */

        /// <summary>
        /// Carrega o dataset est�tico com os ativos
        /// </summary>
        /// <returns></returns>
        public static new int CarregaPGFAtivos()
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CarregaPGFAtivos",true);
            int Retorno = dPagamentosPeriodicos.dPagamentosPeriodicosSt.CarregaAtivos(false);
            CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
            return Retorno;
        }

        /// <summary>
        /// Carrega o dataset est�tico com os ativos por Tipo
        /// </summary>
        /// <returns></returns>
        public static int CarregaPGFAtivos(TipoPagPeriodico Tipo, EMPTipo EMPTipo, Competencia CompCorte = null)
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CarregaPGFAtivos X", true);
            int Retorno = EMPTipo == EMPTipo.Local ? dPagamentosPeriodicos.dPagamentosPeriodicosSt.CarregaAtivos(Tipo, CompCorte) : dPagamentosPeriodicos.dPagamentosPeriodicosStF.CarregaAtivos(Tipo, CompCorte);
            CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
            return Retorno;
        }

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_PGFrow">Linha mae externa</param>
        public PagamentoPeriodico(dPagamentosPeriodicos.PaGamentosFixosRow _PGFrow)
        {
            PGFrow = _PGFrow;
            dPagamentosPeriodicos = (dPagamentosPeriodicos)PGFrow.Table.DataSet;
            //datasetColetivo = true;
        }

        /// <summary>
        /// construtor, usado pelo abs
        /// </summary>
        /// <param name="PGF"></param>
        public PagamentoPeriodico(int PGF)
            : this(PGF, false, null)
        { 
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="PGF">Chave</param>
        /// <param name="ComPool">Usar o pool</param>
        /// <param name="_EMPTProc"></param>
        public PagamentoPeriodico(int PGF, bool ComPool = false, EMPTProc _EMPTProc = null) :
            base(PGF, ComPool, _EMPTProc)
        {            
            PGFrow = null;
            if (ComPool)            
                PGFrow = dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos.FindByPGF(PGF);
            if (PGFrow == null)
            {                                
                EMPTProc1.EmbarcaEmTrans(dPagamentosPeriodicos.PaGamentosFixosTableAdapter);
                if (dPagamentosPeriodicos.PaGamentosFixosTableAdapter.FillByPGF(dPagamentosPeriodicos.PaGamentosFixos, PGF) == 1)
                    PGFrow = dPagamentosPeriodicos.PaGamentosFixos[0];                
            }
        }        

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="RGI"></param>
        /// <param name="FRN"></param>
        /// <param name="Manual"></param>
        public PagamentoPeriodico(long RGI, int FRN, bool Manual)
        {
            if (dPagamentosPeriodicos.PaGamentosFixosTableAdapter.FillByDebito(dPagamentosPeriodicos.PaGamentosFixos, RGI, FRN) >= 1)            
                PGFrow = dPagamentosPeriodicos.PaGamentosFixos[0];
            if((!Encontrado) && (Manual))
                if (dPagamentosPeriodicos.PaGamentosFixosTableAdapter.FillByIdent(dPagamentosPeriodicos.PaGamentosFixos,FRN, (int)RGI) >= 1)
                    PGFrow = dPagamentosPeriodicos.PaGamentosFixos[0];
        }
       
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="TipoPadrao"></param>
        /// <param name="_EMPTProc"></param>
        /// <param name="usarPool"></param>
        public PagamentoPeriodico(int CON, TipoPagPeriodico TipoPadrao, EMPTProc _EMPTProc = null, bool usarPool = false)
            : base(CON, TipoPadrao, _EMPTProc, usarPool)
        {           
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="TipoPadrao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="Valor"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public bool CadastraPGF(int CON, TipoPagPeriodico TipoPadrao, DateTime Vencimento, decimal Valor, Competencia comp)
        {
            if (PGFrow != null)
                return false;
            string PLA = null;
            int SPL = 0;
            string DescricaoPgf = null;

            TableAdapter.EmbarcaEmTrans(dPagamentosPeriodicos.PaGamentosFixosTableAdapter);

            string _Nominal = "";

            switch (TipoPadrao)
            {
                case TipoPagPeriodico.FolhaPIS:
                    PLA = "220006";
                    SPL = EMPTProc1.EMP == 1 ? 43 : 39;
                    DescricaoPgf = "PIS sobre pagamento 8301";
                    _Nominal = "SECRETARIA DA RECEITA FEDERAL";
                    break;
                case TipoPagPeriodico.FolhaIR:
                    PLA = "220006";
                    SPL = EMPTProc1.EMP == 1 ? 43 : 39;
                    DescricaoPgf = "IR sobre folha de pagamento";
                    _Nominal = "SECRETARIA DA RECEITA FEDERAL";
                    break;
                case TipoPagPeriodico.FolhaFGTS:
                    PLA = "220005";
                    SPL = 12;
                    DescricaoPgf = "Fgts";
                    _Nominal = "Fgts";
                    break;
                case TipoPagPeriodico.Folha:
                    PLA = "220001";
                    SPL = EMPTProc1.EMP == 1 ? 28 : 14;
                    DescricaoPgf = "Sal�rio";
                    _Nominal = "Folha de pagamento";
                    break;
                case TipoPagPeriodico.Adiantamento:
                    PLA = "220002";
                    SPL = EMPTProc1.EMP == 1 ? 6 : 135;
                    DescricaoPgf = "Adiantamento Quinzenal";
                    _Nominal = "Folha de pagamento";
                    break;
                default:
                    throw new NotImplementedException(string.Format("N�o utilizar este construtor para Tipo {0}", TipoPadrao));
            }

            PGFrow = dPagamentosPeriodicos.PaGamentosFixos.NewPaGamentosFixosRow();
            PGFrow.PGF_CON = CON;
            PGFrow.PGF_PLA = PLA;
            PGFrow.PGF_SPL = SPL;
            PGFrow.PGFCompetI = comp.CompetenciaBind;
            PGFrow.PGFCompetNaDescricao = true;
            PGFrow.PGFDATAI = DateTime.Now;
            PGFrow.PGFDescricao = DescricaoPgf;
            PGFrow.PGFDia = Vencimento.Day;
            PGFrow.PGFForma = (int)TipoPadrao;
            PGFrow.PGFHistorico = string.Format("{0:dd/MM/yyyy HH:mm:ss} = Cadastrado automaticamente\r\n", DateTime.Now);
            PGFrow.PGFI_USU = DSCentral.USUX(EMPTProc1.Tipo);
            PGFrow.PGFNominal = _Nominal; 
            PGFrow.PGFParcelaNaDesc = false;
            PGFrow.PGFProximaCompetencia = comp.CompetenciaBind;            
            PGFrow.PGFRef = (new Competencia(Vencimento) - comp);
            PGFrow.PGFStatus = (int)StatusPagamentoPeriodico.Ativado;
            PGFrow.PGFValor = Valor;
            dPagamentosPeriodicos.PaGamentosFixos.AddPaGamentosFixosRow(PGFrow);
            dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
            PGFrow.AcceptChanges();
            return true;
        }

        

        
        /*
        private int USUAtual 
        {
            get
            {
                if (EMPTipo.Tipo == EMPT.EMPTipo.Local)
                    return Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                else
                    return Framework.datasets.dUSUarios.USUequivalente(Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU);
            } 
        }*/

        /// <summary>
        /// 
        /// </summary>
        protected override TipoImposto TipoISS
        {
            get { return Framework.DSCentral.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA; }
        }

       
        /*
        private string ComandoBuscaCheque =
"SELECT     TOP (1) CHEques.CHEStatus\r\n" +
"FROM         NOtAs INNER JOIN\r\n" +
"                      PAGamentos ON NOtAs.NOA = PAGamentos.PAG_NOA INNER JOIN\r\n" +
"                      CHEques ON PAGamentos.PAG_CHE = CHEques.CHE\r\n" +
"WHERE     (NOtAs.NOA_PGF = @P1) AND (PAGamentos.PAGIMP IS NULL) AND (CHEques.CHEEmissao IS NULL)\r\n" +
"AND (CHEques.CHEStatus not in (9))" +
"ORDER BY NOtAs.NOADataEmissao DESC;";

        /*
        private string ComandoBuscaUltimaNota =
"SELECT TOP (1) CHEques.CHEStatus, NOtAs.NOACompet\r\n" + 
"FROM     NOtAs INNER JOIN\r\n" + 
"                  PAGamentos ON NOtAs.NOA = PAGamentos.PAG_NOA INNER JOIN\r\n" + 
"                  CHEques ON PAGamentos.PAG_CHE = CHEques.CHE\r\n" + 
"WHERE  (NOtAs.NOA_PGF = @P1) AND (PAGamentos.PAGIMP IS NULL) AND (CHEques.CHEStatus NOT IN (9))\r\n" + 
"ORDER BY NOtAs.NOACompet DESC;";         
        

        private string ComandoBuscaUltimaNota =
"SELECT        TOP (1) CHEques.CHEStatus, NOtAs.NOACompet, NOtAs.NOAStatus\r\n" + 
"FROM            CHEques INNER JOIN\r\n" + 
"                         PAGamentos ON CHEques.CHE = PAGamentos.PAG_CHE RIGHT OUTER JOIN\r\n" + 
"                         NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA\r\n" + 
"WHERE        (NOtAs.NOA_PGF = @P1) and ((CHEques.CHEStatus <>  9) or (CHEques.CHEStatus is null))\r\n" + 
"ORDER BY NOtAs.NOACompet DESC;";


"SELECT TOP (1) CHEques.CHEStatus, NOtAs.NOACompet,NOtAs.NOAStatus\r\n" + 
"FROM     NOtAs INNER JOIN\r\n" + 
"                  PAGamentos ON NOtAs.NOA = PAGamentos.PAG_NOA INNER JOIN\r\n" + 
"                  CHEques ON PAGamentos.PAG_CHE = CHEques.CHE\r\n" + 
"WHERE  (NOtAs.NOA_PGF = @P1) AND (CHEques.CHEStatus NOT IN (9))\r\n" + 
"ORDER BY NOtAs.NOACompet DESC;";
        

        private string ComandoBuscaUltimaNota =
"SELECT        TOP (1) NOA\r\n" +
"FROM            NOtAs\r\n" +
"WHERE        (NOtAs.NOA_PGF = @P1)\r\n" +
"ORDER BY NOtAs.NOACompet DESC;";
*/
        /*
        private string DescricaoInterna(Competencia comp = null)
        {
            if (comp == null)
                comp = new Competencia(PGFrow.PGFProximaCompetencia);
            string retorno = PGFrow.PGFDescricao;
            int tamanhoOriginal = retorno.Length;
            if ((PGFrow.PGFParcelaNaDesc) && (!PGFrow.IsPGFCompetFNull()))
                retorno += string.Format(" ({0}/{1})", 1 + comp.DeltaMeses(PGFrow.PGFCompetI), 1 + Competencia.DeltaMeses(PGFrow.PGFCompetI, PGFrow.PGFCompetF));
            if (PGFrow.PGFCompetNaDescricao)
                retorno += string.Format(" ref.{0}", comp);
            if (retorno.Length > 50)
            {
                int remover = retorno.Length - 50;
                retorno = retorno.Remove(tamanhoOriginal - remover - 1, remover);
            }
            return retorno;

        }*/

        
        /*
        /// <summary>
        /// Forma de pagamento do peri�dico
        /// </summary>
        public TipoPagPeriodico PGFForma
        {
            get { return (TipoPagPeriodico)PGFrow.PGFForma; }
        }

        [Obsolete("Remover ap�s a revis�o de todos")]
        private List<TipoPagPeriodico> FormasRevisadas
        {
            get
            {
                return new List<TipoPagPeriodico>(new TipoPagPeriodico[] { TipoPagPeriodico.Boleto,
                                                                                                   TipoPagPeriodico.Conta,
                                                                                                   //TipoPagPeriodico.DebitoAutomatico,
                                                                                                   TipoPagPeriodico.DebitoAutomaticoConta,
                                                                                                   TipoPagPeriodico.Folha,
                                                                                                   TipoPagPeriodico.Adiantamento});
            }
        }*/

        

        private enum acoesCadastraProximaNota { CadastrarNotaAtual, CadastrarProximaNota, OkJaCadastrado };

        #region Acumular
        private Nota[] _NotasAcumular;

        private decimal _TotalAcumulado;

        private Nota[] NotasAcumular
        {
            get
            {
                CarregaAcumulados(false);
                return _NotasAcumular;
            }
        }

        private decimal TotalAcumulado
        {
            get
            {
                CarregaAcumulados(false);
                return _TotalAcumulado;
            }
        }

        private void CarregaAcumulados(bool Recalcular)
        {
            if ((_NotasAcumular == null) || Recalcular)
            {
                string comando =
     "SELECT        NOtAs.NOA, PAGamentos.PAGValor\r\n" +
"FROM            NOtAs INNER JOIN\r\n" +
"                         PAGamentos ON NOtAs.NOA = PAGamentos.PAG_NOA INNER JOIN\r\n" +
"                         CHEques ON PAGamentos.PAG_CHE = CHEques.CHE\r\n" +
"WHERE        (NOtAs.NOA_PGF = @P1) AND (NOtAs.NOAStatus = 6) AND (CHEques.CHEStatus = 20)\r\n" +
"ORDER BY NOtAs.NOACompet DESC;";
                System.Data.DataTable DT = EMPTProc1.STTA.BuscaSQLTabela(comando);
                _NotasAcumular = new Nota[DT.Rows.Count];
                _TotalAcumulado = 0;
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    _NotasAcumular[i] = new Nota((int)DT.Rows[i]["NOA"]);
                    _TotalAcumulado += (decimal)DT.Rows[i]["PAGValor"];
                }
            }
        }
        #endregion

        /*
        /// <summary>
        /// Busca/Cadastra nota do periodico com esta competencia
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="PodeCriar">Perminte ou n�o criar a nota</param>
        /// <returns></returns>
        [Obsolete("usar BuscaCriaNotaProc",true)]
        public Nota BuscaCriaNota(Competencia comp,bool PodeCriar = true)
        {
            if (dPagamentosPeriodicos.NOtAs.Count == 0)
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGF);
            dPagamentosPeriodicos.NOtAsRow rowNOA = dPagamentosPeriodicos.Busca_rowNOtAs(comp.CompetenciaBind,PGF);
            if (rowNOA != null)
                return new Nota(rowNOA.NOA, EMPTProc1);
            else
            {
                if (PodeCriar)
                    return new Nota(PGFrow.PGF_CON,
                                 null,
                                 0,
                                 DateTime.Now,
                                 0,
                                 NOATipo.Folha,
                                 PLA,
                                 PGFrow.PGF_SPL,
                                 PGFrow.PGFDescricao,
                                 comp,
                                 PAGTipo.Folha,
                                 NOAStatus.SemPagamentos,
                                 null,
                                 null,
                                 PGF,
                                 EMPTProc1);
                else
                    return null;
            }
        }
        */
        

        

        /*
        /// <summary>
        /// Cadastra o pr�xima nota e ajusta a proxima compet�ncia
        /// </summary>
        /// <param name="NotaCheque"></param>
        /// <returns>Retorna falso s� se der erro. Se n�o tiver nada para cadastrar retorna true</returns>
        protected override bool CadastraProximaNota(enumNotaCheque NotaCheque)        
        {            
            //Remover ap�s a revis�o
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CadastraProximaNota 1");
            if (!FormasRevisadas.Contains(PGFForma))
                throw new Exception("Forma nao revisada");

            if (!StatusAtivos.Contains(Status))
                return true;

            if (PGFrow.PGFValor <= 0)
            {
                UltimoErro = new Exception("Valor negativo para PGFValor");
                return false;
            }            
             
            acoesCadastraProximaNota acao;                       
            NOAStatus? UltimoNOAStatus = null;
            bool RegistrarAcumuladoNoHistorico = false;
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Ultima nota",true);
            int? NOAultima = BuscaUltimaNota();
            CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
            if (!NOAultima.HasValue)
                acao = acoesCadastraProximaNota.CadastrarNotaAtual;                        
            else
            {
                Nota NotaUltima = new Nota(NOAultima.Value,EMPTipo.Tipo);                
                ProximaCompetencia = NotaUltima.Competencia;                                
                UltimoNOAStatus = NotaUltima.Status;                
                switch (NotaUltima.VerificaFase())
                {
                    case Enumeracoes.FasePeriodico.AguardaEmissaoCheque:
                        acao = acoesCadastraProximaNota.OkJaCadastrado;
                        break;
                    case Enumeracoes.FasePeriodico.CadastrarProxima:
                        acao = acoesCadastraProximaNota.CadastrarProximaNota;
                        break;
                    case Enumeracoes.FasePeriodico.ComNotaprov:
                        if ((UltimoNOAStatus.Value == NOAStatus.NotaAcumulada) || (UltimoNOAStatus.Value == NOAStatus.CadastradaAjustarValor))
                        {
                            acao = acoesCadastraProximaNota.CadastrarProximaNota;
                            RegistrarAcumuladoNoHistorico = true;
                        }
                        else
                            acao = acoesCadastraProximaNota.OkJaCadastrado;
                        break;
                    default:
                        throw new NotImplementedException(string.Format("N�o implementado para fase = {0}", NotaUltima.VerificaFase()));
                }                
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CadastraProximaNota 2");
            if (acao == acoesCadastraProximaNota.OkJaCadastrado)
            {
                if (PGFrow.RowState == System.Data.DataRowState.Modified)
                {
                    try
                    {
                        TableAdapter.AbreTrasacaoSQL("PagamentosPeriodicos.cs CadastraProximaNota - 961", dPagamentosPeriodicos.PaGamentosFixosTableAdapter);
                        //STTA().AbreTrasacaoLF("PagamentosPeriodicos.cs CadastraProximaNota - 961", dPagamentosPeriodicos.PaGamentosFixosTableAdapter);

                        
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Update",true);
                        dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                        CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                        PGFrow.AcceptChanges();
                        EMPTipo.Commit();
                    }
                    catch (Exception e)
                    {
                       EMPTipo.Vircatch(e);
                        throw;
                    }
                }
                return true;
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CadastraProximaNota 3");
            Competencia Comp = ProximaCompetencia;

            if (acao == acoesCadastraProximaNota.CadastrarProximaNota)
            {
                Comp++;
                ProximaCompetencia = Comp;
            }

            Competencia CompPAG = Comp.CloneCompet(PGFrow.PGFRef);                                    
            DateTime DataPag = CompPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);

            int? FRN = PGFrow.IsPGF_FRNNull() ? (int?)null : PGFrow.PGF_FRN;
            System.Collections.ArrayList Retencoes = new System.Collections.ArrayList();
            if (!PGFrow.IsPGFISSNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoISS, PGFrow.PGFISS, CompontesBasicosProc.SimNao.Padrao));            
            if (cpfcnpj != null)
            {
                if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                {
                    if (!PGFrow.IsPGFINSSPNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSS, PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
                    if (!PGFrow.IsPGFIRNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IR, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
                    if (!PGFrow.IsPGFINSSTNull())
                        throw new Exception(string.Format("Erro no cadastro dos peri�dicos PGF = {0} - {1}",PGF,PGFrow.PGFDescricao));
                }
                else
                {
                    if (!PGFrow.IsPGFINSSPNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
                    if (!PGFrow.IsPGFIRNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IRPF, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
                    if (!PGFrow.IsPGFINSSTNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, PGFrow.PGFINSST, CompontesBasicosProc.SimNao.Padrao));
                }
            }
            if (!PGFrow.IsPGFPISCOFNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.PIS_COFINS_CSLL, PGFrow.PGFPISCOF, CompontesBasicosProc.SimNao.Padrao));
            string Nominal = PGFrow.IsPGFNominalNull() ? "" : PGFrow.PGFNominal;
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("CadastraProximaNota 4");
            try
            {
                TableAdapter.AbreTrasacaoSQL("ContaPagar PagamentoPeriodico - 864",
                                                                       dPagamentosPeriodicos.PaGamentosFixosTableAdapter,
                                                                       dPagamentosPeriodicos.NOtAsTableAdapter,
                                                                       dPagamentosPeriodicos.PAGamentosTableAdapter);             
                //STTA().AbreTrasacaoLF
                PAGTipo PAGTipo;
                string DadosPag = "";
                int CCT = 0;
                bool booPermiteAgrupar = true;
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("CadastraProximaNota 4 A");
                switch ((TipoPagPeriodico)PGFrow.PGFForma)
                {
                    case TipoPagPeriodico.ChequeDeposito:
                        PAGTipo = PAGTipo.deposito;
                        DadosPag = PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag;
                        break;
                    case TipoPagPeriodico.ChequeSindico:
                        PAGTipo = PAGTipo.sindico;
                        break;
                    case TipoPagPeriodico.Boleto:
                        PAGTipo = PAGTipo.boleto;
                        break;
                    case TipoPagPeriodico.Folha:
                    case TipoPagPeriodico.Adiantamento:
                        PAGTipo = PAGTipo.Folha;
                        break;
                    case TipoPagPeriodico.Cheque:                                                            
                    case TipoPagPeriodico.FolhaEletronico:
                    case TipoPagPeriodico.FolhaFGTS:
                    case TipoPagPeriodico.FolhaFGTSEletronico:
                    case TipoPagPeriodico.FolhaIR:
                    case TipoPagPeriodico.FolhaIREletronico:
                    case TipoPagPeriodico.FolhaPIS:
                    case TipoPagPeriodico.FolhaPISEletronico:
                        PAGTipo = PAGTipo.cheque;
                        break;
                    case TipoPagPeriodico.Conta:
                        PAGTipo = PAGTipo.Conta;
                        break;
                    case TipoPagPeriodico.DebitoAutomatico:                    
                    case TipoPagPeriodico.DebitoAutomaticoConta:
                        PAGTipo = PAGTipo.DebitoAutomatico;
                        booPermiteAgrupar = false;
                        break;
                    case TipoPagPeriodico.PECreditoConta:
                    case TipoPagPeriodico.PEOrdemPagamento:
                    case TipoPagPeriodico.PETituloBancario:
                    case TipoPagPeriodico.PETituloBancarioAgrupavel:
                    case TipoPagPeriodico.PETransferencia:
                        PAGTipo = (PAGTipo)(PGFrow.PGFForma);
                        if ((TipoPagPeriodico)PGFrow.PGFForma == TipoPagPeriodico.PEOrdemPagamento)
                            DadosPag = PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag;
                        CCT = PGFrow.PGF_CCT;
                        booPermiteAgrupar = false;
                        break;
                    default:
                        throw new NotImplementedException(string.Format("N�o implementado tratamento para TipoPagPeriodico = {0} em PagamentosPeriodico.cs", (TipoPagPeriodico)PGFrow.PGFForma));
                }

                
                int? novaNOA = null;
                if (NotaCheque == enumNotaCheque.aguarda)
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("IncluiNotaProvisoria");
                    novaNOA = ContaPagar.dNOtAs.dNOtAsStX(EMPTipo.Tipo).IncluiNotaProvisoria(DataPag, Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, booPermiteAgrupar, PGFrow.PGF_PLA, PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN, dNOtAs.TipoChave.PGF, PGFrow.PGF, DadosPag, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                }
                else
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("IncluiNotaSimples");
                    novaNOA = ContaPagar.dNOtAs.dNOtAsStX(EMPTipo.Tipo).IncluiNotaSimples(0, DataPag, DataPag, Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, DadosPag, booPermiteAgrupar, PGFrow.PGF_PLA, PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN.GetValueOrDefault(0), false, dNOtAs.TipoChave.PGF, PGFrow.PGF, CCT, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                }
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("CadastraProximaNota 4 C");
                string DescNF;
                string DescMotivo;         
                    //PAGTipo == PAGTipo.Conta ? "* AGUARDA CONTA * " : "* AGUARDA NF * ";
                switch (PAGTipo)
                {                                     
                    case PAGTipo.boleto:                        
                        DescMotivo = RegistrarAcumuladoNoHistorico ? "Conta Acumulada" : "Cheque emitido";
                        DescNF = "* AGUARDA NF * ";
                        break;                                     
                    case PAGTipo.DebitoAutomatico:
                        DescMotivo = RegistrarAcumuladoNoHistorico ? "Conta Acumulada" : "D�bito Agendado";
                        DescNF = "* AGUARDA CONTA * ";
                        break;
                    case PAGTipo.Conta:
                        DescMotivo = RegistrarAcumuladoNoHistorico ? "Conta Acumulada" : "Cheque emitido";
                        DescNF = "* AGUARDA CONTA * ";
                        break;
                    case PAGTipo.Folha:
                        DescMotivo = "Folha rodada";
                        DescNF = "* AGUARDA RODAR FOLHA * ";
                        break;
                    //case PAGTipo.Acumular:                                        
                    //case PAGTipo.TrafArrecadado:                     
                    default:
                        throw new NotImplementedException(string.Format("PAGTipo n�o tratado em CadastraProximaNota : {0}", PAGTipo));
                }
                
                PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} {2}, cadastrado pr�ximo {3}({4}) valor: {5:n2} vencimento {6:dd/MM/yyyy}",
                                                       DateTime.Now,
                                                       Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                       DescMotivo,
                                                       NotaCheque == enumNotaCheque.aguarda ? DescNF : "",
                                                       ProximaCompetencia,
                                                       PGFrow.PGFValor,
                                                       ProximaData);
                //if (!PGFrow.IsPGFCompetFNull())
                //  if (PGFrow.PGFProximaCompetencia > PGFrow.PGFCompetF)
                //{
                //  PGFrow.PGFStatus = (int)StatusPagamentoPeriodico.Encerrado;
                // }
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("UPDATE",true);
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                PGFrow.AcceptChanges();
                ReagendaAcumulados(DataPag, NotaCheque == enumNotaCheque.aguarda,0);

                CompontesBasicos.Performance.Performance.PerformanceST.Registra("CadastraProximaNota 6");
                //ATEN��O: n�o sei o motivo dar recarga das tabelas por isso mantive para os tipos que j� estavam. Para os demais ela n�o pode ser feita porque o dNotas � coletivos para v�rios objetos PagamentoPeriodico para mais velocidade.
                if (PGFForma.EstaNoGrupo(TipoPagPeriodico.Boleto, TipoPagPeriodico.Conta, TipoPagPeriodico.DebitoAutomaticoConta))
                {
                    dPagamentosPeriodicos.PAGamentos.Clear();
                    dPagamentosPeriodicos.NOtAs.Clear();
                    dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
                    dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
                }
                EMPTipo.Commit();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Recarga");
                if (!dPagamentosPeriodicos.TravarRecarga && novaNOA.HasValue)
                {
                    try
                    {
                        dPagamentosPeriodicos.NOtAsTableAdapter.ClearBeforeFill = false;
                        dPagamentosPeriodicos.PAGamentosTableAdapter.ClearBeforeFill = false;
                        dPagamentosPeriodicos.NOtAsTableAdapter.FillNovaNota(dPagamentosPeriodicos.NOtAs, novaNOA.Value);
                        dPagamentosPeriodicos.PAGamentosTableAdapter.FillByNovaNota(dPagamentosPeriodicos.PAGamentos, novaNOA.Value);
                    }
                    finally 
                    {
                        dPagamentosPeriodicos.NOtAsTableAdapter.ClearBeforeFill = true;
                        dPagamentosPeriodicos.PAGamentosTableAdapter.ClearBeforeFill = true;
                    }
                }
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("CadastraProximaNota 7");
            }
            catch (Exception e)
            {
               EMPTipo.Vircatch(e);
               throw new Exception("erro interno em CadastraProximaNota", e);
            }
            return true;
    
        }
        */
        
        /*
        /// <summary>
        /// Cadastra o pr�ximo cheque
        /// </summary>
        /// <param name="NotaCheque"></param>
        /// <returns>Retorna falso s� se der erro. Se n�o tiver nada para cadastrar retorna true</returns>
        [Obsolete("substitur por CadastraProximaNota")]
        protected override bool CadastrarProximoCheque(enumNotaCheque NotaCheque)
        {
            //ATEN��O
            //Esta fun��o deve ser descontinuada e substitu�da para CadastraProximaNota sendo feita a revis�o para cada PGFrow.PGFForma 

            if (FormasRevisadas.Contains(PGFForma))
                throw new Exception("Forma j� revisada");

            if (PGFrow.PGFValor <= 0)
                return false;
            Competencia Comp = ProximaCompetencia;
            Competencia CompPAG = Comp.CloneCompet(PGFrow.PGFRef);
            System.Data.DataRow Ultima = EMPTipo.STTA.BuscaSQLRow(ComandoBuscaCheque, PGFrow.PGF);
            if (Ultima != null)                            
                return false;
            
            DateTime DataPag = CompPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);

            int FRN = PGFrow.IsPGF_FRNNull() ? 0 : PGFrow.PGF_FRN;
            System.Collections.ArrayList Retencoes = new System.Collections.ArrayList();
            if (!PGFrow.IsPGFISSNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoISS, PGFrow.PGFISS, CompontesBasicosProc.SimNao.Padrao));
            if (!PGFrow.IsPGFINSSPNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
            if (!PGFrow.IsPGFINSSTNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, PGFrow.PGFINSST, CompontesBasicosProc.SimNao.Padrao));
            if (cpfcnpj != null)
            {
                if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                {
                    if (!PGFrow.IsPGFIRNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IR, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
                }
                else
                {
                    if (!PGFrow.IsPGFIRNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IRPF, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
                }
            }
            if (!PGFrow.IsPGFPISCOFNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.PIS_COFINS_CSLL, PGFrow.PGFPISCOF, CompontesBasicosProc.SimNao.Padrao));
            string Nominal = PGFrow.IsPGFNominalNull() ? "" : PGFrow.PGFNominal;
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
            if (!PGFrow.IsPGF_CCTCredNull())
                Nominal = Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(PGFrow.PGF_CON).CONNome;
            //*** MRC - TERMINO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
            try
            {
                TableAdapter.AbreTrasacaoSQL("ContaPagar PagamentoPeriodico - 864",
                                                                       dPagamentosPeriodicos.PaGamentosFixosTableAdapter,
                                                                       dPagamentosPeriodicos.NOtAsTableAdapter,
                                                                       dPagamentosPeriodicos.PAGamentosTableAdapter);
                //*** MRC - INICIO - PAG-FOR (2) ***
                PAGTipo PAGTipo;
                string DadosPag = "";
                int CCT = 0;
                bool booPermiteAgrupar = true;

                switch ((TipoPagPeriodico)PGFrow.PGFForma)
                {
                    case TipoPagPeriodico.ChequeDeposito:
                        PAGTipo = PAGTipo.deposito;
                        DadosPag = PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag;
                        break;
                    case TipoPagPeriodico.ChequeSindico:
                        PAGTipo = PAGTipo.sindico;
                        break;
                    case TipoPagPeriodico.Boleto:
                    case TipoPagPeriodico.Cheque:
                    case TipoPagPeriodico.Conta:
                    case TipoPagPeriodico.DebitoAutomatico:
                    case TipoPagPeriodico.DebitoAutomaticoConta:
                    case TipoPagPeriodico.Folha:
                    case TipoPagPeriodico.Adiantamento:
                    case TipoPagPeriodico.FolhaEletronico:
                    case TipoPagPeriodico.FolhaFGTS:
                    case TipoPagPeriodico.FolhaFGTSEletronico:
                    case TipoPagPeriodico.FolhaIR:
                    case TipoPagPeriodico.FolhaIREletronico:
                    case TipoPagPeriodico.FolhaPIS:
                    case TipoPagPeriodico.FolhaPISEletronico:
                        PAGTipo = PAGTipo.cheque;
                        break;                    
                    case TipoPagPeriodico.PEOrdemPagamento:                        
                    case TipoPagPeriodico.PETituloBancario:                        
                    case TipoPagPeriodico.PETituloBancarioAgrupavel:                        
                    case TipoPagPeriodico.PETransferencia:
                        PAGTipo = (PAGTipo)(PGFrow.PGFForma);
                        if ((TipoPagPeriodico)PGFrow.PGFForma == TipoPagPeriodico.PEOrdemPagamento)
                            DadosPag = PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag;
                        CCT = PGFrow.PGF_CCT;
                        booPermiteAgrupar = false;
                        break;
                    //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                    case TipoPagPeriodico.PECreditoConta:
                        PAGTipo = (PAGTipo)(PGFrow.PGFForma);
                        if (!PGFrow.IsPGF_CCTCredNull())
                        {
                            Cadastros.Condominios.PaginasCondominio.dConta dContas = new Cadastros.Condominios.PaginasCondominio.dConta();
                            Cadastros.Condominios.PaginasCondominio.dConta.ContaCorrenTeDataTable dCont = dContas.ContaCorrenTeTableAdapter.GetContaPgEletronicoByCON(PGFrow.PGF_CON);
                            dContas.ContaCorrenTeTableAdapter.Fill(dCont, PGFrow.PGF_CON);
                            DadosPag = string.Format("{0} - CCT = {1}", dCont.FindByCCT(PGFrow.PGF_CCTCred).DescricaoContaCorrente, PGFrow.PGF_CCTCred);
                        }
                        CCT = PGFrow.PGF_CCT;
                        booPermiteAgrupar = true;
                        break;
                    //*** MRC - TERMINO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                    default:
                        throw new NotImplementedException(string.Format("N�o implementado tratamento para TipoPagPeriodico = {0} em PagamentosPeriodico.cs", (TipoPagPeriodico)PGFrow.PGFForma));
                }

                

                //ContaPagar.dNOtAs.dNOtAsStX(EMPTipo).IncluiNotaSimples(0, DataPag, DataPag,Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, DadosPag, true, PGFrow.PGF_PLA,PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN, false, dNOtAs.TipoChave.PGF, PGFrow.PGF, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                if(NotaCheque == enumNotaCheque.aguarda)
                    ContaPagar.dNOtAs.dNOtAsStX(EMPTipo.Tipo).IncluiNotaProvisoria(DataPag, Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, booPermiteAgrupar, PGFrow.PGF_PLA, PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN, dNOtAs.TipoChave.PGF, PGFrow.PGF,DadosPag, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                else
                    ContaPagar.dNOtAs.dNOtAsStX(EMPTipo.Tipo).IncluiNotaSimples(0, DataPag, DataPag, Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, DadosPag, booPermiteAgrupar, PGFrow.PGF_PLA, PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN, false, dNOtAs.TipoChave.PGF, PGFrow.PGF, CCT, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                
                Comp++;
                PGFrow.PGFProximaCompetencia = Comp.CompetenciaBind;
                PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} Cheque emitido, cadastrado pr�ximo ({2}) valor: {3:n2} vencimento {4:dd/MM/yyyy}",
                                                       DateTime.Now,
                                                       Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                       ProximaCompetencia,
                                                       PGFrow.PGFValor,
                                                       ProximaData);
                
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                PGFrow.AcceptChanges();
                
                dPagamentosPeriodicos.PAGamentos.Clear();
                dPagamentosPeriodicos.NOtAs.Clear();
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
                dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
                EMPTipo.Commit();
            }
            catch (Exception e)
            {
               EMPTipo.Vircatch(e);
                throw new Exception("Erro re-gerado"+e.Message, e);
            }            
            return true;
        }
        */


        /// <summary>
        /// Cadastra o cheque para boletos e conta ou cheque agendado para d�bito autom�tico
        /// </summary>
        /// <param name="Vencimento"></param>
        /// <param name="Valor"></param>
        /// <param name="TipoCad"></param>
        /// <returns></returns>
        [Obsolete("Checar se ainda usa apos revisao")]
        internal bool CadastraProximoPagamento(DateTime Vencimento, decimal Valor, TipoCadastraProximoPagamento TipoCad = TipoCadastraProximoPagamento.efetivo)
        {
            return CadastraProximoPagamento(Vencimento,Vencimento, Valor, null,TipoCad);
        }



        /// <summary>
        /// Cadastra a conta real ou para acumular
        /// </summary>
        public enum TipoCadastraProximoPagamento 
        {
            /// <summary>
            /// Contar real
            /// </summary>
            efetivo,
            /// <summary>
            /// Conta para acumular
            /// </summary>
            acumular,
            /// <summary>
            /// Conta para acumular mas sem o valor ainda
            /// </summary>
            acumularZerado,
        }

        /*
        public bool CadastraUmPagamentoFolha(Competencia comp,DateTime Vencimento)
        {
            try
            {
                STTA().AbreTrasacaoLocal(dPagamentosPeriodicos.PaGamentosFixosTableAdapter);
                //bool eletronica = ((Tipo == TipoPagPeriodico.FolhaIREletronico) || (Tipo == TipoPagPeriodico.FolhaPISEletronico));
                DateTime NOADataEmissao = DateTime.Today;
                int NOANumero = comp.CompetenciaBind;
                dNOtAs.dNOtAsStX(EMPTipo).IncluiNotaSimples(NOANumero,
                                                  NOADataEmissao,
                                                  Vencimento,
                                                  comp,
                                                  NOATipo.Folha,
                                                  PGFrow.PGFNominal,
                                                  PGFrow.PGF_CON,
                                                  eletronica ? PAGTipo.GuiaEletronica : PAGTipo.Guia,
                                                  !eletronica,
                                                  PGFrow.PGF_PLA,
                                                  PGFrow.PGF_SPL,
                                                  DescricaoInterna,
                                                  Valor,
                                                  0, //Fornecedor Cadastrar FOLHA 
                                                  false,
                                                  dNOtAs.TipoChave.PGF,
                                                  PGFrow.PGF);
                PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} Cadastrado recolhimento de {5} sobre a FOLHA ({2}) valor: {3:n2} vencimento {4:dd/MM/yyyy} -> {6}\r\n",
                                               DateTime.Now,
                                               Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                               ProximaCompetencia,
                                               Valor,
                                               Vencimento,
                                               Tipo == TipoPagPeriodico.FolhaIR ? "IR" : "PIS",
                                               eletronica ? string.Format("Pagamento eletr�nico cadastrado para {0:dd/MM/yyyy}", Pagamento) : "Cheque cadastrado");
                if (ValorLiquido != 0)
                    PGFrow.PGFValor = ValorLiquido;
                PGFrow.PGFProximaCompetencia = ProximaCompetencia.Add(1).CompetenciaBind;
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                PGFrow.AcceptChanges();
                EMPTipo.Commit();
            }
            catch (Exception e)
            {
               EMPTipo.Vircatch(e);
                return false;
            }
            return true;
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Vencimento"></param>
        /// <param name="Pagamento"></param>
        /// <param name="Valor"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public bool CadastraProximoPagamento(DateTime Vencimento, DateTime Pagamento, decimal Valor, Competencia comp)
        {
            return CadastraProximoPagamento(Vencimento, Pagamento, Valor, comp, TipoCadastraProximoPagamento.efetivo);
        }

        /// <summary>
        /// Cadastra o cheque para boletos e conta ou cheque agendado para d�bito autom�tico
        /// </summary>
        /// <param name="Vencimento"></param>
        /// <param name="Pagamento"></param>
        /// <param name="Valor"></param>
        /// <param name="comp"></param>
        /// <param name="TipoCad"></param>
        /// <returns></returns>
        [Obsolete("Checar se ainda usa apos revisao")]
        internal bool CadastraProximoPagamento(DateTime Vencimento, DateTime Pagamento, decimal Valor, Competencia comp=null,TipoCadastraProximoPagamento TipoCad = TipoCadastraProximoPagamento.efetivo)
        {
            if (!Encontrado)
                return false;            
            List<TipoPagPeriodico> TiposValidos;
            TiposValidos = new List<TipoPagPeriodico>(new TipoPagPeriodico[] 
                             {   
                                 //TipoPagPeriodico.Conta,
                                 //TipoPagPeriodico.DebitoAutomaticoConta,
                                 TipoPagPeriodico.FolhaPIS,
                                 TipoPagPeriodico.FolhaPISEletronico,
                                 TipoPagPeriodico.FolhaIR,
                                 TipoPagPeriodico.FolhaIREletronico,
                                 TipoPagPeriodico.FolhaFGTS
                             });
            if(!TiposValidos.Contains(Tipo))            
                return false;
            try
            {
                if (comp == null)
                    comp = ProximaCompetencia;
                decimal ValorLiquido = Valor;
                TableAdapter.AbreTrasacaoSQL("ContaPagar PagamentoPeriodico - 1026", dPagamentosPeriodicos.PaGamentosFixosTableAdapter);
                PAGTipo PAGTIpo1 = PAGTipo.cheque;
                string DescricaoPag = "Cheque cadastrado";
                int NOANumero = 0;
                DateTime NOADataEmissao = DateTime.Today;
                NOATipo NOATipo = NOATipo.PagamentoPeriodico;
                bool Agrupa = true;
                string DescricaoDocumento = "";
                switch (Tipo)
                {
                    case TipoPagPeriodico.Conta:
                        PAGTIpo1 = PAGTipo.Conta;
                        DescricaoPag = "Cheque cadastrado";
                        Agrupa = true;
                        DescricaoDocumento = "conta";
                        break;
                    case TipoPagPeriodico.DebitoAutomaticoConta:
                        PAGTIpo1 = PAGTipo.DebitoAutomatico;
                        DescricaoPag = "D�bito agendado";
                        Agrupa = false;
                        DescricaoDocumento = "conta";
                        break;
                    case TipoPagPeriodico.FolhaFGTS:
                        PAGTIpo1 = PAGTipo.Guia;
                        DescricaoPag = "Cheque cadastrado";
                        NOANumero = comp.CompetenciaBind;
                        NOADataEmissao = new DateTime(comp.Ano, comp.Mes, 1).AddDays(-1);
                        NOATipo = NOATipo.Folha;
                        Agrupa = true;
                        DescricaoDocumento = "FGTS";
                        break;
                    case TipoPagPeriodico.FolhaPIS:
                        PAGTIpo1 = PAGTipo.Guia;
                        DescricaoPag = "Cheque cadastrado";
                        NOANumero = comp.CompetenciaBind;
                        NOADataEmissao = new DateTime(comp.Ano, comp.Mes, 1).AddDays(-1);
                        NOATipo = NOATipo.Folha;
                        Agrupa = true;
                        DescricaoDocumento = "PIS";
                        break;
                    case TipoPagPeriodico.FolhaIR:
                        PAGTIpo1 = PAGTipo.Guia;
                        DescricaoPag = "Cheque cadastrado";
                        NOANumero = comp.CompetenciaBind;
                        NOADataEmissao = new DateTime(comp.Ano, comp.Mes, 1).AddDays(-1);
                        NOATipo = NOATipo.Folha;
                        Agrupa = true;
                        DescricaoDocumento = "IR Folha";
                        break;
                };
                if (TipoCad == TipoCadastraProximoPagamento.acumular)
                    PAGTIpo1 = PAGTipo.Acumular;
                switch (Tipo)
                {
                    case TipoPagPeriodico.Conta:
                    case TipoPagPeriodico.DebitoAutomaticoConta:
                    case TipoPagPeriodico.FolhaFGTS:
                    case TipoPagPeriodico.FolhaPIS:
                    case TipoPagPeriodico.FolhaIR:
                        if (TipoCad == TipoCadastraProximoPagamento.efetivo)
                        {
                            //Verifica se tem valores anteriores para acumular
                            /*
                            if (TemAcumulado() != TiposAcumulado.SemAcumulado)
                            {
                                if ((ValAcumulado() == 0))
                                {
                                    decimal ValorRat = Math.Round(Valor / (PAGsAcumular.Count + 1), 2);
                                    ValorLiquido = Valor - ValorRat * PAGsAcumular.Count;
                                    //foreach (int PAG in PAGsAcumular)
                                    //{
                                    //    STTA().ExecutarSQLNonQuery("update PAGamentos set PAGValor = @P1 where PAG = @P2", ValorRat, PAG);
                                    //    STTA().ExecutarSQLNonQuery("UPDATE NOtAs SET NOATotal = @P1 FROM PAGamentos INNER JOIN NOtAs ON PAG_NOA = NOA WHERE (PAG = @P2) AND (NOATotal = 0)", ValorRat, PAG);
                                    //}
                                }
                                else
                                    ValorLiquido = Valor - ValAcumulado();
                                DescricaoPag += " (inclu�dos valores acumulados)";
                            }*/                            
                        }

                        if (!dNOtAs.dNOtAsStX(EMPTProc1.Tipo).IncluiNotaSimples_Efetivo(NOANumero,
                                                                                        NOADataEmissao,
                                                                                        Vencimento,
                                                                                        comp,
                                                                                        NOATipo,
                                                                                        PGFrow.PGFNominal,
                                                                                        PGFrow.PGF_CON,
                                                                                        PAGTIpo1,                            
                                                                                        "",
                                                                                        Agrupa,
                                                                                        PGFrow.PGF_PLA,
                                                                                        PGFrow.PGF_SPL,
                                                                                        DescricaoInterna(comp),   
                                                                                        ValorLiquido,
                                                                                        PGFrow.IsPGF_FRNNull() ? 0 : PGFrow.PGF_FRN,
                                                                                        false,
                                                                                        dNOtAs.TipoChave.PGF,
                                                                                        PGFrow.PGF).HasValue)
                            throw new Exception("Erro na inclus�o da nota");
                        //ReagendaAcumulados(Vencimento, TipoCad == TipoCadastraProximoPagamento.acumular,0);
                        /*
                        if (TemAcumulado())
                        {
                            if (TipoCad == TipoCadastraProximoPagamento.efetivo)
                            {
                                ReagendaAcumulados(Vencimento, false);
                                //if (!dNOtAs.dNOtAsStX(EMPTipo).UltimoChequeCriado.IncluiPAGs(PAGsAcumular.ToArray()))
                                //    throw new Exception("Erro na inclus�o do acumulado\r\n" + dNOtAs.dNOtAsStX(EMPTipo).UltimoChequeCriado.UltimoErro);
                            }
                            else
                            {

                                foreach (int PAG in PAGsAcumular)
                                    STTA().ExecutarSQLNonQuery("update PAGamentos set PAGVencimento = @P1 where PAG = @P2", Vencimento, PAG);
                            }
                        }*/
                        PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} Cadastrado(a) {2} ({3}) valor: {4:n2} vencimento {5:dd/MM/yyyy} -> {6}\r\n",                        
                                                       DateTime.Now,
                                                       Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                       DescricaoDocumento,
                                                       comp,
                                                       Valor,
                                                       Vencimento,
                                                       TipoCad == TipoCadastraProximoPagamento.efetivo ? DescricaoPag : "Valor inferior, acumular");
                        
                        break;                    
                    
                    case TipoPagPeriodico.FolhaPISEletronico:
                    case TipoPagPeriodico.FolhaIREletronico:
                    
                        bool eletronica = ((Tipo == TipoPagPeriodico.FolhaIREletronico) || (Tipo == TipoPagPeriodico.FolhaPISEletronico));

                        dNOtAs.dNOtAsStX(EMPTProc1.Tipo).IncluiNotaSimples_Efetivo(NOANumero, 
                                                          NOADataEmissao, 
                                                          Vencimento, 
                                                          ProximaCompetencia, 
                                                          NOATipo.Folha, 
                                                          PGFrow.PGFNominal, 
                                                          PGFrow.PGF_CON,
                                                          eletronica ? PAGTipo.GuiaEletronica : PAGTipo.Guia,
                                                          "",
                                                          !eletronica, 
                                                          PGFrow.PGF_PLA, 
                                                          PGFrow.PGF_SPL,
                                                          DescricaoInterna(ProximaCompetencia), 
                                                          Valor, 
                                                          0, //Fornecedor Cadastrar FOLHA 
                                                          false, 
                                                          dNOtAs.TipoChave.PGF, 
                                                          PGFrow.PGF);
                        PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} Cadastrado recolhimento de {5} sobre a FOLHA ({2}) valor: {3:n2} vencimento {4:dd/MM/yyyy} -> {6}\r\n",
                                                       DateTime.Now,
                                                       Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                       ProximaCompetencia,
                                                       Valor,
                                                       Vencimento,
                                                       Tipo == TipoPagPeriodico.FolhaIR ? "IR" : "PIS",
                                                       eletronica ? string.Format("Pagamento eletr�nico cadastrado para {0:dd/MM/yyyy}",Pagamento) : "Cheque cadastrado");
                        if (eletronica)
                        {
 
                        }
                        break;
                }
                if (ValorLiquido != 0)
                    PGFrow.PGFValor = ValorLiquido;
                PGFrow.PGFProximaCompetencia = ProximaCompetencia.Add(1).CompetenciaBind;                
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                PGFrow.AcceptChanges();
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                ErroAgendamento = e.Message;
                EMPTProc1.Vircatch(e);
                return false;
            }
            CadastrarProximo();
            return true;
        }

        

        /*
        private DocBacarios.CPFCNPJ _cpfcnpj;

        private DocBacarios.CPFCNPJ cpfcnpj
        {
            get
            {
                if (_cpfcnpj == null)
                {
                    if (PGFrow.IsPGF_FRNNull())
                        return null;
                    _cpfcnpj = new DocBacarios.CPFCNPJ(EMPTipo.STTA.BuscaEscalar_string("select FRNCNPJ from FORNECEDORES where FRN = @P1", PGFrow.PGF_FRN) ?? "");
                }
                return _cpfcnpj;
            }
        }*/
        /*
        /// <summary>
        /// Chave
        /// </summary>
        public int PGF
        {
            get { return PGFrow.PGF; }
        }*/

        internal int? NotaProvisoria()
        {
            return EMPTProc1.STTA.BuscaEscalar_int("select noa from NOtAs where NOA_PGF = @P1 and NOAStatus = 5", PGFrow.PGF);
        }

        

        /*
        internal int ChegadaBoletoDesbloqueio(int NOA, int NOANumero, DateTime NOADataEmissao)
        {
            if ((TipoPagPeriodico)PGFrow.PGFForma != TipoPagPeriodico.Boleto)
                return 0;
            if (PGFrow.IsPGF_FRNNull())
                return 0;
            //string strcnpj = "";
            //STTA().BuscaEscalar("select FRNCNPJ from FORNECEDORES where FRN = @P1", out strcnpj, PGFrow.PGF_FRN);
            //DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(strcnpj);
            if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                if (!PGFrow.IsPGFINSSTNull())
                    throw new Exception("Erro de cadastro. INSS do tomador cadastrado para PJ");

            //chamar CadastraProximoPagamento(

            Competencia Comp = new Competencia(PGFrow.PGFProximaCompetencia == 0 ? PGFrow.PGFCompetI : PGFrow.PGFProximaCompetencia);
            Comp.CON = PGFrow.PGF_CON;
            Competencia CompPAG = Comp.CloneCompet(PGFrow.PGFRef);

            DateTime DataPag = CompPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);

            dNOtAs dNotas = new dNOtAs();
            dNOtAs.NOtAsRow rowNOA;
            if (NOA == 0)
            {
                rowNOA = dNotas.NOtAs.NewNOtAsRow();
                rowNOA.NOA_CON = PGFrow.PGF_CON;
                rowNOA.NOANumero = NOANumero;
                rowNOA.NOADataEmissao = NOADataEmissao;
                rowNOA.NOACompet = Comp.CompetenciaBind;
                rowNOA.NOAAguardaNota = false;
                rowNOA.NOADATAI = DateTime.Now;
                rowNOA.NOAI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                rowNOA.NOAObs = string.Format("Nota referente a contrato. Copet�ncia do servi�o: {0}", Comp);
            }
            else
            {
                dNotas.NOtAsTableAdapter.FillByNOA(dNotas.NOtAs, NOA);
                rowNOA = dNotas.NOtAs[0];
            }
            rowNOA.NOATipo = (int)NOATipo.PagamentoPeriodico;

            System.Collections.ArrayList Retencoes = new System.Collections.ArrayList();
            if (!PGFrow.IsPGFISSNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoISS, PGFrow.PGFISS, CompontesBasicosProc.SimNao.Padrao));
            if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
            {
                if (!PGFrow.IsPGFINSSPNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSS, PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
                if (!PGFrow.IsPGFIRNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IR, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
            }
            else
            {
                if (!PGFrow.IsPGFINSSPNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
                if (!PGFrow.IsPGFINSSTNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, PGFrow.PGFINSST, CompontesBasicosProc.SimNao.Padrao));
                if (!PGFrow.IsPGFIRNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IRPF, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
            }

            if (!PGFrow.IsPGFPISCOFNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.PIS_COFINS_CSLL, PGFrow.PGFPISCOF, CompontesBasicosProc.SimNao.Padrao));
            string Nominal = PGFrow.IsPGFNominalNull() ? "" : PGFrow.PGFNominal;
            try
            {
                STTA().AbreTrasacaoLF("ContaPagar PagamentoPeriodico - 1254",
                                                                       dPagamentosPeriodicos.AGendaBaseTableAdapter,
                                                                       dPagamentosPeriodicos.PaGamentosFixosTableAdapter,
                                                                       dNotas.NOtAsTableAdapter,
                                                                       dNotas.PAGamentosTableAdapter
                                                                       );
                //PAGTipo PAGTipo;
                //string DadosPag;
                //PAGTipo = PAGTipo.boleto;
                //DadosPag = "";

                //
                //dNOtAs.NOtAsRow rowNOA = dNotas.NOtAs[0];
                //rowNOA.NOATipo = 0;
                rowNOA.NOA_PLA = PGFrow.PGF_PLA;
                if (!PGFrow.IsPGF_SPLNull())
                    rowNOA.NOA_SPL = PGFrow.PGF_SPL;
                rowNOA.NOA_FRN = PGFrow.PGF_FRN;
                rowNOA.NOAServico = DescricaoInterna();// PGFrow.PGFDescricao;
                rowNOA.NOAStatus = (int)ContaPagar.NOAStatus.Cadastrada;
                rowNOA.NOATipo = (int)NOATipo.PagamentoPeriodico;
                //if ((Tipo == PAGTipo.deposito) && (DadosPag != ""))
                //  rowNOA.NOADadosPag = DadosPag;
                rowNOA.NOATotal = PGFrow.PGFValor;
                rowNOA.NOADefaultPAGTipo = (int)PAGTipo.boleto;
                rowNOA.NOA_PGF = PGFrow.PGF;
                if (rowNOA.RowState == System.Data.DataRowState.Detached)
                    dNotas.NOtAs.AddNOtAsRow(rowNOA);
                dNotas.NOtAsTableAdapter.Update(rowNOA);
                rowNOA.AcceptChanges();
                NOA = rowNOA.NOA;
                decimal ValorPagamento = PGFrow.PGFValor;

                Cheque NovoCheque;

                foreach (dllImpostos.SolicitaRetencao Sol in Retencoes)
                {
                    dllImpostoNeon.ImpostoNeon Imp = new dllImpostoNeon.ImpostoNeon(Sol.Tipo);
                    Imp.DataNota = rowNOA.NOADataEmissao;
                    Imp.DataPagamento = DataPag;
                    Imp.ValorBase = rowNOA.NOATotal;
                    NovoCheque = new Cheque(Imp.VencimentoEfetivo, Imp.Nominal(), PGFrow.PGF_CON, PAGTipo.Guia, false, 0);
                    dNOtAs.PAGamentosRow Improw = dNotas.PAGamentos.NewPAGamentosRow();
                    Improw.PAG_CHE = NovoCheque.CHErow.CHE;
                    Improw.PAG_NOA = rowNOA.NOA;
                    Improw.PAGTipo = (int)PAGTipo.Guia;
                    Improw.PAGIMP = (int)Sol.Tipo;
                    Improw.PAGValor = Sol.Valor == 0 ? Imp.ValorImposto : Sol.Valor;
                    if ((Sol.Descontar == CompontesBasicosProc.SimNao.Sim) || ((Sol.Descontar == CompontesBasicosProc.SimNao.Padrao) && (Imp.Desconta())))
                        ValorPagamento -= Improw.PAGValor;
                    Improw.PAGVencimento = Imp.VencimentoEfetivo;
                    Improw.CHEStatus = (int)CHEStatus.Cadastrado;
                    Improw.CHEFavorecido = Imp.Nominal();
                    //*** MRC - INICIO - PAG-FOR (16/04/2014 11:15) ***    
                    Improw.PAGPermiteAgrupar = true;
                    //*** MRC - FIM - PAG-FOR (16/04/2014 11:15) ***
                    dNotas.PAGamentos.AddPAGamentosRow(Improw);
                    dNotas.PAGamentosTableAdapter.Update(Improw);
                    Improw.AcceptChanges();
                    NovoCheque.AjustarTotalPelasParcelas();
                };


                NovoCheque = new Cheque(DataPag, PGFrow.PGFNominal, PGFrow.PGF_CON, PAGTipo.boleto, false, 0);
                dNOtAs.PAGamentosRow rowPag = dNotas.PAGamentos.NewPAGamentosRow();
                rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                rowPag.PAG_NOA = rowNOA.NOA;
                rowPag.PAGDATAI = DateTime.Now;
                rowPag.PAGI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                rowPag.PAGTipo = (int)PAGTipo.boleto;
                rowPag.PAGValor = ValorPagamento;
                rowPag.PAGVencimento = DataPag;
                //*** MRC - INICIO - PAG-FOR (16/04/2014 11:15) ***    
                rowPag.PAGPermiteAgrupar = true;
                //*** MRC - FIM - PAG-FOR (16/04/2014 11:15) ***
                dNotas.PAGamentos.AddPAGamentosRow(rowPag);
                dNotas.PAGamentosTableAdapter.Update(rowPag);
                rowPag.AcceptChanges();
                NovoCheque.AjustarTotalPelasParcelas();


                //Commit();
                //

                //ContaPagar.dNOtAs.dNOtAsStX(EMPTipo).IncluiNotaSimples(0, DataPag, DataPag, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, DadosPag, true, PGFrow.PGF_PLA, PGFrow.PGFDescricao, PGFrow.PGFValor, FRN, false, dNOtAs.TipoChave.PGF, PGFrow.PGF, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                Comp++;
                PGFrow.PGFProximaCompetencia = Comp.CompetenciaBind;
                //if (!PGFrow.IsPGFCompetFNull())
                //    if (PGFrow.PGFProximaCompetencia > PGFrow.PGFCompetF)
                //    {
                //        PGFrow.PGFStatus = (int)StatusPagamentoPeriodico.Encerrado;
                //    }
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                PGFrow.AcceptChanges();
                //object erro = null;
                //string errostr = erro.ToString();
                CadastrarProximo();
                EMPTipo.Commit();
            }
            catch (Exception e)
            {
                NOA = 0;
               EMPTipo.Vircatch(e);
                throw e;
            }
            //dPagamentosPeriodicos.PAGamentos.Clear();
            //dPagamentosPeriodicos.NOtAs.Clear();
            //dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
            //dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
            return NOA;
        }
        */
        /*
        /// <summary>
        /// Chamado na chegada do boleto para cadastrar o cheque
        /// </summary>
        /// <param name="NOA"></param>
        /// <param name="NOANumero">N�mero da nota</param>
        /// <param name="NOADataEmissao">Nota fiscal</param>
        /// <returns></returns>
        internal int CadastrarProximoChequeDoBoleto(int NOA,int NOANumero,DateTime NOADataEmissao)
        {
            if ((TipoPagPeriodico)PGFrow.PGFForma != TipoPagPeriodico.Boleto)
                return 0;
            if (PGFrow.IsPGF_FRNNull())
                return 0;
            //string strcnpj = "";
            //STTA().BuscaEscalar("select FRNCNPJ from FORNECEDORES where FRN = @P1", out strcnpj, PGFrow.PGF_FRN);
            //DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(strcnpj);
            if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                if (!PGFrow.IsPGFINSSTNull())
                    throw new Exception("Erro de cadastro. INSS do tomador cadastrado para PJ");
            
            //chamar CadastraProximoPagamento(

            Competencia Comp = new Competencia(PGFrow.PGFProximaCompetencia == 0 ? PGFrow.PGFCompetI : PGFrow.PGFProximaCompetencia);
            Comp.CON = PGFrow.PGF_CON;
            Competencia CompPAG = Comp.CloneCompet(PGFrow.PGFRef);

            DateTime DataPag = CompPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);

            dNOtAs dNotas = new dNOtAs();
            dNOtAs.NOtAsRow rowNOA;
            if (NOA == 0)
            {
                rowNOA = dNotas.NOtAs.NewNOtAsRow();
                rowNOA.NOA_CON = PGFrow.PGF_CON;
                rowNOA.NOANumero = NOANumero;
                rowNOA.NOADataEmissao = NOADataEmissao;
                rowNOA.NOACompet = Comp.CompetenciaBind;
                rowNOA.NOAAguardaNota = false;
                rowNOA.NOADATAI = DateTime.Now;
                rowNOA.NOAI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                rowNOA.NOAObs = string.Format("Nota referente a contrato. Copet�ncia do servi�o: {0}", Comp);
            }
            else
            {
                dNotas.NOtAsTableAdapter.FillByNOA(dNotas.NOtAs, NOA);
                rowNOA = dNotas.NOtAs[0];
            }
            rowNOA.NOATipo = (int)NOATipo.PagamentoPeriodico;            
            
            System.Collections.ArrayList Retencoes = new System.Collections.ArrayList();
            if (!PGFrow.IsPGFISSNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoISS, PGFrow.PGFISS, CompontesBasicosProc.SimNao.Padrao));
            if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
            {
                if (!PGFrow.IsPGFINSSPNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSS, PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
                if (!PGFrow.IsPGFIRNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IR, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
            }
            else
            {
                if (!PGFrow.IsPGFINSSPNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
                if (!PGFrow.IsPGFINSSTNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, PGFrow.PGFINSST, CompontesBasicosProc.SimNao.Padrao));
                if (!PGFrow.IsPGFIRNull())
                    Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IRPF, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
            }
            
            if (!PGFrow.IsPGFPISCOFNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.PIS_COFINS_CSLL, PGFrow.PGFPISCOF, CompontesBasicosProc.SimNao.Padrao));
            string Nominal = PGFrow.IsPGFNominalNull() ? "" : PGFrow.PGFNominal;
            try
            {
                STTA().AbreTrasacaoLF("ContaPagar PagamentoPeriodico - 1254",
                                                                       dPagamentosPeriodicos.AGendaBaseTableAdapter,
                                                                       dPagamentosPeriodicos.PaGamentosFixosTableAdapter,
                                                                       dNotas.NOtAsTableAdapter,
                                                                       dNotas.PAGamentosTableAdapter
                                                                       );
                //PAGTipo PAGTipo;
                //string DadosPag;
                //PAGTipo = PAGTipo.boleto;
                //DadosPag = "";

                //
                //dNOtAs.NOtAsRow rowNOA = dNotas.NOtAs[0];
 //rowNOA.NOATipo = 0;
                rowNOA.NOA_PLA = PGFrow.PGF_PLA;
                if (!PGFrow.IsPGF_SPLNull())
                    rowNOA.NOA_SPL = PGFrow.PGF_SPL;
                rowNOA.NOA_FRN = PGFrow.PGF_FRN;
                rowNOA.NOAServico = DescricaoInterna();// PGFrow.PGFDescricao;
                rowNOA.NOAStatus = (int)ContaPagar.NOAStatus.Cadastrada;
                rowNOA.NOATipo = (int)NOATipo.PagamentoPeriodico;
                //if ((Tipo == PAGTipo.deposito) && (DadosPag != ""))
                //  rowNOA.NOADadosPag = DadosPag;
                rowNOA.NOATotal = PGFrow.PGFValor;
                rowNOA.NOADefaultPAGTipo = (int)PAGTipo.boleto;
                rowNOA.NOA_PGF = PGFrow.PGF;
                if (rowNOA.RowState == System.Data.DataRowState.Detached)
                    dNotas.NOtAs.AddNOtAsRow(rowNOA);
                dNotas.NOtAsTableAdapter.Update(rowNOA);
                rowNOA.AcceptChanges();
                NOA = rowNOA.NOA;
                decimal ValorPagamento = PGFrow.PGFValor;

                Cheque NovoCheque;
                
                foreach (dllImpostos.SolicitaRetencao Sol in Retencoes)
                {
                    dllImpostoNeon.ImpostoNeon Imp = new dllImpostoNeon.ImpostoNeon(Sol.Tipo);
                    Imp.DataNota = rowNOA.NOADataEmissao;
                    Imp.DataPagamento = DataPag;
                    Imp.ValorBase = rowNOA.NOATotal;
                    NovoCheque = new Cheque(Imp.VencimentoEfetivo, Imp.Nominal(), PGFrow.PGF_CON, PAGTipo.Guia, false,0);
                    dNOtAs.PAGamentosRow Improw = dNotas.PAGamentos.NewPAGamentosRow();
                    Improw.PAG_CHE = NovoCheque.CHErow.CHE;
                    Improw.PAG_NOA = rowNOA.NOA;
                    Improw.PAGTipo = (int)PAGTipo.Guia;
                    Improw.PAGIMP = (int)Sol.Tipo;
                    Improw.PAGValor = Sol.Valor == 0 ? Imp.ValorImposto : Sol.Valor;
                    if ((Sol.Descontar == CompontesBasicosProc.SimNao.Sim) || ((Sol.Descontar == CompontesBasicosProc.SimNao.Padrao) && (Imp.Desconta())))
                        ValorPagamento -= Improw.PAGValor;
                    Improw.PAGVencimento = Imp.VencimentoEfetivo;
                    Improw.CHEStatus = (int)CHEStatus.Cadastrado;
                    Improw.CHEFavorecido = Imp.Nominal();
                    //*** MRC - INICIO - PAG-FOR (16/04/2014 11:15) ***    
                    Improw.PAGPermiteAgrupar = true;
                    //*** MRC - FIM - PAG-FOR (16/04/2014 11:15) ***
                    dNotas.PAGamentos.AddPAGamentosRow(Improw);
                    dNotas.PAGamentosTableAdapter.Update(Improw);
                    Improw.AcceptChanges();
                    NovoCheque.AjustarTotalPelasParcelas();
                };

                
                NovoCheque = new Cheque(DataPag, PGFrow.PGFNominal, PGFrow.PGF_CON, PAGTipo.boleto, false,0);
                dNOtAs.PAGamentosRow rowPag = dNotas.PAGamentos.NewPAGamentosRow();
                rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                rowPag.PAG_NOA = rowNOA.NOA;
                rowPag.PAGDATAI = DateTime.Now;
                rowPag.PAGI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                rowPag.PAGTipo = (int)PAGTipo.boleto;
                rowPag.PAGValor = ValorPagamento;
                rowPag.PAGVencimento = DataPag;
                //*** MRC - INICIO - PAG-FOR (16/04/2014 11:15) ***    
                rowPag.PAGPermiteAgrupar = true;
                //*** MRC - FIM - PAG-FOR (16/04/2014 11:15) ***
                dNotas.PAGamentos.AddPAGamentosRow(rowPag);
                dNotas.PAGamentosTableAdapter.Update(rowPag);
                rowPag.AcceptChanges();
                NovoCheque.AjustarTotalPelasParcelas();
                

                //Commit();
                //

                //ContaPagar.dNOtAs.dNOtAsStX(EMPTipo).IncluiNotaSimples(0, DataPag, DataPag, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, DadosPag, true, PGFrow.PGF_PLA, PGFrow.PGFDescricao, PGFrow.PGFValor, FRN, false, dNOtAs.TipoChave.PGF, PGFrow.PGF, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                Comp++;
                PGFrow.PGFProximaCompetencia = Comp.CompetenciaBind;
                //if (!PGFrow.IsPGFCompetFNull())
                //    if (PGFrow.PGFProximaCompetencia > PGFrow.PGFCompetF)
                //    {
                //        PGFrow.PGFStatus = (int)StatusPagamentoPeriodico.Encerrado;
                //    }
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                PGFrow.AcceptChanges();
                //object erro = null;
                //string errostr = erro.ToString();
                CadastrarProximo();
                EMPTipo.Commit();
            }
            catch (Exception e)
            {
                NOA = 0;
               EMPTipo.Vircatch(e);
                throw e;
            }
            //dPagamentosPeriodicos.PAGamentos.Clear();
            //dPagamentosPeriodicos.NOtAs.Clear();
            //dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
            //dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
            return NOA;
        }
        */
    


        

       


        
        private System.Collections.ArrayList tiposComContas;

        
        private System.Collections.ArrayList TiposComContas
        {
            get
            {
                if (tiposComContas == null)
                    tiposComContas = new System.Collections.ArrayList(new TipoPagPeriodico[] { TipoPagPeriodico.DebitoAutomaticoConta });
                return tiposComContas;
            }            
        }

        //remover
        /*
        public void Correcao()
        {
            AjustarAGBReajContrato();
            dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
        }       

        /// <summary>
        /// O contrato esta somente na AGB. Nao existe campo espec�fico no PGF
        /// </summary>
        protected override void AjustarAGBReajContrato()
        {            
            //if ((ContaPagar.TipoPagPeriodico)PGFrow.PGFForma != TipoPagPeriodico.Boleto)
            //    return;
            dPagamentosPeriodicos.AGendaBaseRow AGBrow1 = null;
            if (!PGFrow.IsPGFContrato_AGBNull())
            {
                _rowAGB = null;
                dPagamentosPeriodicos.AGendaBaseTableAdapter.FillByAGB(dPagamentosPeriodicos.AGendaBase, PGFrow.PGFContrato_AGB);
                AGBrow1 = dPagamentosPeriodicos.AGendaBase[0];
            };
            bool DeveEstarAgendado = false;
            //if (StatusAtivos.Contains((StatusPagamentoPeriodico)PGFrow.PGFStatus) && (!PGFrow.IsPGFDataLimiteResNull()))
            if (StatusAtivos.Contains((StatusPagamentoPeriodico)PGFrow.PGFStatus) && (!PGFrow.IsAGBDataInicioNull()))
                    DeveEstarAgendado = true;
            
            if (DeveEstarAgendado)
            {
                DateTime DataVenc = PGFrow.AGBDataInicio;
                                
                if (AGBrow1 == null)
                {
                    AGBrow1 = dPagamentosPeriodicos.AGendaBase.NewAGendaBaseRow();
                    AGBrow1.AGB_CON = PGFrow.PGF_CON;
                    AGBrow1.AGB_TAG = (int)Framework.Enumeracoes.TiposTAG.Contratos;
                    int Resp10;
                    EMPTipo.STTA.BuscaEscalar("SELECT TAGResp_USU FROM TipoAGendamento WHERE (TAG = 10)", out Resp10);            
                    AGBrow1.AGB_USU = Resp10;
                    AGBrow1.AGBDATAI = DateTime.Now;
                    AGBrow1.AGBDescricao = PGFrow.PGFDescricao;
                    AGBrow1.AGBDiaTodo = true;
                    AGBrow1.AGBI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;                   
                    AGBrow1.AGBMemo = string.Format("\r\n{0:dd/MM/yyyy HH:mm} - {1}\r\n    Cadastro inicial: {2:dd/MM/yyyy}", 
                        DateTime.Now,
                        Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                        DataVenc);
                    AGBrow1.AGBDataTermino = AGBrow1.AGBDataInicio = DataVenc;                                        
                }
                if (AGBrow1.AGBDataInicio != DataVenc)
                {
                    AGBrow1.AGBDataTermino = AGBrow1.AGBDataInicio = DataVenc;
                    AGBrow1.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - {1}\r\n    Data Ajustada pelo cadastro do contrato de {2:dd/MM/yyyy} para {3:dd/MM/yyyy}", 
                        DateTime.Now,
                        Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                        AGBrow1.AGBDataInicio,                       
                        DataVenc);
                }
                AGBrow1.AGBStatus = 0;
                if (AGBrow1.RowState == System.Data.DataRowState.Detached)
                    dPagamentosPeriodicos.AGendaBase.AddAGendaBaseRow(AGBrow1);
            }
            else
            {
                if (AGBrow1 != null)
                {
                    AGBrow1.AGBStatus = 4;
                    AGBrow1.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - {1}\r\n    Cancelado pelo cadastro do contrato", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome);
                    if(!PGFrow.IsAGBDataInicioNull())
                        AGBrow1.AGBDataTermino = AGBrow1.AGBDataInicio = PGFrow.AGBDataInicio;                                        
                }
            };
            dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(dPagamentosPeriodicos.AGendaBase);
            if (AGBrow1 != null)
                if ((PGFrow.IsPGFContrato_AGBNull()) || (PGFrow.PGFContrato_AGB != AGBrow1.AGB))
                    PGFrow.PGFContrato_AGB = AGBrow1.AGB;
        }
        
        





        protected override bool CadastraNaAgenda()
        {
            if (!FormasContraladasNaAgenda.Contains(PGFForma))
                return true;
            if (!StatusAtivos.Contains((StatusPagamentoPeriodico)PGFrow.PGFStatus))
            {
                if (rowAGB != null)
                {
                    rowAGB.AGBStatus = (int)AGBStatus.Ok;
                    if (Terminado)
                        rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - Terminado", DateTime.Now);
                    else
                        rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - {1}\r\n    Cancelado pelo cadastro do contrato", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome);

                    if (!PGFrow.IsAGBDataInicioNull())
                        rowAGB.AGBDataTermino = rowAGB.AGBDataInicio = PGFrow.AGBDataInicio;
                    dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(rowAGB);
                    rowAGB.AcceptChanges();
                }
                return true;
            }
            if (FormasRevisadas.Contains(PGFForma))
            {
                if ((rowAGB != null)
                     &&
                    (rowAGB.AGBCompetenciaAno == ProximaCompetencia.Ano)
                     &&
                    (rowAGB.AGBCompetenciaMes == ProximaCompetencia.Mes)
                   )
                    return true;
            }
            //somente cria um agendamento para o boleto/conta          
            DateTime[] datas;
            malote M1 = null;
            malote M2 = null;
            bool RetornaNoVencimento;
            //Competencia Comp = new Competencia(PGFrow.PGFProximaCompetencia == 0 ? PGFrow.PGFCompetI : PGFrow.PGFProximaCompetencia);
            //Comp.CON = PGFrow.PGF_CON;
            Competencia CompPAG = ProximaCompetencia.CloneCompet(PGFrow.PGFRef);       
            DateTime DataPag = CompPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);
            if (Tipo != TipoPagPeriodico.DebitoAutomaticoConta)
            {
                Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowcon = Framework.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(EMPTipo.Tipo).CONDOMINIOS.FindByCON(PGFrow.PGF_CON);
                if (rowcon == null)
                    return false;
                datas = Cheque.CalculaDatas(PAGTipo.boleto, rowcon.CONProcuracao, (malote.TiposMalote)rowcon.CONMalote, DataPag, DateTime.Now, ref M1, ref M2, out RetornaNoVencimento);
            }
            try
            {
                TableAdapter.AbreTrasacaoSQL("ContaPagar PagamentoPeriodico - 1529",
                                                                        dPagamentosPeriodicos.AGendaBaseTableAdapter,
                                                                        dPagamentosPeriodicos.PaGamentosFixosTableAdapter);             
                //dPagamentosPeriodicos.AGendaBaseRow AGBrow;
                //if (PGFrow.IsPGF_AGBNull())
                TiposTAG TAG = (Tipo != TipoPagPeriodico.DebitoAutomaticoConta) ? TiposTAG.BOLETOS_CONTRATOS : TiposTAG.Debito_Automatico;
                if(rowAGB == null)
                {                
                    _rowAGB = dPagamentosPeriodicos.AGendaBase.NewAGendaBaseRow();
                    _rowAGB.AGB_CON = PGFrow.PGF_CON;
                    _rowAGB.AGB_TAG = _rowAGB.AGB_TAG = (int)TAG;
                    _rowAGB.AGBDATAI = DateTime.Now;
                    _rowAGB.AGBDiaTodo = true;
                    _rowAGB.AGBI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                    _rowAGB.AGBMemo = "Cadastro autom�tico";
                }
                else
                {
                    if (rowAGB.AGB_TAG != (int)TAG)
                   {
                        _rowAGB = rowAGB;
                        _rowAGB.AGB_TAG = (int)TAG;
                   }
                }
                _rowAGB.AGBDescricao = DescricaoInterna();
                int Resp5;

                EMPTipo.STTA.BuscaEscalar("SELECT TAGResp_USU FROM TipoAGendamento WHERE (TAG = @P1)", out Resp5, TAG);
                _rowAGB.AGB_USU = Resp5;
                //if (PGFrow.PGFStatus == (int)StatusPagamentoPeriodico.Encerrado)
                //{                    
                //    AGBrow.AGBMemo += string.Format("\r\nContrato terminado");                    
                //    AGBrow.AGBStatus = 4;                   
                //}
                //else
                //{
                DateTime DataEsperada = (M1 == null) ? DataPag.AddDays(-1) : M1.DataEnvio;
                _rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} {2} para {3:dd/MM/yyyy} referente a {4} com vencimento {5:dd/MM/yyyy}{6}", 
                                                  DateTime.Now,
                                                  Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                  Tipo != TipoPagPeriodico.Boleto ? "Conta esperada" : "Boleto esperado",
                                                  DataEsperada, 
                                                  ProximaCompetencia, 
                                                  DataPag,
                                                  Tipo == TipoPagPeriodico.DebitoAutomaticoConta ? " (D�bito autom�tico)" : "");
                _rowAGB.AGBCompetenciaAno = ProximaCompetencia.Ano;
                _rowAGB.AGBCompetenciaMes = ProximaCompetencia.Mes;
                _rowAGB.AGBDataInicio = DataEsperada;
                _rowAGB.AGBDataTermino = DataPag;
                _rowAGB.AGBStatus = 0;

                _rowAGB.AGBA_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                _rowAGB.AGBDATAA = DateTime.Now;
                if (_rowAGB.RowState == System.Data.DataRowState.Detached)
                    dPagamentosPeriodicos.AGendaBase.AddAGendaBaseRow(_rowAGB);
                dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(_rowAGB);
                _rowAGB.AcceptChanges();
                PGFrow.PGF_AGB = _rowAGB.AGB;
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                EMPTipo.Commit();
                PGFrow.AcceptChanges();             
            }
            catch (Exception e)
            {            
               EMPTipo.Vircatch(e);
                throw e;
            };                        
            return true;
             
        }



        /*
        private bool CadastrarProximoDebito(TipoPagPeriodico Forma)
        {

            //AjustarAGBReajContrato(); unificado no CadastrarProximo
            Competencia CompDebito = new Competencia(PGFrow.PGFProximaCompetencia == 0 ? PGFrow.PGFCompetI : PGFrow.PGFProximaCompetencia);
            CompDebito.CON = PGFrow.PGF_CON;
            //Competencia CompRef = Comp.CloneCompet(PGFrow.PGFRef);            
            DateTime DataVencimento = CompDebito.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.balancete);
            //if (PGFrow.PGFContaMesAnterior)
              //  DataVencimento = DataVencimento.AddMonths(1);
            //bool RetornaNoVencimento;            
            //Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowcon = Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(PGFrow.PGF_CON);            
            //if (rowcon == null)
            //    return false;
            //DateTime[] datas = Cheque.CalculaDatas(PAGTipo.boleto,rowcon.CONProcuracao, (malote.TiposMalote)rowcon.CONMalote, DataPag, DateTime.Now, ref M1, ref M2, out RetornaNoVencimento);
            //datas = Cheque.CalculaDatas(PAGTipo.boleto, rowcon.CONProcuracao, (malote.TiposMalote)rowcon.CONMalote, DataPag, DateTime.Now, ref M1, ref M2, out RetornaNoVencimento);
            

            try
            {                
                STTA().AbreTrasacaoLocal(dPagamentosPeriodicos.AGendaBaseTableAdapter,
                    dPagamentosPeriodicos.PaGamentosFixosTableAdapter);                
                dPagamentosPeriodicos.AGendaBaseRow AGBrow;
                if (PGFrow.IsPGF_AGBNull())
                {                    
                    AGBrow = dPagamentosPeriodicos.AGendaBase.NewAGendaBaseRow();
                    AGBrow.AGB_CON = PGFrow.PGF_CON;
                    AGBrow.AGB_TAG = (int)Framework.Enumeracoes.TiposTAG.Debito_Automatico;
                    int Resp;
                    STTA().BuscaEscalar("SELECT TAGResp_USU FROM TipoAGendamento WHERE (TAG = @P1)", out Resp, Framework.Enumeracoes.TiposTAG.Debito_Automatico);                    
                    AGBrow.AGB_USU = Resp;
                    AGBrow.AGBDATAI = DateTime.Now;
                    AGBrow.AGBDescricao = PGFrow.PGFDescricao;
                    AGBrow.AGBDiaTodo = true;
                    AGBrow.AGBI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;                    
                    AGBrow.AGBMemo = "Cadastro autom�tico";
                }
                else
                {                   
                    dPagamentosPeriodicos.AGendaBaseTableAdapter.FillByAGB(dPagamentosPeriodicos.AGendaBase, PGFrow.PGF_AGB);                    
                    AGBrow = dPagamentosPeriodicos.AGendaBase[0];                    
                }             
                if (PGFrow.PGFStatus == (int)StatusPagamentoPeriodico.Encerrado)
                {                    
                    AGBrow.AGBMemo += string.Format("\r\nContrato terminado");                    
                    AGBrow.AGBStatus = 4;                  
                }
                else
                {
                    int antecedencia = 0;
                    AGBrow.AGBMemo += string.Format("\r\nDebito autom�tico esperado para {0:dd/MM/yyyy} refer�nte a {1} com vencimento {2:dd/MM/yyyy}", DataVencimento.AddDays(-antecedencia), CompDebito.Add(PGFrow.PGFRef), DataVencimento);
                    AGBrow.AGBCompetenciaAno = CompDebito.Ano;
                    AGBrow.AGBCompetenciaMes = CompDebito.Mes;                    
                    AGBrow.AGBDataInicio = DataVencimento.AddDays(-antecedencia);
                    AGBrow.AGBDataTermino = AGBrow.AGBDataInicio;                   
                    AGBrow.AGBStatus = 0;
                };           
                AGBrow.AGBA_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                AGBrow.AGBDATAA = DateTime.Now;
                if (AGBrow.RowState == System.Data.DataRowState.Detached)
                    dPagamentosPeriodicos.AGendaBase.AddAGendaBaseRow(AGBrow);
                dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(AGBrow);
                AGBrow.AcceptChanges();              
                PGFrow.PGF_AGB = AGBrow.AGB;
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                EMPTipo.Commit();
                PGFrow.AcceptChanges();              
            }
            catch (Exception e)
            {                
               EMPTipo.Vircatch(e);
                throw e;
            };
            
                        
            return true;

        }
        */

        //private bool Terminado;
        
        
/*
        private static string MascaraCadastro = 
"\r\n" + 
"Q20,0\r\n" + 
"q700\r\n" + 
"OC,Fr\r\n" + 
"N\r\n" + 
"X0,0,6,699,1220\r\n" + 
"A680,13,1,1,4,4,R,\" CADASTRO DE PAGAMENTO MENSAL \"\r\n" + 
"A605,30,1,4,1,1,N,\"Prestador: %PRESTADOR%\"\r\n" + 
"A575,30,1,4,1,1,N,\"%CNPJ%\"\r\n" + 
"A545,30,1,4,1,1,N,\"Servico: %SERVICO%\"\r\n" + 
"\r\n" + 
"A605,720,1,4,1,1,N,\"Primeiro pagamento: %DATAI%\"\r\n" + 
"A575,720,1,4,1,1,N,\"%DATAF%\"\r\n" + 

"A504,30,1,3,1,1,N,\"Obs: O primeiro pagamento (%DATAI%) se refere ao servico prestado em %COMPREF%\"\r\n" +
"A474,30,1,3,1,1,N,\"%PARCELAS%\"\r\n" + 

"A439,13,1,1,3,3,R,\"                VALORES                 \"\r\n" + 


"%RETENCOE%"+
 
"A392,670,1,1,2,2,N,\"Valor Nominal:\"\r\n" + 
"A352,670,1,1,2,2,N,\"Valor do cheque:\"\r\n" + 
"A312,670,1,1,2,2,N,\"Custo total:\"\r\n" + 

"A392,1000,1,1,2,2,R,\"%NOMINAL%\"\r\n" + 
"A352,1000,1,1,2,2,R,\"%LIQUIDO%\"\r\n" + 
"A312,1000,1,1,2,2,R,\"%TOTAL%\"\r\n" + 

"LO211,19,3,1164\r\n" + 
"A202,31,1,3,1,1,N,\"Senhor sindico, favor conferir se os dados estao conforme sua solicitacao\"\r\n" +

"%OBSIMP%" + 

"LO43,659,8,505\r\n" + 
"A30,659,1,1,2,2,N,\"%SINDICO%\"\r\n" + 
"P1\r\n" + 
";";
        /*
        private static CodBar.Impress _ImpCadastro;

        /// <summary>
        /// Impressora de c�pia de cheques
        /// </summary>
        public static CodBar.Impress ImpCadastro
        {
            get
            {
                if (_ImpCadastro == null)
                {
                    _ImpCadastro = new CodBar.Impress(MascaraCadastro);
                    _ImpCadastro.RemoverAcentos = true;
                    string NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de c�pias de Cheque");
                    if (NomeImpressora == "")
                    {
                        System.Windows.Forms.PrintDialog PD = new System.Windows.Forms.PrintDialog();
                        PD.ShowDialog();
                        NomeImpressora = PD.PrinterSettings.PrinterName;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de c�pias de Cheque", NomeImpressora);
                    }
                    _ImpCadastro.NomeImpressora = NomeImpressora;
                }
                return _ImpCadastro;
            }
        }
        */

        private string BlocoRetencao(impCadPeriodico imp, ref int Y, string titulo, decimal Valor, bool Reten)
        {
            //string retorno = string.Format("A{0},25,1,4,1,1,N,\"{1}:{2}\"\r\n",Y,titulo.PadLeft(15),Valor.ToString("n2").PadLeft(8));
            string Texto = string.Format("{0}:{1}",titulo.PadLeft(15),Valor.ToString("n2").PadLeft(8));
            string retorno = imp.CodTXT(20, Y, Texto, 3);
            if (Reten)
                retorno += imp.CodTXT(520, Y, "Reten��o", 2);                
            Y -= 35;
            return retorno;
        }

        /// <summary>
        /// Comando para impressora termica referente ao pagamento peri�dico
        /// </summary>
        /// <returns></returns>
        public override string LinhaDaCopia()
        {
            Competencia Compi = new Competencia(PGFrow.PGFCompetI);
            Compi.CON = PGFrow.PGF_CON;
            return string.Format("Pagamento mensal com in�cio em {0:dd/MM/yyyy}",Compi.DataNaCompetencia(PGFrow.PGFDia));            
        }

        /// <summary>
        /// 
        /// </summary>
        public TipoPagPeriodico TipoPagPeriodico 
        {
            get
            {
                return (TipoPagPeriodico)PGFrow.PGFForma;
            }
        }

        /// <summary>
        /// N�o imprime mais o documento para assinatura do sindico
        /// </summary>
        public bool ImpressoDesativado = true;

        /// <summary>
        /// Imprime se n�o tiver documento ou se for para for�ar
        /// </summary>
        /// <param name="Forcar">For�ar a impress�o</param>
        public override void Imprimir(bool Forcar)
        {
            if (ImpressoDesativado)
                return;
            if (TipoPagPeriodico.EstaNoGrupo(TipoPagPeriodico.DebitoAutomatico, TipoPagPeriodico.DebitoAutomaticoConta, TipoPagPeriodico.Conta, TipoPagPeriodico.FolhaFGTS, TipoPagPeriodico.FolhaPIS, TipoPagPeriodico.FolhaIR))
                return;
            if (!Forcar)
            {
                string comando =
"SELECT     DOC, DOC_PGF, DOCTipo, DOCValido\r\n" +
"FROM         DOCumentacao\r\n" +
"WHERE     (DOC_PGF = @P1) AND (DOCTipo = 0) AND (DOCValido = 1);";
                if (EMPTProc1.STTA.EstaCadastrado(comando, PGFrow.PGF))
                    return;
            };
            DocBacarios.CPFCNPJ CNPJ = null;
            //ImpCadastro.Reset();
            impCadPeriodico ImpCadastro = new impCadPeriodico();
            ImpCadastro.Reset();
            //ImpCadastro.Reset();
            if (PGFrow.IsPGF_FRNNull())
            {
                ImpCadastro.SetCampo("%PRESTADOR%", PGFrow.IsPGFNominalNull() ? "Portador" : PGFrow.PGFNominal);
                ImpCadastro.SetCampo("%CNPJ%", "");
            }
            else
            {
                Cadastros.Fornecedores.Fornecedor  Fornecedor = new Cadastros.Fornecedores.Fornecedor(PGFrow.PGF_FRN);
                ImpCadastro.SetCampo("%PRESTADOR%", Fornecedor.FRNNome);
                CNPJ = Fornecedor.CNPJ;
                if (CNPJ != null)
                {
                    if (CNPJ.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                    {
                        ImpCadastro.SetCampo("%CNPJ%", "C.N.P.J: " + CNPJ.ToString());
                    }
                    else
                    {
                        ImpCadastro.SetCampo("%CNPJ%", "C.P.F: " + CNPJ.ToString());
                    }
                }
            };
            ImpCadastro.SetCampo("%SERVICO%", PGFrow.PGFDescricao);
            Competencia CompI = new Competencia(PGFrow.PGFCompetI);
            CompI.CON = PGFrow.PGF_CON;
            ImpCadastro.SetCampo("%DATAI%",CompI.DataNaCompetencia(PGFrow.PGFDia).ToString("dd/MM/yyyy"));
            if (PGFrow.IsPGFCompetFNull())
            {
                ImpCadastro.SetCampo("%DATAF%", "T�rmino n�o previsto");
                ImpCadastro.SetCampo("%PARCELAS%", "");
            }
            else
            {
                Competencia CompF = new Competencia(PGFrow.PGFCompetF);
                CompF.CON = PGFrow.PGF_CON;
                ImpCadastro.SetCampo("%DATAF%", string.Format("Ultimo pagamento  : {0:dd/MM/yyyy}", CompF.DataNaCompetencia(PGFrow.PGFDia)));
                int Parcelas = 1 + (CompF - CompI);
                ImpCadastro.SetCampo("%PARCELAS%", string.Format("     N�mero de parcelas: {0}", Parcelas));
            }
            Competencia CompRef = CompI.Add(PGFrow.PGFRef);
            ImpCadastro.SetCampo("%COMPREF%", CompRef.ToString());
            int Yimpostos = 500;
            string Retencao = BlocoRetencao(ImpCadastro,ref Yimpostos, "ISS", PGFrow.IsPGFISSNull() ? 0 : PGFrow.PGFISS,true);
            if ((CNPJ == null) || (CNPJ.Tipo == DocBacarios.TipoCpfCnpj.CPF))
            {
                Retencao += BlocoRetencao(ImpCadastro, ref Yimpostos, "INSS Prestador", PGFrow.IsPGFINSSPNull() ? 0 : PGFrow.PGFINSSP, true);
                Retencao += BlocoRetencao(ImpCadastro, ref Yimpostos, "INSS Tomador", PGFrow.IsPGFINSSTNull() ? 0 : PGFrow.PGFINSST, false);
            }
            else
            {
                Retencao += BlocoRetencao(ImpCadastro, ref Yimpostos, "IR", PGFrow.IsPGFIRNull() ? 0 : PGFrow.PGFIR, true);
                Retencao += BlocoRetencao(ImpCadastro, ref Yimpostos, "PIS/COFINS/CSLL", PGFrow.IsPGFPISCOFNull() ? 0 : PGFrow.PGFPISCOF, true);
                Retencao += BlocoRetencao(ImpCadastro, ref Yimpostos, "INSS", PGFrow.IsPGFINSSPNull() ? 0 : PGFrow.PGFINSSP, true);
            };
            ImpCadastro.SetCampo("%RETENCOE%",Retencao);
            ImpCadastro.SetCampo("%NOMINAL%", PGFrow.PGFValor.ToString("n2").PadLeft(9));
            decimal liquido = PGFrow.PGFValor;
            decimal total = PGFrow.PGFValor;
            if (!PGFrow.IsPGFISSNull())
                liquido -= PGFrow.PGFISS;
            if (!PGFrow.IsPGFIRNull())
                liquido -= PGFrow.PGFIR;
            if (!PGFrow.IsPGFPISCOFNull())
                liquido -= PGFrow.PGFPISCOF;
            if (!PGFrow.IsPGFINSSPNull())
                liquido -= PGFrow.PGFINSSP;
            if (!PGFrow.IsPGFINSSTNull())
                total += PGFrow.PGFINSST;
            ImpCadastro.SetCampo("%LIQUIDO%", liquido.ToString("n2").PadLeft(9));
            ImpCadastro.SetCampo("%TOTAL%", total.ToString("n2").PadLeft(9));
            string txtObs = "";
            if (!PGFrow.IsPGFObsImpressoNull())                           
            {
                //string[] separador = new string[] { "\r\n" };
                string[] linhas = PGFrow.PGFObsImpresso.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                int Y = 170;
                foreach (string linha in linhas)
                {
                    txtObs += string.Format("A{0},31,1,3,1,1,N,\"{1}\"\r\n",Y, linha);
                    Y -= 20;
                }
            }
            ImpCadastro.SetCampo("%OBSIMP%", txtObs);
            string nomesindico;
            string BuscaSindico =
"SELECT     PESSOAS.PESNome\r\n" +
"FROM         View_SINDICOS INNER JOIN\r\n" +
"                      PESSOAS ON View_SINDICOS.CDR_PES = PESSOAS.PES\r\n" +
"WHERE     (View_SINDICOS.CDR_CON = @P1);";
            EMPTProc1.STTA.BuscaEscalar(BuscaSindico, out nomesindico, PGFrow.PGF_CON);
            int brancos = (25 + nomesindico.Length) / 2;
            ImpCadastro.SetCampo("%SINDICO%", nomesindico.PadLeft(brancos));
            ImpCadastro.ImprimeDOC();
        }
    }


    /*
    /// <summary>
    /// Status
    /// </summary>
    [Obsolete("Foi tranferio para AbstratosNeon")]
    public enum StatusPagamentoPeriodico
    {
        /// <summary>
        /// inativo
        /// </summary>
        Inativo = 0,
        /// <summary>
        /// O documento nao foi cadastrado
        /// </summary>
        AtivadoSemDOC = 1,
        /// <summary>
        /// Ativado com documento
        /// </summary>
        Ativado = 2,
        /// <summary>
        /// Encerrado
        /// </summary>
        Encerrado = 3,        
    }

    public enum TiposDOC
    {
        Descritivo = 0,
        Contrato = 1,
        Apolice = 2,
        Outro = 3
    }
    */

}
