/*
LH - 25/12/2014 14:20  14.1.4.96 - Pagamento com boleto integrado na mesma tela de emiss�o de cheque
*/

using System;
using System.Windows.Forms;
using CompontesBasicos;
using ContaPagarProc;

namespace ContaPagar.Follow
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cContratos : ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public dNOtAs.PaGamentosFixosRow PGFrow = null;

        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup;

        /// <summary>
        /// 
        /// </summary>
        public cContratos()
        {
            InitializeComponent();
            dFornecedoresLookup = new CadastrosProc.Fornecedores.dFornecedoresLookup();
            dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
            dFornecedoresGradeBindingSource.DataSource = dFornecedoresLookup;
        }

        //private static string ComandoTrava = "SELECT NOA FROM NOtAs WHERE (NOANumero = @P1) AND (NOA_FRN = @P2);";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            switch (Resultado)
            {
                case DialogResult.OK:
                case DialogResult.Yes:                                      
                        PGFrow = (dNOtAs.PaGamentosFixosRow)gridView1.GetFocusedDataRow();                        
                        base.FechaTela(Resultado);                    
                    break;
                case DialogResult.Cancel:
                case DialogResult.No:
                    base.FechaTela(Resultado);
                    break;
            }        
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {           
            if (e.ListSourceRowIndex < 0)
                return;
            if ((e.Column == colPGFDia) || (e.Column == colPGFProximaCompetencia))
            {
                //dNOtAs.PaGamentosFixosRow row = (dNOtAs.PaGamentosFixosRow)gridView1.GetDataRow(e.RowHandle);

                dNOtAs.PaGamentosFixosRow row = ((dNOtAs)dNOtAsBindingSource.DataSource).PaGamentosFixos[e.ListSourceRowIndex];
                if (row == null)
                    return;

                Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(row.PGFProximaCompetencia, row.PGF_CON,null);
                Framework.objetosNeon.Competencia CompPAG = Comp.CloneCompet(row.PGFRef); 
                //Comp.CON = row.PGF_CON;
                if (e.Column == colPGFProximaCompetencia)                                    
                    e.DisplayText = Comp.ToString();                
                else if (e.Column == colPGFDia)
                {
                    DateTime Vencimento = CompPAG.DataNaCompetencia(row.PGFDia, Framework.objetosNeon.Competencia.Regime.mes);
                    e.DisplayText = Vencimento.ToString("dd/MM/yyyy");
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void VersaoInterna()
        {
            panelBotoes.Visible = true;
            BotoesOKCancel(false);
        }
        
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.No);
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
                FechaTela(DialogResult.Yes);
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            FechaTela(DialogResult.Yes);
        }
    }
}
