﻿namespace ContaPagar.Follow {
    
    
    public partial class dPagPerTipo 
    {
        private dPagPerTipoTableAdapters.PeriodicosTableAdapter periodicosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Periodicos
        /// </summary>
        public dPagPerTipoTableAdapters.PeriodicosTableAdapter PeriodicosTableAdapter
        {
            get
            {
                if (periodicosTableAdapter == null)
                {
                    periodicosTableAdapter = new dPagPerTipoTableAdapters.PeriodicosTableAdapter();
                    periodicosTableAdapter.TrocarStringDeConexao();
                };
                return periodicosTableAdapter;
            }
        }

        private dPagPerTipoTableAdapters.HistoricoTableAdapter historicoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Historico
        /// </summary>
        public dPagPerTipoTableAdapters.HistoricoTableAdapter HistoricoTableAdapter
        {
            get
            {
                if (historicoTableAdapter == null)
                {
                    historicoTableAdapter = new dPagPerTipoTableAdapters.HistoricoTableAdapter();
                    historicoTableAdapter.TrocarStringDeConexao();
                };
                return historicoTableAdapter;
            }
        }
    }
}
