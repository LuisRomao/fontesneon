namespace ContaPagar.Follow
{
    partial class cPagamentosPeriodicosCampos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cPagamentosPeriodicosCampos));
            System.Windows.Forms.Label label1;
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPAG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_NOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPAGIMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCHEFavorecido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHENumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPAG_CHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.fKNOtAsPaGamentosFixosBindingSource = new System.Windows.Forms.BindingSource();
            this.PaGamentosFixosbindingSource = new System.Windows.Forms.BindingSource();
            this.dPagamentosPeriodicos = new ContaPagarProc.Follow.dPagamentosPeriodicos();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOANumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADataEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOATotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colNOACompet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBAJ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryBotAJComp = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.lblContaDebito = new System.Windows.Forms.Label();
            this.lblContaCredito = new System.Windows.Forms.Label();
            this.fornecedoresBindingSource = new System.Windows.Forms.BindingSource();
            this.grcForn = new DevExpress.XtraEditors.GroupControl();
            this.lookUpFOR1 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpFOR2 = new DevExpress.XtraEditors.LookUpEdit();
            this.textNominal = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.SubPLanoPLAnocontasBindingSource = new System.Windows.Forms.BindingSource();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lookUpEditPLA1 = new DevExpress.XtraEditors.LookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource();
            this.lookUpEditPLA2 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditValor = new DevExpress.XtraEditors.CalcEdit();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cCompetFimRef = new Framework.objetosNeon.cCompet();
            this.cCompetIniRef = new Framework.objetosNeon.cCompet();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.panelImpostos = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditISS = new DevExpress.XtraEditors.CalcEdit();
            this.calcEditINSSP = new DevExpress.XtraEditors.CalcEdit();
            this.chPIS = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.chIR = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.chINSST = new DevExpress.XtraEditors.CheckEdit();
            this.calcEditINSST = new DevExpress.XtraEditors.CalcEdit();
            this.chINSSP = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.chISS = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditIR = new DevExpress.XtraEditors.CalcEdit();
            this.calcEditPIS = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.calcLIQUIDO = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.GC_identificacao = new DevExpress.XtraEditors.GroupControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.spin_PGFIdentificacao = new DevExpress.XtraEditors.SpinEdit();
            this.textEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.BotImprimir = new DevExpress.XtraEditors.SimpleButton();
            this.memoInterno = new DevExpress.XtraEditors.MemoEdit();
            this.memoImpresso = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.dateEdit4 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.cmbContaCredito = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.chkDepProprioCon = new DevExpress.XtraEditors.CheckEdit();
            this.labelDadosDep = new System.Windows.Forms.Label();
            this.cmbContaDebito = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.textEditComplemento = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.ComboStatus = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.componenteWEB1 = new CompontesBasicos.ComponenteWEB();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.fKDOCumentacaoPaGamentosFixosBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDOCTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colDOCEXT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCValido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.BotCorrecao = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit3 = new DevExpress.XtraEditors.DateEdit();
            this.cCompetProx = new Framework.objetosNeon.cCompet();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKNOtAsPaGamentosFixosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaGamentosFixosbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPagamentosPeriodicos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBotAJComp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcForn)).BeginInit();
            this.grcForn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textNominal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubPLanoPLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditValor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelImpostos)).BeginInit();
            this.panelImpostos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditISS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditINSSP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chPIS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chIR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chINSST.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditINSST.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chINSSP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chISS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditIR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditPIS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcLIQUIDO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC_identificacao)).BeginInit();
            this.GC_identificacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spin_PGFIdentificacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoInterno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoImpresso.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbContaCredito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepProprioCon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbContaDebito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditComplemento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboStatus.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKDOCumentacaoPaGamentosFixosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            label1.Location = new System.Drawing.Point(108, 54);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(55, 13);
            label1.TabIndex = 31;
            label1.Text = "Nominal:";
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.gridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPAG,
            this.colPAGValor,
            this.colPAGVencimento,
            this.colPAG_NOA,
            this.colPAGTipo,
            this.colPAGIMP,
            this.colCHEFavorecido,
            this.colCHEEmissao,
            this.colCHENumero,
            this.colCHEStatus,
            this.colPAG_CHE,
            this.gridColumn2});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView2_CustomRowCellEdit);
            // 
            // colPAG
            // 
            this.colPAG.Caption = "PAG";
            this.colPAG.FieldName = "PAG";
            this.colPAG.Name = "colPAG";
            this.colPAG.OptionsColumn.ReadOnly = true;
            // 
            // colPAGValor
            // 
            this.colPAGValor.Caption = "Valor";
            this.colPAGValor.DisplayFormat.FormatString = "n2";
            this.colPAGValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPAGValor.FieldName = "PAGValor";
            this.colPAGValor.Name = "colPAGValor";
            this.colPAGValor.Visible = true;
            this.colPAGValor.VisibleIndex = 0;
            // 
            // colPAGVencimento
            // 
            this.colPAGVencimento.Caption = "Vencimento";
            this.colPAGVencimento.FieldName = "PAGVencimento";
            this.colPAGVencimento.Name = "colPAGVencimento";
            this.colPAGVencimento.Visible = true;
            this.colPAGVencimento.VisibleIndex = 1;
            // 
            // colPAG_NOA
            // 
            this.colPAG_NOA.Caption = "NOA";
            this.colPAG_NOA.FieldName = "PAG_NOA";
            this.colPAG_NOA.Name = "colPAG_NOA";
            // 
            // colPAGTipo
            // 
            this.colPAGTipo.Caption = "Tipo";
            this.colPAGTipo.ColumnEdit = this.repositoryItemImageComboBox5;
            this.colPAGTipo.FieldName = "PAGTipo";
            this.colPAGTipo.Name = "colPAGTipo";
            this.colPAGTipo.Visible = true;
            this.colPAGTipo.VisibleIndex = 2;
            // 
            // repositoryItemImageComboBox5
            // 
            this.repositoryItemImageComboBox5.AutoHeight = false;
            this.repositoryItemImageComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox5.Name = "repositoryItemImageComboBox5";
            // 
            // colPAGIMP
            // 
            this.colPAGIMP.Caption = "Guia";
            this.colPAGIMP.ColumnEdit = this.repositoryItemImageComboBox4;
            this.colPAGIMP.FieldName = "PAGIMP";
            this.colPAGIMP.Name = "colPAGIMP";
            this.colPAGIMP.Visible = true;
            this.colPAGIMP.VisibleIndex = 3;
            // 
            // repositoryItemImageComboBox4
            // 
            this.repositoryItemImageComboBox4.AutoHeight = false;
            this.repositoryItemImageComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox4.Name = "repositoryItemImageComboBox4";
            // 
            // colCHEFavorecido
            // 
            this.colCHEFavorecido.Caption = "Favorecido";
            this.colCHEFavorecido.FieldName = "CHEFavorecido";
            this.colCHEFavorecido.Name = "colCHEFavorecido";
            this.colCHEFavorecido.Visible = true;
            this.colCHEFavorecido.VisibleIndex = 4;
            // 
            // colCHEEmissao
            // 
            this.colCHEEmissao.Caption = "Emiss�o";
            this.colCHEEmissao.FieldName = "CHEEmissao";
            this.colCHEEmissao.Name = "colCHEEmissao";
            this.colCHEEmissao.Visible = true;
            this.colCHEEmissao.VisibleIndex = 5;
            // 
            // colCHENumero
            // 
            this.colCHENumero.Caption = "Cheque";
            this.colCHENumero.FieldName = "CHENumero";
            this.colCHENumero.Name = "colCHENumero";
            this.colCHENumero.Visible = true;
            this.colCHENumero.VisibleIndex = 6;
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.Caption = "Status";
            this.colCHEStatus.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.Visible = true;
            this.colCHEStatus.VisibleIndex = 7;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colPAG_CHE
            // 
            this.colPAG_CHE.Caption = "CHE";
            this.colPAG_CHE.FieldName = "PAG_CHE";
            this.colPAG_CHE.Name = "colPAG_CHE";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 8;
            this.gridColumn2.Width = 25;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.fKNOtAsPaGamentosFixosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "FK_PAGamentos_NOtAs";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 78);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemImageComboBox2,
            this.repositoryItemImageComboBox3,
            this.repositoryItemImageComboBox4,
            this.repositoryItemImageComboBox5,
            this.repositoryItemButtonEdit2,
            this.repositoryBotAJComp});
            this.gridControl1.Size = new System.Drawing.Size(1513, 641);
            this.gridControl1.TabIndex = 61;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // fKNOtAsPaGamentosFixosBindingSource
            // 
            this.fKNOtAsPaGamentosFixosBindingSource.DataMember = "FK_NOtAs_PaGamentosFixos";
            this.fKNOtAsPaGamentosFixosBindingSource.DataSource = this.PaGamentosFixosbindingSource;
            // 
            // PaGamentosFixosbindingSource
            // 
            this.PaGamentosFixosbindingSource.DataMember = "PaGamentosFixos";
            this.PaGamentosFixosbindingSource.DataSource = this.dPagamentosPeriodicos;
            // 
            // dPagamentosPeriodicos
            // 
            this.dPagamentosPeriodicos.DataSetName = "dPagamentosPeriodicos";
            this.dPagamentosPeriodicos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNOA,
            this.colNOANumero,
            this.colNOADataEmissao,
            this.colNOATotal,
            this.colNOAServico,
            this.colNOAStatus,
            this.gridColumn1,
            this.colNOACompet,
            this.gridColumnBAJ});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNOACompet, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridView1_CustomColumnDisplayText);
            // 
            // colNOA
            // 
            this.colNOA.Caption = "NOA";
            this.colNOA.FieldName = "NOA";
            this.colNOA.Name = "colNOA";
            this.colNOA.OptionsColumn.FixedWidth = true;
            this.colNOA.OptionsColumn.ReadOnly = true;
            this.colNOA.Width = 161;
            // 
            // colNOANumero
            // 
            this.colNOANumero.Caption = "Nota";
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.OptionsColumn.FixedWidth = true;
            this.colNOANumero.Visible = true;
            this.colNOANumero.VisibleIndex = 2;
            this.colNOANumero.Width = 74;
            // 
            // colNOADataEmissao
            // 
            this.colNOADataEmissao.Caption = "Emiss�o";
            this.colNOADataEmissao.FieldName = "NOADataEmissao";
            this.colNOADataEmissao.Name = "colNOADataEmissao";
            this.colNOADataEmissao.OptionsColumn.FixedWidth = true;
            this.colNOADataEmissao.Visible = true;
            this.colNOADataEmissao.VisibleIndex = 3;
            this.colNOADataEmissao.Width = 94;
            // 
            // colNOATotal
            // 
            this.colNOATotal.Caption = "Valor";
            this.colNOATotal.DisplayFormat.FormatString = "n2";
            this.colNOATotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNOATotal.FieldName = "NOATotal";
            this.colNOATotal.Name = "colNOATotal";
            this.colNOATotal.OptionsColumn.FixedWidth = true;
            this.colNOATotal.Visible = true;
            this.colNOATotal.VisibleIndex = 4;
            this.colNOATotal.Width = 100;
            // 
            // colNOAServico
            // 
            this.colNOAServico.Caption = "Servi�o";
            this.colNOAServico.FieldName = "NOAServico";
            this.colNOAServico.Name = "colNOAServico";
            this.colNOAServico.Visible = true;
            this.colNOAServico.VisibleIndex = 5;
            this.colNOAServico.Width = 426;
            // 
            // colNOAStatus
            // 
            this.colNOAStatus.Caption = "Status";
            this.colNOAStatus.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colNOAStatus.FieldName = "NOAStatus";
            this.colNOAStatus.Name = "colNOAStatus";
            this.colNOAStatus.OptionsColumn.FixedWidth = true;
            this.colNOAStatus.Visible = true;
            this.colNOAStatus.VisibleIndex = 6;
            this.colNOAStatus.Width = 140;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowSize = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 30;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.Image = global::ContaPagar.Properties.Resources.Nota1;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colNOACompet
            // 
            this.colNOACompet.Caption = "Compet�ncia";
            this.colNOACompet.FieldName = "NOACompet";
            this.colNOACompet.Name = "colNOACompet";
            this.colNOACompet.OptionsColumn.FixedWidth = true;
            this.colNOACompet.Visible = true;
            this.colNOACompet.VisibleIndex = 0;
            this.colNOACompet.Width = 79;
            // 
            // gridColumnBAJ
            // 
            this.gridColumnBAJ.Caption = "gridColumnBAJ";
            this.gridColumnBAJ.Name = "gridColumnBAJ";
            this.gridColumnBAJ.OptionsColumn.ShowCaption = false;
            this.gridColumnBAJ.Visible = true;
            this.gridColumnBAJ.VisibleIndex = 1;
            this.gridColumnBAJ.Width = 29;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            editorButtonImageOptions2.Image = global::ContaPagar.Properties.Resources.impCheque1;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // repositoryBotAJComp
            // 
            this.repositoryBotAJComp.AutoHeight = false;
            this.repositoryBotAJComp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryBotAJComp.Name = "repositoryBotAJComp";
            this.repositoryBotAJComp.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryBotAJComp.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryBotAJComp_ButtonClick);
            // 
            // lblContaDebito
            // 
            this.lblContaDebito.AutoSize = true;
            this.lblContaDebito.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblContaDebito.Location = new System.Drawing.Point(10, 57);
            this.lblContaDebito.Name = "lblContaDebito";
            this.lblContaDebito.Size = new System.Drawing.Size(83, 13);
            this.lblContaDebito.TabIndex = 33;
            this.lblContaDebito.Text = "Conta D�bito:";
            this.lblContaDebito.Visible = false;
            // 
            // lblContaCredito
            // 
            this.lblContaCredito.AutoSize = true;
            this.lblContaCredito.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblContaCredito.ForeColor = System.Drawing.Color.Red;
            this.lblContaCredito.Location = new System.Drawing.Point(697, 27);
            this.lblContaCredito.Name = "lblContaCredito";
            this.lblContaCredito.Size = new System.Drawing.Size(330, 13);
            this.lblContaCredito.TabIndex = 34;
            this.lblContaCredito.Text = "Conta Corrente para Cr�dito:  Nenhuma conta cadastrada";
            this.lblContaCredito.Visible = false;
            // 
            // fornecedoresBindingSource
            // 
            this.fornecedoresBindingSource.DataMember = "FRNLookup";
            this.fornecedoresBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // grcForn
            // 
            this.grcForn.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grcForn.Appearance.Options.UseFont = true;
            this.grcForn.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grcForn.AppearanceCaption.Options.UseFont = true;
            this.grcForn.Controls.Add(this.lblContaCredito);
            this.grcForn.Controls.Add(label1);
            this.grcForn.Controls.Add(this.lookUpFOR1);
            this.grcForn.Controls.Add(this.lookUpFOR2);
            this.grcForn.Controls.Add(this.textNominal);
            this.grcForn.Dock = System.Windows.Forms.DockStyle.Top;
            this.grcForn.Location = new System.Drawing.Point(0, 140);
            this.grcForn.Name = "grcForn";
            this.grcForn.Size = new System.Drawing.Size(1477, 82);
            this.grcForn.TabIndex = 16;
            this.grcForn.Text = "FORNECEDOR";
            // 
            // lookUpFOR1
            // 
            this.lookUpFOR1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGF_FRN", true));
            this.lookUpFOR1.Location = new System.Drawing.Point(5, 23);
            this.lookUpFOR1.Name = "lookUpFOR1";
            this.lookUpFOR1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpFOR1.Properties.Appearance.Options.UseFont = true;
            this.lookUpFOR1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.lookUpFOR1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "Name1"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Name2")});
            this.lookUpFOR1.Properties.DataSource = this.fornecedoresBindingSource;
            this.lookUpFOR1.Properties.DisplayMember = "FRNCnpj";
            this.lookUpFOR1.Properties.NullText = "CNPJ";
            this.lookUpFOR1.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.OnlyInPopup;
            this.lookUpFOR1.Properties.ValueMember = "FRN";
            this.lookUpFOR1.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpFOR1_Properties_ButtonClick);
            this.lookUpFOR1.Size = new System.Drawing.Size(198, 22);
            this.lookUpFOR1.TabIndex = 30;
            this.lookUpFOR1.TabStop = false;
            this.lookUpFOR1.EditValueChanged += new System.EventHandler(this.lookUpFOR1_EditValueChanged);
            // 
            // lookUpFOR2
            // 
            this.lookUpFOR2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGF_FRN", true));
            this.lookUpFOR2.Location = new System.Drawing.Point(209, 23);
            this.lookUpFOR2.Name = "lookUpFOR2";
            this.lookUpFOR2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpFOR2.Properties.Appearance.Options.UseFont = true;
            this.lookUpFOR2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpFOR2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "Name1"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Name2", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpFOR2.Properties.DataSource = this.fornecedoresBindingSource;
            this.lookUpFOR2.Properties.DisplayMember = "FRNNome";
            this.lookUpFOR2.Properties.ValueMember = "FRN";
            this.lookUpFOR2.Size = new System.Drawing.Size(474, 22);
            this.lookUpFOR2.TabIndex = 29;
            this.lookUpFOR2.TabStop = false;
            this.lookUpFOR2.EditValueChanged += new System.EventHandler(this.lookUpFOR1_EditValueChanged);
            this.lookUpFOR2.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.lookUpFOR2_EditValueChanging);
            // 
            // textNominal
            // 
            this.textNominal.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFNominal", true));
            this.textNominal.Location = new System.Drawing.Point(209, 51);
            this.textNominal.MenuManager = this.BarManager_F;
            this.textNominal.Name = "textNominal";
            this.textNominal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNominal.Properties.Appearance.Options.UseFont = true;
            this.textNominal.Size = new System.Drawing.Size(474, 22);
            this.textNominal.TabIndex = 12;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 26);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 16);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "Dia de vencto";
            // 
            // groupControl3
            // 
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.checkEdit1);
            this.groupControl3.Controls.Add(this.checkEdit2);
            this.groupControl3.Controls.Add(this.lookUpEdit1);
            this.groupControl3.Controls.Add(this.comboBoxEdit1);
            this.groupControl3.Controls.Add(this.lookUpEditPLA1);
            this.groupControl3.Controls.Add(this.lookUpEditPLA2);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(0, 222);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1477, 98);
            this.groupControl3.TabIndex = 19;
            this.groupControl3.Text = "Classifica��o";
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFCompetNaDescricao", true));
            this.checkEdit1.Location = new System.Drawing.Point(207, 74);
            this.checkEdit1.MenuManager = this.BarManager_F;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Caption = "Colocar Compet�ncia na descri��o";
            this.checkEdit1.Size = new System.Drawing.Size(228, 19);
            this.checkEdit1.TabIndex = 67;
            // 
            // checkEdit2
            // 
            this.checkEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFParcelaNaDesc", true));
            this.checkEdit2.Location = new System.Drawing.Point(5, 73);
            this.checkEdit2.MenuManager = this.BarManager_F;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit2.Properties.Appearance.Options.UseFont = true;
            this.checkEdit2.Properties.Caption = "Colocar parcela na descri��o";
            this.checkEdit2.Size = new System.Drawing.Size(196, 19);
            this.checkEdit2.TabIndex = 68;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGF_SPL", true));
            this.lookUpEdit1.Enabled = false;
            this.lookUpEdit1.Location = new System.Drawing.Point(3, 48);
            this.lookUpEdit1.MenuManager = this.BarManager_F;
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLCodigo", "C�digo", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLDescricao", "Descri��o", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLValorISS", "ISS (%)", 15, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEdit1.Properties.DataSource = this.SubPLanoPLAnocontasBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "Descritivo";
            this.lookUpEdit1.Properties.PopupWidth = 400;
            this.lookUpEdit1.Properties.ValueMember = "SPL";
            this.lookUpEdit1.Size = new System.Drawing.Size(200, 20);
            this.lookUpEdit1.TabIndex = 19;
            this.lookUpEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit1_ButtonClick);
            // 
            // SubPLanoPLAnocontasBindingSource
            // 
            this.SubPLanoPLAnocontasBindingSource.DataMember = "SubPLano";
            this.SubPLanoPLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFDescricao", true));
            this.comboBoxEdit1.Location = new System.Drawing.Point(209, 46);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus)});
            this.comboBoxEdit1.Properties.Validating += new System.ComponentModel.CancelEventHandler(this.comboBoxEdit1_Properties_Validating);
            this.comboBoxEdit1.Size = new System.Drawing.Size(474, 22);
            this.comboBoxEdit1.TabIndex = 18;
            this.comboBoxEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.comboBoxEdit1_ButtonClick);
            // 
            // lookUpEditPLA1
            // 
            this.lookUpEditPLA1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGF_PLA", true));
            this.lookUpEditPLA1.Location = new System.Drawing.Point(5, 23);
            this.lookUpEditPLA1.Name = "lookUpEditPLA1";
            this.lookUpEditPLA1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditPLA1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditPLA1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditPLA1.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA1.Properties.DisplayMember = "PLA";
            this.lookUpEditPLA1.Properties.ValueMember = "PLA";
            this.lookUpEditPLA1.Size = new System.Drawing.Size(198, 22);
            this.lookUpEditPLA1.TabIndex = 0;
            this.lookUpEditPLA1.EditValueChanged += new System.EventHandler(this.lookUpEditPLA1_EditValueChanged);
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // lookUpEditPLA2
            // 
            this.lookUpEditPLA2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGF_PLA", true));
            this.lookUpEditPLA2.Location = new System.Drawing.Point(209, 23);
            this.lookUpEditPLA2.Name = "lookUpEditPLA2";
            this.lookUpEditPLA2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditPLA2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditPLA2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditPLA2.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA2.Properties.DisplayMember = "PLADescricao";
            this.lookUpEditPLA2.Properties.ValueMember = "PLA";
            this.lookUpEditPLA2.Size = new System.Drawing.Size(474, 22);
            this.lookUpEditPLA2.TabIndex = 1;
            this.lookUpEditPLA2.TabStop = false;
            this.lookUpEditPLA2.EditValueChanged += new System.EventHandler(this.lookUpEditPLA1_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(5, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 16);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "Bruto";
            // 
            // calcEditValor
            // 
            this.calcEditValor.Location = new System.Drawing.Point(115, 25);
            this.calcEditValor.MenuManager = this.BarManager_F;
            this.calcEditValor.Name = "calcEditValor";
            this.calcEditValor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditValor.Properties.Appearance.Options.UseFont = true;
            this.calcEditValor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditValor.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditValor.Properties.Mask.EditMask = "n2";
            this.calcEditValor.Size = new System.Drawing.Size(100, 22);
            this.calcEditValor.TabIndex = 21;
            this.calcEditValor.EditValueChanged += new System.EventHandler(this.calcEdit1_EditValueChanged);
            this.calcEditValor.Validating += new System.ComponentModel.CancelEventHandler(this.calcEdit1_Validating);
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFForma", true));
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(5, 25);
            this.imageComboBoxEdit1.MenuManager = this.BarManager_F;
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imageComboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(522, 22);
            this.imageComboBoxEdit1.TabIndex = 23;
            this.imageComboBoxEdit1.EditValueChanged += new System.EventHandler(this.imageComboBoxEdit1_EditValueChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.cCompetFimRef);
            this.groupControl1.Controls.Add(this.cCompetIniRef);
            this.groupControl1.Controls.Add(this.spinEdit1);
            this.groupControl1.Controls.Add(this.dateEdit2);
            this.groupControl1.Controls.Add(this.dateEdit1);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 320);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1477, 86);
            this.groupControl1.TabIndex = 57;
            this.groupControl1.Text = "Data";
            // 
            // cCompetFimRef
            // 
            this.cCompetFimRef.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetFimRef.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompetFimRef.Appearance.Options.UseBackColor = true;
            this.cCompetFimRef.Appearance.Options.UseFont = true;
            this.cCompetFimRef.CaixaAltaGeral = true;
            this.cCompetFimRef.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.PaGamentosFixosbindingSource, "PGFCompetF", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompetFimRef.DataBindings.Add(new System.Windows.Forms.Binding("CON", this.PaGamentosFixosbindingSource, "PGF_CON", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompetFimRef.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetFimRef.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetFimRef.IsNull = false;
            this.cCompetFimRef.Location = new System.Drawing.Point(232, 51);
            this.cCompetFimRef.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetFimRef.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetFimRef.Name = "cCompetFimRef";
            this.cCompetFimRef.PermiteNulo = true;
            this.cCompetFimRef.ReadOnly = false;
            this.cCompetFimRef.Size = new System.Drawing.Size(137, 27);
            this.cCompetFimRef.somenteleitura = false;
            this.cCompetFimRef.TabIndex = 65;
            this.cCompetFimRef.Titulo = null;
            this.cCompetFimRef.OnChange += new System.EventHandler(this.cCompetFimRef_OnChange);
            // 
            // cCompetIniRef
            // 
            this.cCompetIniRef.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetIniRef.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompetIniRef.Appearance.Options.UseBackColor = true;
            this.cCompetIniRef.Appearance.Options.UseFont = true;
            this.cCompetIniRef.CaixaAltaGeral = true;
            this.cCompetIniRef.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.PaGamentosFixosbindingSource, "PGFCompetI", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompetIniRef.DataBindings.Add(new System.Windows.Forms.Binding("CON", this.PaGamentosFixosbindingSource, "PGF_CON", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompetIniRef.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetIniRef.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetIniRef.IsNull = false;
            this.cCompetIniRef.Location = new System.Drawing.Point(232, 21);
            this.cCompetIniRef.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetIniRef.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetIniRef.Name = "cCompetIniRef";
            this.cCompetIniRef.PermiteNulo = false;
            this.cCompetIniRef.ReadOnly = false;
            this.cCompetIniRef.Size = new System.Drawing.Size(137, 27);
            this.cCompetIniRef.somenteleitura = false;
            this.cCompetIniRef.TabIndex = 64;
            this.cCompetIniRef.Titulo = null;
            this.cCompetIniRef.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // spinEdit1
            // 
            this.spinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFDia", true));
            this.spinEdit1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(115, 23);
            this.spinEdit1.MenuManager = this.BarManager_F;
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit1.Properties.Appearance.Options.UseFont = true;
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.DisplayFormat.FormatString = "f0";
            this.spinEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEdit1.Properties.EditFormat.FormatString = "f0";
            this.spinEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEdit1.Properties.Mask.EditMask = "f0";
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(48, 22);
            this.spinEdit1.TabIndex = 61;
            this.spinEdit1.EditValueChanged += new System.EventHandler(this.spinEdit1_EditValueChanged);
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.Location = new System.Drawing.Point(375, 55);
            this.dateEdit2.MenuManager = this.BarManager_F;
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.Appearance.Options.UseFont = true;
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit2.Properties.ReadOnly = true;
            this.dateEdit2.Size = new System.Drawing.Size(135, 22);
            this.dateEdit2.TabIndex = 59;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(375, 25);
            this.dateEdit1.MenuManager = this.BarManager_F;
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.dateEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(135, 22);
            this.dateEdit1.TabIndex = 58;
            this.dateEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEdit1_ButtonClick);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(175, 56);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(51, 16);
            this.labelControl5.TabIndex = 57;
            this.labelControl5.Text = "T�rmino";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(175, 26);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(34, 16);
            this.labelControl4.TabIndex = 19;
            this.labelControl4.Text = "In�cio";
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.panelImpostos);
            this.groupControl4.Controls.Add(this.labelControl15);
            this.groupControl4.Controls.Add(this.calcEdit1);
            this.groupControl4.Controls.Add(this.calcLIQUIDO);
            this.groupControl4.Controls.Add(this.labelControl12);
            this.groupControl4.Controls.Add(this.labelControl2);
            this.groupControl4.Controls.Add(this.calcEditValor);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(0, 406);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1477, 147);
            this.groupControl4.TabIndex = 58;
            this.groupControl4.Text = "Valores";
            // 
            // panelImpostos
            // 
            this.panelImpostos.Controls.Add(this.labelControl7);
            this.panelImpostos.Controls.Add(this.calcEditISS);
            this.panelImpostos.Controls.Add(this.calcEditINSSP);
            this.panelImpostos.Controls.Add(this.chPIS);
            this.panelImpostos.Controls.Add(this.labelControl8);
            this.panelImpostos.Controls.Add(this.chIR);
            this.panelImpostos.Controls.Add(this.labelControl9);
            this.panelImpostos.Controls.Add(this.chINSST);
            this.panelImpostos.Controls.Add(this.calcEditINSST);
            this.panelImpostos.Controls.Add(this.chINSSP);
            this.panelImpostos.Controls.Add(this.labelControl10);
            this.panelImpostos.Controls.Add(this.chISS);
            this.panelImpostos.Controls.Add(this.labelControl11);
            this.panelImpostos.Controls.Add(this.calcEditIR);
            this.panelImpostos.Controls.Add(this.calcEditPIS);
            this.panelImpostos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelImpostos.Location = new System.Drawing.Point(2, 58);
            this.panelImpostos.Name = "panelImpostos";
            this.panelImpostos.Size = new System.Drawing.Size(1473, 87);
            this.panelImpostos.TabIndex = 43;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(-3, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(21, 16);
            this.labelControl7.TabIndex = 24;
            this.labelControl7.Text = "ISS";
            // 
            // calcEditISS
            // 
            this.calcEditISS.Location = new System.Drawing.Point(115, 2);
            this.calcEditISS.MenuManager = this.BarManager_F;
            this.calcEditISS.Name = "calcEditISS";
            this.calcEditISS.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.calcEditISS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditISS.Properties.Appearance.Options.UseFont = true;
            this.calcEditISS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditISS.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditISS.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditISS.Properties.Mask.EditMask = "n2";
            this.calcEditISS.Properties.NullText = "--";
            this.calcEditISS.Properties.NullValuePrompt = "--";
            this.calcEditISS.Size = new System.Drawing.Size(100, 22);
            this.calcEditISS.TabIndex = 25;
            this.calcEditISS.EditValueChanged += new System.EventHandler(this.calcEdit2_EditValueChanged);
            // 
            // calcEditINSSP
            // 
            this.calcEditINSSP.Location = new System.Drawing.Point(115, 30);
            this.calcEditINSSP.MenuManager = this.BarManager_F;
            this.calcEditINSSP.Name = "calcEditINSSP";
            this.calcEditINSSP.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.calcEditINSSP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditINSSP.Properties.Appearance.Options.UseFont = true;
            this.calcEditINSSP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditINSSP.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditINSSP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditINSSP.Properties.Mask.EditMask = "n2";
            this.calcEditINSSP.Properties.NullText = "--";
            this.calcEditINSSP.Properties.NullValuePrompt = "--";
            this.calcEditINSSP.Size = new System.Drawing.Size(100, 22);
            this.calcEditINSSP.TabIndex = 26;
            this.calcEditINSSP.EditValueChanged += new System.EventHandler(this.calcEdit3_EditValueChanged);
            // 
            // chPIS
            // 
            this.chPIS.EditValue = true;
            this.chPIS.Location = new System.Drawing.Point(479, 32);
            this.chPIS.MenuManager = this.BarManager_F;
            this.chPIS.Name = "chPIS";
            this.chPIS.Properties.Caption = "";
            this.chPIS.Size = new System.Drawing.Size(21, 19);
            this.chPIS.TabIndex = 40;
            this.chPIS.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(-3, 33);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(110, 16);
            this.labelControl8.TabIndex = 27;
            this.labelControl8.Text = "INSS (Prestador)";
            // 
            // chIR
            // 
            this.chIR.EditValue = true;
            this.chIR.Location = new System.Drawing.Point(479, 4);
            this.chIR.MenuManager = this.BarManager_F;
            this.chIR.Name = "chIR";
            this.chIR.Properties.Caption = "";
            this.chIR.Size = new System.Drawing.Size(21, 19);
            this.chIR.TabIndex = 39;
            this.chIR.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(-3, 58);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(101, 16);
            this.labelControl9.TabIndex = 28;
            this.labelControl9.Text = "INSS (Tomador)";
            // 
            // chINSST
            // 
            this.chINSST.EditValue = true;
            this.chINSST.Location = new System.Drawing.Point(221, 60);
            this.chINSST.MenuManager = this.BarManager_F;
            this.chINSST.Name = "chINSST";
            this.chINSST.Properties.Caption = "";
            this.chINSST.Size = new System.Drawing.Size(21, 19);
            this.chINSST.TabIndex = 38;
            this.chINSST.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // calcEditINSST
            // 
            this.calcEditINSST.Location = new System.Drawing.Point(115, 58);
            this.calcEditINSST.MenuManager = this.BarManager_F;
            this.calcEditINSST.Name = "calcEditINSST";
            this.calcEditINSST.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.calcEditINSST.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditINSST.Properties.Appearance.Options.UseFont = true;
            this.calcEditINSST.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditINSST.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditINSST.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditINSST.Properties.Mask.EditMask = "n2";
            this.calcEditINSST.Properties.NullText = "--";
            this.calcEditINSST.Properties.NullValuePrompt = "--";
            this.calcEditINSST.Size = new System.Drawing.Size(100, 22);
            this.calcEditINSST.TabIndex = 29;
            this.calcEditINSST.EditValueChanged += new System.EventHandler(this.calcEdit4_EditValueChanged);
            // 
            // chINSSP
            // 
            this.chINSSP.EditValue = true;
            this.chINSSP.Location = new System.Drawing.Point(221, 32);
            this.chINSSP.MenuManager = this.BarManager_F;
            this.chINSSP.Name = "chINSSP";
            this.chINSSP.Properties.Caption = "";
            this.chINSSP.Size = new System.Drawing.Size(21, 19);
            this.chINSSP.TabIndex = 37;
            this.chINSSP.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(253, 5);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(15, 16);
            this.labelControl10.TabIndex = 30;
            this.labelControl10.Text = "IR";
            // 
            // chISS
            // 
            this.chISS.EditValue = true;
            this.chISS.Location = new System.Drawing.Point(221, 4);
            this.chISS.MenuManager = this.BarManager_F;
            this.chISS.Name = "chISS";
            this.chISS.Properties.Caption = "";
            this.chISS.Size = new System.Drawing.Size(21, 19);
            this.chISS.TabIndex = 36;
            this.chISS.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(253, 33);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(106, 16);
            this.labelControl11.TabIndex = 31;
            this.labelControl11.Text = "PIS/Cofins/CSLL";
            // 
            // calcEditIR
            // 
            this.calcEditIR.Location = new System.Drawing.Point(373, 2);
            this.calcEditIR.MenuManager = this.BarManager_F;
            this.calcEditIR.Name = "calcEditIR";
            this.calcEditIR.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.calcEditIR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditIR.Properties.Appearance.Options.UseFont = true;
            this.calcEditIR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditIR.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditIR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditIR.Properties.Mask.EditMask = "n2";
            this.calcEditIR.Properties.NullText = "--";
            this.calcEditIR.Properties.NullValuePrompt = "--";
            this.calcEditIR.Size = new System.Drawing.Size(100, 22);
            this.calcEditIR.TabIndex = 32;
            this.calcEditIR.EditValueChanged += new System.EventHandler(this.calcEdit5_EditValueChanged);
            // 
            // calcEditPIS
            // 
            this.calcEditPIS.Location = new System.Drawing.Point(373, 30);
            this.calcEditPIS.MenuManager = this.BarManager_F;
            this.calcEditPIS.Name = "calcEditPIS";
            this.calcEditPIS.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.calcEditPIS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditPIS.Properties.Appearance.Options.UseFont = true;
            this.calcEditPIS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditPIS.Properties.DisplayFormat.FormatString = "n2";
            this.calcEditPIS.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEditPIS.Properties.Mask.EditMask = "n2";
            this.calcEditPIS.Properties.NullText = "--";
            this.calcEditPIS.Properties.NullValuePrompt = "--";
            this.calcEditPIS.Size = new System.Drawing.Size(100, 22);
            this.calcEditPIS.TabIndex = 33;
            this.calcEditPIS.EditValueChanged += new System.EventHandler(this.calcEdit6_EditValueChanged);
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(229, 28);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(72, 16);
            this.labelControl15.TabIndex = 42;
            this.labelControl15.Text = "-> Sal�rios";
            // 
            // calcEdit1
            // 
            this.calcEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFSalarios", true));
            this.calcEdit1.Location = new System.Drawing.Point(316, 25);
            this.calcEdit1.MenuManager = this.BarManager_F;
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEdit1.Properties.Appearance.Options.UseFont = true;
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.calcEdit1.Properties.DisplayFormat.FormatString = "n4";
            this.calcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit1.Properties.Mask.EditMask = "n4";
            this.calcEdit1.Size = new System.Drawing.Size(100, 22);
            this.calcEdit1.TabIndex = 41;
            // 
            // calcLIQUIDO
            // 
            this.calcLIQUIDO.Location = new System.Drawing.Point(583, 25);
            this.calcLIQUIDO.MenuManager = this.BarManager_F;
            this.calcLIQUIDO.Name = "calcLIQUIDO";
            this.calcLIQUIDO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcLIQUIDO.Properties.Appearance.Options.UseFont = true;
            this.calcLIQUIDO.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcLIQUIDO.Properties.DisplayFormat.FormatString = "n2";
            this.calcLIQUIDO.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcLIQUIDO.Properties.Mask.EditMask = "n2";
            this.calcLIQUIDO.Size = new System.Drawing.Size(100, 22);
            this.calcLIQUIDO.TabIndex = 35;
            this.calcLIQUIDO.ValueChanged += new System.EventHandler(this.calcLIQUIDO_ValueChanged);
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(482, 28);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(83, 16);
            this.labelControl12.TabIndex = 34;
            this.labelControl12.Text = "Valor Liquido";
            // 
            // GC_identificacao
            // 
            this.GC_identificacao.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GC_identificacao.Appearance.Options.UseFont = true;
            this.GC_identificacao.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GC_identificacao.AppearanceCaption.Options.UseFont = true;
            this.GC_identificacao.Controls.Add(this.labelControl16);
            this.GC_identificacao.Controls.Add(this.labelControl6);
            this.GC_identificacao.Controls.Add(this.spin_PGFIdentificacao);
            this.GC_identificacao.Controls.Add(this.textEdit2);
            this.GC_identificacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GC_identificacao.Location = new System.Drawing.Point(209, 2);
            this.GC_identificacao.Name = "GC_identificacao";
            this.GC_identificacao.Size = new System.Drawing.Size(1266, 49);
            this.GC_identificacao.TabIndex = 59;
            this.GC_identificacao.Text = "Identifica��o";
            // 
            // labelControl16
            // 
            this.labelControl16.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.PaGamentosFixosbindingSource, "PGFForma", true));
            this.labelControl16.Location = new System.Drawing.Point(570, 23);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(69, 13);
            this.labelControl16.TabIndex = 3;
            this.labelControl16.Text = "labelControl16";
            this.labelControl16.Visible = false;
            // 
            // labelControl6
            // 
            this.labelControl6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.PaGamentosFixosbindingSource, "PGF", true));
            this.labelControl6.Location = new System.Drawing.Point(491, 24);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(63, 13);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "labelControl6";
            this.labelControl6.Visible = false;
            // 
            // spin_PGFIdentificacao
            // 
            this.spin_PGFIdentificacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.PaGamentosFixosbindingSource, "PGFIdentificacao", true));
            this.spin_PGFIdentificacao.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spin_PGFIdentificacao.Location = new System.Drawing.Point(6, 24);
            this.spin_PGFIdentificacao.MenuManager = this.BarManager_F;
            this.spin_PGFIdentificacao.Name = "spin_PGFIdentificacao";
            this.spin_PGFIdentificacao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.spin_PGFIdentificacao.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spin_PGFIdentificacao.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spin_PGFIdentificacao.Size = new System.Drawing.Size(154, 20);
            this.spin_PGFIdentificacao.TabIndex = 0;
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.PaGamentosFixosbindingSource, "PGFCodigoDebito", true));
            this.textEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.textEdit2.Location = new System.Drawing.Point(166, 24);
            this.textEdit2.MenuManager = this.BarManager_F;
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.textEdit2.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.textEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.textEdit2.Size = new System.Drawing.Size(153, 20);
            this.textEdit2.TabIndex = 1;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 35);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1500, 430);
            this.xtraTabControl1.TabIndex = 61;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.AutoScroll = true;
            this.xtraTabPage1.Controls.Add(this.groupControl7);
            this.xtraTabPage1.Controls.Add(this.groupControl10);
            this.xtraTabPage1.Controls.Add(this.groupControl4);
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Controls.Add(this.groupControl3);
            this.xtraTabPage1.Controls.Add(this.grcForn);
            this.xtraTabPage1.Controls.Add(this.groupControl8);
            this.xtraTabPage1.Controls.Add(this.panelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1494, 402);
            this.xtraTabPage1.Text = "Cadastro";
            // 
            // groupControl7
            // 
            this.groupControl7.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl7.AppearanceCaption.Options.UseFont = true;
            this.groupControl7.Controls.Add(this.BotImprimir);
            this.groupControl7.Controls.Add(this.memoInterno);
            this.groupControl7.Controls.Add(this.memoImpresso);
            this.groupControl7.Controls.Add(this.labelControl14);
            this.groupControl7.Controls.Add(this.labelControl13);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl7.Location = new System.Drawing.Point(0, 610);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(1477, 177);
            this.groupControl7.TabIndex = 62;
            this.groupControl7.Text = "Obs";
            // 
            // BotImprimir
            // 
            this.BotImprimir.Enabled = false;
            this.BotImprimir.Location = new System.Drawing.Point(5, 37);
            this.BotImprimir.Name = "BotImprimir";
            this.BotImprimir.Size = new System.Drawing.Size(101, 23);
            this.BotImprimir.TabIndex = 25;
            this.BotImprimir.Text = "Imprimir";
            this.BotImprimir.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // memoInterno
            // 
            this.memoInterno.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.PaGamentosFixosbindingSource, "PGFObsInterna", true));
            this.memoInterno.Location = new System.Drawing.Point(115, 97);
            this.memoInterno.MenuManager = this.BarManager_F;
            this.memoInterno.Name = "memoInterno";
            this.memoInterno.Size = new System.Drawing.Size(568, 75);
            this.memoInterno.TabIndex = 24;
            // 
            // memoImpresso
            // 
            this.memoImpresso.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.PaGamentosFixosbindingSource, "PGFObsImpresso", true));
            this.memoImpresso.Location = new System.Drawing.Point(115, 23);
            this.memoImpresso.MenuManager = this.BarManager_F;
            this.memoImpresso.Name = "memoImpresso";
            this.memoImpresso.Size = new System.Drawing.Size(568, 68);
            this.memoImpresso.TabIndex = 23;
            this.memoImpresso.Leave += new System.EventHandler(this.memoImpresso_Leave);
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(5, -41);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(60, 16);
            this.labelControl14.TabIndex = 22;
            this.labelControl14.Text = "Impresso";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(5, 43);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(74, 16);
            this.labelControl13.TabIndex = 21;
            this.labelControl13.Text = "Uso interno";
            // 
            // groupControl10
            // 
            this.groupControl10.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl10.AppearanceCaption.Options.UseFont = true;
            this.groupControl10.Controls.Add(this.dateEdit4);
            this.groupControl10.Controls.Add(this.labelControl3);
            this.groupControl10.Controls.Add(this.cCompet1);
            this.groupControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl10.Location = new System.Drawing.Point(0, 553);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(1477, 57);
            this.groupControl10.TabIndex = 64;
            this.groupControl10.Text = "Reajuste";
            // 
            // dateEdit4
            // 
            this.dateEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "AGBDataInicio", true));
            this.dateEdit4.EditValue = null;
            this.dateEdit4.Location = new System.Drawing.Point(307, 25);
            this.dateEdit4.MenuManager = this.BarManager_F;
            this.dateEdit4.Name = "dateEdit4";
            this.dateEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.Appearance.Options.UseFont = true;
            this.dateEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Undo)});
            this.dateEdit4.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit4.Size = new System.Drawing.Size(156, 22);
            this.dateEdit4.TabIndex = 58;
            this.dateEdit4.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEdit4_ButtonClick);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(223, 26);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(78, 16);
            this.labelControl3.TabIndex = 57;
            this.labelControl3.Text = "Data Limite:";
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.Appearance.Options.UseFont = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.DataBindings.Add(new System.Windows.Forms.Binding("CON", this.PaGamentosFixosbindingSource, "PGF_CON", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompet1.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.PaGamentosFixosbindingSource, "PGFCompetReajuste", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(13, 25);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = true;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(147, 27);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 56;
            this.cCompet1.Titulo = null;
            this.cCompet1.Load += new System.EventHandler(this.cCompet1_Load);
            // 
            // groupControl8
            // 
            this.groupControl8.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl8.AppearanceCaption.Options.UseFont = true;
            this.groupControl8.Controls.Add(this.cmbContaCredito);
            this.groupControl8.Controls.Add(this.chkDepProprioCon);
            this.groupControl8.Controls.Add(this.labelDadosDep);
            this.groupControl8.Controls.Add(this.cmbContaDebito);
            this.groupControl8.Controls.Add(this.lblContaDebito);
            this.groupControl8.Controls.Add(this.textEditComplemento);
            this.groupControl8.Controls.Add(this.imageComboBoxEdit1);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl8.Location = new System.Drawing.Point(0, 53);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(1477, 87);
            this.groupControl8.TabIndex = 63;
            this.groupControl8.Text = "Forma de Pagamento";
            // 
            // cmbContaCredito
            // 
            this.cmbContaCredito.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGF_CCTCred", true));
            this.cmbContaCredito.Location = new System.Drawing.Point(788, 52);
            this.cmbContaCredito.Name = "cmbContaCredito";
            this.cmbContaCredito.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbContaCredito.Size = new System.Drawing.Size(431, 20);
            this.cmbContaCredito.TabIndex = 53;
            this.cmbContaCredito.Visible = false;
            // 
            // chkDepProprioCon
            // 
            this.chkDepProprioCon.Location = new System.Drawing.Point(617, 55);
            this.chkDepProprioCon.Name = "chkDepProprioCon";
            this.chkDepProprioCon.Properties.Caption = "Conta do pr�prio condom�nio";
            this.chkDepProprioCon.Size = new System.Drawing.Size(165, 19);
            this.chkDepProprioCon.TabIndex = 51;
            this.chkDepProprioCon.TabStop = false;
            this.chkDepProprioCon.CheckedChanged += new System.EventHandler(this.chkDepProprioCon_CheckedChanged);
            // 
            // labelDadosDep
            // 
            this.labelDadosDep.AutoSize = true;
            this.labelDadosDep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDadosDep.Location = new System.Drawing.Point(551, 57);
            this.labelDadosDep.Name = "labelDadosDep";
            this.labelDadosDep.Size = new System.Drawing.Size(60, 13);
            this.labelDadosDep.TabIndex = 52;
            this.labelDadosDep.Text = "Dep�sito:";
            this.labelDadosDep.Visible = false;
            // 
            // cmbContaDebito
            // 
            this.cmbContaDebito.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGF_CCT", true));
            this.cmbContaDebito.Location = new System.Drawing.Point(105, 54);
            this.cmbContaDebito.MenuManager = this.BarManager_F;
            this.cmbContaDebito.Name = "cmbContaDebito";
            this.cmbContaDebito.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbContaDebito.Properties.Appearance.Options.UseFont = true;
            this.cmbContaDebito.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbContaDebito.Size = new System.Drawing.Size(423, 22);
            this.cmbContaDebito.TabIndex = 34;
            this.cmbContaDebito.Visible = false;
            // 
            // textEditComplemento
            // 
            this.textEditComplemento.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFDadosPag", true));
            this.textEditComplemento.Location = new System.Drawing.Point(532, 25);
            this.textEditComplemento.MenuManager = this.BarManager_F;
            this.textEditComplemento.Name = "textEditComplemento";
            this.textEditComplemento.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditComplemento.Properties.Appearance.Options.UseFont = true;
            this.textEditComplemento.Size = new System.Drawing.Size(474, 22);
            this.textEditComplemento.TabIndex = 24;
            this.textEditComplemento.Visible = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.GC_identificacao);
            this.panelControl1.Controls.Add(this.groupControl6);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1477, 53);
            this.panelControl1.TabIndex = 61;
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.ComboStatus);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl6.Location = new System.Drawing.Point(2, 2);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(207, 49);
            this.groupControl6.TabIndex = 0;
            this.groupControl6.Text = "STATUS";
            // 
            // ComboStatus
            // 
            this.ComboStatus.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFStatus", true));
            this.ComboStatus.Location = new System.Drawing.Point(3, 25);
            this.ComboStatus.MenuManager = this.BarManager_F;
            this.ComboStatus.Name = "ComboStatus";
            this.ComboStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboStatus.Size = new System.Drawing.Size(198, 20);
            this.ComboStatus.TabIndex = 41;
            this.ComboStatus.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ComboStatus_EditValueChanging);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.componenteWEB1);
            this.xtraTabPage2.Controls.Add(this.panelControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1513, 719);
            this.xtraTabPage2.Text = "Documenta��o";
            // 
            // componenteWEB1
            // 
            this.componenteWEB1.CaixaAltaGeral = true;
            this.componenteWEB1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.componenteWEB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.componenteWEB1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.componenteWEB1.Location = new System.Drawing.Point(0, 103);
            this.componenteWEB1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.componenteWEB1.MostraEndereco = false;
            this.componenteWEB1.Name = "componenteWEB1";
            this.componenteWEB1.Size = new System.Drawing.Size(1513, 616);
            this.componenteWEB1.somenteleitura = false;
            this.componenteWEB1.TabIndex = 3;
            this.componenteWEB1.Titulo = null;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl2);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1513, 103);
            this.panelControl2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.fKDOCumentacaoPaGamentosFixosBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.MenuManager = this.BarManager_F;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1});
            this.gridControl2.Size = new System.Drawing.Size(1360, 99);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // fKDOCumentacaoPaGamentosFixosBindingSource
            // 
            this.fKDOCumentacaoPaGamentosFixosBindingSource.DataMember = "FK_DOCumentacao_PaGamentosFixos";
            this.fKDOCumentacaoPaGamentosFixosBindingSource.DataSource = this.PaGamentosFixosbindingSource;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDOCTipo,
            this.colDOCEXT,
            this.colDOCDATAI,
            this.colDOCI_USU,
            this.colDOCValido,
            this.colDOCNome});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.Editable = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            // 
            // colDOCTipo
            // 
            this.colDOCTipo.Caption = "Tipo";
            this.colDOCTipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colDOCTipo.FieldName = "DOCTipo";
            this.colDOCTipo.Name = "colDOCTipo";
            this.colDOCTipo.OptionsColumn.FixedWidth = true;
            this.colDOCTipo.OptionsColumn.ReadOnly = true;
            this.colDOCTipo.Visible = true;
            this.colDOCTipo.VisibleIndex = 0;
            this.colDOCTipo.Width = 113;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colDOCEXT
            // 
            this.colDOCEXT.Caption = "EXT";
            this.colDOCEXT.FieldName = "DOCEXT";
            this.colDOCEXT.Name = "colDOCEXT";
            this.colDOCEXT.OptionsColumn.FixedWidth = true;
            this.colDOCEXT.OptionsColumn.ReadOnly = true;
            this.colDOCEXT.Visible = true;
            this.colDOCEXT.VisibleIndex = 3;
            this.colDOCEXT.Width = 39;
            // 
            // colDOCDATAI
            // 
            this.colDOCDATAI.Caption = "Inclus�o";
            this.colDOCDATAI.FieldName = "DOCDATAI";
            this.colDOCDATAI.Name = "colDOCDATAI";
            this.colDOCDATAI.OptionsColumn.FixedWidth = true;
            this.colDOCDATAI.OptionsColumn.ReadOnly = true;
            // 
            // colDOCI_USU
            // 
            this.colDOCI_USU.FieldName = "DOCI_USU";
            this.colDOCI_USU.Name = "colDOCI_USU";
            this.colDOCI_USU.OptionsColumn.FixedWidth = true;
            this.colDOCI_USU.OptionsColumn.ReadOnly = true;
            // 
            // colDOCValido
            // 
            this.colDOCValido.Caption = "V�lido";
            this.colDOCValido.FieldName = "DOCValido";
            this.colDOCValido.Name = "colDOCValido";
            this.colDOCValido.OptionsColumn.FixedWidth = true;
            this.colDOCValido.OptionsColumn.ReadOnly = true;
            this.colDOCValido.Visible = true;
            this.colDOCValido.VisibleIndex = 2;
            this.colDOCValido.Width = 51;
            // 
            // colDOCNome
            // 
            this.colDOCNome.Caption = "Descri��o";
            this.colDOCNome.FieldName = "DOCNome";
            this.colDOCNome.Name = "colDOCNome";
            this.colDOCNome.OptionsColumn.ReadOnly = true;
            this.colDOCNome.Visible = true;
            this.colDOCNome.VisibleIndex = 1;
            this.colDOCNome.Width = 887;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton3);
            this.panelControl3.Controls.Add(this.simpleButton2);
            this.panelControl3.Controls.Add(this.simpleButton4);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(1362, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(149, 99);
            this.panelControl3.TabIndex = 2;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(6, 64);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(138, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "EDITAR";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(5, 35);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(138, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "APAGAR";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(6, 6);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(138, 23);
            this.simpleButton4.TabIndex = 0;
            this.simpleButton4.Text = "NOVO";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.memoEdit1);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1513, 719);
            this.xtraTabPage3.Text = "Hist�rico";
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.PaGamentosFixosbindingSource, "PGFHistorico", true));
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(0, 0);
            this.memoEdit1.MenuManager = this.BarManager_F;
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Size = new System.Drawing.Size(1513, 719);
            this.memoEdit1.TabIndex = 0;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridControl1);
            this.xtraTabPage4.Controls.Add(this.groupControl9);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1513, 719);
            this.xtraTabPage4.Text = "Pagamentos";
            // 
            // groupControl9
            // 
            this.groupControl9.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl9.AppearanceCaption.Options.UseFont = true;
            this.groupControl9.Controls.Add(this.BotCorrecao);
            this.groupControl9.Controls.Add(this.dateEdit3);
            this.groupControl9.Controls.Add(this.cCompetProx);
            this.groupControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl9.Location = new System.Drawing.Point(0, 0);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(1513, 78);
            this.groupControl9.TabIndex = 62;
            this.groupControl9.Text = "Pr�ximo cheque a ser gerado";
            // 
            // BotCorrecao
            // 
            this.BotCorrecao.Location = new System.Drawing.Point(512, 30);
            this.BotCorrecao.Name = "BotCorrecao";
            this.BotCorrecao.Size = new System.Drawing.Size(189, 23);
            this.BotCorrecao.TabIndex = 60;
            this.BotCorrecao.Text = "Corre��o";
            this.BotCorrecao.Visible = false;
            this.BotCorrecao.Click += new System.EventHandler(this.BotCorrecao_Click);
            // 
            // dateEdit3
            // 
            this.dateEdit3.EditValue = null;
            this.dateEdit3.Location = new System.Drawing.Point(128, 27);
            this.dateEdit3.MenuManager = this.BarManager_F;
            this.dateEdit3.Name = "dateEdit3";
            this.dateEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.Appearance.Options.UseFont = true;
            this.dateEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit3.Properties.ReadOnly = true;
            this.dateEdit3.Size = new System.Drawing.Size(147, 22);
            this.dateEdit3.TabIndex = 59;
            // 
            // cCompetProx
            // 
            this.cCompetProx.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetProx.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompetProx.Appearance.Options.UseBackColor = true;
            this.cCompetProx.Appearance.Options.UseFont = true;
            this.cCompetProx.CaixaAltaGeral = true;
            this.cCompetProx.DataBindings.Add(new System.Windows.Forms.Binding("CON", this.PaGamentosFixosbindingSource, "PGF_CON", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompetProx.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.PaGamentosFixosbindingSource, "PGFProximaCompetencia", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompetProx.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetProx.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetProx.IsNull = false;
            this.cCompetProx.Location = new System.Drawing.Point(5, 25);
            this.cCompetProx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetProx.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetProx.Name = "cCompetProx";
            this.cCompetProx.PermiteNulo = false;
            this.cCompetProx.ReadOnly = false;
            this.cCompetProx.Size = new System.Drawing.Size(117, 27);
            this.cCompetProx.somenteleitura = false;
            this.cCompetProx.TabIndex = 57;
            this.cCompetProx.Titulo = null;
            this.cCompetProx.OnChange += new System.EventHandler(this.cCompet1_OnChange_1);
            // 
            // cPagamentosPeriodicosCampos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BindingSourcePrincipal = this.PaGamentosFixosbindingSource;
            this.Controls.Add(this.xtraTabControl1);
            this.DesabilitarAllowDBNull = false;
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cPagamentosPeriodicosCampos";
            this.Size = new System.Drawing.Size(1500, 490);
            this.TipoBuscaPk = CompontesBasicos.ETipoBuscaPk.fill;
            this.Titulo = "Pagamentos Peri�dicos";
            this.cargaFinal += new System.EventHandler(this.cPagamentosPeriodicosCampos_cargaFinal);
            this.Load += new System.EventHandler(this.cPagamentosPeriodicosCampos_Load);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKNOtAsPaGamentosFixosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaGamentosFixosbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPagamentosPeriodicos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBotAJComp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcForn)).EndInit();
            this.grcForn.ResumeLayout(false);
            this.grcForn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textNominal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubPLanoPLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditValor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelImpostos)).EndInit();
            this.panelImpostos.ResumeLayout(false);
            this.panelImpostos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditISS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditINSSP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chPIS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chIR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chINSST.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditINSST.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chINSSP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chISS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditIR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditPIS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcLIQUIDO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC_identificacao)).EndInit();
            this.GC_identificacao.ResumeLayout(false);
            this.GC_identificacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spin_PGFIdentificacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoInterno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoImpresso.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbContaCredito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepProprioCon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbContaDebito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditComplemento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ComboStatus.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKDOCumentacaoPaGamentosFixosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource fornecedoresBindingSource;
        private DevExpress.XtraEditors.GroupControl grcForn;
        private DevExpress.XtraEditors.LookUpEdit lookUpFOR1;
        private DevExpress.XtraEditors.LookUpEdit lookUpFOR2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CalcEdit calcEditValor;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.DateEdit dateEdit2;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.GroupControl GC_identificacao;
        private Framework.objetosNeon.cCompet cCompetIniRef;
        private Framework.objetosNeon.cCompet cCompetFimRef;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CalcEdit calcEditINSSP;
        private DevExpress.XtraEditors.CalcEdit calcEditISS;
        private DevExpress.XtraEditors.CalcEdit calcEditPIS;
        private DevExpress.XtraEditors.CalcEdit calcEditIR;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.CalcEdit calcEditINSST;
        private DevExpress.XtraEditors.CalcEdit calcLIQUIDO;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.CheckEdit chISS;
        private DevExpress.XtraEditors.CheckEdit chPIS;
        private DevExpress.XtraEditors.CheckEdit chIR;
        private DevExpress.XtraEditors.CheckEdit chINSST;
        private DevExpress.XtraEditors.CheckEdit chINSSP;
        private ContaPagarProc.Follow.dPagamentosPeriodicos dPagamentosPeriodicos;
        private System.Windows.Forms.BindingSource fKNOtAsPaGamentosFixosBindingSource;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboStatus;
        private System.Windows.Forms.BindingSource PaGamentosFixosbindingSource;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource fKDOCumentacaoPaGamentosFixosBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCEXT;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCValido;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCNome;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private CompontesBasicos.ComponenteWEB componenteWEB1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADataEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAStatus;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.MemoEdit memoInterno;
        private DevExpress.XtraEditors.MemoEdit memoImpresso;
        private DevExpress.XtraEditors.SimpleButton BotImprimir;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_NOA;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGIMP;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEFavorecido;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colCHENumero;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_CHE;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.TextEdit textEditComplemento;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.DateEdit dateEdit3;
        private Framework.objetosNeon.cCompet cCompetProx;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.DateEdit dateEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraEditors.PanelControl panelImpostos;
        private System.Windows.Forms.BindingSource SubPLanoPLAnocontasBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colNOACompet;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryBotAJComp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBAJ;
        private DevExpress.XtraEditors.TextEdit textNominal;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbContaDebito;
        private System.Windows.Forms.Label lblContaCredito;
        private System.Windows.Forms.Label lblContaDebito;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbContaCredito;
        private DevExpress.XtraEditors.CheckEdit chkDepProprioCon;
        private System.Windows.Forms.Label labelDadosDep;
        private DevExpress.XtraEditors.SpinEdit spin_PGFIdentificacao;
        private DevExpress.XtraEditors.SpinEdit textEdit2;
        private DevExpress.XtraEditors.SimpleButton BotCorrecao;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl6;
    }
}
