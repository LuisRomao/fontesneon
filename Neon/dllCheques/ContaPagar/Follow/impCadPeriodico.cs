﻿using System;
using CodBar;

namespace ContaPagar.Follow
{
    /// <summary>
    /// Impresso periódico
    /// </summary>
    public class impCadPeriodico : Impress
    {
        /// <summary>
        /// Construtor
        /// </summary>        
        public impCadPeriodico()
            : base()
        {
            NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de cópias de Cheque");
            Mascara = AbreForm();
            Mascara += CodCaixa(0, 40, 1700,900);
            Mascara += CodTXT(20, 840, " CADASTRO DE PAGAMENTO MENSAL ", 6, Orientacao.normal, true);
            int XC1 = 35;
            //int XC2 = 250;
            int YC1 = 760;
            int alturaC1 = 40;            
            Mascara += CodTXT(XC1, YC1, "Prestador: %PRESTADOR%", 3);
            Mascara += CodTXT(XC1, YC1 - alturaC1, "%CNPJ%",3);
            Mascara += CodTXT(XC1, YC1 - 2 * alturaC1, "Servico: %SERVICO%", 3);
            int XC3 = 900;
            Mascara += CodTXT(XC3, YC1, "Primeiro pagamento: %DATAI%", 3);
            Mascara += CodTXT(XC3, YC1 - alturaC1, "%DATAF%", 3);

            Mascara += CodTXT(XC1, YC1 - 3 * alturaC1, "Obs: O primeiro pagamento (%DATAI%) se refere ao servico prestado em %COMPREF%", 2);
            Mascara += CodTXT(XC1, YC1 - 4 * alturaC1, "%PARCELAS%", 2);

            Mascara += CodTXT(20, 550, "                VALORES                 ", 5,Orientacao.normal,true);
            Mascara += "%RETENCOE%";

            int XC4 = 800;
            int YC4 = 500;
            int alturaC4 = 40;

            Mascara += CodTXT(XC4, YC4, "Valor Nominal:", 3);
            Mascara += CodTXT(XC4, YC4 - alturaC4, "Valor do cheque:", 3);
            Mascara += CodTXT(XC4, YC4 - 2 * alturaC4, "Custo total:", 3);

            int XC5 = 1240;

            Mascara += CodTXT(XC5, YC4, "%NOMINAL%", 3, Orientacao.normal, true);
            Mascara += CodTXT(XC5, YC4 - alturaC4, "%LIQUIDO%", 3, Orientacao.normal, true);
            Mascara += CodTXT(XC5, YC4 - 2 * alturaC4, "%TOTAL%", 3, Orientacao.normal, true);

            Mascara += CodLinha(20, 315, 1470);
            Mascara += CodTXT(20, 280, "Senhor síndico, favor conferir se os dados estao conforme sua solicitação", 2);

            Mascara += "%OBSIMP%";

            int Xass = 800;
            int Yass = 110;
            Mascara += CodLinha(Xass, Yass, 650, 5);
            Mascara += CodTXT(Xass, Yass - 60, "%SINDICO%", 4);

            Mascara += FechaForm();
        }
    }
}
