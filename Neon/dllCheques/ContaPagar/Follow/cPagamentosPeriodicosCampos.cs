/*
MR - 28/04/2014 09:45 13.2.8.34 - Carregamento do combo de tipos de pagamento para PE com os tipos �teis (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 30/07/2014 16:00           - Tratamento do novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 05/08/2014 11:00           - Tratamento do tipo de conta do condominio ao verificar se condominio faz pagamento eletronico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (05/08/2014 11:00) ***)
MR - 22/06/2015 12:00           - PECreditoConta mensal fixo entre contas do pr�prio condominio (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***)
MR - 14/01/2016 12:30            - Descri��o de conta para cr�dito no caso de transfer�ncia indicando corrente ou poupan�a (Altera��es indicadas por *** MRC - INICIO (14/01/2016 12:30) ***)
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using CompontesBasicos.ObjetosEstaticos;
using dllVirEnum;
using Framework;
using Framework.objetosNeon;
using CompontesBasicosProc;
using CompontesBasicos;
using ContaPagarProc.Follow;
using FrameworkProc;
using CadastrosProc.Fornecedores;
using VirEnumeracoes;
using VirEnumeracoesNeon;

namespace ContaPagar.Follow
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cPagamentosPeriodicosCampos : Framework.ComponentesNeon.CamposCondominio, IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        private Framework.datasets.dPLAnocontas.PLAnocontasRow _PLArow;
        private string _PLA;

        private Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow
        {
            set
            {
                _PLArow = value;
                _PLA = _PLArow.PLA;
            }
            get
            {
                if ((_PLArow != null) && (_PLArow.RowState == DataRowState.Detached))
                    _PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas.FindByPLA(_PLA);
                return _PLArow;
            }
        }

        cPagamentosPeriodicos Chamador 
        {
            get
            {
                if (GradeChamadora != null)
                    return (cPagamentosPeriodicos)GradeChamadora;
                else
                    return null;
            }
        }

        private PagamentoPeriodico _PagamentoPeriodico;

        /// <summary>
        /// 
        /// </summary>
        public PagamentoPeriodico PagamentoPeriodico
        {
            get { return _PagamentoPeriodico ?? (_PagamentoPeriodico = new PagamentoPeriodico(LinhaMae)); }
        }

        #region Contrutores
        /// <summary>
        /// Construtor padr�o (N�O REMOVER)
        /// </summary>
        public cPagamentosPeriodicosCampos()
            : this(null)
        {
        }

        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup;

        /// <summary>
        /// 
        /// </summary>
        public cPagamentosPeriodicosCampos(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
            else
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            InitializeComponent();
            dPagamentosPeriodicos.EMPTProc1 = EMPTProc1;
            if (!DesignMode)
            {
                TableAdapterPrincipal = dPagamentosPeriodicos.PaGamentosFixosTableAdapter;
                dFornecedoresLookup = new dFornecedoresLookup();                
                dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
                fornecedoresBindingSource.DataSource = dFornecedoresLookup;
                pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontasTableAdapter.Get25();
                SubPLanoPLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25;
                blocCalculaDatas = true;
                VirEnumStatusPagamentoPeriodico.CarregaEditorDaGrid(ComboStatus);
                cPagamentosPeriodicosCampos.VirEnumTiposDOC.CarregaEditorDaGrid(colDOCTipo);
                Enumeracoes.VirEnumNOAStatus.CarregaEditorDaGrid(colNOAStatus);
                Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
                Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(colPAGTipo);
                dllImpostoNeon.ImpostoNeon.VirEnumTipoImposto.CarregaEditorDaGrid(colPAGIMP);
                cCompetProx.CompMaxima = new Competencia(DateTime.Today.AddMonths(3));
            }
            if (Framework.DSCentral.USU == 30)
                BotCorrecao.Visible = true;
        }

        /// <summary>
        /// Construtor que abre a tela
        /// </summary>
        /// <param name="PGF"></param>
        /// <param name="_Titulo"></param>
        /// <param name="_EMPTProc"></param>
        public cPagamentosPeriodicosCampos(int PGF, string _Titulo, EMPTProc _EMPTProc = null)
            : this(_EMPTProc)
        {
            pk = PGF;
            Titulo = _Titulo;
            VirShowModulo(EstadosDosComponentes.JanelasAtivas);
        }
        #endregion

        private void CalcCompeteciasDuplicadas()
        {
            CompetenciasUsadas = new SortedList<int, int>();
            foreach (dPagamentosPeriodicos.NOtAsRow rowNOA in dPagamentosPeriodicos.NOtAs)
            {
                NOAStatus NOAStatus1 = (NOAStatus)rowNOA.NOAStatus;
                if (NOAStatus1 == NOAStatus.NotaCancelada)
                    continue;
                if (CompetenciasUsadas.ContainsKey(rowNOA.NOACompet))
                    CompetenciasUsadas[rowNOA.NOACompet] += 1;
                else
                    CompetenciasUsadas.Add(rowNOA.NOACompet, 1);

                if (NOAStatus1 == NOAStatus.Cadastrada)
                {
                    bool Liquidada = true;
                    bool Bloqueada = false;
                    foreach (dPagamentosPeriodicos.PAGamentosRow PAGrow in rowNOA.GetPAGamentosRows())
                    {
                        if ((CHEStatus)PAGrow.CHEStatus == CHEStatus.CadastradoBloqueado)
                            Bloqueada = true;
                        if (!Nota.StatusOK.Contains((CHEStatus)PAGrow.CHEStatus))
                        {
                            Liquidada = false;
                            break;
                        }
                    }
                    if (Liquidada || Bloqueada)
                    {
                        NOAStatus novoNOAStatus = Bloqueada ? NOAStatus.NotaProvisoria : NOAStatus.Liquidada;
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update NOtAs set NOAStatus = @P1 where NOA = @P2 and NOAStatus = @P3", (int)novoNOAStatus, rowNOA.NOA, (int)NOAStatus1);
                        rowNOA.NOAStatus = (int)novoNOAStatus;
                    }
                }

            }
        }

        /// <summary>
        /// Carrega os dados
        /// </summary>
        protected override void FillAutomatico()
        {
            bloc = true;
            if (dPagamentosPeriodicos.PaGamentosFixosTableAdapter.FillByPGF(dPagamentosPeriodicos.PaGamentosFixos, pk) == 1)
            {                
                calcEditISS.EditValue = calcEditINSSP.EditValue = calcEditINSSP.EditValue = calcEditIR.EditValue = calcEditPIS.EditValue = null;

                LinhaMae_F = dPagamentosPeriodicos.PaGamentosFixos[0];
                calcEditValor.EditValue = LinhaMae.PGFValor;
                if (!LinhaMae.IsPGFISSNull())
                    calcEditISS.EditValue = LinhaMae.PGFISS;
                if (!LinhaMae.IsPGFINSSTNull())
                    calcEditINSST.EditValue = LinhaMae.PGFINSST;
                if (!LinhaMae.IsPGFINSSPNull())
                    calcEditINSSP.EditValue = LinhaMae.PGFINSSP;
                if (!LinhaMae.IsPGFIRNull())
                    calcEditIR.EditValue = LinhaMae.PGFIR;
                if (!LinhaMae.IsPGFPISCOFNull())
                    calcEditPIS.EditValue = LinhaMae.PGFPISCOF;
                //spinEditRef.Value = LinhaMae.PGFRef;
                //cCompetIni.Comp = new Competencia(LinhaMae.PGFCompetI,LinhaMae.PGF_CON,null);
                //if (LinhaMae.IsPGFCompetFNull())
                //    cCompetFim.IsNull = true;
                //else
                //    cCompetFim.Comp = new Competencia(LinhaMae.PGFCompetF, LinhaMae.PGF_CON, null);
                Titulo = LinhaMae.PGFDescricao;
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, pk);
                dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, pk);
                dPagamentosPeriodicos.DOCumentacaoTableAdapter.FillByPGF(dPagamentosPeriodicos.DOCumentacao, pk);
                Travado = (StatusPagamentoPeriodico)LinhaMae.PGFStatus == StatusPagamentoPeriodico.Inativo;
                Status = (StatusPagamentoPeriodico)LinhaMae.PGFStatus;
                CalcCompeteciasDuplicadas();
                if (PagamentoPeriodico.Tipo == TipoPagPeriodico.Boleto)
                    cCompetProx.ReadOnly = true;
                    //cCompetProx.somenteleitura = true;
            }
            else
            {
                cCompetIniRef.Comp.Ano = DateTime.Today.Year;
                cCompetIniRef.Comp.Mes = DateTime.Today.Month;
                cCompetFimRef.CompMinima = cCompetIniRef.Comp;            
            };
            //cCompetFimRef.CompMinima = cCompetIniRef.Comp;            
            bloc = false;
        }

        

        private void lookUpFOR1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
                IncluiFRN("");
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                lookUpFOR1.EditValue = null;
        }

        dPagamentosPeriodicos.PaGamentosFixosRow LinhaMae 
        {
            get 
            {
                return (dPagamentosPeriodicos.PaGamentosFixosRow)LinhaMae_F;
            }
        }

        /*
        private static Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresDataTable Fornecedores 
        {
            get 
            {
                return Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fornecedores;
            }
        }*/

        private Cadastros.Fornecedores.Fornecedor Fornecedor;

        private bool IncluiFRN(string CNPJAlvo)
        {
            DocBacarios.CPFCNPJ novoCNPJ = new DocBacarios.CPFCNPJ(CNPJAlvo);
            if (novoCNPJ.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO)
            {
                Fornecedor = Cadastros.Fornecedores.Fornecedor.GetFornecedor(novoCNPJ, true);
                dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
                return true;
            }
            return false;
            /*
            int FRN = -1;
            System.Collections.SortedList CamposNovos = new System.Collections.SortedList();
            CamposNovos.Add("FRNCnpj", CNPJAlvo);
            CamposNovos.Add("FRNTipo", "FSR");
            FRN = ShowModuloAdd(typeof(Cadastros.Fornecedores.cFornecedoresCampos), CamposNovos);
            if (FRN >= 0)
            {
                try
                {
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = false;
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.FillByFRN(Fornecedores, FRN);
                    SemafaroFOR = true;
                    lookUpFOR1.EditValue = lookUpFOR2.EditValue = FRN;
                    //LinhaMae.PGF_FRN = FRN;
                    SemafaroFOR = false;                    
                }
                finally
                {
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = true;
                };
                return true;
            }
            return false;*/
        }

        bool SemafaroFOR;

        private void lookUpFOR2_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (textNominal.Text == lookUpFOR2.Text)
                textNominal.Text = "";
        }

        private void lookUpFOR1_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroFOR)
            {                             
                SemafaroFOR = true;
                if (sender == lookUpFOR1)                
                    lookUpFOR2.EditValue = lookUpFOR1.EditValue;                                    
                if (sender == lookUpFOR2)                
                    lookUpFOR1.EditValue = lookUpFOR2.EditValue;
                 
                if ((textNominal.EditValue == null) || (textNominal.Text == ""))
                    textNominal.Text = lookUpFOR2.Text;
                //*** MRC - INICIO - PAG-FOR (2) ***
                lblContaCredito.Visible = false;
                if (imageComboBoxEdit1.Text != "" && lookUpFOR1.ItemIndex != -1)
                {
                    TipoPagPeriodico Tipo = (TipoPagPeriodico)imageComboBoxEdit1.EditValue;
                    if (Tipo == TipoPagPeriodico.PECreditoConta || Tipo == TipoPagPeriodico.PETransferencia)
                    {
                        lblContaCredito.Visible = true;
                        dFornecedoresCampos dFornecedoresCamposLocal = new dFornecedoresCampos();
                        //dFornecedoresCampos.FORNECEDORESDataTable Forn = new dFornecedoresCampos.FORNECEDORESDataTable();
                        dFornecedoresCamposLocal.FORNECEDORESTableAdapter.Fill(dFornecedoresCamposLocal.FORNECEDORES, (int)lookUpFOR1.EditValue);
                        dFornecedoresCampos.FORNECEDORESRow rowFRN = dFornecedoresCamposLocal.FORNECEDORES[0];
                        string strContaCredito = string.Format("Banco {0} Ag�ncia {1}-{2} Conta {3}-{4}",
                                                               rowFRN.IsFRNBancoCreditoNull() ? "" : rowFRN.FRNBancoCredito,
                                                               rowFRN.IsFRNAgenciaCreditoNull() ? "" : rowFRN.FRNAgenciaCredito,
                                                               rowFRN.IsFRNAgenciaDgCreditoNull() ? "" : rowFRN.FRNAgenciaDgCredito,
                                                               rowFRN.IsFRNContaCreditoNull() ? "" : rowFRN.FRNContaCredito,
                                                               rowFRN.IsFRNContaDgCreditoNull() ? "" : rowFRN.FRNContaDgCredito);
                        //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                        if (rowFRN.IsFRNBancoCreditoNull()
                             || rowFRN.IsFRNAgenciaCreditoNull()
                             || rowFRN.IsFRNAgenciaDgCreditoNull()
                             || rowFRN.IsFRNContaCreditoNull()
                             || rowFRN.IsFRNContaDgCreditoNull()
                             || rowFRN.FRNBancoCredito.Trim() == ""
                             || rowFRN.FRNAgenciaCredito.Trim() == ""
                             || rowFRN.FRNAgenciaDgCredito.Trim() == ""
                             || rowFRN.FRNContaCredito.Trim() == ""
                             || rowFRN.FRNContaDgCredito.Trim() == "")
                        {
                            //*** MRC - INICIO (14/01/2016 12:30) ***
                            lblContaCredito.Text = "Conta para Cr�dito n�o cadastrada ou incompleta";
                            //*** MRC - TERMINO (14/01/2016 12:30) ***
                            lblContaCredito.ForeColor = System.Drawing.Color.Red;                            
                        }
                        //*** MRC - TERMINO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
                        else
                        {
                            //*** MRC - INICIO (14/01/2016 12:30) ***                            
                            lblContaCredito.Text = string.Format("Conta {0} para Cr�dito:   {1}", (rowFRN.FRNContaTipoCredito == (int)FRNContaTipoCredito.ContaCorrete) ? "Corrente" : "Poupan�a", strContaCredito);
                            //*** MRC - TERMINO (14/01/2016 12:30) ***
                            lblContaCredito.ForeColor = System.Drawing.Color.Blue;
                        }
                    }
                }
                //*** MRC - FIM - PAG-FOR (2) ***                
                SemafaroFOR = false;
            }
        }

        private void comboBoxEdit1_Properties_Validating(object sender, CancelEventArgs e)
        {
            string Corrigido = "";
            bool primeiro = true;
            for (int i = 0; i < comboBoxEdit1.Text.Length; i++)
            {
                if (primeiro)
                    Corrigido += char.ToUpper(comboBoxEdit1.Text[i]);
                else
                    Corrigido += char.ToLower(comboBoxEdit1.Text[i]);
                if (comboBoxEdit1.Text[i] != ' ')
                    primeiro = false;
            }
            if (comboBoxEdit1.Text != Corrigido)
                comboBoxEdit1.Text = Corrigido;
        }

        bool SemafaroPLA;

        private void lookUpEditPLA1_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroPLA)
            {
                try
                {
                    SemafaroPLA = true;
                    if (sender == lookUpEditPLA1)
                        lookUpEditPLA2.EditValue = lookUpEditPLA1.EditValue;
                    if (sender == lookUpEditPLA2)
                        lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue;
                    SemafaroPLA = false;
                    if ((lookUpEditPLA2.EditValue == null) || (lookUpEditPLA2.EditValue == DBNull.Value))
                    {
                        lookUpEdit1.Enabled = false;
                        _PLArow = null;
                    }
                    else
                    {
                        string PLA = lookUpEditPLA2.EditValue.ToString();
                        PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas.FindByPLA(PLA);
                        SubPLanoPLAnocontasBindingSource.Filter = string.Format("SPL_PLA = {0}", PLA);
                        lookUpEdit1.Enabled = true;
                    }
                    CarregaLista();
                }
                finally
                {
                    SemafaroPLA = false;
                }
            }
        }

        private Framework.datasets.dSTRs _dSTRs;

        private Framework.datasets.dSTRs dSTRs
        {
            get
            {
                if (_dSTRs == null)
                {
                    _dSTRs = new Framework.datasets.dSTRs();
                    _dSTRs.Fill("PLA");
                };
                return _dSTRs;
            }
        }

        private void CarregaLista()
        {
            comboBoxEdit1.Properties.Items.Clear();
            foreach (Framework.datasets.dSTRs.STRsRow rowSTR in dSTRs.STRs)
            {
                if (rowSTR.STRChave.ToString() == lookUpEditPLA1.EditValue.ToString())
                    comboBoxEdit1.Properties.Items.Add(rowSTR.STRTexto);
            };
        }

        private void comboBoxEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            int nPLA;
            if (int.TryParse(lookUpEditPLA1.EditValue.ToString(), out nPLA))
            {
                switch (e.Button.Kind)
                {
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Plus:
                        dSTRs.Add(nPLA, comboBoxEdit1.Text);
                        CarregaLista();
                        break;
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Minus:
                        dSTRs.Dell(nPLA, comboBoxEdit1.Text);
                        CarregaLista();
                        break;
                }
            };
        }
        
        /*
        private void cCompet3_OnChange(object sender, EventArgs e)
        {
            CalculaDatas();            
        }*/

        bool blocCalculaDatas ;

        private void CalculaDatas()
        {            
            if((!blocCalculaDatas) && (LinhaMae != null))
                try
                {                    
                    blocCalculaDatas = true;
                    if (spinEdit1.Value == 0)
                        return;  
                    Competencia compPAG = cCompetIniRef.Comp.CloneCompet(LinhaMae.PGFRef);
                    dateEdit1.DateTime = compPAG.DataNaCompetencia((int)spinEdit1.Value, Framework.objetosNeon.Competencia.Regime.mes);
                    if (cCompetFimRef.IsNull)
                    {
                        dateEdit2.Visible = false;                        
                    }
                    else
                    {
                        dateEdit2.Visible = true;
                        compPAG = cCompetFimRef.Comp.CloneCompet(LinhaMae.PGFRef);
                        dateEdit2.DateTime = compPAG.DataNaCompetencia((int)spinEdit1.Value, Framework.objetosNeon.Competencia.Regime.mes);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally 
                {
                    blocCalculaDatas = false;                    
                }
            
        }

        private void spinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            CalculaDatas();
        }

        private void RecuperaChs()
        {
            if (ValorBruto != 0)
            {
                chISS.Checked = (ValorPGFISS == Math.Round(ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoISS), 2));
                chINSSP.Checked = (ValorPGFINSSP == Math.Round(ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfRet), 2));
                chINSST.Checked = (ValorPGFINSST == Math.Round(ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfEmp), 2));
                if (dllImpostos.Imposto.AliquotaBase(TipoImposto.IR) != dllImpostos.Imposto.AliquotaBase(TipoImposto.IRPF))
                    throw new Exception("Aliquotas de IR diferentes");
                chIR.Checked = (ValorPGFIR == Math.Round(ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoImposto.IR), 2));
                chPIS.Checked = (ValorPGFPISCOF == Math.Round(ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoImposto.PIS_COFINS_CSLL), 2));
            };
        }

        private void cPagamentosPeriodicosCampos_cargaFinal(object sender, EventArgs e)
        {            
            bool? booPagamentoEletronico = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_bool("SELECT TOP (1) CASE WHEN (CONCodigoComunicacao IS NOT NULL) AND (CONCodigoComunicacao <> '') AND (CCTPrincipal = 1) THEN CCTPagamentoEletronico ELSE 0 END AS PagamentoEletronico FROM CONDOMINIOS INNER JOIN ContaCorrenTe ON CONDOMINIOS.CON = ContaCorrenTe.CCT_CON WHERE (((SELECT CONTipoContaPE FROM CONDOMINIOS WHERE CON = @P1) IN (0,1)) AND (CON = @P1))OR (((SELECT CONTipoContaPE FROM CONDOMINIOS WHERE CON = @P1) IN (2)) AND (CCT_CON = 409)) ORDER BY CCTPrincipal DESC", LinhaMae.PGF_CON);
            //*** MRC - TERMINO - PAG-FOR (05/08/2014 11:00) ***
            if (booPagamentoEletronico.GetValueOrDefault(false))
            {               
               Enumeracoes.VirEnumTipoPagPeriodicoUtil.CarregaEditorDaGrid(imageComboBoxEdit1);                     
               //Preenche combo com contas para debito 
               Cadastros.Condominios.PaginasCondominio.dConta dContas = new Cadastros.Condominios.PaginasCondominio.dConta();
               Cadastros.Condominios.PaginasCondominio.dConta.ContaCorrenTeDataTable dCont = dContas.ContaCorrenTeTableAdapter.GetContaPgEletronicoByCON(LinhaMae.PGF_CON);
               foreach (DataRow row in dCont.Rows)
               { cmbContaDebito.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(row["DescricaoContaCorrente"].ToString(), row["CCT"], 0)); }

               //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
               //Preenche combo com contas para credito
               dContas.ContaCorrenTeTableAdapter.Fill(dCont, LinhaMae.PGF_CON);
               foreach (DataRow row in dCont.Rows)
               { cmbContaCredito.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(row["DescricaoContaCorrente"].ToString(), row["CCT"], 0)); }
                //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
            }
            else
            {
               //Sem pagamento eletronico
               Enumeracoes.VirEnumTipoPagPeriodicoSemPE.CarregaEditorDaGrid(imageComboBoxEdit1);
            }
            //*** MRC - FIM - PAG-FOR (2) ***
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
            chkDepProprioCon.Checked = !LinhaMae.IsPGF_CCTCredNull();
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***

            cCompetFimRef.CompMinima = cCompetIniRef.Comp;
            cCompetFimRef.CompMaxima = new Competencia(DateTime.Today.AddYears(3));
            if (pk == 0)
            {
                Status = StatusPagamentoPeriodico.Inativo;
                LinhaMae.PGFStatus = (int)StatusPagamentoPeriodico.Inativo;
                LinhaMae.PGFCompetI = cCompetIniRef.Comp.CompetenciaBind;        
                LinhaMae.PGFCompetNaDescricao = LinhaMae.PGFParcelaNaDesc = true;
                LinhaMae.PGFRef = 0;
                LinhaMae.PGFISS = LinhaMae.PGFIR = LinhaMae.PGFINSSP = LinhaMae.PGFINSST = LinhaMae.PGFPISCOF = 0;
            }
            blocCalculaDatas = false;
            CalculaDatas();
            bloc = true;
            RecuperaChs();
            bloc = false;
            //cCompetIni.Comp.CON = cCompetFim.Comp.CON = LinhaMae.PGF_CON;            
            //Competencia CompRef = new Competencia(DateTime.Today, LinhaMae.PGF_CON);
            //DateTime dFim;
            //DateTime dIni;
            //CompRef.PeriodoBalancete(out dIni, out dFim);
            //txtCadastro.Text = string.Format("Compet�ncia {0}: de {1:dd/MM/yyyy} a {2:dd/MM/yyyy}", CompRef, dIni, dFim);
            CalculaLiquido();
            
        }

        private bool ValidaDados()
        {
            if (imageComboBoxEdit1.Text == "")
            {
                imageComboBoxEdit1.Focus();
                MessageBox.Show("Forma de Pagamento inv�lida");
                return false;
            }
            if (cmbContaDebito.Visible && cmbContaDebito.Text == "")
            {
                cmbContaDebito.Focus();
                MessageBox.Show("Informe uma Conta para D�bito");
                return false;
            }

            if (cmbContaCredito.Visible && cmbContaCredito.Text == "")
            {
                cmbContaCredito.Focus();
                MessageBox.Show("Informe uma Conta para Cr�dito, obrigat�rio para a forma de pagamento selecionada com cr�dito em conta do pr�prio condom�nio");
                return false;
            }

            if (cmbContaCredito.Visible && (cmbContaCredito.EditValue.Equals(cmbContaDebito.EditValue)))
            {
                cmbContaCredito.Focus();
                MessageBox.Show("As Contas para D�bito e Cr�dito do mesmo condom�nio n�o podem ser iguais");
                return false;
            }

            TipoPagPeriodico Tipo = (TipoPagPeriodico)imageComboBoxEdit1.EditValue;
            if ((Tipo == TipoPagPeriodico.PECreditoConta || Tipo == TipoPagPeriodico.PETransferencia))
            {
                if (grcForn.Enabled && lookUpFOR1.Text == "")
                {
                    lookUpFOR1.Focus();
                    MessageBox.Show("Informe o Fornecedor, obrigat�rio para a forma de pagamento selecionada");
                    return false;
                }
                if (grcForn.Enabled && lblContaCredito.ForeColor == System.Drawing.Color.Red)
                {
                    lookUpFOR1.Focus();
                    MessageBox.Show("Conta para Cr�dito n�o cadastrada ou incompleta para o Fornecedor, obrigat�ria para a forma de pagamento selecionada. Atualize o cadastro do Fornecedor");
                    return false;
                }
            }

            if (lookUpEdit1.Text == "")
            {
                if (lookUpEdit1.Enabled) { lookUpEdit1.Focus(); }
                else { lookUpEditPLA1.Focus(); }
                MessageBox.Show("Classifica��o da nota para a Prefeitura inv�lida");
                return false;
            }

            return true;
        }


        /// <summary>
        /// Grava as altera��es
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            Validate();

            if (!ValidaDados())
                return false;

            TipoPagPeriodico Tipo = (TipoPagPeriodico)imageComboBoxEdit1.EditValue;

            string strcnpj = "";
            DocBacarios.CPFCNPJ cnpj;
            if (!LinhaMae.IsPGF_FRNNull())
            {
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select FRNCNPJ from FORNECEDORES where FRN = @P1", out strcnpj, LinhaMae.PGF_FRN);
                cnpj = new DocBacarios.CPFCNPJ(strcnpj);
            }
            else
                cnpj = new DocBacarios.CPFCNPJ("lixo");
            if (cnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
            {
                if (calcEditINSST.EditValue != null)
                {
                    MessageBox.Show("Erro de cadastro: INSS do tomador cadastrado para PJ");
                    return false;
                }
            }
            
            if (VerificarObs)
            {
                MessageBox.Show("Foram feitas altera��es autom�ticas na observa��o do impresso.\r\nVerificar o texto","ATEN��O");
                VerificarObs = false;
                memoImpresso.Focus();
                return false;
            };            
            LinhaMae.PGFValor = (decimal)calcEditValor.EditValue;
            if ((calcEditISS.EditValue != null) && (calcEditISS.EditValue != DBNull.Value))
                LinhaMae.PGFISS = (decimal)calcEditISS.EditValue;
            else
                LinhaMae.SetPGFISSNull();
            if ((calcEditINSSP.EditValue != null) && (calcEditINSSP.EditValue != DBNull.Value))
                LinhaMae.PGFINSSP = (decimal)calcEditINSSP.EditValue;
            else
                LinhaMae.SetPGFINSSPNull();
            if ((calcEditINSST.EditValue != null) && (calcEditINSST.EditValue != DBNull.Value))
                LinhaMae.PGFINSST = (decimal)calcEditINSST.EditValue;
            else
                LinhaMae.SetPGFINSSTNull();
            if ((calcEditIR.EditValue != null) && (calcEditIR.EditValue != DBNull.Value))
                LinhaMae.PGFIR = (decimal)calcEditIR.EditValue;
            else
                LinhaMae.SetPGFIRNull();
            if ((calcEditPIS.EditValue != null) && (calcEditPIS.EditValue != DBNull.Value))
                LinhaMae.PGFPISCOF = (decimal)calcEditPIS.EditValue;
            else
                LinhaMae.SetPGFPISCOFNull();
            if (spin_PGFIdentificacao.EditValue == null)
                LinhaMae.SetPGFIdentificacaoNull();
            else
                LinhaMae.PGFIdentificacao = (long)spin_PGFIdentificacao.Value;            
            ValidateChildren();
            
            Validate(false);
            LinhaMae.EndEdit();
            if (!LinhaMae.IsPGFCompetFNull() && (LinhaMae.PGFCompetF < LinhaMae.PGFCompetI))
                LinhaMae.PGFCompetF = LinhaMae.PGFCompetI;
            //bloc = blockAntigo;
            bool ComDOC = false;
            //TipoPagPeriodico Tipo = (TipoPagPeriodico)imageComboBoxEdit1.EditValue;
            if (Tipo.EstaNoGrupo(TipoPagPeriodico.DebitoAutomatico,TipoPagPeriodico.DebitoAutomaticoConta,TipoPagPeriodico.Conta,TipoPagPeriodico.Folha))            
                ComDOC = true;            
            else
                foreach (dPagamentosPeriodicos.DOCumentacaoRow rowDOC in LinhaMae.GetDOCumentacaoRows())
                {
                    if (((TiposDOC)rowDOC.DOCTipo == TiposDOC.Descritivo) && (rowDOC.DOCValido))
                        ComDOC = true;
                    if (((TiposDOC)rowDOC.DOCTipo == TiposDOC.Contrato) && (rowDOC.DOCValido))
                        ComDOC = true;
                }
            if ((Status == StatusPagamentoPeriodico.Ativado) && !ComDOC)
                Status = StatusPagamentoPeriodico.AtivadoSemDOC;
            if ((Status == StatusPagamentoPeriodico.AtivadoSemDOC) && ComDOC)
                Status = StatusPagamentoPeriodico.Ativado;
            if (pk == 0)
                LinhaMae.PGFProximaCompetencia = LinhaMae.PGFCompetI;
            LinhaMae.PGFStatus = (int)Status;
            if (Status == StatusPagamentoPeriodico.Inativo)
                if (MessageBox.Show("O status est� como inativo.\r\n\r\nConfirma a desativa��o?", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return false;
            if( LinhaMae.RowState == DataRowState.Detached 
                 ||
                ((int)LinhaMae["PGFStatus", DataRowVersion.Original] != (int)LinhaMae["PGFStatus", DataRowVersion.Current])
                 ||
                ((int)LinhaMae["PGFProximaCompetencia", DataRowVersion.Original] != (int)LinhaMae["PGFProximaCompetencia", DataRowVersion.Current])
              )
            {
                LinhaMae.PGFHistorico = string.Format("{0}{1} Altera��o feita por {2} Status:{3} Proxima competencia:{4}",
                                                       LinhaMae.IsPGFHistoricoNull() ? "" : LinhaMae.PGFHistorico + Environment.NewLine,
                                                       DateTime.Now,
                                                       CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
                                                       ComboStatus.Text,                                                       
                                                       new Competencia(LinhaMae.PGFProximaCompetencia));
            }            
            
            if ((TipoPagPeriodico)LinhaMae.PGFForma == TipoPagPeriodico.DebitoAutomatico)
                if (!LinhaMae.IsPGFDadosPagNull())
                    LinhaMae.PGFDadosPag = StringEdit.Limpa(LinhaMae.PGFDadosPag, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.Maiusculas, StringEdit.TiposLimpesa.RemoveBrancosDuplicados });
            if (base.Update_F())
            {                
                return PagamentoPeriodico.CadastrarProximo();                
            }
            else
                return false;

        }

        private void calcEdit1_Validating(object sender, CancelEventArgs e)
        {
            e.Cancel = (calcEditValor.Value < 0);
        }


        //nao � necess�rio mas � bom prever uma eventual atera��o no componente cCompet que dispare um novo evento por causa do spinEditRefonchange
        //private bool bloccCompet1_OnChange;

        
        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            cCompetFimRef.CompMinima = cCompetIniRef.Comp;
            CalculaDatas();
        }

        private void cCompetFimRef_OnChange(object sender, EventArgs e)
        {            
            CalculaDatas();
        }

        private void cPagamentosPeriodicosCampos_Load(object sender, EventArgs e)
        {
            CalculaDatas();
            //blocCalculaDatas = false;
            //MessageBox.Show("P1");
            //CalculaDatas();
            //MessageBox.Show("P2");
            //if (LinhaMae == null)
            //    MessageBox.Show("LinhaMae");
            //if (cCompetIni == null)
            //    MessageBox.Show("cCompetIni");
            //if (LinhaMae.PGFCompetI == null)
            //    MessageBox.Show("LinhaMae.PGFCompetI");
            //if (cCompetIni.Comp == null)
            //    MessageBox.Show("cCompetIni.Comp");
            //MessageBox.Show("P21");
            //memoEdit2.Text += string.Format("\r\nload I {0} - {1}", LinhaMae.PGFCompetI, cCompetIni.Comp.ToString());
            //MessageBox.Show("P22");
            //if (LinhaMae.IsPGFCompetFNull())
            //    memoEdit2.Text += string.Format("\r\nload F NULO");
            //else
            //    memoEdit2.Text += string.Format("\r\nload F {0} - {1}", LinhaMae.PGFCompetF, cCompetFim.Comp.ToString());
        }

        TipoImposto TipoISS
        {
            get { return Framework.DSCentral.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA; }
        }

        private void RecalcularImpostos()
        {
            if (!bloc)
            {
                bloc = true;
                if (dllImpostos.Imposto.AliquotaBase(TipoImposto.IR) != dllImpostos.Imposto.AliquotaBase(TipoImposto.IRPF))
                    throw new Exception("Aliquotas de IR diferentes");
                try
                {
                    if (chISS.Checked)
                        calcEditISS.EditValue = /*LinhaMae.PGFISS =*/ Math.Round(dllImpostos.Imposto.AliquotaBase(TipoISS) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                    if (chINSSP.Checked)
                        calcEditINSSP.EditValue = /*LinhaMae.PGFINSSP =*/ Math.Round(dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfRet) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                    if (chINSST.Checked)
                        calcEditINSST.EditValue = /*LinhaMae.PGFINSST =*/ Math.Round(dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfEmp) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                    if (chIR.Checked)
                        calcEditIR.EditValue = /*LinhaMae.PGFIR =*/ Math.Round(dllImpostos.Imposto.AliquotaBase(TipoImposto.IR) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                    if (chPIS.Checked)
                        calcEditPIS.EditValue = /*LinhaMae.PGFPISCOF =*/ Math.Round(dllImpostos.Imposto.AliquotaBase(TipoImposto.PIS_COFINS_CSLL) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                    CalculaLiquido();
                }
                finally
                {
                    bloc = false;
                }
            }
        }

        private System.Collections.ArrayList _ObsISS;
        private System.Collections.ArrayList _ObsINSSP;
        private System.Collections.ArrayList _ObsINSST;

        private System.Collections.ArrayList ObsISS
        {
            get
            {
                if (_ObsISS == null)
                {
                    _ObsISS = new System.Collections.ArrayList();
                    _ObsISS.Add("* ISS n�o retido por solicita��o do condom�nio\r\n");
                    _ObsISS.Add("* ISS n�o retido por solicita��o do prestador\r\n");
                };
                return _ObsISS;
            }
        }
        
        private System.Collections.ArrayList ObsINSSP
        {
            get
            {
                if (_ObsINSSP == null)
                {
                    _ObsINSSP = new System.Collections.ArrayList();
                    _ObsINSSP.Add("* INSS n�o retido por solicita��o do condom�nio\r\n");
                    _ObsINSSP.Add("* INSS n�o retido por solicita��o do prestador\r\n");
                };
                return _ObsINSSP;
            }
        }

        private System.Collections.ArrayList ObsINSST
        {
            get
            {
                if (_ObsINSST == null)
                {
                    _ObsINSST = new System.Collections.ArrayList();
                    _ObsINSST.Add("* INSS n�o recolhido por solicita��o do condom�nio\r\n");
                };
                return _ObsINSST;
            }
        }

        bool VerificarObs = false;

        private void ch_CheckedChanged(object sender, EventArgs e)
        {
            if (!bloc)
            {
                bloc = true;
                try
                {
                    if (sender == chISS)
                    {
                        if (chISS.Checked)
                        {
                            calcEditISS.EditValue = Math.Round(dllImpostos.Imposto.AliquotaBase(TipoISS) * ValorBruto, 2);
                            foreach(string ObsISSX in ObsISS)
                                if (memoImpresso.Text.Contains(ObsISSX))
                                {
                                    memoImpresso.Text = memoImpresso.Text.Replace(ObsISSX, "");
                                    VerificarObs = true;
                                }
                        }
                        else
                        {
                            calcEditISS.EditValue = DBNull.Value;
                            string Sel = "";
                            VirInput.Input.Execute("Obs:", ref Sel, ObsISS);
                            if (Sel != "")
                            {
                                memoImpresso.Text += Sel;
                                VerificarObs = true;
                            }                                                    
                        }
                        calcEditISS.Focus();                       
                    }
                    else if (sender == chINSSP)
                    {
                        if (chINSSP.Checked)
                        {
                            calcEditINSSP.EditValue = Math.Round(dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfRet) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                            foreach (string ObsINSSPX in ObsINSSP)
                                if (memoImpresso.Text.Contains(ObsINSSPX))
                                {
                                    memoImpresso.Text = memoImpresso.Text.Replace(ObsINSSPX, "");
                                    VerificarObs = true;
                                }
                        }
                        else
                        {
                            calcEditINSSP.EditValue = null;
                            string Sel = "";
                            VirInput.Input.Execute("Obs:", ref Sel, ObsINSSP);
                            if (Sel != "")
                            {
                                memoImpresso.Text += Sel;
                                VerificarObs = true;
                            }
                        }
                        calcEditINSSP.Focus();
                    }
                    else if (sender == chINSST)
                    {
                        if (chINSST.Checked)
                        {
                            calcEditINSST.EditValue = Math.Round(dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfEmp) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                            foreach (string ObsINSSTX in ObsINSST)
                                if (memoImpresso.Text.Contains(ObsINSSTX))
                                {
                                    memoImpresso.Text = memoImpresso.Text.Replace(ObsINSSTX, "");
                                    VerificarObs = true;
                                }
                        }
                        else
                        {
                            calcEditINSST.EditValue = null;
                            string Sel = "";
                            VirInput.Input.Execute("Obs:", ref Sel, ObsINSST);
                            if (Sel != "")
                            {
                                memoImpresso.Text += Sel;
                                VerificarObs = true;
                            }
                        }
                        calcEditINSST.Focus();
                    }
                    else if (sender == chIR)
                    {
                        if (chIR.Checked)
                            calcEditIR.EditValue = /*LinhaMae.PGFIR =*/ Math.Round(dllImpostos.Imposto.AliquotaBase(TipoImposto.IR) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                        else
                            calcEditIR.EditValue = null;
                        calcEditIR.Focus();
                    }
                    else if (sender == chPIS)
                    {
                        if (chPIS.Checked)
                            calcEditPIS.EditValue = /*LinhaMae.PGFPISCOF =*/ Math.Round(dllImpostos.Imposto.AliquotaBase(TipoImposto.PIS_COFINS_CSLL) * ValorBruto /*LinhaMae.PGFValor*/, 2);
                        else
                            calcEditPIS.EditValue = null;
                        calcEditPIS.Focus();
                    };
                    CalculaLiquido();
                }
                finally
                {
                    bloc = false;
                }
            };
        }

        private decimal Val(object Entrada)
        {            
            return ((Entrada == null) || (Entrada == DBNull.Value)) ? 0 : (decimal)Entrada;
        }

        decimal ValorPGFISS 
        {            
            get { return Val(calcEditISS.EditValue); }
            set { calcEditISS.EditValue = Math.Round(value, 2); }
        }

        decimal ValorPGFINSSP
        {            
            get { return Val(calcEditINSSP.EditValue); }
            set { calcEditINSSP.EditValue = Math.Round(value, 2); }
        }

        decimal ValorPGFINSST
        {            
            get { return Val(calcEditINSST.EditValue); }
            set { calcEditINSST.EditValue = Math.Round(value, 2); }
        }

        decimal ValorPGFIR
        {         
            get { return Val(calcEditIR.EditValue); }
            set { calcEditIR.EditValue = Math.Round(value, 2); }
        }

        decimal ValorPGFPISCOF
        {            
            get { return Val(calcEditPIS.EditValue); }
            set { calcEditPIS.EditValue = Math.Round(value, 2); }
        }

        decimal ValorBruto
        {
            get { return Val(calcEditValor.EditValue); }
            set { calcEditValor.EditValue = Math.Round(value, 2); }
        }

        private System.Collections.ArrayList _ImpostosAplicaveis;

        private System.Collections.ArrayList ImpostosAplicaveis
        {
            get {
                if (_ImpostosAplicaveis == null)
                {
                    _ImpostosAplicaveis = new System.Collections.ArrayList();
                    _ImpostosAplicaveis.Add(TipoISS);
                    _ImpostosAplicaveis.Add(TipoImposto.INSSpfRet);
                    _ImpostosAplicaveis.Add(TipoImposto.INSSpfEmp);
                    _ImpostosAplicaveis.Add(TipoImposto.IR);
                    _ImpostosAplicaveis.Add(TipoImposto.PIS_COFINS_CSLL);
                }
                return _ImpostosAplicaveis;
            }
        }

        private decimal GetValorPGFx(TipoImposto Tipo)
        {
            switch (Tipo)
            {
                case TipoImposto.ISS_SA:
                case TipoImposto.ISSQN:
                    return ValorPGFISS;                
                case TipoImposto.INSSpfRet:
                    return ValorPGFINSSP;
                case TipoImposto.INSSpfEmp:
                    return ValorPGFINSST;
                case TipoImposto.IR:
                    return ValorPGFIR;
                case TipoImposto.PIS_COFINS_CSLL:
                    return ValorPGFPISCOF;
            }
            return 0;
        }

        DevExpress.XtraEditors.CheckEdit Chx(TipoImposto Tipo)
        {
            switch (Tipo)
            {
                case TipoImposto.ISS_SA:
                case TipoImposto.ISSQN:
                    return chISS;
                case TipoImposto.INSSpfRet:
                    return chINSSP;
                case TipoImposto.INSSpfEmp:
                    return chINSST;
                case TipoImposto.IR:
                    return chIR;
                case TipoImposto.PIS_COFINS_CSLL:
                    return chPIS;
            };
            return null;
        }

        private void CalculaLiquido()
        {
            //calcLIQUIDO.EditValue = ValorBruto - ValorPGFISS - ValorPGFINSSP - ValorPGFINSST - ValorPGFIR - ValorPGFPISCOF;
            calcLIQUIDO.EditValue = ValorDescontado;
            RecuperaChs();
        }

        private decimal ValorDescontado
        {
            get
            {
                decimal retorno = ValorBruto;
                if (dllImpostos.Imposto.Desconta(TipoISS))
                    retorno -= ValorPGFISS;
                if (dllImpostos.Imposto.Desconta(TipoImposto.INSSpfRet))
                    retorno -= ValorPGFINSSP;
                if (dllImpostos.Imposto.Desconta(TipoImposto.INSSpfEmp))
                    retorno -= ValorPGFINSST;
                if (dllImpostos.Imposto.Desconta(TipoImposto.IR))
                    retorno -= ValorPGFIR;
                if (dllImpostos.Imposto.Desconta(TipoImposto.PIS_COFINS_CSLL))
                    retorno -= ValorPGFPISCOF;
                return retorno;
            }
        }

        private void CalculaBruto()
        {
            decimal TaxaImpostosTotal = 1;
            decimal ImpostoManual = 0;
            foreach (TipoImposto Tx in ImpostosAplicaveis)
            {
                if (dllImpostos.Imposto.Desconta(Tx))
                    if (Chx(Tx).Checked)
                        TaxaImpostosTotal -= dllImpostos.Imposto.AliquotaBase(Tx);
                    else
                        ImpostoManual += GetValorPGFx(Tx);
            }

            /*
            if (chISS.Checked)
                TaxaImpostosTotal -= dllImpostos.Imposto.AliquotaBase(TipoISS);
            else
                ImpostoManual += ValorPGFISS;
            if (chINSSP.Checked)
                TaxaImpostosTotal -= dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfRet);
            else
                ImpostoManual += ValorPGFINSSP;
            if (chINSST.Checked)
                TaxaImpostosTotal -= dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfEmp);
            else
                ImpostoManual += ValorPGFINSST;
            if (chIR.Checked)
                TaxaImpostosTotal -= dllImpostos.Imposto.AliquotaBase(TipoImposto.IR);
            else
                ImpostoManual += ValorPGFIR;
            if (chPIS.Checked)
                TaxaImpostosTotal -= dllImpostos.Imposto.AliquotaBase(TipoImposto.PIS_COFINS_CSLL);
            else
                ImpostoManual += ValorPGFPISCOF;
             */ 
            ValorBruto = ((decimal)calcLIQUIDO.EditValue + ImpostoManual) / TaxaImpostosTotal;
            decimal LiquidoCalculado = 0;
            decimal Fator = 0;
            while (LiquidoCalculado != (decimal)calcLIQUIDO.EditValue)
            {                
                if (chISS.Checked)
                    ValorPGFISS = ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoISS);
                if (chINSSP.Checked)
                    ValorPGFINSSP = ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfRet);
                if (chINSST.Checked)
                    ValorPGFINSST = ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoImposto.INSSpfEmp);
                if (chIR.Checked)
                    ValorPGFIR = ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoImposto.IR);
                if (chPIS.Checked)
                    ValorPGFPISCOF = ValorBruto * dllImpostos.Imposto.AliquotaBase(TipoImposto.PIS_COFINS_CSLL);
                LiquidoCalculado = ValorDescontado;
                decimal FatorAnterior = Fator;
                if (LiquidoCalculado < (decimal)calcLIQUIDO.EditValue)
                    Fator = 0.01M;
                else if (LiquidoCalculado > (decimal)calcLIQUIDO.EditValue)
                    Fator = -0.01M;
                else
                    Fator = 0;
                if (Fator == -FatorAnterior)
                    break;
                ValorBruto += Fator;

            }
        }

        bool bloc = false;

        private void calcEdit2_EditValueChanged(object sender, EventArgs e)
        {
            if (!bloc)
            {
                try
                {
                    bloc = true;
                    //LinhaMae.PGFISS = (decimal)calcEditISS.EditValue;
                    CalculaLiquido();
                }
                finally
                {
                    bloc = false;
                }
            }
        }

        private void calcEdit3_EditValueChanged(object sender, EventArgs e)
        {
            if (!bloc)
            {
                try
                {
                    bloc = true;
                    //LinhaMae.PGFINSSP = (decimal)calcEditINSSP.EditValue;
                    CalculaLiquido();
                }
                finally
                {
                    bloc = false;
                }
            }
        }

        private void calcEdit4_EditValueChanged(object sender, EventArgs e)
        {
            if (!bloc)
            {
                try
                {
                    bloc = true;
                    //LinhaMae.PGFINSST = (decimal)calcEditINSST.EditValue;
                    CalculaLiquido();
                }
                finally
                {
                    bloc = false;
                }
            }
        }

        private void calcEdit5_EditValueChanged(object sender, EventArgs e)
        {
            if (!bloc)
            {
                try
                {
                    bloc = true;
                    //LinhaMae.PGFIR = (decimal)calcEditIR.EditValue;
                    CalculaLiquido();
                }
                finally
                {
                    bloc = false;
                }
            }
        }

        private void calcEdit6_EditValueChanged(object sender, EventArgs e)
        {
            if (!bloc)
            {
                try
                {
                    bloc = true;
                    //LinhaMae.PGFPISCOF = (decimal)calcEditPIS.EditValue;
                    CalculaLiquido();
                }
                finally
                {
                    bloc = false;
                }
            }
        }

        private void calcLIQUIDO_ValueChanged(object sender, EventArgs e)
        {
            if (!bloc)
            {
                try
                {
                    bloc = true;
                    calcLIQUIDO.EditValue = (decimal)calcLIQUIDO.EditValue;
                    //LinhaMae.PGFValor = (decimal)calcLIQUIDO.EditValue;
                    CalculaBruto();
                }
                finally
                {
                    bloc = false;
                }
            }
        }

        private void calcEdit1_EditValueChanged(object sender, EventArgs e)
        {
            //LinhaMae.PGFValor = (decimal)calcEdit1.EditValue;
            RecalcularImpostos();
        }

        

        //int teste;

        private static VirEnum virEnumStatusPagamentoPeriodico;

        /// <summary>
        /// VirEnum para StatusPagamentoPeriodico
        /// </summary>
        public static VirEnum VirEnumStatusPagamentoPeriodico
        {
            get
            {
                if (virEnumStatusPagamentoPeriodico == null)
                {
                    virEnumStatusPagamentoPeriodico = new VirEnum(typeof(StatusPagamentoPeriodico));
                    virEnumStatusPagamentoPeriodico.GravaNomes(StatusPagamentoPeriodico.Inativo, "Desativado");
                    virEnumStatusPagamentoPeriodico.GravaNomes(StatusPagamentoPeriodico.AtivadoSemDOC, "Ativado (sem documento)");
                    virEnumStatusPagamentoPeriodico.GravaNomes(StatusPagamentoPeriodico.Ativado, "Ativado");
                    virEnumStatusPagamentoPeriodico.GravaNomes(StatusPagamentoPeriodico.Encerrado, "Encerrado");
                }
                return virEnumStatusPagamentoPeriodico;
            }
        }

        private static VirEnum virEnumTiposDOC;

        /// <summary>
        /// VirEnum para StatusPagamentoPeriodico
        /// </summary>
        public static VirEnum VirEnumTiposDOC
        {
            get
            {
                if (virEnumTiposDOC == null)
                {
                    virEnumTiposDOC = new VirEnum(typeof(TiposDOC));
                    virEnumTiposDOC.GravaNomes(TiposDOC.Descritivo, "Descritivo");
                    virEnumTiposDOC.GravaNomes(TiposDOC.Contrato, "Contrato");
                    virEnumTiposDOC.GravaNomes(TiposDOC.Apolice, "Apolice");
                    virEnumTiposDOC.GravaNomes(TiposDOC.Outro, "Outro");                    
                }
                return virEnumTiposDOC;
            }
        }

        private bool Travado 
        {
            get { return grcForn.Enabled; }
            set {
                GC_identificacao.Enabled = groupControl8.Enabled = memoImpresso.Enabled = grcForn.Enabled = groupControl1.Enabled = groupControl3.Enabled = groupControl4.Enabled = value;                
                if (value) grcForn.Enabled = !chkDepProprioCon.Checked;                     
                BotImprimir.Enabled = !grcForn.Enabled;
            }          
        }

        private StatusPagamentoPeriodico Status;

        private void ComboStatus_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (    (e.OldValue != DBNull.Value)
                    &&
                    (e.NewValue != DBNull.Value)
                    &&
                    (PagamentoPeriodico.StatusAtivos.Contains((StatusPagamentoPeriodico)e.OldValue))
                    &&
                    (!PagamentoPeriodico.StatusAtivos.Contains((StatusPagamentoPeriodico)e.NewValue))
                    )
            {
                if (!(MessageBox.Show("Confirma a desativa��o deste pagamento?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
                {
                    e.Cancel = true;
                    return;
                }
            };

            if ((PagamentoPeriodico.StatusAtivos.Contains((StatusPagamentoPeriodico)e.NewValue))
                    &&
                (!CamposObrigatoriosOk(PaGamentosFixosbindingSource))
               )
            {
                e.Cancel = true;
                return;
            }
            Travado = ((StatusPagamentoPeriodico)e.NewValue == StatusPagamentoPeriodico.Inativo);
            Status = (StatusPagamentoPeriodico)e.NewValue;
        }
        

        //private com.websiteseguro.ssl493.WS_Sincro _WSSin;
        private WSSincro.WS_Sincro _WSSin;

        private WSSincro.WS_Sincro WSSin
        {
            get 
            {
                if(_WSSin == null)
                {
                    _WSSin = new WSSincro.WS_Sincro();
                    _WSSin.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.Proxy;
                    if (FuncoesBasicasProc.EstaEmProducao)
                        _WSSin.Url = "http://www.neonimoveis.com.br/Neon23/NeonOnLine/wssincro.asmx";
                    /*
                    if (Framework.DSCentral.EMP == 1)
                    {
                        _WSSin.Proxy = new System.Net.WebProxy("10.135.1.20", 8080);
                        _WSSin.Proxy.Credentials = new System.Net.NetworkCredential("publicacoes", "neon01");
                    }
                    else
                    {
                        _WSSin.Proxy = new System.Net.WebProxy("10.135.1.170", 8080);
                        //_WsP.Proxy.Credentials = new System.Net.NetworkCredential("iris", "neon01");
                    }
                    */ 
                   
                };
                return _WSSin;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dPagamentosPeriodicos.DOCumentacaoRow rowDOC = (dPagamentosPeriodicos.DOCumentacaoRow)gridView3.GetFocusedDataRow();
            if (rowDOC != null)
            {
                if (MessageBox.Show("Confirma o cancelamento do documento?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cPagametnosperiodicosCampos - 1178", dPagamentosPeriodicos.DOCumentacaoTableAdapter);
                        //dPagamentosPeriodicos.DOCumentacaoTableAdapter.AbreTrasacaoLocal("ContaPagar cPagametnosperiodicosCampos - 1164");
                        rowDOC.DOCValido = false;
                        dPagamentosPeriodicos.DOCumentacaoTableAdapter.Update(rowDOC);
                        //com.websiteseguro.ssl493.dDOC dDOC = WSSin.Le(Framework.DSCentral.EMP, rowDOC.DOC);
                        WSSincro.dDOC dDOC = WSSin.Le(Framework.DSCentral.EMP, rowDOC.DOC);
                        dDOC.DOCumentacao[0].Delete();
                        string retorno = WSSin.Grava(dDOC);
                        if (retorno != "ok")
                            throw new Exception(retorno);
                        rowDOC.AcceptChanges();
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    }
                    catch (Exception Ex)
                    {                        
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(Ex);
                        rowDOC.RejectChanges();
                        MessageBox.Show("Erro na publica��o");
                    }
                }
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            cEditaDOC Editor = new cEditaDOC();            
            //Editor.XArquivo.Visible = true;
            if (Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                if ((Editor.XArquivo.Text != null) && (System.IO.File.Exists(Editor.XArquivo.Text)))
                {
                    FileInfo fInfo = new FileInfo(Editor.XArquivo.Text);
                    long numBytes = fInfo.Length;
                    //double dLen = Convert.ToDouble(fInfo.Length / 1000000);
                    if (numBytes > 4000000)
                    {
                        MessageBox.Show("Arquivo muito grande!!!");
                        return;
                    };
                    byte[] bdata;
                    using (System.IO.FileStream fStream = new FileStream(Editor.XArquivo.Text, FileMode.Open, FileAccess.Read))
                    {
                        using (BinaryReader br = new BinaryReader(fStream))
                        {
                            bdata = br.ReadBytes((int)numBytes);
                            br.Close();
                            fStream.Close();
                        }
                    };
                    string ext = Path.GetExtension(Editor.XArquivo.Text).Substring(1);
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cPagametnosperiodicosCampos - 1230",dPagamentosPeriodicos.DOCumentacaoTableAdapter);
                        //dPagamentosPeriodicos.DOCumentacaoTableAdapter.AbreTrasacaoLocal("ContaPagar cPagametnosperiodicosCampos - 1215");
                        dPagamentosPeriodicos.DOCumentacaoRow novarowDOC = dPagamentosPeriodicos.DOCumentacao.NewDOCumentacaoRow();
                        novarowDOC.DOC_PGF = LinhaMae.PGF;
                        novarowDOC.DOCDATAI = DateTime.Now;
                        novarowDOC.DOCEXT = ext;
                        novarowDOC.DOCI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                        novarowDOC.DOCNome = Editor.XDescritivo.Text;
                        novarowDOC.DOCTipo = (int)Editor.XTipo.EditValue;
                        novarowDOC.DOCValido = true;
                        dPagamentosPeriodicos.DOCumentacao.AddDOCumentacaoRow(novarowDOC);
                        dPagamentosPeriodicos.DOCumentacaoTableAdapter.Update(novarowDOC);
                        WSSincro.dDOC dDOC = new WSSincro.dDOC();
                        WSSincro.dDOC.DOCumentacaoRow novarowRemota = dDOC.DOCumentacao.NewDOCumentacaoRow();
                        novarowRemota.DOC = novarowDOC.DOC;
                        novarowRemota.DOC_PGF = novarowDOC.DOC_PGF;
                        novarowRemota.DOCDATAI = novarowDOC.DOCDATAI;
                        novarowRemota.DOCEXT = novarowDOC.DOCEXT;
                        novarowRemota.DOCNome = novarowDOC.DOCNome;
                        novarowRemota.DOCTipo = novarowDOC.DOCTipo;
                        novarowRemota.DOC_EMP = Framework.DSCentral.EMP;
                        novarowRemota.CON = LinhaMae.PGF_CON;
                        dDOC.DOCumentacao.AddDOCumentacaoRow(novarowRemota);
                        string retorno = WSSin.CadastraDOC(dDOC, bdata);
                        if (retorno.StartsWith("ok"))
                        {
                            novarowDOC.AcceptChanges();
                            VirMSSQL.TableAdapter.STTableAdapter.Commit();
                            if(((TiposDOC)novarowDOC.DOCTipo == TiposDOC.Descritivo) && (Status == StatusPagamentoPeriodico.AtivadoSemDOC))
                            {
                                Status = StatusPagamentoPeriodico.AtivadoSemDOC;
                                LinhaMae.PGFStatus = (int)StatusPagamentoPeriodico.AtivadoSemDOC;
                                LinhaMae.PGFHistorico = string.Format("{0}{1} Documento cadastrado: \r\n{3} Status:{2}\r\n\r\n", LinhaMae.IsPGFHistoricoNull() ? "" : LinhaMae.PGFHistorico, DateTime.Now, ComboStatus.Text, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome);
                                Update_F();
                            }
                        }
                        else
                        {
                            novarowDOC.CancelEdit();
                            throw new Exception(retorno);                            
                        }
                    }
                    catch (Exception Ex)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(Ex);
                        MessageBox.Show("Erro na publica��o");
                    }                    
                    //string resposta = WsP.Nova(TabMaeTipada.linhaMae.CONCodigo, Editor.XData.Time, Editor.XDescricao.Text, EMP, ext, Editor.XLocao.Text, (int)(Editor.XTipo.EditValue), bdata);
                    //if (resposta == "ok")
                    //{
                    //    assembleiasBindingSource.DataSource = WsP.GetdWsPublicacoes(TabMaeTipada.linhaMae.CONCodigo, EMP);
                    //}
                    //else
                    //{
                    //    MessageBox.Show(resposta);
                    //}
                }
            }
        }

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            dPagamentosPeriodicos.DOCumentacaoRow row = (dPagamentosPeriodicos.DOCumentacaoRow)gridView3.GetFocusedDataRow();
            if (row != null)
            {
                if (!row.DOCValido)
                    return;
                componenteWEB1.Navigate(string.Format(@"http://www.neonimoveis.com.br/publicacoes/PF/{0}_{1}.{2}", Framework.DSCentral.EMP,row.DOC, row.DOCEXT));
            }
        }

        private void memoImpresso_Leave(object sender, EventArgs e)
        {
            VerificarObs = false;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            PagamentoPeriodico.Imprimir(true);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            dPagamentosPeriodicos.DOCumentacaoRow rowDOC = (dPagamentosPeriodicos.DOCumentacaoRow)gridView3.GetFocusedDataRow();
            if (rowDOC != null)
            {
                if (!rowDOC.DOCValido)
                    return;
                string Descricao = rowDOC.DOCNome;
                if(VirInput.Input.Execute("Novo descritivo:",ref Descricao))                
                {
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cPagametnosperiodicosCampos - 1323", dPagamentosPeriodicos.DOCumentacaoTableAdapter);
                        //dPagamentosPeriodicos.DOCumentacaoTableAdapter.AbreTrasacaoLocal("ContaPagar cPagametnosperiodicosCampos - 1307");
                        rowDOC.DOCNome = Descricao;
                        dPagamentosPeriodicos.DOCumentacaoTableAdapter.Update(rowDOC);
                        WSSincro.dDOC dDOC = WSSin.Le(Framework.DSCentral.EMP, rowDOC.DOC);
                        dDOC.DOCumentacao[0].DOCNome = Descricao;
                        string retorno = WSSin.Grava(dDOC);
                        if (retorno != "ok")
                            throw new Exception(retorno);
                        rowDOC.AcceptChanges();
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    }
                    catch (Exception Ex)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(Ex);
                        rowDOC.RejectChanges();
                        MessageBox.Show("Erro na publica��o");
                    }
                }
            }
        }

        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            if (pk == 0)
                if (!Update_F())
                    e.Cancel = true;
            
            
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dPagamentosPeriodicos.NOtAsRow rowNOA = (dPagamentosPeriodicos.NOtAsRow)gridView1.GetFocusedDataRow();
            if (rowNOA != null)
            {                
                CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(cNotasCampos),string.Format("NOTA {0} - {1:dd/MM/yyyy}", rowNOA.NOAServico, rowNOA.NOADataEmissao), rowNOA.NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);                
            }
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (gridControl1.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
                dPagamentosPeriodicos.PAGamentosRow rowPAG = (dPagamentosPeriodicos.PAGamentosRow)GV.GetFocusedDataRow();
                if (rowPAG != null)
                {
                    dllCheques.Cheque Cheque = new dllCheques.Cheque(rowPAG.PAG_CHE,dllCheques.Cheque.TipoChave.CHE);
                    if (Cheque.Encontrado)
                        if (Cheque.Editar(false) == DialogResult.OK)
                        {
                            dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, pk);
                            dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, pk);
                        }
                }
            }



            
            
        }

        private void imageComboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (bloc)
                return;
            if (imageComboBoxEdit1.Text != "")
            //if(imageComboBoxEdit1.EditValue != null)
            {
                //*** MRC - INICIO - PAG-FOR (2) ***
                TipoPagPeriodico Tipo = (TipoPagPeriodico)imageComboBoxEdit1.EditValue;                
                if (Tipo == TipoPagPeriodico.ChequeDeposito || Tipo == TipoPagPeriodico.DebitoAutomatico ||
                    Tipo == TipoPagPeriodico.PEOrdemPagamento)
                    textEditComplemento.Visible = true;
                else
                    textEditComplemento.Visible = false;
                //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
                if (Tipo == TipoPagPeriodico.PECreditoConta || Tipo == TipoPagPeriodico.PEOrdemPagamento ||
                    Tipo == TipoPagPeriodico.PETituloBancario || Tipo == TipoPagPeriodico.PETituloBancarioAgrupavel || Tipo == TipoPagPeriodico.PETransferencia)
                    lblContaDebito.Visible = cmbContaDebito.Visible = true;
                //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
                else
                {
                    cmbContaDebito.SelectedIndex = -1;
                    LinhaMae.SetPGF_CCTNull();
                    lblContaDebito.Visible = cmbContaDebito.Visible = false;
                }
                //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                if (Tipo == TipoPagPeriodico.PECreditoConta)
                    labelDadosDep.Visible = chkDepProprioCon.Visible = true;
                else
                    labelDadosDep.Visible = chkDepProprioCon.Visible = false;
                //*** MRC - TERMINO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***

                lblContaCredito.Visible = false;
                if (lookUpFOR1.ItemIndex != -1)
                {
                    if (Tipo == TipoPagPeriodico.PECreditoConta || Tipo == TipoPagPeriodico.PETransferencia)
                    {
                        lblContaCredito.Visible = true;
                        dFornecedoresCampos dFornecedoresCamposLocal = new dFornecedoresCampos();
                        //dFornecedoresCampos.FORNECEDORESDataTable Forn = new dFornecedoresCampos.FORNECEDORESDataTable();
                        dFornecedoresCamposLocal.FORNECEDORESTableAdapter.Fill(dFornecedoresCamposLocal.FORNECEDORES, (int)lookUpFOR1.EditValue);
                        dFornecedoresCampos.FORNECEDORESRow FRNrow = dFornecedoresCamposLocal.FORNECEDORES[0];
                        string strContaCredito = string.Format("Banco {0} Ag�ncia {1}-{2} Conta {3}-{4}",
                                                                FRNrow.IsFRNBancoCreditoNull() ? "" : FRNrow.FRNBancoCredito,
                                                                FRNrow.IsFRNAgenciaCreditoNull() ? "" : FRNrow.FRNAgenciaCredito,
                                                                FRNrow.IsFRNAgenciaDgCreditoNull() ? "" : FRNrow.FRNAgenciaDgCredito,
                                                                FRNrow.IsFRNContaCreditoNull() ? "" : FRNrow.FRNContaCredito,
                                                                FRNrow.IsFRNContaDgCreditoNull() ? "" : FRNrow.FRNContaDgCredito);
                        //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                        if (   FRNrow.IsFRNBancoCreditoNull() 
                            || FRNrow.IsFRNAgenciaCreditoNull() 
                            || FRNrow.IsFRNAgenciaDgCreditoNull() 
                            || FRNrow.IsFRNContaCreditoNull() 
                            || FRNrow.IsFRNContaDgCreditoNull() 
                            || FRNrow.FRNBancoCredito.Trim() == "" 
                            || FRNrow.FRNAgenciaCredito.Trim() == "" 
                            || FRNrow.FRNAgenciaDgCredito.Trim() == "" 
                            || FRNrow.FRNContaCredito.Trim() == "" 
                            || FRNrow.FRNContaDgCredito.Trim() == "")
                        {
                            //*** MRC - INICIO (14/01/2016 12:30) ***
                            lblContaCredito.Text = "Conta para Cr�dito n�o cadastrada ou incompleta";
                            //*** MRC - TERMINO (14/01/2016 12:30) ***
                            lblContaCredito.ForeColor = System.Drawing.Color.Red;
                        }
                        //*** MRC - TERMINO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                        else
                        {
                            //*** MRC - INICIO (14/01/2016 12:30) ***
                            lblContaCredito.Text = string.Format("Conta {0} para Cr�dito:   {1}", (FRNrow.FRNContaTipoCredito == (int)FRNContaTipoCredito.ContaCorrete) ? "Corrente" : "Poupan�a", strContaCredito);
                            //*** MRC - TERMINO (14/01/2016 12:30) ***
                            lblContaCredito.ForeColor = System.Drawing.Color.Blue;
                        }
                    }
                }
                //*** MRC - FIM - PAG-FOR (2) ***
                if (Tipo == TipoPagPeriodico.DebitoAutomaticoConta)
                {
                   
                    groupControl9.Text = "Pr�ximo d�bito";
                }
                else if (Tipo == TipoPagPeriodico.Conta)
                {
                    
                    groupControl9.Text = "Pr�xima conta";
                }
                else
                {
                   
                    groupControl9.Text = "Pr�ximo cheque a ser gerado";
                }
            }
        }

        private void cCompet1_OnChange_1(object sender, EventArgs e)
        {
            AjustaProximaData();            
        }

        private void AjustaProximaData()
        {
            Competencia compProxPAG = cCompetProx.Comp.CloneCompet(LinhaMae.PGFRef);
            dateEdit3.DateTime = compProxPAG.DataNaCompetencia((int)spinEdit1.Value, Framework.objetosNeon.Competencia.Regime.mes);
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page == xtraTabPage4)
                AjustaProximaData();
        }

        
        private void dateEdit4_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Undo)
                dateEdit4.EditValue = null;
        }

        

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                if (PLArow == null)
                    return;
                cNovoSPL novo = new cNovoSPL(string.Format("{0} - {1}", PLArow.PLA, PLArow.PLADescricao));

                int SPL = novo.Cadastre(PLArow.PLA);

                if (SPL > 0)
                {
                    //LinhaMae.PGF_SPL = SPL;
                    lookUpEdit1.EditValue = SPL;
                }
            }
        }

        private void dateEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Up:
                    LinhaMae.PGFRef = LinhaMae.PGFRef + 1;
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Down:
                    LinhaMae.PGFRef = LinhaMae.PGFRef - 1;
                    break;
                default:
                    break;
            }            
            CalculaDatas();            
        }

       

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {            
            if (e.Column == colNOACompet)
            {
                if ((e.Value == null) || ((int)e.Value == 0))
                    return;
                Competencia comp = new Competencia((int)e.Value);
                e.DisplayText = comp.ToString();
            }
        }

        private void cCompet1_Load(object sender, EventArgs e)
        {
            cCompet1.CompMaxima = new Competencia().Add(48);
        }

        private void gridView2_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.Name == "gridColumn2")            
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                dPagamentosPeriodicos.PAGamentosRow rowPAG = (dPagamentosPeriodicos.PAGamentosRow)GV.GetDataRow(e.RowHandle);
                if ((rowPAG != null) && !rowPAG.IsPAG_CHENull())
                    e.RepositoryItem = repositoryItemButtonEdit2;
                else
                    e.RepositoryItem = null;                
            }
        }

        private SortedList<int,int> CompetenciasUsadas;

        private Font fonteCancelado;

        private Font FonteCancelado(Font Modelo)
        {
            if (fonteCancelado == null)
                fonteCancelado = new Font(Modelo, FontStyle.Italic | FontStyle.Strikeout);
            return fonteCancelado;
        }

        

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dPagamentosPeriodicos.NOtAsRow rowNOA = (dPagamentosPeriodicos.NOtAsRow)gridView1.GetDataRow(e.RowHandle);
            if (rowNOA == null)
                return;
            NOAStatus Status = (NOAStatus)rowNOA.NOAStatus;
            if (e.Column.FieldName == colNOAStatus.FieldName)
            {                
                try
                {                    
                    e.Appearance.BackColor = Enumeracoes.VirEnumNOAStatus.GetCor(rowNOA.NOAStatus);
                    e.Appearance.BackColor2 = e.Appearance.BackColor;
                    e.Appearance.ForeColor = Color.Black;                    
                }
                catch (Exception)
                {
                    
                }
            }
            else if (e.Column.FieldName == colNOACompet.FieldName)
            {
                try
                {
                    if (CompetenciasUsadas[((int)e.CellValue)] > 1)
                    {
                        e.Appearance.BackColor = System.Drawing.Color.Yellow;
                        e.Appearance.BackColor2 = System.Drawing.Color.Yellow;
                        e.Appearance.ForeColor = Color.Black;
                    }
                }
                catch
                {
                }
            }
            if (Status == NOAStatus.NotaCancelada)
                e.Appearance.Font = FonteCancelado(e.Appearance.Font);
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column == gridColumnBAJ)
            {
                try
                {
                    dPagamentosPeriodicos.NOtAsRow rowNOA = (dPagamentosPeriodicos.NOtAsRow)gridView1.GetDataRow(e.RowHandle);
                    if (rowNOA == null)
                        return;
                    if (CompetenciasUsadas[rowNOA.NOACompet] > 1)                    
                        e.RepositoryItem = repositoryBotAJComp;                    
                }
                catch
                {
                }
            }
        }

        private bool Substituir(dPagamentosPeriodicos.NOtAsRow rowNOACancelar, dPagamentosPeriodicos.NOtAsRow rowNOA)
        {
            dPagamentosPeriodicos.PAGamentosRow[] PAGamentosRows = rowNOACancelar.GetPAGamentosRows();
            if (PAGamentosRows.Length != 1)
                return false;
            dPagamentosPeriodicos.PAGamentosRow PAGamentosRow = PAGamentosRows[0];
            if (PAGamentosRow.IsCHEStatusNull() || ((CHEStatus)PAGamentosRow.CHEStatus != CHEStatus.DebitoAutomatico))
                return false;
            dllCheques.Cheque ChequeCancelar = new dllCheques.Cheque(PAGamentosRow.PAG_CHE, dllCheques.Cheque.TipoChave.CHE);
            if (!ChequeCancelar.Encontrado)
                return false;
            ChequeCancelar.CancelarPagamento(string.Format("Cheque cancelado. Substitu�do pelo pagamento ocorrido em {0}", rowNOA.NOADataEmissao));
            rowNOACancelar.NOAStatus = (int)NOAStatus.NotaCancelada;
            string obs = rowNOACancelar.IsNOAObsNull() ? "" : string.Format("{0}\r\n", rowNOACancelar.NOAObs);
            rowNOACancelar.NOAObs = string.Format("{0}*** Nota cancelada, substitu�da pelo pagamento ocorrido em {0}", obs, rowNOA.NOADataEmissao);
            dPagamentosPeriodicos.NOtAsTableAdapter.Update(rowNOACancelar);
            return true;
        }

        private void repositoryBotAJComp_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dPagamentosPeriodicos.NOtAsRow rowNOA = (dPagamentosPeriodicos.NOtAsRow)gridView1.GetFocusedDataRow();
            if (rowNOA == null)
                return;
            Competencia Ncomp = InputCompetencia.stInput();
            if (Ncomp != null)
            {
                try 
                {
                    Competencia CompOrig = new Competencia(rowNOA.NOACompet);
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar Follow cPagamentosPeriodicosCampos repositoryBotAJComp_ButtonClick - 1569",
                                                            dPagamentosPeriodicos.NOtAsTableAdapter);
                    dPagamentosPeriodicos.NOtAs.DefaultView.Sort = "NOACompet";
                    int i = dPagamentosPeriodicos.NOtAs.DefaultView.Find(Ncomp.CompetenciaBind);
                    if (i >= 0)
                    {
                        dPagamentosPeriodicos.NOtAsRow rowNOA1 = (dPagamentosPeriodicos.NOtAsRow)dPagamentosPeriodicos.NOtAs.DefaultView[i].Row;
                        if ((NOAStatus)rowNOA1.NOAStatus == NOAStatus.Cadastrada)
                            Substituir(rowNOA1,rowNOA);
                    };
                                        
                    rowNOA.NOACompet = Ncomp.CompetenciaBind;
                    string obs = rowNOA.IsNOAObsNull() ? "" : string.Format("{0}\r\n", rowNOA.NOAObs);
                    rowNOA.NOAObs = string.Format("{0}*** Compet�ncia corrigida {0} -> {1}", obs, CompOrig, Ncomp);
                    dPagamentosPeriodicos.NOtAsTableAdapter.Update(rowNOA);
                    PagamentoPeriodico.GravaHistorico(string.Format("Pagamento inicialmente atribu�do a {0} remanejado para {1}", CompOrig, Ncomp));
                    rowNOA.AcceptChanges();
                    VirMSSQL.TableAdapter.CommitSQL();
                    int HNotas = gridView1.FocusedRowHandle;
                    dPagamentosPeriodicos.PAGamentos.Clear();
                    dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, pk);
                    dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, pk);
                    gridView1.FocusedRowHandle = HNotas;
                    CalcCompeteciasDuplicadas();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                }
            }
        }

        
        private void chkDepProprioCon_CheckedChanged(object sender, EventArgs e)
        {
            //Habilita e desabilita dados de credito (condominio ou fornecedor)
            cmbContaCredito.Visible = chkDepProprioCon.Checked;
            if (!chkDepProprioCon.Checked) { cmbContaCredito.EditValue = null; }
            grcForn.Enabled = !chkDepProprioCon.Checked;
        }

        private void BotCorrecao_Click(object sender, EventArgs e)
        {
            //PagamentoPeriodico.CorrecaoEmegencial();
        }
    }
}
