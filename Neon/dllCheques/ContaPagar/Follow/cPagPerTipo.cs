﻿using CompontesBasicos;
using DevExpress.XtraEditors.Controls;
using Framework;
using VirEnumeracoesNeon;
using FrameworkProc.datasets;
using Framework.objetosNeon;
using AbstratosNeon;
using System.Collections.Generic;
using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;

namespace ContaPagar.Follow
{
    /// <summary>
    /// Todos os periódicos de um determinado tipo
    /// </summary>
    public partial class cPagPerTipo : ComponenteBase
    {
        TipoPagPeriodico TipoSel;
        Competencia NovaCompetencia;

        private RadioGroupItem MontaIt(TipoPagPeriodico Tipo,bool enabled)
        {
            return new RadioGroupItem(Tipo, Enumeracoes.VirEnumTipoPagPeriodico.Descritivo(Tipo),enabled);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cPagPerTipo()
        {
            InitializeComponent();
            //TipoPagPeriodico.FolhaFGTS
            radioGroup1.Properties.Items.AddRange(new RadioGroupItem[] 
            { 
                MontaIt(TipoPagPeriodico.FolhaPIS,false),
                MontaIt(TipoPagPeriodico.FolhaPISEletronico,false),
                MontaIt(TipoPagPeriodico.FolhaIR,false),
                MontaIt(TipoPagPeriodico.FolhaIREletronico,false),
                MontaIt(TipoPagPeriodico.FolhaFGTS,true),
                MontaIt(TipoPagPeriodico.FolhaFGTSEletronico,false),
                MontaIt(TipoPagPeriodico.Folha,true),
                MontaIt(TipoPagPeriodico.FolhaEletronico,false)
            });
            AjustaTela(StatusTela.limpa);
        }

        private void radioGroup1_EditValueChanged(object sender, System.EventArgs e)
        {
            Carrega((TipoPagPeriodico)radioGroup1.EditValue);
        }


        private enum StatusTela { limpa, NovoCarregado }

        private void AjustaTela(StatusTela Status)
        {
            switch (Status)
            {
                case StatusTela.limpa:
                    BandArquivo.Visible = false;
                    BCadastra.Enabled = false;
                    BCarregaA.Enabled = true;
                    break;
                case StatusTela.NovoCarregado:
                    BandArquivo.Visible = true;
                    BCadastra.Enabled = true;
                    BCarregaA.Enabled = false;
                    break;
            }
        }

        private void Carrega(TipoPagPeriodico _TipoSel)
        {
            TipoSel = _TipoSel;
            AjustaTela(StatusTela.limpa);
            dPagPerTipo.PeriodicosTableAdapter.Fill(dPagPerTipo.Periodicos, (int)TipoSel);
            int? nUltComp = dPagPerTipo.PeriodicosTableAdapter.UltimaComp((int)TipoSel);
            if (nUltComp.HasValue)
            {
                Competencia comp3 = new Competencia(nUltComp.Value);
                Competencia comp2 = comp3.CloneCompet(-1);
                Competencia comp1 = comp3.CloneCompet(-2);
                colH3.Caption = comp3.ToString();
                colH2.Caption = comp2.ToString();
                colH1.Caption = comp1.ToString();
                SortedList<int, DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn> Lista = new SortedList<int, DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn>();
                Lista.Add(comp1.CompetenciaBind, colH1);
                Lista.Add(comp2.CompetenciaBind, colH2);
                Lista.Add(comp3.CompetenciaBind, colH3);
                dPagPerTipo.HistoricoTableAdapter.Fill(dPagPerTipo.Historico, (int)TipoSel, comp1.CompetenciaBind);
                foreach (dPagPerTipo.PeriodicosRow row in dPagPerTipo.Periodicos)
                {
                    foreach (dPagPerTipo.HistoricoRow rowH in row.GetHistoricoRows())
                    {
                        row[Lista[rowH.NOACompet].FieldName] = rowH.Total;
                    }
                }
            }
        }

        private void bandedGridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            try
            {
                if (e.Column.Equals(colPGFProximaCompetencia))
                {
                    Competencia comp = new Competencia((int)e.Value);
                    e.DisplayText = comp.ToString();
                }
            }
            catch { }
        }

        private DataView DVFolha1;
        private DataView DVFolha2;

        private enum StatusLinha { NaoEncontrado=0, Incluirok=1,IncluirValorErrado=2,IncluirNova=3}

        private int PGFProvisorio = -1;

        private void simpleButton1_Click(object sender, System.EventArgs e)
        {
            if (DVFolha1 == null)
            {
                DVFolha1 = new DataView(dPagPerTipo.Periodicos);
                DVFolha1.Sort = colCONCodigoFolha1.FieldName;
                DVFolha2 = new DataView(dPagPerTipo.Periodicos);
                DVFolha2.Sort = colCONCodigoFolha2A.FieldName;
            }
            AbstratosNeon.ABS_DadosPer DadosPer = null;
            switch ((TipoPagPeriodico)radioGroup1.EditValue)
            {
                case TipoPagPeriodico.FolhaFGTS:
                    DadosPer = AbstratosNeon.ABS_DadosPer.GetDadosPer(TiposArquivo.FGTS);
                    break;
            }
            if (DadosPer == null)
                return;
            dPagPerTipo.PeriodicosRow rowPer;
            if (DadosPer.Carrega())
            {
                NovaCompetencia = new Competencia(DadosPer.Competencia.CompetenciaBind);
                colNova.Caption = string.Format("{0}", NovaCompetencia);

                foreach (AbstratosNeon.ABS_DadosPer.UnidadeResposta Un in DadosPer.Resultado.Values)
                {
                    rowPer = null;
                    int Indice = DVFolha1.Find(Un.Codigo);
                    if (Indice >= 0)
                        rowPer = (dPagPerTipo.PeriodicosRow)DVFolha1[Indice].Row;
                    else
                    {
                        Indice = DVFolha2.Find(Un.Codigo);
                        if (Indice >= 0)
                            rowPer = (dPagPerTipo.PeriodicosRow)DVFolha2[Indice].Row;
                    }


                    if (rowPer == null)
                    {
                        dCondominiosAtivos.CONDOMINIOSRow rowCON = dCondominiosAtivos.dCondominiosTodosST.BuscaPorCodFolha(Un.Codigo);
                        if (rowCON == null)
                        {
                            MessageBox.Show(string.Format("Código de condomínio não encontrado: {0}\r\nGuia ignorada", Un.Codigo));
                            continue;
                        }
                        if (rowCON.CON_EMP != DSCentral.EMP)
                        {
                            MessageBox.Show(string.Format("Condomínio de filial: {0} - {1} - {2}\r\nGuia ignorada", Un.Codigo, rowCON.CONCodigo, rowCON.CONNome));
                            continue;
                        }
                        rowPer = dPagPerTipo.Periodicos.NewPeriodicosRow();
                        rowPer.CON = rowCON.CON;
                        rowPer.CON_EMP = rowCON.CON_EMP;
                        rowPer.CONCodigo = rowCON.CONCodigo;
                        if (!rowCON.IsCONCodigoFolha1Null())
                            rowPer.CONCodigoFolha1 = rowCON.CONCodigoFolha1;
                        if (!rowCON.IsCONCodigoFolha2ANull())
                            rowPer.CONCodigoFolha2A = rowCON.CONCodigoFolha2A;
                        rowPer.CONNome = rowCON.CONNome;
                        rowPer.Status = (int)StatusLinha.IncluirNova;
                        rowPer.PGF = PGFProvisorio--;
                        rowPer.PGFValor = 0;
                        rowPer.PGFForma = (int)TipoSel;
                        rowPer.PGFStatus = (int)StatusPagamentoPeriodico.Inativo;
                        rowPer.PGFProximaCompetencia = NovaCompetencia.CompetenciaBind;
                        dPagPerTipo.Periodicos.AddPeriodicosRow(rowPer);
                    }
                    else
                    {                                                
                        rowPer.Status = (int)StatusLinha.Incluirok;
                    }
                    rowPer.Nova = Un.Valor;
                    rowPer.NovaVenc = Un.Vencimento;
                }
                AjustaTela(StatusTela.NovoCarregado);
            }
        }

        private void BCadastra_Click(object sender, System.EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.Espere("Gerando cheques");
                Esp.AtivaGauge(dPagPerTipo.Periodicos.Count);
                Application.DoEvents();
                int i = 0;
                DateTime prox = DateTime.Now.AddSeconds(3);
                foreach (dPagPerTipo.PeriodicosRow novarow in dPagPerTipo.Periodicos)
                {
                    i++;
                    if (DateTime.Now > prox)
                    {
                        prox = DateTime.Now.AddSeconds(3);
                        Esp.Gauge(i);
                    }
                    if (novarow.CON_EMP != Framework.DSCentral.EMP)
                        continue;
                    if (novarow.IsNovaNull())
                        continue;
                    //PagamentoPeriodico PagamentoPeriodico = new PagamentoPeriodico(novarow.CON, TipoSel);
                    PagamentoPeriodico PagamentoPeriodico = new PagamentoPeriodico(novarow.PGF);
                    if (!PagamentoPeriodico.Encontrado)
                        PagamentoPeriodico.CadastraPGF(novarow.CON,
                                                         TipoSel,
                                                         novarow.NovaVenc,
                                                         novarow.Nova,
                                                         NovaCompetencia);
                    if (PagamentoPeriodico.Status != StatusPagamentoPeriodico.Ativado)
                        PagamentoPeriodico.PGFrow.PGFStatus = (int)StatusPagamentoPeriodico.Ativado;
                    PagamentoPeriodico.CadastraProximoPagamento(novarow.NovaVenc, novarow.NovaVenc, novarow.Nova, NovaCompetencia);
                    //if (!CompontesBasicos.FormPrincipalBase.strEmProducao)
                    //    break;
                }
            }
            Carrega(TipoSel);
        }

        private void bandedGridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.Equals(bandedGridColumn1))
            {
                dPagPerTipo.PeriodicosRow row = (dPagPerTipo.PeriodicosRow)((GridView)sender).GetDataRow(e.RowHandle);
                if ((row != null) && (row.PGF > 0))
                    e.RepositoryItem = repositoryItemBotPGF;
            }

        }


        private void repositoryItemBotPGF_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            dPagPerTipo.PeriodicosRow row = (dPagPerTipo.PeriodicosRow)bandedGridView1.GetFocusedDataRow();
            if ((row == null) && (row.PGF > 0))
                return;     
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Periodicos, row.PGF, false, false);
        }
    }  
}
