using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Framework.objetosNeon;
using Framework;
using FrameworkProc.datasets;
using AbstratosNeon;
using CompontesBasicosProc;
using ContaPagarProc.Follow;
using dllClienteServProc;
using ContaPagarProc;
using VirEnumeracoesNeon;

namespace ContaPagar.Follow
{
    /// <summary>
    /// cPagamentosPeriodicos
    /// </summary>
    public partial class cPagamentosPeriodicos : Framework.ComponentesNeon.NavegadorCondominio
    {
        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup;

        /// <summary>
        /// Construtor
        /// </summary>
        public cPagamentosPeriodicos()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                BindingSource_F.DataSource = dPagamentosPeriodicos.dPagamentosPeriodicosSt;
                dFornecedoresLookup = new CadastrosProc.Fornecedores.dFornecedoresLookup();
                dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
                fornecedoresBindingSource.DataSource = dFornecedoresLookup;
                pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontasTableAdapter.Get25();
                UsuariobindingSource.DataSource = dUSUarios.dUSUariosSt;
                Enumeracoes.VirEnumTipoPagPeriodico.CarregaEditorDaGrid(colPGFForma);
                cPagamentosPeriodicosCampos.VirEnumStatusPagamentoPeriodico.CarregaEditorDaGrid(colPGFStatus);
                CODCONbinding.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST;
                GridView_F.OptionsBehavior.Editable = true;
            }
        }

        

        private void cPagamentosPeriodicos_CondominioAlterado(object sender, EventArgs e)
        {
            dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos.Clear();            
            RefreshDados();            
        }

        /// <summary>
        /// ExcluirRegistro
        /// </summary>
        protected override void ExcluirRegistro_F()
        {
            //Registrar no historioco 
            if (MessageBox.Show("Confirma","Confirma��o",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                LinhaMae_F.Delete();
                dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixosTableAdapter.Update(LinhaMae_F);
            }
        }

        private void Carregar(bool Forcar)
        {
            if ((dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos.Count != 0) && (!Forcar))
                return;
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                if (CON != -1)
                {
                    if (chAtivos.Checked)
                        dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixosTableAdapter.FillByCONnEncerrados(dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos, CON);
                    else
                        dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixosTableAdapter.FillByCON(dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos, CON);
                }
                else
                {
                    if (chAtivos.Checked)
                        //dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixosTableAdapter.FillPGFAtivos(dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos, Framework.DSCentral.EMP);
                        dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixosTableAdapter.FillAtivosRed(dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos, Framework.DSCentral.EMP);
                    else
                        dPagamentosPeriodicos.dPagamentosPeriodicosSt.CarregaTodos(false);
                }
            }
        }

        /// <summary>
        /// RefreshDados
        /// </summary>
        public override void RefreshDados()
        {
            Carregar(true);                        
        }

        private void GridView_F_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0)
                return;
            if ((e.Column == colPGFCompetI) || (e.Column == colPGFCompetF))
                if ((e.Value != null) && (e.Value != DBNull.Value))
                {
                    int iValor = (int)e.Value;
                    e.DisplayText = string.Format("{0:00}/{1:0000}", iValor % 100, iValor / 100);
                }
            if (e.Column == colPGFProximaCompetencia)
                if ((e.Value != null) && (e.Value != DBNull.Value))
                {
                    //dNOtAs.PaGamentosFixosRow row = ((ContaPagar.dNOtAs)gridControl1.DataSource).PaGamentosFixos[e.ListSourceRowIndex];
                    dPagamentosPeriodicos.PaGamentosFixosRow row = ((dPagamentosPeriodicos)BindingSource_F.DataSource).PaGamentosFixos[e.ListSourceRowIndex];
                    if (row == null)
                        return;
                    StatusPagamentoPeriodico status = (StatusPagamentoPeriodico)row.PGFStatus;
                    if ((status == StatusPagamentoPeriodico.Ativado) || (status == StatusPagamentoPeriodico.AtivadoSemDOC))
                    {
                        int iValor = (int)e.Value;
                        e.DisplayText = string.Format("{0:00}/{1:0000}", iValor % 100, iValor / 100);
                    }
                    else
                        e.DisplayText = " - ";
                }
            if (e.Column == gridColumn1)                
                {
                    dPagamentosPeriodicos.PaGamentosFixosRow row = ((dPagamentosPeriodicos)BindingSource_F.DataSource).PaGamentosFixos[e.ListSourceRowIndex];
                    if (row == null)
                        return;
                    StatusPagamentoPeriodico status = (StatusPagamentoPeriodico)row.PGFStatus;
                    if ((status == StatusPagamentoPeriodico.Ativado) || (status == StatusPagamentoPeriodico.AtivadoSemDOC))
                    {
                        Competencia compRef = new Competencia(row.PGFProximaCompetencia).Add(row.PGFRef);
                        e.DisplayText = string.Format("{0:dd/MM/yyyy}", compRef.DataNaCompetencia(row.PGFDia, Competencia.Regime.mes));
                    }
                    else
                        e.DisplayText = " - ";
                }
            
        }

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if ((e.Column == colPGFStatus) && (e.CellValue != null) && (e.CellValue != DBNull.Value))
            {
                StatusPagamentoPeriodico status = (StatusPagamentoPeriodico)e.CellValue;
                switch (status)
                {
                    case StatusPagamentoPeriodico.Inativo:
                        e.Appearance.BackColor = Color.White;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusPagamentoPeriodico.AtivadoSemDOC:
                        e.Appearance.BackColor = Color.Yellow;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusPagamentoPeriodico.Ativado:
                        e.Appearance.BackColor = Color.Lime;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusPagamentoPeriodico.Encerrado:
                        e.Appearance.BackColor = Color.Gray;
                        e.Appearance.ForeColor = Color.White;
                        break;
                    default:
                        e.Appearance.BackColor = Color.Black;
                        e.Appearance.ForeColor = Color.White;
                        break;
                }
            }
        }

        /*
        private void CorrecaoEmergencial()
        {            
            foreach (int sels in GridView_F.GetSelectedRows())
            {                                
                dPagamentosPeriodicos.PaGamentosFixosRow PAGrow = (dPagamentosPeriodicos.PaGamentosFixosRow)GridView_F.GetDataRow(sels);                
                PagamentoPeriodico PagPer = new PagamentoPeriodico(PAGrow.PGF);
                Console.WriteLine(string.Format("PAG {0} : {1}", PagPer.PGF, PagPer.CorrecaoEmegencial()));
            }
        }*/

        private string URLServidorProcessos
        {
            get
            {
                switch (ClienteServProc.TipoServidoSelecionado)
                {
                    case TiposServidorProc.Local:
                        return "WCFPeriodicoNotaL";
                    case TiposServidorProc.QAS:
                        return "WCFPeriodicoNotaH";
                    case TiposServidorProc.Producao:
                        return "WCFPeriodicoNotaP";
                    case TiposServidorProc.Indefinido:
                    case TiposServidorProc.Simulador:
                    default:
                        return "";
                }
            }
        }

        private RetornoServidorProcessos Simulaperiodico_CadastrarProximo(int PGF)
        {
            RetornoServidorProcessos Retorno = new RetornoServidorProcessos("OK","OK");
            try
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = true;
                FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Local, DSCentral.USU);
                PagamentoPeriodicoProc periodico = new PagamentoPeriodicoProc(PGF, false, EMPTProc1);
                if (periodico.Encontrado)
                {
                    if (periodico.CadastrarProximo())
                        Retorno.Mensagem = string.Format("Retorno ok Descri��o{0}", periodico.Descricao);
                    else
                        Retorno.Mensagem = string.Format("Retorno branco. Descri��o{0}", periodico.Descricao);
                }
                else
                {
                    Retorno.ok = false;
                    Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
                    Retorno.Mensagem = string.Format("Peri�dico n�o encontrado PGF = {0}", PGF);
                }
                return Retorno;
            }            
            finally
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = false;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU != 30)
                return;
            string Relatorio = "";
            string URL = URLServidorProcessos;
            SPeriodicoNota.PeriodicoNotaClient PeriodicoNotaClient1 = null;
            if (ClienteServProc.TipoServidoSelecionado != TiposServidorProc.Simulador)
                PeriodicoNotaClient1 = new SPeriodicoNota.PeriodicoNotaClient(URL);
            DateTime Referencia = DateTime.Now.AddSeconds(20);
            foreach (int sels in GridView_F.GetSelectedRows())
            {
                if (DateTime.Now > Referencia)
                {
                    Clipboard.SetText(Relatorio);
                    Console.Beep();
                    Referencia = DateTime.Now.AddSeconds(20);
                }
                dPagamentosPeriodicos.PaGamentosFixosRow PAGrow = (dPagamentosPeriodicos.PaGamentosFixosRow)GridView_F.GetDataRow(sels);
                try
                {
                    RetornoServidorProcessos Ret;
                    if (ClienteServProc.TipoServidoSelecionado == TiposServidorProc.Simulador)
                        Ret = Simulaperiodico_CadastrarProximo(PAGrow.PGF);
                    else
                        Ret = PeriodicoNotaClient1.periodico_CadastrarProximo(VirCrip.VirCripWS.Crip(DSCentral.ChaveCriptografar(DSCentral.EMP, DSCentral.USU)), PAGrow.PGF);
                    if (Ret.ok)
                    {
                        if (Ret.Mensagem.StartsWith("Retorno ok"))
                        {                            
                            Relatorio += string.Format("\r\nAjustado PGF = {0}", PAGrow.PGF);
                        }
                        else
                        {
                            Relatorio += string.Format("\r\nOk PGF = {0}", PAGrow.PGF);
                        }
                    }
                    else
                    {                        
                        Relatorio += string.Format("\r\nPGF = {0} ****Erro****\r\n{1}\r\n", PAGrow.PGF, Ret.Mensagem);
                    }
                }
                catch (Exception ex)
                {                    
                    Relatorio += string.Format("\r\nWS PGF = {0} ****Erro****\r\n{1}\r\n", PAGrow.PGF, VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(ex, false, true, false));
                }
            }
            Clipboard.SetText(Relatorio);
            MessageBox.Show(Relatorio);

            return;

            //CorrecaoEmergencial();
            //return;
            MessageBox.Show("Simula servidor de processos");
            VirMSSQL.TableAdapter.Servidor_De_Processos = true;

            //ContaPagarProc.PagamentoPeriodicoProc PagamentoPeriodicoProc = new ContaPagarProc.PagamentoPeriodicoProc(2382, FrameworkProc.EMPTProc.EMPTipo.Filial, 30,"Virtual");
            int i0 = 0;
            VirInput.Input.Execute("inicio", out i0);
            for (int i = i0; ; i++)
            {
                ContaPagarProc.PagamentoPeriodicoProc PagamentoPeriodicoProc = new ContaPagarProc.PagamentoPeriodicoProc(i, false, new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Local, 30));
                MessageBox.Show(PagamentoPeriodicoProc.CadastrarProximo().ToString());
                PagamentoPeriodicoProc = new ContaPagarProc.PagamentoPeriodicoProc(i, false, new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Filial, 30));
                MessageBox.Show(PagamentoPeriodicoProc.CadastrarProximo().ToString());
                if (MessageBox.Show("Parar", "Parar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    break;
            } 
            /* 
            string buscaerros =
"SELECT        CHE\r\n" +
"FROM            CHEques INNER JOIN\r\n" +
"                         PAGamentos ON CHEques.CHE = PAGamentos.PAG_CHE\r\n" +
"WHERE        (CHEques.CHETipo = 8) AND (CHEques.CHEStatus = 20)\r\n" +
"GROUP BY CHE\r\n" +
"HAVING        (COUNT(PAG) > 1);";
            System.Data.DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(buscaerros);
            int Total = DT.Rows.Count;
            int i = 0;
            foreach (DataRow DR in DT.Rows)
            {
                labelControl1.Text = string.Format("Corre��o {0}/{1}", i++, Total);
                Application.DoEvents();
                dllCheques.Cheque Cheque = new dllCheques.Cheque((int)DR[0],dllCheques.Cheque.TipoChave.CHE);
                Cheque.DividirporPAG();
            }
            labelControl1.Text = string.Format("Corre��o {0}/{1} Fim", i, Total);

            int total = GridView_F.GetSelectedRows().Length;

            */

            /*
            if (total <= 1)
            {
                //correcao de erro
                string comandocor1 =
    "SELECT        NOA, NOA_PGF\r\n" +
    "FROM            NOtAs\r\n" +
    "WHERE        (NOAStatus = 5);";

                string comandocor2 =
    "SELECT   top 1     NOA\r\n" +
    "FROM            NOtAs\r\n" +
    "WHERE        NOA_PGF = @P1\r\n" +
    "ORDER BY NOACompet DESC;";

                string comandocor3 =
    "SELECT        PAG, PAG_CHE\r\n" +
    "FROM            PAGamentos\r\n" +
    "WHERE        (PAG_NOA = @P1);";

                DataTable DTcandidatos = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comandocor1);
                int Testar = DTcandidatos.Rows.Count;
                int contador = 0;
                int nApagados = 0;
                foreach (DataRow DR in DTcandidatos.Rows)
                {
                    contador++;
                    labelControl1.Text = string.Format("Corre��o {0}/{1} ap {2}", contador, Testar, nApagados);
                    Application.DoEvents();
                    int PGF = (int)DR["NOA_PGF"];
                    int NOA = (int)DR["NOA"];
                    int? UltimoNOA = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(comandocor2, PGF);
                    if (UltimoNOA != NOA)
                    {                        
                        nApagados++;
                        DataTable DTapagar = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comandocor3, NOA);
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("XX");
                            foreach (DataRow DR1 in DTapagar.Rows)
                            {
                                int PAG = (int)DR1["PAG"];
                                int CHE = (int)DR1["PAG_CHE"];
                                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from PAGamentos where PAG = @P1", PAG);
                                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from CHEQUES where CHE = @P1", CHE);
                            };
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from NOtAs where NOA = @P1", NOA);
                            VirMSSQL.TableAdapter.CommitSQL();
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.VircatchSQL(ex);
                        }
                    }
                }
                labelControl1.Text = string.Format("Fim Corre��o {0} ap {1}", Testar, nApagados);
                return;
            }
            */
            String resultado = "";
            int ok = 0;
            int cad = 0;

            int total = 0;
            foreach (int sels in GridView_F.GetSelectedRows())
            {
                //ContaPagarProc.PagamentoPeriodicoProc PagamentoPeriodicoProc = new ContaPagarProc.PagamentoPeriodicoProc(PAGrow.PGF,FrameworkProc.EMPTProc.EMPTipo.Filial


                labelControl1.Text = string.Format("ok {0} cad {1} - {2}/{3}",ok,cad,ok+cad,total);
                Application.DoEvents();
                dPagamentosPeriodicos.PaGamentosFixosRow PAGrow = (dPagamentosPeriodicos.PaGamentosFixosRow)GridView_F.GetDataRow(sels);
                resultado += FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(PAGrow.PGF_CON).CONCodigo;
                resultado += string.Format("\t{0}", PAGrow.PGFDescricao); 
                PagamentoPeriodico PagPer = new PagamentoPeriodico(PAGrow.PGF);
                bool foicad = false;
                
                if(PagPer.Tipo.EstaNoGrupo(TipoPagPeriodico.Cheque,
                                           TipoPagPeriodico.ChequeSindico,
                                           TipoPagPeriodico.Boleto,
                                           TipoPagPeriodico.DebitoAutomaticoConta,
                                           TipoPagPeriodico.Conta))                                
                {
                    try
                    {
                        foicad = PagPer.CadastrarProximo();
                    }
                    catch
                    {
                        foicad = false;
                    }
                }
                if (foicad)
                    cad++;
                else
                    ok++;
                if (PagPer.Tipo == TipoPagPeriodico.Cheque)
                    resultado += string.Format("\tc\t{0}\r\n", foicad);
                else if (PagPer.Tipo == TipoPagPeriodico.ChequeSindico)
                    resultado += string.Format("\ts\t{0}\r\n", foicad);
                else if (PagPer.Tipo == TipoPagPeriodico.Boleto)
                    resultado += string.Format("\tb\t{0}\r\n", foicad);
                else if (PagPer.Tipo == TipoPagPeriodico.Conta)
                    resultado += string.Format("\tb\t{0}\r\n", foicad);
                else
                    resultado += string.Format("\t-\t{0}\r\n", "PULADO");
                //if (PagPer.Converte())                    
                //    resultado +=  string.Format("\t{0}\t{1}\r\n", PagPer.competenciaerrada,PagPer.agenda);                    
                //else
                //    resultado += string.Format("\tPULADO\r\n");
            }
            if (resultado != "")
                Clipboard.SetText(resultado);
            //foreach (dPagamentosPeriodicos.PaGamentosFixosRow PAGrow in dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos)
            //foreach (dPagamentosPeriodicos.PaGamentosFixosRow PAGrow in dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos)
            //{
               // if (((StatusPagamentoPeriodico)PAGrow.PGFStatus == StatusPagamentoPeriodico.Ativado) || ((StatusPagamentoPeriodico)PAGrow.PGFStatus == StatusPagamentoPeriodico.AtivadoSemDOC))
              //  {
              //      PagamentoPeriodico Pa = new PagamentoPeriodico(PAGrow.PGF);
             //       Pa.CadastrarProximo();
             //   }
            //}
            MessageBox.Show("ok");
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            AbreCampos(false, "Altera��o");            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Carregar(true);
        }
        
    }
}
