namespace ContaPagar.Follow
{
    /// <summary>
    /// cPagamentosPeriodicos
    /// </summary>
    partial class cPagamentosPeriodicos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cPagamentosPeriodicos));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.colPGF_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookFornecedor = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.fornecedoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPGFCompetF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFCompetI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFDia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFForma = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPGFNominal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.UsuariobindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPGFDATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPGF_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpCODCON = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.CODCONbinding = new System.Windows.Forms.BindingSource(this.components);
            this.colPGFCompetReajuste = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFDataLimiteRes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFINSSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFINSST = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFIR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFPISCOF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPGFDadosPag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFProximaCompetencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFSalarios = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFCodigoDebito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFIdentificacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGF_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFRef = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.chAtivos = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookFornecedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariobindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCODCON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CODCONbinding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAtivos.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chAtivos);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Size = new System.Drawing.Size(1519, 50);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton1, 0);
            this.panelControl1.Controls.SetChildIndex(this.labelControl1, 0);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton2, 0);
            this.panelControl1.Controls.SetChildIndex(this.chAtivos, 0);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookFornecedor,
            this.repositoryItemImageComboBox1,
            this.LookUpUSU,
            this.LookUpCODCON,
            this.repositoryItemImageComboBox2,
            this.repositoryItemButtonEdit1});
            this.GridControl_F.ShowOnlyPredefinedDetails = true;
            this.GridControl_F.Size = new System.Drawing.Size(1519, 697);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseBorderColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPGF_FRN,
            this.colPGFCompetF,
            this.colPGFCompetI,
            this.colPGFDescricao,
            this.colPGFDia,
            this.colPGFForma,
            this.colPGFNominal,
            this.colPGFValor,
            this.colPGFA_USU,
            this.colPGFDATAA,
            this.colPGFDATAI,
            this.colPGFI_USU,
            this.colPGF_CON,
            this.colPGFCompetReajuste,
            this.colPGFDataLimiteRes,
            this.colPGFINSSP,
            this.colPGFINSST,
            this.colPGFIR,
            this.colPGFISS,
            this.colPGFPISCOF,
            this.colPGFStatus,
            this.colPGFDadosPag,
            this.colPGFProximaCompetencia,
            this.colPGFSalarios,
            this.colPGFCodigoDebito,
            this.gridColumn1,
            this.colPGFIdentificacao,
            this.colPGF_PLA,
            this.colPGFRef,
            this.colPGF,
            this.gridColumn2});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.MultiSelect = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPGFDia, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridView_F_RowCellStyle);
            this.GridView_F.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.GridView_F_CustomColumnDisplayText);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "PaGamentosFixos";
            this.BindingSource_F.DataSource = typeof(ContaPagarProc.Follow.dPagamentosPeriodicos);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // colPGF_FRN
            // 
            this.colPGF_FRN.Caption = "Fornecedor";
            this.colPGF_FRN.ColumnEdit = this.LookFornecedor;
            this.colPGF_FRN.FieldName = "PGF_FRN";
            this.colPGF_FRN.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPGF_FRN.Name = "colPGF_FRN";
            this.colPGF_FRN.OptionsColumn.ReadOnly = true;
            this.colPGF_FRN.Visible = true;
            this.colPGF_FRN.VisibleIndex = 5;
            this.colPGF_FRN.Width = 137;
            // 
            // LookFornecedor
            // 
            this.LookFornecedor.AutoHeight = false;
            this.LookFornecedor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookFornecedor.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "FRN Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookFornecedor.DataSource = this.fornecedoresBindingSource;
            this.LookFornecedor.DisplayMember = "FRNNome";
            this.LookFornecedor.Name = "LookFornecedor";
            this.LookFornecedor.NullText = "--  N�o Cadastrado  --";
            this.LookFornecedor.ValueMember = "FRN";
            // 
            // fornecedoresBindingSource
            // 
            this.fornecedoresBindingSource.DataMember = "FRNLookup";
            this.fornecedoresBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // colPGFCompetF
            // 
            this.colPGFCompetF.Caption = "At�";
            this.colPGFCompetF.FieldName = "PGFCompetF";
            this.colPGFCompetF.Name = "colPGFCompetF";
            this.colPGFCompetF.OptionsColumn.ReadOnly = true;
            this.colPGFCompetF.Visible = true;
            this.colPGFCompetF.VisibleIndex = 7;
            this.colPGFCompetF.Width = 59;
            // 
            // colPGFCompetI
            // 
            this.colPGFCompetI.Caption = "De";
            this.colPGFCompetI.FieldName = "PGFCompetI";
            this.colPGFCompetI.Name = "colPGFCompetI";
            this.colPGFCompetI.OptionsColumn.ReadOnly = true;
            this.colPGFCompetI.Visible = true;
            this.colPGFCompetI.VisibleIndex = 6;
            this.colPGFCompetI.Width = 62;
            // 
            // colPGFDescricao
            // 
            this.colPGFDescricao.Caption = "Descri��o";
            this.colPGFDescricao.FieldName = "PGFDescricao";
            this.colPGFDescricao.Name = "colPGFDescricao";
            this.colPGFDescricao.OptionsColumn.ReadOnly = true;
            this.colPGFDescricao.Visible = true;
            this.colPGFDescricao.VisibleIndex = 2;
            this.colPGFDescricao.Width = 154;
            // 
            // colPGFDia
            // 
            this.colPGFDia.Caption = "Dia";
            this.colPGFDia.FieldName = "PGFDia";
            this.colPGFDia.Name = "colPGFDia";
            this.colPGFDia.OptionsColumn.AllowEdit = false;
            this.colPGFDia.OptionsColumn.ReadOnly = true;
            this.colPGFDia.Visible = true;
            this.colPGFDia.VisibleIndex = 1;
            this.colPGFDia.Width = 38;
            // 
            // colPGFForma
            // 
            this.colPGFForma.Caption = "Tipo";
            this.colPGFForma.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colPGFForma.FieldName = "PGFForma";
            this.colPGFForma.Name = "colPGFForma";
            this.colPGFForma.OptionsColumn.ReadOnly = true;
            this.colPGFForma.Visible = true;
            this.colPGFForma.VisibleIndex = 10;
            this.colPGFForma.Width = 87;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colPGFNominal
            // 
            this.colPGFNominal.Caption = "Nominal";
            this.colPGFNominal.FieldName = "PGFNominal";
            this.colPGFNominal.Name = "colPGFNominal";
            this.colPGFNominal.OptionsColumn.ReadOnly = true;
            this.colPGFNominal.Width = 350;
            // 
            // colPGFValor
            // 
            this.colPGFValor.Caption = "Valor";
            this.colPGFValor.DisplayFormat.FormatString = "n2";
            this.colPGFValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPGFValor.FieldName = "PGFValor";
            this.colPGFValor.Name = "colPGFValor";
            this.colPGFValor.OptionsColumn.ReadOnly = true;
            this.colPGFValor.Visible = true;
            this.colPGFValor.VisibleIndex = 11;
            this.colPGFValor.Width = 82;
            // 
            // colPGFA_USU
            // 
            this.colPGFA_USU.Caption = "Altera��o";
            this.colPGFA_USU.ColumnEdit = this.LookUpUSU;
            this.colPGFA_USU.CustomizationCaption = "Usu�rio Altera��o";
            this.colPGFA_USU.FieldName = "PGFA_USU";
            this.colPGFA_USU.Name = "colPGFA_USU";
            this.colPGFA_USU.OptionsColumn.ReadOnly = true;
            // 
            // LookUpUSU
            // 
            this.LookUpUSU.AutoHeight = false;
            this.LookUpUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpUSU.DataSource = this.UsuariobindingSource;
            this.LookUpUSU.DisplayMember = "USUNome";
            this.LookUpUSU.Name = "LookUpUSU";
            this.LookUpUSU.ValueMember = "USU";
            // 
            // UsuariobindingSource
            // 
            this.UsuariobindingSource.DataMember = "USUarios";
            this.UsuariobindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colPGFDATAA
            // 
            this.colPGFDATAA.Caption = "Altera��o";
            this.colPGFDATAA.CustomizationCaption = "Data Altera��o";
            this.colPGFDATAA.FieldName = "PGFDATAA";
            this.colPGFDATAA.Name = "colPGFDATAA";
            this.colPGFDATAA.OptionsColumn.ReadOnly = true;
            // 
            // colPGFDATAI
            // 
            this.colPGFDATAI.Caption = "Cadastro";
            this.colPGFDATAI.CustomizationCaption = "Data de Cadastro";
            this.colPGFDATAI.FieldName = "PGFDATAI";
            this.colPGFDATAI.Name = "colPGFDATAI";
            this.colPGFDATAI.OptionsColumn.ReadOnly = true;
            // 
            // colPGFI_USU
            // 
            this.colPGFI_USU.Caption = "Cadastro";
            this.colPGFI_USU.ColumnEdit = this.LookUpUSU;
            this.colPGFI_USU.CustomizationCaption = "Usu�rio Cadastro";
            this.colPGFI_USU.FieldName = "PGFI_USU";
            this.colPGFI_USU.Name = "colPGFI_USU";
            this.colPGFI_USU.OptionsColumn.ReadOnly = true;
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colPGF_CON
            // 
            this.colPGF_CON.Caption = "CODCON";
            this.colPGF_CON.ColumnEdit = this.LookUpCODCON;
            this.colPGF_CON.FieldName = "PGF_CON";
            this.colPGF_CON.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPGF_CON.Name = "colPGF_CON";
            this.colPGF_CON.OptionsColumn.ReadOnly = true;
            this.colPGF_CON.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.colPGF_CON.Visible = true;
            this.colPGF_CON.VisibleIndex = 0;
            this.colPGF_CON.Width = 74;
            // 
            // LookUpCODCON
            // 
            this.LookUpCODCON.AutoHeight = false;
            this.LookUpCODCON.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpCODCON.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CON Codigo", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CON Nome", 160, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpCODCON.DataSource = this.CODCONbinding;
            this.LookUpCODCON.DisplayMember = "CONCodigo";
            this.LookUpCODCON.Name = "LookUpCODCON";
            this.LookUpCODCON.ShowHeader = false;
            this.LookUpCODCON.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.LookUpCODCON.ValueMember = "CON";
            // 
            // CODCONbinding
            // 
            this.CODCONbinding.DataMember = "CONDOMINIOS";
            this.CODCONbinding.DataSource = typeof(FrameworkProc.datasets.dCondominiosAtivos);
            // 
            // colPGFCompetReajuste
            // 
            this.colPGFCompetReajuste.Caption = "Com Reajuste";
            this.colPGFCompetReajuste.FieldName = "PGFCompetReajuste";
            this.colPGFCompetReajuste.Name = "colPGFCompetReajuste";
            this.colPGFCompetReajuste.OptionsColumn.ReadOnly = true;
            // 
            // colPGFDataLimiteRes
            // 
            this.colPGFDataLimiteRes.Caption = "Data Limite Reajuste";
            this.colPGFDataLimiteRes.FieldName = "PGFDataLimiteRes";
            this.colPGFDataLimiteRes.Name = "colPGFDataLimiteRes";
            this.colPGFDataLimiteRes.OptionsColumn.ReadOnly = true;
            // 
            // colPGFINSSP
            // 
            this.colPGFINSSP.Caption = "INSS Prestador";
            this.colPGFINSSP.DisplayFormat.FormatString = "n2";
            this.colPGFINSSP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPGFINSSP.FieldName = "PGFINSSP";
            this.colPGFINSSP.Name = "colPGFINSSP";
            this.colPGFINSSP.OptionsColumn.ReadOnly = true;
            // 
            // colPGFINSST
            // 
            this.colPGFINSST.Caption = "INSS Tomador";
            this.colPGFINSST.DisplayFormat.FormatString = "n2";
            this.colPGFINSST.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPGFINSST.FieldName = "PGFINSST";
            this.colPGFINSST.Name = "colPGFINSST";
            this.colPGFINSST.OptionsColumn.ReadOnly = true;
            // 
            // colPGFIR
            // 
            this.colPGFIR.Caption = "IR";
            this.colPGFIR.DisplayFormat.FormatString = "n2";
            this.colPGFIR.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPGFIR.FieldName = "PGFIR";
            this.colPGFIR.Name = "colPGFIR";
            this.colPGFIR.OptionsColumn.ReadOnly = true;
            // 
            // colPGFISS
            // 
            this.colPGFISS.Caption = "ISS";
            this.colPGFISS.DisplayFormat.FormatString = "n2";
            this.colPGFISS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPGFISS.FieldName = "PGFISS";
            this.colPGFISS.Name = "colPGFISS";
            this.colPGFISS.OptionsColumn.ReadOnly = true;
            // 
            // colPGFPISCOF
            // 
            this.colPGFPISCOF.Caption = "PIS/COFINS/CSLL";
            this.colPGFPISCOF.FieldName = "PGFPISCOF";
            this.colPGFPISCOF.Name = "colPGFPISCOF";
            this.colPGFPISCOF.OptionsColumn.ReadOnly = true;
            // 
            // colPGFStatus
            // 
            this.colPGFStatus.Caption = "Status";
            this.colPGFStatus.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colPGFStatus.FieldName = "PGFStatus";
            this.colPGFStatus.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colPGFStatus.Name = "colPGFStatus";
            this.colPGFStatus.OptionsColumn.ReadOnly = true;
            this.colPGFStatus.Visible = true;
            this.colPGFStatus.VisibleIndex = 12;
            this.colPGFStatus.Width = 90;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colPGFDadosPag
            // 
            this.colPGFDadosPag.Caption = "Dados Dep�sito";
            this.colPGFDadosPag.FieldName = "PGFDadosPag";
            this.colPGFDadosPag.Name = "colPGFDadosPag";
            this.colPGFDadosPag.OptionsColumn.ReadOnly = true;
            // 
            // colPGFProximaCompetencia
            // 
            this.colPGFProximaCompetencia.Caption = "Pr�ximo";
            this.colPGFProximaCompetencia.FieldName = "PGFProximaCompetencia";
            this.colPGFProximaCompetencia.Name = "colPGFProximaCompetencia";
            this.colPGFProximaCompetencia.OptionsColumn.ReadOnly = true;
            this.colPGFProximaCompetencia.Visible = true;
            this.colPGFProximaCompetencia.VisibleIndex = 8;
            this.colPGFProximaCompetencia.Width = 62;
            // 
            // colPGFSalarios
            // 
            this.colPGFSalarios.Caption = "Sal�rios";
            this.colPGFSalarios.FieldName = "PGFSalarios";
            this.colPGFSalarios.Name = "colPGFSalarios";
            this.colPGFSalarios.OptionsColumn.ReadOnly = true;
            // 
            // colPGFCodigoDebito
            // 
            this.colPGFCodigoDebito.Caption = "C�digo";
            this.colPGFCodigoDebito.FieldName = "PGFCodigoDebito";
            this.colPGFCodigoDebito.Name = "colPGFCodigoDebito";
            this.colPGFCodigoDebito.OptionsColumn.ReadOnly = true;
            this.colPGFCodigoDebito.Visible = true;
            this.colPGFCodigoDebito.VisibleIndex = 4;
            this.colPGFCodigoDebito.Width = 84;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Data";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 9;
            // 
            // colPGFIdentificacao
            // 
            this.colPGFIdentificacao.Caption = "Identifica��o";
            this.colPGFIdentificacao.FieldName = "PGFIdentificacao";
            this.colPGFIdentificacao.Name = "colPGFIdentificacao";
            this.colPGFIdentificacao.OptionsColumn.ReadOnly = true;
            this.colPGFIdentificacao.Visible = true;
            this.colPGFIdentificacao.VisibleIndex = 3;
            this.colPGFIdentificacao.Width = 94;
            // 
            // colPGF_PLA
            // 
            this.colPGF_PLA.FieldName = "PGF_PLA";
            this.colPGF_PLA.Name = "colPGF_PLA";
            this.colPGF_PLA.OptionsColumn.ReadOnly = true;
            // 
            // colPGFRef
            // 
            this.colPGFRef.FieldName = "PGFRef";
            this.colPGFRef.Name = "colPGFRef";
            this.colPGFRef.OptionsColumn.ReadOnly = true;
            // 
            // colPGF
            // 
            this.colPGF.FieldName = "PGF";
            this.colPGF.Name = "colPGF";
            this.colPGF.OptionsColumn.ReadOnly = true;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(1232, 13);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(106, 19);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Teste Periodico";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(1162, 19);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(8, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "--";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 13;
            this.gridColumn2.Width = 30;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.Image = global::ContaPagar.Properties.Resources.perBr1;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions1, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(334, 15);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "Calcular";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // chAtivos
            // 
            this.chAtivos.EditValue = true;
            this.chAtivos.Location = new System.Drawing.Point(415, 17);
            this.chAtivos.MenuManager = this.BarManager_F;
            this.chAtivos.Name = "chAtivos";
            this.chAtivos.Properties.Caption = "Somente Ativos";
            this.chAtivos.Size = new System.Drawing.Size(404, 19);
            this.chAtivos.TabIndex = 4;
            // 
            // cPagamentosPeriodicos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BindingSourcePrincipal = this.BindingSource_F;
            this.ComponenteCampos = typeof(ContaPagar.Follow.cPagamentosPeriodicosCampos);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cPagamentosPeriodicos";
            this.NivelCONOculto = 1;
            this.Size = new System.Drawing.Size(1519, 807);
            this.CondominioAlterado += new System.EventHandler(this.cPagamentosPeriodicos_CondominioAlterado);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookFornecedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariobindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCODCON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CODCONbinding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAtivos.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colPGF_FRN;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFCompetF;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFCompetI;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFDia;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFForma;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFNominal;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFA_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFDATAA;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFI_USU;
        private System.Windows.Forms.BindingSource fornecedoresBindingSource;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookFornecedor;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private System.Windows.Forms.BindingSource UsuariobindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpUSU;
        private DevExpress.XtraGrid.Columns.GridColumn colPGF_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFCompetReajuste;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFDataLimiteRes;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFINSSP;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFINSST;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFIR;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFISS;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFPISCOF;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpCODCON;
        private System.Windows.Forms.BindingSource CODCONbinding;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFDadosPag;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFProximaCompetencia;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFSalarios;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFCodigoDebito;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFIdentificacao;
        private DevExpress.XtraGrid.Columns.GridColumn colPGF_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFRef;
        private DevExpress.XtraGrid.Columns.GridColumn colPGF;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.CheckEdit chAtivos;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}
