
using CompontesBasicos;
using VirEnumeracoesNeon;

namespace ContaPagar.Follow
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cEditaDOC : ComponenteBaseDialog
    {
        /// <summary>
        /// construtor
        /// </summary>
        public cEditaDOC()
        {
            InitializeComponent();
            Follow.cPagamentosPeriodicosCampos.VirEnumTiposDOC.CarregaEditorDaGrid(XTipo);
            XTipo.EditValue = (int)TiposDOC.Descritivo;
        }

        private void XArquivo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                XArquivo.Text = openFileDialog1.FileName;
        }
    }
}
