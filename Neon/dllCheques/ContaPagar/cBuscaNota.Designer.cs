﻿namespace ContaPagar
{
    partial class cBuscaNota
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cBuscaNota));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ButSegue = new DevExpress.XtraEditors.SimpleButton();
            this.TELink = new DevExpress.XtraEditors.MemoEdit();
            this.SEVerificacao = new DevExpress.XtraEditors.TextEdit();
            this.spinEditSP = new DevExpress.XtraEditors.SpinEdit();
            this.labelSP = new DevExpress.XtraEditors.LabelControl();
            this.ButImp = new DevExpress.XtraEditors.SimpleButton();
            this.SBBusca = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.SENota = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.RGPrefeitura = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TELink.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SEVerificacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SENota.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RGPrefeitura.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.ButSegue);
            this.panelControl1.Controls.Add(this.TELink);
            this.panelControl1.Controls.Add(this.SEVerificacao);
            this.panelControl1.Controls.Add(this.spinEditSP);
            this.panelControl1.Controls.Add(this.labelSP);
            this.panelControl1.Controls.Add(this.ButImp);
            this.panelControl1.Controls.Add(this.SBBusca);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.SENota);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.RGPrefeitura);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(946, 246);
            this.panelControl1.TabIndex = 0;
            // 
            // ButSegue
            // 
            this.ButSegue.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButSegue.Appearance.Options.UseFont = true;
            this.ButSegue.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButSegue.ImageOptions.Image")));
            this.ButSegue.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.ButSegue.Location = new System.Drawing.Point(869, 150);
            this.ButSegue.Name = "ButSegue";
            this.ButSegue.Size = new System.Drawing.Size(139, 85);
            this.ButSegue.TabIndex = 15;
            this.ButSegue.Text = "Cadastra Nota";
            this.ButSegue.Visible = false;
            this.ButSegue.Click += new System.EventHandler(this.ButSegue_Click);
            // 
            // TELink
            // 
            this.TELink.Location = new System.Drawing.Point(124, 149);
            this.TELink.Name = "TELink";
            this.TELink.Size = new System.Drawing.Size(739, 86);
            this.TELink.TabIndex = 14;
            this.TELink.EditValueChanged += new System.EventHandler(this.memoEdit1_EditValueChanged);
            // 
            // SEVerificacao
            // 
            this.SEVerificacao.Location = new System.Drawing.Point(527, 58);
            this.SEVerificacao.Name = "SEVerificacao";
            this.SEVerificacao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SEVerificacao.Properties.Appearance.Options.UseFont = true;
            this.SEVerificacao.Size = new System.Drawing.Size(186, 30);
            this.SEVerificacao.TabIndex = 13;
            this.SEVerificacao.EditValueChanged += new System.EventHandler(this.textEdit1_EditValueChanged_1);
            // 
            // spinEditSP
            // 
            this.spinEditSP.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditSP.Location = new System.Drawing.Point(527, 105);
            this.spinEditSP.Name = "spinEditSP";
            this.spinEditSP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEditSP.Properties.Appearance.Options.UseFont = true;
            this.spinEditSP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.spinEditSP.Properties.IsFloatValue = false;
            this.spinEditSP.Properties.Mask.EditMask = "N00";
            this.spinEditSP.Size = new System.Drawing.Size(186, 30);
            this.spinEditSP.TabIndex = 12;
            this.spinEditSP.Visible = false;
            this.spinEditSP.EditValueChanged += new System.EventHandler(this.spinEditSP_EditValueChanged);
            // 
            // labelSP
            // 
            this.labelSP.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSP.Appearance.Options.UseFont = true;
            this.labelSP.Location = new System.Drawing.Point(395, 108);
            this.labelSP.Name = "labelSP";
            this.labelSP.Size = new System.Drawing.Size(101, 23);
            this.labelSP.TabIndex = 11;
            this.labelSP.Text = "Insc Mun.:";
            this.labelSP.Visible = false;
            // 
            // ButImp
            // 
            this.ButImp.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButImp.Appearance.Options.UseFont = true;
            this.ButImp.Enabled = false;
            this.ButImp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButImp.ImageOptions.Image")));
            this.ButImp.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.ButImp.Location = new System.Drawing.Point(869, 14);
            this.ButImp.Name = "ButImp";
            this.ButImp.Size = new System.Drawing.Size(139, 85);
            this.ButImp.TabIndex = 10;
            this.ButImp.Text = "Imprimir PDF";
            this.ButImp.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // SBBusca
            // 
            this.SBBusca.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SBBusca.Appearance.Options.UseFont = true;
            this.SBBusca.Enabled = false;
            this.SBBusca.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("SBBusca.ImageOptions.Image")));
            this.SBBusca.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.SBBusca.Location = new System.Drawing.Point(748, 14);
            this.SBBusca.Name = "SBBusca";
            this.SBBusca.Size = new System.Drawing.Size(115, 85);
            this.SBBusca.TabIndex = 8;
            this.SBBusca.Text = "Buscar";
            this.SBBusca.Click += new System.EventHandler(this.SBBusca_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(14, 149);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(47, 23);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Link:";
            // 
            // SENota
            // 
            this.SENota.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SENota.Location = new System.Drawing.Point(527, 11);
            this.SENota.Name = "SENota";
            this.SENota.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SENota.Properties.Appearance.Options.UseFont = true;
            this.SENota.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SENota.Properties.IsFloatValue = false;
            this.SENota.Properties.Mask.EditMask = "N00";
            this.SENota.Size = new System.Drawing.Size(186, 30);
            this.SENota.TabIndex = 4;
            this.SENota.EditValueChanged += new System.EventHandler(this.spinEdit1_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(395, 61);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(98, 23);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Validador:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(395, 14);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(53, 23);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Nota:";
            // 
            // RGPrefeitura
            // 
            this.RGPrefeitura.Location = new System.Drawing.Point(124, 5);
            this.RGPrefeitura.Name = "RGPrefeitura";
            this.RGPrefeitura.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(4, "Mauá"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Santo André"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "São Bernardo"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "São Caetano"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "São Paulo")});
            this.RGPrefeitura.Size = new System.Drawing.Size(256, 126);
            this.RGPrefeitura.TabIndex = 1;
            this.RGPrefeitura.SelectedIndexChanged += new System.EventHandler(this.radioGroup1_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(14, 14);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(101, 23);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Prefeitura:";
            // 
            // webBrowser2
            // 
            this.webBrowser2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser2.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser2.Location = new System.Drawing.Point(0, 246);
            this.webBrowser2.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.Size = new System.Drawing.Size(946, 249);
            this.webBrowser2.TabIndex = 5;
            this.webBrowser2.WebBrowserShortcutsEnabled = false;
            this.webBrowser2.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser2_DocumentCompleted);
            // 
            // cBuscaNota
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.webBrowser2);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cBuscaNota";
            this.Size = new System.Drawing.Size(946, 495);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TELink.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SEVerificacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SENota.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RGPrefeitura.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.RadioGroup RGPrefeitura;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton SBBusca;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit SENota;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.WebBrowser webBrowser2;
        private DevExpress.XtraEditors.SpinEdit spinEditSP;
        private DevExpress.XtraEditors.LabelControl labelSP;
        private DevExpress.XtraEditors.SimpleButton ButImp;
        private DevExpress.XtraEditors.TextEdit SEVerificacao;
        private DevExpress.XtraEditors.MemoEdit TELink;
        private DevExpress.XtraEditors.SimpleButton ButSegue;
    }
}
