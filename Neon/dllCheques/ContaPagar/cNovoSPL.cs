using System;
using System.Windows.Forms;

namespace ContaPagar
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cNovoSPL : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Titulo"></param>
        public cNovoSPL(string Titulo)
        {
            InitializeComponent();
            Plano.Text = Titulo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PLA"></param>
        /// <returns></returns>
        public int Cadastre(string PLA)
        {
            if (VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cNovoSPL - 25");
                    string Comando1 = "INSERT INTO SubPLano(SPL_PLA,SPLCodigo,SPLDescricao,SPLServico,SPLNoCondominio) VALUES (@P1,@P2,@P3,1,@P4)";
                    int SPL = VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc(Comando1, PLA, Codigo.Text, Descricao.Text, checkNoLocal.Checked);
                    //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update NOtAs set NOA_SPL = @P1 where NOA_PLA = @P2 and NOA_SPL is null", SPL, row.NOA_PLA);
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    Framework.datasets.dPLAnocontas.dPLAnocontasSt25.SubPLanoTableAdapter.Fill25(Framework.datasets.dPLAnocontas.dPLAnocontasSt25.SubPLano);
                    return SPL;
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                    return -1;
                }
            }
            else
                return -1;
        }
    }
}
