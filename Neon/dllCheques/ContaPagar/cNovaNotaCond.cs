﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using ContaPagarProc;

namespace ContaPagar
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cNovaNotaCond : ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cNovaNotaCond()
        {
            InitializeComponent();
        }        

        private void gridControl1_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AjustaRetorno();
                FechaTela(DialogResult.OK);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dNOtAs.CONDOMINIOSRow CONrow;

        private void AjustaRetorno()
        {
            CONrow = (dNOtAs.CONDOMINIOSRow)gridView1.GetFocusedDataRow();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            AjustaRetorno();            
            return base.CanClose();
        }
    }
}
