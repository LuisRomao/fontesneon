using System;
using System.Data;
using Framework;
using FrameworkProc.datasets;
using ContaPagarProc;
using VirEnumeracoesNeon;

namespace ContaPagar
{
    /// <summary>
    /// 
    /// </summary>
    public partial class NotasGrade : Framework.ComponentesNeon.NavegadorCondominio
    {
        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup;

        /// <summary>
        /// Construtor
        /// </summary>
        public NotasGrade()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                dNOtAsBindingSource.DataSource = dNOtAs.dNOtAsSt;
                dFornecedoresLookup = new CadastrosProc.Fornecedores.dFornecedoresLookup();
                dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
                fornecedoresBindingSource.DataSource = dFornecedoresLookup;
                pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas;
                uSUariosBindingSource.DataSource = dUSUarios.dUSUariosSt;
                dCondominiosAtivosBindingSource.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST;
                Enumeracoes.VirEnumNOAStatus.CarregaEditorDaGrid(colNOAStatus);
                Enumeracoes.VirEnumNOATipo.CarregaEditorDaGrid(colNOATipo);                
                Framework.Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
                Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(colPAGTipo);
                travado = true; ;
                DataIni.DateTime = DateTime.Today.AddMonths(-2);
                travado = false;
                GridView_F.OptionsBehavior.Editable = true;                
            }
            if (Framework.DSCentral.USU == 30)
                simpleButton3.Visible = true;
        }

        

        private bool travado;

        /// <summary>
        /// Refresh dos dados com re-carga dos fornecedores se o CON for o mesmo
        /// </summary>
        /// <param name="_CON"></param>
        public void RefreshDados(int _CON)
        {
            if ((_CON == CON) || (CON == -1))
            {
                Calcular();                
            }
        }        

        private dNOtAs.NOtAsRow LinhaMae{
            get{
                DataRowView DRV = (DataRowView)GridView_F.GetRow(GridView_F.FocusedRowHandle);
                if ((DRV == null) || (DRV.Row == null))
                    return null;
                else
                    return (dNOtAs.NOtAsRow)DRV.Row;
            }
        }

        /// <summary>
        /// Exclui a linha
        /// </summary>
        protected override void ExcluirRegistro_F()
        {

            dNOtAs.NOtAsRow LinhaExcluir = LinhaMae;
            if ((LinhaExcluir != null) && (LinhaExcluir.GetPAGamentosRows().Length == 0))
            {
                LinhaExcluir.Delete();
                dNOtAs.dNOtAsSt.NOtAsTableAdapter.Update(LinhaExcluir);                
            }
        }

        private void NotasGrade_CondominioAlterado(object sender, EventArgs e)
        {
            ButtonRec.Enabled = false;
            dNOtAs.dNOtAsSt.PaGamentosFixos.Clear();
            dNOtAs.dNOtAsSt.PAGamentos.Clear();
            dNOtAs.dNOtAsSt.NOtAs.Clear();   
            //colNOA_CON.Visible = (CON == -1);
            //Calcular();
            //ButtonRec.Enabled = (CON != -1);
        }

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colNOAStatus)
            {
                DataRowView DRV = (DataRowView)GridView_F.GetRow(e.RowHandle);
                if ((DRV == null) || (DRV.Row == null))
                    return;
                dNOtAs.NOtAsRow NOArow = (dNOtAs.NOtAsRow)DRV.Row;

                e.Appearance.BackColor = Enumeracoes.VirEnumNOAStatus.GetCor((NOAStatus)NOArow.NOAStatus);

                
                
            }
        }

        

        private void AjustaStatus() 
        {
            foreach (dNOtAs.NOtAsRow NOArow in dNOtAs.dNOtAsSt.NOtAs)
            {
                
                if ((NOAStatus)NOArow.NOAStatus == NOAStatus.Cadastrada)
                {
                    bool Liquidada = true;
                    bool Bloqueada = false;
                    foreach (dNOtAs.PAGamentosRow PAGrow in NOArow.GetPAGamentosRows())
                    {
                        if ((CHEStatus)PAGrow.CHEStatus == CHEStatus.CadastradoBloqueado)
                            Bloqueada = true;
                        if (!Nota.StatusOK.Contains((CHEStatus)PAGrow.CHEStatus))
                        {
                            Liquidada = false;
                            break;
                        }
                    }
                    if (Liquidada || Bloqueada)
                    {                        
                        NOArow.NOAStatus = Bloqueada ?  (int)NOAStatus.NotaProvisoria : (int)NOAStatus.Liquidada;
                        dNOtAs.dNOtAsSt.NOtAsTableAdapter.Update(NOArow);
                        NOArow.AcceptChanges();                        
                    }
                }
            };
            
            
        }               
       
        /// <summary>
        /// Faz a carga dos dados
        /// </summary>
        public void Calcular()
        {
            if (travado)
                return;
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.Espere("Carregando dados");
                try
                {
                    GridView_F.BeginDataUpdate();
                    System.Windows.Forms.Application.DoEvents();
                    dNOtAs.dNOtAsSt.PaGamentosFixos.Clear();
                    dNOtAs.dNOtAsSt.PAGamentos.Clear();
                    dNOtAs.dNOtAsSt.NOtAs.Clear();
                    if (CON != -1)
                    {
                        ESP.Espere("Carregando Notas");
                        System.Windows.Forms.Application.DoEvents();
                        dNOtAs.dNOtAsSt.NOtAsTableAdapter.FilNotasRecentes(dNOtAs.dNOtAsSt.NOtAs, CON, DataIni.DateTime);
                        ESP.Espere("Carregando Cheques");
                        System.Windows.Forms.Application.DoEvents();
                        dNOtAs.dNOtAsSt.PAGamentosTableAdapter.FillComCheque(dNOtAs.dNOtAsSt.PAGamentos, CON, DataIni.DateTime);
                    }
                    else
                    {
                        ESP.Espere("Carregando Notas");
                        System.Windows.Forms.Application.DoEvents();
                        dNOtAs.dNOtAsSt.NOtAsTableAdapter.FillByGeral(dNOtAs.dNOtAsSt.NOtAs, DataIni.DateTime);
                        ESP.Espere("Carregando Cheques");
                        System.Windows.Forms.Application.DoEvents();
                        dNOtAs.dNOtAsSt.PAGamentosTableAdapter.FillByGeral(dNOtAs.dNOtAsSt.PAGamentos, DataIni.DateTime);
                    }
                    ESP.Espere("Ajustando");
                    System.Windows.Forms.Application.DoEvents();
                    AjustaStatus();
                }
                finally
                {
                    GridView_F.EndDataUpdate();
                }
            }
            GridView_F.CollapseAllDetails();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
        }

        private void DataIni_EditValueChanged(object sender, EventArgs e)
        {
            Calcular();
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (GridControl_F.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)GridControl_F.FocusedView;
                DataRow DR = GV.GetFocusedDataRow();
                if (DR is dNOtAs.NOtAsRow)
                {
                    FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow CONrow = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(LinhaMae.NOA_CON);
                    AbreCampos(false, string.Format("{0} - Nota:{1}", CONrow.CONCodigo, LinhaMae.NOANumero));
                }
                else
                {
                    if (DR is dNOtAs.PAGamentosRow)
                    {
                        dNOtAs.PAGamentosRow PAGrow = (dNOtAs.PAGamentosRow)DR;
                        Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Cheque, PAGrow.PAG_CHE, false, true);
                    }
 
                }
            }            
        }

        private void GridView_F_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column == colNOACompet)
            {
                if ((e.Value != null) && (e.Value is int))
                {
                    Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia((int)e.Value);
                    e.DisplayText = comp.ToString();
                }
            }
        }

        private void Tipo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!LinhaMae.IsNOA_PGFNull())
                Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Periodicos, LinhaMae.NOA_PGF, false, false);
        }

        /*
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //base.BtnIncluir_F_ItemClick(sender, e);
        }
        */

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            cNovaNota cNovaNota1 = new cNovaNota(this, NOATipo.Avulsa, -1);
            cNovaNota1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);            
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            if (dNOtAs.dNOtAsSt.NOtAs.Count == 0)
                return;
            Calcular();
        }

        private void ButtonRec_Click(object sender, EventArgs e)
        {
            if (CON == -1)
                return;
            cNovaNota cNovaNota1 = new cNovaNota(this,NOATipo.Recibo,CON);
            cNovaNota1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            colNOA_CON.Visible = (CON == -1);
            Calcular();
            ButtonRec.Enabled = (CON != -1);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            string comando =
"SELECT        PAG, NOA\r\n" +
"FROM            dbo.PAGamentos INNER JOIN\r\n" +
"                         dbo.NOtAs ON dbo.PAGamentos.PAG_NOA = dbo.NOtAs.NOA                         \r\n" +
"WHERE        (PAGVencimento = CONVERT(DATETIME, '2016-05-13 00:00:00', 102)) AND (PAGIMP = 6) AND (NOACompet = 201603) order by NOA;";
            DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comando);
            foreach (DataRow DR in DT.Rows)
            {
                int NOA = (int)DR["NOA"];
                int PAG = (int)DR["PAG"];
                ContaPagarProc.NotaProc nota = new ContaPagarProc.NotaProc(NOA);
                nota.DNOtAs.PAGamentosTableAdapter.FillByNOA(nota.DNOtAs.PAGamentos, NOA);
                foreach (dNOtAs.PAGamentosRow rowPAG in nota.LinhaMae.GetPAGamentosRows())
                {
                    if (rowPAG.PAG == PAG)
                    {
                        rowPAG.PAGVencimento = new DateTime(2016, 04, 15);
                        nota.AjustaParcelaNoCheque(rowPAG);
                        break;
                    }
                }
            }
        }

        /*
        private void simpleButton5_Click(object sender, EventArgs e)
        {
            cBuscaNota cBuscaNota1 = new cBuscaNota();
            cBuscaNota1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            //if (cBuscaNota1.ShowEmPopUp() == System.Windows.Forms.DialogResult.OK)
            //{
            //    cNovaNota cNovaNota1 = new cNovaNota(this, cBuscaNota1);
            //    cNovaNota1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            //}
        }*/
    }
}

