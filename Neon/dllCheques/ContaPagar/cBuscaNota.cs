﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using CompontesBasicosProc;
using CompontesBasicos;
using FrameworkProc;
using iTextSharp.tool.xml;
using Framework.objetosNeon;
using dllEmissorNFS_e;




namespace ContaPagar
{
    /// <summary>
    /// Classe para automação da digitação das notas eletronicas
    /// </summary>
    public partial class cBuscaNota : CompontesBasicos.ComponenteBase
    {

        private BuscaNFE BuscaNota;

        /// <summary>
        /// Construtor
        /// </summary>
        public cBuscaNota()
        {
            InitializeComponent();
            BuscaNota = new BuscaNFE(webBrowser2);
            RGPrefeitura.EditValue = (Framework.DSCentral.EMP == 1) ? 1 : 0;
        }


        enum Prefeituras { SA = 0, SBC = 1, SCS = 2, SP = 3, Maua = 4 }



        private bool BloqueiaBusca = false;

        private Prefeituras PrefeituraSel
        {
            get => (Prefeituras)RGPrefeitura.EditValue;
            set => RGPrefeitura.EditValue = (int)value;
        }

        private void BuscaEnable()
        {
            bool Dadosok = false;
            ButSegue.Visible = false;
            
            if ((PrefeituraSel.EstaNoGrupo(Prefeituras.SA,Prefeituras.SBC,Prefeituras.SCS,Prefeituras.Maua)) && (SENota.Value != 0) && (SEVerificacao.Text != ""))
                Dadosok = true;
            else if ((PrefeituraSel.EstaNoGrupo(Prefeituras.SP)) && (SENota.Value != 0) && (SEVerificacao.Text != "") && (spinEditSP.Value != 0))
                Dadosok = true;
            SBBusca.Enabled = Dadosok;
        }

        private bool ParseLinkSP(string TextoComLink)
        {
            _DadosNFeLido = null;
            string[] Linhas = TextoComLink.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string Linha in Linhas)
            {
                if (Linha.Contains("https://nfe.prefeitura.sp.gov.br/nfe.aspx"))
                {
                    string[] Elementos = Linha.Split(new string[] { "?", "&" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string elemento in Elementos)
                    {
                        if (elemento.StartsWith("ccm="))
                            int.TryParse(elemento.Substring(4), out DadosNFeLido.InsMunP);
                        if (elemento.StartsWith("nf="))
                            int.TryParse(elemento.Substring(3), out DadosNFeLido.NumeroNF);
                        if (elemento.StartsWith("cod="))
                            DadosNFeLido.Verificacao = elemento.Substring(4);
                    }
                }
                else if (Linha.StartsWith("CNPJ:"))
                {
                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Linha.Substring(6));
                }
            }
            return ((DadosNFeLido.InsMunP != 0) && (DadosNFeLido.NumeroNF != 0) && (DadosNFeLido.Verificacao != ""));
        }

        private void BuscaPeloLink()
        {
            switch (PrefeituraSel)
            {
                case Prefeituras.SA:
                case Prefeituras.SBC:
                case Prefeituras.SCS:
                case Prefeituras.Maua:
                    PAcao = ProximaAcao.Ajusta_Clica;
                    webBrowser2.Navigate(TELink.Text);
                    break;
                case Prefeituras.SP:
                    if (ParseLinkSP(TELink.Text))
                    {
                        SalvaPDFIMG(DadosNFeLido.InsMunP, DadosNFeLido.NumeroNF, DadosNFeLido.Verificacao);
                        ButSegue.Visible = true;
                    }
                    PAcao = ProximaAcao.nenhuma;
                    break;
            }
            
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelSP.Visible = spinEditSP.Visible = (PrefeituraSel == Prefeituras.SP);
            BuscaEnable();
        }
        

        private void spinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            BuscaEnable();
        }

        private void spinEdit2_EditValueChanged(object sender, EventArgs e)
        {
            BuscaEnable();
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (BloqueiaBusca)
                return;
            try
            {
                BloqueiaBusca = true;
                if ((TELink.Text != "") && (validaURL()))
                    BuscaPeloLink();
            }
            finally
            {
                BloqueiaBusca = false;
            }

        }

        //public string ArquivoPDF;

        private string ArquivoTMP
        {
            get
            {
                string CaminhoBase;
                CaminhoBase = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\TMP\\";
                return string.Format("{0}{1:yyyyMMddHHmmss}.pdf", CaminhoBase, DateTime.Now);
            }
        }

        private void SBBusca_Click(object sender, EventArgs e)
        {
            _DadosNFeLido = null;
            ButImp.Enabled = false;
            string URL = "";            
            if ((SENota.Value != 0) && (SEVerificacao.Text != ""))
            {
                string mascara = "";
                switch (PrefeituraSel)
                {
                    case Prefeituras.SA:
                        mascara = "http://santoandre.ginfes.com.br/birt/frameset?__report=nfs_ver13.rptdesign&cdVerificacao={0}&numNota={1}";
                        PAcao = ProximaAcao.Ajusta_Clica;
                        break;
                    case Prefeituras.SBC:
                        mascara = "http://nfse.ginfes.com.br/birt/frameset?__report=nfs_sao_bernardo_campo.rptdesign&cdVerificacao={0}&numNota={1}";
                        PAcao = ProximaAcao.Ajusta_Clica;
                        break;
                    case Prefeituras.SCS:
                        mascara = "http://saocaetano.ginfes.com.br/birt/frameset?__report=nfs_ver15.rptdesign&cdVerificacao={0}&numNota={1}";
                        PAcao = ProximaAcao.Ajusta_Clica;
                        break;
                    case Prefeituras.Maua:
                        mascara = "http://maua.ginfes.com.br/birt/frameset?__report=nfs_ver4.rptdesign&cdVerificacao={0}&numNota={1}";
                        PAcao = ProximaAcao.Ajusta_Clica;
                        break;
                    case Prefeituras.SP:
                        URL = "";
                        if (spinEditSP.Value != 0)
                        {
                            DadosNFeLido.InsMunP = (int)spinEditSP.Value;
                            DadosNFeLido.NumeroNF = (int)SENota.Value;
                            DadosNFeLido.Verificacao = SEVerificacao.Text.Replace("-", "").Trim().ToUpper();
                            SalvaPDFIMG(DadosNFeLido.InsMunP, DadosNFeLido.NumeroNF, DadosNFeLido.Verificacao);
                            ButSegue.Visible = true;
                        }
                        break;
                }
                URL = string.Format(mascara, SEVerificacao.Text , SENota.Value);
            }
            if (URL != "")                            
                webBrowser2.Navigate(URL);                                            
        }

        private bool validaURL()
        {
            if (TELink.Text.Contains("santoandre.ginfes.com.br/birt/frameset?__report=nfs_ver13.rptdesign&cdVerificacao="))
            {
                PrefeituraSel = Prefeituras.SA;
                return true;
            }
            else if (TELink.Text.Contains("saocaetano.ginfes.com.br/birt/frameset?__report=nfs_ver15.rptdesign&cdVerificacao="))
            {
                PrefeituraSel = Prefeituras.SCS;
                return true;
            }
            else if (TELink.Text.Contains("nfse.ginfes.com.br/birt/frameset?__report=nfs_sao_bernardo_campo.rptdesign&cdVerificacao="))
            {
                PrefeituraSel = Prefeituras.SBC;
                return true;
            }
            else if (TELink.Text.Contains("maua.ginfes.com.br/birt/frameset?__report=nfs_ver4.rptdesign&cdVerificacao="))
            {
                PrefeituraSel = Prefeituras.Maua;
                return true;
            }
            else if (TELink.Text.Contains("nfe.prefeitura.sp.gov.br/nfe.aspx?ccm="))
            {
                PrefeituraSel = Prefeituras.SP;
                return true;
            }
            else
                return false;
        }

        private bool AjustaPagina()
        {
            if (webBrowser2.Document.Forms.Count == 0)
                return false;
            string xs = webBrowser2.Document.Forms[0].OuterHtml;
            if (xs.Contains("Nota fiscal inexistente"))
                return false;
            else
            {
                xs = xs.Replace("target=_blank", "");
                webBrowser2.Document.Forms[0].OuterHtml = xs;
                return true;
            }
        }

        //bool AguardaPaginaInicial = false;
        //bool AguardaPDF = false;

        enum ProximaAcao { nenhuma, Ajusta_Clica, SalvaPDF }

        ProximaAcao PAcao = ProximaAcao.nenhuma;

        private void CadastraNota()
        {
            //clientFTP.PutPasta(ArquivoTMP, string.Format("Notas\\{0}", DadosNFeLido.competencia));
            cNovaNota cNovaNota1 = new cNovaNota(null, DadosNFeLido);
            cNovaNota1.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
            //_DadosNFeLido = null;
            TELink.Text = "";
            
        }

        

        private void webBrowser2_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            switch (PAcao)
            {
                case ProximaAcao.Ajusta_Clica:
                    if (AjustaPagina())
                    {
                        PAcao = ProximaAcao.SalvaPDF;
                        Clica();
                    }
                    else
                        PAcao = ProximaAcao.nenhuma;
                    break;
                case ProximaAcao.SalvaPDF:
                    PAcao = ProximaAcao.nenhuma;
                    DadosNFeLido.ArquivoTMP = ArquivoTMP;
                    salvaPDF(webBrowser2.Url.AbsoluteUri, DadosNFeLido.ArquivoTMP);
                    ButImp.Enabled = true;
                    CadastraNota();
                    //FechaTela(DialogResult.OK);
                    break;/*
                case ProximaAcao.SalvaPDFIMG:
                    PAcao = ProximaAcao.nenhuma;
                    SalvaPDFIMG();
                    ButImp.Enabled = true;
                    //CadastraNota();
                    //FechaTela(DialogResult.OK);
                    break;*/
            };
            /*
            if (AguardaPaginaInicial)
            {
                AguardaPaginaInicial = false;
                AjustaPagina();
                AguardaPDF = true;
                Clica();
            }
            else if (AguardaPDF)
            {
                AguardaPaginaInicial = false;
                AguardaPDF = false;
                salvaPDF(webBrowser2.Url.AbsoluteUri, "c:\\lixo\\nota.pdf");
            }
            else
            {
                AguardaPaginaInicial = false;
                AguardaPDF = false;
            }*/
        }

        private void Clica()
        {
            foreach (HtmlElement html in webBrowser2.Document.GetElementsByTagName("a"))
            {
                if (html.InnerText == "Exportar PDF")
                {
                    html.InvokeMember("click");
                }
            }
        }

        private void salvaPDF(string Url, string FileName)
        {            
            using (var wc = new System.Net.WebClient())
            {         
                wc.DownloadFile(Url, FileName);                
            }
            ExtractTextFromPdf(FileName);            
        }

        private DadosNFe _DadosNFeLido;

        private DadosNFe DadosNFeLido => _DadosNFeLido ?? (_DadosNFeLido = new DadosNFe());
        /*
        public int NumeroNF = 0;
        public int Verificacao = 0;
        public DateTime Emissao = DateTime.MinValue;
        public decimal Valor;
        public decimal RetencaISS = 0;
        public decimal RetencaoPIS = 0;
        public decimal RetencaoCOF = 0;
        public decimal RetencaoIR = 0;
        public decimal RetencaoINSS = 0;
        public decimal RetencaoCSLL = 0;
        public string Descricao = "";
        public DocBacarios.CPFCNPJ CNPJTomador = null;
        public DocBacarios.CPFCNPJ CNPJPrestador = null;
        public Competencia competencia = null;
        public string RazaoP;
        public string RazaoT;
        public int InsMunP = 0;
        public int InsMunT = 0;*/

        enum Bandas
        {
            cabecalho,
            Prestador,
            Tomandor,
            Discriminacao,
            Servico,
            Retencao,
            Valores
        }

        private string Parse(string path)
        {
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(path))
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
            }            
            return text.ToString();
        }

        private DadosNFe ExtractTextFromPdf_SA(string[] linhas)
        {
            Bandas banda = Bandas.cabecalho;
            string[] Palavras;
            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Prestador de Serviço"))
                    banda = Bandas.Prestador;
                else if (linhas[i].Contains("Tomador de Serviço"))
                    banda = Bandas.Tomandor;
                else if (linhas[i].Contains("Discriminação do Serviço"))
                    banda = Bandas.Discriminacao;
                else if (linhas[i].Contains("Código do Serviço / Atividade"))
                    banda = Bandas.Servico;
                else if (linhas[i].Contains("Tributos Federais"))
                    banda = Bandas.Retencao;
                else if (linhas[i].Contains("Detalhamento de Valores - Prestador do Serviço"))
                    banda = Bandas.Valores;
                else
                    switch (banda)
                    {
                        case Bandas.cabecalho:
                            if (linhas[i].Contains("NOTA FISCAL ELETRÔNICA DE SERVIÇO - NFS-e"))
                                int.TryParse(linhas[i + 1], out DadosNFeLido.NumeroNF);
                            else if (linhas[i].Contains("Data e Hora da Emissão"))
                            {
                                DateTime.TryParse(linhas[i].Substring(23, 19), out DadosNFeLido.Emissao);
                                //string teste = linhas[i].Substring(23, 9).Trim();
                                string[] strComp = linhas[i].Substring(23, 10).Trim().Split('/');
                                if (strComp.Length == 3)
                                {
                                    int mes = 0;
                                    int ano = 0;
                                    if (int.TryParse(strComp[1], out mes) && (int.TryParse(strComp[2], out ano)))
                                        DadosNFeLido.competencia = new Competencia(mes, ano);
                                };
                                DadosNFeLido.Verificacao = linhas[i].Substring(linhas[i].IndexOf("Verificação") + 12).Trim();
                            }
                            break;
                        case Bandas.Prestador:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoP = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith(" CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Palavras[2]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunP);
                                }
                            }
                            break;
                        case Bandas.Tomandor:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoT = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith(" CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJTomador = new DocBacarios.CPFCNPJ(Palavras[2]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunT);
                                }
                            }
                            break;
                        case Bandas.Servico:
                            string[] partes = linhas[i].Split('-');
                            if (partes.Length == 2)
                                DadosNFeLido.Descricao = partes[1].Trim();
                            break;
                        case Bandas.Retencao:
                            Palavras = linhas[i].Split(' ');
                            for (int j = 0; j < Palavras.Length - 1; j++)
                            {
                                if (Palavras[j] == "PIS(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoPIS);
                                else if (Palavras[j] == "COFINS(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoCOF);
                                else if (Palavras[j] == "IR(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoIR);
                                else if (Palavras[j] == "INSS(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoINSS);
                                else if (Palavras[j] == "CSLL(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoCSLL);
                            }
                            break;
                        case Bandas.Valores:
                            if (linhas[i].Contains("Valor do Serviço"))
                            {
                                Palavras = linhas[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                decimal.TryParse(Palavras[4], out DadosNFeLido.Valor);
                            }
                            else if (linhas[i].Contains("(-) ISSQN Retido"))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[3], out DadosNFeLido.RetencaISS);
                            }
                            break;
                    }
            }
            return DadosNFeLido;
        }

        private DadosNFe ExtractTextFromPdf_SCS(string[] linhas)
        {
            Bandas banda = Bandas.cabecalho;
            string[] Palavras;
            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Prestador de Serviço"))
                    banda = Bandas.Prestador;
                else if (linhas[i].Contains("Tomador de Serviço"))
                    banda = Bandas.Tomandor;
                else if (linhas[i].Contains("Discriminação do Serviço"))
                    banda = Bandas.Discriminacao;
                else if (linhas[i].Contains("Código do Serviço / Atividade"))
                    banda = Bandas.Servico;
                else if (linhas[i].Contains("Tributos Federais"))
                    banda = Bandas.Retencao;
                else if (linhas[i].Contains("Detalhamento de Valores - Prestador do Serviço"))
                    banda = Bandas.Valores;
                else
                    switch (banda)
                    {
                        case Bandas.cabecalho:
                            if (linhas[i].Contains("NOTA FISCAL ELETRÔNICA DE SERVIÇO - NFS-e"))
                                int.TryParse(linhas[i + 1], out DadosNFeLido.NumeroNF);
                            else if (linhas[i].Contains("Data e Hora da Emissão"))
                            {
                                DateTime.TryParse(linhas[i].Substring(23, 19), out DadosNFeLido.Emissao);
                                //string teste = linhas[i].Substring(23, 9).Trim();
                                string[] strComp = linhas[i].Substring(23, 10).Trim().Split('/');
                                if (strComp.Length == 3)
                                {
                                    int mes = 0;
                                    int ano = 0;
                                    if (int.TryParse(strComp[1], out mes) && (int.TryParse(strComp[2], out ano)))
                                        DadosNFeLido.competencia = new Competencia(mes, ano);
                                };
                                DadosNFeLido.Verificacao = linhas[i].Substring(linhas[i].IndexOf("Verificação") + 12).Trim();
                            }
                            break;
                        case Bandas.Prestador:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoP = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith(" CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Palavras[2]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunP);
                                }
                            }
                            break;
                        case Bandas.Tomandor:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoT = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith(" CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJTomador = new DocBacarios.CPFCNPJ(Palavras[2]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunT);
                                }
                            }
                            break;
                        case Bandas.Servico:
                            string[] partes = linhas[i].Split('-');
                            if (partes.Length == 2)
                                DadosNFeLido.Descricao = partes[1].Trim();
                            break;
                        case Bandas.Retencao:
                            Palavras = linhas[i].Split(' ');
                            for (int j = 0; j < Palavras.Length - 2; j++)
                            {
                                if (Palavras[j] == "PIS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoPIS);
                                else if (Palavras[j] == "COFINS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoCOF);
                                else if (Palavras[j] == "IR")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoIR);
                                else if (Palavras[j] == "INSS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoINSS);
                                else if (Palavras[j] == "CSLL")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoCSLL);
                            }
                            break;
                        case Bandas.Valores:
                            if (linhas[i].Contains("Valor do Serviço"))
                            {
                                Palavras = linhas[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                decimal.TryParse(Palavras[4], out DadosNFeLido.Valor);
                            }
                            else if (linhas[i].Contains("(-) ISSQN Retido"))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[3], out DadosNFeLido.RetencaISS);
                            }
                            break;
                    }
            }
            return DadosNFeLido;
        }

        private DadosNFe ExtractTextFromPdf_Maua(string[] linhas)
        {
            Bandas banda = Bandas.cabecalho;
            string[] Palavras;
            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Dados do Prestador de Serviços"))
                    banda = Bandas.Prestador;
                else if (linhas[i].Contains("Dados do Tomador de Serviços"))
                    banda = Bandas.Tomandor;
                else if (linhas[i].Contains("Discriminação dos Serviços"))
                    banda = Bandas.Discriminacao;
                else if (linhas[i].Contains("Código do Serviço / Atividade"))
                    banda = Bandas.Servico;
                else if (linhas[i].Contains("Tributos Federais"))
                    banda = Bandas.Retencao;
                else if (linhas[i].Contains("Detalhamento de Valores - Prestador dos Serviços"))
                    banda = Bandas.Valores;
                else
                    switch (banda)
                    {
                        case Bandas.cabecalho:
                            if (linhas[i].Contains("NOTA FISCAL ELETRÔNICA DE SERVIÇO - NFS-e"))
                                int.TryParse(linhas[i + 1], out DadosNFeLido.NumeroNF);
                            else if (linhas[i].Contains("Data e Hora da Emissão"))
                            {
                                DateTime.TryParse(linhas[i].Substring(23, 19), out DadosNFeLido.Emissao);
                                string[] strComp = linhas[i].Substring(23, 10).Trim().Split('/');
                                if (strComp.Length == 3)
                                {
                                    int mes = 0;
                                    int ano = 0;
                                    if (int.TryParse(strComp[1], out mes) && (int.TryParse(strComp[2], out ano)))
                                        DadosNFeLido.competencia = new Competencia(mes, ano);
                                };
                                //DadosNFeLido.Verificacao = linhas[i].Substring(linhas[i].IndexOf("Verificação") + 12).Trim();
                            }
                            else if (linhas[i].Contains("Código de Verificação"))
                            {
                                Palavras = linhas[i + 1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                DadosNFeLido.Verificacao = Palavras[Palavras.Length - 1];
                            }
                            break;
                        case Bandas.Prestador:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoP = linhas[i].Substring(18).Trim();
                            else if (linhas[i].StartsWith("CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Palavras[1]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunP);
                                }
                            }
                            break;
                        case Bandas.Tomandor:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoT = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith("CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJTomador = new DocBacarios.CPFCNPJ(Palavras[1]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunT);
                                }
                            }
                            break;
                        case Bandas.Servico:
                            string[] partes = linhas[i].Split('-');
                            if (partes.Length == 2)
                                DadosNFeLido.Descricao = partes[1].Trim();
                            break;
                        case Bandas.Retencao:
                            Palavras = linhas[i].Split(' ');
                            for (int j = 0; j < Palavras.Length - 1; j++)
                            {
                                if (Palavras[j] == "PIS")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoPIS);
                                else if (Palavras[j] == "COFINS")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoCOF);
                                else if (Palavras[j] == "IR(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoIR);
                                else if (Palavras[j] == "INSS(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoINSS);
                                else if (Palavras[j] == "CSLL(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoCSLL);
                            }
                            break;
                        case Bandas.Valores:
                            if (linhas[i].Contains("Valor dos Serviços"))
                            {
                                Palavras = linhas[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                decimal.TryParse(Palavras[4], out DadosNFeLido.Valor);
                            }
                            else if ((linhas[i].Contains("(-) ISS Retido")) && (linhas[i].Contains("(X) Sim")))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[3], out DadosNFeLido.RetencaISS);
                            }
                            break;
                    }
            }            
            return DadosNFeLido;
        }

        private DadosNFe ExtractTextFromPdf_SBC(string[] linhas)
        {
            Bandas banda = Bandas.cabecalho;
            string[] Palavras;
            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Dados do Prestador de Serviços"))
                    banda = Bandas.Prestador;
                else if (linhas[i].Contains("Dados do Tomador de Serviços"))
                    banda = Bandas.Tomandor;
                else if (linhas[i].Contains("Discriminação dos Serviços"))
                    banda = Bandas.Discriminacao;
                else if (linhas[i].Contains("Codificação do Serviço Prestado"))
                    banda = Bandas.Servico;
                else if (linhas[i].Contains("Retenção de Tributos Federais (R$)"))
                    banda = Bandas.Retencao;
                else if (linhas[i].Contains("Detalhamento de Valores dos Serviços"))
                    banda = Bandas.Valores;
                else
                    switch (banda)
                    {
                        case Bandas.cabecalho:
                            if (linhas[i].Contains("NOTA FISCAL DE SERVIÇOS ELETRÔNICA - NFS-e"))
                                int.TryParse(linhas[i + 1], out DadosNFeLido.NumeroNF);
                            else if (linhas[i].Contains("Data e Hora da Emissão"))
                            {
                                DateTime.TryParse(linhas[i + 1].Substring(0, 19), out DadosNFeLido.Emissao);
                                //string teste = linhas[i + 1].Substring(32, 7).Trim();
                                string[] strComp = linhas[i + 1].Substring(32, 7).Trim().Split('/');
                                if (strComp.Length == 2)
                                {
                                    int mes = 0;
                                    int ano = 0;
                                    if (int.TryParse(strComp[0], out mes) && (int.TryParse(strComp[1], out ano)))
                                        DadosNFeLido.competencia = new Competencia(mes, ano);
                                };
                                DadosNFeLido.Verificacao = linhas[i + 1].Substring(linhas[i + 1].IndexOf("verificação") + 12).Trim();
                            }
                            break;
                        case Bandas.Prestador:
                            if (linhas[i].Contains("Razão Social / Nome"))
                                DadosNFeLido.RazaoP = linhas[i].Substring(20).Trim();
                            else if (linhas[i].StartsWith("CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Palavras[1]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunP);
                                }
                            }
                            break;
                        case Bandas.Tomandor:
                            if (linhas[i].Contains("Razão Social / Nome"))
                                DadosNFeLido.RazaoT = linhas[i].Substring(21).Trim();
                            else if (linhas[i].StartsWith("CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJTomador = new DocBacarios.CPFCNPJ(Palavras[1]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunT);
                                }
                            }
                            break;
                        case Bandas.Servico:
                            string[] partes = linhas[i].Split('-');
                            if (partes.Length == 2)
                                DadosNFeLido.Descricao = partes[1].Trim();
                            break;
                        case Bandas.Retencao:
                            Palavras = linhas[i].Split(' ');
                            for (int j = 0; j < Palavras.Length - 2; j++)
                            {
                                if (Palavras[j] == "PIS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoPIS);
                                else if (Palavras[j] == "COFINS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoCOF);
                                else if (Palavras[j] == "IR")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoIR);
                                else if (Palavras[j] == "INSS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoINSS);
                                else if (Palavras[j] == "CSLL")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoCSLL);
                            }
                            break;
                        case Bandas.Valores:
                            if (linhas[i].Contains("Valor dos Serviços R$"))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[4], out DadosNFeLido.Valor);
                            }
                            else if ((linhas[i].Contains("(-) ISS Retido")) && (linhas[i].Contains("(X) Sim")))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[3], out DadosNFeLido.RetencaISS);
                            }
                            break;
                    }
            }            
            return DadosNFeLido;
        }        

        private DadosNFe ExtractTextFromPdf(string path)
        {            
            string[] linhas = Parse(path).Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                        
            switch(PrefeituraSel)
            {
                case Prefeituras.SA:
                    ExtractTextFromPdf_SA(linhas);
                    break;
                case Prefeituras.SCS:
                    ExtractTextFromPdf_SCS(linhas);
                    break;
                case Prefeituras.SBC:
                    ExtractTextFromPdf_SBC(linhas);
                    break;
                case Prefeituras.Maua:
                    ExtractTextFromPdf_Maua(linhas);
                    break;
                default:
                    throw new NotImplementedException(string.Format("Prefeitura não implementada:{0}", PrefeituraSel));
            }

            //string relatorio = DadosNFeLido.Relatorio();
            //Clipboard.SetText(relatorio);
            //MessageBox.Show(relatorio);
            return DadosNFeLido;
        }        

        private void SalvaPDFIMG(int strCCM, int strNF , string strCod  )
        {
            
            string contents = String.Format(
                            "<html><head><meta name='viewport' content='width=device-width, minimum-scale=0.1' />" +                                                        
                            "<title>Nf - e</title></head><body style='margin: 0px;'>" +                                                        
                            "<img align = 'middle' style = 'border-width: 0px; ' src='https://nfe.prefeitura.sp.gov.br/contribuinte/notaprintimg.aspx?ccm={0}&amp;nf={1}&amp;cod={2}&amp;imprimir=1' />" +                            
                            "</body></html>",
                            strCCM, strNF, strCod);
            DadosNFeLido.ArquivoTMP = ArquivoTMP;            
            using (Document doc = new Document())
            {
                using (FileStream arquivoPdf = new FileStream(DadosNFeLido.ArquivoTMP, FileMode.Create))
                {
                    PdfWriter writer = PdfWriter.GetInstance(doc, arquivoPdf);
                    doc.Open();
                    StringReader sr = new StringReader(contents);
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, sr);
                    doc.Close();                 
                }
            }            
            webBrowser2.Navigate(DadosNFeLido.ArquivoTMP);
            ButImp.Enabled = true;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            webBrowser2.Print();            
        }

        private void textEdit1_EditValueChanged_1(object sender, EventArgs e)
        {
            BuscaEnable();
        }

        private void spinEditSP_EditValueChanged(object sender, EventArgs e)
        {
            BuscaEnable();
        }

        private void memoEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (BloqueiaBusca)
                return;
            try
            {
                BloqueiaBusca = true;
                if ((TELink.Text != "") && (validaURL()))
                {
                    _DadosNFeLido = null;
                    ButImp.Enabled = false;
                    BuscaPeloLink();
                }
            }
            finally
            {
                BloqueiaBusca = false;
            }
        }

        private void ButSegue_Click(object sender, EventArgs e)
        {
            CadastraNota();
        }
    }
}
