﻿/*
LH - 04/06/2014                  - Novo construtor para transferência física
MR - 27/04/2015 12:00            - Inclusão do status do pagamento = status do cheque ao incluir parcela no cheque, para tratamento de "guia de recolhimento - eletrônico" (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
MR - 20/05/2015 17:00            - Permite AddPAG com CCT para pagamento eletrônico de valor arrecadado, por exemplo (Alterações indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***)
                                 - Seta PAGStatus no addPAG
MR - 10/06/2015 08:20            - PECreditoConta entre contas do próprio condominio, seta CHEAdicional com o CCT de Crédito apenas (Alterações indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***)
*/

using System;
using System.Collections;
using dllCheques;
using VirEnumeracoesNeon;
using Framework.objetosNeon;
using FrameworkProc;
using ContaPagarProc;
using dllImpostoNeonProc;
using VirEnumeracoes;
using CompontesBasicosProc;
using System.Collections.Generic;
using CompontesBasicos;

namespace ContaPagar
{
    /// <summary>
    /// 
    /// </summary>
    public class Nota:NotaProc
    {
        private static System.Collections.ArrayList _StatusOK;

        internal static System.Collections.ArrayList StatusOK
        {
            get
            {
                return _StatusOK ?? (_StatusOK = new System.Collections.ArrayList(new CHEStatus[] {
                    CHEStatus.Cancelado,
                    CHEStatus.Compensado,
                    CHEStatus.CompensadoAguardaCopia,
                    CHEStatus.NaoEncontrado,
                    CHEStatus.Retirado,
                    CHEStatus.Caixinha
                    //CHEStatus.DebitoAutomatico
                }));
            }
        }

        public void MostraPDF()
        {            
            ComponenteWEB novoWeb = new ComponenteWEB() { MostraEndereco = false, Doc = System.Windows.Forms.DockStyle.Fill };                        
            string URL = string.Format("www.neonimoveis.com.br/neon23/neononline/pdfdoc.aspx?ARQUIVO={0}", NomeArquivoPDFServidor);            
            novoWeb.Navigate(URL);
            novoWeb.Titulo = string.Format("Nota {0}", LinhaMae.NOANumero);
            novoWeb.VirShowModulo(EstadosDosComponentes.JanelasAtivas);            
            ((DevExpress.XtraTab.XtraTabPage)novoWeb.Parent).ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;            
        }

        /*
        /// <summary>
        /// Nome do fornecedor
        /// </summary>
        /// <param name="FRN"></param>
        /// <returns></returns>
        override protected string FRNNome(int FRN)
        {
            return Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fornecedores.FindByFRN(FRN).FRNNome;
        }*/
        
        
        /*
        private void detalhar()
        {
            //if (detalhada)
            //    return;
            DNOtAs.PAGamentosTableAdapter.FillByNOA(DNOtAs.PAGamentos, NOA);
            PAG_Pagamentos = new SortedList();
            PAG_PagamentosCancelados = new SortedList();
            PAGISS = null;
            foreach (dNOtAs.PAGamentosRow row in DNOtAs.PAGamentos)
            {
                if ((CHEStatus)row.CHEStatus == CHEStatus.Cancelado)
                    PAG_PagamentosCancelados.Add(row.PAG, row);
                else
                {
                    if (row.IsPAGIMPNull())
                        PAG_Pagamentos.Add(row.PAG, row);
                    else
                        PAGISS = row;
                }
            }
        }
        */

        /*
        private SortedList PAG_Pagamentos; //dNOtAs.PAGamentosRow

        private SortedList PAG_PagamentosCancelados; //dNOtAs.PAGamentosRow

        private dNOtAs.PAGamentosRow PAGISS;
        */

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool AjustarComPGF()
        {
            if (!Encontrada())
                return false;
            if (LinhaMae.IsNOA_PGFNull())
                return false;
            if (DNOtAs.PaGamentosFixosTableAdapter.FillByPGF(DNOtAs.PaGamentosFixos, LinhaMae.NOA_PGF) == 0)
                return false;
            else
            {
                dNOtAs.PaGamentosFixosRow rowPGF = DNOtAs.PaGamentosFixos[0];
                if (rowPGF.PGFValor == LinhaMae.NOATotal)
                    return false;
                else 
                {
                    return AlteraValor(rowPGF.PGFValor, rowPGF.IsPGFISSNull() ? 0 : rowPGF.PGFISS);
                }
            }
        }

        public string Correcao()
        {
            detalhar();
            decimal Total = 0;
            foreach (dNOtAs.PAGamentosRow row in DNOtAs.PAGamentos)
            {
                Total += row.PAGValor;
            }
            if (Total > linhaMae.NOATotal)
            {
                if (PAG_Pagamentos.Count != 1)
                    return "Verificar";
                if (!PAGPrincipal.IsCHENumeroNull())
                    return "Emitido";
                PAGPrincipal.PAGValor -= (Total - linhaMae.NOATotal);
                PAGPrincipal.PAGPermiteAgrupar = true;
                try
                {
                    EMPTProc1.AbreTrasacaoSQL("ContaPagar Notas - 141", DNOtAs.PAGamentosTableAdapter);
                    DNOtAs.PAGamentosTableAdapter.Update(PAGPrincipal);
                    new Cheque(PAGPrincipal.PAG_CHE, Cheque.TipoChave.CHE).AjustarTotalPelasParcelas();
                    EMPTProc1.Commit();
                }
                catch (Exception ex)
                {
                    EMPTProc1.Vircatch(ex);
                    return "erro em try";
                }
                return "Corrigida";
            }
            else
                return "ok";
        }

        /// <summary>
        /// Deveria ser mais flexivel
        /// </summary>
        /// <returns></returns>
        public bool AlteraValor(decimal Total, decimal iss)
        {
            detalhar();
            if (PAG_Pagamentos.Count != 1)
                return false;
            if ((iss != 0) && (PAGISSrow == null))
                return false;
            if ((iss == 0) && (PAGISSrow != null))
                return false;
            dNOtAs.PAGamentosRow rowPAG = (dNOtAs.PAGamentosRow)PAG_Pagamentos.GetValueList()[0];
            try
            {
                EMPTProc1.AbreTrasacaoSQL("ContaPagar Notas - 119", DNOtAs.PAGamentosTableAdapter, DNOtAs.NOtAsTableAdapter);
                LinhaMae.NOATotal = Total;
                DNOtAs.NOtAsTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
                if (rowPAG.PAGValor != Total - iss)
                {
                    rowPAG.PAGValor = Total - iss;
                    DNOtAs.PAGamentosTableAdapter.Update(rowPAG);
                    rowPAG.AcceptChanges();
                    new Cheque(rowPAG.PAG_CHE,Cheque.TipoChave.CHE).AjustarTotalPelasParcelas();
                }

                if ((iss != 0) && (PAGISSrow.PAGValor != iss))
                {
                    PAGISSrow.PAGValor = iss;
                    DNOtAs.PAGamentosTableAdapter.Update(PAGISSrow);
                    PAGISSrow.AcceptChanges();
                    new Cheque(PAGISSrow.PAG_CHE, Cheque.TipoChave.CHE).AjustarTotalPelasParcelas();
                }
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                return false;
            }
            return true;
        }

        //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
        /// <summary>
        /// Inclui um pagametno na nota
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="Vencimento"></param>
        /// <param name="ValorPagamento"></param>
        /// <param name="Somar">Soma o valor do novo PAG ao todal da nota</param>
        /// <param name="AcumularPAG">Se existir um PAG com o cheque nao emitido soma no PAG</param>
        /// <param name="Favorecido"></param>
        /// <param name="Agrupa"></param>
        /// <param name="DadosPag"></param>
        /// <param name="CCT"></param>
        /// <returns></returns>
        public int addPAG(PAGTipo Tipo, DateTime Vencimento, decimal ValorPagamento, bool Somar, bool AcumularPAG, string Favorecido, bool Agrupa, string DadosPag = null, int? CCT = null)
        //public int addPAG(PAGTipo Tipo, DateTime Vencimento, decimal ValorPagamento,bool Somar,bool AcumularPAG, string Favorecido, bool Agrupa, string DadosPag = null)
        //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
        {
            int PAG = 0;
            try
            {
                EMPTProc1.AbreTrasacaoSQL("ContaPagar Nota - 177", DNOtAs.PAGamentosTableAdapter,
                                                                               DNOtAs.NOtAsTableAdapter);
                dNOtAs.PAGamentosRow rowPag = null;
                if (AcumularPAG)
                {
                    dNOtAs.PAGamentosRow[] rowPAGs = LinhaMae.GetPAGamentosRows();
                    if (rowPAGs.Length == 0)
                        DNOtAs.PAGamentosTableAdapter.FillByNOA(DNOtAs.PAGamentos, NOA);
                    rowPAGs = LinhaMae.GetPAGamentosRows();
                    foreach (dNOtAs.PAGamentosRow rowPAGTeste in rowPAGs)
                    {
                        int? CHE_CCT = null;
                        if (!rowPAGTeste.IsCHE_CCTNull())
                            CHE_CCT = rowPAGTeste.CHE_CCT;
                        if ((rowPAGTeste.PAGTipo == (int)Tipo)
                              &&
                            (rowPAGTeste.PAGVencimento == Vencimento)
                              &&
                            (rowPAGTeste.PAGPermiteAgrupar)
                              &&
                            (!rowPAGTeste.IsCHEStatusNull())
                              &&
                            ((CHEStatus)rowPAGTeste.CHEStatus == CHEStatus.Cadastrado)                            
                              &&
                            (!CCT.HasValue || CHE_CCT == CCT)                            
                          )
                        {
                            rowPag = rowPAGTeste;
                            break;
                        }
                    }
                }
                Cheque NovoCheque = null;
                if (rowPag == null)
                {
                    rowPag = DNOtAs.PAGamentos.NewPAGamentosRow();                    
                    if (Tipo != PAGTipo.Acumular)
                    {
                        //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                        //NovoCheque = new Cheque(Vencimento, Favorecido, LinhaMae.NOA_CON, Tipo, !Agrupa,null,null,DadosPag);
                        NovoCheque = new Cheque(Vencimento, Favorecido, LinhaMae.NOA_CON, Tipo, !Agrupa, null, CCT, DadosPag);
                        rowPag.PAGStatus = NovoCheque.CHErow.CHEStatus;
                        //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                        rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                    }

                    rowPag.PAG_NOA = NOA;
                    rowPag.PAGDATAI = DateTime.Now;
                    rowPag.PAGI_USU = EMPTProc1.USU;
                    rowPag.PAGTipo = (int)Tipo;
                    rowPag.PAGValor = ValorPagamento;
                    rowPag.PAGVencimento = Vencimento;
                    if (DadosPag != null)
                        rowPag.PAGAdicional = DadosPag;
                    rowPag.PAGPermiteAgrupar = Agrupa;
                    DNOtAs.PAGamentos.AddPAGamentosRow(rowPag);                                        
                }
                else
                {
                    rowPag.PAGValor += ValorPagamento;
                    NovoCheque = new Cheque(rowPag.PAG_CHE,Cheque.TipoChave.CHE);
                }
                DNOtAs.PAGamentosTableAdapter.Update(rowPag);
                rowPag.AcceptChanges();
                if (NovoCheque != null)
                {
                    NovoCheque.LiberaEdicao = true;
                    NovoCheque.AjustarTotalPelasParcelas();
                }
                PAG = rowPag.PAG;
                if (Somar)
                {
                    LinhaMae.NOATotal += ValorPagamento;
                    DNOtAs.NOtAsTableAdapter.Update(LinhaMae);
                    LinhaMae.AcceptChanges();
                }
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
            }
            return PAG;
                
        }

        #region Construtores

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public Nota(EMPTProc _EMPTProc1)
            : base(_EMPTProc1)
        {            
        }

        /// <summary>
        /// Construtor para uma nota já existente. verifique depois se "encotrada" está como true
        /// </summary>
        /// <param name="NOA"></param>
        /// <param name="_EMPTProc1"></param>
        public Nota(int NOA, EMPTProc _EMPTProc1 = null)
            : base(NOA, _EMPTProc1)            
        {            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_LinhaMae"></param>
        /// <param name="_EMPTProc1"></param>
        public Nota(dNOtAs.NOtAsRow _LinhaMae, EMPTProc _EMPTProc1 = null)
            : base(_LinhaMae, _EMPTProc1)
        {            
        }

        /// <summary>
        /// Construtor para uma nota já existente. verifique depois se "encontrada" está como true
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="Compet"></param>
        /// <param name="Tipo"></param>
        /// <param name="Numero"></param>
        public Nota(int CON, Competencia Compet, NOATipo Tipo,int Numero):base(CON,Compet,Tipo,Numero)
        {            
        }

        /// <summary>
        /// Cria uma nova nota sem os pagamentos
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="FRN"></param>
        /// <param name="NumeroNota"></param>
        /// <param name="DataEmissao"></param>
        /// <param name="ValorTotal"></param>
        /// <param name="Tipo"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="servico"></param>
        /// <param name="Comp"></param>
        /// <param name="ArquivoNotaPDF"></param>
        /// <param name="ArquivoNotaXML"></param>
        /// <param name="PAGTipo"></param>
        /// <param name="NOAStatus"></param>
        /// <param name="Obs"></param>
        /// <param name="DadosPag"></param>
        /// <param name="PGF"></param>
        /// <param name="_EMPTProc1"></param>
        public Nota(int CON, int? FRN, int NumeroNota, DateTime DataEmissao, decimal ValorTotal,NOATipo Tipo,string PLA,int SPL,string servico,
                    Competencia Comp, string ArquivoNotaPDF = "", string ArquivoNotaXML = "", PAGTipo PAGTipo = PAGTipo.cheque, NOAStatus NOAStatus = NOAStatus.SemPagamentos,
                    string Obs = null, string DadosPag = null, int? PGF = null, EMPTProc _EMPTProc1 = null)
            : base(CON, FRN, NumeroNota, DataEmissao, ValorTotal, Tipo, PLA, SPL, servico, Comp, ArquivoNotaPDF , ArquivoNotaXML , PAGTipo, NOAStatus, Obs, DadosPag, PGF, _EMPTProc1)
        {                                                  
        }

        [Obsolete("pegar pelo condomínio")]
        private bool TributoEletronico(int CON)
        {
            return EMPTProc1.STTA.BuscaEscalar_bool("select CONGPSEletronico from condominios where CON = @P1", CON).GetValueOrDefault(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SolRetencoes"></param>
        public void CadastraRetencaoNaRam(List<dllImpostos.SolicitaRetencao> SolRetencoes)
        {
            foreach (dllImpostos.SolicitaRetencao Sol in SolRetencoes)
            {
                if (Sol == null)
                    continue;
                ImpostoNeonProc Imp = new ImpostoNeonProc(Sol.Tipo);
                Imp.DataNota = LinhaMae.NOADataEmissao;
                Imp.DataPagamento = LinhaMae.NOADataEmissao.AddMonths(1);
                Imp.ValorBase = linhaMae.NOATotal;
                if ((Sol.Tipo == TipoImposto.ISSQN) || (Sol.Tipo == TipoImposto.ISS_SA))
                    Imp.Competencia = new Competencia(LinhaMae.NOACompet);
                PAGTipo RetPAGTipo;
                if (Sol.Tipo.EstaNoGrupo(TipoImposto.ISSQN, TipoImposto.ISS_SA, TipoImposto.INSSpf, TipoImposto.INSSpfEmp, TipoImposto.INSSpfRet)
                    ||
                    !TributoEletronico(LinhaMae.NOA_CON)
                   )

                    RetPAGTipo = PAGTipo.Guia;
                else
                    RetPAGTipo = PAGTipo.PEGuia;
                dNOtAs.PAGamentosRow Improw = DNOtAs.PAGamentos.NewPAGamentosRow();
                Improw.PAGPermiteAgrupar = true;
                Improw.PAG_NOA = LinhaMae.NOA;
                Improw.PAGTipo = (int)RetPAGTipo;
                Improw.PAGIMP = (int)Sol.Tipo;
                Improw.PAGValor = Sol.Valor == 0 ? Imp.ValorImposto : Sol.Valor;
                //if ((Sol.Descontar == SimNao.Sim) || ((Sol.Descontar == SimNao.Padrao) && (Imp.Desconta())))
                //    ValorPagamento -= Improw.PAGValor;
                Improw.PAGVencimento = Imp.VencimentoEfetivo;
                Improw.CHEStatus = (int)CHEStatus.Cadastrado;
                Improw.CHEFavorecido = Imp.Nominal();
                DNOtAs.PAGamentos.AddPAGamentosRow(Improw);
            }
        }

        

        

        #endregion       

        /// <summary>
        /// Destrava nota
        /// </summary>
        /// <param name="NOADataEmissao"></param>
        /// <param name="DataVencimento"></param>
        /// <param name="Valor"></param>
        /// <param name="DadosAd"></param>
        internal void Destrava(DateTime NOADataEmissao, DateTime DataVencimento, decimal Valor, /*Follow.PagamentoPeriodico.TipoCadastraProximoPagamento TipoCad, decimal ValorAjusteParcelasAcumuladas,*/string DadosAd = "")
        {            
            try
            {
                //System.Windows.Forms.MessageBox.Show("1");
                EMPTProc1.AbreTrasacaoSQL("Nota.cs Destrava - 368", DNOtAs.NOtAsTableAdapter, DNOtAs.PAGamentosTableAdapter);                
                LinhaMae.NOADataEmissao = NOADataEmissao;
                linhaMae.NOATotal = Valor;
                Status = NOAStatus.Cadastrada;

                /*
                switch (TipoCad)
                {
                    case ContaPagar.Follow.PagamentoPeriodico.TipoCadastraProximoPagamento.efetivo:
                        Status = NOAStatus.Cadastrada;
                        break;
                    /*
                case ContaPagar.Follow.PagamentoPeriodico.TipoCadastraProximoPagamento.acumular:
                    Status = NOAStatus.NotaAcumulada;
                    break;
                case ContaPagar.Follow.PagamentoPeriodico.TipoCadastraProximoPagamento.acumularZerado:
                    Status = NOAStatus.CadastradaAjustarValor;
                    break;
                    case ContaPagar.Follow.PagamentoPeriodico.TipoCadastraProximoPagamento.acumular:                                                
                    case ContaPagar.Follow.PagamentoPeriodico.TipoCadastraProximoPagamento.acumularZerado:
                        Status = NOAStatus.NotaCancelada;
                        break; 
                     default:
                        throw new NotImplementedException(string.Format("TipoCad = {0}", TipoCad));                        
                }*/
                //System.Windows.Forms.MessageBox.Show("2");
                switch ((CHEStatus)rowPAGUnica.CHEStatus)
                {
                    case CHEStatus.CadastradoBloqueado:
                    case CHEStatus.DebitoAutomatico:
                        //System.Windows.Forms.MessageBox.Show("21");
                        rowPAGUnica.PAGValor = Valor;
                        rowPAGUnica.PAGVencimento = DataVencimento;
                        if (DadosAd != null)
                            rowPAGUnica.PAGAdicional = DadosAd;                                                
                        if (Valor == 0)                           
                            rowPAGUnica.CHEStatus = (int)CHEStatus.Cancelado;
                        //System.Windows.Forms.MessageBox.Show("22");
                        AjustaParcelaNoCheque(rowPAGUnica, false);
                        //System.Windows.Forms.MessageBox.Show("23");
                        break;
                }
                //System.Windows.Forms.MessageBox.Show("F1");
                /*
                if (
                    (rowPAGUnica.CHEStatus == (int)CHEStatus.CadastradoBloqueado)
                    ||
                    (rowPAGUnica.CHEStatus == (int)CHEStatus.DebitoAutomatico)
                   )
                {
                    rowPAGUnica.PAGValor = Valor;
                    rowPAGUnica.PAGVencimento = DataVencimento;
                    if (TipoCad == Follow.PagamentoPeriodico.TipoCadastraProximoPagamento.efetivo)
                        AjustaParcelaNoCheque(rowPAGUnica, false);
                    else
                    {
                        DNOtAs.PAGamentosTableAdapter.Update(rowPAGUnica);
                        rowPAGUnica.AcceptChanges();
                    }
                }*/

                DNOtAs.NOtAsTableAdapter.Update(LinhaMae);
                //System.Windows.Forms.MessageBox.Show("F2");
                LinhaMae.AcceptChanges();
                //System.Windows.Forms.MessageBox.Show("F3");
                Periodico.CadastrarProximo();
                //System.Windows.Forms.MessageBox.Show("F4");
                //if (TipoCad != Follow.PagamentoPeriodico.TipoCadastraProximoPagamento.efetivo)
                //  Periodico.CadastrarProximo();
                //else
                //    Periodico.ReagendaAcumulados(DataVencimento, false, ValorAjusteParcelasAcumuladas);
                EMPTProc1.Commit();
                //System.Windows.Forms.MessageBox.Show("F5");
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro ao destravar nota",e);
            }

        }

        /*
        /// <summary>
        /// Objeto com as informações para cadastro dos pagamentos/Cheques
        /// </summary>
        public class PreCheque 
        {
            /// <summary>
            /// Nominal do pagamentos
            /// </summary>
            public string Nominal { get; set; }
            /// <summary>
            /// Valor
            /// </summary>
            public decimal Valor { get; set; }
            /// <summary>
            /// Vencimento
            /// </summary>
            public DateTime DataVencimento { get; set; }
            /// <summary>
            /// Tipo do cheque
            /// </summary>
            public PAGTipo Tipo { get; set; }
        }

        /// <summary>
        /// Destrava nota
        /// </summary>
        /// <param name="NOADataEmissao"></param>
        /// <param name="PreCheques"></param>
        public void DestravaCadastrar(DateTime NOADataEmissao,  List<PreCheque> PreCheques)
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("DestravaCadastrar A");
            try
            {
                EMPTProc1.AbreTrasacaoSQL("Nota.cs Destrava - 368", DNOtAs.NOtAsTableAdapter, DNOtAs.PAGamentosTableAdapter);
                //bool Primeira = false;
                if (Status == NOAStatus.NotaProvisoria)
                {
                    LinhaMae.NOADataEmissao = NOADataEmissao;
                    linhaMae.NOATotal = 0;                    
                    //Primeira = true;
                }
                foreach (PreCheque Pre in PreCheques)
                {
                    linhaMae.NOATotal += Pre.Valor;
                    dNOtAs.PAGamentosRow rowPag;
                    if (Status == NOAStatus.NotaProvisoria)
                    {
                        Status = NOAStatus.Cadastrada;
                        rowPag = rowPAGUnica;                                                                                                
                    }
                    else
                    {
                        rowPag = DNOtAs.PAGamentos.NewPAGamentosRow();                        
                        rowPag.PAG_NOA = NOA;
                        rowPag.PAGDATAI = DateTime.Now;
                        rowPag.PAGI_USU = EMPTProc1.USU;                        
                        rowPag.PAGPermiteAgrupar = true;                        
                    }
                    rowPag.PAGValor = Pre.Valor;
                    rowPag.PAGVencimento = Pre.DataVencimento;
                    rowPag.CHEFavorecido = Pre.Nominal;
                    rowPag.PAGTipo = (int)Pre.Tipo;      
                    if(rowPag.RowState == DataRowState.Detached)
                        DNOtAs.PAGamentos.AddPAGamentosRow(rowPag);
                    AjustaParcelaNoCheque(rowPag);
                };
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("UPDATE Nota");
                DNOtAs.NOtAsTableAdapter.Update(LinhaMae);
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("DestravaCadastrar A");
                LinhaMae.AcceptChanges();
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro ao destravar nota", e);
            }
        }

        

        private dNOtAs.PAGamentosRow _rowPAGUnica;

        private dNOtAs.PAGamentosRow rowPAGUnica
        {
            get 
            {
                if (_rowPAGUnica == null)
                {
                    if (DNOtAs.PAGamentos.Count == 0)
                        DNOtAs.PAGamentosTableAdapter.FillByNOA(DNOtAs.PAGamentos, NOA);
                    if (DNOtAs.PAGamentos.Count != 1)
                        throw new NotImplementedException("Não implementado.Somente previsto para contas onde temos somente 1 pag");
                    _rowPAGUnica = LinhaMae.GetPAGamentosRows()[0];
                };
                return _rowPAGUnica;
            }
        }*/

        private Follow.PagamentoPeriodico _Periodico;

        /// <summary>
        /// Periódico
        /// </summary>
        public Follow.PagamentoPeriodico Periodico
        {
            get 
            {
                if((_Periodico == null) && (!LinhaMae.IsNOA_PGFNull()))
                    _Periodico = new Follow.PagamentoPeriodico(LinhaMae.NOA_PGF);
                return _Periodico;
            }
            set
            {
                _Periodico = value;
            }
        }

        

        
    }
}
