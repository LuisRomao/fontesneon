﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Framework.Alarmes;
using Framework.objetosNeon;
using Framework.datasets;
using VirEnumeracoesNeon;
using ContaPagarProc;

namespace ContaPagar.ContasAgLuz
{
    /// <summary>
    /// Tratamento para débito automático
    /// </summary>
    public partial class cTratamentoDebAut : cTratamentoBase
    {
        private dllCheques.Cheque cheque;

        private int? CCD;
        private int? CHE;
        private Follow.PagamentoPeriodico periodico;
        //private int? PGF;

        private ATVTipo Tipo
        { get { return (ATVTipo)(Aviso1.LinhaMae.AVI_TAV); } }

        private System.Collections.ArrayList TiposcomChaveCCD
        {
            get { return new System.Collections.ArrayList(new ATVTipo[] { ATVTipo.DebitoAutomaticoComValorIncorreto, ATVTipo.DebitoAutomaticoNaoPrevisto}); }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="AVI"></param>
        public cTratamentoDebAut(int AVI):base(AVI)
        {
            InitializeComponent();
            CHE = null;
            //PGF = null;
            periodico = null;
            cheque = null;
            CCD = null;
            cBotaoIntegradorN.Enabled = cBotaoIntegradorC.Enabled = cBotaoIntegradorP.Enabled = false;
            if (Abortar_Por_Erro)
                return;            
            Framework.Enumeracoes.virEnumstatusconsiliado.virEnumstatusconsiliadoSt.CarregaEditorDaGrid(colCCDConsolidado);
            Framework.Enumeracoes.virEnumTipoLancamentoNeon.virEnumTipoLancamentoNeonSt.CarregaEditorDaGrid(colCCDTipoLancamento);
            if (TiposcomChaveCCD.Contains(Tipo))
            {
                CCD = Aviso1.LinhaMae.AVIChave;
                CHE = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select che from CHEques where CHE_CCD = @P1", CCD);
                simpleButton1.Text = "Aguardar";
                simpleButton2.Text = "Cancelar aviso";
                simpleButton3.Visible = false;
            }
            else if(Tipo == ATVTipo.DebitoAutomaticoNaoOcorreu1)                            
                periodico = new Follow.PagamentoPeriodico(Aviso1.LinhaMae.AVIChave);            
            else            
                CHE = Aviso1.LinhaMae.AVIChave;                            
            DateTime DI = DateTime.Today.AddDays(-1);
            if (CHE.HasValue)
            {
                cheque = new dllCheques.Cheque(CHE.Value,dllCheques.Cheque.TipoChave.CHE);
                if (!cheque.Encontrado)
                    throw new Exception(string.Format("Erro em cTratamentoDebAut: Cheque eletrônico não encontrado AVIrow.AVIChave = <{0}>", Aviso1.LinhaMae.AVIChave));
                dateEdit1.DateTime = cheque.CHErow.CHEVencimento;
                calcEdit1.Value = cheque.CHErow.CHEValor;
                DI = cheque.CHErow.CHEVencimento.AddDays(-4);
            }                                    
            DateTime DF = DateTime.Today;            
            bool ChaveEncontradas = false;
            if (periodico != null)
            {
                if (!Aviso1.LinhaMae.IsAVICompNull())
                {
                    cCompet1.Comp = new Competencia(Aviso1.LinhaMae.AVIComp);
                    NotaProc NotaCadastrada = periodico.BuscaCriaNotaProc(cCompet1.Comp, false);
                    if (NotaCadastrada != null)
                    {
                        cBotaoIntegradorN.Enabled = true;
                        cBotaoIntegradorN.Chave = NotaCadastrada.NOA;
                        ContaPagarProc.dNOtAs.PAGamentosRow rowPAG = NotaCadastrada.rowPAGUnica;
                        if (rowPAG != null)
                        {
                            CHE = rowPAG.PAG_CHE;
                            cheque = new dllCheques.Cheque(CHE.Value, dllChequesProc.ChequeProc.TipoChave.CHE);
                            dateEdit1.DateTime = cheque.Vencimento;
                            calcEdit1.Value = cheque.Valor;
                            DateTime Dteste = cheque.Vencimento.AddDays(-4);
                            if (Dteste < DI)
                                DI = Dteste;
                        }
                    }
                }
                ChaveEncontradas = dExtratos.BuscaChavesPGF(periodico.PGF);
            }

            else if (CHE.HasValue)
            {
                ChaveEncontradas = dExtratos.BuscaChaves(CHE.Value);
                if (ChaveEncontradas)
                {
                    cCompet1.Comp = dExtratos.Comp;
                    calcEdit1.Value = dExtratos.NOATotal;
                };
            }
            
            textEditDescricao.Visible = false;
            textEditFornecedor.Visible = false;

            if (ChaveEncontradas)
            {
                labelCONDOMINIO.Text = dExtratos.strCondominio;
                if (dExtratos.NOA.HasValue)
                {
                    cBotaoIntegradorN.Enabled = true;
                    cBotaoIntegradorN.Chave = dExtratos.NOA.Value;
                }
                if (dExtratos.PGF.HasValue)
                {
                    cBotaoIntegradorP.Enabled = true;
                    cBotaoIntegradorP.Chave = dExtratos.PGF.Value;
                }
                if (CHE.HasValue)
                {
                    cBotaoIntegradorC.Enabled = true;
                    cBotaoIntegradorC.Chave = CHE.Value;                    
                }
                else
                {                    
                    if (periodico != null)
                    {                        
                        dateEdit1.DateTime = periodico.ProximaData;
                        cCompet1.Comp = periodico.ProximaCompetencia;                                             
                    }
                    else
                        if (!Aviso1.LinhaMae.IsAVICompNull())
                            cCompet1.Comp = new Competencia(Aviso1.LinhaMae.AVIComp);
                    DI = dateEdit1.DateTime.AddDays(-4);
                    simpleButton1.Enabled = false;
                    simpleButton2.Enabled = false;
                    simpleButton3.Enabled = false;
                    calcEdit1.Value = dExtratos.PGFValor;
                }
                spinEdit1.Value = dExtratos.longIdent;
                spinEditIdent.Value = dExtratos.intIdent;
                if (dExtratos.DescricaoPer != null)                                    
                {
                    textEditDescricao.Text = dExtratos.DescricaoPer;
                    textEditDescricao.Visible = true;
                }
                textEditFornecedor.Visible = dExtratos.FRN.HasValue;
                if (dExtratos.FRN.HasValue)
                    textEditFornecedor.Text = dExtratos.Fornecedor;
            }
            else
            {
                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = null;
                if (!Aviso1.LinhaMae.IsAVI_CONNull())
                    rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(Aviso1.LinhaMae.AVI_CON);
                if (rowCON != null)
                    labelCONDOMINIO.Text = string.Format("{0} - {1}", rowCON.CONCodigo, rowCON.CONNome);
                else
                    labelCONDOMINIO.Visible = false;
            }

            if (!CCD.HasValue)
            {
                dExtratos.ContaCorrenTeTableAdapter.Fill(dExtratos.ContaCorrenTe, DI, DF, Aviso1.LinhaMae.AVI_CON);
                dExtratos.SaldoContaCorrenteTableAdapter.Fill(dExtratos.SaldoContaCorrente, DI, DF, Aviso1.LinhaMae.AVI_CON);
                dExtratos.ContaCorrenteDetalheTableAdapter.Fill(dExtratos.ContaCorrenteDetalhe, DI, DF, Aviso1.LinhaMae.AVI_CON);                
            }
            else
            {
                dExtratos.ContaCorrenTeTableAdapter.FillByCCD(dExtratos.ContaCorrenTe, CCD.Value);
                if (dExtratos.SaldoContaCorrenteTableAdapter.FillByCCD(dExtratos.SaldoContaCorrente, CCD.Value) > 0)
                    dExtratos.ContaCorrenteDetalheTableAdapter.FillBySCC(dExtratos.ContaCorrenteDetalhe, dExtratos.SaldoContaCorrente[0].SCC);
            }
            dExtratos.CalculaSaldoFinal();

            cCompet1.AjustaTela();
            
            
            
        }

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.Name == colSCCData.Name)
            {
                if ((e.CellValue != DBNull.Value) && (e.CellValue != null) && (e.CellValue is DateTime))
                    if ((DateTime)e.CellValue == dateEdit1.DateTime)
                        e.Appearance.ForeColor = Color.Red;
            }
        }

        private void gridControl1_Load(object sender, EventArgs e)
        {
            for (int hand = 0; hand < gridView1.RowCount; hand++)
            {
                if ((((dExtratos.ContaCorrenTeRow)gridView1.GetDataRow(hand)).CCTPrincipal) || (CCD != 0))
                {
                    gridView1.ExpandMasterRow(hand);
                    DevExpress.XtraGrid.Views.Grid.GridView GridSCC = (DevExpress.XtraGrid.Views.Grid.GridView)gridView1.GetDetailView(hand, 0);
                    for (int hand1 = 0; hand1 < GridSCC.RowCount; hand1++)
                    {
                        if ((((dExtratos.SaldoContaCorrenteRow)GridSCC.GetDataRow(hand1)).SCCData == dateEdit1.DateTime) || (CCD != 0))
                        {
                            GridSCC.ExpandMasterRow(hand1);
                        }
                    }
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (TiposcomChaveCCD.Contains(Tipo))
            {
                FechaTela(DialogResult.OK);
            }
            else
                if (MessageBox.Show("Confirma que o fato deste débito não ter ocorrido deve ser ignorado?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    string Justificativa = "";
                    if (VirInput.Input.Execute("Justificativa:", ref Justificativa, true) && (Justificativa != null))
                    {
                        try
                        {

                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cTratamentoDebAut - 211");
                            cheque.Status = CHEStatus.BaixaManual;
                            cheque.GravaObs(string.Format("Débito não ocorreu e o pagamento foi ignorado com a justificativa:\r\n{0}\r\nDescrição do ALARME:{1}\r\nHistórico do alarme:\r\n{1}", Justificativa, Aviso1.LinhaMae.AVIDescricao, Aviso1.LinhaMae.AVIHistorico));


                            int? Gerente = Framework.Lookup.LookupGerente.GetGerente(Aviso1.LinhaMae.AVI_CON);
                            if (Gerente.HasValue)
                                Aviso1.Delete();
                                //Aviso1.ReEdita(Framework.datasets.ATVTipo.Gererico, Gerente, string.Format("{0:dd/MM/yyyy HH:mm:ss} - {1}:\r\nAviso ignorado com a justificativa:\r\n{2}\r\n", DateTime.Now, CompontesBasicos.FormPrincipalBase.USULogadoNome, Justificativa.Trim()));

                            VirMSSQL.TableAdapter.STTableAdapter.Commit();
                            FechaTela(DialogResult.OK);
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                        }
                    }
                }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (TiposcomChaveCCD.Contains(Tipo))
            {                
                if (!CCD.HasValue)
                {
                    if (MessageBox.Show("O cancelamento do aviso irá remover da previsão de caixa o saldo do lançamento. Confirma?", "CONFIRMAÇÂO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        dExtratos.CHEquesTableAdapter.Fill(dExtratos.CHEques, dExtratos.NOA.Value, (int)CHEStatus.DebitoAutomatico);
                        foreach (dExtratos.CHEquesRow rowCHE in dExtratos.CHEques)
                        {
                            dllCheques.Cheque ChequeCancelar = new dllCheques.Cheque(rowCHE.CHE,dllCheques.Cheque.TipoChave.CHE);
                            if (ChequeCancelar.Encontrado)
                                ChequeCancelar.CancelarPagamento("Saldo Cancelado\r\nDados do aviso:\r\n" + Aviso1.LinhaMae.AVIHistorico);
                        }
                        Aviso1.Delete();
                        FechaTela(DialogResult.OK);
                    }
                }
                else
                {
                    if (MessageBox.Show("Confirma o cancelamento do aviso?", "CONFIRMAÇÂO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {                        
                        Aviso1.Delete();
                        FechaTela(DialogResult.OK);
                    }
                }
            }
            else
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cTratamentoDebAut - 263");
                    cheque.CHErow.CHETipo = (int)PAGTipo.cheque;
                    cheque.Status = CHEStatus.Cadastrado;
                    cheque.GravaObs(string.Format("Débito não ocorreu e o pagamento foi cadastrado como cheque\r\nDescrição do ALARME:{0}\r\nHistórico do alarme:\r\n{0}", Aviso1.LinhaMae.AVIDescricao, Aviso1.LinhaMae.AVIHistorico));
                    Aviso1.Delete();
                    FechaTela(DialogResult.OK);
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                }
        }

        private List<statusconsiliado> _StatusValidos;

        private List<statusconsiliado> StatusValidos
        {
            get 
            {
                if (_StatusValidos == null)
                {
                    _StatusValidos = new List<statusconsiliado>();
                    _StatusValidos.Add(statusconsiliado.AguardaConta);
                    _StatusValidos.Add(statusconsiliado.Aguardando);
                    _StatusValidos.Add(statusconsiliado.Antigo);
                    _StatusValidos.Add(statusconsiliado.ChequeNaoEncontrado);
                    //_StatusValidos.Add(statusconsiliado.Deposito);
                    //_StatusValidos.Add(statusconsiliado.Manual);
                }
                return _StatusValidos; 
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView GridEmFoco = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
            if (GridEmFoco == null)
                MessageBox.Show("selecione a linha do extrato que se refere a este pagamento");
            else
            {
                DataRow DR = GridEmFoco.GetFocusedDataRow();
                if (!(DR is dExtratos.ContaCorrenteDetalheRow))
                    MessageBox.Show("selecione a linha do extrato que se refere a este pagamento");
                else
                {
                    dExtratos.ContaCorrenteDetalheRow rowCCD = (dExtratos.ContaCorrenteDetalheRow)DR;
                    if (rowCCD == null)
                        MessageBox.Show("selecione a linha do extrato que se refere a este pagamento");
                    else
                    {
                        statusconsiliado CCDConsolidado = (statusconsiliado)rowCCD.CCDConsolidado;
                        if (!StatusValidos.Contains(CCDConsolidado))
                        {
                            MessageBox.Show("A linha selecionada já está consolidada");
                        } 
                        else
                        {
                            if (rowCCD.CCDValor != -calcEdit1.Value)
                                if (MessageBox.Show("A linha selecionada tem valor diferente da conta. Confirma ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                                    return;                            
                            AbstratosNeon.ABS_Conciliacao Conciliacao = AbstratosNeon.ABS_Conciliacao.Conciliacao();
                            Competencia Comp = null;
                            if (!Aviso1.LinhaMae.IsAVICompNull())
                                Comp = new Competencia(Aviso1.LinhaMae.AVIComp);
                            statusconsiliado Retorno = Conciliacao.ConciliarDebitoAutomatico(rowCCD.CCD, dExtratos.PGF.Value, cheque.CHErow.CHEVencimento,Comp);                            
                            if (Retorno == statusconsiliado.Automatico)
                                FechaTela(DialogResult.OK);
                        }
                    }
                }
            }           
            
        }

        private Font _FIta;

        private Font FIta(Font Prototipo)
        {
            return _FIta ?? (_FIta = new Font(Prototipo, FontStyle.Italic));
        }

        private void gridView3_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                DevExpress.XtraGrid.Views.Grid.GridView Grid = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                dExtratos.ContaCorrenteDetalheRow CCDrow = (dExtratos.ContaCorrenteDetalheRow)Grid.GetDataRow(e.RowHandle);                
                if (CCDrow.CCD == CCD)
                {
                    e.Appearance.BackColor = Color.LightPink;
                    e.Appearance.ForeColor = Color.Black;
                }
                if (!CCD.HasValue)
                {
                    if (!StatusValidos.Contains((statusconsiliado)CCDrow.CCDConsolidado))                    
                    {
                        e.Appearance.BackColor = Color.FromArgb(210, 210, 210); //200 escuro
                        e.Appearance.ForeColor = Color.FromArgb(70, 70, 70);//50 escuro de mais
                        //e.Appearance.BackColor = Color.LightGray; 
                        //e.Appearance.BackColor2 = Color.LightGray; 
                        //e.Appearance.ForeColor = Color.Gray;
                        e.Appearance.Font = FIta(e.Appearance.Font);
                    }
                }

            }
        }

        private void gridControl1_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
        {        
            DevExpress.XtraGrid.Views.Grid.GridView view = e.View as DevExpress.XtraGrid.Views.Grid.GridView;

            if (view != null)

                view.DetailHeight = 1000;

        }

        private void panelControl2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelTipo_Click(object sender, EventArgs e)
        {

        }

        
    }
}
