using System;
using Framework.objetosNeon;

namespace ContaPagar.ContasAgLuz
{
    /// <summary>
    /// 
    /// </summary>
    public class ContaAgLuz
    {
        /// <summary>
        /// 
        /// </summary>
        public bool Valido;

        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;

        /// <summary>
        /// 
        /// </summary>
        public TipoConta Tipo;

        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;

        /// <summary>
        /// 
        /// </summary>
        public CodigosFornecedorCodBar fornecedor;

        /// <summary>
        /// 
        /// </summary>
        public long RGI;

        /// <summary>
        /// 
        /// </summary>
        public Competencia comp;

        /// <summary>
        /// 
        /// </summary>
        public DateTime vencimento;


        /// <summary>
        /// 
        /// </summary>
        public bool RGIAlternativo;

        /// <summary>
        /// 
        /// </summary>
        public int FRN;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CodigoDeBarras"></param>
        public ContaAgLuz(string CodigoDeBarras)
        {
            comp = null;
            if ((CodigoDeBarras.Length != 44) || (CodigoDeBarras[0] != '8'))
            {
                Valido = false;
                UltimoErro = new Exception(string.Format("C�digo de barras inv�lido: <{0}>)", CodigoDeBarras));
                return;
            };
            foreach (char c in CodigoDeBarras)
                if (!char.IsDigit(c))
                {
                    Valido = false;
                    UltimoErro = new Exception(string.Format("C�digo de barras inv�lido: <{0}>\r\nCaractere: <{1}>)", CodigoDeBarras,c));
                    return;
                }
            Valido = true;
            switch (CodigoDeBarras[1])
            {
                case '2':
                    Tipo = TipoConta.Agua;
                    break;
                case '3':
                    Tipo = TipoConta.Luz;
                    break;
                case '4':
                    Tipo = TipoConta.Telefone;
                    break;
                default:
                    Tipo = TipoConta.Erro;
                    break;
            };
            if (Tipo == TipoConta.Erro)
            {
                Valido = false;
                UltimoErro = new Exception(string.Format("C�digo de barras com tipo inv�lido: <{0}>\r\nTipo: <{1}>)", CodigoDeBarras, CodigoDeBarras[1]));
                return;
            }
            if ((CodigoDeBarras[2] != '6') && (CodigoDeBarras[2] != '8'))
            {
                Valido = false;
                UltimoErro = new Exception(string.Format("N�o implementado 'Identificador de Valor Efetivo ou Refer�ncia' <> 6 : <{0}>)", CodigoDeBarras, CodigoDeBarras[1]));
                return;
            };
            Valor = decimal.Parse(CodigoDeBarras.Substring(4, 11))/100;
            fornecedor = (CodigosFornecedorCodBar)int.Parse(CodigoDeBarras.Substring(15, 4));
            if (fornecedor == CodigosFornecedorCodBar.VIVO)
                fornecedor = CodigosFornecedorCodBar.TELEFONICA;
            RGIAlternativo = false;
            vencimento = DateTime.MinValue;

            //REFERENCIA ONENOTE debito passo 1

            FRN = 0;

            if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select FRN from FORNECEDORES where FRNCodigoDebito = @P1", out FRN, fornecedor))
            {
                Valido = false;
                UltimoErro = new Exception(string.Format("Fornecedor n�o cadastrado. <{0}>",fornecedor));
                return;
            }

            //CodigosFornecedorCodBar CodFor = fornecedor;
            DateTime? VencLido;
            int dia;
            int mes;
            int ano;
            switch (fornecedor)
            {
                case CodigosFornecedorCodBar.DAESCS:
                    RGI = long.Parse(CodigoDeBarras.Substring(25, 8));
                    dia = int.Parse(CodigoDeBarras.Substring(19, 2));
                    mes = int.Parse(CodigoDeBarras.Substring(21, 2));
                    ano = int.Parse(CodigoDeBarras.Substring(23, 2))+2000;
                    vencimento = new DateTime(ano, mes, dia);
                    comp = new Competencia(int.Parse(CodigoDeBarras.Substring(33, 4) + CodigoDeBarras.Substring(37, 2)));
                    break;
                case CodigosFornecedorCodBar.VIVO:
                    RGI = long.Parse(CodigoDeBarras.Substring(20, 12));
                    //RGI = long.Parse(CodigoDeBarras.Substring(34, 9));
                    vencimento = DateTime.Today;
                    VencLido = DateTime.Today;
                    if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                    {
                        Valido = false;
                        return;
                    }
                    vencimento = VencLido.Value;
                    break;
                case CodigosFornecedorCodBar.SABESP:
                    RGI = long.Parse(CodigoDeBarras.Substring(24, 8));
                    comp = new Competencia(int.Parse(string.Format("20{0}{1}", CodigoDeBarras.Substring(39, 2), CodigoDeBarras.Substring(41, 2))));
                    //FRN = 1595;
                    RGIAlternativo = false;
                    vencimento = DateTime.Today;
                    VencLido = DateTime.Today;
                    if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                    {
                        Valido = false;
                        return;
                    }
                    vencimento = VencLido.Value;
                    break;
                case CodigosFornecedorCodBar.ELETROPAULO:
                    RGI = long.Parse(CodigoDeBarras.Substring(32, 12));
                    RGIAlternativo = true;
                    vencimento = DateTime.Today;
                    VencLido = DateTime.Today;
                    if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                    {
                        Valido = false;
                        return;
                    }
                    vencimento = VencLido.Value;
                    break;
                case CodigosFornecedorCodBar.SANED:
                    RGI = long.Parse(CodigoDeBarras.Substring(22, 6));
                    comp = new Competencia(int.Parse(CodigoDeBarras.Substring(30, 4) + CodigoDeBarras.Substring(28, 2)));
                    dia = int.Parse(CodigoDeBarras.Substring(34, 2));
                    mes = int.Parse(CodigoDeBarras.Substring(36, 2));
                    ano = int.Parse(CodigoDeBarras.Substring(38, 4));
                    vencimento = new DateTime(ano, mes, dia);
                    break;
                case CodigosFornecedorCodBar.CONGAS:
                    Tipo = TipoConta.Gas;
                    //FRN = 602;
                    RGI = long.Parse(CodigoDeBarras.Substring(36, 7));
                    vencimento = DateTime.Today;
                    VencLido = DateTime.Today;
                    if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                    {
                        Valido = false;
                        return;
                    };
                    vencimento = VencLido.Value;
                    break;
                case CodigosFornecedorCodBar.NET1:
                case CodigosFornecedorCodBar.NET2:
                    Tipo = TipoConta.TV;
                    //FRN = 330;
                    //RGI = long.Parse(CodigoDeBarras.Substring(36, 7));
                    //comp = new Competencia(int.Parse(CodigoDeBarras.Substring(30, 4) + CodigoDeBarras.Substring(28, 2)));
                    dia = int.Parse(CodigoDeBarras.Substring(25, 2));
                    mes = int.Parse(CodigoDeBarras.Substring(23, 2));
                    ano = int.Parse(CodigoDeBarras.Substring(19, 4));
                    vencimento = new DateTime(ano, mes, dia);
                    RGI = 0;
                    if (!VirInput.Input.Execute("C�digo NET", out RGI))
                    {
                        Valido = false;
                        return;
                    }
                    break;
                case CodigosFornecedorCodBar.TELEFONICA:
                    if (CodigoDeBarras.Substring(38, 6) == "999999")
                    {
                        if (!VirInput.Input.Execute("N�mero do telefone", out RGI))
                        {
                            Valido = false;
                            return;
                        }
                        vencimento = DateTime.Today;
                        VencLido = DateTime.Today;
                        if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                        {
                            Valido = false;
                            return;
                        }
                        vencimento = VencLido.Value;
                    }
                    else
                    {
                        RGI = long.Parse(CodigoDeBarras.Substring(24, 8));
                        comp = new Competencia(int.Parse("20" + CodigoDeBarras.Substring(35, 2) + CodigoDeBarras.Substring(33, 2)));
                        dia = int.Parse(CodigoDeBarras.Substring(42, 2));
                        mes = int.Parse(CodigoDeBarras.Substring(40, 2));
                        ano = int.Parse("20" + CodigoDeBarras.Substring(38, 2));
                        vencimento = new DateTime(ano, mes, dia);
                    }
                    break;
                case CodigosFornecedorCodBar.ULTRAGAZ:
                    Tipo = TipoConta.Gas;
                    //FRN = 190;
                    if (!VirInput.Input.Execute("C�digo do cliente", out RGI))
                    {
                        Valido = false;
                        return;
                    }

                    dia = int.Parse(CodigoDeBarras.Substring(25, 2));
                    mes = int.Parse(CodigoDeBarras.Substring(23, 2));
                    ano = int.Parse(CodigoDeBarras.Substring(19, 4));
                    vencimento = new DateTime(ano, mes, dia);
                    break;
                case CodigosFornecedorCodBar.LIVRE:
                    RGI = long.Parse(CodigoDeBarras.Substring(21, 10));
                    vencimento = DateTime.Today;
                    VencLido = DateTime.Today;
                    if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                    {
                        Valido = false;
                        return;
                    }
                    vencimento = VencLido.Value;
                    break;
                case CodigosFornecedorCodBar.NEXTEL:                    
                    if (!VirInput.Input.Execute("Identifica��o do cliente", out RGI))
                    {
                        Valido = false;
                        return;
                    }
                    vencimento = DateTime.Today;
                    VencLido = DateTime.Today;
                    if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                    {
                        Valido = false;
                        return;
                    }
                    vencimento = VencLido.Value;
                    break;
                case CodigosFornecedorCodBar.CLARO:
                    RGI = long.Parse(CodigoDeBarras.Substring(27, 9));
                    RGIAlternativo = false;
                    dia = int.Parse(CodigoDeBarras.Substring(25, 2));
                    mes = int.Parse(CodigoDeBarras.Substring(23, 2));
                    ano = int.Parse(CodigoDeBarras.Substring(19, 4));
                    vencimento = new DateTime(ano, mes, dia);
                    break;
                case CodigosFornecedorCodBar.TIM:
                    if (!VirInput.Input.Execute("Iden. d�bito (sem dig.)", out RGI))
                    {
                        Valido = false;
                        return;
                    }
                    RGIAlternativo = false;
                    vencimento = DateTime.Today;
                    VencLido = DateTime.Today;
                    if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                    {
                        Valido = false;
                        return;
                    }
                    vencimento = VencLido.Value;
                    break;
                case CodigosFornecedorCodBar.SEMASA:
                    RGI = long.Parse(CodigoDeBarras.Substring(27, 8));
                    dia = int.Parse(CodigoDeBarras.Substring(25, 2));
                    mes = int.Parse(CodigoDeBarras.Substring(23, 2));
                    ano = int.Parse(CodigoDeBarras.Substring(19, 4));
                    vencimento = new DateTime(ano, mes, dia);
                    break;
                default:
                    Valido = false;
                    UltimoErro = new Exception(string.Format("N�o implementado 'fornecedor' <0>  : <{1}>)", fornecedor, CodigoDeBarras));
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", UltimoErro.Message, "Debito autom�tico n�o configurado");
                    return;
            }

            /*
            if (fornecedor == 82) //GVT
            {
                /*
                RGI = long.Parse(CodigoDeBarras.Substring(34, 9));
                vencimento = DateTime.Today;
                DateTime? VencLido = DateTime.Today;
                if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                {
                    Valido = false;
                    return;
                }
                vencimento = VencLido.Value;                
            }

            else if (fornecedor == 97) //SABESP
            {
                /*
                RGI = long.Parse(CodigoDeBarras.Substring(24, 8));
                comp = new Competencia(int.Parse(string.Format("20{0}{1}", CodigoDeBarras.Substring(39, 2), CodigoDeBarras.Substring(41, 2))));
                //FRN = 1595;
                RGIAlternativo = false;
                vencimento = DateTime.Today;
                DateTime? VencLido = DateTime.Today;
                if(!VirInput.Input.Execute("Vencimento", ref VencLido,null,null))
                {                    
                    Valido = false;
                    return;
                }
                vencimento = VencLido.Value;
            }
            else if (fornecedor == 48) //ELETROPAULO
            {
                /*
                RGI = long.Parse(CodigoDeBarras.Substring(32, 12));
                RGIAlternativo = true;
                vencimento = DateTime.Today;
                DateTime? VencLido = DateTime.Today;
                if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                {                    
                    Valido = false;
                    return;
                }
                vencimento = VencLido.Value; 
            }
            else if (fornecedor == 122)  //SANED
            {
                //FRN = 2865;
                RGI = long.Parse(CodigoDeBarras.Substring(22, 6));
                comp = new Competencia(int.Parse(CodigoDeBarras.Substring(30, 4) + CodigoDeBarras.Substring(28, 2)));
                int dia = int.Parse(CodigoDeBarras.Substring(34, 2));
                int mes = int.Parse(CodigoDeBarras.Substring(36, 2));
                int ano = int.Parse(CodigoDeBarras.Substring(38, 4));
                vencimento = new DateTime(ano, mes, dia);
            }
            else if (fornecedor == 57) //congas
            {
                /*
                Tipo = TipoConta.Gas;
                //FRN = 602;
                RGI = long.Parse(CodigoDeBarras.Substring(36, 7));
                vencimento = DateTime.Today;
                DateTime? VencLido = DateTime.Today;
                if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                {                    
                    Valido = false;
                    return;
                };
                vencimento = VencLido.Value;
            }
            else if ((fornecedor == 296) || (fornecedor == 251))  //net
            {
                /*
                Tipo = TipoConta.TV;
                //FRN = 330;
                //RGI = long.Parse(CodigoDeBarras.Substring(36, 7));
                //comp = new Competencia(int.Parse(CodigoDeBarras.Substring(30, 4) + CodigoDeBarras.Substring(28, 2)));
                int dia = int.Parse(CodigoDeBarras.Substring(25, 2));
                int mes = int.Parse(CodigoDeBarras.Substring(23, 2));
                int ano = int.Parse(CodigoDeBarras.Substring(19, 4));
                vencimento = new DateTime(ano, mes, dia);
                RGI = 0;
                if(!VirInput.Input.Execute("C�digo NET",out RGI))
                {
                    Valido = false;
                    return;
                }
            }
            else if (fornecedor == 1029) //telefonica
            {
                //FRN = 1114;
                //Existem 2 tipos, a residncial nao tem o n�mero do telefone
                /*
                if (CodigoDeBarras.Substring(38, 6) == "999999")
                {
                    if (!VirInput.Input.Execute("N�mero do telefone", out RGI))
                    {
                        Valido = false;
                        return;
                    }
                    vencimento = DateTime.Today;
                    DateTime? VencLido = DateTime.Today;
                    if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                    {                        
                        Valido = false;
                        return;
                    }
                    vencimento = VencLido.Value;
                }
                else
                {
                    RGI = long.Parse(CodigoDeBarras.Substring(24, 8));
                    comp = new Competencia(int.Parse("20" + CodigoDeBarras.Substring(35, 2) + CodigoDeBarras.Substring(33, 2)));
                    int dia = int.Parse(CodigoDeBarras.Substring(42, 2));
                    int mes = int.Parse(CodigoDeBarras.Substring(40, 2));
                    int ano = int.Parse("20" + CodigoDeBarras.Substring(38, 2));
                    vencimento = new DateTime(ano, mes, dia);
                }
            }
            else if (fornecedor == 137) //ULTRAGAZ
            {
                /*
                Tipo = TipoConta.Gas;
                //FRN = 190;
                if (!VirInput.Input.Execute("C�digo do cliente", out RGI))
                {
                    Valido = false;
                    return;
                }

                int dia = int.Parse(CodigoDeBarras.Substring(25, 2));
                int mes = int.Parse(CodigoDeBarras.Substring(23, 2));
                int ano = int.Parse(CodigoDeBarras.Substring(19, 4));
                vencimento = new DateTime(ano, mes, dia);
                
                
            }
            else if (fornecedor == 78) //livre
            {
                //FRN = 852;
                /*RGI = long.Parse(CodigoDeBarras.Substring(21, 10));
                vencimento = DateTime.Today;
                DateTime? VencLido = DateTime.Today;
                if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                {                    
                    Valido = false;
                    return;
                }
                vencimento = VencLido.Value;
            }

            else if (fornecedor == 89) //Nextel
            {
                
                //FRN = 358;
                if (!VirInput.Input.Execute("Identifica��o do cliente", out RGI))
                {
                    Valido = false;
                    return;
                }
                vencimento = DateTime.Today;
                DateTime? VencLido = DateTime.Today;
                if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                {                    
                    Valido = false;
                    return;
                }
                vencimento = VencLido.Value;
            }

            else if (fornecedor == 162) //CLARO
            {
                //FRN = 2273;
                RGI = long.Parse(CodigoDeBarras.Substring(27, 9));                
                RGIAlternativo = false;
                int dia = int.Parse(CodigoDeBarras.Substring(25, 2));
                int mes = int.Parse(CodigoDeBarras.Substring(23, 2));
                int ano = int.Parse(CodigoDeBarras.Substring(19, 4));
                vencimento = new DateTime(ano, mes, dia);                
            }

            else if (fornecedor == 109) //TIM
            {
                /*
                if (!VirInput.Input.Execute("Iden. d�bito (sem dig.)", out RGI))
                {
                    Valido = false;
                    return;
                }
                RGIAlternativo = false;
                vencimento = DateTime.Today;
                DateTime? VencLido = DateTime.Today;
                if (!VirInput.Input.Execute("Vencimento", ref VencLido, null, null))
                {                    
                    Valido = false;
                    return;
                }
                vencimento = VencLido.Value; 
            }

            else
            {
                
                Valido = false;
                UltimoErro = new Exception(string.Format("N�o implementado 'fornecedor' <0>  : <{1}>)", fornecedor, CodigoDeBarras));
                return;
            };  */
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Cod"></param>
        /// <returns></returns>
        static public TipoConta TipoPorFornecedor(CodigosFornecedorCodBar Cod)
        {
            switch (Cod)
            {                
                case CodigosFornecedorCodBar.VIVO:
                case CodigosFornecedorCodBar.TELEFONICA:
                case CodigosFornecedorCodBar.LIVRE:
                case CodigosFornecedorCodBar.NEXTEL:
                case CodigosFornecedorCodBar.CLARO:
                case CodigosFornecedorCodBar.TIM:                
                    return TipoConta.Telefone;
                case CodigosFornecedorCodBar.SABESP:
                case CodigosFornecedorCodBar.SANED:
                case CodigosFornecedorCodBar.DAESCS:
                    return TipoConta.Agua;
                case CodigosFornecedorCodBar.ELETROPAULO:
                    return TipoConta.Luz;                
                case CodigosFornecedorCodBar.CONGAS:
                case CodigosFornecedorCodBar.ULTRAGAZ:
                    return TipoConta.Gas;
                case CodigosFornecedorCodBar.NET1:
                case CodigosFornecedorCodBar.NET2:
                    return TipoConta.TV;                                                
                default:
                    return TipoConta.Erro;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TipoConta
        {
            /// <summary>
            /// 
            /// </summary>
            Erro = 0,
            /// <summary>
            /// 
            /// </summary>
            Agua = 2,
            /// <summary>
            /// 
            /// </summary>
            Luz = 3,
            /// <summary>
            /// 
            /// </summary>
            Telefone = 4,
            /// <summary>
            /// 
            /// </summary>
            Gas = 5,
            /// <summary>
            /// 
            /// </summary>
            TV = 6
        }

        /// <summary>
        /// 
        /// </summary>
        public enum CodigosFornecedorCodBar
        {
            /// <summary>
            /// DAE S�o caetano
            /// </summary>
            DAESCS = 35,
            /// <summary>
            /// Este c�digo n�o deve mais ser usado. agora virou telef�nica 1029
            /// </summary>
            VIVO = 82,
            /// <summary>
            /// 
            /// </summary>
            SABESP = 97,
            /// <summary>
            /// 
            /// </summary>
            ELETROPAULO = 48,
            /// <summary>
            /// 
            /// </summary>
            SANED = 122,
            /// <summary>
            /// 
            /// </summary>
            CONGAS = 57,
            /// <summary>
            /// 
            /// </summary>
            NET1 = 296,
            /// <summary>
            /// 
            /// </summary>
            NET2 = 251,
            /// <summary>
            /// 
            /// </summary>
            TELEFONICA = 1029,
            /// <summary>
            /// 
            /// </summary>
            ULTRAGAZ = 137,
            /// <summary>
            /// 
            /// </summary>
            LIVRE = 78,
            /// <summary>
            /// 
            /// </summary>
            NEXTEL = 89,
            /// <summary>
            /// 
            /// </summary>
            CLARO = 162,
            /// <summary>
            /// 
            /// </summary>
            TIM = 109,
            /// <summary>
            /// 
            /// </summary>
            SEMASA = 113,
        }
    }
}
