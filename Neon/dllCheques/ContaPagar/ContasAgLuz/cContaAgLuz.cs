
using CompontesBasicosProc;
using ContaPagar.Follow;
using VirEnumeracoesNeon;
using Framework.objetosNeon;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace ContaPagar.ContasAgLuz
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cContaAgLuz : CompontesBasicos.ComponenteBase
    {

        private CadastrosProc.Fornecedores.dFornecedoresLookup _dFornecedoresLookup;

        internal CadastrosProc.Fornecedores.dFornecedoresLookup DFornecedoresLookup
        {
            get
            {
                if (_dFornecedoresLookup == null)
                {
                    _dFornecedoresLookup = new CadastrosProc.Fornecedores.dFornecedoresLookup();
                    _dFornecedoresLookup.FRNLookupTableAdapter.Fill(_dFornecedoresLookup.FRNLookup);                    
                }
                return _dFornecedoresLookup;
            }
        }

        private SortedList<int, Cadastros.Fornecedores.Fornecedor> PoolFornecedores;

        private Cadastros.Fornecedores.Fornecedor BuscaFonrecedor(int FRN)
        {
            if (PoolFornecedores.ContainsKey(FRN))
                Fornecedor = PoolFornecedores[FRN];
            else
            {
                Fornecedor = new Cadastros.Fornecedores.Fornecedor(FRN);
                if (!Fornecedor.encontrdo)
                    Fornecedor = null;
                else
                    PoolFornecedores.Add(FRN, Fornecedor);
            }
            return Fornecedor;
        }

        private Cadastros.Fornecedores.Fornecedor Fornecedor;

        private ContaAgLuz contaAgLuz1;

        private PagamentoPeriodico PagamentoPeriodico1;

        private int REF = 0;

        //private Framework.datasets.dFornecedores.FORNECEDORESRow rowFRN;

        /// <summary>
        /// Construtor
        /// </summary>
        public cContaAgLuz()
        {
            InitializeComponent();
            PoolFornecedores = new SortedList<int, Cadastros.Fornecedores.Fornecedor>();
            dAcompanhamentoBindingSource.DataSource = dAcompanhamento.dAcompanhamentoSt;
            fORNECEDORESBindingSource.DataSource = DFornecedoresLookup;
            XCompetencia.Comp = new Competencia();
            cCompProx.Comp = new Competencia();
        }

        private void BProcurar_Click(object sender, EventArgs e)
        {
            Procurar(true);
        }

        private void BtSegue_Click(object sender, EventArgs e)
        {
            if ((PagamentoPeriodico1 != null) && (PagamentoPeriodico1.Encontrado))
                //if (PagamentoPeriodico1.Tipo == TipoPagPeriodico.Conta)
                //{
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Liberando nota");
                    Application.DoEvents();
                    int? NOAProv = PagamentoPeriodico1.NotaProvisoria();
                    if (!NOAProv.HasValue)
                    {
                        PagamentoPeriodico1.CadastrarProximo();
                        PagamentoPeriodico1.CadastrarProximaSemQuitarAnteriro = false;
                        NOAProv = PagamentoPeriodico1.NotaProvisoria();
                    }
                    if (!NOAProv.HasValue)
                    {
                        string mensagemerro = string.Format("Erro: O pagamento da compet�ncia {0} ainda est� pendente. Cancele se for o caso.", PagamentoPeriodico1.ProximaCompetencia);
                        MessageBox.Show(mensagemerro);
                        return;
                        //throw new Exception(mensagemerro);
                    };
                    Nota NotaTravada = new Nota(NOAProv.Value);
                    //decimal ValorAjusteParcelasAcumuladas = 0;
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("cContaAgLuz BtSegue_Click - 842");
                        string mensagem;
                        decimal ValorEfetivo = XValor.Value;
                        if (ValorEfetivo != 0)                        
                            mensagem = string.Format("Chegada da conta, nota destravada{3}. Compet�ncia: {0} Vencimento: {1:dd/MM/yyyy} Valor: {2:n2}",
                                                                                                                         cCompProx.Comp,
                                                                                                                         XVencimento.DateTime,
                                                                                                                         XValor.Value,
                                                                                                                         PagamentoPeriodico1.Tipo == TipoPagPeriodico.DebitoAutomaticoConta ? " -> D�bito autom�tico" : string.Empty);                            
                        
                        else
                            mensagem = string.Format("Chegada da conta, nota ACUMULADA. Compet�ncia: {0} Vencimento original: {1:dd/MM/yyyy} Valor: {2:n2}", cCompProx.Comp, XVencimento.DateTime, XValor.Value);
                        
                        if (ValorEfetivo != 0)                        
                            PagamentoPeriodico1.PGFrow.PGFValor = ValorEfetivo;                        
                        PagamentoPeriodico1.GravaHistorico(mensagem, false);
                        NotaTravada.Periodico = PagamentoPeriodico1;
                        NotaTravada.Destrava(DateTime.Today,
                                             XVencimento.DateTime,
                                             ValorEfetivo,                                             
                                             CodigoDigitado);
                        if (PagamentoPeriodico1.Tipo == TipoPagPeriodico.DebitoAutomaticoConta)
                            PagamentoPeriodico1.CadastrarProximo();
                        VirMSSQL.TableAdapter.CommitSQL();
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(ex);
                        throw ex;
                    }
                    Limpa();
                }
            //}
            /*
            else
            {
                if (PagamentoPeriodico1.CadastraProximoPagamento(chAcumular.Checked ? XVencimento.DateTime.AddMonths(1) : XVencimento.DateTime,
                                                                 XValor.Value,
                                                                 chAcumular.Checked ? PagamentoPeriodico.TipoCadastraProximoPagamento.acumular : PagamentoPeriodico.TipoCadastraProximoPagamento.efetivo))
                {
                    Limpa();
                }
                else
                {
                    MessageBox.Show("Erro ao cadastrar");
                }
            }
            */
        }

        private bool CadastroNovo()
        {
            if (lookupCondominio_F1.CON_sel == -1)
            {
                lookupCondominio_F1.Focus();
                MessageBox.Show("Condom�nio n�o definido");
                return false;
            }

            if ((contaAgLuz1 == null) && (!CB_SemCodigo.Checked))
                return false;

            if (XCompetencia.IsNull)
            {
                XCompetencia.Focus();
                MessageBox.Show("Compet�ncia da conta n�o definida");
                return false;
            };

            Competencia CompPAG = new Competencia(XVencimento.DateTime);
            int Ref = (CompPAG - XCompetencia.Comp);

            if ((Ref < -1) || (Ref > 1))
            {
                XCompetencia.Focus();
                MessageBox.Show("Compet�ncia/Vencimento inv�lidos");
                return false;
            }
            if (radioGroup1.SelectedIndex == -1)
            {
                radioGroup1.Focus();
                MessageBox.Show("Tipo de pagamento n�o definido");
                return false;
            }
            dContaAgLuz.PaGamentosFixosRow novaRow = dContaAgLuz.PaGamentosFixos.NewPaGamentosFixosRow();
            novaRow.PGF_CON = lookupCondominio_F1.CON_sel;



            int FRN;


            ContaAgLuz.TipoConta TipoConta;
            if (contaAgLuz1 != null)
            {
                FRN = contaAgLuz1.FRN;
                TipoConta = contaAgLuz1.Tipo;
            }
            else
            {
                FRN = (int)lFornecedor.EditValue;
                TipoConta = ContaAgLuz.TipoPorFornecedor(CodigoFornecedor);
            }
            novaRow.PGF_FRN = FRN;
            //Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresRow FRNrow = Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fornecedores.FindByFRN(FRN);
            //Fornecedor = new Cadastros.Fornecedores.Fornecedor(FRN);
            BuscaFonrecedor(FRN);

            switch (TipoConta)
            {
                case ContaAgLuz.TipoConta.Erro:
                    break;
                case ContaAgLuz.TipoConta.Agua:
                    novaRow.PGF_PLA = "202000";
                    novaRow.PGF_SPL = 87;
                    if (Fornecedor != null)
                        novaRow.PGFDescricao = Fornecedor.FRNNome;
                    else
                        novaRow.PGFDescricao = "Conta de �gua";
                    break;
                case ContaAgLuz.TipoConta.Luz:
                    novaRow.PGF_PLA = "201000";
                    novaRow.PGF_SPL = 127;
                    if (Fornecedor != null)
                        novaRow.PGFDescricao = Fornecedor.FRNNome;
                    else
                        novaRow.PGFDescricao = "ELETROPAULO";
                    break;
                case ContaAgLuz.TipoConta.Telefone:
                    novaRow.PGF_PLA = "204001";
                    novaRow.PGF_SPL = 33;
                    novaRow.PGFDescricao = Fornecedor.FRNNome;
                    break;
                case ContaAgLuz.TipoConta.Gas:
                    novaRow.PGF_PLA = "203000";
                    novaRow.PGF_SPL = 71;
                    novaRow.PGFDescricao = Fornecedor.FRNNome;
                    break;
                case ContaAgLuz.TipoConta.TV:
                    novaRow.PGF_PLA = "260002";
                    novaRow.PGF_SPL = 85;
                    novaRow.PGFDescricao = Fornecedor.FRNNome;
                    break;
                default:
                    break;
            }
            /*
            if (contaAgLuz1.Tipo == ContaAgLuz.TipoConta.Luz)
            {
                novaRow.PGF_PLA = "201000";
                novaRow.PGF_SPL = 127;
                if (FRNrow != null)
                    novaRow.PGFDescricao = FRNrow.FRNNome;
                else
                    novaRow.PGFDescricao = "ELETROPAULO";
            }
            else if (contaAgLuz1.Tipo == ContaAgLuz.TipoConta.Agua)
            {
                novaRow.PGF_PLA = "202000";
                novaRow.PGF_SPL = 87;
                if (FRNrow != null)
                    novaRow.PGFDescricao = FRNrow.FRNNome;
                else
                    novaRow.PGFDescricao = "Conta de �gua";
            };*/
            if (contaAgLuz1 != null)
                novaRow.PGFDia = contaAgLuz1.vencimento.Day;
            else
                novaRow.PGFDia = XVencimento.DateTime.Day;
            novaRow.PGFValor = XValor.Value;
            //novaRow.PGFCompetI = XCompetencia.Comp.CompetenciaBind;

            novaRow.PGFCompetI = XCompetencia.Comp.CompetenciaBind;
            if (radioGroup1.SelectedIndex == 0)
                novaRow.PGFForma = (int)TipoPagPeriodico.DebitoAutomaticoConta;
            else
                novaRow.PGFForma = (int)TipoPagPeriodico.Conta;
            novaRow.PGFNominal = Fornecedor.FRNNome;
            //novaRow.PGFContaMesAnterior = (XCompetencia.Comp < new Competencia(XVencimento.DateTime.Month, XVencimento.DateTime.Year));
            novaRow.PGFRef = Ref;
            novaRow.PGFStatus = (int)StatusPagamentoPeriodico.Ativado;
            novaRow.PGFProximaCompetencia = novaRow.PGFCompetI;
            if (contaAgLuz1 == null)
                novaRow.PGFHistorico = string.Format("{0:dd/MM/yyyy HH:mm:ss} Cadastrado manual a partir da conta SEM codigo de barras\r\n", DateTime.Now);
            else
                novaRow.PGFHistorico = string.Format("{0:dd/MM/yyyy HH:mm:ss} Cadastrado manual a partir da conta\r\n", DateTime.Now);
            long RGI = (contaAgLuz1 == null) ? (long)XIdentificacao.Value : contaAgLuz1.RGI;
            if (TipoConta == ContaAgLuz.TipoConta.Luz)
                novaRow.PGFDadosPag = "ELETROPAULO/SP-";
            else
                novaRow.PGFDadosPag = "SABESP";
            switch (CodigoFornecedor)
            {
                case ContaAgLuz.CodigosFornecedorCodBar.NET1:
                case ContaAgLuz.CodigosFornecedorCodBar.NET2:
                    novaRow.PGFCodigoDebito = RGI % 1000000;
                    break;
                case ContaAgLuz.CodigosFornecedorCodBar.TIM:
                    novaRow.PGFCodigoDebito = RGI % 10000000000;
                    break;
                default:
                    novaRow.PGFCodigoDebito = RGI;
                    break;
            }
            /*
            if (FRN == 296)
                novaRow.PGFCodigoDebito = RGI % 1000000;
            else if (FRN == 109)
                novaRow.PGFCodigoDebito = RGI % 10000000000;
            else
                novaRow.PGFCodigoDebito = RGI;*/
            if (spinIdent.Value != 0)
                novaRow.PGFIdentificacao = (long)spinIdent.Value;
            novaRow.PGFProximaCompConta = XCompetencia.Comp.CompetenciaBind;
            novaRow.PGFCompetNaDescricao = true;
            novaRow.PGFParcelaNaDesc = false;
            dContaAgLuz.PaGamentosFixos.AddPaGamentosFixosRow(novaRow);


            dContaAgLuz.PaGamentosFixosTableAdapter.Update(novaRow);
            novaRow.AcceptChanges();
            PagamentoPeriodico NovoPagamentoPeriodico = new PagamentoPeriodico(novaRow.PGF);
            NovoPagamentoPeriodico.CadastrarProximo();
            //VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoApaga, rowImporta.Codigo_do_cliente);
            //CadastroManual(false,null);
            //RegistrNota(contaAgLuz1);
            //contaAgLuz1 = null;
            return true;
        }

        private void CalculaData()
        {
            if ((PagamentoPeriodico1 != null) && (PagamentoPeriodico1.Encontrado))
            {
                Competencia compPAG = cCompProx.Comp.CloneCompet(REF);
                dateEditProx.DateTime = compPAG.DataNaCompetencia(PagamentoPeriodico1.PGFrow.PGFDia, Competencia.Regime.mes);
            }
        }

        private void CB_SemCodigo_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if ((bool)e.NewValue == true)
                if (MessageBox.Show("ESTA FUN��O DEVE SER USADA SOMENTE PARA CONTAS NOVAS QUE J� EST�O EM DEBITO AUTOM�TICO E N�O TEM CODIGO DE BARRAS\r\n\r\n� este o caso ?", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    e.Cancel = true;                
        }

        private void cCompProx_OnChange(object sender, EventArgs e)
        {
            CalculaData();
        }

        private void dateEditProx_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Up:
                    //dateEditProx.DateTime = dateEditProx.DateTime.AddMonths(1);
                    REF++;
                    CalculaData();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Down:
                    //dateEditProx.DateTime = dateEditProx.DateTime.AddMonths(-1);
                    REF--;
                    CalculaData();
                    break;
            }
        }
       

        private void lFornecedor_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                int FRNCodigoDebito = (int)lFornecedor.GetColumnValue("FRNCodigoDebito");
                CB_SemCodigo.Visible = Label_TipoCod.Visible = FRNCodigoDebito.EstaNoGrupo(296, 251, 35);
                if (FRNCodigoDebito == 35)
                    Label_TipoCod.Text = "<< C�digo DAE sem o d�gito";
                else
                    Label_TipoCod.Text = "<< C�digo NET";
            }
            catch
            {
                Label_TipoCod.Visible = false;
            }
        }

        private bool ChecaPreencimentos()
        {
            if (XIdentificacao.Value == 0)
            {
                XIdentificacao.Focus();
                return false;
            }
            if (lFornecedor.EditValue == null)
            {
                lFornecedor.Focus();
                return false;
            }
            if ((XValor.Value == 0) && (!chAcumular.Checked))
            {
                XValor.Focus();
                return false;
            }
            if (XVencimento.EditValue == null)
            {
                XVencimento.Focus();
                return false;
            }
            return true;
        }

        private bool Procurar(bool manual)
        {
            textEdit1.Visible = false;
            if (!ChecaPreencimentos())
                return false;
                        
            // Forma de busca dependo fornecedor
            switch (CodigoFornecedor)
            {
                case ContaAgLuz.CodigosFornecedorCodBar.NET1:
                case ContaAgLuz.CodigosFornecedorCodBar.NET2:
                    PagamentoPeriodico1 = new PagamentoPeriodico((long)XIdentificacao.Value % 1000000, (int)lFornecedor.EditValue, manual);
                    break;
                case ContaAgLuz.CodigosFornecedorCodBar.TIM:
                    PagamentoPeriodico1 = new PagamentoPeriodico((long)XIdentificacao.Value % 10000000000, (int)lFornecedor.EditValue, manual);
                    break;
                default:
                    PagamentoPeriodico1 = new PagamentoPeriodico((long)XIdentificacao.Value, (int)lFornecedor.EditValue, manual);
                    break;
            }


            if (PagamentoPeriodico1.Encontrado)
            {
                BtSegue.Visible = true;
                //Lerro.Visible = false;
                //groupControlAcumular.Visible = false;
                decimal ValorPorParcela = XValor.Value;                
                cCompProx.BackColor = XCompetencia.BackColor = Color.Transparent;
                XVencimento.BackColor = dateEditProx.BackColor = Color.White;
                ValorCadastro.BackColor = XValor.BackColor = Color.White;
                radioGroup1.BackColor = Color.White;
                if (XTipoEsperado.SelectedIndex != -1)
                    XTipoEsperado.BackColor = Color.White;
                BProcurar.Visible = false;
                decimal maximo = PagamentoPeriodico1.PGFrow.PGFValor * 1.3M;
                decimal minimo = PagamentoPeriodico1.PGFrow.PGFValor * 0.7M;                

                if (ValorPorParcela != 0)
                    if ((ValorPorParcela > maximo) || (ValorPorParcela < minimo))
                    {
                        ValorCadastro.BackColor = XValor.BackColor = Color.Yellow;
                        labelErro.Text = "Varia��o do valor maior que 30%";
                        MessageBox.Show("ATEN��O: Varia��o do valor maior que 30%.");
                    }
                if (PagamentoPeriodico1.Tipo == TipoPagPeriodico.DebitoAutomaticoConta)
                    radioGroup1.SelectedIndex = 0;
                else if (PagamentoPeriodico1.Tipo == TipoPagPeriodico.Conta)
                    radioGroup1.SelectedIndex = 1;
                else
                    radioGroup1.SelectedIndex = -1;
                if (radioGroup1.SelectedIndex != XTipoEsperado.SelectedIndex)
                {
                    labelErro.Text = "Verifique o tipo de d�bito";
                    MessageBox.Show("ATEN��O: Verifique o tipo de d�bito.");
                    XTipoEsperado.BackColor = radioGroup1.BackColor = Color.Yellow;
                }
                /*
                if (PagamentoPeriodico1.ProximaCompetencia != XCompetencia.Comp)
                {                    
                    labelErro.Visible = true;
                    labelErro.Text = "Compet�ncia incorreta";
                    //labelErro.Text += string.Format(" {0} {1}", PagamentoPeriodico1.ProximaCompetencia, XCompetencia.Comp);
                    BtSegue.Visible = false;
                    XCompetencia.BackColor = cCompProx.BackColor = Color.Red;                    
                }
                else
                {
                    if (Math.Round((XVencimento.DateTime - PagamentoPeriodico1.ProximaData).Days / 30M) != 0)
                    {
                        labelErro.Text = "Vencimento incorreto";
                        BtSegue.Visible = false;
                        XVencimento.BackColor = dateEditProx.BackColor = Color.Red;
                    }
                    else
                    {
                        labelErro.Visible = false;
                    }
                }
                */
                lookupCondominio_F1.Enabled = false;
                lookupCondominio_F1.CON_sel = PagamentoPeriodico1.PGFrow.PGF_CON;
                BotaoGrava.Text = "Alterar Cadastro";

                if (PagamentoPeriodico1.PGFrow.IsPGFIdentificacaoNull())
                    spinIdent.Value = 0;
                else
                    spinIdent.Value = PagamentoPeriodico1.PGFrow.PGFIdentificacao;
                ValorCadastro.EditValue = PagamentoPeriodico1.PGFrow.PGFValor;
                if (PagamentoPeriodico1.PGFrow.IsPGFCodigoDebitoNull())
                    spinCodigo.Value = 0;
                else
                    spinCodigo.Value = PagamentoPeriodico1.PGFrow.PGFCodigoDebito;
                REF = PagamentoPeriodico1.PGFrow.PGFRef;
                cCompProx.Comp = PagamentoPeriodico1.ProximaCompetencia;
                cCompProx.AjustaTela();
                dateEditProx.DateTime = PagamentoPeriodico1.ProximaData;
                GrupoCadastro.Visible = true;
                BProcurar.Visible = false;


                if (XCompetencia.IsNull)
                {
                    TimeSpan Dif = (XVencimento.DateTime - dateEditProx.DateTime);
                    int ErroMeses = (int)Math.Round(Dif.Days / 30M);
                    XCompetencia.Comp = cCompProx.Comp.CloneCompet(ErroMeses);
                    XCompetencia.IsNull = false;
                    XCompetencia.AjustaTela();
                };

                if (PagamentoPeriodico1.ProximaCompetencia != XCompetencia.Comp)
                {
                    ContaPagarProc.NotaProc UltimaNota1 = PagamentoPeriodico1.UltimaNota();
                    if ((PagamentoPeriodico1.ProximaCompetencia.CloneCompet(1) == XCompetencia.Comp) && (UltimaNota1 != null) && (UltimaNota1.VerificaFase() == FasePeriodico.AguardaEmissaoCheque))
                    {
                        labelErro.Visible = true;
                        labelErro.Text = "Aten��o: A conta anterior ainda n�o foi quitada";
                        BtSegue.Visible = true;
                        XCompetencia.BackColor = cCompProx.BackColor = Color.Yellow;
                        PagamentoPeriodico1.CadastrarProximaSemQuitarAnteriro = true;
                    }
                    else
                    {
                        labelErro.Visible = true;
                        labelErro.Text = "Compet�ncia incorreta";                        
                        BtSegue.Visible = false;
                        XCompetencia.BackColor = cCompProx.BackColor = Color.Red;
                    }
                }
                else
                    if (Math.Round((XVencimento.DateTime - PagamentoPeriodico1.ProximaData).Days / 30M) != 0)
                {
                    labelErro.Text = "Vencimento incorreto";
                    BtSegue.Visible = false;
                    XVencimento.BackColor = dateEditProx.BackColor = Color.Red;
                }
                else
                    labelErro.Visible = false;

                return true;
            }
            else
            {
                BtSegue.Visible = false;
                if (!manual || CB_SemCodigo.Checked)
                {
                    BProcurar.Visible = false;
                    lookupCondominio_F1.Enabled = true;
                    lookupCondominio_F1.CON_sel = -1;
                    radioGroup1.SelectedIndex = -1;
                    //if (contaAgLuz1.fornecedor == 162) //claro
                    //{
                    //    int telefone = 0;
                    //    if(!VirInput.Input.Execute("Numero do telefone",out telefone))
                    //        return false;
                    //    if (telefone <   100000000)
                    //        telefone += 1100000000;
                    //    spinCodigo.Value = telefone;
                    //    spinIdent.Value = XIdentificacao.Value;
                    //}
                    //else 

                    //if (contaAgLuz1 != null)
                    //{
                    switch (CodigoFornecedor)
                    {
                        case ContaAgLuz.CodigosFornecedorCodBar.NET1:
                        case ContaAgLuz.CodigosFornecedorCodBar.NET2:
                            spinIdent.Value = XIdentificacao.Value;
                            spinCodigo.Value = XIdentificacao.Value % 1000000;
                            break;
                        case ContaAgLuz.CodigosFornecedorCodBar.TIM:
                            spinIdent.Value = 0;
                            spinCodigo.Value = XIdentificacao.Value % 10000000000;
                            break;
                        default:
                            spinIdent.Value = 0;
                            spinCodigo.Value = XIdentificacao.Value;
                            break;
                    }

                    if (XCompetencia.IsNull)
                    {
                        XCompetencia.Comp = new Competencia(XVencimento.DateTime.Month, XVencimento.DateTime.Year);
                        if (XVencimento.DateTime.Day <= 15)
                            XCompetencia.Comp--;
                    }

                    cCompProx.Enabled = false;
                    cCompProx.Comp = XCompetencia.Comp;
                    cCompProx.AjustaTela();

                    if (manual)
                        ValorCadastro.Value = XValor.Value;

                    BotaoGrava.Text = "Cadastrar";
                    dateEditProx.DateTime = XVencimento.DateTime;
                    dateEditProx.Enabled = false;
                    GrupoCadastro.Visible = true;
                    BtSegue.Visible = false;
                }
                else
                {
                    GrupoCadastro.Visible = false;
                    if (manual)
                        MessageBox.Show("Cadastro de conta n�o encontrado");
                };
                PagamentoPeriodico1 = null;
                return false;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Limpa();
        }


        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (PagamentoPeriodico1 == null)
            {
                if (CadastroNovo())
                    Procurar(false);
            }
            else
            {
                PagamentoPeriodico1.PGFrow.PGFProximaCompetencia = cCompProx.Comp.CompetenciaBind;
                Competencia CompPAG = new Competencia(dateEditProx.DateTime);
                int Ref = (CompPAG - cCompProx.Comp);

                if ((Ref < -1) || (Ref > 1))
                {
                    cCompProx.Focus();
                    MessageBox.Show("Compet�ncia/Vencimento inv�lidos");
                    return;
                }
                decimal ValorRef = (decimal)ValorCadastro.EditValue;
                /*
                if (!chAcumular.Checked)
                    switch (PagamentoPeriodico1.TemAcumulado())
                    {
                        case PagamentoPeriodico.TiposAcumulado.SemAcumulado:
                            break;
                        case PagamentoPeriodico.TiposAcumulado.AcumuladoComValor:
                            //ValorRef -= PagamentoPeriodico1.ValAcumulado();
                            break;
                        case PagamentoPeriodico.TiposAcumulado.AcumuladoSemValor:
                            ValorRef = ValorRef / PagamentoPeriodico1.PAGsAcumular.Count + 1;
                            break;
                        default:
                            break;
                    }*/
                PagamentoPeriodico1.PGFrow.PGFValor = ValorRef;
                PagamentoPeriodico1.PGFrow.PGFRef = Ref;
                if (spinIdent.Value != 0)
                    PagamentoPeriodico1.PGFrow.PGFIdentificacao = (int)spinIdent.Value;
                if (radioGroup1.SelectedIndex == 0)
                    PagamentoPeriodico1.Tipo = TipoPagPeriodico.DebitoAutomaticoConta;
                else if (radioGroup1.SelectedIndex == 1)
                    PagamentoPeriodico1.Tipo = TipoPagPeriodico.Conta;
                string Justificativa = string.Empty;
                if (VirInput.Input.Execute("Motivo", ref Justificativa))
                {
                    PagamentoPeriodico1.Salvar(Justificativa);
                    Procurar(false);
                }
            }
        }

        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.Text = string.Empty;
        }

        string CodigoDigitado = "";

        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textEdit1.Text.Length == 44)
                {
                    textEdit1.Visible = false;
                    string Supostocodigo = textEdit1.Text;
                    contaAgLuz1 = new ContaAgLuz(Supostocodigo);
                    if (contaAgLuz1.Valido)
                    {
                        CodigoDigitado = Supostocodigo;
                        XValor.Value = contaAgLuz1.Valor;
                        XValor.Properties.ReadOnly = true;
                        XIdentificacao.Value = contaAgLuz1.RGI;
                        XIdentificacao.Properties.ReadOnly = true;
                        if (contaAgLuz1.vencimento == DateTime.MinValue)
                            XVencimento.Text = string.Empty;
                        else
                        {
                            XVencimento.DateTime = contaAgLuz1.vencimento;
                            XVencimento.Properties.ReadOnly = true;
                        };
                        if (contaAgLuz1.comp == null)
                        {
                            XCompetencia.IsNull = true;
                            XCompetencia.ReadOnly = false;
                        }
                        else
                        {
                            XCompetencia.Comp = contaAgLuz1.comp;
                            XCompetencia.IsNull = false;
                            XCompetencia.AjustaTela();
                            XCompetencia.ReadOnly = true;
                        }
                        //rowFRN = Framework.datasets.dFornecedores.GetdFornecedoresST("FSR").FORNECEDORES.FindByFRN(contaAgLuz1.FRN);
                        //Fornecedor = new Cadastros.Fornecedores.Fornecedor(contaAgLuz1.FRN);
                        BuscaFonrecedor(contaAgLuz1.FRN);
                        if (Fornecedor == null)                                                    
                            throw new Exception("fornecedor n�o encontrado: " + contaAgLuz1.fornecedor.ToString());                        
                        else
                        {
                            lFornecedor.EditValue = Fornecedor.FRN;
                            lFornecedor.Properties.ReadOnly = true;
                        }
                        //PagamentoPeriodico NovoPagamentoPeriodico = new PagamentoPeriodico(contaAgLuz1.RGI, ref contaAgLuz1.comp, contaAgLuz1.vencimento, contaAgLuz1.FRN, contaAgLuz1.Valor);
                        //if (NovoPagamentoPeriodico.PGFrow == null)
                        //{
                        //    CadastroManual(true, contaAgLuz1.comp);
                        //    return false;
                        //}
                        if (Procurar(false))
                            lookupCondominio_F1.Enabled = false;
                        else
                        {
                            lookupCondominio_F1.Enabled = true;
                            ValorCadastro.Value = XValor.Value;
                        }
                        //PagamentoPeriodico PagamentoPeriodico1 = new PagamentoPeriodico(contaAgLuz1.RGI, contaAgLuz1.FRN);
                        //if (!PagamentoPeriodico1.Encontrado)
                        //{
                        //    Limpa();
                        //}
                        //RegistrNota(contaAgLuz1);

                    }
                    else
                    {
                        MessageBox.Show(string.Format("C�digo de barras inv�lido\r\n{0}", contaAgLuz1.UltimoErro));
                        Limpa();
                    }
                }
                textEdit1.Text = string.Empty;
                e.Handled = true;
            }
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != '\b'))
                e.Handled = true;
        }

        private void XTipoEsperado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (XTipoEsperado.SelectedIndex != -1)
                XTipoEsperado.BackColor = Color.White;
        }


        ContaAgLuz.CodigosFornecedorCodBar CodigoFornecedor
        {
            get
            {
                try
                {
                    return contaAgLuz1 == null ? (ContaAgLuz.CodigosFornecedorCodBar)lFornecedor.GetColumnValue("FRNCodigoDebito") : contaAgLuz1.fornecedor;
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Limpa os campos
        /// </summary>
        public void Limpa()
        {
            CodigoDigitado = "";
            spinIdent.Value = 0;
            ValorCadastro.Value = 0;
            BtSegue.Visible = false;
            textEdit1.Visible = true;
            lFornecedor.Properties.ReadOnly = false;
            lFornecedor.EditValue = null;
            XCompetencia.IsNull = true;
            XCompetencia.ReadOnly = false;
            XValor.Properties.ReadOnly = false;
            XValor.EditValue = null;
            XVencimento.Properties.ReadOnly = false;
            XVencimento.EditValue = null;
            XIdentificacao.Properties.ReadOnly = false;
            XIdentificacao.Value = 0;
            GrupoCadastro.Visible = false;
            BProcurar.Visible = true;
            textEdit1.Text = string.Empty;
            PagamentoPeriodico1 = null;
            dateEditProx.Enabled = true;
            cCompProx.Enabled = true;
            textEdit1.Focus();
            cCompProx.BackColor = XCompetencia.BackColor = Color.Transparent;
            XVencimento.BackColor = dateEditProx.BackColor = Color.White;
            ValorCadastro.BackColor = XValor.BackColor = Color.White;
            radioGroup1.BackColor = Color.White;
            if (XTipoEsperado.SelectedIndex != -1)
                XTipoEsperado.BackColor = Color.White;
            labelErro.Visible = false;
            contaAgLuz1 = null;
            chAcumular.Checked = false;
            //groupControlAcumular.Visible = false;
            CB_SemCodigo.Checked = false;
            Label_TipoCod.Visible = false;
            CB_SemCodigo.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ZeraTipo()
        {
            XTipoEsperado.BackColor = Color.Yellow;
            XTipoEsperado.SelectedIndex = -1;
        }

        private void chAcumular_CheckedChanged(object sender, EventArgs e)
        {
            if (chAcumular.Checked)
                XValor.Value = 0;
        }

        private void lFornecedor_Properties_PopupFilter(object sender, DevExpress.XtraEditors.Controls.PopupFilterEventArgs e)
        {
            e.Criteria = DevExpress.Data.Filtering.CriteriaOperator.Parse("RAV = ?", (int)RAVPadrao.ContaConsumo);
        }
    }
}
