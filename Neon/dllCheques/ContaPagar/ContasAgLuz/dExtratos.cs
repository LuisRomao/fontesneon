﻿using Framework.objetosNeon;

namespace ContaPagar.ContasAgLuz
{


    public partial class dExtratos
    {

        #region Table Adapters
        private dExtratosTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dExtratosTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dExtratosTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dExtratosTableAdapters.SaldoContaCorrenteTableAdapter saldoContaCorrenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoContaCorrente
        /// </summary>
        public dExtratosTableAdapters.SaldoContaCorrenteTableAdapter SaldoContaCorrenteTableAdapter
        {
            get
            {
                if (saldoContaCorrenteTableAdapter == null)
                {
                    saldoContaCorrenteTableAdapter = new dExtratosTableAdapters.SaldoContaCorrenteTableAdapter();
                    saldoContaCorrenteTableAdapter.TrocarStringDeConexao();
                };
                return saldoContaCorrenteTableAdapter;
            }
        }

        private dExtratosTableAdapters.ContaCorrenteDetalheTableAdapter contaCorrenteDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteDetalhe
        /// </summary>
        public dExtratosTableAdapters.ContaCorrenteDetalheTableAdapter ContaCorrenteDetalheTableAdapter
        {
            get
            {
                if (contaCorrenteDetalheTableAdapter == null)
                {
                    contaCorrenteDetalheTableAdapter = new dExtratosTableAdapters.ContaCorrenteDetalheTableAdapter();
                    contaCorrenteDetalheTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenteDetalheTableAdapter;
            }
        }

        private dExtratosTableAdapters.ChavesTableAdapter chavesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Chaves
        /// </summary>
        public dExtratosTableAdapters.ChavesTableAdapter ChavesTableAdapter
        {
            get
            {
                if (chavesTableAdapter == null)
                {
                    chavesTableAdapter = new dExtratosTableAdapters.ChavesTableAdapter();
                    chavesTableAdapter.TrocarStringDeConexao();
                };
                return chavesTableAdapter;
            }
        }

        private dExtratosTableAdapters.CHEquesTableAdapter cHEquesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CHEques
        /// </summary>
        public dExtratosTableAdapters.CHEquesTableAdapter CHEquesTableAdapter
        {
            get
            {
                if (cHEquesTableAdapter == null)
                {
                    cHEquesTableAdapter = new dExtratosTableAdapters.CHEquesTableAdapter();
                    cHEquesTableAdapter.TrocarStringDeConexao();
                };
                return cHEquesTableAdapter;
            }
        }

        #endregion

        internal void CalculaSaldoFinal()
        {
            foreach (SaldoContaCorrenteRow SCCrow in SaldoContaCorrente)
            {
                SCCrow.SaldoTotal = SCCrow.IsSCCValorFNull() ? 0 : SCCrow.SCCValorF;
                if (SCCrow.ContaCorrenTeRow.CCTTipo == 3)
                    SCCrow.SaldoTotal += SCCrow.SCCValorApF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? NOA;
        /// <summary>
        /// 
        /// </summary>
        public int? PGF;
        /// <summary>
        /// 
        /// </summary>
        public int? FRN;
        /// <summary>
        /// 
        /// </summary>
        public long longIdent;
        /// <summary>
        /// 
        /// </summary>
        public long intIdent;
        /// <summary>
        /// 
        /// </summary>
        public Competencia Comp;
        /// <summary>
        /// 
        /// </summary>
        public decimal NOATotal;
        /// <summary>
        /// 
        /// </summary>
        public string strCondominio;
        /// <summary>
        /// 
        /// </summary>
        public string DescricaoPer;
        /// <summary>
        /// 
        /// </summary>
        public decimal PGFValor;
        /// <summary>
        /// 
        /// </summary>
        public int PGFDia;
        /// <summary>
        /// 
        /// </summary>
        public string Fornecedor;


        private enum TipoBusca { chaveCHE, chavePGF }

        private bool BuscaChaves(int chave, TipoBusca TBusca)
        {
            bool encontrado;
            if (TBusca == TipoBusca.chaveCHE)
                encontrado = (ChavesTableAdapter.FillByCHE(Chaves, chave) > 0);
            else
                encontrado = (ChavesTableAdapter.FillByPGF(Chaves, chave) > 0);
            if (!encontrado)
                return false;
            if (TBusca == TipoBusca.chaveCHE)
            {
                NOA = Chaves[0].NOA;
                Comp = new Competencia(Chaves[0].NOACompet);
                NOATotal = 0;
                foreach (ChavesRow Chavesrow in Chaves)
                {
                    Competencia TestComp = new Competencia(Chaves[0].NOACompet);
                    if (TestComp > Comp)
                        Comp = TestComp;
                    NOATotal += Chavesrow.NOATotal;
                }
            }
            else
            {
                NOA = null;
                Comp = null;
                NOATotal = 0;
            };
            PGF = Chaves[0].PGF;
            intIdent = Chaves[0].IsPGFIdentificacaoNull() ? 0 : Chaves[0].PGFIdentificacao;
            longIdent = Chaves[0].IsPGFCodigoDebitoNull() ? 0 : Chaves[0].PGFCodigoDebito;
            DescricaoPer = Chaves[0].IsPGFCodigoDebitoNull() ? "" : Chaves[0].PGFDescricao;
            strCondominio = string.Format("{0} - {1}", Chaves[0].CONCodigo, Chaves[0].CONNome);
            PGFValor = Chaves[0].PGFValor;
            PGFDia = Chaves[0].PGFDia;
            if (Chaves[0].IsFRNNull())
                FRN = null;
            else
            {
                FRN = Chaves[0].FRN;
                Fornecedor = string.Format("{0}{1}",
                                               Chaves[0].IsFRNNull() ? "" : Chaves[0].FRNNome,
                                               Chaves[0].IsFRNFantasiaNull() ? "" : " - " + Chaves[0].FRNFantasia);
            }
            return true;
        }

        /// <summary>
        /// Busca as chaves
        /// </summary>
        /// <param name="CHE"></param>
        /// <returns></returns>
        public bool BuscaChaves(int CHE)
        {
            return BuscaChaves(CHE, TipoBusca.chaveCHE);
        }

        /// <summary>
        /// Busca as chaves
        /// </summary>
        /// <param name="PGF"></param>
        /// <returns></returns>
        public bool BuscaChavesPGF(int PGF)
        {
            return BuscaChaves(PGF, TipoBusca.chavePGF);
        }
    }
}
