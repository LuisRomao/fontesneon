namespace ContaPagar.ContasAgLuz
{
    partial class cContaAgLuz
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.XTipoEsperado = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.XVencimento = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.XValor = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chAcumular = new DevExpress.XtraEditors.CheckEdit();
            this.Label_TipoCod = new DevExpress.XtraEditors.LabelControl();
            this.CB_SemCodigo = new DevExpress.XtraEditors.CheckEdit();
            this.XIdentificacao = new DevExpress.XtraEditors.SpinEdit();
            this.BProcurar = new DevExpress.XtraEditors.SimpleButton();
            this.lFornecedor = new DevExpress.XtraEditors.LookUpEdit();
            this.fORNECEDORESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.XCompetencia = new Framework.objetosNeon.cCompet();
            this.dAcompanhamentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GrupoCadastro = new DevExpress.XtraEditors.GroupControl();
            this.ValorCadastro = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelErro = new DevExpress.XtraEditors.LabelControl();
            this.spinIdent = new DevExpress.XtraEditors.SpinEdit();
            this.spinCodigo = new DevExpress.XtraEditors.SpinEdit();
            this.BtSegue = new DevExpress.XtraEditors.SimpleButton();
            this.BotaoGrava = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditProx = new DevExpress.XtraEditors.DateEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.cCompProx = new Framework.objetosNeon.cCompet();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.dContaAgLuzBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dContaAgLuz = new ContaPagar.ContasAgLuz.dContaAgLuz();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XTipoEsperado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XVencimento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XVencimento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XValor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chAcumular.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CB_SemCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XIdentificacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lFornecedor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORNECEDORESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAcompanhamentoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrupoCadastro)).BeginInit();
            this.GrupoCadastro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ValorCadastro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinIdent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditProx.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditProx.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dContaAgLuzBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dContaAgLuz)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.XTipoEsperado);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.textEdit1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(632, 83);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 54);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(79, 13);
            this.labelControl11.TabIndex = 78;
            this.labelControl11.Text = "Tipo Esperado";
            // 
            // XTipoEsperado
            // 
            this.XTipoEsperado.Location = new System.Drawing.Point(117, 42);
            this.XTipoEsperado.Name = "XTipoEsperado";
            this.XTipoEsperado.Properties.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.XTipoEsperado.Properties.Appearance.Options.UseBackColor = true;
            this.XTipoEsperado.Properties.Columns = 2;
            this.XTipoEsperado.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "D�bito autom�tico"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Conta")});
            this.XTipoEsperado.Size = new System.Drawing.Size(338, 35);
            this.XTipoEsperado.TabIndex = 77;
            this.XTipoEsperado.SelectedIndexChanged += new System.EventHandler(this.XTipoEsperado_SelectedIndexChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(461, 10);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(147, 31);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "Limpar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(98, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "C�digo de barras.";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(117, 16);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(338, 20);
            this.textEdit1.TabIndex = 0;
            this.textEdit1.Enter += new System.EventHandler(this.textEdit1_Enter);
            this.textEdit1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEdit1_KeyDown);
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(14, 62);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Columns = 2;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "D�bito autom�tico"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Conta")});
            this.radioGroup1.Size = new System.Drawing.Size(371, 27);
            this.radioGroup1.TabIndex = 76;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(7, 147);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(66, 13);
            this.labelControl6.TabIndex = 72;
            this.labelControl6.Text = "Identifica��o:";
            // 
            // XVencimento
            // 
            this.XVencimento.EditValue = null;
            this.XVencimento.Location = new System.Drawing.Point(101, 118);
            this.XVencimento.Name = "XVencimento";
            this.XVencimento.Properties.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.XVencimento.Properties.Appearance.Options.UseBackColor = true;
            this.XVencimento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.XVencimento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.XVencimento.Size = new System.Drawing.Size(143, 20);
            this.XVencimento.TabIndex = 71;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(14, 121);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(59, 13);
            this.labelControl5.TabIndex = 70;
            this.labelControl5.Text = "Vencimento:";
            // 
            // XValor
            // 
            this.XValor.Location = new System.Drawing.Point(101, 89);
            this.XValor.Name = "XValor";
            this.XValor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.XValor.Properties.DisplayFormat.FormatString = "n2";
            this.XValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.XValor.Properties.EditFormat.FormatString = "n2";
            this.XValor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.XValor.Size = new System.Drawing.Size(143, 20);
            this.XValor.TabIndex = 69;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(45, 96);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 68;
            this.labelControl4.Text = "Valor:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(7, 58);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(66, 13);
            this.labelControl3.TabIndex = 66;
            this.labelControl3.Text = "Compet�ncia:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(14, 32);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(59, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Fornecedor:";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(7, 105);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(66, 13);
            this.labelControl7.TabIndex = 77;
            this.labelControl7.Text = "Identifica��o:";
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.chAcumular);
            this.groupControl1.Controls.Add(this.Label_TipoCod);
            this.groupControl1.Controls.Add(this.CB_SemCodigo);
            this.groupControl1.Controls.Add(this.XIdentificacao);
            this.groupControl1.Controls.Add(this.BProcurar);
            this.groupControl1.Controls.Add(this.lFornecedor);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.XCompetencia);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.XValor);
            this.groupControl1.Controls.Add(this.XVencimento);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 83);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(632, 176);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Dados da conta";
            // 
            // chAcumular
            // 
            this.chAcumular.Location = new System.Drawing.Point(251, 92);
            this.chAcumular.Name = "chAcumular";
            this.chAcumular.Properties.Caption = "Acumular";
            this.chAcumular.Size = new System.Drawing.Size(75, 19);
            this.chAcumular.TabIndex = 82;
            this.chAcumular.CheckedChanged += new System.EventHandler(this.chAcumular_CheckedChanged);
            // 
            // Label_TipoCod
            // 
            this.Label_TipoCod.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_TipoCod.Appearance.ForeColor = System.Drawing.Color.Red;
            this.Label_TipoCod.Appearance.Options.UseFont = true;
            this.Label_TipoCod.Appearance.Options.UseForeColor = true;
            this.Label_TipoCod.Location = new System.Drawing.Point(254, 147);
            this.Label_TipoCod.Name = "Label_TipoCod";
            this.Label_TipoCod.Size = new System.Drawing.Size(96, 16);
            this.Label_TipoCod.TabIndex = 81;
            this.Label_TipoCod.Text = "<< C�digo NET";
            this.Label_TipoCod.Visible = false;
            // 
            // CB_SemCodigo
            // 
            this.CB_SemCodigo.Location = new System.Drawing.Point(406, 30);
            this.CB_SemCodigo.Name = "CB_SemCodigo";
            this.CB_SemCodigo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.CB_SemCodigo.Properties.Appearance.Options.UseFont = true;
            this.CB_SemCodigo.Properties.Caption = "Conta SEM c�digo de barras";
            this.CB_SemCodigo.Size = new System.Drawing.Size(202, 19);
            this.CB_SemCodigo.TabIndex = 79;
            this.CB_SemCodigo.Visible = false;
            this.CB_SemCodigo.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.CB_SemCodigo_EditValueChanging);
            // 
            // XIdentificacao
            // 
            this.XIdentificacao.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.XIdentificacao.Location = new System.Drawing.Point(101, 144);
            this.XIdentificacao.Name = "XIdentificacao";
            this.XIdentificacao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions1)});
            this.XIdentificacao.Size = new System.Drawing.Size(143, 20);
            this.XIdentificacao.TabIndex = 77;
            // 
            // BProcurar
            // 
            this.BProcurar.Location = new System.Drawing.Point(406, 71);
            this.BProcurar.Name = "BProcurar";
            this.BProcurar.Size = new System.Drawing.Size(202, 56);
            this.BProcurar.TabIndex = 76;
            this.BProcurar.Text = "Procurar";
            this.BProcurar.Visible = false;
            this.BProcurar.Click += new System.EventHandler(this.BProcurar_Click);
            // 
            // lFornecedor
            // 
            this.lFornecedor.Location = new System.Drawing.Point(101, 29);
            this.lFornecedor.Name = "lFornecedor";
            this.lFornecedor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lFornecedor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "FRN Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lFornecedor.Properties.DataSource = this.fORNECEDORESBindingSource;
            this.lFornecedor.Properties.DisplayMember = "FRNNome";
            this.lFornecedor.Properties.ShowHeader = false;
            this.lFornecedor.Properties.ValueMember = "FRN";
            this.lFornecedor.Properties.PopupFilter += new DevExpress.XtraEditors.Controls.PopupFilterEventHandler(this.lFornecedor_Properties_PopupFilter);
            this.lFornecedor.Size = new System.Drawing.Size(265, 20);
            this.lFornecedor.TabIndex = 75;
            this.lFornecedor.EditValueChanged += new System.EventHandler(this.lFornecedor_EditValueChanged);
            // 
            // fORNECEDORESBindingSource
            // 
            this.fORNECEDORESBindingSource.DataMember = "FRNLookup";
            this.fORNECEDORESBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // XCompetencia
            // 
            this.XCompetencia.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.XCompetencia.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XCompetencia.Appearance.Options.UseBackColor = true;
            this.XCompetencia.Appearance.Options.UseFont = true;
            this.XCompetencia.CaixaAltaGeral = true;
            this.XCompetencia.Doc = System.Windows.Forms.DockStyle.None;
            this.XCompetencia.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.XCompetencia.IsNull = false;
            this.XCompetencia.Location = new System.Drawing.Point(101, 55);
            this.XCompetencia.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.XCompetencia.LookAndFeel.UseDefaultLookAndFeel = false;
            this.XCompetencia.Name = "XCompetencia";
            this.XCompetencia.PermiteNulo = true;
            this.XCompetencia.ReadOnly = false;
            this.XCompetencia.Size = new System.Drawing.Size(117, 27);
            this.XCompetencia.somenteleitura = false;
            this.XCompetencia.TabIndex = 65;
            this.XCompetencia.Titulo = null;
            // 
            // dAcompanhamentoBindingSource
            // 
            this.dAcompanhamentoBindingSource.DataSource = typeof(ContaPagar.ContasAgLuz.dAcompanhamento);
            this.dAcompanhamentoBindingSource.Position = 0;
            // 
            // GrupoCadastro
            // 
            this.GrupoCadastro.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrupoCadastro.AppearanceCaption.Options.UseFont = true;
            this.GrupoCadastro.Controls.Add(this.ValorCadastro);
            this.GrupoCadastro.Controls.Add(this.labelControl10);
            this.GrupoCadastro.Controls.Add(this.labelErro);
            this.GrupoCadastro.Controls.Add(this.spinIdent);
            this.GrupoCadastro.Controls.Add(this.spinCodigo);
            this.GrupoCadastro.Controls.Add(this.BtSegue);
            this.GrupoCadastro.Controls.Add(this.BotaoGrava);
            this.GrupoCadastro.Controls.Add(this.dateEditProx);
            this.GrupoCadastro.Controls.Add(this.labelControl9);
            this.GrupoCadastro.Controls.Add(this.cCompProx);
            this.GrupoCadastro.Controls.Add(this.labelControl8);
            this.GrupoCadastro.Controls.Add(this.labelControl7);
            this.GrupoCadastro.Controls.Add(this.radioGroup1);
            this.GrupoCadastro.Controls.Add(this.lookupCondominio_F1);
            this.GrupoCadastro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrupoCadastro.Location = new System.Drawing.Point(0, 259);
            this.GrupoCadastro.Name = "GrupoCadastro";
            this.GrupoCadastro.Size = new System.Drawing.Size(632, 259);
            this.GrupoCadastro.TabIndex = 2;
            this.GrupoCadastro.Text = "Cadastro (Peri�dico)";
            // 
            // ValorCadastro
            // 
            this.ValorCadastro.Location = new System.Drawing.Point(127, 163);
            this.ValorCadastro.Name = "ValorCadastro";
            this.ValorCadastro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValorCadastro.Properties.DisplayFormat.FormatString = "n2";
            this.ValorCadastro.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ValorCadastro.Properties.EditFormat.FormatString = "n2";
            this.ValorCadastro.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ValorCadastro.Size = new System.Drawing.Size(143, 20);
            this.ValorCadastro.TabIndex = 90;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(7, 166);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(28, 13);
            this.labelControl10.TabIndex = 89;
            this.labelControl10.Text = "Valor:";
            // 
            // labelErro
            // 
            this.labelErro.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelErro.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelErro.Appearance.Options.UseFont = true;
            this.labelErro.Appearance.Options.UseForeColor = true;
            this.labelErro.Location = new System.Drawing.Point(5, 213);
            this.labelErro.Name = "labelErro";
            this.labelErro.Size = new System.Drawing.Size(185, 19);
            this.labelErro.TabIndex = 87;
            this.labelErro.Text = "Compet�ncia incorreta";
            // 
            // spinIdent
            // 
            this.spinIdent.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinIdent.Location = new System.Drawing.Point(206, 102);
            this.spinIdent.Name = "spinIdent";
            this.spinIdent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions2)});
            this.spinIdent.Size = new System.Drawing.Size(117, 20);
            this.spinIdent.TabIndex = 86;
            // 
            // spinCodigo
            // 
            this.spinCodigo.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinCodigo.Location = new System.Drawing.Point(79, 102);
            this.spinCodigo.Name = "spinCodigo";
            this.spinCodigo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions3)});
            this.spinCodigo.Properties.ReadOnly = true;
            this.spinCodigo.Size = new System.Drawing.Size(117, 20);
            this.spinCodigo.TabIndex = 85;
            // 
            // BtSegue
            // 
            this.BtSegue.Location = new System.Drawing.Point(406, 62);
            this.BtSegue.Name = "BtSegue";
            this.BtSegue.Size = new System.Drawing.Size(202, 56);
            this.BtSegue.TabIndex = 84;
            this.BtSegue.Text = "Segue >>";
            this.BtSegue.Click += new System.EventHandler(this.BtSegue_Click);
            // 
            // BotaoGrava
            // 
            this.BotaoGrava.Location = new System.Drawing.Point(406, 32);
            this.BotaoGrava.Name = "BotaoGrava";
            this.BotaoGrava.Size = new System.Drawing.Size(202, 23);
            this.BotaoGrava.TabIndex = 83;
            this.BotaoGrava.Text = "Alterar Cadastro";
            this.BotaoGrava.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // dateEditProx
            // 
            this.dateEditProx.EditValue = null;
            this.dateEditProx.Location = new System.Drawing.Point(315, 135);
            this.dateEditProx.Name = "dateEditProx";
            this.dateEditProx.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dateEditProx.Properties.Appearance.Options.UseBackColor = true;
            this.dateEditProx.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions4),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down)});
            this.dateEditProx.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditProx.Size = new System.Drawing.Size(119, 20);
            this.dateEditProx.TabIndex = 82;
            this.dateEditProx.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditProx_ButtonClick);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(250, 138);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(59, 13);
            this.labelControl9.TabIndex = 81;
            this.labelControl9.Text = "Vencimento:";
            // 
            // cCompProx
            // 
            this.cCompProx.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.cCompProx.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompProx.Appearance.Options.UseBackColor = true;
            this.cCompProx.Appearance.Options.UseFont = true;
            this.cCompProx.CaixaAltaGeral = true;
            this.cCompProx.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompProx.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompProx.IsNull = false;
            this.cCompProx.Location = new System.Drawing.Point(127, 130);
            this.cCompProx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompProx.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompProx.Name = "cCompProx";
            this.cCompProx.PermiteNulo = false;
            this.cCompProx.ReadOnly = false;
            this.cCompProx.Size = new System.Drawing.Size(117, 27);
            this.cCompProx.somenteleitura = false;
            this.cCompProx.TabIndex = 80;
            this.cCompProx.Titulo = null;
            this.cCompProx.OnChange += new System.EventHandler(this.cCompProx_OnChange);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(7, 138);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(114, 13);
            this.labelControl8.TabIndex = 79;
            this.labelControl8.Text = "Compet�ncia esperada:";
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(14, 36);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(371, 20);
            this.lookupCondominio_F1.somenteleitura = true;
            this.lookupCondominio_F1.TabIndex = 75;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            // 
            // dContaAgLuzBindingSource
            // 
            this.dContaAgLuzBindingSource.DataMember = "Diario";
            this.dContaAgLuzBindingSource.DataSource = this.dContaAgLuz;
            // 
            // dContaAgLuz
            // 
            this.dContaAgLuz.DataSetName = "dContaAgLuz";
            this.dContaAgLuz.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cContaAgLuz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GrupoCadastro);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cContaAgLuz";
            this.Size = new System.Drawing.Size(632, 518);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XTipoEsperado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XVencimento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XVencimento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XValor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chAcumular.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CB_SemCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XIdentificacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lFornecedor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORNECEDORESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAcompanhamentoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrupoCadastro)).EndInit();
            this.GrupoCadastro.ResumeLayout(false);
            this.GrupoCadastro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ValorCadastro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinIdent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditProx.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditProx.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dContaAgLuzBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dContaAgLuz)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private Framework.objetosNeon.cCompet XCompetencia;
        private DevExpress.XtraEditors.DateEdit XVencimento;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CalcEdit XValor;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private System.Windows.Forms.BindingSource dContaAgLuzBindingSource;
        private dContaAgLuz dContaAgLuz;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.DateEdit dateEditProx;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private Framework.objetosNeon.cCompet cCompProx;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LookUpEdit lFornecedor;
        private System.Windows.Forms.BindingSource fORNECEDORESBindingSource;
        private System.Windows.Forms.BindingSource dAcompanhamentoBindingSource;
        private DevExpress.XtraEditors.SimpleButton BProcurar;
        private DevExpress.XtraEditors.GroupControl GrupoCadastro;
        private DevExpress.XtraEditors.SpinEdit XIdentificacao;
        private DevExpress.XtraEditors.SimpleButton BtSegue;
        private DevExpress.XtraEditors.SimpleButton BotaoGrava;
        private DevExpress.XtraEditors.SpinEdit spinCodigo;
        private DevExpress.XtraEditors.SpinEdit spinIdent;
        private DevExpress.XtraEditors.LabelControl labelErro;
        private DevExpress.XtraEditors.CalcEdit ValorCadastro;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.RadioGroup XTipoEsperado;
        private DevExpress.XtraEditors.CheckEdit CB_SemCodigo;
        private DevExpress.XtraEditors.LabelControl Label_TipoCod;
        private DevExpress.XtraEditors.CheckEdit chAcumular;
    }
}
