﻿namespace ContaPagar.ContasAgLuz
{


    partial class dAcompanhamento
    {
        private static dAcompanhamento _dAcompanhamentoSt;

        /// <summary>
        /// dataset estático:dAcompanhamento
        /// </summary>
        public static dAcompanhamento dAcompanhamentoSt
        {
            get
            {
                if (_dAcompanhamentoSt == null)
                {
                    _dAcompanhamentoSt = new dAcompanhamento();
                    _dAcompanhamentoSt.FORNECEDORESTableAdapter.Fill(_dAcompanhamentoSt.FORNECEDORES);
                }
                return _dAcompanhamentoSt;
            }
        }

        private dAcompanhamentoTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FORNECEDORES
        /// </summary>
        public dAcompanhamentoTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new dAcompanhamentoTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao();
                };
                return fORNECEDORESTableAdapter;
            }
        }

        private dAcompanhamentoTableAdapters.ContasTableAdapter contasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Contas
        /// </summary>
        public dAcompanhamentoTableAdapters.ContasTableAdapter ContasTableAdapter
        {
            get
            {
                if (contasTableAdapter == null)
                {
                    contasTableAdapter = new dAcompanhamentoTableAdapters.ContasTableAdapter();
                    contasTableAdapter.TrocarStringDeConexao();
                };
                return contasTableAdapter;
            }
        }

        private dAcompanhamentoTableAdapters.DebitoTableAdapter debitoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Debito
        /// </summary>
        public dAcompanhamentoTableAdapters.DebitoTableAdapter DebitoTableAdapter
        {
            get
            {
                if (debitoTableAdapter == null)
                {
                    debitoTableAdapter = new dAcompanhamentoTableAdapters.DebitoTableAdapter();
                    debitoTableAdapter.TrocarStringDeConexao();
                };
                return debitoTableAdapter;
            }
        }

        private dAcompanhamentoTableAdapters.PLASTableAdapter pLASTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PLAS
        /// </summary>
        public dAcompanhamentoTableAdapters.PLASTableAdapter PLASTableAdapter
        {
            get
            {
                if (pLASTableAdapter == null)
                {
                    pLASTableAdapter = new dAcompanhamentoTableAdapters.PLASTableAdapter();
                    pLASTableAdapter.TrocarStringDeConexao();
                };
                return pLASTableAdapter;
            }
        }

    }
}
