﻿namespace ContaPagar.ContasAgLuz
{
    partial class cTratamentoDebAut
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cTratamentoDebAut));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSCCAberto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSCCData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSCCValorI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CalcEditpadrao = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colSCCValorF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dExtratosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dExtratos = new ContaPagar.ContasAgLuz.dExtratos();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCT_BCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAtiva = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTPrincipal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAplicacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCCDCategoria = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDCodHistorico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDConsolidado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDescComplemento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDTipoLancamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.labelCONDOMINIO = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.textEditFornecedor = new DevExpress.XtraEditors.TextEdit();
            this.textEditDescricao = new DevExpress.XtraEditors.TextEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditIdent = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cBotaoIntegradorP = new Framework.Integrador.cBotaoIntegrador();
            this.cBotaoIntegradorN = new Framework.Integrador.cBotaoIntegrador();
            this.cBotaoIntegradorC = new Framework.Integrador.cBotaoIntegrador();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEditpadrao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtratosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtratos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFornecedor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescricao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditIdent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl2.Size = new System.Drawing.Size(1529, 714);
            this.panelControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl2_Paint);
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.Bisque;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSCC,
            this.colSCCAberto,
            this.colSCCData,
            this.colSCCValorI,
            this.colSCCValorF,
            this.colSaldoTotal});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsDetail.ShowDetailTabs = false;
            this.gridView2.OptionsDetail.SmartDetailExpand = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView2_RowCellStyle);
            // 
            // colSCC
            // 
            this.colSCC.FieldName = "SCC";
            this.colSCC.Name = "colSCC";
            this.colSCC.OptionsColumn.ReadOnly = true;
            // 
            // colSCCAberto
            // 
            this.colSCCAberto.Caption = "Aberto";
            this.colSCCAberto.FieldName = "SCCAberto";
            this.colSCCAberto.Name = "colSCCAberto";
            this.colSCCAberto.OptionsColumn.ReadOnly = true;
            this.colSCCAberto.Visible = true;
            this.colSCCAberto.VisibleIndex = 2;
            this.colSCCAberto.Width = 82;
            // 
            // colSCCData
            // 
            this.colSCCData.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colSCCData.AppearanceCell.Options.UseFont = true;
            this.colSCCData.Caption = "Data";
            this.colSCCData.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colSCCData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colSCCData.FieldName = "SCCData";
            this.colSCCData.Name = "colSCCData";
            this.colSCCData.OptionsColumn.ReadOnly = true;
            this.colSCCData.Visible = true;
            this.colSCCData.VisibleIndex = 0;
            this.colSCCData.Width = 102;
            // 
            // colSCCValorI
            // 
            this.colSCCValorI.Caption = "Saldo Inicial";
            this.colSCCValorI.ColumnEdit = this.CalcEditpadrao;
            this.colSCCValorI.DisplayFormat.FormatString = "n2";
            this.colSCCValorI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSCCValorI.FieldName = "SCCValorI";
            this.colSCCValorI.Name = "colSCCValorI";
            this.colSCCValorI.OptionsColumn.ReadOnly = true;
            this.colSCCValorI.Visible = true;
            this.colSCCValorI.VisibleIndex = 1;
            this.colSCCValorI.Width = 107;
            // 
            // CalcEditpadrao
            // 
            this.CalcEditpadrao.AutoHeight = false;
            this.CalcEditpadrao.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEditpadrao.Mask.EditMask = "n2";
            this.CalcEditpadrao.Mask.UseMaskAsDisplayFormat = true;
            this.CalcEditpadrao.Name = "CalcEditpadrao";
            // 
            // colSCCValorF
            // 
            this.colSCCValorF.Caption = "Saldo Final";
            this.colSCCValorF.ColumnEdit = this.CalcEditpadrao;
            this.colSCCValorF.DisplayFormat.FormatString = "n2";
            this.colSCCValorF.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSCCValorF.FieldName = "SCCValorF";
            this.colSCCValorF.Name = "colSCCValorF";
            this.colSCCValorF.OptionsColumn.ReadOnly = true;
            this.colSCCValorF.Visible = true;
            this.colSCCValorF.VisibleIndex = 3;
            this.colSCCValorF.Width = 96;
            // 
            // colSaldoTotal
            // 
            this.colSaldoTotal.ColumnEdit = this.CalcEditpadrao;
            this.colSaldoTotal.FieldName = "SaldoTotal";
            this.colSaldoTotal.Name = "colSaldoTotal";
            this.colSaldoTotal.Visible = true;
            this.colSaldoTotal.VisibleIndex = 4;
            this.colSaldoTotal.Width = 105;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "ContaCorrenTe";
            this.gridControl1.DataSource = this.dExtratosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode2.LevelTemplate = this.gridView3;
            gridLevelNode2.RelationName = "FK_ContaCorrenteDetalhe_SaldoContaCorrente";
            gridLevelNode1.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            gridLevelNode1.RelationName = "FK_SaldoContaCorrente_ContaCorrenTe";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(2, 57);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.CalcEditpadrao,
            this.repositoryItemImageComboBox1,
            this.repositoryItemImageComboBox2});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1231, 651);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView3,
            this.gridView2});
            this.gridControl1.Load += new System.EventHandler(this.gridControl1_Load);
            this.gridControl1.ViewRegistered += new DevExpress.XtraGrid.ViewOperationEventHandler(this.gridControl1_ViewRegistered);
            // 
            // dExtratosBindingSource
            // 
            this.dExtratosBindingSource.DataSource = this.dExtratos;
            this.dExtratosBindingSource.Position = 0;
            // 
            // dExtratos
            // 
            this.dExtratos.DataSetName = "dExtratos";
            this.dExtratos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCCT,
            this.colCCT_BCO,
            this.colCCTAgencia,
            this.colCCTConta,
            this.colCCTAtiva,
            this.colCCTPrincipal,
            this.colCCTAplicacao,
            this.colCCTTitulo});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsDetail.SmartDetailExpand = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCCTTitulo, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCCT
            // 
            this.colCCT.FieldName = "CCT";
            this.colCCT.Name = "colCCT";
            this.colCCT.OptionsColumn.ReadOnly = true;
            // 
            // colCCT_BCO
            // 
            this.colCCT_BCO.Caption = "Banco";
            this.colCCT_BCO.FieldName = "CCT_BCO";
            this.colCCT_BCO.Name = "colCCT_BCO";
            this.colCCT_BCO.OptionsColumn.ReadOnly = true;
            this.colCCT_BCO.Visible = true;
            this.colCCT_BCO.VisibleIndex = 1;
            this.colCCT_BCO.Width = 104;
            // 
            // colCCTAgencia
            // 
            this.colCCTAgencia.Caption = "Agencia";
            this.colCCTAgencia.FieldName = "CCTAgencia";
            this.colCCTAgencia.Name = "colCCTAgencia";
            this.colCCTAgencia.OptionsColumn.ReadOnly = true;
            this.colCCTAgencia.Visible = true;
            this.colCCTAgencia.VisibleIndex = 2;
            this.colCCTAgencia.Width = 54;
            // 
            // colCCTConta
            // 
            this.colCCTConta.Caption = "Conta";
            this.colCCTConta.FieldName = "CCTConta";
            this.colCCTConta.Name = "colCCTConta";
            this.colCCTConta.OptionsColumn.ReadOnly = true;
            this.colCCTConta.Visible = true;
            this.colCCTConta.VisibleIndex = 3;
            this.colCCTConta.Width = 94;
            // 
            // colCCTAtiva
            // 
            this.colCCTAtiva.Caption = "Ativa";
            this.colCCTAtiva.FieldName = "CCTAtiva";
            this.colCCTAtiva.Name = "colCCTAtiva";
            this.colCCTAtiva.OptionsColumn.ReadOnly = true;
            this.colCCTAtiva.Visible = true;
            this.colCCTAtiva.VisibleIndex = 4;
            this.colCCTAtiva.Width = 51;
            // 
            // colCCTPrincipal
            // 
            this.colCCTPrincipal.Caption = "Principal";
            this.colCCTPrincipal.FieldName = "CCTPrincipal";
            this.colCCTPrincipal.Name = "colCCTPrincipal";
            this.colCCTPrincipal.OptionsColumn.ReadOnly = true;
            this.colCCTPrincipal.Visible = true;
            this.colCCTPrincipal.VisibleIndex = 5;
            this.colCCTPrincipal.Width = 52;
            // 
            // colCCTAplicacao
            // 
            this.colCCTAplicacao.Caption = "Aplicação";
            this.colCCTAplicacao.FieldName = "CCTAplicacao";
            this.colCCTAplicacao.Name = "colCCTAplicacao";
            this.colCCTAplicacao.OptionsColumn.ReadOnly = true;
            this.colCCTAplicacao.Visible = true;
            this.colCCTAplicacao.VisibleIndex = 6;
            this.colCCTAplicacao.Width = 66;
            // 
            // colCCTTitulo
            // 
            this.colCCTTitulo.Caption = "Título";
            this.colCCTTitulo.FieldName = "CCTTitulo";
            this.colCCTTitulo.Name = "colCCTTitulo";
            this.colCCTTitulo.OptionsColumn.ReadOnly = true;
            this.colCCTTitulo.Visible = true;
            this.colCCTTitulo.VisibleIndex = 0;
            this.colCCTTitulo.Width = 267;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // gridView3
            // 
            this.gridView3.Appearance.Row.BackColor = System.Drawing.Color.LightYellow;
            this.gridView3.Appearance.Row.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView3.Appearance.Row.Options.UseBackColor = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCCDCategoria,
            this.colCCDCodHistorico,
            this.colCCDConsolidado,
            this.colCCDDescComplemento,
            this.colCCDDescricao,
            this.colCCDDocumento,
            this.colCCDTipoLancamento,
            this.colCCDValor,
            this.colCCD});
            this.gridView3.GridControl = this.gridControl1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView3_RowStyle);
            // 
            // colCCDCategoria
            // 
            this.colCCDCategoria.Caption = "Categoria";
            this.colCCDCategoria.FieldName = "CCDCategoria";
            this.colCCDCategoria.Name = "colCCDCategoria";
            this.colCCDCategoria.OptionsColumn.ReadOnly = true;
            // 
            // colCCDCodHistorico
            // 
            this.colCCDCodHistorico.Caption = "Cod. Histórico";
            this.colCCDCodHistorico.FieldName = "CCDCodHistorico";
            this.colCCDCodHistorico.Name = "colCCDCodHistorico";
            this.colCCDCodHistorico.OptionsColumn.ReadOnly = true;
            // 
            // colCCDConsolidado
            // 
            this.colCCDConsolidado.Caption = "Status";
            this.colCCDConsolidado.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCCDConsolidado.FieldName = "CCDConsolidado";
            this.colCCDConsolidado.Name = "colCCDConsolidado";
            this.colCCDConsolidado.OptionsColumn.ReadOnly = true;
            this.colCCDConsolidado.Visible = true;
            this.colCCDConsolidado.VisibleIndex = 4;
            this.colCCDConsolidado.Width = 117;
            // 
            // colCCDDescComplemento
            // 
            this.colCCDDescComplemento.Caption = "Complemento";
            this.colCCDDescComplemento.FieldName = "CCDDescComplemento";
            this.colCCDDescComplemento.Name = "colCCDDescComplemento";
            this.colCCDDescComplemento.OptionsColumn.ReadOnly = true;
            this.colCCDDescComplemento.Visible = true;
            this.colCCDDescComplemento.VisibleIndex = 1;
            this.colCCDDescComplemento.Width = 244;
            // 
            // colCCDDescricao
            // 
            this.colCCDDescricao.Caption = "Descrição";
            this.colCCDDescricao.FieldName = "CCDDescricao";
            this.colCCDDescricao.Name = "colCCDDescricao";
            this.colCCDDescricao.OptionsColumn.ReadOnly = true;
            this.colCCDDescricao.Visible = true;
            this.colCCDDescricao.VisibleIndex = 0;
            this.colCCDDescricao.Width = 218;
            // 
            // colCCDDocumento
            // 
            this.colCCDDocumento.Caption = "Doc";
            this.colCCDDocumento.FieldName = "CCDDocumento";
            this.colCCDDocumento.Name = "colCCDDocumento";
            this.colCCDDocumento.OptionsColumn.ReadOnly = true;
            this.colCCDDocumento.Visible = true;
            this.colCCDDocumento.VisibleIndex = 3;
            this.colCCDDocumento.Width = 97;
            // 
            // colCCDTipoLancamento
            // 
            this.colCCDTipoLancamento.Caption = "Tipo de Lançamento";
            this.colCCDTipoLancamento.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colCCDTipoLancamento.FieldName = "CCDTipoLancamento";
            this.colCCDTipoLancamento.Name = "colCCDTipoLancamento";
            this.colCCDTipoLancamento.OptionsColumn.ReadOnly = true;
            // 
            // colCCDValor
            // 
            this.colCCDValor.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colCCDValor.AppearanceCell.Options.UseFont = true;
            this.colCCDValor.Caption = "Valor";
            this.colCCDValor.ColumnEdit = this.CalcEditpadrao;
            this.colCCDValor.DisplayFormat.FormatString = "n2";
            this.colCCDValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCCDValor.FieldName = "CCDValor";
            this.colCCDValor.Name = "colCCDValor";
            this.colCCDValor.OptionsColumn.ReadOnly = true;
            this.colCCDValor.Visible = true;
            this.colCCDValor.VisibleIndex = 2;
            this.colCCDValor.Width = 97;
            // 
            // colCCD
            // 
            this.colCCD.FieldName = "CCD";
            this.colCCD.Name = "colCCD";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.gridControl1);
            this.panelControl3.Controls.Add(this.panelControl5);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(292, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1235, 710);
            this.panelControl3.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.labelCONDOMINIO);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(2, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1231, 55);
            this.panelControl5.TabIndex = 1;
            // 
            // labelCONDOMINIO
            // 
            this.labelCONDOMINIO.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCONDOMINIO.Appearance.Options.UseFont = true;
            this.labelCONDOMINIO.Location = new System.Drawing.Point(4, 15);
            this.labelCONDOMINIO.Name = "labelCONDOMINIO";
            this.labelCONDOMINIO.Size = new System.Drawing.Size(117, 19);
            this.labelCONDOMINIO.TabIndex = 1;
            this.labelCONDOMINIO.Text = "Data Prevista:";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.textEditFornecedor);
            this.panelControl4.Controls.Add(this.textEditDescricao);
            this.panelControl4.Controls.Add(this.spinEdit1);
            this.panelControl4.Controls.Add(this.cCompet1);
            this.panelControl4.Controls.Add(this.labelControl5);
            this.panelControl4.Controls.Add(this.spinEditIdent);
            this.panelControl4.Controls.Add(this.labelControl4);
            this.panelControl4.Controls.Add(this.cBotaoIntegradorP);
            this.panelControl4.Controls.Add(this.cBotaoIntegradorN);
            this.panelControl4.Controls.Add(this.cBotaoIntegradorC);
            this.panelControl4.Controls.Add(this.labelControl3);
            this.panelControl4.Controls.Add(this.simpleButton3);
            this.panelControl4.Controls.Add(this.simpleButton2);
            this.panelControl4.Controls.Add(this.simpleButton1);
            this.panelControl4.Controls.Add(this.labelControl2);
            this.panelControl4.Controls.Add(this.calcEdit1);
            this.panelControl4.Controls.Add(this.dateEdit1);
            this.panelControl4.Controls.Add(this.labelControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(290, 710);
            this.panelControl4.TabIndex = 1;
            // 
            // textEditFornecedor
            // 
            this.textEditFornecedor.Location = new System.Drawing.Point(7, 30);
            this.textEditFornecedor.MenuManager = this.BarManager_F;
            this.textEditFornecedor.Name = "textEditFornecedor";
            this.textEditFornecedor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditFornecedor.Properties.Appearance.Options.UseFont = true;
            this.textEditFornecedor.Properties.ReadOnly = true;
            this.textEditFornecedor.Size = new System.Drawing.Size(277, 20);
            this.textEditFornecedor.TabIndex = 68;
            this.textEditFornecedor.Visible = false;
            // 
            // textEditDescricao
            // 
            this.textEditDescricao.Location = new System.Drawing.Point(7, 4);
            this.textEditDescricao.MenuManager = this.BarManager_F;
            this.textEditDescricao.Name = "textEditDescricao";
            this.textEditDescricao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditDescricao.Properties.Appearance.Options.UseFont = true;
            this.textEditDescricao.Properties.ReadOnly = true;
            this.textEditDescricao.Size = new System.Drawing.Size(277, 20);
            this.textEditDescricao.TabIndex = 67;
            this.textEditDescricao.Visible = false;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(144, 134);
            this.spinEdit1.MenuManager = this.BarManager_F;
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.ReadOnly = true;
            this.spinEdit1.Size = new System.Drawing.Size(140, 20);
            this.spinEdit1.TabIndex = 66;
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.Appearance.Options.UseFont = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(147, 186);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = true;
            this.cCompet1.Size = new System.Drawing.Size(137, 27);
            this.cCompet1.somenteleitura = true;
            this.cCompet1.TabIndex = 65;
            this.cCompet1.Titulo = null;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(29, 188);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(112, 19);
            this.labelControl5.TabIndex = 17;
            this.labelControl5.Text = "Competência:";
            // 
            // spinEditIdent
            // 
            this.spinEditIdent.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditIdent.Location = new System.Drawing.Point(144, 160);
            this.spinEditIdent.MenuManager = this.BarManager_F;
            this.spinEditIdent.Name = "spinEditIdent";
            this.spinEditIdent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditIdent.Properties.ReadOnly = true;
            this.spinEditIdent.Size = new System.Drawing.Size(140, 20);
            this.spinEditIdent.TabIndex = 16;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(84, 148);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 19);
            this.labelControl4.TabIndex = 14;
            this.labelControl4.Text = "Conta:";
            // 
            // cBotaoIntegradorP
            // 
            this.cBotaoIntegradorP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBotaoIntegradorP.Appearance.Options.UseFont = true;
            this.cBotaoIntegradorP.CaixaAltaGeral = true;
            this.cBotaoIntegradorP.Doc = System.Windows.Forms.DockStyle.None;
            this.cBotaoIntegradorP.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cBotaoIntegradorP.Location = new System.Drawing.Point(145, 428);
            this.cBotaoIntegradorP.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoIntegradorP.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cBotaoIntegradorP.Name = "cBotaoIntegradorP";
            this.cBotaoIntegradorP.Size = new System.Drawing.Size(126, 61);
            this.cBotaoIntegradorP.somenteleitura = false;
            this.cBotaoIntegradorP.TabIndex = 13;
            this.cBotaoIntegradorP.Tipo = Framework.Integrador.TiposIntegra.Periodicos;
            this.cBotaoIntegradorP.Titulo = null;
            // 
            // cBotaoIntegradorN
            // 
            this.cBotaoIntegradorN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBotaoIntegradorN.Appearance.Options.UseFont = true;
            this.cBotaoIntegradorN.CaixaAltaGeral = true;
            this.cBotaoIntegradorN.Doc = System.Windows.Forms.DockStyle.None;
            this.cBotaoIntegradorN.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cBotaoIntegradorN.Location = new System.Drawing.Point(7, 497);
            this.cBotaoIntegradorN.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoIntegradorN.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cBotaoIntegradorN.Name = "cBotaoIntegradorN";
            this.cBotaoIntegradorN.Size = new System.Drawing.Size(126, 61);
            this.cBotaoIntegradorN.somenteleitura = false;
            this.cBotaoIntegradorN.TabIndex = 12;
            this.cBotaoIntegradorN.Tipo = Framework.Integrador.TiposIntegra.Nota;
            this.cBotaoIntegradorN.Titulo = null;
            // 
            // cBotaoIntegradorC
            // 
            this.cBotaoIntegradorC.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBotaoIntegradorC.Appearance.Options.UseFont = true;
            this.cBotaoIntegradorC.CaixaAltaGeral = true;
            this.cBotaoIntegradorC.Doc = System.Windows.Forms.DockStyle.None;
            this.cBotaoIntegradorC.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cBotaoIntegradorC.Location = new System.Drawing.Point(7, 428);
            this.cBotaoIntegradorC.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoIntegradorC.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cBotaoIntegradorC.Name = "cBotaoIntegradorC";
            this.cBotaoIntegradorC.Size = new System.Drawing.Size(126, 61);
            this.cBotaoIntegradorC.somenteleitura = false;
            this.cBotaoIntegradorC.TabIndex = 11;
            this.cBotaoIntegradorC.Tipo = Framework.Integrador.TiposIntegra.Cheque;
            this.cBotaoIntegradorC.Titulo = null;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(17, 401);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(77, 19);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Rastrear:";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(17, 344);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(254, 52);
            this.simpleButton3.TabIndex = 6;
            this.simpleButton3.Text = "Manual";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(16, 286);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(254, 52);
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "Cadastrar Cheque";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(17, 228);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(254, 52);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "Ignorar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(17, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(121, 19);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Valor Previsto:";
            // 
            // calcEdit1
            // 
            this.calcEdit1.Location = new System.Drawing.Point(144, 102);
            this.calcEdit1.MenuManager = this.BarManager_F;
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEdit1.Properties.Appearance.Options.UseFont = true;
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Properties.DisplayFormat.FormatString = "n2";
            this.calcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit1.Properties.ReadOnly = true;
            this.calcEdit1.Size = new System.Drawing.Size(140, 26);
            this.calcEdit1.TabIndex = 2;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(144, 72);
            this.dateEdit1.MenuManager = this.BarManager_F;
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(140, 26);
            this.dateEdit1.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(21, 74);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(117, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Data Prevista:";
            // 
            // cTratamentoDebAut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "cTratamentoDebAut";
            this.Size = new System.Drawing.Size(1529, 807);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEditpadrao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtratosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExtratos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFornecedor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescricao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditIdent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource dExtratosBindingSource;
        private dExtratos dExtratos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT_BCO;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAtiva;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTPrincipal;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAplicacao;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTTitulo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colSCC;
        private DevExpress.XtraGrid.Columns.GridColumn colSCCAberto;
        private DevExpress.XtraGrid.Columns.GridColumn colSCCData;
        private DevExpress.XtraGrid.Columns.GridColumn colSCCValorI;
        private DevExpress.XtraGrid.Columns.GridColumn colSCCValorF;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDCategoria;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDCodHistorico;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDConsolidado;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDescComplemento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDTipoLancamento;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit CalcEditpadrao;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private Framework.Integrador.cBotaoIntegrador cBotaoIntegradorP;
        private Framework.Integrador.cBotaoIntegrador cBotaoIntegradorN;
        private Framework.Integrador.cBotaoIntegrador cBotaoIntegradorC;
        private DevExpress.XtraGrid.Columns.GridColumn colCCD;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SpinEdit spinEditIdent;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LabelControl labelCONDOMINIO;
        private DevExpress.XtraEditors.TextEdit textEditDescricao;
        private DevExpress.XtraEditors.TextEdit textEditFornecedor;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoTotal;
    }
}
