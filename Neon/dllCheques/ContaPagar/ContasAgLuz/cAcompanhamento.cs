using System;
using System.Drawing;
using System.Windows.Forms;
using Framework;
using VirEnumeracoesNeon;

namespace ContaPagar.ContasAgLuz
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAcompanhamento : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cAcompanhamento()
        {
            InitializeComponent();
            Enumeracoes.VirEnumTipoPagPeriodico.CarregaEditorDaGrid(colPGFForma);
            dAcompanhamento.PLASTableAdapter.Fill(dAcompanhamento.PLAS);
            checkedListBoxControl1.Items.Clear();
            foreach (dAcompanhamento.PLASRow rowPLAS in dAcompanhamento.PLAS)
            {
                checkedListBoxControl1.Items.Add (rowPLAS.PGF_PLA, rowPLAS.PLADescricao,CheckState.Checked, true);
            }
        }

        private void Carregar()
        {
            try
            {
                bandedGridView1.BeginDataUpdate();
                dAcompanhamento.Debito.Clear();
                dAcompanhamento.DebitoTableAdapter.ClearBeforeFill = false;
                foreach (DevExpress.XtraEditors.Controls.CheckedListBoxItem chit in checkedListBoxControl1.Items)
                    if (chit.CheckState == CheckState.Checked)
                        dAcompanhamento.DebitoTableAdapter.FillByPLA(dAcompanhamento.Debito, (string)chit.Value);
                foreach (dAcompanhamento.DebitoRow row in dAcompanhamento.Debito)
                {
                    Framework.objetosNeon.Competencia compPAG = new Framework.objetosNeon.Competencia(row.PGFProximaCompetencia).Add(row.PGFRef);
                    row.DataConta = compPAG.DataNaCompetencia(row.PGFDia, Framework.objetosNeon.Competencia.Regime.mes);
                }
            }
            finally
            {
                dAcompanhamento.DebitoTableAdapter.ClearBeforeFill = true;
                bandedGridView1.EndDataUpdate();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Carregar();                    
        }
        
        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column == colPGFProximaCompetencia) 
                if ((e.Value != null) && (e.Value != DBNull.Value))
                {
                    int iValor;
                    if (int.TryParse(e.Value.ToString(), out iValor))
                    {
                        e.DisplayText = string.Format("{0:00}/{1:0000}", iValor % 100, iValor / 100);
                    }
                }
        }
        
        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            //if Chegada da conta
            if (e.Column == colAGBDataInicio)
            {
                dAcompanhamento.DebitoRow row = (dAcompanhamento.DebitoRow)bandedGridView1.GetDataRow(e.RowHandle);
                if ((row != null) && (!row.IsAGBDataInicioNull()))
                {
                    if (row.AGBDataInicio < DateTime.Today)
                    {
                        e.Appearance.BackColor = Color.Red;
                    }
                    else
                        if (row.AGBDataInicio < DateTime.Today.AddDays(1))
                        {
                            e.Appearance.BackColor = Color.Yellow;
                        }
                    e.Appearance.ForeColor = Color.Black;
                }
            };

            if (e.Column == colDataConta)
            {
                dAcompanhamento.DebitoRow row = (dAcompanhamento.DebitoRow)bandedGridView1.GetDataRow(e.RowHandle);
                if (row != null)
                {                    
                    if ((TipoPagPeriodico)row.PGFForma == TipoPagPeriodico.DebitoAutomaticoConta)
                    {
                        if (row.DataConta < DateTime.Now.AddDays(-3))
                        {
                            e.Appearance.BackColor = Color.Red;
                        }
                        else
                            if (row.DataConta < DateTime.Today)
                            {
                                e.Appearance.BackColor = Color.Yellow;
                            }
                    }
                    else
                    {

                        if (row.DataConta < DateTime.Today.AddDays(0))
                        {
                            e.Appearance.BackColor = Color.Red;
                        }
                        else
                            if (row.DataConta < DateTime.Now.AddDays(3))
                            {
                                e.Appearance.BackColor = Color.Yellow;
                            }

                    }
                    e.Appearance.ForeColor = Color.Black;
                }
            };

            
        }

        

        

        private cContaAgLuz cContaAgLuz1;

        private cContaAgLuz CContaAgLuz
        {
            get { return cContaAgLuz1 ?? (cContaAgLuz1 = new cContaAgLuz()); }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            CContaAgLuz.Limpa();
            CContaAgLuz.ZeraTipo();
            CContaAgLuz.ShowEmPopUp();
            Carregar();
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dAcompanhamento.DebitoRow PGFrow = (dAcompanhamento.DebitoRow)bandedGridView1.GetFocusedDataRow();
            if (PGFrow != null)
            {
                //if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
                CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(ContaPagar.Follow.cPagamentosPeriodicosCampos), "Cadastro", PGFrow.PGF, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                    /*
                else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                    try
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM PaGamentosFixos WHERE (PGF = @P1)", PGFrow.PGF);
                    }
                    catch { }*/
            }
        }

        
    }
}
