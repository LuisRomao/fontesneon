﻿namespace ContaPagar.ContasAgLuz {


    partial class dContaAgLuz
    {
        

        private dContaAgLuzTableAdapters.PaGamentosFixosTableAdapter paGamentosFixosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PaGamentosFixos
        /// </summary>
        public dContaAgLuzTableAdapters.PaGamentosFixosTableAdapter PaGamentosFixosTableAdapter
        {
            get
            {
                if (paGamentosFixosTableAdapter == null)
                {
                    paGamentosFixosTableAdapter = new dContaAgLuzTableAdapters.PaGamentosFixosTableAdapter();
                    paGamentosFixosTableAdapter.TrocarStringDeConexao();
                };
                return paGamentosFixosTableAdapter;
            }
        }
    }
}
