﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ContaPagar
{
    /// <summary>
    /// Destrava nota
    /// </summary>
    public partial class cDestrava : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor padrão
        /// </summary>
        public cDestrava()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Numero"></param>
        /// <param name="Data"></param>
        public cDestrava(int? Numero,DateTime Data):this()
        {
            date_DataEmissao.DateTime = Data;
            if (Numero.HasValue)
            {
                spin_Numero.Value = Numero.Value;
                spin_Numero.ReadOnly = true;
                date_DataEmissao.ReadOnly = true;
            }            
        }

        /// <summary>
        /// Verifica se pode fechar
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            if (memo_Justificativa.Text == "")
            {
                memo_Justificativa.ErrorText = "Campo obrigatório";
                return false;
            }
            else
                return base.CanClose();            
        }

        private void memo_Justificativa_Validating(object sender, CancelEventArgs e)
        {
            if (memo_Justificativa.Text == "")
            {
                memo_Justificativa.ErrorText = "Campo obrigatório";
                e.Cancel = true;
            }
        }


    }
}
