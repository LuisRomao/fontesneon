namespace ContaPagar
{
    partial class cNovoSPL
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cNovoSPL));
            this.Plano = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.Codigo = new DevExpress.XtraEditors.TextEdit();
            this.Descricao = new DevExpress.XtraEditors.TextEdit();
            this.checkNoLocal = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Codigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descricao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkNoLocal.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
        
            // 
            // Plano
            // 
            this.Plano.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Plano.Appearance.Options.UseFont = true;
            this.Plano.Location = new System.Drawing.Point(14, 57);
            this.Plano.Name = "Plano";
            this.Plano.Size = new System.Drawing.Size(46, 19);
            this.Plano.TabIndex = 0;
            this.Plano.Text = "Plano";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(14, 96);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(62, 19);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "C�digo:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(14, 131);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(84, 19);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Descri��o:";
            // 
            // Codigo
            // 
            this.Codigo.Location = new System.Drawing.Point(110, 90);
            this.Codigo.Name = "Codigo";
            this.Codigo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Codigo.Properties.Appearance.Options.UseFont = true;
            
            this.Codigo.Size = new System.Drawing.Size(132, 29);
            
            this.Codigo.TabIndex = 3;
            // 
            // Descricao
            // 
            this.Descricao.Location = new System.Drawing.Point(110, 125);
            this.Descricao.Name = "Descricao";
            this.Descricao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Descricao.Properties.Appearance.Options.UseFont = true;
            
            this.Descricao.Size = new System.Drawing.Size(723, 29);
            
            this.Descricao.TabIndex = 4;
            // 
            // checkNoLocal
            // 
            this.checkNoLocal.EditValue = true;
            this.checkNoLocal.Location = new System.Drawing.Point(12, 179);
            this.checkNoLocal.Name = "checkNoLocal";
            this.checkNoLocal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkNoLocal.Properties.Appearance.Options.UseFont = true;
            this.checkNoLocal.Properties.Caption = "Prestado no condom�nio";
            this.checkNoLocal.Size = new System.Drawing.Size(230, 24);
            this.checkNoLocal.TabIndex = 5;
            // 
            // cNovoSPL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.checkNoLocal);
            this.Controls.Add(this.Descricao);
            this.Controls.Add(this.Codigo);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.Plano);
           
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cNovoSPL";
            this.Size = new System.Drawing.Size(863, 233);
            this.Controls.SetChildIndex(this.Plano, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.Codigo, 0);
            this.Controls.SetChildIndex(this.Descricao, 0);
            this.Controls.SetChildIndex(this.checkNoLocal, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Codigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descricao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkNoLocal.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.LabelControl Plano;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.LabelControl labelControl2;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.LabelControl labelControl3;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit Codigo;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit Descricao;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CheckEdit checkNoLocal;

    }
}
