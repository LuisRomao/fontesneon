﻿using System;
using System.Windows.Forms;

namespace ContaPagar
{
    /// <summary>
    /// 
    /// </summary>
    public partial class avisoINSS : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public avisoINSS()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Retificar"></param>
        /// <returns></returns>
        public static bool Avisar(bool Retificar)
        {
            avisoINSS aviso = new avisoINSS();
            if (Retificar)
                aviso.memoEdit1.Text = "Este tributo deveria estar em uma declaração da SEFIP que deverá ser retificada";
            return (aviso.VirShowModulo( CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.OK);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.Cancel);
        }
    }
}
