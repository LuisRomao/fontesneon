using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Cadastros.Sindicos;
using dllCheques;
using dllImpostoNeon;
using dllImpostos;
using Framework;
using Framework.objetosNeon;


namespace ContaPagar
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cNotasCampos : Framework.ComponentesNeon.CamposCondominio
    {        
        /// <summary>
        ///indica que a nota foi cadastrada no banco imediatamene antes da abertura e deve ser apagada no caso de cancelamento 
        /// </summary>
        public bool ApagarSeCancelar = false;
        
        private bool SemafaroFOR = true;
        private bool BlocCNPJFormat = true;
        private bool SemafaroDefaultPAGTipo;
        
        /// <summary>
        /// InibirVerificaDuplicidadeDeNota
        /// </summary>
        public bool InibirVerificaDuplicidadeDeNota;

        private int NOAAbrir;

        //private bool JaVerificouContratos = false;


        //private System.Collections.ArrayList ContratosTestados;
      
        private bool VerificaDuplicidadeDeNota()
        {
            
            if (LinhaMae.IsNOA_FRNNull())
                return false;
            string comandoBusca =
"SELECT     NOtAs.NOADATAI, CONDOMINIOS.CONCodigo,NOA\r\n" +
"FROM         NOtAs INNER JOIN\r\n" +
"                      CONDOMINIOS ON NOtAs.NOA_CON = CONDOMINIOS.CON\r\n" +
"WHERE     (NOtAs.NOANumero = @P1) AND (NOtAs.NOA_FRN = @P2) AND (NOtAs.NOA <> @P3);";

            if ((LinhaMae.NOANumero > 2) && (!InibirVerificaDuplicidadeDeNota))
            {
                DataRow Ret = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(comandoBusca, LinhaMae.NOANumero, LinhaMae.NOA_FRN, LinhaMae.NOA);
                if (Ret != null)
                {
                    DateTime DataInc = (DateTime)Ret["NOADATAI"];
                    if (MessageBox.Show(String.Format("Nota j� cadastrada em :{0:dd/MM/yyyy hh:mm}\r\n\r\nNota: {1}\r\n\r\nCondom�nio: {2}\r\n\r\nAbrir Nota?", DataInc, nOANumeroSpinEdit.EditValue, (string)Ret["CONCodigo"]), "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        NOAAbrir = (int)Ret["NOA"];
                        timer1.Enabled = true;
                    }
                    return true;
                };
            }

            string ComandoBusca25 =
"SELECT     NOtAs.NOADATAI,NOA\r\n" +
"FROM         NOtAs\r\n" +
"WHERE     \r\n" +
"(NOtAs.NOA_FRN = @P1) \r\n" +
"AND \r\n" +
"(NOtAs.NOA <> @P2) \r\n" +
"AND \r\n" +
"(NOtAs.NOA_CON = @P3) \r\n" +
"AND \r\n" +
"(NOATotal = @P4) \r\n" +
"AND \r\n" +
"(NOtAs.NOACompet = @P5);";
            if (!LinhaMae.IsNOA_FRNNull())
            {
                object[] Parametros = new object[5];
                Parametros[0] = LinhaMae.NOA_FRN;
                Parametros[1] = LinhaMae.NOA;
                Parametros[2] = LinhaMae.NOA_CON;
                Parametros[3] = LinhaMae.NOATotal;
                Parametros[4] = LinhaMae.NOACompet;
                DataRow Ret = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoBusca25, Parametros);
                if (Ret != null)
                {
                    DateTime DataInc = (DateTime)Ret["NOADATAI"];
                    if (MessageBox.Show(string.Format("ATEN��O\r\nJ� existe uma nota desta compet�ncia cadastrada neste valor em {0}\r\n\r\nAbrir Nota?", DataInc), "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        NOAAbrir = (int)Ret["NOA"];
                        timer1.Enabled = true;
                        //CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludo(typeof(cNotasCampos), "VERIFICAR", CompontesBasicos.TipoDeCarga.pk, (int)Ret["NOA"], false);
                    };
                    return true;
                };
            }
            return false;
        }

        private ContaPagar.dNOtAs.NOtAsRow LinhaMae {
            get 
            {
                return (LinhaMae_F == null) ? null : (ContaPagar.dNOtAs.NOtAsRow)LinhaMae_F;
            }
        }

        private Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow _rowCON;

        private Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON {
            get 
            {
                if ((_rowCON == null) || (_rowCON.RowState != DataRowState.Unchanged))
                //if (_rowCON == null)
                    _rowCON = Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(LinhaMae.NOA_CON);
                return _rowCON;
            }
        }

        private Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresRow _rowFRN;

        private Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresRow rowFRN
        {
            get 
            {
                if (LinhaMae.IsNOA_FRNNull())
                {
                    _rowFRN = null;                 
                }
                else
                {
                    if ((_rowFRN == null) || (_rowFRN.RowState != DataRowState.Unchanged) || (_rowFRN.FRN != LinhaMae.NOA_FRN))
                        _rowFRN = Fornecedores.FindByFRN(LinhaMae.NOA_FRN);
                }
                return _rowFRN;
            }
        }

        private bool RetNoVencimento;

        private void AjustaDataLimite() 
        {
            if ((ComboPAGTipoDefault.EditValue == null) || (ComboPAGTipoDefault.EditValue == DBNull.Value))
                return;
            AjustaDataLimite(
                (dCheque.PAGTipo)ComboPAGTipoDefault.EditValue,
                rowCON.IsCONProcuracaoNull() ? false : rowCON.CONProcuracao,
                (malote.TiposMalote)rowCON.CONMalote,
                DateTime.Now);
        }

        private void AjustaDataLimite(dCheque.PAGTipo PAGTIPO, bool Procuracao,malote.TiposMalote TipoMalote,DateTime HoraCadastro)
        {
            RetNoVencimento = false;

            if (PAGTIPO == dCheque.PAGTipo.sindico)
            {
                
                
                    malote malotelimite = malote.MalotePara(TipoMalote, malote.SentidoMalote.ida, HoraCadastro, 0);
                    DataLimite.DateTime = malotelimite.DataMalote.AddDays(1);                                    
            }
            else
            {
                if (Procuracao)
                {
                    if(HoraCadastro.Hour < 13)
                        DataLimite.DateTime = HoraCadastro.Date;
                    else
                        DataLimite.DateTime = HoraCadastro.Date.AddDays(1);
                    if (DataLimite.DateTime.DayOfWeek == DayOfWeek.Monday)
                        DataLimite.DateTime = DataLimite.DateTime.AddDays(-2);
                    else if (DataLimite.DateTime.DayOfWeek == DayOfWeek.Sunday)
                        DataLimite.DateTime = DataLimite.DateTime.AddDays(-1);
                }
                else
                {
                    malote malotelimite = malote.MalotePara(TipoMalote, malote.SentidoMalote.ida, HoraCadastro, 1);


                    if (malotelimite.DataRetorno.Hour > 12)
                    {
                        DataLimite.DateTime = malotelimite.DataRetorno.Date.AddDays(1);
                        RetNoVencimento = true;
                    }
                    else
                        DataLimite.DateTime = malotelimite.DataRetorno.Date;
                    if (DataLimite.DateTime.DayOfWeek == DayOfWeek.Monday)
                        DataLimite.DateTime = DataLimite.DateTime.AddDays(-2);
                };
            };
            Antec.Visible = RetNoVencimento;
        }

        private void cNotasCampos_Load(object sender, EventArgs e)
        {           
            try
            {                
                if (rowCON == null)
                    throw new Exception("Condom�nio n�o encontrado");
                if (rowCON.IsCONMaloteNull() || rowCON.IsCONProcuracaoNull())
                {
                    MessageBox.Show("Cadastro do condom�nio incompleto!\r\nVerifique o dia do malote e a procura��o");
                    FechaTela(DialogResult.Abort);
                    return;
                };               
                ComboFavorecido.Items.Add(rowCON.CONNome);            
                dSindicos.SindicosRow rowSindico = dSindicos.dSindicosSt.Sindicos.FindByCON(rowCON.CON);             
                if ((rowSindico != null) && (!rowSindico.IsPESNomeNull()))
                    ComboFavorecido.Items.Add(rowSindico.PESNome);             
                comboMalotes1.EditValue = rowCON.CONMalote;              
                ChProc.Checked = rowCON.CONProcuracao;              
                SemafaroFOR = BlocCNPJFormat = false;            
                if (LinhaMae != null)
                {
                    if (!LinhaMae.IsNOA_FRNNull())
                        AjustaCNPJ(LinhaMae.NOA_FRN);
                    if (LinhaMae.RowState == DataRowState.Detached)
                    {
                        LinhaMae.NOAStatus = (int)dNOtAs.NOAStatus.Parcial;
                        LinhaMae.NOATipo = (int)ContaPagar.dNOtAs.NOATipo.Avulsa;
                        LinhaMae.NOAAguardaNota = false;
                        LinhaMae.NOADefaultPAGTipo = (int)dCheque.PAGTipo.cheque;
                        LinhaMae.NOANumero = 0;
                        LinhaMae.NOAProblema = false;
                    }
                    if (!LinhaMae.IsNOA_PGFNull())
                    {
                        cBotaoIntegrador1.Visible = true;
                        cBotaoIntegrador1.Chave = LinhaMae.NOA_PGF;
                    }
                };                
                AjustaDataLimite();               
                SemafaroDefaultPAGTipo = true;            
                dateEdit1.DateTime = DateTime.Today.AddMonths(1);             
                decimal Liquido = 0;
                decimal bruto = 0;            
                foreach (dNOtAs.PAGamentosRow rowPAG in LinhaMae.GetPAGamentosRows())
                {
                    bruto += rowPAG.PAGValor;
                    if (!rowPAG.IsPAGIMPNull())
                        switch ((TipoImposto)rowPAG.PAGIMP)
                        {
                            case TipoImposto.IR:
                                ImpIR = new ImpostoNeon(TipoImposto.IR);
                                ImpIR.DataNota = LinhaMae.NOADataEmissao;
                                ImpIR.DataPagamento = MenorData;
                                ImpIR.ValorBase = LinhaMae.NOATotal;
                                ImpIRrow = rowPAG;
                                break;
                            case TipoImposto.PIS_COFINS_CSLL:
                                ImpContr = new ImpostoNeon(TipoImposto.PIS_COFINS_CSLL);
                                ImpContr.DataNota = LinhaMae.NOADataEmissao;
                                ImpContr.DataPagamento = MenorData;
                                ImpContr.ValorBase = LinhaMae.NOATotal;
                                ImpContrrow = rowPAG;
                                break;
                            case TipoImposto.ISS_SA:
                            case TipoImposto.ISSQN:

                                ImpISS = new ImpostoNeon(Tipocidade);
                                ImpISS.DataNota = LinhaMae.NOADataEmissao;
                                ImpISS.DataPagamento = MenorData;
                                ImpISS.ValorBase = LinhaMae.NOATotal;
                                ImpISSrow = rowPAG;
                                break;
                            case TipoImposto.INSS:
                                ImpINSS = new ImpostoNeon(TipoImposto.INSS);
                                ImpINSS.DataNota = LinhaMae.NOADataEmissao;
                                ImpINSS.DataPagamento = MenorData;
                                ImpINSS.ValorBase = LinhaMae.NOATotal;
                                ImpINSSrow = rowPAG;
                                break;
                            case TipoImposto.INSSpfEmp:
                                ImpINSSTomador = new ImpostoNeon(TipoImposto.INSSpfEmp);
                                ImpINSSTomador.DataNota = LinhaMae.NOADataEmissao;
                                ImpINSSTomador.DataPagamento = MenorData;
                                ImpINSSTomador.ValorBase = LinhaMae.NOATotal;
                                ImpINSSTomadorrow = rowPAG;
                                break;
                            case TipoImposto.INSSpfRet:
                                ImpINSSPrestador = new ImpostoNeon(TipoImposto.INSSpfRet);
                                ImpINSSPrestador.DataNota = LinhaMae.NOADataEmissao;
                                ImpINSSPrestador.DataPagamento = MenorData;
                                ImpINSSPrestador.ValorBase = LinhaMae.NOATotal;
                                ImpINSSPrestadorrow = rowPAG;
                                break;
                        }
                    else
                        Liquido += rowPAG.PAGValor;
                };             
                nOANumeroSpinEdit.Focus();
                chPagao.Checked = false;            
                if (LinhaMae["NOATotal"] != DBNull.Value)
                    if ((LinhaMae.NOATotal == Liquido) && (Liquido > 0) && (Liquido != bruto))
                        chPagao.Checked = true;           
                ajustaBotoesPFPJ();               
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Erro encontrado (1)."), ex);
            }
        }

        private Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresDataTable Fornecedores;

        private dNOtAs dNOtAsLocal;

        /// <summary>
        /// Carrega a linha pk
        /// </summary>
        protected override void FillAutomatico()
        {
            if (dNOtAsLocal.NOtAsTableAdapter.FillByNOA(dNOtAsLocal.NOtAs, pk) == 1)
            {
                LinhaMae_F = dNOtAsLocal.NOtAs[0];
                dNOtAsLocal.PAGamentosTableAdapter.FillByNOA(dNOtAsLocal.PAGamentos, pk);                
            }
            else
                dNOtAsLocal.PAGamentos.Clear();
            //dNOtAsLocal.PaGamentosFixosTableAdapter.Fill(dNOtAsLocal.PaGamentosFixos, rowCON.CON);
            Ativado = true;
            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cNotasCampos()
        {
            InitializeComponent();
            dNOtAsLocal = new dNOtAs();
            if (!DesignMode)
            {
                dNOtAsBindingSource.DataSource = dNOtAsLocal;// dNOtAsLocal;
                TableAdapterPrincipal = dNOtAsLocal.NOtAsTableAdapter;
                fornecedoresBindingSource.DataSource = Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD;
                //pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas;                
                //pLAnocontasBindingSource.Filter = "PLA like '2%'";
                pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas;
                SubPLanoPLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25;
                Fornecedores = Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fornecedores;
                uSUariosBindingSource.DataSource = Framework.datasets.dUSUarios.dUSUariosSt;
                cCompet1.CompMaxima = new Competencia(DateTime.Today.AddMonths(3));
            }
#if (DEBUG)
            bAutoTeste.Visible = bAutoTeste1.Visible = true;
#endif
        }


        /// <summary>
        /// Update
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            
            if (!CamposObrigatoriosOk(dNOtAsBindingSource))
            {
                MessageBox.Show("Campo obrigat�rio");
                foreach (DevExpress.XtraEditors.BaseEdit c in Controlesobrigatorios)
                    if (c.ErrorText != "")
                    {
                        c.Focus();
                        break;
                    }
                return false;
            };
            dNOtAsBindingSource.EndEdit();
            if (!Validate())
                return false;
            //Cancelando = false;
            if (VerificaDuplicidadeDeNota())
                if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.VerificaFuncionalidade("DUPLICIDADE") <= 0)
                    return false;
            //if (VerificaContratos())
            //    return false;
            gridView1.CloseEditor();
            
            pAGamentosBindingSource.EndEdit();
            
            decimal Total = 0;
            bool Quitada = true;
            int Parcela = 1;
            dNOtAs.PAGamentosRow[] PAGs = LinhaMae.GetPAGamentosRows();
            int NParcelas = 0;

            System.Collections.SortedList ParaNumerar = new System.Collections.SortedList();
            int segundodiferencial = 1;
            foreach (dNOtAs.PAGamentosRow rowPAG in PAGs){
                ParaNumerar.Add(rowPAG.PAGVencimento.AddSeconds(segundodiferencial++), rowPAG);
                bool Cancelado = ((!rowPAG.IsPAG_CHENull()) && ((Enumeracoes.CHEStatus)rowPAG.CHEStatus == Enumeracoes.CHEStatus.Cancelado));
                if (rowPAG.IsPAGIMPNull() && (!Cancelado))                                    
                    NParcelas++;
            };
            
            
            foreach (dNOtAs.PAGamentosRow rowPAG in ParaNumerar.Values)
            {
                bool Cancelado = ((!rowPAG.IsPAG_CHENull()) && ((Enumeracoes.CHEStatus)rowPAG.CHEStatus == Enumeracoes.CHEStatus.Cancelado));
                if (Cancelado)
                    continue;
                if (rowPAG.PAGValor < 0) 
                {
                    MessageBox.Show("Parcela com valor negativo!!");
                    return false;
                };
                if (rowPAG.IsPAGDOCNull())
                    Quitada = false;
                bool somaNoTotaldaNota = true;
                if (!rowPAG.IsPAGIMPNull() && chPagao.Checked)
                    somaNoTotaldaNota = false;
                if (!rowPAG.IsPAGIMPNull() && ((TipoImposto)rowPAG.PAGIMP == TipoImposto.INSSpfEmp))
                    somaNoTotaldaNota = false;
                if (somaNoTotaldaNota)
                     Total += rowPAG.PAGValor;
                if (NParcelas > 1)
                {
                    if ((rowPAG.IsPAGIMPNull()) && !rowPAG.IsPAG_CHENull() && Cheque.Editavel((Enumeracoes.CHEStatus)rowPAG.CHEStatus))
                    {
                        string PAGN = String.Format("{0}/{1}", Parcela++, NParcelas);
                        if (rowPAG.IsPAGNNull() && rowPAG.IsCHENumeroNull())
                            rowPAG.PAGN = PAGN;
                    };
                }
            }
            if (LinhaMae.NOATotal != Total)
            {
                LinhaMae.NOAStatus = (int)dNOtAs.NOAStatus.Parcial;//parcial
                MessageBox.Show(String.Format("Os pagamentos n�o conferem com o total da nota!!\r\nNota:{0:n2}\r\nPagamentos:{1:n2}", LinhaMae.NOATotal, Total));
            }
            else {
                LinhaMae.NOAStatus = (int)(Quitada ? dNOtAs.NOAStatus.Liquidada : dNOtAs.NOAStatus.Cadastrada);
            }            
            if (base.Update_F()) {
                try
                {
                    SemafaroDefaultPAGTipo = false;
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cNotasCampos - 441", dNOtAsLocal.PAGamentosTableAdapter);
                    System.Collections.ArrayList Linhas = new System.Collections.ArrayList();
                    //dNOtAs.PAGamentosRow[] rowPags = LinhaMae.GetPAGamentosRows();
                    foreach (dNOtAs.PAGamentosRow rowPag1 in dNOtAsLocal.PAGamentos)
                        Linhas.Add(rowPag1);
                    //Array.ForEach(rowPags,delegate(dNOtAs.PAGamentosRow rowPag)
                    foreach(dNOtAs.PAGamentosRow rowPag in Linhas)
                    {
                        switch (rowPag.RowState)
                        {
                            case DataRowState.Added:
                                ColocaParcelaNocheque(rowPag);                                                            
                                break;
                            case DataRowState.Deleted:
                                RemoveParcelaDoCheque(rowPag);                                
                                break;
                            case DataRowState.Modified:
                                RemoveParcelaDoCheque(rowPag);
                                ColocaParcelaNocheque(rowPag);
                                break;

                        };
                         
                    };
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                    MessageBox.Show(String.Format("Erro ao gravar:\r\n{0}\r\nA Grava��o das parcelas N�O foi feita!!!", e.Message));
                    throw new Exception("Erro ao gravar",e);                    
                };
                //MessageBox.Show(TesteGeraErro.Substring(10));
                if (GradeChamadora != null)
                {
                    NotasGrade Chamador = (NotasGrade)GradeChamadora;

                    Chamador.RefreshDados(LinhaMae.NOA_CON);
                }
                return true;
            }
            else
                return false;

        }

        //public string TesteGeraErro = "";

        private void RemoveParcelaDoCheque(dNOtAs.PAGamentosRow rowPag) {
            if ((dCheque.PAGTipo)rowPag["PAGTipo", DataRowVersion.Original] == dCheque.PAGTipo.Acumular)
                return;
            Cheque Cheque;
            if (rowPag.RowState == DataRowState.Deleted)
            {
                Cheque = new Cheque((int)rowPag["PAG_CHE", DataRowVersion.Original],Cheque.TipoChave.CHE);
                dNOtAsLocal.PAGamentosTableAdapter.Update(rowPag);
            }
            else
            {
                Cheque = new Cheque(rowPag.PAG_CHE, Cheque.TipoChave.CHE);
                rowPag.SetPAG_CHENull();
                dNOtAsLocal.PAGamentosTableAdapter.Update(rowPag);
                rowPag.AcceptChanges();
            };
            
            
            Cheque.AjustarTotalPelasParcelas();                                                      
        }

        private void ColocaParcelaNocheque(dNOtAs.PAGamentosRow rowPag)
        {
            if ((dCheque.PAGTipo)rowPag.PAGTipo == dCheque.PAGTipo.Acumular)
                return;
            /*
             imposto
            if (!rowPag.IsPAGIMPNull())
                Nominal = "Secretaria da Receita Federal";
            else
                Nominal = Fornecedores.FindByFRN(LinhaMae.NOA_FRN).FRNNome;
             * 
             */
            if (rowPag.IsPAGIMPNull())
            {
                if ((rowPag.IsCHEFavorecidoNull()) || (rowPag.CHEFavorecido == ""))
                    if (!LinhaMae.IsNOA_FRNNull())
                        rowPag.CHEFavorecido = Fornecedores.FindByFRN(LinhaMae.NOA_FRN).FRNNome;
                    else
                        rowPag.CHEFavorecido = "";
            }
            Cheque Cheque = new Cheque(rowPag.PAGVencimento, rowPag.CHEFavorecido, rowCON.CON,(dCheque.PAGTipo)rowPag.PAGTipo,!rowPag.PermiteAgrupar,0);
            rowPag.PAG_CHE = Cheque.CHErow.CHE;
            
            dNOtAsLocal.PAGamentosTableAdapter.Update(rowPag);
            rowPag.AcceptChanges();
            Cheque.AjustarTotalPelasParcelas();
            /*
            dNOtAs.CHEquesRow Cheque;
            string Nominal;
            
            Cheque = dNOtAsLocal.BuscaCheque(Nominal, rowPag.PAGVencimento, LinhaMae.NOA_CON);
            if (Cheque.RowState == DataRowState.Detached)
            {
                Cheque.CHE_BCO = rowCON.CON_BCO;
                Cheque.CHEI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                Cheque.CHEDATAI = DateTime.Now;
                Cheque.CHEValor = 0;
                if (rowPag.PAGTipo == (int)dllCheques.dCheque.PAGTipo.sindico)
                { 
                    Framework.objetosNeon.malote malote = Framework.objetosNeon.malote.MalotePara((Framework.objetosNeon.malote.TiposMalote)rowCON.CONMalote, Framework.objetosNeon.malote.SentidoMalote.volta, Cheque.CHEVencimento.AddHours(13).AddMinutes(30), 0);
                    if (!rowCON.CONProcuracao)
                    {
                        Cheque.CHEIdaData = malote.DataEnvio;
                        malote++;
                        Cheque.CHERetornoData = malote.DataRetorno;
                    }
                    else
                        Cheque.CHERetornoData = Cheque.CHEVencimento;
                }
                else
                {
                    Framework.objetosNeon.malote malote = Framework.objetosNeon.malote.MalotePara((Framework.objetosNeon.malote.TiposMalote)rowCON.CONMalote, Framework.objetosNeon.malote.SentidoMalote.volta, Cheque.CHEVencimento.AddHours(10), 0);
                    if (!rowCON.CONProcuracao)
                    {
                        Cheque.CHERetornoData = malote.DataRetorno;
                        malote--;
                        Cheque.CHEIdaData = malote.DataEnvio;
                    }
                    else
                        Cheque.CHERetornoData = Cheque.CHEVencimento;
                };
                //Cheque.CHEIdaData = Framework.objetosNeon.malote.Data((Framework.objetosNeon.malote.TiposMalote)rowCON.CONMalote, Framework.objetosNeon.malote.SentidoMalote.ida, Cheque.CHEVencimento, -2);
                dNOtAsLocal.CHEques.AddCHEquesRow(Cheque);
            }
            else
            {
                Cheque.CHEDATAA = DateTime.Now;
                Cheque.CHEA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
            };
            dNOtAsLocal.CHEquesTableAdapter.Update(Cheque);
            Cheque.AcceptChanges();
             */ 
            //rowPag.PAG_CHE = Cheque.CHE;
            //Cheque.CHEValor += rowPag.PAGValor;
            //dNOtAsLocal.CHEquesTableAdapter.Update(Cheque);
            //Cheque.AcceptChanges();
            
        }
        
        private void CNPJFormat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (DocBacarios.CPFCNPJ.IsValid(CNPJFormat.Text))
                {
                    CNPJValido();
                }
                else
                {
                    MessageBox.Show("CNPJ inv�lido");
                }
            }
        }

        private DocBacarios.CPFCNPJ cCNPJ;

        private void ajustaBotoesPFPJ() 
        {
            NIT_IM.Visible = NIT_IMValor.Visible = BotINSS.Visible = BotIR.Visible = BotPis.Visible = BotISS.Visible = false;
            if ((cCNPJ == null) || (rowFRN == null))
                return;
            
            if (cCNPJ.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
            {
                BotISS.Visible = !rowFRN.IsFRNRegistroNull();
                BotIR.Visible = BotPis.Visible = BotINSS.Visible = true;
                if (rowFRN.IsFRNRegistroNull())
                {
                    NIT_IM.Visible = true;
                    NIT_IM.Text = "Inscri��o Municipal n�o cadastrada";
                    NIT_IM.ForeColor = Color.Red;
                }
                else
                {
                    NIT_IM.ForeColor = Color.Black;
                    NIT_IMValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
                    NIT_IM.Text = "I.M.";
                    NIT_IM.Visible = NIT_IMValor.Visible = true;
                    NIT_IMValor.EditValue = rowFRN.FRNRegistro;
                }
            }
            else
                if (cCNPJ.Tipo == DocBacarios.TipoCpfCnpj.CPF)
                {
                    BotISS.Visible = true;
                    BotINSS.Visible = !rowFRN.IsFRNRegistroNull();
                    BotIR.Visible = true;
                    if (rowFRN.IsFRNRegistroNull())
                    {
                        NIT_IM.Visible = true;
                        NIT_IM.Text = "NIT/PIS/PASEP n�o cadastrado";
                        NIT_IM.ForeColor = Color.Red;
                    }
                    else
                    {
                        NIT_IM.ForeColor = Color.Black;
                        NIT_IMValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                        NIT_IMValor.Properties.DisplayFormat.FormatString = @"000\.00000\.00-0";
                        NIT_IM.Text = "NIT:";
                        NIT_IM.Visible = NIT_IMValor.Visible = true;
                        NIT_IMValor.EditValue = rowFRN.FRNRegistro;
                    }
                   
                    
                    
                }
        }

        private void CNPJValido(){
                    cCNPJ = new DocBacarios.CPFCNPJ(CNPJFormat.Text);
                    bool BlocCNPJFormatAnt = BlocCNPJFormat;
                    BlocCNPJFormat = true;
                    CNPJFormat.Text = cCNPJ.ToString(true);
                    BlocCNPJFormat = BlocCNPJFormatAnt;
                    string CNPJAlvo = cCNPJ.ToString(true);
                    int FRN = -1;
                    foreach (Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresRow Candidato in Fornecedores)
                        if ((!Candidato.IsFRNCnpjNull()) && (Candidato.FRNCnpj == CNPJAlvo))
                        {
                            FRN = Candidato.FRN;
                            break;
                        }
                    //Verifica se o fornecedor nao foi cadastrado novamente neste meio tempo
                    if (FRN == -1)
                        if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select FRN from fornecedores where FRNCnpj = @P1 and FRNTipo = 'FSR'", out FRN, CNPJAlvo))
                            Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fill();
                    if (FRN > 0)
                    {
                        SemafaroFOR = true;
                        lookUpFOR1.EditValue = lookUpFOR2.EditValue = FRN;
                        LinhaMae.NOA_FRN = FRN;
                        SemafaroFOR = false;
                        nOADataEmissaoDateEdit.Focus();
                        //NotaJaCadastrada();
                        AjustaCNPJ(FRN);
                    }
                    else
                    {
                        if (IncluiFRN(CNPJAlvo))
                            nOADataEmissaoDateEdit.Focus();
                    };
                    
        }

        private void CNPJFormat_TextChanged(object sender, EventArgs e)
        {
            if (BlocCNPJFormat)
                return;
            try
            {
                BlocCNPJFormat = true;
                if ((CNPJFormat.Text.Length > 13) && (DocBacarios.CPFCNPJ.IsValid(CNPJFormat.Text)))
                {
                    CNPJValido();                                                                
                }
                else {
                    if (CNPJFormat.Text.Length == 14)
                        MessageBox.Show("CNPJ inv�lido");
                }
            }
            finally
            {
                BlocCNPJFormat = false;
            };
        }
       
        private void AjustaCNPJ(int FRN) {
                //rowFRN = null;
                BlocCNPJFormat = true;
                if (FRN == -1)
                    CNPJFormat.Text = "";
                else
                {
                    LinhaMae.NOA_FRN = FRN;
                    //rowFRN = Fornecedores.FindByFRN(FRN);
                    if (rowFRN == null)
                    {
                        Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fill();
                        try
                        {
                            Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = false;
                            Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Sigla = "ADV";
                            Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fill();
                        }
                        finally
                        {
                            Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Sigla = "FSR";
                            Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = true;
                        }
                        //rowFRN = Fornecedores.FindByFRN(FRN);
                        if (rowFRN == null)
                            throw new Exception(string.Format("Fornecedor n�o encontrado : {0}",FRN));
                    }
                    CNPJFormat.Text = (rowFRN.IsFRNCnpjNull()) ? "" : rowFRN.FRNCnpj;
                    cCNPJ = new DocBacarios.CPFCNPJ(CNPJFormat.Text);
                    ajustaBotoesPFPJ();
                };
                BlocCNPJFormat = false;
        }

        private void lookUpFOR2_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroFOR) {                
                int FRN = -1;
                SemafaroFOR = true;
                if (sender == lookUpFOR1)
                {
                    lookUpFOR2.EditValue = lookUpFOR1.EditValue;
                    FRN = (int)lookUpFOR1.EditValue;
                }
                if (sender == lookUpFOR2)
                {
                    lookUpFOR1.EditValue = lookUpFOR2.EditValue;
                    FRN = (int)lookUpFOR2.EditValue;
                }
                
                AjustaCNPJ(FRN);
                //NotaJaCadastrada();
                SemafaroFOR = false;
            }
        }

        bool SemafaroPLA;

        private Framework.datasets.dSTRs _dSTRs;

        private Framework.datasets.dSTRs dSTRs
        {
            get {
                if (_dSTRs == null) {
                    _dSTRs = new Framework.datasets.dSTRs();
                    _dSTRs.Fill("PLA");
                };
                return _dSTRs;
            }
        }

        private Framework.datasets.dPLAnocontas.PLAnocontasRow _PLArow;
        private string _PLA;

        private Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow
        {
            set { 
                _PLArow = value;
                _PLA = _PLArow.PLA;
            }
            get 
            {                
                if ((_PLArow != null) && (_PLArow.RowState == DataRowState.Detached))
                    _PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas.FindByPLA(_PLA);
                return _PLArow; 
            }
        }

        private void lookUpEditPLA2_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroPLA)
            {
                try
                {
                    SemafaroPLA = true;
                    if (sender == lookUpEditPLA1)
                        lookUpEditPLA2.EditValue = lookUpEditPLA1.EditValue;
                    if (sender == lookUpEditPLA2)
                        lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue;
                    if ((lookUpEditPLA2.EditValue == null) || (lookUpEditPLA2.EditValue == DBNull.Value))
                    {
                        lookUpEdit1.Enabled = false;
                        _PLArow = null;
                    }
                    else
                    {
                        string PLA = lookUpEditPLA2.EditValue.ToString();
                        PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas.FindByPLA(PLA);                        
                        SubPLanoPLAnocontasBindingSource.Filter = string.Format("SPL_PLA = {0}", PLA);
                        lookUpEdit1.Enabled = true;
                    }                    
                    CarregaLista();
                }
                finally
                {
                    SemafaroPLA = false;
                }
            }
        }

        private void CarregaLista() {            
            comboBoxEdit1.Properties.Items.Clear();
            foreach (Framework.datasets.dSTRs.STRsRow rowSTR in dSTRs.STRs)
            {
                if (rowSTR.STRChave.ToString() == lookUpEditPLA1.EditValue.ToString())
                    comboBoxEdit1.Properties.Items.Add(rowSTR.STRTexto);
            };
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
                ContaPagar.dNOtAs.PAGamentosRow rowPAG = (ContaPagar.dNOtAs.PAGamentosRow)DRV.Row;
                rowPAG.PAGA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                rowPAG.PAGDATAA = DateTime.Now;
                if (rowPAG.IsPAGIMPNull())
                     RevisarDataDosImpostos();
            }
            else
                RevisarDataDosImpostos();
        }

        private void RevisarDataDosImpostos() {
            if ((ImpIR != null) && (ImpIR.DataPagamento != MenorData))
            {
                if ((Enumeracoes.CHEStatus)ImpIRrow.CHEStatus == Enumeracoes.CHEStatus.Cadastrado)
                {
                    ImpIR.DataPagamento = MenorData;
                    ImpIRrow.PAGVencimento = ImpIR.VencimentoEfetivo;
                }
            };
            if ((ImpContr != null) && (ImpContr.DataPagamento != MenorData))
            {
                if ((Enumeracoes.CHEStatus)ImpContrrow.CHEStatus == Enumeracoes.CHEStatus.Cadastrado)
                {
                    ImpContr.DataPagamento = MenorData;
                    ImpContrrow.PAGVencimento = ImpContr.VencimentoEfetivo;
                }
            }
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            ContaPagar.dNOtAs.PAGamentosRow rowPAG = (ContaPagar.dNOtAs.PAGamentosRow)DRV.Row;
            rowPAG.PAGTipo = (int)ComboPAGTipoDefault.EditValue;
            rowPAG.PAGI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
            rowPAG.PAGDATAI = DateTime.Now;
            rowPAG.CHEStatus = (int)Enumeracoes.CHEStatus.Cadastrado;
        }

        //private bool Cancelando;

        /// <summary>
        /// Cancela o cadastramento
        /// </summary>
        /// <param name="TesteCanClose"></param>
        /// <returns></returns>
        public override bool Cancela_F(bool TesteCanClose)
        {
            SemafaroDefaultPAGTipo = false;
            //Cancelando = true;
            if ((ApagarSeCancelar) && (MessageBox.Show("Confima o cancelamento ?","Confirma��o",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2) == DialogResult.Yes))
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cNotasCampos - 911",
                                                                           dNOtAsLocal.PAGamentosTableAdapter, 
                                                                           dNOtAsLocal.NOtAsTableAdapter);
                    dNOtAs.PAGamentosRow[] Pagamentos = LinhaMae.GetPAGamentosRows();
                    foreach (dNOtAs.PAGamentosRow PAGrow in Pagamentos)
                    {
                        RemoveParcelaDoCheque(PAGrow);
                        PAGrow.Delete();
                    }
                    dNOtAsLocal.PAGamentosTableAdapter.Update(dNOtAsLocal.PAGamentos);
                    LinhaMae.Delete();
                    dNOtAsLocal.NOtAsTableAdapter.Update(dNOtAsLocal.NOtAs);
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    FechaTela(DialogResult.Cancel);
                    return true;
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                    return false;
                }
            }
            else
                return base.Cancela_F(TesteCanClose);
        }

        private void comboBoxEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            int nPLA;
            if (int.TryParse(lookUpEditPLA1.EditValue.ToString(), out nPLA))
            {
                switch (e.Button.Kind)
                {
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Plus:
                        dSTRs.Add(nPLA, comboBoxEdit1.Text);
                        CarregaLista();
                        break;
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Minus:
                        dSTRs.Dell(nPLA, comboBoxEdit1.Text);
                        CarregaLista();
                        break;
                }
            };
        }

        private System.Collections.ArrayList StatusEditavel = new System.Collections.ArrayList(new Enumeracoes.CHEStatus[] { Enumeracoes.CHEStatus.Cadastrado, Enumeracoes.CHEStatus.DebitoAutomatico, Enumeracoes.CHEStatus.Eletronico });

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if ((nOATotalSpinEdit.EditValue == null) || (nOATotalSpinEdit.EditValue == DBNull.Value))
                return;
            decimal Valor = (decimal)nOATotalSpinEdit.EditValue;
            decimal Retido = 0;
            bool ParcelaRetencaoPaga = false;
            int Parcelas = 0;

            foreach (dNOtAs.PAGamentosRow rowPAG in LinhaMae.GetPAGamentosRows())
            {
                if (!rowPAG.IsPAG_CHENull() && (Framework.Enumeracoes.CHEStatus)rowPAG.CHEStatus == Enumeracoes.CHEStatus.Cancelado)
                    continue;
                if (!rowPAG.IsCHEStatusNull() && StatusEditavel.Contains((Enumeracoes.CHEStatus)rowPAG.CHEStatus) && (rowPAG.IsPAGIMPNull()))
                    rowPAG.Delete();
                else
                {
                    if (rowPAG.IsPAGIMPNull())
                    {
                        Parcelas++;
                        Valor -= (rowPAG.PAGValor + Retido);
                        Retido = 0;
                        ParcelaRetencaoPaga = true;
                    }
                    else
                    {
                        if (!chPagao.Checked && ((TipoImposto)rowPAG.PAGIMP != TipoImposto.INSSpfEmp))
                            Retido += rowPAG.PAGValor;
                    }
                }
            };
            DateTime data = dateEdit1.DateTime;
            if (Valor-Retido <= 0)
                return;
            decimal Ultima = Valor - Retido;
            
            for (int i = Parcelas+1; i <= spinEdit1.Value; i++, data = data.AddMonths(1))
            {
                dNOtAs.PAGamentosRow novaPAG = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                novaPAG.PAG_NOA = LinhaMae.NOA;
                novaPAG.CHEStatus = (int)Enumeracoes.CHEStatus.Cadastrado;
                novaPAG.PAGTipo = (int)ComboPAGTipoDefault.EditValue;
                if (i < spinEdit1.Value)
                {
                    novaPAG.PAGValor = decimal.Round(Valor / (spinEdit1.Value - Parcelas), 2);
                    
                    if (!ParcelaRetencaoPaga) 
                    {
                        ParcelaRetencaoPaga = true;
                        novaPAG.PAGValor -= Retido;
                    };
                    Ultima -= novaPAG.PAGValor;
                }
                else
                    novaPAG.PAGValor = Ultima;
                novaPAG.PAGVencimento = data;
                novaPAG.PAGN = String.Format("{0}/{1}", i, spinEdit1.Value);
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(novaPAG);
                if(Framework.DSCentral.EMP == 1)
                      dividiremvaloremenores(novaPAG, 4999);
            };
            RevisarDataDosImpostos();
           
        }

        private void dividiremvaloremenores(dNOtAs.PAGamentosRow Semente, decimal Teto) 
        {
            int parcelas = 1 + (int)Math.Truncate(Semente.PAGValor / Teto);
            if (parcelas > 1)
            {
                decimal Valordividido = Math.Round(Semente.PAGValor / parcelas, 2);
                decimal Saldo = Semente.PAGValor - Valordividido;
                Semente.PAGValor = Valordividido;
                Semente.PermiteAgrupar = false;
                for (int i = 2; i <= parcelas; i++) 
                {
                    dNOtAs.PAGamentosRow novaPAG = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                    novaPAG.PAG_NOA = Semente.PAG_NOA;
                    novaPAG.CHEStatus = Semente.CHEStatus;
                    novaPAG.PAGTipo = Semente.PAGTipo;
                    novaPAG.PAGValor = (i == parcelas) ? Saldo : Valordividido;
                    Saldo -= novaPAG.PAGValor;
                    novaPAG.PAGVencimento = Semente.PAGVencimento;
                    novaPAG.PAGN = Semente.PAGN;
                    novaPAG.PermiteAgrupar = false;
                    dNOtAsLocal.PAGamentos.AddPAGamentosRow(novaPAG);
                }
            }
        }

        private bool Deletavel(dNOtAs.PAGamentosRow rowDel)
        {
            if (rowDel.IsCHEStatusNull())
                return false;
            if (!StatusEditavel.Contains((Framework.Enumeracoes.CHEStatus)rowDel.CHEStatus))
                return false;            
            return true;
        }

        private void Botoes_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //DataRowView DRV = (DataRowView)gridView1.GetRow(gridView1.FocusedRowHandle);
            dNOtAs.PAGamentosRow rowDel = (dNOtAs.PAGamentosRow)gridView1.GetFocusedDataRow();
            if (rowDel == null)
                return;
            //dNOtAs.PAGamentosRow rowDel = (dNOtAs.PAGamentosRow)DRV.Row;
            if (!Deletavel(rowDel))
                return;


            if ((ImpIRrow != null) && (ImpIRrow == rowDel))
            {
                ImpIRrow = null;
                ImpIR = null;
            };
            if ((ImpContrrow != null) && (ImpContrrow == rowDel))
            {
                ImpContrrow = null;
                ImpContr = null;
            };
            if ((ImpINSSrow != null) && (ImpINSSrow == rowDel))
            {
                ImpINSSrow = null;
                ImpINSS = null;
            };
            if ((ImpISSrow != null) && (ImpISSrow == rowDel))
            {
                ImpISSrow = null;
                ImpISS = null;
            };
            if ((ImpINSSTomadorrow != null) && (ImpINSSTomadorrow == rowDel))
            {
                ImpINSSTomadorrow = null;
                ImpINSSTomador = null;
            };
            if ((ImpINSSPrestadorrow != null) && (ImpINSSPrestadorrow == rowDel))
            {
                ImpINSSPrestadorrow = null;
                ImpINSSPrestador = null;
            };
            rowDel.Delete();
            RevisarDataDosImpostos();

        }
        
        private bool IncluiFRN(string CNPJAlvo)
        {
            int FRN = -1;
            System.Collections.SortedList CamposNovos = new System.Collections.SortedList();
            CamposNovos.Add("FRNCnpj", CNPJAlvo);
            CamposNovos.Add("FRNTipo", "FSR");
            FRN = ShowModuloAdd(typeof(Cadastros.Fornecedores.cFornecedoresCampos), CamposNovos);
            if (FRN > 0)
            {
                try
                {
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = false;
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.FillByFRN(Fornecedores, FRN);
                    SemafaroFOR = true;
                    lookUpFOR1.EditValue = lookUpFOR2.EditValue = FRN;
                    LinhaMae.NOA_FRN = FRN;
                    SemafaroFOR = false;
                    AjustaCNPJ(FRN);
                    //NotaJaCadastrada();
                }
                finally
                {
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = true;
                };
                return true;
            }
            return false;
        }

        private void lookUpFOR1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
                IncluiFRN("");
        }

        /// <summary>
        /// Deveria estar no componente base
        /// Por falta de tempo foi feito manual
        /// </summary>
        private System.Collections.ArrayList _Controlesobrigatorios;

        /// <summary>
        /// Deveria estar no componente base
        /// Por falta de tempo foi feito manual
        /// </summary>
        private System.Collections.ArrayList Controlesobrigatorios {
            get {
                if (_Controlesobrigatorios == null) {
                    _Controlesobrigatorios = new System.Collections.ArrayList();
                    _Controlesobrigatorios.Add(nOANumeroSpinEdit);
                    _Controlesobrigatorios.Add(nOADataEmissaoDateEdit);
                    _Controlesobrigatorios.Add(nOATotalSpinEdit);
                    _Controlesobrigatorios.Add(lookUpFOR1);
                    _Controlesobrigatorios.Add(lookUpEditPLA1);
                    _Controlesobrigatorios.Add(comboBoxEdit1);
                };
                return _Controlesobrigatorios;
            }
        }

        /*
        private void ReterImpostos() {
            return;
            bool simples = false;
            if(rowFRN != null)
              if (!rowFRN.IsFRNSimplesNull())
                if (rowFRN.FRNSimples)
                    simples = true;
            if (!simples)
            {
                if ((rowFRN.FRNCnpj.Length > 14) && (LinhaMae.NOATotal > 5000M))
                {
                    if (ImpContr == null)
                    {
                        ImpContr = new ImpostoNeon(TipoImposto.PIS_COFINS_CSLL);
                        ImpContr.DataNota = LinhaMae.NOADataEmissao;
                        ImpContr.DataPagamento = MenorData;
                        ImpContr.ValorBase = LinhaMae.NOATotal;
                        ImpContrrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                        ImpContrrow.PAG_NOA = LinhaMae.NOA;
                        ImpContrrow.PAGTipo = (int)dCheque.PAGTipo.cheque;
                        ImpContrrow.PAGIMP = (int)TipoImposto.PIS_COFINS_CSLL;
                        ImpContrrow.PAGValor = 0;
                        ImpContrrow.PAGVencimento = DateTime.MinValue;
                        dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpContrrow);
                    };

                    ImpContr.DataNota = LinhaMae.NOADataEmissao;
                    ImpContr.DataPagamento = MenorData;
                    ImpContr.ValorBase = LinhaMae.NOATotal;
                    ImpContrrow.PAGValor = ImpContr.ValorImposto;
                    ImpContrrow.PAGVencimento = ImpContr.VencimentoEfetivo;

                }
                else
                {
                    if (ImpContr != null)
                    {
                        ImpContr = null;
                        ImpContrrow.Delete();
                        ImpContrrow = null;

                    };
                };
            };
        }
        */

        private bool blockEnter;

        private void groupControl5_Enter(object sender, EventArgs e)
        {
            if (!blockEnter)
            {
                try
                {
                    blockEnter = true;
                    if (CamposObrigatoriosOk(dNOtAsBindingSource))
                    {                        
                        dNOtAsBindingSource.EndEdit();
                        VerificaDuplicidadeDeNota();
                        //VerificaContratos();
                    }
                    else
                    {
                        MessageBox.Show("Campo obrigat�rio");
                        foreach (DevExpress.XtraEditors.BaseEdit c in Controlesobrigatorios)
                            if (c.ErrorText != "")
                            {
                                c.Focus();
                                break;
                            }

                    }
                }
                finally
                {
                    blockEnter = false;
                }
            }
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            dNOtAs.PAGamentosRow rowPAG = (dNOtAs.PAGamentosRow)((DataRowView)e.Row).Row;            
            if (!rowPAG.IsPAG_CHENull() && !StatusEditavel.Contains((Framework.Enumeracoes.CHEStatus)rowPAG.CHEStatus))
            {
                MessageBox.Show("Cheque j� emitido");
                e.Valid = false;
            }
            if (rowPAG["PAGValor"] != DBNull.Value)
            {
                if (rowPAG.PAGValor != Math.Round(rowPAG.PAGValor, 2))
                    rowPAG.PAGValor = Math.Round(rowPAG.PAGValor, 2);
            }
            else
                rowPAG.PAGValor = 0;
            
        }
        
        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return;
            dNOtAs.PAGamentosRow linha = ((dNOtAs.PAGamentosRow)DRV.Row);
            if (e.Column == colPAGVencimento)
            {
                if (linha["PAGVencimento"] != DBNull.Value) {
                    if (linha.PAGVencimento < DataLimite.DateTime)
                    {
                        if ((RetNoVencimento) && (linha.PAGVencimento == DataLimite.DateTime.AddDays(-1)))
                            e.Appearance.BackColor = Color.Yellow;
                        else
                        {
                            e.Appearance.BackColor = Color.Red;
                            e.Appearance.ForeColor = Color.White;
                        }
                    };
                }
            }
        }

        //private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        //{
        //    AjustaDataLimite();
        //}

        private void cNotasCampos_cargaInicial(object sender, EventArgs e)
        {
            dCheque.dChequeSt.VirEnumPAGTipo.CarregaEditorDaGrid(ComboPAGTipoDefault);
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(ComboTipo);
            //dCheque.VirEnumCHEStatus.CarregaEditorDaGrid(ComboTipo);
            dCheque.dChequeSt.VirEnumPAGTipo.CarregaEditorDaGrid(colPAGTipo);
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
            //dCheque.VirEnumCHEStatus.CarregaEditorDaGrid(colCHEStatus);
            ImpostoNeon.VirEnumTipoImposto.CarregaEditorDaGrid(colPAGIMP);
            //repositoryTipoPag.Items.Clear();
            //ComboPAGTipoDefault
            //foreach (System.Collections.DictionaryEntry DE in dllCheques.dCheque.dChequeSt.PAGTipotxt)
            //{
            //    dllCheques.dCheque.PAGTipo Tipo = (dCheque.PAGTipo)DE.Key;
                //ComboPAGTipoDefault.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(DE.Value.ToString(), DE.Key));
                //ComboPAGTipoDefault.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(DE.Value.ToString(), (object)((int)Tipo)));
            //    repositoryTipoPag.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(DE.Value.ToString(), (object)((int)Tipo)));
            //}
        }

        private void ComboPAGTipoDefault_EditValueChanged(object sender, EventArgs e)
        {
            if (SemafaroDefaultPAGTipo)
            {
                ComboPAGTipoDefault.DoValidate();
                AjustaDataLimite();
            }
            if ( ComboPAGTipoDefault.EditValue != null)
            {
                labelDadosDep.Visible = textDadosDep.Visible = ((dCheque.PAGTipo)ComboPAGTipoDefault.EditValue == dCheque.PAGTipo.deposito);               
            }
        }

        private List<DevExpress.XtraGrid.Columns.GridColumn> ColunasEditaveis;

        private void LinhaReadOnly(bool valor)
        {
            if (ColunasEditaveis == null)
            {
                ColunasEditaveis = new List<DevExpress.XtraGrid.Columns.GridColumn>();
                foreach (DevExpress.XtraGrid.Columns.GridColumn coluna in gridView1.Columns)
                    if (!coluna.OptionsColumn.ReadOnly)
                        if (coluna != colbotaoCheque)
                            ColunasEditaveis.Add(coluna);
            }
            foreach (DevExpress.XtraGrid.Columns.GridColumn coluna in ColunasEditaveis)
                coluna.OptionsColumn.ReadOnly = valor;                    
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.FocusedRowHandle);
            if ((DRV == null) || (DRV.Row == null))
            {
                gridView1.OptionsBehavior.Editable = true;
                return;
            }
            dNOtAs.PAGamentosRow linha = ((dNOtAs.PAGamentosRow)DRV.Row);
            bool LinhaEditavel = true;
            if (!linha.IsCHEStatusNull())
                LinhaEditavel = (StatusEditavel.Contains((Framework.Enumeracoes.CHEStatus)linha.CHEStatus));
            else
                LinhaEditavel = (((dCheque.PAGTipo)linha.PAGTipo == dCheque.PAGTipo.Caixinha)
                              || ((dCheque.PAGTipo)linha.PAGTipo == dCheque.PAGTipo.GuiaEletronica)
                              || ((dCheque.PAGTipo)linha.PAGTipo == dCheque.PAGTipo.Acumular));

            LinhaReadOnly(!LinhaEditavel);
            /*
            if (!linha.IsCHEStatusNull())
                gridView1.OptionsBehavior.Editable = (StatusEditavel.Contains((Framework.Enumeracoes.CHEStatus)linha.CHEStatus));
            else
                gridView1.OptionsBehavior.Editable = (((dCheque.PAGTipo)linha.PAGTipo == dCheque.PAGTipo.Caixinha) 
                                                   || ((dCheque.PAGTipo)linha.PAGTipo == dCheque.PAGTipo.GuiaEletronica)
                                                   || ((dCheque.PAGTipo)linha.PAGTipo == dCheque.PAGTipo.Acumular));
            */
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (ImpIRrow != null)                    
                  return;
            decimal IR = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(TipoImposto.IR) + 0.004999M, 2);
            if (VirInput.Input.Execute("IR Destacado", ref IR,2,true)) {
                IR = Math.Round(IR, 2);
                if (IR <= 0)
                    return;
                ImpIR = new dllImpostoNeon.ImpostoNeon(TipoImposto.IR);
                ImpIR.DataNota = LinhaMae.NOADataEmissao;
                ImpIR.DataPagamento = MenorData;
                ImpIR.ValorBase = LinhaMae.NOATotal;
                //if (IR != ImpIR.ValorImposto)
                //    MessageBox.Show("Valor do IR n�o confere\r\nEsperado: " + ImpIR.ValorImposto.ToString("n2") + "\r\nInformado:" + IR.ToString("n2"), "ATEN��O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ImpIRrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                ImpIRrow.PAG_NOA = LinhaMae.NOA;
                ImpIRrow.PAGTipo = (int)dCheque.PAGTipo.Guia;
                ImpIRrow.PAGIMP = (int)TipoImposto.IR;
                ImpIRrow.PAGValor = IR;
                ImpIRrow.PAGVencimento = ImpIR.VencimentoEfetivo;
                ImpIRrow.CHEStatus = (int)Enumeracoes.CHEStatus.Cadastrado;
                ImpIRrow.CHEFavorecido = ImpIR.Nominal();
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpIRrow);
            }
        }

        private ImpostoNeon ImpIR = null;
        private dNOtAs.PAGamentosRow ImpIRrow = null;
        private ImpostoNeon ImpContr = null;
        private dNOtAs.PAGamentosRow ImpContrrow = null;
        private ImpostoNeon ImpISS = null;
        private dNOtAs.PAGamentosRow ImpISSrow = null;
        private ImpostoNeon ImpINSS = null;
        private dNOtAs.PAGamentosRow ImpINSSrow = null;
        private ImpostoNeon ImpINSSTomador = null;
        private dNOtAs.PAGamentosRow ImpINSSTomadorrow = null;
        private ImpostoNeon ImpINSSPrestador = null;
        private dNOtAs.PAGamentosRow ImpINSSPrestadorrow = null;

        private DateTime MenorData{
            get {
                DateTime Retornar = DateTime.MaxValue;
                foreach (dNOtAs.PAGamentosRow rowPag in LinhaMae.GetPAGamentosRows())
                    if (rowPag.RowState != DataRowState.Deleted)
                        if (rowPag.IsPAGIMPNull())
                            if (rowPag.PAGVencimento < Retornar)
                                 Retornar = rowPag.PAGVencimento;
                if (Retornar == DateTime.MaxValue)
                    return LinhaMae.NOADataEmissao;
                else
                    return Retornar;
            }
        }

        private void nOATotalSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (nOATotalSpinEdit.EditValue != null)
            {
                nOATotalSpinEdit.EditValue = Math.Round((decimal)nOATotalSpinEdit.EditValue, 2);
                //NotaJaCadastrada();
            }
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
                if ((DRV != null) && (DRV.Row != null))
                {
                    dNOtAs.PAGamentosRow Linha = (dNOtAs.PAGamentosRow)DRV.Row;
                    if (Linha.IsCHEStatusNull())
                    {
                        //e.Appearance.BackColor = Color.AliceBlue;
                        e.Appearance.BackColor = Color.Pink;
                        e.Appearance.ForeColor = Color.Black;
                    }
                    else
                        if ((Enumeracoes.CHEStatus)Linha.CHEStatus == Enumeracoes.CHEStatus.Cancelado)
                        {
                            e.Appearance.BackColor = Color.Wheat;
                            e.Appearance.ForeColor = Color.Black;
                            e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Strikeout | FontStyle.Italic);
                        }
                        else
                            if ((Enumeracoes.CHEStatus)Linha.CHEStatus == Enumeracoes.CHEStatus.PagamentoCancelado)
                            {
                                e.Appearance.BackColor = Color.Silver;
                                e.Appearance.ForeColor = Color.Black;
                                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Strikeout | FontStyle.Italic);
                            }
                            else
                            {
                                if (!Linha.IsPAGIMPNull())
                                {
                                    e.Appearance.BackColor = Color.FromArgb(255, 255, 100);
                                    e.Appearance.ForeColor = Color.FromArgb(100, 100, 100);
                                }
                                else
                                {
                                    e.Appearance.BackColor = Color.FromArgb(100, 255, 255);
                                    e.Appearance.ForeColor = Color.Black;
                                }
                            }
                }
            }
        }

        private void comboBoxEdit1_Properties_Validating(object sender, CancelEventArgs e)
        {
            string Corrigido = "";
            bool primeiro = true;
            for (int i = 0; i < comboBoxEdit1.Text.Length; i++)
            {
                if (primeiro)
                    Corrigido += char.ToUpper(comboBoxEdit1.Text[i]);
                else
                    Corrigido += char.ToLower(comboBoxEdit1.Text[i]);
                if (comboBoxEdit1.Text[i] != ' ')
                    primeiro = false;
            }
            if (comboBoxEdit1.Text != Corrigido)
                comboBoxEdit1.Text = Corrigido;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (ImpContrrow != null)
                return;
            decimal ValCont = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(TipoImposto.PIS_COFINS_CSLL) + 0.004999M, 2);
            if (VirInput.Input.Execute("Val.Destacado", ref ValCont,2,true))
            {
                ValCont = Math.Round(ValCont, 2);
                if (ValCont <= 0)
                    return;
                ImpContr = new dllImpostoNeon.ImpostoNeon(TipoImposto.PIS_COFINS_CSLL);
                ImpContr.DataNota = LinhaMae.NOADataEmissao;
                ImpContr.DataPagamento = MenorData;
                ImpContr.ValorBase = LinhaMae.NOATotal;
                //if (ValCont != ImpContr.ValorImposto)
                //    MessageBox.Show("Valor destacado n�o confere\r\nEsperado: " + ImpContr.ValorImposto.ToString("n2") + "\r\nInformado:" + ValCont.ToString("n2"), "ATEN��O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ImpContrrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                ImpContrrow.PAG_NOA = LinhaMae.NOA;
                ImpContrrow.PAGTipo = (int)dCheque.PAGTipo.Guia;
                ImpContrrow.PAGIMP = (int)TipoImposto.PIS_COFINS_CSLL;
                ImpContrrow.PAGValor = ValCont;
                ImpContrrow.PAGVencimento = ImpContr.VencimentoEfetivo;
                ImpContrrow.CHEStatus = (int)Enumeracoes.CHEStatus.Cadastrado;
                ImpContrrow.CHEFavorecido = ImpContr.Nominal();
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpContrrow);
            }
        }

        static TipoImposto Tipocidade
        {
            get { return Framework.DSCentral.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA; }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (ImpISS != null)
                return;
            

            decimal ValCont = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(Tipocidade) + 0.004999M, 2);
            if (VirInput.Input.Execute("Val.Destacado", ref ValCont,2,true))
            {
                ValCont = Math.Round(ValCont, 2);
                if (ValCont <= 0)
                    return;
                ImpISS = new dllImpostoNeon.ImpostoNeon(Tipocidade);
                ImpISS.DataNota = LinhaMae.NOADataEmissao;
                ImpISS.DataPagamento = MenorData;
                ImpISS.ValorBase = LinhaMae.NOATotal;
                ImpISS.Competencia = new Competencia(LinhaMae.NOACompet);
                // if (ValCont != ImpContr.ValorImposto)
                //    MessageBox.Show("Valor destacado n�o confere\r\nEsperado: " + ImpContr.ValorImposto.ToString("n2") + "\r\nInformado:" + ValCont.ToString("n2"), "ATEN��O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ImpISSrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                ImpISSrow.PAG_NOA = LinhaMae.NOA;
                ImpISSrow.PAGTipo = (int)dCheque.PAGTipo.Guia;
                ImpISSrow.PAGIMP = (int)Tipocidade;
                ImpISSrow.PAGValor = ValCont;
                ImpISSrow.PAGVencimento = ImpISS.VencimentoEfetivo;                
                ImpISSrow.CHEFavorecido = ImpISS.Nominal();
                ImpISSrow.CHEStatus = (int)Enumeracoes.CHEStatus.Cadastrado;
                //if (MessageBox.Show("Guia j� emitida?", "Guia", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                //    ImpISSrow.PAGDataGuia = DateTime.Today;
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpISSrow);
            }

        }

        private void bAutoTeste_Click(object sender, EventArgs e)
        {
            malote M1 = null;
            malote M2 = null;
            bool RetornaNaData;
            dCheque.PAGTipo[] PAGTestar = new dCheque.PAGTipo[] { dCheque.PAGTipo.boleto, dCheque.PAGTipo.sindico };
            memoEdit1.Text  = "************AUTO TESTE************\r\n\r\n";
            memoEdit1.Text += "-------Data Limite-------\r\n";
            for (bool procuracao = false; ; procuracao = true)
            {
                memoEdit1.Text += procuracao ? "\r\nProcuracao:\r\n" : "sem Procuracao:\r\n";
                foreach (dCheque.PAGTipo PAGTIPO in PAGTestar)
                {
                    memoEdit1.Text += "\r\n    Tipo Pagamento :"+PAGTIPO.ToString();
                    foreach (malote.TiposMalote TipoMalote in Enum.GetValues(typeof(malote.TiposMalote)))
                    {
                        memoEdit1.Text += "\r\n\r\n     Tipo Malote :" + TipoMalote.ToString() + "\r\n";
                        for (DateTime Data = new DateTime(2009, 1, 5); Data < new DateTime(2009, 1, 13); Data = Data.AddDays(1))
                        {
                            foreach (int horas in new int[] { 10, 15 })
                            {
                                DateTime HoraCadastro = Data.AddHours(horas);
                                AjustaDataLimite(PAGTIPO, procuracao, TipoMalote, HoraCadastro);
                                memoEdit1.Text += String.Format("      Cadastro {0:dd/MM:HH(ddd)} -> Limite {1:dd/MM/yyyy (ddd)}{2}\r\n",
                                    HoraCadastro, 
                                    DataLimite.DateTime, 
                                    RetNoVencimento ? " Malote retorna no dia de Venct pela manha" : "");
                                foreach (DateTime DataT in new DateTime[] { DataLimite.DateTime.AddDays(-4), DataLimite.DateTime.AddDays(-3), DataLimite.DateTime.AddDays(-2), DataLimite.DateTime.AddDays(-1), DataLimite.DateTime })
                                {
                                    DateTime[] Datas = Cheque.CalculaDatas(PAGTIPO, procuracao, TipoMalote, DataT,HoraCadastro, ref M1, ref M2,out RetornaNaData);
                                    memoEdit1.Text += String.Format("                 Vencimento {0:dd/MM(ddd)}: Ida ({1}) -> Volta ({2}) {3} {4}\r\n",
                                        DataT,
                                        M1 == null ? Datas[0].ToString("dd/MM:HH(ddd)") : M1.ToString("ss"),
                                        M2 == null ? Datas[1].ToString("dd/MM:HH(ddd)") : M2.ToString("ss"),
                                        RetornaNaData ? "*":" ",
                                        (Datas[0] < HoraCadastro) ? "Falha" : "Ok   "
                                        );
                                }
                            }                            
                        }
                    }
                }
                if (procuracao)
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            dCheque.PAGTipo[] PAGTestar = new dCheque.PAGTipo[] { dCheque.PAGTipo.boleto, dCheque.PAGTipo.sindico };
            memoEdit1.Text = "************AUTO TESTE************\r\n\r\n";
            memoEdit1.Text += "-------Cheques-------\r\n";
            memoEdit1.Text += String.Format("Data Referencia:{0}\r\n",DateTime.Today);
            malote M1=null;
            malote M2=null;
            bool RetornaNaData;
            for (bool procuracao = false; ; procuracao = true)
            {
                memoEdit1.Text += procuracao ? "\r\nProcuracao:\r\n" : "sem Procuracao:\r\n";
                foreach (dCheque.PAGTipo PAGTIPO in PAGTestar)
                {
                    memoEdit1.Text += "\r\n    Tipo Pagamento :" + PAGTIPO.ToString();
                    foreach (malote.TiposMalote TipoMalote in Enum.GetValues(typeof(malote.TiposMalote)))
                    {
                        memoEdit1.Text += String.Format("\r\n\r\n     Tipo Malote :{0}\r\n", TipoMalote);
                        for (DateTime Data = DateTime.Today.AddDays(-5); Data < DateTime.Today.AddDays(20); Data = Data.AddDays(1))
                        {
                            DateTime[] Datas = Cheque.CalculaDatas(PAGTIPO, procuracao, TipoMalote, Data, DateTime.Today, ref M1, ref M2, out RetornaNaData);
                            memoEdit1.Text += String.Format("      Vencimento {0:dd/MM(ddd)}: Ida ({1}) -> Volta ({2}) {3}\r\n",
                                Data,
                                M1 == null ? Datas[0].ToString("dd/MM:HH(ddd)") : M1.ToString("ss"),
                                M2 == null ? Datas[1].ToString("dd/MM:HH(ddd)") : M2.ToString("ss"),
                                RetornaNaData ? "*":" ");
                        }
                    }
                }
                if (procuracao)
                    break;
            }
        }

        private void INSSPJ() 
        {
            if (ImpINSSrow != null)
                return;
            decimal INSS = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(TipoImposto.INSS) + 0.004999M, 2);
            if (VirInput.Input.Execute("INSS m�o-de-obra", ref INSS, 2, true))
            {
                INSS = Math.Round(INSS, 2);
                if (INSS <= 0)
                    return;
                ImpINSS = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSS);
                ImpINSS.DataNota = LinhaMae.NOADataEmissao;
                ImpINSS.DataPagamento = MenorData;
                ImpINSS.ValorBase = LinhaMae.NOATotal;
                //if (INSS != ImpINSS.ValorImposto)
                //    MessageBox.Show("Valor do INSS n�o confere\r\nEsperado: " + ImpINSS.ValorImposto.ToString("n2") + "\r\nInformado:" + INSS.ToString("n2"), "ATEN��O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ImpINSSrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                ImpINSSrow.PAG_NOA = LinhaMae.NOA;
                ImpINSSrow.PAGTipo = (int)dCheque.PAGTipo.Guia;
                ImpINSSrow.PAGIMP = (int)TipoImposto.INSS;
                ImpINSSrow.PAGValor = INSS;
                ImpINSSrow.PAGVencimento = ImpINSS.VencimentoEfetivo;
                ImpINSSrow.CHEStatus = (int)Enumeracoes.CHEStatus.Cadastrado;
                ImpINSSrow.CHEFavorecido = ImpINSS.Nominal();
                dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpINSSrow);
            }
        }

        private void INSSPF()
        {
            if (ImpINSSTomadorrow == null)
            {
                decimal INSST = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(TipoImposto.INSSpfEmp) + 0.004999M, 2);
                if (VirInput.Input.Execute("INSS tomador", ref INSST, 2, true))
                {
                    INSST = Math.Round(INSST, 2);
                    if (INSST > 0)
                    {
                        ImpINSSTomador = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSSpfEmp);
                        ImpINSSTomador.DataNota = LinhaMae.NOADataEmissao;
                        ImpINSSTomador.DataPagamento = MenorData;
                        ImpINSSTomador.ValorBase = LinhaMae.NOATotal;
                        ImpINSSTomadorrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                        ImpINSSTomadorrow.PAG_NOA = LinhaMae.NOA;
                        ImpINSSTomadorrow.PAGTipo = (int)dCheque.PAGTipo.Guia;
                        ImpINSSTomadorrow.PAGIMP = (int)TipoImposto.INSSpfEmp;
                        ImpINSSTomadorrow.PAGValor = INSST;
                        ImpINSSTomadorrow.PAGVencimento = ImpINSSTomador.VencimentoEfetivo;
                        ImpINSSTomadorrow.CHEStatus = (int)Enumeracoes.CHEStatus.Cadastrado;
                        ImpINSSTomadorrow.CHEFavorecido = ImpINSSTomador.Nominal();
                        dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpINSSTomadorrow);
                    }
                }
                decimal INSSP = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(TipoImposto.INSSpfRet) + 0.004999M, 2);
                decimal Teto = Imposto.Teto(TipoImposto.INSSpfRet);
                if (Teto < INSSP)
                    INSSP = Teto;
                if (VirInput.Input.Execute("INSS Prestador", ref INSSP, 2, true))
                {
                    if (Teto < INSSP)
                    {
                        MessageBox.Show(string.Format("ATEN��O: Valor m�ximo do INSS = {0:n2}. Valor corrigido",Teto));
                        INSSP = Teto;
                    }
                    else
                        INSSP = Math.Round(INSSP, 2);
                    if (INSSP > 0)
                    {
                        ImpINSSPrestador = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSSpfRet);
                        ImpINSSPrestador.DataNota = LinhaMae.NOADataEmissao;
                        ImpINSSPrestador.DataPagamento = MenorData;
                        ImpINSSPrestador.ValorBase = LinhaMae.NOATotal;
                        ImpINSSPrestadorrow = dNOtAsLocal.PAGamentos.NewPAGamentosRow();
                        ImpINSSPrestadorrow.PAG_NOA = LinhaMae.NOA;
                        ImpINSSPrestadorrow.PAGTipo = (int)dCheque.PAGTipo.Guia;
                        ImpINSSPrestadorrow.PAGIMP = (int)TipoImposto.INSSpfRet;
                        ImpINSSPrestadorrow.PAGValor = INSSP;
                        ImpINSSPrestadorrow.PAGVencimento = ImpINSSPrestador.VencimentoEfetivo;
                        ImpINSSPrestadorrow.CHEStatus = (int)Enumeracoes.CHEStatus.Cadastrado;
                        ImpINSSPrestadorrow.CHEFavorecido = ImpINSSPrestador.Nominal();
                        dNOtAsLocal.PAGamentos.AddPAGamentosRow(ImpINSSPrestadorrow);
                    }
                }
            }
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            switch (cCNPJ.Tipo)
            {
                case DocBacarios.TipoCpfCnpj.CNPJ:
                    INSSPJ();
                    break;
                case DocBacarios.TipoCpfCnpj.CPF:
                    INSSPF();
                    break;
                case DocBacarios.TipoCpfCnpj.INVALIDO:                   
                default:
                    break;
            }                      
        }

        private void lookUpFOR2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
            {
                if (rowFRN == null)
                    return;
                int FRN = rowFRN.FRN;
                Cadastros.Fornecedores.cFornecedoresCampos EditorFRN = new Cadastros.Fornecedores.cFornecedoresCampos();               
                EditorFRN.Fill(CompontesBasicos.TipoDeCarga.pk, FRN, false);
                if (EditorFRN.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    Fornecedores.Clear();
                    AjustaCNPJ(FRN);
                }
            }
        }

        private void nOATotalSpinEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
            {
                nOATotalSpinEdit.DoValidate();
                if (!CamposObrigatoriosOk(dNOtAsBindingSource))
                {
                    MessageBox.Show("Campo obrigat�rio");
                    foreach (DevExpress.XtraEditors.BaseEdit c in Controlesobrigatorios)
                        if (c.ErrorText != "")
                        {
                            c.Focus();
                            break;
                        }
                }
                else
                {
                    if ((LinhaMae.NOANumero == 0) || (LinhaMae.IsNOA_FRNNull()))
                    {
                        MessageBox.Show("Este recurso s� est� dispon�vel para notas fiscais v�lidas (n�mero/CNPJ)", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    };

                    BindingSourcePrincipal.EndEdit();
                    NotaMae.cNotaMae cNotaMae;
                    //if (LinhaMae.NOA == -1)
                    //{
                        InibirVerificaDuplicidadeDeNota = true;
                        if (!Update_F())
                        {
                            InibirVerificaDuplicidadeDeNota = false;
                            return;
                        }
                    //};
                    InibirVerificaDuplicidadeDeNota = false;
                    cNotaMae = new ContaPagar.NotaMae.cNotaMae(LinhaMae.NOA_CON, LinhaMae.NOA_FRN, LinhaMae.NOANumero);
                    System.Collections.SortedList NovasNotas = new System.Collections.SortedList();
                    if (cNotaMae.ShowEmPopUp() == DialogResult.OK)
                    {
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar cNotasCampos - 1809",dNOtAs.dNOtAsSt.NOtAsTableAdapter);                            
                            foreach (NotaMae.dNotaMae.NOtAsRow rowFilhas in cNotaMae.dNotaMae.NOtAs)
                            {
                                if (rowFilhas.NOA < 0)
                                {
                                    dNOtAs.NOtAsRow novaRow = dNOtAs.dNOtAsSt.NOtAs.NewNOtAsRow();
                                    novaRow.NOA_CON = LinhaMae.NOA_CON;
                                    novaRow.NOA_FRN = LinhaMae.NOA_FRN;
                                    novaRow.NOA_PLA = rowFilhas.NOA_PLA;
                                    novaRow.NOAAguardaNota = LinhaMae.NOAAguardaNota;
                                    if (!LinhaMae.IsNOACodServicoNull())
                                        novaRow.NOACodServico = LinhaMae.NOACodServico;
                                    novaRow.NOADataEmissao = LinhaMae.NOADataEmissao;
                                    novaRow.NOACompet = LinhaMae.NOACompet;
                                    novaRow.NOADATAI = DateTime.Now;
                                    novaRow.NOADefaultPAGTipo = LinhaMae.NOADefaultPAGTipo;
                                    novaRow.NOAI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                                    novaRow.NOANumero = LinhaMae.NOANumero;
                                    if (!LinhaMae.IsNOAObsNull())
                                        novaRow.NOAObs = LinhaMae.NOAObs;
                                    novaRow.NOAServico = rowFilhas.NOAServico;
                                    novaRow.NOAStatus = (int)dNOtAs.NOAStatus.Parcial;
                                    novaRow.NOATipo = LinhaMae.NOATipo;
                                    novaRow.NOATotal = rowFilhas.NOATotal;
                                    dNOtAs.dNOtAsSt.NOtAs.AddNOtAsRow(novaRow);
                                    dNOtAs.dNOtAsSt.NOtAsTableAdapter.Update(novaRow);
                                    novaRow.AcceptChanges();
                                    NovasNotas.Add(novaRow.NOA,string.Format("Nota:{0} - {1}", novaRow.NOANumero, novaRow.NOA_PLA));                                    
                                }
                            }
                            VirMSSQL.TableAdapter.STTableAdapter.Commit();
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                            MessageBox.Show("Erro ao Gravar");
                            return;
                        }
                        foreach (System.Collections.DictionaryEntry DE in NovasNotas)
                        {
                            cNotasCampos NovaTab = (cNotasCampos)CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludo(typeof(cNotasCampos), (string)DE.Value, CompontesBasicos.TipoDeCarga.pk, (int)DE.Key, false);
                            NovaTab.InibirVerificaDuplicidadeDeNota = true;
                        }
                        InibirVerificaDuplicidadeDeNota = true;
                    }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (NOAAbrir != 0)
            {
                cNotasCampos NovaTab = (cNotasCampos)CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludo(typeof(cNotasCampos), "VERIFICAR", CompontesBasicos.TipoDeCarga.pk, NOAAbrir, true);
                NovaTab.InibirVerificaDuplicidadeDeNota = true;
            }
            NOAAbrir = 0;
        }

        

        

        private void nOADataEmissaoDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if ((LinhaMae["NOACompet"] == DBNull.Value) || (LinhaMae.NOACompet < 201100))
            {
                LinhaMae.NOACompet = nOADataEmissaoDateEdit.DateTime.Year * 100 + nOADataEmissaoDateEdit.DateTime.Month;
                cCompet1.CompetenciaBind = LinhaMae.NOACompet;
            }
        }

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                if (PLArow == null)
                    return;
                cNovoSPL novo = new cNovoSPL(string.Format("{0} - {1}", PLArow.PLA, PLArow.PLADescricao));
                novo.Cadastre(PLArow.PLA);
                /*
                //novo.Plano.Text = string.Format("{0} - {1}", PLArow.PLA, PLArow.PLADescricao);
                if (novo.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    try
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoLocal();
                        string Comando1 = "INSERT INTO SubPLano(SPL_PLA,SPLCodigo,SPLDescricao,SPLServico,SPLNoCondominio) VALUES (@P1,@P2,@P3,1,@P4)";
                        int SPL = VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc(Comando1, PLArow.PLA, novo.Codigo.Text, novo.Descricao.Text, novo.checkNoLocal.Checked);
                        //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update NOtAs set NOA_SPL = @P1 where NOA_PLA = @P2 and NOA_SPL is null", SPL, row.NOA_PLA);
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                        Framework.datasets.dPLAnocontas.dPLAnocontasSt25.SubPLanoTableAdapter.Fill25(Framework.datasets.dPLAnocontas.dPLAnocontasSt25.SubPLano);
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                    }
                   
                    
                }
                */
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if(e.Column.Equals(colBotao))
            {
                dNOtAs.PAGamentosRow rowDel = (dNOtAs.PAGamentosRow)gridView1.GetDataRow(e.RowHandle);                
                e.RepositoryItem = ((rowDel != null) && Deletavel(rowDel)) ? Botoes : null;
            }
            else if (e.Column.Equals(colbotaoCheque))
            {
                dNOtAs.PAGamentosRow rowDel = (dNOtAs.PAGamentosRow)gridView1.GetDataRow(e.RowHandle);
                e.RepositoryItem = ((rowDel != null) && !rowDel.IsPAG_CHENull()) ? BotaoCheque : null;
            }

        }

        private void BotaoCheque_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dNOtAs.PAGamentosRow rowPAG = (dNOtAs.PAGamentosRow)gridView1.GetFocusedDataRow();
            if ((rowPAG == null) || rowPAG.IsPAG_CHENull())
                return;
            dllCheques.Cheque Cheque = new dllCheques.Cheque(rowPAG.PAG_CHE, dllCheques.Cheque.TipoChave.CHE);
            if (Cheque.Encontrado)
                Cheque.Editar(false);
                //if (Cheque.Editar(false) == DialogResult.OK)
                //{
                
                //}
        }

        private void groupControl1_Enter(object sender, EventArgs e)
        {
            bool contemImpostos = (ImpIR == null) && (ImpContr == null) && (ImpISS == null) && (ImpINSS == null) && (ImpINSSTomador == null) && (ImpINSSPrestador == null);
            nOADataEmissaoDateEdit.Properties.ReadOnly = !contemImpostos;
            cCompet1.ReadOnly = !contemImpostos;
            nOATotalSpinEdit.Properties.ReadOnly = !contemImpostos;
        }

        
        
    }
}

