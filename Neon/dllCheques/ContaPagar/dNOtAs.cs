﻿/*
LH - 01/12/2014 14:00 14.1.4.94  - alterção no funcionamento dos pagamentos periódicos com boleto
MR - 26/12/2014 10:00            - Inclusão do parâmetro GPS não obrigatório ao incluir nota (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
MR - 27/04/2015 12:00            - Inclusão do campo PAGStatus não obrigatório no PAGamentosTableAdapter
MR - 20/05/2015 17:00            - Inclusão do campo CHE_CCT no PAGamentosTableAdapter
                                 - Pagamento de tributo eletronico com inclusao do "guia de recolhimento - eletrônico" (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***)
                                 - Honorarios com pagamento eletrônico para condominios habilitados (Alterações indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***)
MR - 10/06/2015 08:20            - Inclusão do campo NOA_CCTCred no NOtAsTableAdapter
                                   Aumento do tamanho do campo PAGAdicional no PAGamentosTableAdapter                       
MR - 15/06/2015 11:30            - Pagamento de tributo eletronico não usa "guia de recolhimento - eletrônico" para INSSpfRet, INSSpfEmp ou INSSpf que será convertido na SEFIP Folha (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (15/06/2015 11:30) ***)
MR - 22/06/2015 12:00            - PECreditoConta mensal fixo entre contas do próprio condominio (Alterações indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***)
                                 - Honorários para Linhagem (FRN = 149) não vai fazer eletrônico forçadamente, por solicitação da Iris, devido a cobrança da taxa de TED para Banco do Brasil
*/

using dllCheques;
using System;
using dllVirEnum;
using Framework.objetosNeon;
using System.Drawing;
using Framework;
using FrameworkProc;
using VirMSSQL;
using VirDB.Bancovirtual;

namespace ContaPagar {
    

    partial class dNOtAs
    {


        #region Estáticos
        private static dNOtAs _dNOtAsSt;
        private static dNOtAs _dNOtAsStF;


        /// <summary>
        /// dataset estático:dNOtAs
        /// </summary>
        public static dNOtAs dNOtAsSt
        {
            get
            {
                if (_dNOtAsSt == null)
                {
                    _dNOtAsSt = new dNOtAs(EMPTProc.EMPTipo.Local);
                    VirDB.VirtualTableAdapter.AjustaAutoInc(_dNOtAsSt);
                }
                return _dNOtAsSt;
            }
        }

        /// <summary>
        /// dataset estático:dNOtAs
        /// </summary>
        public static dNOtAs dNOtAsStF
        {
            get
            {
                if (_dNOtAsStF == null)
                {
                    _dNOtAsStF = new dNOtAs(EMPTProc.EMPTipo.Filial);
                    VirDB.VirtualTableAdapter.AjustaAutoInc(_dNOtAsStF);
                }
                return _dNOtAsStF;
            }
        }

        /// <summary>
        /// dataset estático:dPagamentosPeriodicos Local/Filial
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public static dNOtAs dNOtAsStX(EMPTProc.EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTProc.EMPTipo.Local ? dNOtAsSt : dNOtAsStF;
        } 
        #endregion

        /// <summary>
        /// Construitor com EMPTipo
        /// </summary>
        /// <param name="_EMPTipo"></param>
        public dNOtAs(EMPTProc.EMPTipo _EMPTipo)
            : this()
        {
            this._EMPTipo = new EMPTProc(_EMPTipo);
        }

        private EMPTProc _EMPTipo;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTipo
        {
            get
            {
                if (_EMPTipo == null)
                    _EMPTipo = new EMPTProc(EMPTProc.EMPTipo.Local);
                return _EMPTipo;
            }
        }

        #region Adapters

        private dNOtAsTableAdapters.NOtAsTableAdapter nOtAsTableAdapter;

        private dNOtAsTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dNOtAsTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dNOtAsTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: NOtAs
        /// </summary>
        public dNOtAsTableAdapters.NOtAsTableAdapter NOtAsTableAdapter
        {
            get
            {
                if (nOtAsTableAdapter == null)
                {
                    nOtAsTableAdapter = new dNOtAsTableAdapters.NOtAsTableAdapter();
                    nOtAsTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return nOtAsTableAdapter;
            }
        }

        private dNOtAsTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dNOtAsTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dNOtAsTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return pAGamentosTableAdapter;
            }
        }

        private dNOtAsTableAdapters.PaGamentosFixosTableAdapter paGamentosFixosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PaGamentosFixos
        /// </summary>
        public dNOtAsTableAdapters.PaGamentosFixosTableAdapter PaGamentosFixosTableAdapter
        {
            get
            {
                if (paGamentosFixosTableAdapter == null)
                {
                    paGamentosFixosTableAdapter = new dNOtAsTableAdapters.PaGamentosFixosTableAdapter();
                    paGamentosFixosTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return paGamentosFixosTableAdapter;
            }
        }

        #endregion


        /// <summary>
        /// Indica qual amarração será feita
        /// </summary>
        public enum TipoChave
        {
            /// <summary>
            /// 
            /// </summary>
            Nenhuma,
            /// <summary>
            /// 
            /// </summary>
            BOL,
            /// <summary>
            /// 
            /// </summary>
            PGF,
            /// <summary>
            /// 
            /// </summary>
            PES
        }

        #region NotaSimples                

        /// <summary>
        /// Inclui uma nota com um cheque de forma simples
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CompServ"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="FRN"></param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, ContaPagar.dNOtAs.NOATipo NOATipo, string Favorecido, 
            int CON, dCheque.PAGTipo Tipo, bool Agrupa, string PLA,int SPL, string Servico, decimal Valor, int FRN)
        {
            return IncluiNotaSimples(numeroNF, Emissao, Vencimento, CompServ, NOATipo, Favorecido, CON, Tipo, Agrupa, PLA,SPL, Servico, Valor, FRN, false, TipoChave.Nenhuma, 0);
        }        

        /// <summary>
        /// Inclui uma nota com um cheque de forma simples
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="CompServ">Competência do serviço</param>
        /// <param name="SPL">Sub plano</param>
        /// <param name="FRN">Fornecedor se nao houver coloque zero</param>
        /// <param name="Retencoes">lista das retenções</param>
        /// <param name="SomenteImpostos">Não gera os pagamentos somentes as retenções</param>
        /// <param name="Chave">Chave</param>
        /// <param name="TipoCh">Tipo da chave</param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, ContaPagar.dNOtAs.NOATipo NOATipo, string Favorecido, 
            int CON, dCheque.PAGTipo Tipo, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos, 
            TipoChave TipoCh, int Chave, params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            return IncluiNotaSimples(numeroNF, Emissao, Vencimento, CompServ, NOATipo, Favorecido, CON, Tipo, "", Agrupa, PLA, SPL, Servico, Valor, FRN, SomenteImpostos, TipoCh, Chave, 0, Retencoes);
        }

        
        //*** MRC - INICIO - PAG-FOR (2) ***
        //public bool IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, ContaPagar.dNOtAs.NOATipo NOATipo, string Favorecido,
        //    int CON, dCheque.PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos, 
        //    TipoChave TipoCh, int Chave, params dllImpostos.SolicitaRetencao[] Retencoes)
        //{
        //    return IncluiNotaSimples(numeroNF, Emissao, Vencimento,CompServ, NOATipo, Favorecido, CON, Tipo, DadosPag, Agrupa, PLA,SPL, Servico, Valor, FRN, SomenteImpostos, TipoCh, Chave, 0, Retencoes);
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CompServ"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="DadosPag"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="FRN"></param>
        /// <param name="SomenteImpostos"></param>
        /// <param name="TipoCh"></param>
        /// <param name="Chave"></param>
        /// <param name="CCT"></param>
        /// <param name="Retencoes"></param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, ContaPagar.dNOtAs.NOATipo NOATipo, string Favorecido,
            int CON, dCheque.PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos,
            TipoChave TipoCh, int Chave, int CCT = 0, params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            return IncluiNotaSimples(numeroNF, Emissao, Vencimento, CompServ, NOATipo, Favorecido, CON, Tipo, DadosPag, Agrupa, PLA, SPL, Servico, Valor, FRN, SomenteImpostos, TipoCh, Chave, 0, CCT, Retencoes);
        }
        //*** MRC - FIM - PAG-FOR (2) ***

        /// <summary>
        /// 
        /// </summary>
        public Cheque UltimoChequeCriado;

        //public bool IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, ContaPagar.dNOtAs.NOATipo NOATipo, string Favorecido,
        //    int CON, dCheque.PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos, 
        //    TipoChave TipoCh, int Chave, int CCD, params dllImpostos.SolicitaRetencao[] Retencoes) 
        /// <summary>
        /// Inclui uma nota com um cheque de forma simples
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="CCD">Linque com o extrato</param>
        /// <param name="FRN">Fornecedor se nao houver coloque zero</param>
        /// <param name="Retencoes">lista das retenções</param>
        /// <param name="Chave">Chave</param>
        /// <param name="TipoCh">Tipo da chave</param>
        /// <param name="DadosPag">Dados do deposito</param>
        /// <param name="SomenteImpostos">Não gera os pagamentos somentes as retenções</param>
        /// <param name="CompServ"></param>
        /// <param name="SPL"></param>
        /// <param name="CCT"></param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, ContaPagar.dNOtAs.NOATipo NOATipo, string Favorecido,
            int CON, dCheque.PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos,
            TipoChave TipoCh, int Chave, int CCD, int CCT = 0, params dllImpostos.SolicitaRetencao[] Retencoes)        
        {
        //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
            return IncluiNotaSimples(numeroNF, Emissao, Vencimento, CompServ, NOATipo, Favorecido, CON, Tipo, DadosPag, Agrupa, PLA, SPL, Servico, Valor, FRN, SomenteImpostos, TipoCh, Chave, CCD, CCT, 0, Retencoes);
        }

        /// <summary>
        /// Inclui uma nota com um cheque de forma simples
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="CCD">Linque com o extrato</param>
        /// <param name="FRN">Fornecedor se nao houver coloque zero</param>
        /// <param name="Retencoes">lista das retenções</param>
        /// <param name="Chave">Chave</param>
        /// <param name="TipoCh">Tipo da chave</param>
        /// <param name="DadosPag">Dados do deposito</param>
        /// <param name="SomenteImpostos">Não gera os pagamentos somentes as retenções</param>
        /// <param name="CompServ"></param>
        /// <param name="SPL"></param>
        /// <param name="CCT"></param>
        /// <param name="GPS">Linque com o GPS</param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, ContaPagar.dNOtAs.NOATipo NOATipo, string Favorecido,
            int CON, dCheque.PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos,
            TipoChave TipoCh, int Chave, int CCD, int CCT, int GPS = 0, params dllImpostos.SolicitaRetencao[] Retencoes)
        {
        //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***
            //legado
            int? novoFRN = null;
            int? novoCCD = null;
            int? novoCCT = null;
            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
            int? novoGPS = null;
            //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***
            if (FRN != 0)
                novoFRN = FRN;
            if (CCD != 0)
                novoCCD = CCD;
            if (CCT != 0)
                novoCCT = CCT;
            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
            if (GPS != 0)
                novoGPS = GPS;
            //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***

            return IncluiNotaSimples_Efetivo(NOAStatus.Cadastrada,
                                             numeroNF,
                                             Emissao,
                                             Vencimento,
                                             CompServ,
                                             NOATipo,
                                             Favorecido,
                                             CON,
                                             Tipo,
                                             DadosPag,
                                             Agrupa,
                                             PLA,
                                             SPL,
                                             Servico,
                                             Valor,
                                             novoFRN,
                                             SomenteImpostos,
                                             TipoCh,
                                             Chave,
                                             novoCCD,
                                             novoCCT,
                                             //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
                                             novoGPS,
                                             //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***
                                             Retencoes);
        }

        internal int? IncluiNotaProvisoria(DateTime Vencimento, 
                                          Competencia CompServ, 
                                          ContaPagar.dNOtAs.NOATipo NOATipo, 
                                          string Favorecido,
                                          int CON, 
                                          dCheque.PAGTipo Tipo, 
                                          bool Agrupa, 
                                          string PLA, 
                                          int SPL, 
                                          string Servico, 
                                          decimal Valor, 
                                          int? FRN,
                                          TipoChave TipoCh, 
                                          int Chave, 
                                          string DadosPag = "", params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            return IncluiNotaSimples_Efetivo(NOAStatus.NotaProvisoria,
                                             0,
                                             Vencimento.AddDays(-5),
                                             Vencimento,
                                             CompServ,
                                             NOATipo,
                                             Favorecido,
                                             CON,
                                             Tipo,
                                             DadosPag,
                                             Agrupa,
                                             PLA,
                                             SPL,
                                             Servico,
                                             Valor,
                                             FRN,
                                             false,
                                             TipoCh,
                                             Chave,
                                             null,
                                             null,
                                             //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
                                             null,
                                             //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***
                                             Retencoes);
        }

        private Framework.datasets.dContasLogicas.ContaCorrenTeRow GETrowCCT(int? CCT)
        {
            if (!CCT.HasValue)
                return null;
            else
                return Framework.datasets.dContasLogicas.dContasLogicasSt.ContaCorrenTe.FindByCCT(CCT.Value);
        }

        private int? USUChamador;

        private int? IncluiNotaSimples_Efetivo(NOAStatus _NOAStatus,
                                               int numeroNF, 
                                               DateTime Emissao, 
                                               DateTime Vencimento, 
                                               Competencia CompServ, 
                                               ContaPagar.dNOtAs.NOATipo NOATipo, 
                                               string Favorecido,
                                               int CON, 
                                               dCheque.PAGTipo Tipo, 
                                               string DadosPag, 
                                               bool Agrupa, 
                                               string PLA, 
                                               int SPL, 
                                               string Servico, 
                                               decimal Valor, 
                                               int? FRN, 
                                               bool SomenteImpostos,
                                               TipoChave TipoCh, 
                                               int Chave, 
                                               int? CCD, 
                                               int? CCT,
                                               //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
                                               int? GPS,
                                               //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***
                                               params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 1");
            if (Valor < 0)
                return null;
            if ((Valor == 0) && (Tipo != dCheque.PAGTipo.Acumular))
                return null;
            Cheque NovoCheque = null;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("ContaPagar dNotas - 238",
                                                                        NOtAsTableAdapter, 
                                                                        PAGamentosTableAdapter);                
                NOtAsRow rowNOA = NOtAs.NewNOtAsRow();
                rowNOA.NOA_CON = CON;
                rowNOA.NOA_PLA = PLA;
                rowNOA.NOA_SPL = SPL;
                if (FRN.HasValue)
                    rowNOA.NOA_FRN = FRN.Value;
                rowNOA.NOAServico = Servico;
                rowNOA.NOADataEmissao = Emissao;
                rowNOA.NOAAguardaNota = false;
                rowNOA.NOADATAI = DateTime.Now;                
                rowNOA.NOAI_USU = USUChamador.GetValueOrDefault(Framework.datasets.dUSUarios.USUAtual(EMPTipo.Tipo));
                rowNOA.NOANumero = numeroNF;
                rowNOA.NOAStatus = (int)_NOAStatus;
                rowNOA.NOATipo = (int)NOATipo;
                
                if (DadosPag != "")
                    //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                    if (Tipo == dllCheques.dCheque.PAGTipo.PECreditoConta && DadosPag.Contains("- CCT = "))
                        rowNOA.NOA_CCTCred = Convert.ToInt32(DadosPag.Substring(DadosPag.IndexOf("CCT = ") + 6));
                    else
                    //*** MRC - TERMINO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                        rowNOA.NOADadosPag = DadosPag;
                    
                if (CCT.HasValue)
                    rowNOA.NOA_CCT = CCT.Value;
                
                rowNOA.NOATotal = Valor;
                if (Tipo == dCheque.PAGTipo.Acumular)
                    rowNOA.NOADefaultPAGTipo = (int)dCheque.PAGTipo.cheque;
                else
                    rowNOA.NOADefaultPAGTipo = (int)Tipo;
                if (TipoCh == TipoChave.PGF)
                    rowNOA.NOA_PGF = Chave;
                if (TipoCh == TipoChave.PES)
                    rowNOA.NOA_PES = Chave;
                if (CompServ != null)
                    rowNOA.NOACompet = CompServ.CompetenciaBind;
                else
                    rowNOA.NOACompet = rowNOA.NOADataEmissao.Year * 100 + rowNOA.NOADataEmissao.Month;
                NOtAs.AddNOtAsRow(rowNOA);
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("UPDATE",true);
                NOtAsTableAdapter.Update(rowNOA);
                CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 2");
                rowNOA.AcceptChanges();

                decimal ValorPagamento = Valor;

                //*** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = Framework.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(CON);
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 3");
                if (Retencoes != null)
                    foreach (dllImpostos.SolicitaRetencao Sol in Retencoes)
                    {
                        dllImpostoNeon.ImpostoNeon Imp = new dllImpostoNeon.ImpostoNeon(Sol.Tipo);
                        Imp.DataNota = Emissao;
                        Imp.DataPagamento = Vencimento;
                        Imp.ValorBase = Valor;
                        //Referência OneNote: iss com data errada dNOtAs.cs 11.1.7.5 14/11/2011
                        if ((Sol.Tipo == dllImpostos.TipoImposto.ISSQN) || (Sol.Tipo == dllImpostos.TipoImposto.ISS_SA))
                            Imp.Competencia = CompServ;
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                        dCheque.PAGTipo RetPAGTipo;
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (15/06/2015 11:30) ***
                        if (rowCON.IsCONCodigoComunicacaoTributosNull() || rowCON.CONCodigoComunicacaoTributos == "" ||
                            Sol.Tipo == dllImpostos.TipoImposto.ISSQN || Sol.Tipo == dllImpostos.TipoImposto.ISS_SA ||
                            Sol.Tipo == dllImpostos.TipoImposto.INSSpf || Sol.Tipo == dllImpostos.TipoImposto.INSSpfEmp || Sol.Tipo == dllImpostos.TipoImposto.INSSpfRet)
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (15/06/2015 11:30) ***
                               RetPAGTipo = dCheque.PAGTipo.Guia;
                        else
                            RetPAGTipo = dCheque.PAGTipo.PEGuia;
                        NovoCheque = new Cheque(Imp.VencimentoEfetivo, Imp.Nominal(), CON, RetPAGTipo, (_NOAStatus == NOAStatus.NotaProvisoria), CCD, null, "", (_NOAStatus == NOAStatus.NotaProvisoria));
                        //*** MRC - TERMINO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                        PAGamentosRow Improw = PAGamentos.NewPAGamentosRow();
                        Improw.PAGPermiteAgrupar = true;
                        Improw.PAG_CHE = NovoCheque.CHErow.CHE;
                        Improw.PAG_NOA = rowNOA.NOA;
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                        Improw.PAGTipo = (int)RetPAGTipo;
                        Improw.PAGStatus = (int)NovoCheque.CHEStatus;
                        //*** MRC - TERMINO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                        Improw.PAGIMP = (int)Sol.Tipo;
                        Improw.PAGValor = Sol.Valor == 0 ? Imp.ValorImposto : Sol.Valor;
                        if ((Sol.Descontar == CompontesBasicos.SimNao.Sim) || ((Sol.Descontar == CompontesBasicos.SimNao.Padrao) && (Imp.Desconta())))
                            ValorPagamento -= Improw.PAGValor;
                        Improw.PAGVencimento = Imp.VencimentoEfetivo;
                        //Improw.CHEStatus = (int)(_NOAStatus == NOAStatus.NotaProvisoria ? Enumeracoes.CHEStatus.CadastradoBloqueado : Enumeracoes.CHEStatus.Cadastrado);
                        Improw.CHEStatus = (int)NovoCheque.CHEStatus;
                        Improw.CHEFavorecido = Imp.Nominal();
                        PAGamentos.AddPAGamentosRow(Improw);
                        PAGamentosTableAdapter.Update(Improw);
                        Improw.AcceptChanges();
                        NovoCheque.AjustarTotalPelasParcelas();
                    };

                if (!SomenteImpostos)
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 4");
                    dCheque.PAGTipo PAGTipo;
                    Framework.datasets.dContasLogicas.ContaCorrenTeRow rowCCT = GETrowCCT(CCT);
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 4 A");
                    //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                    if (Tipo != dCheque.PAGTipo.Honorarios || (Tipo == dCheque.PAGTipo.Honorarios && FRN == 149) || rowCON.IsCONCodigoComunicacaoNull() || rowCON.CONCodigoComunicacao == "" || !CCT.HasValue || rowCCT == null || !rowCCT.CCTPagamentoEletronico)
                        PAGTipo = Tipo;
                    else                    
                        PAGTipo = dCheque.PAGTipo.PECreditoConta;
                    //*** MRC - TERMINO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                    PAGamentosRow rowPag = PAGamentos.NewPAGamentosRow();

                    if (Tipo != dCheque.PAGTipo.Acumular)
                    {                        
                        //if (_NOAStatus == NOAStatus.NotaProvisoria)
                        //    Agrupa = false;

                        //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                        //NovoCheque = new Cheque(Vencimento, Favorecido, CON, Tipo, !Agrupa, CCD, CCT, DadosPag, _NOAStatus == NOAStatus.NotaProvisoria);
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 4 B");
                        NovoCheque = new Cheque(Vencimento, Favorecido, CON, PAGTipo, !Agrupa, CCD, CCT, DadosPag, _NOAStatus == NOAStatus.NotaProvisoria,EMPTipo.Tipo);
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 4 C");
                        rowPag.PAGStatus = NovoCheque.CHErow.CHEStatus;
                        //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***

                        //if (_NOAStatus == NOAStatus.NotaProvisoria)
                        //    NovoCheque.CHErow.CHEStatus = (int)Enumeracoes.CHEStatus.CadastradoBloqueado;                        
                        rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                        if (Tipo != dCheque.PAGTipo.DebitoAutomatico)
                            if (CCD.HasValue)
                                if (!NovoCheque.AmarrarCCD(CCD.Value))
                                    throw new Exception("CCD não é nulo");
                    }
                    else
                        NovoCheque = null;
                    rowPag.PAG_NOA = rowNOA.NOA;
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 5");
                    if (GPS.HasValue)
                        rowPag.PAG_GPS = GPS.Value;
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                                    
                    rowPag.PAGDATAI = DateTime.Now;
                    rowPag.PAGI_USU = Framework.datasets.dUSUarios.USUAtual(EMPTipo.Tipo);
                    //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                    //rowPag.PAGTipo = (int)Tipo;
                    rowPag.PAGTipo = (int)PAGTipo;
                    //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                    rowPag.PAGValor = ValorPagamento;
                    rowPag.PAGVencimento = Vencimento;
                    //*** MRC - INICIO - PAG-FOR (2) ***
                    if (DadosPag != "")
                        rowPag.PAGAdicional = DadosPag;
                    rowPag.PAGPermiteAgrupar = Agrupa;
                    //*** MRC - FIM - PAG-FOR (2) ***
                    PAGamentos.AddPAGamentosRow(rowPag);
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("UPDATE");
                    PAGamentosTableAdapter.Update(rowPag);
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 6");
                    rowPag.AcceptChanges();                    
                    if (NovoCheque != null)
                    {
                        NovoCheque.LiberaEdicao = true;
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("AjustarTotalPelasParcelas");
                        NovoCheque.AjustarTotalPelasParcelas();
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 7");
                    }
                    if (TipoCh == TipoChave.BOL)
                        TableAdapter.ST().ExecutarSQLNonQuery("insert into BODxPAG (BOD, PAG) VALUES(@P1,@P2)",Chave,rowPag.PAG);
                }
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 7");
                EMPTipo.Commit();                
                UltimoChequeCriado = NovoCheque;
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 8");
                return rowNOA.NOA;
            }
            catch (Exception e)
            {
                EMPTipo.Vircatch(e);
                //TableAdapter.ST().Vircatch(e);
                throw new Exception("Erro na inclusão da nota", e);
            }            
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public enum NOAStatus
        {
            /// <summary>
            /// 
            /// </summary>
            NaoEncontrada=-1,
            /// <summary>
            /// 
            /// </summary>
            Cadastrada=0, 
            /// <summary>
            /// 
            /// </summary>
            Parcial=1, 
            /// <summary>
            /// 
            /// </summary>
            Liquidada=2,
            /// <summary>
            /// 
            /// </summary>
            SemPagamentos=3,
            /// <summary>
            /// Nota cancelada
            /// </summary>
            NotaCancelada = 4,
            /// <summary>
            /// A nota real ainda não chegou
            /// </summary>
            NotaProvisoria = 5,
            /// <summary>
            /// Nota com pagamento acumulado na próxima
            /// </summary>
            NotaAcumulada = 6,
            /// <summary>
            /// Nota com pagamento acumulado na próxima com valor a ajustar
            /// </summary>
            CadastradaAjustarValor = 7,
        }

        private VirEnum virEnumNOAStatus;

        /// <summary>
        /// 
        /// </summary>
        public VirEnum VirEnumNOAStatus {
            get {
                if (virEnumNOAStatus == null) {
                    virEnumNOAStatus = new VirEnum(typeof(NOAStatus));
                    virEnumNOAStatus.GravaNomes(NOAStatus.Cadastrada, "Nota cadastrada", Color.FromArgb(255, 255, 100));//amarelo
                    virEnumNOAStatus.GravaNomes(NOAStatus.Parcial, "Nota incompleta", Color.FromArgb(255, 200, 200));//vermelho
                    virEnumNOAStatus.GravaNomes(NOAStatus.Liquidada, "Pagamentos efetuados", Color.FromArgb(200, 255, 255));//verde
                    virEnumNOAStatus.GravaNomes(NOAStatus.SemPagamentos, "Sem Pagamentos", Color.Silver);
                    virEnumNOAStatus.GravaNomes(NOAStatus.NotaCancelada, "Nota Cancelada", Color.FromArgb(200, 200, 200)); //cinza
                    virEnumNOAStatus.GravaNomes(NOAStatus.NotaProvisoria, "Nota Provisória", Color.DarkGoldenrod); // marrom    
                    virEnumNOAStatus.GravaNomes(NOAStatus.NotaAcumulada, "Nota Acumulada", Color.Orange); // laranja
                    virEnumNOAStatus.GravaNomes(NOAStatus.CadastradaAjustarValor, "Nota Acumulada sem valor", Color.Orange); // laranja
                }
                return virEnumNOAStatus;
            }
        }



        /*
        private NOAStatus ParseNOAStatus(int Entrada)
        {
            return (NOAStatus)System.Enum.Parse(typeof(NOAStatus), Entrada.ToString());                
        }
        */
        /*
        public CHEquesRow BuscaCheque(string Fav, System.DateTime data,int CON) {
            CHEquesRow Cheque;
            if (CHEquesTableAdapter.FillByFav(CHEques, data, Fav, CON) > 0)
            {                
                Cheque = CHEques[0];
                return Cheque;
            };
            
            Cheque = CHEques.NewCHEquesRow();
            Cheque.CHEFavorecido = Fav;
            Cheque.CHEVencimento = data;
            Cheque.CHEBanco = data;            
            Cheque.CHE_CON = CON; 
            return Cheque;
            
        }
        */

        /// <summary>
        /// Tipos de nota
        /// </summary>
        public enum NOATipo { 
            /// <summary>
            /// Avulsa
            /// </summary>
            Avulsa = 0,
            /// <summary>
            /// Da folha de pagamentos
            /// </summary>
            Folha = 1,
            /// <summary>
            /// Importado de arquiovo XML
            /// </summary>
            ImportacaoXML = 2,
            /// <summary>
            /// Gerado pelo Pagamento Periódico
            /// </summary>
            PagamentoPeriodico = 3,
            /// <summary>
            /// Repasse
            /// </summary>
            Repasse = 4,
            /// <summary>
            /// 
            /// </summary>
            NotaParcial = 5,
            /// <summary>
            /// 
            /// </summary>
            Recibo = 6,
            /// <summary>
            /// 
            /// </summary>
            ReciboParcial = 7,
            /// <summary>
            /// 
            /// </summary>
            TransferenciaF = 8
        }

        private static VirEnum virEnumNOATipo;

        /// <summary>
        /// VirEnum para NOATipo
        /// </summary>
        public static VirEnum VirEnumNOATipo
        {
            get
            {
                if (virEnumNOATipo == null)
                {
                    virEnumNOATipo = new VirEnum(typeof(NOATipo));
                    virEnumNOATipo.GravaNomes(NOATipo.Avulsa, "Nota Avulsa");
                    virEnumNOATipo.GravaNomes(NOATipo.Folha, "Folha de pagamento");
                    virEnumNOATipo.GravaNomes(NOATipo.ImportacaoXML, "Imp. DTM");
                    virEnumNOATipo.GravaNomes(NOATipo.PagamentoPeriodico, "Pagamento Periódico");
                    virEnumNOATipo.GravaNomes(NOATipo.NotaParcial, "Nota Parcial");
                    virEnumNOATipo.GravaNomes(NOATipo.Recibo, "Recibo");
                    virEnumNOATipo.GravaNomes(NOATipo.ReciboParcial, "Recibo Parcial");
                    virEnumNOATipo.GravaNomes(NOATipo.Repasse, "Repasse");
                    virEnumNOATipo.GravaNomes(NOATipo.TransferenciaF, "Transferência entre contas");
                }
                return virEnumNOATipo;
            }
        }

        /*
        private NOATipo ParseNOATipo(int Entrada)
        {
            return (NOATipo)System.Enum.Parse(typeof(NOATipo), Entrada.ToString());
        }
        */
    }
}
