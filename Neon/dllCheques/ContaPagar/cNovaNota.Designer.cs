﻿namespace ContaPagar
{
    partial class cNovaNota
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cNovaNota));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.CNPJFormat = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.xtraTabTipo = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageNota = new DevExpress.XtraTab.XtraTabPage();
            this.ButImp = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpFOR2 = new DevExpress.XtraEditors.LookUpEdit();
            this.fornecedoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageRec = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.groupControlTomador = new DevExpress.XtraEditors.GroupControl();
            this.TXTContrato = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditCondominio = new DevExpress.XtraEditors.TextEdit();
            this.CNPJcond = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.groupControlNota = new DevExpress.XtraEditors.GroupControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.groupControlSevicos = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dNovaNota = new ContaPagar.dNovaNota();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookPLADES = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.dPLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colSPL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpSub = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.SubPLanoPLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.lookCTL = new DevExpress.XtraEditors.LookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lookUpEditPLA1 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditPLA2 = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CNPJFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabTipo)).BeginInit();
            this.xtraTabTipo.SuspendLayout();
            this.xtraTabPageNota.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).BeginInit();
            this.xtraTabPageRec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTomador)).BeginInit();
            this.groupControlTomador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCondominio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CNPJcond.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlNota)).BeginInit();
            this.groupControlNota.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSevicos)).BeginInit();
            this.groupControlSevicos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNovaNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookPLADES)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubPLanoPLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookCTL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // CNPJFormat
            // 
            this.CNPJFormat.Location = new System.Drawing.Point(104, 19);
            this.CNPJFormat.Name = "CNPJFormat";
            this.CNPJFormat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CNPJFormat.Properties.Appearance.Options.UseFont = true;
            this.CNPJFormat.Size = new System.Drawing.Size(361, 40);
            this.CNPJFormat.TabIndex = 0;
            this.CNPJFormat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CNPJFormat_KeyDown);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.xtraTabTipo);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 21);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(0, 152);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Emissor (Fornecedor / Prestador)";
            // 
            // xtraTabTipo
            // 
            this.xtraTabTipo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabTipo.Location = new System.Drawing.Point(1, 20);
            this.xtraTabTipo.Name = "xtraTabTipo";
            this.xtraTabTipo.SelectedTabPage = this.xtraTabPageNota;
            this.xtraTabTipo.Size = new System.Drawing.Size(0, 130);
            this.xtraTabTipo.TabIndex = 5;
            this.xtraTabTipo.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageNota,
            this.xtraTabPageRec});
            // 
            // xtraTabPageNota
            // 
            this.xtraTabPageNota.Controls.Add(this.ButImp);
            this.xtraTabPageNota.Controls.Add(this.lookUpFOR2);
            this.xtraTabPageNota.Controls.Add(this.labelControl3);
            this.xtraTabPageNota.Controls.Add(this.CNPJFormat);
            this.xtraTabPageNota.Name = "xtraTabPageNota";
            this.xtraTabPageNota.Size = new System.Drawing.Size(0, 102);
            this.xtraTabPageNota.Text = "xtraTabPageNota";
            // 
            // ButImp
            // 
            this.ButImp.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButImp.Appearance.Options.UseFont = true;
            this.ButImp.Enabled = false;
            this.ButImp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButImp.ImageOptions.Image")));
            this.ButImp.Location = new System.Drawing.Point(471, 8);
            this.ButImp.Name = "ButImp";
            this.ButImp.Size = new System.Drawing.Size(146, 50);
            this.ButImp.TabIndex = 33;
            this.ButImp.Text = "Abrir PDF";
            this.ButImp.Visible = false;
            this.ButImp.Click += new System.EventHandler(this.ButImp_Click);
            // 
            // lookUpFOR2
            // 
            this.lookUpFOR2.Location = new System.Drawing.Point(104, 64);
            this.lookUpFOR2.Name = "lookUpFOR2";
            this.lookUpFOR2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpFOR2.Properties.Appearance.Options.UseFont = true;
            this.lookUpFOR2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.lookUpFOR2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "CNPJ"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpFOR2.Properties.DataSource = this.fornecedoresBindingSource;
            this.lookUpFOR2.Properties.DisplayMember = "FRNNome";
            this.lookUpFOR2.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.lookUpFOR2.Properties.ValueMember = "FRN";
            this.lookUpFOR2.Size = new System.Drawing.Size(681, 30);
            this.lookUpFOR2.TabIndex = 32;
            this.lookUpFOR2.TabStop = false;
            this.lookUpFOR2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lookUpFOR2_KeyDown);
            // 
            // fornecedoresBindingSource
            // 
            this.fornecedoresBindingSource.DataMember = "FRNLookup";
            this.fornecedoresBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(15, 30);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(57, 23);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "CNPJ:";
            // 
            // xtraTabPageRec
            // 
            this.xtraTabPageRec.Controls.Add(this.labelControl7);
            this.xtraTabPageRec.Name = "xtraTabPageRec";
            this.xtraTabPageRec.Size = new System.Drawing.Size(1442, 102);
            this.xtraTabPageRec.Text = "xtraTabPageRec";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(15, 17);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(57, 23);
            this.labelControl7.TabIndex = 33;
            this.labelControl7.Text = "CNPJ:";
            // 
            // groupControlTomador
            // 
            this.groupControlTomador.Controls.Add(this.TXTContrato);
            this.groupControlTomador.Controls.Add(this.labelControl1);
            this.groupControlTomador.Controls.Add(this.textEditCondominio);
            this.groupControlTomador.Controls.Add(this.CNPJcond);
            this.groupControlTomador.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControlTomador.Enabled = false;
            this.groupControlTomador.Location = new System.Drawing.Point(0, 173);
            this.groupControlTomador.Name = "groupControlTomador";
            this.groupControlTomador.Size = new System.Drawing.Size(0, 137);
            this.groupControlTomador.TabIndex = 3;
            this.groupControlTomador.Text = "Tomador";
            // 
            // TXTContrato
            // 
            this.TXTContrato.Appearance.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTContrato.Appearance.ForeColor = System.Drawing.Color.Red;
            this.TXTContrato.Appearance.Options.UseFont = true;
            this.TXTContrato.Appearance.Options.UseForeColor = true;
            this.TXTContrato.Location = new System.Drawing.Point(509, 37);
            this.TXTContrato.Name = "TXTContrato";
            this.TXTContrato.Size = new System.Drawing.Size(181, 39);
            this.TXTContrato.TabIndex = 5;
            this.TXTContrato.Text = "CONTRATO";
            this.TXTContrato.Visible = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(17, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 23);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "CNPJ:";
            // 
            // textEditCondominio
            // 
            this.textEditCondominio.Location = new System.Drawing.Point(18, 84);
            this.textEditCondominio.Name = "textEditCondominio";
            this.textEditCondominio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditCondominio.Properties.Appearance.Options.UseFont = true;
            this.textEditCondominio.Properties.ReadOnly = true;
            this.textEditCondominio.Size = new System.Drawing.Size(770, 30);
            this.textEditCondominio.TabIndex = 3;
            // 
            // CNPJcond
            // 
            this.CNPJcond.Location = new System.Drawing.Point(107, 39);
            this.CNPJcond.Name = "CNPJcond";
            this.CNPJcond.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CNPJcond.Properties.Appearance.Options.UseFont = true;
            this.CNPJcond.Size = new System.Drawing.Size(361, 40);
            this.CNPJcond.TabIndex = 2;
            this.CNPJcond.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEdit4_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(239, 36);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(86, 23);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Emissão:";
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(332, 36);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(157, 26);
            this.dateEdit1.TabIndex = 2;
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(107, 36);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit1.Properties.Appearance.Options.UseFont = true;
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Size = new System.Drawing.Size(107, 26);
            this.spinEdit1.TabIndex = 1;
            this.spinEdit1.EditValueChanged += new System.EventHandler(this.spinEdit1_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(17, 36);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(83, 23);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Número:";
            // 
            // groupControlNota
            // 
            this.groupControlNota.Controls.Add(this.labelControl6);
            this.groupControlNota.Controls.Add(this.cCompet1);
            this.groupControlNota.Controls.Add(this.dateEdit1);
            this.groupControlNota.Controls.Add(this.labelControl2);
            this.groupControlNota.Controls.Add(this.labelControl4);
            this.groupControlNota.Controls.Add(this.spinEdit1);
            this.groupControlNota.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControlNota.Enabled = false;
            this.groupControlNota.Location = new System.Drawing.Point(0, 310);
            this.groupControlNota.Name = "groupControlNota";
            this.groupControlNota.Size = new System.Drawing.Size(0, 85);
            this.groupControlNota.TabIndex = 7;
            this.groupControlNota.Text = "Nota";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(509, 36);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(131, 23);
            this.labelControl6.TabIndex = 47;
            this.labelControl6.Text = "Competência:";
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.Appearance.Options.UseFont = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(671, 35);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = true;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 46;
            this.cCompet1.TabStop = false;
            this.cCompet1.Titulo = "Framework.objetosNeon.cCompet - Framework.objetosNeon.cCompet - ";
            // 
            // groupControlSevicos
            // 
            this.groupControlSevicos.Controls.Add(this.gridControl1);
            this.groupControlSevicos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlSevicos.Location = new System.Drawing.Point(0, 494);
            this.groupControlSevicos.Name = "groupControlSevicos";
            this.groupControlSevicos.Size = new System.Drawing.Size(0, 0);
            this.groupControlSevicos.TabIndex = 8;
            this.groupControlSevicos.Text = "Serviços";
            this.groupControlSevicos.Visible = false;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Servicos";
            this.gridControl1.DataSource = this.dNovaNota;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 19);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookPLADES,
            this.LookUpSub,
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(0, 0);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dNovaNota
            // 
            this.dNovaNota.DataSetName = "dNovaNota";
            this.dNovaNota.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn1,
            this.colPLA,
            this.colSPL,
            this.colServico,
            this.colValor});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 5;
            this.gridColumn2.Width = 30;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Conta";
            this.gridColumn1.ColumnEdit = this.repositoryItemLookPLADES;
            this.gridColumn1.FieldName = "PLA";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 186;
            // 
            // repositoryItemLookPLADES
            // 
            this.repositoryItemLookPLADES.AutoHeight = false;
            this.repositoryItemLookPLADES.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookPLADES.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "Código", 41, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descrição", 300, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookPLADES.DataSource = this.dPLAnocontasBindingSource;
            this.repositoryItemLookPLADES.DisplayMember = "PLADescricao";
            this.repositoryItemLookPLADES.Name = "repositoryItemLookPLADES";
            this.repositoryItemLookPLADES.PopupWidth = 400;
            this.repositoryItemLookPLADES.ValueMember = "PLA";
            // 
            // dPLAnocontasBindingSource
            // 
            this.dPLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.dPLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colPLA
            // 
            this.colPLA.Caption = "Plano";
            this.colPLA.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colPLA.FieldName = "PLA";
            this.colPLA.Name = "colPLA";
            this.colPLA.OptionsColumn.AllowEdit = false;
            this.colPLA.OptionsColumn.ReadOnly = true;
            this.colPLA.Visible = true;
            this.colPLA.VisibleIndex = 0;
            this.colPLA.Width = 120;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "Código", 41, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descrição", 300, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit1.DataSource = this.dPLAnocontasBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "PLA";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.PopupWidth = 400;
            this.repositoryItemLookUpEdit1.ValueMember = "PLA";
            // 
            // colSPL
            // 
            this.colSPL.Caption = "Classificação";
            this.colSPL.ColumnEdit = this.LookUpSub;
            this.colSPL.FieldName = "SPL";
            this.colSPL.Name = "colSPL";
            this.colSPL.OptionsColumn.AllowEdit = false;
            this.colSPL.OptionsColumn.ReadOnly = true;
            this.colSPL.Visible = true;
            this.colSPL.VisibleIndex = 2;
            this.colSPL.Width = 196;
            // 
            // LookUpSub
            // 
            this.LookUpSub.AutoHeight = false;
            this.LookUpSub.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpSub.DataSource = this.SubPLanoPLAnocontasBindingSource;
            this.LookUpSub.DisplayMember = "SPLDescricao";
            this.LookUpSub.Name = "LookUpSub";
            this.LookUpSub.ValueMember = "SPL";
            // 
            // SubPLanoPLAnocontasBindingSource
            // 
            this.SubPLanoPLAnocontasBindingSource.DataMember = "SubPLano";
            this.SubPLanoPLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colServico
            // 
            this.colServico.Caption = "Serviço";
            this.colServico.FieldName = "Servico";
            this.colServico.Name = "colServico";
            this.colServico.Visible = true;
            this.colServico.VisibleIndex = 3;
            this.colServico.Width = 326;
            // 
            // colValor
            // 
            this.colValor.Caption = "Valor";
            this.colValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colValor.FieldName = "Valor";
            this.colValor.Name = "colValor";
            this.colValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:n2}")});
            this.colValor.Visible = true;
            this.colValor.VisibleIndex = 4;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.lookCTL);
            this.groupControl2.Controls.Add(this.simpleButton1);
            this.groupControl2.Controls.Add(this.calcEdit1);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.lookUpEdit1);
            this.groupControl2.Controls.Add(this.comboBoxEdit1);
            this.groupControl2.Controls.Add(this.lookUpEditPLA1);
            this.groupControl2.Controls.Add(this.lookUpEditPLA2);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Enabled = false;
            this.groupControl2.Location = new System.Drawing.Point(0, 395);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(0, 99);
            this.groupControl2.TabIndex = 9;
            this.groupControl2.Text = "Classificação";
            // 
            // lookCTL
            // 
            this.lookCTL.Location = new System.Drawing.Point(596, 25);
            this.lookCTL.MenuManager = this.BarManager_F;
            this.lookCTL.Name = "lookCTL";
            this.lookCTL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookCTL.Properties.Appearance.Options.UseFont = true;
            this.lookCTL.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookCTL.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookCTL.Properties.DataSource = this.conTasLogicasBindingSource;
            this.lookCTL.Properties.DisplayMember = "CTLTitulo";
            this.lookCTL.Properties.ShowHeader = false;
            this.lookCTL.Properties.ValueMember = "CTL";
            this.lookCTL.Size = new System.Drawing.Size(192, 26);
            this.lookCTL.TabIndex = 50;
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = typeof(FrameworkProc.datasets.dContasLogicas);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(596, 54);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(192, 29);
            this.simpleButton1.TabIndex = 21;
            this.simpleButton1.TabStop = false;
            this.simpleButton1.Text = "Incluir outro serviço";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // calcEdit1
            // 
            this.calcEdit1.Location = new System.Drawing.Point(876, 25);
            this.calcEdit1.MenuManager = this.BarManager_F;
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEdit1.Properties.Appearance.Options.UseFont = true;
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Properties.Mask.EditMask = "n2";
            this.calcEdit1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEdit1.Size = new System.Drawing.Size(129, 26);
            this.calcEdit1.TabIndex = 4;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(812, 25);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(57, 23);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "Valor:";
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Enabled = false;
            this.lookUpEdit1.Location = new System.Drawing.Point(5, 57);
            this.lookUpEdit1.MenuManager = this.BarManager_F;
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLCodigo", "Código", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLDescricao", "Descrição", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLValorISS", "ISS (%)", 15, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEdit1.Properties.DataSource = this.SubPLanoPLAnocontasBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "Descritivo";
            this.lookUpEdit1.Properties.PopupWidth = 400;
            this.lookUpEdit1.Properties.ValueMember = "SPL";
            this.lookUpEdit1.Size = new System.Drawing.Size(209, 26);
            this.lookUpEdit1.TabIndex = 2;
            this.lookUpEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit1_ButtonClick);
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(220, 57);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus)});
            this.comboBoxEdit1.Properties.MaxLength = 50;
            this.comboBoxEdit1.Size = new System.Drawing.Size(370, 26);
            this.comboBoxEdit1.TabIndex = 3;
            this.comboBoxEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.comboBoxEdit1_ButtonClick);
            this.comboBoxEdit1.Validating += new System.ComponentModel.CancelEventHandler(this.comboBoxEdit1_Validating);
            // 
            // lookUpEditPLA1
            // 
            this.lookUpEditPLA1.Location = new System.Drawing.Point(5, 25);
            this.lookUpEditPLA1.Name = "lookUpEditPLA1";
            this.lookUpEditPLA1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditPLA1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditPLA1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditPLA1.Properties.DataSource = this.dPLAnocontasBindingSource;
            this.lookUpEditPLA1.Properties.DisplayMember = "PLA";
            this.lookUpEditPLA1.Properties.ValueMember = "PLA";
            this.lookUpEditPLA1.Size = new System.Drawing.Size(209, 26);
            this.lookUpEditPLA1.TabIndex = 0;
            this.lookUpEditPLA1.EditValueChanged += new System.EventHandler(this.lookUpEditPLA1_EditValueChanged);
            // 
            // lookUpEditPLA2
            // 
            this.lookUpEditPLA2.Location = new System.Drawing.Point(220, 25);
            this.lookUpEditPLA2.Name = "lookUpEditPLA2";
            this.lookUpEditPLA2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditPLA2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditPLA2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditPLA2.Properties.DataSource = this.dPLAnocontasBindingSource;
            this.lookUpEditPLA2.Properties.DisplayMember = "PLADescricao";
            this.lookUpEditPLA2.Properties.ValueMember = "PLA";
            this.lookUpEditPLA2.Size = new System.Drawing.Size(370, 26);
            this.lookUpEditPLA2.TabIndex = 1;
            this.lookUpEditPLA2.TabStop = false;
            this.lookUpEditPLA2.EditValueChanged += new System.EventHandler(this.lookUpEditPLA1_EditValueChanged);
            // 
            // cNovaNota
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.groupControlSevicos);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControlNota);
            this.Controls.Add(this.groupControlTomador);
            this.Controls.Add(this.groupControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cNovaNota";
            this.Size = new System.Drawing.Size(0, 0);
            this.Titulo = "Nova Nota";
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControlTomador, 0);
            this.Controls.SetChildIndex(this.groupControlNota, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.groupControlSevicos, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CNPJFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabTipo)).EndInit();
            this.xtraTabTipo.ResumeLayout(false);
            this.xtraTabPageNota.ResumeLayout(false);
            this.xtraTabPageNota.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).EndInit();
            this.xtraTabPageRec.ResumeLayout(false);
            this.xtraTabPageRec.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTomador)).EndInit();
            this.groupControlTomador.ResumeLayout(false);
            this.groupControlTomador.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCondominio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CNPJcond.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlNota)).EndInit();
            this.groupControlNota.ResumeLayout(false);
            this.groupControlNota.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSevicos)).EndInit();
            this.groupControlSevicos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNovaNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookPLADES)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubPLanoPLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookCTL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit CNPJFormat;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GroupControl groupControlTomador;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditCondominio;
        private DevExpress.XtraEditors.TextEdit CNPJcond;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl TXTContrato;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.GroupControl groupControlNota;
        private DevExpress.XtraEditors.GroupControl groupControlSevicos;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private dNovaNota dNovaNota;
        private DevExpress.XtraGrid.Columns.GridColumn colPLA;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource dPLAnocontasBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSPL;
        private DevExpress.XtraGrid.Columns.GridColumn colServico;
        private DevExpress.XtraGrid.Columns.GridColumn colValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookPLADES;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpSub;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA2;
        private System.Windows.Forms.BindingSource SubPLanoPLAnocontasBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraTab.XtraTabControl xtraTabTipo;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageNota;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRec;
        private System.Windows.Forms.BindingSource fornecedoresBindingSource;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LookUpEdit lookUpFOR2;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookCTL;
        private DevExpress.XtraEditors.SimpleButton ButImp;
    }
}
