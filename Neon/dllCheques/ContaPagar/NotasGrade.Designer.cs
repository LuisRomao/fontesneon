namespace ContaPagar
{
    partial class NotasGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotasGrade));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPAGValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TipoPAG = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCHENumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dNOtAsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colNOANumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADataEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpFRN = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.fornecedoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colNOATotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colNOAServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Status = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colNOATipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Tipo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colNOAA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colNOAI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAObs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colNOAAguardaNota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pAGamentosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.DataIni = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.colNOA_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit1CODCON = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.dCondominiosAtivosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOACompet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonRec = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.colNOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpFRN_CNPJ = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoPAG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNOtAsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpFRN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGamentosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataIni.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataIni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit1CODCON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosAtivosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpFRN_CNPJ)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.ButtonRec);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.DataIni);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Size = new System.Drawing.Size(0, 83);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton4, 0);
            this.panelControl1.Controls.SetChildIndex(this.DataIni, 0);
            this.panelControl1.Controls.SetChildIndex(this.labelControl1, 0);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton1, 0);
            this.panelControl1.Controls.SetChildIndex(this.ButtonRec, 0);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton2, 0);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton3, 0);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.dNOtAsBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            gridLevelNode1.LevelTemplate = this.gridView1;
            gridLevelNode1.RelationName = "FK_PAGamentos_NOtAs";
            this.GridControl_F.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.GridControl_F.Location = new System.Drawing.Point(0, 83);
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpFRN,
            this.Status,
            this.repositoryItemLookUpPLA,
            this.Tipo,
            this.repositoryItemLookUpUSU,
            this.TipoPAG,
            this.repositoryItemImageComboBox1,
            this.LookUpEdit1CODCON,
            this.repositoryItemButtonEdit1,
            this.repositoryItemCalcEdit1,
            this.repositoryItemMemoExEdit1,
            this.LookUpFRN_CNPJ});
            this.GridControl_F.Size = new System.Drawing.Size(0, 0);
            this.GridControl_F.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.GridView_F.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.GridView_F.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.GridView_F.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.GridView_F.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseFont = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.GridView_F.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.GridView_F.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colNOANumero,
            this.colNOADataEmissao,
            this.colNOA_FRN,
            this.colNOATotal,
            this.colNOAServico,
            this.colNOA_PLA,
            this.colNOAStatus,
            this.colNOATipo,
            this.colNOAA_USU,
            this.colNOAI_USU,
            this.colNOADATAA,
            this.colNOADATAI,
            this.colNOAObs,
            this.colNOAAguardaNota,
            this.colNOA_CON,
            this.gridColumn2,
            this.colNOACompet,
            this.colNOA,
            this.gridColumn4});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.AutoExpandAllGroups = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsDetail.ShowDetailTabs = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNOADATAI, DevExpress.Data.ColumnSortOrder.Descending)});
            this.GridView_F.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridView_F_RowCellStyle);
            this.GridView_F.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.GridView_F_CustomColumnDisplayText);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(57)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(57)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.FilterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.gridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(57)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(57)))));
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(158)))), ((int)(((byte)(64)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(247)))), ((int)(((byte)(230)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(253)))), ((int)(((byte)(246)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(247)))), ((int)(((byte)(230)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(139)))), ((int)(((byte)(41)))));
            this.gridView1.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(139)))), ((int)(((byte)(41)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPAGValor,
            this.colPAGVencimento,
            this.colPAGTipo,
            this.colCHENumero,
            this.colPAGN,
            this.colCHEStatus,
            this.gridColumn3});
            this.gridView1.GridControl = this.GridControl_F;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colPAGValor
            // 
            this.colPAGValor.Caption = "Valor";
            this.colPAGValor.DisplayFormat.FormatString = "n2";
            this.colPAGValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPAGValor.FieldName = "PAGValor";
            this.colPAGValor.Name = "colPAGValor";
            this.colPAGValor.OptionsColumn.ReadOnly = true;
            this.colPAGValor.Visible = true;
            this.colPAGValor.VisibleIndex = 1;
            this.colPAGValor.Width = 96;
            // 
            // colPAGVencimento
            // 
            this.colPAGVencimento.Caption = "Vencimento";
            this.colPAGVencimento.DisplayFormat.FormatString = "d";
            this.colPAGVencimento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPAGVencimento.FieldName = "PAGVencimento";
            this.colPAGVencimento.Name = "colPAGVencimento";
            this.colPAGVencimento.OptionsColumn.ReadOnly = true;
            this.colPAGVencimento.Visible = true;
            this.colPAGVencimento.VisibleIndex = 0;
            this.colPAGVencimento.Width = 87;
            // 
            // colPAGTipo
            // 
            this.colPAGTipo.Caption = "Tipo";
            this.colPAGTipo.ColumnEdit = this.TipoPAG;
            this.colPAGTipo.FieldName = "PAGTipo";
            this.colPAGTipo.Name = "colPAGTipo";
            this.colPAGTipo.OptionsColumn.ReadOnly = true;
            this.colPAGTipo.Visible = true;
            this.colPAGTipo.VisibleIndex = 2;
            this.colPAGTipo.Width = 89;
            // 
            // TipoPAG
            // 
            this.TipoPAG.AutoHeight = false;
            this.TipoPAG.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TipoPAG.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cheque", 0, -1)});
            this.TipoPAG.Name = "TipoPAG";
            // 
            // colCHENumero
            // 
            this.colCHENumero.Caption = "Cheque";
            this.colCHENumero.FieldName = "CHENumero";
            this.colCHENumero.Name = "colCHENumero";
            this.colCHENumero.OptionsColumn.ReadOnly = true;
            this.colCHENumero.Visible = true;
            this.colCHENumero.VisibleIndex = 4;
            // 
            // colPAGN
            // 
            this.colPAGN.Caption = "Parcela";
            this.colPAGN.FieldName = "PAGN";
            this.colPAGN.Name = "colPAGN";
            this.colPAGN.OptionsColumn.ReadOnly = true;
            this.colPAGN.Visible = true;
            this.colPAGN.VisibleIndex = 3;
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.Caption = "Status";
            this.colCHEStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.OptionsColumn.ReadOnly = true;
            this.colCHEStatus.Visible = true;
            this.colCHEStatus.VisibleIndex = 5;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ShowCaption = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 6;
            this.gridColumn3.Width = 30;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // dNOtAsBindingSource
            // 
            this.dNOtAsBindingSource.DataMember = "NOtAs";
            this.dNOtAsBindingSource.DataSource = typeof(ContaPagarProc.dNOtAs);
            // 
            // colNOANumero
            // 
            this.colNOANumero.Caption = "Nota";
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.OptionsColumn.ReadOnly = true;
            this.colNOANumero.Visible = true;
            this.colNOANumero.VisibleIndex = 7;
            this.colNOANumero.Width = 76;
            // 
            // colNOADataEmissao
            // 
            this.colNOADataEmissao.Caption = "Emiss�o";
            this.colNOADataEmissao.FieldName = "NOADataEmissao";
            this.colNOADataEmissao.Name = "colNOADataEmissao";
            this.colNOADataEmissao.OptionsColumn.ReadOnly = true;
            this.colNOADataEmissao.Visible = true;
            this.colNOADataEmissao.VisibleIndex = 8;
            // 
            // colNOA_FRN
            // 
            this.colNOA_FRN.Caption = "Fornecedor";
            this.colNOA_FRN.ColumnEdit = this.LookUpFRN;
            this.colNOA_FRN.FieldName = "NOA_FRN";
            this.colNOA_FRN.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colNOA_FRN.Name = "colNOA_FRN";
            this.colNOA_FRN.OptionsColumn.AllowEdit = false;
            this.colNOA_FRN.OptionsColumn.ReadOnly = true;
            this.colNOA_FRN.Visible = true;
            this.colNOA_FRN.VisibleIndex = 6;
            this.colNOA_FRN.Width = 172;
            // 
            // LookUpFRN
            // 
            this.LookUpFRN.AutoHeight = false;
            this.LookUpFRN.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpFRN.DataSource = this.fornecedoresBindingSource;
            this.LookUpFRN.DisplayMember = "FRNNome";
            this.LookUpFRN.Name = "LookUpFRN";
            this.LookUpFRN.NullText = " --";
            this.LookUpFRN.ValueMember = "FRN";
            // 
            // fornecedoresBindingSource
            // 
            this.fornecedoresBindingSource.DataMember = "FRNLookup";
            this.fornecedoresBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // colNOATotal
            // 
            this.colNOATotal.Caption = "Total";
            this.colNOATotal.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colNOATotal.DisplayFormat.FormatString = "n2";
            this.colNOATotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNOATotal.FieldName = "NOATotal";
            this.colNOATotal.Name = "colNOATotal";
            this.colNOATotal.OptionsColumn.ReadOnly = true;
            this.colNOATotal.Visible = true;
            this.colNOATotal.VisibleIndex = 12;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colNOAServico
            // 
            this.colNOAServico.Caption = "Servi�o";
            this.colNOAServico.FieldName = "NOAServico";
            this.colNOAServico.Name = "colNOAServico";
            this.colNOAServico.OptionsColumn.ReadOnly = true;
            this.colNOAServico.Visible = true;
            this.colNOAServico.VisibleIndex = 11;
            this.colNOAServico.Width = 207;
            // 
            // colNOA_PLA
            // 
            this.colNOA_PLA.Caption = "Conta";
            this.colNOA_PLA.FieldName = "NOA_PLA";
            this.colNOA_PLA.Name = "colNOA_PLA";
            this.colNOA_PLA.OptionsColumn.ReadOnly = true;
            this.colNOA_PLA.Visible = true;
            this.colNOA_PLA.VisibleIndex = 9;
            this.colNOA_PLA.Width = 67;
            // 
            // colNOAStatus
            // 
            this.colNOAStatus.Caption = "Status";
            this.colNOAStatus.ColumnEdit = this.Status;
            this.colNOAStatus.FieldName = "NOAStatus";
            this.colNOAStatus.Name = "colNOAStatus";
            this.colNOAStatus.OptionsColumn.AllowEdit = false;
            this.colNOAStatus.OptionsColumn.ReadOnly = true;
            this.colNOAStatus.Visible = true;
            this.colNOAStatus.VisibleIndex = 2;
            this.colNOAStatus.Width = 91;
            // 
            // Status
            // 
            this.Status.AutoHeight = false;
            this.Status.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Status.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cadastrada", 0, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Incompleta", 1, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Paga", 2, -1)});
            this.Status.Name = "Status";
            // 
            // colNOATipo
            // 
            this.colNOATipo.Caption = "Tipo";
            this.colNOATipo.ColumnEdit = this.Tipo;
            this.colNOATipo.FieldName = "NOATipo";
            this.colNOATipo.Name = "colNOATipo";
            this.colNOATipo.OptionsColumn.ReadOnly = true;
            this.colNOATipo.Visible = true;
            this.colNOATipo.VisibleIndex = 3;
            this.colNOATipo.Width = 112;
            // 
            // Tipo
            // 
            this.Tipo.AutoHeight = false;
            this.Tipo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Tipo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nota", 0, -1)});
            this.Tipo.Name = "Tipo";
            this.Tipo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.Tipo_ButtonClick);
            // 
            // colNOAA_USU
            // 
            this.colNOAA_USU.Caption = "Altera��o";
            this.colNOAA_USU.ColumnEdit = this.repositoryItemLookUpUSU;
            this.colNOAA_USU.FieldName = "NOAA_USU";
            this.colNOAA_USU.Name = "colNOAA_USU";
            this.colNOAA_USU.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemLookUpUSU
            // 
            this.repositoryItemLookUpUSU.AutoHeight = false;
            this.repositoryItemLookUpUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpUSU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USU Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpUSU.DataSource = this.uSUariosBindingSource;
            this.repositoryItemLookUpUSU.DisplayMember = "USUNome";
            this.repositoryItemLookUpUSU.Name = "repositoryItemLookUpUSU";
            this.repositoryItemLookUpUSU.ShowHeader = false;
            this.repositoryItemLookUpUSU.ValueMember = "USU";
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colNOAI_USU
            // 
            this.colNOAI_USU.Caption = "Cadastro";
            this.colNOAI_USU.ColumnEdit = this.repositoryItemLookUpUSU;
            this.colNOAI_USU.FieldName = "NOAI_USU";
            this.colNOAI_USU.Name = "colNOAI_USU";
            this.colNOAI_USU.OptionsColumn.AllowEdit = false;
            this.colNOAI_USU.OptionsColumn.ReadOnly = true;
            this.colNOAI_USU.Visible = true;
            this.colNOAI_USU.VisibleIndex = 4;
            this.colNOAI_USU.Width = 123;
            // 
            // colNOADATAA
            // 
            this.colNOADATAA.Caption = "Data Altera��o";
            this.colNOADATAA.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm";
            this.colNOADATAA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNOADATAA.FieldName = "NOADATAA";
            this.colNOADATAA.Name = "colNOADATAA";
            this.colNOADATAA.OptionsColumn.ReadOnly = true;
            // 
            // colNOADATAI
            // 
            this.colNOADATAI.Caption = "Data de Cadastro";
            this.colNOADATAI.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm";
            this.colNOADATAI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNOADATAI.FieldName = "NOADATAI";
            this.colNOADATAI.Name = "colNOADATAI";
            this.colNOADATAI.OptionsColumn.AllowEdit = false;
            this.colNOADATAI.OptionsColumn.ReadOnly = true;
            this.colNOADATAI.Visible = true;
            this.colNOADATAI.VisibleIndex = 1;
            this.colNOADATAI.Width = 122;
            // 
            // colNOAObs
            // 
            this.colNOAObs.Caption = "Obs";
            this.colNOAObs.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colNOAObs.FieldName = "NOAObs";
            this.colNOAObs.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colNOAObs.Name = "colNOAObs";
            this.colNOAObs.OptionsColumn.AllowMove = false;
            this.colNOAObs.OptionsColumn.AllowSize = false;
            this.colNOAObs.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colNOAObs.OptionsColumn.ReadOnly = true;
            this.colNOAObs.OptionsFilter.AllowFilter = false;
            this.colNOAObs.Visible = true;
            this.colNOAObs.VisibleIndex = 14;
            this.colNOAObs.Width = 30;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            // 
            // colNOAAguardaNota
            // 
            this.colNOAAguardaNota.Caption = "Aguardando a Nota";
            this.colNOAAguardaNota.FieldName = "NOAAguardaNota";
            this.colNOAAguardaNota.Name = "colNOAAguardaNota";
            this.colNOAAguardaNota.OptionsColumn.ReadOnly = true;
            this.colNOAAguardaNota.Width = 129;
            // 
            // pAGamentosBindingSource
            // 
            this.pAGamentosBindingSource.DataMember = "FK_PAGamentos_NOtAs";
            this.pAGamentosBindingSource.DataSource = this.dNOtAsBindingSource;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Descri��o da conta";
            this.gridColumn1.ColumnEdit = this.repositoryItemLookUpPLA;
            this.gridColumn1.FieldName = "NOA_PLA";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 10;
            this.gridColumn1.Width = 159;
            // 
            // repositoryItemLookUpPLA
            // 
            this.repositoryItemLookUpPLA.AutoHeight = false;
            this.repositoryItemLookUpPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpPLA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLA Descricao", 77, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpPLA.DataSource = this.pLAnocontasBindingSource;
            this.repositoryItemLookUpPLA.DisplayMember = "PLADescricao";
            this.repositoryItemLookUpPLA.Name = "repositoryItemLookUpPLA";
            this.repositoryItemLookUpPLA.ShowHeader = false;
            this.repositoryItemLookUpPLA.ValueMember = "PLA";
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(175, 45);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(138, 23);
            this.simpleButton4.TabIndex = 4;
            this.simpleButton4.Text = "Atualizar Fornecedores";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // DataIni
            // 
            this.DataIni.EditValue = null;
            this.DataIni.Location = new System.Drawing.Point(72, 48);
            this.DataIni.MenuManager = this.BarManager_F;
            this.DataIni.Name = "DataIni";
            this.DataIni.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataIni.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DataIni.Size = new System.Drawing.Size(97, 20);
            this.DataIni.TabIndex = 5;
            this.DataIni.EditValueChanged += new System.EventHandler(this.DataIni_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 51);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Data Inicial:";
            // 
            // colNOA_CON
            // 
            this.colNOA_CON.Caption = "Condom�nio";
            this.colNOA_CON.ColumnEdit = this.LookUpEdit1CODCON;
            this.colNOA_CON.FieldName = "NOA_CON";
            this.colNOA_CON.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colNOA_CON.Name = "colNOA_CON";
            this.colNOA_CON.OptionsColumn.AllowEdit = false;
            this.colNOA_CON.OptionsColumn.ReadOnly = true;
            this.colNOA_CON.Visible = true;
            this.colNOA_CON.VisibleIndex = 0;
            this.colNOA_CON.Width = 78;
            // 
            // LookUpEdit1CODCON
            // 
            this.LookUpEdit1CODCON.AutoHeight = false;
            this.LookUpEdit1CODCON.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit1CODCON.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "C�digo", 10, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "Nome", 62, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCnpj", "CNPJ", 30, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.LookUpEdit1CODCON.DataSource = this.dCondominiosAtivosBindingSource;
            this.LookUpEdit1CODCON.DisplayMember = "CONCodigo";
            this.LookUpEdit1CODCON.Name = "LookUpEdit1CODCON";
            this.LookUpEdit1CODCON.PopupWidth = 600;
            this.LookUpEdit1CODCON.ValueMember = "CON";
            // 
            // dCondominiosAtivosBindingSource
            // 
            this.dCondominiosAtivosBindingSource.DataMember = "CONDOMINIOS";
            this.dCondominiosAtivosBindingSource.DataSource = typeof(FrameworkProc.datasets.dCondominiosAtivos);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 15;
            this.gridColumn2.Width = 30;
            // 
            // colNOACompet
            // 
            this.colNOACompet.Caption = "Comp.";
            this.colNOACompet.FieldName = "NOACompet";
            this.colNOACompet.Name = "colNOACompet";
            this.colNOACompet.OptionsColumn.AllowEdit = false;
            this.colNOACompet.OptionsColumn.ReadOnly = true;
            this.colNOACompet.Visible = true;
            this.colNOACompet.VisibleIndex = 13;
            this.colNOACompet.Width = 60;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = global::ContaPagar.Properties.Resources.nfeicone;
            this.simpleButton1.Location = new System.Drawing.Point(560, 0);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(117, 78);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "N.F.";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // ButtonRec
            // 
            this.ButtonRec.Enabled = false;
            this.ButtonRec.ImageOptions.Image = global::ContaPagar.Properties.Resources.Recibo;
            this.ButtonRec.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.ButtonRec.Location = new System.Drawing.Point(683, 0);
            this.ButtonRec.Name = "ButtonRec";
            this.ButtonRec.Size = new System.Drawing.Size(117, 78);
            this.ButtonRec.TabIndex = 8;
            this.ButtonRec.Text = "Recibo";
            this.ButtonRec.Click += new System.EventHandler(this.ButtonRec_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(320, 15);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(234, 23);
            this.simpleButton2.TabIndex = 9;
            this.simpleButton2.Text = "Calcular";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // colNOA
            // 
            this.colNOA.FieldName = "NOA";
            this.colNOA.Name = "colNOA";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(1042, 14);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 10;
            this.simpleButton3.Text = "Corre��o";
            this.simpleButton3.Visible = false;
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "CNPJ/CPF";
            this.gridColumn4.ColumnEdit = this.LookUpFRN_CNPJ;
            this.gridColumn4.FieldName = "copiaNOA_FRN";
            this.gridColumn4.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 78;
            // 
            // LookUpFRN_CNPJ
            // 
            this.LookUpFRN_CNPJ.AutoHeight = false;
            this.LookUpFRN_CNPJ.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpFRN_CNPJ.DataSource = this.fornecedoresBindingSource;
            this.LookUpFRN_CNPJ.DisplayMember = "FRNCnpj";
            this.LookUpFRN_CNPJ.Name = "LookUpFRN_CNPJ";
            this.LookUpFRN_CNPJ.NullText = " -- ?? --";
            this.LookUpFRN_CNPJ.ValueMember = "FRN";
            // 
            // NotasGrade
            // 
            this.Autofill = false;
            this.BindingSourcePrincipal = this.dNOtAsBindingSource;
            this.ComponenteCampos = typeof(ContaPagar.cNotasCampos);
            this.dataLayout = new System.DateTime(2012, 5, 17, 0, 0, 0, 0);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "NotasGrade";
            this.NivelCONOculto = 1;
            this.Size = new System.Drawing.Size(0, 0);
            this.Titulo = "Notas";
            this.CondominioAlterado += new System.EventHandler(this.NotasGrade_CondominioAlterado);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoPAG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNOtAsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpFRN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGamentosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataIni.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataIni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit1CODCON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosAtivosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpFRN_CNPJ)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource dNOtAsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADataEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_FRN;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATipo;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAA_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADATAA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAObs;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAAguardaNota;
        private System.Windows.Forms.BindingSource pAGamentosBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpFRN;
        private System.Windows.Forms.BindingSource fornecedoresBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox Status;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpPLA;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox Tipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpUSU;
        private System.Windows.Forms.BindingSource uSUariosBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGTipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox TipoPAG;
        private DevExpress.XtraGrid.Columns.GridColumn colCHENumero;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGN;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit DataIni;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_CON;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit1CODCON;
        private System.Windows.Forms.BindingSource dCondominiosAtivosBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colNOACompet;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton ButtonRec;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpFRN_CNPJ;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
    }
}
