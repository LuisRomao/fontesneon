using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Framework;
using VirEnumeracoesNeon;

namespace ContaPagar.NotaMae
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cNotaMae : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor padrao. Usar com paremetros
        /// </summary>
        public cNotaMae()
        {
            InitializeComponent();
        }
         
        /*
        public cNotaMae(dNOtAs.NOtAsRow rowModelo)
        {
            InitializeComponent();
            dNotaMae.EnforceConstraints = false;
            dNotaMae.NOtAsTableAdapter.Fill(dNotaMae.NOtAs,rowModelo.NOA_CON, rowModelo.NOANumero, rowModelo.NOA_FRN);
            rowBase = dNotaMae.NOtAs.NewNOtAsRow();
            rowBase.NOA = -1;
            rowBase.NOA_PLA = rowModelo.NOA_PLA;
            rowBase.NOAServico = rowModelo.NOAServico;
            rowBase.NOATotal = rowModelo.NOATotal;
            dNotaMae.NOtAs.AddNOtAsRow(rowBase);
            AjustaTitulo();
        }
        */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="FRN"></param>
        /// <param name="Nota"></param>
        public cNotaMae(int CON, int FRN, int Nota)
        {
            InitializeComponent();
            dNotaMae.NOtAsTableAdapter.Fill(dNotaMae.NOtAs, CON, Nota, FRN);
            rowBase = dNotaMae.NOtAs[0];
            AjustaTitulo();
        }

        private dNotaMae.NOtAsRow rowBase;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NOA"></param>
        public cNotaMae(int NOA)
        {
            InitializeComponent();
            rowBase = dNotaMae.NOtAsTableAdapter.GetDataByNOA(NOA)[0];
            dNotaMae.NOtAsTableAdapter.Fill(dNotaMae.NOtAs, rowBase.NOA_CON, rowBase.NOANumero, rowBase.NOA_FRN);
            AjustaTitulo();
        }

        private void AjustaTitulo()
        {
            LTitulo.Text = string.Format("Nota: {0}", rowBase.NOANumero);
            LTitulo2.Text = string.Format("Fornecedor: {0} {1} ({2})\r\nData:{3:dd/MM/yyyy}", rowBase.FRNCnpj, rowBase.FRNNome, rowBase.FRNFantasia, rowBase.NOADataEmissao);
            pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas;
            
            Enumeracoes.VirEnumNOAStatus.CarregaEditorDaGrid(colNOAStatus);
            //dNotaMae.EnforceConstraints = false;
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dNotaMae.NOtAsRow rowFoco = (dNotaMae.NOtAsRow)gridView1.GetDataRow(e.FocusedRowHandle);
            bool Editavel = true;
            if (rowFoco != null)
                Editavel = rowFoco.RowState != DataRowState.Unchanged; //rowFoco.NOA > 0;// (rowFoco.IsNOA_FRNNull());
            colNOAObs.OptionsColumn.ReadOnly = colNOATotal.OptionsColumn.ReadOnly = colNOA_PLA.OptionsColumn.ReadOnly = colNOAServico.OptionsColumn.ReadOnly = !Editavel;              
        }

        private int NOANovas = -1;

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            dNotaMae.NOtAsRow rowFoco = (dNotaMae.NOtAsRow)gridView1.GetDataRow(e.RowHandle);
            rowFoco.NOA = NOANovas--;
            rowFoco.NOA_CON = rowBase.NOA_CON;
            rowFoco.NOA_FRN = rowBase.NOA_FRN;
            rowFoco.NOADataEmissao = rowBase.NOADataEmissao;
            rowFoco.NOANumero = rowBase.NOANumero;
            rowFoco.NOAStatus = (int)NOAStatus.Parcial;
            rowFoco.FRNCnpj = rowBase.FRNCnpj;
            rowFoco.FRNFantasia = rowBase.FRNFantasia;
            rowFoco.FRNNome = rowBase.FRNNome;
            rowFoco.USUNome = "NOVA";
        }
        
        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dNotaMae.NOtAsRow rowPintar = (dNotaMae.NOtAsRow)gridView1.GetDataRow(e.RowHandle);
            if (rowPintar == null)
                e.Appearance.BackColor = Color.FromArgb(255, 240, 240);
            else if (rowPintar.NOA < 0)
                e.Appearance.BackColor = Color.Yellow;
            //else if (rowPintar.NOA == -1)
            //    e.Appearance.BackColor = Color.Lime;
            else
                e.Appearance.BackColor = Color.FromArgb(240, 240, 240); ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
           
            if (Validate(false))
                base.FechaTela(Resultado);
        }

    }
}
