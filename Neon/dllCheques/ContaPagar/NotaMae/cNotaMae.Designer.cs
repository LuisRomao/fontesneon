namespace ContaPagar.NotaMae
{
    partial class cNotaMae
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cNotaMae));
            this.LTitulo = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.LTitulo2 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dNotaMaeBindingSource = new System.Windows.Forms.BindingSource();
            this.dNotaMae = new ContaPagar.NotaMae.dNotaMae();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNOATotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource();
            this.colNOAStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colNOADATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAObs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUSUNome = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNotaMaeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNotaMae)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // LTitulo
            // 
            this.LTitulo.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LTitulo.Appearance.ForeColor = System.Drawing.Color.Red;
            this.LTitulo.Appearance.Options.UseFont = true;
            this.LTitulo.Appearance.Options.UseForeColor = true;
            this.LTitulo.Location = new System.Drawing.Point(5, 6);
            this.LTitulo.Name = "LTitulo";
            this.LTitulo.Size = new System.Drawing.Size(117, 25);
            this.LTitulo.TabIndex = 4;
            this.LTitulo.Text = "Nota: 0000";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.LTitulo2);
            this.panelControl1.Controls.Add(this.LTitulo);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1536, 91);
            this.panelControl1.TabIndex = 5;
            // 
            // LTitulo2
            // 
            this.LTitulo2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LTitulo2.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.LTitulo2.Appearance.Options.UseFont = true;
            this.LTitulo2.Appearance.Options.UseForeColor = true;
            this.LTitulo2.Location = new System.Drawing.Point(31, 37);
            this.LTitulo2.Name = "LTitulo2";
            this.LTitulo2.Size = new System.Drawing.Size(114, 46);
            this.LTitulo2.TabIndex = 5;
            this.LTitulo2.Text = "Fornecedor: \r\nData:";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "NOtAs";
            this.gridControl1.DataSource = this.dNotaMaeBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 126);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemLookPLA});
            this.gridControl1.Size = new System.Drawing.Size(1536, 681);
            this.gridControl1.TabIndex = 6;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dNotaMaeBindingSource
            // 
            this.dNotaMaeBindingSource.DataSource = this.dNotaMae;
            this.dNotaMaeBindingSource.Position = 0;
            // 
            // dNotaMae
            // 
            this.dNotaMae.DataSetName = "dNotaMae";
            this.dNotaMae.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNOATotal,
            this.colNOAServico,
            this.colNOA_PLA,
            this.colNOAStatus,
            this.colNOADATAI,
            this.colNOAObs,
            this.colUSUNome});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.NewItemRowText = "Incluir novas notas";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // colNOATotal
            // 
            this.colNOATotal.Caption = "Valor";
            this.colNOATotal.DisplayFormat.FormatString = "n2";
            this.colNOATotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNOATotal.FieldName = "NOATotal";
            this.colNOATotal.Name = "colNOATotal";
            this.colNOATotal.OptionsColumn.FixedWidth = true;
            this.colNOATotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NOATotal", "{0:n2}")});
            this.colNOATotal.Visible = true;
            this.colNOATotal.VisibleIndex = 2;
            // 
            // colNOAServico
            // 
            this.colNOAServico.Caption = "Servi�o";
            this.colNOAServico.FieldName = "NOAServico";
            this.colNOAServico.Name = "colNOAServico";
            this.colNOAServico.Visible = true;
            this.colNOAServico.VisibleIndex = 1;
            this.colNOAServico.Width = 436;
            // 
            // colNOA_PLA
            // 
            this.colNOA_PLA.Caption = "Plano";
            this.colNOA_PLA.ColumnEdit = this.repositoryItemLookPLA;
            this.colNOA_PLA.FieldName = "NOA_PLA";
            this.colNOA_PLA.Name = "colNOA_PLA";
            this.colNOA_PLA.OptionsColumn.FixedWidth = true;
            this.colNOA_PLA.Visible = true;
            this.colNOA_PLA.VisibleIndex = 0;
            // 
            // repositoryItemLookPLA
            // 
            this.repositoryItemLookPLA.AutoHeight = false;
            this.repositoryItemLookPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookPLA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 41, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLA Descricao", 77, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookPLA.DataSource = this.pLAnocontasBindingSource;
            this.repositoryItemLookPLA.DisplayMember = "PLA";
            this.repositoryItemLookPLA.Name = "repositoryItemLookPLA";
            this.repositoryItemLookPLA.NullText = "n�o definido";
            this.repositoryItemLookPLA.ShowHeader = false;
            this.repositoryItemLookPLA.ValueMember = "PLA";
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colNOAStatus
            // 
            this.colNOAStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colNOAStatus.FieldName = "NOAStatus";
            this.colNOAStatus.Name = "colNOAStatus";
            this.colNOAStatus.OptionsColumn.FixedWidth = true;
            this.colNOAStatus.OptionsColumn.ReadOnly = true;
            this.colNOAStatus.OptionsColumn.ShowCaption = false;
            this.colNOAStatus.Visible = true;
            this.colNOAStatus.VisibleIndex = 3;
            this.colNOAStatus.Width = 68;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colNOADATAI
            // 
            this.colNOADATAI.Caption = "Cadastro";
            this.colNOADATAI.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.colNOADATAI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNOADATAI.FieldName = "NOADATAI";
            this.colNOADATAI.Name = "colNOADATAI";
            this.colNOADATAI.OptionsColumn.FixedWidth = true;
            this.colNOADATAI.OptionsColumn.ReadOnly = true;
            this.colNOADATAI.Visible = true;
            this.colNOADATAI.VisibleIndex = 4;
            this.colNOADATAI.Width = 82;
            // 
            // colNOAObs
            // 
            this.colNOAObs.Caption = "Obs";
            this.colNOAObs.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colNOAObs.FieldName = "NOAObs";
            this.colNOAObs.Name = "colNOAObs";
            this.colNOAObs.OptionsColumn.FixedWidth = true;
            this.colNOAObs.Visible = true;
            this.colNOAObs.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            // 
            // colUSUNome
            // 
            this.colUSUNome.FieldName = "USUNome";
            this.colUSUNome.Name = "colUSUNome";
            this.colUSUNome.OptionsColumn.FixedWidth = true;
            this.colUSUNome.OptionsColumn.ReadOnly = true;
            this.colUSUNome.OptionsColumn.ShowCaption = false;
            this.colUSUNome.Visible = true;
            this.colUSUNome.VisibleIndex = 5;
            this.colUSUNome.Width = 106;
            // 
            // cNotaMae
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cNotaMae";
            this.Size = new System.Drawing.Size(1536, 807);
            this.Titulo = "Nota m�e";
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNotaMaeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNotaMae)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl LTitulo;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dNotaMaeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAObs;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUNome;
        private DevExpress.XtraEditors.LabelControl LTitulo2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookPLA;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        /// <summary>
        /// 
        /// </summary>
        public dNotaMae dNotaMae;
    }
}
