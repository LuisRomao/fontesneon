﻿namespace ContaPagar.NotaMae {


    partial class dNotaMae
    {
        private static dNotaMaeTableAdapters.NOtAsTableAdapter nOtAsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOtAs
        /// </summary>
        public static dNotaMaeTableAdapters.NOtAsTableAdapter NOtAsTableAdapter
        {
            get
            {
                if (nOtAsTableAdapter == null)
                {
                    nOtAsTableAdapter = new dNotaMaeTableAdapters.NOtAsTableAdapter();
                    nOtAsTableAdapter.TrocarStringDeConexao();
                };
                return nOtAsTableAdapter;
            }
        }
    }
}
