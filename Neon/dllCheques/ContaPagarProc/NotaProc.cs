﻿using System;
using System.Collections.Generic;
using VirEnumeracoesNeon;
using Framework.objetosNeon;
using FrameworkProc;
using VirMSSQL;
using dllChequesProc;
using System.Data;
using CompontesBasicosProc;
using System.Collections;



namespace ContaPagarProc
{
    /// <summary>
    /// 
    /// </summary>
    public class NotaProc:IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                {
                    if (TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                }
                return _EMPTProc1;
            }
            set => _EMPTProc1 = value;            
        }

        private dNOtAs _dNOtAs;

        /// <summary>
        /// 
        /// </summary>
        public dNOtAs DNOtAs
        {
            get
            {
                if (_dNOtAs == null)
                {
                    if (linhaMae == null)
                        _dNOtAs = new dNOtAs(EMPTProc1);
                    else
                        _dNOtAs = (dNOtAs)linhaMae.Table.DataSet;
                }
                return _dNOtAs;
            }
        }

        /// <summary>
        /// Linha mãe
        /// </summary>
        protected dNOtAs.NOtAsRow linhaMae;

        /// <summary>
        /// Linha mãe
        /// </summary>
        public dNOtAs.NOtAsRow LinhaMae
        {
            get { return linhaMae; }
        }

        public Competencia Competencia
        {
            get
            {
                return new Competencia(LinhaMae.NOACompet, linhaMae.NOA_CON, DBNull.Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Encontrada()
        {
            return linhaMae != null;
        }

        /// <summary>
        /// 
        /// </summary>
        public NOAStatus Status
        {
            get
            {
                if (!Encontrada())
                    return NOAStatus.NaoEncontrada;
                return (NOAStatus)LinhaMae.NOAStatus;
            }
            set
            {
                LinhaMae.NOAStatus = (int)value;
            }
        }

        /*
        internal void CancelaNotaprovisoria()
        {
            try
            {
                EMPTProc1.AbreTrasacaoSQL("NotaProc CancelaNotaprovisoria 113", DNOtAs.NOtAsTableAdapter, DNOtAs.PAGamentosTableAdapter);
                if (Status != NOAStatus.NotaProvisoria)
                    return;
                List<dNOtAs.PAGamentosRow> Apagar = new List<dNOtAs.PAGamentosRow>();
                foreach (dNOtAs.PAGamentosRow PAGrow in DNOtAs.PAGamentos)
                    Apagar.Add(PAGrow);
                foreach (dNOtAs.PAGamentosRow PAGrow in Apagar)
                {
                    RemoveParcelaDoCheque(PAGrow);
                    PAGrow.Delete();
                    DNOtAs.PAGamentosTableAdapter.Update(PAGrow);
                };
                linhaMae.Delete();
                DNOtAs.NOtAsTableAdapter.Update(linhaMae);
                EMPTProc1.Commit();
            }
            catch (Exception ex)
            {
                EMPTProc1.Vircatch(ex);
                throw new Exception("Erro ao cancelar nota provisória", ex);
            }
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public FasePeriodico VerificaFase()
        {
            if (Status == NOAStatus.NotaProvisoria)
                return FasePeriodico.ComNotaprov;
            FasePeriodico retorno = FasePeriodico.CadastrarProxima;
            if (DNOtAs.PAGamentos.Count == 0)
            {
                EMPTProc1.EmbarcaEmTrans(DNOtAs.PAGamentosTableAdapter);
                DNOtAs.PAGamentosTableAdapter.FillByNOA(DNOtAs.PAGamentos, NOA);
            }
            foreach (dNOtAs.PAGamentosRow PAGrow in DNOtAs.PAGamentos)
            {
                if (PAGrow.IsCHEStatusNull())
                    continue;
                if (!PAGrow.IsPAGIMPNull())
                    continue;
                if ((CHEStatus)PAGrow.CHEStatus == CHEStatus.CadastradoBloqueado)
                    return FasePeriodico.ComNotaprov;
                if (ChequeProc.Editavel((CHEStatus)PAGrow.CHEStatus))
                    retorno = FasePeriodico.AguardaEmissaoCheque;
            }
            return retorno;
        }

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc"></param>
        public NotaProc(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
        }

        /// <summary>
        /// Construtor para uma nota já existente. verifique depois se "encotrada" está como true
        /// </summary>
        /// <param name="NOA"></param>
        /// <param name="_EMPTProc1"></param>
        public NotaProc(int NOA, EMPTProc _EMPTProc1 = null)
            : this(_EMPTProc1)
        {
            EMPTProc1.EmbarcaEmTrans(DNOtAs.NOtAsTableAdapter);
            if (DNOtAs.NOtAsTableAdapter.FillByNOA(DNOtAs.NOtAs, NOA) > 0)
                linhaMae = DNOtAs.NOtAs[0];
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_LinhaMae"></param>
        /// <param name="_EMPTProc1"></param>
        public NotaProc(dNOtAs.NOtAsRow _LinhaMae, EMPTProc _EMPTProc1)
            : this(_EMPTProc1)
        {
            linhaMae = _LinhaMae;
        }

        /// <summary>
        /// Construtor para uma nota já existente. verifique depois se "encotrada" está como true
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="Compet"></param>
        /// <param name="Tipo"></param>
        /// <param name="Numero"></param>
        public NotaProc(int CON, Competencia Compet, NOATipo Tipo, int Numero)
        {
            DNOtAs.NOtAsTableAdapter.EmbarcaEmTransST();
            if (DNOtAs.NOtAsTableAdapter.FillByTipo(DNOtAs.NOtAs, CON, Numero, (int)Tipo, Compet.CompetenciaBind) > 0)
                linhaMae = DNOtAs.NOtAs[0];
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="FRN"></param>
        /// <param name="NumeroNota"></param>
        /// <param name="DataEmissao"></param>
        /// <param name="ValorTotal"></param>
        /// <param name="Tipo"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="servico"></param>
        /// <param name="Comp"></param>
        /// <param name="PAGTipo"></param>
        /// <param name="NOAStatus"></param>
        /// <param name="Obs"></param>
        /// <param name="DadosPag"></param>
        /// <param name="PGF"></param>
        /// <param name="_EMPTProc1"></param>
        public NotaProc(int CON, int? FRN, int NumeroNota, DateTime DataEmissao, decimal ValorTotal, NOATipo Tipo, string PLA, int SPL, string servico,
                    Competencia Comp,string ArquivoNotaPDF = "",string ArquivoNotaXML = "", PAGTipo PAGTipo = PAGTipo.cheque, NOAStatus NOAStatus = NOAStatus.SemPagamentos,
                    string Obs = null, string DadosPag = null, int? PGF = null, EMPTProc _EMPTProc1 = null)
            : this(_EMPTProc1)
        {
            try
            {
                EMPTProc1.AbreTrasacaoSQL("ContaPagar Notas - 172", DNOtAs.NOtAsTableAdapter);
                linhaMae = DNOtAs.NOtAs.NewNOtAsRow();
                linhaMae.NOA_CON = CON;
                linhaMae.NOANumero = NumeroNota;
                linhaMae.NOADataEmissao = DataEmissao;
                if (Comp == null)
                    Comp = new Competencia(DataEmissao);
                linhaMae.NOACompet = Comp.CompetenciaBind;
                linhaMae.NOAAguardaNota = false;
                linhaMae.NOADATAI = DateTime.Now;
                linhaMae.NOAI_USU = EMPTProc1.USU;
                if ((Obs != null) && (Obs != ""))
                    linhaMae.NOAObs = Obs;
                linhaMae.NOATipo = (int)Tipo;
                linhaMae.NOA_PLA = PLA;
                linhaMae.NOA_SPL = SPL;
                if (FRN.GetValueOrDefault(0) != 0)
                    linhaMae.NOA_FRN = FRN.Value;
                linhaMae.NOAServico = servico;
                linhaMae.NOAStatus = (int)NOAStatus;
                if (DadosPag != null)
                    linhaMae.NOADadosPag = DadosPag;
                linhaMae.NOATotal = ValorTotal;
                linhaMae.NOADefaultPAGTipo = (int)PAGTipo;
                if (PGF.HasValue)
                    linhaMae.NOA_PGF = PGF.Value;
                DNOtAs.NOtAs.AddNOtAsRow(linhaMae);
                DNOtAs.NOtAsTableAdapter.Update(linhaMae);                
                PublicaNotas(ArquivoNotaPDF, ArquivoNotaXML);                                
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                linhaMae = null;
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro no cadastro da nota", e);
            }
        }
        #endregion
                

        private ClientWCFFtp.ClientFTP _clientFTP;

        private ClientWCFFtp.ClientFTP clientFTP
        {
            get
            {
                if (_clientFTP == null)
                    _clientFTP = new ClientWCFFtp.ClientFTP(FuncoesBasicasProc.EstaEmProducao, EMPTProc1.USU, EMPTProc1.EMP);
                return _clientFTP;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected string NomeArquivoPDFServidor => string.Format("Notas\\{0}\\{1}_{2}", Competencia.CompetenciaBind, EMPTProc1.EMP, NOA);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ArquivoNotaPDF"></param>
        /// <param name="ArquivoNotaXML"></param>
        public void PublicaNotas(string ArquivoNotaPDF , string ArquivoNotaXML)
        {
            if (ArquivoNotaPDF != "")
            {
                string NomeRemoto = string.Format("{0}.pdf", NomeArquivoPDFServidor);
                if (clientFTP.Put(ArquivoNotaPDF, NomeRemoto))
                {
                    LinhaMae.NOAPDF = NomeArquivoPDFServidor;
                    EMPTProc1.EmbarcaEmTrans(DNOtAs.NOtAsTableAdapter);
                    DNOtAs.NOtAsTableAdapter.Update(LinhaMae);
                }
            };
            if (ArquivoNotaXML != "")
            {
                string NomeRemoto = string.Format("{0}.xml", NomeArquivoPDFServidor);
                clientFTP.Put(ArquivoNotaXML, NomeRemoto);
            }
        }

        private string CaminhoBase => System.IO.Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")) + "\\TMP\\";
                
            
        

        public string ImpressoNota()
        {
            if (!LinhaMae.IsNOAPDFNull())
            {
                string NomeArquivo = string.Format("{0}{1}.pdf", CaminhoBase, System.IO.Path.GetFileName(LinhaMae.NOAPDF));
                if (clientFTP.Get(NomeArquivo, string.Format("{0}.pdf", LinhaMae.NOAPDF)))
                    return NomeArquivo;
                else
                {
                    if (clientFTP.Get(NomeArquivo, string.Format("{0}.pdf", LinhaMae.NOAPDF)))
                        return NomeArquivo;
                    else
                        return "";                    
                }
            }
            else
                return "";
        }

        /// <summary>
        /// 
        /// </summary>
        public int NOA
        {
            get { return LinhaMae.NOA; }
        }

        internal void AjustaChequeAcumular(DateTime NovaData, bool travada, decimal ValorMedio)
        {
            try
            {
                EMPTProc1.AbreTrasacaoSQL("Nota.cs AjustaChequeAcumular - 423", DNOtAs.NOtAsTableAdapter, DNOtAs.PAGamentosTableAdapter);
                if (DNOtAs.PAGamentos.Count == 0)
                    DNOtAs.PAGamentosTableAdapter.FillByNOA(DNOtAs.PAGamentos, NOA);
                if (DNOtAs.PAGamentos.Count != 1)
                    throw new Exception(string.Format("Nota acumulado com mais de uma linha NOA:{0}", NOA));
                if (ValorMedio != 0)
                {
                    LinhaMae.NOATotal = ValorMedio;
                    DNOtAs.NOtAsTableAdapter.Update(LinhaMae);
                    LinhaMae.AcceptChanges();
                }
                foreach (dNOtAs.PAGamentosRow rowPAG in DNOtAs.PAGamentos)
                {
                    rowPAG.PAGVencimento = NovaData;
                    if (ValorMedio != 0)
                        rowPAG.PAGValor = ValorMedio;
                    AjustaParcelaNoCheque(rowPAG, travada);
                }
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw (e);
            }
        }

        const string ComandoVerificaRHS = "SELECT RHS FROM RHTarefaSubdetalhe WHERE (RHS_CHE = @P1)";
        const string ComandoLimpaRHS = "update RHTarefaSubdetalhe set RHS_CHE = null WHERE (RHS_CHE = @P1)";
        const string ComandoReprogramaRHS = "update RHTarefaSubdetalhe set RHS_CHE = @P2 WHERE (RHS = @P1)";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rowPag"></param>
        /// <param name="Travada"></param>
        /// <param name="Justificativa"></param>
        public void AjustaParcelaNoCheque(dNOtAs.PAGamentosRow rowPag, bool Travada = false, string Justificativa = "")
        {
            try
            {
                dNOtAs dNOtAsLocal = (dNOtAs)rowPag.Table.DataSet;
                EMPTProc1.AbreTrasacaoSQL("Nota.cs AjustaParcelaNoCheque - 417", dNOtAsLocal.PAGamentosTableAdapter);
                switch (rowPag.RowState)
                {
                    case DataRowState.Added:
                        ColocaParcelaNocheque(rowPag, Travada);
                        break;
                    case DataRowState.Deleted:
                        RemoveParcelaDoCheque(rowPag);
                        break;
                    case DataRowState.Modified:
                        DataTable RHSs = null;
                        if (!rowPag.IsPAG_CHENull())
                        {
                            RHSs = EMPTProc1.STTA.BuscaSQLTabela(ComandoVerificaRHS, rowPag.PAG_CHE);
                            EMPTProc1.STTA.ExecutarSQLNonQuery(ComandoLimpaRHS, rowPag.PAG_CHE);
                        }
                        RemoveParcelaDoCheque(rowPag);
                        int? novoCHE = ColocaParcelaNocheque(rowPag, Travada, Justificativa);
                        if ((RHSs != null) && novoCHE.HasValue)
                            foreach (DataRow DR in RHSs.Rows)
                                EMPTProc1.STTA.ExecutarSQLNonQuery(ComandoReprogramaRHS, DR["RHS"], novoCHE.Value);
                        break;

                };
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception(string.Format("Recriada:{0}", e.Message), e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Matar()
        {            
            try
            {
                EMPTProc1.AbreTrasacaoSQL("MantaNota NotaProc 318", DNOtAs.PAGamentosTableAdapter, DNOtAs.NOtAsTableAdapter);
                if (DNOtAs.PAGamentos.Count == 0)
                    DNOtAs.PAGamentosTableAdapter.FillByNOA(DNOtAs.PAGamentos, NOA);
                dNOtAs.PAGamentosRow[] PAGrows = linhaMae.GetPAGamentosRows();
                foreach (dNOtAs.PAGamentosRow PAGrow in PAGrows)
                {
                    RemoveParcelaDoCheque(PAGrow);
                    PAGrow.Delete();
                }
                linhaMae.Delete();
                DNOtAs.PAGamentosTableAdapter.Update(DNOtAs.PAGamentos);
                DNOtAs.NOtAsTableAdapter.Update(linhaMae);
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                TableAdapter.VircatchSQL(e);
                return false;
            }
            return true;
        }
     
        private void RemoveParcelaDoCheque(dNOtAs.PAGamentosRow rowPag/*, dNOtAs dNOtAsLocal*/)
        {            
            dNOtAs dNOtAsLocal = (dNOtAs)rowPag.Table.DataSet;
            if ((PAGTipo)rowPag["PAGTipo", DataRowVersion.Original] == PAGTipo.Acumular)
                return;
            ChequeProc Cheque = null;
            if (rowPag.RowState == DataRowState.Deleted)
                Cheque = new ChequeProc((int)rowPag["PAG_CHE", DataRowVersion.Original], ChequeProc.TipoChave.CHE, EMPTProc1);
            else
            {
                if (!rowPag.IsPAG_CHENull())
                {
                    Cheque = new ChequeProc(rowPag.PAG_CHE, ChequeProc.TipoChave.CHE, EMPTProc1);
                    rowPag.SetPAG_CHENull();
                }
            }
            try
            {
                dNOtAsLocal.PAGamentosTableAdapter.Update(rowPag);
            }
            catch (Exception e)
            {
                if (VirExceptionProc.VirExceptionProc.IdentificaTipoDeErro(e) == VirExceptionProc.VirTipoDeErro.concorrencia)
                    throw new Exception(string.Format(EMPTProc1.STTA.RastreiaConcorrencia(rowPag), e.Message), e);
                else
                    throw new Exception(string.Format("Erro:{0}", e.Message), e);
            }
            if (Cheque != null)
                Cheque.AjustarTotalPelasParcelas();
        }        

        private int? ColocaParcelaNocheque(dNOtAs.PAGamentosRow rowPag, bool Travada = false, string Justificativa = "")
        {
            dNOtAs dNOtAsLocal = (dNOtAs)LinhaMae.Table.DataSet;
            if ((PAGTipo)rowPag.PAGTipo == PAGTipo.Acumular)
                return null;
            if (rowPag.IsPAGIMPNull())
            {
                if ((rowPag.IsCHEFavorecidoNull()) || (rowPag.CHEFavorecido == ""))
                    if (!LinhaMae.IsNOA_FRNNull())
                    {
                        dNOtAs.FORNECEDORESRow rowFRN = DNOtAs.FORNECEDORES.FindByFRN(LinhaMae.NOA_FRN);
                        if (rowFRN == null)
                        {
                            DNOtAs.FORNECEDORESTableAdapter.FillByFRN(DNOtAs.FORNECEDORES, LinhaMae.NOA_FRN);
                            rowFRN = DNOtAs.FORNECEDORES.FindByFRN(LinhaMae.NOA_FRN);
                        }
                        rowPag.CHEFavorecido = rowFRN.FRNNome;
                    }
                    else if (!LinhaMae.IsNOA_CCTCredNull())
                    {
                        dNOtAs.CONDOMINIOSRow rowCON = DNOtAs.CONDOMINIOS.FindByCON(LinhaMae.NOA_CON);
                        if (rowCON == null)
                        {
                            DNOtAs.CONDOMINIOSTableAdapter.FillByCON(DNOtAs.CONDOMINIOS, LinhaMae.NOA_CON);                            
                            rowCON = DNOtAs.CONDOMINIOS.FindByCON(LinhaMae.NOA_CON);
                        }
                        rowPag.CHEFavorecido = rowCON.CONNome;
                    }
                    else
                        rowPag.CHEFavorecido = "";
            }

            int CCT = 0;
            PAGTipo Tipo = (PAGTipo)rowPag.PAGTipo;
            
            if (Tipo.EstaNoGrupo(PAGTipo.PECreditoConta,PAGTipo.PEOrdemPagamento, PAGTipo.PETituloBancario,PAGTipo.PETituloBancarioAgrupavel,PAGTipo.PETransferencia))
            {
                if (LinhaMae.IsNOA_CCTNull())
                    throw new Exception(string.Format("Pagamento eletrônico sem conta de débito indicada. Dados: NOA={0} PAG={1}\r\nChecar validação",rowPag.PAG_NOA,rowPag.PAG));
                CCT = LinhaMae.NOA_CCT;
            }
            
            string strPAGAdicional = "";
            if (!rowPag.IsPAGAdicionalNull())
                strPAGAdicional = rowPag.PAGAdicional;
            if ((!rowPag.IsCHEStatusNull()) && (rowPag.CHEStatus == (int)CHEStatus.Cancelado))
                rowPag.PAGPermiteAgrupar = false;
            int? CCG = rowPag.IsPAG_CCGNull() ? (int?)null : rowPag.PAG_CCG;
            ChequeProc Cheque = new ChequeProc(rowPag.PAGVencimento, rowPag.CHEFavorecido, linhaMae.NOA_CON, Tipo, !rowPag.PAGPermiteAgrupar, null, CCT, strPAGAdicional, Travada, CCG, EMPTProc1);
            rowPag.PAG_CHE = Cheque.CHErow.CHE;            
            rowPag.PAGStatus = Cheque.CHErow.CHEStatus;            
            dNOtAsLocal.PAGamentosTableAdapter.Update(rowPag);
            rowPag.AcceptChanges();
            //ATENÇÃO um cheque cancelado não pode executar o AjustarTotalPelasParcelas antes de ser marcado como cancelado, caso contrário vai matar o cheque
            if ((!rowPag.IsCHEStatusNull()) && (rowPag.CHEStatus == (int)CHEStatus.Cancelado))
                Cheque.CancelarPagamento(Justificativa);
            else
                Cheque.AjustarTotalPelasParcelas();
            return Cheque.CHErow.CHE;
        }

        private dNOtAs.PAGamentosRow _rowPAGUnica;

        /// <summary>
        /// 
        /// </summary>
        public dNOtAs.PAGamentosRow rowPAGUnica
        {
            get
            {
                if (_rowPAGUnica == null)
                {
                    if (DNOtAs.PAGamentos.Count == 0)
                        DNOtAs.PAGamentosTableAdapter.FillByNOA(DNOtAs.PAGamentos, NOA);
                    if (DNOtAs.PAGamentos.Count != 1)
                        return null;
                    _rowPAGUnica = LinhaMae.GetPAGamentosRows()[0];
                };
                return _rowPAGUnica;
            }
        }



        /// <summary>
        /// Destrava nota
        /// </summary>
        /// <param name="NOADataEmissao"></param>
        /// <param name="PreCheques"></param>
        /// <returns>Retorna o NOA</returns>
        public int DestravaCadastrar(DateTime NOADataEmissao, List<PreCheque> PreCheques)
        {            
            try
            {
                EMPTProc1.AbreTrasacaoSQL("NotaProc.cs DestravaCadastrar - 440", DNOtAs.NOtAsTableAdapter, DNOtAs.PAGamentosTableAdapter);
                //bool Primeira = false;
                if (Status == NOAStatus.NotaProvisoria)
                {
                    LinhaMae.NOADataEmissao = NOADataEmissao;
                    linhaMae.NOATotal = 0;
                    //Primeira = true;
                }
                foreach (PreCheque Pre in PreCheques)
                {
                    linhaMae.NOATotal += Pre.Valor;
                    dNOtAs.PAGamentosRow rowPag;
                    if (Status == NOAStatus.NotaProvisoria)
                    {
                        Status = NOAStatus.Cadastrada;
                        rowPag = rowPAGUnica;
                    }
                    else
                    {
                        rowPag = DNOtAs.PAGamentos.NewPAGamentosRow();
                        rowPag.PAG_NOA = NOA;
                        rowPag.PAGDATAI = DateTime.Now;
                        rowPag.PAGI_USU = EMPTProc1.USU;
                        rowPag.PAGPermiteAgrupar = Pre.permiteagrupar;
                        if (Status == NOAStatus.SemPagamentos)
                            Status = NOAStatus.Cadastrada;
                    }
                    rowPag.PAGValor = Pre.Valor;
                    rowPag.PAGVencimento = Pre.DataVencimento;
                    rowPag.CHEFavorecido = Pre.Nominal;
                    rowPag.PAGTipo = (int)Pre.Tipo;
                    if (Pre.CCG.HasValue)
                        rowPag.PAG_CCG = Pre.CCG.Value;
                    if (rowPag.RowState == DataRowState.Detached)
                        DNOtAs.PAGamentos.AddPAGamentosRow(rowPag);
                    AjustaParcelaNoCheque(rowPag);
                    Pre.CHE = rowPag.PAG_CHE;
                };                
                DNOtAs.NOtAsTableAdapter.Update(LinhaMae);                
                LinhaMae.AcceptChanges();
                EMPTProc1.Commit();
                return linhaMae.NOA;
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro ao destravar nota", e);
            }
        }

        protected SortedList PAG_Pagamentos; //dNOtAs.PAGamentosRow

        protected SortedList PAG_PagamentosCancelados; //dNOtAs.PAGamentosRow

        protected dNOtAs.PAGamentosRow PAGISSrow;
         protected dNOtAs.PAGamentosRow PAGPIS_COFINS_CSLLrow;
        protected dNOtAs.PAGamentosRow PAGIrrow;
        protected dNOtAs.PAGamentosRow PAGPrincipal;

        protected void detalhar()
        {
            //if (detalhada)
            //    return;
            EMPTProc1.EmbarcaEmTrans(DNOtAs.PAGamentosTableAdapter);
            DNOtAs.PAGamentosTableAdapter.FillByNOA(DNOtAs.PAGamentos, NOA);
            PAG_Pagamentos = new SortedList();
            PAG_PagamentosCancelados = new SortedList();
            PAGISSrow = null;
            PAGPIS_COFINS_CSLLrow = null;
            PAGIrrow = null;
            foreach (dNOtAs.PAGamentosRow row in DNOtAs.PAGamentos)
            {
                if ((CHEStatus)row.CHEStatus == CHEStatus.Cancelado)
                {
                    PAG_PagamentosCancelados.Add(row.PAG, row);                    
                }
                else
                {
                    if (row.IsPAGIMPNull())
                    {
                        PAG_Pagamentos.Add(row.PAG, row);
                        PAGPrincipal = row;
                    }
                    else
                        switch ((VirEnumeracoes.TipoImposto)row.PAGIMP)
                        {
                            case VirEnumeracoes.TipoImposto.IR:
                                PAGIrrow = row;
                                break;
                            case VirEnumeracoes.TipoImposto.ISSQN:
                            case VirEnumeracoes.TipoImposto.ISS_SA:
                                PAGISSrow = row;
                                break;
                            case VirEnumeracoes.TipoImposto.PIS_COFINS_CSLL:
                                PAGPIS_COFINS_CSLLrow = row;
                                break;
                        }                        
                }
            }
        }

        public void AjustaRetencoesFaturamento(decimal TotalGeralDaNota)
        {
            detalhar();
            decimal retPIS = Math.Round(TotalGeralDaNota * 0.0065M, 2, MidpointRounding.AwayFromZero);
            decimal retCof = Math.Round(TotalGeralDaNota * 0.03M, 2, MidpointRounding.AwayFromZero);
            decimal retCsll = Math.Round(TotalGeralDaNota * 0.01M, 2, MidpointRounding.AwayFromZero);
            decimal TotalPIS_COFINS_CSLL = retPIS + retCof + retCsll;
            decimal Abater = 0;
            if((PAGPIS_COFINS_CSLLrow != null) && (PAGPIS_COFINS_CSLLrow.PAGValor != TotalPIS_COFINS_CSLL))
            {
                Abater += TotalPIS_COFINS_CSLL - PAGPIS_COFINS_CSLLrow.PAGValor;
                EMPTProc1.EmbarcaEmTrans(DNOtAs.PAGamentosTableAdapter);
                PAGPIS_COFINS_CSLLrow.PAGValor = TotalPIS_COFINS_CSLL;
                DNOtAs.PAGamentosTableAdapter.Update(PAGPIS_COFINS_CSLLrow);
                new ChequeProc(PAGPIS_COFINS_CSLLrow.PAG_CHE, ChequeProc.TipoChave.CHE,EMPTProc1).AjustarTotalPelasParcelas();
            }
            if (PAGIrrow != null)
            {
                dllImpostos.Imposto ImpIR = new dllImpostos.Imposto(VirEnumeracoes.TipoImposto.IR);
                ImpIR.ValorBase = TotalGeralDaNota;
                decimal ValorCalc = ImpIR.ValorImposto;
                if (ValorCalc != PAGIrrow.PAGValor)
                {
                    Abater += ValorCalc - PAGIrrow.PAGValor;
                    EMPTProc1.EmbarcaEmTrans(DNOtAs.PAGamentosTableAdapter);
                    PAGIrrow.PAGValor = ValorCalc;
                    DNOtAs.PAGamentosTableAdapter.Update(PAGIrrow);
                    new ChequeProc(PAGIrrow.PAG_CHE, ChequeProc.TipoChave.CHE, EMPTProc1).AjustarTotalPelasParcelas();
                }
            }
            if (PAGISSrow != null)
            {
                dllImpostos.Imposto ImpISS = new dllImpostos.Imposto(EMPTProc1.EMP == 1 ? VirEnumeracoes.TipoImposto.ISSQN : VirEnumeracoes.TipoImposto.ISS_SA);
                ImpISS.ValorBase = TotalGeralDaNota;
                decimal ValorCalc = ImpISS.ValorImposto;
                decimal aliISS = dllImpostos.Imposto.AliquotaBase(Framework.DSCentral.EMP == 1 ? VirEnumeracoes.TipoImposto.ISSQN : VirEnumeracoes.TipoImposto.ISS_SA); 
                ValorCalc = Math.Round(TotalGeralDaNota * aliISS, 2, MidpointRounding.AwayFromZero);
                if (ValorCalc != PAGISSrow.PAGValor)
                {
                    Abater += ValorCalc - PAGISSrow.PAGValor;
                    EMPTProc1.EmbarcaEmTrans(DNOtAs.PAGamentosTableAdapter);
                    PAGISSrow.PAGValor = ValorCalc;
                    DNOtAs.PAGamentosTableAdapter.Update(PAGISSrow);
                    new ChequeProc(PAGISSrow.PAG_CHE, ChequeProc.TipoChave.CHE, EMPTProc1).AjustarTotalPelasParcelas();
                }
            }
            if (Abater > 0)
            {
                EMPTProc1.EmbarcaEmTrans(DNOtAs.PAGamentosTableAdapter);
                PAGPrincipal.PAGValor -= Abater;
                DNOtAs.PAGamentosTableAdapter.Update(PAGPrincipal);
                new ChequeProc(PAGPrincipal.PAG_CHE, ChequeProc.TipoChave.CHE, EMPTProc1).AjustarTotalPelasParcelas();
            }
        }

        /*
        public bool ReterImposto(List<VirEnumeracoes.TipoImposto> Impostos)
        {
            detalhar();   
            //dNOtAs.PAGamentosRow[] rowPAGs = linhaMae.GetPAGamentosRows();
            if (PAG_Pagamentos.Count == 1)
            {
                dNOtAs.PAGamentosRow rowPAGPrincipal = (dNOtAs.PAGamentosRow)PAG_Pagamentos.GetByIndex(0);
                if (linhaMae.NOATotal != rowPAGPrincipal.PAGValor)
                    return false;
                foreach (VirEnumeracoes.TipoImposto Timp in Impostos)
                {
                    decimal ValCont = Math.Round(LinhaMae.NOATotal * Imposto.AliquotaBase(Timp) + 0.004999M, 2);

                    Imposto imp = new ImpostoNeonProc(Timp, LinhaMae.NOADataEmissao, LinhaMae.NOADataEmissao, LinhaMae.NOATotal, new Competencia(LinhaMae.NOACompet));
                    dNOtAs.PAGamentosRow NovaPAG = DNOtAs.PAGamentos.NewPAGamentosRow();
                    if (!rowPAGPrincipal.IsPAG_CTLNull())
                        NovaPAG.PAG_CTL = rowPAGPrincipal.PAG_CTL;
                    if (((PAGTipo)rowPAGPrincipal.PAGTipo).EstaNoGrupo(PAGTipo.PECreditoConta))
                        NovaPAG.PAGTipo = (int)PAGTipo.PEGuia;
                    else
                        NovaPAG.PAGTipo = (int)PAGTipo.Guia;
                    NovaPAG.PAGIMP = (int)Timp;
                    NovaPAG.CHEFavorecido = imp.Nominal();
                    NovaPAG.PAGPermiteAgrupar = true;
                    NovaPAG.CHEStatus = (int)CHEStatus.Cadastrado;
                    NovaPAG.PAGValor = imp.ValorImposto;
                    NovaPAG.PAGVencimento = imp.VencimentoEfetivo;
                    NovaPAG.PAG_NOA = linhaMae.NOA;
                    rowPAGPrincipal.PAGValor -= imp.ValorImposto;                    
                    DNOtAs.PAGamentos.AddPAGamentosRow(NovaPAG);                    
                    DNOtAs.PAGamentosTableAdapter.Update(NovaPAG);
                    AjustaParcelaNoCheque(NovaPAG);                    
                }
                DNOtAs.PAGamentosTableAdapter.Update(rowPAGPrincipal);
                AjustaParcelaNoCheque(rowPAGPrincipal);
                DNOtAs.NOtAsTableAdapter.Update(LinhaMae);
                return true;
            }
            else
                return false;
        }*/
        
}

    /// <summary>
    /// Objeto com as informações para cadastro dos pagamentos/Cheques
    /// </summary>
    public class PreCheque
    {
        /// <summary>
        /// Nominal do pagamentos
        /// </summary>
        public string Nominal { get; set; }
        /// <summary>
        /// Valor
        /// </summary>
        public decimal Valor { get; set; }
        /// <summary>
        /// Vencimento
        /// </summary>
        public DateTime DataVencimento { get; set; }
        /// <summary>
        /// Tipo do cheque
        /// </summary>
        public PAGTipo Tipo { get; set; }
        /// <summary>
        /// Permite ou não agrupar
        /// </summary>
        public bool permiteagrupar { get; set; }
        /// <summary>
        /// Conta para crédito
        /// </summary>
        public int? CCG { get; set; }
        /// <summary>
        /// Cheque gerado
        /// </summary>
        public int CHE;
    }
}
