﻿using System;
using FrameworkProc;

namespace ContaPagarProc.LoteRepasse
{
    partial class dLoteRepasse : IEMPTProc
    {

        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dLoteRepasse(EMPTProc _EMPTProc1)
            : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        private dLoteRepasseTableAdapters.FaturarTableAdapter faturarTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Faturar
        /// </summary>
        public dLoteRepasseTableAdapters.FaturarTableAdapter FaturarTableAdapter
        {
            get
            {
                if (faturarTableAdapter == null)
                {
                    faturarTableAdapter = new dLoteRepasseTableAdapters.FaturarTableAdapter();
                    faturarTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return faturarTableAdapter;
            }
        }

        private dLoteRepasseTableAdapters.NOtAsTableAdapter nOtAsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOtAs
        /// </summary>
        public dLoteRepasseTableAdapters.NOtAsTableAdapter NOtAsTableAdapter
        {
            get
            {
                if (nOtAsTableAdapter == null)
                {
                    nOtAsTableAdapter = new dLoteRepasseTableAdapters.NOtAsTableAdapter();
                    nOtAsTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return nOtAsTableAdapter;
            }
        }

        private dLoteRepasseTableAdapters.FATuramentoTableAdapter fATuramentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FATuramento
        /// </summary>
        public dLoteRepasseTableAdapters.FATuramentoTableAdapter FATuramentoTableAdapter
        {
            get
            {
                if (fATuramentoTableAdapter == null)
                {
                    fATuramentoTableAdapter = new dLoteRepasseTableAdapters.FATuramentoTableAdapter();
                    fATuramentoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return fATuramentoTableAdapter;
            }
        }

    }
}
