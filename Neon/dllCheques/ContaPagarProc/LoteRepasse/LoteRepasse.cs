﻿using System;
using VirEnumeracoesNeon;
using FrameworkProc;

namespace ContaPagarProc.LoteRepasse
{
    /// <summary>
    /// Objeto para cadastro em lote dos repasses
    /// </summary>
    public class LoteRepasse : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1 { get => _EMPTProc1 ?? (_EMPTProc1 = new EMPTProc(EMPTipo.Local)); set => _EMPTProc1 = value; }

        private dLoteRepasse DLoteRepasse;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        /// <param name="_dLoteRepasse"></param>
        public LoteRepasse(EMPTProc _EMPTProc1, dLoteRepasse _dLoteRepasse)
        {
            DLoteRepasse = _dLoteRepasse;
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        private dllImpostos.SolicitaRetencao[] Retencoes(dLoteRepasse.NOtAsRow rowREFT, decimal ValorTotal)
        {
            System.Collections.Generic.List<dllImpostos.SolicitaRetencao> Reter = new System.Collections.Generic.List<dllImpostos.SolicitaRetencao>();
            /*
            if (DT.Rows.Count == 1)
            {
                if (DT.Rows[0]["PGFISS"] != DBNull.Value)
                {
                    decimal ISS = (decimal)DT.Rows[0]["PGFISS"];
                    if (ISS != 0)
                    {
                        VirEnumeracoes.TipoImposto Timposto = EMPTProc1.EMP == 1 ? VirEnumeracoes.TipoImposto.ISSQN : VirEnumeracoes.TipoImposto.ISS_SA;
                        decimal ValISS = Math.Round(dllImpostos.Imposto.AliquotaBase(Timposto)* ValorTotal + 0.004999M, 2,MidpointRounding.AwayFromZero);
                        Reter.Add(new dllImpostos.SolicitaRetencao(Timposto, ValISS, VirEnumeracoes.SimNao.Sim));
                    }
                };
                if (DT.Rows[0]["PGFPISCOF"] != DBNull.Value)
                {
                    decimal COF = (decimal)DT.Rows[0]["PGFPISCOF"];
                    if (COF != 0)
                    {
                        VirEnumeracoes.TipoImposto Timposto = VirEnumeracoes.TipoImposto.PIS_COFINS_CSLL;
                        decimal ValCOF = Math.Round(dllImpostos.Imposto.AliquotaBase(Timposto) * ValorTotal + 0.004999M, 2, MidpointRounding.AwayFromZero);
                        Reter.Add(new dllImpostos.SolicitaRetencao(Timposto, ValCOF, VirEnumeracoes.SimNao.Sim));
                    }
                };
            }*/
            return Reter.ToArray();
        }

        /// <summary>
        /// Gerar Cheques
        /// </summary>
        /// <param name="FornecedorCom"></param>
        /// <param name="FornecedorSem"></param>
        /// <returns></returns>
        public bool GeraChequesRepasse()
        {
            Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(201804);
            //int SPL = EMPTProc1.EMP == 1 ? 2 : 35;
            //AbstratosNeon.ABS_Fornecedor Fornecedor;
            //DLoteRepasse.PaGamentosFixosTableAdapter.Fill(DLoteRepasse.PaGamentosFixos);

            //Busca as notas do mês para incluir
            DLoteRepasse.NOtAsTableAdapter.FillByCOMP(DLoteRepasse.NOtAs, Comp.CompetenciaBind);
            //Busca os faturamentos evitando duplicidade
            DLoteRepasse.FATuramentoTableAdapter.FillByComp(DLoteRepasse.FATuramento, Comp.CompetenciaBind);
            


            dNOtAs dNOtAs1 = new dNOtAs(EMPTProc1);
            int contador = 0;
            AbstratosNeon.ABS_Fornecedor Fornecedor = null;
            //System.Collections.Generic.List<dllImpostos.SolicitaRetencao> Reter = new System.Collections.Generic.List<dllImpostos.SolicitaRetencao>();
            //dllImpostos.SolicitaRetencao Teste = new dllImpostos.SolicitaRetencao(VirEnumeracoes.TipoImposto.ISSQN, 0, VirEnumeracoes.SimNao.Sim);
            foreach (dLoteRepasse.ImportarRow rowImp in DLoteRepasse.Importar)
            {
                //Reter.Clear();
                //System.Data.DataTable DT = EMPTProc1.STTA.BuscaSQLTabela(comando, rowImp.CON, FornecedorCom);
                //System.Data.DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comando, rowImp.CON, FornecedorCom.FRN);
                //if (DT.Rows.Count == 1)
                //{
                //    int Dia = (int)DT.Rows[0]["PGFDia"];
                //    rowImp.Data = new DateTime(rowImp.Data.Year, rowImp.Data.Month, Dia);
                //}
                //int Dia = rowImp.PaGamentosFixosRow.PGFDia;
                //Framework.objetosNeon.Competencia CompServ = new Framework.objetosNeon.Competencia(rowImp.Data).Add(-1);
                contador++;
                //if(contador == 11)
                //    Console.Write("*************contador:{0} - {1}\r\n", contador, rowImp.CON);
                Console.Write("contador:{0} - {1}\r\n", contador, rowImp.CON);

                //Fornecedor = rowImp.ComFaturamento ? FornecedorCom : FornecedorSem;
                //int? CCT = null;
                //if (!rowImp.IsCCTNull())
                //    CCT = rowImp.CCT;
                //PAGTipo PagTipo = (PAGTipo)rowImp.PagTipo;
                //if (Fornecedor == null)
                //    PagTipo = PAGTipo.cheque;
                if (rowImp.FATuramentoRow != null)
                    continue;

                dLoteRepasse.NOtAsRow rowREF = rowImp.NOtAsRow;
                if (rowREF == null)
                {
                    Console.Write("contador:{0} - {1} PULADO\r\n", contador, rowImp.CON);
                    continue;
                }



                try
                {
                    EMPTProc1.AbreTrasacaoSQL("ContaPagarProc LoteRepasse - 124", DLoteRepasse.FATuramentoTableAdapter);
                    if ((Fornecedor == null) || (Fornecedor.FRN != rowREF.NOA_FRN))
                        Fornecedor = AbstratosNeon.ABS_Fornecedor.GetFornecedor(rowREF.NOA_FRN);
                    decimal TotalGeral = rowREF.NOATotal;
                    int? NOA_CCT = null;
                    if (!rowREF.IsNOA_CCTNull())
                        NOA_CCT = rowREF.NOA_CCT;
                    if (!rowImp.IsAdministrativasNull())
                    {
                        dNOtAs1.IncluiNotaSimples_Fornecedor(0, rowREF.NOADataEmissao, rowREF.PAGVencimento, Comp, NOATipo.Repasse, rowImp.CON, (PAGTipo)rowREF.PAGTipo, true, PadraoPLA.PLAPadraoCodigo(PLAPadrao.RepasseDespAdmin), rowREF.NOA_SPL, "Despesas Administrativas", rowImp.Administrativas, Fornecedor, false, null, null, NOA_CCT, NOAStatus.Cadastrada);
                        TotalGeral += rowImp.Administrativas;
                    }
                    if (!rowImp.IsimpressosNull())
                    {
                        dNOtAs1.IncluiNotaSimples_Fornecedor(0, rowREF.NOADataEmissao, rowREF.PAGVencimento, Comp, NOATipo.Repasse, rowImp.CON, (PAGTipo)rowREF.PAGTipo, true, PadraoPLA.PLAPadraoCodigo(PLAPadrao.RepasseImpressos), rowREF.NOA_SPL, "Impressos", rowImp.impressos, Fornecedor, false, null, null, NOA_CCT, NOAStatus.Cadastrada);
                        TotalGeral += rowImp.impressos;
                    }
                    if (!rowImp.IsMalotesNull())
                    {
                        dNOtAs1.IncluiNotaSimples_Fornecedor(0, rowREF.NOADataEmissao, rowREF.PAGVencimento, Comp, NOATipo.Repasse, rowImp.CON, (PAGTipo)rowREF.PAGTipo, true, PadraoPLA.PLAPadraoCodigo(PLAPadrao.RepasseConducao), rowREF.NOA_SPL, "Condução", rowImp.Malotes, Fornecedor, false, null, null, NOA_CCT, NOAStatus.Cadastrada);
                        TotalGeral += rowImp.Malotes;
                    }
                    if (!rowImp.IsXeroxNull())
                    {
                        dNOtAs1.IncluiNotaSimples_Fornecedor(0, rowREF.NOADataEmissao, rowREF.PAGVencimento, Comp, NOATipo.Repasse, rowImp.CON, (PAGTipo)rowREF.PAGTipo, true, PadraoPLA.PLAPadraoCodigo(PLAPadrao.RepasseCopias), rowREF.NOA_SPL, "Cópias e impressos", rowImp.Xerox, Fornecedor, false, null, null, NOA_CCT, NOAStatus.Cadastrada);
                        TotalGeral += rowImp.Xerox;
                    }
                    NotaProc NotaMae = new NotaProc(rowREF.NOA);
                    NotaMae.AjustaRetencoesFaturamento(TotalGeral);
                    dLoteRepasse.FATuramentoRow rowFAT = DLoteRepasse.FATuramento.NewFATuramentoRow();
                    rowFAT.FATComp = Comp.CompetenciaBind;
                    rowFAT.FAT_CON = rowImp.CON;
                    DLoteRepasse.FATuramento.AddFATuramentoRow(rowFAT);
                    DLoteRepasse.FATuramentoTableAdapter.Update(rowFAT);
                    EMPTProc1.Commit();
                }
                catch (Exception ex)
                {
                    EMPTProc1.Vircatch(ex);
                    throw new Exception("Erro gerando cheques de repasse", ex);
                }
            };

            return true;


        }
    }
}
