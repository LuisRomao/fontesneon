﻿/*
MR - 26/12/2014 10:00 -           - Declarado PAGamentosTableAdapter como "public static" 
*/

//using dllVirEnum;
using System;
using System.Collections.Generic;
using System.Data;
using VirEnumeracoesNeon;
using FrameworkProc;
using Framework.objetosNeon;

namespace ContaPagarProc.Follow
{


    partial class dPagamentosPeriodicos : IEMPTProc
    {
        /// <summary>
        /// 
        /// </summary>
        public bool Coletivo;
        /// <summary>
        /// 
        /// </summary>
        public bool NotasCarregadas;

        #region estáticos
        private static dPagamentosPeriodicos _dPagamentosPeriodicosSt;
        private static dPagamentosPeriodicos _dPagamentosPeriodicosStF;

        /// <summary>
        /// dataset estático:dPagamentosPeriodicos
        /// </summary>
        public static dPagamentosPeriodicos dPagamentosPeriodicosSt
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("Uso de objeto estático no servidor de processos dPagamentosPeriodicos.cs 31");
                if (_dPagamentosPeriodicosSt == null)
                    _dPagamentosPeriodicosSt = new dPagamentosPeriodicos();
                _dPagamentosPeriodicosSt.Coletivo = true;
                return _dPagamentosPeriodicosSt;
            }
        }

        /// <summary>
        /// dataset estático:dPagamentosPeriodicos Filial
        /// </summary>
        public static dPagamentosPeriodicos dPagamentosPeriodicosStF
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("Uso de objeto estático no servidor de processos dPagamentosPeriodicos.cs 47");
                if (_dPagamentosPeriodicosStF == null)
                {
                    _dPagamentosPeriodicosStF = new dPagamentosPeriodicos(new EMPTProc(FrameworkProc.EMPTipo.Filial));
                }
                _dPagamentosPeriodicosStF.Coletivo = true;
                return _dPagamentosPeriodicosStF;
            }
        }

        /// <summary>
        /// dataset estático:dPagamentosPeriodicos Local/Filial
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public static dPagamentosPeriodicos dPagamentosPeriodicosStX(EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTipo.Local ? dPagamentosPeriodicosSt : dPagamentosPeriodicosStF;
        }

        #endregion

        /// <summary>
        /// Construitor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc"></param>
        public dPagamentosPeriodicos(EMPTProc _EMPTProc)
            : this()
        {
            if (_EMPTProc != null)
                this._EMPTProc1 = _EMPTProc;
        }

        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        #region TableAdapters
        private dPagamentosPeriodicosTableAdapters.PaGamentosFixosTableAdapter paGamentosFixosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PaGamentosFixos
        /// </summary>
        public dPagamentosPeriodicosTableAdapters.PaGamentosFixosTableAdapter PaGamentosFixosTableAdapter
        {
            get
            {
                if (paGamentosFixosTableAdapter == null)
                {
                    paGamentosFixosTableAdapter = new dPagamentosPeriodicosTableAdapters.PaGamentosFixosTableAdapter();
                    paGamentosFixosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return paGamentosFixosTableAdapter;
            }
        }

        private dPagamentosPeriodicosTableAdapters.NOtAsTableAdapter nOtAsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOtAs
        /// </summary>
        public dPagamentosPeriodicosTableAdapters.NOtAsTableAdapter NOtAsTableAdapter
        {
            get
            {
                if (nOtAsTableAdapter == null)
                {
                    nOtAsTableAdapter = new dPagamentosPeriodicosTableAdapters.NOtAsTableAdapter();
                    nOtAsTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return nOtAsTableAdapter;
            }
        }

        private dPagamentosPeriodicosTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dPagamentosPeriodicosTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dPagamentosPeriodicosTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return pAGamentosTableAdapter;
            }
        }

        private dPagamentosPeriodicosTableAdapters.DOCumentacaoTableAdapter dOCumentacaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DOCumentacao
        /// </summary>
        public dPagamentosPeriodicosTableAdapters.DOCumentacaoTableAdapter DOCumentacaoTableAdapter
        {
            get
            {
                if (dOCumentacaoTableAdapter == null)
                {
                    dOCumentacaoTableAdapter = new dPagamentosPeriodicosTableAdapters.DOCumentacaoTableAdapter();
                    dOCumentacaoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return dOCumentacaoTableAdapter;
            }
        }

        private dPagamentosPeriodicosTableAdapters.AGendaBaseTableAdapter aGendaBaseTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: AGendaBase
        /// </summary>
        public dPagamentosPeriodicosTableAdapters.AGendaBaseTableAdapter AGendaBaseTableAdapter
        {
            get
            {
                if (aGendaBaseTableAdapter == null)
                {
                    aGendaBaseTableAdapter = new dPagamentosPeriodicosTableAdapters.AGendaBaseTableAdapter();
                    aGendaBaseTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return aGendaBaseTableAdapter;
            }
        }

        #endregion

        #region DataView
        private DataView dV_PaGamentosFixos_PGFForma_PGF_CON;

        private DataView DV_PaGamentosFixos_PGFForma_PGF_CON
        {
            get
            {
                if (dV_PaGamentosFixos_PGFForma_PGF_CON == null)
                {
                    dV_PaGamentosFixos_PGFForma_PGF_CON = new DataView(PaGamentosFixos);
                    dV_PaGamentosFixos_PGFForma_PGF_CON.Sort = "PGFForma,PGF_CON";
                };
                return dV_PaGamentosFixos_PGFForma_PGF_CON;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PGFForma"></param>
        /// <param name="PGF_CON"></param>
        /// <returns></returns>
        public PaGamentosFixosRow Busca_rowPaGamentosFixos(int PGFForma, int PGF_CON)
        {
            int ind;
            ind = DV_PaGamentosFixos_PGFForma_PGF_CON.Find(new object[] { PGFForma, PGF_CON });
            return (ind < 0) ? null : (PaGamentosFixosRow)(DV_PaGamentosFixos_PGFForma_PGF_CON[ind].Row);
        }

        private DataView dV_NOtAs_NOACompet_NOA_PGF;

        private DataView DV_NOtAs_NOACompet_NOA_PGF
        {
            get
            {
                if (dV_NOtAs_NOACompet_NOA_PGF == null)
                {
                    dV_NOtAs_NOACompet_NOA_PGF = new DataView(NOtAs);
                    dV_NOtAs_NOACompet_NOA_PGF.Sort = "NOACompet DESC,NOA_PGF";
                };
                return dV_NOtAs_NOACompet_NOA_PGF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NOACompet"></param>
        /// <param name="NOA_PGF"></param>
        /// <returns></returns>
        public NOtAsRow Busca_rowNOtAs(int NOACompet, int NOA_PGF)
        {
            int ind = DV_NOtAs_NOACompet_NOA_PGF.Find(new object[] { NOACompet, NOA_PGF });
            return (ind < 0) ? null : (NOtAsRow)(DV_NOtAs_NOACompet_NOA_PGF[ind].Row);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NOA_PGF"></param>
        /// <returns></returns>
        public NOtAsRow UltimaNota(int NOA_PGF)
        {
            string FiltroAnterior = DV_NOtAs_NOACompet_NOA_PGF.RowFilter;
            try
            {
                DV_NOtAs_NOACompet_NOA_PGF.RowFilter = string.Format("NOA_PGF = {0}", NOA_PGF);
                if (DV_NOtAs_NOACompet_NOA_PGF.Count == 0)
                    return null;
                else if (DV_NOtAs_NOACompet_NOA_PGF.Count == 1)
                    return (NOtAsRow)DV_NOtAs_NOACompet_NOA_PGF[0].Row;
                else
                {
                    DV_NOtAs_NOACompet_NOA_PGF.RowFilter = string.Format("NOA_PGF = {0} and NOACompet = {1}", NOA_PGF, ((NOtAsRow)DV_NOtAs_NOACompet_NOA_PGF[0].Row).NOACompet);
                    if (DV_NOtAs_NOACompet_NOA_PGF.Count == 1)
                        return (NOtAsRow)DV_NOtAs_NOACompet_NOA_PGF[0].Row;
                    else
                    {
                        SortedList<int, NOtAsRow> Referencias = new SortedList<int, NOtAsRow>();
                        List<NotaProc> Candidatos = new List<NotaProc>();
                        for (int i = 0; i < DV_NOtAs_NOACompet_NOA_PGF.Count; i++)
                        {
                            NOtAsRow row = (NOtAsRow)DV_NOtAs_NOACompet_NOA_PGF[i].Row;
                            Referencias.Add(row.NOA, row);
                            Candidatos.Add(new NotaProc(row.NOA, EMPTProc1));
                        }
                        int NOArep = NotaRepresentativa(Candidatos);
                        return Referencias[NOArep];
                    }
                }
            }
            finally
            {
                DV_NOtAs_NOACompet_NOA_PGF.RowFilter = FiltroAnterior;
            }
        }

        internal static int NotaRepresentativa(List<NotaProc> Notas)
        {
            NotaProc Retorno = null;
            List<NotaProc> Provisorias = new List<NotaProc>();
            FasePeriodico FaseRetorno = FasePeriodico.ComNotaprov;
            foreach (NotaProc Nota in Notas)
            {
                if (Nota.Status == NOAStatus.NotaProvisoria)
                    Provisorias.Add(Nota);
                else
                {
                    if (Retorno == null)
                    {
                        FaseRetorno = Nota.VerificaFase();
                        Retorno = Nota;
                    }
                    else
                    {
                        switch (Nota.VerificaFase())
                        {
                            case FasePeriodico.AguardaEmissaoCheque:
                                if (FaseRetorno == FasePeriodico.CadastrarProxima)
                                {
                                    FaseRetorno = FasePeriodico.AguardaEmissaoCheque;
                                    Retorno = Nota;
                                }
                                break;
                            case FasePeriodico.CadastrarProxima:
                                break;
                            case FasePeriodico.ComNotaprov:
                                throw new Exception("Resultado inesperado em dPagamentosPeriodicos 299");
                        }
                    }
                }
            }
            if (Retorno == null)
            {
                for (int i = 1; i < Provisorias.Count; i++)
                    Provisorias[i].Matar();
                return Provisorias[0].NOA;
            }
            for (int i = 0; i < Provisorias.Count; i++)
                Provisorias[i].Matar();
            return Retorno.NOA;
        }

        #endregion                               

        internal int CarregaNotas(int PGF)
        {
            if (Coletivo)
                throw new Exception("Carga de notas individuais em conjunto de dados coletivo");
            NotasCarregadas = true;
            EMPTProc1.EmbarcaEmTrans(NOtAsTableAdapter);
            return NOtAsTableAdapter.Fill(NOtAs, PGF);
        }

        /*
        /// <summary>
        /// Evita que os dados das notas geradas seja carregados no datase. é usado para melhoria de performance.Recomendado o usu de try finally
        /// </summary>
        public bool TravarRecarga = false;
        */

        /// <summary>
        /// Carrega os ativos
        /// </summary>
        /// <returns></returns>
        public int CarregaAtivos(bool ComNotas)
        {
            Coletivo = true;
            NOtAs.Clear();
            PaGamentosFixosTableAdapter.FillAtivosRed(PaGamentosFixos, EMPTProc1.EMP);
            if (ComNotas)
            {
                NOtAsTableAdapter.FillAtivos(NOtAs, EMPTProc1.EMP);
                NotasCarregadas = true;
            }
            else
                NotasCarregadas = false;
            return PaGamentosFixos.Count;
        }

        /// <summary>
        /// Carrega os Todos
        /// </summary>
        /// <returns></returns>
        public int CarregaTodos(bool ComNotas)
        {
            Coletivo = true;
            NOtAs.Clear();
            PaGamentosFixosTableAdapter.FillTodosRed(PaGamentosFixos, EMPTProc1.EMP);
            if (ComNotas)
            {
                NOtAsTableAdapter.FillTodos(NOtAs, EMPTProc1.EMP);
                NotasCarregadas = true;
            }
            else
                NotasCarregadas = false;
            return PaGamentosFixos.Count;
        }

        /// <summary>
        /// Carrega os ativos
        /// </summary>
        /// <returns></returns>
        public int CarregaAtivos(TipoPagPeriodico Tipo, Competencia CompCorte = null)
        {
            NOtAs.Clear();
            AGendaBase.Clear();
            int retorno = PaGamentosFixosTableAdapter.FillByFolhaPool(PaGamentosFixos, (int)Tipo);
            if (CompCorte != null)
                NOtAsTableAdapter.FillByFormaComp(NOtAs, (int)Tipo, CompCorte.CompetenciaBind);
            AGendaBaseTableAdapter.FillByForma(AGendaBase, (int)Tipo);
            return retorno;
        }
    }
}
