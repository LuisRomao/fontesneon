﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AbstratosNeon;
using VirEnumeracoesNeon;
using Framework.objetosNeon;
using FrameworkProc;
using ContaPagarProc.Follow;
using VirMSSQL;
using dllChequesProc;
using CompontesBasicosProc;
using VirEnumeracoes;

namespace ContaPagarProc
{
    /// <summary>
    /// Objeto PagamentoPeriodico somente processos (Abstrato)
    /// </summary>
    public class PagamentoPeriodicoProc : ABS_PagamentoPeriodico, IEMPTProc
    {
        /// <summary>
        /// Indica se o dataset é coletivo ou só para este PGF
        /// </summary>
        //protected bool datasetColetivo = false;

        private void MatarNota(dPagamentosPeriodicos.NOtAsRow NOArow)
        {
            NotaProc NotaMatar = new NotaProc(NOArow.NOA);
            NotaMatar.Matar();
        }

        /*
        public bool CorrecaoEmegencial()
        {
            bool retorno = false;
            SortedList<int, dPagamentosPeriodicos.NOtAsRow> provisorias = new SortedList<int, dPagamentosPeriodicos.NOtAsRow>();
            List<int> CompetenciasUsadas = new List<int>();
            int MaiorCompetencia = 0;
            if (dPagamentosPeriodicos.NOtAs.Count == 0)
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGF);
            foreach (dPagamentosPeriodicos.NOtAsRow NOArow in dPagamentosPeriodicos.NOtAs)
            {
                if (NOArow.NOAStatus != 5)
                {
                    if (MaiorCompetencia < NOArow.NOACompet)
                        MaiorCompetencia = NOArow.NOACompet;
                    if (!CompetenciasUsadas.Contains(NOArow.NOACompet))
                        CompetenciasUsadas.Add(NOArow.NOACompet);
                    if (provisorias.ContainsKey(NOArow.NOACompet))
                    {
                        MatarNota(provisorias[NOArow.NOACompet]);
                        retorno = true;
                        provisorias.Remove(NOArow.NOACompet);
                    }
                }
                else
                {
                    if (CompetenciasUsadas.Contains(NOArow.NOACompet) || provisorias.ContainsKey(NOArow.NOACompet))
                    {
                        MatarNota(NOArow);
                        retorno = true;
                    }
                    else
                        provisorias.Add(NOArow.NOACompet, NOArow);
                }                
            }
            if (provisorias.Count > 1)
                for(int i = 1;i < provisorias.Count;i++)
                     MatarNota(provisorias.Values[i]);
            Console.WriteLine(string.Format("{0} - {1}", PGF, retorno));
            return retorno;
        }*/

        /// <summary>
        /// 
        /// </summary>
        public override StatusPagamentoPeriodico Status
        {
            get
            {
                if (!Encontrado)
                    return StatusPagamentoPeriodico.Inativo;
                else
                    return (StatusPagamentoPeriodico)PGFrow.PGFStatus;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual TipoImposto TipoISS {
            get
            {                
                return EMPTProc1.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA;
            }
        }        
        
        

        /// <summary>
        /// Busca/Cadastra nota do periodico com esta competencia
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="PodeCriar"></param>
        /// <returns></returns>
        public NotaProc BuscaCriaNotaProc(Competencia comp, bool PodeCriar = true)
        {
            if ((dPagamentosPeriodicos.NOtAs.Count == 0) && (!dPagamentosPeriodicos.Coletivo))
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGF);
            dPagamentosPeriodicos.NOtAsRow rowNOA = dPagamentosPeriodicos.Busca_rowNOtAs(comp.CompetenciaBind, PGF);
            if (rowNOA != null)
                return new NotaProc(rowNOA.NOA, EMPTProc1);
            else
                if (PodeCriar)
                    return new NotaProc(PGFrow.PGF_CON,
                                null,
                                0,
                                DateTime.Now,
                                0,
                                NOATipo.Folha,
                                PLA,
                                PGFrow.PGF_SPL,
                                PGFrow.PGFDescricao,
                                comp,
                                "",
                                "",
                                PAGTipo.Folha,
                                NOAStatus.SemPagamentos,
                                null,
                                null,
                                PGF,
                                EMPTProc1);
            else
                return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        protected string DescricaoInterna(Competencia comp = null)
        {
            if (comp == null)
                comp = new Competencia(PGFrow.PGFProximaCompetencia);
            string retorno = PGFrow.PGFDescricao;
            int tamanhoOriginal = retorno.Length;
            if ((PGFrow.PGFParcelaNaDesc) && (!PGFrow.IsPGFCompetFNull()))
                retorno += string.Format(" ({0}/{1})", 1 + comp.DeltaMeses(PGFrow.PGFCompetI), 1 + Competencia.DeltaMeses(PGFrow.PGFCompetI, PGFrow.PGFCompetF));
            if (PGFrow.PGFCompetNaDescricao)
                retorno += string.Format(" ref.{0}", comp);
            if (retorno.Length > 50)
            {
                int remover = retorno.Length - 50;
                retorno = retorno.Remove(tamanhoOriginal - remover - 1, remover);
            }
            return retorno;

        }

        private DocBacarios.CPFCNPJ _cpfcnpj;

        /// <summary>
        /// 
        /// </summary>
        protected DocBacarios.CPFCNPJ cpfcnpj
        {
            get
            {
                if (_cpfcnpj == null)
                {
                    if (PGFrow.IsPGF_FRNNull())
                        return null;
                    _cpfcnpj = new DocBacarios.CPFCNPJ(EMPTProc1.STTA.BuscaEscalar_string("select FRNCNPJ from FORNECEDORES where FRN = @P1", PGFrow.PGF_FRN) ?? "");
                }
                return _cpfcnpj;
            }
        }

        /// <summary>
        /// Próximo vencimento
        /// </summary>
        public DateTime ProximaData
        {
            get
            {
                Competencia CompPag = ProximaCompetencia.Add(PGFrow.PGFRef);
                return CompPag.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);
            }
        }

        #region Construtores
        /// <summary>
        /// 
        /// </summary>
        public PagamentoPeriodicoProc()
        {
            //datasetColetivo = false;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="TipoPadrao"></param>
        /// <param name="_EMPTProc"></param>
        /// <param name="usarPool"></param>
        public PagamentoPeriodicoProc(int CON, TipoPagPeriodico TipoPadrao, EMPTProc _EMPTProc = null, bool usarPool = false)
        {
            //datasetColetivo = usarPool;            
            EMPTProc1 = _EMPTProc ?? new EMPTProc(EMPTipo.Local);
            TipoPagPeriodico TipoAlternativo = TipoPadrao;
            switch (TipoPadrao)
            {
                case TipoPagPeriodico.FolhaPIS:
                    TipoAlternativo = TipoPagPeriodico.FolhaPISEletronico;
                    break;
                case TipoPagPeriodico.FolhaFGTS:
                    TipoAlternativo = TipoPagPeriodico.FolhaFGTSEletronico;
                    break;
                case TipoPagPeriodico.Folha:
                case TipoPagPeriodico.Adiantamento:
                    TipoAlternativo = TipoPagPeriodico.FolhaEletronico;
                    break;
                case TipoPagPeriodico.FolhaIR:
                    TipoAlternativo = TipoPagPeriodico.FolhaIREletronico;
                    break;
            }
            EMPTProc1.EmbarcaEmTrans(dPagamentosPeriodicos.PaGamentosFixosTableAdapter);
            if (usarPool)
            {
                dPagamentosPeriodicos = EMPTProc1.Tipo == FrameworkProc.EMPTipo.Local ? dPagamentosPeriodicos.dPagamentosPeriodicosSt : dPagamentosPeriodicos.dPagamentosPeriodicosStF;
                PGFrow = dPagamentosPeriodicos.Busca_rowPaGamentosFixos((int)TipoPadrao, CON);
                if (PGFrow == null)
                    PGFrow = dPagamentosPeriodicos.Busca_rowPaGamentosFixos((int)TipoAlternativo, CON);
            }
            else
            {                
                if (dPagamentosPeriodicos.PaGamentosFixosTableAdapter.FillByFolha(dPagamentosPeriodicos.PaGamentosFixos, (int)TipoPadrao, CON) >= 1)
                    PGFrow = dPagamentosPeriodicos.PaGamentosFixos[0];
                else
                {
                    if (dPagamentosPeriodicos.PaGamentosFixosTableAdapter.FillByFolha(dPagamentosPeriodicos.PaGamentosFixos, (int)TipoAlternativo, CON) >= 1)
                        PGFrow = dPagamentosPeriodicos.PaGamentosFixos[0];
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PGF"></param>
        /// <param name="usarPool"></param>
        /// <param name="objEMPTProc">EMPTProc tipado como objeto</param>        
        /// <remarks>Manter a assinatura com objeto para construção vai abstrato</remarks>
        public PagamentoPeriodicoProc(int PGF, bool usarPool = false, object objEMPTProc = null)
        {
            EMPTProc _EMPTProc = (EMPTProc)objEMPTProc;
            if (TableAdapter.Servidor_De_Processos)
                usarPool = false;
            //datasetColetivo = usarPool;
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;// new EMPTProc(_EMPTipo);                        
            PGFrow = null;
            if (usarPool)
                PGFrow = dPagamentosPeriodicos.dPagamentosPeriodicosSt.PaGamentosFixos.FindByPGF(PGF);
            if (PGFrow == null)
            {
                EMPTProc1.EmbarcaEmTrans(dPagamentosPeriodicos.PaGamentosFixosTableAdapter);
                //dPagamentosPeriodicos.PaGamentosFixosTableAdapter.EmbarcaEmTransST();
                if (dPagamentosPeriodicos.PaGamentosFixosTableAdapter.FillByPGF(dPagamentosPeriodicos.PaGamentosFixos, PGF) == 1)
                    PGFrow = dPagamentosPeriodicos.PaGamentosFixos[0];

            }
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        public enum TiposAcumulado
        {
            /// <summary>
            /// 
            /// </summary>
            SemAcumulado,
            /// <summary>
            /// 
            /// </summary>
            AcumuladoComValor,
            /// <summary>
            /// 
            /// </summary>
            AcumuladoSemValor,
        }


        /// <summary>
        /// 
        /// </summary>
        [Obsolete("Descontinuado o uso de acumulado")]
        public TiposAcumulado? _TipoAcumulado;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Obsolete("Descontinuado o uso de acumulado")]        
        public TiposAcumulado TemAcumulado()
        {
            return TiposAcumulado.SemAcumulado;
            /*
            if (_TipoAcumulado == null)
                LevantaValorAcumulado();
            return _TipoAcumulado.Value;
            */
        }

        //private decimal? _ValorAcumulado;

        /// <summary>
        /// 
        /// </summary>
        public List<NotaProc> PAGsAcumular;

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Obsolete("Descontinuado o uso de acumulado")]        
        public decimal ValAcumulado()
        {
            return 0;
            
        }*/

        /*
        [Obsolete("Descontinuado o uso de acumulado")]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataPag"></param>
        /// <param name="Travada"></param>
        /// <param name="ValorMedio"></param>
        public void ReagendaAcumulados(DateTime DataPag, bool Travada, decimal ValorMedio)
        {          
            if (TemAcumulado() != TiposAcumulado.SemAcumulado)
                foreach (NotaProc Notaac in PAGsAcumular)
                    Notaac.AjustaChequeAcumular(DataPag, Travada, ValorMedio);
        }*/

        /*
        private decimal LevantaValorAcumulado()
        {
            _TipoAcumulado = TiposAcumulado.SemAcumulado;
            _ValorAcumulado = 0;
            PAGsAcumular = new List<NotaProc>();
            if (dPagamentosPeriodicos.NOtAs.Count == 0)
            {
                EMPTProc1.EmbarcaEmTrans(dPagamentosPeriodicos.NOtAsTableAdapter, dPagamentosPeriodicos.PAGamentosTableAdapter);
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
                dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
            }
            foreach (dPagamentosPeriodicos.NOtAsRow NOArow in dPagamentosPeriodicos.NOtAs)
            {
                if (NOArow.NOAStatus == (int)NOAStatus.NotaProvisoria)
                    continue;
                if (NOArow.GetPAGamentosRows().Length != 1)
                    continue;
                if (NOArow.GetPAGamentosRows()[0].IsPAG_CHENull())
                    continue;
                if (!ChequeProc.Editavel((CHEStatus)NOArow.GetPAGamentosRows()[0].CHEStatus))
                    continue;
                NotaProc Nota1 = new NotaProc(NOArow.NOA,EMPTProc1);
                switch (Nota1.Status)
                {
                    case NOAStatus.NotaAcumulada:
                        if (_TipoAcumulado == TiposAcumulado.SemAcumulado)
                            _TipoAcumulado = TiposAcumulado.AcumuladoComValor;
                        break;
                    case NOAStatus.CadastradaAjustarValor:
                        _TipoAcumulado = TiposAcumulado.AcumuladoSemValor;
                        break;
                    case NOAStatus.Cadastrada:
                    case NOAStatus.Parcial:
                    case NOAStatus.Liquidada:
                    case NOAStatus.SemPagamentos:
                    case NOAStatus.NotaCancelada:
                    case NOAStatus.NotaProvisoria:
                        continue;
                    case NOAStatus.NaoEncontrada:
                    default:
                        throw new NotImplementedException(string.Format("Acumulado para nota tipo: {0}", Nota1.Status));
                }
                _ValorAcumulado += NOArow.GetPAGamentosRows()[0].PAGValor;
                PAGsAcumular.Add(Nota1);
            }
            return _ValorAcumulado.Value;
        }*/

        /// <summary>
        /// Forma de pagamento do periódico
        /// </summary>
        public TipoPagPeriodico PGFForma
        {
            get { return (TipoPagPeriodico)PGFrow.PGFForma; }
        }

        [Obsolete("Remover após a revisão de todos")]
        protected List<TipoPagPeriodico> FormasRevisadas
        {
            get
            {
                return new List<TipoPagPeriodico>(new TipoPagPeriodico[] { TipoPagPeriodico.Boleto,
                                                                           TipoPagPeriodico.Cheque,
                                                                           TipoPagPeriodico.ChequeSindico,
                                                                           TipoPagPeriodico.ChequeDeposito,
                                                                           TipoPagPeriodico.PECreditoConta,
                                                                           TipoPagPeriodico.Conta,
                                                                           TipoPagPeriodico.DebitoAutomatico,
                                                                           TipoPagPeriodico.DebitoAutomaticoConta,
                                                                           TipoPagPeriodico.Folha,
                                                                           TipoPagPeriodico.Adiantamento,
                                                                           TipoPagPeriodico.FolhaIR,
                                                                           TipoPagPeriodico.FolhaPIS});
            }
        }

        /*
        private string ComandoBuscaCheque =
"SELECT     TOP (1) CHEques.CHEStatus\r\n" +
"FROM         NOtAs INNER JOIN\r\n" +
"                      PAGamentos ON NOtAs.NOA = PAGamentos.PAG_NOA INNER JOIN\r\n" +
"                      CHEques ON PAGamentos.PAG_CHE = CHEques.CHE\r\n" +
"WHERE     (NOtAs.NOA_PGF = @P1) AND (PAGamentos.PAGIMP IS NULL) AND (CHEques.CHEEmissao IS NULL)\r\n" +
"AND (CHEques.CHEStatus not in (9))" +
"ORDER BY NOtAs.NOADataEmissao DESC;";

        /// <summary>
        /// Cadastra o próximo cheque
        /// </summary>
        /// <param name="NotaCheque"></param>
        /// <returns>Retorna falso só se der erro. Se não tiver nada para cadastrar retorna true</returns>
        [Obsolete("substitur por CadastraProximaNota")]
        protected bool CadastrarProximoCheque(enumNotaCheque NotaCheque)
        {
            //ATENÇÃO
            //Esta função deve ser descontinuada e substituída para CadastraProximaNota sendo feita a revisão para cada PGFrow.PGFForma 

            if (FormasRevisadas.Contains(PGFForma))
                throw new Exception("Forma já revisada");

            if (PGFrow.PGFValor <= 0)
                return false;
            Competencia Comp = ProximaCompetencia;
            Competencia CompPAG = Comp.CloneCompet(PGFrow.PGFRef);
            System.Data.DataRow Ultima = EMPTProc1.STTA.BuscaSQLRow(ComandoBuscaCheque, PGFrow.PGF);
            if (Ultima != null)
                return true;

            DateTime DataPag = CompPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);

            int FRN = PGFrow.IsPGF_FRNNull() ? 0 : PGFrow.PGF_FRN;
            System.Collections.ArrayList Retencoes = new System.Collections.ArrayList();
            if (!PGFrow.IsPGFISSNull())
            {
                //TipoImposto TI = TipoISS;
                //Console.WriteLine("Ponto 1 Pagamentosperiodicosproc linha 422");
                //SolicitaRetencao Solicitacao = new SolicitaRetencao(TI, PGFrow.PGFISS, CompontesBasicosProc.SimNao.Padrao);
                //Retencoes.Add(Solicitacao);
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoISS, PGFrow.PGFISS, CompontesBasicosProc.SimNao.Padrao));
            }
            if (!PGFrow.IsPGFINSSPNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, PGFrow.PGFINSSP, CompontesBasicosProc.SimNao.Padrao));
            if (!PGFrow.IsPGFINSSTNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, PGFrow.PGFINSST, CompontesBasicosProc.SimNao.Padrao));
            if (cpfcnpj != null)
            {
                if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                {
                    if (!PGFrow.IsPGFIRNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IR, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
                }
                else
                {
                    if (!PGFrow.IsPGFIRNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IRPF, PGFrow.PGFIR, CompontesBasicosProc.SimNao.Padrao));
                }
            }
            if (!PGFrow.IsPGFPISCOFNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.PIS_COFINS_CSLL, PGFrow.PGFPISCOF, CompontesBasicosProc.SimNao.Padrao));
            string Nominal = PGFrow.IsPGFNominalNull() ? "" : PGFrow.PGFNominal;
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
            if (!PGFrow.IsPGF_CCTCredNull())
                Nominal = NominalPagEletronico;
            //*** MRC - TERMINO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
            try
            {
                EMPTProc1.AbreTrasacaoSQL("ContaPagar PagamentoPeriodico - 864",
                                                                       dPagamentosPeriodicos.PaGamentosFixosTableAdapter,
                                                                       dPagamentosPeriodicos.NOtAsTableAdapter,
                                                                       dPagamentosPeriodicos.PAGamentosTableAdapter);
                //*** MRC - INICIO - PAG-FOR (2) ***
                PAGTipo PAGTipo;
                string DadosPag = "";
                int CCT = 0;
                bool booPermiteAgrupar = true;

                switch ((TipoPagPeriodico)PGFrow.PGFForma)
                {
                    case TipoPagPeriodico.ChequeDeposito:
                        PAGTipo = PAGTipo.deposito;
                        DadosPag = PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag;
                        break;
                    case TipoPagPeriodico.ChequeSindico:
                        PAGTipo = PAGTipo.sindico;
                        break;
                    case TipoPagPeriodico.Boleto:
                    case TipoPagPeriodico.Cheque:
                    case TipoPagPeriodico.Conta:
                    case TipoPagPeriodico.DebitoAutomatico:
                    case TipoPagPeriodico.DebitoAutomaticoConta:
                    case TipoPagPeriodico.Folha:
                    case TipoPagPeriodico.Adiantamento:
                    case TipoPagPeriodico.FolhaEletronico:
                    case TipoPagPeriodico.FolhaFGTS:
                    case TipoPagPeriodico.FolhaFGTSEletronico:
                    case TipoPagPeriodico.FolhaIR:
                    case TipoPagPeriodico.FolhaIREletronico:
                    case TipoPagPeriodico.FolhaPIS:
                    case TipoPagPeriodico.FolhaPISEletronico:
                        PAGTipo = PAGTipo.cheque;
                        break;
                    case TipoPagPeriodico.PEOrdemPagamento:
                    case TipoPagPeriodico.PETituloBancario:
                    case TipoPagPeriodico.PETituloBancarioAgrupavel:
                    case TipoPagPeriodico.PETransferencia:
                        PAGTipo = (PAGTipo)(PGFrow.PGFForma);
                        if ((TipoPagPeriodico)PGFrow.PGFForma == TipoPagPeriodico.PEOrdemPagamento)
                            DadosPag = PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag;
                        CCT = PGFrow.PGF_CCT;
                        booPermiteAgrupar = false;
                        break;
                    //*** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                    case TipoPagPeriodico.PECreditoConta:
                        PAGTipo = (PAGTipo)(PGFrow.PGFForma);
                        if (!PGFrow.IsPGF_CCTCredNull())
                        {
                            DadosPag = DadosPagEl();
                            
                        }
                        CCT = PGFrow.PGF_CCT;
                        booPermiteAgrupar = true;
                        break;
                    //*** MRC - TERMINO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***
                    default:
                        throw new NotImplementedException(string.Format("Não implementado tratamento para TipoPagPeriodico = {0} em PagamentosPeriodico.cs", (TipoPagPeriodico)PGFrow.PGFForma));
                }



                //ContaPagar.dNOtAs.dNOtAsStX(EMPTipo).IncluiNotaSimples(0, DataPag, DataPag,Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, DadosPag, true, PGFrow.PGF_PLA,PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN, false, dNOtAs.TipoChave.PGF, PGFrow.PGF, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                if (NotaCheque == enumNotaCheque.aguarda)
                    DNOtAs.IncluiNotaProvisoria(DataPag, Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, booPermiteAgrupar, PGFrow.PGF_PLA, PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN, dNOtAs.TipoChave.PGF, PGFrow.PGF, DadosPag, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                else
                    DNOtAs.IncluiNotaSimples(0, DataPag, DataPag, Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, DadosPag, booPermiteAgrupar, PGFrow.PGF_PLA, PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN, false, dNOtAs.TipoChave.PGF, PGFrow.PGF, CCT, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));

                Comp++;
                PGFrow.PGFProximaCompetencia = Comp.CompetenciaBind;
                PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} Cheque emitido, cadastrado próximo ({2}) valor: {3:n2} vencimento {4:dd/MM/yyyy}",
                                                       DateTime.Now,
                                                       EMPTProc1.USUNome,
                                                       ProximaCompetencia,
                                                       PGFrow.PGFValor,
                                                       ProximaData);

                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                PGFrow.AcceptChanges();

                dPagamentosPeriodicos.PAGamentos.Clear();
                dPagamentosPeriodicos.NOtAs.Clear();
                dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
                dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro re-gerado" + e.Message, e);
            }
            return true;
        }*/

        private dNOtAs _DNOtAs;

        private dNOtAs DNOtAs
        {
            get 
            {
                if (_DNOtAs == null)
                {
                    if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                        _DNOtAs = new dNOtAs(EMPTProc1);
                    else
                        _DNOtAs = dNOtAs.dNOtAsStX(EMPTProc1.Tipo);
                }                               
                return _DNOtAs;
            }
            set { _DNOtAs = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected EMPTProc eMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial (implementação de IEMPTProc)
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (eMPTProc1 == null)
                {
                    if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        eMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                }
                return eMPTProc1;
            }
            set
            {
                eMPTProc1 = value;
            }
        }

        #region dataset
        /// <summary>
        /// Dataset interno. Se não for indicado um ele cria
        /// </summary>
        private dPagamentosPeriodicos _dPagamentosPeriodicos;

        /*
        /// <summary>
        /// Linha mestre
        /// </summary>
        public dPagamentosPeriodicos.PaGamentosFixosRow PGFrow;
        */
        /// <summary>
        /// Dataset
        /// </summary>
        public dPagamentosPeriodicos dPagamentosPeriodicos
        {
            get { return _dPagamentosPeriodicos ?? (_dPagamentosPeriodicos = new dPagamentosPeriodicos(EMPTProc1)); }
            set { _dPagamentosPeriodicos = value; }
        }
        #endregion

        /// <summary>
        /// Linha mestre
        /// </summary>
        public dPagamentosPeriodicos.PaGamentosFixosRow PGFrow;

        /// <summary>
        /// Verifica se tem linha mae ativa
        /// </summary>
        public override bool Encontrado
        {
            get { return (PGFrow != null); }
        }

        /// <summary>
        /// PLA
        /// </summary>
        public override string PLA
        {
            get { return PGFrow.PGF_PLA; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Abstratos.ABS_Competencia ABS_ProximaCompetencia
        {
            get { return ProximaCompetencia; }
        }

        /// <summary>
        /// Proxima competencia esperada
        /// </summary>
        public Competencia ProximaCompetencia
        {
            get
            {
                return new Competencia(PGFrow.PGFProximaCompetencia, PGFrow.PGF_CON, null);
            }
            set
            {
                if ((PGFrow != null) && (PGFrow.PGFProximaCompetencia != value.CompetenciaBind))
                    PGFrow.PGFProximaCompetencia = value.CompetenciaBind;
            }
        }

        /// <summary>
        /// Descrição
        /// </summary>
        public override string Descricao
        {
            get { return PGFrow.PGFDescricao; }
        }

        private List<TipoPagPeriodico> _FormasContraladasNaAgenda;

        /// <summary>
        /// 
        /// </summary>
        protected List<TipoPagPeriodico> FormasContraladasNaAgenda
        {
            get
            {

                return _FormasContraladasNaAgenda ?? (_FormasContraladasNaAgenda = new List<TipoPagPeriodico>(new TipoPagPeriodico[] { TipoPagPeriodico.Boleto,
                                                                                                                                                               TipoPagPeriodico.Conta,
                                                                                                                                                               TipoPagPeriodico.DebitoAutomaticoConta}));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected dPagamentosPeriodicos.AGendaBaseRow _rowAGB;

        /// <summary>
        /// 
        /// </summary>
        protected dPagamentosPeriodicos.AGendaBaseRow rowAGB
        {
            get
            {
                _rowAGB = PGFrow.AGendaBaseRow;
                if ((_rowAGB == null) || (_rowAGB.RowState == DataRowState.Detached))
                {
                    if (!PGFrow.IsPGF_AGBNull())
                    {
                        if (dPagamentosPeriodicos.AGendaBaseTableAdapter.FillByAGB(dPagamentosPeriodicos.AGendaBase, PGFrow.PGF_AGB) > 0)
                            _rowAGB = dPagamentosPeriodicos.AGendaBase[0];
                    }
                }
                if ((_rowAGB != null) && (_rowAGB.IsAGBMemoNull()))
                    _rowAGB.AGBMemo = "";
                return _rowAGB;
            }
        }

        //protected abstract void AjustarAGBReajContrato();

        /// <summary>
        /// O contrato esta somente na AGB. Nao existe campo específico no PGF
        /// </summary>
        protected void AjustarAGBReajContrato()
        {
            //if ((ContaPagar.TipoPagPeriodico)PGFrow.PGFForma != TipoPagPeriodico.Boleto)
            //    return;
            dPagamentosPeriodicos.AGendaBaseRow AGBrow1 = null;
            if (!PGFrow.IsPGFContrato_AGBNull())
            {
                _rowAGB = null;
                dPagamentosPeriodicos.AGendaBaseTableAdapter.FillByAGB(dPagamentosPeriodicos.AGendaBase, PGFrow.PGFContrato_AGB);
                AGBrow1 = dPagamentosPeriodicos.AGendaBase[0];
            };
            bool DeveEstarAgendado = false;
            //if (StatusAtivos.Contains((StatusPagamentoPeriodico)PGFrow.PGFStatus) && (!PGFrow.IsPGFDataLimiteResNull()))
            if (StatusAtivos.Contains((StatusPagamentoPeriodico)PGFrow.PGFStatus) && (!PGFrow.IsAGBDataInicioNull()))
                DeveEstarAgendado = true;

            if (DeveEstarAgendado)
            {
                DateTime DataVenc = PGFrow.AGBDataInicio;

                if (AGBrow1 == null)
                {
                    AGBrow1 = dPagamentosPeriodicos.AGendaBase.NewAGendaBaseRow();
                    AGBrow1.AGB_CON = PGFrow.PGF_CON;
                    AGBrow1.AGB_TAG = (int)TiposTAG.Contratos;
                    int Resp10;
                    EMPTProc1.STTA.BuscaEscalar("SELECT TAGResp_USU FROM TipoAGendamento WHERE (TAG = 10)", out Resp10);
                    AGBrow1.AGB_USU = Resp10;
                    AGBrow1.AGBDATAI = DateTime.Now;
                    AGBrow1.AGBDescricao = PGFrow.PGFDescricao;
                    AGBrow1.AGBDiaTodo = true;
                    AGBrow1.AGBI_USU = EMPTProc1.USU;
                    AGBrow1.AGBMemo = string.Format("\r\n{0:dd/MM/yyyy HH:mm} - {1}\r\n    Cadastro inicial: {2:dd/MM/yyyy}",
                        DateTime.Now,
                        EMPTProc1.USUNome,
                        DataVenc);
                    AGBrow1.AGBDataTermino = AGBrow1.AGBDataInicio = DataVenc;
                }
                if (AGBrow1.AGBDataInicio != DataVenc)
                {
                    AGBrow1.AGBDataTermino = AGBrow1.AGBDataInicio = DataVenc;
                    AGBrow1.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - {1}\r\n    Data Ajustada pelo cadastro do contrato de {2:dd/MM/yyyy} para {3:dd/MM/yyyy}",
                        DateTime.Now,
                        EMPTProc1.USUNome,
                        AGBrow1.AGBDataInicio,
                        DataVenc);
                }
                AGBrow1.AGBStatus = 0;
                if (AGBrow1.RowState == DataRowState.Detached)
                    dPagamentosPeriodicos.AGendaBase.AddAGendaBaseRow(AGBrow1);
            }
            else
            {
                if (AGBrow1 != null)
                {
                    AGBrow1.AGBStatus = 4;
                    AGBrow1.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - {1}\r\n    Cancelado pelo cadastro do contrato", DateTime.Now, EMPTProc1.USUNome);
                    if (!PGFrow.IsAGBDataInicioNull())
                        AGBrow1.AGBDataTermino = AGBrow1.AGBDataInicio = PGFrow.AGBDataInicio;
                }
            };
            dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(dPagamentosPeriodicos.AGendaBase);
            if (AGBrow1 != null)
                if ((PGFrow.IsPGFContrato_AGBNull()) || (PGFrow.PGFContrato_AGB != AGBrow1.AGB))
                    PGFrow.PGFContrato_AGB = AGBrow1.AGB;
        }

        

        /// <summary>
        /// 
        /// </summary>        
        protected enum enumNotaCheque 
        { 
            /// <summary>
            /// 
            /// </summary>
            chegou,
            /// <summary>
            /// 
            /// </summary>
            aguarda 
        }

        

        /// <summary>
        /// Último erro
        /// </summary>
        public Exception UltimoErro;

        private enum acoesCadastraProximaNota { CadastrarNotaAtual, CadastrarProximaNota, OkJaCadastrado };

        /// <summary>
        /// Chave
        /// </summary>
        public int PGF
        {
            get { return PGFrow.PGF; }
        }

        /*
        private const string ComandoBuscaUltimaNota =
"SELECT        TOP (1) NOA,NOtAs.NOACompet\r\n" +
"FROM            NOtAs\r\n" +
"WHERE        (NOtAs.NOA_PGF = @P1)\r\n" +
"ORDER BY NOtAs.NOACompet DESC;";

        private const string ComandoBuscaNotasCompet =
"SELECT NOA\r\n" +
"FROM   NOtAs\r\n" +
"WHERE  (NOtAs.NOA_PGF = @P1) and (NOtAs.NOACompet = @P2)";*/

        private int? BuscaUltimaNota()
        {
            if (!dPagamentosPeriodicos.NotasCarregadas)
            {
                if (dPagamentosPeriodicos.Coletivo)
                    throw new Exception("Uso de conjunto dados coletivo sem as notas em BuscaUltimaNota");
                else
                {
                    EMPTProc1.EmbarcaEmTrans(dPagamentosPeriodicos.NOtAsTableAdapter);                    
                    dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGF);
                }
            }
            if (dPagamentosPeriodicos.NOtAs.Count == 0)
            {
                return null;
                /*
                DataRow DR = EMPTProc1.STTA.BuscaSQLRow(ComandoBuscaUltimaNota, PGF);
                if (DR == null)
                    return null;
                DataTable DT = EMPTProc1.STTA.BuscaSQLTabela(ComandoBuscaNotasCompet, PGF, DR["NOACompet"]);
                if(DT.Rows.Count == 1)
                    return (int)DT.Rows[0]["NOA"];
                List<NotaProc> Candidatos = new List<NotaProc>();
                foreach (DataRow DR1 in DT.Rows)
                    Candidatos.Add(new NotaProc((int)DR1["NOA"],EMPTProc1));
                return dPagamentosPeriodicos.NotaRepresentativa(Candidatos);*/
            }
            else
            {
                dPagamentosPeriodicos.NOtAsRow rowNOA = dPagamentosPeriodicos.UltimaNota(PGF);
                if (rowNOA == null)
                    return null;
                else
                    return rowNOA.NOA;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public NotaProc UltimaNota()
        {
            int? NOAultima = BuscaUltimaNota();
            if (!NOAultima.HasValue)
                return null;
            else
                return new NotaProc(NOAultima.Value, EMPTProc1);
        }

        /// <summary>
        /// Força o cadastramento de uma nova competência sem quitar a anterior
        /// </summary>
        public bool CadastrarProximaSemQuitarAnteriro = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NotaCheque"></param>
        /// <returns>indica se houve recálculo</returns>
        protected bool CadastraProximaNota(enumNotaCheque NotaCheque)
        {
            //Remover após a revisão
            //System.Windows.Forms.MessageBox.Show("C1");
            if (!FormasRevisadas.Contains(PGFForma))
                throw new Exception("Forma nao revisada");

            if (!StatusAtivos.Contains(Status))
                return false;

            if (PGFrow.PGFValor <= 0)
            {
                UltimoErro = new Exception("Valor negativo para PGFValor");
                return false;
            }
            //System.Windows.Forms.MessageBox.Show("C2");
            acoesCadastraProximaNota acao;
            NOAStatus? UltimoNOAStatus = null;
            bool RegistrarAcumuladoNoHistorico = false;
            //int? NOAultima = BuscaUltimaNota();            
            NotaProc NotaUltima = UltimaNota();
            //if (!NOAultima.HasValue)
            if (NotaUltima == null)
                acao = acoesCadastraProximaNota.CadastrarNotaAtual;
            else
            {
                //NotaProc NotaUltima = new NotaProc(NOAultima.Value, EMPTProc1);
                ProximaCompetencia = NotaUltima.Competencia;
                UltimoNOAStatus = NotaUltima.Status;
                switch (NotaUltima.VerificaFase())
                {
                    case FasePeriodico.AguardaEmissaoCheque:
                        if(CadastrarProximaSemQuitarAnteriro)
                            acao = acoesCadastraProximaNota.CadastrarProximaNota;
                        else 
                            acao = acoesCadastraProximaNota.OkJaCadastrado;
                        break;
                    case FasePeriodico.CadastrarProxima:
                        acao = acoesCadastraProximaNota.CadastrarProximaNota;
                        break;
                    case FasePeriodico.ComNotaprov:
                        if ((UltimoNOAStatus.Value == NOAStatus.NotaAcumulada) || (UltimoNOAStatus.Value == NOAStatus.CadastradaAjustarValor))
                        {
                            acao = acoesCadastraProximaNota.CadastrarProximaNota;
                            RegistrarAcumuladoNoHistorico = true;
                        }
                        else
                            acao = acoesCadastraProximaNota.OkJaCadastrado;
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Não implementado para fase = {0}", NotaUltima.VerificaFase()));
                }
            }
            //System.Windows.Forms.MessageBox.Show("C3");
            if (acao == acoesCadastraProximaNota.OkJaCadastrado)
            {
                if (PGFrow.RowState == DataRowState.Modified)
                {
                    try
                    {
                        EMPTProc1.AbreTrasacaoSQL("PagamentosPeriodicos.cs CadastraProximaNota - 961", dPagamentosPeriodicos.PaGamentosFixosTableAdapter);
                        //STTA().AbreTrasacaoLF("PagamentosPeriodicos.cs CadastraProximaNota - 961", dPagamentosPeriodicos.PaGamentosFixosTableAdapter);

                        /*
                        ///////TESTE
                        dPagamentosPeriodicos.PaGamentosFixosRow rowAmostra = dPagamentosPeriodicos.PaGamentosFixosTableAdapter.GetDataByPGF(PGFrow.PGF)[0];
                        foreach (System.Data.DataColumn DC in dPagamentosPeriodicos.PaGamentosFixos.Columns)
                        {
                            if (!rowAmostra[DC.ColumnName].Equals(PGFrow[DC.ColumnName, System.Data.DataRowVersion.Original]))
                                System.Windows.Forms.MessageBox.Show(string.Format("{0} {1} / {2}", DC.ColumnName, rowAmostra[DC.ColumnName], PGFrow[DC.ColumnName, System.Data.DataRowVersion.Original]));
                        }
                        ///////TESTE
                        */
                        
                        dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                        
                        PGFrow.AcceptChanges();
                        EMPTProc1.Commit();
                        return true;
                    }
                    catch (Exception e)
                    {
                        EMPTProc1.Vircatch(e);
                        throw;
                    }
                }
                return false;
            }
            
            Competencia Comp = ProximaCompetencia;

            if (acao == acoesCadastraProximaNota.CadastrarProximaNota)
            {
                Comp++;
                ProximaCompetencia = Comp;
            }
            //System.Windows.Forms.MessageBox.Show("C4");
            Competencia CompPAG = Comp.CloneCompet(PGFrow.PGFRef);
            DateTime DataPag = CompPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);

            int? FRN = PGFrow.IsPGF_FRNNull() ? (int?)null : PGFrow.PGF_FRN;
            System.Collections.ArrayList Retencoes = new System.Collections.ArrayList();
            if (!PGFrow.IsPGFISSNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoISS, PGFrow.PGFISS, SimNao.Padrao));
            if (cpfcnpj != null)
            {
                if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                {
                    if (!PGFrow.IsPGFINSSPNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSS, PGFrow.PGFINSSP, SimNao.Padrao));
                    if (!PGFrow.IsPGFIRNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IR, PGFrow.PGFIR, SimNao.Padrao));
                    if (!PGFrow.IsPGFINSSTNull())
                        throw new Exception(string.Format("Erro no cadastro dos periódicos PGF = {0} - {1}", PGF, PGFrow.PGFDescricao));
                }
                else
                {
                    if (!PGFrow.IsPGFINSSPNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet, PGFrow.PGFINSSP, SimNao.Padrao));
                    if (!PGFrow.IsPGFIRNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.IRPF, PGFrow.PGFIR, SimNao.Padrao));
                    if (!PGFrow.IsPGFINSSTNull())
                        Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp, PGFrow.PGFINSST, SimNao.Padrao));
                }
            }
            if (!PGFrow.IsPGFPISCOFNull())
                Retencoes.Add(new dllImpostos.SolicitaRetencao(TipoImposto.PIS_COFINS_CSLL, PGFrow.PGFPISCOF, SimNao.Padrao));
            string Nominal = PGFrow.IsPGFNominalNull() ? "" : PGFrow.PGFNominal;
//            System.Windows.Forms.MessageBox.Show("C5");
            try
            {
                EMPTProc1.AbreTrasacaoSQL("ContaPagar PagamentoPeriodico - 864",
                                                                       dPagamentosPeriodicos.PaGamentosFixosTableAdapter,
                                                                       dPagamentosPeriodicos.NOtAsTableAdapter,
                                                                       dPagamentosPeriodicos.PAGamentosTableAdapter);
                //STTA().AbreTrasacaoLF
                PAGTipo PAGTipo;
                string DadosPag = "";
                int CCT = 0;
                bool booPermiteAgrupar = true;
                
                switch ((TipoPagPeriodico)PGFrow.PGFForma)
                {
                    case TipoPagPeriodico.ChequeDeposito:
                        PAGTipo = PAGTipo.deposito;
                        DadosPag = PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag;
                        break;
                    case TipoPagPeriodico.ChequeSindico:
                        PAGTipo = PAGTipo.sindico;
                        break;
                    case TipoPagPeriodico.Boleto:
                        PAGTipo = PAGTipo.boleto;
                        break;
                    case TipoPagPeriodico.Folha:
                    case TipoPagPeriodico.Adiantamento:
                        PAGTipo = PAGTipo.Folha;
                        break;
                    case TipoPagPeriodico.Cheque:
                    case TipoPagPeriodico.FolhaEletronico:
                    case TipoPagPeriodico.FolhaFGTS:
                    case TipoPagPeriodico.FolhaFGTSEletronico:
                    case TipoPagPeriodico.FolhaIR:
                    case TipoPagPeriodico.FolhaIREletronico:
                    case TipoPagPeriodico.FolhaPIS:
                    case TipoPagPeriodico.FolhaPISEletronico:
                        PAGTipo = PAGTipo.cheque;
                        break;
                    case TipoPagPeriodico.Conta:
                        PAGTipo = PAGTipo.Conta;
                        break;
                    case TipoPagPeriodico.DebitoAutomatico:
                    case TipoPagPeriodico.DebitoAutomaticoConta:
                        PAGTipo = PAGTipo.DebitoAutomatico;
                        booPermiteAgrupar = false;
                        break;                                        
                    case TipoPagPeriodico.PETituloBancario:
                    case TipoPagPeriodico.PETituloBancarioAgrupavel:                    
                        PAGTipo = (PAGTipo)(PGFrow.PGFForma);
                        if ((TipoPagPeriodico)PGFrow.PGFForma == TipoPagPeriodico.PECreditoConta)
                            DadosPag = string.Format("{0}", PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag);
                        CCT = PGFrow.PGF_CCT;
                        booPermiteAgrupar = false;
                        break;
                    case TipoPagPeriodico.PECreditoConta:
                    case TipoPagPeriodico.PEOrdemPagamento:                                        
                    case TipoPagPeriodico.PETransferencia:
                        PAGTipo = (PAGTipo)(PGFrow.PGFForma);
                        if (!PGFrow.IsPGF_CCTCredNull())
                            DadosPag = string.Format("{0}CCT = {1}",
                                                     PGFrow.IsPGFDadosPagNull() ? "" : PGFrow.PGFDadosPag + " - ",
                                                     PGFrow.PGF_CCTCred);
                        CCT = PGFrow.PGF_CCT;
                        booPermiteAgrupar = false;
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Não implementado tratamento para TipoPagPeriodico = {0} em PagamentosPeriodico.cs", (TipoPagPeriodico)PGFrow.PGFForma));
                }


                int? novaNOA = null;
                if (NotaCheque == enumNotaCheque.aguarda)
                {
                    
                    novaNOA = DNOtAs.IncluiNotaProvisoria(DataPag, Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, booPermiteAgrupar, PGFrow.PGF_PLA, PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN, dNOtAs.TipoChave.PGF, PGFrow.PGF, DadosPag, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                }
                else
                {
                    novaNOA = DNOtAs.IncluiNotaSimples_Efetivo(0, DataPag, DataPag, Comp, NOATipo.PagamentoPeriodico, Nominal, PGFrow.PGF_CON, PAGTipo, DadosPag, booPermiteAgrupar, PGFrow.PGF_PLA, PGFrow.PGF_SPL, DescricaoInterna(), PGFrow.PGFValor, FRN.GetValueOrDefault(0), false, dNOtAs.TipoChave.PGF, PGFrow.PGF,null, CCT ,null,null,NOAStatus.Cadastrada, (dllImpostos.SolicitaRetencao[])Retencoes.ToArray(typeof(dllImpostos.SolicitaRetencao)));
                }
                
                string DescNF;
                string DescMotivo;
                //PAGTipo == PAGTipo.Conta ? "* AGUARDA CONTA * " : "* AGUARDA NF * ";
                switch (PAGTipo)
                {
                    case PAGTipo.boleto:
                        DescMotivo = RegistrarAcumuladoNoHistorico ? "Conta Acumulada" : "Cheque emitido";
                        DescNF = "* AGUARDA NF * ";
                        break;
                    case PAGTipo.DebitoAutomatico:
                        DescMotivo = RegistrarAcumuladoNoHistorico ? "Conta Acumulada" : "Débito Agendado";
                        DescNF = "* AGUARDA CONTA * ";
                        break;
                    case PAGTipo.Conta:
                        DescMotivo = RegistrarAcumuladoNoHistorico ? "Conta Acumulada" : "Cheque emitido";
                        DescNF = "* AGUARDA CONTA * ";
                        break;
                    case PAGTipo.Folha:
                        DescMotivo = "Folha rodada";
                        DescNF = "* AGUARDA RODAR FOLHA * ";
                        break;
                    case PAGTipo.cheque:
                    case PAGTipo.sindico:
                    case PAGTipo.deposito:
                    case PAGTipo.PECreditoConta:
                        DescMotivo = "Cheque gerado";
                        DescNF = "* ? * ";
                        break;
                    //case PAGTipo.Acumular:                                        
                    //case PAGTipo.TrafArrecadado:                     
                    default:
                        throw new NotImplementedException(string.Format("PAGTipo não tratado em CadastraProximaNota : {0}", PAGTipo));
                }

                PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} {2}, cadastrado próximo {3}({4}) valor: {5:n2} vencimento {6:dd/MM/yyyy}",
                                                       DateTime.Now,
                                                       EMPTProc1.USUNome,
                                                       DescMotivo,
                                                       NotaCheque == enumNotaCheque.aguarda ? DescNF : "",
                                                       ProximaCompetencia,
                                                       PGFrow.PGFValor,
                                                       ProximaData);
                //if (!PGFrow.IsPGFCompetFNull())
                //  if (PGFrow.PGFProximaCompetencia > PGFrow.PGFCompetF)
                //{
                //  PGFrow.PGFStatus = (int)StatusPagamentoPeriodico.Encerrado;
                // }
                
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                
                PGFrow.AcceptChanges();
                //ReagendaAcumulados(DataPag, NotaCheque == enumNotaCheque.aguarda, 0);

                
                //ATENÇÂO: não sei o motivo dar recarga das tabelas por isso mantive para os tipos que já estavam. Para os demais ela não pode ser feita porque o dNotas é coletivos para vários objetos PagamentoPeriodico para mais velocidade.
                if (PGFForma.EstaNoGrupo(TipoPagPeriodico.Boleto, TipoPagPeriodico.Conta, TipoPagPeriodico.DebitoAutomaticoConta) && !dPagamentosPeriodicos.Coletivo)
                {
                    dPagamentosPeriodicos.PAGamentos.Clear();
                    dPagamentosPeriodicos.NOtAs.Clear();
                    dPagamentosPeriodicos.NOtAsTableAdapter.Fill(dPagamentosPeriodicos.NOtAs, PGFrow.PGF);
                    dPagamentosPeriodicos.PAGamentosTableAdapter.FillByPGF(dPagamentosPeriodicos.PAGamentos, PGFrow.PGF);
                }
                EMPTProc1.Commit();
                
                if (/*!dPagamentosPeriodicos.TravarRecarga &&*/ novaNOA.HasValue && !dPagamentosPeriodicos.Coletivo)
                {
                    try
                    {
                        dPagamentosPeriodicos.NOtAsTableAdapter.ClearBeforeFill = false;
                        dPagamentosPeriodicos.PAGamentosTableAdapter.ClearBeforeFill = false;
                        dPagamentosPeriodicos.NOtAsTableAdapter.FillNovaNota(dPagamentosPeriodicos.NOtAs, novaNOA.Value);
                        dPagamentosPeriodicos.PAGamentosTableAdapter.FillByNovaNota(dPagamentosPeriodicos.PAGamentos, novaNOA.Value);
                    }
                    finally
                    {
                        dPagamentosPeriodicos.NOtAsTableAdapter.ClearBeforeFill = true;
                        dPagamentosPeriodicos.PAGamentosTableAdapter.ClearBeforeFill = true;
                    }
                }
                
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("erro interno em CadastraProximaNota", e);
            }
            //System.Windows.Forms.MessageBox.Show("CF");
            return true;

        }


        private bool Terminado;

        private static System.Collections.ArrayList _StatusAtivos;

        /// <summary>
        /// 
        /// </summary>
        public static System.Collections.ArrayList StatusAtivos
        {
            get { return _StatusAtivos ?? (_StatusAtivos = new System.Collections.ArrayList(new StatusPagamentoPeriodico[] { StatusPagamentoPeriodico.Ativado, StatusPagamentoPeriodico.AtivadoSemDOC })); }
        }

        /// <summary>
        /// Remove da agenda se o tipo nao for controlado
        /// </summary>
        /// <param name="Forma"></param>
        private void ChecaRemoveAgenda(TipoPagPeriodico Forma)
        {
            if (!FormasContraladasNaAgenda.Contains(Forma) && (rowAGB != null) && (rowAGB.AGBStatus != (int)AGBStatus.Ok))
            {
                rowAGB.AGBStatus = (int)AGBStatus.Ok;
                rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - Forma de pagamento não controlada na agenda", DateTime.Now);
                if (!PGFrow.IsAGBDataInicioNull())
                    rowAGB.AGBDataTermino = rowAGB.AGBDataInicio = PGFrow.AGBDataInicio;
                dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(rowAGB);
                rowAGB.AcceptChanges();
            }
            if ((StatusPagamentoPeriodico)PGFrow.PGFStatus == StatusPagamentoPeriodico.Encerrado)
            {
                if (rowAGB != null)
                {
                    rowAGB.AGBStatus = (int)AGBStatus.Ok;
                    rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - Terminado", DateTime.Now);
                    if (!PGFrow.IsAGBDataInicioNull())
                        rowAGB.AGBDataTermino = rowAGB.AGBDataInicio = PGFrow.AGBDataInicio;
                    dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(rowAGB);
                    rowAGB.AcceptChanges();
                }
            }
        }

        private SortedList<int,NotaProc> NotasFilhas()
        {
            if (!dPagamentosPeriodicos.NotasCarregadas)
                dPagamentosPeriodicos.CarregaNotas(PGF);
            SortedList<int, NotaProc> retorno = new SortedList<int, NotaProc>();
            foreach (dPagamentosPeriodicos.NOtAsRow rowNOA in dPagamentosPeriodicos.NOtAs)
                retorno.Add(rowNOA.NOA, new NotaProc(rowNOA.NOA, EMPTProc1));
            return retorno;
        }

        private bool MataPendencia()
        {
            bool retorno = false;
            foreach (NotaProc Notax in NotasFilhas().Values)
                if (Notax.Status == NOAStatus.NotaProvisoria)
                {
                    Notax.Matar();
                    retorno = true;
                }
            return retorno;
        }

        /// <summary>
        /// Função que atualiza o cadastro verificando o proximo evento e cadastrando se necessário
        /// </summary>
        /// <returns>Retorna se houve alterações</returns>
        public override bool CadastrarProximo()
        {
            //System.Windows.Forms.MessageBox.Show("A1");
            if (Status == StatusPagamentoPeriodico.Encerrado)
                return MataPendencia();
            TipoPagPeriodico Forma = (TipoPagPeriodico)PGFrow.PGFForma;
            ChecaRemoveAgenda(Forma);                        
            bool retorno = false;
            Terminado = false;
            AjustarAGBReajContrato();
            //System.Windows.Forms.MessageBox.Show("A2");
            //Verificar se não terminou o período caso tenha competência de término
            if (!PGFrow.IsPGFCompetFNull())
                if (PGFrow.PGFProximaCompetencia > PGFrow.PGFCompetF)
                {
                    //System.Windows.Forms.MessageBox.Show("A21");
                    PGFrow.PGFStatus = (int)StatusPagamentoPeriodico.Encerrado;
                    PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} Última competência gerada: encerrado\r\n", DateTime.Now);
                    dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                    PGFrow.AcceptChanges();
                    Terminado = true;
                }
            //System.Windows.Forms.MessageBox.Show("A22");
            switch (Forma)
            {                                                                                                   
                case TipoPagPeriodico.Cheque:
                case TipoPagPeriodico.ChequeSindico:
                case TipoPagPeriodico.ChequeDeposito:
                case TipoPagPeriodico.PECreditoConta:
                    //System.Windows.Forms.MessageBox.Show("A3");
                    if (StatusAtivos.Contains((StatusPagamentoPeriodico)PGFrow.PGFStatus))
                        retorno = CadastraProximaNota(enumNotaCheque.chegou);                    
                    else
                        retorno = true;
                    //System.Windows.Forms.MessageBox.Show("A4");
                    break;

                case TipoPagPeriodico.Conta:
                case TipoPagPeriodico.Boleto:
                case TipoPagPeriodico.DebitoAutomatico:
                case TipoPagPeriodico.DebitoAutomaticoConta:
                case TipoPagPeriodico.Folha:
                case TipoPagPeriodico.Adiantamento:
                    //System.Windows.Forms.MessageBox.Show("W1");
                    CadastraNaAgenda();
                   // System.Windows.Forms.MessageBox.Show("W11.");
                    retorno = CadastraProximaNota(enumNotaCheque.aguarda);
                 //   System.Windows.Forms.MessageBox.Show("W12");
                    break;
                //case TipoPagPeriodico.Conta:
                //case TipoPagPeriodico.DebitoAutomatico:
                //case TipoPagPeriodico.DebitoAutomaticoConta:
                case TipoPagPeriodico.PETituloBancario:
                case TipoPagPeriodico.PETituloBancarioAgrupavel:
                   // System.Windows.Forms.MessageBox.Show("W2");
                    retorno = CadastraNaAgenda();
                    break;
                case TipoPagPeriodico.FolhaFGTS:
                case TipoPagPeriodico.FolhaIR:
                case TipoPagPeriodico.FolhaPIS:
                   // System.Windows.Forms.MessageBox.Show("W3");
                    retorno = false;
                    break;
                case TipoPagPeriodico.PETransferencia:
                case TipoPagPeriodico.PEOrdemPagamento:
                default:
                    throw new NotImplementedException(string.Format("Não implementado tratamento para Forma = {0} em CadastrarProximo PagamentosPeriodico.cs", (TipoPagPeriodico)PGFrow.PGFForma)); 
            };
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("3");
            //System.Windows.Forms.MessageBox.Show("FF");
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual string NominalPagEletronico
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual string DadosPagEl()
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataPag"></param>
        /// <param name="ValorEfetivo"></param>
        /// <param name="CCD"></param>
        /// <param name="CompServ"></param>
        /// <returns></returns>
        public override bool ConciliaDebitoAutomatico(DateTime DataPag, decimal ValorEfetivo, int CCD, Abstratos.ABS_Competencia CompServ = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Forcar"></param>
        public override void Imprimir(bool Forcar)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string LinhaDaCopia()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="justificativa"></param>
        /// <returns></returns>
        public override bool Salvar(string justificativa)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override decimal ValorProximoDebitoProgramado()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        public TipoPagPeriodico Tipo
        {
            get
            {
                return (TipoPagPeriodico)PGFrow.PGFForma;
            }
            set
            {
                PGFrow.PGFForma = (int)value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual bool CadastraNaAgenda()
        {
            if (!FormasContraladasNaAgenda.Contains(PGFForma))
                return false;
            if (!StatusAtivos.Contains((StatusPagamentoPeriodico)PGFrow.PGFStatus))
            {
                if (rowAGB != null)
                {
                    rowAGB.AGBStatus = (int)AGBStatus.Ok;
                    if (Terminado)
                        rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - Terminado", DateTime.Now);
                    else
                        rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} - {1}\r\n    Cancelado pelo cadastro do contrato", DateTime.Now, EMPTProc1.USUNome);

                    if (!PGFrow.IsAGBDataInicioNull())
                        rowAGB.AGBDataTermino = rowAGB.AGBDataInicio = PGFrow.AGBDataInicio;
                    dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(rowAGB);
                    rowAGB.AcceptChanges();
                }
                return true;
            }
            if (FormasRevisadas.Contains(PGFForma))
            {
                if ((rowAGB != null)
                     &&
                    (rowAGB.AGBCompetenciaAno == ProximaCompetencia.Ano)
                     &&
                    (rowAGB.AGBCompetenciaMes == ProximaCompetencia.Mes)
                   )
                    return true;
            }
            //somente cria um agendamento para o boleto/conta          
            DateTime[] datas;
            malote M1 = null;
            malote M2 = null;
            bool RetornaNoVencimento;
            //Competencia Comp = new Competencia(PGFrow.PGFProximaCompetencia == 0 ? PGFrow.PGFCompetI : PGFrow.PGFProximaCompetencia);
            //Comp.CON = PGFrow.PGF_CON;
            Competencia CompPAG = ProximaCompetencia.CloneCompet(PGFrow.PGFRef);
            DateTime DataPag = CompPAG.DataNaCompetencia(PGFrow.PGFDia, Competencia.Regime.mes);
            if (Tipo != TipoPagPeriodico.DebitoAutomaticoConta)
            {
                bool CONProcuracao;
                malote.TiposMalote TMalote;
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                {
                    DataRow rowcon = EMPTProc1.STTA.BuscaSQLRow("select CONProcuracao,CONMalote from Condominios where CON = @P1", PGFrow.PGF_CON);
                    if (rowcon == null)
                        return false;
                    CONProcuracao = rowcon["CONProcuracao"] == DBNull.Value ? false : (bool)rowcon["CONProcuracao"];
                    if (rowcon["CONMalote"] == DBNull.Value)
                        return false;
                    TMalote = (malote.TiposMalote)rowcon["CONMalote"];
                }
                else
                {
                    FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowcon = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(EMPTProc1.Tipo).CONDOMINIOS.FindByCON(PGFrow.PGF_CON);
                    if (rowcon == null)
                        return false;                    
                    CONProcuracao = rowcon.IsCONProcuracaoNull() ? false : rowcon.CONProcuracao;
                    if (rowcon.IsCONMaloteNull())
                        return false;
                    TMalote = (malote.TiposMalote)rowcon.CONMalote;
                }
                datas = ChequeProc.CalculaDatas(PAGTipo.boleto, CONProcuracao, TMalote, DataPag, DateTime.Now, ref M1, ref M2, out RetornaNoVencimento);
            }
            try
            {
                EMPTProc1.AbreTrasacaoSQL("ContaPagar PagamentoPeriodico - 1529",
                                                                        dPagamentosPeriodicos.AGendaBaseTableAdapter,
                                                                        dPagamentosPeriodicos.PaGamentosFixosTableAdapter);
                //dPagamentosPeriodicos.AGendaBaseRow AGBrow;
                //if (PGFrow.IsPGF_AGBNull())
                TiposTAG TAG = (Tipo != TipoPagPeriodico.DebitoAutomaticoConta) ? TiposTAG.BOLETOS_CONTRATOS : TiposTAG.Debito_Automatico;
                if (rowAGB == null)
                {
                    _rowAGB = dPagamentosPeriodicos.AGendaBase.NewAGendaBaseRow();
                    _rowAGB.AGB_CON = PGFrow.PGF_CON;
                    _rowAGB.AGB_TAG = _rowAGB.AGB_TAG = (int)TAG;
                    _rowAGB.AGBDATAI = DateTime.Now;
                    _rowAGB.AGBDiaTodo = true;
                    _rowAGB.AGBI_USU = EMPTProc1.USU;
                    _rowAGB.AGBMemo = "Cadastro automático";
                }
                else
                {
                    if (rowAGB.AGB_TAG != (int)TAG)
                    {
                        _rowAGB = rowAGB;
                        _rowAGB.AGB_TAG = (int)TAG;
                    }
                }
                _rowAGB.AGBDescricao = DescricaoInterna();
                int Resp5;

                EMPTProc1.STTA.BuscaEscalar("SELECT TAGResp_USU FROM TipoAGendamento WHERE (TAG = @P1)", out Resp5, TAG);
                _rowAGB.AGB_USU = Resp5;
                //if (PGFrow.PGFStatus == (int)StatusPagamentoPeriodico.Encerrado)
                //{                    
                //    AGBrow.AGBMemo += string.Format("\r\nContrato terminado");                    
                //    AGBrow.AGBStatus = 4;                   
                //}
                //else
                //{
                DateTime DataEsperada = (M1 == null) ? DataPag.AddDays(-1) : M1.DataEnvio;
                _rowAGB.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} {2} para {3:dd/MM/yyyy} referente a {4} com vencimento {5:dd/MM/yyyy}{6}",
                                                  DateTime.Now,
                                                  EMPTProc1.USUNome,
                                                  Tipo != TipoPagPeriodico.Boleto ? "Conta esperada" : "Boleto esperado",
                                                  DataEsperada,
                                                  ProximaCompetencia,
                                                  DataPag,
                                                  Tipo == TipoPagPeriodico.DebitoAutomaticoConta ? " (Débito automático)" : "");
                _rowAGB.AGBCompetenciaAno = ProximaCompetencia.Ano;
                _rowAGB.AGBCompetenciaMes = ProximaCompetencia.Mes;
                _rowAGB.AGBDataInicio = DataEsperada;
                _rowAGB.AGBDataTermino = DataPag;
                _rowAGB.AGBStatus = 0;

                _rowAGB.AGBA_USU = EMPTProc1.USU;
                _rowAGB.AGBDATAA = DateTime.Now;
                if (_rowAGB.RowState == DataRowState.Detached)
                    dPagamentosPeriodicos.AGendaBase.AddAGendaBaseRow(_rowAGB);
                dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(_rowAGB);
                _rowAGB.AcceptChanges();
                PGFrow.PGF_AGB = _rowAGB.AGB;
                dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);
                EMPTProc1.Commit();
                PGFrow.AcceptChanges();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw e;
            };
            return true;
        }

        /// <summary>
        /// Grava no histórico e na agenda
        /// </summary>
        /// <param name="MenH"></param>
        /// <param name="MenA">Mensagem da Agenda, se null usa a mesma do histórico</param>
        internal void GravaHistorico_Agenda(string MenH, string MenA = null)
        {
            if (MenH == null)
                MenH = "";
            if (PGFrow.IsPGFHistoricoNull())
                PGFrow.PGFHistorico = "";
            PGFrow.PGFHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} {1} {2}"
                                                 , DateTime.Now,
                                                 EMPTProc1.USUNome,
                                                 MenH);
            EMPTProc1.EmbarcaEmTrans(dPagamentosPeriodicos.PaGamentosFixosTableAdapter, dPagamentosPeriodicos.AGendaBaseTableAdapter);            
            dPagamentosPeriodicos.PaGamentosFixosTableAdapter.Update(PGFrow);            
            if(rowAGB != null)
               rowAGB.AGBMemo = string.Format("{0}\r\n{1:dd/MM/yyyy HH:mm:ss} {2} {3}",
                                                 rowAGB.IsAGBMemoNull() ? "" : rowAGB.AGBMemo + "\r\n",
                                                 DateTime.Now,
                                                 EMPTProc1.USUNome,
                                                 MenA ?? MenH);
            dPagamentosPeriodicos.AGendaBaseTableAdapter.Update(rowAGB);            
        }
    }


    /*
    /// <summary>
    /// Objeto PagamentoPeriodico somente processos
    /// </summary>
    public class PagamentoPeriodicoProc : ABS_PagamentoPeriodicoProc
    {
        

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="PGF"></param>
        /// <param name="_EMPTipo"></param>
        /// <param name="USU"></param>
        public PagamentoPeriodicoProc(int PGF, EMPTProc.EMPTipo _EMPTipo, int USU):
            base(PGF,false,_EMPTipo)
        {
            this.USU = USU;
        }

        protected override string NominalPagEletronico
        {
            get { throw new NotImplementedException(); }
        }

        protected override string DadosPagEl()
        {
            throw new NotImplementedException();
        }

        public override Abstratos.ABS_Competencia ABS_ProximaCompetencia
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CadastrarProximoDebitoAutomatico(DateTime DataPag, decimal ValorEfetivo, int CCD)
        {
            throw new NotImplementedException();
        }

        public override void Imprimir(bool Forcar)
        {
            throw new NotImplementedException();
        }

        public override string LinhaDaCopia()
        {
            throw new NotImplementedException();
        }

        public override bool Salvar(string justificativa)
        {
            throw new NotImplementedException();
        }
        

        public override decimal ValorProximoDebitoProgramado()
        {
            throw new NotImplementedException();
        }

        
        protected override void AjustarAGBReajContrato()
        {
            throw new NotImplementedException();
        }

        protected override bool CadastraNaAgenda()
        {
            throw new NotImplementedException();
        }

        

        
    }

    */
}
