﻿/*
LH - 01/12/2014 14:00 14.1.4.94  - alterção no funcionamento dos pagamentos periódicos com boleto
MR - 26/12/2014 10:00            - Inclusão do parâmetro GPS não obrigatório ao incluir nota (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
MR - 27/04/2015 12:00            - Inclusão do campo PAGStatus não obrigatório no PAGamentosTableAdapter
MR - 20/05/2015 17:00            - Inclusão do campo CHE_CCT no PAGamentosTableAdapter
                                 - Pagamento de tributo eletronico com inclusao do "guia de recolhimento - eletrônico" (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***)
                                 - Honorarios com pagamento eletrônico para condominios habilitados (Alterações indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***)
MR - 10/06/2015 08:20            - Inclusão do campo NOA_CCTCred no NOtAsTableAdapter
                                   Aumento do tamanho do campo PAGAdicional no PAGamentosTableAdapter                       
MR - 15/06/2015 11:30            - Pagamento de tributo eletronico não usa "guia de recolhimento - eletrônico" para INSSpfRet, INSSpfEmp ou INSSpf que será convertido na SEFIP Folha (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (15/06/2015 11:30) ***)
MR - 22/06/2015 12:00            - PECreditoConta mensal fixo entre contas do próprio condominio (Alterações indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (22/06/2015 12:00) ***)
                                 - Honorários para Linhagem (FRN = 149) não vai fazer eletrônico forçadamente, por solicitação da Iris, devido a cobrança da taxa de TED para Banco do Brasil
*/


using System;
using Framework.objetosNeon;
using VirEnumeracoesNeon;
using FrameworkProc;
using FrameworkProc.datasets;
using VirMSSQL;
using dllChequesProc;
using dllImpostoNeonProc;
using System.Data;
using System.Collections.Generic;
using CompontesBasicosProc;
using VirEnumeracoes;


namespace ContaPagarProc
{


    partial class dNOtAs : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }


        #region Estáticos
        private static dNOtAs _dNOtAsSt;
        private static dNOtAs _dNOtAsStF;


        /// <summary>
        /// dataset estático:dNOtAs
        /// </summary>
        public static dNOtAs dNOtAsSt
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de propriedade estática em Servidor de Processos");
                if (_dNOtAsSt == null)
                {
                    _dNOtAsSt = new dNOtAs(new EMPTProc(EMPTipo.Local));
                    VirDB.VirtualTableAdapter.AjustaAutoInc(_dNOtAsSt);
                }
                return _dNOtAsSt;
            }
        }

        /// <summary>
        /// dataset estático:dNOtAs
        /// </summary>
        public static dNOtAs dNOtAsStF
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de propriedade estática em Servidor de Processos");
                if (_dNOtAsStF == null)
                {
                    _dNOtAsStF = new dNOtAs(new EMPTProc(EMPTipo.Filial));
                    VirDB.VirtualTableAdapter.AjustaAutoInc(_dNOtAsStF);
                }
                return _dNOtAsStF;
            }
        }

        /// <summary>
        /// dataset estático:dPagamentosPeriodicos Local/Filial
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public static dNOtAs dNOtAsStX(EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTipo.Local ? dNOtAsSt : dNOtAsStF;
        }
        #endregion


        /// <summary>
        /// Construitor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dNOtAs(EMPTProc _EMPTProc1)
            : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }



        #region Adapters

        private dNOtAsTableAdapters.NOtAsTableAdapter nOtAsTableAdapter;


        private dNOtAsTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dNOtAsTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dNOtAsTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: NOtAs
        /// </summary>
        public dNOtAsTableAdapters.NOtAsTableAdapter NOtAsTableAdapter
        {
            get
            {
                if (nOtAsTableAdapter == null)
                {
                    nOtAsTableAdapter = new dNOtAsTableAdapters.NOtAsTableAdapter();
                    nOtAsTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return nOtAsTableAdapter;
            }
        }

        private dNOtAsTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dNOtAsTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dNOtAsTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return pAGamentosTableAdapter;
            }
        }

        private dNOtAsTableAdapters.PaGamentosFixosTableAdapter paGamentosFixosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PaGamentosFixos
        /// </summary>
        public dNOtAsTableAdapters.PaGamentosFixosTableAdapter PaGamentosFixosTableAdapter
        {
            get
            {
                if (paGamentosFixosTableAdapter == null)
                {
                    paGamentosFixosTableAdapter = new dNOtAsTableAdapters.PaGamentosFixosTableAdapter();
                    paGamentosFixosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return paGamentosFixosTableAdapter;
            }
        }

        private dNOtAsTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FORNECEDORES
        /// </summary>
        public dNOtAsTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new dNOtAsTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return fORNECEDORESTableAdapter;
            }
        }

        #endregion


        /// <summary>
        /// Indica qual amarração será feita
        /// </summary>
        public enum TipoChave
        {
            /// <summary>
            /// 
            /// </summary>
            Nenhuma,
            /// <summary>
            /// 
            /// </summary>
            BOL,
            /// <summary>
            /// 
            /// </summary>
            PGF,
            /// <summary>
            /// 
            /// </summary>
            PES
        }

        #region NotaSimples

        /*
        /// <summary>
        /// Inclui uma nota com um cheque de forma simples
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CompServ"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="FRN"></param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, NOATipo NOATipo, string Favorecido,
            int CON, PAGTipo Tipo, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN)
        {
            return IncluiNotaSimples(numeroNF, Emissao, Vencimento, CompServ, NOATipo, Favorecido, CON, Tipo, Agrupa, PLA, SPL, Servico, Valor, FRN, false, TipoChave.Nenhuma, 0);
        }*/

        /*
        /// <summary>
        /// Inclui uma nota com um cheque de forma simples
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="CompServ">Competência do serviço</param>
        /// <param name="SPL">Sub plano</param>
        /// <param name="FRN">Fornecedor se nao houver coloque zero</param>
        /// <param name="Retencoes">lista das retenções</param>
        /// <param name="SomenteImpostos">Não gera os pagamentos somentes as retenções</param>
        /// <param name="Chave">Chave</param>
        /// <param name="TipoCh">Tipo da chave</param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, NOATipo NOATipo, string Favorecido,
            int CON, PAGTipo Tipo, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos,
            TipoChave TipoCh, int Chave, params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            return IncluiNotaSimples(numeroNF, Emissao, Vencimento, CompServ, NOATipo, Favorecido, CON, Tipo, "", Agrupa, PLA, SPL, Servico, Valor, FRN, SomenteImpostos, TipoCh, Chave, 0, Retencoes);
        }*/


        //*** MRC - INICIO - PAG-FOR (2) ***
        //public bool IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, NOATipo NOATipo, string Favorecido,
        //    int CON, PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos, 
        //    TipoChave TipoCh, int Chave, params dllImpostos.SolicitaRetencao[] Retencoes)
        //{
        //    return IncluiNotaSimples(numeroNF, Emissao, Vencimento,CompServ, NOATipo, Favorecido, CON, Tipo, DadosPag, Agrupa, PLA,SPL, Servico, Valor, FRN, SomenteImpostos, TipoCh, Chave, 0, Retencoes);
        //}

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CompServ"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="DadosPag"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="FRN"></param>
        /// <param name="SomenteImpostos"></param>
        /// <param name="TipoCh"></param>
        /// <param name="Chave"></param>
        /// <param name="CCT"></param>
        /// <param name="Retencoes"></param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, NOATipo NOATipo, string Favorecido,
            int CON, PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos,
            TipoChave TipoCh, int Chave, int CCT = 0, params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            return IncluiNotaSimples(numeroNF, Emissao, Vencimento, CompServ, NOATipo, Favorecido, CON, Tipo, DadosPag, Agrupa, PLA, SPL, Servico, Valor, FRN, SomenteImpostos, TipoCh, Chave, 0, CCT, Retencoes);
        }*/


        /// <summary>
        /// 
        /// </summary>
        public ChequeProc UltimoChequeCriado;

        /*
        //public bool IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, NOATipo NOATipo, string Favorecido,
        //    int CON, PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos, 
        //    TipoChave TipoCh, int Chave, int CCD, params dllImpostos.SolicitaRetencao[] Retencoes) 
        /// <summary>
        /// Inclui uma nota com um cheque de forma simples
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="CCD">Linque com o extrato</param>
        /// <param name="FRN">Fornecedor se nao houver coloque zero</param>
        /// <param name="Retencoes">lista das retenções</param>
        /// <param name="Chave">Chave</param>
        /// <param name="TipoCh">Tipo da chave</param>
        /// <param name="DadosPag">Dados do deposito</param>
        /// <param name="SomenteImpostos">Não gera os pagamentos somentes as retenções</param>
        /// <param name="CompServ"></param>
        /// <param name="SPL"></param>
        /// <param name="CCT"></param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, DateTime Emissao, DateTime Vencimento, Competencia CompServ, NOATipo NOATipo, string Favorecido,
            int CON, PAGTipo Tipo, string DadosPag, bool Agrupa, string PLA, int SPL, string Servico, decimal Valor, int FRN, bool SomenteImpostos,
            TipoChave TipoCh, int Chave, int CCD, int CCT = 0, params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
            return IncluiNotaSimples(numeroNF, Emissao, Vencimento, CompServ, NOATipo, Favorecido, CON, Tipo, DadosPag, Agrupa, PLA, SPL, Servico, Valor, FRN, SomenteImpostos, TipoCh, Chave, CCD, CCT, 0, Retencoes);
        }*/


        /*
        /// <summary>
        /// Inclui uma nota com um cheque de forma simples
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="CCD">Linque com o extrato</param>
        /// <param name="FRN">Fornecedor se nao houver coloque zero</param>
        /// <param name="Retencoes">lista das retenções</param>
        /// <param name="Chave">Chave</param>
        /// <param name="TipoCh">Tipo da chave</param>
        /// <param name="DadosPag">Dados do deposito</param>
        /// <param name="SomenteImpostos">Não gera os pagamentos somente as retenções</param>
        /// <param name="CompServ"></param>
        /// <param name="SPL"></param>
        /// <param name="CCT"></param>
        /// <param name="GPS">Linque com o GPS</param>
        /// <returns>NOA</returns>
        public int? IncluiNotaSimples(int numeroNF, 
                                      DateTime Emissao, 
                                      DateTime Vencimento, 
                                      Competencia CompServ, 
                                      NOATipo NOATipo, 
                                      string Favorecido,
                                      int CON, 
                                      PAGTipo Tipo, 
                                      string DadosPag, 
                                      bool Agrupa, 
                                      string PLA, 
                                      int SPL, 
                                      string Servico, 
                                      decimal Valor, 
                                      int FRN, 
                                      bool SomenteImpostos,
                                      TipoChave TipoCh, 
                                      int Chave, 
                                      int CCD, 
                                      int CCT, 
                                      int GPS = 0, 
                                      params dllImpostos.SolicitaRetencao[] Retencoes)
        {            
            //legado
            int? novoFRN = null;
            int? novoCCD = null;
            int? novoCCT = null;            
            int? novoGPS = null;            
            if (FRN != 0)
                novoFRN = FRN;
            if (CCD != 0)
                novoCCD = CCD;
            if (CCT != 0)
                novoCCT = CCT;
            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
            if (GPS != 0)
                novoGPS = GPS;
            //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***

            return IncluiNotaSimples_Efetivo(NOAStatus.Cadastrada,
                                             numeroNF,
                                             Emissao,
                                             Vencimento,
                                             CompServ,
                                             NOATipo,
                                             Favorecido,
                                             CON,
                                             Tipo,
                                             DadosPag,
                                             Agrupa,
                                             PLA,
                                             SPL,
                                             Servico,
                                             Valor,
                                             novoFRN,
                                             SomenteImpostos,
                                             TipoCh,
                                             Chave,
                                             novoCCD,
                                             novoCCT,                                             
                                             novoGPS,
                                             
                                             Retencoes);
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Vencimento"></param>
        /// <param name="CompServ"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="FRN"></param>
        /// <param name="TipoCh"></param>
        /// <param name="Chave"></param>
        /// <param name="DadosPag"></param>
        /// <param name="Retencoes"></param>
        /// <returns></returns>
        public int? IncluiNotaProvisoria(DateTime Vencimento,
                                          Competencia CompServ,
                                          NOATipo NOATipo,
                                          string Favorecido,
                                          int CON,
                                          PAGTipo Tipo,
                                          bool Agrupa,
                                          string PLA,
                                          int SPL,
                                          string Servico,
                                          decimal Valor,
                                          int? FRN,
                                          TipoChave TipoCh,
                                          int Chave,
                                          string DadosPag = "", params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            return IncluiNotaSimples_Efetivo(0,
                                             Vencimento.AddDays(-5),
                                             Vencimento,
                                             CompServ,
                                             NOATipo,
                                             Favorecido,
                                             CON,
                                             Tipo,
                                             DadosPag,
                                             Agrupa,
                                             PLA,
                                             SPL,
                                             Servico,
                                             Valor,
                                             FRN,
                                             false,
                                             TipoCh,
                                             Chave,
                                             null,
                                             null,
                                             null,
                                             null,
                                             NOAStatus.NotaProvisoria,
                                             Retencoes);
        }

        private dContasLogicas.ContaCorrenTeRow GETrowCCT(int? CCT)
        {
            if (!CCT.HasValue)
                return null;
            else
                return dContasLogicas.dContasLogicasSt.ContaCorrenTe.FindByCCT(CCT.Value);
        }

        //private bool? tributoEletronico;

        [Obsolete("pegar pelo condomínio")]
        private bool TributoEletronico(int CON)
        {
            return EMPTProc1.STTA.BuscaEscalar_bool("select CONGPSEletronico from condominios where CON = @P1", CON).GetValueOrDefault(false);
        }

        /// <summary>
        /// Inclui nota simples com fornecedor
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CompServ"></param>
        /// <param name="NOATipo"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="fornecedor"></param>
        /// <param name="SomenteImpostos"></param>
        /// <param name="PGF"></param>
        /// <param name="CCD"></param>
        /// <param name="CCT"></param>
        /// <param name="_NOAStatus"></param>
        /// <param name="Retencoes"></param>
        /// <returns></returns>
        public int? IncluiNotaSimples_Fornecedor(int numeroNF,
                                                 DateTime Emissao,
                                                 DateTime Vencimento,
                                                 Competencia CompServ,
                                                 NOATipo NOATipo,
                                                 int CON,
                                                 PAGTipo Tipo,
                                                 bool Agrupa,
                                                 string PLA,
                                                 int SPL,
                                                 string Servico,
                                                 decimal Valor,
                                                 AbstratosNeon.ABS_Fornecedor ABSfornecedor,
                                                 bool SomenteImpostos = false,
                                                 int? PGF = null,
                                                 int? CCD = null,
                                                 int? CCT = null,
                                                 NOAStatus _NOAStatus = NOAStatus.Cadastrada,
                                                params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            string DadosPag = "";
            CadastrosProc.Fornecedores.FornecedorProc fornecedor = (CadastrosProc.Fornecedores.FornecedorProc)ABSfornecedor;
            if ((Tipo == PAGTipo.deposito) && (fornecedor.Conta != null))
            {
                DadosPag = fornecedor.Conta.ToString();
            }
            return IncluiNotaSimples_Efetivo(numeroNF,
                                             Emissao,
                                             Vencimento,
                                             CompServ,
                                             NOATipo,
                                             fornecedor.FRNNome,
                                             CON,
                                             Tipo,
                                             DadosPag,
                                             Agrupa,
                                             PLA,
                                             SPL,
                                             Servico,
                                             Valor,
                                             fornecedor.FRN,
                                             SomenteImpostos,
                                             PGF.HasValue ? TipoChave.PGF : TipoChave.Nenhuma,
                                             PGF,
                                             CCD,
                                             CCT,
                                             null,
                                             fornecedor.Conta.CCG,
                                             _NOAStatus,
                                             Retencoes
                                            );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroNF"></param>
        /// <param name="Emissao"></param>
        /// <param name="Vencimento"></param>
        /// <param name="CompServ"></param>
        /// <param name="NOATipo"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="DadosPag"></param>
        /// <param name="Agrupa"></param>
        /// <param name="PLA"></param>
        /// <param name="SPL"></param>
        /// <param name="Servico"></param>
        /// <param name="Valor"></param>
        /// <param name="FRN"></param>
        /// <param name="SomenteImpostos"></param>
        /// <param name="TipoCh"></param>
        /// <param name="Chave"></param>
        /// <param name="CCD"></param>
        /// <param name="CCT"></param>
        /// <param name="GPS"></param>
        /// <param name="CCG"></param>
        /// <param name="_NOAStatus"></param>
        /// <param name="Retencoes"></param>
        /// <returns></returns>
        public int? IncluiNotaSimples_Efetivo(int numeroNF,
                                          DateTime Emissao,
                                          DateTime Vencimento,
                                          Competencia CompServ,
                                          NOATipo NOATipo,
                                          string Favorecido,
                                          int CON,
                                          PAGTipo Tipo,
                                          string DadosPag,
                                          bool Agrupa,
                                          string PLA,
                                          int SPL,
                                          string Servico,
                                          decimal Valor,
                                          int? FRN = null,
                                          bool SomenteImpostos = false,
                                          TipoChave TipoCh = TipoChave.Nenhuma,
                                          int? Chave = null,
                                          int? CCD = null,
                                          int? CCT = null,
                                          int? GPS = null,
                                          int? CCG = null,
                                          NOAStatus _NOAStatus = NOAStatus.Cadastrada,
                                          params dllImpostos.SolicitaRetencao[] Retencoes)
        {
            if (Valor < 0)
                return null;
            if ((Valor == 0) && (Tipo != PAGTipo.Acumular))
                return null;
            ChequeProc NovoCheque = null;
            try
            {
                EMPTProc1.AbreTrasacaoSQL("ContaPagar dNotas - 238",
                                                                        NOtAsTableAdapter,
                                                                        PAGamentosTableAdapter);
                NOtAsRow rowNOA = NOtAs.NewNOtAsRow();
                rowNOA.NOA_CON = CON;
                rowNOA.NOA_PLA = PLA;
                rowNOA.NOA_SPL = SPL;
                if ((FRN.HasValue) && (FRN.Value > 0))
                    rowNOA.NOA_FRN = FRN.Value;
                rowNOA.NOAServico = Servico;
                rowNOA.NOADataEmissao = Emissao;
                rowNOA.NOAAguardaNota = false;
                rowNOA.NOADATAI = DateTime.Now;
                rowNOA.NOAI_USU = EMPTProc1.USU;
                rowNOA.NOANumero = numeroNF;
                rowNOA.NOAStatus = (int)_NOAStatus;
                rowNOA.NOATipo = (int)NOATipo;

                if (DadosPag != "")
                    if (Tipo == PAGTipo.PECreditoConta && DadosPag.Contains("- CCT = "))
                        rowNOA.NOA_CCTCred = Convert.ToInt32(DadosPag.Substring(DadosPag.IndexOf("CCT = ") + 6));
                    else
                        rowNOA.NOADadosPag = DadosPag;

                if (CCT.HasValue)
                    rowNOA.NOA_CCT = CCT.Value;

                rowNOA.NOATotal = Valor;
                if (Tipo == PAGTipo.Acumular)
                    rowNOA.NOADefaultPAGTipo = (int)PAGTipo.cheque;
                else
                    rowNOA.NOADefaultPAGTipo = (int)Tipo;
                if (Chave.HasValue)
                {
                    if (TipoCh == TipoChave.PGF)
                        rowNOA.NOA_PGF = Chave.Value;
                    if (TipoCh == TipoChave.PES)
                        rowNOA.NOA_PES = Chave.Value;
                }
                if (CompServ != null)
                    rowNOA.NOACompet = CompServ.CompetenciaBind;
                else
                    rowNOA.NOACompet = rowNOA.NOADataEmissao.Year * 100 + rowNOA.NOADataEmissao.Month;
                NOtAs.AddNOtAsRow(rowNOA);
                NOtAsTableAdapter.Update(rowNOA);
                rowNOA.AcceptChanges();

                decimal ValorPagamento = Valor;

                if (Retencoes != null)
                    foreach (dllImpostos.SolicitaRetencao Sol in Retencoes)
                    {
                        if (Sol == null)
                            continue;
                        ImpostoNeonProc Imp = new ImpostoNeonProc(Sol.Tipo);
                        Imp.DataNota = Emissao;
                        Imp.DataPagamento = Vencimento;
                        Imp.ValorBase = Valor;
                        //Referência OneNote: iss com data errada dNOtAs.cs 11.1.7.5 14/11/2011
                        if ((Sol.Tipo == TipoImposto.ISSQN) || (Sol.Tipo == TipoImposto.ISS_SA))
                            Imp.Competencia = CompServ;
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                        PAGTipo RetPAGTipo;
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (15/06/2015 11:30) ***                        
                        if (Sol.Tipo.EstaNoGrupo(TipoImposto.ISSQN, TipoImposto.ISS_SA, TipoImposto.INSSpf, TipoImposto.INSSpfEmp, TipoImposto.INSSpfRet)
                            ||
                            !TributoEletronico(CON)
                           )
                            //*** MRC - INICIO - TRIBUTO ELETRONICO (15/06/2015 11:30) ***
                            RetPAGTipo = PAGTipo.Guia;
                        else
                            RetPAGTipo = PAGTipo.PEGuia;
                        NovoCheque = new ChequeProc(Imp.VencimentoEfetivo,
                                                    Imp.Nominal(),
                                                    CON,
                                                    RetPAGTipo,
                                                    (_NOAStatus == NOAStatus.NotaProvisoria),
                                                    CCD,
                                                    null,
                                                    "",
                                                    (_NOAStatus == NOAStatus.NotaProvisoria),
                                                    null,
                                                    EMPTProc1);
                        PAGamentosRow Improw = PAGamentos.NewPAGamentosRow();
                        Improw.PAGPermiteAgrupar = true;
                        Improw.PAG_CHE = NovoCheque.CHErow.CHE;
                        Improw.PAG_NOA = rowNOA.NOA;
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                        Improw.PAGTipo = (int)RetPAGTipo;
                        Improw.PAGStatus = (int)NovoCheque.CHEStatus;
                        //*** MRC - TERMINO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                        Improw.PAGIMP = (int)Sol.Tipo;
                        Improw.PAGValor = Sol.Valor == 0 ? Imp.ValorImposto : Sol.Valor;
                        if ((Sol.Descontar == SimNao.Sim) || ((Sol.Descontar == SimNao.Padrao) && (Imp.Desconta())))
                            ValorPagamento -= Improw.PAGValor;
                        Improw.PAGVencimento = Imp.VencimentoEfetivo;
                        //Improw.CHEStatus = (int)(_NOAStatus == NOAStatus.NotaProvisoria ? Enumeracoes.CHEStatus.CadastradoBloqueado : Enumeracoes.CHEStatus.Cadastrado);
                        Improw.CHEStatus = (int)NovoCheque.CHEStatus;
                        Improw.CHEFavorecido = Imp.Nominal();
                        PAGamentos.AddPAGamentosRow(Improw);
                        PAGamentosTableAdapter.Update(Improw);
                        Improw.AcceptChanges();
                        NovoCheque.AjustarTotalPelasParcelas();
                    };

                if (!SomenteImpostos)
                {

                    PAGTipo PAGTipo1;
                    bool PermiteEletronicos = false;
                    if (CCT.HasValue)
                        PermiteEletronicos = EMPTProc1.STTA.BuscaEscalar_bool("select CCTPagamentoEletronico from contacorrente where CCT = @P1", CCT.Value).GetValueOrDefault(false);

                    if (Tipo != PAGTipo.Honorarios || (Tipo == PAGTipo.Honorarios && FRN == 149) || !PermiteEletronicos)
                        PAGTipo1 = Tipo;
                    else
                        PAGTipo1 = PAGTipo.PECreditoConta;
                    if ((PAGTipo1 == PAGTipo.PECreditoConta) && (!PermiteEletronicos))
                        PAGTipo1 = PAGTipo.cheque;

                    PAGamentosRow rowPag = PAGamentos.NewPAGamentosRow();

                    if (Tipo != PAGTipo.Acumular)
                    {
                        NovoCheque = new ChequeProc(Vencimento, Favorecido, CON, PAGTipo1, !Agrupa, CCD, CCT, DadosPag, _NOAStatus == NOAStatus.NotaProvisoria, CCG, EMPTProc1);
                        rowPag.PAGStatus = NovoCheque.CHErow.CHEStatus;
                        rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                        if (Tipo != PAGTipo.DebitoAutomatico)
                            if (CCD.HasValue)
                                if (!NovoCheque.AmarrarCCD(CCD.Value))
                                    throw new Exception("CCD não é nulo");
                    }
                    else
                        NovoCheque = null;
                    rowPag.PAG_NOA = rowNOA.NOA;
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
                    //CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 5");
                    if (GPS.HasValue)
                        rowPag.PAG_GPS = GPS.Value;
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                                    
                    rowPag.PAGDATAI = DateTime.Now;
                    rowPag.PAGI_USU = EMPTProc1.USU;
                    //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                    //rowPag.PAGTipo = (int)Tipo;
                    rowPag.PAGTipo = (int)PAGTipo1;
                    //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
                    rowPag.PAGValor = ValorPagamento;
                    rowPag.PAGVencimento = Vencimento;
                    //*** MRC - INICIO - PAG-FOR (2) ***
                    if (DadosPag != "")
                        rowPag.PAGAdicional = DadosPag;
                    rowPag.PAGPermiteAgrupar = Agrupa;
                    //*** MRC - FIM - PAG-FOR (2) ***
                    PAGamentos.AddPAGamentosRow(rowPag);
                    //CompontesBasicos.Performance.Performance.PerformanceST.Registra("UPDATE");
                    PAGamentosTableAdapter.Update(rowPag);
                    //CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 6");
                    rowPag.AcceptChanges();
                    if (NovoCheque != null)
                    {
                        NovoCheque.LiberaEdicao = true;
                        //CompontesBasicos.Performance.Performance.PerformanceST.Registra("AjustarTotalPelasParcelas");
                        NovoCheque.AjustarTotalPelasParcelas();
                        //CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 7");
                    }
                    if (TipoCh == TipoChave.BOL)
                        TableAdapter.ST().ExecutarSQLNonQuery("insert into BODxPAG (BOD, PAG) VALUES(@P1,@P2)", Chave, rowPag.PAG);
                }
                //CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 7");
                EMPTProc1.Commit();
                UltimoChequeCriado = NovoCheque;
                //CompontesBasicos.Performance.Performance.PerformanceST.Registra("INC 8");
                return rowNOA.NOA;
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                //TableAdapter.ST().Vircatch(e);
                throw new Exception("Erro na inclusão da nota", e);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Justificativa"></param>
        /// <param name="PagamentoPeriodicoDestravar"></param>
        /// <param name="JustificativaDestrava"></param>
        /// <returns></returns>
        public bool GravaNoBanco(string Justificativa, PagamentoPeriodicoProc PagamentoPeriodicoDestravar, string JustificativaDestrava)
        {
            dNOtAs.NOtAsRow LinhaMae = NOtAs[0];
            NotaProc Nota = new NotaProc(LinhaMae, EMPTProc1);
            try
            {
                EMPTProc1.AbreTrasacaoSQL("ContaPagarProc dNotas.cs - 747", PAGamentosTableAdapter, NOtAsTableAdapter);
                if (LinhaMae.RowState != DataRowState.Unchanged)
                    NOtAsTableAdapter.Update(LinhaMae);
                List<dNOtAs.PAGamentosRow> Linhas = new List<dNOtAs.PAGamentosRow>();
                foreach (dNOtAs.PAGamentosRow rowPag1 in PAGamentos)
                    Linhas.Add(rowPag1);
                foreach (dNOtAs.PAGamentosRow rowPag in Linhas)
                    Nota.AjustaParcelaNoCheque(rowPag, false, Justificativa);
                if (PagamentoPeriodicoDestravar != null)
                    if (JustificativaDestrava == null)
                        PagamentoPeriodicoDestravar.GravaHistorico_Agenda(string.Format("Chegada da NF {0} emissão: {1:dd/MM/yyyy} - Pagamento cadastrado", LinhaMae.NOANumero, LinhaMae.NOADataEmissao));
                    else
                        PagamentoPeriodicoDestravar.GravaHistorico_Agenda(string.Format("Destravamento manual NF {0} emissão: {1:dd/MM/yyyy} - Pagamento cadastrado\r\nJustificativa:\r\n{2}", LinhaMae.NOANumero, LinhaMae.NOADataEmissao, JustificativaDestrava));
                EMPTProc1.Commit();
                return true;
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                //MessageBox.Show(String.Format("Erro ao gravar:\r\n{0}\r\nA Gravação das parcelas NÃO foi feita!!!", e.Message));
                throw new Exception("Erro ao gravar", e);
            };
        }





        /*
        private NOAStatus ParseNOAStatus(int Entrada)
        {
            return (NOAStatus)System.Enum.Parse(typeof(NOAStatus), Entrada.ToString());                
        }
        */
        /*
        public CHEquesRow BuscaCheque(string Fav, System.DateTime data,int CON) {
            CHEquesRow Cheque;
            if (CHEquesTableAdapter.FillByFav(CHEques, data, Fav, CON) > 0)
            {                
                Cheque = CHEques[0];
                return Cheque;
            };
            
            Cheque = CHEques.NewCHEquesRow();
            Cheque.CHEFavorecido = Fav;
            Cheque.CHEVencimento = data;
            Cheque.CHEBanco = data;            
            Cheque.CHE_CON = CON; 
            return Cheque;
            
        }
        */





        /*
        private NOATipo ParseNOATipo(int Entrada)
        {
            return (NOATipo)System.Enum.Parse(typeof(NOATipo), Entrada.ToString());
        }
        */
    }
}
