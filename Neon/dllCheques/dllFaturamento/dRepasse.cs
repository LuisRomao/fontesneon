﻿namespace dllFaturamento
{


    partial class dRepasse
    {
        private dRepasseTableAdapters.MateriaisTableAdapter materiaisTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Materiais
        /// </summary>
        public dRepasseTableAdapters.MateriaisTableAdapter MateriaisTableAdapter
        {
            get
            {
                if (materiaisTableAdapter == null)
                {
                    materiaisTableAdapter = new dRepasseTableAdapters.MateriaisTableAdapter();
                    materiaisTableAdapter.TrocarStringDeConexao();
                };
                return materiaisTableAdapter;
            }
        }



        private static dRepasseTableAdapters.FaturamentoTableAdapter faturamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Faturamento
        /// </summary>
        public static dRepasseTableAdapters.FaturamentoTableAdapter FaturamentoTableAdapter
        {
            get
            {
                if (faturamentoTableAdapter == null)
                {
                    faturamentoTableAdapter = new dRepasseTableAdapters.FaturamentoTableAdapter();
                    faturamentoTableAdapter.TrocarStringDeConexao();
                };
                return faturamentoTableAdapter;
            }
        }

        private dRepasseTableAdapters.Materiais1TableAdapter materiais1TableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Materiais1
        /// </summary>
        public dRepasseTableAdapters.Materiais1TableAdapter Materiais1TableAdapter
        {
            get
            {
                if (materiais1TableAdapter == null)
                {
                    materiais1TableAdapter = new dRepasseTableAdapters.Materiais1TableAdapter();
                    materiais1TableAdapter.TrocarStringDeConexao();
                };
                return materiais1TableAdapter;
            }
        }

        private dRepasseTableAdapters.INSumosTableAdapter iNSumosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: INSumos
        /// </summary>
        public dRepasseTableAdapters.INSumosTableAdapter INSumosTableAdapter
        {
            get
            {
                if (iNSumosTableAdapter == null)
                {
                    iNSumosTableAdapter = new dRepasseTableAdapters.INSumosTableAdapter();
                    iNSumosTableAdapter.TrocarStringDeConexao();
                };
                return iNSumosTableAdapter;
            }
        }

        private dRepasseTableAdapters.REPACTableAdapter rEPACTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: REPAC
        /// </summary>
        public dRepasseTableAdapters.REPACTableAdapter REPACTableAdapter
        {
            get
            {
                if (rEPACTableAdapter == null)
                {
                    rEPACTableAdapter = new dRepasseTableAdapters.REPACTableAdapter();
                    rEPACTableAdapter.TrocarStringDeConexao();
                };
                return rEPACTableAdapter;
            }
        }

        private dRepasseTableAdapters.REPassesTableAdapter rEPassesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: REPasses
        /// </summary>
        public dRepasseTableAdapters.REPassesTableAdapter REPassesTableAdapter
        {
            get
            {
                if (rEPassesTableAdapter == null)
                {
                    rEPassesTableAdapter = new dRepasseTableAdapters.REPassesTableAdapter();
                    rEPassesTableAdapter.TrocarStringDeConexao();
                };
                return rEPassesTableAdapter;
            }
        }

        private dRepasseTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dRepasseTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dRepasseTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

    }
}

namespace dllFaturamento.dRepasseTableAdapters {
    
    
    public partial class FaturamentoTableAdapter {
    }
}
