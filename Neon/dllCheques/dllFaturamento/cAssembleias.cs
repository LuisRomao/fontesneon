using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace dllFaturamento
{
    /// <summary>
    /// cAssembleias
    /// </summary>
    public partial class cAssembleias : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cAssembleias()
        {
            InitializeComponent();
        }

        private void cAssembleias_OnClose(object sender, EventArgs e)
        {
            gridView1.UpdateCurrentRow();
            gridView1.ValidateEditor();
            gridView1.CloseEditor();
            Validate();
            novosBindingSource.EndEdit();
        }
    }
}
