﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace dllFaturamento
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cConfirmar : ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public cConfirmar()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Taxa"></param>
        /// <param name="Assembleia"></param>
        /// <param name="Taxa13"></param>
        /// <param name="Dirf"></param>
        public void Prepara(int Taxa, int Assembleia, int Taxa13, int Dirf)
        {
            checkEdit1.Checked = checkEdit2.Checked = checkEdit3.Checked = false;
            spinEdit1.Value = Taxa;
            spinEdit2.Value = Assembleia;
            spinEdit3.Value = Taxa13;
            spinEdit4.Value = Dirf;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool chTaxa => checkEdit1.Checked; 

        /// <summary>
        /// 
        /// </summary>
        public bool chAssembleia => checkEdit2.Checked; 

        /// <summary>
        /// 
        /// </summary>
        public bool chTaxa13 => checkEdit3.Checked; 

        /// <summary>
        /// 
        /// </summary>
        public bool chDirf => checkEdit4.Checked; 
    }
}
