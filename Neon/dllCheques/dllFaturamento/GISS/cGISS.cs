using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using dllEmissorNFS_e;
using System.IO;
using CompontesBasicos.ObjetosEstaticos;
using CompontesBasicos.Espera;

namespace dllFaturamento.GISS
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cGISS : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cGISS()
        {
            InitializeComponent();
            cCompet1.Comp.CompetenciaBind = DateTime.Today.Year * 100 + DateTime.Today.Month;
            cCompet1.AjustaTela();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Carrega();
                
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            dGISS.GISS.Clear();
            simpleButton1.Enabled = false;
            
        }

        private void Carrega()
        {
            string comando = "UPDATE NOtAs SET NOACompet = @P1 WHERE (NOADataEmissao BETWEEN @P2 AND @P3) and NOACompet = 0";
            DateTime DI = new DateTime(cCompet1.Comp.Ano,cCompet1.Comp.Mes,1);
            DateTime DF = DI.AddMonths(1).AddDays(-1);
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando, cCompet1.Comp.CompetenciaBind,DI,DF);
            simpleButton1.Enabled = (dGISS.GISSTableAdapter.Fill(dGISS.GISS, cCompet1.Comp.CompetenciaBind, CidadeEMP) > 0);
            Valida();
        }

        private string Caminho;

        private int CidadeEMP = 366;

        private int contador;
        private int nArquivo;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
            Caminho = string.Format("{0}\\GISS\\", Path.GetDirectoryName(Application.ExecutablePath));
            if (!Directory.Exists(Caminho))
                Directory.CreateDirectory(Caminho);
            foreach (string arquivo in Directory.GetFiles(Caminho))
                File.Delete(arquivo);
            giss giss = new giss();
            string UltimoCodCon = "";
            string Ncodigo = "";
            DateTime proxTime = DateTime.Now;
            using (cEspera Esp = new cEspera(this))
            {
                Esp.AtivaGauge(dGISS.GISS.Count);
                int i = 0;
                contador = 0;
                nArquivo = 0;
                foreach (dGISS.GISSRow row in dGISS.GISS)
                {
                    i++;
                    if (proxTime <= DateTime.Now)
                    {
                        Application.DoEvents();
                        proxTime = DateTime.Now.AddSeconds(3);
                        Esp.Gauge(i);
                    }
                    if (!row.IsErroNull())
                        continue;
                    if (UltimoCodCon != row.CONCodigo)
                    {
                        Gravagiss(giss, UltimoCodCon, Ncodigo);
                        UltimoCodCon = row.CONCodigo;
                        contador = 0;
                        nArquivo = 0;
                        Ncodigo = row.IsCONUsuPrefNull() ? "" : row.CONUsuPref;
                    }
                    contador++;
                    if (contador == 7)
                    {                        
                        contador = 0;
                        Gravagiss(giss, UltimoCodCon, Ncodigo);
                        nArquivo++;
                    }
                    dgiss.NotasPRow nova;
                    nova = giss.dgiss.NotasP.NewNotasPRow();
                    nova.DT_EMISSAO_NF = row.NOADataEmissao;

                 //   nova.NR_DOC_NF_INICIAL = row.NOANumero;
                    
                    if (row.NOANumero == 0)
                        nova.NR_DOC_NF_INICIAL = row.NOA;
                    else
                        nova.NR_DOC_NF_INICIAL = row.NOANumero;
                    
                    if (row.ReteveISS)
                        nova.TP_DOC_NF = 5; //1 para tributada 5 para retida
                    else
                        nova.TP_DOC_NF = 1;
                    nova.VL_DOC_NF = row.NOATotal;
                    nova.CD_ATIVIDADE = row.SPLCodigo;
                    if (row.FRN_CID == CidadeEMP)
                    {
                        //nova.CD_PREST_TOM_ESTABELECIDO = "S";
                        //nova.CD_LOCAL_PRESTACAO = "D";
                        nova.DoMunicipio = true;
                        nova.Local = true;
                    }
                    else
                    {
                        //nova.CD_PREST_TOM_ESTABELECIDO = "N";
                        //nova.CD_LOCAL_PRESTACAO = row.SPLNoCondominio ? "D" : "F";
                        nova.DoMunicipio = false;
                        nova.Local = row.SPLNoCondominio ? true : false;
                    };
                    nova.NM_RAZAO_SOCIAL = row.FRNNome;
                    DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(row.IsFRNCnpjNull() ? "" : row.FRNCnpj);
                    nova.NR_CNPJ_CPF = cnpj.Valor;
                   
                    nova.NR_INSCRICAO_MUNICIPAL = row.IsFRNRegistroNull() ? 0 : (int)row.FRNRegistro;
                    int intIE = 0;
                    if (!row.IsFRNIENull())
                        int.TryParse(row.FRNIE, out intIE);
                    nova.NR_INSCRICAO_ESTADUAL = intIE;
                    nova.NM_LOGRADOURO = row.FRNEndereco;
                    nova.NR_LOGRADOURO = row.IsFRNNumeroNull() ? "1" : row.FRNNumero;
                    nova.CD_CEP = 0;
                    try
                    {
                        nova.CD_CEP = int.Parse(StringEdit.Limpa(row.FRNCep, StringEdit.TiposLimpesa.SoNumeros));
                    }
                    catch { };
                    nova.NM_BAIRRO = row.FRNBairro;
                    nova.CD_ESTADO = row.CIDUf;
                    nova.NM_CIDADE = row.CIDNome;
                    if ((row.IsFRNSimplesNull()) || (row.FRNSimples == false))
                    {
                        nova.FL_SIMPLES = "N";
                        nova.VL_ALIQUOTA = "";
                    }
                    else
                    {
                        nova.FL_SIMPLES = "S";
                        nova.VL_ALIQUOTA = row.IsFRNSimplesAliquotaNull() ? "0" : string.Format("{0:00.00}",row.FRNSimplesAliquota).Replace(',','.');
                    }
                    giss.dgiss.NotasP.AddNotasPRow(nova);

                };
                Gravagiss(giss, UltimoCodCon, Ncodigo);
            }
            
        }

        private bool Gravagiss(giss giss, string Arquivo,string Ncodigo)
        {
            bool retorno = false;
            if (Arquivo != "")
            {
                string nArq = (nArquivo == 0) ? "" : string.Format("_{0}",nArquivo);
                Arquivo = string.Format("{0}{1}-{2}{3}.dat", Caminho, StringEdit.Limpa(Arquivo, StringEdit.TiposLimpesa.NomeArquivo), StringEdit.Limpa(Ncodigo, StringEdit.TiposLimpesa.NomeArquivo), nArq);
                retorno = giss.GeraArquivo(Arquivo);
                if (retorno)
                {
                    giss.dgiss.NotasP.Clear();
                    giss.dgissF.NotasP.Clear();
                }
                return retorno;
            }
            else
                return false;
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dGISS.GISSRow row = (dGISS.GISSRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos("ContaPagar.cNotasCampos", "NOTA", row.NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                    break;
            }
        }

        private void Valida()
        {
            try
            {
                gridView1.BeginDataUpdate();
                int i = 0; ;
                //string comando = "SELECT PAG_NOA, PAGIMP FROM PAGamentos WHERE (PAG_NOA = @P1) AND (PAGIMP = 6)";
                dGISS.NOtAsRetTableAdapter.Fill(dGISS.NOtAsRet, cCompet1.Comp.CompetenciaBind);
                DateTime proxTime = DateTime.Now;
                using (cEspera Esp = new cEspera(this))
                {
                    Esp.AtivaGauge(dGISS.GISS.Count);
                    foreach (dGISS.GISSRow row in dGISS.GISS)
                    {
                        i++;
                        if ((!row.IsFRNSimplesNull()) && (row.FRNSimples))
                        {
                            if (row.IsFRNSimplesAliquotaNull())
                                row.Erro = "Aliquota do simples n�o informada";
                            else
                            {
                                System.Collections.ArrayList possiveis = new System.Collections.ArrayList(new decimal[] { 2, 2.79M, 3.5M, 3.84M, 3.87M, 4.23M, 4.26M, 4.31M, 4.61M, 4.65M, 5 });
                                if(! possiveis.Contains(row.FRNSimplesAliquota))
                                    row.Erro = "Aliquota do simples errada ";
                            }
                        }
                        if (proxTime <= DateTime.Now)
                        {
                            Application.DoEvents();
                            proxTime = DateTime.Now.AddSeconds(3);
                            Esp.Gauge(i);
                        }
                        row.ReteveISS = (dGISS.NOtAsRet.FindByNOA(row.NOA) != null);
                        if (row.IsFRNTipoNull())
                        {
                            row.Erro = "Sem Fornecedor";
                        }
                        else if (row.IsSPL_PLANull())
                        {
                            row.Erro = "Sem C�digo da prefeitura";
                        }
                        if (row.IsFRN_CIDNull())
                        {
                            row.Erro = "cidade do fornecedor";
                        }
                        else
                        {
                            if ((row.FRN_CID == CidadeEMP) && (!row.ReteveISS))
                            {
                                row.Erro = "RETEN��O do mesmo munic�pio";
                            }
                            if(!row.IsSPLNoCondominioNull())
                                if ((row.FRN_CID != CidadeEMP) && (!row.SPLNoCondominio) && (row.ReteveISS))
                                {
                                    row.Erro = "RETEN��O servi�o prestado fora";
                                }
                        }
                    }
                }
            }
            finally
            {
                gridView1.EndDataUpdate();
            }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if(e.RowHandle < 0)
                return;
            dGISS.GISSRow row = (dGISS.GISSRow)gridView1.GetDataRow(e.RowHandle);
            if(row == null)
                return;
            if(e.Column.FieldName == colFRNTipo.FieldName)
                if(row.IsFRNTipoNull())
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.Black;
                }
            if (e.Column.FieldName == colSPLCodigo.FieldName)
                if (row.IsSPLCodigoNull())
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.Black;
                }
            if (e.Column.FieldName == colErro.FieldName)
                if (!row.IsErroNull())
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.Black;
                }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            int pos = 0;
            dGISS.GISSRow row = (dGISS.GISSRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            if (!row.IsSPL_PLANull())
                return;
            cNovoSPL novo = new cNovoSPL();            
            novo.Plano.Text = string.Format("{0} - {1}",row.NOA_PLA,row.PLADescricao);
            if (novo.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllFaturamento cGiss - 303");
                    string Comando1 = "INSERT INTO SubPLano(SPL_PLA,SPLCodigo,SPLDescricao,SPLServico,SPLNoCondominio) VALUES (@P1,@P2,@P3,1,@P4)";
                    int SPL = VirMSSQL.TableAdapter.STTableAdapter.IncluirAutoInc(Comando1, row.NOA_PLA, novo.Codigo.Text, novo.Descricao.Text, novo.checkNoLocal.Checked);
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update NOtAs set NOA_SPL = @P1 where NOA_PLA = @P2 and NOA_SPL is null", SPL, row.NOA_PLA);
                    VirMSSQL.TableAdapter.CommitSQL();
                    pos = gridView1.FocusedRowHandle;
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                }
                Carrega();
                gridView1.FocusedRowHandle = pos;
            }
        }
    }
}
