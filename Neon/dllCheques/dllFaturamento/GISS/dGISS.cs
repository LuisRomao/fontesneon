﻿namespace dllFaturamento.GISS {


    partial class dGISS
    {
        partial class NOtAsRetDataTable
        {
        }
    
        private dGISSTableAdapters.GISSTableAdapter gISSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: GISS
        /// </summary>
        public dGISSTableAdapters.GISSTableAdapter GISSTableAdapter
        {
            get
            {
                if (gISSTableAdapter == null)
                {
                    gISSTableAdapter = new dGISSTableAdapters.GISSTableAdapter();
                    gISSTableAdapter.TrocarStringDeConexao();
                };
                return gISSTableAdapter;
            }
        }

        private dGISSTableAdapters.NOtAsRetTableAdapter nOtAsRetTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOtAsRet
        /// </summary>
        public dGISSTableAdapters.NOtAsRetTableAdapter NOtAsRetTableAdapter
        {
            get
            {
                if (nOtAsRetTableAdapter == null)
                {
                    nOtAsRetTableAdapter = new dGISSTableAdapters.NOtAsRetTableAdapter();
                    nOtAsRetTableAdapter.TrocarStringDeConexao();
                };
                return nOtAsRetTableAdapter;
            }
        }
    }
}
