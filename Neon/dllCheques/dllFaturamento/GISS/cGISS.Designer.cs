namespace dllFaturamento.GISS
{
    partial class cGISS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cGISS));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dGISSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dGISS = new dllFaturamento.GISS.dGISS();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOANumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADataEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOATotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAObs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOACodServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOACompet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRN_CID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDIBGE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNSimples = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLADescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSPL_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSPLCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSPLDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSPLServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSPLNoCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSPL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON_CID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONUsuPref = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONSenhaPref = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colSPLValorISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSPLReterISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReteveISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNSimplesAliquota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNIE = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGISSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGISS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
           
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1037, 67);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(350, 5);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(216, 62);
            this.simpleButton3.TabIndex = 9;
            this.simpleButton3.Text = "C�digo da prefeitura";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Enabled = false;
            this.simpleButton1.Location = new System.Drawing.Point(128, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(216, 62);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "Gerar Arquivos";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(5, 35);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(117, 32);
            this.simpleButton2.TabIndex = 7;
            this.simpleButton2.Text = "Recalcular";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.CompetenciaBind = 201103;
            this.cCompet1.CON = ((object)(resources.GetObject("cCompet1.CON")));
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
           
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(5, 5);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 6;
            this.cCompet1.Titulo = null;
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.dGISSBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 67);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1037, 440);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dGISSBindingSource
            // 
            this.dGISSBindingSource.DataMember = "GISS";
            this.dGISSBindingSource.DataSource = this.dGISS;
            // 
            // dGISS
            // 
            this.dGISS.DataSetName = "dGISS";
            this.dGISS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseBorderColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNOA,
            this.colNOANumero,
            this.colNOADataEmissao,
            this.colNOA_FRN,
            this.colNOATotal,
            this.colNOAServico,
            this.colNOA_PLA,
            this.colNOAObs,
            this.colNOACodServico,
            this.colNOACompet,
            this.colFRNTipo,
            this.colFRNCnpj,
            this.colFRNNome,
            this.colFRNEndereco,
            this.colFRNBairro,
            this.colFRN_CID,
            this.colFRNCep,
            this.colCIDNome,
            this.colCIDUf,
            this.colCIDIBGE,
            this.colFRNSimples,
            this.colFRNRegistro,
            this.colPLADescricao,
            this.colSPL_PLA,
            this.colSPLCodigo,
            this.colSPLDescricao,
            this.colSPLServico,
            this.colSPLNoCondominio,
            this.colSPL,
            this.colCON,
            this.colCONCodigo,
            this.colCON_CID,
            this.colCONUsuPref,
            this.colCONSenhaPref,
            this.colErro,
            this.gridColumn1,
            this.colSPLValorISS,
            this.colSPLReterISS,
            this.colReteveISS,
            this.colFRNSimplesAliquota,
            this.colFRNNumero,
            this.colFRNIE});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCodigo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNOANumero, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // colNOA
            // 
            this.colNOA.FieldName = "NOA";
            this.colNOA.Name = "colNOA";
            this.colNOA.OptionsColumn.ReadOnly = true;
            // 
            // colNOANumero
            // 
            this.colNOANumero.Caption = "N�mero da nota";
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.Visible = true;
            this.colNOANumero.VisibleIndex = 0;
            this.colNOANumero.Width = 105;
            // 
            // colNOADataEmissao
            // 
            this.colNOADataEmissao.Caption = "Emiss�o";
            this.colNOADataEmissao.FieldName = "NOADataEmissao";
            this.colNOADataEmissao.Name = "colNOADataEmissao";
            this.colNOADataEmissao.Visible = true;
            this.colNOADataEmissao.VisibleIndex = 1;
            this.colNOADataEmissao.Width = 115;
            // 
            // colNOA_FRN
            // 
            this.colNOA_FRN.FieldName = "NOA_FRN";
            this.colNOA_FRN.Name = "colNOA_FRN";
            // 
            // colNOATotal
            // 
            this.colNOATotal.Caption = "Valor Total";
            this.colNOATotal.DisplayFormat.FormatString = "n2";
            this.colNOATotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNOATotal.FieldName = "NOATotal";
            this.colNOATotal.Name = "colNOATotal";
            this.colNOATotal.Visible = true;
            this.colNOATotal.VisibleIndex = 2;
            this.colNOATotal.Width = 119;
            // 
            // colNOAServico
            // 
            this.colNOAServico.Caption = "Servi�o";
            this.colNOAServico.FieldName = "NOAServico";
            this.colNOAServico.Name = "colNOAServico";
            this.colNOAServico.Visible = true;
            this.colNOAServico.VisibleIndex = 3;
            this.colNOAServico.Width = 84;
            // 
            // colNOA_PLA
            // 
            this.colNOA_PLA.Caption = "Conta";
            this.colNOA_PLA.FieldName = "NOA_PLA";
            this.colNOA_PLA.Name = "colNOA_PLA";
            this.colNOA_PLA.Visible = true;
            this.colNOA_PLA.VisibleIndex = 6;
            // 
            // colNOAObs
            // 
            this.colNOAObs.Caption = "OBS";
            this.colNOAObs.FieldName = "NOAObs";
            this.colNOAObs.Name = "colNOAObs";
            this.colNOAObs.Visible = true;
            this.colNOAObs.VisibleIndex = 7;
            // 
            // colNOACodServico
            // 
            this.colNOACodServico.Caption = "Servi�o";
            this.colNOACodServico.FieldName = "NOACodServico";
            this.colNOACodServico.Name = "colNOACodServico";
            this.colNOACodServico.Visible = true;
            this.colNOACodServico.VisibleIndex = 4;
            this.colNOACodServico.Width = 163;
            // 
            // colNOACompet
            // 
            this.colNOACompet.FieldName = "NOACompet";
            this.colNOACompet.Name = "colNOACompet";
            // 
            // colFRNTipo
            // 
            this.colFRNTipo.Caption = "Tipo";
            this.colFRNTipo.FieldName = "FRNTipo";
            this.colFRNTipo.Name = "colFRNTipo";
            this.colFRNTipo.Visible = true;
            this.colFRNTipo.VisibleIndex = 5;
            // 
            // colFRNCnpj
            // 
            this.colFRNCnpj.Caption = "CNPJ";
            this.colFRNCnpj.FieldName = "FRNCnpj";
            this.colFRNCnpj.Name = "colFRNCnpj";
            this.colFRNCnpj.Visible = true;
            this.colFRNCnpj.VisibleIndex = 9;
            this.colFRNCnpj.Width = 86;
            // 
            // colFRNNome
            // 
            this.colFRNNome.Caption = "Fornecedor";
            this.colFRNNome.FieldName = "FRNNome";
            this.colFRNNome.Name = "colFRNNome";
            this.colFRNNome.Visible = true;
            this.colFRNNome.VisibleIndex = 8;
            this.colFRNNome.Width = 151;
            // 
            // colFRNEndereco
            // 
            this.colFRNEndereco.Caption = "Endere�o";
            this.colFRNEndereco.FieldName = "FRNEndereco";
            this.colFRNEndereco.Name = "colFRNEndereco";
            this.colFRNEndereco.Visible = true;
            this.colFRNEndereco.VisibleIndex = 10;
            this.colFRNEndereco.Width = 211;
            // 
            // colFRNBairro
            // 
            this.colFRNBairro.Caption = "Bairro";
            this.colFRNBairro.FieldName = "FRNBairro";
            this.colFRNBairro.Name = "colFRNBairro";
            this.colFRNBairro.Visible = true;
            this.colFRNBairro.VisibleIndex = 11;
            // 
            // colFRN_CID
            // 
            this.colFRN_CID.FieldName = "FRN_CID";
            this.colFRN_CID.Name = "colFRN_CID";
            // 
            // colFRNCep
            // 
            this.colFRNCep.Caption = "CEP";
            this.colFRNCep.FieldName = "FRNCep";
            this.colFRNCep.Name = "colFRNCep";
            this.colFRNCep.Visible = true;
            this.colFRNCep.VisibleIndex = 12;
            // 
            // colCIDNome
            // 
            this.colCIDNome.Caption = "Cidade";
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            this.colCIDNome.Visible = true;
            this.colCIDNome.VisibleIndex = 13;
            // 
            // colCIDUf
            // 
            this.colCIDUf.Caption = "UF";
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.Visible = true;
            this.colCIDUf.VisibleIndex = 14;
            // 
            // colCIDIBGE
            // 
            this.colCIDIBGE.Caption = "IBGE";
            this.colCIDIBGE.FieldName = "CIDIBGE";
            this.colCIDIBGE.Name = "colCIDIBGE";
            this.colCIDIBGE.Visible = true;
            this.colCIDIBGE.VisibleIndex = 15;
            // 
            // colFRNSimples
            // 
            this.colFRNSimples.Caption = "Simples";
            this.colFRNSimples.FieldName = "FRNSimples";
            this.colFRNSimples.Name = "colFRNSimples";
            this.colFRNSimples.Visible = true;
            this.colFRNSimples.VisibleIndex = 16;
            // 
            // colFRNRegistro
            // 
            this.colFRNRegistro.FieldName = "FRNRegistro";
            this.colFRNRegistro.Name = "colFRNRegistro";
            this.colFRNRegistro.Visible = true;
            this.colFRNRegistro.VisibleIndex = 17;
            // 
            // colPLADescricao
            // 
            this.colPLADescricao.Caption = "Conta";
            this.colPLADescricao.FieldName = "PLADescricao";
            this.colPLADescricao.Name = "colPLADescricao";
            this.colPLADescricao.Visible = true;
            this.colPLADescricao.VisibleIndex = 18;
            // 
            // colSPL_PLA
            // 
            this.colSPL_PLA.FieldName = "SPL_PLA";
            this.colSPL_PLA.Name = "colSPL_PLA";
            this.colSPL_PLA.Visible = true;
            this.colSPL_PLA.VisibleIndex = 19;
            // 
            // colSPLCodigo
            // 
            this.colSPLCodigo.Caption = "C�digo Serv.";
            this.colSPLCodigo.FieldName = "SPLCodigo";
            this.colSPLCodigo.Name = "colSPLCodigo";
            this.colSPLCodigo.Visible = true;
            this.colSPLCodigo.VisibleIndex = 20;
            // 
            // colSPLDescricao
            // 
            this.colSPLDescricao.Caption = "Descri��o";
            this.colSPLDescricao.FieldName = "SPLDescricao";
            this.colSPLDescricao.Name = "colSPLDescricao";
            this.colSPLDescricao.Visible = true;
            this.colSPLDescricao.VisibleIndex = 21;
            // 
            // colSPLServico
            // 
            this.colSPLServico.FieldName = "SPLServico";
            this.colSPLServico.Name = "colSPLServico";
            this.colSPLServico.Visible = true;
            this.colSPLServico.VisibleIndex = 22;
            // 
            // colSPLNoCondominio
            // 
            this.colSPLNoCondominio.Caption = "Servico no local";
            this.colSPLNoCondominio.FieldName = "SPLNoCondominio";
            this.colSPLNoCondominio.Name = "colSPLNoCondominio";
            this.colSPLNoCondominio.Visible = true;
            this.colSPLNoCondominio.VisibleIndex = 23;
            // 
            // colSPL
            // 
            this.colSPL.FieldName = "SPL";
            this.colSPL.Name = "colSPL";
            this.colSPL.OptionsColumn.ReadOnly = true;
            // 
            // colCON
            // 
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            // 
            // colCON_CID
            // 
            this.colCON_CID.FieldName = "CON_CID";
            this.colCON_CID.Name = "colCON_CID";
            // 
            // colCONUsuPref
            // 
            this.colCONUsuPref.Caption = "I.M.";
            this.colCONUsuPref.FieldName = "CONUsuPref";
            this.colCONUsuPref.Name = "colCONUsuPref";
            this.colCONUsuPref.Visible = true;
            this.colCONUsuPref.VisibleIndex = 24;
            // 
            // colCONSenhaPref
            // 
            this.colCONSenhaPref.FieldName = "CONSenhaPref";
            this.colCONSenhaPref.Name = "colCONSenhaPref";
            // 
            // colErro
            // 
            this.colErro.FieldName = "Erro";
            this.colErro.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colErro.Name = "colErro";
            this.colErro.Visible = true;
            this.colErro.VisibleIndex = 25;
            this.colErro.Width = 300;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 26;
            this.gridColumn1.Width = 33;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colSPLValorISS
            // 
            this.colSPLValorISS.FieldName = "SPLValorISS";
            this.colSPLValorISS.Name = "colSPLValorISS";
            // 
            // colSPLReterISS
            // 
            this.colSPLReterISS.FieldName = "SPLReterISS";
            this.colSPLReterISS.Name = "colSPLReterISS";
            // 
            // colReteveISS
            // 
            this.colReteveISS.FieldName = "ReteveISS";
            this.colReteveISS.Name = "colReteveISS";
            // 
            // colFRNSimplesAliquota
            // 
            this.colFRNSimplesAliquota.FieldName = "FRNSimplesAliquota";
            this.colFRNSimplesAliquota.Name = "colFRNSimplesAliquota";
            // 
            // colFRNNumero
            // 
            this.colFRNNumero.FieldName = "FRNNumero";
            this.colFRNNumero.Name = "colFRNNumero";
            // 
            // colFRNIE
            // 
            this.colFRNIE.FieldName = "FRNIE";
            this.colFRNIE.Name = "colFRNIE";
            // 
            // cGISS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
           
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGISS";
            this.Size = new System.Drawing.Size(1037, 507);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGISSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGISS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dGISSBindingSource;
        private dGISS dGISS;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADataEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_FRN;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAObs;
        private DevExpress.XtraGrid.Columns.GridColumn colNOACodServico;
        private DevExpress.XtraGrid.Columns.GridColumn colNOACompet;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNNome;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colFRN_CID;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNCep;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDIBGE;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNSimples;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colPLADescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colSPL_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colSPLCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colSPLDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colSPLServico;
        private DevExpress.XtraGrid.Columns.GridColumn colSPLNoCondominio;
        private DevExpress.XtraGrid.Columns.GridColumn colSPL;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_CID;
        private DevExpress.XtraGrid.Columns.GridColumn colCONUsuPref;
        private DevExpress.XtraGrid.Columns.GridColumn colCONSenhaPref;
        private DevExpress.XtraGrid.Columns.GridColumn colErro;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.Columns.GridColumn colSPLValorISS;
        private DevExpress.XtraGrid.Columns.GridColumn colSPLReterISS;
        private DevExpress.XtraGrid.Columns.GridColumn colReteveISS;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNSimplesAliquota;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNIE;
    }
}
