namespace dllFaturamento
{
    partial class cFaturamento
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cFaturamento));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCIDIBGE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONEndereco2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSHistorico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCONNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSVerificacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colNOATotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPS_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSReterContri = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSRetIR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSReterISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CustomRowCellEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colNOANumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADataEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCompetenciaEdit1 = new CompontesBasicos.RepositoryItemCompetenciaEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dFaturamentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dFaturamento = new dllFaturamento.dFaturamento();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLRP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLRPProtocolo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLRPStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colLRPSEnvio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repItemButtonEditNF = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNOA_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOATotal1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNOA_PLA1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAServico1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOATotal3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcValor = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRPS1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPS_NOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPS_LRP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCONNome1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCnpj1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErro1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOATotal2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSVerificacao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSComp1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPS_PLA1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSReterISS1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSReterContri1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSRetIR1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADataEmissao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOANumero1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColVerNota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemButtonEditNF1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryVerNota = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxCaminho = new DevExpress.XtraEditors.TextEdit();
            this.ButtonEmitir1 = new DevExpress.XtraEditors.SimpleButton();
            this.ButReEnv = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonAss = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonVerificar = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditGravarArquivos = new DevExpress.XtraEditors.CheckEdit();
            this.ButtonEmitir = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomRowCellEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCompetenciaEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFaturamentoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFaturamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemButtonEditNF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcValor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemButtonEditNF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryVerNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxCaminho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGravarArquivos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseBorderColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRPS,
            this.colRPSStatus,
            this.colCIDIBGE,
            this.colCIDUf,
            this.colCONBairro,
            this.colCONCep,
            this.colCONCnpj,
            this.colCONEndereco2,
            this.colCONNome,
            this.colRPSHistorico,
            this.colCONNumero,
            this.colErro,
            this.colCONCodigo,
            this.colRPSVerificacao,
            this.gridColumn1,
            this.colNOATotal,
            this.colRPS_PLA,
            this.colRPSReterContri,
            this.colRPSRetIR,
            this.colRPSReterISS,
            this.gridColumn2,
            this.colNOANumero,
            this.colNOADataEmissao,
            this.colRPSComp});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsDetail.ShowDetailTabs = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView2_RowCellStyle);
            // 
            // colRPS
            // 
            this.colRPS.Caption = "RPS";
            this.colRPS.FieldName = "RPS";
            this.colRPS.Name = "colRPS";
            this.colRPS.OptionsColumn.ReadOnly = true;
            this.colRPS.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colRPS.Visible = true;
            this.colRPS.VisibleIndex = 0;
            this.colRPS.Width = 111;
            // 
            // colRPSStatus
            // 
            this.colRPSStatus.Caption = "Status";
            this.colRPSStatus.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colRPSStatus.FieldName = "RPSStatus";
            this.colRPSStatus.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colRPSStatus.Name = "colRPSStatus";
            this.colRPSStatus.OptionsColumn.ReadOnly = true;
            this.colRPSStatus.Visible = true;
            this.colRPSStatus.VisibleIndex = 14;
            this.colRPSStatus.Width = 114;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colCIDIBGE
            // 
            this.colCIDIBGE.Caption = "C�digo Mun.";
            this.colCIDIBGE.FieldName = "CIDIBGE";
            this.colCIDIBGE.Name = "colCIDIBGE";
            this.colCIDIBGE.Visible = true;
            this.colCIDIBGE.VisibleIndex = 8;
            // 
            // colCIDUf
            // 
            this.colCIDUf.Caption = "UF";
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.Width = 39;
            // 
            // colCONBairro
            // 
            this.colCONBairro.Caption = "Bairro";
            this.colCONBairro.FieldName = "CONBairro";
            this.colCONBairro.Name = "colCONBairro";
            // 
            // colCONCep
            // 
            this.colCONCep.Caption = "CEP";
            this.colCONCep.FieldName = "CONCep";
            this.colCONCep.Name = "colCONCep";
            // 
            // colCONCnpj
            // 
            this.colCONCnpj.Caption = "CNPJ";
            this.colCONCnpj.FieldName = "CONCnpj";
            this.colCONCnpj.Name = "colCONCnpj";
            this.colCONCnpj.Width = 128;
            // 
            // colCONEndereco2
            // 
            this.colCONEndereco2.Caption = "Endere�o";
            this.colCONEndereco2.FieldName = "CONEndereco2";
            this.colCONEndereco2.Name = "colCONEndereco2";
            this.colCONEndereco2.Width = 170;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Nome";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 6;
            this.colCONNome.Width = 213;
            // 
            // colRPSHistorico
            // 
            this.colRPSHistorico.Caption = "Hist�rico";
            this.colRPSHistorico.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRPSHistorico.FieldName = "RPSHistorico";
            this.colRPSHistorico.Name = "colRPSHistorico";
            this.colRPSHistorico.Visible = true;
            this.colRPSHistorico.VisibleIndex = 9;
            this.colRPSHistorico.Width = 55;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            // 
            // colCONNumero
            // 
            this.colCONNumero.Caption = "N�mero";
            this.colCONNumero.FieldName = "CONNumero";
            this.colCONNumero.Name = "colCONNumero";
            // 
            // colErro
            // 
            this.colErro.FieldName = "Erro";
            this.colErro.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colErro.Name = "colErro";
            this.colErro.Visible = true;
            this.colErro.VisibleIndex = 13;
            this.colErro.Width = 150;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 5;
            // 
            // colRPSVerificacao
            // 
            this.colRPSVerificacao.Caption = "Verifica��o";
            this.colRPSVerificacao.FieldName = "RPSVerificacao";
            this.colRPSVerificacao.Name = "colRPSVerificacao";
            this.colRPSVerificacao.OptionsColumn.ReadOnly = true;
            this.colRPSVerificacao.Visible = true;
            this.colRPSVerificacao.VisibleIndex = 2;
            this.colRPSVerificacao.Width = 91;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 15;
            this.gridColumn1.Width = 90;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.Image = global::dllFaturamento.Properties.Resources.Nota1;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down, "", -1, true, false, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colNOATotal
            // 
            this.colNOATotal.Caption = "Total";
            this.colNOATotal.DisplayFormat.FormatString = "n2";
            this.colNOATotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNOATotal.FieldName = "NOATotal";
            this.colNOATotal.Name = "colNOATotal";
            // 
            // colRPS_PLA
            // 
            this.colRPS_PLA.Caption = "Plano";
            this.colRPS_PLA.FieldName = "RPS_PLA";
            this.colRPS_PLA.Name = "colRPS_PLA";
            this.colRPS_PLA.Visible = true;
            this.colRPS_PLA.VisibleIndex = 7;
            // 
            // colRPSReterContri
            // 
            this.colRPSReterContri.Caption = "Ret PIS/COFINS/CSLL";
            this.colRPSReterContri.FieldName = "RPSReterContri";
            this.colRPSReterContri.Name = "colRPSReterContri";
            this.colRPSReterContri.Visible = true;
            this.colRPSReterContri.VisibleIndex = 10;
            // 
            // colRPSRetIR
            // 
            this.colRPSRetIR.Caption = "Ret IR";
            this.colRPSRetIR.FieldName = "RPSRetIR";
            this.colRPSRetIR.Name = "colRPSRetIR";
            this.colRPSRetIR.Visible = true;
            this.colRPSRetIR.VisibleIndex = 11;
            // 
            // colRPSReterISS
            // 
            this.colRPSReterISS.Caption = "Ret ISS";
            this.colRPSReterISS.FieldName = "RPSReterISS";
            this.colRPSReterISS.Name = "colRPSReterISS";
            this.colRPSReterISS.Visible = true;
            this.colRPSReterISS.VisibleIndex = 12;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.CustomRowCellEdit;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 16;
            this.gridColumn2.Width = 30;
            // 
            // CustomRowCellEdit
            // 
            this.CustomRowCellEdit.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.CustomRowCellEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CustomRowCellEdit.Name = "CustomRowCellEdit";
            this.CustomRowCellEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.CustomRowCellEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ButtonImpresso_ButtonClick);
            // 
            // colNOANumero
            // 
            this.colNOANumero.Caption = "N�mero NF";
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.Visible = true;
            this.colNOANumero.VisibleIndex = 1;
            this.colNOANumero.Width = 83;
            // 
            // colNOADataEmissao
            // 
            this.colNOADataEmissao.Caption = "Emiss�o";
            this.colNOADataEmissao.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.colNOADataEmissao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNOADataEmissao.FieldName = "NOADataEmissao";
            this.colNOADataEmissao.Name = "colNOADataEmissao";
            this.colNOADataEmissao.Visible = true;
            this.colNOADataEmissao.VisibleIndex = 3;
            // 
            // colRPSComp
            // 
            this.colRPSComp.Caption = "Compet�ncia";
            this.colRPSComp.ColumnEdit = this.repositoryItemCompetenciaEdit1;
            this.colRPSComp.FieldName = "RPSComp";
            this.colRPSComp.Name = "colRPSComp";
            this.colRPSComp.Visible = true;
            this.colRPSComp.VisibleIndex = 4;
            // 
            // repositoryItemCompetenciaEdit1
            // 
            this.repositoryItemCompetenciaEdit1.AutoHeight = false;
            this.repositoryItemCompetenciaEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Left, "", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right)});
            this.repositoryItemCompetenciaEdit1.Mask.EditMask = "00/0000";
            this.repositoryItemCompetenciaEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.repositoryItemCompetenciaEdit1.Name = "repositoryItemCompetenciaEdit1";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "LoteRPs";
            this.gridControl1.DataSource = this.dFaturamentoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode2.LevelTemplate = this.gridView3;
            gridLevelNode2.RelationName = "FK_NOAxRPS_RPS";
            gridLevelNode1.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            gridLevelNode1.RelationName = "FK_RPS_LoteRPs";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 84);
            this.gridControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemImageComboBox2,
            this.repositoryItemButtonEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemMemoExEdit1,
            this.CustomRowCellEdit,
            this.repositoryItemCompetenciaEdit1,
            this.repItemButtonEditNF});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1428, 623);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView3,
            this.gridView2});
            // 
            // dFaturamentoBindingSource
            // 
            this.dFaturamentoBindingSource.DataSource = this.dFaturamento;
            this.dFaturamentoBindingSource.Position = 0;
            // 
            // dFaturamento
            // 
            this.dFaturamento.DataSetName = "dFaturamento";
            this.dFaturamento.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.gridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLRP,
            this.colLRPProtocolo,
            this.colLRPStatus,
            this.colLRPSEnvio});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // colLRP
            // 
            this.colLRP.Caption = "Lote";
            this.colLRP.FieldName = "LRP";
            this.colLRP.Name = "colLRP";
            this.colLRP.OptionsColumn.ReadOnly = true;
            this.colLRP.Visible = true;
            this.colLRP.VisibleIndex = 0;
            // 
            // colLRPProtocolo
            // 
            this.colLRPProtocolo.Caption = "Protocolo";
            this.colLRPProtocolo.FieldName = "LRPProtocoloL";
            this.colLRPProtocolo.Name = "colLRPProtocolo";
            this.colLRPProtocolo.OptionsColumn.ReadOnly = true;
            this.colLRPProtocolo.Visible = true;
            this.colLRPProtocolo.VisibleIndex = 1;
            this.colLRPProtocolo.Width = 120;
            // 
            // colLRPStatus
            // 
            this.colLRPStatus.Caption = "Status";
            this.colLRPStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colLRPStatus.FieldName = "LRPStatus";
            this.colLRPStatus.Name = "colLRPStatus";
            this.colLRPStatus.OptionsColumn.ReadOnly = true;
            this.colLRPStatus.Visible = true;
            this.colLRPStatus.VisibleIndex = 3;
            this.colLRPStatus.Width = 113;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colLRPSEnvio
            // 
            this.colLRPSEnvio.Caption = "Envio";
            this.colLRPSEnvio.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.colLRPSEnvio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colLRPSEnvio.FieldName = "LRPSEnvio";
            this.colLRPSEnvio.Name = "colLRPSEnvio";
            this.colLRPSEnvio.OptionsColumn.ReadOnly = true;
            this.colLRPSEnvio.Visible = true;
            this.colLRPSEnvio.VisibleIndex = 2;
            this.colLRPSEnvio.Width = 141;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repItemButtonEditNF
            // 
            this.repItemButtonEditNF.AutoHeight = false;
            editorButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions6.Image")));
            this.repItemButtonEditNF.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repItemButtonEditNF.Name = "repItemButtonEditNF";
            this.repItemButtonEditNF.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repItemButtonEditNF.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repItemButtonEditNF_ButtonClick);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNOA_PLA,
            this.colNOAServico,
            this.colNOATotal1,
            this.gridColumn3});
            this.gridView3.GridControl = this.gridControl1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // colNOA_PLA
            // 
            this.colNOA_PLA.Caption = "Conta";
            this.colNOA_PLA.FieldName = "NOA_PLA";
            this.colNOA_PLA.Name = "colNOA_PLA";
            this.colNOA_PLA.OptionsColumn.ReadOnly = true;
            this.colNOA_PLA.Visible = true;
            this.colNOA_PLA.VisibleIndex = 0;
            this.colNOA_PLA.Width = 113;
            // 
            // colNOAServico
            // 
            this.colNOAServico.Caption = "Descri��o";
            this.colNOAServico.FieldName = "NOAServico";
            this.colNOAServico.Name = "colNOAServico";
            this.colNOAServico.OptionsColumn.ReadOnly = true;
            this.colNOAServico.Visible = true;
            this.colNOAServico.VisibleIndex = 1;
            this.colNOAServico.Width = 303;
            // 
            // colNOATotal1
            // 
            this.colNOATotal1.Caption = "Valor";
            this.colNOATotal1.DisplayFormat.FormatString = "n2";
            this.colNOATotal1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNOATotal1.FieldName = "NOATotal";
            this.colNOATotal1.Name = "colNOATotal1";
            this.colNOATotal1.OptionsColumn.ReadOnly = true;
            this.colNOATotal1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NOATotal", "{0:n2}")});
            this.colNOATotal1.Visible = true;
            this.colNOATotal1.VisibleIndex = 2;
            this.colNOATotal1.Width = 118;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.ColumnEdit = this.repItemButtonEditNF;
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ShowCaption = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 30;
            // 
            // gridView5
            // 
            this.gridView5.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView5.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView5.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView5.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView5.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView5.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView5.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView5.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView5.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView5.Appearance.Empty.Options.UseBackColor = true;
            this.gridView5.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView5.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView5.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView5.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView5.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView5.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView5.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView5.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView5.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView5.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView5.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView5.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(159)))), ((int)(((byte)(69)))));
            this.gridView5.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView5.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView5.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView5.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView5.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(152)))), ((int)(((byte)(49)))));
            this.gridView5.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.gridView5.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView5.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView5.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView5.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView5.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView5.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView5.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView5.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView5.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView5.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView5.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView5.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView5.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView5.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView5.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView5.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView5.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView5.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView5.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView5.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.gridView5.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.gridView5.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView5.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView5.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView5.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(176)))), ((int)(((byte)(84)))));
            this.gridView5.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.gridView5.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView5.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView5.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView5.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView5.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView5.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView5.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView5.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView5.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.gridView5.Appearance.Preview.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.gridView5.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView5.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(177)))), ((int)(((byte)(90)))));
            this.gridView5.Appearance.Preview.Options.UseBackColor = true;
            this.gridView5.Appearance.Preview.Options.UseBorderColor = true;
            this.gridView5.Appearance.Preview.Options.UseFont = true;
            this.gridView5.Appearance.Preview.Options.UseForeColor = true;
            this.gridView5.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView5.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.Row.Options.UseBackColor = true;
            this.gridView5.Appearance.Row.Options.UseForeColor = true;
            this.gridView5.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView5.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView5.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.gridView5.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView5.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView5.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView5.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView5.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNOA_PLA1,
            this.colNOAServico1,
            this.colNOATotal3});
            this.gridView5.GridControl = this.gridControl2;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.EnableAppearanceOddRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.PaintStyleName = "Flat";
            // 
            // colNOA_PLA1
            // 
            this.colNOA_PLA1.Caption = "C�digo";
            this.colNOA_PLA1.FieldName = "NOA_PLA";
            this.colNOA_PLA1.Name = "colNOA_PLA1";
            this.colNOA_PLA1.OptionsColumn.ReadOnly = true;
            this.colNOA_PLA1.Visible = true;
            this.colNOA_PLA1.VisibleIndex = 0;
            this.colNOA_PLA1.Width = 90;
            // 
            // colNOAServico1
            // 
            this.colNOAServico1.Caption = "Produto";
            this.colNOAServico1.FieldName = "NOAServico";
            this.colNOAServico1.Name = "colNOAServico1";
            this.colNOAServico1.OptionsColumn.ReadOnly = true;
            this.colNOAServico1.Visible = true;
            this.colNOAServico1.VisibleIndex = 1;
            this.colNOAServico1.Width = 191;
            // 
            // colNOATotal3
            // 
            this.colNOATotal3.Caption = "Valor";
            this.colNOATotal3.ColumnEdit = this.repositoryItemCalcValor;
            this.colNOATotal3.FieldName = "NOATotal";
            this.colNOATotal3.Name = "colNOATotal3";
            this.colNOATotal3.OptionsColumn.ReadOnly = true;
            this.colNOATotal3.Visible = true;
            this.colNOATotal3.VisibleIndex = 2;
            this.colNOATotal3.Width = 95;
            // 
            // repositoryItemCalcValor
            // 
            this.repositoryItemCalcValor.AutoHeight = false;
            this.repositoryItemCalcValor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcValor.Mask.EditMask = "n2";
            this.repositoryItemCalcValor.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcValor.Name = "repositoryItemCalcValor";
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "RPS";
            this.gridControl2.DataSource = this.dFaturamentoBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.LevelTemplate = this.gridView5;
            gridLevelNode3.RelationName = "FK_NOAxRPS_RPS";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gridControl2.Location = new System.Drawing.Point(0, 60);
            this.gridControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl2.MainView = this.gridView4;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox3,
            this.repositoryItemCalcValor,
            this.repositoryVerNota,
            this.repItemButtonEditNF1});
            this.gridControl2.Size = new System.Drawing.Size(1428, 647);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4,
            this.gridView5});
            // 
            // gridView4
            // 
            this.gridView4.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView4.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView4.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView4.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView4.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView4.Appearance.Empty.Options.UseBackColor = true;
            this.gridView4.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView4.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView4.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView4.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView4.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView4.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView4.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView4.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView4.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView4.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView4.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView4.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView4.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView4.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView4.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView4.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView4.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView4.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView4.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView4.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView4.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView4.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView4.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView4.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView4.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView4.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView4.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView4.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView4.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView4.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView4.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView4.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView4.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView4.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView4.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView4.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView4.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView4.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView4.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView4.Appearance.Preview.Options.UseBackColor = true;
            this.gridView4.Appearance.Preview.Options.UseFont = true;
            this.gridView4.Appearance.Preview.Options.UseForeColor = true;
            this.gridView4.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView4.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView4.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.Row.Options.UseBackColor = true;
            this.gridView4.Appearance.Row.Options.UseBorderColor = true;
            this.gridView4.Appearance.Row.Options.UseForeColor = true;
            this.gridView4.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView4.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView4.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView4.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView4.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView4.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView4.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView4.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView4.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRPS1,
            this.colRPS_NOA,
            this.colRPS_LRP,
            this.colRPSStatus1,
            this.colCONNome1,
            this.colCONCnpj1,
            this.colErro1,
            this.colNOATotal2,
            this.colCONCodigo1,
            this.colRPSVerificacao1,
            this.colRPSComp1,
            this.colRPS_PLA1,
            this.colRPSReterISS1,
            this.colRPSISS,
            this.colRPSReterContri1,
            this.colRPSRetIR1,
            this.colCON,
            this.colNOADataEmissao1,
            this.colNOANumero1,
            this.gridColVerNota,
            this.gridColumn4});
            this.gridView4.GridControl = this.gridControl2;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsDetail.ShowDetailTabs = false;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.EnableAppearanceOddRow = true;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.PaintStyleName = "Flat";
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNOADataEmissao1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCodigo1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNOANumero1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView4_RowCellStyle);
            this.gridView4.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView4_CustomRowCellEdit);
            // 
            // colRPS1
            // 
            this.colRPS1.Caption = "RPS";
            this.colRPS1.FieldName = "RPS";
            this.colRPS1.Name = "colRPS1";
            this.colRPS1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPS1.OptionsColumn.ReadOnly = true;
            this.colRPS1.Visible = true;
            this.colRPS1.VisibleIndex = 2;
            this.colRPS1.Width = 61;
            // 
            // colRPS_NOA
            // 
            this.colRPS_NOA.Caption = "NOA";
            this.colRPS_NOA.FieldName = "RPS_NOA";
            this.colRPS_NOA.Name = "colRPS_NOA";
            this.colRPS_NOA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPS_NOA.OptionsColumn.ReadOnly = true;
            // 
            // colRPS_LRP
            // 
            this.colRPS_LRP.Caption = "Lote";
            this.colRPS_LRP.FieldName = "RPS_LRP";
            this.colRPS_LRP.Name = "colRPS_LRP";
            this.colRPS_LRP.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPS_LRP.OptionsColumn.ReadOnly = true;
            this.colRPS_LRP.Visible = true;
            this.colRPS_LRP.VisibleIndex = 1;
            this.colRPS_LRP.Width = 61;
            // 
            // colRPSStatus1
            // 
            this.colRPSStatus1.Caption = "Status";
            this.colRPSStatus1.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colRPSStatus1.FieldName = "RPSStatus";
            this.colRPSStatus1.Name = "colRPSStatus1";
            this.colRPSStatus1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPSStatus1.OptionsColumn.ReadOnly = true;
            this.colRPSStatus1.Visible = true;
            this.colRPSStatus1.VisibleIndex = 11;
            this.colRPSStatus1.Width = 113;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colCONNome1
            // 
            this.colCONNome1.Caption = "Condom�nio";
            this.colCONNome1.FieldName = "CONNome";
            this.colCONNome1.Name = "colCONNome1";
            this.colCONNome1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colCONNome1.OptionsColumn.ReadOnly = true;
            this.colCONNome1.Visible = true;
            this.colCONNome1.VisibleIndex = 8;
            this.colCONNome1.Width = 228;
            // 
            // colCONCnpj1
            // 
            this.colCONCnpj1.Caption = "CNPJ";
            this.colCONCnpj1.FieldName = "CONCnpj";
            this.colCONCnpj1.Name = "colCONCnpj1";
            this.colCONCnpj1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colCONCnpj1.OptionsColumn.ReadOnly = true;
            this.colCONCnpj1.Visible = true;
            this.colCONCnpj1.VisibleIndex = 6;
            this.colCONCnpj1.Width = 114;
            // 
            // colErro1
            // 
            this.colErro1.FieldName = "Erro";
            this.colErro1.Name = "colErro1";
            this.colErro1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colErro1.OptionsColumn.ReadOnly = true;
            // 
            // colNOATotal2
            // 
            this.colNOATotal2.Caption = "Valor";
            this.colNOATotal2.ColumnEdit = this.repositoryItemCalcValor;
            this.colNOATotal2.FieldName = "NOATotal";
            this.colNOATotal2.Name = "colNOATotal2";
            this.colNOATotal2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colNOATotal2.OptionsColumn.ReadOnly = true;
            this.colNOATotal2.Visible = true;
            this.colNOATotal2.VisibleIndex = 10;
            // 
            // colCONCodigo1
            // 
            this.colCONCodigo1.Caption = "CODCON";
            this.colCONCodigo1.FieldName = "CONCodigo";
            this.colCONCodigo1.Name = "colCONCodigo1";
            this.colCONCodigo1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colCONCodigo1.OptionsColumn.ReadOnly = true;
            this.colCONCodigo1.Visible = true;
            this.colCONCodigo1.VisibleIndex = 7;
            this.colCONCodigo1.Width = 73;
            // 
            // colRPSVerificacao1
            // 
            this.colRPSVerificacao1.Caption = "Verifica��o";
            this.colRPSVerificacao1.FieldName = "RPSVerificacao";
            this.colRPSVerificacao1.Name = "colRPSVerificacao1";
            this.colRPSVerificacao1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPSVerificacao1.OptionsColumn.ReadOnly = true;
            this.colRPSVerificacao1.Visible = true;
            this.colRPSVerificacao1.VisibleIndex = 5;
            // 
            // colRPSComp1
            // 
            this.colRPSComp1.FieldName = "RPSComp";
            this.colRPSComp1.Name = "colRPSComp1";
            this.colRPSComp1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPSComp1.OptionsColumn.ReadOnly = true;
            // 
            // colRPS_PLA1
            // 
            this.colRPS_PLA1.Caption = "Produto";
            this.colRPS_PLA1.FieldName = "RPS_PLA";
            this.colRPS_PLA1.Name = "colRPS_PLA1";
            this.colRPS_PLA1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPS_PLA1.OptionsColumn.ReadOnly = true;
            this.colRPS_PLA1.Visible = true;
            this.colRPS_PLA1.VisibleIndex = 9;
            // 
            // colRPSReterISS1
            // 
            this.colRPSReterISS1.Caption = "Ret ISS";
            this.colRPSReterISS1.FieldName = "RPSReterISS";
            this.colRPSReterISS1.Name = "colRPSReterISS1";
            this.colRPSReterISS1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPSReterISS1.OptionsColumn.ReadOnly = true;
            this.colRPSReterISS1.Visible = true;
            this.colRPSReterISS1.VisibleIndex = 12;
            // 
            // colRPSISS
            // 
            this.colRPSISS.Caption = "Aliquota ISS";
            this.colRPSISS.FieldName = "RPSISS";
            this.colRPSISS.Name = "colRPSISS";
            this.colRPSISS.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPSISS.OptionsColumn.ReadOnly = true;
            // 
            // colRPSReterContri1
            // 
            this.colRPSReterContri1.Caption = "Ret";
            this.colRPSReterContri1.FieldName = "RPSReterContri";
            this.colRPSReterContri1.Name = "colRPSReterContri1";
            this.colRPSReterContri1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPSReterContri1.OptionsColumn.ReadOnly = true;
            this.colRPSReterContri1.Visible = true;
            this.colRPSReterContri1.VisibleIndex = 13;
            // 
            // colRPSRetIR1
            // 
            this.colRPSRetIR1.Caption = "Val IR";
            this.colRPSRetIR1.ColumnEdit = this.repositoryItemCalcValor;
            this.colRPSRetIR1.FieldName = "RPSRetIR";
            this.colRPSRetIR1.Name = "colRPSRetIR1";
            this.colRPSRetIR1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPSRetIR1.OptionsColumn.ReadOnly = true;
            this.colRPSRetIR1.Visible = true;
            this.colRPSRetIR1.VisibleIndex = 14;
            // 
            // colCON
            // 
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colCON.OptionsColumn.ReadOnly = true;
            // 
            // colNOADataEmissao1
            // 
            this.colNOADataEmissao1.Caption = "Emiss�o";
            this.colNOADataEmissao1.FieldName = "NOADataEmissao";
            this.colNOADataEmissao1.Name = "colNOADataEmissao1";
            this.colNOADataEmissao1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colNOADataEmissao1.OptionsColumn.ReadOnly = true;
            this.colNOADataEmissao1.Visible = true;
            this.colNOADataEmissao1.VisibleIndex = 3;
            // 
            // colNOANumero1
            // 
            this.colNOANumero1.Caption = "Nota";
            this.colNOANumero1.FieldName = "NOANumero";
            this.colNOANumero1.Name = "colNOANumero1";
            this.colNOANumero1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colNOANumero1.OptionsColumn.ReadOnly = true;
            this.colNOANumero1.Visible = true;
            this.colNOANumero1.VisibleIndex = 4;
            // 
            // gridColVerNota
            // 
            this.gridColVerNota.Caption = "gridColumn4";
            this.gridColVerNota.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColVerNota.Name = "gridColVerNota";
            this.gridColVerNota.OptionsColumn.ShowCaption = false;
            this.gridColVerNota.Visible = true;
            this.gridColVerNota.VisibleIndex = 16;
            this.gridColVerNota.Width = 30;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "gridColumn4";
            this.gridColumn4.ColumnEdit = this.repItemButtonEditNF1;
            this.gridColumn4.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 15;
            this.gridColumn4.Width = 30;
            // 
            // repItemButtonEditNF1
            // 
            this.repItemButtonEditNF1.AutoHeight = false;
            editorButtonImageOptions7.Image = global::dllFaturamento.Properties.Resources.Nota1;
            this.repItemButtonEditNF1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repItemButtonEditNF1.Name = "repItemButtonEditNF1";
            this.repItemButtonEditNF1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repItemButtonEditNF1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repItemButtonEditNF1_ButtonClick);
            // 
            // repositoryVerNota
            // 
            this.repositoryVerNota.AutoHeight = false;
            editorButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions8.Image")));
            this.repositoryVerNota.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryVerNota.Name = "repositoryVerNota";
            this.repositoryVerNota.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryVerNota.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryVerNota_ButtonClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1434, 72);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Location = new System.Drawing.Point(277, 6);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(152, 55);
            this.simpleButton5.TabIndex = 12;
            this.simpleButton5.Text = "Relat�rio";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(141, 6);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 5;
            this.cCompet1.Titulo = null;
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(6, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(117, 55);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Recalcular";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(823, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(231, 42);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Enviar Lote (MANUAL)";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Location = new System.Drawing.Point(4, 5);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(325, 42);
            this.simpleButton6.TabIndex = 13;
            this.simpleButton6.Text = "Imprime Notas";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Location = new System.Drawing.Point(692, 9);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(325, 58);
            this.simpleButton4.TabIndex = 11;
            this.simpleButton4.Text = "Recupera/Imprime Notas";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click_2);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(434, 7);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(97, 60);
            this.simpleButton3.TabIndex = 10;
            this.simpleButton3.Text = "Listar";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click_3);
            // 
            // TxCaminho
            // 
            this.TxCaminho.Location = new System.Drawing.Point(1097, 36);
            this.TxCaminho.Name = "TxCaminho";
            this.TxCaminho.Properties.ReadOnly = true;
            this.TxCaminho.Size = new System.Drawing.Size(306, 20);
            this.TxCaminho.TabIndex = 9;
            this.TxCaminho.Visible = false;
            // 
            // ButtonEmitir1
            // 
            this.ButtonEmitir1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEmitir1.Appearance.Options.UseFont = true;
            this.ButtonEmitir1.Enabled = false;
            this.ButtonEmitir1.Location = new System.Drawing.Point(5, 38);
            this.ButtonEmitir1.Name = "ButtonEmitir1";
            this.ButtonEmitir1.Size = new System.Drawing.Size(143, 27);
            this.ButtonEmitir1.TabIndex = 8;
            this.ButtonEmitir1.Text = "Enviar uma RPS";
            this.ButtonEmitir1.Click += new System.EventHandler(this.simpleButton4_Click_1);
            // 
            // ButReEnv
            // 
            this.ButReEnv.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButReEnv.Appearance.Options.UseFont = true;
            this.ButReEnv.Location = new System.Drawing.Point(311, 7);
            this.ButReEnv.Name = "ButReEnv";
            this.ButReEnv.Size = new System.Drawing.Size(117, 60);
            this.ButReEnv.TabIndex = 7;
            this.ButReEnv.Text = "Re - enivar";
            this.ButReEnv.Click += new System.EventHandler(this.simpleButton3_Click_2);
            // 
            // ButtonAss
            // 
            this.ButtonAss.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAss.Appearance.Options.UseFont = true;
            this.ButtonAss.Enabled = false;
            this.ButtonAss.Location = new System.Drawing.Point(537, 8);
            this.ButtonAss.Name = "ButtonAss";
            this.ButtonAss.Size = new System.Drawing.Size(149, 59);
            this.ButtonAss.TabIndex = 6;
            this.ButtonAss.Text = "Assembl�ia / 13�";
            this.ButtonAss.Click += new System.EventHandler(this.simpleButton3_Click_1);
            // 
            // ButtonVerificar
            // 
            this.ButtonVerificar.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonVerificar.Appearance.Options.UseFont = true;
            this.ButtonVerificar.Enabled = false;
            this.ButtonVerificar.Location = new System.Drawing.Point(154, 7);
            this.ButtonVerificar.Name = "ButtonVerificar";
            this.ButtonVerificar.Size = new System.Drawing.Size(151, 58);
            this.ButtonVerificar.TabIndex = 4;
            this.ButtonVerificar.Text = "Verificar Notas";
            this.ButtonVerificar.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // checkEditGravarArquivos
            // 
            this.checkEditGravarArquivos.Location = new System.Drawing.Point(1203, 11);
            this.checkEditGravarArquivos.Name = "checkEditGravarArquivos";
            this.checkEditGravarArquivos.Properties.Caption = "Gravar arquivos";
            this.checkEditGravarArquivos.Size = new System.Drawing.Size(111, 19);
            this.checkEditGravarArquivos.TabIndex = 3;
            this.checkEditGravarArquivos.Visible = false;
            this.checkEditGravarArquivos.CheckedChanged += new System.EventHandler(this.checkEditGravarArquivos_CheckedChanged);
            // 
            // ButtonEmitir
            // 
            this.ButtonEmitir.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEmitir.Appearance.Options.UseFont = true;
            this.ButtonEmitir.Enabled = false;
            this.ButtonEmitir.Location = new System.Drawing.Point(5, 5);
            this.ButtonEmitir.Name = "ButtonEmitir";
            this.ButtonEmitir.Size = new System.Drawing.Size(143, 27);
            this.ButtonEmitir.TabIndex = 2;
            this.ButtonEmitir.Text = "Enviar RPSs";
            this.ButtonEmitir.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 72);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1434, 735);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.panelControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1428, 707);
            this.xtraTabPage1.Text = "Lotes";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButton4);
            this.panelControl2.Controls.Add(this.ButtonEmitir);
            this.panelControl2.Controls.Add(this.ButtonEmitir1);
            this.panelControl2.Controls.Add(this.ButtonVerificar);
            this.panelControl2.Controls.Add(this.TxCaminho);
            this.panelControl2.Controls.Add(this.simpleButton3);
            this.panelControl2.Controls.Add(this.ButtonAss);
            this.panelControl2.Controls.Add(this.checkEditGravarArquivos);
            this.panelControl2.Controls.Add(this.ButReEnv);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1428, 84);
            this.panelControl2.TabIndex = 2;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Controls.Add(this.panelControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1428, 707);
            this.xtraTabPage2.Text = "Nota";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton6);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1428, 60);
            this.panelControl3.TabIndex = 3;
            // 
            // cFaturamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cFaturamento";
            this.Size = new System.Drawing.Size(1434, 807);
            this.Load += new System.EventHandler(this.cFaturamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomRowCellEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCompetenciaEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFaturamentoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFaturamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemButtonEditNF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcValor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemButtonEditNF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryVerNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxCaminho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGravarArquivos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dFaturamentoBindingSource;
        private dFaturamento dFaturamento;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colRPS;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colLRP;
        private DevExpress.XtraGrid.Columns.GridColumn colLRPProtocolo;
        private DevExpress.XtraGrid.Columns.GridColumn colLRPStatus;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDIBGE;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraGrid.Columns.GridColumn colCONBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCep;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colCONEndereco2;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSHistorico;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colErro;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraEditors.CheckEdit checkEditGravarArquivos;
        private DevExpress.XtraEditors.SimpleButton ButtonVerificar;
        private DevExpress.XtraGrid.Columns.GridColumn colLRPSEnvio;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSVerificacao;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.SimpleButton ButtonEmitir;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SimpleButton ButtonAss;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal;
        private DevExpress.XtraGrid.Columns.GridColumn colRPS_PLA;
        private DevExpress.XtraEditors.SimpleButton ButReEnv;
        private DevExpress.XtraEditors.SimpleButton ButtonEmitir1;
        private DevExpress.XtraEditors.TextEdit TxCaminho;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSReterContri;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSRetIR;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSReterISS;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal1;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit CustomRowCellEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADataEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSComp;
        private CompontesBasicos.RepositoryItemCompetenciaEdit repositoryItemCompetenciaEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repItemButtonEditNF;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colRPS1;
        private DevExpress.XtraGrid.Columns.GridColumn colRPS_NOA;
        private DevExpress.XtraGrid.Columns.GridColumn colRPS_LRP;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCnpj1;
        private DevExpress.XtraGrid.Columns.GridColumn colErro1;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal2;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo1;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSVerificacao1;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSComp1;
        private DevExpress.XtraGrid.Columns.GridColumn colRPS_PLA1;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSReterISS1;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSISS;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSReterContri1;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSRetIR1;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADataEmissao1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PLA1;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico1;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcValor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColVerNota;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryVerNota;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repItemButtonEditNF1;
    }
}
