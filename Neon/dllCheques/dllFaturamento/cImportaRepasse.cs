using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Framework;
using VirEnumeracoesNeon;
using Abstratos;
using AbstratosNeon;
using ContaPagarProc.LoteRepasse;
using dllClienteServProc;
using CompontesBasicosProc;
using CompontesBasicos;
using System.Threading;
using System.Drawing;


namespace dllFaturamento
{
    /// <summary>
    /// cImportaRepasse
    /// </summary>
    public partial class cImportaRepasse : Framework.ComponentesNeon.NavegadorCondominio
    {
        private ABS_Fornecedor Fornecedor1;
        private ABS_Fornecedor Fornecedor2;
        private dLoteRepasse dLoteRepasse1;
        /// <summary>
        /// Construtor
        /// </summary>
        public cImportaRepasse()
        {
            InitializeComponent();
            dLoteRepasse1 = new dLoteRepasse();
            importarBindingSource.DataSource = dLoteRepasse1;
            Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(colPagTipo);
            if (DSCentral.EMP == 1)
            {
                Fornecedor1 = ABS_Fornecedor.GetFornecedor(1119);
                Fornecedor2 = ABS_Fornecedor.GetFornecedor(2942);
            }
            else
            {
                Fornecedor1 = ABS_Fornecedor.GetFornecedor(172);
                Fornecedor2 = ABS_Fornecedor.GetFornecedor(7427);
            }            
        }

        

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma", "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                return;
            GeraEfetivo();            
        }

        private SortedList<int, ABS_Condominio> Condominos;

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dLoteRepasse1.FaturarTableAdapter.Fill(dLoteRepasse1.Faturar);

         

            //if ((datecom.DateTime == DateTime.MinValue) || (datesem.DateTime == DateTime.MinValue))
            //    return;
            dLoteRepasse1.Importar.Clear();
            Condominos = ABS_Condominio.ABSGetCondominios(false);
            //dRepasse.Materiais.Clear();
            //if (CON == -1)
            //{
            //Condominos = ABS_Condominio.ABSGetCondominios(true);                
            //dRepasse.FaturamentoTableAdapter.Fill(dRepasse.Faturamento);                
            //dRepasse.MateriaisTableAdapter.Fill(dRepasse.Materiais);
            //}
            //else
            //{
            Condominos = new SortedList<int, ABS_Condominio>();
            //    Condominos.Add(CON, ABS_Condominio.GetCondominio(CON));                
            //    dRepasse.FaturamentoTableAdapter.FillBy(dRepasse.Faturamento, Condominos[CON].CONCodigo);                
            //    dRepasse.MateriaisTableAdapter.FillBy(dRepasse.Materiais, Condominos[CON].CONCodigo);
            //}
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.Espere("Carregando dados");
                ESP.AtivaGauge(dRepasse.Faturamento.Count);
                int n = 0;

                foreach (dLoteRepasse.FaturarRow rowFAT in dLoteRepasse1.Faturar)
                {
                    n++;
                    ESP.GaugeTempo(n);
                    if (ESP.Abortar)
                    {
                        dLoteRepasse1.Importar.Clear();
                        MessageBox.Show("Cancelado pelo usu�rio");
                        break;
                    }
                    dLoteRepasse.ImportarRow novRow;
                    novRow = dLoteRepasse1.Importar.FindByCON(rowFAT.CON);
                    if (novRow == null)
                    {
                        novRow = dLoteRepasse1.Importar.NewImportarRow();
                        novRow.CON = rowFAT.CON;
                        //novRow.Data = DataFaturar;
                        novRow.NomeCond = string.Format("{0} - {1}", rowFAT.CONCodigo, rowFAT.CONNome);
                        novRow.Total = 0;                        
                        novRow.ComFaturamento = true;
                        try
                        {
                            ABS_ContaCorrente Conta = Condominos[rowFAT.CON].ABSConta;
                            novRow.PagTipo = (int)(Conta.PagamentoEletronico ? PAGTipo.PECreditoConta : PAGTipo.cheque);
                            if (Conta.CCT.HasValue)
                                novRow.CCT = Conta.CCT.Value;
                        }
                        catch
                        {
                            novRow.PagTipo = (int)(PAGTipo.cheque);
                        }
                    }
                    if (rowFAT.INS_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.RepasseDespAdmin))
                    {
                        novRow.Administrativas = rowFAT.ValorTotal;
                        novRow.Total += rowFAT.ValorTotal;
                    }
                    if (rowFAT.INS_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.RepasseCopias))
                    {
                        novRow.Xerox = rowFAT.ValorTotal; 
                        novRow.Total += rowFAT.ValorTotal;
                    }

                    if (rowFAT.INS_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.RepasseConducao))
                    {
                        novRow.Malotes = rowFAT.ValorTotal;
                        novRow.Total += rowFAT.ValorTotal;
                    }
                    if (rowFAT.INS_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.RepasseImpressos))
                    {
                        novRow.impressos = rowFAT.ValorTotal;
                        novRow.Total += rowFAT.ValorTotal;
                    };
                    if ((novRow.Total > 0) && (novRow.RowState == DataRowState.Detached))
                        dLoteRepasse1.Importar.AddImportarRow(novRow);
                }

                /*
                foreach (dRepasse.FaturamentoRow rowFAT in dRepasse.Faturamento)
                {
                    n++;
                    ESP.GaugeTempo(n);
                    if (ESP.Abortar)
                    {
                        dLoteRepasse1.Importar.Clear();
                        MessageBox.Show("Cancelado pelo usu�rio");
                        break;
                        
                    }
                    decimal CustoViagem = 0;
                    VirOleDb.TableAdapter.STTableAdapter.BuscaEscalar("SELECT [Valor Passagem]  FROM  [Faturamento Outros]", out CustoViagem);
                    //dCondominiosAtivos.CONDOMINIOSRow rowCON = null;

                    ABS_Condominio Condominio = null;

                    foreach (ABS_Condominio CONBusca in Condominos.Values)
                        if (CONBusca.CONCodigo == rowFAT.CODCON)
                        {
                            Condominio = CONBusca;
                            break;
                        }

                    if (Condominio != null)
                    {
                        DateTime DataFaturar = rowFAT.Emite_Boleto ? datecom.DateTime : datesem.DateTime;
                        dLoteRepasse.ImportarRow novRow;
                        novRow = dLoteRepasse1.Importar.FindByCON(Condominio.CON);
                        if (novRow == null)
                        {
                            novRow = dLoteRepasse1.Importar.NewImportarRow();
                            novRow.CON = Condominio.CON;                            
                            novRow.Data = DataFaturar;
                            novRow.NomeCond = string.Format("{0} - {1}", Condominio.CONCodigo, Condominio.Nome);
                            novRow.Total = 0;
                            //novRow.ComFaturamento = rowFAT.Emite_Boleto;
                            novRow.ComFaturamento = true;
                            try
                            {
                                ABS_ContaCorrente Conta = Condominio.ABSConta;
                                novRow.PagTipo = (int)(Conta.PagamentoEletronico ? PAGTipo.PECreditoConta : PAGTipo.cheque);
                                if (Conta.CCT.HasValue)
                                    novRow.CCT = Conta.CCT.Value;
                            }
                            catch 
                            {
                                novRow.PagTipo = (int)(PAGTipo.cheque);
                            }
                        }


                        if ((!rowFAT.IsDespesas_GeraisNull()) && (rowFAT.Despesas_Gerais > 0))
                        {
                            novRow.Administrativas = rowFAT.Despesas_Gerais;
                            novRow.Total += novRow.Administrativas;
                        }
                        if (rowFAT.Quantidade_Xerox * rowFAT.Valor_Xerox > 0)
                        {
                            novRow.Xerox = rowFAT.Quantidade_Xerox * rowFAT.Valor_Xerox;
                            novRow.Total += novRow.Xerox;
                        }

                        if (rowFAT.Viagens > 0)
                        {
                            novRow.Malotes = rowFAT.Viagens * CustoViagem;
                            novRow.Total += novRow.Malotes;
                        }



                        foreach (dRepasse.MateriaisRow rowMat in rowFAT.GetMateriaisRows())
                        {
                            novRow.impressos = rowMat.ValorTotal;
                            novRow.Total += novRow.impressos;
                        }
                        if ((novRow.Total > 0) && (novRow.RowState == DataRowState.Detached))
                            dLoteRepasse1.Importar.AddImportarRow(novRow);
                    }
                }*/
            }
            
            LigarBotao();
        }

        private const int TentavasRecuperar = 10;

        private RetornoServidorProcessos Retorno = null;

        private SPeriodicoNota.PeriodicoNotaClient PeriodicoNotaClient1;

        private string URLServidorProcessos
        {
            get
            {
                switch (ClienteServProc.TipoServidoSelecionado)
                {
                    case TiposServidorProc.Local:
                        return "WCFPeriodicoNotaL";
                    case TiposServidorProc.QAS:
                        return "WCFPeriodicoNotaH";
                    case TiposServidorProc.Producao:
                        return "WCFPeriodicoNotaP";
                    case TiposServidorProc.Indefinido:
                    case TiposServidorProc.Simulador:
                    default:
                        return "";
                }
            }
        }

        private RetornoServidorProcessos SimulaContaPagarProc_LoteRepasse_GeraChequesRepasse(dLoteRepasse _dLoteRepasse)
        {
            RetornoServidorProcessos Retorno = new RetornoServidorProcessos("OK", "OK");
            try
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = true;                
                FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Local, DSCentral.USU);
                LoteRepasse LoteRepasse1 = new LoteRepasse(EMPTProc1, _dLoteRepasse);
                //ABS_Fornecedor Fornecedor = ABS_Fornecedor.GetFornecedor(FRN);
                if (!LoteRepasse1.GeraChequesRepasse())
                {
                    Retorno.ok = false;
                    Retorno.Mensagem = "Erro ao gerar: Checar o motivo";
                    Retorno.MensagemSimplificada = "Erro ao gerar";
                    Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
                }
                return Retorno;
            }
            catch (Exception e)
            {
                Retorno.ok = false;
                Retorno.Mensagem = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, true, true, true);
                Retorno.Mensagem = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, false, false, false);
                Retorno.TipoDeErro = VirExceptionProc.VirExceptionProc.IdentificaTipoDeErro(e);
                return Retorno;
            }
            finally
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = false;
            }
        }

        //private ABS_Fornecedor FornecedorCom { get => (ABS_Fornecedor)cmbFornecedorCom.EditValue; }
        //private ABS_Fornecedor FornecedorSem { get => (ABS_Fornecedor)cmbFornecedorSem.EditValue; }

        private void ThreadChamadaAoServidor()
        {
            try
            {                
                //Retorno = PeriodicoNotaClient1.LoteRepasse_GeraChequesRepasse(VirCrip.VirCripWS.Crip(DSCentral.ChaveCriptografar()), dLoteRepasse1, FornecedorCom.FRN, FornecedorSem.FRN);
            }
            catch (Exception e)
            {
                Retorno = new RetornoServidorProcessos("Erro interno na chamada", string.Format("Erro interno na chamada:\r\n{0}", e.Message));
                Retorno.ok = false;
                Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
            }
        }

        private void GeraEfetivo()
        {

            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this, true))
            {

                //if (ClienteServProc.TipoServidoSelecionado != TiposServidorProc.SemServidor)
                if(1!=1)
                {

                    ESP.Espere("Gerando cheques");
                    ESP.AtivaGauge(30);
                    Application.DoEvents();
                    Thread ThreadRH = null;
                    bool terminado = false;
                    int tentativas = 0;
                    //ABS_Fornecedor Fornecedor = (ABS_Fornecedor)cmbFornecedor.EditValue;
                    dLoteRepasse dLote = (dLoteRepasse)dLoteRepasse1.Copy();
                    
                    while (!terminado && (tentativas < TentavasRecuperar))
                    {
                        tentativas++;
                        if (ClienteServProc.TipoServidoSelecionado == TiposServidorProc.Simulador)                        
                        {
                            ESP.Espere("ATEN��O: uso do simulador");
                            ThreadRH = new Thread(() => Retorno = SimulaContaPagarProc_LoteRepasse_GeraChequesRepasse(dLote));
                        }
                        else
                        {
                            PeriodicoNotaClient1 = new SPeriodicoNota.PeriodicoNotaClient(URLServidorProcessos);
                            ThreadRH = new Thread(new ThreadStart(ThreadChamadaAoServidor));
                        }
                        ThreadRH.Start();
                        DateTime proxima = DateTime.Now.AddSeconds(3);
                        while (ThreadRH.IsAlive)
                        {
                            if (DateTime.Now > proxima)
                            {
                                ESP.Gauge();
                                proxima = DateTime.Now.AddSeconds(1);
                            }
                            Application.DoEvents();
                        }
                        if (Retorno.ok)
                            terminado = true;
                        else
                        {
                            if (Retorno.TipoDeErro != VirExceptionProc.VirTipoDeErro.recuperavel)
                                terminado = true;
                            else
                            {
                                ESP.Espere(string.Format("Gravando (Tentativa {0})", tentativas));
                                ESP.AtivaGauge(30);
                                ESP.Gauge(0);
                            }
                        }
                    }
                    if (Retorno.ok)
                    {
                        MessageBox.Show("Cheques gerados");
                        dLoteRepasse1.Importar.Clear();
                    }
                    else
                    {
                        if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                        {
                            Clipboard.SetText(Retorno.Mensagem);
                            MessageBox.Show(Retorno.Mensagem);
                        }
                        switch (Retorno.TipoDeErro)
                        {
                            case VirExceptionProc.VirTipoDeErro.concorrencia:
                                MessageBox.Show("Os dados que deveriam ser gravados foram alterados por outro usu�rio.\r\n\r\nDADOS N�O GRAVADOS!!", "ATEN��O");
                                break;                            
                            case VirExceptionProc.VirTipoDeErro.efetivo_bug:
                                MessageBox.Show("Dados n�o gravados!! Erro reportado");
                                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", Retorno.Mensagem, "ERRO NO SERVIDOR DE PROCESSOS");
                                break;
                            case VirExceptionProc.VirTipoDeErro.local:
                                MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados n�o gravados!!", Retorno.MensagemSimplificada));
                                break;                            
                            case VirExceptionProc.VirTipoDeErro.recuperavel:
                                MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados n�o gravados!!", Retorno.MensagemSimplificada));
                                break;
                        }
                    }
                }
                else
                {
                    FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Local, DSCentral.USU);
                    dLoteRepasse dLote = (dLoteRepasse)dLoteRepasse1.Copy();
                    LoteRepasse LoteRepasse1 = new LoteRepasse(EMPTProc1, dLote);
                    //ABS_Fornecedor Fornecedor = ABS_Fornecedor.GetFornecedor(FRN);

                    if (!LoteRepasse1.GeraChequesRepasse())
                    {
                        Retorno.ok = false;
                        Retorno.Mensagem = "Erro ao gerar: Checar o motivo";
                        Retorno.MensagemSimplificada = "Erro ao gerar";
                        Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
                    }
                }
            }
        }       

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dLoteRepasse.ImportarRow row = (dLoteRepasse.ImportarRow)GridView_F.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if (e.Column == colPagTipo)
            {
                e.Appearance.BackColor = Enumeracoes.VirEnumPAGTipo.GetCor(row.PagTipo);
                e.Appearance.BackColor2 = Enumeracoes.VirEnumPAGTipo.GetCor(row.PagTipo);
                e.Appearance.ForeColor = Color.Black;
            }           
        }

        private void cmbFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            LigarBotao();
        }

        private void LigarBotao()
        {
            BotCadastrar.Enabled = (dLoteRepasse1.Importar.Count != 0);
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this, true))
            {

                if (ClienteServProc.TipoServidoSelecionado != TiposServidorProc.SemServidor)
                {

                    ESP.Espere("Gerando cheques");
                    ESP.AtivaGauge(30);
                    Application.DoEvents();
                    Thread ThreadRH = null;
                    bool terminado = false;
                    int tentativas = 0;
                    
                    dLoteRepasse dLote = (dLoteRepasse)dLoteRepasse1.Copy();

                    while (!terminado && (tentativas < TentavasRecuperar))
                    {
                        tentativas++;
                        if (ClienteServProc.TipoServidoSelecionado == TiposServidorProc.Simulador)
                        {
                            ESP.Espere("ATEN��O: uso do simulador");
                            ThreadRH = new Thread(() => Retorno = SimulaContaPagarProc_LoteRepasse_GeraChequesRepasse(dLote));
                        }
                        else
                        {
                            PeriodicoNotaClient1 = new SPeriodicoNota.PeriodicoNotaClient(URLServidorProcessos);                            
                        }
                        Retorno = PeriodicoNotaClient1.LoteRepasse_GeraChequesRepasse(VirCrip.VirCripWS.Crip(DSCentral.ChaveCriptografar()), dLoteRepasse1, 0, 0);
                        DateTime proxima = DateTime.Now.AddSeconds(3);                        
                        if (Retorno.ok)
                            terminado = true;
                        else
                        {
                            if (Retorno.TipoDeErro != VirExceptionProc.VirTipoDeErro.recuperavel)
                                terminado = true;
                            else
                            {
                                ESP.Espere(string.Format("Gravando (Tentativa {0})", tentativas));
                                ESP.AtivaGauge(30);
                                ESP.Gauge(0);
                            }
                        }
                    }
                    if (Retorno.ok)
                    {
                        MessageBox.Show("Cheques gerados");
                        dLoteRepasse1.Importar.Clear();
                    }
                    else
                    {
                        if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                        {
                            Clipboard.SetText(Retorno.Mensagem);
                            MessageBox.Show(Retorno.Mensagem);
                        }
                        switch (Retorno.TipoDeErro)
                        {
                            case VirExceptionProc.VirTipoDeErro.concorrencia:
                                MessageBox.Show("Os dados que deveriam ser gravados foram alterados por outro usu�rio.\r\n\r\nDADOS N�O GRAVADOS!!", "ATEN��O");
                                break;
                            case VirExceptionProc.VirTipoDeErro.efetivo_bug:
                                MessageBox.Show("Dados n�o gravados!! Erro reportado");
                                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", Retorno.Mensagem, "ERRO NO SERVIDOR DE PROCESSOS");
                                break;
                            case VirExceptionProc.VirTipoDeErro.local:
                                MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados n�o gravados!!", Retorno.MensagemSimplificada));
                                break;
                            case VirExceptionProc.VirTipoDeErro.recuperavel:
                                MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados n�o gravados!!", Retorno.MensagemSimplificada));
                                break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Processo antigo descontinuado, utilizar o simulado");
                    throw new Exception("Processo antigo descontinuado, utilizar o simulado");
                }
            }
        }



        //Corre��o (apagar)

       

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            string Nominal = Framework.DSCentral.EMP == 1 ? "NEON IM�VEIS E ADM LTDA -016" : "ADMINISTRADORA DE IMOVEIS NEON SA";
            string CandidatosAgrupar =
"SELECT        COUNT(dbo.CHEques.CHE) AS numero, dbo.CHEques.CHEVencimento, dbo.CHEques.CHE_CON, dbo.CHEques.CHE_CCT, dbo.CHEques.CHETipo\r\n" +
"FROM            dbo.CHEques\r\n" +
"WHERE        (dbo.CHEques.CHEFavorecido = '"+ Nominal +"') AND (dbo.CHEques.CHEStatus = 1) AND (dbo.CHEques.CHEVencimento > CONVERT(DATETIME, '2018-05-01 00:00:00', 102))\r\n" +
"GROUP BY dbo.CHEques.CHEFavorecido, dbo.CHEques.CHEVencimento, dbo.CHEques.CHE_CON, dbo.CHEques.CHE_CCT, dbo.CHEques.CHETipo\r\n" +
"HAVING        (COUNT(dbo.CHEques.CHE) > 1)";
            string BuscaSecundaria =
"SELECT   distinct     dbo.CHEques.CHE, dbo.PAGamentos.PAGPermiteAgrupar\r\n" +
"FROM            dbo.CHEques INNER JOIN\r\n" +
"                         dbo.PAGamentos ON dbo.CHEques.CHE = dbo.PAGamentos.PAG_CHE\r\n" +
"WHERE        \r\n" +
"(dbo.CHEques.CHEFavorecido = '" + Nominal + "') AND \r\n" +
"(dbo.CHEques.CHEStatus = 1) AND \r\n" +
"(dbo.CHEques.CHEVencimento = @P1) AND \r\n" +
"(dbo.CHEques.CHE_CON = @P2) AND \r\n" +
"(dbo.CHEques.CHETipo = 20) AND \r\n" +
"(dbo.CHEques.CHE_CCT = @P3)\r\n";
            //" AND (dbo.PAGamentos.PAGPermiteAgrupar = 0);";
            DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(CandidatosAgrupar);
            int conta = 0;
            foreach (DataRow DR in DT.Rows)
            {
                conta++;
                Console.WriteLine(string.Format("Conta: {0}/{1}",conta, DT.Rows.Count));
                DataTable DT1 = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(BuscaSecundaria,DR["CHEVencimento"], DR["CHE_CON"], DR["CHE_CCT"]);
                dllCheques.Cheque Ch0 = new dllCheques.Cheque((int)(DT1.Rows[0]["CHE"]));
                for (int i = 1;i< DT1.Rows.Count; i++)
                {
                    dllCheques.Cheque Ch = new dllCheques.Cheque((int)(DT1.Rows[i]["CHE"]));
                    Ch0.AgruparRecebe(Ch);
                }
            }
        }

        //Corre��o (apagar FIM)


        private DataView dV_INSumos_INSDescricao;

        private DataView DV_INSumos_INSDescricao
        {
            get
            {
                if (dV_INSumos_INSDescricao == null)
                {
                    dV_INSumos_INSDescricao = new DataView(dRepasse.INSumos);
                    dV_INSumos_INSDescricao.Sort = "INSDescricao,INSValor";
                };
                return dV_INSumos_INSDescricao;
            }
        }

        internal dRepasse.INSumosRow Busca_rowINSumos(string INSDescricao,decimal INSValor)
        {
            int ind = DV_INSumos_INSDescricao.Find(new object[] { INSDescricao,INSValor });
            return (ind < 0) ? null : (dRepasse.INSumosRow)(DV_INSumos_INSDescricao[ind].Row);
        }

        private DataView dV_REPasses_REP_CONint;

        private DataView DV_REPasses_REP_CONint
        {
            get
            {
                if (dV_REPasses_REP_CONint == null)
                {
                    dV_REPasses_REP_CONint = new DataView(dRepasse.REPasses);
                    dV_REPasses_REP_CONint.Sort = "REP_CON , REP_INS";
                };
                return dV_REPasses_REP_CONint;
            }
        }

        internal dRepasse.REPassesRow Busca_rowREPasses(int REP_CON, int REP_INS)
        {
            int ind = DV_REPasses_REP_CONint.Find(new object[] { REP_CON , REP_INS });
            return (ind < 0) ? null : (dRepasse.REPassesRow)(DV_REPasses_REP_CONint[ind].Row);
        }

        private DataView dV_INSumos_INS_PLA;

        private DataView DV_INSumos_INS_PLA
        {
            get
            {
                if (dV_INSumos_INS_PLA == null)
                {
                    dV_INSumos_INS_PLA = new DataView(dRepasse.INSumos);
                    dV_INSumos_INS_PLA.Sort = "INS_PLA";
                };
                return dV_INSumos_INS_PLA;
            }
        }

        internal dRepasse.INSumosRow Busca_rowINSumos(string INS_PLA)
        {
            int ind = DV_INSumos_INS_PLA.Find(new object[] { INS_PLA });
            return (ind < 0) ? null : (dRepasse.INSumosRow)(DV_INSumos_INS_PLA[ind].Row);
        }


        private void simpleButton4_Click(object sender, EventArgs e)
        {
            dRepasse.Materiais1TableAdapter.Fill(dRepasse.Materiais1);
            dRepasse.INSumosTableAdapter.Fill(dRepasse.INSumos);
            foreach (dRepasse.Materiais1Row rowAc in dRepasse.Materiais1)
            {
                dRepasse.INSumosRow rowMSSQL = Busca_rowINSumos(rowAc.Descri��o, rowAc.Valor_Unit�rio);
                if (rowMSSQL == null)
                {
                    rowMSSQL = dRepasse.INSumos.NewINSumosRow();
                    rowMSSQL.INSAtivo = true;
                    rowMSSQL.INSDescricao = rowAc.Descri��o;
                    rowMSSQL.INSUnidade = "p�";
                    rowMSSQL.INSValor = rowAc.Valor_Unit�rio;
                    rowMSSQL.INS_PLA = "240007";
                    dRepasse.INSumos.AddINSumosRow(rowMSSQL);
                }
                else
                {
                    if (rowMSSQL.INSValor != rowAc.Valor_Unit�rio)
                        MessageBox.Show(string.Format("CHECAR:{0} {1} {2}",rowAc.Descri��o, rowMSSQL.INSValor , rowAc.Valor_Unit�rio));
                }
            }
            dRepasse.INSumosTableAdapter.Update(dRepasse.INSumos);


            dRepasse.CONDOMINIOSTableAdapter.Fill(dRepasse.CONDOMINIOS);
            dRepasse.REPACTableAdapter.Fill(dRepasse.REPAC);
            dRepasse.REPassesTableAdapter.Fill(dRepasse.REPasses);
            foreach (dRepasse.REPACRow rowacr in dRepasse.REPAC)
            {
                dRepasse.CONDOMINIOSRow rowCON = dRepasse.CONDOMINIOS.FindByCONCodigo(rowacr.CODCON);                
                if (rowCON != null)
                {
                    dRepasse.INSumosRow rowINS = Busca_rowINSumos(rowacr.Descri��o, rowacr.Valor_Unit�rio);
                    dRepasse.REPassesRow nova = Busca_rowREPasses(rowCON.CON, rowINS.INS);
                    if (nova == null)
                    {
                        nova = dRepasse.REPasses.NewREPassesRow();
                        nova.REPQuantidade = rowacr.Quantidade;
                        nova.REP_CON = rowCON.CON;
                        nova.REP_INS = rowINS.INS;
                        dRepasse.REPasses.AddREPassesRow(nova);
                        dRepasse.REPassesTableAdapter.Update(nova);
                    }
                }
            }
            dRepasse.REPassesTableAdapter.Update(dRepasse.REPasses);

            dRepasse.INSumosRow row240000 = Busca_rowINSumos("240000");
            if (row240000 == null)
            {
                row240000 = dRepasse.INSumos.NewINSumosRow();
                row240000.INSAtivo = true;
                row240000.INSDescricao = "Despesas Administrativas";
                row240000.INSUnidade = "R$";
                row240000.INSValor = 1;
                row240000.INS_PLA = "240000";
                dRepasse.INSumos.AddINSumosRow(row240000);
                dRepasse.INSumosTableAdapter.Update(row240000);
            }

            dRepasse.INSumosRow row240001 = Busca_rowINSumos("240001");
            if (row240001 == null)
            {
                row240001 = dRepasse.INSumos.NewINSumosRow();
                row240001.INSAtivo = true;
                row240001.INSDescricao = "C�pias";
                row240001.INSUnidade = "Un";
                row240001.INSValor = 0.18M;
                row240001.INS_PLA = "240001";
                dRepasse.INSumos.AddINSumosRow(row240001);
                dRepasse.INSumosTableAdapter.Update(row240001);
            }

            dRepasse.INSumosRow row240004 = Busca_rowINSumos("240004");            
            if (row240004 == null)
            {
                row240004 = dRepasse.INSumos.NewINSumosRow();
                row240004.INSAtivo = true;
                row240004.INSDescricao = "Malotes";
                row240004.INSUnidade = "Un";
                row240004.INSValor = 4.5M;
                row240004.INS_PLA = "240004";
                dRepasse.INSumos.AddINSumosRow(row240004);
                dRepasse.INSumosTableAdapter.Update(row240004);
            }
            

            dRepasse.FaturamentoTableAdapter.Fill(dRepasse.Faturamento);
            
            foreach (dRepasse.FaturamentoRow rowFAT in dRepasse.Faturamento)
            {
                dRepasse.CONDOMINIOSRow rowCON = dRepasse.CONDOMINIOS.FindByCONCodigo(rowFAT.CODCON);
                if (rowCON == null)
                    continue;
                if ((!rowFAT.IsDespesas_GeraisNull()) && (rowFAT.Despesas_Gerais > 0))
                {
                    dRepasse.REPassesRow nova = Busca_rowREPasses(rowCON.CON, row240000.INS);
                    if (nova == null)
                    {
                        nova = dRepasse.REPasses.NewREPassesRow();
                        nova.REPQuantidade = (int)rowFAT.Despesas_Gerais;
                        nova.REP_CON = rowCON.CON;
                        nova.REP_INS = row240000.INS;
                        dRepasse.REPasses.AddREPassesRow(nova);
                        dRepasse.REPassesTableAdapter.Update(nova);
                    }
                }
                if (rowFAT.Quantidade_Xerox * rowFAT.Valor_Xerox > 0)
                {
                    dRepasse.REPassesRow nova = Busca_rowREPasses(rowCON.CON, row240001.INS);
                    if (nova == null)
                    {
                        nova = dRepasse.REPasses.NewREPassesRow();
                        nova.REPQuantidade = rowFAT.Quantidade_Xerox;
                        nova.REP_CON = rowCON.CON;
                        nova.REP_INS = row240001.INS;
                        if (rowFAT.Valor_Xerox != row240001.INSValor)
                            nova.REPValorDif = rowFAT.Valor_Xerox;
                        dRepasse.REPasses.AddREPassesRow(nova);
                        dRepasse.REPassesTableAdapter.Update(nova);
                    }
                }

                if (rowFAT.Viagens > 0)
                {
                    dRepasse.REPassesRow nova = Busca_rowREPasses(rowCON.CON, row240004.INS);
                    if (nova == null)
                    {
                        nova = dRepasse.REPasses.NewREPassesRow();
                        nova.REPQuantidade = rowFAT.Viagens;
                        nova.REP_CON = rowCON.CON;
                        nova.REP_INS = row240004.INS;
                        dRepasse.REPasses.AddREPassesRow(nova);
                        dRepasse.REPassesTableAdapter.Update(nova);
                    }
                }
            }
            
            MessageBox.Show(string.Format("ok"));
        }
    }
}
