using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using dllEmissorNFS_e;
using DocBacarios;
using Framework.objetosNeon;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace dllFaturamento
{
    /// <summary>
    /// cFaturamento
    /// </summary>
    public partial class cFaturamento : CompontesBasicos.ComponenteBase
    {

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumStatusRPS : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumStatusRPS(Type Tipo):base(Tipo)
            {
            }

            private static virEnumStatusRPS _virEnumStatusRPSSt;

            /// <summary>
            /// VirEnum est�tico para campo StatusRPS
            /// </summary>
            public static virEnumStatusRPS virEnumStatusRPSSt
            {
                get
                {
                    if (_virEnumStatusRPSSt == null)
                    {
                        _virEnumStatusRPSSt = new virEnumStatusRPS(typeof(StatusRPS));
                        _virEnumStatusRPSSt.GravaNomes(StatusRPS.ComErro, "Com erro");
                        _virEnumStatusRPSSt.GravaNomes(StatusRPS.Convertida, "Convertida");
                        _virEnumStatusRPSSt.GravaNomes(StatusRPS.NaoEnviada, "N�o Enviada");
                        _virEnumStatusRPSSt.GravaNomes(StatusRPS.Enviada, "Enviada");
                        _virEnumStatusRPSSt.GravaCor(StatusRPS.ComErro, Color.Red);
                        _virEnumStatusRPSSt.GravaCor(StatusRPS.Convertida, Color.Lime);
                        _virEnumStatusRPSSt.GravaCor(StatusRPS.NaoEnviada, Color.White);
                        _virEnumStatusRPSSt.GravaCor(StatusRPS.Enviada, Color.Yellow);

                    }
                    return _virEnumStatusRPSSt;
                }
            }
        }

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumStatusLote : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumStatusLote(Type Tipo):base(Tipo)
            {
            }

            private static virEnumStatusLote _virEnumStatusLoteSt;

            /// <summary>
            /// VirEnum est�tico para campo StatusLote
            /// </summary>
            public static virEnumStatusLote virEnumStatusLoteSt
            {
                get
                {
                    if (_virEnumStatusLoteSt == null)
                    {
                        _virEnumStatusLoteSt = new virEnumStatusLote(typeof(StatusLote));
                        _virEnumStatusLoteSt.GravaNomes(StatusLote.aberto, "Aberto");
                        _virEnumStatusLoteSt.GravaNomes(StatusLote.geradoManual, "Gerado Manual");
                        _virEnumStatusLoteSt.GravaNomes(StatusLote.geradoWS, "Gerado WS");
                        _virEnumStatusLoteSt.GravaNomes(StatusLote.retornado, "Retornado");
                        _virEnumStatusLoteSt.GravaCor(StatusLote.aberto, Color.Aqua);
                        _virEnumStatusLoteSt.GravaCor(StatusLote.geradoManual, Color.Yellow);
                        _virEnumStatusLoteSt.GravaCor(StatusLote.geradoWS, Color.Yellow);
                        _virEnumStatusLoteSt.GravaCor(StatusLote.retornado, Color.Lime);
                        //_virEnumStatusLoteSt.GravaCor(StatusLote.XXX, System.Drawing.Color.White);

                    }
                    return _virEnumStatusLoteSt;
                }
            }
        }

        private int IM
        {
            get { return Framework.DSCentral.EMP == 1 ? 46653 : 161504; }
        }

        private int CIDADEIBGE
        {
            get { return Framework.DSCentral.EMP == 1 ? 3548708 : 3547809; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cFaturamento()
        {
            InitializeComponent();
            virEnumStatusRPS.virEnumStatusRPSSt.CarregaEditorDaGrid(colRPSStatus);
            virEnumStatusRPS.virEnumStatusRPSSt.CarregaEditorDaGrid(colRPSStatus1);
            virEnumStatusLote.virEnumStatusLoteSt.CarregaEditorDaGrid(colLRPStatus);
            if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU == 30)
                simpleButton4.Visible =  true;
        }

        private dFaturamento.LoteRPsRow rowLoteAberto = null;

        private Framework.objetosNeon.Competencia CompAtual;

        private void cFaturamento_Load(object sender, EventArgs e)
        {
            CompAtual = new Framework.objetosNeon.Competencia(DateTime.Today.Month,DateTime.Today.Year);

            cCompet1.Comp = CompAtual.CloneCompet((DateTime.Today.Day < 15) ? -1:0);            
            cCompet1.AjustaTela();
            //Carregar();            
        }

        private dFaturamento.LoteRPsRow AbreLote(int RPSminimo)
        {
            foreach (dFaturamento.LoteRPsRow rowLRP in dFaturamento.LoteRPs)
                if (((StatusLote)rowLRP.LRPStatus == StatusLote.aberto) && (rowLRP.LRP > RPSminimo))
                {
                    return rowLRP;                    
                }
            dFaturamento.LoteRPsRow LRPAberto = dFaturamento.LoteRPs.NewLoteRPsRow();
            LRPAberto.LRPStatus = (int)StatusLote.aberto;
            LRPAberto.LRPComp = cCompet1.Comp.CompetenciaBind;
            dFaturamento.LoteRPs.AddLoteRPsRow(LRPAberto);
            dFaturamento.LoteRPsTableAdapter.Update(LRPAberto);
            LRPAberto.AcceptChanges();
            return LRPAberto;
        }

        /// <summary>
        /// Reorganiza os lotes colocando as notas n�o emitidas em um lote aberto.
        /// </summary>
        private void ReorngamizaLotes()
        {
            foreach (dFaturamento.LoteRPsRow rowLRP in dFaturamento.LoteRPs)
                if ((StatusLote)rowLRP.LRPStatus == StatusLote.aberto)
                {
                    if (rowLoteAberto == null)
                        rowLoteAberto = rowLRP;

                    else
                        foreach (dFaturamento.RPSRow rowRPS in rowLRP.GetRPSRows())
                        {
                            if ((StatusRPS)rowRPS.RPSStatus == StatusRPS.NaoEnviada)
                            {
                                rowRPS.RPS_LRP = rowLoteAberto.LRP;
                                dFaturamento.RPSTableAdapter.Update(rowRPS);
                                rowRPS.AcceptChanges();
                            }
                        }
                }
                else
                    if (((StatusLote)rowLRP.LRPStatus == StatusLote.geradoManual)
                        ||
                        ((StatusLote)rowLRP.LRPStatus == StatusLote.geradoWS))
                {
                    bool TodosOK = true;
                    foreach (dFaturamento.RPSRow rowRPS in rowLRP.GetRPSRows())
                    {
                        if ((StatusRPS)rowRPS.RPSStatus != StatusRPS.Convertida)
                            TodosOK = false;
                    }
                    if (TodosOK)
                    {
                        rowLRP.LRPStatus = (int)StatusLote.retornado;
                        dFaturamento.LoteRPsTableAdapter.Update(rowLRP);
                        rowLRP.AcceptChanges();
                    }
                }
            if (rowLoteAberto == null)
            {
                rowLoteAberto = AbreLote(-1);
            }
        }

        private void Carregar()
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.Espere("Ajusta reten��es");
                AjustarRetencoes();
                dFaturamento.RPS.Clear();
                rowLoteAberto = null;
                ESP.Espere("Carrega Notas");
                dFaturamento.NOAxRPSTableAdapter.FillByComp(dFaturamento.NOAxRPS, cCompet1.Comp.CompetenciaBind);
                ESP.Espere("Carrega RPS");
                dFaturamento.RPSTableAdapter.FillComDados(dFaturamento.RPS, cCompet1.Comp.CompetenciaBind);
                ESP.Espere("Carrega Lotes");
                dFaturamento.LoteRPsTableAdapter.FillByComp(dFaturamento.LoteRPs, cCompet1.Comp.CompetenciaBind);                                                
                ESP.Espere("Reorganiza Lotes");
                ReorngamizaLotes();

                if (cCompet1.Comp >= CompAtual.CloneCompet(-1))
                {                    
                        string Erros = "";
                        bool RecarregarComRPS = false;
                        ESP.Espere("Carregando dados 230001");
                        if (dFaturamento.NovosTableAdapter.Fill(dFaturamento.Novos, PadraoPLA.PLAPadraoCodigo(PLAPadrao.TaxaDeADM), cCompet1.Comp.CompetenciaBind) > 0)
                        {
                            RecarregarComRPS = true;
                            ESP.AtivaGauge(dFaturamento.Novos.Count);
                            foreach (dFaturamento.NovosRow novarow in dFaturamento.Novos)
                            {
                                ESP.Gauge();
                                CadastraNF(novarow, ref Erros);
                            };

                        };
                        if (RecarregarComRPS)
                        {
                            dFaturamento.RPSTableAdapter.FillComDados(dFaturamento.RPS, cCompet1.Comp.CompetenciaBind);
                            dFaturamento.NOAxRPSTableAdapter.FillByComp(dFaturamento.NOAxRPS, cCompet1.Comp.CompetenciaBind);
                            RecarregarComRPS = false;
                        }
                        //foreach (PLAPadrao PLA in Colecoes.PLAsRepasse)
                        //{
                        ESP.Espere(string.Format("Carregando dados Repasse"));
                        if (dFaturamento.NovosTableAdapter.FillByRepasse(dFaturamento.Novos, cCompet1.Comp.CompetenciaBind) > 0)
                        {
                            RecarregarComRPS = true;
                            ESP.AtivaGauge(dFaturamento.Novos.Count);
                            foreach (dFaturamento.NovosRow novarow in dFaturamento.Novos)
                            {
                                ESP.Gauge();
                                CadastraNFRepasse(novarow, ref Erros);
                            };

                        };
                        //}
                        if (RecarregarComRPS)
                        {
                            dFaturamento.RPSTableAdapter.FillComDados(dFaturamento.RPS, cCompet1.Comp.CompetenciaBind);
                            dFaturamento.NOAxRPSTableAdapter.FillByComp(dFaturamento.NOAxRPS, cCompet1.Comp.CompetenciaBind);
                        }
                        if (Erros != "")
                        {
                            Clipboard.SetText(Erros);
                            MessageBox.Show(string.Format("ERROS na carga (dados na �rea de transfer�ncia):\r\n{0}", Erros));
                        }
                    

                };
                ESP.Espere("Validando Dados");
                dFaturamento.ValidaTodas();
                ButtonVerificar.Enabled = (cCompet1.Comp <= CompAtual);
                ButReEnv.Enabled = ButtonAss.Enabled = ButtonEmitir.Enabled = (cCompet1.Comp >= CompAtual.CloneCompet(-1));
                ButtonEmitir1.Enabled = ButtonEmitir1.Enabled = true;
            }            
        }



        //private bool EmTESTE = true;

        private bool CadastraNFRepasse(dFaturamento.NovosRow novarow, ref string Erros)
        {
            dFaturamento.RPSRow rowRPSTaxa = null;
            foreach (dFaturamento.RPSRow rowRPS in dFaturamento.RPS)
                if ((rowRPS.CON == novarow.CON) && (rowRPS.RPS_PLA == PadraoPLA.PLAPadraoCodigo(PLAPadrao.TaxaDeADM)))
                {
                    rowRPSTaxa = rowRPS;
                    if ((StatusRPS)rowRPSTaxa.RPSStatus == StatusRPS.NaoEnviada)
                        break;
                }
            if (rowRPSTaxa == null)
            {
                Erros += string.Format("\r\n erro Repasse sem faturamento - Nota ignorada\r\nCondom�nio:{0}", novarow.CONCodigo);
                return false;
            }
            if((StatusRPS)rowRPSTaxa.RPSStatus != StatusRPS.NaoEnviada)
            {
                Erros += string.Format("\r\n erro Repasse com nota de faturamento j� emitida - Nota ignorada ({0})\r\nCondom�nio:{1}", novarow.CONCodigo, novarow.NOA);
                return false;
            }
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("cFaturamento.cs 292", dFaturamento.NOAxRPSTableAdapter);
                rowRPSTaxa.RPSHistorico += string.Format("\r\nInclu�do repasse {0}",novarow.NOA_PLA);
                //ContaPagarProc.NotaProc notaAgregar = new ContaPagarProc.NotaProc(novarow.NOA);
                //List<TipoImposto> ReterImp = new List<TipoImposto>();
                //if (rowRPSTaxa.RPSReterISS)                                    
                //    ReterImp.Add(Framework.DSCentral.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA);                                   
                //if (rowRPSTaxa.RPSReterContri)
                //    ReterImp.Add(TipoImposto.PIS_COFINS_CSLL);
                //if (ReterImp.Count > 0)
                //    notaAgregar.ReterImposto(ReterImp);                
                dFaturamento.NOAxRPSRow novaNOARPS = dFaturamento.NOAxRPS.NewNOAxRPSRow();
                novaNOARPS.NOA = novarow.NOA;
                novaNOARPS.RPS = rowRPSTaxa.RPS;
                dFaturamento.NOAxRPS.AddNOAxRPSRow(novaNOARPS);
                dFaturamento.NOAxRPSTableAdapter.Update(novaNOARPS);
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }
            return true;
        }

        private bool CadastraNF(dFaturamento.NovosRow novarow,ref string Erros)
        {
            decimal aliISS = dllImpostos.Imposto.AliquotaBase(Framework.DSCentral.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA);
            bool reter = false;
            bool reterContribuicoes = (dFaturamento.BuscaISSTableAdapter.FillByPisCofCsll(dFaturamento.BuscaISS, novarow.NOA) == 1);
            decimal? retIR = null;
            if (dFaturamento.BuscaISSTableAdapter.FillByIR(dFaturamento.BuscaISS, novarow.NOA) == 1)
                retIR = dFaturamento.BuscaISS[0].PAGValor;
            int LinhasISS = dFaturamento.BuscaISSTableAdapter.Fill(dFaturamento.BuscaISS, novarow.NOA);
            if (LinhasISS == 0)
            {                
                if ((novarow.CIDIBGE == CIDADEIBGE) && (Framework.DSCentral.EMP == 1))
                {
                    Erros += string.Format("\r\n erro na reten��o - Sem Reten��o para mesma cidade - Nota ignorada\r\nCondom�nio:{0}", novarow.CONCodigo);
                    return false;
                }
            }
            else if (LinhasISS == 1)
            {
                reter = true;
                if (Math.Abs(dFaturamento.BuscaISS[0].NOATotal * aliISS - dFaturamento.BuscaISS[0].PAGValor) > 0.02M)
                {
                    Erros += string.Format("\r\n erro na reten��o - Valor n�o confere - Nota ignorada\r\nCondom�nio:{0}", novarow.CONCodigo);
                    return false;
                }
                if (novarow.CIDIBGE != CIDADEIBGE)
                {
                    Erros += string.Format("\r\n erro na reten��o - Reten��o para outra cidade - Nota ignorada\r\nCondom�nio:{0}", novarow.CONCodigo);
                    return false;
                }
            }
            else
            {
                Erros += string.Format("\r\n erro na reten��o - Mais de uma linha de ISS - Nota ignorada\r\nCondom�nio:{0}", novarow.CONCodigo);
                return false;
            }
            dFaturamento.RPSRow NovaRPS = dFaturamento.RPS.NewRPSRow();
            NovaRPS.RPS_PLA = novarow.NOA_PLA;
            NovaRPS.RPSComp = cCompet1.Comp.CompetenciaBind;
            NovaRPS.RPS_LRP = rowLoteAberto.LRP;
            NovaRPS.RPS_NOA = novarow.NOA;
            NovaRPS.RPSHistorico = string.Format("{0:dd/MM/yyyy HH:mm:ss} Incluida no lote\r\n", DateTime.Now);
            NovaRPS.RPSISS = (Framework.DSCentral.EMP == 1) ? 2 : 3;
            NovaRPS.RPSReterISS = reter;
            NovaRPS.RPSReterContri = reterContribuicoes;
            NovaRPS.RPSStatus = (int)StatusRPS.NaoEnviada;
            if (retIR.HasValue)
                NovaRPS.RPSRetIR = retIR.Value;
            dFaturamento.RPS.AddRPSRow(NovaRPS);
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("cFaturamento.cs 293", dFaturamento.RPSTableAdapter, dFaturamento.NOAxRPSTableAdapter);
                dFaturamento.RPSTableAdapter.Update(NovaRPS);
                dFaturamento.NOAxRPSRow novaNOARPS = dFaturamento.NOAxRPS.NewNOAxRPSRow();
                novaNOARPS.NOA = novarow.NOA;
                novaNOARPS.RPS = NovaRPS.RPS;
                dFaturamento.NOAxRPS.AddNOAxRPSRow(novaNOARPS);
                dFaturamento.NOAxRPSTableAdapter.Update(novaNOARPS);
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }            
            return true;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            /*
            Apoio.FazerValidacao = checkEditGravarArquivos.Checked;
            int MaxPorLote;
            if (!VirInput.Input.Execute("N. m�x. de RPS no lote", out MaxPorLote))
                return;

            bool NenhumRPS = true;
            foreach (dFaturamento.RPSRow rowRPS in rowLoteAberto.GetRPSRows())
                if ((StatusRPS)rowRPS.RPSStatus == StatusRPS.NaoEnviada)
                {
                    NenhumRPS = false;
                    break;
                };
            if (NenhumRPS)
                return;

            dFaturamento.LoteRPsRow novoLoteAberto = AbreLote(rowLoteAberto.LRP);

            CPFCNPJ cnpjEmissor = new CPFCNPJ("55.055.651/0001-70");

            LoteRPS Lote = new LoteRPS(VerssaoXSD.v202, cnpjEmissor, 46653, 3548708, rowLoteAberto.LRP);
            int Contador = 0;
            foreach (dFaturamento.RPSRow rowRPS in rowLoteAberto.GetRPSRows())
            {
                if (((StatusRPS)rowRPS.RPSStatus != StatusRPS.NaoEnviada) || (Contador >= MaxPorLote))
                {
                    rowRPS.RPS_LRP = novoLoteAberto.LRP;
                    dFaturamento.RPSTableAdapter.Update(rowRPS);
                    rowRPS.AcceptChanges();
                    continue;
                }

                CPFCNPJ cnpjTomador = new CPFCNPJ(rowRPS.CONCnpj);
                //validar CNPJ
                //validar demais campos
                
                LoteRPS.virTomador Tomador = new LoteRPS.virTomador(VerssaoXSD.v202,
                                                                    cnpjTomador, 
                                                                    rowRPS.CONNome,
                                                                    rowRPS.CONEndereco2,
                                                                    rowRPS.CONNumero,
                                                                    rowRPS.CONBairro,
                                                                    rowRPS.CIDIBGE,
                                                                    rowRPS.CIDUf,
                                                                    int.Parse(rowRPS.CONCep),
                                                                    "Telefone",
                                                                    "nf-e_cond@neonimoveis.com.br");
                LoteRPS.RetIss ret;
                if (rowRPS.CIDIBGE == 3548708)
                    ret = LoteRPS.RetIss.comRetencao;
                else
                    ret = LoteRPS.RetIss.semRetencao;
                Lote.IncluirRPS(rowRPS.NOATotal, 2M, ret, Tomador, rowRPS.RPS,(rowRPS.RPS_PLA == "230001" ? "Taxa de administra��o" : "Tx. Assembleia"));
                Contador++;
            }
            string Caminho = string.Format("{0}\\Arquivos\\", System.IO.Path.GetDirectoryName(Application.ExecutablePath));
            if (!System.IO.Directory.Exists(Caminho))
                System.IO.Directory.CreateDirectory(Caminho);
            if (Lote.GeraXML(Caminho, ArquivoNaoAssinado.RAM))
            {
                rowLoteAberto.LRPStatus = (int)StatusLote.geradoManual;
                dFaturamento.LoteRPsTableAdapter.Update(rowLoteAberto);
                rowLoteAberto.AcceptChanges();
                foreach (dFaturamento.RPSRow rowRPS in rowLoteAberto.GetRPSRows())                            
                {
                    rowRPS.RPSStatus = (int)StatusRPS.Enviada;
                    dFaturamento.RPSTableAdapter.Update(rowRPS);
                    rowRPS.AcceptChanges();                    
                }
                rowLoteAberto = novoLoteAberto;
                MessageBox.Show("ok");
            }
            else
                MessageBox.Show(string.Format("Erro\r\n{0}", Lote.strErro));
            */
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Carregar();
        }

        private string _caminho;

        private string Caminho
        {
            get 
            {
                if (_caminho == null)
                {
                    _caminho = string.Format("{0}\\Arquivos\\", System.IO.Path.GetDirectoryName(Application.ExecutablePath));
                    if (!System.IO.Directory.Exists(_caminho))
                        System.IO.Directory.CreateDirectory(_caminho);
                }
                return _caminho;
            }
        }

        private static string EmailRetorno
        {
            get
            {
                return Framework.DSCentral.EMP == 1 ? "nfe_cond@neonimoveis.com.br" : "eduardo@neonimoveis.com.br";
            }
        }

        private static string TelefoneRetorno
        {
            get
            {
                return Framework.DSCentral.EMProw.EMPFone;                
            }
        }

        private bool EnviarLoteAberto(int RPSEspecifico, bool Taxa, bool Assembleia, bool Taxa13, bool Dirf)
        {
            //string relatorio = "";
            Apoio.FazerValidacao = checkEditGravarArquivos.Checked;
            int MaxPorLote = 10;
            
            bool NenhumRPS = true;
            foreach (dFaturamento.RPSRow rowRPS in rowLoteAberto.GetRPSRows())
                if (RPSEspecifico == -1)
                {
                    if ((StatusRPS)rowRPS.RPSStatus == StatusRPS.NaoEnviada)
                    {
                        NenhumRPS = false;
                        break;
                    };
                }
                else 
                {
                    if ((rowRPS.RPS == RPSEspecifico) && ((StatusRPS)rowRPS.RPSStatus == StatusRPS.NaoEnviada))
                    {
                        NenhumRPS = false;
                        break;
                    };
                }
            if (NenhumRPS)
                return false;

            dFaturamento.LoteRPsRow novoLoteAberto = AbreLote(rowLoteAberto.LRP);

            //CPFCNPJ cnpjEmissor = new CPFCNPJ("55.055.651/0001-70");

            //MaxPorLote = 1;

            LoteRPS Lote = new LoteRPS(VerssaoXSD.v300, cnpjEmissor, IM, CIDADEIBGE, rowLoteAberto.LRP);
            int Contador = 0;
            foreach (dFaturamento.RPSRow rowRPS in rowLoteAberto.GetRPSRows())
            {
                bool ReAlocar;
                if (RPSEspecifico == -1)
                {
                    ReAlocar = ((StatusRPS)rowRPS.RPSStatus != StatusRPS.NaoEnviada) || (Contador >= MaxPorLote);
                    if ((rowRPS.RPS_PLA == "230001") && !Taxa)
                        ReAlocar = true;
                    if ((rowRPS.RPS_PLA == "230002") && !Assembleia)
                        ReAlocar = true;
                    if ((rowRPS.RPS_PLA == "230008") && !Taxa13)
                        ReAlocar = true;
                    if ((rowRPS.RPS_PLA == "220034") && !Dirf)
                        ReAlocar = true;
                }
                else
                    ReAlocar = ((StatusRPS)rowRPS.RPSStatus != StatusRPS.NaoEnviada) || (Contador >= MaxPorLote) || (rowRPS.RPS != RPSEspecifico);
                if (ReAlocar)
                {
                    rowRPS.RPS_LRP = novoLoteAberto.LRP;
                    dFaturamento.RPSTableAdapter.Update(rowRPS);
                    rowRPS.AcceptChanges();
                    continue;
                }

                CPFCNPJ cnpjTomador = new CPFCNPJ(rowRPS.CONCnpj);                                
                LoteRPS.virTomador Tomador = new LoteRPS.virTomador(VerssaoXSD.v300,
                                                                    cnpjTomador,
                                                                    rowRPS.CONNome,
                                                                    rowRPS.CONEndereco2,
                                                                    rowRPS.CONNumero,
                                                                    rowRPS.CONBairro,
                                                                    rowRPS.CIDIBGE,
                                                                    rowRPS.CIDUf,
                                                                    int.Parse(rowRPS.CONCep),
                                                                    TelefoneRetorno,
                                                                    EmailRetorno);
                LoteRPS.RetIss ret;
                //if (rowRPS.CIDIBGE == CIDADEIBGE)
                //    ret = LoteRPS.RetIss.comRetencao;
                //else
                //    ret = LoteRPS.RetIss.semRetencao;

                ret = rowRPS.RPSReterISS ? LoteRPS.RetIss.comRetencao : LoteRPS.RetIss.semRetencao;
                DateTime EmissaoRPS;
                Competencia CompRPS = new Competencia(rowRPS.RPSComp);
                if (CompRPS == (new Competencia(DateTime.Today)))
                    EmissaoRPS = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                else
                {
                    Competencia CompRPSpos = CompRPS.CloneCompet(1);
                    EmissaoRPS = new DateTime(CompRPSpos.Ano, CompRPSpos.Mes, 1).AddDays(-1);
                };
                string txtServ = "";
                decimal TotalGeralDaNota = 0;
                foreach (dFaturamento.NOAxRPSRow rowsomar in rowRPS.GetNOAxRPSRows())
                {
                    TotalGeralDaNota += rowsomar.NOATotal;
                    if (rowRPS.RPS_PLA == "230001")
                        txtServ += string.Format("{0} R$ {1:n2}|Quebra|", rowsomar.NOAServico, rowsomar.NOATotal);
                }
                //decimal ValorCarga = rowRPS.NOATotal * 0.1785m;
                decimal ValorCarga = TotalGeralDaNota * 0.1785m;
                if (rowRPS.RPS_PLA == "230001")
                    txtServ += string.Format("Valor aproximado dos Tributos: R$ {0:n2} (17,85 %) Fonte: IBPT",ValorCarga);
                else if (rowRPS.RPS_PLA == "230002")
                    txtServ = string.Format("Tx. Assembleia Valor aproximado dos Tributos: R$ {0:n2} (17,85 %) Fonte: IBPT",ValorCarga);
                else if (rowRPS.RPS_PLA == "230008")
                    txtServ = string.Format("Tx. Contratual (13) Valor aproximado dos Tributos: R$ {0:n2} (17,85 %) Fonte: IBPT", ValorCarga);
                else if (rowRPS.RPS_PLA == "220034")
                    txtServ = string.Format("Tx. elabora��o de DIRF/RAIS Valor aproximado dos Tributos: R$ {0:n2} (17,85 %) Fonte: IBPT", ValorCarga);
                decimal? retPIS = null;
                decimal? retCof = null;
                decimal? retCsll = null;
                decimal? retIr = null;
                if (rowRPS.RPSReterContri)
                {
                    retPIS = Math.Round(TotalGeralDaNota * 0.0065M, 2, MidpointRounding.AwayFromZero);
                    retCof = Math.Round(TotalGeralDaNota * 0.03M, 2, MidpointRounding.AwayFromZero);
                    retCsll = Math.Round(TotalGeralDaNota * 0.01M, 2, MidpointRounding.AwayFromZero); 
                    
                }
                if (!rowRPS.IsRPSRetIRNull())
                    retIr = rowRPS.RPSRetIR;
                Lote.IncluirRPS(TotalGeralDaNota, rowRPS.RPSISS, ret, Tomador, rowRPS.RPS, txtServ, EmissaoRPS,retPIS,retCof,retCsll,retIr);
                
                Contador++;
            }
            if (Lote.GeraXML(Caminho, checkEditGravarArquivos.Checked ? ArquivoNaoAssinado.Gravar : ArquivoNaoAssinado.RAM))
            {
                rowLoteAberto.LRPStatus = (int)StatusLote.geradoWS;
                rowLoteAberto.LRPProtocoloL = Lote.protocolo;
                rowLoteAberto.LRPSEnvio = Lote.DataEnvio;
                dFaturamento.LoteRPsTableAdapter.Update(rowLoteAberto);
                rowLoteAberto.AcceptChanges();
                foreach (dFaturamento.RPSRow rowRPS in rowLoteAberto.GetRPSRows())
                {
                    rowRPS.RPSStatus = (int)StatusRPS.Enviada;
                    dFaturamento.RPSTableAdapter.Update(rowRPS);
                    rowRPS.AcceptChanges();
                }
                rowLoteAberto = novoLoteAberto;
                return true;
            }
            else
            {
                if (Lote.strErro != null)
                    MessageBox.Show(string.Format("Erro\r\n{0}", Lote.strErro));
                if (Lote.Avisos != "")
                    MessageBox.Show(Lote.Avisos, "Aviso");
                return false;
            }
            
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            cConfirmar cConf = new cConfirmar();
            int nTaxa = 0;
            int nAssembleia = 0;
            int nTaxa13 = 0;
            int nDirf = 0;

            foreach (dFaturamento.RPSRow rowRPS in rowLoteAberto.GetRPSRows())
                
                    if ((StatusRPS)rowRPS.RPSStatus == StatusRPS.NaoEnviada)
                    {
                        if (rowRPS.RPS_PLA == "230001")
                            nTaxa++;
                        if (rowRPS.RPS_PLA == "230002")
                            nAssembleia++;
                        if (rowRPS.RPS_PLA == "230008")
                            nTaxa13++;                       
                        if (rowRPS.RPS_PLA == "220034")
                            nDirf++;                             
                    };
                
                

            cConf.Prepara(nTaxa, nAssembleia, nTaxa13, nDirf);
            if (cConf.ShowEmPopUp() == DialogResult.OK)
            {
                Apoio.FazerValidacao = checkEditGravarArquivos.Checked;
                Apoio.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.Proxy;
                using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                {
                    Application.DoEvents();
                    while (EnviarLoteAberto(-1, cConf.chTaxa, cConf.chAssembleia,cConf.chTaxa13,cConf.chDirf))
                    {
                        Application.DoEvents();
                        if (Esp.Abortar)
                            break;
                    }
                }
            }        
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {

            string Relatorio = "";
            Apoio.FazerValidacao = checkEditGravarArquivos.Checked;
            Apoio.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.Proxy;
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.AtivaGauge(dFaturamento.RPS.Count);
                Application.DoEvents();
                int Conta = 0;
                DateTime ProxTela = DateTime.Now.AddSeconds(3);
                foreach (dFaturamento.RPSRow rowRPS in dFaturamento.RPS)
                {
                    Conta++;
                    if ((StatusRPS)rowRPS.RPSStatus == StatusRPS.Enviada)
                    {
                        string retorno = VerificaRPS(rowRPS.RPS);
                        if (retorno != "")
                            Relatorio += string.Format("Condom�nio:{0}\r\nErro reportado:\r\n----\r\n{1}\r\n----\r\n\r\n", rowRPS.CONCodigo, retorno);
                    }
                    if (ProxTela < DateTime.Now)
                    {
                        ProxTela = DateTime.Now.AddSeconds(3);
                        Esp.Gauge(Conta);
                        Application.DoEvents();
                    }
                    if (Esp.Abortar)
                        break;
                }
                if (Relatorio != "")
                {
                    string Arquivo = Caminho + "Erros.TXT";
                    if (System.IO.File.Exists(Arquivo))
                        System.IO.File.Delete(Arquivo);
                    System.IO.File.AppendAllText(Arquivo, Relatorio);
                    if (MessageBox.Show(string.Format("Erros reportados em {0}\r\nAbrir ?", Arquivo), "ERROS", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        process.StartInfo.FileName = Arquivo;
                        process.Start();
                    }
                }
            }
            Carregar();
        }

        private NotaGerada _NFSe;

        private CPFCNPJ _cnpjEmissor;

        private CPFCNPJ cnpjEmissor
        { get { return _cnpjEmissor ?? (_cnpjEmissor = new CPFCNPJ(Framework.DSCentral.EMP == 1 ? "55.055.651/0001-70" : "06.888.260/0001-21")); } }

        private NotaGerada NFSe
        { get { return _NFSe ?? (_NFSe = new NotaGerada(cnpjEmissor.Valor, IM)); } }

        private string VerificaRPS(int RPS)
        {
            //System.Windows.Forms.MessageBox.Show("TESTE - Vai for�ar produ��o");
            //CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao = true;
            string retorno = "";            
            if (NFSe.ConsultaRPS(VerssaoXSD.v300, Caminho, RPS, checkEditGravarArquivos.Checked ? ArquivoNaoAssinado.Gravar : ArquivoNaoAssinado.RAM))
            {                
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllFaturamento cFaturamento - 616",dFaturamento.RPSTableAdapter);
                try
                {
                    dFaturamento.RPSTableAdapter.AjustaNota1(NFSe.DataEmissao, (int)NFSe.Numero, RPS);
                    Competencia comp = new Competencia(NFSe.Competencia.Month, NFSe.Competencia.Year);
                    dFaturamento.RPSTableAdapter.AjustaNota2((int)StatusRPS.Convertida, NFSe.CodigoVerificacao, comp.CompetenciaBind, RPS);
                    VirMSSQL.TableAdapter.CommitSQL();
                }
                catch (Exception e)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(e);
                    return e.Message;
                }
                retorno = "";
            };
            if (NFSe.Aviso != "")
                retorno = string.Format("RPS:{1}\r\nAviso: {0}", NFSe.Aviso,RPS);
            return retorno;
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if(e.RowHandle < 0)
                return;
            if (e.Column.FieldName == colLRPStatus.FieldName)
            {
                StatusLote Status = (StatusLote)e.CellValue;
                e.Appearance.BackColor = virEnumStatusLote.virEnumStatusLoteSt.GetCor(Status);
                e.Appearance.ForeColor = Color.Black;
            }
        }

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            if (e.Column.FieldName == colRPSStatus.FieldName)
            {
                StatusRPS Status = (StatusRPS)e.CellValue;
                e.Appearance.BackColor = virEnumStatusRPS.virEnumStatusRPSSt.GetCor(Status);
                e.Appearance.ForeColor = Color.Black;
            }
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            dFaturamento.RPS.Clear();
            dFaturamento.LoteRPs.Clear();
            ButtonEmitir.Enabled = ButtonEmitir1.Enabled = ButtonVerificar.Enabled = ButtonAss.Enabled = ButReEnv.Enabled = false;            
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (gridControl1.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
                dFaturamento.RPSRow RPSRow = (dFaturamento.RPSRow)GV.GetFocusedDataRow();
                if (RPSRow != null)
                {
                    switch (e.Button.Kind)
                    {
                        case DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph:
                            foreach (dFaturamento.NOAxRPSRow rowNOAx in RPSRow.GetNOAxRPSRows())
                                CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos("ContaPagar.cNotasCampos", string.Format("NOTA: {0}",rowNOAx.NOA_PLA), rowNOAx.NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                            break;
                        case DevExpress.XtraEditors.Controls.ButtonPredefines.Delete:
                            if (((StatusRPS)RPSRow.RPSStatus == StatusRPS.NaoEnviada) || ((StatusRPS)RPSRow.RPSStatus == StatusRPS.ComErro))
                            {
                                RPSRow.RPSStatus = (int)StatusRPS.Cancelada;
                                dFaturamento.RPSTableAdapter.Update(RPSRow);
                                RPSRow.AcceptChanges();
                            }
                            break;
                        case DevExpress.XtraEditors.Controls.ButtonPredefines.Down:
                            if (((StatusLote)RPSRow.LoteRPsRow.LRPStatus != StatusLote.aberto) && ((StatusRPS)RPSRow.RPSStatus != StatusRPS.Convertida))
                            {
                                RPSRow.RPS_LRP = rowLoteAberto.LRP;
                                dFaturamento.RPSTableAdapter.Update(RPSRow);
                                RPSRow.AcceptChanges();
                            }
                            break;
                    }
                }
            }
        }

        private void simpleButton3_Click_1(object sender, EventArgs e)
        {
            //DateTime DI = new DateTime(cCompet1.Comp.Ano, cCompet1.Comp.Mes, 1);
            //DateTime DF = DI.AddMonths(1).AddDays(-1);

            string Erros = "";
            dFaturamento.NovosTableAdapter.ClearBeforeFill = false;
            int carregados = dFaturamento.NovosTableAdapter.Fill(dFaturamento.Novos, "230002", cCompet1.Comp.CompetenciaBind);
            dFaturamento.NovosTableAdapter.ClearBeforeFill = false;
            carregados    += dFaturamento.NovosTableAdapter.Fill(dFaturamento.Novos, "230008", cCompet1.Comp.CompetenciaBind);
            dFaturamento.NovosTableAdapter.ClearBeforeFill = false;
            carregados += dFaturamento.NovosTableAdapter.Fill(dFaturamento.Novos, "220034", cCompet1.Comp.CompetenciaBind);
            dFaturamento.NovosTableAdapter.ClearBeforeFill = true;
            

            if ( carregados > 0)
            //if (dFaturamento.NovosTableAdapter.Fill(dFaturamento.Novos, "230002", DI, DF, Framework.DSCentral.EMP) > 0)
            {
                using (cAssembleias cAss = new cAssembleias())
                {
                    cAss.novosBindingSource.DataSource = dFaturamento;
                    if (cAss.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) != DialogResult.OK)
                        return;
                };
                foreach (dFaturamento.NovosRow novarow in dFaturamento.Novos)
                    if (novarow.Marcada)
                        CadastraNF(novarow, ref Erros);
                Carregar();
                if (Erros != "")
                {
                    Clipboard.SetText(Erros);
                    MessageBox.Show(string.Format("ERROS na carga (dados na �rea de transfer�ncia):\r\n{0}", Erros));
                }
            };

        }

        private void simpleButton3_Click_2(object sender, EventArgs e)
        {
            dFaturamento.LoteRPsRow LRProw = (dFaturamento.LoteRPsRow)gridView1.GetFocusedDataRow();
            if((LRProw != null) && ((StatusLote)LRProw.LRPStatus == StatusLote.geradoWS))
            {
                foreach(dFaturamento.RPSRow RPSrow in LRProw.GetRPSRows())
                    if (((StatusRPS)RPSrow.RPSStatus == StatusRPS.ComErro) || ((StatusRPS)RPSrow.RPSStatus == StatusRPS.Enviada) || ((StatusRPS)RPSrow.RPSStatus == StatusRPS.NaoEnviada))
                    {
                        RPSrow.RPS_LRP = rowLoteAberto.LRP;
                        RPSrow.RPSHistorico += string.Format("\r\nTransferida para lote {0} para reenvio em {1} por {2}",rowLoteAberto.LRP,DateTime.Now,Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome);
                        RPSrow.RPSStatus = (int)StatusRPS.NaoEnviada;
                        dFaturamento.RPSTableAdapter.Update(RPSrow);
                        RPSrow.AcceptChanges();
                    }
            }
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            if (gridControl1.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
                dFaturamento.RPSRow RPSRow = (dFaturamento.RPSRow)GV.GetFocusedDataRow();
                if ((RPSRow != null) && ((StatusRPS)RPSRow.RPSStatus == StatusRPS.NaoEnviada))
                {
                    //if (cCompet1.Comp != CompAtual)
                    //{
                    //    if (MessageBox.Show("Esta nota n�o � da compet�ncia atual. Confirma o envio mesmo assim?", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    //        return;
                    //}
                    Apoio.FazerValidacao = checkEditGravarArquivos.Checked;
                    Apoio.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.Proxy;
                    using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                    {
                        Application.DoEvents();
                        EnviarLoteAberto(RPSRow.RPS,true,true,true,true);                        
                    }
                }
            }
        }

        private void checkEditGravarArquivos_CheckedChanged(object sender, EventArgs e)
        {
            TxCaminho.Visible = checkEditGravarArquivos.Checked;
            TxCaminho.Text = Caminho;
        }

        /*
        private void simpleButton3_Click_3(object sender, EventArgs e)
        {
            int Alteradas = RevisarValor();
            if ( Alteradas != 0 )
            {
                MessageBox.Show(string.Format("Aten��o {0} notas alteradas",Alteradas));
                Carregar();
            }
        }*/

        /// <summary>
        /// N�o deveria ser necess�rio
        /// </summary>
        private void AjustarRetencoes()
        {
            string comandoLimpar = "UPDATE dbo.RPS SET RPSReterContri = 0, RPSRetIR = null WHERE (RPSStatus = 0)";
            string comandoColocaCof =
"UPDATE       dbo.RPS\r\n" +
"SET                RPSReterContri = 1\r\n" +
"FROM            dbo.RPS INNER JOIN\r\n" +
"                         dbo.NOtAs ON dbo.RPS.RPS_NOA = dbo.NOtAs.NOA INNER JOIN\r\n" +
"                         dbo.PAGamentos ON dbo.NOtAs.NOA = dbo.PAGamentos.PAG_NOA INNER JOIN\r\n" +
"                         dbo.CHEques ON dbo.PAGamentos.PAG_CHE = dbo.CHEques.CHE\r\n" +
"WHERE        (dbo.RPS.RPSStatus = 0) AND (dbo.PAGamentos.PAGIMP = 5) AND (dbo.CHEques.CHEStatus <> 0) AND (dbo.RPS.RPSReterContri = 0);";
            string comandoColocaIR =
"UPDATE       dbo.RPS\r\n" +
"SET                RPSRetIR = PAGValor\r\n" +
"FROM            dbo.RPS INNER JOIN\r\n" +
"                         dbo.NOtAs ON dbo.RPS.RPS_NOA = dbo.NOtAs.NOA INNER JOIN\r\n" +
"                         dbo.PAGamentos ON dbo.NOtAs.NOA = dbo.PAGamentos.PAG_NOA INNER JOIN\r\n" +
"                         dbo.CHEques ON dbo.PAGamentos.PAG_CHE = dbo.CHEques.CHE\r\n" +
"WHERE        (dbo.RPS.RPSStatus = 0) AND (dbo.PAGamentos.PAGIMP = 1) AND (dbo.CHEques.CHEStatus <> 0) AND (dbo.RPS.RPSRetIR is null);";
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoLimpar);
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoColocaCof);
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoColocaIR);                
        }

        /*
        private int RevisarValor()
        {                        
            int comAlteracao = 0;
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.Espere("Verificando Valores");
                Esp.AtivaGauge(dFaturamento.RPS.Count);
                foreach (dFaturamento.RPSRow rowRPS in dFaturamento.RPS)
                {
                    if ((StatusRPS)rowRPS.RPSStatus == StatusRPS.NaoEnviada)
                    {                                                
                        ContaPagar.Nota Nota = new ContaPagar.Nota(rowRPS.RPS_NOA);
                        if (Nota.Encontrada())
                        {
                            if (Nota.AjustarComPGF())
                                comAlteracao++;
                        }
                    }
                    Esp.Gauge();
                }
            }
            return comAlteracao;
        }*/

        private void simpleButton3_Click_3(object sender, EventArgs e)
        {
            string Lista = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\r",
                                       "Lote",
                                       "RPS",
                                       "CODCON",
                                       "Nome",
                                       "Cnpj",
                                       "Status",
                                       "Total",
                                       "ISS");
            foreach (dFaturamento.RPSRow rowRPS in dFaturamento.RPS)
            {

                Lista += string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6:n2}\t{7:n2}\r",
                                       rowRPS.RPS_LRP,
                                       rowRPS.RPS,
                                       rowRPS.CONCodigo,
                                       rowRPS.CONNome,
                                       rowRPS.CONCnpj,
                                       virEnumStatusRPS.virEnumStatusRPSSt.Descritivo(rowRPS.RPSStatus),
                                       rowRPS.NOATotal,
                                       rowRPS.RPSISS);
                
            }
            Clipboard.SetText(Lista);
        }

        private void simpleButton4_Click_2(object sender, EventArgs e)
        {            
            // este log deve ser eliminado depois de tudo testado
            System.Text.StringBuilder SB = new System.Text.StringBuilder();

            using (CompontesBasicosProc.AcumulaPDF AcPDF = new CompontesBasicosProc.AcumulaPDF())
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Carregando condom�nios");
                    SortedList<int, AbstratosNeon.ABS_Condominio> Condominios = AbstratosNeon.ABS_Condominio.ABSGetCondominios(true);
                    ESP.Espere("Recuperando notas");
                    ESP.AtivaGauge(dFaturamento.RPS.Count);
                    int contador = 0;
                    foreach (dFaturamento.RPSRow rowRPS in dFaturamento.RPS)
                    {
                        ESP.GaugeTempo(contador++);
                        Console.Write(string.Format("contador = {0}\r\n", contador));
                        if(contador == 424)
                            Console.Write(string.Format("************ contador = {0}\r\n", contador));
                        if (rowRPS.RPSStatus == 0)
                        {
                            SB.AppendFormat("LRP = {2} RPS = {1} CON = {0} N�O EMITIDA\r\n", rowRPS.CON, rowRPS.RPS, rowRPS.RPS_LRP);
                            Console.Write(string.Format("LRP = {2} RPS = {1} CON = {0} N�O EMITIDA\r\n", rowRPS.CON, rowRPS.RPS, rowRPS.RPS_LRP));
                            continue;
                        }
                        bool Principal = true;
                        foreach (dFaturamento.NOAxRPSRow row in rowRPS.GetNOAxRPSRows())
                        {                            
                            if (row.IsNOAPDFNull() && (!rowRPS.IsRPSVerificacaoNull()))
                            {
                                if (RecuperaPDF(row.NOA, rowRPS.NOANumero, rowRPS.RPSVerificacao,Principal))
                                {
                                    if (Principal)
                                        AcPDF.add(PDFRecuperado);
                                    SB.AppendFormat("RECUPERADO RPS = {1} CON = {0} NOA=  {2}\r\n", rowRPS.CON, rowRPS.RPS, row.NOA);
                                    Console.Write(string.Format("RECUPERADO RPS = {1} CON = {0} NOA=  {2}\r\n", rowRPS.CON, rowRPS.RPS, row.NOA));
                                    Principal = false;
                                }
                                else
                                {
                                    SB.AppendFormat("*** N�O *** RECUPERADO RPS = {1} CON = {0} NOA=  {2}\r\n", rowRPS.CON, rowRPS.RPS, row.NOA);
                                    Console.Write(string.Format("*** N�O *** RECUPERADO RPS = {1} CON = {0} NOA=  {2}\r\n", rowRPS.CON, rowRPS.RPS, row.NOA));
                                    break;
                                }
                            }
                            else
                            {
                                if (!row.IsNOAPDFNull())
                                {
                                    SB.AppendFormat("PDF = {0}\r\n", row.NOAPDF);
                                    Console.Write("PDF = {0}\r\n", row.NOAPDF);
                                }
                            }
                        }
                    }
                    Console.Write(string.Format("FIM loop\r\n"));
                }
                if (AcPDF.Termina(true) == 0)
                    MessageBox.Show("Nenhuma nota recuperada");
            }
            Clipboard.SetText(SB.ToString());            
        }

        private BuscaNFE _BuscaNota;        

        private void PDFRetornado(object sender, EventArgs e)
        {
            switch (BuscaNota.status)
            {
                case BuscaNFE.statusBusca.Pronto:
                    if (NOAsendorecuperada.HasValue)
                    {
                        ContaPagar.Nota NotaPDF = new ContaPagar.Nota(NOAsendorecuperada.Value);
                        if (NotaPDF.Encontrada())
                        {
                            NotaPDF.PublicaNotas(BuscaNota.DadosNFeLido.ArquivoTMP, "");
                            PDFRecuperado = BuscaNota.DadosNFeLido.ArquivoTMP;
                        }
                    }
                    BuscaNota.status = BuscaNFE.statusBusca.Livre;
                    break;
                case BuscaNFE.statusBusca.Livre:
                case BuscaNFE.statusBusca.Aguardando:
                    break;
                case BuscaNFE.statusBusca.Erro:
                    NOAsendorecuperada = null;
                    BuscaNota.status = BuscaNFE.statusBusca.Livre;
                    break;
            };            
        }

        private BuscaNFE BuscaNota
        {
            get
            {
                if (_BuscaNota == null)
                {
                    _BuscaNota = new BuscaNFE();
                    _BuscaNota.CadastraNota += PDFRetornado;
                }
                return _BuscaNota;
            }
        }

        private int? NOAsendorecuperada;

        private string PDFRecuperado = "";

        private bool RecuperaPDF(int NOA,int Nota,string Verificador,bool Principal)
        {
            if (Principal)
            {
                NOAsendorecuperada = NOA;
                PDFRecuperado = "";
                return BuscaNota.Busca(Framework.DSCentral.EMP == 1 ? Prefeituras.SBC : Prefeituras.SA, Nota, Verificador, null, true) && (PDFRecuperado != null);
            }
            else
            {
                if (PDFRecuperado != "")
                {
                    ContaPagar.Nota NotaPDF = new ContaPagar.Nota(NOA);
                    if (NotaPDF.Encontrada())
                        NotaPDF.PublicaNotas(PDFRecuperado, "");
                    return true;
                }
                else
                    return false;
            }
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder SB = new System.Text.StringBuilder();
            SB.AppendFormat("RPS\tCON\tValor\r\n\r\n");
            string x;
            foreach (dFaturamento.RPSRow RPS in dFaturamento.RPS)
            {
                if ((StatusRPS)RPS.RPSStatus != StatusRPS.NaoEnviada)
                    continue;
                foreach (dFaturamento.NOAxRPSRow rowdet in RPS.GetNOAxRPSRows())
                {
                    x = rowdet.NOA_PLA;
                }
                SB.AppendFormat("{0}\t{1}\t{2:n2}\r\n", RPS.RPS, RPS.CONCodigo, 10);
            }
            Clipboard.SetText(SB.ToString());
            MessageBox.Show("Relat�rio na �rea de transfer�ncia para colar em planilha");

        }

        private void ButtonImpresso_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView Grid = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;           
            dFaturamento.RPSRow rpsRow = (dFaturamento.RPSRow)Grid.GetFocusedDataRow();
            ContaPagar.Nota NotaPDF = new ContaPagar.Nota(rpsRow.RPS_NOA);
            if (NotaPDF.Encontrada() && !NotaPDF.LinhaMae.IsNOAPDFNull())
                NotaPDF.MostraPDF();            
        }

        private void repItemButtonEditNF_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (gridControl1.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
                dFaturamento.NOAxRPSRow rowNOAx = (dFaturamento.NOAxRPSRow)GV.GetFocusedDataRow();
                if (rowNOAx != null)
                {
                    switch (e.Button.Kind)
                    {
                        case DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph:
                            CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos("ContaPagar.cNotasCampos", string.Format("NOTA: {0}", rowNOAx.NOA_PLA), rowNOAx.NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                            break;                        
                    }
                }
            }
        }

        

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            using (CompontesBasicosProc.AcumulaPDF AcPDF = new CompontesBasicosProc.AcumulaPDF())
            {
                foreach (int cur in gridView4.GetSelectedRows())
                {
                    dFaturamento.RPSRow rowRPS = (dFaturamento.RPSRow)gridView4.GetDataRow(cur);
                    ContaPagar.Nota NotaPDF = new ContaPagar.Nota(rowRPS.RPS_NOA);
                    if (NotaPDF.Encontrada() && !NotaPDF.LinhaMae.IsNOAPDFNull())
                    {
                        string NomeArquivo = NotaPDF.ImpressoNota();
                        if (NomeArquivo != "")
                            AcPDF.add(NomeArquivo);
                    }
                }
                AcPDF.Termina(true);
            }
        }

        private void gridView4_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            if (e.Column.FieldName == colRPSStatus1.FieldName)
            {
                StatusRPS Status = (StatusRPS)e.CellValue;
                e.Appearance.BackColor = virEnumStatusRPS.virEnumStatusRPSSt.GetCor(Status);
                e.Appearance.ForeColor = Color.Black;
            }
        }

        private void gridView4_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column == gridColVerNota)
            {
                dFaturamento.RPSRow rowRPS = (dFaturamento.RPSRow)gridView4.GetDataRow(e.RowHandle);
                if ((rowRPS != null) && (!rowRPS.IsNOAPDFNull()))
                {
                    e.RepositoryItem = repositoryVerNota;
                }
            }
        }

        private void repositoryVerNota_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dFaturamento.RPSRow rpsRow = (dFaturamento.RPSRow)gridView4.GetFocusedDataRow();
            ContaPagar.Nota NotaPDF = new ContaPagar.Nota(rpsRow.RPS_NOA);
            if (NotaPDF.Encontrada() && !NotaPDF.LinhaMae.IsNOAPDFNull())
                NotaPDF.MostraPDF();
        }

        private void repItemButtonEditNF1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {                        
                dFaturamento.RPSRow RPSRow = (dFaturamento.RPSRow)gridView4.GetFocusedDataRow();
                if (RPSRow != null)
                {
                    switch (e.Button.Kind)
                    {
                        case DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph:
                            CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos("ContaPagar.cNotasCampos", string.Format("NOTA: {0}", RPSRow.NOANumero), RPSRow.RPS_NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                            break;
                    }
                }
            
        }
    }
}
