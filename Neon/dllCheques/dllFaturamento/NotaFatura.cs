﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirPDFModelo;
using System.IO;
using System.Windows.Forms;
using Framework.objetosNeon;
using DocBacarios;
using VirEnumeracoes;
using dllEmissorNFS_e;


namespace dllFaturamento
{
    /// <summary>
    /// Componente representando de uma nota fiscal emitida
    /// </summary>
    public class NotaFatura
    {
        private Gerador gerador1;

        private Gerador Gerador1 { get => gerador1==null ? (gerador1 = new Gerador()) : gerador1; }

        private string arquivoModelo = null;

        private string arquivoGabarito = null;

        private string Caminho
        {
            get
            {
                if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    return Path.GetDirectoryName(Application.ExecutablePath)+"\\MODELOS";
                else
                    return @"C:\FontesVS\documentação\NFS-e\Modelos";
            }
        }

        private string ArquivoTMP
        {
            get
            {
                string CaminhoBase;
                if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    CaminhoBase = Application.ExecutablePath + "\\TMP\\";
                else
                    CaminhoBase = @"C:\Lixo\TMP\";
                return string.Format("{0}{1:yyyyMMddHHmmss}.pdf",CaminhoBase,DateTime.Now);
            }
        }

        private string ArquivoModelo
        {
            get
            {
                if (arquivoModelo == null)
                    arquivoModelo = string.Format("{0}\\ModeloNFL{1:00}.pdf", Caminho, Framework.DSCentral.EMP);
                return arquivoModelo;
            }
        }

        private string ArquivoGabarito
        {
            get
            {
                if (arquivoGabarito == null)
                    arquivoGabarito = string.Format("{0}\\ModeloNF{1:00}.xml", Caminho, Framework.DSCentral.EMP);
                return arquivoGabarito;
            }
        }

        private dFaturamento _dFaturamento;

        private dFaturamento.RPSRow LinhaMae;

        /// <summary>
        /// Dataset interno
        /// </summary>
        private dFaturamento DFaturamento
        {
            get => _dFaturamento ?? (_dFaturamento = new dFaturamento());
            set => _dFaturamento = value;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="RPS"></param>
        public NotaFatura(int RPS)
        {
            LinhaMae = DFaturamento.RPS.FindByRPS(RPS);
            if (LinhaMae == null)
            {
                DFaturamento.RPSTableAdapter.FillByRPS(DFaturamento.RPS, RPS);
                LinhaMae = DFaturamento.RPS[0];
                DFaturamento.NOAxRPSTableAdapter.FillByRPS(DFaturamento.NOAxRPS,RPS);
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="linhaMae"></param>
        public NotaFatura(dFaturamento.RPSRow linhaMae)
        {
            LinhaMae = linhaMae;
            DFaturamento = (dFaturamento)LinhaMae.Table.DataSet;
        }

        private dGabarito dGabarito;

        private dGabarito DGabarito
        {
            get
            {
                if (dGabarito == null)
                {
                    string NomeArquivoMapeamento = ArquivoGabarito;
                    dGabarito = new dGabarito();
                    dGabarito.ReadXml(NomeArquivoMapeamento);                     
                }                                    
                return dGabarito;
            }
        }

        /// <summary>
        /// Competência da nota
        /// </summary>
        public Competencia Competencia { get => new Competencia(LinhaMae.RPSComp); }

        private decimal aliISS { get => dllImpostos.Imposto.AliquotaBase(Framework.DSCentral.EMP == 1 ? TipoImposto.ISSQN : TipoImposto.ISS_SA); }

        /// <summary>
        /// Imprime a nota
        /// </summary>
        public void ImprimeNF()
        {
            if (LinhaMae.RPSStatus != (int)StatusRPS.Convertida)
            {
                MessageBox.Show("Nota não emitida");
                return;
            }
            try
            {
                string PathTMP = Application.StartupPath + @"\TMP\";
                string strArquivo = String.Format("{0}Notas.pdf", PathTMP);
                if (!Directory.Exists(PathTMP))
                    Directory.CreateDirectory(PathTMP);
                if (File.Exists(strArquivo))
                    try
                    {
                        File.Delete(strArquivo);
                    }
                    catch
                    {
                        strArquivo = String.Format("{0}Comprovante_{1:ddMMyyhhmmss}.pdf", PathTMP, DateTime.Now);
                    };

                //SetValor("NUMERONF", LinhaMae.NOANumero);
                SetValor("NUMERONF", LinhaMae.NOANumero.ToString());
                SetValor("DATAEMISSAO", LinhaMae.NOADataEmissao.ToString("dd/MM/yyyy HH:mm:ss"));                
                SetValor("COMPETENCIA", Competencia.ToString());
                SetValor("COMPETENCIADIA", Competencia.CloneCompet(1).DataNaCompetencia(1,Competencia.Regime.mes).AddDays(-1).ToString("dd/MM/yyyy"));
                SetValor("NUMERORPS", LinhaMae.RPS.ToString());
                SetValor("CODIGOVERIFICACAO", LinhaMae.RPSVerificacao);
                //SetValor("LOCALPrest", );
                SetValor("TONOME", LinhaMae.CONNome);
                SetValor("TOCNPJ", new CPFCNPJ(LinhaMae.CONCnpj).ToString());
                SetValor("TOMUNICIPIO", LinhaMae.CIDNome);
                SetValor("TOUF", LinhaMae.CIDUf);
                SetValor("TOMUNICIPIOUF", string.Format("{0} - {1}", LinhaMae.CIDNome, LinhaMae.CIDUf));                                
                SetValor("TOENDERECO", string.Format("{0} ,{1} - {2} CEP: {3}",LinhaMae.CONEndereco2, LinhaMae.CONNumero, LinhaMae.CONBairro, LinhaMae.CONCep));                
                string retPIS = "";
                string retCof = "";
                string retCsll = "";
                string retIr = "";
                decimal TotalGeralDaNota = 0;
                string txtServ = "";
                bool Pares = LinhaMae.GetNOAxRPSRows().Length > 5;
                int Contador = 0;
                foreach (dFaturamento.NOAxRPSRow rowNOAx in LinhaMae.GetNOAxRPSRows())
                {
                    Contador++;
                    string quebra = "\r\n"; 
                    if (Pares && (Contador % 2 == 1))
                        quebra = " - ";
                    TotalGeralDaNota += rowNOAx.NOATotal;
                    if (LinhaMae.RPS_PLA == "230001")
                        txtServ += string.Format("{0} R$ {1:n2}{2}", rowNOAx.NOAServico, rowNOAx.NOATotal,quebra);
                }
                decimal ValorCarga = TotalGeralDaNota * 0.1785m;
                if (LinhaMae.RPS_PLA == "230001")
                    txtServ += string.Format("Valor aproximado dos Tributos : R$ {0:n2} (17,85 %) : Fonte: IBPT", ValorCarga);
                else if (LinhaMae.RPS_PLA == "230002")
                    txtServ = string.Format("Tx. Assembleia\r\nValor aproximado dos Tributos – R$ {0:n2} (17,85 %) – Fonte: IBPT", ValorCarga);
                else if (LinhaMae.RPS_PLA == "230008")
                    txtServ = string.Format("Tx. Contratual (13)\r\nValor aproximado dos Tributos – R$ {0:n2} (17,85 %) – Fonte: IBPT", ValorCarga);
                else if (LinhaMae.RPS_PLA == "220034")
                    txtServ = string.Format("Tx. elaboração de DIRF/RAIS Valor aproximado dos Tributos: R$ {0:n2} (17,85 %) Fonte: IBPT", ValorCarga);
                decimal TOTRETFED = 0;
                decimal Vret;
                if (LinhaMae.RPSReterContri)
                {
                    Vret = Math.Round(TotalGeralDaNota * 0.0065M, 2, MidpointRounding.AwayFromZero);
                    TOTRETFED += Vret;
                    retPIS = Vret.ToString("n2");
                    Vret = Math.Round(TotalGeralDaNota * 0.03M, 2, MidpointRounding.AwayFromZero);
                    TOTRETFED += Vret;
                    retCof = Vret.ToString("n2");
                    Vret = Math.Round(TotalGeralDaNota * 0.01M, 2, MidpointRounding.AwayFromZero);
                    TOTRETFED += Vret;
                    retCsll = Vret.ToString("n2");
                }
                if (!LinhaMae.IsRPSRetIRNull())
                {
                    TOTRETFED += LinhaMae.RPSRetIR;
                    retIr = LinhaMae.RPSRetIR.ToString("n2");
                }
                SetValor("RETPIS", retPIS);
                SetValor("RETCSLL", retCsll);
                SetValor("RETCOFINS", retCof);
                SetValor("RETIR", retIr);
                SetValor("TOTSERVICOS", TotalGeralDaNota.ToString("n2"));
                SetValor("TOTSERVICOS1", TotalGeralDaNota.ToString("n2"));
                SetValor("BASE", TotalGeralDaNota.ToString("n2"));
                SetValor("TOTALNOTA", TotalGeralDaNota.ToString("n2"));
                SetValor("TOTRETFED", TOTRETFED.ToString("n2"));
                decimal ISSRetido = 0;
                if(LinhaMae.RPSReterISS)
                    ISSRetido = Math.Round(TotalGeralDaNota * aliISS, 2, MidpointRounding.AwayFromZero);
                SetValor("ALIISS", (aliISS * 100).ToString("n2"));
                SetValor("VALISS", Math.Round(TotalGeralDaNota * aliISS, 2, MidpointRounding.AwayFromZero).ToString("n2"));
                SetValor("RETERSN",LinhaMae.RPSReterISS ? "(X) Sim () Não" : "( ) Sim (X) Não");
                if (ISSRetido != 0)                
                    SetValor("ISSRETIDO", ISSRetido.ToString("n2"));                                                    
                decimal TotalLiquido = TotalGeralDaNota - ISSRetido - TOTRETFED;
                SetValor("TOTALLIQUIDO", TotalLiquido.ToString("n2"));
                SetValor("DESCRI", txtServ);
                Gerador1.GeraPDF(ArquivoModelo, strArquivo, DGabarito);
                System.Diagnostics.Process.Start(strArquivo);                
            }
            catch (Exception e)
            {                
                MessageBox.Show(string.Format("Erro na configuração do impresso:{0}",e.Message), "ERRO", MessageBoxButtons.OK);
            }
        }

        private dGabarito.GabaritoRow rowMap;

        private void SetValor(string Nome,string Conteudo)
        {
            rowMap = DGabarito.Gabarito.FindByNomeCampo(Nome);
            if (rowMap != null)
                rowMap.Conteudo = Conteudo;
        }
    }
}
