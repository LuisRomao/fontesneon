﻿namespace dllFaturamento
{


    partial class dFaturamento
    {
        private dFaturamentoTableAdapters.NOAxRPSTableAdapter nOAxRPSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOAxRPS
        /// </summary>
        public dFaturamentoTableAdapters.NOAxRPSTableAdapter NOAxRPSTableAdapter
        {
            get
            {
                if (nOAxRPSTableAdapter == null)
                {
                    nOAxRPSTableAdapter = new dFaturamentoTableAdapters.NOAxRPSTableAdapter();
                    nOAxRPSTableAdapter.TrocarStringDeConexao();
                };
                return nOAxRPSTableAdapter;
            }
        }

        private dFaturamentoTableAdapters.BuscaISSTableAdapter buscaISSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BuscaISS
        /// </summary>
        public dFaturamentoTableAdapters.BuscaISSTableAdapter BuscaISSTableAdapter
        {
            get
            {
                if (buscaISSTableAdapter == null)
                {
                    buscaISSTableAdapter = new dFaturamentoTableAdapters.BuscaISSTableAdapter();
                    buscaISSTableAdapter.TrocarStringDeConexao();
                };
                return buscaISSTableAdapter;
            }
        }

        private dFaturamentoTableAdapters.LoteRPsTableAdapter loteRPsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: LoteRPs
        /// </summary>
        public dFaturamentoTableAdapters.LoteRPsTableAdapter LoteRPsTableAdapter
        {
            get
            {
                if (loteRPsTableAdapter == null)
                {
                    loteRPsTableAdapter = new dFaturamentoTableAdapters.LoteRPsTableAdapter();
                    loteRPsTableAdapter.TrocarStringDeConexao();
                };
                return loteRPsTableAdapter;
            }
        }

        private dFaturamentoTableAdapters.NovosTableAdapter novosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Novos
        /// </summary>
        public dFaturamentoTableAdapters.NovosTableAdapter NovosTableAdapter
        {
            get
            {
                if (novosTableAdapter == null)
                {
                    novosTableAdapter = new dFaturamentoTableAdapters.NovosTableAdapter();
                    novosTableAdapter.TrocarStringDeConexao();
                };
                return novosTableAdapter;
            }
        }

        private dFaturamentoTableAdapters.RPSTableAdapter rPSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RPS
        /// </summary>
        public dFaturamentoTableAdapters.RPSTableAdapter RPSTableAdapter
        {
            get
            {
                if (rPSTableAdapter == null)
                {
                    rPSTableAdapter = new dFaturamentoTableAdapters.RPSTableAdapter();
                    rPSTableAdapter.TrocarStringDeConexao();
                };
                return rPSTableAdapter;
            }
        }

        /// <summary>
        /// Valida Todas
        /// </summary>
        /// <returns></returns>
        public bool ValidaTodas()
        {
            bool retorno = true;
            foreach (RPSRow rowValidar in RPS)
            {
                if (((dllEmissorNFS_e.StatusRPS)rowValidar.RPSStatus == dllEmissorNFS_e.StatusRPS.ComErro)
                    ||
                    ((dllEmissorNFS_e.StatusRPS)rowValidar.RPSStatus == dllEmissorNFS_e.StatusRPS.NaoEnviada))
                {
                    if (!Valida(rowValidar))
                        retorno = false;
                }
            }
            return retorno;
        }

        /// <summary>
        /// Valida
        /// </summary>
        /// <param name="rowValidar"></param>
        /// <returns></returns>
        public bool Valida(RPSRow rowValidar)
        {
            if (rowValidar.RPSStatus == (int)dllEmissorNFS_e.StatusRPS.Convertida)
                return false;
            string Erro = "";
            if (rowValidar.IsCIDIBGENull())
            {
                Erro += "Código IBGE|";
                rowValidar.RowError = "IBGE";
            };
            if (rowValidar.IsCIDUfNull())
                Erro += "UF|";
            if (rowValidar.IsCONBairroNull())
                Erro += "Bairro|";
            if (rowValidar.IsCONCepNull())
                Erro += "CEP|";
            else
            {
                string CEPLimpo = rowValidar.CONCep.Trim().Replace("-", "");
                rowValidar.CONCep = CEPLimpo;
                if (CEPLimpo.Length != 8)
                    Erro += "CEP|";
            }
            if (rowValidar.IsCONCnpjNull())
                Erro += "CNPJ|";
            else
            {
                DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(rowValidar.CONCnpj);
                if (cnpj.Tipo != DocBacarios.TipoCpfCnpj.CNPJ)
                    Erro += "CNPJ";
            }
            if (rowValidar.IsCONEndereco2Null())
                Erro += "Endereço|";
            if (rowValidar.IsCONNomeNull())
                Erro += "Nome Condomínio";
            if (rowValidar.IsCONNumeroNull())
            {
                Erro += "Número|";
                rowValidar.RowError = "Número";
            }
            /*
            if (rowValidar.Is)
                Erro += "Número|";
            if (rowValidar.IsCONNumeroNull())
                Erro += "Número|";
            if (rowValidar.IsCONNumeroNull())
                Erro += "Número|";
            if (rowValidar.IsCONNumeroNull())
                Erro += "Número|";
            */
            if (Erro != "")
            {
                rowValidar.Erro = Erro;
                rowValidar.RPSStatus = (int)dllEmissorNFS_e.StatusRPS.ComErro;
                return false;
            }
            else
            {
                rowValidar.RPSStatus = (int)dllEmissorNFS_e.StatusRPS.NaoEnviada;
                return true;
            }
        }

    }
}
