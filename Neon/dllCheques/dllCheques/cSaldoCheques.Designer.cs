﻿namespace dllCheques
{
    partial class cSaldoCheques
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cSaldoCheques));
            this.dcFolhasDeCheque = new dllCheques.dcFolhasDeCheque();
            this.bindingSourcedcFolhasDeCheque = new System.Windows.Forms.BindingSource(this.components);
            this.contasTableAdapter = new dllCheques.dcFolhasDeChequeTableAdapters.ContasTableAdapter();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigoFolha1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCT_BCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAtiva = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTPrincipal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFolhasBR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFolhasASSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFolhasASS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFolhasSindico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAplicacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lookupGerente1 = new Framework.Lookup.LookupGerente();
            this.BotImpresso = new VirBotaoNeon.cBotaoImpNeon();
            this.lookupAssistente1 = new Framework.Lookup.LookupGerente();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dcFolhasDeCheque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcedcFolhasDeCheque)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lookupAssistente1);
            this.panelControl1.Controls.Add(this.BotImpresso);
            this.panelControl1.Controls.Add(this.lookupGerente1);
            this.panelControl1.Size = new System.Drawing.Size(1490, 70);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            this.BtnIncluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            this.BtnAlterar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            this.BtnExcluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridControl_F.DataSource = this.bindingSourcedcFolhasDeCheque;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Location = new System.Drawing.Point(0, 70);
            this.GridControl_F.Size = new System.Drawing.Size(1490, 677);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView_F.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView_F.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCON,
            this.colCONCodigo,
            this.colCONNome,
            this.colCONCodigoFolha1,
            this.colCONStatus,
            this.colCCT,
            this.colCCT_BCO,
            this.colCCTAgencia,
            this.colCCTConta,
            this.colCCTAtiva,
            this.colCCTPrincipal,
            this.colCCTTitulo,
            this.colFolhasBR,
            this.colFolhasASSP,
            this.colFolhasASS,
            this.colTotal,
            this.colFolhasSindico,
            this.colCCTAplicacao});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.CheckBoxSelectorColumnWidth = 30;
            this.GridView_F.OptionsSelection.MultiSelect = true;
            this.GridView_F.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.GridView_F.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.False;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTotal, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // dcFolhasDeCheque
            // 
            this.dcFolhasDeCheque.DataSetName = "dcFolhasDeCheque";
            this.dcFolhasDeCheque.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingSourcedcFolhasDeCheque
            // 
            this.bindingSourcedcFolhasDeCheque.DataMember = "Contas";
            this.bindingSourcedcFolhasDeCheque.DataSource = this.dcFolhasDeCheque;
            // 
            // contasTableAdapter
            // 
            this.contasTableAdapter.ClearBeforeFill = true;
            this.contasTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("contasTableAdapter.GetNovosDados")));
            this.contasTableAdapter.TabelaDataTable = null;
            // 
            // colCON
            // 
            this.colCON.Caption = "CON";
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 2;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Nome";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 3;
            this.colCONNome.Width = 195;
            // 
            // colCONCodigoFolha1
            // 
            this.colCONCodigoFolha1.Caption = "Cod";
            this.colCONCodigoFolha1.FieldName = "CONCodigoFolha1";
            this.colCONCodigoFolha1.Name = "colCONCodigoFolha1";
            this.colCONCodigoFolha1.Visible = true;
            this.colCONCodigoFolha1.VisibleIndex = 1;
            this.colCONCodigoFolha1.Width = 56;
            // 
            // colCONStatus
            // 
            this.colCONStatus.FieldName = "CONStatus";
            this.colCONStatus.Name = "colCONStatus";
            // 
            // colCCT
            // 
            this.colCCT.FieldName = "CCT";
            this.colCCT.Name = "colCCT";
            this.colCCT.OptionsColumn.ReadOnly = true;
            // 
            // colCCT_BCO
            // 
            this.colCCT_BCO.Caption = "Banco";
            this.colCCT_BCO.FieldName = "CCT_BCO";
            this.colCCT_BCO.Name = "colCCT_BCO";
            this.colCCT_BCO.Visible = true;
            this.colCCT_BCO.VisibleIndex = 5;
            this.colCCT_BCO.Width = 57;
            // 
            // colCCTAgencia
            // 
            this.colCCTAgencia.Caption = "Ag";
            this.colCCTAgencia.FieldName = "CCTAgencia";
            this.colCCTAgencia.Name = "colCCTAgencia";
            this.colCCTAgencia.Width = 34;
            // 
            // colCCTConta
            // 
            this.colCCTConta.Caption = "Conta";
            this.colCCTConta.FieldName = "CCTConta";
            this.colCCTConta.Name = "colCCTConta";
            this.colCCTConta.Width = 78;
            // 
            // colCCTAtiva
            // 
            this.colCCTAtiva.FieldName = "CCTAtiva";
            this.colCCTAtiva.Name = "colCCTAtiva";
            // 
            // colCCTPrincipal
            // 
            this.colCCTPrincipal.FieldName = "CCTPrincipal";
            this.colCCTPrincipal.Name = "colCCTPrincipal";
            // 
            // colCCTTitulo
            // 
            this.colCCTTitulo.Caption = "Título";
            this.colCCTTitulo.FieldName = "CCTTitulo";
            this.colCCTTitulo.Name = "colCCTTitulo";
            this.colCCTTitulo.Visible = true;
            this.colCCTTitulo.VisibleIndex = 4;
            this.colCCTTitulo.Width = 138;
            // 
            // colFolhasBR
            // 
            this.colFolhasBR.Caption = "Folhas";
            this.colFolhasBR.FieldName = "FolhasBR";
            this.colFolhasBR.Name = "colFolhasBR";
            this.colFolhasBR.Visible = true;
            this.colFolhasBR.VisibleIndex = 6;
            this.colFolhasBR.Width = 56;
            // 
            // colFolhasASSP
            // 
            this.colFolhasASSP.Caption = "Folhas ass.1";
            this.colFolhasASSP.FieldName = "FolhasASSP";
            this.colFolhasASSP.Name = "colFolhasASSP";
            this.colFolhasASSP.Visible = true;
            this.colFolhasASSP.VisibleIndex = 8;
            this.colFolhasASSP.Width = 70;
            // 
            // colFolhasASS
            // 
            this.colFolhasASS.Caption = "Folhas ass";
            this.colFolhasASS.FieldName = "FolhasASS";
            this.colFolhasASS.Name = "colFolhasASS";
            this.colFolhasASS.Visible = true;
            this.colFolhasASS.VisibleIndex = 7;
            this.colFolhasASS.Width = 63;
            // 
            // colTotal
            // 
            this.colTotal.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colTotal.AppearanceCell.ForeColor = System.Drawing.Color.Red;
            this.colTotal.AppearanceCell.Options.UseFont = true;
            this.colTotal.AppearanceCell.Options.UseForeColor = true;
            this.colTotal.Caption = "Folhas Neon";
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.ReadOnly = true;
            this.colTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:n0}")});
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 9;
            this.colTotal.Width = 70;
            // 
            // colFolhasSindico
            // 
            this.colFolhasSindico.Caption = "Folhas síndico";
            this.colFolhasSindico.FieldName = "FolhasSindico";
            this.colFolhasSindico.Name = "colFolhasSindico";
            this.colFolhasSindico.Visible = true;
            this.colFolhasSindico.VisibleIndex = 10;
            this.colFolhasSindico.Width = 77;
            // 
            // colCCTAplicacao
            // 
            this.colCCTAplicacao.Caption = "Aplicação";
            this.colCCTAplicacao.FieldName = "CCTAplicacao";
            this.colCCTAplicacao.Name = "colCCTAplicacao";
            // 
            // lookupGerente1
            // 
            this.lookupGerente1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente1.Appearance.Options.UseBackColor = true;
            this.lookupGerente1.CaixaAltaGeral = true;
            this.lookupGerente1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente1.Location = new System.Drawing.Point(12, 15);
            this.lookupGerente1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente1.Name = "lookupGerente1";
            this.lookupGerente1.Size = new System.Drawing.Size(290, 20);
            this.lookupGerente1.somenteleitura = false;
            this.lookupGerente1.TabIndex = 2;
            this.lookupGerente1.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.gerente;
            this.lookupGerente1.Titulo = null;
            this.lookupGerente1.USUSel = -1;
            this.lookupGerente1.alterado += new System.EventHandler(this.lookupGerente1_alterado);
            // 
            // BotImpresso
            // 
            this.BotImpresso.AbrirArquivoExportado = true;
            this.BotImpresso.AcaoBotao = dllBotao.Botao.botao;
            this.BotImpresso.AssuntoEmail = "Solicitação de talões";
            this.BotImpresso.BotaoEmail = false;
            this.BotImpresso.BotaoImprimir = true;
            this.BotImpresso.BotaoPDF = false;
            this.BotImpresso.BotaoTela = true;
            this.BotImpresso.CorpoEmail = "Solicitação de talões";
            this.BotImpresso.CreateDocAutomatico = false;
            this.BotImpresso.Impresso = null;
            this.BotImpresso.Location = new System.Drawing.Point(316, 5);
            this.BotImpresso.Name = "BotImpresso";
            this.BotImpresso.NaoUsarxlsX = false;
            this.BotImpresso.NomeArquivoAnexo = "Taloes";
            this.BotImpresso.PriComp = null;
            this.BotImpresso.Size = new System.Drawing.Size(191, 56);
            this.BotImpresso.TabIndex = 35;
            this.BotImpresso.Titulo = "Solicitação de talões";
            this.BotImpresso.clicado += new dllBotao.BotaoEventHandler(this.BotNadaConsta_clicado);
            this.BotImpresso.NotificaErro += new dllBotao.ErroBotaoHandler(this.BotNadaConsta_NotificaErro);
            // 
            // lookupAssistente1
            // 
            this.lookupAssistente1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupAssistente1.Appearance.Options.UseBackColor = true;
            this.lookupAssistente1.CaixaAltaGeral = true;
            this.lookupAssistente1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupAssistente1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupAssistente1.Location = new System.Drawing.Point(12, 41);
            this.lookupAssistente1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupAssistente1.Name = "lookupAssistente1";
            this.lookupAssistente1.Size = new System.Drawing.Size(290, 20);
            this.lookupAssistente1.somenteleitura = false;
            this.lookupAssistente1.TabIndex = 36;
            this.lookupAssistente1.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.assistente;
            this.lookupAssistente1.Titulo = null;
            this.lookupAssistente1.USUSel = -1;
            this.lookupAssistente1.alterado += new System.EventHandler(this.lookupGerente1_alterado);
            // 
            // cSaldoCheques
            // 
            this.Autofill = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BotaoAlterar = false;
            this.BotaoExcluir = false;
            this.BotaoIncluir = false;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cSaldoCheques";
            this.Size = new System.Drawing.Size(1490, 807);
            this.Titulo = "Saldo cheques";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dcFolhasDeCheque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcedcFolhasDeCheque)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bindingSourcedcFolhasDeCheque;
        private dcFolhasDeCheque dcFolhasDeCheque;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoFolha1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT_BCO;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAtiva;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTPrincipal;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colFolhasBR;
        private DevExpress.XtraGrid.Columns.GridColumn colFolhasASSP;
        private DevExpress.XtraGrid.Columns.GridColumn colFolhasASS;
        private DevExpress.XtraGrid.Columns.GridColumn colTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colFolhasSindico;
        private dcFolhasDeChequeTableAdapters.ContasTableAdapter contasTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAplicacao;
        private Framework.Lookup.LookupGerente lookupGerente1;
        private VirBotaoNeon.cBotaoImpNeon BotImpresso;
        private Framework.Lookup.LookupGerente lookupAssistente1;
    }
}
