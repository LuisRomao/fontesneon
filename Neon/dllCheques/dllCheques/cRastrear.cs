using System.Windows.Forms;
using CompontesBasicos;

namespace dllCheques
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cRastrear : ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public cRastrear()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            Form FormularioDialogo = (Form)Parent;
            if (FormularioDialogo.DialogResult == DialogResult.Cancel)
                return true;
            //if (Numero.Value == null)
              //  return false;
            if (lookupCondominio_F1.CON_sel <= 0)
                return false;
            return base.CanClose();
        }

        private void Numero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                FechaTela(DialogResult.OK);
        }
    }
}
