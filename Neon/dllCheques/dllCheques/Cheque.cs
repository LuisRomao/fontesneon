/*
LH - 22/04/2014        13.2.8.20 - FIX bug travamentos 
MR - 24/04/2014 12:00  13.2.8.32 - Construtor para concilia��o de Pagamento Eletr�nico, tratamento da baixa e contrutor de alteracao de status com dados adicionais de Pagto Eletronico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (24/04/2014 12:00) ***)
MR - 28/04/2014 09:45            - Envio manual de D�gitos de Agencia e Conta no registro de pagamento eletr�nico, Tratamento de DOC e TEC como transferencia para banco 237 (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 29/04/2014 15:25            - Cancelamento de pagamento eletr�nico e tratamento do status de n�o aprovado pelo sindico nos pontos necessarios (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***)
LH - 08/05/2014 14:47  13.2.8.44 - FIX Faltava update na linha 2270
MR - 15/05/2014 11:45  13.2.9.0  - Permitir envio de cancelamento de pagamento eletronico mesmo que vencimento j� tenha passado (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (15/05/2014 11:45) ***)
MR - 22/07/2014 10:30            - Altera��es na c�pia de cheque visando altera��es no layout do pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (22/07/2014 10:30) ***)
MR - 23/07/2014 10:00            - Novas altera��es na c�pia de cheque visando altera��es no layout do pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (23/07/2014 10:00) ***)
MR - 30/07/2014 16:00            - Tratamento do novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 05/08/2014 11:00            - Tratamento do tipo de conta do condominio ao verificar o CNPJ a ser enviado no registro de pagamento eletronico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (05/08/2014 11:00) ***)
MR - 06/08/2014 13:00            - Tratamento do status de inconsistencia no pagamento eletr�nico nos pontos necessarios (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***)
LH - 29/08/2014 11:12  14.1.4.36 - Erro na baixa do cheque, estava conflitando como a baixa pela concilia��o. A grava��o na tabela CCD ficou opcional.                                   
LH - 25/12/2014 14:20  14.1.4.96 - Pagamento com boleto integrado na mesma tela de emiss�o de cheque
MR - 16/01/2015 11:00            - Tratamento dos status de pagamento eletronico na fun��o Editavel (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (16/01/2015 11:00) ***)
MR - 23/01/2015 16:00            - Tratamento dos status ComSindico na fun��o Editavel (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (23/01/2015 16:00) ***)
MR - 27/04/2015 12:00            - Pagamento de tributo eletronico com inclusao do "guia de recolhimento - eletr�nico" (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
MR - 20/05/2015 17:00            - Registro de remessa PECreditoConta para contas do proprio condominio com CCT indicado no campo CHEAdicional (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***)
MR - 10/06/2015 08:20            - PECreditoConta entre contas do pr�prio condominio, seta CHEAdicional com o CCT de Cr�dito apenas (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***)
MR - 15/06/2015 11:30            - PECreditoConta referente a nota de honorario imprime recibo (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (15/06/2015 11:30) ***)
MR - 08/07/2015 09:00            - Corre��o de bug no cancelamento de pagamento eletr�nico, n�o seta para gerar arquivo de cancelamento se ainda n�o foi emitido (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (08/07/2015 09:00) ***)
MR - 18/08/2015 10:00            - Cancelamento tributo eletronico PEGuia / Notas (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***)
MR - 05/01/2016 13:30            - Tipo de conta cr�dito para fornecedor diferenciado entre corrente e poupan�a (Altera��es indicadas por *** MRC - INICIO (05/01/2016 13:30) ***)
MR - 14/01/2016 12:30            - Retirada a Finalidade DOC/TED que passa a ser enviado com valor default
MR - 08/06/2016 11:00            - Trata n�mero sequencial de remessa para pagamentos eletr�nicos por banco e n�o mais por condominio (Altera��es indicadas por *** MRC - INICIO (08/06/2016 11:00) ***)*/

using Abstratos;
using AbstratosNeon;
using BoletosCalc;
using CodBar;
using CompontesBasicosProc;
using CompontesBasicos;
using dllC12;
using dllCheques.Properties;
using dllChequesProc;
using dllImpostoNeon;
using dllImpostoNeonProc;
using Framework;
using Framework.objetosNeon;
using FrameworkProc;
using FrameworkProc.objetosNeon;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using VirMSSQL;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using DocBacarios;

namespace dllCheques
{
    /// <summary>
    /// Objeto Cheque (derivado do ABS_Cheque)
    /// </summary>
    public class Cheque : ChequeProc
    {


        static private string ComandoCHEs =
             "SELECT        CHE, CHEValor\r\n" +
             "FROM            CHEques\r\n" +
             "WHERE        (CHE_CCT = @P1) AND (CHEVencimento between @P2 and @P3) AND (CHETipo IN (20, 21, 22, 23, 24)) AND (CHE_CCD IS NULL) and (CHEStatus <> 0);";

        static private string ComandoCHEsSAL =
             "SELECT        CHE, CHEValor\r\n" +
             "FROM            CHEques\r\n" +
             "WHERE        (CHE_CCT = @P1) AND (CHEVencimento between @P2 and @P3) AND (CHETipo = 28) AND (CHE_CCD IS NULL) and (CHEStatus <> 0);";

        static private string ComandoCHEsSALSEMCCT =
"SELECT        dbo.CHEques.CHE, dbo.CHEques.CHEValor, dbo.ContaCorrenTe.CCT\r\n" +
"FROM            dbo.CHEques INNER JOIN\r\n" +
"                         dbo.CONDOMINIOS ON dbo.CHEques.CHE_CON = dbo.CONDOMINIOS.CON INNER JOIN\r\n" +
"                         dbo.ContaCorrenTe ON dbo.CONDOMINIOS.CON = dbo.ContaCorrenTe.CCT_CON\r\n" +
"WHERE        (dbo.CHEques.CHEVencimento BETWEEN @P2 AND @P3) AND (dbo.CHEques.CHETipo = 28) AND (dbo.CHEques.CHE_CCD IS NULL) AND (dbo.CHEques.CHEStatus <> 0) AND (dbo.ContaCorrenTe.CCT = @P1)\r\n" +
"\r\n" +
";";
        

        private dCheque.ContaCorrenTeRow _CCTrow;

        private dCheque.ContaCorrenTeRow CCTrow
        {
            get
            {
                if (_CCTrow == null)
                    if (dCheque.ContaCorrenTeTableAdapter.Fill(dCheque.ContaCorrenTe, CHErow.CHE_CCT) == 1)
                        _CCTrow = dCheque.ContaCorrenTe[0];
                    else
                        return null;
                return _CCTrow;
            }
        }


        /// <summary>
        /// Abre tela para pedir a linha digit�vel
        /// </summary>
        /// <param name="strParcela"></param>
        /// <param name="strPAGAdicional"></param>
        /// <param name="datVencimento"></param>
        /// <param name="decValor"></param>
        /// <returns></returns>
        public static bool SolicitaLinhaDigitavel(string strParcela, ref string strPAGAdicional, ref DateTime datVencimento, ref decimal decValor)
        {
            while (true)
            {
                //Solicita linha digit�vel
                if (!VirInput.Input.Execute("Linha Digit�vel (digite apenas os n�meros) - Parcela " + strParcela + " :", ref strPAGAdicional, false, 47))
                    return false;
                if (strPAGAdicional == "")
                    return true;

                //Limpa linha digit�vel
                strPAGAdicional = strPAGAdicional.Replace(".", "").Replace(" ", "");

                //Verifica se tem apenas n�meros
                Int64 lngLinhaDigitavel;
                if (!Int64.TryParse(strPAGAdicional.Substring(0, 10), out lngLinhaDigitavel) || !Int64.TryParse(strPAGAdicional.Substring(10, 10), out lngLinhaDigitavel) || !Int64.TryParse(strPAGAdicional.Substring(20, 10), out lngLinhaDigitavel) || !Int64.TryParse(strPAGAdicional.Substring(30, 10), out lngLinhaDigitavel) || !Int64.TryParse(strPAGAdicional.Substring(40), out lngLinhaDigitavel))
                    MessageBox.Show(string.Format("Linha Digit�vel incorreta (n�o num�rica). Favor digitar novamente."));
                else
                {
                    //Confere linha digit�vel
                    if (strPAGAdicional.Length == 44)
                    {
                        //Possivelmente usou leitor com c�digo de barras, ent�o converte em linha digit�vel e pede confirma��o
                        strPAGAdicional = BoletoCalc.ConverteCodigoBarrasEmLinhaDigitavel(strPAGAdicional);
                        if (MessageBox.Show(string.Format("Confirma a Linha Digit�vel abaixo ?\n\n{0}", strPAGAdicional.Substring(0, 5) + "." + strPAGAdicional.Substring(5, 5) + " " + strPAGAdicional.Substring(10, 5) + "." + strPAGAdicional.Substring(15, 6) + " " + strPAGAdicional.Substring(21, 5) + "." + strPAGAdicional.Substring(26, 6) + " " + strPAGAdicional.Substring(32, 1) + " " + strPAGAdicional.Substring(33)), "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            break;
                    }
                    else if (strPAGAdicional.Length < 47)
                        MessageBox.Show(string.Format("Linha Digit�vel incompleta. Favor digitar toda a linha digit�vel do t�tulo."));
                    else if (!BoletoCalc.VerificaLinhaDigitavel(strPAGAdicional))
                        MessageBox.Show(string.Format("Linha Digit�vel incorreta. Favor digitar novamente."));
                    else
                        break;
                }
            }

            //Seta vencimento e valor de acordo com os dados da linha digit�vel e do grid
            DateTime DataRef = new DateTime(1997, 10, 7);
            TimeSpan Fator = datVencimento.Date.Subtract(DataRef);
            bool booAjustaDACCodigoBarras = false;
            if (Convert.ToInt32(strPAGAdicional.Substring(33, 4)) != 0)
                datVencimento = DataRef.AddDays(Convert.ToInt32(strPAGAdicional.Substring(33, 4)));
            else
            {
                strPAGAdicional = strPAGAdicional.Substring(0, 33) + Fator.ToString().Substring(0, 4) + strPAGAdicional.Substring(strPAGAdicional.Length - 10, 10);
                booAjustaDACCodigoBarras = true;
            }
            if (Convert.ToInt32(strPAGAdicional.Substring(strPAGAdicional.Length - 10, 10)) != 0)
                decValor = (Convert.ToDecimal(strPAGAdicional.Substring(strPAGAdicional.Length - 10, 10)) / 100);
            else
            {
                strPAGAdicional = strPAGAdicional.Substring(0, 37) + decValor.ToString("0.00").Replace(",", "").PadLeft(10, '0');
                booAjustaDACCodigoBarras = true;
            }
            if (booAjustaDACCodigoBarras)
                strPAGAdicional = strPAGAdicional.Substring(0, 32) + BoletoCalc.Modulo11_2_9(strPAGAdicional.Substring(0, 4) + strPAGAdicional.Substring(33, 14) + strPAGAdicional.Substring(4, 5) + strPAGAdicional.Substring(10, 10) + strPAGAdicional.Substring(21, 10), false) + strPAGAdicional.Substring(33, 14);


            //Pede confirma��o para inserir exibindo vencimento e valor
            string strMen = "";
            if (decValor != 0)
                strMen = string.Format("Confirma o cadastro do t�tulo abaixo ?\n\nVencimento - {0:dd/MM/yyyy}\nValor - {1:n2}", datVencimento, decValor);
            else
                strMen = string.Format("Confirma o cadastro do t�tulo abaixo ?\n\nVencimento - {0:dd/MM/yyyy}\nValor N�O DEFINIDO", datVencimento);
            if (MessageBox.Show(strMen, "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return false;
            else
                return true;
        }
        


        /// <summary>
        /// Ajusta o valor do cheque gerando uma parcela com o saldo.
        /// </summary>
        /// <param name="NovoValor"></param>
        /// <param name="Observacao"></param>
        /// <returns></returns>
        public bool AjustarValor(decimal NovoValor, string Observacao)
        {
            dCheque.PAGamentosRow[] PAGs = CHErow.GetPAGamentosRows();
            if (PAGs.Length != 1)
                return false;
            decimal erro = NovoValor - PAGs[0].PAGValor;
            // nao est� previsto o ajuste para valores menores do que o original

            if (CHEStatus == CHEStatus.CadastradoBloqueado)
            {
                PAGs[0].PAGValor = NovoValor;
                TableAdapter.ST().ExecutarSQLNonQuery("update PAGamentos set PAGValor = @P1 where PAG = @P2", NovoValor, PAGs[0].PAG);
            }
            else
            {
                if (erro < 0)
                    return false;
                System.Data.DataRow PAGModelo = TableAdapter.ST().BuscaSQLRow("select * from PAGamentos where PAG = @P1", PAGs[0].PAG);
                PAGModelo["PAG"] = DBNull.Value;
                PAGModelo["PAG_CHE"] = CHErow.CHE;
                PAGModelo["PAGValor"] = erro;
                PAGModelo["PAGI_USU"] = EMPTProc1.USU;
                PAGModelo["PAGA_USU"] = DBNull.Value;
                PAGModelo["PAGDATAI"] = DateTime.Now;
                PAGModelo["PAGDATAA"] = DBNull.Value;
                TableAdapter.ST().Insert("PAGamentos", PAGModelo, false, false);
            }
            LiberaEdicao = true;
            AjustarTotalPelasParcelas();
            GravaObs(Observacao);
            return true;
        }

        /// <summary>
        /// Localiza conjunto de cheques eletr�micos para uma data.
        /// </summary>
        /// <param name="CCT"></param>
        /// <param name="Data"></param>
        /// <param name="ValorTotal"></param>
        /// <param name="Salarios"></param>
        /// <returns></returns>
        public static List<Cheque> BuscaChequesEletronicosSomados(int CCT, DateTime Data, decimal ValorTotal, bool Salarios = false)
        {
            List<Cheque> resultado = null;
            DateTime DataA = Data;
            if (Data < DateTime.Today.AddDays(-7))
            {
                DataA = Data.SomaDiasUteis(2, Sentido.Traz);
                Data = Data.SomaDiasUteis(2);
            };

            DataTable DT;

            if (Salarios)
            {
                DT = TableAdapter.ST().BuscaSQLTabela(ComandoCHEsSAL, CCT, DataA, Data);
                if (DT.Rows.Count == 0)
                {
                    DT = TableAdapter.ST().BuscaSQLTabela(ComandoCHEsSALSEMCCT, CCT, DataA, Data);
                    if (DT.Rows.Count == 0)
                        return null;
                }
            }
            else
            {
                DT = TableAdapter.ST().BuscaSQLTabela(ComandoCHEs, CCT, DataA, Data);
                if (DT.Rows.Count == 0)
                    return null;
            }



            Agrupador Ag = new Agrupador();


            SortedList<int, decimal> Itens = new SortedList<int, decimal>();


            foreach (DataRow DR in DT.Rows)
                Itens.Add((int)DR["CHE"], (decimal)DR["CHEValor"]);

            List<int> Resposta = Ag.Agrupar(ValorTotal, Itens);
            if (Resposta != null)
            {
                resultado = new List<Cheque>();
                foreach (int CHE in Resposta)
                    resultado.Add(new Cheque(CHE, TipoChave.CHE));
            }



            //decimal Total = 0;
            //foreach(DataRow DR in DT.Rows)
            //    Total += (decimal)DR["CHEValor"];
            //if(Total == ValorTotal)
            //{
            //    resultado = new List<Cheque>();
            //    foreach(DataRow DR in DT.Rows)            
            //        resultado.Add(new Cheque((int)DR["CHE"],TipoChave.CHE));
            //}
            return resultado;
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="PAGTIPO"></param>
        /// <param name="Procuracao"></param>
        /// <param name="TipoMalote"></param>
        /// <param name="Vencimento"></param>
        /// <param name="HoraCadastro"></param>
        /// <param name="MaloteIda"></param>
        /// <param name="MaloteVolta"></param>
        /// <param name="RetornaNaData"></param>
        /// <returns></returns>
        public static DateTime[] CalculaDatas(PAGTipo PAGTIPO, bool Procuracao, malote.TiposMalote TipoMalote, DateTime Vencimento, DateTime HoraCadastro, ref malote MaloteIda, ref malote MaloteVolta, out bool RetornaNaData)
        {
            RetornaNaData = false;
            try
            {                
                DateTime DEmitir = DateTime.MinValue;
                DateTime DRetorno = DateTime.MinValue;
                switch (PAGTIPO)
                {
                    case PAGTipo.Caixinha:
                    case PAGTipo.DebitoAutomatico:
                    case PAGTipo.GuiaEletronica:
                    case PAGTipo.ChequeSindico:
                    case PAGTipo.Folha:
                        DEmitir = DRetorno = Vencimento;
                        MaloteIda = MaloteVolta = null;
                        break;
                    case PAGTipo.sindico:
                        MaloteIda = new malote(TipoMalote, Vencimento);
                        while (MaloteIda.DataMalote >= Vencimento)
                            MaloteIda--;
                        MaloteVolta = MaloteIda;
                        MaloteVolta++;
                        DEmitir = MaloteIda.DataEnvio;
                        DRetorno = MaloteVolta.DataRetorno;
                        break;
                    case PAGTipo.cheque:
                    case PAGTipo.boleto:
                    case PAGTipo.deposito:
                    case PAGTipo.TrafArrecadado:
                    case PAGTipo.Guia:
                    case PAGTipo.Honorarios:
                    case PAGTipo.Conta:                    
                    case PAGTipo.PECreditoConta:
                    case PAGTipo.PETransferencia:
                    case PAGTipo.PEOrdemPagamento:
                    case PAGTipo.PETituloBancario:                    
                    case PAGTipo.PETituloBancarioAgrupavel:
                    //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    case PAGTipo.PEGuia:
                        //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***                            
                        if (Vencimento.DayOfWeek == DayOfWeek.Saturday)
                            Vencimento = Vencimento.AddDays(2);
                        else
                            if (Vencimento.DayOfWeek == DayOfWeek.Sunday)
                                Vencimento = Vencimento.AddDays(1);
                        if (Procuracao)
                        {
                            DEmitir = Vencimento.AddHours(12);
                            DRetorno = Vencimento.AddHours(13);
                            MaloteIda = MaloteVolta = null;
                        }
                        else
                        {
                            MaloteVolta = malote.MalotePara(TipoMalote, malote.SentidoMalote.volta, Vencimento.AddHours(10), 0);
                            MaloteIda = MaloteVolta;
                            MaloteIda--;
                            if (MaloteIda.DataEnvio < HoraCadastro)
                            {
                                MaloteVolta = malote.MalotePara(TipoMalote, malote.SentidoMalote.volta, Vencimento.AddHours(13), 0);
                                MaloteIda = MaloteVolta;
                                MaloteIda--;
                                RetornaNaData = (Vencimento.Date == MaloteVolta.DataMalote);
                            };
                            DEmitir = MaloteIda.DataEnvio;
                            DRetorno = MaloteVolta.DataRetorno;
                        }
                        break;                    
                    default:
                        throw new NotImplementedException(string.Format("Tipo n�o tratado '{0}' em cheque.cs - 1211",PAGTIPO));
                }
                DateTime[] Retorno = new DateTime[] { DEmitir, DRetorno };
                return Retorno;
            }
            catch
            {
                return null;
            }

        }
        */

        /// <summary>
        /// Calculas as datas de ida e volta
        /// </summary>
        public void CalculaDatas()
        {
            if (rowCON.IsCONProcuracaoNull())
            {
                MessageBox.Show(string.Format("Erro no cadastro do condominio {0}: procura��o"), rowCON.CONCodigo);
                return;
            };
            if (rowCON.IsCONMaloteNull())
            {
                MessageBox.Show(string.Format("Erro no cadastro do condominio {0}: Malote"), rowCON.CONCodigo);
                return;
            }
            DateTime[] Datas = CalculaDatas((PAGTipo)CHErow.CHETipo, rowCON.CONProcuracao, (malote.TiposMalote)rowCON.CONMalote, CHErow.CHEVencimento, DateTime.Now, ref MaloteIda, ref MaloteVolta, out RetornaNaData);
            CHErow.CHEIdaData = Datas[0];
            CHErow.CHERetornoData = Datas[1];
        }

        /// <summary>
        /// move os PAG 2100 para outro cheque ou retorna este mesmo se todos os PAG forem 2100. N�o gera o GPS
        /// </summary>
        /// <returns></returns>
        public Cheque ConverteEletronico2100()
        {
            if ((CHEStatus)CHErow.CHEStatus != CHEStatus.Cadastrado)
                return null;
            try
            {

                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 379", dCheque.CHEquesTableAdapter, dCheque.PAGamentosTableAdapter);
                Cheque retornar = null;
                string comando1 =
    "UPDATE    PAGamentos\r\n" +
    "SET              PAGTipo = @P1\r\n" +
    "FROM         PAGamentos\r\n" +
    "WHERE     PAG = @P2";
                string comando2 =
    "UPDATE    PAGamentos\r\n" +
    "SET             PAGTipo = @P1, PAG_CHE = @P2\r\n" +
    "FROM         PAGamentos\r\n" +
    "WHERE     PAG = @P3;";
                ArrayList Linhas2100 = new ArrayList();
                dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHErow.CHE);
                foreach (dCheque.PAGamentosRow rowPAG in CHErow.GetPAGamentosRows())
                {
                    if (rowPAG.IsPAGIMPNull())
                        continue;
                    dllImpostos.Imposto Imposto = new dllImpostos.Imposto((TipoImposto)rowPAG.PAGIMP);
                    if (Imposto.Codigo == "2100")
                        Linhas2100.Add(rowPAG);
                }
                if ((Linhas2100.Count == 0) && (CHErow.GetPAGamentosRows().Length > 0))
                    retornar = new Cheque(CHErow.CHEVencimento, CHErow.CHEFavorecido, CHErow.CHE_CON, PAGTipo.GuiaEletronica, true, 0,null,"",false, EMPTProc1);
                else
                    if (Linhas2100.Count == CHErow.GetPAGamentosRows().Length)
                {
                    //converte tudo
                    foreach (dCheque.PAGamentosRow rowPAG in Linhas2100)
                        EMPTProc1.STTA.ExecutarSQLNonQuery(comando1, (int)PAGTipo.GuiaEletronica, rowPAG.PAG);

                    CHErow.CHETipo = (int)PAGTipo.GuiaEletronica;
                    CHErow.CHEStatus = (int)CHEStatus.Eletronico;
                    dCheque.CHEquesTableAdapter.Update(CHErow);
                    CHErow.AcceptChanges();
                    retornar = this;
                }
                else
                {
                    retornar = new Cheque(CHErow.CHEVencimento, CHErow.CHEFavorecido, CHErow.CHE_CON, PAGTipo.GuiaEletronica, true, 0, null, string.Empty, false, EMPTProc1);
                    foreach (dCheque.PAGamentosRow rowPAG in Linhas2100)
                        EMPTProc1.STTA.ExecutarSQLNonQuery(comando2, (int)PAGTipo.GuiaEletronica, retornar.CHErow.CHE, rowPAG.PAG);
                    retornar.AjustarTotalPelasParcelas();
                    AjustarTotalPelasParcelas();
                }
                EMPTProc1.Commit();
                return retornar;
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                return null;
            }
        }

        /// <summary>
        /// Divide o cheque em partes
        /// </summary>
        /// <returns>retorna nulo se houver mais de uma parcela ou os valores forem inferiores</returns>
        public Cheque[] Dividir(params decimal[] Valores)
        {
            dCheque.PAGamentosRow[] PAGs = CHErow.GetPAGamentosRows();
            if (PAGs.Length != 1)
                return null;
            ArrayList StatusValidos = new ArrayList(new CHEStatus[] { CHEStatus.Cadastrado, CHEStatus.DebitoAutomatico });
            if (!StatusValidos.Contains(Status))
                return null;
            decimal Saldo = CHErow.CHEValor;
            foreach (decimal Vparcial in Valores)
                Saldo -= Vparcial;
            if (Saldo <= 0)
                return null;
            Cheque[] retorno = new Cheque[Valores.Length + 1];
            int i = 0;
            System.Data.DataRow PAGModelo = TableAdapter.ST().BuscaSQLRow("select * from PAGamentos where PAG = @P1", PAGs[0].PAG);
            foreach (decimal Vparcial in Valores)
            {
                retorno[i] = new Cheque(CHErow.CHEVencimento, CHErow.CHEFavorecido, CHErow.CHE_CON, (PAGTipo)CHErow.CHETipo, true, 0);
                PAGModelo["PAG"] = DBNull.Value;
                PAGModelo["PAG_CHE"] = retorno[i].CHErow.CHE;
                PAGModelo["PAGValor"] = Vparcial;
                PAGModelo["PAGI_USU"] = EMPTProc1.USU;
                PAGModelo["PAGA_USU"] = DBNull.Value;
                PAGModelo["PAGDATAI"] = DateTime.Now;
                PAGModelo["PAGDATAA"] = DBNull.Value;
                TableAdapter.ST().Insert("PAGamentos", PAGModelo, false, false);
                retorno[i].LiberaEdicao = true;
                retorno[i].AjustarTotalPelasParcelas();
            }
            TableAdapter.ST().ExecutarSQLNonQuery("update PAGamentos set PAGValor = @P1 where PAG = @P2", Saldo, PAGs[0].PAG);
            retorno[Valores.Length] = this;
            LiberaEdicao = true;
            AjustarTotalPelasParcelas();
            return retorno;
        }

        /// <summary>
        /// Divido o cheque em um por PAG
        /// </summary>
        /// <returns></returns>
        public Cheque[] DividirporPAG()
        {
            dCheque.PAGamentosRow[] PAGrows = CHErow.GetPAGamentosRows();
            Cheque[] retorno = new Cheque[PAGrows.Length];
            retorno[0] = this;
            try
            {
                EMPTProc1.AbreTrasacaoSQL("DividirporPAG Cheque.cs - 1066");
                for (int i = 1; i < PAGrows.Length; i++)
                {
                    TableAdapter.ST().ExecutarSQLNonQuery("update PAGamentos set PAG_CHE = null,PAGPermiteAgrupar = 0 where PAG = @P1", PAGrows[i].PAG);
                    retorno[i] = new Cheque(Vencimento, Favorecido, CON, Tipo, true);
                    retorno[i].IncluiPAGs(PAGrows[i].PAG);
                    retorno[i].AjustarTotalPelasParcelas();
                };
                AjustarTotalPelasParcelas();
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
            }
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Eh2100Puro()
        {
            dCheque.PAGamentosTableAdapter.EmbarcaEmTransST();
            dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHErow.CHE);
            foreach (dCheque.PAGamentosRow rowPAG in CHErow.GetPAGamentosRows())
            {
                if (rowPAG.IsPAGIMPNull())
                    return false;
                dllImpostos.Imposto Imposto = new dllImpostos.Imposto((TipoImposto)rowPAG.PAGIMP);
                if (Imposto.Codigo != "2100")
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataAgendar"></param>
        /// <param name="ValorOutras"></param>
        /// <param name="LiberacaoManual"></param>
        /// <returns></returns>
        public bool EmiteEletrocnico2100(DateTime DataAgendar, decimal ValorOutras, bool LiberacaoManual)
        {
            if (CHErow.CHEValor < ValorOutras)
                return false;
            FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(CHErow.CHE_CON);
            if (rowCON == null)
                return false;


            dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();

            //string CONCep = "";
            //if(!TableAdapter.ST().BuscaEscalar("select CONCep from condominios where CON = @P1", out CONCep,CHErow.CHE_CON))
            //    return false;

            string CONCep = "0" + CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(rowCON.CONCep, CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.SoNumeros);

            DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(rowCON.CONCnpj);
            novalinha.GPSNomeClientePagador = rowCON.CONNome;
            novalinha.GPSTipo = (int)TipoImposto.INSS;
            novalinha.GPS_CON = CHErow.CHE_CON;
            string manobra = string.Format("{0} - {1}", rowCON.CONCodigo, rowCON.CONEndereco);
            if (manobra.Length > 40)
                manobra = manobra.Substring(1, 40);
            novalinha.GPSEndereco = manobra;
            novalinha.GPSCEP = int.Parse(CONCep);
            novalinha.GPSUF = rowCON.CIDUf;
            novalinha.GPSCidade = (rowCON.CIDNome.Length > 20) ? rowCON.CIDNome.Substring(0, 20) : rowCON.CIDNome;
            novalinha.GPSBairro = rowCON.CONBairro;
            novalinha.GPSCNPJ = Cnpj.Valor;
            novalinha.GPSDataPagto = DataAgendar;
            novalinha.GPSValorPrincipal = CHErow.CHEValor - ValorOutras;
            novalinha.GPSValorMulta = 0;
            novalinha.GPSValorJuros = 0;
            novalinha.GPSOutras = ValorOutras;
            novalinha.GPSValorTotal = CHErow.CHEValor;
            novalinha.GPSCodigoReceita = 2100;
            novalinha.GPSIdentificador = Cnpj.Valor;
            Framework.objetosNeon.Competencia Compet1 = new Competencia(CHErow.CHEVencimento);
            Compet1--;
            novalinha.GPSAno = Compet1.Ano;
            novalinha.GPSMes = Compet1.Mes;

            novalinha.GPSVencimento = CHErow.CHEVencimento;
            novalinha.GPSNomeIdentificador = Cnpj.ToString();
            if (rowCON.IsCONAgenciaNull())
            {
                MessageBox.Show(string.Format("Condom�nio sem agencia:{0} - {1}", rowCON.CONCodigo, rowCON.CON));
                return false;
            }
            novalinha.GPSAgencia = int.Parse(rowCON.CONAgencia);
            novalinha.GPSConta = int.Parse(rowCON.CONConta);
            if (novalinha.GPSValorTotal > 20000)
                novalinha.GPSStatus = (int)StatusArquivo.Liberar_Gerencia;
            else
                if (novalinha.GPSValorTotal > 15000)
                novalinha.GPSStatus = (int)StatusArquivo.Liberar_Gerente;
            else
                    if ((novalinha.GPSValorTotal > 5000) || (LiberacaoManual))
                novalinha.GPSStatus = (int)StatusArquivo.Liberar;
            else
                novalinha.GPSStatus = (int)StatusArquivo.ParaEnviar;
            novalinha.GPSOrigem = (int)OrigemImposto.Folha;
            novalinha.GPSI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
            novalinha.GPSDATAI = DateTime.Now;
            dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
            dGPS.dGPSSt.GPSTableAdapter.EmbarcaEmTransST();
            dGPS.dGPSSt.GPSTableAdapter.Update(dGPS.dGPSSt.GPS);
            dGPS.dGPSSt.GPS.AcceptChanges();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NovoTipo"></param>
        /// <returns></returns>
        public bool MudaTipo(PAGTipo NovoTipo)
        {
            if (NovoTipo == PAGTipo.GuiaEletronica)
            {
                if (this.CHErow.CHETipo != (int)PAGTipo.Guia)
                    return false;
                return false;// ParaEletronico();
            }
            return true;
        }

        //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
        /// <summary>
        /// Separa o pagamento em um novo cheque especifico
        /// </summary>
        /// <param name="intPAG">PAG</param>
        /// <param name="decValor">Valor</param>
        public Cheque SeparaPAG(int intPAG, decimal decValor)
        {
            //Remove o PAG do cheque
            TableAdapter.ST().ExecutarSQLNonQuery("UPDATE PAGamentos SET PAG_CHE = null WHERE PAG = @P1", intPAG);
            LiberaEdicao = true;
            AjustarTotalPelasParcelas();

            //Cria novo cheque, salva dados comuns do cheque anterior
            Cheque Cheque = new Cheque(CHErow.CHEVencimento, CHErow.CHEFavorecido, CHErow.CHE_CON, (PAGTipo)CHErow.CHETipo, true, null, CHErow.CHE_CCT);
            Cheque.CHErow.CHENumero = Cheque.CHErow.CHE;
            Cheque.CHErow.CHEValor = decValor;
            Cheque.CHErow.CHEEmissao = CHErow.CHEEmissao;
            Cheque.CHErow.CHEObs = CHErow.CHEObs + String.Format("{0:dd/MM/yy HH:mm:ss} - {1}\r\n", DateTime.Now, "Cheque separado (original No " + CHErow.CHE + ")");

            //Insere o PAG no novo cheque
            TableAdapter.ST().ExecutarSQLNonQuery("UPDATE PAGamentos SET PAG_CHE = @P2 WHERE PAG = @P1", intPAG, Cheque.CHErow.CHE);
            Cheque.LiberaEdicao = true;
            Cheque.AjustarTotalPelasParcelas();

            //Retorna o novo cheque
            return Cheque;
        }

        

        /// <summary>
        /// Favorecido  (passar para o ABS)
        /// </summary>
        public int CON { get { return CHErow.CHE_CON; } }

        /// <summary>
        /// Favorecido  (passar para o ABS)
        /// </summary>
        public string Favorecido { get { return CHErow.CHEFavorecido; } }

        /// <summary>
        /// se � eletronico
        /// </summary>
        public bool isEletronico
        {
            get
            {
                return dCheque.PAGTiposEletronicos.Contains(Tipo);
            }
        }

        /// <summary>
        /// Vencimento (passar para o ABS)
        /// </summary>
        public DateTime Vencimento { get { return CHErow.CHEVencimento; } }

        /*
        private EMPTProc _EMPTipo;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTipo
        {
            get
            {
                if (_EMPTipo == null)
                    _EMPTipo = new EMPTProc(EMPTProc.EMPTipo.Local);
                return _EMPTipo;
            }
        }*/
        /*
        private VirMSSQL.TableAdapter STTA()
        {
            return TableAdapter.ST(EMPTipo == EMPT.Local ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial);
        }

        private void Vircatch(Exception e)
        {
            STTA().Vircatch(e);
        }

        private bool Commit()
        {
            return STTA().Commit();
        }*/




        /*
        /// <summary>
        /// �ltimo erro
        /// </summary>
        [Obsolete("usar UltimaException.Message")]
        public string UltimoErro
        {
            get
            {
                return UltimaException == null ? "" : UltimaException.Message;
            }
        }*/



        /// <summary>
        /// 
        /// </summary>
        public ArrayList Verso
        {
            get
            {
                //if (CHErow.CHEValor == 1690.40M)
                //{
                //    MessageBox.Show("V1");
                //}
                ArrayList Retorno = new ArrayList();
                //if (CHErow.CHEValor == 1690.40M)
                //{
                //    MessageBox.Show("V2");
                //}                
                Array.ForEach(CHErow.GetPAGamentosRows(),
                delegate (dCheque.PAGamentosRow PAGrow)
                {
                    Retorno.Add(String.Format("                {0}-{1}-{2:n2}", PAGrow.NOA_PLA, PAGrow.NOAServico, PAGrow.PAGValor));
                    if (((PAGTipo)PAGrow.PAGTipo == PAGTipo.deposito) || ((PAGTipo)PAGrow.PAGTipo == PAGTipo.TrafArrecadado))
                        if (!PAGrow.IsNOADadosPagNull())
                            Retorno.Add(string.Format("Dep�sito: {0}", PAGrow.NOADadosPag));
                });
                return Retorno;
            }
        }



        #region Construtores    

        /// <summary>
        /// Construtor para o Abstrato
        /// </summary>
        /// <param name="CHE"></param>
        public Cheque(int CHE) : this(CHE, TipoChave.CHE) { }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="TipoCh"></param>
        /// <param name="_EMPTProc1"></param>         
        public Cheque(int Chave, TipoChave TipoCh, EMPTProc _EMPTProc1 = null) : base(Chave, TipoCh, _EMPTProc1) { }        

        /// <summary>
        /// Cosntrutor
        /// </summary>
        /// <param name="conta">Conta</param>
        /// <param name="Numero">N�mero do cheque</param>
        /// <param name="UsaCCT">usa CCT ou SCC</param>
        /// <param name="_EMPTProc1"></param>
        public Cheque(int conta, int Numero, bool UsaCCT, EMPTProc _EMPTProc1 = null)
        {
            if (_EMPTProc1 == null)
                _EMPTProc1 = new EMPTProc(EMPTipo.Local);
            EMPTProc1 = _EMPTProc1;
            TableAdapter.EmbarcaEmTrans(dCheque.CHEquesTableAdapter, dCheque.PAGamentosTableAdapter);
            if (UsaCCT)
            {
                Encontrado = (dCheque.CHEquesTableAdapter.FillByNumero(dCheque.CHEques, conta, Numero) == 1);
                if (Encontrado)
                    CHErow = dCheque.CHEques[0];
            }
            else
            {
                int nEncontrados = dCheque.CHEquesTableAdapter.FillBySCCNumero(dCheque.CHEques, conta, Numero);
                Encontrado = (nEncontrados >= 1);
                if (Encontrado)
                {
                    CHErow = dCheque.CHEques[0];
                    for (int i = 0; i < nEncontrados; i++)
                        if ((CHEStatus)dCheque.CHEques[i].CHEStatus != CHEStatus.Cancelado)
                        {
                            CHErow = dCheque.CHEques[i];
                            break;
                        }
                }
            }
            if (Encontrado)
                dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHErow.CHE);
            ;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="row">linha da tabela</param>
        public Cheque(dCheque.CHEquesRow row)
        {
            Encontrado = true;
            CHErow = row;
            _EMPTProc1 = ((dCheque)(row.Table.DataSet)).EMPTProc1;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Vencimento"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="NovoCheque"></param>
        /// <param name="CCD"></param>
        /// <param name="CCT"></param>
        /// <param name="Adicional"></param>
        /// <param name="bloqueado"></param>
        /// <param name="_EMPTProc1"></param>
        public Cheque(DateTime Vencimento, string Favorecido, int CON, PAGTipo Tipo, bool NovoCheque, int? CCD = null, int? CCT = null, string Adicional = "", bool bloqueado = false, EMPTProc _EMPTProc1 = null)
        {
            if (_EMPTProc1 == null)
                _EMPTProc1 = new EMPTProc(EMPTipo.Local);
            EMPTProc1 = _EMPTProc1;
            if (bloqueado)
                if (CCD.HasValue)
                    throw new ArgumentException("cheque bloqueado com CCD");
            //if(!NovoCheque)
            //    throw new ArgumentException("cheque bloqueado com agrupamento");
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***
            if (Tipo == PAGTipo.PECreditoConta && Adicional.Contains("- CCT = "))
                Adicional = Adicional.Substring(Adicional.IndexOf("CCT = "));
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (10/06/2015 08:20) ***

            try
            {
                if (CCD.GetValueOrDefault(0) != 0)
                    NovoCheque = true;
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 444", dCheque.CHEquesTableAdapter);
                if (!NovoCheque)
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Busca CHEQUE", true);
                    if (bloqueado)
                        if (Adicional == string.Empty)
                            dCheque.CHEquesTableAdapter.FillParaIncluirBloc(dCheque.CHEques, Vencimento, Favorecido, CON, (int)Tipo);
                        else
                            dCheque.CHEquesTableAdapter.FillParaIncAdBloc(dCheque.CHEques, Vencimento, Favorecido, CON, (int)Tipo, Adicional);
                    else
                        if (Adicional == string.Empty)
                        dCheque.CHEquesTableAdapter.FillParaIncluir(dCheque.CHEques, Vencimento, Favorecido, CON, (int)Tipo);
                    else
                        dCheque.CHEquesTableAdapter.FillParaIncAd(dCheque.CHEques, Vencimento, Favorecido, CON, (int)Tipo, Adicional);
                    foreach (dCheque.CHEquesRow rowCHE in dCheque.CHEques)
                    {
                        int cheCCT = 0;
                        if (!rowCHE.IsCHE_CCTNull())
                            cheCCT = rowCHE.CHE_CCT;
                        if (CCT.GetValueOrDefault(0) == 0 || cheCCT == CCT.Value)
                        {
                            CHErow = rowCHE;
                            break;
                        }
                    }
                    CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                }
                if (CHErow == null)
                {
                    CHErow = dCheque.CHEques.NewCHEquesRow();
                    CHErow.CHE_CON = CON;
                    CHErow.CHETipo = (int)Tipo;

                    switch (Tipo)
                    {
                        case PAGTipo.DebitoAutomatico:
                            if (CCD.GetValueOrDefault(0) == 0)
                                if (bloqueado)
                                    CHErow.CHEStatus = (int)CHEStatus.CadastradoBloqueado;
                                else
                                {
                                    CHErow.CHEStatus = (int)CHEStatus.DebitoAutomatico;
                                    CHErow.CHEEmissao = DateTime.Today;
                                }
                            else
                            {
                                CHErow.CHEStatus = (int)CHEStatus.Compensado;
                                CHErow.CHE_CCD = CCD.Value;
                                CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - d�bito autom�tico\r\n    Data:{2:dd/MM/yyyy}",
                                                         (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                                         DateTime.Now,
                                                         Vencimento
                                                         );
                            }
                            break;
                        case PAGTipo.Caixinha:
                            CHErow.CHEStatus = (int)CHEStatus.Caixinha;
                            break;
                        case PAGTipo.GuiaEletronica:
                            CHErow.CHEStatus = (int)CHEStatus.Eletronico;
                            break;
                        case PAGTipo.cheque:
                        case PAGTipo.sindico:
                        case PAGTipo.boleto:
                        case PAGTipo.deposito:
                        case PAGTipo.Guia:
                        case PAGTipo.Honorarios:
                        case PAGTipo.Conta:
                        case PAGTipo.Acumular:
                        case PAGTipo.PECreditoConta:
                        case PAGTipo.PETransferencia:
                        case PAGTipo.PEOrdemPagamento:
                        case PAGTipo.PETituloBancario:
                        case PAGTipo.PETituloBancarioAgrupavel:
                        case PAGTipo.PEGuia:
                        case PAGTipo.TrafArrecadado:
                        case PAGTipo.ChequeSindico:
                        case PAGTipo.Folha:
                            if (bloqueado)
                                CHErow.CHEStatus = (int)CHEStatus.CadastradoBloqueado;
                            else
                                CHErow.CHEStatus = (int)CHEStatus.Cadastrado;
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Construtor Cheque: Tipo n�o implementado = {0}", Tipo));
                    }

                    //*** MRC - INICIO - PAG-FOR (2) ***
                    if (CCT.GetValueOrDefault(0) != 0)
                        CHErow.CHE_CCT = CCT.Value;
                    if (Adicional != string.Empty)
                        CHErow.CHEAdicional = Adicional;
                    //*** MRC - FIM - PAG-FOR (2) ***
                    CHErow.CHEDATAI = DateTime.Now;
                    CHErow.CHEFavorecido = Favorecido;
                    CHErow.CHEI_USU = DSCentral.USUX(EMPTProc1.Tipo);
                    CHErow.CHEValor = 0;
                    CHErow.CHEVencimento = Vencimento;
                    if (rowCON.IsCONMaloteNull())
                    {
                        VirException.cVirException VErro = new VirException.cVirException(string.Format("Erro no cadastro do condom�nio {0} - {1}\r\nMalote n�o definido", rowCON.CONCodigo, rowCON.CONNome));
                        VErro.Abortar = false;
                        VErro.AvisarUsuario = true;
                        VErro.Vircath = false;
                        throw VErro;
                    };
                    if (rowCON.IsCONProcuracaoNull())
                    {
                        VirException.cVirException VErro = new VirException.cVirException(string.Format("Erro no cadastro do condom�nio {0} - {1}\r\nUso de procura��o n�o definido", rowCON.CONCodigo, rowCON.CONNome));
                        VErro.Abortar = false;
                        VErro.AvisarUsuario = true;
                        VErro.Vircath = false;
                        throw VErro;
                    };
                    DateTime[] Datas = CalculaDatas(Tipo, rowCON.CONProcuracao, (malote.TiposMalote)rowCON.CONMalote, Vencimento, DateTime.Now, ref MaloteIda, ref MaloteVolta, out RetornaNaData);
                    CHErow.CHEIdaData = Datas[0];
                    CHErow.CHERetornoData = Datas[1];
                    dCheque.CHEques.AddCHEquesRow(CHErow);
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("UPDATE", true);
                    dCheque.CHEquesTableAdapter.Update(CHErow);
                    CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                    CHErow.AcceptChanges();
                };
                Encontrado = true;
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro gravando cheque: " + e.Message, e);
            }
        }

        #endregion





        //*** MRC - INICIO - PAG-FOR (2) ***    
        #region Registro

        [Obsolete("Ap�s o saneamentos das contas usar como condominio.Conta.CPF_CNPJ")]
        private DocBacarios.CPFCNPJ CNPJContaPE(int CON)
        {
            string Comando =
"SELECT CONCnpj \r\n" +
"FROM CONDOMINIOS \r\n" +
"WHERE  \r\n" +
"(\r\n" +
" ((SELECT CONTipoContaPE FROM CONDOMINIOS WHERE CON = @P1) IN (0)) AND (CON = @P1)\r\n" +
")\r\n" +
" OR \r\n" +
"(\r\n" +
" ((SELECT CONTipoContaPE FROM CONDOMINIOS WHERE CON = @P1) IN (1,2)) AND (CON = @P2)\r\n" +
")\r\n" +
";";
            int condominioPool = EMPTProc1.EMP == 1 ? 409 : 594;

            string strCNPJ = EMPTProc1.STTA.BuscaEscalar_string(Comando, CON, condominioPool);
            return new DocBacarios.CPFCNPJ(strCNPJ);
        }

        ABS_Condominio _condominio;

        private ABS_Condominio condominio
        {
            get => _condominio ?? (_condominio = ABS_Condominio.GetCondominio(CON));
        }

        string ComandoCorrecaoCCGFaltante =
"SELECT   PAG_CCG, NOA_CCTCred, NOA_FRN, CCT_CCG\r\n" +
"FROM         CHEques INNER JOIN\r\n" +
"             PAGamentos ON CHE = PAG_CHE INNER JOIN\r\n" +
"             NOtAs ON PAG_NOA = NOA LEFT OUTER JOIN\r\n" +
"             ContaCorrenTe ON NOA_CCTCred = CCT\r\n" +
"WHERE        (CHE = @P1);";

        /// <summary>
        /// Registra Cheque (Pagamento Eletr�nico)
        /// </summary>
        /// <param name="Remessa"></param>
        /// <param name="NomeArquivo"></param>
        /// <param name="Path"></param>
        /// <param name="SequencialRemessaArquivo"></param>
        /// <returns></returns>
        public bool Registrar(EDIBoletos.ChequeRemessaBra Remessa, out string NomeArquivo, string Path = "", int SequencialRemessaArquivo = 0)
        {
            //Inicia nome de arquivo (retorno)
            NomeArquivo = string.Empty;

            //Verifica se ja gerou a remessa
            bool SoRegistra = (Remessa != null);

            //Verifica se cria a remessa
            if (!SoRegistra)
            {
                Remessa = new EDIBoletos.ChequeRemessaBra();
                Remessa.IniciaRemessa();

                //Remessa.CodComunicacao = Convert.ToInt32(rowCON.CONCodigoComunicacao);
                Remessa.CodComunicacao = EMPTProc1.EMP == 1 ? 154134 : 3342;
                Remessa.ContaDebito = CCTrow.CCTConta.ToString().Length <= 7 ? Convert.ToInt32(CCTrow.CCTConta.ToString()) : Convert.ToInt32(CCTrow.CCTConta.ToString().Substring(0, 7));
                Remessa.NomeEmpresa = rowCON.CONNome;
                //Remessa.CPFCNPJ = condominio.Conta.CPF_CNPJ;
                Remessa.CPFCNPJ = CNPJContaPE(CHErow.CHE_CON);                
                Remessa.SequencialRemessaArquivo = SequencialRemessaArquivo;                
                Remessa.SequencialRemessa = EMPTProc1.STTA.BuscaEscalar_int("select (CASE WHEN BCOSequencialRemessaCheque = 99999 THEN 1 ELSE BCOSequencialRemessaCheque + 1 END) from BANCOS where BCO = @P1", 237).Value;                
            }

            //Inicia cheque
            //Cheque cheque = new Cheque(CHErow);
            PAGTipo Tipo = (PAGTipo)CHErow.CHETipo;
            dCheque.PAGamentosRow[] PAG = CHErow.GetPAGamentosRows();

            //Seta os dados comuns da remessa            
            if ((CHEStatus)CHErow.CHEStatus == CHEStatus.Cancelado || (CHEStatus)CHErow.CHEStatus == CHEStatus.PagamentoCancelado)
                TipoMovimento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposMovimento.Exclusao);
            else
            {
                TipoMovimento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposMovimento.Inclusao);
                CodMovimento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
            }            
            SituacaoAgendamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
            NumeroPagamento = CHErow.CHENumero;
            DataEmissao = CHErow.CHEEmissao;
            NumDocumento = PAG.Length > 0 ? PAG[0].NOANumero : 0;
            if (NumDocumento == 0)
                TipoDocumento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposDocumento.Fatura);
            else
                TipoDocumento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposDocumento.NotaFiscalFatura);
            EDIBoletos.ChequeRemessaBra.TiposContaFornecedor intTipoConta;            
            /*
            if (!CHErow.IsCHE_CCGNull())
            {
                ContaCorrenteNeon ContaDestino = new ContaCorrenteNeon(CHErow.CHE_CCG, ContaCorrenteNeon.TipoChave.CCG);
                Banco = ContaDestino.BCO;
                Agencia = ContaDestino.Agencia;
                DigAgencia = ContaDestino.AgenciaDg.ToString();
                Conta = ContaDestino.NumeroConta;
                DigConta = ContaDestino.NumeroDg;
                intTipoConta = (int)(ContaDestino.Tipo == CompontesBasicosProc.ContaCorrente.TipoConta.Poupanca ? EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaPoupanca : EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaCorrente));
            }*/

            Nome = CHErow.CHEFavorecido;
            if ((PAG.Length != 0) && (!PAG[0].IsFRNCnpjNull()))
                CPFCNPJ_Credito = new DocBacarios.CPFCNPJ(PAG[0].FRNCnpj);
            

            //Seta os dados espec�ficos da remessa
            if (Tipo == PAGTipo.PECreditoConta)
            {
                //Checar se � conta do fornecedor com dados incompletos e corrigir
                if ((CHErow.IsCHE_CCGNull()) && CHErow.IsCHEAdicionalNull() && PAG[0].IsFRNBancoCreditoNull())
                {
                    DataRow DR = EMPTProc1.STTA.BuscaSQLRow(ComandoCorrecaoCCGFaltante, CHE);
                    if(DR["PAG_CCG"] != DBNull.Value)
                        CHErow.CHE_CCG = (int)DR["PAG_CCG"];
                    else if (DR["CCT_CCG"] != DBNull.Value)
                        CHErow.CHE_CCG = (int)DR["CCT_CCG"];
                    else if (DR["NOA_CCTCred"] != DBNull.Value)
                    {
                        ABS_ContaCorrente ContaCorrente = ABS_ContaCorrente.GetContaCorrenteNeon((int)DR["NOA_CCTCred"], TipoChaveContaCorrenteNeon.CCT, true);
                        CHErow.CHE_CCG = ContaCorrente.CCG.Value;
                    }
                    else if (DR["NOA_FRN"] != DBNull.Value)
                    {
                        ABS_ContaCorrente ContaCorrente = ABS_ContaCorrente.GetContaCorrenteNeon((int)DR["NOA_FRN"], TipoChaveContaCorrenteNeon.FRN, true);
                        CHErow.CHE_CCG = ContaCorrente.CCG.Value;
                    }
                }


                //Verifica se credito em conta do pr�prio condom�nio ou de fornecedor e pega os respectivos dados
                if (CHErow.IsCHE_CCGNull())
                {
                    int intCCTDeposito = CHErow.IsCHEAdicionalNull() ? 0 : (!CHErow.CHEAdicional.Contains("CCT = ") ? 0 : Convert.ToInt32(CHErow.CHEAdicional.Replace("CCT = ", string.Empty)));
                    if (intCCTDeposito != 0)
                    {
                        //Conta do pr�prio condom�nio
                        //cheque.CPFCNPJ = new DocBacarios.CPFCNPJ(dCheque.DadosComplementaresTableAdapter.VerificaCONCnpjPE(CHErow.CHE_CON).ToString());
                        CPFCNPJ_Credito = CNPJContaPE(CHErow.CHE_CON);
                        dCheque.ContaCorrenTeDepositoTableAdapter.Fill(dCheque.ContaCorrenTeDeposito, intCCTDeposito);
                        Banco_Credito = Convert.ToInt32(dCheque.ContaCorrenTeDeposito[0].CCT_BCO);
                        Agencia_Credito = Convert.ToInt32(dCheque.ContaCorrenTeDeposito[0].CCTAgencia);
                        DigAgencia_Credito = dCheque.ContaCorrenTeDeposito[0].CCTAgenciaDg.ToString();
                        Conta_Credito = dCheque.ContaCorrenTeDeposito[0].CCTConta.ToString().Length <= 7 ? Convert.ToInt32(dCheque.ContaCorrenTeDeposito[0].CCTConta.ToString()) : Convert.ToInt32(dCheque.ContaCorrenTeDeposito[0].CCTConta.ToString().Substring(0, 7));
                        DigConta_Credito = dCheque.ContaCorrenTeDeposito[0].CCTContaDg.ToString();
                        intTipoConta = (dCheque.ContaCorrenTeDeposito[0].CCTTipo == (int)CCTTipo.Poupanca) ? EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaPoupanca : EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaCorrente;
                    }
                    else
                    {
                        //Conta de fornecedor
                        Banco_Credito = Convert.ToInt32(PAG[0].FRNBancoCredito);
                        Agencia_Credito = Convert.ToInt32(PAG[0].FRNAgenciaCredito);
                        DigAgencia_Credito = PAG[0].FRNAgenciaDgCredito;
                        Conta_Credito = Convert.ToInt32(PAG[0].FRNContaCredito);
                        DigConta_Credito = PAG[0].FRNContaDgCredito;
                        intTipoConta = (PAG[0].FRNContaTipoCredito == (int)FRNContaTipoCredito.Poupanca) ? EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaPoupanca : EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaCorrente;
                    }
                    TipoConta_Credito = (int)intTipoConta;
                }

                //Verifica se e banco Bradesco (transferencia entre contas) ou nao (doc ou ted)           
                if (Banco_Credito == 237)
                {
                    //Verifica se transferencia em dia posterior (transf. normal) ou no mesmo dia (transf. real time)                    
                    if ((CHErow.CHEVencimento > DateTime.Now.Date) ||
                        ((CHEStatus)CHErow.CHEStatus == CHEStatus.Cancelado || (CHEStatus)CHErow.CHEStatus == CHEStatus.PagamentoCancelado))                        
                        //Modalidade 1
                        ModalidadePagamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposModalidadePagamento.CreditoContaCorrentePoupanca);
                    else if (CHErow.CHEVencimento == DateTime.Now.Date)
                        //Modalidade 5 
                        ModalidadePagamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposModalidadePagamento.CreditoContaCorrenteRealTime);
                    else
                        return false;
                    //TipoConta_Credito = intTipoConta;
                }
                else
                {
                    //Verifica valor da transfer�ncia menor que 1000 (doc) ou nao (ted)
                    if (CHErow.CHEValor < 1000)
                        //Modalidade 3
                        ModalidadePagamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposModalidadePagamento.DOCCompe);
                    else
                        //Modalidade 8 
                        ModalidadePagamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposModalidadePagamento.TED);                    
                    TipoContaDOCTED = TipoConta_Credito;                    
                }                
                DataVencimento = CHErow.CHEVencimento;
                Valor = CHErow.CHEValor;
            }
            else if (Tipo == PAGTipo.PETransferencia)
            {
                if (CHErow.CHEValor < 1000)
                    //Modalidade 3
                    ModalidadePagamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposModalidadePagamento.DOCCompe);
                else
                    //Modalidade 8 
                    ModalidadePagamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposModalidadePagamento.TED);
                if (CHErow.IsCHE_CCGNull())
                {
                    Banco_Credito = Convert.ToInt32(PAG[0].FRNBancoCredito);
                    Agencia_Credito = Convert.ToInt32(PAG[0].FRNAgenciaCredito);
                    DigAgencia_Credito = PAG[0].FRNAgenciaDgCredito;
                    Conta_Credito = Convert.ToInt32(PAG[0].FRNContaCredito);
                    DigConta_Credito = PAG[0].FRNContaDgCredito;
                    TipoContaDOCTED = (PAG[0].FRNContaTipoCredito == (int)FRNContaTipoCredito.Poupanca) ? Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaPoupanca) : Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaCorrente); 
                }
                else
                    TipoContaDOCTED = TipoConta_Credito;

                DataVencimento = CHErow.CHEVencimento;
                Valor = CHErow.CHEValor;
            }
            else if (Tipo == PAGTipo.PEOrdemPagamento)
            {
                //Modalidade 2
                ModalidadePagamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposModalidadePagamento.ChequeOrdemPagamento);
                Endereco_Credito = PAG[0].IsFRNEnderecoNull() ? string.Empty : PAG[0].FRNEndereco;
                CEP = PAG[0].IsFRNCepNull() ? string.Empty : PAG[0].FRNCep;
                InstrucaoChequeOP = CHErow.IsCHEAdicionalNull() ? string.Empty : CHErow.CHEAdicional;
                Agencia_Credito = Convert.ToInt32(PAG[0].FRNAgenciaCredito); ;
                DigAgencia_Credito = PAG[0].FRNAgenciaDgCredito;                
                DataVencimento = CHErow.CHEVencimento;
                Valor = CHErow.CHEValor;
            }            
            else if (Tipo == PAGTipo.PETituloBancario || Tipo == PAGTipo.PETituloBancarioAgrupavel)            
            {
                //Modalidade 31
                ModalidadePagamento = Convert.ToInt32(EDIBoletos.ChequeRemessaBra.TiposModalidadePagamento.TitulosTerceiros);
                LinhaDigitavel = CHErow.CHEAdicional;
            }
            else
                return false;

            //Adiciona o cheque na remessa
            Remessa.Cheques.Add(this);

            //Verifica se gera o arquivo
            if (!SoRegistra)
                NomeArquivo = Remessa.GravaArquivo(Path);

            return true;
        }

        /// <summary>
        /// Altera o status de registro do Cheque
        /// </summary>
        /// <param name="intStatus"></param>
        /// <param name="strObservacao"></param>
        /// <returns></returns>
        public void AlteraStatus(CHEStatus intStatus, string strObservacao)
        {
            //Altera Status do Registro
            CHErow.CHEStatus = (int)intStatus;

            //Inclui observa��o
            CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - {2}",
                                          (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                          DateTime.Now,
                                          strObservacao
                                          );
            try
            {
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 683", dCheque.CHEquesTableAdapter);
                dCheque.CHEquesTableAdapter.Update(CHErow);
                EMPTProc1.Commit();
                CHErow.AcceptChanges();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro ao alterar status do cheque: " + e.Message, e);
            }
        }
        
        /// <summary>
        /// Altera o status de registro do Cheque setando dados adicionais para Pagamento Eletronico
        /// </summary>
        /// <param name="intStatus"></param>
        /// <param name="strObservacao"></param>
        /// <param name="datData">Data do Pagamento</param>
        /// <param name="decValor">Valor do Pagamento</param>
        /// <returns></returns>
        public void AlteraStatus(CHEStatus intStatus, string strObservacao, DateTime datData, Decimal decValor)
        {
            //Altera Status do Registro
            CHErow.CHEStatus = (int)intStatus;

            //Altera Data e Valor (Pagamento Eletronico)
            CHErow.CHEPEDataPagto = datData;
            CHErow.CHEPEValorPagto = decValor;

            //Inclui observa��o
            CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - {2}",
                                          (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                          DateTime.Now,
                                          strObservacao
                                          );
            try
            {
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 683", dCheque.CHEquesTableAdapter);
                dCheque.CHEquesTableAdapter.Update(CHErow);
                EMPTProc1.Commit();
                CHErow.AcceptChanges();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro ao alterar status do cheque: " + e.Message, e);
            }
        }
                
        /// <summary>
        /// Altera o status de cancelamento de Cheque referente a Pagamento Eletronico
        /// </summary>
        /// <param name="intStatus"></param>
        /// <param name="strObservacao"></param>
        public void AlteraPECancelamentoStatus(PECancelamentoStatus intStatus, string strObservacao)
        {
            //Altera Status de Cancelamento do Registro
            CHErow.CHEPEStatusCancelamento = (int)intStatus;

            //Inclui observa��o
            CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - {2}",
                                          (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                          DateTime.Now,
                                          strObservacao
                                          );
            try
            {
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 683", dCheque.CHEquesTableAdapter);
                dCheque.CHEquesTableAdapter.Update(CHErow);
                EMPTProc1.Commit();
                CHErow.AcceptChanges();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro ao alterar status do cheque: " + e.Message, e);
            }
        }


        #endregion

        #region Mascaras
        /*
                private static string MascaraCopiaDPL =
        "^02c0001\r" + //comprimento da etiquita setado para o m�nimo
        "^02m\r" +     //m�trico 
        "^02L\r" +     //inicia impresso
        "z\r" +        //n�o corta o zero
        "D11" +        //Tamanho do dot 1 x 1
        "%QuadroIDAVOLTA%" +        
        "A5\r" +       //Reverso
        "422200000000570%CONCodigo%\r" +
        "A1\r" +       //Cancela Reverso
        "4j0015000000780%CHE%\r" + //C�digo de barras
        "%DETALHES%" +
        "1X1100004910420b0390098200100010\r" + //Ret�ngulo
        "412200005100480%CHE_BCO%\r" +
        "412200006000480%CONAgencia%\r" +
        "412200007400480%CONConta%\r" +
        "412200009400480%CHENumero%\r" +
        "422200011300490%CHEValor%\r" +
        "411100005100550Port.\r" +
        "403300005800555%CHEFavorecido%\r" +
        "402200007300600%Local%, %CHEVencimento%\r" +
        "1X1100007300740l00050700\r" +
        "402200007300770%CONNome%\r" +
        "%PERIODICOS%" +
        "%QuadroImpostos%" +
        "Q0001\r" +          //Quantidade;
        "E\r";               //Imprime 

                private static string MascaraCopiaEPL =
        "\r\n" +
        "OC,Fr\r\n" +
        "Q20,0\r\n" +
        "q600\r\n" +
        "N\r\n" +
        "%QuadroIDAVOLTA%" +
        "A200,10,1,2,2,2,R,\"%CONCodigo%\"\r\n" +
        "B150,10,1,2C,1,4,100,N,\"%CHE%\"\r\n" +
        "%DETALHES%" +
        "X30,400,10,350,1200\r\n" +
        "A330,410,1,4,1,1,N,\"%CHE_BCO%\"\r\n" +
        "A330,500,1,4,1,1,N,\"%CONAgencia%\"\r\n" +
        "A330,640,1,4,1,1,N,\"%CONConta%\"\r\n" +
        "A330,800,1,4,1,1,N,\"%CHENumero%\"\r\n" +
        "A330,940,1,3,2,2,N,\"%CHEValor%\"\r\n" +
        "A263,420,1,1,1,1,N,\"Portador:\"\r\n" +
        "A270,510,1,4,1,1,N,\"%CHEFavorecido%\"\r\n" +
        "A220,610,1,4,1,1,N,\"%Local%, %CHEVencimento%\"\r\n" +
        "LO100,610,5,570\r\n" +
        "A90,610,1,2,1,1,N,\"%CONNome%\"\r\n" +
        "%PERIODICOS%" +
        "%QuadroImpostos%" +
        "P1\r\n";
        */
        //*** MRC - INICIO - PAG-FOR (22/07/2014 10:30) ***
        /*
        private static string MascaraCopiaPE =
"\r\n" +
"OC,Fr\r\n" +
"Q20,0\r\n" +
"q600\r\n" +
"N\r\n" +
"%QuadroIDAVOLTA%" +
"%DETALHES%" +
"X30,%COLBoxStart%,10,350,%COLBoxEnd%\r\n" +
"A330,%COL%,1,4,1,1,N,\"PAGAMENTO ELETRONICO %CHENumero%\"\r\n" +
"A290,%COL%,1,3,2,2,N,\" %CHEValor% - %CHEVencimento%\"\r\n" +
"A220,%COL%,1,3,1,1,N,\"Favor liberar pagamento atraves do site\"\r\n" +
"A180,%COL%,1,4,1,1,N,\"www.autorizacaoremota.com.br\"\r\n" +
"A120,%COL%,1,3,1,1,N,\"%CONNome%\"\r\n" +
"A90,%COL%,1,2,1,1,N,\"%CONCodigo%\"\r\n" +
"%PERIODICOS%" +
"%QuadroImpostos%" +
"P1\r\n";
        */
        //*** MRC - TERMINO - PAG-FOR (22/07/2014 10:30) ***

        private enum TipoQuadroIdaVolta { VaiAssinaVolta, VaiAssinaFica, Eletronico, ChequeSindico }

        //private string QuadroIDAVOLTA(bool sindico,bool Eletronico)
        private string QuadroIDAVOLTA(TipoQuadroIdaVolta Tipo)
        {
            string retorno = string.Empty;
            switch (Tipo)
            {
                case TipoQuadroIdaVolta.VaiAssinaVolta:
                    retorno += ImpCopia.CodTXT(0, 910, "Data de envio para assinatura", 2);
                    retorno += ImpCopia.CodTXT(0, 850, "%CHEIdaData%", 4);
                    retorno += ImpCopia.CodTXT(0, 800, "Data limite de retorno", 2);
                    retorno += ImpCopia.CodTXT(0, 685, "%CHERetornoData%", 8);
                    retorno += ImpCopia.CodTXT(0, 670, "Retorno efetivo", 2);
                    retorno += ImpCopia.CodTXT(0, 570, "__/ __/ ____", 7);
                    break;
                case TipoQuadroIdaVolta.VaiAssinaFica:
                    retorno += ImpCopia.CodTXT(0, 910, "Data de envio", 2);
                    retorno += ImpCopia.CodTXT(0, 850, "%CHEIdaData%", 4);
                    retorno += ImpCopia.CodTXT(0, 800, "Data para retorno da copia", 2);
                    retorno += ImpCopia.CodTXT(0, 685, "%CHERetornoData%", 8);
                    break;
                case TipoQuadroIdaVolta.ChequeSindico:
                    retorno += ImpCopia.CodTXT(0, 700, "CHEQUE EMITIDO", 5);
                    retorno += ImpCopia.CodTXT(0, 600, "PELO S�NDICO", 5);
                    break;
                case TipoQuadroIdaVolta.Eletronico:
                default:
                    break;
            }
            return retorno;

        }

        #endregion


        //private static string QuadroIDAVOLTAEPL =







        /*
        private static string QuadroIDAVOLTADPL =
"421100000000100Data de envio para assinatura\r" +
"403300000000150%CHEIdaData%\r" +
"421100000000200Data limite de retorno\r" +
"423300000000280%CHERetornoData%\r" +
"421100000000330Retorno Efetivo\r" +
"422200000000410__ /__ /____\r";
        
        private static string QuadroIDAVOLTAPagSindico
        {
            get
            {
                string retorno = "";
                retorno += ImpCopia.CodTXT(0, 910, "Data de envio", 2);
                retorno += ImpCopia.CodTXT(0, 850, "%CHEIdaData%", 4);
                retorno += ImpCopia.CodTXT(0, 800, "Data para retorno da copia", 2);
                retorno += ImpCopia.CodTXT(0, 700, "%CHERetornoData%", 8);
                return retorno;
            }
        }
        /*
        private static string QuadroIDAVOLTAPagSindicoEPL =
"A600,10,1,2,1,1,N,\"Data de envio\"\r\n" +
"A570,10,1,4,1,1,N,\"%CHEIdaData%\"\r\n" +
"A530,10,1,2,1,1,N,\"Data para retorno da copia\"\r\n" +
"A500,10,1,4,1,1,N,\"%CHERetornoData%\"\r\n";

        private static string QuadroIDAVOLTAPagSindicoDPL =
"421100000000100Data de envio\r" +
"403300000000150%CHEIdaData%\r" +
"421100000000200Data para retorno da copia\r" +
"423300000000280%CHERetornoData%\r";
        
        //*** MRC - INICIO - PAG-FOR (22/07/2014 10:30) ***
        private static string QuadroIDAVOLTAPE =
"A600,10,1,2,1,1,N,\"Data de envio para aprovacao\"\r\n" +
"A570,10,1,4,1,1,N,\"%CHEIdaData%\"\r\n" +
"A530,10,1,2,1,1,N,\"Data limite para libera��o\"\r\n" +
"A500,10,1,2,1,1,N,\"do pagamento no site\"\r\n" +
"A460,10,1,4,1,1,N,\"%CHERetornoData%\"\r\n" +
"A200,10,1,2,2,2,R,\"%CONCodigo%\"\r\n" +
"B150,10,1,2C,1,4,100,N,\"%CHE%\"\r\n";
        //*** MRC - TERMINO - PAG-FOR (22/07/2014 10:30) ***    
        
        private static string MascNotaDebito =
"Q20,0\r\n" +
"q831\r\n" +
"N\r\n" +
"S4\r\n" +
"OC\r\n" +
"Fr\r\n" +
"R70,0\r\n" +
"N\r\n" +
"A684,131,1,1,4,4,N,\"NOTA DE D�BITO / RECIBO\"\r\n" +
"A605,42,1,1,2,2,N,\"%NOME%\"\r\n" +
"A600,880,1,1,2,2,N,\"Data:%DATA%\"\r\n" +
"A573,42,1,1,2,2,N,\"%Endereco%\"\r\n" +
"A565,800,1,1,2,2,N,\"Bruto\"\r\n" +
"A565,1000,1,1,2,2,N,\"%BRUTO%\"\r\n" +
"A535,800,1,1,2,2,N,\"- Reten�ao\"\r\n" +
"A535,1000,1,1,2,2,N,\"%RETISS%\"\r\n" +
"A505,800,1,1,2,2,N,\"TOTAL liq\"\r\n" +
"A505,1000,1,1,2,2,R,\"%TOTAL%\"\r\n" +
            

"A541,42,1,1,2,2,N,\"CNPJ: %CNPJ%\"\r\n" +
            
"A469,20,1,1,3,3,R,\"                CLIENTE                \"\r\n" +
"A420,39,1,1,2,2,N,\"%NomeCLI%\"\r\n" +
"A387,39,1,1,2,2,N,\"%EnderecoCLI%\"\r\n" +
"A354,39,1,1,2,2,N,\"CNPJ:%CNPJCLI%\"\r\n" +
"A315,20,1,1,3,3,R,\"         DESCRI�AO         \"\r\n" +
"A316,860,1,1,3,3,R,\"   VALOR   \"\r\n" +
            
"%LINHAS%" +
            
"LO45,700,3,500\r\n" +
"A40,900,1,4,1,1,N,\"ass\"\r\n" +
"X0,0,8,710,1250\r\n" +
"P2\r\n";
        */






        //private impressos.impCopia _ImpCopia;
        /*
        private static LinguagemCodBar _LinguagemCB = LinguagemCodBar.Indefinida;

        /// <summary>
        /// 
        /// </summary>
        public static LinguagemCodBar LinguagemCB
        {
            get 
            {
                if (_LinguagemCB == LinguagemCodBar.Indefinida)
                {
                    try
                    {
                        _LinguagemCB = (LinguagemCodBar)Enum.Parse(typeof(CodBar.LinguagemCodBar), CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Linguagem codbar"));
                    }
                    catch
                    {
                        _LinguagemCB = CodBar.LinguagemCodBar.EPL2;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Linguagem codbar", LinguagemCB.ToString());
                    }  
                }
                return _LinguagemCB;
            }
        }
*/
        /*
        /// <summary>
        /// Impressora de c�pia de cheques
        /// </summary>
        public impressos.impCopia ImpCopia
        {
            get
            {
                if (_ImpCopia == null)
                {                                                          
                    _ImpCopia = new impressos.impCopia(LinguagemCB);
                    _ImpCopia.Reset();
                    string NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de c�pias de Cheque");
                    if (NomeImpressora == "")
                    {
                        PrintDialog PD = new PrintDialog();
                        PD.ShowDialog();
                        NomeImpressora = PD.PrinterSettings.PrinterName;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de c�pias de Cheque", NomeImpressora);
                    }
                    _ImpCopia.NomeImpressora = NomeImpressora;
                }
                return _ImpCopia;
            }
        }

        //*** MRC - INICIO - PAG-FOR (2) ***    
        //private static CodBar.Impress _ImpCopiaPE;

        /// <summary>
        /// Impressora de c�pia de pagamento eletronico
        /// </summary>
        public static CodBar.Impress ImpCopiaPE
        {
            get
            {
                if (_ImpCopiaPE == null)
                {
                    _ImpCopiaPE = new CodBar.Impress(MascaraCopiaPE);
                    _ImpCopiaPE.RemoverAcentos = true;
                    string NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de c�pias de Cheque");
                    if (NomeImpressora == "")
                    {
                        PrintDialog PD = new PrintDialog();
                        PD.ShowDialog();
                        NomeImpressora = PD.PrinterSettings.PrinterName;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de c�pias de Cheque", NomeImpressora);
                    }
                    _ImpCopiaPE.NomeImpressora = NomeImpressora;
                }
                return _ImpCopiaPE;
            }
        }
        //*** MRC - FIM - PAG-FOR (2) ***
        */

        #region Funcionalidades        

        /// <summary>
        /// Inclui pagamentos em um cheque
        /// </summary>
        /// <param name="PAGs"></param>
        /// <returns></returns>
        public bool IncluiPAGs(params int[] PAGs)
        {
            if (!Encontrado)
                throw new Exception("Cheque n�o encontrado");
            EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 13");
            try
            {
                foreach (int PAG in PAGs)
                    if (TableAdapter.ST().ExecutarSQLNonQuery("update PAGamentos set PAG_CHE = @P1 where PAG = @P2", CHErow.CHE, PAG) != 1)
                        throw new Exception("erro em IncluiPAGs");
                AjustarTotalPelasParcelas();
                EMPTProc1.Commit();
                return true;
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
            }
            return false;
        }

        /// <summary>
        /// Estorna o cheque
        /// </summary>
        public bool Estorna(int CCD)
        {
            switch (Status)
            {
                case CHEStatus.NaoEncontrado:
                    UltimaException = new Exception("Cheque n�o encontrado");
                    return false;
                case CHEStatus.Cancelado:
                case CHEStatus.PagamentoCancelado:
                case CHEStatus.PagamentoNaoEfetivadoPE:
                    UltimaException = new Exception("Cheque cancelado");
                    return false;
                case CHEStatus.Cadastrado:
                    UltimaException = new Exception("Cheque n�o emitido!!!");
                    return false;
                case CHEStatus.AguardaRetorno:
                case CHEStatus.Aguardacopia:
                case CHEStatus.Retornado:
                case CHEStatus.EnviadoBanco:
                case CHEStatus.Retirado:
                case CHEStatus.ComSindico:
                case CHEStatus.DebitoAutomatico:
                    UltimaException = new Exception("Cheque n�o compensado!!!");
                    return false;
                case CHEStatus.Caixinha:
                case CHEStatus.Eletronico:
                case CHEStatus.CompensadoAguardaCopia:
                case CHEStatus.BaixaManual:
                    UltimaException = new Exception("Tipo inv�lido!!!");
                    return false;

                case CHEStatus.Compensado:
                    if (Tipo != PAGTipo.DebitoAutomatico)
                    {
                        UltimaException = new Exception("Tipo inv�lido!!!");
                        return false;
                    }
                    if (CHErow.IsCHE_CCDNull())
                    {
                        UltimaException = new Exception("Tipo inv�lido!!!");
                        return false;
                    }
                    if (CHErow.CHE_CCD != CCD)
                    {
                        UltimaException = new Exception("CCD errado!!!");
                        return false;
                    }
                    Status = CHEStatus.DebitoAutomatico;
                    break;
                default:
                    return false;
            }

            UltimaException = null;

            try
            {


                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 1037", dCheque.CHEquesTableAdapter, dCheque.PAGamentosTableAdapter);
                CHErow.SetCHE_CCDNull();
                CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - Estorno\r\n",
                                         (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                         DateTime.Now
                                         );
                dCheque.CHEquesTableAdapter.Update(CHErow);
                CHErow.AcceptChanges();
                EMPTProc1.Commit();

            }
            catch (Exception e)
            {
                UltimaException = e;
                EMPTProc1.Vircatch(e);
                return false;
            }
            return true;
        }

        private bool? _CONDivideSaldos;

        private bool CONDivideSaldos
        {
            get
            {
                if (!_CONDivideSaldos.HasValue)
                    _CONDivideSaldos = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_bool("select CONDivideSaldos from condominios where CON = @P1", CON);
                return _CONDivideSaldos.GetValueOrDefault(false);
            }
        }

        /// <summary>
        /// Baixa o cheque
        /// </summary>
        /// <param name="CCD"></param>
        /// <param name="Data"></param>
        /// <param name="Categoria"></param>
        /// <param name="GravarNaCCD">Deve ser falso se a fun��o chamadora ja for gravar na tabela CCD</param>
        /// <returns></returns>
        public bool Baixar(int CCD, DateTime Data, int Categoria, bool GravarNaCCD = true)
        {
            switch (Status)
            {
                case CHEStatus.NaoEncontrado:
                    UltimaException = new Exception("Cheque n�o encontrado");
                    return false;
                case CHEStatus.Cancelado:
                case CHEStatus.PagamentoCancelado:
                case CHEStatus.PagamentoNaoEfetivadoPE:
                    UltimaException = new Exception("Cheque cancelado");
                    return false;
                case CHEStatus.Cadastrado:
                    UltimaException = new Exception("Cheque n�o emitido!!!");
                    return false;
                //case CHEStatus.AguardaEnvioPE:
                //    UltimaException = new Exception("Pagamento Eletr�nico n�o enviado para o Banco!!!");
                //    return false;
                //case CHEStatus.AguardaConfirmacaoBancoPE:
                //    UltimaException = new Exception("Pagamento Eletr�nico n�o confirmado pelo Banco!!!");
                //    return false;
                //case CHEStatus.PagamentoNaoAprovadoSindicoPE:
                //    UltimaException = new Exception("Pagamento Eletr�nico n�o aprovado pelo S�ndico!!!");
                //    return false;                
                case CHEStatus.PagamentoInconsistentePE:
                    UltimaException = new Exception("Pagamento Eletr�nico recusado pelo Banco!!!");
                    return false;
                case CHEStatus.AguardaRetorno:
                case CHEStatus.Aguardacopia:
                    Status = CHEStatus.CompensadoAguardaCopia;
                    break;
                case CHEStatus.Caixinha:
                case CHEStatus.DebitoAutomatico:
                    UltimaException = new Exception(string.Format("Tipo inv�lido: {0}!!!", Status));
                    return false;
                case CHEStatus.Eletronico:
                    //ATEN��O:
                    //O status eletronico significaria "cadastrado para enviar" e mudaria para "envido/agendado" se tiv�ssemos o retono. Neste caso daria erro de "Tipo inv�lido" se fosse feita tentativa de baixa
                    Status = CHEStatus.Compensado;
                    break;
                case CHEStatus.Retornado:
                case CHEStatus.EnviadoBanco:
                case CHEStatus.Retirado:
                case CHEStatus.ComSindico:
                case CHEStatus.PagamentoConfirmadoBancoPE:
                case CHEStatus.AguardaEnvioPE:
                case CHEStatus.AguardaConfirmacaoBancoPE:
                case CHEStatus.PagamentoNaoAprovadoSindicoPE:
                    Status = CHEStatus.Compensado;
                    break;
                case CHEStatus.Compensado:
                case CHEStatus.CompensadoAguardaCopia:
                case CHEStatus.BaixaManual:
                    UltimaException = new Exception("Cheque j� compensado");
                    return false;

                default:
                    break;
            }

            UltimaException = null;

            try
            {
                string comando =
"UPDATE    ContaCorrenteDetalhe\r\n" +
"SET              CCDConsolidado = 2, CCDTipoLancamento = 1\r\n" +
"WHERE     (CCD = @P1);";

                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 1114", dCheque.CHEquesTableAdapter, dCheque.PAGamentosTableAdapter);


                CHErow.CHE_CCD = CCD;
                string strTipoEmissao = "Cheque";
                PAGTipo Tipo = (PAGTipo)CHErow.CHETipo;
                if (Tipo.EstaNoGrupo(PAGTipo.PECreditoConta, PAGTipo.PEOrdemPagamento, PAGTipo.PETituloBancario, PAGTipo.PETituloBancarioAgrupavel, PAGTipo.PETransferencia))
                    strTipoEmissao = "Pagamento Eletr�nico";
                else if (Tipo == PAGTipo.Folha)
                    strTipoEmissao = "Folha de Pagamento";
                CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - {3} compensado\r\n    Data:{2:dd/MM/yyyy}",
                                         (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                         DateTime.Now,
                                         Data,
                                         strTipoEmissao
                                         );

                dCheque.CHEquesTableAdapter.Update(CHErow);
                CHErow.AcceptChanges();
                /*
                if (!CONDivideSaldos)
                {
                    dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHErow.CHE);
                    foreach (dCheque.PAGamentosRow rowPAG in dCheque.PAGamentos)
                    {
                        dCheque.Lancamentos_BradescoRow novaLBrow = dCheque.Lancamentos_Bradesco.NewLancamentos_BradescoRow();
                        novaLBrow.Codcon = rowCON.CONCodigo;
                        novaLBrow.Valor_Pago = (float)rowPAG.PAGValor;
                        novaLBrow.Cr�dito = false;
                        novaLBrow.C�digo_Categoria = Categoria.ToString("000");
                        novaLBrow.Data_de_Leitura = DateTime.Today;
                        novaLBrow.Data_de_Lan�amento = Data;
                        novaLBrow.Tipopag = rowPAG.NOA_PLA;
                        novaLBrow.CCD = CCD;
                        string ManDescricao;
                        if (rowPAG.NOA_PLA == "213000")
                            ManDescricao = string.Empty;
                        else
                            ManDescricao = String.Format("{0} - ", Framework.datasets.dPLAnocontas.dPLAnocontasSt.GetDescricao(rowPAG.NOA_PLA));
                        ManDescricao += rowPAG.NOAServico;
                        if (ManDescricao.Length > 50)
                            ManDescricao = ManDescricao.Substring(0, 50);
                        novaLBrow.Descri��o = ManDescricao;
                        novaLBrow.M�s_Compet�ncia = CHErow.CHEVencimento.ToString("MMyyyy");
                        if (CHErow.IsCHENumeroNull())
                            novaLBrow.N�mero_Documento = 0;
                        else
                            novaLBrow.N�mero_Documento = (double)CHErow.CHENumero;
                        dCheque.Lancamentos_Bradesco.AddLancamentos_BradescoRow(novaLBrow);
                        dCheque.Lancamentos_BradescoTableAdapter.Update(novaLBrow);
                        novaLBrow.AcceptChanges();

                    };
                };*/
                //if (!CHErow.IsCHEantigoNull())
                //    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("UPDATE cheques SET Datacaiu = @P1, Pago = 1 WHERE (cheques.ContadorCheque = @P2)",Data, CHErow.CHEantigo);
                if (UltimaException != null)
                    throw UltimaException;
                if (GravarNaCCD)
                    TableAdapter.ST().ExecutarSQLNonQuery(comando, CCD);
                EMPTProc1.Commit();

            }
            catch (Exception e)
            {
                UltimaException = e;
                EMPTProc1.Vircatch(e);
                return false;
            }
            return true;
        }


        /// <summary>
        /// /// Marca o cheque como emitido gravando os dados necess�rios
        /// </summary>
        /// <param name="NumeroCheque">N�mero do cheque</param>
        /// <param name="BCO">Banco</param>
        /// <param name="AG">Agencia</param>
        /// <param name="Conta">Conta</param>
        /// <param name="CCT">CCT</param>
        /// <returns></returns>
        public bool Emite(int NumeroCheque, int BCO, int AG, int Conta, int CCT = 0)
        {
            //int CCT;
            //refer�ncia ONENOTE:fluxo1 
            if (CCT != 0)
                return Emite(NumeroCheque, CCT);
            else if (TableAdapter.ST().BuscaEscalar("SELECT CCT FROM ContaCorrenTe WHERE (CCTAplicacao = 0) AND (CCT_BCO = @P1) AND (CCTAgencia = @P2) AND (CCTConta = @P3)", out CCT, BCO, AG, Conta) && (CCT != 0))
                return Emite(NumeroCheque, CCT);
            else
            {
                TableAdapter.ST().ExecutarSQLNonQuery(Resources.ComandoNovaCCTValue, BCO, AG, Conta);
                if (TableAdapter.ST().BuscaEscalar("SELECT CCT FROM ContaCorrenTe WHERE (CCT_BCO = @P1) AND (CCTAgencia = @P2) AND (CCTConta = @P3)", out CCT, BCO, AG, Conta) && (CCT != 0))
                    return Emite(NumeroCheque, CCT);
                else
                    return false;
            }
        }

        private bool GeraGuiaUnificada()
        {
            try
            {
                if (rowPAGs.Length == 0)
                {
                    if (dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHE) == 0)
                        return false;
                    else rowPAGs = CHErow.GetPAGamentosRows();

                }
                TipoImposto TipoImp = (TipoImposto)rowPAGs[0].PAGIMP;
                if (TipoImp.EstaNoGrupo(TipoImposto.INSSpfEmp, TipoImposto.INSSpfRet))
                    TipoImp = TipoImposto.INSSpf;
                ImpostoNeon Imp = new ImpostoNeon(TipoImp);
                string[] CamposTestar = Imp.GuiaDoPrestador ? new string[] { "PAGIMP", "FRNCnpj", "FRNNome", "FRNCep", "FRNEndereco" } : new string[] { "PAGIMP" };
                foreach (dCheque.PAGamentosRow rowPAG in CHErow.GetPAGamentosRows())
                {
                    if (rowPAG.IsPAGIMPNull() || (PAGTipo)rowPAG.PAGTipo != PAGTipo.PEGuia)
                        throw new Exception("erro no cheque. Tipo do item diferente do tipo do cheque");
                    foreach (string CampoCompara in CamposTestar)
                        if (!rowPAG[CampoCompara].Equals(rowPAGs[0][CampoCompara]))
                            return false;
                    Imp.ValorImposto += rowPAG.PAGValor;
                }
                Imp.VencimentoEfetivo = CHErow.CHEVencimento;
                CPFCNPJ CNPJCodomino = new CPFCNPJ(rowCON.CONCnpj);

                Imp.cpfcnpjTomador = CNPJCodomino;
                if (!rowPAGs[0].IsFRNCnpjNull())
                    Imp.cpfcnpjPrestador = new CPFCNPJ(rowPAGs[0].FRNCnpj);
                Imp.NomeTomador = rowCON.CONNome;
                if (!rowPAGs[0].IsFRNNomeNull())
                    Imp.NomePrestador = rowPAGs[0].FRNNome;
                dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();
                novalinha.GPSTipo = (int)TipoImp;
                novalinha.GPS_CON = CHErow.CHE_CON;
                novalinha.GPSNomeClientePagador = rowCON.CONNome;
                novalinha.GPSCNPJ = CNPJCodomino.Valor;
                novalinha.GPSAgencia = int.Parse(rowCON.CONAgencia);
                novalinha.GPSDigAgencia = rowCON.CONDigitoAgencia;
                novalinha.GPSConta = int.Parse(rowCON.CONConta);
                novalinha.GPSDigConta = rowCON.CONDigitoConta;
                novalinha.GPSCodComunicacao = "117045";
                string CEP = "0";
                if (Imp.GuiaDoPrestador)
                {
                    novalinha.GPSIdentificador = Imp.cpfcnpjPrestador.Valor;
                    novalinha.GPSNomeIdentificador = Imp.NomePrestador;
                    foreach (char c in rowPAGs[0].FRNCep)
                        if (char.IsDigit(c))
                            CEP += c;
                    novalinha.GPSCEP = int.Parse(CEP);
                    novalinha.GPSEndereco = rowPAGs[0].FRNEndereco;
                    novalinha.GPSUF = string.Empty;
                    novalinha.GPSCidade = string.Empty;
                    novalinha.GPSBairro = string.Empty;
                }
                else
                {
                    novalinha.GPSIdentificador = Imp.cpfcnpjTomador.Valor;
                    novalinha.GPSNomeIdentificador = Imp.NomeTomador;
                    foreach (char c in rowCON.CONCep)
                        if (char.IsDigit(c))
                            CEP += c;
                    novalinha.GPSCEP = int.Parse(CEP);
                    novalinha.GPSEndereco = rowCON.CONEndereco;
                    novalinha.GPSUF = rowCON.CIDUf;
                    novalinha.GPSCidade = (rowCON.CIDNome.Length > 20) ? rowCON.CIDNome.Substring(0, 20) : rowCON.CIDNome;
                    novalinha.GPSBairro = rowCON.CONBairro;
                }
                if (Imp.Guia == guias.DARF)
                    novalinha.GPSApuracao = Imp.Apuracao;
                else
                {
                    novalinha.GPSMes = Imp.Apuracao.Month;
                    novalinha.GPSAno = Imp.Apuracao.Year;
                }
                novalinha.GPSVencimento = Imp.Vencimento;
                novalinha.GPSDataPagto = Imp.VencimentoEfetivo;
                novalinha.GPSValorJuros = 0;
                novalinha.GPSValorMulta = 0;
                novalinha.GPSOutras = 0;
                novalinha.GPSValorPrincipal = Imp.ValorImposto;
                novalinha.GPSValorTotal = Imp.ValorImposto;
                novalinha.GPSCodigoReceita = int.Parse(Imp.Codigo);
                novalinha.GPSStatus = (int)StatusArquivo.Aguardando_Envio;
                novalinha.GPSOrigem = (int)OrigemImposto.Nota;
                novalinha.GPSI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                novalinha.GPSDATAI = DateTime.Now;
                dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
                dGPS.dGPSSt.GPSTableAdapter.Update(dGPS.dGPSSt.GPS);

                foreach (dCheque.PAGamentosRow rowPAG in CHErow.GetPAGamentosRows())
                {
                    //Seta no pagamento a gps associada e o status do mesmo
                    dCheque.PAGamentosTableAdapter.SetaGPS(novalinha.GPS, CHErow.CHEStatus, rowPAG.PAG);
                    dCheque.PAGamentos.AcceptChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(ex);
                return false;
            }
        }

        /// <summary>
        /// Marca o cheque como emitido gravando os dados necess�rios
        /// </summary>
        /// <returns></returns>
        public bool Emite(int NumeroCheque, int CCT)
        {
            string strTipoEmissao = "Cheque";
            dCheque.FolhasCHequeRow rowFCH = null;
            if (dCheque.FolhasCHequeTableAdapter.Fill(dCheque.FolhasCHeque, NumeroCheque, CCT) == 1)
                rowFCH = dCheque.FolhasCHeque[0];
            ;

            DateTime[] Datas = CalculaDatas((PAGTipo)CHErow.CHETipo, rowCON.CONProcuracao, (malote.TiposMalote)rowCON.CONMalote, CHErow.CHEVencimento, DateTime.Now, ref MaloteIda, ref MaloteVolta, out RetornaNaData);
            if (!((PAGTipo)CHErow.CHETipo).EstaNoGrupo(PAGTipo.ChequeSindico))
                if (!rowCON.CONProcuracao || ((PAGTipo)CHErow.CHETipo).EstaNoGrupo(PAGTipo.sindico))
                {
                    MaloteIda = new malote(CHErow.CHE_CON, DateTime.Today);
                    while (MaloteIda.DataEnvio < DateTime.Today)
                        MaloteIda++;
                }
            CHErow.CHE_CCT = CCT;
            //obs
            //CHErow.CHEA_USU
            //CHErow.CHECancelado = false;
            //CHErow.CHEDATAA
            CHErow.CHEEmissao = DateTime.Now;
            if (MaloteIda != null)
            {
                CHErow.CHEIda_MAL = MaloteIda.rowMAL.MAL;
                CHErow.CHEIdaData = MaloteIda.DataMalote;
                if (MaloteVolta.DataRetorno <= MaloteIda.DataRetorno)
                {
                    MaloteVolta = MaloteIda;
                    MaloteVolta++;
                    CHErow.CHERetornoData = MaloteVolta.DataRetorno;
                }
            };
            //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***    
            //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
            if (isEletronico)
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                CHErow.CHENumero = CHErow.CHE;
            else
                CHErow.CHENumero = NumeroCheque;
            //*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) ***    
            bool obsnaoretorna = false;
            //Casos em que nao tem copia (salario etc)
            if (!ImprimeCopia())
                //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
                //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                if (isEletronico)
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    CHErow.CHEStatus = (int)CHEStatus.AguardaEnvioPE;
                else
                {
                    CHErow.CHEStatus = (int)CHEStatus.Retornado;
                    obsnaoretorna = true;
                }
            //*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) ***
            else
                if (rowCON.CONProcuracao)
                //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) *** 
                //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                if (isEletronico)
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    CHErow.CHEStatus = (int)CHEStatus.AguardaEnvioPE;
                else
                {
                    CHErow.CHEStatus = (int)CHEStatus.Retornado;
                    obsnaoretorna = true;
                }
            //*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) ***
            else
                switch (Tipo)
                {
                    case PAGTipo.sindico:
                        strTipoEmissao = "Cheque emitido";
                        CHErow.CHEStatus = (int)CHEStatus.Aguardacopia;
                        obsnaoretorna = true;
                        break;
                    case PAGTipo.cheque:
                    case PAGTipo.boleto:
                    case PAGTipo.deposito:
                    case PAGTipo.Guia:
                    case PAGTipo.Honorarios:
                    case PAGTipo.GuiaEletronica:
                    case PAGTipo.Caixinha:
                    case PAGTipo.DebitoAutomatico:
                    case PAGTipo.Conta:
                    case PAGTipo.Acumular:
                    case PAGTipo.TrafArrecadado:
                        strTipoEmissao = "Cheque emitido";
                        CHErow.CHEStatus = (int)CHEStatus.AguardaRetorno;
                        break;
                    case PAGTipo.PECreditoConta:
                    case PAGTipo.PETransferencia:
                    case PAGTipo.PEOrdemPagamento:
                    case PAGTipo.PETituloBancario:
                    case PAGTipo.PETituloBancarioAgrupavel:
                        strTipoEmissao = "Pagamento Eletr�nico emitido";
                        CHErow.CHEStatus = (int)CHEStatus.AguardaEnvioPE;
                        break;
                    //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    case PAGTipo.PEGuia:
                        strTipoEmissao = "Tributo Eletr�nico emitido";
                        CHErow.CHEStatus = (int)CHEStatus.AguardaEnvioPE;
                        break;
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    case PAGTipo.ChequeSindico:
                        strTipoEmissao = "Cheque s�ndico cadastrado";
                        CHErow.CHEStatus = (int)CHEStatus.Retornado;
                        obsnaoretorna = true;
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Tipo {0} n�o tratado em cheque.cs - 2059", Tipo));
                }
            string historicoFolha = rowFCH == null ? string.Empty : string.Format("Hist�rico da folha de cheque:\r\n{0}--------------------------\r\n", rowFCH.FCHHistorico);

            CHErow.CHEObs = String.Format("{10}{0}{1:dd/MM/yy HH:mm:ss} - {11} por {2}\r\n    {3} {4}-{5} {6}-{7} {8:000000}{9}",
                                          (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                          DateTime.Now,
                                          CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
                                          CCTrow.CCT_BCO,
                                          CCTrow.CCTAgencia,
                                          CCTrow.CCTAgenciaDg,
                                          CCTrow.CCTConta,
                                          CCTrow.CCTContaDg,
                                          CHErow.CHENumero,
                                          obsnaoretorna ? "\r\nCheque emitido com status Retornado" : string.Empty,
                                          historicoFolha,
                                          strTipoEmissao
                                          );
            CHErow.SetCHERetorno_MALNull();
            try
            {
                //Abre a transacao
                //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 1279", dCheque.CHEquesTableAdapter, dCheque.FolhasCHequeTableAdapter, dCheque.PAGamentosTableAdapter, dGPS.dGPSSt.GPSTableAdapter);
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***

                //Salva dados do cheque
                if (dCheque.CHEquesTableAdapter.Update(CHErow) < 1)
                    //VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(new Exception("N�o Gravou"));
                    throw new Exception("N�o Gravou");
                
                CHErow.AcceptChanges();

                //Verifica se cheque referente a Guia de Recolhimento Eletr�nico e insere as guias na tabela GPS 
                switch (Tipo)
                {
                    case PAGTipo.PEGuia:
                        if (!GeraGuiaUnificada())
                        {
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("Unifica��o de guia n�o foi poss�vel. CHE = {0}", CHE), "UNIFICA�C�O de guia");
                            foreach (dCheque.PAGamentosRow rowPAG in CHErow.GetPAGamentosRows())
                            {
                                ImpostoNeon Imp = null;
                                if ((PAGTipo)rowPAG.PAGTipo == PAGTipo.PEGuia)
                                    if (!rowPAG.IsPAGIMPNull())
                                    {
                                        //Seta imposto
                                        TipoImposto TipoImp = (TipoImposto)rowPAG.PAGIMP;
                                        if ((TipoImp == TipoImposto.INSSpfEmp) || (TipoImp == TipoImposto.INSSpfRet))
                                            TipoImp = TipoImposto.INSSpf;
                                        Imp = new ImpostoNeon(TipoImp);
                                        Imp.VencimentoEfetivo = CHErow.CHEVencimento;
                                        if (!rowCON.IsCONCnpjNull())
                                            Imp.cpfcnpjTomador = new DocBacarios.CPFCNPJ(rowCON.CONCnpj);
                                        if (!rowPAG.IsFRNCnpjNull())
                                            Imp.cpfcnpjPrestador = new DocBacarios.CPFCNPJ(rowPAG.FRNCnpj);
                                        Imp.NomeTomador = rowCON.CONNome;
                                        if (!rowPAG.IsFRNNomeNull())
                                            Imp.NomePrestador = rowPAG.FRNNome;
                                        Imp.ValorImposto += rowPAG.PAGValor;

                                        //Insere na tabela GPS
                                        dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();
                                        novalinha.GPSTipo = rowPAG.PAGIMP;
                                        novalinha.GPS_CON = CHErow.CHE_CON;
                                        //Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = Framework.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(CHErow.CHE_CON);
                                        DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(rowCON.CONCnpj);
                                        novalinha.GPSNomeClientePagador = rowCON.CONNome;
                                        novalinha.GPSCNPJ = Cnpj.Valor;
                                        novalinha.GPSAgencia = int.Parse(rowCON.CONAgencia);
                                        novalinha.GPSDigAgencia = rowCON.CONDigitoAgencia;
                                        novalinha.GPSConta = int.Parse(rowCON.CONConta);
                                        novalinha.GPSDigConta = rowCON.CONDigitoConta;
                                        novalinha.GPSCodComunicacao = "117045";
                                        string CEP = "0";
                                        if (Imp.GuiaDoPrestador)
                                        {
                                            novalinha.GPSIdentificador = Imp.cpfcnpjPrestador.Valor;
                                            novalinha.GPSNomeIdentificador = Imp.NomePrestador;
                                            foreach (char c in rowPAG.FRNCep)
                                                if (char.IsDigit(c))
                                                    CEP += c;
                                            novalinha.GPSCEP = int.Parse(CEP);
                                            novalinha.GPSEndereco = rowPAG.FRNEndereco;
                                            novalinha.GPSUF = string.Empty;
                                            novalinha.GPSCidade = string.Empty;
                                            novalinha.GPSBairro = string.Empty;
                                        }
                                        else
                                        {
                                            novalinha.GPSIdentificador = Imp.cpfcnpjTomador.Valor;
                                            novalinha.GPSNomeIdentificador = Imp.NomeTomador;
                                            foreach (char c in rowCON.CONCep)
                                                if (char.IsDigit(c))
                                                    CEP += c;
                                            novalinha.GPSCEP = int.Parse(CEP);
                                            novalinha.GPSEndereco = rowCON.CONEndereco;
                                            novalinha.GPSUF = rowCON.CIDUf;
                                            novalinha.GPSCidade = (rowCON.CIDNome.Length > 20) ? rowCON.CIDNome.Substring(0, 20) : rowCON.CIDNome;
                                            novalinha.GPSBairro = rowCON.CONBairro;
                                        }
                                        if (Imp.Guia == guias.DARF)
                                            novalinha.GPSApuracao = Imp.Apuracao;
                                        else
                                        {
                                            novalinha.GPSMes = Imp.Apuracao.Month;
                                            novalinha.GPSAno = Imp.Apuracao.Year;
                                        }
                                        novalinha.GPSVencimento = Imp.Vencimento;
                                        novalinha.GPSDataPagto = Imp.VencimentoEfetivo;
                                        novalinha.GPSValorJuros = 0;
                                        novalinha.GPSValorMulta = 0;
                                        novalinha.GPSOutras = 0;
                                        novalinha.GPSValorPrincipal = Imp.ValorImposto;
                                        novalinha.GPSValorTotal = Imp.ValorImposto;
                                        novalinha.GPSCodigoReceita = int.Parse(Imp.Codigo);
                                        novalinha.GPSStatus = (int)StatusArquivo.Aguardando_Envio;
                                        novalinha.GPSOrigem = (int)OrigemImposto.Nota;
                                        novalinha.GPSI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                                        novalinha.GPSDATAI = DateTime.Now;
                                        dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
                                        dGPS.dGPSSt.GPSTableAdapter.Update(dGPS.dGPSSt.GPS);
                                        dGPS.dGPSSt.GPS.AcceptChanges();

                                        //Seta no pagamento a gps associada e o status do mesmo
                                        dCheque.PAGamentosTableAdapter.SetaGPS(novalinha.GPS, CHErow.CHEStatus, rowPAG.PAG);
                                        dCheque.PAGamentos.AcceptChanges();
                                    }
                            }
                        }
                        break;
                    case PAGTipo.PEContaConsumo:
                        foreach (dCheque.PAGamentosRow rowPAG in CHErow.GetPAGamentosRows())
                        {
                            if ((PAGTipo)rowPAG.PAGTipo == PAGTipo.PEContaConsumo)
                            {
                                //Insere na tabela GPS
                                dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();
                                novalinha.GPSTipo = (int)TipoImposto.CONTA_CONSUMO;
                                novalinha.GPS_CON = CHErow.CHE_CON;
                                DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(rowCON.CONCnpj);
                                novalinha.GPSNomeClientePagador = rowCON.CONNome;
                                novalinha.GPSCNPJ = Cnpj.Valor;
                                novalinha.GPSAgencia = int.Parse(rowCON.CONAgencia);
                                novalinha.GPSDigAgencia = rowCON.CONDigitoAgencia;
                                novalinha.GPSConta = int.Parse(rowCON.CONConta);
                                novalinha.GPSDigConta = rowCON.CONDigitoConta;
                                novalinha.GPSCodComunicacao = "117045";
                                //Competencia comp = new Competencia(rowPAG.noacompetencia);
                                //novalinha.GPSMes = comp.Mes;
                                //novalinha.GPSAno = comp.Ano;
                                //novalinha.GPSVencimento = 
                                novalinha.GPSDataPagto = CHErow.CHEVencimento;
                                //novalinha.GPSCEP = int.Parse(rowCON.CONCep.Replace("-", ""));
                                //novalinha.GPSEndereco = rowCON.CONEndereco;
                                //novalinha.GPSUF = rowCON.CIDUf;
                                //novalinha.GPSCidade = (rowCON.CIDNome.Length > 20) ? rowCON.CIDNome.Substring(0, 20) : rowCON.CIDNome;
                                //novalinha.GPSBairro = rowCON.CONBairro;
                                novalinha.GPSValorJuros = 0;
                                novalinha.GPSValorMulta = 0;
                                novalinha.GPSOutras = 0;
                                novalinha.GPSValorPrincipal = rowPAG.PAGValor;
                                novalinha.GPSValorTotal = rowPAG.PAGValor;
                                //novalinha.GPSCodigoBarra = rowPAG.PAGDadosAdicionai
                                //novalinha.GPSCodigoReceita = Convert.ToInt32(novarow.CodigoBarra.Substring(16, 4));
                                novalinha.GPSIdentificador = Cnpj.Valor;
                                novalinha.GPSNomeIdentificador = rowCON.CONNome;
                                novalinha.GPSStatus = (int)StatusArquivo.Aguardando_Envio;
                                novalinha.GPSOrigem = (int)OrigemImposto.Nota;                               
                                novalinha.GPSI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                                novalinha.GPSDATAI = DateTime.Now;
                                dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
                                dGPS.dGPSSt.GPSTableAdapter.Update(dGPS.dGPSSt.GPS);
                                dGPS.dGPSSt.GPS.AcceptChanges();

                                //Seta no pagamento a gps associada e o status do mesmo
                                dCheque.PAGamentosTableAdapter.SetaGPS(novalinha.GPS, CHErow.CHEStatus, rowPAG.PAG);
                                dCheque.PAGamentos.AcceptChanges();
                            }
                        }
                        break;
                }
                                                    
                //Atualiza folha de cheque
                if (rowFCH != null)
                {
                    rowFCH.Delete();
                    dCheque.FolhasCHequeTableAdapter.Update(dCheque.FolhasCHeque);
                }                
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                UltimaException = e;
                EMPTProc1.Vircatch(e);
                throw new Exception("Recriada: Erro na grava��o da emiss�o: " + e.Message, e);
            };
            return true;
        }

        /// <summary>
        /// Este tipo de cheque precisa de c�pia?
        /// </summary>
        /// <returns></returns>
        public bool ImprimeCopia()
        {
            if (Framework.DSCentral.EMP == 3)
                return true;
            ArrayList SemCopia = new ArrayList(new String[] { "220001", "220002", "220017" });
            bool Retorno = false;
            Array.ForEach(CHErow.GetPAGamentosRows(), delegate (dCheque.PAGamentosRow PAGrow)
            {
                if (!SemCopia.Contains(PAGrow.NOA_PLA))
                    Retorno = true;
            });
            return Retorno;


        }

        private decimal Retencao;



        private string GeraDet(string[] L, decimal PAGValor, bool Eletronico)
        {
            string retorno = string.Empty;
            int alturalinha = 35;
            int fonteValor = Eletronico ? 2 : 5;

            int YBaseEstimado = ImpCopia.YTopoDet - alturalinha;
            for (int j = 1; j < L.Length; j++)
            {
                if (L[j] == null)
                    L[j] = string.Empty;
                if ((L[j].Trim() != string.Empty) && (L[j].Trim().ToUpper() != L[j - 1].Trim().ToUpper()))
                    YBaseEstimado -= alturalinha;
            }

            if (ImpCopia.YMinimoDet + 20 > YBaseEstimado)
            {
                ImpCopia.Coluna++;
                ImpCopia.YTopoDet = ImpCopia.YTopoDet0;
                ImpCopia.YMinimoDet = 0;
                retorno += ImpCopia.CodLinha(ImpCopia.XrefColuna0 + 10 + ImpCopia.Coluna * ImpCopia.LarguraColuna, 0, 1, 1000);
            }

            retorno += ImpCopia.CodTXT(ImpCopia.XrefColuna0 + 50 + ImpCopia.Coluna * ImpCopia.LarguraColuna, ImpCopia.YTopoDet, L[0], 3);
            ImpCopia.YTopoDet -= alturalinha;
            retorno += ImpCopia.CodTXT(ImpCopia.XrefColuna0 + (ImpCopia.Coluna + 1) * ImpCopia.LarguraColuna - 10, ImpCopia.YTopoDet, String.Format("{0:n2}", PAGValor), fonteValor, Impress.Orientacao.normal, false, 1, 1, Impress.Justificado.direita);

            for (int j = 1; j < L.Length; j++)
                if ((L[j].Trim() != string.Empty) && (L[j].Trim().ToUpper() != L[j - 1].Trim().ToUpper()))
                {
                    retorno += ImpCopia.CodTXT(ImpCopia.XrefColuna0 + 80 + ImpCopia.Coluna * ImpCopia.LarguraColuna, ImpCopia.YTopoDet, L[j], 2);
                    ImpCopia.YTopoDet -= alturalinha;
                }


            ImpCopia.YTopoDet -= (alturalinha + 10); //20
            return retorno;
        }

        private impressos.impCopia ImpCopia;

        /// <summary>
        /// Imprime copia de cheque e as guias no caso de imposto
        /// </summary>
        /// <param name="SegundaVia"></param>
        /// <returns></returns>
        public bool ImprimirCopia(bool SegundaVia)
        {
            if (Tipo.EstaNoGrupo(PAGTipo.GuiaEletronica,
                                PAGTipo.Caixinha,
                                PAGTipo.DebitoAutomatico,
                                PAGTipo.Acumular,
                                //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                                PAGTipo.PEGuia))
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***                            
                return false;
            Retencao = 0;
            if (!Encontrado)
                return false;
            if (SegundaVia)
                GravaObs("Impressa segunda via da c�pia");
            string Local1 = Framework.DSCentral.EMP == 1 ? "Sao Bernardo" : "Santo Andre";
            string strDet = string.Empty;
            string Conta;
            SortedList<int, ABS_PagamentoPeriodico> PGFs = new SortedList<int, ABS_PagamentoPeriodico>();
            SortedList<string, ImpostoNeon> Guias = new SortedList<string, ImpostoNeon>();
            bool Eletronico = dCheque.PAGTiposEletronicos.Contains(Tipo);
            ImpCopia = new impressos.impCopia(Eletronico);
            ImpCopia.Reset();
            if (rowCON.CONProcuracao || Eletronico)
                ImpCopia.SetCampo("%QuadroIDAVOLTA%", string.Empty);
            else
            {
                TipoQuadroIdaVolta TipoQ = TipoQuadroIdaVolta.VaiAssinaVolta;
                switch (Tipo)
                {
                    case PAGTipo.cheque:
                    case PAGTipo.boleto:
                    case PAGTipo.deposito:
                    case PAGTipo.Guia:
                    case PAGTipo.Honorarios:
                    case PAGTipo.Conta:
                    case PAGTipo.TrafArrecadado:
                        TipoQ = TipoQuadroIdaVolta.VaiAssinaVolta;
                        break;
                    case PAGTipo.sindico:
                        TipoQ = TipoQuadroIdaVolta.VaiAssinaFica;
                        break;
                    case PAGTipo.PECreditoConta:
                    case PAGTipo.PETransferencia:
                    case PAGTipo.PEOrdemPagamento:
                    case PAGTipo.PETituloBancario:
                    case PAGTipo.PETituloBancarioAgrupavel:
                        TipoQ = TipoQuadroIdaVolta.Eletronico;
                        break;
                    case PAGTipo.ChequeSindico:
                        TipoQ = TipoQuadroIdaVolta.ChequeSindico;
                        break;
                    case PAGTipo.GuiaEletronica:
                    case PAGTipo.Caixinha:
                    case PAGTipo.DebitoAutomatico:
                    case PAGTipo.Acumular:
                    //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    case PAGTipo.PEGuia:
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***                            
                    default:
                        throw new NotImplementedException(string.Format("Tipo n�o tratado em cheque.cs 2253 : {0}", Tipo));
                }
                ImpCopia.SetCampo("%QuadroIDAVOLTA%", QuadroIDAVOLTA(TipoQ));
                ImpCopia.SetCampo("%CHEIdaData%", CHErow.CHEIdaData.ToString("dd/MM/yyyy"));
                if (TipoQ.EstaNoGrupo(TipoQuadroIdaVolta.VaiAssinaVolta, TipoQuadroIdaVolta.VaiAssinaFica))
                {
                    CalculaDatas((PAGTipo)CHErow.CHETipo, rowCON.CONProcuracao, (malote.TiposMalote)rowCON.CONMalote, CHErow.CHEVencimento, CHErow.CHEEmissao, ref MaloteIda, ref MaloteVolta, out RetornaNaData);
                    ImpCopia.SetCampo("%CHERetornoData%", MaloteVolta.DataMalote.ToString("dd/MM/yyyy"));
                }
            }

            ImpCopia.SetCampo("%CONCodigo%", rowCON.CONCodigo);
            if (Tipo == PAGTipo.ChequeSindico)
                ImpCopia.SetCampo("%CHE%", string.Empty);
            else
                if (!SegundaVia)
                ImpCopia.SetCampo("%CHE%", String.Format("101{0:0000000000}", CHErow.CHE));
            else
                ImpCopia.SetCampo("%CHE%", String.Format("191{0:0000000000}", CHErow.CHE));
            ImpCopia.SetCampo("%CHE_BCO%", CCTrow.CCT_BCO.ToString("000"));
            ImpCopia.SetCampo("%CONAgencia%", String.Format("{0}-{1}", CCTrow.CCTAgencia, CCTrow.CCTAgenciaDg));
            ImpCopia.SetCampo("%CONConta%", String.Format("{0}-{1}", CCTrow.CCTConta, CCTrow.CCTContaDg));
            //ImpCopia.SetCampo("%CHENumero%", CHErow.CHENumero.ToString("000000"));
            ImpCopia.SetCampoCHENumero(CHErow.CHENumero);
            ImpCopia.SetCampo("%CHEValor%", CHErow.CHEValor.ToString("n2").PadLeft(9));
            ImpCopia.SetCampo("%CHEFavorecido%", CHErow.CHEFavorecido);
            ImpCopia.SetCampoCHEValor_CHEVencimento(CHErow.CHEValor, CHErow.CHEVencimento);
            ImpCopia.SetCampo("%Local%", Local1);
            string FormatoData = Eletronico ? "dd/MM/yyyy" : "d' de 'MMMM' de 'yyyy";
            ImpCopia.SetCampo("%CHEVencimento%", CHErow.CHEVencimento.ToString(FormatoData));
            ImpCopia.SetCampo("%CONNome%", rowCON.CONNome);

            foreach (dCheque.PAGamentosRow rowDet in CHErow.GetPAGamentosRows())
            {

                dllImpostoNeon.ImpostoNeon Imp = null;

                if ((PAGTipo)rowDet.PAGTipo == PAGTipo.Guia)

                    if (!rowDet.IsPAGIMPNull())
                    {
                        TipoImposto TipoImp = (TipoImposto)rowDet.PAGIMP;
                        if ((TipoImp == TipoImposto.INSSpfEmp) || (TipoImp == TipoImposto.INSSpfRet))
                            TipoImp = TipoImposto.INSSpf;
                        string ChaveGuia = TipoImp.ToString();
                        if (dllImpostoNeon.ImpostoNeon.Guia_Nome_Prestador(TipoImp))
                            ChaveGuia += rowDet.FRNCnpj;
                        if (!Guias.ContainsKey(ChaveGuia))
                        {
                            Imp = new ImpostoNeon(TipoImp);
                            Imp.DataPagamento = CHErow.CHEVencimento;
                            Imp.VencimentoEfetivo = CHErow.CHEVencimento;
                            Imp.DataNota = rowDet.NOADataEmissao;
                            if (!rowCON.IsCONCnpjNull())
                                Imp.cpfcnpjTomador = new DocBacarios.CPFCNPJ(rowCON.CONCnpj);
                            if (!rowDet.IsFRNCnpjNull())
                                Imp.cpfcnpjPrestador = new DocBacarios.CPFCNPJ(rowDet.FRNCnpj);
                            Imp.NomeTomador = rowCON.CONNome;
                            if (!rowDet.IsFRNNomeNull())
                                Imp.NomePrestador = rowDet.FRNNome;
                            Imp.Telefone = string.Format("{0} - {1}", EMPFone, rowCON.CONCodigo);
                            Guias.Add(ChaveGuia, Imp);
                        }
                        else
                            Imp = Guias[ChaveGuia];
                        Imp.ValorImposto += rowDet.PAGValor;
                        Imp.NFs += string.Format("NF:{0} ", rowDet.NOANumero);
                    }
;

                string[] L = new string[5];
                L[0] = rowDet.IsFRNCnpjNull() ? string.Empty : (rowDet.FRNCnpj + " ") + rowDet.FRNFantasia;
                string NdeN = (rowDet.IsPAGNNull() ? string.Empty : " " + rowDet.PAGN);
                if (rowDet.NOA_PLA == "213000")
                    Conta = string.Empty;
                else
                    Conta = Framework.datasets.dPLAnocontas.dPLAnocontasSt.GetDescricao(rowDet.NOA_PLA);
                L[1] = (rowDet.NOANumero == 0) ? string.Empty : "NF:" + rowDet.NOANumero.ToString() + NdeN + " ";
                if (Imp != null)
                {
                    L[2] = string.Format("{0}", dllImpostos.Imposto.DescRedImp((TipoImposto)rowDet.PAGIMP));
                    L[3] = string.Format("{0}", rowDet.NOAServico);
                }
                else
                {
                    L[2] = Conta;
                    L[3] = rowDet.NOAServico;
                };
                if (!rowDet.IsNOA_PGFNull())
                    if (!PGFs.ContainsKey(rowDet.NOA_PGF))
                    {
                        ABS_PagamentoPeriodico periodico = ABS_PagamentoPeriodico.GetPagamentoPeriodico(rowDet.NOA_PGF);
                        PGFs.Add(rowDet.NOA_PGF, periodico);
                        L[4] = periodico.LinhaDaCopia();
                    }
                strDet += GeraDet(L, rowDet.PAGValor, Eletronico);

                /*
                if (i == Limite)
                {
                    i = 1;
                    Limite = 9;
                    Coluna++;
                }*/

            }

            //*** MRC - INICIO - PAG-FOR (22/07/2014 10:30) ***                
            ImpCopia.SetCampo("%QuadroImpostos%", GeraQuadroImpostos(ImpCopia.Coluna + 1, TipoCopia.ChequePapel));
            //*** MRC - TERMINO - PAG-FOR (22/07/2014 10:30) ***                
            ImpCopia.SetCampo("%DETALHES%", strDet);
            //ImpCopia.SetCampo("%PERIODICOS%", strPeriodicos);
            if (!ImpCopia.ImprimeDOC())
                return false;

            foreach (ImpostoNeon Imp in Guias.Values)
            {
                if ((Imp.Tipo == TipoImposto.INSSpf) && (Imp.Codigo == "2100"))
                {
                    bool? sefip = TableAdapter.ST().BuscaEscalar_bool("select CONSefip from CONDOMINIOS where con = @P1", CHErow.CHE_CON);
                    if (sefip.GetValueOrDefault(false))
                        continue;
                }
                Imp.Imprime();
            }

            //*** MRC - INICIO - PAGAMENTO ELETRONICO (15/06/2015 11:30) ***
            //Verifica se e PECreditoConta de honorario para imprimir nota debito
            bool booPECreditoContaHonorario = false;
            if ((PAGTipo)CHErow.CHETipo == PAGTipo.PECreditoConta)
            {
                string BuscaNOAHonorario = "SELECT DISTINCT NOA\r\n" +
                                           "  FROM PAGamentos INNER JOIN\r\n" +
                                           "  NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA\r\n" +
                                           "WHERE (PAGamentos.PAG_CHE = @P1) AND (NOtAs.NOADefaultPAGTipo = @P2);";
                System.Data.DataTable T = TableAdapter.ST().BuscaSQLTabela(BuscaNOAHonorario, CHErow.CHE, PAGTipo.Honorarios);
                if ((T != null) && (T.Rows.Count > 0))
                    booPECreditoContaHonorario = true;
            }
            if ((PAGTipo)CHErow.CHETipo == PAGTipo.Honorarios || booPECreditoContaHonorario)
                ImprimeNotaDebito(Retencao);
            //*** MRC - TERMINO - PAGAMENTO ELETRONICO (15/06/2015 11:30) ***

            //foreach (object OPagamentoPeriodico in PGFs)
            foreach (ABS_PagamentoPeriodico periodico in PGFs.Values)
            {
                periodico.Imprimir(false);
                periodico.CadastrarProximo();
            }


            return true;
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        override protected void verificaPGF()
        {
            foreach (dCheque.PAGamentosRow rowDet in CHErow.GetPAGamentosRows())
            {
                if (!rowDet.IsNOA_PGFNull())
                {
                    object OPagamentoPeriodico = CompontesBasicos.ModuleInfo.ContruaObjeto("ContaPagar.Follow.PagamentoPeriodico", rowDet.NOA_PGF);
                    System.Reflection.MethodInfo MFCadastrarProximo = OPagamentoPeriodico.GetType().GetMethod("CadastrarProximo");
                    MFCadastrarProximo.Invoke(OPagamentoPeriodico, null);
                }
            }

        }
        */

        private impressos.impNotaDeb ImpNotaDeb;

        /*
        private impressos.impNotaDeb ImpNotaDeb
        {
            get 
            {
                if (_ImpNotaDeb == null)               
                    _ImpNotaDeb = new impressos.impNotaDeb(LinguagemCB);                                                        
                return _ImpNotaDeb;
            }
        }*/



        private void ImprimeNotaDebito(decimal RetISS)
        {

            string BuscaDadosADV =
"SELECT DISTINCT FORNECEDORES.FRNCnpj, FORNECEDORES.FRNNome, FORNECEDORES.FRNEndereco, FORNECEDORES.FRNFone1\r\n" +
"FROM         PAGamentos INNER JOIN\r\n" +
"                      NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA INNER JOIN\r\n" +
"                      FORNECEDORES ON NOtAs.NOA_FRN = FORNECEDORES.FRN\r\n" +
"WHERE     (PAGamentos.PAG_CHE = @P1);";
            System.Data.DataTable T = TableAdapter.ST().BuscaSQLTabela(BuscaDadosADV, CHErow.CHE);
            if ((T == null) || (T.Rows.Count == 0))
                return;
            System.Data.DataRow DRADV = T.Rows[0];
            ImpNotaDeb = new impressos.impNotaDeb();
            ImpNotaDeb.Reset();
            ImpNotaDeb.SetCampo("%NOME%", CHErow.CHEFavorecido);
            ImpNotaDeb.SetCampoData(CHErow.CHEVencimento, CHErow.CHEValor + RetISS, RetISS, CHErow.CHEValor);
            ImpNotaDeb.SetCampo("%Endereco%", DRADV["FRNEndereco"].ToString());
            //ImpNotaDeb.SetCampo("%BRUTO%", string.Format("{0:n2}",CHErow.CHEValor + RetISS).PadLeft(10));
            //ImpNotaDeb.SetCampo("%RETISS%", string.Format("{0:n2}", RetISS).PadLeft(10));
            //ImpNotaDeb.SetCampo("%TOTAL%",string.Format("{0:n2}",CHErow.CHEValor).PadLeft(10));
            //ImpNotaDeb.SetCampo("%Telefone%", DRADV["FRNFone1"].ToString());
            ImpNotaDeb.SetCampo("%CNPJ%", DRADV["FRNCNPJ"].ToString());
            ImpNotaDeb.SetCampo("%EnderecoCLI%", string.Empty);
            ImpNotaDeb.SetCampo("%NomeCLI%", rowCON.CONNome);
            ImpNotaDeb.SetCampo("%CNPJCLI%", rowCON.CONCnpj);
            //"TelefoneCLI
            //string LinhasRec = "";
            //int i = 0;
            ImpNotaDeb.SetCampoLinhas(CHErow.GetPAGamentosRows());
            /*
            foreach (dCheque.PAGamentosRow rowPAG in CHErow.GetPAGamentosRows())
            {
                LinhasRec += string.Format("A{0},25,1,1,2,2,N,\"{1}\"\r\n", 275 - i * 24, rowPAG.NOAServico);
                //LinhasRec += string.Format("A{0},860,1,1,2,2,N,\"{1}\"\r\n", 275 - i * 24, rowPAG.PAGValor.ToString("n2").PadLeft(9));
                LinhasRec += string.Format("A{0},860,1,1,2,2,N,\"{1}\"\r\n", 275 - i * 24, rowPAG.NOATotal.ToString("n2").PadLeft(9));
                i++;
            }
            ImpNotaDeb.SetCampo("%LINHAS%", LinhasRec);
            */
            //"");
            //%H1%                         
            //MessageBox.Show("imprimindo");
            ImpNotaDeb.ImprimeDOC();
        }

        private string _EMPFone = string.Empty;

        string EMPFone
        {
            get
            {
                if (_EMPFone == string.Empty)
                    TableAdapter.ST().BuscaEscalar("select EMPFone from EMPRESAS where EMP = @P1", out _EMPFone, rowCON.CON_EMP);
                return _EMPFone;
            }
        }

        private enum TipoCopia { ChequePapel, ChequeEletronico };

        /*
        static int LarguraEsquerda { get { return Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 400 : 491; } }

        static int LarguraColunaPapel { get { return Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 850 : 1000; } }

        static int LarguraColunaEletronico { get { return Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 600 : 706; } }

        static int DeltaYImposto { get { return Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? -120 : 0; } }

        static int YTituloImposto { get { return Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 505 : 0; } }

        static int YTValor { get { return Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 463 : 0; } }

        static int YTVencimento { get { return Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 443 : 0; } }

        static int YRecolher { get { return Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 423 : 0; } }

        */

        //private string GeraQuadroImpostos(int Coluna, int LenColuna = 850, int LenRecuo = 0) 
        private string GeraQuadroImpostos(int nColunaUsadas, TipoCopia Tipo)
        {
            string retorno = string.Empty;
            int? NF = null;
            int nNF = -1;
            //int PontoZero = LarguraEsquerda + nColunaUsadas * (Tipo == TipoCopia.ChequePapel ? LarguraColunaPapel : LarguraColunaEletronico);
            int PontoZero = ImpCopia.XrefColuna0 + 30 + ImpCopia.LarguraColuna * nColunaUsadas;

            //int Ponto1 = PontoZero + Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 50 : 59;
            //int Ponto2 = PontoZero + Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 100 : 110;
            //int Ponto3 = PontoZero + Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 300 : 205;
            //string MascaraLinha = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? "LO21,{0},579,4\r\n" : "";
            //string MascaraTitulo = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? "A573,{0},1,4,1,1,N,\"{1}\"\r\n" : "";
            //string MascaraTituloImposto = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? "A{0},{1},1,1,2,2,N,\"{2}\"\r\n" : "";
            //string MascaraValorT = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? "A{0},{0},1,3,1,1,N,\"Valor:\"\r\n" : "";
            //string MascaraValorV = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? "A{0},{1},1,3,1,1,N,\"{2:n2}\"\r\n" : "";
            //string MascaraVencimentoT = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? "A{0},{1},1,3,1,1,N,\"Vencimento:\"\r\n" : "";
            //string MascaraVencimentoV = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? "A{0},{1},1,3,1,1,N,\"{2:dd/MM/yyyy}\"\r\n" : "";
            //string MascaraRecolher = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? "A{0},{1},1,3,1,1,N,\"{2}\"\r\n" : "";
            //int deltaNF = Cheque.LinguagemCB == LinguagemCodBar.EPL2 ? 390 : 459;

            int deltaNF = 450;
            if (dCheque.ImpostosRetidosTableAdapter.FillAcumulado(dCheque.ImpostosRetidos, CHErow.CHE) == 0)
                return string.Empty;
            else
            {
                foreach (dllChequesProc.dCheque.ImpostosRetidosRow rowImposto in dCheque.ImpostosRetidos)
                {
                    if (!NF.HasValue || (rowImposto.NOANumero != NF.Value))
                    {
                        nNF++;
                        NF = rowImposto.NOANumero;
                        string Titulo = (NF.Value == 0) ? "IMPOSTOS" : string.Format("IMPOSTOS NF:{0}", NF);
                        //retorno += string.Format(MascaraLinha, PontoZero + nNF * deltaNF);
                        retorno += ImpCopia.CodLinha(PontoZero + nNF * deltaNF, 0, 1, 1000);
                        //retorno += string.Format(MascaraTitulo, Ponto1 + nNF * deltaNF, Titulo);                        
                        retorno += ImpCopia.CodTXT(PontoZero + 30 + nNF * deltaNF, 750, Titulo, 4);
                    }
                    if (dllImpostos.Imposto.Desconta((TipoImposto)rowImposto.PAGIMP))
                        Retencao += rowImposto.PAGValor;
                    string TituloImposto;
                    int nDetas;
                    switch ((TipoImposto)rowImposto.PAGIMP)
                    {
                        case TipoImposto.ISSQN:
                        case TipoImposto.ISS_SA:
                            TituloImposto = "ISS";
                            nDetas = 0;
                            break;
                        case TipoImposto.INSS:
                        case TipoImposto.INSSpf:
                            TituloImposto = "INSS";
                            nDetas = 1;
                            break;
                        case TipoImposto.INSSpfEmp:
                            TituloImposto = "INSS - Tomador";
                            nDetas = 1;
                            break;
                        case TipoImposto.PIS_COFINS_CSLL:
                            TituloImposto = "PIS/COFINS/CSLL";
                            nDetas = 2;
                            break;
                        case TipoImposto.INSSpfRet:
                            TituloImposto = "INSS - Prestador";
                            nDetas = 2;
                            break;
                        case TipoImposto.IR:
                        case TipoImposto.IRPF:
                            TituloImposto = "IR";
                            nDetas = 3;
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Imposto n�o implementado: rowImposto.PAGIMP = {0}", rowImposto.PAGIMP));
                    }
                    retorno += ImpCopia.CodTXT(PontoZero + 30 + nNF * deltaNF, 690 - nDetas * 150, TituloImposto, 2);
                    //retorno += string.Format(MascaraTituloImposto, YTituloImposto + nDetas * DeltaYImposto, Ponto1, TituloImposto);
                    retorno += ImpCopia.CodTXT(PontoZero + 60 + nNF * deltaNF, 650 - nDetas * 150, "Valor:", 2);
                    //retorno += string.Format(MascaraValorT, YTValor + nDetas * DeltaYImposto, Ponto2);
                    retorno += ImpCopia.CodTXT(PontoZero + 270 + nNF * deltaNF, 650 - nDetas * 150, rowImposto.PAGValor.ToString("n2"), 2);
                    //retorno += string.Format(MascaraValorV, YTValor + nDetas * DeltaYImposto, Ponto3, rowImposto.PAGValor);
                    retorno += ImpCopia.CodTXT(PontoZero + 60 + nNF * deltaNF, 620 - nDetas * 150, "Vencimento:", 2);
                    //retorno += string.Format(MascaraValorT, YTVencimento + nDetas * DeltaYImposto,Ponto2);
                    retorno += ImpCopia.CodTXT(PontoZero + 270 + nNF * deltaNF, 620 - nDetas * 150, rowImposto.PAGVencimento.ToString("dd/MM/yyyy"), 2);
                    //retorno += string.Format(MascaraValorV, YTVencimento + nDetas * DeltaYImposto, Ponto3, rowImposto.PAGVencimento);
                    retorno += ImpCopia.CodTXT(PontoZero + 60 + nNF * deltaNF, 590 - nDetas * 150, rowImposto.IsCHENumeroNull() ? "Recolher" : "Cheque:" + rowImposto.CHENumero.ToString(), 2);
                    //retorno += string.Format(MascaraRecolher,YRecolher + nDetas * DeltaYImposto, Ponto2, rowImposto.IsCHENumeroNull() ? "Recolher" : "Cheque:" + rowImposto.CHENumero.ToString());
                }
                return retorno;
            }
        }
        //*** MRC - TERMINO - PAG-FOR (22/07/2014 10:30) ***  
        //*** MRC - TERMINO - PAG-FOR (23/07/2014 10:00) ***                

        /// <summary>
        /// Inclui observa��o no cheque. Colaca a data faz a grava��o em transa��o
        /// </summary>
        /// <param name="Obs"></param>
        public void GravaObs(string Obs)
        {
            CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - {2}:\r\n{3}",
                                          (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                          DateTime.Now,
                                          CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
                                          Obs
                                          );

            try
            {
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 2120", dCheque.CHEquesTableAdapter);
                dCheque.CHEquesTableAdapter.Update(CHErow);
                EMPTProc1.Commit();
                CHErow.AcceptChanges();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Recriada: Erro no retorno", e);
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Obs"></param>
        public void CancelarEmissao(string Obs)
        {
            Status = CHEStatus.Cancelado;
            CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - Cancelada a emiss�o por {2}\r\nJustificativa: {3}",
                                  (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                  DateTime.Now,
                                  CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
                                  Obs
                                  );

            
            //Seta status de cancelamento em caso de pagamento eletr�nico para envio do cancelamento via processo autom�tico (apenas se foi emitido)
            PAGTipo Tipo = (PAGTipo)CHErow.CHETipo;            
            if (
                (!CHErow.IsCHENumeroNull()) 
                   &&
                (Tipo.EstaNoGrupo(PAGTipo.PECreditoConta, PAGTipo.PEOrdemPagamento, PAGTipo.PETituloBancario, PAGTipo.PETituloBancarioAgrupavel, PAGTipo.PETransferencia))
               )                
                CHErow.CHEPEStatusCancelamento = (int)PECancelamentoStatus.AguardaEnvioCancelamentoPE;
            

            try
            {                
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 1703", dCheque.CHEquesTableAdapter, dGPS.dGPSSt.GPSTableAdapter);                
                dCheque.CHEquesTableAdapter.Update(CHErow);
                CHErow.AcceptChanges();                
                int intCCT = 0;                
                if (Tipo.EstaNoGrupo(PAGTipo.PECreditoConta, PAGTipo.PEOrdemPagamento, PAGTipo.PETituloBancario, PAGTipo.PETituloBancarioAgrupavel, PAGTipo.PETransferencia))                    
                    intCCT = CHErow.CHE_CCT;
                string strAdicional = string.Empty;
                if (!CHErow.IsCHEAdicionalNull())
                    strAdicional = CHErow.CHEAdicional;
                //no caso de um reagendamento de um cr�dito em conta ele vira cheque
                bool TrocarTipo = (Tipo == PAGTipo.Folha);
                if (TrocarTipo)                
                    Tipo = PAGTipo.ChequeSindico;
                
                Cheque NovoCheque = new Cheque(CHErow.CHEVencimento, CHErow.CHEFavorecido, CHErow.CHE_CON, Tipo, true, 0, intCCT, strAdicional);
                //*** MRC - TERMINO - PAG-FOR (29/04/2014 15:25) ***
                System.Data.DataTable DT = TableAdapter.ST().BuscaSQLTabela("select * from PAGamentos where PAG_CHE = @P1", CHErow.CHE);
                foreach (System.Data.DataRow DR in DT.Rows)
                {                    
                    //Seta envio de exclusao de tributo para o banco se agendado
                    if (Tipo == PAGTipo.PEGuia && DR["PAG_GPS"] != DBNull.Value)
                    {
                        dGPS.dGPSSt.GPSTableAdapter.FillByGPS(dGPS.dGPSSt.GPS, (int)DR["PAG_GPS"]);
                        if (dGPS.dGPSSt.GPS.Count > 0)
                        {
                            dGPS.GPSRow rowGPS = dGPS.dGPSSt.GPS[0];
                            if (Colecoes.StatusArquivoCancelaveisBanco.Contains((StatusArquivo)rowGPS.GPSStatus))
                            {
                                rowGPS.GPSComentario = (rowGPS.IsGPSComentarioNull() ? string.Empty : rowGPS.GPSComentario) + String.Format("{0:dd/MM/yy HH:mm:ss} - Exclus�o no banco programada por {2} - Motivo: {1}\r\n", DateTime.Now, Obs, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome);
                                rowGPS.GPSStatus = (int)StatusArquivo.Aguardando_Envio_Exclusao;
                                dGPS.dGPSSt.GPSTableAdapter.Update(rowGPS);
                            }
                        }
                    }
                    DR["PAGStatus"] = NovoCheque.CHErow.CHEStatus;
                    DR["PAG_GPS"] = DBNull.Value;
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                    DR["PAG_CHE"] = NovoCheque.CHErow.CHE;
                    DR["PAG"] = DBNull.Value;
                    if (TrocarTipo)
                        DR["PAGTipo"] = (int)PAGTipo.ChequeSindico;
                    TableAdapter.ST().Insert("PAGamentos", DR, false, false);
                }
                NovoCheque.AjustarTotalPelasParcelas();
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                UltimaException = e;
                EMPTProc1.Vircatch(e);
                throw new VirException.cVirException("Erro cancelar emiss�o: " + e.Message, e);
            };
        }



        private bool Salvar()
        {

            try
            {
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 1756", dCheque.CHEquesTableAdapter);
                if (dCheque.CHEquesTableAdapter.Update(CHErow) < 1)
                {
                    VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(new Exception("Retorno=0"));
                    return true;
                }
                CHErow.AcceptChanges();
                EMPTProc1.Commit();
                return true;
            }
            catch (Exception e)
            {
                UltimaException = e;
                EMPTProc1.Vircatch(e);
                throw new Exception("Recriada: Erro no retorno", e);
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Obs"></param>
        public void CancelarCheque(string Obs)
        {
            Status = CHEStatus.Cancelado;
            CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - Cancelado o cheque por {2}\r\nJustificativa: {3}",
                                  (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                  DateTime.Now,
                                  CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
                                  Obs
                                  );
            Salvar();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SomenteCopia"></param>
        /// <param name="ComSegundaVia"></param>
        /// <returns></returns>
        public bool Retornar(bool SomenteCopia, bool ComSegundaVia)
        {
            bool retorno = false;
            switch (Status)
            {
                case CHEStatus.NaoEncontrado:
                case CHEStatus.Cancelado:
                case CHEStatus.Cadastrado:
                case CHEStatus.Retornado:
                case CHEStatus.EnviadoBanco:
                case CHEStatus.Retirado:
                case CHEStatus.Compensado:
                case CHEStatus.ComSindico:
                case CHEStatus.PagamentoNaoEfetivadoPE:
                    break;
                case CHEStatus.AguardaRetorno:
                case CHEStatus.Aguardacopia:
                    string Comentario = string.Empty;
                    if ((Status == CHEStatus.AguardaRetorno) && (SomenteCopia))
                        Comentario = "\r\nATEN��O: O cheque estava programado para retornar mas s� veio a c�pia.";
                    if ((Status == CHEStatus.Aguardacopia) && (!SomenteCopia))
                        Comentario = "\r\nATEN��O: O cheque estava programado para ficar com o s�ndico por�m retornou para a Neon.";
                    Status = SomenteCopia ? CHEStatus.ComSindico : CHEStatus.Retornado;
                    CHErow.CHERetornoData = DateTime.Now;
                    //CHErow.CHERetorno_MAL = new malote()

                    CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - {5} por {2}{3}{4}",
                                          (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                          CHErow.CHERetornoData,
                                          CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
                                          ComSegundaVia ? " (segunda via)" : string.Empty,
                                          Comentario,
                                          SomenteCopia ? "C�PIA retornada" : "Cheque retornado"
                                          );

                    try
                    {
                        EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 1824", dCheque.CHEquesTableAdapter);
                        if (dCheque.CHEquesTableAdapter.Update(CHErow) < 1)
                        {
                            TableAdapter.ST().RollBack("Concorr�ncia");
                            CHErow.RejectChanges();
                            return false;
                        }
                        CHErow.AcceptChanges();
                        EMPTProc1.Commit();
                        retorno = true;
                    }
                    catch (Exception e)
                    {
                        retorno = false;
                        UltimaException = e;
                        EMPTProc1.Vircatch(e);
                        throw new Exception("Recriada: Erro no retorno", e);
                    };

                    break;

                //*** MRC - INICIO - PAG-FOR (2) ***    
                case CHEStatus.AguardaEnvioPE:
                case CHEStatus.AguardaConfirmacaoBancoPE:
                case CHEStatus.PagamentoConfirmadoBancoPE:
                //*** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***    
                case CHEStatus.PagamentoNaoAprovadoSindicoPE:
                //*** MRC - TERMINO - PAG-FOR (29/04/2014 15:25) ***
                //*** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***
                case CHEStatus.PagamentoInconsistentePE:
                    //*** MRC - TERMINO - PAG-FOR (06/08/2014 13:00) ***    
                    CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - {4} por {2}{3}",
                                          (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                          DateTime.Now,
                                          CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome,
                                          ComSegundaVia ? " (segunda via)" : string.Empty,
                                          "C�pia retornada"
                                          );
                    try
                    {
                        EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 2105", dCheque.CHEquesTableAdapter);
                        if (dCheque.CHEquesTableAdapter.Update(CHErow) < 1)
                        {
                            VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(new Exception("Retorno=0"));
                            return false;
                        }
                        CHErow.AcceptChanges();
                        EMPTProc1.Commit();
                        retorno = true;
                    }
                    catch (Exception e)
                    {
                        UltimaException = e;
                        EMPTProc1.Vircatch(e);
                        throw new Exception("Recriada: Erro no retorno", e);
                    };
                    break;
                //*** MRC - FIM - PAG-FOR (2) *** 
                case CHEStatus.CompensadoAguardaCopia:
                    Status = CHEStatus.Compensado;
                    CHErow.CHERetornoData = DateTime.Now;
                    //CHErow.CHERetorno_MAL = new malote()
                    CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - C�pia retornada por {2}",
                                          (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                          CHErow.CHERetornoData,
                                          CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome
                                          );
                    try
                    {
                        EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 1856", dCheque.CHEquesTableAdapter);
                        if (dCheque.CHEquesTableAdapter.Update(CHErow) < 1)
                        {
                            VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(new Exception("Retorno=0"));
                            return false;
                        }
                        CHErow.AcceptChanges();
                        EMPTProc1.Commit();
                        retorno = true;
                    }
                    catch (Exception e)
                    {
                        UltimaException = e;
                        EMPTProc1.Vircatch(e);
                        throw new Exception("Recriada: Erro no retorno", e);
                    };
                    break;
                default:
                    break;
            }
            return retorno;
        }


        /*
        /// <summary>
        /// 
        /// </summary>
        public PAGTipo Tipo
        {
            get
            {
                if (!Encontrado || (CHErow == null))
                    return PAGTipo.cheque;
                return (PAGTipo)CHErow.CHETipo;
            }
            set
            {
                if (!Encontrado || (CHErow == null))
                    return;
                CHErow.CHETipo = (int)value;
            }
        }*/

        private string destinoPDF;

        private string DestinoPDF
        {
            get
            {
                if (destinoPDF == null)
                {
                    destinoPDF = dEstacao.dEstacaoSt.GetValor("Destino PDF");
                    if (destinoPDF == "")
                    {
                        destinoPDF = @"\\email\volume_1\publicacoes\";
                        dEstacao.dEstacaoSt.SetValor("Destino PDF", destinoPDF);
                    }
                }
                return destinoPDF;
            }
        }

        /// <summary>
        /// Abre os comprovantes na tela.
        /// </summary>
        public override void AbreComprovante()
        {                        
            //Verifica se e guia de recolhimento eletr�nico
            if (Tipo == PAGTipo.PEGuia)
            {
                //Varre os pagamentos
                dCheque.PAGamentosRow[] PAGrows = CHErow.GetPAGamentosRows();
                for (int i = 0; i < PAGrows.Length; i++)
                {
                    //Verifica se comprovante existe
                    if (!File.Exists(DestinoPDF + @"Financeiro\Recibos\Individual\recibo_tributo_" + PAGrows[i].PAG_GPS + ".pdf"))
                        MessageBox.Show(string.Format("Comprovante (Nota = {0} / Valor = {1}) n�o encontrado !!!", PAGrows[i].NOANumero, PAGrows[i].PAGValor.ToString("0.00")));
                    else
                    {
                        System.Diagnostics.Process.Start(DestinoPDF + @"Financeiro\Recibos\Individual\recibo_tributo_" + PAGrows[i].PAG_GPS + ".pdf");
                        if (MessageBox.Show(string.Format("Deseja apontar a impress�o do comprovante (Nota = {0} / Valor = {1})?", PAGrows[i].NOANumero, PAGrows[i].PAGValor.ToString("0.00")), "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            DSCentral.IMPressoesTableAdapter.Incluir(CHErow.CHE_CON, 159, 1, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Tributo No " + PAGrows[i].PAG_GPS, false);
                            MessageBox.Show(string.Format("Apontadas: {0}", 1));
                        }
                    }
                }
            }
            else
            {                
                //Verifica se comprovante existe
                if (!File.Exists(DestinoPDF + @"Financeiro\Recibos\Individual\recibo_" + CHErow.CHENumero + ".pdf"))
                    MessageBox.Show("Comprovante n�o encontrado !!!");
                else
                {
                    System.Diagnostics.Process.Start(DestinoPDF + @"Financeiro\Recibos\Individual\recibo_" + CHErow.CHENumero + ".pdf");
                    if (MessageBox.Show("Deseja apontar a impress�o do comprovante?", "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        DSCentral.IMPressoesTableAdapter.Incluir(CHErow.CHE_CON, 159, 1, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Pagamento No " + CHErow.CHENumero, false);
                        MessageBox.Show(string.Format("Apontadas: {0}", 1));
                    }
                }                
            }            
        }

        /*
        /// <summary>
        /// Junta todos os PDFs dos comprovantes em um �nico documento tempor�rio.
        /// </summary>
        /// <returns></returns>
        public override string MergeComprovantesTMP()
        {
            string NomeArquivoPDF = string.Format("{0}\\TMP\\Comprovante.pdf", Application.StartupPath);
            if (File.Exists(NomeArquivoPDF))
                try
                { File.Delete(NomeArquivoPDF); }
                catch
                { NomeArquivoPDF = String.Format("{0}Comprovante_{1:ddMMyyhhmmss}.pdf", Application.StartupPath, DateTime.Now); };
            Document PDF = MergeComprovantes(NomeArquivoPDF);
            if (PDF != null)
                return NomeArquivoPDF;
            else
                return "";
        }*/

        private class KitMerge
        {
            public Document PDFGerado;
            public PdfCopy Copiador;
            public string ArquivoPDFGerado;
            public int Paginas;

            public KitMerge(string NomeArquivo = "")
            {
                if (NomeArquivo != "")
                    ArquivoPDFGerado = NomeArquivo;
                else
                {
                    ArquivoPDFGerado = string.Format("{0}\\TMP\\Comprovante.pdf", Application.StartupPath);
                    if (File.Exists(ArquivoPDFGerado))
                    {
                        try
                        { File.Delete(ArquivoPDFGerado); }
                        catch
                        { ArquivoPDFGerado = String.Format("{0}\\TMP\\Comprovante_{1:ddMMyyhhmmss}.pdf", Application.StartupPath, DateTime.Now); };
                    }
                }
                PDFGerado = new Document();
                Copiador = new PdfCopy(PDFGerado, new FileStream(ArquivoPDFGerado, FileMode.Create));
                PDFGerado.Open();
                Paginas = 0;
            }

            public bool Fecha()
            {
                if (Paginas != 0)
                {
                    PDFGerado.Close();
                    return true;
                }
                else
                    return false;
            }
        }

        //private static StringBuilder Log;

        /// <summary>
        /// Junta todos os PDFs dos comprovantes em um �nico documento.
        /// </summary>
        /// <param name="CHEs"></param>
        /// <returns></returns>
        public new static string MergeComprovantes(List<int> CHEs)
        {
            //Log = new StringBuilder();
            KitMerge Kit = new KitMerge();
            foreach (int CHE in CHEs)
            {                
                Cheque ChequeX = new Cheque(CHE);
                //Log.AppendFormat("CHE {0} {1:n2} {2:dd/MM/yyyy}\r\n", CHE, ChequeX.Valor, ChequeX.Vencimento);
                ChequeX.MergeComprovantes(Kit);
            }
            //Clipboard.SetText(Log.ToString()); MessageBox.Show(Log.ToString());
            if (!Kit.Fecha())
                return null;            
            return Kit.ArquivoPDFGerado;
        }

        /// <summary>
        /// Junta todos os PDFs dos comprovantes em um �nico documento.
        /// </summary>
        /// <param name="CHEs"></param>
        /// <returns></returns>
        public new static string MergeComprovantes(List<ABS_Cheque> CHEs)
        {            
            KitMerge Kit = new KitMerge();
            foreach (Cheque ChequeX in CHEs)                            
                ChequeX.MergeComprovantes(Kit);                        
            if (!Kit.Fecha())
                return null;
            return Kit.ArquivoPDFGerado;
        }

        /// <summary>
        /// Carrega os comprovantes para um PDF
        /// </summary>
        /// <returns></returns>
        public string MergeComprovantes()
        {
            return MergeComprovantes((KitMerge)null);
        }

        /// <summary>
        /// Junta todos os PDFs dos comprovantes em um �nico documento.
        /// </summary>
        /// <param name="_Kit"></param>
        /// <returns></returns>
        private string MergeComprovantes(KitMerge _Kit)
        {
            if (!Tipo.EstaNoGrupo(PAGTipo.PEGuia, /*PAGTipo.GuiaEletronica,*/PAGTipo.PECreditoConta,PAGTipo.PEGuia,PAGTipo.PETituloBancario,PAGTipo.PETituloBancarioAgrupavel,PAGTipo.PETransferencia))
                return "";            
            KitMerge Kit = _Kit ?? new KitMerge();            
            int Paginas = 0;                        
            //Verifica se e guia de recolhimento eletr�nico
            if (Tipo == PAGTipo.PEGuia)
            {
                //Log.AppendFormat("    GUIA\r\n");
                //Varre os pagamentos
                dCheque.PAGamentosRow[] PAGrows = CHErow.GetPAGamentosRows();
                for (int i = 0; i < PAGrows.Length; i++)
                {
                    string NomeArquivo = string.Format("{0}Financeiro\\Recibos\\Individual\\recibo_tributo_{1}.pdf", DestinoPDF, PAGrows[i].PAG_GPS);                    
                    if (!File.Exists(NomeArquivo))
                        MessageBox.Show(string.Format("Comprovante (Nota = {0} / Valor = {1:n0} / Data {2:dd/MM/yyyy}) n�o encontrado !!!", PAGrows[i].NOANumero, PAGrows[i].PAGValor, CHErow.CHEVencimento));
                    else                    
                        Paginas += MergeUMPDF(Kit, NomeArquivo);                                         
                }
            }
            else
            {                
                //Verifica se comprovante existe
                string NomeArquivo = string.Format("{0}Financeiro\\Recibos\\Individual\\recibo_{1}.pdf", DestinoPDF, CHErow.CHENumero);
                
                if (!File.Exists(NomeArquivo))
                    MessageBox.Show(string.Format("Comprovante n�o encontrado !!! Cheque = {0} Valor = {1:n2} Data = {2:dd/MM/yyyy}", CHErow.CHENumero , CHErow.CHEValor,CHErow.CHEVencimento));
                else                
                    Paginas += MergeUMPDF(Kit, NomeArquivo);                                    
            }
            Kit.Paginas += Paginas;
            if ((Paginas == 0) && (_Kit == null))
                return null;
            if (_Kit == null)
            {
                if (!Kit.Fecha())
                    return null;
            }
            return Kit.ArquivoPDFGerado;
        }

        private int MergeUMPDF(KitMerge Kit, string strArquivo)
        {
            int Paginas = 0;
            PdfReader reader = new PdfReader(strArquivo);
            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
                Paginas++;
                PdfImportedPage importedPage = Kit.Copiador.GetImportedPage(reader, i);
                Kit.Copiador.AddPage(importedPage);
            }
            reader.Close();
            return Paginas;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SomenteLeitura"></param>
        /// <returns></returns>
        public DialogResult Editar(bool SomenteLeitura)
        {
            cCheque cCheque = new cCheque(CHErow, SomenteLeitura);
            return cCheque.VirShowModulo(EstadosDosComponentes.PopUp);
        }

        private void AddLinha(string linha)
        {
            int pos = 400 - linhas++ * 30;
            int corte = 78;
            string s1;
            string s2;
            if (linha.Length < corte)
            {
                s1 = linha;
                s2 = string.Empty;
            }
            else
            {
                while ((linha[corte] != ' ') && (corte > 40))
                    corte--;
                s1 = linha.Substring(0, corte);
                s2 = linha.Substring(corte);
            };
            mascaraRecibo += "A" + pos.ToString() + ",40,1,4,1,1,N,\"" + s1 + "\"\r\n";
            if (s2 != string.Empty)
                AddLinha(s2);
        }

        private string mascaraRecibo;
        private int linhas;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Nome"></param>
        public void Recibo(string Nome)
        {
            string dadosdocheque;
            if (Tipo == PAGTipo.PECreditoConta
                ||
                 Tipo == PAGTipo.PEOrdemPagamento
                ||
                 Tipo == PAGTipo.PETituloBancario
                ||
                 Tipo == PAGTipo.PETituloBancarioAgrupavel
                || Tipo == PAGTipo.PETransferencia)
                dadosdocheque = string.Format("Pagamento Eletronico {0}", CHErow.CHENumero);
            else
                dadosdocheque = string.Format("Banco {0} Agencia {1}-{2} Conta {3}-{4} Cheque {5}", CCTrow.CCT_BCO, CCTrow.CCTAgencia, CCTrow.CCTAgenciaDg, CCTrow.CCTConta, CCTrow.CCTContaDg, CHErow.CHENumero);

            impressos.impRecibo imp = new impressos.impRecibo(Nome,
                                                              CHErow.CHEFavorecido,
                                                              rowCON.CONNome,
                                                              CHErow.CHEValor,
                                                              dadosdocheque,
                                                              CHErow.GetPAGamentosRows());
            imp.Reset();
            imp.ImprimeDOC();


            /*
            linhas = 0;
            mascaraRecibo =
"\r\n" +
"OC,Fr\r\n" +
"Q20,0\r\n" +
"q600\r\n" +
"N\r\n" +                
"X30,10,10,600,1300\r\n" +
"A550,500,1,5,1,1,N,\"RECIBO\"\r\n";
            string manobra = "Eu ";
            if (Nome == "")
                manobra += "_____________________________ recebi de ";
            else
                manobra += Nome + " recebi de ";
            manobra += rowComplementar.CONNome + " a quantia de ";
            //manobra += CHErow.CHEValor.ToString("c") + " (cem reais) referente a:";
            manobra += CHErow.CHEValor.ToString("c") + " referente a:";
            AddLinha(manobra);

            foreach (dCheque.PAGamentosRow rp in CHErow.GetPAGamentosRows())
                 AddLinha("   - " + rp.NOAServico + " "+ rp.PAGValor.ToString("n2"));
            //AddLinha("   - xxxxxx 70,00");
            //mascaraRecibo += 
//"A210,40,1,4,1,1,N,\"Banco "+ CHErow.cct_bco +"000  Agencia 0000-0  Conta 0000-0  Cheque 00000000\"\r\n" +
//"A210,40,1,4,1,1,N,\""+ string.Format("Banco {0} Agencia {1}-{2} Conta {3}-{4} Cheque {5}\"\r\n",CCTrow.CCT_BCO,CCTrow.CCTAgencia,CCTrow.CCTAgenciaDg,CCTrow.CCTConta,CCTrow.CCTContaDg,CHErow.CHENumero)+
//*** MRC - INICIO - PAG-FOR (2) ***
//*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) *** 
if (Tipo == PAGTipo.PECreditoConta || Tipo == PAGTipo.PEOrdemPagamento ||
    Tipo == PAGTipo.PETituloBancario || Tipo == PAGTipo.PETituloBancarioAgrupavel || Tipo == PAGTipo.PETransferencia)
//*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) *** 
   mascaraRecibo +=
   "A210,40,1,4,1,1,N,\"" + string.Format("Pagamento Eletronico {5}\"\r\n", CCTrow.CCT_BCO, CCTrow.CCTAgencia, CCTrow.CCTAgenciaDg, CCTrow.CCTConta, CCTrow.CCTContaDg, CHErow.CHENumero);
else
   mascaraRecibo +=
   "A210,40,1,4,1,1,N,\"" + string.Format("Banco {0} Agencia {1}-{2} Conta {3}-{4} Cheque {5}\"\r\n", CCTrow.CCT_BCO, CCTrow.CCTAgencia, CCTrow.CCTAgenciaDg, CCTrow.CCTConta, CCTrow.CCTContaDg, CHErow.CHENumero);
mascaraRecibo += 
//*** MRC - FIM - PAG-FOR (2) *** 
"A170,40,1,4,1,1,N,\"CPF:___.___.___-__\"\r\n" +
"A140,40,1,4,1,1,N,\"RG:_______________\"\r\n" +
"A100,40,1,4,1,1,N,\""+
((Framework.DSCentral.EMP == 1) ? "Sao Bernardo " : "Santo Andre ")+
DateTime.Today.ToString("dd/MM/yyyy")+" \"\r\n" +
"LO100,610,5,570\r\n" +
"A90,610,1,2,1,1,N,\""+CHErow.CHEFavorecido+"\"\r\n" +
//"A330,500,1,4,1,1,N,\"%CONAgencia%\"\r\n" +
//"A330,640,1,4,1,1,N,\"%CONConta%\"\r\n" +
//"A330,800,1,4,1,1,N,\"%CHENumero%\"\r\n" +
//"A330,940,1,3,2,2,N,\"%CHEValor%\"\r\n" +
//"A263,420,1,1,1,1,N,\"Portador:\"\r\n" +
//"A270,510,1,4,1,1,N,\"%CHEFavorecido%\"\r\n" +
//"A220,610,1,4,1,1,N,\"%Local%, %CHEVencimento%\"\r\n" +
//"LO100,610,5,570\r\n" +
//"A90,610,1,2,1,1,N,\"%CONNome%\"\r\n" +
"P1\r\n";

            CodBar.Impress Im = new CodBar.Impress("");
            Im.RemoverAcentos = true;
            string NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de c�pias de Cheque");
            if (NomeImpressora == "")
            {
                System.Windows.Forms.PrintDialog PD = new System.Windows.Forms.PrintDialog();
                PD.ShowDialog();
                NomeImpressora = PD.PrinterSettings.PrinterName;
                CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de c�pias de Cheque", NomeImpressora);
            }
            Im.NomeImpressora = NomeImpressora;
            Im.ImprimeDOC(mascaraRecibo);        */
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Justificativa"></param>
        /// <returns></returns>
        public bool SalvarAlteracoes(string Justificativa)
        {
            /*
            ArrayList BODsAlterados = new ArrayList();
            decimal Total = 0;
            foreach (dBoletos.BOletoDetalheRow rowBOD in rowPrincipal.GetBOletoDetalheRows())
            {
                if (rowBOD.BODValor != decimal.Round(rowBOD.BODValor, 2))
                    rowBOD.BODValor = decimal.Round(rowBOD.BODValor, 2);
                if (rowBOD.RowState != DataRowState.Unchanged)
                    BODsAlterados.Add(rowBOD);
                Total += rowBOD.BODValor;
            };

            foreach (dBoletos.BOletoDetalheRow rowBOD in BODrowsOriginal)
            {
                if (rowBOD.RowState == DataRowState.Deleted)
                    BODsAlterados.Add(rowBOD);

            };

            if (rowPrincipal.BOLValorPrevisto != Total)
                rowPrincipal.BOLValorPrevisto = Total;
            */

            SortedList CamposAlterados = VirDB.VirtualTableAdapter.CamposEfetivamenteAlterados(CHErow);

            if ((CamposAlterados.Count > 0) /*|| (BODsAlterados.Count > 0)*/ )
            {
                if (Justificativa == string.Empty)
                    if (!VirInput.Input.Execute("Justificativa", ref Justificativa, true))
                        return false;
                ;
                string Just = DateTime.Now.ToString() + " - Cheque alterado por " + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome + "\r\nJustificativa:" + Justificativa;
                CHErow.CHEObs = (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n") + Just;
                //listar alteracoes
                if (CamposAlterados.ContainsKey("CHEVencimento"))
                    CHErow.CHEObs += String.Format("\r\nVencimento: {0:dd/MM/yyyy} -> {1:dd/MM/yyyy}", CHErow["CHEVencimento", System.Data.DataRowVersion.Original], CHErow.CHEVencimento);
                if (CamposAlterados.ContainsKey("CHEFavorecido"))
                    CHErow.CHEObs += String.Format("\r\nFavorecido: {0} -> {1}", CHErow["CHEFavorecido", System.Data.DataRowVersion.Original], CHErow.CHEFavorecido);
                if (CamposAlterados.ContainsKey("CHETipo"))
                    CHErow.CHEObs += String.Format("\r\nTipo: {0} -> {1}", Framework.Enumeracoes.VirEnumPAGTipo.Descritivo(CHErow["CHETipo", System.Data.DataRowVersion.Original]), Framework.Enumeracoes.VirEnumPAGTipo.Descritivo(CHErow.CHETipo));

                //if (CamposAlterados.ContainsKey("BOLValorPrevisto"))
                //    rowPrincipal.BOLJustificativa += "\r\nValor: " + ((decimal)rowPrincipal["BOLValorPrevisto", DataRowVersion.Original]).ToString("n2") + " - > " + CHErow.CHEValor.ToString("n2");
                //if (CamposAlterados.ContainsKey("BOLVencto"))
                //    rowPrincipal.BOLJustificativa += "\r\nVencimento: " + ((DateTime)rowPrincipal["BOLVencto", DataRowVersion.Original]).ToString("dd/MM/yyyy") + " - > " + rowPrincipal.BOLVencto.ToString("dd/MM/yyyy");
                //if (CamposAlterados.ContainsKey("BOLNome"))
                //    rowPrincipal.BOLJustificativa += "\r\nDestinat�rio: " + rowPrincipal["BOLNome", DataRowVersion.Original].ToString() + " - > " + rowPrincipal.BOLNome.ToString();
                //if (CamposAlterados.ContainsKey("BOLEndereco"))
                //    rowPrincipal.BOLJustificativa += "\r\nEndere�o: " + rowPrincipal["BOLEndereco", DataRowVersion.Original].ToString() + " - > " + rowPrincipal.BOLEndereco.ToString();
                //if ((CamposAlterados.ContainsKey("BOLCompetenciaMes")) || (CamposAlterados.ContainsKey("BOLCompetenciaAno")))
                //{
                //    Competencia compOrig = new Competencia((Int16)rowPrincipal["BOLCompetenciaMes", DataRowVersion.Original], (Int16)rowPrincipal["BOLCompetenciaAno", DataRowVersion.Original]);
                //    Competencia compNova = new Competencia((Int16)rowPrincipal["BOLCompetenciaMes", DataRowVersion.Current], (Int16)rowPrincipal["BOLCompetenciaAno", DataRowVersion.Current]);
                //    rowPrincipal.BOLJustificativa += "\r\nCompet�ncia: " + compOrig.ToString() + " - > " + compNova.ToString();
                //};
                /*
                if (BODsAlterados.Count > 0)
                {
                    rowPrincipal.BOLJustificativa += "\r\n  Itens alterados: ";
                    foreach (dBoletos.BOletoDetalheRow rowBOD in BODsAlterados)
                        switch (rowBOD.RowState)
                        {
                            case DataRowState.Added:
                                rowPrincipal.BOLJustificativa += "\r\n  Inclu�do: " + rowBOD.BODMensagem + "  +" + rowBOD.BODValor.ToString("n2");
                                break;
                            case DataRowState.Deleted:
                                rowPrincipal.BOLJustificativa += "\r\n  Exclu�do: " + DadosLinhasOriginais[rowBOD.GetHashCode()].ToString();
                                break;
                            case DataRowState.Detached:
                                break;
                            case DataRowState.Modified:
                                decimal ValorOriginal = (decimal)rowBOD["BODValor", DataRowVersion.Original];
                                rowPrincipal.BOLJustificativa += "\r\n  " + rowBOD.BODMensagem + ": " + ValorOriginal.ToString("n2") + " - > " + rowBOD.BODValor.ToString("n2");
                                break;
                            case DataRowState.Unchanged:
                                break;
                            default:
                                break;
                        };
                };
                */
                CHErow.CHEA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                CHErow.CHEDATAA = DateTime.Now;



                try
                {
                    EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 2103", dCheque.CHEquesTableAdapter);
                    dCheque.CHEquesTableAdapter.Update(CHErow);
                    //foreach (dBoletos.BOletoDetalheRow rowBOD in BODsAlterados)
                    //    dBoletos.BOletoDetalheTableAdapter.Update(rowBOD);
                    EMPTProc1.Commit();
                    //dBoletos.BOLetosTableAdapter.ImplantaTrans(false);
                    //dBoletos.BOletoDetalheTableAdapter.ImplantaTrans(false);
                    CHErow.AcceptChanges();
                    //foreach (dBoletos.BOletoDetalheRow rowBOD in BODsAlterados)
                    //    if ((rowBOD.RowState != DataRowState.Deleted) && (rowBOD.RowState != DataRowState.Detached))
                    //        rowBOD.AcceptChanges();                    
                    return true;
                }
                catch (Exception e)
                {
                    UltimaException = e;
                    CHErow.RejectChanges();
                    //foreach (dBoletos.BOletoDetalheRow rowBOD in BODsAlterados)
                    //    if ((rowBOD.RowState != DataRowState.Deleted) && (rowBOD.RowState != DataRowState.Detached))
                    //        rowBOD.RejectChanges();
                    EMPTProc1.Vircatch(e);
                    return false;
                }

            }
            else
                return true;

        }

        #endregion
    }

}
