﻿using System;

namespace dllCheques
{
    /*
    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    [Obsolete("usar Framework.CHEStatus")]
    public enum CHEStatus
    {
        Antigo = -2,
        NaoEncontrado = -1,
        Cancelado = 0,
        Cadastrado = 1,
        AguardaRetorno = 2,
        Aguardacopia = 3,
        Retornado = 4,
        EnviadoBanco = 5,
        Retirado = 6,
        Compensado = 7,
        CompensadoAguardaCopia = 8,
        PagamentoCancelado = 9,
        DebitoAutomatico = 10,
        Caixinha = 11,
        Eletronico = 12,
        ComSindico = 13,
        BaixaManual = 14
    }


    /// <summary>
    /// virEnum para campo 
    /// </summary>
    [Obsolete("usar Framework.Enumeracoes.virEnumCHEStatus")]
    public class virEnumCHEStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumCHEStatus(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumCHEStatus _virEnumCHEStatusSt;

        /// <summary>
        /// VirEnum estático para campo CHEStatus
        /// </summary>
        public static virEnumCHEStatus virEnumCHEStatusSt
        {
            get
            {
                if (_virEnumCHEStatusSt == null)
                {
                    _virEnumCHEStatusSt = new virEnumCHEStatus(typeof(CHEStatus));
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.NaoEncontrado, "Cheque não encontrado");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.Cancelado, "Cancelado");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.Cadastrado, "Cadastrado");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.AguardaRetorno, "Aguarda retorno");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.Aguardacopia, "Aguarda Cópia");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.Retornado, "Retornado");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.Retirado, "Retirado pelo favorecido");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.EnviadoBanco, "Enviado para o banco");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.Compensado, "Compensado");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.CompensadoAguardaCopia, "Compensado sem o retorno da cópia");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.PagamentoCancelado, "Pagamento Cancelado");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.Eletronico, "Pagamento Eletrônico");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.Caixinha, "Pagamento via caixinha");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.DebitoAutomatico, "Débito automático agendado");
                    _virEnumCHEStatusSt.GravaNomes(CHEStatus.BaixaManual, "Baixa Manual");
                }
                return _virEnumCHEStatusSt;
            }
        }
    }

    */
}