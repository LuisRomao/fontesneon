namespace dllCheques
{
    partial class cChequesEmitir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNOA_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colNOANumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_CHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dChequesImprimirBindingSource = new System.Windows.Forms.BindingSource();
            this.dChequesImprimir = new dllCheques.dChequesImprimir();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCHEValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEFavorecido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTituloGrupo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEIdaData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHERetornoData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHENumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEObs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colCHE_CCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHE_CCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCHETipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaloteIda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaloteVolta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCONAuditor1_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource();
            this.colCONAuditor2_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumeroGrupo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.BotCorrecao = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lookupGerente1 = new Framework.Lookup.LookupGerente();
            this.chTESTE = new DevExpress.XtraEditors.CheckEdit();
            this.timeEdit1 = new DevExpress.XtraEditors.TimeEdit();
            this.grupoFiltro = new DevExpress.XtraEditors.GroupControl();
            this.chCinza = new DevExpress.XtraEditors.CheckEdit();
            this.chAmarelo = new DevExpress.XtraEditors.CheckEdit();
            this.chVermelho = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.BotRecibo = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.BtCopia = new DevExpress.XtraEditors.SimpleButton();
            this.DataInicio = new DevExpress.XtraEditors.DateEdit();
            this.chNaoVerificar = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dChequesImprimirBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dChequesImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chTESTE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grupoFiltro)).BeginInit();
            this.grupoFiltro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chCinza.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAmarelo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVermelho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataInicio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataInicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chNaoVerificar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView2.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(159)))), ((int)(((byte)(69)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(152)))), ((int)(((byte)(49)))));
            this.gridView2.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(176)))), ((int)(((byte)(84)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView2.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.Preview.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(177)))), ((int)(((byte)(90)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseBorderColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNOA_PLA,
            this.colNOAServico,
            this.colFRNCnpj,
            this.colFRNFantasia,
            this.colFRNFone1,
            this.colPAGTipo,
            this.colNOANumero,
            this.colPAG_CHE,
            this.colPAGValor,
            this.colNOA,
            this.colFRNNome,
            this.gridColumn2});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsDetail.ShowDetailTabs = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colNOA_PLA
            // 
            this.colNOA_PLA.Caption = "Conta";
            this.colNOA_PLA.FieldName = "NOA_PLA";
            this.colNOA_PLA.Name = "colNOA_PLA";
            this.colNOA_PLA.OptionsColumn.ReadOnly = true;
            this.colNOA_PLA.Visible = true;
            this.colNOA_PLA.VisibleIndex = 0;
            // 
            // colNOAServico
            // 
            this.colNOAServico.Caption = "Servi�o";
            this.colNOAServico.FieldName = "NOAServico";
            this.colNOAServico.Name = "colNOAServico";
            this.colNOAServico.OptionsColumn.ReadOnly = true;
            this.colNOAServico.Visible = true;
            this.colNOAServico.VisibleIndex = 1;
            // 
            // colFRNCnpj
            // 
            this.colFRNCnpj.Caption = "Cnpj";
            this.colFRNCnpj.FieldName = "FRNCnpj";
            this.colFRNCnpj.Name = "colFRNCnpj";
            this.colFRNCnpj.OptionsColumn.ReadOnly = true;
            this.colFRNCnpj.Width = 141;
            // 
            // colFRNFantasia
            // 
            this.colFRNFantasia.Caption = "Fornecedor";
            this.colFRNFantasia.FieldName = "FRNFantasia";
            this.colFRNFantasia.Name = "colFRNFantasia";
            this.colFRNFantasia.OptionsColumn.ReadOnly = true;
            this.colFRNFantasia.Visible = true;
            this.colFRNFantasia.VisibleIndex = 2;
            // 
            // colFRNFone1
            // 
            this.colFRNFone1.Caption = "Fone";
            this.colFRNFone1.FieldName = "FRNFone1";
            this.colFRNFone1.Name = "colFRNFone1";
            this.colFRNFone1.OptionsColumn.ReadOnly = true;
            this.colFRNFone1.Visible = true;
            this.colFRNFone1.VisibleIndex = 3;
            // 
            // colPAGTipo
            // 
            this.colPAGTipo.Caption = "Tipo";
            this.colPAGTipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colPAGTipo.FieldName = "PAGTipo";
            this.colPAGTipo.Name = "colPAGTipo";
            this.colPAGTipo.OptionsColumn.ReadOnly = true;
            this.colPAGTipo.Visible = true;
            this.colPAGTipo.VisibleIndex = 4;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colNOANumero
            // 
            this.colNOANumero.Caption = "Nota";
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.OptionsColumn.ReadOnly = true;
            this.colNOANumero.Visible = true;
            this.colNOANumero.VisibleIndex = 5;
            // 
            // colPAG_CHE
            // 
            this.colPAG_CHE.Caption = "CHE";
            this.colPAG_CHE.FieldName = "PAG_CHE";
            this.colPAG_CHE.Name = "colPAG_CHE";
            this.colPAG_CHE.OptionsColumn.ReadOnly = true;
            // 
            // colPAGValor
            // 
            this.colPAGValor.Caption = "Valor";
            this.colPAGValor.DisplayFormat.FormatString = "n2";
            this.colPAGValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPAGValor.FieldName = "PAGValor";
            this.colPAGValor.Name = "colPAGValor";
            this.colPAGValor.OptionsColumn.ReadOnly = true;
            this.colPAGValor.Visible = true;
            this.colPAGValor.VisibleIndex = 6;
            // 
            // colNOA
            // 
            this.colNOA.Caption = "NOA";
            this.colNOA.FieldName = "NOA";
            this.colNOA.Name = "colNOA";
            this.colNOA.OptionsColumn.ReadOnly = true;
            // 
            // colFRNNome
            // 
            this.colFRNNome.Caption = "Fornecedor (Raz�o)";
            this.colFRNNome.FieldName = "FRNNome";
            this.colFRNNome.Name = "colFRNNome";
            this.colFRNNome.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 7;
            this.gridColumn2.Width = 23;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            editorButtonImageOptions1.Image = global::dllCheques.Properties.Resources.Nota1;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.dChequesImprimirBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "ChequesparaEmitir_DetalhesCHE";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 166);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemMemoEdit1,
            this.repositoryItemButtonEdit1,
            this.repositoryItemImageComboBox2,
            this.repositoryItemButtonEdit2,
            this.repositoryItemLookUpEdit1});
            this.gridControl1.Size = new System.Drawing.Size(0, 0);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // dChequesImprimirBindingSource
            // 
            this.dChequesImprimirBindingSource.DataMember = "ChequesparaEmitir";
            this.dChequesImprimirBindingSource.DataSource = this.dChequesImprimir;
            // 
            // dChequesImprimir
            // 
            this.dChequesImprimir.DataSetName = "dChequesImprimir";
            this.dChequesImprimir.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gainsboro;
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView1.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseBorderColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCHEValor,
            this.colCHEVencimento,
            this.colCHEFavorecido,
            this.colCONCodigo,
            this.colTituloGrupo,
            this.colCHEIdaData,
            this.colCHERetornoData,
            this.colCHEEmissao,
            this.colCHE,
            this.colCHENumero,
            this.colCHEObs,
            this.colCHE_CCD,
            this.colCHE_CCT,
            this.colCHEStatus,
            this.colCHETipo,
            this.colMaloteIda,
            this.colMaloteVolta,
            this.gridColumn1,
            this.colCONAuditor1_USU,
            this.colCONAuditor2_USU,
            this.colNumeroGrupo});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(257, 312, 208, 184);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupFormat = " [#image]{1} {2}";
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "CONNome", null, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Min, "NumeroGrupo", null, "Dias = {0:n0}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsSelection.InvertSelection = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTituloGrupo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCHEVencimento, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView1_KeyDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // colCHEValor
            // 
            this.colCHEValor.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colCHEValor.AppearanceCell.Options.UseFont = true;
            this.colCHEValor.Caption = "Valor";
            this.colCHEValor.DisplayFormat.FormatString = "n2";
            this.colCHEValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCHEValor.FieldName = "CHEValor";
            this.colCHEValor.Name = "colCHEValor";
            this.colCHEValor.OptionsColumn.AllowEdit = false;
            this.colCHEValor.OptionsColumn.FixedWidth = true;
            this.colCHEValor.OptionsColumn.ReadOnly = true;
            this.colCHEValor.Visible = true;
            this.colCHEValor.VisibleIndex = 6;
            this.colCHEValor.Width = 94;
            // 
            // colCHEVencimento
            // 
            this.colCHEVencimento.Caption = "Vencimento";
            this.colCHEVencimento.FieldName = "CHEVencimento";
            this.colCHEVencimento.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colCHEVencimento.Name = "colCHEVencimento";
            this.colCHEVencimento.OptionsColumn.AllowEdit = false;
            this.colCHEVencimento.OptionsColumn.FixedWidth = true;
            this.colCHEVencimento.OptionsColumn.ReadOnly = true;
            this.colCHEVencimento.Visible = true;
            this.colCHEVencimento.VisibleIndex = 9;
            this.colCHEVencimento.Width = 100;
            // 
            // colCHEFavorecido
            // 
            this.colCHEFavorecido.Caption = "Favorecido";
            this.colCHEFavorecido.FieldName = "CHEFavorecido";
            this.colCHEFavorecido.Name = "colCHEFavorecido";
            this.colCHEFavorecido.OptionsColumn.AllowEdit = false;
            this.colCHEFavorecido.OptionsColumn.ReadOnly = true;
            this.colCHEFavorecido.Visible = true;
            this.colCHEFavorecido.VisibleIndex = 1;
            this.colCHEFavorecido.Width = 515;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.AllowEdit = false;
            this.colCONCodigo.OptionsColumn.FixedWidth = true;
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "CONCodigo", "{0:n0}")});
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            this.colCONCodigo.Width = 85;
            // 
            // colTituloGrupo
            // 
            this.colTituloGrupo.Caption = "Condom�nio";
            this.colTituloGrupo.FieldName = "TituloGrupo";
            this.colTituloGrupo.Name = "colTituloGrupo";
            this.colTituloGrupo.OptionsColumn.AllowEdit = false;
            this.colTituloGrupo.OptionsColumn.ReadOnly = true;
            this.colTituloGrupo.Width = 160;
            // 
            // colCHEIdaData
            // 
            this.colCHEIdaData.Caption = "Ida";
            this.colCHEIdaData.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.colCHEIdaData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCHEIdaData.FieldName = "CHEIdaData";
            this.colCHEIdaData.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colCHEIdaData.Name = "colCHEIdaData";
            this.colCHEIdaData.OptionsColumn.AllowEdit = false;
            this.colCHEIdaData.OptionsColumn.FixedWidth = true;
            this.colCHEIdaData.OptionsColumn.ReadOnly = true;
            this.colCHEIdaData.Width = 109;
            // 
            // colCHERetornoData
            // 
            this.colCHERetornoData.Caption = "Volta";
            this.colCHERetornoData.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.colCHERetornoData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCHERetornoData.FieldName = "CHERetornoData";
            this.colCHERetornoData.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colCHERetornoData.Name = "colCHERetornoData";
            this.colCHERetornoData.OptionsColumn.AllowEdit = false;
            this.colCHERetornoData.OptionsColumn.FixedWidth = true;
            this.colCHERetornoData.OptionsColumn.ReadOnly = true;
            this.colCHERetornoData.Width = 110;
            // 
            // colCHEEmissao
            // 
            this.colCHEEmissao.Caption = "Emiss�o";
            this.colCHEEmissao.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.colCHEEmissao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCHEEmissao.FieldName = "CHEEmissao";
            this.colCHEEmissao.Name = "colCHEEmissao";
            this.colCHEEmissao.OptionsColumn.AllowEdit = false;
            this.colCHEEmissao.OptionsColumn.FixedWidth = true;
            this.colCHEEmissao.OptionsColumn.ReadOnly = true;
            this.colCHEEmissao.Visible = true;
            this.colCHEEmissao.VisibleIndex = 3;
            this.colCHEEmissao.Width = 93;
            // 
            // colCHE
            // 
            this.colCHE.Caption = "CHE";
            this.colCHE.FieldName = "CHE";
            this.colCHE.Name = "colCHE";
            this.colCHE.OptionsColumn.AllowEdit = false;
            this.colCHE.OptionsColumn.ReadOnly = true;
            // 
            // colCHENumero
            // 
            this.colCHENumero.Caption = "N�mero";
            this.colCHENumero.FieldName = "CHENumero";
            this.colCHENumero.MaxWidth = 100;
            this.colCHENumero.Name = "colCHENumero";
            this.colCHENumero.OptionsColumn.AllowEdit = false;
            this.colCHENumero.OptionsColumn.ReadOnly = true;
            this.colCHENumero.Visible = true;
            this.colCHENumero.VisibleIndex = 4;
            this.colCHENumero.Width = 86;
            // 
            // colCHEObs
            // 
            this.colCHEObs.Caption = "Obs";
            this.colCHEObs.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colCHEObs.FieldName = "CHEObs";
            this.colCHEObs.Name = "colCHEObs";
            this.colCHEObs.OptionsColumn.AllowEdit = false;
            this.colCHEObs.OptionsColumn.ReadOnly = true;
            this.colCHEObs.OptionsFilter.AllowFilter = false;
            this.colCHEObs.Width = 33;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // colCHE_CCD
            // 
            this.colCHE_CCD.Caption = "CCD";
            this.colCHE_CCD.FieldName = "CHE_CCD";
            this.colCHE_CCD.Name = "colCHE_CCD";
            this.colCHE_CCD.OptionsColumn.AllowEdit = false;
            this.colCHE_CCD.OptionsColumn.ReadOnly = true;
            // 
            // colCHE_CCT
            // 
            this.colCHE_CCT.Caption = "CCT";
            this.colCHE_CCT.FieldName = "CHE_CCT";
            this.colCHE_CCT.Name = "colCHE_CCT";
            this.colCHE_CCT.OptionsColumn.AllowEdit = false;
            this.colCHE_CCT.OptionsColumn.ReadOnly = true;
            this.colCHE_CCT.Width = 40;
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.Caption = "Status";
            this.colCHEStatus.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.OptionsColumn.AllowEdit = false;
            this.colCHEStatus.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colCHETipo
            // 
            this.colCHETipo.Caption = "Tipo";
            this.colCHETipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCHETipo.FieldName = "CHETipo";
            this.colCHETipo.Name = "colCHETipo";
            this.colCHETipo.OptionsColumn.AllowEdit = false;
            this.colCHETipo.OptionsColumn.FixedWidth = true;
            this.colCHETipo.OptionsColumn.ReadOnly = true;
            this.colCHETipo.Visible = true;
            this.colCHETipo.VisibleIndex = 5;
            this.colCHETipo.Width = 130;
            // 
            // colMaloteIda
            // 
            this.colMaloteIda.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F);
            this.colMaloteIda.AppearanceCell.Options.UseFont = true;
            this.colMaloteIda.AppearanceCell.Options.UseTextOptions = true;
            this.colMaloteIda.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaloteIda.Caption = "Mal. Ida";
            this.colMaloteIda.FieldName = "MaloteIda";
            this.colMaloteIda.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colMaloteIda.Name = "colMaloteIda";
            this.colMaloteIda.OptionsColumn.AllowEdit = false;
            this.colMaloteIda.OptionsColumn.FixedWidth = true;
            this.colMaloteIda.OptionsColumn.ReadOnly = true;
            this.colMaloteIda.Visible = true;
            this.colMaloteIda.VisibleIndex = 7;
            this.colMaloteIda.Width = 104;
            // 
            // colMaloteVolta
            // 
            this.colMaloteVolta.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F);
            this.colMaloteVolta.AppearanceCell.Options.UseFont = true;
            this.colMaloteVolta.AppearanceCell.Options.UseTextOptions = true;
            this.colMaloteVolta.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaloteVolta.Caption = "Mal Volta";
            this.colMaloteVolta.FieldName = "MaloteVolta";
            this.colMaloteVolta.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colMaloteVolta.Name = "colMaloteVolta";
            this.colMaloteVolta.OptionsColumn.AllowEdit = false;
            this.colMaloteVolta.OptionsColumn.FixedWidth = true;
            this.colMaloteVolta.OptionsColumn.ReadOnly = true;
            this.colMaloteVolta.Visible = true;
            this.colMaloteVolta.VisibleIndex = 8;
            this.colMaloteVolta.Width = 104;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 10;
            this.gridColumn1.Width = 23;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions2.Image = global::dllCheques.Properties.Resources.impCheque1;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colCONAuditor1_USU
            // 
            this.colCONAuditor1_USU.Caption = "Gerente";
            this.colCONAuditor1_USU.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colCONAuditor1_USU.FieldName = "CONAuditor1_USU";
            this.colCONAuditor1_USU.Name = "colCONAuditor1_USU";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USU Nome", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit1.DataSource = this.uSUariosBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "USUNome";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ShowHeader = false;
            this.repositoryItemLookUpEdit1.ValueMember = "USU";
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colCONAuditor2_USU
            // 
            this.colCONAuditor2_USU.Caption = "Assistente";
            this.colCONAuditor2_USU.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colCONAuditor2_USU.FieldName = "CONAuditor2_USU";
            this.colCONAuditor2_USU.Name = "colCONAuditor2_USU";
            this.colCONAuditor2_USU.Visible = true;
            this.colCONAuditor2_USU.VisibleIndex = 2;
            this.colCONAuditor2_USU.Width = 200;
            // 
            // colNumeroGrupo
            // 
            this.colNumeroGrupo.FieldName = "NumeroGrupo";
            this.colNumeroGrupo.Name = "colNumeroGrupo";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.BotCorrecao);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.lookupGerente1);
            this.panelControl1.Controls.Add(this.chTESTE);
            this.panelControl1.Controls.Add(this.timeEdit1);
            this.panelControl1.Controls.Add(this.grupoFiltro);
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.BotRecibo);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.BtCopia);
            this.panelControl1.Controls.Add(this.DataInicio);
            this.panelControl1.Controls.Add(this.chNaoVerificar);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.dateEdit1);
            this.panelControl1.Controls.Add(this.radioGroup1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(0, 166);
            this.panelControl1.TabIndex = 1;
            // 
            // BotCorrecao
            // 
            this.BotCorrecao.Location = new System.Drawing.Point(1083, 78);
            this.BotCorrecao.Name = "BotCorrecao";
            this.BotCorrecao.Size = new System.Drawing.Size(136, 23);
            this.BotCorrecao.TabIndex = 26;
            this.BotCorrecao.Text = "Correcao CCG";
            this.BotCorrecao.Visible = false;
            this.BotCorrecao.Click += new System.EventHandler(this.BotCorrecao_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Location = new System.Drawing.Point(596, 15);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(160, 138);
            this.groupControl2.TabIndex = 25;
            this.groupControl2.Text = "Eletr�nicos";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Violet;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(5, 112);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelControl6.Size = new System.Drawing.Size(151, 19);
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "N�o Efetivado";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.MediumPurple;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(5, 90);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelControl5.Size = new System.Drawing.Size(151, 19);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Recusado pelo s�ndico";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(5, 68);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelControl4.Size = new System.Drawing.Size(151, 19);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Ag. confima��o";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Silver;
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(5, 46);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelControl3.Size = new System.Drawing.Size(151, 19);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Enviando";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(5, 24);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelControl2.Size = new System.Drawing.Size(151, 19);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Emitir";
            // 
            // lookupGerente1
            // 
            this.lookupGerente1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente1.Appearance.Options.UseBackColor = true;
            this.lookupGerente1.CaixaAltaGeral = true;
            this.lookupGerente1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente1.Location = new System.Drawing.Point(153, 122);
            this.lookupGerente1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupGerente1.Name = "lookupGerente1";
            this.lookupGerente1.Size = new System.Drawing.Size(289, 20);
            this.lookupGerente1.somenteleitura = false;
            this.lookupGerente1.TabIndex = 23;
            this.lookupGerente1.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.gerente;
            this.lookupGerente1.Titulo = null;
            this.lookupGerente1.USUSel = -1;
            this.lookupGerente1.alterado += new System.EventHandler(this.lookupGerente1_alterado);
            // 
            // chTESTE
            // 
            this.chTESTE.Location = new System.Drawing.Point(947, 7);
            this.chTESTE.Name = "chTESTE";
            this.chTESTE.Properties.Caption = "Hora de teste";
            this.chTESTE.Size = new System.Drawing.Size(133, 19);
            this.chTESTE.TabIndex = 22;
            this.chTESTE.Visible = false;
            // 
            // timeEdit1
            // 
            this.timeEdit1.EditValue = new System.DateTime(2011, 9, 1, 0, 0, 0, 0);
            this.timeEdit1.Location = new System.Drawing.Point(1050, 7);
            this.timeEdit1.Name = "timeEdit1";
            this.timeEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeEdit1.Properties.Mask.EditMask = "g";
            this.timeEdit1.Size = new System.Drawing.Size(169, 20);
            this.timeEdit1.TabIndex = 21;
            this.timeEdit1.Visible = false;
            // 
            // grupoFiltro
            // 
            this.grupoFiltro.Controls.Add(this.chCinza);
            this.grupoFiltro.Controls.Add(this.chAmarelo);
            this.grupoFiltro.Controls.Add(this.chVermelho);
            this.grupoFiltro.Location = new System.Drawing.Point(762, 15);
            this.grupoFiltro.Name = "grupoFiltro";
            this.grupoFiltro.Size = new System.Drawing.Size(121, 138);
            this.grupoFiltro.TabIndex = 20;
            this.grupoFiltro.Text = "Filtro - Datas";
            // 
            // chCinza
            // 
            this.chCinza.EditValue = true;
            this.chCinza.Location = new System.Drawing.Point(5, 112);
            this.chCinza.Name = "chCinza";
            this.chCinza.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.chCinza.Properties.Appearance.Options.UseBackColor = true;
            this.chCinza.Properties.Caption = "Futuro";
            this.chCinza.Size = new System.Drawing.Size(110, 19);
            this.chCinza.TabIndex = 2;
            this.chCinza.TabStop = false;
            this.chCinza.CheckStateChanged += new System.EventHandler(this.checkEdit1_CheckStateChanged);
            // 
            // chAmarelo
            // 
            this.chAmarelo.EditValue = true;
            this.chAmarelo.Location = new System.Drawing.Point(5, 68);
            this.chAmarelo.Name = "chAmarelo";
            this.chAmarelo.Properties.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.chAmarelo.Properties.Appearance.Options.UseBackColor = true;
            this.chAmarelo.Properties.Caption = "Pr�ximo Malote";
            this.chAmarelo.Size = new System.Drawing.Size(110, 19);
            toolTipTitleItem1.Text = "Crit�rio para pr�ximo malote:";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "At� 10:00 - Malote da tarde\r\n10:00 - 12:00 Malote do banco\r\nap�s as 12:00 Malote " +
    "da manh�";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.chAmarelo.SuperTip = superToolTip1;
            this.chAmarelo.TabIndex = 1;
            this.chAmarelo.TabStop = false;
            this.chAmarelo.CheckStateChanged += new System.EventHandler(this.checkEdit1_CheckStateChanged);
            // 
            // chVermelho
            // 
            this.chVermelho.EditValue = true;
            this.chVermelho.Location = new System.Drawing.Point(5, 24);
            this.chVermelho.Name = "chVermelho";
            this.chVermelho.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.chVermelho.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.chVermelho.Properties.Appearance.Options.UseBackColor = true;
            this.chVermelho.Properties.Appearance.Options.UseForeColor = true;
            this.chVermelho.Properties.Caption = "Atrasado";
            this.chVermelho.Size = new System.Drawing.Size(110, 19);
            this.chVermelho.TabIndex = 0;
            this.chVermelho.TabStop = false;
            this.chVermelho.CheckStateChanged += new System.EventHandler(this.checkEdit1_CheckStateChanged);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(503, 78);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(87, 75);
            this.simpleButton5.TabIndex = 18;
            this.simpleButton5.Text = "Retorno";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click_1);
            // 
            // BotRecibo
            // 
            this.BotRecibo.Enabled = false;
            this.BotRecibo.Location = new System.Drawing.Point(503, 15);
            this.BotRecibo.Name = "BotRecibo";
            this.BotRecibo.Size = new System.Drawing.Size(87, 23);
            this.BotRecibo.TabIndex = 17;
            this.BotRecibo.Text = "Recibo";
            this.BotRecibo.Click += new System.EventHandler(this.BotRecibo_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(410, 78);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(87, 23);
            this.simpleButton4.TabIndex = 15;
            this.simpleButton4.Text = "Rastrear";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click_1);
            // 
            // BtCopia
            // 
            this.BtCopia.Location = new System.Drawing.Point(503, 44);
            this.BtCopia.Name = "BtCopia";
            this.BtCopia.Size = new System.Drawing.Size(87, 23);
            this.BtCopia.TabIndex = 9;
            this.BtCopia.Text = "C�pia";
            this.BtCopia.Visible = false;
            this.BtCopia.Click += new System.EventHandler(this.BtCopia_Click);
            // 
            // DataInicio
            // 
            this.DataInicio.EditValue = null;
            this.DataInicio.Location = new System.Drawing.Point(223, 10);
            this.DataInicio.Name = "DataInicio";
            this.DataInicio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataInicio.Properties.Appearance.Options.UseFont = true;
            this.DataInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DataInicio.Size = new System.Drawing.Size(175, 30);
            this.DataInicio.TabIndex = 7;
            this.DataInicio.Visible = false;
            this.DataInicio.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // chNaoVerificar
            // 
            this.chNaoVerificar.Location = new System.Drawing.Point(266, 95);
            this.chNaoVerificar.Name = "chNaoVerificar";
            this.chNaoVerificar.Properties.Caption = "N�o verificar cheque";
            this.chNaoVerificar.Size = new System.Drawing.Size(132, 19);
            this.chNaoVerificar.TabIndex = 6;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(410, 44);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(87, 23);
            this.simpleButton3.TabIndex = 5;
            this.simpleButton3.Text = "Cheque ->";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(410, 15);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(87, 23);
            this.simpleButton2.TabIndex = 4;
            this.simpleButton2.Text = "Cheque <-";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(154, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 25);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Datas";
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(223, 46);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(175, 30);
            this.dateEdit1.TabIndex = 2;
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(7, 5);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.AllowFocused = false;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Novos", "(usar Radio enum)")});
            this.radioGroup1.Size = new System.Drawing.Size(140, 148);
            this.radioGroup1.TabIndex = 1;
            this.radioGroup1.TabStop = false;
            this.radioGroup1.SelectedIndexChanged += new System.EventHandler(this.radioGroup1_SelectedIndexChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(155, 92);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(105, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Atualizar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cChequesEmitir
            // 
            this.Autofill = false;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cChequesEmitir";
            this.Size = new System.Drawing.Size(0, 0);
            this.Titulo = "Cheques para emitir";
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dChequesImprimirBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dChequesImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chTESTE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grupoFiltro)).EndInit();
            this.grupoFiltro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chCinza.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAmarelo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVermelho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataInicio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataInicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chNaoVerificar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.BindingSource dChequesImprimirBindingSource;
        private dChequesImprimir dChequesImprimir;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEFavorecido;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colTituloGrupo;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.CheckEdit chNaoVerificar;
        private DevExpress.XtraEditors.DateEdit DataInicio;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEIdaData;
        private DevExpress.XtraGrid.Columns.GridColumn colCHERetornoData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFantasia;
        private DevExpress.XtraEditors.SimpleButton BtCopia;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colCHE;
        private DevExpress.XtraGrid.Columns.GridColumn colCHENumero;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEObs;
        private DevExpress.XtraGrid.Columns.GridColumn colCHE_CCD;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFone1;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGTipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCHE_CCT;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCHETipo;
        private DevExpress.XtraGrid.Columns.GridColumn colMaloteIda;
        private DevExpress.XtraGrid.Columns.GridColumn colMaloteVolta;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton BotRecibo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_CHE;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGValor;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNNome;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.GroupControl grupoFiltro;
        private DevExpress.XtraEditors.CheckEdit chAmarelo;
        private DevExpress.XtraEditors.CheckEdit chVermelho;
        private DevExpress.XtraEditors.CheckEdit chCinza;
        private DevExpress.XtraEditors.CheckEdit chTESTE;
        private DevExpress.XtraEditors.TimeEdit timeEdit1;
        private Framework.Lookup.LookupGerente lookupGerente1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAuditor1_USU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource uSUariosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAuditor2_USU;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn colNumeroGrupo;
        private DevExpress.XtraEditors.SimpleButton BotCorrecao;
    }
}
