/*
MR - 28/04/2014 09:45            - Carregamento do combo de tipos de pagamento para PE com os tipos �teis (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 29/04/2014 15:25            - Cancelamento de pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***)
MR - 15/05/2014 11:45            - Permite cancelar um pagamento eletr�nico ap�s emitido (e n�o compensado), mesmo que n�o tenha sido confirmado o recebimento pelo banco (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (15/05/2014 11:45) ***)
MR - 22/07/2014 10:30            - Bot�o RECIBO invisivel para pagamentos eletr�nicos e BAIXA MANUAL invisivel (no form) para todos os tipos de pagamento (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (22/07/2014 10:30) ***)
MR - 30/07/2014 16:00            - Tratamento do novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 06/08/2014 13:00            - Tratamento do status de inconsistencia no pagamento eletr�nico nos pontos necessarios (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***)
MR - 12/08/2014 10:00            - Inclus�o de bot�o para visualiza��o de comprovante para pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO (12/08/2014 10:00) ***)
                                 - Apontamento de impress�o de comprovante de pagamento eletr�nico manual (Altera��es indicadas por *** MRC - INICIO (12/08/2014 10:00) ***)
MR - 27/04/2015 12:00            - Inclus�o dos campos com de gps associada e status do pagamento (PAG_GPS e PAGStatus) no grid de pagamentos do cheque (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
                                 - Bot�o de comprovantes habilitado para exibir comprovantes de guias de recolhimento eletr�nico (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
MR - 20/05/2015 17:00            - Carrega combo de PAGTipo com todos os valores mas n�o deixa editar para Guia e PEGuia (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***)
MR - 15/06/2015 11:30            - Desabilita copia de cheque para PEGuia (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (15/06/2015 11:30) ***)
MR - 18/08/2015 10:00            - Cancelamento tributo eletronico PEGuia / Notas (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***)
*/

using CompontesBasicosProc;
using dllChequesProc;
using Framework;
using FrameworkProc;
using System;
using System.Windows.Forms;
using VirEnumeracoesNeon;

namespace dllCheques
{
    /// <summary>
    /// Componente visivel que representa um cheque
    /// </summary>
    /// <remarks>Contem um objeto "Cheque"</remarks>
    public partial class cCheque : CompontesBasicos.ComponenteCamposBase, IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        private bool Alterado = false;

        private Cheque Cheque = null;

        private dCheque.CHEquesRow rowPrincipal;

        /// <summary>
        /// Construtor
        /// </summary>
        public cCheque() : this(null)
        {
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc"></param>
        public cCheque(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
            else
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CHE"></param>
        /// <param name="_EMPTProc"></param>
        public cCheque(int CHE, EMPTProc _EMPTProc = null) : this(_EMPTProc)
        {
            Cheque = new Cheque(CHE, dllCheques.Cheque.TipoChave.CHE, _EMPTProc);
            rowPrincipal = Cheque.CHErow;
            ChequebindingSource.DataSource = rowPrincipal.Table.DataSet;
            ChequebindingSource.Find("CHE", rowPrincipal.CHE);
            somenteleitura = true;
            ConfiguracaoInicial();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rowCheque">Linha mae</param>
        /// <param name="SomenteLeitura">Somente leitura</param>
        public cCheque(dCheque.CHEquesRow rowCheque, bool SomenteLeitura) : this()
        {
            //InitializeComponent();
            somenteleitura = SomenteLeitura;
            SetaCheque(rowCheque);
            ConfiguracaoInicial();
        }

        private void BBaixaManual_Click(object sender, EventArgs e)
        {
            string Justificativa = string.Empty;
            if (VirInput.Input.Execute("Justificativa:", ref Justificativa, true) && (Justificativa != null))
            {
                CHEStatus StatusAnterior = (CHEStatus)Cheque.CHErow.CHEStatus;
                Cheque.CHErow.CHEStatus = (int)CHEStatus.BaixaManual;
                Cheque.GravaObs(string.Format("O cheque n�o foi conciliado e foi baixado manualmente: STatus {0} -> {1}\r\nJustificativa:\r\n{2}\r\n",
                                             Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.Descritivo(StatusAnterior),
                                             Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.Descritivo(CHEStatus.BaixaManual),
                                             Justificativa));
                BBaixaManual.Enabled = false;
                ComboPAGTipoDefault.Properties.ReadOnly = false;
                VenctoDateEdit.Properties.ReadOnly = false;
                BotCEmissao.Enabled = false;
            }
        }

        //*** MRC - INICIO (12/08/2014 10:00) ***
        private void BotCComprovante_Click(object sender, EventArgs e)
        {
            Cheque.AbreComprovante();
            /*

            //Verifica diretorio dos comprovantes
            string DestinoPDF = dEstacao.dEstacaoSt.GetValor("Destino PDF");
            if (DestinoPDF == "")
            {
                DestinoPDF = @"\\email\volume_1\publicacoes\";
                dEstacao.dEstacaoSt.SetValor("Destino PDF", DestinoPDF);
            }

            //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
            //Verifica se e guia de recolhimento eletronico
            if (Cheque.Tipo == PAGTipo.PEGuia)
            {
                //Varre os pagamentos
                dCheque.PAGamentosRow[] PAGrows = Cheque.CHErow.GetPAGamentosRows();
                for (int i = 0; i < PAGrows.Length; i++)
                {
                    //Verifica se comprovante existe
                    if (!File.Exists(DestinoPDF + @"Financeiro\Recibos\Individual\recibo_tributo_" + PAGrows[i].PAG_GPS + ".pdf"))
                        MessageBox.Show(string.Format("Comprovante (Nota = {0} / Valor = {1}) n�o encontrado !!!", PAGrows[i].NOANumero, PAGrows[i].PAGValor.ToString("0.00")));
                    else
                    {
                        System.Diagnostics.Process.Start(DestinoPDF + @"Financeiro\Recibos\Individual\recibo_tributo_" + PAGrows[i].PAG_GPS + ".pdf");
                        if (MessageBox.Show(string.Format("Deseja apontar a impress�o do comprovante (Nota = {0} / Valor = {1})?", PAGrows[i].NOANumero, PAGrows[i].PAGValor.ToString("0.00")), "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            Framework.DSCentral.IMPressoesTableAdapter.Incluir(Cheque.CHErow.CHE_CON, 159, 1, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Tributo No " + PAGrows[i].PAG_GPS, false);
                            MessageBox.Show(string.Format("Apontadas: {0}", 1));
                        }
                    }
                }
            }
            else
            {
            //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                //Verifica se comprovante existe
                if (!File.Exists(DestinoPDF + @"Financeiro\Recibos\Individual\recibo_" + Cheque.CHErow.CHENumero + ".pdf"))
                    MessageBox.Show("Comprovante n�o encontrado !!!");
                else
                {
                    System.Diagnostics.Process.Start(DestinoPDF + @"Financeiro\Recibos\Individual\recibo_" + Cheque.CHErow.CHENumero + ".pdf");
                    if (MessageBox.Show("Deseja apontar a impress�o do comprovante?", "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        Framework.DSCentral.IMPressoesTableAdapter.Incluir(Cheque.CHErow.CHE_CON, 159, 1, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Pagamento No " + Cheque.CHErow.CHENumero, false);
                        MessageBox.Show(string.Format("Apontadas: {0}", 1));
                    }
                }
            //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
            }
            //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
            */
        }

        /*
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Cheque.CHErow.CHEStatus = (int)CHEStatus.Cancelado;
            Cheque.SalvarAlteracoes("Cancelado manualmente");
        }*/

        //*** MRC - INICIO - PAG-FOR (2) ***
        private void BotCCopia_Click(object sender, EventArgs e)
        //*** MRC - FIM - PAG-FOR (2) ***
        {
            Cheque.ImprimirCopia(true);
        }

        private void BotCEmissao_Click(object sender, EventArgs e)
        {
            bool RegistrarAviso = false;
            if (Cheque.Status == CHEStatus.AguardaConfirmacaoBancoPE)
            {
                if (MessageBox.Show("O pagemento foi enviado ao banco e n�o tivemos o retorno. Verifique se o pagamento foi feito ou agendado para evitar o pagamento em duplicidade.\r\nConfirma mesmo assim o cancelamento", "MUITA ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    RegistrarAviso = true;
                else
                    return;
            }
            string justificativa = string.Empty;
            bool CancelaEmissao = false;
            if ((Cheque.Tipo.EstaNoGrupo(PAGTipo.TrafArrecadado, PAGTipo.Conta, PAGTipo.cheque)) && (Cheque.CHErow.IsCHENumeroNull()))
            {
                VirInput.Input.Execute("Justificativa", ref justificativa);
                CancelaEmissao = false;
            }
            else
            {
                //*** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                //Verifica se esta cancelando cheque com guias de recolhimento eletr�nico
                if (Cheque.Tipo == PAGTipo.PEGuia)
                {
                    //Lista pagamentos para escolha do cancelamento     

                    //Se cancela individual ent�o quebra cheque e seta cheque gerado para cancelamento

                }

                fCancelaCheque FCan = new fCancelaCheque(Cheque.Tipo);
                if (FCan.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                    if (FCan.radioGroup1.EditValue != null)
                    {
                        CancelaEmissao = (bool)FCan.radioGroup1.EditValue;
                        justificativa = FCan.memoEdit1.Text;
                    }
            };
            if (justificativa.Trim().EstaNoGrupo(string.Empty, "."))
            {
                MessageBox.Show("Justificativa inv�lida");
                return;
            }
            if (RegistrarAviso)
                justificativa = string.Format("CANCELAMENTO SOLICITADO MESMO SEM O RETORNO DO BANCO\r\n{0}",justificativa);
            if (CancelaEmissao)
                Cheque.CancelarEmissao(justificativa);
            else
                Cheque.CancelarPagamento(justificativa);
            FechaTela(DialogResult.OK);
        }

        /*
        private void BotCcheque_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma o cancelamento do cheque?", "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                string Obs = "";
                if (VirInput.Input.Execute("Obs", ref Obs, true))
                {
                    Cheque.CancelarCheque(Obs);
                    FechaTela(DialogResult.OK);
                }
            }
        }*/

        //*** MRC - INICIO - PAG-FOR (2) ***
        private void BotCRecibo_Click(object sender, EventArgs e)
        //*** MRC - FIM - PAG-FOR (2) ***
        {
            Cheque.Recibo(string.Empty);
        }

        private void BotEletronico_Click(object sender, EventArgs e)
        {
            DateTime? Agendar = DateTime.Today.AddDays(1);
            decimal ValorOtras = 0;
            if (VirInput.Input.Execute("Agendar para:", ref Agendar, DateTime.Today, Cheque.CHErow.CHEVencimento)
                &&
                VirInput.Input.Execute("Valor de outras entidades", ref ValorOtras))
            {

                //if(!Framework.datasets.dFERiados.IsUtil(Agendar.Value,Framework.datasets.dFERiados.Sabados.naoUteis,Framework.datasets.dFERiados.Feriados.naoUteis))
                if (!Agendar.Value.IsUtil())
                {
                    MessageBox.Show("Data inv�lida");
                    return;
                }

                if (ValorOtras > Cheque.CHErow.CHEValor)
                {
                    MessageBox.Show("Valor inv�lido");
                    return;
                }

                Cheque Convertido = Cheque.ConverteEletronico2100();
                Convertido.EmiteEletrocnico2100(Agendar.Value, 0, true);
                FechaTela(DialogResult.OK);
            }
        }

        private void ButtonEletronico_Click(object sender, EventArgs e)
        {
            if (VirInput.Input.Senha("Senha").ToUpper() == "AZUL")
                Cheque.MudaTipo(PAGTipo.GuiaEletronica);
        }

        private dCheque dChequeAgrupar;

        private System.Collections.Generic.List<Cheque> _ChequesFundir;

        private System.Collections.Generic.List<Cheque> ChequesFundir => _ChequesFundir ?? (_ChequesFundir = new System.Collections.Generic.List<Cheque>());

        private void VerificaSeTemParaAgrupar()
        {
            dChequeAgrupar = new dCheque();
            dChequeAgrupar.CHEquesTableAdapter.FillParaIncluir(dChequeAgrupar.CHEques, Cheque.Vencimento, Cheque.CHErow.CHEFavorecido, Cheque.CON, (int)Cheque.Tipo);
            foreach (dCheque.CHEquesRow rowCaondidato in dChequeAgrupar.CHEques)
            {
                if (rowCaondidato.CHE != Cheque.CHE)
                {
                    ChequesFundir.Add(new Cheque(rowCaondidato.CHE));
                    SBAgrupar.Visible = true;
                }
            }
        }

        private void ConfiguracaoInicial()
        {
            colunaAlteracao = rowPrincipal.Table.Columns["CHEA_USU"];
            colunaDataAlteracao = rowPrincipal.Table.Columns["CHEDATAA"];
            colunaInclusao = rowPrincipal.Table.Columns["CHEI_USU"];
            colunaDataInclusao = rowPrincipal.Table.Columns["CHEDATAI"];
            PLAnocontasbindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt;
            Framework.Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(colPAGTipo);
            //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colPAGStatus);
            //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***

            switch (Cheque.Tipo)
            {
                case PAGTipo.PECreditoConta:
                case PAGTipo.PEOrdemPagamento:
                case PAGTipo.PETituloBancario:
                case PAGTipo.PETituloBancarioAgrupavel:
                case PAGTipo.PETransferencia:
                    Enumeracoes.VirEnumPAGTipoUtil.CarregaEditorDaGrid(ComboPAGTipoDefault);
                    BotCEmissao.Text = "Cancela Pagto";
                    BotCRecibo.Visible = false;
                    BotCComprovante.Visible = true;
                    break;
                case PAGTipo.Folha:
                    Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(ComboPAGTipoDefault);
                    ComboPAGTipoDefault.ReadOnly = true;
                    BotCEmissao.Text = "Cancela Cr�dito em conta";
                    BotCRecibo.Visible = true;
                    BotCComprovante.Visible = false;
                    break;
                case PAGTipo.PEGuia:
                    Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(ComboPAGTipoDefault);
                    BotCEmissao.Text = "Cancela Pagto Tributo";
                    BotCRecibo.Visible = false;
                    BotCComprovante.Visible = true;
                    break;
                case PAGTipo.Guia:
                    Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(ComboPAGTipoDefault);
                    BotCEmissao.Text = "Cancela Cheque";
                    BotCRecibo.Visible = true;
                    BotCComprovante.Visible = false;
                    break;
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
                case PAGTipo.cheque:
                case PAGTipo.sindico:
                case PAGTipo.boleto:
                case PAGTipo.Honorarios:
                case PAGTipo.GuiaEletronica:
                case PAGTipo.Caixinha:
                case PAGTipo.DebitoAutomatico:
                case PAGTipo.Acumular:
                case PAGTipo.deposito:
                default:
                    Enumeracoes.VirEnumPAGTipoSemPE.CarregaEditorDaGrid(ComboPAGTipoDefault);
                    BotCEmissao.Text = "Cancela Cheque";
                    BotCRecibo.Visible = true;
                    BotCComprovante.Visible = false;
                    break;
                case PAGTipo.TrafArrecadado:
                    Framework.Enumeracoes.VirEnumPAGTipoSemPE.CarregaEditorDaGrid(ComboPAGTipoDefault);
                    BotCEmissao.Text = "Canc. Tranf. Arrecad.";
                    BotCRecibo.Visible = false;
                    BotCComprovante.Visible = false;
                    break;
                case PAGTipo.Conta:
                    Framework.Enumeracoes.VirEnumPAGTipoSemPE.CarregaEditorDaGrid(ComboPAGTipoDefault);
                    BotCEmissao.Text = "Canc. Conta.";
                    BotCRecibo.Visible = false;
                    BotCComprovante.Visible = false;
                    break;
            }

            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(ComboStatus);
            TFavorecido.Properties.ReadOnly = true;
            ComboPAGTipoDefault.Properties.ReadOnly = true;
            VenctoDateEdit.Properties.ReadOnly = true;
            BotCEmissao.Enabled = false;            
            BotCCopia.Enabled = (!Cheque.CHErow.IsCHENumeroNull() && !Cheque.CHErow.IsCHEEmissaoNull() && !Cheque.CHErow.IsCHE_CCTNull() && Cheque.Tipo != PAGTipo.PEGuia);            
            if (BotCRecibo.Visible)                
                BotCRecibo.Enabled = (!Cheque.CHErow.IsCHENumeroNull() && !Cheque.CHErow.IsCHE_CCTNull());                        
            BotCComprovante.Enabled = false;            
            switch (Cheque.Status)
            {
                case CHEStatus.NaoEncontrado:
                    break;
                case CHEStatus.Cancelado:
                    break;
                case CHEStatus.Cadastrado:
                    TFavorecido.Properties.ReadOnly = somenteleitura;                    
                    ComboPAGTipoDefault.Properties.ReadOnly = somenteleitura;
                    VenctoDateEdit.Properties.ReadOnly = somenteleitura;
                    //if((Cheque.Tipo == PAGTipo.TrafArrecadado) || (Cheque.Tipo == PAGTipo.Conta))
                    BotCEmissao.Enabled = true;
                    if (!somenteleitura)
                    {
                        VerificaSeTemParaAgrupar();
                        BotEletronico.Visible = Cheque.Eh2100Puro();
                    }
                    break;
                case CHEStatus.AguardaRetorno:
                case CHEStatus.Aguardacopia:
                case CHEStatus.Retornado:
                case CHEStatus.EnviadoBanco:
                case CHEStatus.Retirado:
                case CHEStatus.ComSindico:
                    if (Cheque.Tipo == PAGTipo.Folha)
                        ComboPAGTipoDefault.Properties.ReadOnly = true;
                    else
                        ComboPAGTipoDefault.Properties.ReadOnly = somenteleitura;
                    VenctoDateEdit.Properties.ReadOnly = somenteleitura;
                    BotCEmissao.Enabled = true;
                    BBaixaManual.Enabled = true;
                    break;                
                case CHEStatus.AguardaEnvioPE:
                case CHEStatus.AguardaConfirmacaoBancoPE:
                    ComboPAGTipoDefault.Properties.ReadOnly = somenteleitura;
                    VenctoDateEdit.Properties.ReadOnly = somenteleitura;
                    //BotCEmissao.Enabled = Cheque.Tipo != PAGTipo.PEGuia ? true : false;
                    BotCEmissao.Enabled = true;
                    BBaixaManual.Enabled = true;
                    break;                
                case CHEStatus.PagamentoNaoAprovadoSindicoPE:                
                case CHEStatus.PagamentoInconsistentePE:
                case CHEStatus.PagamentoNaoEfetivadoPE:
                    ComboPAGTipoDefault.Properties.ReadOnly = somenteleitura;
                    VenctoDateEdit.Properties.ReadOnly = somenteleitura;
                    BotCEmissao.Enabled = true;
                    BBaixaManual.Enabled = false;
                    break;
                //*** MRC - TERMINO - PAG-FOR (29/04/2014 15:25) ***
                case CHEStatus.Compensado:
                //*** MRC - INICIO - PAG-FOR (2) ***
                case CHEStatus.PagamentoConfirmadoBancoPE:
                    //*** MRC - FIM - PAG-FOR (2) ***
                    //*** MRC - INICIO (12/08/2014 10:00) ***
                    if (BotCComprovante.Visible)
                        BotCComprovante.Enabled = true;
                    //*** MRC - TERMINO (12/08/2014 10:00) ***                
                    break;
                case CHEStatus.CompensadoAguardaCopia:
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dCheque.PAGamentosRow rowPag = (dCheque.PAGamentosRow)gridView1.GetFocusedDataRow();
            CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("indefinido 1");
            if (rowPag != null)
                CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos("ContaPagar.cNotasCampos", "NOTA", rowPag.PAG_NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("indefinido 2");
            //string relatorio = CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();
            //MessageBox.Show(relatorio);
        }

        private void SetaCheque(dCheque.CHEquesRow rowCheque)
        {
            rowPrincipal = rowCheque;
            Cheque = new Cheque(rowCheque);
            ChequebindingSource.DataSource = rowCheque.Table.DataSet;
            ChequebindingSource.Find("CHE", rowCheque.CHE);
            //bOLetosBindingSource.DataSource = rowBoleto.Table;
            //bOLetosBindingSource.Position = bOLetosBindingSource.Find("BOL", rowBoleto.BOL);            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string Obs = string.Empty;
            if (VirInput.Input.Execute("Obs", ref Obs, true))
                Cheque.GravaObs(Obs);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            Console.WriteLine("Fechando");
            return base.CanClose();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            ChequebindingSource.EndEdit();
            if (!Validate(true))
                return false;
            if (VirDB.VirtualTableAdapter.EfetivamenteAlterados(rowPrincipal))
                Alterado = true;
            foreach (dCheque.PAGamentosRow PAGRow in rowPrincipal.GetPAGamentosRows())
                if (VirDB.VirtualTableAdapter.EfetivamenteAlterados(PAGRow))
                    Alterado = true;

            if (!Alterado)
                return true;
            else
                if (base.Update_F())
            {
                string Obs = string.Empty;
                if (!VirInput.Input.Execute("Obs", ref Obs, true))
                    return false;
                if (Cheque.SalvarAlteracoes(Obs))
                    return true;
                else
                {
                    MessageBox.Show("Erro ao gravar.\r\nAltera��o cancelada\r\nErro:" + Cheque.UltimaException.Message);
                    return true;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        private void SBAgrupar_Click(object sender, EventArgs e)
        {
            bool fechar = false;
            foreach (Cheque ChequeSomar in ChequesFundir)
                if (Cheque.AgruparRecebe(ChequeSomar))
                    fechar = true;
            if (fechar)
                FechaTela(DialogResult.OK);
        }
    }
}

