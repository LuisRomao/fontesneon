﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using VirEnumeracoesNeon;

namespace dllCheques
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cFolhasDeCheque : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cFolhasDeCheque()
        {
            InitializeComponent();
            virEnumFCHStatus.virEnumFCHStatusSt.CarregaEditorDaGrid(colFCHStatus);
            dUSUariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosStTodos;
        }

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            simpleButton1.Enabled = false;
            TravaCH = true;
            dcFolhasDeCheque.FolhasCHeque.Clear();
            dcFolhasDeCheque.ContaCorrenTe.Clear();
            if (lookupCondominio_F1.CON_sel != -1)
            {
                simpleButton1.Enabled = true;
                dcFolhasDeCheque.ContaCorrenTeTableAdapter.Fill(dcFolhasDeCheque.ContaCorrenTe, lookupCondominio_F1.CON_sel);
                foreach (dcFolhasDeCheque.ContaCorrenTeRow rowCCT in dcFolhasDeCheque.ContaCorrenTe)
                    if (rowCCT.IsCCTTituloNull())
                    {
                        rowCCT.CCTTitulo = "Conta Corrente";
                        dcFolhasDeCheque.ContaCorrenTeTableAdapter.Update(rowCCT);
                        rowCCT.AcceptChanges();
                    }
                dcFolhasDeCheque.FolhasCHequeTableAdapter.FillBy(dcFolhasDeCheque.FolhasCHeque, lookupCondominio_F1.CON_sel);
                checkedListBoxControl1.CheckAll();
            }
            TravaCH = false;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string comandoTestaCheque = "select CHE from cheques where CHE_CCT = @P1 and CHENumero = @P2 and CHESTATUS <> 14";
            //SortedList<int, string> Alternativas = new SortedList<int, string>();
            SortedList Alternativas = new SortedList();
            foreach (dcFolhasDeCheque.ContaCorrenTeRow rowCCT in dcFolhasDeCheque.ContaCorrenTe)
                Alternativas.Add(rowCCT.CCT, rowCCT.Descritivo);
            int CCT;
            if (!VirInput.Input.Execute("Conta", Alternativas, out CCT))
                return;
            dcFolhasDeCheque.ContaCorrenTeRow CCTrow = dcFolhasDeCheque.ContaCorrenTe.FindByCCT(CCT);
            int PrimeiroNumero = 0;
            foreach (dcFolhasDeCheque.FolhasCHequeRow FCHrow in CCTrow.GetFolhasCHequeRows())
                if (PrimeiroNumero <= FCHrow.FCHNumero)
                    PrimeiroNumero = FCHrow.FCHNumero + 1;
            int UltimoNumero;
            if (!VirInput.Input.Execute("Primeiro cheque do talão", out PrimeiroNumero, PrimeiroNumero))
                return;
            UltimoNumero = PrimeiroNumero + 19;
            if(!VirInput.Input.Execute("Último cheque do talão",out UltimoNumero,UltimoNumero))
                return;
            int numerodefolhas = UltimoNumero - PrimeiroNumero +1;
            if (UltimoNumero - PrimeiroNumero > 100)
            {
                MessageBox.Show(string.Format("número máximo excedido. Não é permitido o castro de mais de 60 cheques de uma só vez."));
                return;
            }
            if (MessageBox.Show(string.Format("Confirma o cadasto de {0} folhas de cheque?", numerodefolhas), "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;
            for (int iteste = PrimeiroNumero; iteste <= UltimoNumero; iteste++)
                if (VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado(comandoTestaCheque, CCT, iteste))
                {
                    MessageBox.Show(string.Format("Erro. Cheque {0} já cadastrado", iteste));
                    return;
                }
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllCheques cFolhasDeCheque - 81",dcFolhasDeCheque.FolhasCHequeTableAdapter);
                for (int i = PrimeiroNumero; i <= UltimoNumero; i++)
                {                    
                    dcFolhasDeCheque.FolhasCHequeRow novaFCH = dcFolhasDeCheque.FolhasCHeque.AddFolhasCHequeRow(DateTime.Now,
                                                                                 Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU,
                                                                                 i,
                                                                                 (int)FCHStatus.Talao,                                                                                 
                                                                                 CCTrow,
                                                                                 string.Format("{0:dd/MM/yyyy HH:mm} - Folha de cheque cadastrada por {1}\r\n", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome));
                    dcFolhasDeCheque.FolhasCHequeTableAdapter.Update(novaFCH);
                };
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                MessageBox.Show("Erro no cadastro");
                dcFolhasDeCheque.FolhasCHeque.Clear();
                dcFolhasDeCheque.ContaCorrenTe.Clear();
                throw (ex);
            } 
        }

        private bool TravaCH;

        private void ajustaFiltro()
        {
            if (checkedListBoxControl1.CheckedItems.Count != checkedListBoxControl1.ItemCount)
            {
                string filtro = "";
                foreach (DataRowView DR in checkedListBoxControl1.CheckedItems)
                {
                    dcFolhasDeCheque.ContaCorrenTeRow rowch = (dcFolhasDeCheque.ContaCorrenTeRow)DR.Row;
                    filtro += string.Format("{0}(FCH_CCT = {1})", filtro == "" ? "" : " OR ", rowch.CCT);
                };
                gridView1.ActiveFilterString = filtro;
            }
            else
                gridView1.ActiveFilterString = "";
        }

        private void checkedListBoxControl1_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            if (!TravaCH)
                ajustaFiltro();
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column != colFCHStatus)
                return;
            dcFolhasDeCheque.FolhasCHequeRow row = (dcFolhasDeCheque.FolhasCHequeRow)gridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            e.Appearance.BackColor = virEnumFCHStatus.virEnumFCHStatusSt.GetCor(row.FCHStatus);
            e.Appearance.ForeColor = Color.Black;
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            BotSelSin.Enabled = BotSelCan.Enabled = (gridView1.GetSelectedRows().Length != 0);
        }
        
        private void BotSelCan_Click(object sender, EventArgs e)
        {
            int nsel = gridView1.GetSelectedRows().Length;
            if (nsel <= 0)
                return;
            string Justificativa = "";
            if (!VirInput.Input.Execute("Justificativa:", ref Justificativa, true))
                return;
            Justificativa = string.Format("{0:dd/MM/yyyy HH:mm} - Folha de cheque cancelada por {1}\r\nJustificativa\r\n{2}\r\n",
                                                     DateTime.Now,
                                                     Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                     Justificativa);
            string comandocadastracancelado = "INSERT INTO CHEques (CHE_CON, CHENumero, CHEValor, CHEVencimento, CHEDATAI, CHEI_USU, CHEObs, CHE_CCT, CHETipo, CHEStatus) VALUES (@P1,@P2, 0,@P3,@P4,@P5,@P6,@P7,@P8,@P9)";
            if (MessageBox.Show(string.Format("Confirma o cancelamento de {0} folha(s) de cheque ?", nsel), "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllCheques cFolhasDeCheque - 163", dcFolhasDeCheque.FolhasCHequeTableAdapter);
                    foreach (int i in gridView1.GetSelectedRows())
                    {
                        
                        dcFolhasDeCheque.FolhasCHequeRow rowMatar = (dcFolhasDeCheque.FolhasCHequeRow)gridView1.GetDataRow(i);
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandocadastracancelado,
                                                                        lookupCondominio_F1.CON_sel,
                                                                        rowMatar.FCHNumero,
                                                                        DateTime.Now,
                                                                        DateTime.Now,
                                                                        Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU,
                                                                        string.Format("{0}{1}",rowMatar.FCHHistorico,Justificativa),
                                                                        rowMatar.FCH_CCT,
                                                                        (int)PAGTipo.cheque,
                                                                        (int)CHEStatus.Cancelado);
                        rowMatar.Delete();
                    }
                    dcFolhasDeCheque.FolhasCHequeTableAdapter.Update(dcFolhasDeCheque.FolhasCHeque);
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);                    
                    throw (ex);
                } 
            }
        }

        private void BotSelSin_Click(object sender, EventArgs e)
        {
            int nsel = gridView1.GetSelectedRows().Length;
            if (nsel <= 0)
                return;
            SortedList Alternativas = new SortedList();
            Alternativas.Add((int)FCHStatus.Sindico, "Cheque em poder do síndico");
            Alternativas.Add((int)FCHStatus.AssinadaParcial, "Cheque com uma assinatura (falta outra)");
            Alternativas.Add((int)FCHStatus.Assinada, "Cheque assinado");
            Alternativas.Add((int)FCHStatus.Talao, "Cheque com a Neon");
            int iNovoStatus;
            if (!VirInput.Input.Execute("O novo status dos cheques é:", Alternativas, out iNovoStatus))
                return;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllCheques cFolhasDeCheque - 206", dcFolhasDeCheque.FolhasCHequeTableAdapter);
                foreach (int i in gridView1.GetSelectedRows())
                {                    
                    dcFolhasDeCheque.FolhasCHequeRow rowAlterar = (dcFolhasDeCheque.FolhasCHequeRow)gridView1.GetDataRow(i);
                    if (rowAlterar.FCHStatus == iNovoStatus)
                        continue;
                    string Justificativa = string.Format("{0:dd/MM/yyyy HH:mm} - Status da folha de cheque alterado por {1}\r\n     De \"{2}\" para \"{3}\"\r\n",
                                                 DateTime.Now,
                                                 Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                 virEnumFCHStatus.virEnumFCHStatusSt.Descritivo(rowAlterar.FCHStatus),
                                                 virEnumFCHStatus.virEnumFCHStatusSt.Descritivo(iNovoStatus));
                    rowAlterar.FCHStatus = iNovoStatus;
                    rowAlterar.FCHHistorico += Justificativa;
                }
                dcFolhasDeCheque.FolhasCHequeTableAdapter.Update(dcFolhasDeCheque.FolhasCHeque);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                throw (ex);
            } 

        }
    }
}
