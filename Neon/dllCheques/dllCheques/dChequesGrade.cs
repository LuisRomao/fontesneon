﻿namespace dllCheques {


    partial class dChequesGrade
    {
        private dllCheques.dChequesGradeTableAdapters.ChPendentesTableAdapter chPendentesTableAdapter;
        private dllCheques.dChequesGradeTableAdapters.PagamentosTableAdapter pagamentosTableAdapter;

        public dllCheques.dChequesGradeTableAdapters.PagamentosTableAdapter PagamentosTableAdapter {
            get {
                if (pagamentosTableAdapter == null) {
                    pagamentosTableAdapter = new dllCheques.dChequesGradeTableAdapters.PagamentosTableAdapter();
                    pagamentosTableAdapter.TrocarStringDeConexao();
                };
                return pagamentosTableAdapter;
            }
        }

        public dllCheques.dChequesGradeTableAdapters.ChPendentesTableAdapter ChPendentesTableAdapter
        {
            get
            {
                if (chPendentesTableAdapter == null)
                {
                    chPendentesTableAdapter = new dllCheques.dChequesGradeTableAdapters.ChPendentesTableAdapter();
                    chPendentesTableAdapter.TrocarStringDeConexao();
                };
                return chPendentesTableAdapter;
            }
        }
    }
}
