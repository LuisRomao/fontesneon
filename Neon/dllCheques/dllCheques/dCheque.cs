﻿/*
MR - 28/04/2014 09:45 - 13.2.8.34 - Criacao de nova enumeracao para Tipos de Pagamento (Todos os úteis - para exibição nos combos) (Alterações indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 30/07/2014 16:00             - Inclusão de novo tipo de pagamento eletrônico para titulo agrupável - varias notas um único boleto (Alterações indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 05/08/2014 11:00             - Inclusão da funcao (VerificaCONCnpjPE) que verifica o CNPJ a ser enviado no pagamento eletrônico de acordo com o tipo de conta do condominio no DadosComplementaresTableAdapter
LH - 25/12/2014 14:20 - 14.1.4.96 - Pagamento com boleto integrado na mesma tela de emissão de cheque
MR - 10/02/2015 12:00             - Inclusão do campo com tipo de envio de arquivos para envio de remessa de pagamento eletronico (CONRemessa_TEA) no DadosComplementaresTableAdapter
MR - 27/04/2015 12:00             - Pagamento de tributo eletronico com inclusao do "guia de recolhimento - eletrônico" (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
                                  - Inclusão dos campos com de gps associada e status do pagamento para controle de pagamento de tributo eletronico e quebra de cheque se necessario (PAG_GPS e PAGStatus) no PAGamentosTableAdapter
                                  - Inclusão de query SetaGPS para setar campos de controle de emissão de "guia de recolhimento - eletrônico", no PAGamentosTableAdapter
MR - 20/05/2015 17:00             - Inclusão do ContaCorrenTeDepositoTableAdapter para busca de conta para depósito no caso de PECreditoConta (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***)
MR - 05/01/2016 13:30             - Inclusão do campo FRNContaTipoCredito nos Fills do FORNECEDORESTableAdapter
 */

using System;
using dllVirEnum;
using Framework;
using System.Collections.Generic;
using VirDB.Bancovirtual;
using FrameworkProc;

namespace dllCheques
{

    partial class dCheque
    {
        /*
        TRANFERIDO para FrameworkProc
        /// <summary>
        /// Tipos de Pagamento (Todos)
        /// </summary>
        public enum PAGTipo
        {
            /// <summary>
            /// Cheque a ser retirado na Neon
            /// </summary>
            cheque = 0,
            /// <summary>
            /// O sindigo efetua o pagamento
            /// </summary>
            sindico = 1,
            /// <summary>
            /// Boleto bancário
            /// </summary>
            boleto = 2,
            /// <summary>
            /// Será feito depósito
            /// </summary>
            deposito = 3,
            //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
            /// <summary>
            /// Guia de Recolhimento (Cheque)
            /// </summary>
            Guia = 4,
            //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
            /// <summary>
            /// Honorarios
            /// </summary>
            Honorarios = 5,
            /// <summary>
            /// Guia Eletrônica Folha
            /// </summary>
            GuiaEletronica = 6,
            /// <summary>
            /// Caixinha
            /// </summary>
            Caixinha = 7,
            /// <summary>
            /// Debito automático (não emite cheque)
            /// </summary>
            DebitoAutomatico = 8,
            /// <summary>
            /// Conta (emite cheque)
            /// </summary>
            Conta = 9,
            /// <summary>
            /// Conta com valor para acumular
            /// </summary>
            Acumular = 10,
            /// <summary>
            /// Crédito em conta corrente ou poupanca - Pagamento Eletrônico
            /// </summary>
            PECreditoConta = 20,
            /// <summary>
            /// Transferência (DOC ou TED) - Pagamento Eletrônico
            /// </summary>
            PETransferencia = 21,
            /// <summary>
            /// Ordem de Pagamento - Pagamento Eletrônico
            /// </summary>
            PEOrdemPagamento = 22,
            /// <summary>
            /// Titulo Bancário (Boleto) - Pagamento Eletrônico
            /// </summary>
            PETituloBancario = 23,
            //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
            /// <summary>
            /// Titulo Bancário Agrupável (Várias notas, um único boleto) - Pagamento Eletrônico
            /// </summary>
            PETituloBancarioAgrupavel = 24,
            /// <summary>
            /// Trasnferencia física de valor arrecadado
            /// </summary>
            TrafArrecadado = 25,
            /// <summary>
            /// Cheque emitido direto pelo síndico
            /// </summary>
            ChequeSindico = 26,            
            /// <summary>
            /// Guia de Recolhimento (Pagamento Eletrônico)
            /// </summary>
            PEGuia = 27,
            /// <summary>
            /// Folha de pagamento (pode virar cheque)
            /// </summary>
            Folha = 28
        }
        */

        static private List<PAGTipo> _PAGTiposEletronicos;

        /// <summary>
        /// Tipos de pagamento eletrônicos
        /// </summary>
        static public List<PAGTipo> PAGTiposEletronicos
        {
            get
            {
                if(_PAGTiposEletronicos == null)
                {
                    _PAGTiposEletronicos = new List<PAGTipo>(new PAGTipo[] { PAGTipo.PECreditoConta,
                                                                             PAGTipo.PEOrdemPagamento,
                                                                             PAGTipo.PETituloBancario,
                                                                             PAGTipo.PETituloBancarioAgrupavel,
                                                                             PAGTipo.PETransferencia,
                                                                             //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                                                                             PAGTipo.PEGuia });
                                                                             //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                }
                return _PAGTiposEletronicos;
            }
        }

        private EMPTProc _EMPTipo;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTipo
        {
            get
            {
                if (_EMPTipo == null)
                    _EMPTipo = new EMPTProc(EMPTProc.EMPTipo.Local);
                return _EMPTipo;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTipo"></param>
        public dCheque(EMPTProc.EMPTipo _EMPTipo)
            : this()
        {
            this._EMPTipo = new EMPTProc(_EMPTipo);
        }

        private VirEnum virEnumPAGTipo;

        /// <summary>
        /// VirEnum para PAGTipo (Todos)
        /// </summary>
        public VirEnum VirEnumPAGTipo
        {
            get
            {
                if (virEnumPAGTipo == null)
                {
                    virEnumPAGTipo = new VirEnum(typeof(PAGTipo));
                    virEnumPAGTipo.GravaNomes(PAGTipo.boleto, "Boleto bancário");
                    virEnumPAGTipo.GravaNomes(PAGTipo.cheque, "Carteira");
                    virEnumPAGTipo.GravaNomes(PAGTipo.deposito, "Depósito em conta");
                    virEnumPAGTipo.GravaNomes(PAGTipo.sindico, "Será pago pelo síndico");
                    //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    virEnumPAGTipo.GravaNomes(PAGTipo.Guia, "Guia de recolhimento - Cheque");
                    virEnumPAGTipo.GravaNomes(PAGTipo.PEGuia, "Guia de recolhimento - Eletrônico");
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    virEnumPAGTipo.GravaNomes(PAGTipo.Honorarios, "Honorários");
                    virEnumPAGTipo.GravaNomes(PAGTipo.Conta, "Conta(água, luz etc)");
                    virEnumPAGTipo.GravaNomes(PAGTipo.GuiaEletronica, "Tributo eletrônico (Guia)");
                    virEnumPAGTipo.GravaNomes(PAGTipo.DebitoAutomatico, "Débito automático");
                    virEnumPAGTipo.GravaNomes(PAGTipo.PECreditoConta, "Pagamento Eletrônico - Crédito em Conta (Corrente ou Poupança)");
                    virEnumPAGTipo.GravaNomes(PAGTipo.PETransferencia, "Pagamento Eletrônico - Transferência (DOC ou TED)");
                    virEnumPAGTipo.GravaNomes(PAGTipo.PEOrdemPagamento, "Pagamento Eletrônico - Ordem de Pagamento");
                    virEnumPAGTipo.GravaNomes(PAGTipo.PETituloBancario, "Pagamento Eletrônico - Título Bancário");                    
                    virEnumPAGTipo.GravaNomes(PAGTipo.PETituloBancarioAgrupavel, "Pagamento Eletrônico - Título Bancário Agrupável");
                    virEnumPAGTipo.GravaNomes(PAGTipo.TrafArrecadado, "Transferencia física de valor arrecadado");
                    virEnumPAGTipo.GravaNomes(PAGTipo.ChequeSindico, "Cheque Síndico");
                }
                return virEnumPAGTipo;
            }
        }
  
        private VirEnum virEnumPAGTipoUtil;

        //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
        /// <summary>
        /// VirEnum para PAGTipo (Todos os úteis - para exibição nos combos)
        /// </summary>
        public VirEnum VirEnumPAGTipoUtil
        {
           get
           {
              if (virEnumPAGTipoUtil == null)
              {
                 virEnumPAGTipoUtil = new VirEnum(typeof(PAGTipo));
                 virEnumPAGTipoUtil.CarregarDefault = false;
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.boleto, "Boleto bancário");
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.cheque, "Carteira");
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.deposito, "Depósito em conta");
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.sindico, "Será pago pelo síndico");
                 //virEnumPAGTipoUtil.GravaNomes(PAGTipo.Guia, "Guia de recolhimento");
                 //virEnumPAGTipoUtil.GravaNomes(PAGTipo.Honorarios, "Honorários");
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.Conta, "Conta(água, luz etc)");
                 //virEnumPAGTipoUtil.GravaNomes(PAGTipo.GuiaEletronica, "Tributo eletrônico (Guia)");
                 //virEnumPAGTipoUtil.GravaNomes(PAGTipo.DebitoAutomatico, "Débito automático");
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.PECreditoConta, "Pagamento Eletrônico - Crédito em Conta (Corrente ou Poupança)");
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.PETituloBancario, "Pagamento Eletrônico - Título Bancário");                 
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.PETituloBancarioAgrupavel, "Pagamento Eletrônico - Título Bancário Agrupável");
                 //virEnumPAGTipoUtil.GravaNomes(PAGTipo.TrafArrecadado, "Transferencia física de valor arrecadado");
                 virEnumPAGTipoUtil.GravaNomes(PAGTipo.ChequeSindico, "Cheque Síndico");
              }
              return virEnumPAGTipoUtil;
           }
        }
        //*** MRC - TERMINO - PAG-FOR (28/04/2014 09:45) ***

        private VirEnum virEnumPAGTipoSemPE;

        /// <summary>
        /// VirEnum para PAGTipo (sem Pagamento Eletrônico)
        /// </summary>
        public VirEnum VirEnumPAGTipoSemPE
        {
            get
            {
                if (virEnumPAGTipoSemPE == null)
                {                    
                    virEnumPAGTipoSemPE = new VirEnum(typeof(PAGTipo));
                    virEnumPAGTipoSemPE.CarregarDefault = false;
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.boleto, "Boleto bancário");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.cheque, "Carteira");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.deposito, "Depósito em conta");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.sindico, "Será pago pelo síndico");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.Guia, "Guia de recolhimento");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.Honorarios, "Honorários");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.Conta, "Conta(água, luz etc)");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.GuiaEletronica, "Tributo eletrônico (Guia)");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.DebitoAutomatico, "Débito automático");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.TrafArrecadado, "Transferencia física de valor arrecadado");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.ChequeSindico, "Cheque Síndico");
                }
                return virEnumPAGTipoSemPE;
            }
        }
                
        private static dCheque _dChequeSt;

        /// <summary>
        /// dataset estático:dCheque
        /// </summary>
        public static dCheque dChequeSt
        {
            get
            {
                if (_dChequeSt == null)
                    _dChequeSt = new dCheque();
                return _dChequeSt;
            }
        }        

        /// <summary>
        /// Esta variável permite que a classe emitas messagebox e só deve ser setada para true por telas assistindas por usuários (nunca por processos)
        /// </summary>
        public bool PerminteDialogo;

        #region Table Adapters

        private dChequeTableAdapters.ImpostosRetidosTableAdapter impostosRetidosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ImpostosRetidos
        /// </summary>
        public dChequeTableAdapters.ImpostosRetidosTableAdapter ImpostosRetidosTableAdapter
        {
            get
            {
                if (impostosRetidosTableAdapter == null)
                {
                    impostosRetidosTableAdapter = new dChequeTableAdapters.ImpostosRetidosTableAdapter();
                    impostosRetidosTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return impostosRetidosTableAdapter;
            }
        }

        private dChequeTableAdapters.Lancamentos_BradescoTableAdapter lancamentos_BradescoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Lancamentos_Bradesco
        /// </summary>
        public dChequeTableAdapters.Lancamentos_BradescoTableAdapter Lancamentos_BradescoTableAdapter
        {
            get
            {
                if (lancamentos_BradescoTableAdapter == null)
                {
                    lancamentos_BradescoTableAdapter = new dChequeTableAdapters.Lancamentos_BradescoTableAdapter();
                    lancamentos_BradescoTableAdapter.TrocarStringDeConexao(TiposDeBanco.Access);
                };
                return lancamentos_BradescoTableAdapter;
            }
        }

        private dChequeTableAdapters.CHEquesTableAdapter cHEquesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CHEques
        /// </summary>
        public dChequeTableAdapters.CHEquesTableAdapter CHEquesTableAdapter
        {
            get
            {
                if (cHEquesTableAdapter == null)
                {
                    cHEquesTableAdapter = new dChequeTableAdapters.CHEquesTableAdapter();
                    cHEquesTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return cHEquesTableAdapter;
            }
        }

        private dChequeTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dChequeTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dChequeTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return pAGamentosTableAdapter;
            }
        }

        /*
        private dChequeTableAdapters.DadosComplementaresTableAdapter dadosComplementaresTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: DadosComplementares
        /// </summary>
        public dChequeTableAdapters.DadosComplementaresTableAdapter DadosComplementaresTableAdapter
        {
            get
            {
                if (dadosComplementaresTableAdapter == null)
                {
                    dadosComplementaresTableAdapter = new dChequeTableAdapters.DadosComplementaresTableAdapter();
                    dadosComplementaresTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return dadosComplementaresTableAdapter;
            }
        }*/

        private dChequeTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dChequeTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dChequeTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return contaCorrenTeTableAdapter;
            }
        }

        //*** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
        private dChequeTableAdapters.ContaCorrenTeDepositoTableAdapter contaCorrenTeDepositoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTeDeposito
        /// </summary>
        public dChequeTableAdapters.ContaCorrenTeDepositoTableAdapter ContaCorrenTeDepositoTableAdapter
        {
            get
            {
                if (contaCorrenTeDepositoTableAdapter == null)
                {
                    contaCorrenTeDepositoTableAdapter = new dChequeTableAdapters.ContaCorrenTeDepositoTableAdapter();
                    contaCorrenTeDepositoTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return contaCorrenTeDepositoTableAdapter;
            }
        }
        //*** MRC - TERMINO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***

        private dChequeTableAdapters.FolhasCHequeTableAdapter folhasCHequeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FolhasCHeque
        /// </summary>
        public dChequeTableAdapters.FolhasCHequeTableAdapter FolhasCHequeTableAdapter
        {
            get
            {
                if (folhasCHequeTableAdapter == null)
                {
                    folhasCHequeTableAdapter = new dChequeTableAdapters.FolhasCHequeTableAdapter();
                    folhasCHequeTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return folhasCHequeTableAdapter;
            }
        }

        #endregion
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CHE"></param>
        /// <returns></returns>
        public bool ApagarCheque(int CHE)
        {
            if (CHEquesTableAdapter.FillByCHE(CHEques, CHE) != 1)
                return false;

            CHEquesRow Cheque = CHEques[0];

            if (!Cheque.IsCHEantigoNull())
            {
                string ComandoDel1 = "delete from Pagamentos where contador = @P1";
                string ComandoDel2 = "delete from cheques where contadorcheque = @P1";
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoDel1, Cheque.CHEantigo);
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoDel2, Cheque.CHEantigo);
            };
                        
            return true;
        }        
    }
}
