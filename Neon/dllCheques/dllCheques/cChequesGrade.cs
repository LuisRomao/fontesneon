using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace dllCheques
{
    public partial class cChequesGrade : CompontesBasicos.ComponenteBaseBindingSource
    {
        public cChequesGrade()
        {
            InitializeComponent();
            DataI.DateTime = DateTime.Today;
            DataF.DateTime = DateTime.Today;
        }

        private void recalcular() {
            if (radioGroup1.EditValue != null)
                switch ((int)radioGroup1.EditValue)
                {
                    case 0:
                        dChequesGrade.ChPendentesTableAdapter.Fill(dChequesGrade.ChPendentes, DateTime.Today);
                        dChequesGrade.PagamentosTableAdapter.Fill(dChequesGrade.Pagamentos);
                        break;
                    case 1:
                        dChequesGrade.ChPendentesTableAdapter.FillByPeriodo(dChequesGrade.ChPendentes, DataI.DateTime, DataF.DateTime);
                        dChequesGrade.PagamentosTableAdapter.FillByPeriodo(dChequesGrade.Pagamentos, DataI.DateTime, DataF.DateTime);
                        break;
                    case 2:
                        break;
                };
        }

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            recalcular();    
            
        }

        private dllCheques.dChequesGrade.ChPendentesRow LinhaMae;

        //private bool EmitirCh(){
        
        //}

        private cChequesEmitir _Emissor;

        private cChequesEmitir Emissor {
            get {
                if (_Emissor == null)
                    _Emissor = new cChequesEmitir(true);
                return _Emissor;
            }
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            string CorpoDoEmail="";
            dllCheques.dImpNotaDeb.EscritorioRow rowFRN;
            dllCheques.dImpNotaDeb.SacadoRow rowCON;
            if (gridView1.GetSelectedRows().Length == 0)
                if (gridView1.FocusedRowHandle == -1)
                {
                    MessageBox.Show("Nenhum cheque selecionado");
                    return;
                }
                else
                    gridView1.SelectRow(gridView1.FocusedRowHandle);
            dllCheques.ImpNotaDeb Imp = new dllCheques.ImpNotaDeb();
            foreach (int i in gridView1.GetSelectedRows())
            {
                DataRowView DRV = (DataRowView)gridView1.GetRow(i);
                LinhaMae = (dllCheques.dChequesGrade.ChPendentesRow)DRV.Row;
                

                if (!LinhaMae.Emitido)
                    if (!Emissor.Imprimir(LinhaMae.ContadorCheque))
                        continue;
                    else
                    {
                        LinhaMae.Favorecido = Emissor.UltimoFavorecido;
                        LinhaMae.Emitido = true;
                    }
                
                DataRow DadosCON = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select CON,CONEscrit_FRN from condominios where CONCodigo = @P1", LinhaMae.Codcon);
                if (DadosCON != null)
                {
                    if ((!LinhaMae.IsFavorecidoNull()) && (LinhaMae.Favorecido != "")) 
                    {
                        if (Imp.dImpNotaDeb.EscritorioTableAdapter.FillByNome(Imp.dImpNotaDeb.Escritorio, LinhaMae.Favorecido) == 0)
                        {
                            MessageBox.Show(LinhaMae.Favorecido + " n�o � um escrit�rio de advocacia cadastrado");
                            continue;
                        }
                    }
                    else
                    {
                        if (DadosCON["CONEscrit_FRN"] == DBNull.Value)
                        {
                            MessageBox.Show("Escrit�rio de advocacia n�o cadastrado");
                            continue;
                        };
                        if (Imp.dImpNotaDeb.EscritorioTableAdapter.Fill(Imp.dImpNotaDeb.Escritorio, (int)DadosCON["CONEscrit_FRN"]) == 0)
                            continue;
                    }
                    
                    
                    rowFRN = Imp.dImpNotaDeb.Escritorio[0];
                    if (Imp.dImpNotaDeb.SacadoTableAdapter.Fill(Imp.dImpNotaDeb.Sacado, (int)DadosCON["CON"]) == 0)
                        continue;
                    else
                        rowCON = Imp.dImpNotaDeb.Sacado[0];
                    dllCheques.dImpNotaDeb.DadosImpressoRow novalinha = Imp.dImpNotaDeb.DadosImpresso.NewDadosImpressoRow();
                    dllCheques.dImpNotaDeb.DadosImpressoRow copianovalinha = Imp.dImpNotaDeb.DadosImpresso.NewDadosImpressoRow();
                    copianovalinha.DATA = novalinha.DATA = DateTime.Today;
                    copianovalinha.FRNNome = novalinha.FRNNome = rowFRN.FRNNome;
                    copianovalinha.FRNEndereco = novalinha.FRNEndereco = rowFRN.FRNEndereco + " - " + rowFRN.FRNBairro + " - " + rowFRN.CIDNome + "/" + rowFRN.CIDUf;
                    if (!rowFRN.IsFRNCepNull())
                        copianovalinha.FRNCep = novalinha.FRNCep = rowFRN.FRNCep;
                    if (!rowFRN.IsFRNFone1Null())
                        copianovalinha.FRNFone1 = novalinha.FRNFone1 = rowFRN.FRNFone1;
                    if (!rowFRN.IsFRNCnpjNull())
                    {
                        DocBacarios.CPFCNPJ CNPJ = new DocBacarios.CPFCNPJ(rowFRN.FRNCnpj);
                        copianovalinha.FRNCnpj = novalinha.FRNCnpj = CNPJ.ToString(true);
                        if (CNPJ.Tipo == DocBacarios.TipoCpfCnpj.CPF)
                            copianovalinha.CPFNPJF = novalinha.CPFNPJF = "CPF:";
                        else
                            copianovalinha.CPFNPJF = novalinha.CPFNPJF = "CNPJ:";
                    };
                    if (!rowFRN.IsFRNRegistroNull() && !rowFRN.IsFRNRegistroUFNull())
                        copianovalinha.RegistroOAB = novalinha.RegistroOAB = rowFRN.FRNRegistro + "/" + rowFRN.FRNRegistroUF;
                    copianovalinha.CONNome = novalinha.CONNome = rowCON.CONNome;
                    CorpoDoEmail += "Condom�nio: " + rowCON.CONNome+"\r\n";
                    copianovalinha.CONEndereco = novalinha.CONEndereco = rowCON.CONEndereco + " - " + rowCON.CONBairro + " - " + rowCON.CIDNome + "/" + rowCON.CIDUf;
                    if (!rowCON.IsCONCepNull())
                        copianovalinha.CONCep = novalinha.CONCep = rowCON.CONCep;
                    if (!rowCON.IsCONCnpjNull())
                        copianovalinha.CONCnpj = novalinha.CONCnpj = rowCON.CONCnpj;
                    string Descri = "";
                    string Valores = "";
                    decimal Total = 0;
                    foreach (dChequesGrade.PagamentosRow Pg in LinhaMae.GetPagamentosRows())
                    {
                        if (Descri != "")
                        {
                            Descri += "\r\n";
                            Valores += "\r\n";
                        };
                        CorpoDoEmail += "    " + Pg.Descri��o + " " + Pg.Valor.ToString("n2");
                        Descri += Pg.Descri��o;
                        Valores += Pg.Valor.ToString("n2");
                        Total += Pg.Valor;
                    };

                    copianovalinha.DESCRICAO = novalinha.DESCRICAO = Descri;
                    copianovalinha.VALOR = novalinha.VALOR = Valores;
                    copianovalinha.IRRF = novalinha.IRRF = 0;
                    copianovalinha.TOTAL = novalinha.TOTAL = Total;
                    CorpoDoEmail += "              Total: " + Total.ToString("n2") + "\r\n\r\n";
                    //Imp.Total.Text = Total.ToString("n2") + "\r\n0,00\r\n" + Total.ToString("n2");
                    //Imp.Total.Text = "1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7";
                    Imp.dImpNotaDeb.DadosImpresso.AddDadosImpressoRow(novalinha);
                    Imp.dImpNotaDeb.DadosImpresso.AddDadosImpressoRow(copianovalinha);
                    
                }
            };
            if (Imp.dImpNotaDeb.DadosImpresso.Rows.Count == 0)
                return;
            Imp.CreateDocument();
            switch (cBotaoImpBol1.BotaoClicado)
            {                
                case dllImpresso.Botoes.Botao.email:
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Recibos", Imp, CorpoDoEmail, "Recibos de honor�rios");
                    break;
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.imprimir_frente:
                    Imp.Print();
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                case dllImpresso.Botoes.Botao.PDF_frente:
                    cBotaoImpBol1.GerarPDF(Imp);
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    Imp.ShowPreviewDialog();
                    break;
            };
            
        }

        private void DataI_EditValueChanged(object sender, EventArgs e)
        {
            if ((radioGroup1.EditValue != null) && ((int)radioGroup1.EditValue == 1))
                recalcular();
        }
    }
}

