﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace dllCheques
{
    /// <summary>
    /// Form para escolher a folha de cheque
    /// </summary>
    public partial class cChequeSindico : ComponenteBaseDialog
    {
        internal dChequesImprimir.ChequesSindicoRow FCHrow;

        /// <summary>
        /// Construtor padrão
        /// </summary>
        public cChequeSindico()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="dChequesImprimir1"></param>
        public cChequeSindico(dChequesImprimir dChequesImprimir1):this()
        {
            chequesSindicoBindingSource.DataSource = dChequesImprimir1;
        }

        /// <summary>
        /// Fecha tela
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if (Resultado == DialogResult.OK)
                FCHrow = (dChequesImprimir.ChequesSindicoRow)gridView1.GetFocusedDataRow();
            if (FCHrow == null)
            {                
                MessageBox.Show("Selecione o cheque");
                Resultado = DialogResult.Cancel;
            }
            else
                base.FechaTela(Resultado);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            FechaTela(DialogResult.OK);
        }
    }
}
