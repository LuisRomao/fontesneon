﻿using System;
using CodBar;
using dllChequesProc;

namespace dllCheques.impressos
{
    /// <summary>
    /// Impresso da nota de débito
    /// </summary>
    public class impNotaDeb : Impress
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="data"></param>
        /// <param name="Bruto"></param>
        /// <param name="Retencao"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        public string SetCampoData(DateTime data,decimal Bruto,decimal Retencao,decimal Total)
        {
            string Retorno = "";
            Retorno += CodTXT(Xref3, Yref1 - 2 * alturaLinha, string.Format("{0:dd/MM/yyyy}", data), 3, Orientacao.normal, false, 1, 1, Justificado.direita);
            Retorno += CodTXT(Xref3, Yref1 - 3 * alturaLinha,     string.Format("{0:n2}", Bruto), 3, Orientacao.normal, false, 1, 1, Justificado.direita);
            Retorno += CodTXT(Xref3, Yref1 - 4 * alturaLinha, string.Format("{0:n2}", Retencao), 3, Orientacao.normal, false, 1, 1, Justificado.direita);
            Retorno += CodTXT(Xref3, Yref1 - 5 * alturaLinha, string.Format("{0:n2}", Total), 3, Orientacao.normal, true, 1, 1, Justificado.direita);
            SetCampo("%DIREITA%", Retorno);
            return Retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rowPAGs"></param>
        /// <returns></returns>
        public string SetCampoLinhas(dCheque.PAGamentosRow[] rowPAGs)
        {
            string Retorno = "";
            int Y = YDes;
            foreach (dCheque.PAGamentosRow rowPAG in rowPAGs)
            {
                Y -= alturaLinha;
                Retorno += CodTXT(Xref1, Y, rowPAG.NOAServico, fonteP);
                Retorno += CodTXT(Xref3, Y, string.Format("{0:n2}", rowPAG.NOATotal), fonteP, Orientacao.normal, false, 1, 1, Justificado.direita);
            };
            SetCampo("%LINHAS%", Retorno);
            return Retorno;
        }

        private int Xref1;
        private int Xref3;
        private int Yref1;
        private int YDes;
        private int alturaLinha;
        private int fonteP;

        /// <summary>
        /// Construtor
        /// </summary>        
        public impNotaDeb()
            : base()
        {
            int Xbase = 0;
            int Ybase = 0;
            int larguraCaixa = 1500;
            int alturaCaixa = 980;
            fonteP = 2;
            Mascara = AbreForm();
            Mascara += CodCaixa(Xbase, Ybase, larguraCaixa, alturaCaixa);
            Yref1 = Ybase + alturaCaixa - 170;
            alturaLinha = 40;
            Mascara += CodTXT(Xbase + larguraCaixa / 2, Yref1, "NOTA DE DÉBITO / RECIBO", 6, Orientacao.normal, false, 1, 1, Justificado.centro);            
            Xref1 = Xbase + 40;
            Mascara += CodTXT(Xref1, Yref1 - 2 * alturaLinha, "%NOME%", fonteP);
            Mascara += CodTXT(Xref1, Yref1 - 3 * alturaLinha, "%Endereco%", fonteP);
            Mascara += CodTXT(Xref1, Yref1 - 4 * alturaLinha, "CNPJ: %CNPJ%", fonteP);
            int Xref2 = Xbase + 1000;
            Mascara += CodTXT(Xref2, Yref1 - 2 * alturaLinha, "Data:", fonteP);
            Mascara += CodTXT(Xref2, Yref1 - 3 * alturaLinha, "Bruto:", fonteP);
            Mascara += CodTXT(Xref2, Yref1 - 4 * alturaLinha, "Retenção:", fonteP);
            Mascara += CodTXT(Xref2, Yref1 - 5 * alturaLinha, "TOTAL liq:", fonteP);
            Mascara += CodLinha(Xbase + 10, Yref1 - 5 * alturaLinha - 2, larguraCaixa - 20);
            Xref3 = Xbase + larguraCaixa - 40;
            Mascara += "%DIREITA%";
            int YCli = Yref1 - 6 * alturaLinha - 40;
            Mascara += CodTXT(Xbase + larguraCaixa / 2, YCli, "CLIENTE", 5,Orientacao.normal,true,1,1,Justificado.centro);
            Mascara += CodTXT(Xref1, YCli - alturaLinha - 3, "%NomeCLI%", fonteP);
            Mascara += CodTXT(Xref1, YCli - 2 * alturaLinha - 3, "%EnderecoCLI%", fonteP);
            Mascara += CodTXT(Xref1, YCli - 3 * alturaLinha - 3, "CNPJ:%CNPJCLI%", fonteP);
            Mascara += CodLinha(Xbase + 10, YCli - 3 * alturaLinha - 3 - 2, larguraCaixa - 20);
            YDes = YCli - 4 * alturaLinha - 45;
            Mascara += CodTXT(Xref1 + 100, YDes + 5, "DESCRIÇÃO", 5, Orientacao.normal, true, 1, 1, Justificado.esquerda);
            Mascara += CodTXT(Xref3, YDes + 5, "VALOR", 5, Orientacao.normal, true, 1, 1, Justificado.direita);
            Mascara += "%LINHAS%";
            int larguralinha = 600;
            Mascara += CodLinha(Xbase + 800, Ybase + 80, larguralinha, 2);
            Mascara += CodTXT(Xbase + 800 + larguralinha / 2, Ybase + 40, "ass", 1, Orientacao.normal, false, 1, 1, Justificado.centro);
            Mascara += FechaForm(2);
        }
    }
}
