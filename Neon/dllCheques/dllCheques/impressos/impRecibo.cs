﻿using System;
using CodBar;
using dllChequesProc;

namespace dllCheques.impressos
{
    /// <summary>
    /// Impresso do recibo
    /// </summary>
    public class impRecibo : Impress
    {
        private string[] Quebrar(string linha)
        {
            string[] retorno = new string[2];
            int corte = 69;            
            if (linha.Length < corte)
            {
                retorno[0] = linha;
                retorno[1] = "";
            }
            else
            {
                while ((linha[corte] != ' ') && (corte > 40))
                    corte--;
                retorno[0] = linha.Substring(0, corte).Trim();
                retorno[1] = linha.Substring(corte).Trim();
            };            
            return retorno;
        }

        /// <summary>
        /// Construtor
        /// </summary>        
        /// <param name="Nome"></param>
        /// <param name="Favorecido"></param>
        /// <param name="Pagador"></param>
        /// <param name="Valor"></param>
        /// <param name="DadosCheque"></param>
        /// <param name="itens"></param>
        public impRecibo(string Nome,string Favorecido, string Pagador, decimal Valor,string DadosCheque, dCheque.PAGamentosRow[] itens)
            : base()
        {
            Mascara = AbreForm();
            int LarguraTotal = 1600;
            int YBase = 50;
            Mascara += CodCaixa(0, YBase, LarguraTotal, 900, 5, 5);
            Mascara += CodTXT(LarguraTotal / 2, 810, "RECIBO", 7, Orientacao.normal, false, 1, 1, Justificado.centro);
            int Yref1 = 750;
            int Xref1 = 60;
            int alturaLinha = 55;
            int alturaLinha1 = 70;
            int Linhas = 0;
            int FontePadrao = 3;
            if (Nome == "")
                Nome = "_____________________________";
            string[] manobra = Quebrar(string.Format("Eu {0} recebi de {1} a quantia de R${2:n2} referente a:",Nome,Pagador,Valor));
            Mascara += CodTXT(Xref1, Yref1, manobra[0], FontePadrao);
            Linhas++;
            if (manobra[1] != "")
            {
                Mascara += CodTXT(Xref1, Yref1 - alturaLinha, manobra[1], FontePadrao);
                Linhas++;
            }
            foreach (dCheque.PAGamentosRow rp in itens)
            {
                Mascara += CodTXT(Xref1 + 100, Yref1 - Linhas * alturaLinha, string.Format("- {0} {1:n2}", rp.NOAServico, rp.PAGValor), FontePadrao);
                Linhas++;
            }           
            Mascara += CodTXT(Xref1, Yref1 - Linhas * alturaLinha - 5, DadosCheque, FontePadrao);

            int Yred = 10;
            Mascara += CodTXT(Xref1, YBase + 2 * alturaLinha1 + Yred, "CPF:____.____.____-___", 4);
            Mascara += CodTXT(Xref1, YBase + 1 * alturaLinha1 + Yred, "RG: __________________", 4);
            Mascara += CodTXT(Xref1, YBase + Yred, string.Format("{0} {1:dd/MM/yyyy}", Framework.DSCentral.EMP == 1 ? "São Bernardo" : "Santo André", DateTime.Today), FontePadrao);
            int XLinha = LarguraTotal / 2;
            int larguraLinha = LarguraTotal / 2 - 50;
            Mascara += CodLinha(XLinha, 200, larguraLinha);
            Mascara += CodTXT(XLinha + larguraLinha/2, 160, Favorecido, 1, Orientacao.normal, false, 1, 1, Justificado.centro);            
            Mascara += FechaForm();
        }
    }
}
