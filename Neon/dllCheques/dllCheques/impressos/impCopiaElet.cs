﻿using System;
using CodBar;

namespace dllCheques.impressos
{
    public class impCopiaElet : Impress
    {
        public impCopiaElet(LinguagemCodBar Linguagem)
            : base(Linguagem)
        {
            Mascara = AbreForm();
            int larguraCaixa = 870;
            Mascara += CodCaixa(0, 50, larguraCaixa, 385);
            int XrefEsquerda = 30;
            int XrefCentro = XrefEsquerda + larguraCaixa / 2;
            
            int YrefTopo = 370;
            int alturaLinha = 40;
            Mascara += CodTXT(XrefCentro, YrefTopo, "PAGAMENTO ELETRONICO %CHENumero%", 3, Orientacao.normal, true, 1, 1, Justificado.centro);
            YrefTopo -= 55;
            Mascara += CodTXT(XrefCentro, YrefTopo, "%CHEValor% - %CHEVencimento%", 5, Orientacao.normal, true, 1, 1, Justificado.centro);
            Mascara += CodTXT(XrefEsquerda, YrefTopo - 2 * alturaLinha, "Favor liberar pagamento atraves do site", 3, Orientacao.normal);
            Mascara += CodTXT(XrefEsquerda, YrefTopo - 3 * alturaLinha, "www.autorizacaoremota.com.br", 4, Orientacao.normal);
            YrefTopo -= 5;
            Mascara += CodTXT(XrefEsquerda, YrefTopo - 4 * alturaLinha, "%CONNome%", 3, Orientacao.normal);
            YrefTopo -= 12;
            Mascara += CodTXT(XrefEsquerda, YrefTopo - 5 * alturaLinha, "%CONCodigo%", 3, Orientacao.normal, true);
            Mascara += "%DETALHES%";
            Mascara += "%QuadroImpostos%";
            
            Mascara += FechaForm();

            //"%QuadroIDAVOLTA%" +
            //"%DETALHES%" +
            
            //"%PERIODICOS%" +
            //"%QuadroImpostos%" +                                    
        }
    }
}
