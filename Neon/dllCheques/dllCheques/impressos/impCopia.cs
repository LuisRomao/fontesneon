﻿/*
MR - 08/01/2015 10:30 -          - Ajustes no layout da copia do pagamento eletrônico (Alterações indicadas por *** MRC - INICIO (08/01/2015 10:30) ***)
MR - 14/01/2016 12:30 -          - Inclusão de detalhes na copia do pagamento eletrônico (Alterações indicadas por *** MRC - INICIO (14/01/2016 12:30) ***)
*/

using System;
using CodBar;

namespace dllCheques.impressos
{
    /// <summary>
    /// Impresso da cópia
    /// </summary>
    internal class impCopia : Impress
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CHENumero"></param>
        public void SetCampoCHENumero(int CHENumero)
        {
            if (Eletronico)
                //*** MRC - INICIO (08/01/2015 10:30) ***
                SetCampo("%CHENumeroEl%", CodTXT(XrefEsquerda, YrefTopo0, string.Format("PAGAMENTO ELETRONICO {0}", CHENumero), 3, Orientacao.normal, false, 1, 1, Justificado.esquerda));
                //*** MRC - TERMINO (08/01/2015 10:30) ***
            else
                SetCampo("%CHENumero%", string.Format("{0:000000}", CHENumero));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CHEValor"></param>
        /// <param name="CHEVencimento"></param>
        public void SetCampoCHEValor_CHEVencimento(decimal CHEValor, DateTime CHEVencimento)
        {
            if (Eletronico)
                //*** MRC - INICIO (08/01/2015 10:30) ***
                SetCampo("%CHEValor_CHEVencimento%", CodTXT(XrefEsquerda, YrefTopoVV, string.Format("{0:n2} - {1:dd/MM/yyyy}", CHEValor, CHEVencimento), 5, Orientacao.normal, false, 1, 1, Justificado.esquerda));
                //*** MRC - TERMINO (08/01/2015 10:30) ***
        }

        private bool Eletronico;
        private int XrefCentro;
        //*** MRC - INICIO (08/01/2015 10:30) ***
        private int XrefEsquerda;
        private int YrefTopo0 = 320;
        private int YrefTopoVV = 260;
        //*** MRC - TERMINO (08/01/2015 10:30) ***
        private int alturaCaixa = 385;

        internal int XrefColuna0;
        internal int LarguraColuna;
        internal int Coluna = 0;
        internal int YTopoDet0 = 950;
        internal int YTopoDet;
        internal int YMinimoDet;

        /// <summary>
        /// Construtor
        /// </summary>        
        /// <param name="_Eletronico"></param>
        public impCopia(bool _Eletronico):base()
        {
            YTopoDet = YTopoDet0;
            Eletronico = _Eletronico;
            Mascara = AbreForm();
            if (Eletronico)
            {
                //*** MRC - INICIO (08/01/2015 10:30) ***
                XrefColuna0 = 0;
                XrefEsquerda = 0;
                LarguraColuna = 850;
                int larguraCaixa = LarguraColuna - XrefEsquerda;                
                //*** MRC - INICIO (14/01/2016 12:30) ***
                Mascara += "%DETALHES%";
                //*** MRC - TERMINO (14/01/2016 12:30) ***

                YMinimoDet = alturaCaixa;
                Mascara += CodCaixa(0, 0, larguraCaixa, alturaCaixa);                
                XrefCentro = larguraCaixa / 2;
                XrefEsquerda += 50;
                int YrefTopo = YrefTopo0 - 80;
                int alturaLinha = 42;
                //CodTXT(XrefEsquerda, YrefTopo0, string.Format("PAGAMENTO ELETRONICO {0}", CHENumero), 3, Orientacao.normal, false, 1, 1, Justificado.esquerda)
                Mascara += "%CHENumeroEl%";
                //CodTXT(XrefEsquerda, YrefTopoVV, string.Format("{0:n2} - {1:dd/MM/yyyy}", CHEValor, CHEVencimento), 5, Orientacao.normal, false, 1, 1, Justificado.esquerda)
                Mascara += "%CHEValor_CHEVencimento%";
                Mascara += CodTXT(XrefEsquerda, YrefTopo - 2 * alturaLinha, "FAVOR LIBERAR PAGAMENTO", 4, Orientacao.normal);
                Mascara += CodTXT(XrefEsquerda, YrefTopo - 3 * alturaLinha, "ATRAVES DO SITE.", 4, Orientacao.normal);
                YrefTopo -= 10;
                Mascara += CodTXT(XrefEsquerda, YrefTopo - 4 * alturaLinha, "%CONNome%", 3, Orientacao.normal);
                YrefTopo -= 10;
                Mascara += CodTXT(XrefEsquerda, YrefTopo - 5 * alturaLinha, "%CONCodigo%", 3, Orientacao.normal, true);
                //*** MRC - TERMINO (08/01/2015 10:30) ***            
            }
            else
            {
                XrefColuna0 = 480;
                LarguraColuna = 1000;
                Mascara += "%QuadroIDAVOLTA%";
                Mascara += CodTXT(50, 430, "%CONCodigo%", 6, Orientacao.normal, true);
                Mascara += CodBarra(50, 220, "%CHE%", 150);
                Mascara += "%DETALHES%";
                
                YMinimoDet = alturaCaixa;
                int YbaseCaixa = 100;
                Mascara += CodCaixa(XrefColuna0 + 30, YbaseCaixa, LarguraColuna - 30, alturaCaixa);
                YMinimoDet = alturaCaixa + YbaseCaixa;
                Mascara += CodTXT(0530, YbaseCaixa + 330, "%CHE_BCO%", 3);
                Mascara += CodTXT(0630, YbaseCaixa + 330, "%CONAgencia%", 3);
                Mascara += CodTXT(0760, YbaseCaixa + 330, "%CONConta%", 3);
                Mascara += CodTXT(0965, YbaseCaixa + 330, "%CHENumero%", 3);
                Mascara += CodTXT(1110, YbaseCaixa + 310, "%CHEValor%", 6);
                Mascara += CodTXT(0530, YbaseCaixa + 255, "Port.", 0);
                Mascara += CodTXT(0600, YbaseCaixa + 235, "%CHEFavorecido%", 3);
                Mascara += CodTXT(0750, YbaseCaixa + 200, "%Local%, %CHEVencimento%", 2);
                Mascara += CodLinha(750, YbaseCaixa + 65, 700, 1);
                Mascara += CodTXT(750, YbaseCaixa + 25, "%CONNome%", 2);
            }
            //Mascara += "%PERIODICOS%";
            Mascara += "%QuadroImpostos%";
            Mascara += FechaForm();
        }
    }
}
