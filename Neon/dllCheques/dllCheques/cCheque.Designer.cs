namespace dllCheques
{
    partial class cCheque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCheque));
            System.Windows.Forms.Label bOLTipoCRAILabel;
            System.Windows.Forms.Label bOLMensagemLabel;
            System.Windows.Forms.Label bOLEmissaoLabel;
            System.Windows.Forms.Label bOLEnderecoLabel;
            System.Windows.Forms.Label bOLVenctoLabel;
            System.Windows.Forms.Label bOLValorPrevistoLabel;
            System.Windows.Forms.Label bOLCompetenciaAnoLabel;
            System.Windows.Forms.Label cHENumeroLabel;
            System.Windows.Forms.Label label1;
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.SBAgrupar = new DevExpress.XtraEditors.SimpleButton();
            this.BotCComprovante = new DevExpress.XtraEditors.SimpleButton();
            this.BotGuia = new DevExpress.XtraEditors.SimpleButton();
            this.BotEletronico = new DevExpress.XtraEditors.SimpleButton();
            this.BBaixaManual = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonEletronico = new DevExpress.XtraEditors.SimpleButton();
            this.BotCCopia = new DevExpress.XtraEditors.SimpleButton();
            this.BotCRecibo = new DevExpress.XtraEditors.SimpleButton();
            this.ComboStatus = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.ChequebindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ComboPAGTipoDefault = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.cHENumeroTextBox = new System.Windows.Forms.TextBox();
            this.BotCEmissao = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.bOLEmissaoDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.VenctoDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.TFavorecido = new DevExpress.XtraEditors.TextEdit();
            this.bOLValorPrevistoSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.bOletoDetalheGridControl = new DevExpress.XtraGrid.GridControl();
            this.PagbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPAGValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPAGDOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PLALookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colNOA_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PlalookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.PLAnocontasbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colNOAServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOANumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPAGStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryPAGStatus = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPAG_GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            bOLTipoCRAILabel = new System.Windows.Forms.Label();
            bOLMensagemLabel = new System.Windows.Forms.Label();
            bOLEmissaoLabel = new System.Windows.Forms.Label();
            bOLEnderecoLabel = new System.Windows.Forms.Label();
            bOLVenctoLabel = new System.Windows.Forms.Label();
            bOLValorPrevistoLabel = new System.Windows.Forms.Label();
            bOLCompetenciaAnoLabel = new System.Windows.Forms.Label();
            cHENumeroLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChequebindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboPAGTipoDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEmissaoDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEmissaoDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VenctoDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VenctoDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TFavorecido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLValorPrevistoSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOletoDetalheGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PagbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PLALookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlalookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PLAnocontasbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPAGStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // bOLTipoCRAILabel
            // 
            bOLTipoCRAILabel.AutoSize = true;
            bOLTipoCRAILabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bOLTipoCRAILabel.Location = new System.Drawing.Point(284, 12);
            bOLTipoCRAILabel.Name = "bOLTipoCRAILabel";
            bOLTipoCRAILabel.Size = new System.Drawing.Size(34, 13);
            bOLTipoCRAILabel.TabIndex = 8;
            bOLTipoCRAILabel.Text = "Tipo:";
            // 
            // bOLMensagemLabel
            // 
            bOLMensagemLabel.AutoSize = true;
            bOLMensagemLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bOLMensagemLabel.Location = new System.Drawing.Point(22, 125);
            bOLMensagemLabel.Name = "bOLMensagemLabel";
            bOLMensagemLabel.Size = new System.Drawing.Size(31, 13);
            bOLMensagemLabel.TabIndex = 30;
            bOLMensagemLabel.Text = "Obs:";
            // 
            // bOLEmissaoLabel
            // 
            bOLEmissaoLabel.AutoSize = true;
            bOLEmissaoLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bOLEmissaoLabel.Location = new System.Drawing.Point(22, 10);
            bOLEmissaoLabel.Name = "bOLEmissaoLabel";
            bOLEmissaoLabel.Size = new System.Drawing.Size(56, 13);
            bOLEmissaoLabel.TabIndex = 10;
            bOLEmissaoLabel.Text = "Emiss�o:";
            // 
            // bOLEnderecoLabel
            // 
            bOLEnderecoLabel.AutoSize = true;
            bOLEnderecoLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bOLEnderecoLabel.Location = new System.Drawing.Point(22, 63);
            bOLEnderecoLabel.Name = "bOLEnderecoLabel";
            bOLEnderecoLabel.Size = new System.Drawing.Size(33, 13);
            bOLEnderecoLabel.TabIndex = 26;
            bOLEnderecoLabel.Text = "Fav.:";
            // 
            // bOLVenctoLabel
            // 
            bOLVenctoLabel.AutoSize = true;
            bOLVenctoLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bOLVenctoLabel.Location = new System.Drawing.Point(284, 88);
            bOLVenctoLabel.Name = "bOLVenctoLabel";
            bOLVenctoLabel.Size = new System.Drawing.Size(49, 13);
            bOLVenctoLabel.TabIndex = 12;
            bOLVenctoLabel.Text = "Vencto:";
            // 
            // bOLValorPrevistoLabel
            // 
            bOLValorPrevistoLabel.AutoSize = true;
            bOLValorPrevistoLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bOLValorPrevistoLabel.Location = new System.Drawing.Point(22, 89);
            bOLValorPrevistoLabel.Name = "bOLValorPrevistoLabel";
            bOLValorPrevistoLabel.Size = new System.Drawing.Size(39, 13);
            bOLValorPrevistoLabel.TabIndex = 18;
            bOLValorPrevistoLabel.Text = "Valor:";
            // 
            // bOLCompetenciaAnoLabel
            // 
            bOLCompetenciaAnoLabel.AutoSize = true;
            bOLCompetenciaAnoLabel.Location = new System.Drawing.Point(689, 164);
            bOLCompetenciaAnoLabel.Name = "bOLCompetenciaAnoLabel";
            bOLCompetenciaAnoLabel.Size = new System.Drawing.Size(73, 13);
            bOLCompetenciaAnoLabel.TabIndex = 22;
            bOLCompetenciaAnoLabel.Text = "Competencia:";
            bOLCompetenciaAnoLabel.Visible = false;
            // 
            // cHENumeroLabel
            // 
            cHENumeroLabel.AutoSize = true;
            cHENumeroLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cHENumeroLabel.Location = new System.Drawing.Point(500, 89);
            cHENumeroLabel.Name = "cHENumeroLabel";
            cHENumeroLabel.Size = new System.Drawing.Size(54, 13);
            cHENumeroLabel.TabIndex = 54;
            cHENumeroLabel.Text = "N�mero:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(22, 36);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(47, 13);
            label1.TabIndex = 58;
            label1.Text = "Status:";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.SBAgrupar);
            this.panelControl1.Controls.Add(this.BotCComprovante);
            this.panelControl1.Controls.Add(this.BotGuia);
            this.panelControl1.Controls.Add(this.BotEletronico);
            this.panelControl1.Controls.Add(this.BBaixaManual);
            this.panelControl1.Controls.Add(this.ButtonEletronico);
            this.panelControl1.Controls.Add(this.BotCCopia);
            this.panelControl1.Controls.Add(this.BotCRecibo);
            this.panelControl1.Controls.Add(label1);
            this.panelControl1.Controls.Add(this.ComboStatus);
            this.panelControl1.Controls.Add(this.ComboPAGTipoDefault);
            this.panelControl1.Controls.Add(cHENumeroLabel);
            this.panelControl1.Controls.Add(this.cHENumeroTextBox);
            this.panelControl1.Controls.Add(this.BotCEmissao);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Controls.Add(this.memoEdit1);
            this.panelControl1.Controls.Add(bOLTipoCRAILabel);
            this.panelControl1.Controls.Add(bOLMensagemLabel);
            this.panelControl1.Controls.Add(this.bOLEmissaoDateEdit);
            this.panelControl1.Controls.Add(bOLEmissaoLabel);
            this.panelControl1.Controls.Add(this.VenctoDateEdit);
            this.panelControl1.Controls.Add(bOLEnderecoLabel);
            this.panelControl1.Controls.Add(bOLVenctoLabel);
            this.panelControl1.Controls.Add(this.TFavorecido);
            this.panelControl1.Controls.Add(this.bOLValorPrevistoSpinEdit);
            this.panelControl1.Controls.Add(bOLValorPrevistoLabel);
            this.panelControl1.Controls.Add(bOLCompetenciaAnoLabel);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(816, 286);
            this.panelControl1.TabIndex = 53;
            // 
            // SBAgrupar
            // 
            this.SBAgrupar.Location = new System.Drawing.Point(692, 261);
            this.SBAgrupar.Name = "SBAgrupar";
            this.SBAgrupar.Size = new System.Drawing.Size(108, 19);
            this.SBAgrupar.TabIndex = 64;
            this.SBAgrupar.Text = "Agrupar";
            this.SBAgrupar.Visible = false;
            this.SBAgrupar.Click += new System.EventHandler(this.SBAgrupar_Click);
            // 
            // BotCComprovante
            // 
            this.BotCComprovante.Location = new System.Drawing.Point(692, 238);
            this.BotCComprovante.Name = "BotCComprovante";
            this.BotCComprovante.Size = new System.Drawing.Size(108, 19);
            this.BotCComprovante.TabIndex = 61;
            this.BotCComprovante.Text = "Comprovante";
            this.BotCComprovante.Visible = false;
            this.BotCComprovante.Click += new System.EventHandler(this.BotCComprovante_Click);
            // 
            // BotGuia
            // 
            this.BotGuia.Enabled = false;
            this.BotGuia.Location = new System.Drawing.Point(692, 111);
            this.BotGuia.Name = "BotGuia";
            this.BotGuia.Size = new System.Drawing.Size(108, 19);
            this.BotGuia.TabIndex = 57;
            this.BotGuia.Text = "Guia";
            this.BotGuia.Visible = false;
            // 
            // BotEletronico
            // 
            this.BotEletronico.Location = new System.Drawing.Point(692, 213);
            this.BotEletronico.Name = "BotEletronico";
            this.BotEletronico.Size = new System.Drawing.Size(108, 19);
            this.BotEletronico.TabIndex = 60;
            this.BotEletronico.Text = "Pagar Eletr�nico";
            this.BotEletronico.Visible = false;
            this.BotEletronico.Click += new System.EventHandler(this.BotEletronico_Click);
            // 
            // BBaixaManual
            // 
            this.BBaixaManual.Enabled = false;
            this.BBaixaManual.Location = new System.Drawing.Point(692, 136);
            this.BBaixaManual.Name = "BBaixaManual";
            this.BBaixaManual.Size = new System.Drawing.Size(108, 19);
            this.BBaixaManual.TabIndex = 58;
            this.BBaixaManual.Text = "Baixa Manual";
            this.BBaixaManual.Visible = false;
            this.BBaixaManual.Click += new System.EventHandler(this.BBaixaManual_Click);
            // 
            // ButtonEletronico
            // 
            this.ButtonEletronico.Location = new System.Drawing.Point(576, 10);
            this.ButtonEletronico.Name = "ButtonEletronico";
            this.ButtonEletronico.Size = new System.Drawing.Size(99, 19);
            this.ButtonEletronico.TabIndex = 63;
            this.ButtonEletronico.Text = "ELETR�NICO";
            this.ButtonEletronico.Visible = false;
            this.ButtonEletronico.Click += new System.EventHandler(this.ButtonEletronico_Click);
            // 
            // BotCCopia
            // 
            this.BotCCopia.Location = new System.Drawing.Point(692, 61);
            this.BotCCopia.Name = "BotCCopia";
            this.BotCCopia.Size = new System.Drawing.Size(108, 19);
            this.BotCCopia.TabIndex = 55;
            this.BotCCopia.Text = "C�pia";
            this.BotCCopia.Click += new System.EventHandler(this.BotCCopia_Click);
            // 
            // BotCRecibo
            // 
            this.BotCRecibo.Location = new System.Drawing.Point(692, 86);
            this.BotCRecibo.Name = "BotCRecibo";
            this.BotCRecibo.Size = new System.Drawing.Size(108, 19);
            this.BotCRecibo.TabIndex = 56;
            this.BotCRecibo.Text = "Recibo";
            this.BotCRecibo.Click += new System.EventHandler(this.BotCRecibo_Click);
            // 
            // ComboStatus
            // 
            this.ComboStatus.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ChequebindingSource, "CHEStatus", true));
            this.ComboStatus.Location = new System.Drawing.Point(100, 33);
            this.ComboStatus.Name = "ComboStatus";
            this.ComboStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboStatus.Properties.ReadOnly = true;
            this.ComboStatus.Size = new System.Drawing.Size(472, 20);
            this.ComboStatus.TabIndex = 57;
            // 
            // ChequebindingSource
            // 
            this.ChequebindingSource.DataMember = "CHEques";
            this.ChequebindingSource.DataSource = typeof(dllChequesProc.dCheque);
            // 
            // ComboPAGTipoDefault
            // 
            this.ComboPAGTipoDefault.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ChequebindingSource, "CHETipo", true));
            this.ComboPAGTipoDefault.Location = new System.Drawing.Point(352, 7);
            this.ComboPAGTipoDefault.Name = "ComboPAGTipoDefault";
            this.ComboPAGTipoDefault.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboPAGTipoDefault.Size = new System.Drawing.Size(220, 20);
            this.ComboPAGTipoDefault.TabIndex = 56;
            // 
            // cHENumeroTextBox
            // 
            this.cHENumeroTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ChequebindingSource, "CHENumero", true));
            this.cHENumeroTextBox.Location = new System.Drawing.Point(575, 86);
            this.cHENumeroTextBox.Name = "cHENumeroTextBox";
            this.cHENumeroTextBox.ReadOnly = true;
            this.cHENumeroTextBox.Size = new System.Drawing.Size(100, 21);
            this.cHENumeroTextBox.TabIndex = 55;
            // 
            // BotCEmissao
            // 
            this.BotCEmissao.Location = new System.Drawing.Point(576, 36);
            this.BotCEmissao.Name = "BotCEmissao";
            this.BotCEmissao.Size = new System.Drawing.Size(224, 19);
            this.BotCEmissao.TabIndex = 54;
            this.BotCEmissao.Text = "Cancelar Cheque";
            this.BotCEmissao.Click += new System.EventHandler(this.BotCEmissao_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(692, 10);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(108, 19);
            this.simpleButton1.TabIndex = 53;
            this.simpleButton1.Text = "Incluir Observa��o";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(692, 180);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 27);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 59;
            this.cCompet1.Titulo = "Framework.objetosNeon.cCompet - ";
            this.cCompet1.Visible = false;
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ChequebindingSource, "CHEObs", true));
            this.memoEdit1.EditValue = "";
            this.memoEdit1.Location = new System.Drawing.Point(100, 122);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Size = new System.Drawing.Size(574, 156);
            this.memoEdit1.TabIndex = 51;
            // 
            // bOLEmissaoDateEdit
            // 
            this.bOLEmissaoDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ChequebindingSource, "CHEEmissao", true));
            this.bOLEmissaoDateEdit.EditValue = null;
            this.bOLEmissaoDateEdit.Location = new System.Drawing.Point(100, 7);
            this.bOLEmissaoDateEdit.Name = "bOLEmissaoDateEdit";
            this.bOLEmissaoDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.bOLEmissaoDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.bOLEmissaoDateEdit.Properties.ReadOnly = true;
            this.bOLEmissaoDateEdit.Size = new System.Drawing.Size(178, 20);
            this.bOLEmissaoDateEdit.TabIndex = 11;
            // 
            // VenctoDateEdit
            // 
            this.VenctoDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ChequebindingSource, "CHEVencimento", true));
            this.VenctoDateEdit.EditValue = null;
            this.VenctoDateEdit.Location = new System.Drawing.Point(352, 88);
            this.VenctoDateEdit.Name = "VenctoDateEdit";
            this.VenctoDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VenctoDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.VenctoDateEdit.Size = new System.Drawing.Size(136, 20);
            this.VenctoDateEdit.TabIndex = 13;
            // 
            // TFavorecido
            // 
            this.TFavorecido.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ChequebindingSource, "CHEFavorecido", true));
            this.TFavorecido.Location = new System.Drawing.Point(100, 60);
            this.TFavorecido.Name = "TFavorecido";
            this.TFavorecido.Properties.MaxLength = 100;
            this.TFavorecido.Size = new System.Drawing.Size(575, 20);
            this.TFavorecido.TabIndex = 27;
            // 
            // bOLValorPrevistoSpinEdit
            // 
            this.bOLValorPrevistoSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ChequebindingSource, "CHEValor", true));
            this.bOLValorPrevistoSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.bOLValorPrevistoSpinEdit.Location = new System.Drawing.Point(100, 86);
            this.bOLValorPrevistoSpinEdit.Name = "bOLValorPrevistoSpinEdit";
            this.bOLValorPrevistoSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.bOLValorPrevistoSpinEdit.Properties.DisplayFormat.FormatString = "n2";
            this.bOLValorPrevistoSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bOLValorPrevistoSpinEdit.Properties.ReadOnly = true;
            this.bOLValorPrevistoSpinEdit.Size = new System.Drawing.Size(178, 20);
            this.bOLValorPrevistoSpinEdit.TabIndex = 19;
            // 
            // bOletoDetalheGridControl
            // 
            this.bOletoDetalheGridControl.DataSource = this.PagbindingSource;
            this.bOletoDetalheGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bOletoDetalheGridControl.Location = new System.Drawing.Point(0, 321);
            this.bOletoDetalheGridControl.MainView = this.gridView1;
            this.bOletoDetalheGridControl.Name = "bOletoDetalheGridControl";
            this.bOletoDetalheGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.PLALookUpEdit1,
            this.PlalookUpEdit2,
            this.repositoryItemImageComboBox1,
            this.repositoryItemButtonEdit2,
            this.repositoryPAGStatus});
            this.bOletoDetalheGridControl.Size = new System.Drawing.Size(816, 238);
            this.bOletoDetalheGridControl.TabIndex = 54;
            this.bOletoDetalheGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // PagbindingSource
            // 
            this.PagbindingSource.DataMember = "FK_PAGamentos_CHEques";
            this.PagbindingSource.DataSource = this.ChequebindingSource;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPAGValor,
            this.colPAGVencimento,
            this.colPAGTipo,
            this.colPAGDOC,
            this.gridColumn1,
            this.colNOA_PLA,
            this.colNOAServico,
            this.colPAGN,
            this.colFRNCnpj,
            this.colFRNNome,
            this.colFRNFantasia,
            this.colNOANumero,
            this.gridColumn2,
            this.colPAGStatus,
            this.colPAG_GPS});
            this.gridView1.GridControl = this.bOletoDetalheGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.NewItemRowText = "Incluir linha";
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colPAGValor
            // 
            this.colPAGValor.Caption = "Valor";
            this.colPAGValor.DisplayFormat.FormatString = "n2";
            this.colPAGValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPAGValor.FieldName = "PAGValor";
            this.colPAGValor.Name = "colPAGValor";
            this.colPAGValor.OptionsColumn.FixedWidth = true;
            this.colPAGValor.OptionsColumn.ReadOnly = true;
            this.colPAGValor.Visible = true;
            this.colPAGValor.VisibleIndex = 8;
            // 
            // colPAGVencimento
            // 
            this.colPAGVencimento.Caption = "Vemcimento";
            this.colPAGVencimento.FieldName = "PAGVencimento";
            this.colPAGVencimento.Name = "colPAGVencimento";
            this.colPAGVencimento.OptionsColumn.FixedWidth = true;
            this.colPAGVencimento.OptionsColumn.ReadOnly = true;
            this.colPAGVencimento.Visible = true;
            this.colPAGVencimento.VisibleIndex = 7;
            // 
            // colPAGTipo
            // 
            this.colPAGTipo.Caption = "Tipo";
            this.colPAGTipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colPAGTipo.FieldName = "PAGTipo";
            this.colPAGTipo.Name = "colPAGTipo";
            this.colPAGTipo.OptionsColumn.FixedWidth = true;
            this.colPAGTipo.OptionsColumn.ReadOnly = true;
            this.colPAGTipo.Visible = true;
            this.colPAGTipo.VisibleIndex = 2;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colPAGDOC
            // 
            this.colPAGDOC.Caption = "DOC";
            this.colPAGDOC.FieldName = "PAGDOC";
            this.colPAGDOC.Name = "colPAGDOC";
            this.colPAGDOC.OptionsColumn.FixedWidth = true;
            this.colPAGDOC.OptionsColumn.ReadOnly = true;
            this.colPAGDOC.Visible = true;
            this.colPAGDOC.VisibleIndex = 3;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Descri��o da conta";
            this.gridColumn1.ColumnEdit = this.PLALookUpEdit1;
            this.gridColumn1.FieldName = "NOA_PLA";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // PLALookUpEdit1
            // 
            this.PLALookUpEdit1.AutoHeight = false;
            this.PLALookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PLALookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descri��o", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "Cod.")});
            this.PLALookUpEdit1.DisplayMember = "PLADescricao";
            this.PLALookUpEdit1.Name = "PLALookUpEdit1";
            this.PLALookUpEdit1.NullText = "";
            this.PLALookUpEdit1.ShowHeader = false;
            this.PLALookUpEdit1.ValueMember = "PLA";
            // 
            // colNOA_PLA
            // 
            this.colNOA_PLA.Caption = "Conta";
            this.colNOA_PLA.ColumnEdit = this.PlalookUpEdit2;
            this.colNOA_PLA.FieldName = "NOA_PLA";
            this.colNOA_PLA.Name = "colNOA_PLA";
            this.colNOA_PLA.OptionsColumn.FixedWidth = true;
            this.colNOA_PLA.OptionsColumn.ReadOnly = true;
            this.colNOA_PLA.Visible = true;
            this.colNOA_PLA.VisibleIndex = 0;
            // 
            // PlalookUpEdit2
            // 
            this.PlalookUpEdit2.AutoHeight = false;
            this.PlalookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PlalookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "Cod", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descri��o", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.PlalookUpEdit2.DataSource = this.PLAnocontasbindingSource;
            this.PlalookUpEdit2.DisplayMember = "PLA";
            this.PlalookUpEdit2.Name = "PlalookUpEdit2";
            this.PlalookUpEdit2.NullText = "";
            this.PlalookUpEdit2.ValueMember = "PLA";
            // 
            // PLAnocontasbindingSource
            // 
            this.PLAnocontasbindingSource.DataMember = "PLAnocontas";
            this.PLAnocontasbindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colNOAServico
            // 
            this.colNOAServico.Caption = "Descricao";
            this.colNOAServico.FieldName = "NOAServico";
            this.colNOAServico.Name = "colNOAServico";
            this.colNOAServico.OptionsColumn.ReadOnly = true;
            this.colNOAServico.Visible = true;
            this.colNOAServico.VisibleIndex = 4;
            this.colNOAServico.Width = 341;
            // 
            // colPAGN
            // 
            this.colPAGN.Caption = "Parcela";
            this.colPAGN.FieldName = "PAGN";
            this.colPAGN.Name = "colPAGN";
            this.colPAGN.OptionsColumn.FixedWidth = true;
            this.colPAGN.OptionsColumn.ReadOnly = true;
            this.colPAGN.Visible = true;
            this.colPAGN.VisibleIndex = 5;
            // 
            // colFRNCnpj
            // 
            this.colFRNCnpj.Caption = "CNPJ";
            this.colFRNCnpj.FieldName = "FRNCnpj";
            this.colFRNCnpj.Name = "colFRNCnpj";
            this.colFRNCnpj.OptionsColumn.FixedWidth = true;
            this.colFRNCnpj.OptionsColumn.ReadOnly = true;
            // 
            // colFRNNome
            // 
            this.colFRNNome.Caption = "Fornecedor";
            this.colFRNNome.FieldName = "FRNNome";
            this.colFRNNome.Name = "colFRNNome";
            this.colFRNNome.OptionsColumn.ReadOnly = true;
            this.colFRNNome.Visible = true;
            this.colFRNNome.VisibleIndex = 1;
            this.colFRNNome.Width = 339;
            // 
            // colFRNFantasia
            // 
            this.colFRNFantasia.Caption = "Fornecedor";
            this.colFRNFantasia.FieldName = "FRNFantasia";
            this.colFRNFantasia.Name = "colFRNFantasia";
            this.colFRNFantasia.OptionsColumn.ReadOnly = true;
            // 
            // colNOANumero
            // 
            this.colNOANumero.Caption = "Nota";
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.OptionsColumn.FixedWidth = true;
            this.colNOANumero.OptionsColumn.ReadOnly = true;
            this.colNOANumero.Visible = true;
            this.colNOANumero.VisibleIndex = 6;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 9;
            this.gridColumn2.Width = 30;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            editorButtonImageOptions1.Image = global::dllCheques.Properties.Resources.Nota1;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions1, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // colPAGStatus
            // 
            this.colPAGStatus.Caption = "Status do Pagamento";
            this.colPAGStatus.ColumnEdit = this.repositoryPAGStatus;
            this.colPAGStatus.FieldName = "PAGStatus";
            this.colPAGStatus.Name = "colPAGStatus";
            this.colPAGStatus.OptionsColumn.ReadOnly = true;
            // 
            // repositoryPAGStatus
            // 
            this.repositoryPAGStatus.AutoHeight = false;
            this.repositoryPAGStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPAGStatus.Name = "repositoryPAGStatus";
            // 
            // colPAG_GPS
            // 
            this.colPAG_GPS.Caption = "GPS";
            this.colPAG_GPS.FieldName = "PAG_GPS";
            this.colPAG_GPS.Name = "colPAG_GPS";
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // cCheque
            // 
            this.Controls.Add(this.bOletoDetalheGridControl);
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCheque";
            this.Size = new System.Drawing.Size(816, 584);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.bOletoDetalheGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChequebindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboPAGTipoDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEmissaoDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLEmissaoDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VenctoDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VenctoDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TFavorecido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOLValorPrevistoSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOletoDetalheGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PagbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PLALookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlalookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PLAnocontasbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPAGStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton BotCEmissao;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.DateEdit bOLEmissaoDateEdit;
        private DevExpress.XtraEditors.DateEdit VenctoDateEdit;
        private DevExpress.XtraEditors.TextEdit TFavorecido;
        private DevExpress.XtraEditors.SpinEdit bOLValorPrevistoSpinEdit;
        private DevExpress.XtraGrid.GridControl bOletoDetalheGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit PlalookUpEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit PLALookUpEdit1;
        private System.Windows.Forms.BindingSource ChequebindingSource;
        private System.Windows.Forms.BindingSource PagbindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGDOC;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGN;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNNome;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFantasia;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private System.Windows.Forms.TextBox cHENumeroTextBox;
        private System.Windows.Forms.BindingSource PLAnocontasbindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboPAGTipoDefault;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboStatus;
        private DevExpress.XtraEditors.SimpleButton BotCRecibo;
        private DevExpress.XtraEditors.SimpleButton BotCCopia;
        private DevExpress.XtraEditors.SimpleButton ButtonEletronico;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraEditors.SimpleButton BBaixaManual;
        private DevExpress.XtraEditors.SimpleButton BotEletronico;
        private DevExpress.XtraEditors.SimpleButton BotGuia;
        private DevExpress.XtraEditors.SimpleButton BotCComprovante;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_GPS;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryPAGStatus;
        private DevExpress.XtraEditors.SimpleButton SBAgrupar;
    }
}
