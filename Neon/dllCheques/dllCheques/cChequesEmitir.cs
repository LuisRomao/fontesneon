/*
MR - 20/05/2014 13:00  13.2.9.3  - N�o permite a emiss�o de pagamento eletr�nico de t�tulo sem informa��o de linha digit�vel, j� que � possivel cadastrar o pagamento sem o t�tulo na m�o (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (20/05/2014 13:00) ***)
                                   Diferencia a cor da c�lula de tipo de pagamento quando for pagamento eletr�nico (azul)
MR - 22/07/2014 17:00            - Corrigido o erro de abertura de impressora de cheques, feito apenas se n�o for pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (22/07/2014 17:00) ***)
MR - 23/07/2014 10:00            - Inclus�o na tela de retorno dos cheques com status aguardando retorno referentes a pagamentos eletr�nicos j� vencidos e que n�o foram efetivamente pagos, com cor diferenciada (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (23/07/2014 10:00) ***)
                                   Inclus�o de novo filtro para cheques na situa��o acima (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (23/07/2014 10:00) ***)
MR - 30/07/2014 16:00            - Tratamento do novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 06/08/2014 13:00            - Inclus�o na tela de retorno dos cheques com status inconsistentes (pagamentos recusados), com cor diferenciada (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***)
                                   Inclus�o de novo filtro para cheques na situa��o acima (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***)
LH - 25/12/2014 14:20  14.1.4.96 - Pagamento com boleto integrado na mesma tela de emiss�o de cheque
LH - 28/12/2014 15:20  14.2.4.9  - Fix. erro nas chaves {} n�o estava colocrindo a coluan de ida e volta
LH - 21/02/2015 08:20  14.2.4.30 - Reestrutura��o do radioGroup1
MR - 27/04/2015 12:00            - Emiss�o de tributo eletronico do tipo "guia de recolhimento - eletr�nico" (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
MR - 20/05/2015 17:00            - N�o deixa alterar o cheque quando � do tipo Guia ou Guia Eletr�nica (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***)
MR - 15/02/2016 10:00            - Fica vermelho no grid apenas um dia antes no caso de pagamentos eletr�ncos aguardando retorno, mesmo que o retorno do malote seja outro (Altera��es indicadas por *** MRC - INICIO - PAGAMENTO ELETRONICO (15/02/2016 10:00) ***)
*/

using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors.Controls;
using Framework;
using Framework.objetosNeon;
using FrameworkProc;
using FrameworkProc.datasets;
using virImpressoraCh;
using CompontesBasicosProc;
using BoletosCalc;
using System.Collections.Generic;
using dllChequesProc;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using Abstratos;


namespace dllCheques
{


    /// <summary>
    /// 
    /// </summary>
    public partial class cChequesEmitir : CompontesBasicos.ComponenteBaseBindingSource
    {
        /// <summary>
        /// 
        /// </summary>
        public enum Radio
        {
            /// <summary>
            /// 
            /// </summary>
            Emitir,            
            //EmitirAntigos,
            /// <summary>
            /// 
            /// </summary>
            Emitidos,            
            //EmitidosAntigos,
            /// <summary>
            /// 
            /// </summary>
            AguardaRetorno,
            /// <summary>
            /// 
            /// </summary>
            AguardaCopia,
            /// <summary>
            /// Aguarda d�bito
            /// </summary>
            AguardaDebito
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            int Tipo = (int)args[0];
            int USUSel;
            switch (Tipo)
            {
                case 1:
                    USUSel = (int)args[1];
                    selAtrasados(cChequesEmitir.Radio.Emitir, lookupGerente1.USUSel);
                    break;
                case 2:
                    USUSel = (int)args[1];
                    selAtrasados(cChequesEmitir.Radio.AguardaRetorno, lookupGerente1.USUSel);
                    break;
                case 3:
                    USUSel = (int)args[1];
                    selAtrasados(cChequesEmitir.Radio.AguardaCopia, lookupGerente1.USUSel);
                    break;
                case 4:
                    int CHETipo = (int)args[1];
                    bool CONProcuracao = (bool)args[2];
                    malote.TiposMalote CONMalote = (malote.TiposMalote)args[3];
                    DateTime CHEVencimento = (DateTime)args[4];
                    malote M1 = null;
                    malote M2 = null;
                    bool RetornaNoVencimento;
                    return new object[] {Cheque.CalculaDatas((PAGTipo)CHETipo, CONProcuracao, CONMalote, CHEVencimento, DateTime.Now, ref M1, ref M2, out RetornaNoVencimento)};                      
            }
            return null;
        }

        #region Construtores

        
        //public cChequesEmitir(bool _Honorarios)
        //{
        //    InitializeComponent();
        //    Honorarios = _Honorarios;
        //    Fnegro = new Font(colMaloteIda.AppearanceCell.Font, FontStyle.Bold);
        //    Fnormal = new Font(colMaloteIda.AppearanceCell.Font, FontStyle.Regular);
        //}

        /// <summary>
        /// Construtor principal
        /// </summary>
        public cChequesEmitir()
        {
            InitializeComponent();
            uSUariosBindingSource.DataSource = dUSUarios.dUSUariosStTodos;
            radioGroup1.Properties.Items.Clear();
            radioGroup1.Properties.Items.AddRange(new RadioGroupItem[]
                {
                new RadioGroupItem(Radio.Emitir, "Emitir"),                                //0
                //new RadioGroupItem(Radio.EmitirAntigos, "Antigos",false),                  //1
                new RadioGroupItem(Radio.Emitidos, "Emitidos"),                            //2
                //new RadioGroupItem(Radio.EmitidosAntigos, "Emitidos (Antigos)",false),     //3
                new RadioGroupItem(Radio.AguardaRetorno, "Retorno"),                       //4 
                new RadioGroupItem(Radio.AguardaCopia, "Ag. C�pia"),
                new RadioGroupItem(Radio.AguardaDebito,"Ag. D�bito na conta")
                });
            dateEdit1.DateTime = DateTime.Today.AddDays(20);
            DataInicio.DateTime = DateTime.Today.AddDays(-2);
            Framework.Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(colPAGTipo);
            Framework.Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);                        
            //chPurple.Visible = chPurple.Checked = false;            
            //chVioleta.Visible = chVioleta.Checked = false;            
            radioGroup1.EditValue = Radio.Emitir;
            AjustaTela();
            //CarregaDados();            
            simpleButton2.Visible = simpleButton3.Visible = ((Impressor.ModeloImpressora == ImpCheque.Modelos.Perto) || (Impressor.ModeloImpressora == ImpCheque.Modelos.Tela));

#if (DEBUG)
            simpleButton5.Visible = true;
#endif
            if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
            {
                chTESTE.Visible = true;
                timeEdit1.Visible = true;
                timeEdit1.Time = DateTime.Now;
                BotCorrecao.Visible = true;
            }

        } 
        #endregion
 

        #region Componentes privados
        private ImpCheque _Impressor;

        


        private ImpCheque Impressor
        {
            get
            {
                if (_Impressor == null)
                    //if (CompontesBasicos.FormPrincipalBase.FormPrincipal.EmProducao)
                    _Impressor = ImpCheque.ImpChequeSt;
                    //else
                    //    _Impressor = new ImpCheque(ImpCheque.Modelos.Tela);
                return _Impressor;
            }
        }

        //private CodBar.Impress _ImpCopia;
/*
        private string MascaraCopia = 
Environment.NewLine +
"OC,Fr\r\n" +
"Q20,0\r\n" +
"q600\r\n" +
"N\r\n" +
"%QuadroIDAVOLTA%"+ 
"A200,10,1,2,2,2,R,\"%CONCodigo%\"\r\n" +
"B150,10,1,2C,1,4,100,N,\"%CHE%\"\r\n" + 
"%DETALHES%"+
"X30,400,10,350,1200\r\n" +
"A330,410,1,4,1,1,N,\"%CHE_BCO%\"\r\n" +
"A330,500,1,4,1,1,N,\"%CONAgencia%\"\r\n" +
"A330,640,1,4,1,1,N,\"%CONConta%\"\r\n" +
"A330,800,1,4,1,1,N,\"%CHENumero%\"\r\n" +
"A330,950,1,3,2,2,N,\"%CHEValor%\"\r\n" + 
"A263,420,1,1,1,1,N,\"Portador:\"\r\n" +
"A270,510,1,4,1,1,N,\"%CHEFavorecido%\"\r\n" +
"A220,610,1,4,1,1,N,\"%Local%, %CHEVencimento%\"\r\n" + 
"LO100,610,5,570\r\n" +
"A90,610,1,2,1,1,N,\"%CONNome%\"\r\n" + 
"P1\r\n";

        private string QuadroIDAVOLTA =
"A600,10,1,2,1,1,N,\"Data de envio para assinatura\"\r\n" +
"A570,10,1,4,1,1,N,\"%CHEIdaData%\"\r\n" +
"A530,10,1,2,1,1,N,\"Data limite de retorno\"\r\n" +
"A500,10,1,5,1,1,N,\"%CHERetornoData%\"\r\n" +
"A430,10,1,3,1,1,N,\"Retorno efetivo\"\r\n" +
"A380,10,1,2,2,2,N,\"__/__/____\"\r\n";


        private CodBar.Impress ImpCopia
        {
            get
            {
                if (_ImpCopia == null)
                {
                    _ImpCopia = new CodBar.Impress(MascaraCopia);
                    _ImpCopia.RemoverAcentos = true;
                    string NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de c�pias de Cheque");
                    if (NomeImpressora == "")
                    {
                        System.Windows.Forms.PrintDialog PD = new System.Windows.Forms.PrintDialog();
                        PD.ShowDialog();
                        NomeImpressora = PD.PrinterSettings.PrinterName;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de c�pias de Cheque",NomeImpressora);
                    }
                    _ImpCopia.NomeImpressora = NomeImpressora;
                    //_ImpCopia.NomeImpressora = "Zebra  TLP2844";                
                    //if(_ImpCopia.NomeImpressora != "Zebra TLP2844")
                    //    MessageBox.Show("<"+_ImpCopia.NomeImpressora+">");
                    //if (_ImpCopia.NomeImpressora == )
                    //    MessageBox.Show("2");
                }
                return _ImpCopia;
            }
        }
        */
        //private bool Honorarios;

        //private ArrayList Strs;
        //private string NomeDoBanco;

        static string Local
        {
            get
            {
                return Framework.DSCentral.EMP == 1 ? "S�o Bernardo do Campo" : "Santo Andr�";
            }
        }

        #endregion

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            Imprimir();            
        }
        

        private dChequesImprimir.ChequesparaEmitirRow LinhaMae {
            get { 
            DataRowView DRV = (DataRowView)gridView1.GetRow(gridView1.FocusedRowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return null;
            else
                return (dChequesImprimir.ChequesparaEmitirRow)DRV.Row;
            }
        }

        private void Imprimir() 
        {
            if (LinhaMae != null)
            {
                //bool Ok;
                if ((radioGroup1.EditValue != null) && ((Radio)radioGroup1.EditValue == Radio.Emitir))
                {
                    
                    if (Imprimir(LinhaMae))
                        CarregaDados();
                }
                else
                    return;                
            }
                
        }

        private static ArrayList Alternativas
        {
            get
            {
                ArrayList Retorno = new ArrayList();
                foreach (Framework.datasets.dFornecedores.FORNECEDORESRow ADV in Framework.datasets.dFornecedores.GetdFornecedoresST("ADV").FORNECEDORES)
                    Retorno.Add(ADV.FRNNome);
                return Retorno;
            }
        }

        /// <summary>
        /// Ultimo Favorecido
        /// </summary>
        public string UltimoFavorecido;

        private static string DetalheDoCh(int linha, string Texto1, string Texto2 ,decimal Valor)
        {
            int P1 = 600 - (linha-1) * 50;
            string Mascara =
"A"+P1.ToString()+",420,1,3,1,1,N,\""+Texto1+"\"\r\n" +
"A" + (P1 - 10).ToString() + ",1020,1,1,2,2,N,\"" + Valor.ToString("n2").PadLeft(9) + "\"\r\n" +
"A" + (P1 - 20).ToString() + ",420,1,3,1,1,N,\"" + Texto2 + "\"\r\n";
            return Mascara;
        }


      

        private Cheque Cheque;

        private bool Imprimir(dChequesImprimir.ChequesparaEmitirRow CheRow)        
        {
            Cheque = new Cheque(CheRow.CHE, dllCheques.Cheque.TipoChave.CHE);
            if (Cheque.Status == CHEStatus.CadastradoBloqueado)
            {
                MessageBox.Show("Cheque n�o liberado.");
                return false;
            }
            if (Cheque.Status == CHEStatus.AguardaEnvioPE)
            {
                MessageBox.Show("Cheque ainda n�o enviado ao banco.");
                return false;
            }
            if (Cheque.Status == CHEStatus.AguardaConfirmacaoBancoPE)
            {
                MessageBox.Show("Cheque enviado ao banco mas ainda n�o confirmado.");
                return false;
            }
            if (Cheque.Status.EstaNoGrupo(CHEStatus.PagamentoInconsistentePE, CHEStatus.PagamentoNaoEfetivadoPE))
            {
                MessageBox.Show("Cheque j� enviado uma vez. Se for o caso cancele e envie o substituto");
                return false;
            }
            int NumeroCheque = 0;                            
            try
            {                
                if(Cheque.isEletronico)
                {
                    //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    if (Cheque.Tipo == PAGTipo.PEGuia)
                    {
                        if (MessageBox.Show(string.Format("Confirma a emiss�o da Guia de Recolhimento Eletr�nica abaixo ?\n\nVencimento - {0}\nValor Total - {1}", CheRow.CHEVencimento.ToString("dd/MM/yyyy"), CheRow.CHEValor.ToString("0.00")), "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                            return false;
                    }
                    else
                    {
                        //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***                   
                        if ((Cheque.Tipo == PAGTipo.PETituloBancario || Cheque.Tipo == PAGTipo.PETituloBancarioAgrupavel) && Cheque.CHErow.IsCHEAdicionalNull())
                        {
                            if (!SolicitaLinhaDigitavel(Cheque))
                                return false;
                        }
                        if (MessageBox.Show(string.Format("Confirma a emiss�o do Pagamento Eletr�nico abaixo ?\n\nVencimento - {0}\nValor - {1}", CheRow.CHEVencimento.ToString("dd/MM/yyyy"), CheRow.CHEValor.ToString("0.00")), "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                            return false;
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    }
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                }
                else if (Cheque.Tipo == PAGTipo.ChequeSindico)
                {
                    if (dChequesImprimir.ChequesSindicoTableAdapter.Fill(dChequesImprimir.ChequesSindico, CheRow.CON) == 0)
                    {
                        MessageBox.Show("N�o existem folhas de cheque com o s�ndico!!");
                        return false;
                    }
                    cChequeSindico cChequeSindico1 = new cChequeSindico(dChequesImprimir);
                    if (cChequeSindico1.ShowEmPopUp() != DialogResult.OK)
                        return false;

                    NumeroCheque = cChequeSindico1.FCHrow.FCHNumero;
                    CheRow.CHE_CCT = cChequeSindico1.FCHrow.CCT;
                    
                }
                else
                {
                    Impressor.Abrir();
                    if ((Impressor.ModeloImpressora == ImpCheque.Modelos.Perto) && (!chNaoVerificar.Checked))
                    {
                        if (!Impressor.ChequeDentro)
                            if (!Impressor.LeDadosDoCheque())
                            {
                                MessageBox.Show("Erro:" + Impressor.StatusPerto());
                                return false;
                            };
                        NumeroCheque = (int)Impressor.CMC7Numero;
                        if ((int.Parse(CheRow.CONAgencia) != Impressor.CMC7Agencia) || (int.Parse(CheRow.CONConta) != Impressor.CMC7Conta))
                        {
                            MessageBox.Show("Ag�ncia/Conta do cheque n�o confere\r\nAg�ncia cadastro:" + CheRow.CONAgencia.ToString() + " Agencia do cheque:" + Impressor.CMC7Agencia.ToString() + "\r\nConta Cadastrada:" + CheRow.CONConta.ToString() + " conta do cheque:" + Impressor.CMC7Conta.ToString());
                            Impressor.RemoveCheque();
                            return false;
                        }

                    }
                    else
                    {
                        if ((!VirInput.Input.Execute(CheRow.CONCodigo + " N�", out NumeroCheque)) || (NumeroCheque <= 0))
                            return false;
                    }
                    if (CheRow.IsCHEFavorecidoNull() || (CheRow.CHEFavorecido == ""))
                    {
                        UltimoFavorecido = "";
                        if (!VirInput.Input.Execute("Favorecido", ref UltimoFavorecido, false))
                            return false;
                        else
                            CheRow.CHEFavorecido = UltimoFavorecido;
                    }
                    ImpCheque.Status Status = Impressor.VerificaStatus();
                    if ((Status != ImpCheque.Status.sem_papel) && ((Status != ImpCheque.Status.ok)))
                    {
                        MessageBox.Show("Status da impressora:" + Status.ToString());
                        Impressor.Fechar();
                        return false;
                    }
                    if (Status == ImpCheque.Status.sem_papel)
                        MessageBox.Show("Coloque o cheque");

                    if (!Impressor.Imprime(CheRow.CON_BCO, CheRow.CHEValor, Local, CheRow.CHEFavorecido, CheRow.CHEVencimento))
                    {
                        if (Impressor.ModeloImpressora == ImpCheque.Modelos.Perto)
                            MessageBox.Show("Erro:" + Impressor.StatusPerto());
                        return false;
                    };

                    chNaoVerificar.Checked = false;
                    if (MessageBox.Show("Vire o cheque", "Impress�o", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                        return false;
                    Impressor.Imprime(Cheque.Verso);
                }
                
                if (!Cheque.Emite(NumeroCheque, CheRow.CON_BCO, int.Parse(CheRow.CONAgencia), int.Parse(CheRow.CONConta), CheRow.IsCHE_CCTNull() ? 0 : CheRow.CHE_CCT))                
                {
                    MessageBox.Show(String.Format("          ATEN��O\r\n\r\nErro oa gravar emiss�o do cheque!!\r\nA grava��o N�O foi feita.\r\nVerifique se o cheque n�o foi emitito simultaneamente por outra esta��o.\r\n\r\nErro reportado:\r\n{0}", Cheque.UltimaException.Message));
                    return false;
                };

                if (dChequesImprimir.ChequesparaEmitir.Count <= 1)
                    FiltroAtivo = false;
                BotRecibo.Enabled = true;

                if (Cheque.ImprimeCopia())
                    return Cheque.ImprimirCopia(false);


                return true;

            }
            finally
            {
                if(!Cheque.isEletronico)               
                   Impressor.Fechar();
            }
        }

        private bool SolicitaLinhaDigitavel(Cheque Cheque)
        {           
            DateTime datVencimento = DateTime.Today;
            decimal decValor = 0;
            string strPAGAdicional = "";
            if (!Cheque.SolicitaLinhaDigitavel("�nica", ref strPAGAdicional, ref datVencimento, ref decValor))
                return false;

            //Verifica se valor e vencimento nao est�o de acordo com o cheque
            if (decValor == 0)
                decValor = Cheque.CHErow.CHEValor;
            if ((datVencimento != Cheque.CHErow.CHEVencimento) || (decValor != Cheque.CHErow.CHEValor))
            {
                MessageBox.Show(string.Format("Vencimento e/ou valor indicados na linha digit�vel informada n�o conferem com os dados do pagamento. Por favor, verifique se o t�tulo est� correto."));
                return false;
            }
            Cheque.CHErow.CHEAdicional = strPAGAdicional;
            return true;

        }
        


        private void ImprimeCopia(dChequesImprimir.ChequesparaEmitirRow CheRow,int NumeroCheque,bool ComCodBar) 
        {
            Cheque Cheque1 = new dllCheques.Cheque(CheRow.CHE,dllCheques.Cheque.TipoChave.CHE);
            if(Cheque1.Encontrado)
                Cheque1.ImprimirCopia(true);            
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
                 Imprimir();
        }

        private Radio Tiporadio
        {
            get
            {
                if (radioGroup1.EditValue == null)
                    return Radio.Emitir;
                else
                    return (Radio)radioGroup1.EditValue;
            }
        }

        private enum CorStatusVencimento
        {
            branco,
            amarelo,
            vermelho,            
            NaoTratado
        }

        private CorStatusVencimento Classifica(dChequesImprimir.ChequesparaEmitirRow linha)
        {
            //if (linha.CHEStatus == (int)CHEStatus.PagamentoInconsistentePE)
            //    return CorStatusVencimento.Purpura;
            switch (Tiporadio)
            {
                case Radio.Emitir:
                    break;
                case Radio.Emitidos:
                    break;
                case Radio.AguardaRetorno:
                    break;
                case Radio.AguardaCopia:
                    break;
                case Radio.AguardaDebito:
                    DateTime DataRef = DataComCarencia((PAGTipo)linha.CHETipo);
                    if (linha.CHEVencimento < DataRef)
                        return CorStatusVencimento.vermelho;
                    else if (linha.CHEVencimento == DataRef)
                        return CorStatusVencimento.amarelo;
                    else
                        return CorStatusVencimento.branco;
                default:
                    throw new NotImplementedException(string.Format("Tipo r�dio n�o tratado em cChequeEmitir.cs 583 = {0}", Tiporadio));
            }
            return CorStatusVencimento.NaoTratado;
        }

        private void Filtrar()
        {
            DateTime Agora = DateTime.Now;
            if (chTESTE.Checked)
                Agora = timeEdit1.Time;
            int Horadecorte = ((Agora.Hour > 10) ? 24 : 11);
            if (Tiporadio == Radio.AguardaRetorno)
                Horadecorte = 17;
            DateTime proxMalote = DateTime.Today.AddHours(Horadecorte);
            List<dChequesImprimir.ChequesparaEmitirRow> remover = new List<dChequesImprimir.ChequesparaEmitirRow>();
            foreach (dChequesImprimir.ChequesparaEmitirRow rowTestar in dChequesImprimir.ChequesparaEmitir)
            {                                
                switch (Tiporadio)
                {
                    case Radio.AguardaDebito:
                        switch (Classifica(rowTestar))
                        {
                            case CorStatusVencimento.branco:
                                if (!chCinza.Checked)
                                    remover.Add(rowTestar);
                                break;
                            case CorStatusVencimento.amarelo:
                                if (!chAmarelo.Checked)
                                    remover.Add(rowTestar);
                                break;
                            case CorStatusVencimento.vermelho:
                                if (!chVermelho.Checked)
                                    remover.Add(rowTestar);
                                break;                            
                            case CorStatusVencimento.NaoTratado:
                                break;
                            default:
                                break;
                        }
                        break;
                    case Radio.Emitir:
                        if (rowTestar.CHEIdaData < Agora)
                        {
                            if (!chVermelho.Checked)
                                remover.Add(rowTestar);
                        }
                        else if (rowTestar.CHEIdaData <= proxMalote)
                        {
                            if (!chAmarelo.Checked)
                                remover.Add(rowTestar);
                        }
                        else
                        {
                            if (!chCinza.Checked)
                                remover.Add(rowTestar);
                        }
                        break;
                    case Radio.AguardaRetorno:
                        //*** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***
                        if (rowTestar.CHEStatus == (int)CHEStatus.PagamentoInconsistentePE)
                        {
                            //if (!chPurple.Checked)
                            //    remover.Add(rowTestar);
                        }
                        //*** MRC - INICIO - PAG-FOR (23/07/2014 10:00) ***
                        else if (rowTestar.CHEStatus == (int)CHEStatus.Retornado && rowTestar.CHE == rowTestar.CHENumero)
                        //*** MRC - TERMINO - PAG-FOR (06/08/2014 13:00) ***
                        {
                            //if (!chVioleta.Checked)
                              //  remover.Add(rowTestar);
                        }
                        else if (rowTestar.CHERetornoData < Agora)
                        //*** MRC - TERMINO - PAG-FOR (23/07/2014 10:00) ***
                        {
                            if (!chVermelho.Checked)
                                remover.Add(rowTestar);
                        }
                        else if (rowTestar.CHERetornoData < proxMalote)
                        {
                            if (!chAmarelo.Checked)
                                remover.Add(rowTestar);
                        }
                        else
                        {
                            if (!chCinza.Checked)
                                remover.Add(rowTestar);
                        }
                        break;
                    case Radio.Emitidos:

                        break;
                }
            };
            foreach (dChequesImprimir.ChequesparaEmitirRow rowdel in remover)
                rowdel.Delete();

            dChequesImprimir.ChequesparaEmitir.AcceptChanges();

        }

//bool ajustado = false;
        private void CarregaDados()
        {            
            if (radioGroup1.EditValue == null)
                return;            
            try
            {
                gridView1.BeginDataUpdate();
                
                switch (Tiporadio)
                {
                    case Radio.Emitir:
                        dChequesImprimir.DetalhesCHE.Clear();
                        if (FiltroAtivo)
                        {
                            string mascara = (Impressor.CMC7Banco == 341 ? "00000" : "000000");
                            if (dChequesImprimir.ChequesparaEmitirTableAdapter.Fillfiltrado(dChequesImprimir.ChequesparaEmitir, dateEdit1.DateTime, Impressor.CMC7Banco, Impressor.CMC7Conta.ToString(mascara), Impressor.CMC7Agencia.ToString("0000")) == 0)
                                MessageBox.Show(string.Format("Cheque n�o encontrado:\r\n{0} {1:0000} {2}", Impressor.CMC7Banco, Impressor.CMC7Agencia, Impressor.CMC7Conta));
                        }
                        else                        
                            dChequesImprimir.ChequesparaEmitirTableAdapter.Fill(dChequesImprimir.ChequesparaEmitir, dateEdit1.DateTime);                            
                        

                        dChequesImprimir.DetalhesCHETableAdapter.FillBy(dChequesImprimir.DetalhesCHE, dateEdit1.DateTime);
                        dChequesImprimir.AjustaTitulos(dChequesImprimir.Agrupamento.malote_Envio);
                        if (!FiltroAtivo)
                            Filtrar();
                        gridView1.GroupSummarySortInfo.Clear();
                        gridView1.GroupFormat = " [#image]{1} {2}";
                        gridView1.GroupSummarySortInfo.Add(gridView1.GroupSummary[1], ColumnSortOrder.Ascending, colTituloGrupo);                        
                        break;
                       
                    case Radio.Emitidos:
                        dChequesImprimir.DetalhesCHE.Clear();
                        if (FiltroAtivo)
                        {
                            string mascara = (Impressor.CMC7Banco == 341 ? "00000" : "000000");
                            int Carregados = dChequesImprimir.ChequesparaEmitirTableAdapter.FillByNumero(dChequesImprimir.ChequesparaEmitir, Impressor.CMC7Banco, Impressor.CMC7Conta.ToString(mascara), Impressor.CMC7Agencia.ToString("0000"), Impressor.CMC7Numero);
                            if (Carregados > 0)
                                dChequesImprimir.DetalhesCHETableAdapter.Fill(dChequesImprimir.DetalhesCHE, dChequesImprimir.ChequesparaEmitir[0].CHE);
                        }
                        else
                        {  
                            dChequesImprimir.ChequesparaEmitirTableAdapter.FillEmitidos(dChequesImprimir.ChequesparaEmitir, DataInicio.DateTime, dateEdit1.DateTime);
                            dChequesImprimir.DetalhesCHETableAdapter.FillEmitidos(dChequesImprimir.DetalhesCHE, DataInicio.DateTime, dateEdit1.DateTime);
                        }
                        dChequesImprimir.AjustaTitulos(dChequesImprimir.Agrupamento.Condominio);
                        gridView1.GroupSummarySortInfo.Clear();
                        gridView1.GroupFormat = " [#image]{1}";
                       
                        break;                        
                    case Radio.AguardaRetorno:
                        dChequesImprimir.DetalhesCHE.Clear();
                        dChequesImprimir.ChequesparaEmitirTableAdapter.FilAguarda(dChequesImprimir.ChequesparaEmitir);
                        dChequesImprimir.DetalhesCHETableAdapter.FilAguarda(dChequesImprimir.DetalhesCHE);
                        dChequesImprimir.AjustaTitulos(dChequesImprimir.Agrupamento.malote_Retorno);
                        Filtrar();
                        gridView1.GroupSummarySortInfo.Clear();
                        gridView1.GroupFormat = " [#image]{1} {2}";
                        gridView1.GroupSummarySortInfo.Add(gridView1.GroupSummary[1], ColumnSortOrder.Ascending, colTituloGrupo);
                        
                        break;
                    case Radio.AguardaCopia:
                        dChequesImprimir.DetalhesCHE.Clear();
                        dChequesImprimir.ChequesparaEmitirTableAdapter.FillAguardaRecibo(dChequesImprimir.ChequesparaEmitir);
                        RemoveBalanceteFechado();
                        dChequesImprimir.DetalhesCHETableAdapter.FilAguarda(dChequesImprimir.DetalhesCHE);
                        dChequesImprimir.AjustaTitulos(dChequesImprimir.Agrupamento.malote_Retorno);
                        Filtrar();
                        gridView1.GroupSummarySortInfo.Clear();
                        gridView1.GroupFormat = " [#image]{1} {2}";
                        gridView1.GroupSummarySortInfo.Add(gridView1.GroupSummary[1], ColumnSortOrder.Ascending, colTituloGrupo);
                        break;
                    case Radio.AguardaDebito:
                        dChequesImprimir.DetalhesCHE.Clear();
                        dChequesImprimir.ChequesparaEmitirTableAdapter.FillByCompensar(dChequesImprimir.ChequesparaEmitir);
                        //RemoveBalanceteFechado();
                        dChequesImprimir.DetalhesCHETableAdapter.FillByCompensar(dChequesImprimir.DetalhesCHE);
                        dChequesImprimir.AjustaTitulos(dChequesImprimir.Agrupamento.dataPrevista);
                        Filtrar();
                        gridView1.GroupSummarySortInfo.Clear();
                        gridView1.GroupFormat = " [#image]{1}";
                        gridView1.GroupSummarySortInfo.Add(gridView1.GroupSummary[1], ColumnSortOrder.Ascending, colTituloGrupo);
                        break;
                    default:
                        throw new NotImplementedException(string.Format("CarregaDados enumera��o n�o tratada: {0}", Tiporadio));
                }
            }
            finally
            {
                gridView1.EndDataUpdate();
                gridView1.ExpandAllGroups();
            };
        }


        private void RemoveBalanceteFechado()
        {
            bool temalteracao = false;
            dChequesImprimir.BALancetesTableAdapter.Fill(dChequesImprimir.BALancetes);

            foreach (dChequesImprimir.ChequesparaEmitirRow rowTestar in dChequesImprimir.ChequesparaEmitir)
            {
                dChequesImprimir.BALancetesRow BALrow = dChequesImprimir.BALancetes.FindByBAL_CON(rowTestar.CON);
                if ((BALrow != null) && (!rowTestar.IsSCCDataNull()) && (rowTestar.SCCData <= BALrow.DataCorte))
                {
                    temalteracao = true;
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update cheques set chestatus = 7 where che = @P1",rowTestar.CHE);
                }
            }

            if (temalteracao)
                dChequesImprimir.ChequesparaEmitirTableAdapter.FillAguardaRecibo(dChequesImprimir.ChequesparaEmitir);
        }

        //private Font Fnegro;
        //private Font Fnormal;

        bool blocCH = false;        

        private void AjustaTela()
        {            
            grupoFiltro.Enabled = Tiporadio.EstaNoGrupo(Radio.Emitir, Radio.AguardaRetorno, Radio.AguardaCopia,Radio.AguardaDebito);            
            blocCH = true;
            if(Tiporadio.EstaNoGrupo(Radio.AguardaDebito))
            {
                chVermelho.Checked = chAmarelo.Checked = true;
                chCinza.Checked = false;            
            }
            else
            {
                chVermelho.Checked = chAmarelo.Checked = chCinza.Checked = true;            
            }                        
            blocCH = false;            
            BtCopia.Visible = DataInicio.Visible = Tiporadio.EstaNoGrupo(Radio.Emitidos);
            dateEdit1.Visible = labelControl1.Visible = (Tiporadio != Radio.AguardaRetorno);
            colCHEEmissao.Visible = colCHENumero.Visible = Tiporadio != Radio.Emitir;            
            Font Fnegro = new Font(colMaloteIda.AppearanceCell.Font, FontStyle.Bold);
            Font Fnormal = new Font(colMaloteIda.AppearanceCell.Font, FontStyle.Regular);
            colMaloteIda.AppearanceCell.Font = (Tiporadio == Radio.Emitir) ? Fnegro : Fnormal;
            colMaloteVolta.AppearanceCell.Font = Fnormal;
            colMaloteVolta.AppearanceCell.Font = Fnormal;
            colMaloteIda.Visible = colMaloteVolta.Visible = Tiporadio != Radio.AguardaDebito;            
            CarregaDados();
            gridView1.Focus();
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            AjustaTela();            
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            CarregaDados();
        }

        private SortedList<PAGTipo, DateTime> _DataComCarencia;

        private DateTime DataComCarencia(PAGTipo PAGTipo)
        {
            if (_DataComCarencia == null)
            {
                _DataComCarencia = new SortedList<PAGTipo, DateTime>();                
                _DataComCarencia.Add(PAGTipo.cheque, DateTime.Today.SomaDiasUteis(4,Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.sindico, DateTime.Today.SomaDiasUteis(6, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.boleto, DateTime.Today.SomaDiasUteis(2, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.deposito, DateTime.Today.SomaDiasUteis(2, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.Guia, DateTime.Today.SomaDiasUteis(2, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.Honorarios, DateTime.Today.SomaDiasUteis(4, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.GuiaEletronica, DateTime.Today.SomaDiasUteis(1, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.Caixinha, DateTime.Today.SomaDiasUteis(0, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.DebitoAutomatico, DateTime.Today.SomaDiasUteis(3, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.Conta, DateTime.Today.SomaDiasUteis(2, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.Acumular, DateTime.Today.SomaDiasUteis(0, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.PECreditoConta, DateTime.Today.SomaDiasUteis(1, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.PETransferencia, DateTime.Today.SomaDiasUteis(1, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.PEOrdemPagamento, DateTime.Today.SomaDiasUteis(1, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.PETituloBancario, DateTime.Today.SomaDiasUteis(1, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.PETituloBancarioAgrupavel, DateTime.Today.SomaDiasUteis(1, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.PEGuia, DateTime.Today.SomaDiasUteis(1, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.TrafArrecadado, DateTime.Today.SomaDiasUteis(2, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.ChequeSindico, DateTime.Today.SomaDiasUteis(4, Sentido.Traz));
                _DataComCarencia.Add(PAGTipo.Folha, DateTime.Today.SomaDiasUteis(4, Sentido.Traz));
            }
            return _DataComCarencia[PAGTipo];
        }

        //private string teste;

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return;
            dChequesImprimir.ChequesparaEmitirRow linha = ((dChequesImprimir.ChequesparaEmitirRow)DRV.Row);
            DateTime Agora = DateTime.Now;
            if (chTESTE.Checked)
                Agora = timeEdit1.Time;
            DateTime Hoje = Agora.Date;
            int Horadecorte = ((Agora.Hour > 10) ? 24 : 11);
            CHEStatus Status = (CHEStatus)linha.CHEStatus;
            if (e.Column == colCHEFavorecido)
            {                
                if (Status.EstaNoGrupo(CHEStatus.CadastradoBloqueado,
                                       CHEStatus.PagamentoInconsistentePE,
                                       CHEStatus.AguardaEnvioPE,
                                       CHEStatus.PagamentoNaoAprovadoSindicoPE,
                                       CHEStatus.AguardaConfirmacaoBancoPE,
                                       CHEStatus.PagamentoNaoEfetivadoPE))                
                    e.Appearance.BackColor = Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.GetCor(Status);                
            }

            
            else if (e.Column == colCHETipo)
            {
                PAGTipo Tipo = (PAGTipo)linha.CHETipo;
                
                if (Tipo.EstaNoGrupo(PAGTipo.PECreditoConta,
                                     PAGTipo.PEOrdemPagamento,
                                     PAGTipo.PETituloBancario,
                                     PAGTipo.PETituloBancarioAgrupavel,
                                     PAGTipo.PETransferencia,                                     
                                     PAGTipo.PEGuia)                                     
                    &&
                    Status.EstaNoGrupo(CHEStatus.Cadastrado,
                                        CHEStatus.AguardaConfirmacaoBancoPE,
                                        CHEStatus.AguardaEnvioPE,
                                        CHEStatus.PagamentoInconsistentePE,
                                        CHEStatus.PagamentoNaoAprovadoSindicoPE,
                                        CHEStatus.PagamentoNaoEfetivadoPE)
                   )
                    e.Appearance.BackColor = Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.GetCor(Status);                
            }
            

            else if (e.Column == colCHEVencimento)
            {                
                CorStatusVencimento Cor = Classifica(linha);
                switch (Cor)
                {
                    case CorStatusVencimento.branco:
                        return;
                    case CorStatusVencimento.amarelo:
                        e.Appearance.BackColor = Color.Yellow;
                        e.Appearance.ForeColor = Color.Black;
                        return;
                    case CorStatusVencimento.vermelho:
                        e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                        return;                                        
                    case CorStatusVencimento.NaoTratado:
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Cor n�o tratada em cChequeEmitir.cs 882 = {0}",Cor));
                }
                /*
                //if (linha.CHEStatus == (int)CHEStatus.PagamentoInconsistentePE)
                //{
                //    e.Appearance.BackColor = Color.MediumPurple;
                //    e.Appearance.ForeColor = Color.White;
                //}
                //else
                //{
                    DateTime DataRef = Hoje;
                    
                    if (Tiporadio == Radio.AguardaDebito)
                        DataRef = DataComCarencia((PAGTipo)linha.CHETipo);
                    if (linha.CHEVencimento < DataRef)
                    {
                        //*** MRC - INICIO - PAG-FOR (23/07/2014 10:00) ***
                        if (linha.CHEStatus == (int)CHEStatus.Retornado && linha.CHE == linha.CHENumero)
                            e.Appearance.BackColor = Color.Violet;
                        else
                            //*** MRC - TERMINO - PAG-FOR (23/07/2014 10:00) ***
                            e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                    }
                    else
                        if (linha.CHEVencimento == DataRef)
                        {
                            e.Appearance.BackColor = Color.Yellow;
                            e.Appearance.ForeColor = Color.Black;
                        }
                //}
                */
            }
            else if (((e.Column == colCHEIdaData) || (e.Column == colMaloteIda)) && (Tiporadio == Radio.Emitir))
            {
                if ((((PAGTipo)linha.CHETipo).EstaNoGrupo(PAGTipo.Folha,PAGTipo.DebitoAutomatico)) && (Tiporadio == Radio.Emitir))
                {
                    e.Appearance.ForeColor = e.Appearance.BackColor;
                    return;
                }
                if (linha.IsCHEIdaDataNull())
                {
                    e.Appearance.ForeColor = Color.SaddleBrown;
                }
                else
                {                    
                    //if (linha.CHEStatus == (int)CHEStatus.PagamentoInconsistentePE)
                    //{
                    //    e.Appearance.BackColor = Color.MediumPurple;
                    //    e.Appearance.ForeColor = Color.White;
                    //}
                    //if (linha.CHEStatus == (int)CHEStatus.Retornado && linha.CHEVencimento < Agora.Date && linha.CHE == linha.CHENumero)
                    //{
                    //    e.Appearance.BackColor = Color.Violet;
                    //    e.Appearance.ForeColor = Color.White;
                    //}
                    if (linha.CHEIdaData < Agora)                    
                    {
                        e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                    }
                    else
                        if (linha.CHEIdaData <= Hoje.AddHours(Horadecorte))
                        {
                            e.Appearance.BackColor = Color.Yellow;
                            e.Appearance.ForeColor = Color.Black;
                        }
                }
            }

            else if ((e.Column == colCHERetornoData) || (e.Column == colMaloteVolta))
            {                
                if ((linha.TituloGrupo.StartsWith("Assinar at�")) && (Tiporadio == Radio.Emitir))
                {
                    e.Appearance.ForeColor = e.Appearance.BackColor;
                    return;
                }
                if ((((PAGTipo)linha.CHETipo).EstaNoGrupo(PAGTipo.sindico, PAGTipo.Folha, PAGTipo.DebitoAutomatico)) && (Tiporadio == Radio.Emitir))
                {
                    e.Appearance.ForeColor = e.Appearance.BackColor;
                    return;
                }
                if (linha.CHEStatus == (int)CHEStatus.Aguardacopia)
                        e.Appearance.BackColor = Color.Aqua;
                    else
                        if (linha.CHEStatus == (int)CHEStatus.CompensadoAguardaCopia)
                        e.Appearance.BackColor = Color.Lime;

                    //else if (linha.CHEStatus == (int)CHEStatus.PagamentoInconsistentePE)
                    //{
                    //    e.Appearance.BackColor = Color.MediumPurple;
                    //    e.Appearance.ForeColor = Color.White;
                    //}

                    else
                    {
                        if (linha.IsCHERetornoDataNull())
                            return;

                        //*** MRC - INICIO - PAGAMENTO ELETRONICO (15/02/2016 10:00) ***
                        PAGTipo Tipo = (PAGTipo)linha.CHETipo;

                        if (linha.CHEStatus == (int)CHEStatus.AguardaRetorno &&
                                 Tipo.EstaNoGrupo(PAGTipo.PECreditoConta,
                                                           PAGTipo.PEOrdemPagamento,
                                                           PAGTipo.PETituloBancario,
                                                           PAGTipo.PETituloBancarioAgrupavel,
                                                           PAGTipo.PETransferencia,
                                                           PAGTipo.PEGuia))
                        {
                            if (linha.CHEVencimento.Date.AddHours(-24) <= Agora.Date)
                            {
                                e.Appearance.BackColor = Color.Red;
                                e.Appearance.ForeColor = Color.White;
                            }
                        }
                        //*** MRC - TERMINO - PAGAMENTO ELETRONICO (15/02/2016 10:00) ***
                        else if (linha.CHERetornoData < Agora)
                        {
                            e.Appearance.BackColor = Color.Red;
                            e.Appearance.ForeColor = Color.White;
                        }
                        else
                            if (linha.CHERetornoData <= Hoje.AddHours(17))
                        {
                            e.Appearance.BackColor = Color.Yellow;
                            e.Appearance.ForeColor = Color.Black;
                        }
                    }
                
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            CarregaDados();
        }

        private bool FiltroAtivo = false;

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (Impressor.LeDadosDoCheque())
            {
                FiltroAtivo = true;
                CarregaDados();
            }
            else {
                MessageBox.Show("Erro:" + Impressor.StatusPerto());
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (Impressor.ModeloImpressora == ImpCheque.Modelos.Perto)
                Impressor.RemoveCheque();
            FiltroAtivo = false;
            CarregaDados();            
        }

        private void BtCopia_Click(object sender, EventArgs e)
        {
            if (LinhaMae == null)
                return;
            if(Tiporadio == Radio.Emitidos)
                new Cheque(LinhaMae.CHE, dllCheques.Cheque.TipoChave.CHE).ImprimirCopia(true);
            //else if (radioGroup1.SelectedIndex == 3)
              //       ImprimeCopia(LinhaMae, LinhaMae.CHENumero, true);
        }



        /*
        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                bool encontrado = false;
                string Original = textEdit1.Text;
                if (Original.Length > 4)
                {
                    string Limpo;
                    int EMP;
                    int Tipo;
                    int CHE;
                    try
                    {
                        Limpo = (Original[0] == '0') ? Original.Substring(1, Original.Length - 2) : Original.Substring(0, Original.Length - 1);
                        EMP = int.Parse(Limpo.Substring(0, 1));
                        Tipo = int.Parse(Limpo.Substring(1, 2));
                        CHE = int.Parse(Limpo.Substring(3));
                        //MessageBox.Show(string.Format("EMP {0} - Tipo {1} - CHE {2}",EMP,Tipo,CHE));
                    }
                    catch
                    {
                        MessageBox.Show("Leitura inv�lida, tente novamente");
                        textEdit1.Text = "";
                        e.Handled = true;
                        return;
                    }
                    Cheque Cheque = null;
                    if (Tipo == 91) 
                    {
                        Cheque = new Cheque(CHE);
                        Cheque.GravaObs("Usada segunda via da c�pia de cheque");
                        Tipo = 1;
                    };
                    if (Tipo == 1)
                    {
                        if(Cheque == null)
                             Cheque = new Cheque(CHE);
                         //MessageBox.Show(string.Format("Cheque encontrado status:{0}",Cheque.Status));
                        switch (Cheque.Status)
                        {                                
                            case CHEStatus.NaoEncontrado:
                                MessageBox.Show("Cheque n�o encontrado!");
                                break;
                            case CHEStatus.Cancelado:
                                MessageBox.Show("Cheque cancelado!\r\nFa�a a corre��o antes do retorno");
                                break;
                            case CHEStatus.Cadastrado:
                                MessageBox.Show("Cheque n�o emitido");
                                break;
                            case CHEStatus.AguardaRetorno:                               
                            case CHEStatus.Aguardacopia:
                            case CHEStatus.CompensadoAguardaCopia:
                                
                                if (Cheque.Retornar())
                                {
                                    dChequesImprimir.ChequesparaEmitirRow rowdel = dChequesImprimir.ChequesparaEmitir.FindByCHE(CHE);
                                    if (rowdel != null)
                                        rowdel.Delete();
                                }
                                else
                                {
                                    textEdit1.Text = "";
                                    throw new Exception("N�o retornado.\r\nMotivo:" + Cheque.UltimoErro);                                    
                                }
                                
                                break;                            
                            case CHEStatus.Compensado: 
                            case CHEStatus.Retornado:                          
                            default:
                                //MessageBox.Show(string.Format("Abrir tela"));
                                Cheque.Editar(true);
                                //MessageBox.Show(string.Format("Retorno:{0}", Cheque.Editar(true)));
                                break;
                        }
                                                   
                    }
                    else
                        MessageBox.Show("Leitura inv�lida, tente novamente");                    
                }
                textEdit1.Text = "";
                e.Handled = true;
            }
        }*/

        private dCheque _dChequeRec;

        private dCheque dChequeRec {
            get {
                if (_dChequeRec == null)
                {
                    _dChequeRec = new dCheque();
                    _dChequeRec.PerminteDialogo = false;
                };
                return _dChequeRec;
            }
        }

       

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != '\b'))
                e.Handled = true;
        }

        

        private void chRH_CheckedChanged(object sender, EventArgs e)
        {
            CarregaDados();            
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            /*
            string obs = "";            
            if (!VirInput.Input.Execute("Motivo:", ref obs, true))
                return;
            obs = String.Format("{0} - Cancelamento feito por {1}\r\nObs:{2}", DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome, obs);
            dCheque.dChequeSt.ApagarCheque(LinhaMae.CHE);           
            dCheque.dChequeSt.CHEquesTableAdapter.CancelarCheque(obs, LinhaMae.CHE);
            CarregaDados();
            */
        }

        /*
private static string MascaraLista = 
"\r\n" +
"OC,Fr\r\n" +
"Q20,0\r\n" +
"q600\r\n" +
"N\r\n"+
"%conteudo%"+
"P1\r\n";

        private static CodBar.Impress _ImpLista;

        /// <summary>
        /// 
        /// </summary>
        public static CodBar.Impress ImpLista
        {
            get
            {
                if (_ImpLista == null)
                {
                    _ImpLista = new CodBar.Impress(MascaraLista);
                    _ImpLista.RemoverAcentos = true;
                    string NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de c�pias de Cheque");
                    if (NomeImpressora == "")
                    {
                        PrintDialog PD = new PrintDialog();
                        PD.ShowDialog();
                        NomeImpressora = PD.PrinterSettings.PrinterName;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de c�pias de Cheque", NomeImpressora);
                    }
                    _ImpLista.NomeImpressora = NomeImpressora;
                }
                return _ImpLista;
            }
        }
        

        private void botLista_Click(object sender, EventArgs e)
        {
            SortedList Lista = new SortedList();
            //foreach (dChequesImprimir.ChequesparaEmitirRow rowImp in dChequesImprimir.ChequesparaEmitir)
            //{                
            //    if (!Lista.ContainsKey(rowImp.CONCodigo))
            //        Lista.Add(rowImp.CONCodigo, 0);
            //    Lista[rowImp.CONCodigo] = (int)Lista[rowImp.CONCodigo] + 1;
            //};

            for(int i=0;i< gridView1.RowCount;i++)
                
                {
                    dChequesImprimir.ChequesparaEmitirRow rowImp = (dChequesImprimir.ChequesparaEmitirRow)gridView1.GetDataRow(i);
                    if (rowImp == null)
                        continue;
                    if (!Lista.ContainsKey(rowImp.CONCodigo))
                        Lista.Add(rowImp.CONCodigo, 0);
                    Lista[rowImp.CONCodigo] = (int)Lista[rowImp.CONCodigo] + 1;
                };

            string strTXT = "";

            int linha = 1;
            foreach (DictionaryEntry DE in Lista)            
                strTXT += String.Format("A30,{0},0,4,1,1,N,\"{1} = {2}\"\r\n", linha++ * 25, DE.Key, DE.Value);
            
            ImpLista.Reset();
            ImpLista.SetCampo("%conteudo%", strTXT);
            ImpLista.ImprimeDOC();
        }
        */

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Cheque Cheque = new Cheque(LinhaMae.CHE, dllCheques.Cheque.TipoChave.CHE);
            if (!Cheque.Encontrado)
            {
                MessageBox.Show("Este cheque n�o est� mais dispon�vel");
                return;
            }

            /* Este trecho foi deslado porque os cheques eletr�nicos em teoria podem ser editados. Acredito que a inten��o era limitar s� as guias. 
               Estou deixando esta informa��o caso venhamos a ter problemas no futuro com a libera��o da edi��o 
           //*** MRC - INICIO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
           //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
            bool SomenteLeitura = false;
            if (Cheque.Tipo == PAGTipo.PECreditoConta || Cheque.Tipo == PAGTipo.PEOrdemPagamento ||
                Cheque.Tipo == PAGTipo.PETituloBancario || Cheque.Tipo == PAGTipo.PETituloBancarioAgrupavel || Cheque.Tipo == PAGTipo.PETransferencia ||
                Cheque.Tipo == PAGTipo.PEGuia || Cheque.Tipo == PAGTipo.Guia)
                SomenteLeitura = true;
            //*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) ***
            //*** MRC - TERMINO - PAGAMENTO ELETRONICO (20/05/2015 17:00) ***
            */

            bool SomenteLeitura = false;
            if (Cheque.Tipo.EstaNoGrupo(PAGTipo.PEGuia, PAGTipo.Guia))
                SomenteLeitura = true;


            if (Cheque.Editar(SomenteLeitura) == DialogResult.OK)
               CarregaDados();
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            cRastrear Rastreador = new cRastrear();
            if (Rastreador.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                if (Rastreador.lookupCondominio_F1.CON_sel <= 0)
                    return;
                string strCHEs = "SELECT CHE From CHEques WHERE (CHENumero = @P1) AND (CHE_CON = @P2)";
                DataTable CHEs = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(strCHEs, Rastreador.Numero.Value, Rastreador.lookupCondominio_F1.CON_sel);
                if ((CHEs != null) && (CHEs.Rows.Count > 0))
                    foreach (DataRow dr in CHEs.Rows)
                    {
                        Cheque Cheque = new Cheque((int)dr[0], dllCheques.Cheque.TipoChave.CHE);
                        cCheque cCheque = new cCheque(Cheque.CHErow, false);
                        cCheque.Titulo = String.Format("CHEQUE {0}", dr[0]);
                        cCheque.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                    }
                else
                    MessageBox.Show("N�o Encontrado");
            };
            return;            
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (Cheque != null)
                if (Cheque.Encontrado)
                    Cheque.Recibo("");
        }

//        private string ComandoAntigo =
//"SELECT     ContadorCheque, Pago\r\n" +
//"FROM         cheques\r\n" +
//"WHERE     (ContadorCheque = @P1);";

        

        
        private void repositoryItemButtonEdit2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (gridControl1.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView; 
                dChequesImprimir.DetalhesCHERow rowDetChe = (dChequesImprimir.DetalhesCHERow)GV.GetFocusedDataRow();
                if (rowDetChe != null)
                {
                    CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos("ContaPagar.cNotasCampos", "NOTA", rowDetChe.NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                }
            }
        }

        private void BotRecibo_Click(object sender, EventArgs e)
        {
            if (Cheque != null)
                if (Cheque.Encontrado)
                    Cheque.Recibo("");
        }

        private void simpleButton5_Click_1(object sender, EventArgs e)
        {
            cRetornoCheque cRetorno = new cRetornoCheque();
            cRetorno.ShowEmPopUp();
            CarregaDados();
        }

        /// <summary>
        /// Seleciona somente os atrasados para emitir ou retornar.
        /// </summary>
        public void selAtrasados(Radio radio,int Gerente)
        {
            blocCH = true;
            radioGroup1.EditValue = radio;
            chVermelho.Checked = true;            
            chAmarelo.Checked = chCinza.Checked = false;            
            blocCH = false;
            CarregaDados();
            lookupGerente1.USUSel = Gerente;
        }

        private void checkEdit1_CheckStateChanged(object sender, EventArgs e)
        {
            if (!blocCH)
            {
                blocCH = true;
                CarregaDados();
                blocCH = false;
            }
        }

        private void lookupGerente1_alterado(object sender, EventArgs e)
        {
            if (lookupGerente1.USUSel == -1)
            {
                if (gridView1.ActiveFilterString.Contains("[CONAuditor1_USU]"))
                    gridView1.ActiveFilterString = "";
            }
            else
                gridView1.ActiveFilterString = string.Format("[CONAuditor1_USU] = {0}", (int)lookupGerente1.USUSel);
        }

        private void BotCorrecao_Click(object sender, EventArgs e)
        {
            string ComandoBuscaErros =
"SELECT        dbo.CHEques.CHE, dbo.PaGamentosFixos.PGF_CCTCred\r\n" +
"FROM            dbo.CHEques INNER JOIN\r\n" +
"                         dbo.PAGamentos ON dbo.CHEques.CHE = dbo.PAGamentos.PAG_CHE INNER JOIN\r\n" +
"                         dbo.NOtAs ON dbo.PAGamentos.PAG_NOA = dbo.NOtAs.NOA LEFT OUTER JOIN\r\n" +
"                         dbo.PaGamentosFixos ON dbo.NOtAs.NOA_PGF = dbo.PaGamentosFixos.PGF\r\n" +
"WHERE        (dbo.CHEques.CHETipo IN (20)) AND (dbo.CHEques.CHEStatus = 1) AND (dbo.CHEques.CHEAdicional IS NULL) AND (dbo.CHEques.CHE_CCG IS NULL) AND (dbo.NOtAs.NOA_FRN IS NULL)\r\n;";
            string ComandoCorrecao = "update Cheques set CHE_CCG = @P2 where CHE = @P1";
            DataTable TabelaErros = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(ComandoBuscaErros);
            foreach (DataRow DR in TabelaErros.Rows)
            {
                int CHE = (int)DR["CHE"];
                int CCT = (int)DR["PGF_CCTCred"];
                ABS_ContaCorrente conta = ABS_ContaCorrente.GetContaCorrenteNeon(CCT, TipoChaveContaCorrenteNeon.CCT,true);
                if (conta.CCG.HasValue)
                {
                    int CCG = conta.CCG.Value;
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoCorrecao,CHE,CCG);
                }
            }
        }
    }
}

