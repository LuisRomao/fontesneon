﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using CompontesBasicos;
using DevExpress.XtraReports.UI;

namespace dllCheques
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cSaldoCheques : ComponeteGradeNavegadorPaiCampos    
    {
        /// <summary>
        /// 
        /// </summary>
        public cSaldoCheques()
        {
            InitializeComponent();
            dcFolhasDeCheque.CarregarRelatorioCheques();
        }

        private void cSaldoCheques_CondominioAlterado(object sender, EventArgs e)
        {            
                dcFolhasDeCheque.CarregarRelatorioCheques();
        }

      

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            dcFolhasDeCheque.CarregarRelatorioCheques();
            if ((lookupGerente1.USUSel == -1) && (lookupAssistente1.USUSel == -1))
                bindingSourcedcFolhasDeCheque.Filter = "";
            else if ((lookupGerente1.USUSel != -1) && (lookupAssistente1.USUSel == -1))
                bindingSourcedcFolhasDeCheque.Filter = string.Format("CONAuditor1_USU = {0}", lookupGerente1.USUSel);
            else if ((lookupGerente1.USUSel == -1) && (lookupAssistente1.USUSel != -1))
                bindingSourcedcFolhasDeCheque.Filter = string.Format("CONAuditor2_USU = {0}", lookupAssistente1.USUSel);
            else
                bindingSourcedcFolhasDeCheque.Filter = string.Format("CONAuditor1_USU = {0} AND CONAuditor2_USU = {1}", lookupGerente1.USUSel, lookupAssistente1.USUSel);
            base.RefreshDados();
        }

        private void lookupGerente1_alterado(object sender, EventArgs e)
        {
            RefreshDados();
        }        

        private void BotNadaConsta_NotificaErro(object sender, Exception e)
        {
            MessageBox.Show(e.Message);
        }

        private class Carta
        {
            internal string Banco;
            internal string Agencia;
            internal string Lista;
            internal string Att;
            internal string email;
            internal int nBanco;

            public Carta(int nBanco, int CCTAgencia, int CCTAgenciaDg)
            {
                switch (nBanco)
                {
                    case 237: Banco = "Bradesco SA";
                        break;
                    case 104: Banco = "Caixa";
                        break;
                    case 341: Banco = "Itaú";
                        break;
                }                
                Agencia = string.Format("{0}-{1}", CCTAgencia, CCTAgenciaDg);
                this.nBanco = nBanco;
            }
        }

        private void BotNadaConsta_clicado(object sender, dllBotao.BotaoArgs args)
        {
            Dictionary<string,Carta> Cartas = new Dictionary<string,Carta>();
            //string Lista = "";
            //string Agencia = "";
            foreach (int i in GridView_F.GetSelectedRows())
            {
                dcFolhasDeCheque.ContasRow row = (dcFolhasDeCheque.ContasRow)GridView_F.GetDataRow(i);
                string Chave = string.Format("{0} {1}",row.CCT_BCO,row.CCTAgencia);
                Carta Car;
                if (!Cartas.ContainsKey(Chave))
                {
                    Car = new Carta(row.CCT_BCO,row.CCTAgencia,row.CCTAgenciaDg);
                    if (!row.IsAGEattNull())
                        Car.Att = row.AGEatt;
                    if (!row.IsAGEemailNull())
                        Car.email = row.AGEemail;
                    Cartas.Add(Chave, Car);
                }
                else
                    Car = Cartas[Chave];

                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(row.CON);
                string Numero = string.Format("{0}", row.CCTConta).PadLeft(6,'0');
                Car.Lista += string.Format("\t{0}\t{1}.{2}-{3}\r\n", rowCON.CONNome, Numero.Substring(0, 3), Numero.Substring(3), row.CCTContaDg);
                
            }
            if (Cartas.Count == 0)
                return;
            dllImpresso.ImpRTFLogoSimples ImpressoFinal = null;
            
            foreach (Carta Car in Cartas.Values)
            {
                dllImpresso.ImpRTFLogoSimples Impresso = new dllImpresso.ImpRTFLogoSimples("", -1, 77);
                SortedList SL = new System.Collections.SortedList();
                SL.Add("Banco", Car.Banco);
                SL.Add("Agencia", Car.Agencia);
                SL.Add("Cidade", Framework.DSCentral.EMP == 1 ? "São Bernardo" : "Santo André");
                SL.Add("Quantidade", "06");
                SL.Add("Lista", Car.Lista);
                SL.Add("Operador", Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome);
                if (Car.Att != null)
                    SL.Add("Att", Car.Att);
                Impresso.CarregarImpresso(Car.nBanco == 104 ? "Sol. talões CX" : "Sol. de talões", SL);
                Impresso.CreateDocument();
                if ((args.Botao == dllBotao.Botao.botao) && (Car.email != null))
                {
                    string Corpo = Impresso.QuadroRTF.Text;
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(Car.email, "Solicitação", Impresso, Corpo, "Solicitação de Talão de cheques");
                }
                else
                {
                    if (ImpressoFinal == null)
                        ImpressoFinal = Impresso;
                    else
                        ImpressoFinal.Pages.AddRange(Impresso.Pages);
                }
            }
            if (ImpressoFinal != null)
            {
                switch (args.Botao)
                {
                    case dllBotao.Botao.botao:
                    case dllBotao.Botao.imprimir:
                        ImpressoFinal.Print();
                        break;
                    case dllBotao.Botao.tela:
                        ImpressoFinal.ShowPreviewDialog();
                        break;
                }
            }

            MessageBox.Show("Terminado");
        }
    }
}
