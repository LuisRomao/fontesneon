/*
MR - 30/07/2014 16:00            - Tratamento do novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 06/08/2014 13:00            - Tratamento do status de inconsistencia no pagamento eletr�nico nos pontos necessarios (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***)
*/

using System;
using System.Drawing;
using System.Windows.Forms;
using VirEnumeracoesNeon;

namespace dllCheques
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cRetornoCheque : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cRetornoCheque()
        {
            InitializeComponent();
            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
        }

        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //bool encontrado = false;
                string Original = textEdit1.Text;
                if (Original.Length > 4)
                {
                    string Limpo;
                    int EMP;
                    int Tipo;
                    int CHE;
                    try
                    {
                        Limpo = (Original[0] == '0') ? Original.Substring(1, Original.Length - 2) : Original.Substring(0, Original.Length - 1);
                        EMP = int.Parse(Limpo.Substring(0, 1));
                        Tipo = int.Parse(Limpo.Substring(1, 2));
                        CHE = int.Parse(Limpo.Substring(3));
                        //UltimaAcao.Text = string.Format("EMP {0} - Tipo {1} - CHE {2}",EMP,Tipo,CHE);
                    }
                    catch
                    {
                        UltimaAcao.Text = "Leitura inv�lida";
                        textEdit1.Visible = false;
                        textEdit1.Text = "";
                        e.Handled = true;
                        MarcaErro();
                        return;
                    }
                    Cheque Cheque = null;
                    bool SegundaVia = false;
                    if (Tipo == 91)
                    {
                        SegundaVia = true;
                        //Cheque = new Cheque(CHE);
                        //Cheque.GravaObs("Usada segunda via da c�pia de cheque");
                        //UltimaAcao.Text += " Segunda via";
                        Tipo = 1;
                    };
                    if (Tipo == 1)
                    {
                        //if (Cheque == null)
                        Cheque = new Cheque(CHE, dllCheques.Cheque.TipoChave.CHE);
                        //MessageBox.Show(string.Format("Cheque encontrado status:{0}",Cheque.Status));
                        switch (Cheque.Status)
                        {
                            case CHEStatus.NaoEncontrado:
                                UltimaAcao.Text = "Cheque n�o encontrado!";
                                MarcaErro();
                                break;
                            case CHEStatus.Cancelado:
                            case CHEStatus.PagamentoCancelado:
                                UltimaAcao.Text = "Cheque cancelado!\r\nFa�a a corre��o antes do retorno";
                                MarcaErro();
                                break;
                            case CHEStatus.Cadastrado:
                                UltimaAcao.Text = "Cheque n�o emitido";
                                MarcaErro();
                                break;
                            case CHEStatus.AguardaRetorno:
                            case CHEStatus.Aguardacopia:
                            case CHEStatus.CompensadoAguardaCopia:                            
                            case CHEStatus.AguardaEnvioPE:
                            case CHEStatus.AguardaConfirmacaoBancoPE:
                            case CHEStatus.PagamentoConfirmadoBancoPE:
                            case CHEStatus.PagamentoNaoAprovadoSindicoPE:                            
                            case CHEStatus.PagamentoInconsistentePE:                            
                                if (Cheque.Retornar(TipoRetorno.SelectedIndex == 1, SegundaVia))                                
                                {                                    
                                    if (Cheque.Tipo == PAGTipo.PECreditoConta || Cheque.Tipo == PAGTipo.PEOrdemPagamento ||
                                        Cheque.Tipo == PAGTipo.PETituloBancario || Cheque.Tipo == PAGTipo.PETituloBancarioAgrupavel || Cheque.Tipo == PAGTipo.PETransferencia)                                    
                                        UltimaAcao.Text = string.Format("Condom�nio:{0} Pag. Eletr�nico:{1}", Cheque.rowCON.CONCodigo, Cheque.CHErow.CHENumero);
                                    else                                  
                                        UltimaAcao.Text = string.Format("Condom�nio:{0} Cheque:{1}", Cheque.rowCON.CONCodigo, Cheque.CHErow.CHENumero);
                                    MarcaOK();
                                }
                                else
                                {
                                    UltimaAcao.Text = string.Format("Erro na grava��o: {0}", Cheque.UltimaException.Message);
                                    MarcaErro();
                                }
                                break;
                            case CHEStatus.Compensado:
                            case CHEStatus.Retornado:
                            case CHEStatus.ComSindico:
                            case CHEStatus.EnviadoBanco:
                            case CHEStatus.Retirado:
                                Cheque.Editar(true);
                                Limpa();
                                break;
                            case CHEStatus.Caixinha:
                            case CHEStatus.DebitoAutomatico:
                            case CHEStatus.Eletronico:
                                UltimaAcao.Text = string.Format("Cheque encontrado mas o status n�o permite retorno:{0}", Cheque.Status);
                                MarcaErro();
                                break;
                            default:
                                Limpa();
                                break;
                        }

                    }
                    else
                        MessageBox.Show("Leitura inv�lida, tente novamente");
                }
                textEdit1.Text = "";
                e.Handled = true;
            }
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != '\b'))
                e.Handled = true;
        }

        private void Limpa()
        {
            textEdit1.Text = "";
            textEdit1.Visible = true;
            xtraTabControl1.Visible = false;
            UltimaAcao.Text = "";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Limpa();            
        }

        private void TipoRetorno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TipoRetorno.SelectedIndex != -1)
            {
                simpleButton1.Visible = true;
                TipoRetorno.BackColor = Color.Transparent;
                Limpa();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            Limpa();            
        }

        private void MarcaErro()
        {
            xtraTabControl1.Visible = true;
            xtraTabControl1.SelectedTabPage = xtraTabPage2;
            textEdit1.Visible = false;
        }

        private void MarcaOK()
        {
            xtraTabControl1.Visible = true;
            xtraTabControl1.SelectedTabPage = xtraTabPage1;
            textEdit1.Visible = false;
            timer1.Enabled = true;
        }

        
    }
}
