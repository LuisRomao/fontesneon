﻿/*
MR - 23/07/2014 10:00 -          - Inclusão de cheques com status aguardando retorno de pagamento eletrônico vencido nos "FilAguarda" para visualização na tela de emissão/retorno (Alterações indicadas por *** MRC - INICIO - PAG-FOR (23/07/2014 10:00) ***)
MR - 06/08/2014 13:00            - Tratamento do status de inconsistencia no pagamento eletrônico nos pontos necessarios (Alterações indicadas por *** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***)
MR - 26/12/2014 10:00            - Inclusão do campo CONAuditor2_USU em todas as querys Fill do ChequesparaEmitirTableAdapter
*/

using Framework.objetosNeon;
using System;
using Framework;
using FrameworkProc;
using VirEnumeracoesNeon;

namespace dllCheques
{


    partial class dChequesImprimir
    {
        #region Table Adapters

        private dChequesImprimirTableAdapters.ChequesSindicoTableAdapter chequesSindicoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ChequesSindico
        /// </summary>
        public dChequesImprimirTableAdapters.ChequesSindicoTableAdapter ChequesSindicoTableAdapter
        {
            get
            {
                if (chequesSindicoTableAdapter == null)
                {
                    chequesSindicoTableAdapter = new dChequesImprimirTableAdapters.ChequesSindicoTableAdapter();
                    chequesSindicoTableAdapter.TrocarStringDeConexao();
                };
                return chequesSindicoTableAdapter;
            }
        }

        private dChequesImprimirTableAdapters.BALancetesTableAdapter bALancetesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BALancetes
        /// </summary>
        public dChequesImprimirTableAdapters.BALancetesTableAdapter BALancetesTableAdapter
        {
            get
            {
                if (bALancetesTableAdapter == null)
                {
                    bALancetesTableAdapter = new dChequesImprimirTableAdapters.BALancetesTableAdapter();
                    bALancetesTableAdapter.TrocarStringDeConexao();
                };
                return bALancetesTableAdapter;
            }
        }

        /*
        private dChequesImprimirTableAdapters.PagamentosTableAdapter pagamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Pagamentos
        /// </summary>
        public dChequesImprimirTableAdapters.PagamentosTableAdapter PagamentosTableAdapter
        {
            get
            {
                if (pagamentosTableAdapter == null)
                {
                    pagamentosTableAdapter = new dChequesImprimirTableAdapters.PagamentosTableAdapter();
                    pagamentosTableAdapter.TrocarStringDeConexao();
                };
                return pagamentosTableAdapter;
            }
        }*/

        private dChequesImprimirTableAdapters.DetalhesCHETableAdapter detalhesCHETableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DetalhesCHE
        /// </summary>
        public dChequesImprimirTableAdapters.DetalhesCHETableAdapter DetalhesCHETableAdapter
        {
            get
            {
                if (detalhesCHETableAdapter == null)
                {
                    detalhesCHETableAdapter = new dChequesImprimirTableAdapters.DetalhesCHETableAdapter();
                    detalhesCHETableAdapter.TrocarStringDeConexao();
                };
                return detalhesCHETableAdapter;
            }
        }



        private dChequesImprimirTableAdapters.ChequesparaEmitirTableAdapter chequesparaEmitirTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ChequesparaEmitir
        /// </summary>
        public dChequesImprimirTableAdapters.ChequesparaEmitirTableAdapter ChequesparaEmitirTableAdapter
        {
            get
            {
                if (chequesparaEmitirTableAdapter == null)
                {
                    chequesparaEmitirTableAdapter = new dChequesImprimirTableAdapters.ChequesparaEmitirTableAdapter();
                    chequesparaEmitirTableAdapter.TrocarStringDeConexao();
                };
                return chequesparaEmitirTableAdapter;
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        internal enum Agrupamento
        {
            /// <summary>
            /// 
            /// </summary>
            Condominio,
            /// <summary>
            /// 
            /// </summary>
            malote_Envio,
            /// <summary>
            /// 
            /// </summary>
            malote_Retorno,
            /// <summary>
            /// Data prevista
            /// </summary>
            dataPrevista,
        }

        /// <summary>
        /// Acerta o título do grupo
        /// </summary>
        internal void AjustaTitulos(Agrupamento AG)
        {
            //int MenorGrupo = 0;

            foreach (ChequesparaEmitirRow CheErow in ChequesparaEmitir)
            {
                malote M1 = null;
                malote M2 = null;
                //Caso particular onde o cheque retorna no malote da manhã no dia do vencimento
                bool RetornaNoVencimento;
                if (CheErow.IsCONMaloteNull())
                    continue;
                if (CheErow.IsCONProcuracaoNull())
                    continue;
                DateTime[] datas = Cheque.CalculaDatas((PAGTipo)CheErow.CHETipo, CheErow.CONProcuracao, (malote.TiposMalote)CheErow.CONMalote, CheErow.CHEVencimento, DateTime.Now, ref M1, ref M2, out RetornaNoVencimento);
                if (M1 == null)
                {
                    if ((datas == null) || (datas.Length < 1))
                        throw new Exception(string.Format("Erro em dChequesImprimir.cs 150. {0} CHE = {1}", (datas == null) ? "datas == null" : "datas.Length < 1", CheErow.CHE));
                    CheErow.MaloteIda = datas[0].ToString("dd/MM HH:mm");
                }
                else
                    CheErow.MaloteIda = M1.ToString("ss");
                if (M2 == null)
                {
                    if ((datas == null) || (datas.Length < 2))
                        throw new Exception(string.Format("Erro em dChequesImprimir.cs 158. {0} CHE = {1}", (datas == null) ? "datas == null" : "datas.Length < 2", CheErow.CHE));
                    CheErow.MaloteVolta = datas[1].ToString("dd/MM HH:mm");
                }
                else
                    CheErow.MaloteVolta = M2.ToString("ss");
                if ((CHEStatus)CheErow.CHEStatus == CHEStatus.Cadastrado)
                {
                    CheErow.CHEIdaData = datas[0];
                    //CheErow.AcceptChanges();                    
                }
                //System.Collections.ArrayList ParaRetornar = new System.Collections.ArrayList(new CHEStatus[] { CHEStatus.Cadastrado, CHEStatus.Aguardacopia, CHEStatus.AguardaRetorno, CHEStatus.CompensadoAguardaCopia });
                //*** MRC - INICIO - PAG-FOR (2) *** 
                //*** MRC - INICIO - PAG-FOR (06/08/2014 13:00) ***
                System.Collections.ArrayList ParaRetornar = new System.Collections.ArrayList(new CHEStatus[] { CHEStatus.Cadastrado,
                                                                                                                           CHEStatus.Aguardacopia,
                                                                                                                           CHEStatus.AguardaRetorno,
                                                                                                                           CHEStatus.CompensadoAguardaCopia,
                                                                                                                           CHEStatus.AguardaEnvioPE,
                                                                                                                           CHEStatus.AguardaConfirmacaoBancoPE,
                                                                                                                           CHEStatus.PagamentoNaoAprovadoSindicoPE,
                                                                                                                           CHEStatus.PagamentoInconsistentePE });
                //*** MRC - TERMINO - PAG-FOR (06/08/2014 13:00) ***   
                //*** MRC - FIM - PAG-FOR (2) ***
                if (ParaRetornar.Contains((CHEStatus)CheErow.CHEStatus))
                    CheErow.CHERetornoData = datas[1];
                CheErow.RetornaNoVencimento = RetornaNoVencimento;
                int DiasMal = 0;
                switch (AG)
                {
                    case Agrupamento.Condominio:
                        CheErow.TituloGrupo = string.Format("{0} - {1} - {2}{3}",
                                          CheErow.CONCodigo,
                                          CheErow.CONNome,
                                          CheErow.IsCONMaloteNull() ? "*MALOTE NÃO CADASTRADO*" : Framework.Enumeracoes.VirEnumTiposMalote.Descritivo(CheErow.CONMalote),
                                          ((CheErow.IsCONProcuracaoNull()) || (!CheErow.CONProcuracao)) ? "" : " PROCURAÇÃO");
                        break;

                    case Agrupamento.malote_Envio:
                        if (CheErow.CHETipo == (int)PAGTipo.ChequeSindico)
                        {
                            DiasMal = ((TimeSpan)(CheErow.CHEVencimento - DateTime.Today)).Days;
                            CheErow.TituloGrupo = string.Format("Cheque síndico - Vencimento : {0:dd/MM/yy}", CheErow.CHEVencimento);
                        }
                        if (CheErow.CHETipo == (int)PAGTipo.Folha)
                        {
                            DiasMal = ((TimeSpan)(CheErow.CHEVencimento.SomaDiasUteis(5) - DateTime.Today)).Days;
                            CheErow.TituloGrupo = string.Format("Aguardando geração da folha prevista para {0:dd/MM/yy}", CheErow.CHEVencimento);
                        }
                        else if (CheErow.CHETipo == (int)PAGTipo.DebitoAutomatico)
                        {
                            DiasMal = ((TimeSpan)(CheErow.CHEVencimento.SomaDiasUteis(5) - DateTime.Today)).Days;
                            CheErow.TituloGrupo = string.Format("Aguardando débito automático prevista para {0:dd/MM/yy}", CheErow.CHEVencimento);
                        }
                        else if (CheErow.CHEStatus == (int)CHEStatus.CadastradoBloqueado)
                        {
                            if (M1 == null)
                            {
                                DiasMal = ((TimeSpan)(datas[0].Date.SomaDiasUteis(3, Sentido.Traz) - DateTime.Today)).Days - 3;
                                CheErow.TituloGrupo = string.Format("Aguardo - Assinar até : {0:dd/MM/yy}", datas[0]);
                            }
                            else
                            {
                                DiasMal = ((TimeSpan)(M1.DataMalote.SomaDiasUteis(3, Sentido.Traz) - DateTime.Today)).Days - 3;
                                CheErow.TituloGrupo = string.Format("Aguardo - Malote: {0:ss}", M1);
                            };
                        }
                        else
                        {
                            if (M1 == null)
                            {
                                DiasMal = ((TimeSpan)(datas[0].Date - DateTime.Today)).Days;
                                CheErow.TituloGrupo = string.Format("Assinar até : {0:dd/MM/yy}", datas[0]);
                            }
                            else
                            {
                                DiasMal = ((TimeSpan)(M1.DataMalote - DateTime.Today)).Days;
                                CheErow.TituloGrupo = string.Format("Malote: {0:ss}", M1);
                            };
                        }
                        //MenorGrupo = Math.Min(MenorGrupo, DiasMal);
                        CheErow.NumeroGrupo = DiasMal;
                        break;
                    case Agrupamento.malote_Retorno:
                        if (M2 == null)
                        {
                            DiasMal = ((TimeSpan)(datas[1].Date - DateTime.Today)).Days;
                            CheErow.TituloGrupo = string.Format("Retornar Assinado até : {0:dd/MM/yy}", datas[1]);
                        }
                        else
                        {
                            DiasMal = ((TimeSpan)(M2.DataMalote - DateTime.Today)).Days;
                            CheErow.TituloGrupo = string.Format("Malote: {0:ss}", M2);
                        };
                        //MenorGrupo = Math.Min(MenorGrupo, DiasMal);
                        CheErow.NumeroGrupo = DiasMal;
                        break;
                    case Agrupamento.dataPrevista:
                        int Dias = ((TimeSpan)(CheErow.CHEVencimento - DateTime.Today)).Days;
                        CheErow.TituloGrupo = string.Format("{0:dd/MM/yy} - {1} Dias = {2}",
                                          CheErow.CHEVencimento,
                                          Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.Nomes[CheErow.CHEStatus],
                                          Dias
                                          );
                        CheErow.NumeroGrupo = Dias;
                        break;
                    default:
                        break;
                }

            };
        }
    }
}

namespace dllCheques.dChequesImprimirTableAdapters {
    
    
    public partial class ChequesparaEmitirTableAdapter {
    }
}
