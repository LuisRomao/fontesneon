﻿namespace dllCheques {


    partial class dImpNotaDeb
    {
        private dllCheques.dImpNotaDebTableAdapters.EscritorioTableAdapter escritorioTableAdapter;
        
        /// <summary>
        /// TableAdapter para tabela Escritorio
        /// </summary>
        public dllCheques.dImpNotaDebTableAdapters.EscritorioTableAdapter EscritorioTableAdapter { 
            get{
                if (escritorioTableAdapter == null) {
                    escritorioTableAdapter = new dllCheques.dImpNotaDebTableAdapters.EscritorioTableAdapter();
                    escritorioTableAdapter.TrocarStringDeConexao();                    
                }
                return escritorioTableAdapter;
            }
        }

        private dllCheques.dImpNotaDebTableAdapters.SacadoTableAdapter sacadoTableAdapter;

        /// <summary>
        /// TableAdapter para tabela Sacado
        /// </summary>
        public dllCheques.dImpNotaDebTableAdapters.SacadoTableAdapter SacadoTableAdapter {
            get {
                if (sacadoTableAdapter == null){
                    sacadoTableAdapter = new dllCheques.dImpNotaDebTableAdapters.SacadoTableAdapter();
                    sacadoTableAdapter.TrocarStringDeConexao();
                }
                return sacadoTableAdapter;
            }
            
            
        }

    }
}
