﻿namespace dllCheques {
    
    
    public partial class dcFolhasDeCheque 
    {
        private dcFolhasDeChequeTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dcFolhasDeChequeTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dcFolhasDeChequeTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dcFolhasDeChequeTableAdapters.FolhasCHequeTableAdapter folhasCHequeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FolhasCHeque
        /// </summary>
        public dcFolhasDeChequeTableAdapters.FolhasCHequeTableAdapter FolhasCHequeTableAdapter
        {
            get
            {
                if (folhasCHequeTableAdapter == null)
                {
                    folhasCHequeTableAdapter = new dcFolhasDeChequeTableAdapters.FolhasCHequeTableAdapter();
                    folhasCHequeTableAdapter.TrocarStringDeConexao();
                };
                return folhasCHequeTableAdapter;
            }
        }

        private dcFolhasDeChequeTableAdapters.ContasTableAdapter contasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Contas
        /// </summary>
        public dcFolhasDeChequeTableAdapters.ContasTableAdapter ContasTableAdapter
        {
            get
            {
                if (contasTableAdapter == null)
                {
                    contasTableAdapter = new dcFolhasDeChequeTableAdapters.ContasTableAdapter();
                    contasTableAdapter.TrocarStringDeConexao();
                };
                return contasTableAdapter;
            }
        }

        private dcFolhasDeChequeTableAdapters.FolhasTableAdapter folhasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Folhas
        /// </summary>
        public dcFolhasDeChequeTableAdapters.FolhasTableAdapter FolhasTableAdapter
        {
            get
            {
                if (folhasTableAdapter == null)
                {
                    folhasTableAdapter = new dcFolhasDeChequeTableAdapters.FolhasTableAdapter();
                    folhasTableAdapter.TrocarStringDeConexao();
                };
                return folhasTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CarregarRelatorioCheques()
        {
            ContasTableAdapter.FillBy(Contas, Framework.DSCentral.EMP);
            foreach (ContasRow rowContax in Contas)
            {
                rowContax.FolhasBR = rowContax.FolhasSindico = rowContax.FolhasASS = rowContax.FolhasASSP = 0;
            }
            FolhasTableAdapter.Fill(Folhas);
            ContasRow rowConta;
            foreach (FolhasRow rowF in Folhas)
            {
                if (rowF.FCH_CCT == 1106)
                    System.Console.WriteLine(string.Format("CCT = {0} FCHStatus = {1}",rowF.FCH_CCT,rowF.FCHStatus));
                rowConta = Contas.FindByCCT(rowF.FCH_CCT);
                if (rowConta != null)
                {
                    switch ((FCHStatus)rowF.FCHStatus)
                    {
                        case FCHStatus.Talao:
                            rowConta.FolhasBR = rowF.Qtd;
                            break;
                        case FCHStatus.Sindico:
                            rowConta.FolhasSindico = rowF.Qtd;
                            break;
                        case FCHStatus.Assinada:
                            rowConta.FolhasASS = rowF.Qtd;
                            break;
                        case FCHStatus.AssinadaParcial:
                            rowConta.FolhasASSP = rowF.Qtd;
                            break;
                    }
                }
            }
        }
    }
}

namespace dllCheques.dcFolhasDeChequeTableAdapters {
    
    
    public partial class ContaCorrenTeTableAdapter {
    }
}
