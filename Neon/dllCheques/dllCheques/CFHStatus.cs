﻿using System;
using System.Drawing;

namespace dllCheques
{

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum FCHStatus
    {
        /// <summary>
        /// 
        /// </summary>
        Talao = 0,
        /// <summary>
        /// 
        /// </summary>
        Sindico = 1,
        /// <summary>
        /// 
        /// </summary>
        AssinadaParcial = 2,
        /// <summary>
        /// 
        /// </summary>
        Assinada = 3,
        /// <summary>
        /// 
        /// </summary>
        Cancelada = 4
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumFCHStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumFCHStatus(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumFCHStatus _virEnumFCHStatusSt;

        /// <summary>
        /// VirEnum estático para campo FCHStatus
        /// </summary>
        public static virEnumFCHStatus virEnumFCHStatusSt
        {
            get
            {
                if (_virEnumFCHStatusSt == null)
                {
                    _virEnumFCHStatusSt = new virEnumFCHStatus(typeof(FCHStatus));
                    _virEnumFCHStatusSt.GravaNomes(FCHStatus.Talao, "Folha em branco", Color.White);
                    _virEnumFCHStatusSt.GravaNomes(FCHStatus.Sindico, "Folha com o síndico", Color.Aqua);
                    _virEnumFCHStatusSt.GravaNomes(FCHStatus.Assinada, "Folha assinada", Color.Lime);
                    _virEnumFCHStatusSt.GravaNomes(FCHStatus.AssinadaParcial, "Assinda (parcial)", Color.Yellow);
                    _virEnumFCHStatusSt.GravaNomes(FCHStatus.Cancelada, "Cancelada", Color.Silver);
                }
                return _virEnumFCHStatusSt;
            }
        }
    }
}