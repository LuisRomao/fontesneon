/*
MR - 29/04/2014 15:25            - Novo construtor passando o tipo de pagamento a ser cancelado (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***)
MR - 30/07/2014 16:00            - Tratamento do novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 18/08/2015 10:00            - Cancelamento tributo eletronico PEGuia / Notas (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***)
*/

using CompontesBasicosProc;
using CompontesBasicos;
using VirEnumeracoesNeon;

namespace dllCheques
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fCancelaCheque : ComponenteBaseDialog
    {
        
        /// <summary>
        /// 
        /// </summary>
        public fCancelaCheque()
        {
            InitializeComponent();            
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        public fCancelaCheque(PAGTipo Tipo):this()
        {           
           if (Tipo.EstaNoGrupo(PAGTipo.PECreditoConta, PAGTipo.PEOrdemPagamento, PAGTipo.PETituloBancario, PAGTipo.PETituloBancarioAgrupavel, PAGTipo.PETransferencia))           
           {
               radioGroup1.Properties.Items[0].Description = "Cancelar pagamento eletr�nico: ser� emitido novamente";
               radioGroup1.Properties.Items[1].Description = "Cancelamento do pagamento. N�O ser� emitido novamente";
           }           
           else if (Tipo == PAGTipo.PEGuia)
           {
               radioGroup1.Properties.Items[0].Description = "Cancelar tributo eletr�nico: ser� emitido novamente";
               radioGroup1.Properties.Items[1].Description = "Cancelamento do tributo. N�O ser� emitido novamente";
           }
           else if (Tipo == PAGTipo.Folha)
           {
               radioGroup1.Properties.Items[0].Description = "Cancelar cr�dito em conta folha e substituir por cheque";
               radioGroup1.Properties.Items[1].Description = "Cancelar cr�dito em conta folha. N�O ser� emitido novamente";
           }
        }        
    }
}
