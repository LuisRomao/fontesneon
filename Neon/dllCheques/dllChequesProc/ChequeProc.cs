﻿using Abstratos;
using dllImpostoNeonProc;
using VirEnumeracoesNeon;
using Framework.objetosNeon;
using FrameworkProc;
using System;
using CompontesBasicosProc;
using DocBacarios;
using VirEnumeracoes;

namespace dllChequesProc
{
    /// <summary>
    /// 
    /// </summary>
    public class ChequeProc : ABS_Cheque, IEMPTProc
    {

        private dCheque _dCheque;

        //private ContaCorrenteNeon _ContaCredito;
        private ABS_ContaCorrente _ContaCredito;

        /// <summary>
        /// Conta para crédito do cheque
        /// </summary>
        private ABS_ContaCorrente ContaCredito
        {
            get
            {
                if ((_ContaCredito == null) && (CHErow != null) && (!CHErow.IsCHE_CCGNull()))
                {
                    //_ContaCredito = new ContaCorrenteNeon(CHErow.CHE_CCG, ContaCorrenteNeon.TipoChave.CCG);
                    _ContaCredito = ABS_ContaCorrente.GetContaCorrenteNeon(CHErow.CHE_CCG,TipoChaveContaCorrenteNeon.CCG,false);
                }
                return _ContaCredito;
            }
        }

        #region Conta de crédito
        /// <summary>
        /// Agência crédito do cheque
        /// </summary>
        public override int Agencia_Credito
        {
            get
            {
                if (ContaCredito != null)
                    return ContaCredito.Agencia;
                else
                    return base.Agencia_Credito;
            }
            set => base.Agencia_Credito = value;
        }

        /// <summary>
        /// Banco crédito do cheque
        /// </summary>
        public override int Banco_Credito
        {
            get
            {
                if (ContaCredito != null)
                    return ContaCredito.BCO;
                else
                    return base.Banco_Credito;
            }
            set => base.Banco_Credito = value;
        }

        /// <summary>
        /// Número da conta para crédito do cheque
        /// </summary>
        public override int Conta_Credito
        {
            get
            {
                if (ContaCredito != null)
                    return ContaCredito.NumeroConta;
                else
                    return base.Conta_Credito;
            }
            set => base.Conta_Credito = value;
        }

        /// <summary>
        /// Cnpj ou CPF para crédito
        /// </summary>
        public override CPFCNPJ CPFCNPJ_Credito
        {
            get
            {
                if (ContaCredito != null)
                    return ContaCredito.CPF_CNPJ;
                else
                    return base.CPFCNPJ_Credito;
            }
            set => base.CPFCNPJ_Credito = value;
        }

        /// <summary>
        /// Dígito da agência de crédito
        /// </summary>
        public override string DigAgencia_Credito
        {
            get
            {
                if (ContaCredito != null)
                    return ContaCredito.AgenciaDg.HasValue ? ContaCredito.AgenciaDg.ToString() : "";
                else
                    return base.DigAgencia_Credito;
            }
            set => base.DigAgencia_Credito = value;
        }

        /// <summary>
        /// Dígito da conta de crédito
        /// </summary>
        public override string DigConta_Credito
        {
            get
            {
                if (ContaCredito != null)
                    return ContaCredito.NumeroDg;
                else
                    return base.DigConta_Credito;
            }
            set => base.DigConta_Credito = value;
        }

        /// <summary>
        /// Não vem do cadastro da conta. Implementar se necessário
        /// </summary>
        public override string Endereco_Credito { get => base.Endereco_Credito; set => base.Endereco_Credito = value; }

        /// <summary>
        /// Nominal do cheque ou titular da conta de crédito
        /// </summary>
        public override string Nome
        {
            get
            {
                if (ContaCredito != null)
                    return ContaCredito.Titular;
                else
                    return base.Nome;
            }
            set => base.Nome = value;
        }

        /// <summary>
        /// Tipo de conta de crédito
        /// </summary>
        public override int TipoConta_Credito
        {
            get
            {
                if (ContaCredito != null)
                    return (int)(ContaCredito.Tipo == TipoConta.Poupanca ? EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaPoupanca : EDIBoletos.ChequeRemessaBra.TiposContaFornecedor.ContaCorrente);
                else
                    return base.TipoConta_Credito;
            }
            set => base.TipoConta_Credito = value;
        }
        #endregion


        private FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow _rowCON;
        /// <summary>
        /// 
        /// </summary>
        protected EMPTProc _EMPTProc1;

        /// <summary>
        /// Linha da tabela cheque
        /// </summary>
        public dCheque.CHEquesRow CHErow;

        /// <summary>
        /// Indica se o cheque foi encontrado no banco de dados
        /// </summary>
        public bool Encontrado;

        /// <summary>
        /// 
        /// </summary>
        public bool LiberaEdicao = false;
        /// <summary>
        /// 
        /// </summary>
        public malote MaloteIda;
        /// <summary>
        /// 
        /// </summary>
        public malote MaloteVolta;
        /// <summary>
        /// 
        /// </summary>
        public bool RetornaNaData = false;

        /// <summary>
        /// Último erro
        /// </summary>
        public Exception UltimaException;

        /// <summary>
        /// 
        /// </summary>
        public ChequeProc()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="TipoCh"></param>
        /// <param name="_EMPTProc1"></param>
        public ChequeProc(int Chave, TipoChave TipoCh, EMPTProc _EMPTProc1)
        {
            EMPTProc1 = _EMPTProc1;
            EMPTProc1.EmbarcaEmTrans(dCheque.CHEquesTableAdapter, dCheque.PAGamentosTableAdapter);
            switch (TipoCh)
            {
                case TipoChave.CCD:
                    if (dCheque.CHEquesTableAdapter.FillByCCD(dCheque.CHEques, Chave) == 1)
                    {
                        CHErow = dCheque.CHEques[0];
                        dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHErow.CHE);
                        Encontrado = true;
                    }
                    else
                        Encontrado = false;
                    return;
                case TipoChave.CHE:
                    if (dCheque.CHEquesTableAdapter.FillByCHE(dCheque.CHEques, Chave) == 1)
                    {
                        CHErow = dCheque.CHEques[0];
                        dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHErow.CHE);
                        Encontrado = true;
                    }
                    else
                        Encontrado = false;
                    return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Vencimento"></param>
        /// <param name="Favorecido"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        /// <param name="NovoCheque"></param>
        /// <param name="CCD"></param>
        /// <param name="CCT"></param>
        /// <param name="Adicional"></param>
        /// <param name="bloqueado"></param>
        /// <param name="EMPTProc"></param>
        /// <param name="CCG"></param>
        public ChequeProc(DateTime Vencimento, string Favorecido, int CON, PAGTipo Tipo, bool NovoCheque, int? CCD = null, int? CCT = null, string Adicional = "", bool bloqueado = false, int? CCG = null, EMPTProc EMPTProc = null)
        {
            if (bloqueado && CCG.HasValue)
                throw new NotImplementedException("Cheque bloqueado com conta de crédito não implementado em ChequeProc.cs");
            EMPTProc1 = EMPTProc;
            if (bloqueado)
                if (CCD.HasValue)
                    throw new ArgumentException("cheque bloqueado com CCD");
            if (!CCG.HasValue)
            {
                if (Tipo == PAGTipo.PECreditoConta && Adicional.Contains("- CCT = "))
                    Adicional = Adicional.Substring(Adicional.IndexOf("CCT = "));
            }


            try
            {
                if (CCD.GetValueOrDefault(0) != 0)
                    NovoCheque = true;
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 444", dCheque.CHEquesTableAdapter);
                if (!NovoCheque)
                {
                    if (bloqueado)
                    {
                        if (Adicional == string.Empty)
                            dCheque.CHEquesTableAdapter.FillParaIncluirBloc(dCheque.CHEques, Vencimento, Favorecido, CON, (int)Tipo);
                        else
                            dCheque.CHEquesTableAdapter.FillParaIncAdBloc(dCheque.CHEques, Vencimento, Favorecido, CON, (int)Tipo, Adicional);
                    }
                    else
                    {
                        if (CCG.HasValue && (Tipo == PAGTipo.PECreditoConta))
                        {
                            dCheque.CHEquesTableAdapter.FillByParaIncluirCCG(dCheque.CHEques, Vencimento, CON, (int)Tipo, CCG);
                        }
                        else
                        {
                            if (Adicional == string.Empty)
                                dCheque.CHEquesTableAdapter.FillParaIncluir(dCheque.CHEques, Vencimento, Favorecido, CON, (int)Tipo);
                            else
                                dCheque.CHEquesTableAdapter.FillParaIncAd(dCheque.CHEques, Vencimento, Favorecido, CON, (int)Tipo, Adicional);
                        }
                    }
                    foreach (dCheque.CHEquesRow rowCHE in dCheque.CHEques)
                    {

                        if (CCT.GetValueOrDefault(0) == 0 || (rowCHE.IsCHE_CCTNull()) || rowCHE.CHE_CCT == CCT.Value)
                        {
                            CHErow = rowCHE;
                            break;
                        }
                    }

                }
                if (CHErow == null)
                {
                    CHErow = dCheque.CHEques.NewCHEquesRow();
                    CHErow.CHE_CON = CON;
                    CHErow.CHETipo = (int)Tipo;

                    switch (Tipo)
                    {
                        case PAGTipo.DebitoAutomatico:
                            if (CCD.GetValueOrDefault(0) == 0)
                                if (bloqueado)
                                    CHErow.CHEStatus = (int)CHEStatus.CadastradoBloqueado;
                                else
                                {
                                    CHErow.CHEStatus = (int)CHEStatus.DebitoAutomatico;
                                    CHErow.CHEEmissao = DateTime.Today;
                                }
                            else
                            {
                                CHErow.CHEStatus = (int)CHEStatus.Compensado;
                                CHErow.CHE_CCD = CCD.Value;
                                CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - débito automático\r\n    Data:{2:dd/MM/yyyy}",
                                                         (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                                         DateTime.Now,
                                                         Vencimento
                                                         );
                            }
                            break;
                        case PAGTipo.Caixinha:
                            CHErow.CHEStatus = (int)CHEStatus.Caixinha;
                            break;
                        case PAGTipo.GuiaEletronica:
                            CHErow.CHEStatus = (int)CHEStatus.Eletronico;
                            break;
                        case PAGTipo.cheque:
                        case PAGTipo.sindico:
                        case PAGTipo.boleto:
                        case PAGTipo.deposito:
                        case PAGTipo.Guia:
                        case PAGTipo.Honorarios:
                        case PAGTipo.Conta:
                        case PAGTipo.Acumular:
                        case PAGTipo.PEOrdemPagamento:
                        case PAGTipo.PETituloBancario:
                        case PAGTipo.PETituloBancarioAgrupavel:
                        case PAGTipo.PEGuia:
                        case PAGTipo.TrafArrecadado:
                        case PAGTipo.ChequeSindico:
                        case PAGTipo.Folha:
                            if (bloqueado)
                                CHErow.CHEStatus = (int)CHEStatus.CadastradoBloqueado;
                            else
                                CHErow.CHEStatus = (int)CHEStatus.Cadastrado;
                            break;
                        case PAGTipo.PECreditoConta:
                        case PAGTipo.PETransferencia:
                            if (bloqueado)
                                CHErow.CHEStatus = (int)CHEStatus.CadastradoBloqueado;
                            else
                                CHErow.CHEStatus = (int)CHEStatus.Cadastrado;
                            if (CCG.HasValue)
                                CHErow.CHE_CCG = CCG.Value;
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Construtor Cheque: Tipo não implementado = {0}", Tipo));
                    }

                    //*** MRC - INICIO - PAG-FOR (2) ***
                    if (CCT.GetValueOrDefault(0) != 0)
                        CHErow.CHE_CCT = CCT.Value;
                    if (Adicional != string.Empty)
                        CHErow.CHEAdicional = Adicional;
                    //*** MRC - FIM - PAG-FOR (2) ***
                    CHErow.CHEDATAI = DateTime.Now;
                    CHErow.CHEFavorecido = Favorecido;
                    CHErow.CHEI_USU = EMPTProc1.USU;
                    CHErow.CHEValor = 0;
                    CHErow.CHEVencimento = Vencimento;
                    if (rowCON.IsCONMaloteNull())
                        //VirException.cVirException VErro = new VirException.cVirException(string.Format("Erro no cadastro do condomínio {0} - {1}\r\nMalote não definido", rowCON.CONCodigo, rowCON.CONNome));
                        //VErro.Abortar = false;
                        //VErro.AvisarUsuario = true;
                        //VErro.Vircath = false;
                        throw new Exception(string.Format("Erro no cadastro do condomínio {0} - {1}\r\nMalote não definido", rowCON.CONCodigo, rowCON.CONNome));
                    ;
                    if (rowCON.IsCONProcuracaoNull())
                        //VirException.cVirException VErro = new VirException.cVirException(string.Format("Erro no cadastro do condomínio {0} - {1}\r\nUso de procuração não definido", rowCON.CONCodigo, rowCON.CONNome));
                        //VErro.Abortar = false;
                        //VErro.AvisarUsuario = true;
                        //VErro.Vircath = false;
                        throw new Exception(string.Format("Erro no cadastro do condomínio {0} - {1}\r\nUso de procuração não definido", rowCON.CONCodigo, rowCON.CONNome));
                    ;
                    DateTime[] Datas = CalculaDatas(Tipo, rowCON.CONProcuracao, (malote.TiposMalote)rowCON.CONMalote, Vencimento, DateTime.Now, ref MaloteIda, ref MaloteVolta, out RetornaNaData);
                    CHErow.CHEIdaData = Datas[0];
                    CHErow.CHERetornoData = Datas[1];
                    dCheque.CHEques.AddCHEquesRow(CHErow);

                    dCheque.CHEquesTableAdapter.Update(CHErow);

                    CHErow.AcceptChanges();
                };
                Encontrado = true;
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro gravando cheque: " + e.Message, e);
            }
        }

        private dCheque.PAGamentosRow[] _rowPAGs;

        /// <summary>
        /// Pagamentos do cheque
        /// </summary>
        public dCheque.PAGamentosRow[] rowPAGs
        {
            get => _rowPAGs ?? (_rowPAGs = CHErow.GetPAGamentosRows());
            set => _rowPAGs = value;
        }

        /// <summary>
        /// Estorna um cheque já compensado - geralmente eletrônico
        /// </summary>
        /// <returns></returns>
        public bool Estorna()
        {
            if ((!Status.EstaNoGrupo(CHEStatus.Compensado, CHEStatus.CompensadoAguardaCopia)) || CHErow.IsCHE_CCDNull())
                return false;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("ChequeProc.cs 436", dCheque.ContaCorrenteDetalheTableAdapter,dCheque.CHEquesTableAdapter);
                dCheque.ContaCorrenteDetalheTableAdapter.Fill(dCheque.ContaCorrenteDetalhe, CHErow.CHE_CCD);
                dCheque.ContaCorrenteDetalheRow CCDrow = dCheque.ContaCorrenteDetalhe[0];
                if (-CCDrow.CCDValor > Valor)
                {
                    dCheque.ContaCorrenteDetalheRow CCDrowSeparada = dCheque.ContaCorrenteDetalhe.NewContaCorrenteDetalheRow();
                    foreach (System.Data.DataColumn DC in dCheque.ContaCorrenteDetalhe.Columns)
                    {
                        if (!DC.ColumnName.EstaNoGrupo("CCD", "CCDValor"))
                            CCDrowSeparada[DC] = CCDrow[DC];
                    }
                    CCDrow.CCDValor += Valor;
                    CCDrowSeparada.CCDValor = -Valor;
                    CCDrowSeparada.CCDTipoLancamento = (int)TipoLancamentoNeon.Estorno;
                    dCheque.ContaCorrenteDetalhe.AddContaCorrenteDetalheRow(CCDrowSeparada);
                    dCheque.ContaCorrenteDetalheTableAdapter.Update(CCDrowSeparada);
                    CHErow.CHE_CCD = CCDrowSeparada.CCD;
                }
                else                
                    CCDrow.CCDTipoLancamento = (int)TipoLancamentoNeon.Estorno;                                    
                dCheque.ContaCorrenteDetalheTableAdapter.Update(CCDrow);
                CHErow.CHEStatus = (int)CHEStatus.PagamentoNaoEfetivadoPE;
                dCheque.CHEquesTableAdapter.Update(CHErow);
                VirMSSQL.TableAdapter.CommitSQL();
                return true;
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
                return false;
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        protected void verificaPGF()
        {
            if (dCheque.PAGamentos.Count == 0)
            {
                EMPTProc1.EmbarcaEmTrans(dCheque.PAGamentosTableAdapter);
                dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHErow.CHE);
            }
            foreach (dCheque.PAGamentosRow rowDet in CHErow.GetPAGamentosRows())
                if (!rowDet.IsNOA_PGFNull())
                {
                    AbstratosNeon.ABS_PagamentoPeriodico PagamentoPeriodico = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodicoProc(rowDet.NOA_PGF, EMPTProc1);
                    PagamentoPeriodico.CadastrarProximo();
                }
        }

        /// <summary>
        /// Ajusta o cabeçalho do cheque após a alteração de alguma parcela; Se nenhuma parela for encontrada o cheque é apagado;
        /// </summary>
        public void AjustarTotalPelasParcelas()
        {
            if (!Encontrado)
                throw new Exception("Cheque não encontrado");
            if (!LiberaEdicao)
                if (!Editavel())
                    throw new Exception("Tentativa de edição de cheque emitido");

            try
            {
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 990", dCheque.CHEquesTableAdapter);
                object oTotal = dCheque.CHEquesTableAdapter.TotalDasParcelas(CHErow.CHE);
                decimal Total = oTotal == null ? 0 : (decimal)oTotal;
                if ((Total == 0) && (CHEStatus != CHEStatus.Cancelado))
                {                    
                    dCheque.CHEquesTableAdapter.DeleteZerados(CHErow.CHE);
                    CHErow.Delete();
                    dCheque.CHEquesTableAdapter.Update(CHErow);
                    Encontrado = false;
                }
                else
                {
                    if (((CHEStatus)CHErow.CHEStatus == CHEStatus.DebitoAutomatico) && (CHErow.CHETipo == (int)PAGTipo.cheque))
                        CHErow.CHEStatus = (int)CHEStatus.Cadastrado;
                    CHErow.CHEValor = Total;
                    dCheque.CHEquesTableAdapter.Update(CHErow);
                    CHErow.AcceptChanges();
                };
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro gravando cheque: " + e.Message, e);
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CCD"></param>
        /// <returns></returns>
        public bool AmarrarCCD(int CCD)
        {
            if (!CHErow.IsCHE_CCDNull())
                return false;
            try
            {
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 959", dCheque.CHEquesTableAdapter);
                CHErow.CHE_CCD = CCD;
                CHErow.CHEStatus = (int)CHEStatus.Compensado;
                dCheque.CHEquesTableAdapter.Update(CHErow);
                CHErow.AcceptChanges();
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                EMPTProc1.Vircatch(e);
            }
            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="PAGTIPO"></param>
        /// <param name="Procuracao"></param>
        /// <param name="TipoMalote"></param>
        /// <param name="Vencimento"></param>
        /// <param name="HoraCadastro"></param>
        /// <param name="MaloteIda"></param>
        /// <param name="MaloteVolta"></param>
        /// <param name="RetornaNaData"></param>
        /// <returns></returns>
        public static DateTime[] CalculaDatas(PAGTipo PAGTIPO, bool Procuracao, malote.TiposMalote TipoMalote, DateTime Vencimento, DateTime HoraCadastro, ref malote MaloteIda, ref malote MaloteVolta, out bool RetornaNaData)
        {
            RetornaNaData = false;
            try
            {
                DateTime DEmitir = DateTime.MinValue;
                DateTime DRetorno = DateTime.MinValue;
                switch (PAGTIPO)
                {
                    case PAGTipo.Caixinha:
                    case PAGTipo.DebitoAutomatico:
                    case PAGTipo.GuiaEletronica:
                    case PAGTipo.ChequeSindico:
                    case PAGTipo.Folha:
                        DEmitir = DRetorno = Vencimento;
                        MaloteIda = MaloteVolta = null;
                        break;
                    case PAGTipo.sindico:
                        MaloteIda = new malote(TipoMalote, Vencimento);
                        while (MaloteIda.DataMalote >= Vencimento)
                            MaloteIda--;
                        MaloteVolta = MaloteIda;
                        MaloteVolta++;
                        DEmitir = MaloteIda.DataEnvio;
                        DRetorno = MaloteVolta.DataRetorno;
                        break;
                    case PAGTipo.cheque:
                    case PAGTipo.boleto:
                    case PAGTipo.deposito:
                    case PAGTipo.TrafArrecadado:
                    case PAGTipo.Guia:
                    case PAGTipo.Honorarios:
                    case PAGTipo.Conta:
                    case PAGTipo.PECreditoConta:
                    case PAGTipo.PETransferencia:
                    case PAGTipo.PEOrdemPagamento:
                    case PAGTipo.PETituloBancario:
                    case PAGTipo.PETituloBancarioAgrupavel:
                    //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    case PAGTipo.PEGuia:
                        //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***                            
                        if (Vencimento.DayOfWeek == DayOfWeek.Saturday)
                            Vencimento = Vencimento.AddDays(2);
                        else
                            if (Vencimento.DayOfWeek == DayOfWeek.Sunday)
                            Vencimento = Vencimento.AddDays(1);
                        if (Procuracao)
                        {
                            DEmitir = Vencimento.AddHours(12);
                            DRetorno = Vencimento.AddHours(13);
                            MaloteIda = MaloteVolta = null;
                        }
                        else
                        {
                            MaloteVolta = malote.MalotePara(TipoMalote, malote.SentidoMalote.volta, Vencimento.AddHours(10), 0);
                            MaloteIda = MaloteVolta;
                            MaloteIda--;
                            if (MaloteIda.DataEnvio < HoraCadastro)
                            {
                                MaloteVolta = malote.MalotePara(TipoMalote, malote.SentidoMalote.volta, Vencimento.AddHours(13), 0);
                                MaloteIda = MaloteVolta;
                                MaloteIda--;
                                RetornaNaData = (Vencimento.Date == MaloteVolta.DataMalote);
                            };
                            DEmitir = MaloteIda.DataEnvio;
                            DRetorno = MaloteVolta.DataRetorno;
                        }
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Tipo não tratado '{0}' em cheque.cs - 1211", PAGTIPO));
                }
                DateTime[] Retorno = new DateTime[] { DEmitir, DRetorno };
                return Retorno;
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Obs"></param>
        public void CancelarPagamento(string Obs) 
        {
            //string LOG = string.Format("Tipo = {0}",EMPTProc1.Tipo);
            //System.IO.File.AppendAllText("D:\\lixo\\log.log", LOG);
            Status = CHEStatus.PagamentoCancelado;
            CHErow.CHEObs = String.Format("{0}{1:dd/MM/yy HH:mm:ss} - Cancelado o pagamento por {2}\r\nJustificativa: {3}",
                                  (CHErow.IsCHEObsNull() ? string.Empty : CHErow.CHEObs + "\r\n\r\n"),
                                  DateTime.Now,
                                  EMPTProc1.USUNome,
                                  Obs
                                  );

            //*** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***
            //Seta status de cancelamento em caso de pagamento eletronico para envio do cancelamento via processo automatico
            PAGTipo Tipo = (PAGTipo)CHErow.CHETipo;
            //*** MRC - INICIO - PAGAMENTO ELETRONICO (08/07/2015 09:00) *** 
            if ((!CHErow.IsCHENumeroNull()) &&
                (Tipo == PAGTipo.PECreditoConta || Tipo == PAGTipo.PEOrdemPagamento ||
                Tipo == PAGTipo.PETituloBancario || Tipo == PAGTipo.PETituloBancarioAgrupavel || Tipo == PAGTipo.PETransferencia))
                //*** MRC - TERMINO - PAGAMENTO ELETRONICO (08/07/2015 09:00) *** 
                CHErow.CHEPEStatusCancelamento = (int)PECancelamentoStatus.AguardaEnvioCancelamentoPE;
            //*** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***

            try
            {
                EMPTProc1.AbreTrasacaoSQL("dllCheques Cheque - 1737", dCheque.CHEquesTableAdapter);
                dCheque.CHEquesTableAdapter.Update(CHErow);
                CHErow.AcceptChanges();
                //System.Data.DataTable DT = VirMSSQL.TableAdapter.ST().BuscaSQLTabela("select * from PAGamentos where PAG_CHE = @P1", CHErow.CHE);
                System.Data.DataTable DT = EMPTProc1.STTA.BuscaSQLTabela("select * from PAGamentos where PAG_CHE = @P1", CHErow.CHE);
                foreach (System.Data.DataRow DR in DT.Rows)
                    //Seta envio de exclusao de tributo para o banco se agendado
                    if (Tipo == PAGTipo.PEGuia && DR["PAG_GPS"] != DBNull.Value)
                    {
                        if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                            throw new Exception("Exclusão de tributo não implementarda para servidor de processoa ChequeProc.cs - 651");
                        dGPS dGPS1 = new dGPS();
                        EMPTProc1.EmbarcaEmTrans(dGPS1.GPSTableAdapter);
                        dGPS1.GPSTableAdapter.FillByGPS(dGPS1.GPS, (int)DR["PAG_GPS"]);
                        if (dGPS1.GPS.Count > 0)
                        {
                            dGPS.GPSRow rowGPS = dGPS1.GPS[0];
                            if (Colecoes.StatusArquivoCancelaveisBanco.Contains((StatusArquivo)rowGPS.GPSStatus))
                            {
                                rowGPS.GPSComentario = (rowGPS.IsGPSComentarioNull() ? string.Empty : rowGPS.GPSComentario) + String.Format("{0:dd/MM/yy HH:mm:ss} - Exclusão no banco programada por {2} - Motivo: {1}\r\n", DateTime.Now, Obs, EMPTProc1.USUNome);
                                rowGPS.GPSStatus = (int)StatusArquivo.Aguardando_Envio_Exclusao;
                                dGPS1.GPSTableAdapter.Update(rowGPS);
                            }
                        }
                    }
                //*** MRC - TERMINO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                //verificaPGF();
                EMPTProc1.Commit();
            }
            catch (Exception e)
            {
                UltimaException = e;
                EMPTProc1.Vircatch(e);
                throw new Exception("Erro ao cancelar pagamento: " + e.Message, e);
            };
        }

        /// <summary>
        /// Retorna se o cheque é editável
        /// </summary>
        /// <returns></returns>
        public bool Editavel()
        {
            return Editavel((CHEStatus)CHErow.CHEStatus);
        }

        /// <summary>
        /// Retorna se o Status é editável
        /// </summary>
        /// <param name="Status"></param>
        /// <returns></returns>
        public static bool Editavel(CHEStatus Status)
        {
            switch (Status)
            {
                case CHEStatus.NaoEncontrado:
                case CHEStatus.Cancelado:
                case CHEStatus.AguardaRetorno:
                case CHEStatus.Aguardacopia:
                case CHEStatus.Retornado:
                case CHEStatus.EnviadoBanco:
                case CHEStatus.Retirado:
                case CHEStatus.Compensado:
                case CHEStatus.CompensadoAguardaCopia:
                case CHEStatus.PagamentoCancelado:
                case CHEStatus.BaixaManual:
                case CHEStatus.AguardaEnvioPE:
                case CHEStatus.AguardaConfirmacaoBancoPE:
                case CHEStatus.PagamentoConfirmadoBancoPE:
                case CHEStatus.PagamentoNaoAprovadoSindicoPE:
                case CHEStatus.PagamentoInconsistentePE:
                case CHEStatus.ComSindico:
                case CHEStatus.PagamentoNaoEfetivadoPE:
                case CHEStatus.Antigo:
                    return false;
                case CHEStatus.DebitoAutomatico:
                case CHEStatus.Cadastrado:
                case CHEStatus.Caixinha:
                case CHEStatus.Eletronico:
                case CHEStatus.CadastradoBloqueado:
                    return true;
                default:
                    throw new NotImplementedException(string.Format("Tratamento para status = {0} não tratado\r\nCheque.cs", Status));
            }
        }

        /// <summary>
        /// Status
        /// </summary>
        public CHEStatus CHEStatus => (CHEStatus)CHErow.CHEStatus;         

        /// <summary>
        /// Status Cancelamento
        /// </summary>
        public PECancelamentoStatus PECancelamentoStatus => (CHErow.IsCHEPEStatusCancelamentoNull() ? PECancelamentoStatus.AguardaEnvioCancelamentoPE : (PECancelamentoStatus)CHErow.CHEPEStatusCancelamento);

        /// <summary>
        /// ds interno
        /// </summary>
        public dCheque dCheque
        {
            get
            {
                if (_dCheque == null)
                    _dCheque = new dCheque(EMPTProc1);
                return _dCheque;
            }
        }

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Condominio
        /// </summary>
        public FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON
        {
            get
            {
                if ((_rowCON == null) && (CHErow != null))
                    _rowCON = EMPTProc1.rowCON(CHErow.CHE_CON);
                //_rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosSTX(EMPTProc1.Tipo).CONDOMINIOS.FindByCON(CHErow.CHE_CON);
                return _rowCON;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public CHEStatus Status
        {
            get
            {
                if (!Encontrado || (CHErow == null))
                    return CHEStatus.NaoEncontrado;
                return (CHEStatus)CHErow.CHEStatus;
            }
            set
            {
                if (!Encontrado || (CHErow == null))
                    return;
                CHErow.CHEStatus = (int)value;
            }

        }

        /// <summary>
        /// Valor do cheque
        /// </summary>
        public override decimal Valor
        {
            get
            {
                if (CHErow != null)
                    return CHErow.CHEValor;
                else
                    return 0;
            }

            set
            {
                if (CHErow != null)
                    CHErow.CHEValor = value;
            }
        }

        /// <summary>
        /// Tipo de chave
        /// </summary>
        public enum TipoChave
        {
            /// <summary>
            /// CCD
            /// </summary>
            CCD,
            /// <summary>
            /// CHE
            /// </summary>
            CHE
        }

        

        /*
         //Verifica diretorio dos comprovantes
            string DestinoPDF = dEstacao.dEstacaoSt.GetValor("Destino PDF");
            if (DestinoPDF == "")
            {
                DestinoPDF = @"\\email\volume_1\publicacoes\";
                dEstacao.dEstacaoSt.SetValor("Destino PDF", DestinoPDF);
            }
        */

        /// <summary>
        /// 
        /// </summary>
        public PAGTipo Tipo
        {
            get
            {
                if (!Encontrado || (CHErow == null))
                    return PAGTipo.cheque;
                return (PAGTipo)CHErow.CHETipo;
            }
            set
            {
                if (!Encontrado || (CHErow == null))
                    return;
                CHErow.CHETipo = (int)value;
            }
        }

        //public string UltimoErro;

        /// <summary>
        /// Agrupo os pagamentos do cheque doador
        /// </summary>
        /// <param name="ChequeDoador"></param>
        /// <returns></returns>
        public bool AgruparRecebe(ChequeProc ChequeDoador)
        {
            if (  
                (DataVencimento != ChequeDoador.DataVencimento)
                  ||
                (CHErow.CHE_CON != ChequeDoador.CHErow.CHE_CON)
                  ||
                (CHErow.CHEFavorecido.Trim() != ChequeDoador.CHErow.CHEFavorecido.Trim())
                  ||
                (Tipo != ChequeDoador.Tipo)
               )                
                return false;
            try
            {
                EMPTProc1.AbreTrasacaoSQL("AgruparRecebe ChequeProc.cs 907", dCheque.CHEquesTableAdapter, dCheque.PAGamentosTableAdapter);
                ChequeDoador.AgrupaMata(this);
                AjustarTotalPelasParcelas();
                EMPTProc1.Commit();
                return true;
            }
            catch (Exception ex)
            {
                EMPTProc1.Vircatch(ex);
                return false;
            }
            
        }

        /// <summary>
        /// Transfere os itens do cheque para o receptor e mata o cheque
        /// </summary>
        /// <param name="ChequeReceptor"></param>
        /// <returns></returns>
        internal bool AgrupaMata(ChequeProc ChequeReceptor)
        {
            try
            {
                EMPTProc1.AbreTrasacaoSQL("AgruparMata ChequeProc.cs 924", dCheque.CHEquesTableAdapter,dCheque.PAGamentosTableAdapter);
                int lidos = dCheque.PAGamentosTableAdapter.FillByCHE(dCheque.PAGamentos, CHE);
                foreach (dCheque.PAGamentosRow rowPAG in CHErow.GetPAGamentosRows())
                {
                    if (!rowPAG.PAGPermiteAgrupar)
                        throw new Exception("Cheque não permite agrupamento");
                    rowPAG.PAG_CHE = ChequeReceptor.CHE.Value;
                }
                dCheque.PAGamentosTableAdapter.Update(dCheque.PAGamentos);
                CHErow.Delete();
                dCheque.CHEquesTableAdapter.Update(CHErow);
                EMPTProc1.Commit();
            }
            catch (Exception ex)
            {
                EMPTProc1.Vircatch(ex);
            }
            return false;
        }

        /// <summary>
        /// CHAVE
        /// </summary>
        public int? CHE
        {
            get
            {
                if (CHErow != null)
                    return CHErow.CHE;
                else
                    return null;
            }
        }
    }
}
