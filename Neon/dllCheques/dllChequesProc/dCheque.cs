﻿/*
MR - 28/04/2014 09:45 - 13.2.8.34 - Criacao de nova enumeracao para Tipos de Pagamento (Todos os úteis - para exibição nos combos) (Alterações indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 30/07/2014 16:00             - Inclusão de novo tipo de pagamento eletrônico para titulo agrupável - varias notas um único boleto (Alterações indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 05/08/2014 11:00             - Inclusão da funcao (VerificaCONCnpjPE) que verifica o CNPJ a ser enviado no pagamento eletrônico de acordo com o tipo de conta do condominio no DadosComplementaresTableAdapter
LH - 25/12/2014 14:20 - 14.1.4.96 - Pagamento com boleto integrado na mesma tela de emissão de cheque
MR - 10/02/2015 12:00             - Inclusão do campo com tipo de envio de arquivos para envio de remessa de pagamento eletronico (CONRemessa_TEA) no DadosComplementaresTableAdapter
MR - 27/04/2015 12:00             - Pagamento de tributo eletronico com inclusao do "guia de recolhimento - eletrônico" (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
                                  - Inclusão dos campos com de gps associada e status do pagamento para controle de pagamento de tributo eletronico e quebra de cheque se necessario (PAG_GPS e PAGStatus) no PAGamentosTableAdapter
                                  - Inclusão de query SetaGPS para setar campos de controle de emissão de "guia de recolhimento - eletrônico", no PAGamentosTableAdapter
MR - 20/05/2015 17:00             - Inclusão do ContaCorrenTeDepositoTableAdapter para busca de conta para depósito no caso de PECreditoConta (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***)
MR - 05/01/2016 13:30             - Inclusão do campo FRNContaTipoCredito nos Fills do FORNECEDORESTableAdapter
 */


using VirEnumeracoesNeon;
using System.Collections.Generic;
using FrameworkProc;

namespace dllChequesProc
{

    partial class dCheque : IEMPTProc
    {


        static private List<PAGTipo> _PAGTiposEletronicos;

        /// <summary>
        /// Tipos de pagamento eletrônicos
        /// </summary>
        static public List<PAGTipo> PAGTiposEletronicos
        {
            get
            {
                if (_PAGTiposEletronicos == null)
                {
                    _PAGTiposEletronicos = new List<PAGTipo>(new PAGTipo[] { PAGTipo.PECreditoConta,
                                                                             PAGTipo.PEOrdemPagamento,
                                                                             PAGTipo.PETituloBancario,
                                                                             PAGTipo.PETituloBancarioAgrupavel,
                                                                             PAGTipo.PETransferencia,
                                                                             //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                                                                             PAGTipo.PEGuia });
                    //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                }
                return _PAGTiposEletronicos;
            }
        }

        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dCheque(EMPTProc _EMPTProc1)
            : this()
        {
            if (_EMPTProc1 == null)
                _EMPTProc1 = new EMPTProc(EMPTipo.Local);
            EMPTProc1 = _EMPTProc1;
        }


        private static dCheque _dChequeSt;

        /// <summary>
        /// dataset estático:dCheque
        /// </summary>
        public static dCheque dChequeSt
        {
            get
            {
                if (_dChequeSt == null)
                    _dChequeSt = new dCheque();
                return _dChequeSt;
            }
        }

        /// <summary>
        /// Esta variável permite que a classe emitas messagebox e só deve ser setada para true por telas assistindas por usuários (nunca por processos)
        /// </summary>
        public bool PerminteDialogo;

        #region Table Adapters

        private dChequeTableAdapters.ContaCorrenteDetalheTableAdapter contaCorrenteDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteDetalhe
        /// </summary>
        public dChequeTableAdapters.ContaCorrenteDetalheTableAdapter ContaCorrenteDetalheTableAdapter
        {
            get
            {
                if (contaCorrenteDetalheTableAdapter == null)
                {
                    contaCorrenteDetalheTableAdapter = new dChequeTableAdapters.ContaCorrenteDetalheTableAdapter();
                    contaCorrenteDetalheTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenteDetalheTableAdapter;
            }
        }

        private dChequeTableAdapters.ImpostosRetidosTableAdapter impostosRetidosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ImpostosRetidos
        /// </summary>
        public dChequeTableAdapters.ImpostosRetidosTableAdapter ImpostosRetidosTableAdapter
        {
            get
            {
                if (impostosRetidosTableAdapter == null)
                {
                    impostosRetidosTableAdapter = new dChequeTableAdapters.ImpostosRetidosTableAdapter();
                    impostosRetidosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return impostosRetidosTableAdapter;
            }
        }

        /*
        private dChequeTableAdapters.Lancamentos_BradescoTableAdapter lancamentos_BradescoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Lancamentos_Bradesco
        /// </summary>
        public dChequeTableAdapters.Lancamentos_BradescoTableAdapter Lancamentos_BradescoTableAdapter
        {
            get
            {
                if (lancamentos_BradescoTableAdapter == null)
                {
                    lancamentos_BradescoTableAdapter = new dChequeTableAdapters.Lancamentos_BradescoTableAdapter();
                    lancamentos_BradescoTableAdapter.TrocarStringDeConexao(TiposDeBanco.Access);
                };
                return lancamentos_BradescoTableAdapter;
            }
        }*/

        private dChequeTableAdapters.CHEquesTableAdapter cHEquesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CHEques
        /// </summary>
        public dChequeTableAdapters.CHEquesTableAdapter CHEquesTableAdapter
        {
            get
            {
                if (cHEquesTableAdapter == null)
                {
                    cHEquesTableAdapter = new dChequeTableAdapters.CHEquesTableAdapter();
                    cHEquesTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cHEquesTableAdapter;
            }
        }

        private dChequeTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dChequeTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dChequeTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return pAGamentosTableAdapter;
            }
        }

        /*
        private dChequeTableAdapters.DadosComplementaresTableAdapter dadosComplementaresTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: DadosComplementares
        /// </summary>
        public dChequeTableAdapters.DadosComplementaresTableAdapter DadosComplementaresTableAdapter
        {
            get
            {
                if (dadosComplementaresTableAdapter == null)
                {
                    dadosComplementaresTableAdapter = new dChequeTableAdapters.DadosComplementaresTableAdapter();
                    dadosComplementaresTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return dadosComplementaresTableAdapter;
            }
        }*/

        private dChequeTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dChequeTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dChequeTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return contaCorrenTeTableAdapter;
            }
        }

        //*** MRC - INICIO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***
        private dChequeTableAdapters.ContaCorrenTeDepositoTableAdapter contaCorrenTeDepositoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTeDeposito
        /// </summary>
        public dChequeTableAdapters.ContaCorrenTeDepositoTableAdapter ContaCorrenTeDepositoTableAdapter
        {
            get
            {
                if (contaCorrenTeDepositoTableAdapter == null)
                {
                    contaCorrenTeDepositoTableAdapter = new dChequeTableAdapters.ContaCorrenTeDepositoTableAdapter();
                    contaCorrenTeDepositoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return contaCorrenTeDepositoTableAdapter;
            }
        }
        //*** MRC - TERMINO - TRIBUTO ELETRONICO (20/05/2015 17:00) ***

        private dChequeTableAdapters.FolhasCHequeTableAdapter folhasCHequeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FolhasCHeque
        /// </summary>
        public dChequeTableAdapters.FolhasCHequeTableAdapter FolhasCHequeTableAdapter
        {
            get
            {
                if (folhasCHequeTableAdapter == null)
                {
                    folhasCHequeTableAdapter = new dChequeTableAdapters.FolhasCHequeTableAdapter();
                    folhasCHequeTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return folhasCHequeTableAdapter;
            }
        }

        #endregion
        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CHE"></param>
        /// <returns></returns>
        public bool ApagarCheque(int CHE)
        {
            if (CHEquesTableAdapter.FillByCHE(CHEques, CHE) != 1)
                return false;

            CHEquesRow Cheque = CHEques[0];
            
            if (!Cheque.IsCHEantigoNull())
            {
                string ComandoDel1 = "delete from Pagamentos where contador = @P1";
                string ComandoDel2 = "delete from cheques where contadorcheque = @P1";
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoDel1, Cheque.CHEantigo);
                VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoDel2, Cheque.CHEantigo);
            };

            return true;
        }*/
    }
}


