﻿namespace WSSincro
{


    partial class dDOC
    {
        private static dDOC _dDOCSt;

        /// <summary>
        /// dataset estático:dDOC
        /// </summary>
        public static dDOC dDOCSt
        {
            get
            {
                if (_dDOCSt == null)
                {
                    _dDOCSt = new dDOC();
                }
                return _dDOCSt;
            }
        }

        private dDOCTableAdapters.DOCumentacaoTableAdapter dOCumentacaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DOCumentacao
        /// </summary>
        public dDOCTableAdapters.DOCumentacaoTableAdapter DOCumentacaoTableAdapter
        {
            get
            {
                if (dOCumentacaoTableAdapter == null)
                {
                    dOCumentacaoTableAdapter = new dDOCTableAdapters.DOCumentacaoTableAdapter();
                };
                return dOCumentacaoTableAdapter;
            }
        }

    }
}
