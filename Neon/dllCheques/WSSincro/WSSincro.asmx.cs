using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Configuration;
using System.IO;

namespace WSSincro
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://www.neonimoveis.com.br/neononline2/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class WS_Sincro : System.Web.Services.WebService
    {
        /// <summary>
        /// Cadastra um DOC
        /// </summary>
        /// <param name="_dDOC"></param>
        /// <param name="dados"></param>
        /// <returns></returns>
        [WebMethod]
        public string CadastraDOC(dDOC _dDOC, byte[] dados)
        {
            try
            {
                dDOC.DOCumentacaoRow row = _dDOC.DOCumentacao[0];
                string Arquivo = string.Format(@"{0}publicacoes\PF\{1}_{2}.{3}", ConfigurationSettings.AppSettings["publicacoes"], row.DOC_EMP, row.DOC, row.DOCEXT);
                using (MemoryStream ms = new MemoryStream(dados))
                {
                    using (FileStream fs = new FileStream(Arquivo, FileMode.Create))
                    {
                        ms.WriteTo(fs);
                        fs.Close();
                    };
                };
                _dDOC.DOCumentacaoTableAdapter.Update(_dDOC.DOCumentacao);
                _dDOC.AcceptChanges();
                return string.Format("ok - {0}",Arquivo);
            }
            catch (Exception e)
            {
                string retorno = string.Format("Erro - {0}\r\n",DateTime.Now);
                while (e != null)
                {
                    retorno += string.Format("{0}\r\n{1}\r\n\r\n",e.Message,e.StackTrace);
                    e = e.InnerException;
                }
                return retorno;
            }
        }

        /// <summary>
        /// Le um DOC
        /// </summary>
        /// <param name="EMP"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        [WebMethod]
        public dDOC Le(int EMP, int doc)
        {
            dDOC novodDOC = new dDOC();
            novodDOC.DOCumentacaoTableAdapter.FillByDOC(novodDOC.DOCumentacao,doc,EMP);
            return novodDOC;
        }

        /// <summary>
        /// Grava
        /// </summary>
        /// <param name="_dDOC"></param>
        /// <returns></returns>
        [WebMethod]
        public string Grava(dDOC _dDOC)
        {
            try
            {                
                _dDOC.DOCumentacaoTableAdapter.Update(_dDOC.DOCumentacao);
                _dDOC.AcceptChanges();
                return string.Format("ok");
            }
            catch (Exception e)
            {
                string retorno = string.Format("Erro - {0}\r\n", DateTime.Now);
                while (e != null)
                {
                    retorno += string.Format("{0}\r\n{1}\r\n\r\n", e.Message, e.StackTrace);
                    e = e.InnerException;
                }
                return retorno;
            }
        }
    }
}
