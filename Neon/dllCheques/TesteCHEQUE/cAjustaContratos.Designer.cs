﻿namespace TesteCHEQUE
{
    partial class cAjustaContratos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.pAGamentosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPAG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_NOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGDOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGDATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_CHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGIMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGPermiteAgrupar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGAdicional = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOAServico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOANumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADataEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOATotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_PGF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADadosPag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNBancoCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNAgenciaCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNAgenciaDgCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNContaCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNContaDgCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNContaTipoCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGamentosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(31, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.pAGamentosBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(54, 134);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(710, 380);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPAG,
            this.colPAGValor,
            this.colPAGVencimento,
            this.colPAG_NOA,
            this.colPAGA_USU,
            this.colPAGI_USU,
            this.colPAGTipo,
            this.colPAGDOC,
            this.colPAGDATAA,
            this.colPAGDATAI,
            this.colPAG_CHE,
            this.colPAGN,
            this.colPAGIMP,
            this.colPAGStatus,
            this.colPAG_GPS,
            this.colPAGPermiteAgrupar,
            this.colPAGAdicional,
            this.colNOA_PLA,
            this.colNOAServico,
            this.colFRNCnpj,
            this.colFRNNome,
            this.colFRNFantasia,
            this.colNOANumero,
            this.colNOADataEmissao,
            this.colNOATotal,
            this.colNOA_PGF,
            this.colNOADadosPag,
            this.colFRNEndereco,
            this.colFRNCep,
            this.colFRNBancoCredito,
            this.colFRNAgenciaCredito,
            this.colFRNAgenciaDgCredito,
            this.colFRNContaCredito,
            this.colFRNContaDgCredito,
            this.colFRNContaTipoCredito});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // pAGamentosBindingSource
            // 
            this.pAGamentosBindingSource.DataMember = "PAGamentos";
            this.pAGamentosBindingSource.DataSource = typeof(dllChequesProc.dCheque);
            // 
            // colPAG
            // 
            this.colPAG.FieldName = "PAG";
            this.colPAG.Name = "colPAG";
            this.colPAG.Visible = true;
            this.colPAG.VisibleIndex = 0;
            // 
            // colPAGValor
            // 
            this.colPAGValor.FieldName = "PAGValor";
            this.colPAGValor.Name = "colPAGValor";
            this.colPAGValor.Visible = true;
            this.colPAGValor.VisibleIndex = 1;
            // 
            // colPAGVencimento
            // 
            this.colPAGVencimento.FieldName = "PAGVencimento";
            this.colPAGVencimento.Name = "colPAGVencimento";
            this.colPAGVencimento.Visible = true;
            this.colPAGVencimento.VisibleIndex = 2;
            // 
            // colPAG_NOA
            // 
            this.colPAG_NOA.FieldName = "PAG_NOA";
            this.colPAG_NOA.Name = "colPAG_NOA";
            this.colPAG_NOA.Visible = true;
            this.colPAG_NOA.VisibleIndex = 3;
            // 
            // colPAGA_USU
            // 
            this.colPAGA_USU.FieldName = "PAGA_USU";
            this.colPAGA_USU.Name = "colPAGA_USU";
            this.colPAGA_USU.Visible = true;
            this.colPAGA_USU.VisibleIndex = 4;
            // 
            // colPAGI_USU
            // 
            this.colPAGI_USU.FieldName = "PAGI_USU";
            this.colPAGI_USU.Name = "colPAGI_USU";
            this.colPAGI_USU.Visible = true;
            this.colPAGI_USU.VisibleIndex = 5;
            // 
            // colPAGTipo
            // 
            this.colPAGTipo.FieldName = "PAGTipo";
            this.colPAGTipo.Name = "colPAGTipo";
            this.colPAGTipo.Visible = true;
            this.colPAGTipo.VisibleIndex = 6;
            // 
            // colPAGDOC
            // 
            this.colPAGDOC.FieldName = "PAGDOC";
            this.colPAGDOC.Name = "colPAGDOC";
            this.colPAGDOC.Visible = true;
            this.colPAGDOC.VisibleIndex = 7;
            // 
            // colPAGDATAA
            // 
            this.colPAGDATAA.FieldName = "PAGDATAA";
            this.colPAGDATAA.Name = "colPAGDATAA";
            this.colPAGDATAA.Visible = true;
            this.colPAGDATAA.VisibleIndex = 8;
            // 
            // colPAGDATAI
            // 
            this.colPAGDATAI.FieldName = "PAGDATAI";
            this.colPAGDATAI.Name = "colPAGDATAI";
            this.colPAGDATAI.Visible = true;
            this.colPAGDATAI.VisibleIndex = 9;
            // 
            // colPAG_CHE
            // 
            this.colPAG_CHE.FieldName = "PAG_CHE";
            this.colPAG_CHE.Name = "colPAG_CHE";
            this.colPAG_CHE.Visible = true;
            this.colPAG_CHE.VisibleIndex = 10;
            // 
            // colPAGN
            // 
            this.colPAGN.FieldName = "PAGN";
            this.colPAGN.Name = "colPAGN";
            this.colPAGN.Visible = true;
            this.colPAGN.VisibleIndex = 11;
            // 
            // colPAGIMP
            // 
            this.colPAGIMP.FieldName = "PAGIMP";
            this.colPAGIMP.Name = "colPAGIMP";
            this.colPAGIMP.Visible = true;
            this.colPAGIMP.VisibleIndex = 12;
            // 
            // colPAGStatus
            // 
            this.colPAGStatus.FieldName = "PAGStatus";
            this.colPAGStatus.Name = "colPAGStatus";
            this.colPAGStatus.Visible = true;
            this.colPAGStatus.VisibleIndex = 13;
            // 
            // colPAG_GPS
            // 
            this.colPAG_GPS.FieldName = "PAG_GPS";
            this.colPAG_GPS.Name = "colPAG_GPS";
            this.colPAG_GPS.Visible = true;
            this.colPAG_GPS.VisibleIndex = 14;
            // 
            // colPAGPermiteAgrupar
            // 
            this.colPAGPermiteAgrupar.FieldName = "PAGPermiteAgrupar";
            this.colPAGPermiteAgrupar.Name = "colPAGPermiteAgrupar";
            this.colPAGPermiteAgrupar.Visible = true;
            this.colPAGPermiteAgrupar.VisibleIndex = 15;
            // 
            // colPAGAdicional
            // 
            this.colPAGAdicional.FieldName = "PAGAdicional";
            this.colPAGAdicional.Name = "colPAGAdicional";
            this.colPAGAdicional.Visible = true;
            this.colPAGAdicional.VisibleIndex = 16;
            // 
            // colNOA_PLA
            // 
            this.colNOA_PLA.FieldName = "NOA_PLA";
            this.colNOA_PLA.Name = "colNOA_PLA";
            this.colNOA_PLA.Visible = true;
            this.colNOA_PLA.VisibleIndex = 17;
            // 
            // colNOAServico
            // 
            this.colNOAServico.FieldName = "NOAServico";
            this.colNOAServico.Name = "colNOAServico";
            this.colNOAServico.Visible = true;
            this.colNOAServico.VisibleIndex = 18;
            // 
            // colFRNCnpj
            // 
            this.colFRNCnpj.FieldName = "FRNCnpj";
            this.colFRNCnpj.Name = "colFRNCnpj";
            this.colFRNCnpj.Visible = true;
            this.colFRNCnpj.VisibleIndex = 19;
            // 
            // colFRNNome
            // 
            this.colFRNNome.FieldName = "FRNNome";
            this.colFRNNome.Name = "colFRNNome";
            this.colFRNNome.Visible = true;
            this.colFRNNome.VisibleIndex = 20;
            // 
            // colFRNFantasia
            // 
            this.colFRNFantasia.FieldName = "FRNFantasia";
            this.colFRNFantasia.Name = "colFRNFantasia";
            this.colFRNFantasia.Visible = true;
            this.colFRNFantasia.VisibleIndex = 21;
            // 
            // colNOANumero
            // 
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.Visible = true;
            this.colNOANumero.VisibleIndex = 22;
            // 
            // colNOADataEmissao
            // 
            this.colNOADataEmissao.FieldName = "NOADataEmissao";
            this.colNOADataEmissao.Name = "colNOADataEmissao";
            this.colNOADataEmissao.Visible = true;
            this.colNOADataEmissao.VisibleIndex = 23;
            // 
            // colNOATotal
            // 
            this.colNOATotal.FieldName = "NOATotal";
            this.colNOATotal.Name = "colNOATotal";
            this.colNOATotal.Visible = true;
            this.colNOATotal.VisibleIndex = 24;
            // 
            // colNOA_PGF
            // 
            this.colNOA_PGF.FieldName = "NOA_PGF";
            this.colNOA_PGF.Name = "colNOA_PGF";
            this.colNOA_PGF.Visible = true;
            this.colNOA_PGF.VisibleIndex = 25;
            // 
            // colNOADadosPag
            // 
            this.colNOADadosPag.FieldName = "NOADadosPag";
            this.colNOADadosPag.Name = "colNOADadosPag";
            this.colNOADadosPag.Visible = true;
            this.colNOADadosPag.VisibleIndex = 26;
            // 
            // colFRNEndereco
            // 
            this.colFRNEndereco.FieldName = "FRNEndereco";
            this.colFRNEndereco.Name = "colFRNEndereco";
            this.colFRNEndereco.Visible = true;
            this.colFRNEndereco.VisibleIndex = 27;
            // 
            // colFRNCep
            // 
            this.colFRNCep.FieldName = "FRNCep";
            this.colFRNCep.Name = "colFRNCep";
            this.colFRNCep.Visible = true;
            this.colFRNCep.VisibleIndex = 28;
            // 
            // colFRNBancoCredito
            // 
            this.colFRNBancoCredito.FieldName = "FRNBancoCredito";
            this.colFRNBancoCredito.Name = "colFRNBancoCredito";
            this.colFRNBancoCredito.Visible = true;
            this.colFRNBancoCredito.VisibleIndex = 29;
            // 
            // colFRNAgenciaCredito
            // 
            this.colFRNAgenciaCredito.FieldName = "FRNAgenciaCredito";
            this.colFRNAgenciaCredito.Name = "colFRNAgenciaCredito";
            this.colFRNAgenciaCredito.Visible = true;
            this.colFRNAgenciaCredito.VisibleIndex = 30;
            // 
            // colFRNAgenciaDgCredito
            // 
            this.colFRNAgenciaDgCredito.FieldName = "FRNAgenciaDgCredito";
            this.colFRNAgenciaDgCredito.Name = "colFRNAgenciaDgCredito";
            this.colFRNAgenciaDgCredito.Visible = true;
            this.colFRNAgenciaDgCredito.VisibleIndex = 31;
            // 
            // colFRNContaCredito
            // 
            this.colFRNContaCredito.FieldName = "FRNContaCredito";
            this.colFRNContaCredito.Name = "colFRNContaCredito";
            this.colFRNContaCredito.Visible = true;
            this.colFRNContaCredito.VisibleIndex = 32;
            // 
            // colFRNContaDgCredito
            // 
            this.colFRNContaDgCredito.FieldName = "FRNContaDgCredito";
            this.colFRNContaDgCredito.Name = "colFRNContaDgCredito";
            this.colFRNContaDgCredito.Visible = true;
            this.colFRNContaDgCredito.VisibleIndex = 33;
            // 
            // colFRNContaTipoCredito
            // 
            this.colFRNContaTipoCredito.FieldName = "FRNContaTipoCredito";
            this.colFRNContaTipoCredito.Name = "colFRNContaTipoCredito";
            this.colFRNContaTipoCredito.Visible = true;
            this.colFRNContaTipoCredito.VisibleIndex = 34;
            // 
            // cAjustaContratos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.button1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cAjustaContratos";
            this.Size = new System.Drawing.Size(785, 528);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGamentosBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource pAGamentosBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_NOA;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGA_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGDOC;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGDATAA;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_CHE;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGN;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGIMP;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_GPS;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGPermiteAgrupar;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGAdicional;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colNOAServico;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNNome;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFantasia;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADataEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PGF;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADadosPag;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNCep;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNBancoCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNAgenciaCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNAgenciaDgCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNContaCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNContaDgCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNContaTipoCredito;
    }
}
