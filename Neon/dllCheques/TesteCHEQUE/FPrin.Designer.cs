namespace TesteCHEQUE
{
    partial class FPrin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            CompontesBasicos.ModuleInfo moduleInfo1 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo2 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo3 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo4 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo5 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo6 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo7 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo8 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo9 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo10 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo11 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo12 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo13 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo14 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo15 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo16 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo17 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo18 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo19 = new CompontesBasicos.ModuleInfo();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup3 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup4 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup5 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem3 = new DevExpress.XtraNavBar.NavBarItem();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.DockPanel_FContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            this.DockPanel_F.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureEdit_F
            // 
            this.PictureEdit_F.Cursor = System.Windows.Forms.Cursors.Default;
            // 
            // NavBarControl_F
            // 
            this.NavBarControl_F.ActiveGroup = this.navBarGroup2;
            this.NavBarControl_F.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup2,
            this.navBarGroup3,
            this.navBarGroup4,
            this.navBarGroup5});
            this.NavBarControl_F.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem2,
            this.navBarItem3});
            this.NavBarControl_F.OptionsNavPane.ExpandedWidth = 194;
            this.NavBarControl_F.Size = new System.Drawing.Size(194, 430);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            this.DefaultLookAndFeel_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.DefaultLookAndFeel_F.LookAndFeel.UseWindowsXPTheme = true;
            // 
            // DefaultToolTipController_F
            // 
            // 
            // 
            // 
            this.DefaultToolTipController_F.DefaultController.AutoPopDelay = 10000;
            this.DefaultToolTipController_F.DefaultController.Rounded = true;
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // DockPanel_FContainer
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.DockPanel_FContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.DockPanel_FContainer.Location = new System.Drawing.Point(3, 20);
            this.DockPanel_FContainer.Size = new System.Drawing.Size(194, 511);
            // 
            // DockPanel_F
            // 
            this.DockPanel_F.Appearance.BackColor = System.Drawing.Color.White;
            this.DockPanel_F.Appearance.Options.UseBackColor = true;
            this.DockPanel_F.Options.AllowDockFill = false;
            this.DockPanel_F.Options.ShowCloseButton = false;
            this.DockPanel_F.Options.ShowMaximizeButton = false;
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "navBarGroup1";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem1)});
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "teste ret";
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Grupo 1";
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // navBarGroup3
            // 
            this.navBarGroup3.Caption = "Cheques";
            this.navBarGroup3.Expanded = true;
            this.navBarGroup3.Name = "navBarGroup3";
            // 
            // navBarGroup4
            // 
            this.navBarGroup4.Caption = "Notas";
            this.navBarGroup4.Expanded = true;
            this.navBarGroup4.Name = "navBarGroup4";
            // 
            // navBarGroup5
            // 
            this.navBarGroup5.Caption = "navBarGroup5";
            this.navBarGroup5.Expanded = true;
            this.navBarGroup5.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem2),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem3)});
            this.navBarGroup5.Name = "navBarGroup5";
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "Teste";
            this.navBarItem2.Name = "navBarItem2";
            this.navBarItem2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem2_LinkClicked);
            // 
            // navBarItem3
            // 
            this.navBarItem3.Caption = "Agrupa Cheques";
            this.navBarItem3.Name = "navBarItem3";
            this.navBarItem3.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem3_LinkClicked);
            // 
            // FPrin
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(782, 556);
            moduleInfo1.Grupo = 2;
            moduleInfo1.ImagemG = null;
            moduleInfo1.ImagemP = null;
            moduleInfo1.ModuleType = null;
            moduleInfo1.ModuleTypestr = "ContaPagar.NotasGrade";
            moduleInfo1.Name = "Notas";
            moduleInfo2.Grupo = 1;
            moduleInfo2.ImagemG = null;
            moduleInfo2.ImagemP = null;
            moduleInfo2.ModuleType = null;
            moduleInfo2.ModuleTypestr = "dllCheques.cChequesEmitir";
            moduleInfo2.Name = "Imprimir Cheques";
            moduleInfo3.Grupo = 0;
            moduleInfo3.ImagemG = null;
            moduleInfo3.ImagemP = null;
            moduleInfo3.ModuleType = null;
            moduleInfo3.ModuleTypestr = "dllCheques.cChequesGrade";
            moduleInfo3.Name = "Honor�rios";
            moduleInfo4.Grupo = 2;
            moduleInfo4.ImagemG = null;
            moduleInfo4.ImagemP = null;
            moduleInfo4.ModuleType = null;
            moduleInfo4.ModuleTypestr = "ContaPagar.Follow.cPagamentosPeriodicos";
            moduleInfo4.Name = "Pagamentos Periodicos";
            moduleInfo5.Grupo = 0;
            moduleInfo5.ImagemG = null;
            moduleInfo5.ImagemP = null;
            moduleInfo5.ModuleType = null;
            moduleInfo5.ModuleTypestr = "ContaPagar.Acompanhamento.cAcompImpostos";
            moduleInfo5.Name = "Impostos";
            moduleInfo6.Grupo = 0;
            moduleInfo6.ImagemG = null;
            moduleInfo6.ImagemP = null;
            moduleInfo6.ModuleType = null;
            moduleInfo6.ModuleTypestr = "dllFaturamento.cImportaRepasse";
            moduleInfo6.Name = "Faturamento";
            moduleInfo7.Grupo = 0;
            moduleInfo7.ImagemG = null;
            moduleInfo7.ImagemP = null;
            moduleInfo7.ModuleType = null;
            moduleInfo7.ModuleTypestr = null;
            moduleInfo7.Name = "Errado";
            moduleInfo8.Grupo = 0;
            moduleInfo8.ImagemG = null;
            moduleInfo8.ImagemP = null;
            moduleInfo8.ModuleType = null;
            moduleInfo8.ModuleTypestr = "dllFaturamento.cFaturamento";
            moduleInfo8.Name = "EmitirNotas";
            moduleInfo9.Grupo = 0;
            moduleInfo9.ImagemG = null;
            moduleInfo9.ImagemP = null;
            moduleInfo9.ModuleType = null;
            moduleInfo9.ModuleTypestr = "dllFaturamento.GISS.cGISS";
            moduleInfo9.Name = "GISS";
            moduleInfo10.Grupo = 0;
            moduleInfo10.ImagemG = null;
            moduleInfo10.ImagemP = null;
            moduleInfo10.ModuleType = null;
            moduleInfo10.ModuleTypestr = "ContaPagar.ContasAgLuz.cContaAgLuz";
            moduleInfo10.Name = "Agua luz";
            moduleInfo11.Grupo = 0;
            moduleInfo11.ImagemG = null;
            moduleInfo11.ImagemP = null;
            moduleInfo11.ModuleType = null;
            moduleInfo11.ModuleTypestr = "ContaPagar.ContasAgLuz.cAcompanhamento";
            moduleInfo11.Name = "Acompanhamento";
            moduleInfo12.Grupo = 0;
            moduleInfo12.ImagemG = null;
            moduleInfo12.ImagemP = null;
            moduleInfo12.ModuleType = null;
            moduleInfo12.ModuleTypestr = "TesteCHEQUE.UserControl1";
            moduleInfo12.Name = "correcao numero";
            moduleInfo13.Grupo = 0;
            moduleInfo13.ImagemG = null;
            moduleInfo13.ImagemP = null;
            moduleInfo13.IniciarAberto = true;
            moduleInfo13.ModuleType = null;
            moduleInfo13.ModuleTypestr = "Framework.Alarmes.cMeusAlarmes";
            moduleInfo13.Name = "Avisos";
            moduleInfo14.Grupo = 0;
            moduleInfo14.ImagemG = null;
            moduleInfo14.ImagemP = null;
            moduleInfo14.ModuleType = null;
            moduleInfo14.ModuleTypestr = "dllCheques.cFolhasDeCheque";
            moduleInfo14.Name = "Tal�o";
            moduleInfo15.Grupo = 0;
            moduleInfo15.ImagemG = null;
            moduleInfo15.ImagemP = null;
            moduleInfo15.ModuleType = null;
            moduleInfo15.ModuleTypestr = "dllCheques.cSaldoCheques";
            moduleInfo15.Name = "Saldo cheques";
            moduleInfo16.Grupo = 0;
            moduleInfo16.ImagemG = null;
            moduleInfo16.ImagemP = null;
            moduleInfo16.ModuleType = null;
            moduleInfo16.ModuleTypestr = "TesteCHEQUE.cAjustaContratos";
            moduleInfo16.Name = "Ajusta Contratos";
            moduleInfo17.Grupo = 0;
            moduleInfo17.ImagemG = null;
            moduleInfo17.ImagemP = null;
            moduleInfo17.ModuleType = null;
            moduleInfo17.ModuleTypestr = "ContaPagar.Follow.cPagPerTipo";
            moduleInfo17.Name = "Peri�dicos DP";
            moduleInfo18.Grupo = 0;
            moduleInfo18.ImagemG = null;
            moduleInfo18.ImagemP = null;
            moduleInfo18.ModuleType = null;
            moduleInfo18.ModuleTypestr = "TesteCHEQUE.testeForn";
            moduleInfo18.Name = "TESTEFORN";
            moduleInfo19.Grupo = 2;
            moduleInfo19.ImagemG = null;
            moduleInfo19.ImagemP = null;
            moduleInfo19.ModuleType = typeof(ContaPagar.cBuscaNota);            
            moduleInfo19.Name = "NFS-e";
            this.ModuleInfoCollection.AddRange(new CompontesBasicos.ModuleInfo[] {
            moduleInfo1,
            moduleInfo2,
            moduleInfo3,
            moduleInfo4,
            moduleInfo5,
            moduleInfo6,
            moduleInfo7,
            moduleInfo8,
            moduleInfo9,
            moduleInfo10,
            moduleInfo11,
            moduleInfo12,
            moduleInfo13,
            moduleInfo14,
            moduleInfo15,
            moduleInfo16,
            moduleInfo17,
            moduleInfo18,
            moduleInfo19});
            this.Name = "FPrin";
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.DockPanel_FContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            this.DockPanel_F.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup3;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup4;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup5;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem3;
    }
}
