﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace TesteCHEQUE
{
    public partial class UserControl1 : CompontesBasicos.ComponenteBase
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            dataSet1.CONDOMINIOSTableAdapter.Fill(dataSet1.CONDOMINIOS);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dataSet1.CONDOMINIOSTableAdapter.Update(dataSet1.CONDOMINIOS);
            dataSet1.CONDOMINIOS.AcceptChanges();
        }
    }
}
