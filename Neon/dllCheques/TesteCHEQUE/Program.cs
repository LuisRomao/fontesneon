using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VirDB.Bancovirtual;

namespace TesteCHEQUE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            CompontesBasicosProc.FuncoesBasicasProc.AmbienteDesenvolvimento = true;                        
            BancoVirtual.Popular("QAS - SBC CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
            BancoVirtual.Popular("QAS - SA CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");            
            BancoVirtual.PopularFilial("QAS - SBC CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
            BancoVirtual.PopularFilial("QAS - SA CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
            //BancoVirtual.PopularFilial("NEON SBC F (cuidado)", @"Data Source=cp.neonimoveis.com;Initial Catalog=NEON;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            //BancoVirtual.PopularFilial("NEON SA F (cuidado)", @"Data Source=cp.neonimoveis.com;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");

            VirDB.Bancovirtual.BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
            VirDB.Bancovirtual.BancoVirtual.PopularAcces("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
            //VirDB.Bancovirtual.BancoVirtual.PopularAcces("LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\neon.MDA;Jet OLEDB:Database Password=7778");

            VirDB.Bancovirtual.BancoVirtual.PopularAccesH("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\hist.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
            VirDB.Bancovirtual.BancoVirtual.PopularAccesH("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
            //VirDB.Bancovirtual.BancoVirtual.PopularAccesH("LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\neon.MDA;Jet OLEDB:Database Password=7778");



            BancoVirtual.Popular("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON;Initial Catalog=Neon;Persist Security Info=True;User ID=NEONP;Connect Timeout=30;Password=TPC4P2DP");
            BancoVirtual.Popular("NEON SA  (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            BancoVirtual.PopularFilial("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON;Initial Catalog=Neon;Persist Security Info=True;User ID=NEONP;Connect Timeout=30;Password=TPC4P2DP");
            BancoVirtual.PopularFilial("NEON SA  (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            //BancoVirtual.Popular("NEON SA (cuidado)", @"Data Source=cp.neonimoveis.com;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");                                
            //VirDB.Bancovirtual.BancoVirtual.PopularAcces("NEON (servidor 2)", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\newscc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\newscc\neon.mda;Jet OLEDB:Database Password=96333018");
            VirDB.Bancovirtual.BancoVirtual.PopularAcces("NEON Access (CUIDADO)", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon01\newscc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon01\newscc\neon.mda;Jet OLEDB:Database Password=96333018");            
            VirDB.Bancovirtual.BancoVirtual.PopularAcces("NEON Access G:", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=G:\neon.MDA;Jet OLEDB:Database Password=7778");
            //VirDB.Bancovirtual.BancoVirtual.PopularAccesH("NEON (servidor 2)", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\newscc\hist.MDB;Persist Security Info=True;User ID=NEON;Password=96333018;Jet OLEDB:System database=\\neon02\newscc\neon.MDA;Jet OLEDB:Database Password=96333018");

            VirDB.Bancovirtual.BancoVirtual.PopularAcces("NEON Access SA", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\SA\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=C:\clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
            VirDB.Bancovirtual.BancoVirtual.PopularAccesH("NEON Access H SA", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=g:\neon.MDA;Jet OLEDB:Database Password=7778");
            VirDB.Bancovirtual.BancoVirtual.PopularInternet("INTERNET Local", @"Data Source=Lapvirtual2017\SQLEXPRESS;Initial Catalog=neoninternet;Persist Security Info=True;User ID=sa;Password=venus", 1);
            VirDB.Bancovirtual.BancoVirtual.PopularInternet("INTERNET REAL1", @"Data Source=sqlserver01.neonimoveis.com.br;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=neonimoveis_1;Password=bancosql2000", 1);
            VirDB.Bancovirtual.BancoVirtual.PopularInternet("INTERNET REAL2", @"Data Source=sqlserver01.neonimoveis.com.br;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=neonimoveis_2;Password=formiga9999;Connect Timeout=20", 2);
            using (Login.fLogin NovoLg = new Login.fLogin())
            {
                NovoLg.ShowDialog();
            }
            Application.Run(new FPrin());
        }
    }
}