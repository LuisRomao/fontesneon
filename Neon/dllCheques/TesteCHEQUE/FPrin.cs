using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using VirEnumeracoes;

namespace TesteCHEQUE
{
    public partial class FPrin : CompontesBasicos.FormPrincipalBase
    {
        public FPrin()
        {
            InitializeComponent();
        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            dllImpostos.SolicitaRetencao Sir = new dllImpostos.SolicitaRetencao(TipoImposto.IR,0,SimNao.Sim);
            dllImpostos.SolicitaRetencao Sinss = new dllImpostos.SolicitaRetencao(TipoImposto.INSSpf,0,SimNao.Sim);
            dllImpostos.SolicitaRetencao Sinsse = new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfEmp,0,SimNao.Sim);
            dllImpostos.SolicitaRetencao Sinssp = new dllImpostos.SolicitaRetencao(TipoImposto.INSSpfRet,0,SimNao.Sim);
            //ContaPagar.dNOtAs.dNOtAsSt.IncluiNotaSimples(88676, DateTime.Today.AddDays(-4), new DateTime(2009, 11, 1), ContaPagar.dNOtAs.NOATipo.Avulsa, "Luis", 233, dllCheques.dCheque.PAGTipo.cheque, true, "207007", "servico Teste", 1000, 0);
            //ContaPagar.dNOtAs.dNOtAsSt.IncluiNotaSimples(88677, DateTime.Today.AddDays(-4), new DateTime(2009, 11, 1), ContaPagar.dNOtAs.NOATipo.Avulsa, "Luis", 233, dllCheques.dCheque.PAGTipo.cheque, true, "207007", "servico Teste", 1000, 0, false, Sir, Sinss);
            //ContaPagar.dNOtAs.dNOtAsSt.IncluiNotaSimples(88678, DateTime.Today.AddDays(-4), new DateTime(2009, 11, 1), ContaPagar.dNOtAs.NOATipo.Avulsa, "Luis", 233, dllCheques.dCheque.PAGTipo.cheque, true, "207007", "servico Teste", 1000, 0, true, Sir, Sinss);
            //ContaPagar.dNOtAs.dNOtAsSt.IncluiNotaSimples(88679, DateTime.Today.AddDays(-4), new DateTime(2009, 11, 1), ContaPagar.dNOtAs.NOATipo.Avulsa, "Luis", 233, dllCheques.dCheque.PAGTipo.cheque, true, "207007", "servico Teste", 1000, 0, false, Sir, Sinsse, Sinssp);
        }

        private void navBarItem2_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao = true;
            OpenFileDialog X = new OpenFileDialog();
            X.Multiselect = true;
            if (X.ShowDialog() == DialogResult.OK)
            {
                foreach (string Arquvivo in X.FileNames)
                {
                    string nome = System.IO.Path.GetFileNameWithoutExtension(Arquvivo);
                    int EMP = int.Parse(nome.Substring(0, 1));
                    int NOA = int.Parse(nome.Substring(2));

                    ContaPagar.Nota Nota1 = new ContaPagar.Nota(NOA, new FrameworkProc.EMPTProc(EMP == 1 ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial));
                    Nota1.PublicaNotas(Arquvivo, "");
                    Console.WriteLine(string.Format("-----> {0}\r\n",Arquvivo));
                    DateTime Esperar = DateTime.Now.AddSeconds(1);
                    while (DateTime.Now < Esperar)
                        Application.DoEvents();
                }
            }
            Console.WriteLine(string.Format("FIM\r\n"));
        }

        private void navBarItem3_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            int CHE1;
            int CHE2;
            VirInput.Input.Execute("Cheque 1", out CHE1);
            VirInput.Input.Execute("Cheque 1", out CHE2);
            dllCheques.Cheque CH1 = new dllCheques.Cheque(CHE1);
            dllCheques.Cheque CH2 = new dllCheques.Cheque(CHE2);
            CH1.AgruparRecebe(CH2);

        }
    }
}

