﻿namespace TesteCHEQUE
{
    partial class testeForn
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dFornecedoresCamposBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFone3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNDataAtualizacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNAtualizacao_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNDataInclusao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNInclusao_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRN_CID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFone4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNRegistroUF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNSimples = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNIE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNSimplesAliquota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNSenha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNAtivoSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNContaDgCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNContaCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNAgenciaDgCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNAgenciaCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNBancoCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNContaTipoCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNRetemISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNRetemINSSTomador = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNRetemINSSPrestador = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFornecedoresCamposBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "FORNECEDORES";
            this.gridControl1.DataSource = this.dFornecedoresCamposBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(58, 201);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(811, 375);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dFornecedoresCamposBindingSource
            // 
            this.dFornecedoresCamposBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresCampos);
            this.dFornecedoresCamposBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFRN,
            this.colFRNCnpj,
            this.colFRNNome,
            this.colFRNEndereco,
            this.colFRNBairro,
            this.colFRNCep,
            this.colFRNFone1,
            this.colFRNFone2,
            this.colFRNFone3,
            this.colFRNEmail,
            this.colFRNSite,
            this.colFRNDataAtualizacao,
            this.colFRNAtualizacao_USU,
            this.colFRNDataInclusao,
            this.colFRNInclusao_USU,
            this.colFRNTipo,
            this.colFRN_CID,
            this.colFRNFone4,
            this.colFRNFantasia,
            this.colFRNRegistro,
            this.colFRNRegistroUF,
            this.colFRNSimples,
            this.colFRNNumero,
            this.colFRNIE,
            this.colFRNSimplesAliquota,
            this.colFRNSenha,
            this.colFRNAtivoSite,
            this.colFRNContaDgCredito,
            this.colFRNContaCredito,
            this.colFRNAgenciaDgCredito,
            this.colFRNAgenciaCredito,
            this.colFRNBancoCredito,
            this.colFRNContaTipoCredito,
            this.colFRNRetemISS,
            this.colFRNRetemINSSTomador,
            this.colFRNRetemINSSPrestador,
            this.colCIDNome,
            this.colCIDUf});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colFRN
            // 
            this.colFRN.FieldName = "FRN";
            this.colFRN.Name = "colFRN";
            this.colFRN.Visible = true;
            this.colFRN.VisibleIndex = 0;
            // 
            // colFRNCnpj
            // 
            this.colFRNCnpj.FieldName = "FRNCnpj";
            this.colFRNCnpj.Name = "colFRNCnpj";
            this.colFRNCnpj.Visible = true;
            this.colFRNCnpj.VisibleIndex = 1;
            // 
            // colFRNNome
            // 
            this.colFRNNome.FieldName = "FRNNome";
            this.colFRNNome.Name = "colFRNNome";
            this.colFRNNome.Visible = true;
            this.colFRNNome.VisibleIndex = 2;
            // 
            // colFRNEndereco
            // 
            this.colFRNEndereco.FieldName = "FRNEndereco";
            this.colFRNEndereco.Name = "colFRNEndereco";
            this.colFRNEndereco.Visible = true;
            this.colFRNEndereco.VisibleIndex = 3;
            // 
            // colFRNBairro
            // 
            this.colFRNBairro.FieldName = "FRNBairro";
            this.colFRNBairro.Name = "colFRNBairro";
            this.colFRNBairro.Visible = true;
            this.colFRNBairro.VisibleIndex = 4;
            // 
            // colFRNCep
            // 
            this.colFRNCep.FieldName = "FRNCep";
            this.colFRNCep.Name = "colFRNCep";
            this.colFRNCep.Visible = true;
            this.colFRNCep.VisibleIndex = 5;
            // 
            // colFRNFone1
            // 
            this.colFRNFone1.FieldName = "FRNFone1";
            this.colFRNFone1.Name = "colFRNFone1";
            this.colFRNFone1.Visible = true;
            this.colFRNFone1.VisibleIndex = 6;
            // 
            // colFRNFone2
            // 
            this.colFRNFone2.FieldName = "FRNFone2";
            this.colFRNFone2.Name = "colFRNFone2";
            this.colFRNFone2.Visible = true;
            this.colFRNFone2.VisibleIndex = 7;
            // 
            // colFRNFone3
            // 
            this.colFRNFone3.FieldName = "FRNFone3";
            this.colFRNFone3.Name = "colFRNFone3";
            this.colFRNFone3.Visible = true;
            this.colFRNFone3.VisibleIndex = 8;
            // 
            // colFRNEmail
            // 
            this.colFRNEmail.FieldName = "FRNEmail";
            this.colFRNEmail.Name = "colFRNEmail";
            this.colFRNEmail.Visible = true;
            this.colFRNEmail.VisibleIndex = 9;
            // 
            // colFRNSite
            // 
            this.colFRNSite.FieldName = "FRNSite";
            this.colFRNSite.Name = "colFRNSite";
            this.colFRNSite.Visible = true;
            this.colFRNSite.VisibleIndex = 10;
            // 
            // colFRNDataAtualizacao
            // 
            this.colFRNDataAtualizacao.FieldName = "FRNDataAtualizacao";
            this.colFRNDataAtualizacao.Name = "colFRNDataAtualizacao";
            this.colFRNDataAtualizacao.Visible = true;
            this.colFRNDataAtualizacao.VisibleIndex = 11;
            // 
            // colFRNAtualizacao_USU
            // 
            this.colFRNAtualizacao_USU.FieldName = "FRNAtualizacao_USU";
            this.colFRNAtualizacao_USU.Name = "colFRNAtualizacao_USU";
            this.colFRNAtualizacao_USU.Visible = true;
            this.colFRNAtualizacao_USU.VisibleIndex = 12;
            // 
            // colFRNDataInclusao
            // 
            this.colFRNDataInclusao.FieldName = "FRNDataInclusao";
            this.colFRNDataInclusao.Name = "colFRNDataInclusao";
            this.colFRNDataInclusao.Visible = true;
            this.colFRNDataInclusao.VisibleIndex = 13;
            // 
            // colFRNInclusao_USU
            // 
            this.colFRNInclusao_USU.FieldName = "FRNInclusao_USU";
            this.colFRNInclusao_USU.Name = "colFRNInclusao_USU";
            this.colFRNInclusao_USU.Visible = true;
            this.colFRNInclusao_USU.VisibleIndex = 14;
            // 
            // colFRNTipo
            // 
            this.colFRNTipo.FieldName = "FRNTipo";
            this.colFRNTipo.Name = "colFRNTipo";
            this.colFRNTipo.Visible = true;
            this.colFRNTipo.VisibleIndex = 15;
            // 
            // colFRN_CID
            // 
            this.colFRN_CID.FieldName = "FRN_CID";
            this.colFRN_CID.Name = "colFRN_CID";
            this.colFRN_CID.Visible = true;
            this.colFRN_CID.VisibleIndex = 16;
            // 
            // colFRNFone4
            // 
            this.colFRNFone4.FieldName = "FRNFone4";
            this.colFRNFone4.Name = "colFRNFone4";
            this.colFRNFone4.Visible = true;
            this.colFRNFone4.VisibleIndex = 17;
            // 
            // colFRNFantasia
            // 
            this.colFRNFantasia.FieldName = "FRNFantasia";
            this.colFRNFantasia.Name = "colFRNFantasia";
            this.colFRNFantasia.Visible = true;
            this.colFRNFantasia.VisibleIndex = 18;
            // 
            // colFRNRegistro
            // 
            this.colFRNRegistro.FieldName = "FRNRegistro";
            this.colFRNRegistro.Name = "colFRNRegistro";
            this.colFRNRegistro.Visible = true;
            this.colFRNRegistro.VisibleIndex = 19;
            // 
            // colFRNRegistroUF
            // 
            this.colFRNRegistroUF.FieldName = "FRNRegistroUF";
            this.colFRNRegistroUF.Name = "colFRNRegistroUF";
            this.colFRNRegistroUF.Visible = true;
            this.colFRNRegistroUF.VisibleIndex = 20;
            // 
            // colFRNSimples
            // 
            this.colFRNSimples.FieldName = "FRNSimples";
            this.colFRNSimples.Name = "colFRNSimples";
            this.colFRNSimples.Visible = true;
            this.colFRNSimples.VisibleIndex = 21;
            // 
            // colFRNNumero
            // 
            this.colFRNNumero.FieldName = "FRNNumero";
            this.colFRNNumero.Name = "colFRNNumero";
            this.colFRNNumero.Visible = true;
            this.colFRNNumero.VisibleIndex = 22;
            // 
            // colFRNIE
            // 
            this.colFRNIE.FieldName = "FRNIE";
            this.colFRNIE.Name = "colFRNIE";
            this.colFRNIE.Visible = true;
            this.colFRNIE.VisibleIndex = 23;
            // 
            // colFRNSimplesAliquota
            // 
            this.colFRNSimplesAliquota.FieldName = "FRNSimplesAliquota";
            this.colFRNSimplesAliquota.Name = "colFRNSimplesAliquota";
            this.colFRNSimplesAliquota.Visible = true;
            this.colFRNSimplesAliquota.VisibleIndex = 24;
            // 
            // colFRNSenha
            // 
            this.colFRNSenha.FieldName = "FRNSenha";
            this.colFRNSenha.Name = "colFRNSenha";
            this.colFRNSenha.Visible = true;
            this.colFRNSenha.VisibleIndex = 25;
            // 
            // colFRNAtivoSite
            // 
            this.colFRNAtivoSite.FieldName = "FRNAtivoSite";
            this.colFRNAtivoSite.Name = "colFRNAtivoSite";
            this.colFRNAtivoSite.Visible = true;
            this.colFRNAtivoSite.VisibleIndex = 26;
            // 
            // colFRNContaDgCredito
            // 
            this.colFRNContaDgCredito.FieldName = "FRNContaDgCredito";
            this.colFRNContaDgCredito.Name = "colFRNContaDgCredito";
            this.colFRNContaDgCredito.Visible = true;
            this.colFRNContaDgCredito.VisibleIndex = 27;
            // 
            // colFRNContaCredito
            // 
            this.colFRNContaCredito.FieldName = "FRNContaCredito";
            this.colFRNContaCredito.Name = "colFRNContaCredito";
            this.colFRNContaCredito.Visible = true;
            this.colFRNContaCredito.VisibleIndex = 28;
            // 
            // colFRNAgenciaDgCredito
            // 
            this.colFRNAgenciaDgCredito.FieldName = "FRNAgenciaDgCredito";
            this.colFRNAgenciaDgCredito.Name = "colFRNAgenciaDgCredito";
            this.colFRNAgenciaDgCredito.Visible = true;
            this.colFRNAgenciaDgCredito.VisibleIndex = 29;
            // 
            // colFRNAgenciaCredito
            // 
            this.colFRNAgenciaCredito.FieldName = "FRNAgenciaCredito";
            this.colFRNAgenciaCredito.Name = "colFRNAgenciaCredito";
            this.colFRNAgenciaCredito.Visible = true;
            this.colFRNAgenciaCredito.VisibleIndex = 30;
            // 
            // colFRNBancoCredito
            // 
            this.colFRNBancoCredito.FieldName = "FRNBancoCredito";
            this.colFRNBancoCredito.Name = "colFRNBancoCredito";
            this.colFRNBancoCredito.Visible = true;
            this.colFRNBancoCredito.VisibleIndex = 31;
            // 
            // colFRNContaTipoCredito
            // 
            this.colFRNContaTipoCredito.FieldName = "FRNContaTipoCredito";
            this.colFRNContaTipoCredito.Name = "colFRNContaTipoCredito";
            this.colFRNContaTipoCredito.Visible = true;
            this.colFRNContaTipoCredito.VisibleIndex = 32;
            // 
            // colFRNRetemISS
            // 
            this.colFRNRetemISS.FieldName = "FRNRetemISS";
            this.colFRNRetemISS.Name = "colFRNRetemISS";
            this.colFRNRetemISS.Visible = true;
            this.colFRNRetemISS.VisibleIndex = 33;
            // 
            // colFRNRetemINSSTomador
            // 
            this.colFRNRetemINSSTomador.FieldName = "FRNRetemINSSTomador";
            this.colFRNRetemINSSTomador.Name = "colFRNRetemINSSTomador";
            this.colFRNRetemINSSTomador.Visible = true;
            this.colFRNRetemINSSTomador.VisibleIndex = 34;
            // 
            // colFRNRetemINSSPrestador
            // 
            this.colFRNRetemINSSPrestador.FieldName = "FRNRetemINSSPrestador";
            this.colFRNRetemINSSPrestador.Name = "colFRNRetemINSSPrestador";
            this.colFRNRetemINSSPrestador.Visible = true;
            this.colFRNRetemINSSPrestador.VisibleIndex = 35;
            // 
            // colCIDNome
            // 
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            this.colCIDNome.Visible = true;
            this.colCIDNome.VisibleIndex = 36;
            // 
            // colCIDUf
            // 
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.Visible = true;
            this.colCIDUf.VisibleIndex = 37;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(119, 51);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(233, 51);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 2;
            this.simpleButton2.Text = "simpleButton2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // testeForn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.gridControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "testeForn";
            this.Size = new System.Drawing.Size(982, 652);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFornecedoresCamposBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dFornecedoresCamposBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colFRN;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNNome;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNCep;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFone1;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFone2;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFone3;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNSite;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNDataAtualizacao;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNAtualizacao_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNDataInclusao;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNInclusao_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colFRN_CID;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFone4;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFantasia;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNRegistroUF;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNSimples;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNIE;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNSimplesAliquota;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNSenha;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNAtivoSite;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNContaDgCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNContaCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNAgenciaDgCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNAgenciaCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNBancoCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNContaTipoCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNRetemISS;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNRetemINSSTomador;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNRetemINSSPrestador;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}
