﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesteCHEQUE
{
    public partial class testeForn : CompontesBasicos.ComponenteBase
    {
        public testeForn()
        {
            InitializeComponent();
        }

        private CadastrosProc.Fornecedores.dFornecedoresCampos DS;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DS = new CadastrosProc.Fornecedores.dFornecedoresCampos();
            dFornecedoresCamposBindingSource.DataSource = DS;

            try
            {
                DS.FORNECEDORESTableAdapter.FillByCNPJ(DS.FORNECEDORES, "28.308.581/0001-42");
            }
            catch { }


        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DS = new CadastrosProc.Fornecedores.dFornecedoresCampos();
            dFornecedoresCamposBindingSource.DataSource = DS;
            DS.FORNECEDORESTableAdapter.FillByCNPJ(DS.FORNECEDORES, "01.124.173/0001-84");

            
        }
    }
}
