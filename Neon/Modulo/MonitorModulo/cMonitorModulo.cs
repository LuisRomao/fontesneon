using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

namespace MonitorModulo
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cMonitorModulo : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cMonitorModulo()
        {
            InitializeComponent();
            //antigo: RemotingConfiguration.Configure("MonitorClient.config", false);                      
        }

        private bool CanalConfigurado = false;

        private void ConfiguraCanal()
        {
            if (CanalConfigurado)
                return;
            ChannelServices.RegisterChannel(new System.Runtime.Remoting.Channels.Http.HttpChannel(), false);
            string URIremota = string.Format("http://{0}:8989/statusModulo.rem",radioGroup1.EditValue ?? OutroServidor.Text);                        
            WellKnownClientTypeEntry WKCTE = new WellKnownClientTypeEntry(typeof(moduloH.statusModulo), URIremota);
            RemotingConfiguration.RegisterWellKnownClientType(WKCTE);          
            CanalConfigurado = true;
            radioGroup1.Enabled = false;
        }

        

        

       

        delegate void dAjustaMemo(string txt);

        private dAjustaMemo _AjustaMemo;

        private dAjustaMemo AjustaMemo
        {
            get
            {
                return _AjustaMemo ?? (_AjustaMemo = RegistraMem);
            }
        }

        void RegistraMem(string txt)
        {
            if (!memoEdit1.InvokeRequired)
                memoEdit1.Text = string.Format("{0}\r\n{1}", txt, memoEdit1.Text);
            else                            
                Invoke(AjustaMemo, new object[] { txt });
            
        }

        

        

        

        

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            ChamaStatusModulo();            
        }

        private void ChamaStatusModulo() 
        {
            try
            {
                ConfiguraCanal();
                moduloH.statusModulo statusModuloRetorno = new moduloH.statusModulo();

                memoEdit1.Text = statusModuloRetorno.StatusAplicativo;
                //memoEdit1.Text += string.Format("\r\n**statusModulo**\r\nVers�o:\r\n{0}\r\nErro:{1}\r\n",
                //        statusModuloRetorno.Vesao,
                //        statusModuloRetorno.UltimoErro);
                
            }
            catch (Exception e)
            {
                memoEdit1.Text = string.Format("Erro:{0}\r\n**********\r\n{1}\r\n**********\r\n", e.Message, e.StackTrace);
            }
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            new moduloH.statusModulo().ChamaMenu();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ConfiguraCanal();
            if (texTarefa.Text != "")
                new moduloH.statusModulo().TarefaAplicativo(texTarefa.Text);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            ConfiguraCanal();
            memoEdit1.Text = string.Format("Enviar Impostos: {0}\r\n", new moduloH.statusModulo().EnviarImpostos() ? "Enviado" : "Nada para enviar");
                
        }
    }
}
