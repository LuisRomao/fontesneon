﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using VirDB.Bancovirtual;

namespace CorrecaoHistorico
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
//#if (DEBUG)
            BancoVirtual.Popular("copia SBC 64", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonSBC;Persist Security Info=True;User ID=sa;Password=venus");
            BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
            BancoVirtual.PopularAccesH("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\hist.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
//#endif
            BancoVirtual.Popular("NEON SBC (cuidado)", @"Data Source=NEON20\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Connect Timeout=30;Password=TPC4P2D");

            BancoVirtual.PopularAcces("NEON Access ", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\NEWSCC\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\NEWSCC\neon.mda;Jet OLEDB:Database Password=96333018");
            BancoVirtual.PopularAccesH("NEON AccessH", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\NEWSCC\hist.mdb ;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\NEWSCC\neon.mda;Jet OLEDB:Database Password=96333018");
                            

            Application.Run(new Form1());
        }
    }
}
