﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CorrecaoHistorico
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        
        private DataSet1TableAdapters.det1TableAdapter TA;        
        //private DataSet1.Saldos_e_PoupançaRow rowS;
        private DataSet1.BalanceteRow rowB;

        private void button1_Click(object sender, EventArgs e)
        {                        
            //if (dataSet1.Saldos_e_PoupançaTableAdapter.Fill(dataSet1.Saldos_e_Poupança, textBox1.Text) == 0)
            if (dataSet1.BalanceteTableAdapter.Fill(dataSet1.Balancete, textBox1.Text) == 0)
                MessageBox.Show("Não encontrado");
            //rowS = dataSet1.Saldos_e_Poupança[0];
            rowB = dataSet1.Balancete[0];
            TA = new DataSet1TableAdapters.det1TableAdapter();
            try
            {
                //int Lidos = TA.Fill(dataSet1.det1, textBox1.Text, textBox2.Text);
                int Lidos = TA.FillByQ2(dataSet1.det1, textBox1.Text, "042013");
                textBox3.Text = Lidos.ToString();
            }
            catch { }
            dataSet1.Histórico_LançamentosTableAdapter.Fill(dataSet1.Histórico_Lançamentos,textBox1.Text,rowB.BALDataInicio,rowB.BALdataFim);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            short? iV = dataSet1.Histórico_LançamentosTableAdapter.maximo();
            if (iV.HasValue)
                iV++;
            else
                iV = 1;
            string PLA = "213000";
            foreach (DataSet1.det1Row rowDeb in dataSet1.det1)
            {
                string comando = "";
                if (rowDeb.quadro == 1)
                {
                    comando = "select pla from PLAnocontas where PLADescricao = @P1 AND (PLA LIKE '1%')";
                    PLA = "198000";
                    if (rowDeb.titulo.ToUpper() == "CONDOMINIO.")
                        rowDeb.titulo = "CONDOMINIO";
                    if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(comando, out PLA, rowDeb.titulo))
                        PLA = "198000";
                }
                else
                {
                    if (rowDeb.negrito)
                    {
                        PLA = "213000";
                        comando = "select pla from PLAnocontas where PLADescricao = @P1 AND (PLA LIKE '2%')";
                        if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(comando, out PLA, rowDeb.titulo))
                            PLA = "213000";
                        if (PLA == "204000")
                            PLA = "204001";
                        continue;
                    }
                };
                DataSet1.Histórico_LançamentosRow rownovaH = dataSet1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                rownovaH.Codcon = rowDeb.Codcon;
                rownovaH.Crédito = (rowDeb.quadro == 1);
                rownovaH.Data_de_Lançamento = rowB.BALDataInicio;
                if (rowDeb.titulo == "")
                    rownovaH.Descrição = " ";
                else if (rowDeb.titulo.Length > 60)
                    rownovaH.Descrição = rowDeb.titulo.Substring(0, 60);
                else
                    rownovaH.Descrição = rowDeb.titulo;
                rownovaH.Mês_Competência = "042013";
                rownovaH.Tipopag = PLA;
                rownovaH.Valor_Pago = (float)rowDeb.valor;
                rownovaH.Versão = iV.Value;
                iV++;
                dataSet1.Histórico_Lançamentos.AddHistórico_LançamentosRow(rownovaH);

            }                        
            dataSet1.Histórico_LançamentosTableAdapter.Update(dataSet1.Histórico_Lançamentos);
            MessageBox.Show("ok");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*
            dataSet1.DataTable1TableAdapter.Fill(dataSet1.DataTable1, textBox1.Text, rowB.BALDataInicio, rowB.BALdataFim);
            foreach (DataSet1.DataTable1Row rowB in dataSet1.DataTable1)
            {
                DataSet1.Histórico_LançamentosRow rownovaH = dataSet1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                rownovaH.Codcon = textBox1.Text;
                rownovaH.Crédito = true;
                rownovaH.Data_de_Lançamento = rowB.BOLPagamento;
                if (rowB.PLADescricaoLivre)
                {
                    string manobra = rowB.BODMensagem;
                    if (manobra.Contains(" (Pagamento Parcial)"))
                        manobra = manobra.Replace(" (Pagamento Parcial)", "").Trim();
                    rownovaH.Descrição = manobra;
                }
                else
                    rownovaH.Descrição = rowB.PLADescricao;
                //rownovaH.Descrição = rowB.BODMensagem;
                rownovaH.Mês_Competência = "042013";
                rownovaH.Tipopag = rowB.BOD_PLA;
                rownovaH.Valor_Pago = (float)rowB.BODValor;
                //rownovaH.Apartamento
                //rownovaH.Bloco
                rownovaH.Número_Documento = rowB.BOL;
                dataSet1.Histórico_Lançamentos.AddHistórico_LançamentosRow(rownovaH);
            }
            dataSet1.Histórico_LançamentosTableAdapter.Update(dataSet1.Histórico_Lançamentos);
            MessageBox.Show("ok");*/
        }

        private void button2_Click(object sender, EventArgs e)
        {

            dataSet1.Histórico_LançamentosTableAdapter.DeleteQuery(textBox1.Text, rowB.BALDataInicio, rowB.BALdataFim);
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            DataSet1.Histórico_LançamentosRow row = (DataSet1.Histórico_LançamentosRow)DRV.Row;
            if (row.RowState == DataRowState.Added)
                dataSet1.Histórico_LançamentosTableAdapter.Update(row);
            else
            {
                //string PLAOrig = (string)row["Tipopag", DataRowVersion.Original];
                //string descricaoorig = (string)row["Descrição", DataRowVersion.Original];
                string PLA = (string)row["Tipopag", DataRowVersion.Current];
                string descricao = (string)row["Descrição", DataRowVersion.Current];
                //float ValorOrig = (float)row["Valor Pago", DataRowVersion.Original];
                float Valor = (float)row["Valor Pago", DataRowVersion.Current];
                string comando =
"UPDATE       [Histórico Lançamentos]\r\n" +
"SET                Tipopag =@P1, Descrição =@P2, [Valor Pago] =@P3\r\n" +
"WHERE        \r\n" +
"([Histórico Lançamentos].[Versão] = @P4);";
                dataSet1.Histórico_LançamentosTableAdapter.ExecutarSQLNonQuery(comando,
                                                                               PLA,
                                                                               descricao,
                                                                               Valor,
                                                                               row.Versão);
            }
            row.AcceptChanges();
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DataSet1.Histórico_LançamentosRow row = (DataSet1.Histórico_LançamentosRow)gridView1.GetDataRow(e.RowHandle);
            row.Descrição = " ";
            row.Codcon = textBox1.Text;
            row.Data_de_Lançamento = rowB.BALDataInicio;
            
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            string resultado = "Inicio";
            try
            {
                TA = new DataSet1TableAdapters.det1TableAdapter();
                DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela("select CONCodigo from CONDOMINIOS where CONStatus = 1 and CON_EMP = 1");
                resultado = string.Format("Total:{0}", DT.Rows.Count);
                memoEdit1.Text = resultado;
                Application.DoEvents();
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    labelControl1.Text = string.Format("{0}/{1}", i, DT.Rows.Count);
                    resultado += string.Format("\r\n{0}", DT.Rows[i][0]);
                    textBox1.Text = (string)DT.Rows[i][0];
                    memoEdit1.Text = resultado;
                    Application.DoEvents();
                    if (dataSet1.BalanceteTableAdapter.Fill(dataSet1.Balancete, textBox1.Text) == 0)
                        //if (dataSet1.Saldos_e_PoupançaTableAdapter.Fill(dataSet1.Saldos_e_Poupança, textBox1.Text) == 0)
                        resultado += string.Format(": Não encontrado");
                    else
                    {
                        //rowS = dataSet1.Saldos_e_Poupança[0];                    
                        rowB = dataSet1.Balancete[0];
                        try
                        {
                            int Lidos = dataSet1.Histórico_LançamentosTableAdapter.Fill(dataSet1.Histórico_Lançamentos, textBox1.Text, rowB.BALDataInicio, rowB.BALdataFim);

                            if (Lidos > 0)
                            {
                                resultado += string.Format(": Lidos: {0} - {1} {2}", Lidos, rowB.BALDataInicio, rowB.BALdataFim);
                                textBox3.Text = Lidos.ToString();
                            }
                            else
                            {
                                int Recuperados = recuperar();
                                if (Recuperados > 0)
                                    resultado += string.Format(": Recuperado: {0}", Recuperados);
                                else
                                    resultado += string.Format(": NÃO ENCONTRADO EM BAL");
                            }
                        }
                        catch (Exception ex)
                        {
                            resultado += string.Format(": Erro: {0}", ex.Message);
                        }
                    }
                }
            }
            finally
            {
                memoEdit1.Text = resultado;
                Clipboard.SetText(resultado);
            }
        }

        short? iV = null;

        private int recuperar()
        {
            if (!iV.HasValue)
            {
                iV = dataSet1.Histórico_LançamentosTableAdapter.maximo();
                if (iV.HasValue)
                    iV++;
                else
                    iV = 1;
            }
            string PLA = "213000";
            int Lidos = TA.FillByQ2(dataSet1.det1, textBox1.Text, "042013");
            foreach (DataSet1.det1Row rowDeb in dataSet1.det1)
            {
                string comando = "";
                if (rowDeb.quadro == 1)
                {
                    comando = "select pla from PLAnocontas where PLADescricao = @P1 AND (PLA LIKE '1%')";
                    PLA = "198000";
                    if (rowDeb.titulo.ToUpper() == "CONDOMINIO.")
                        rowDeb.titulo = "CONDOMINIO";
                    if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(comando, out PLA, rowDeb.titulo))
                        PLA = "198000";
                }
                else
                {
                    if (rowDeb.negrito)
                    {
                        PLA = "213000";
                        comando = "select pla from PLAnocontas where PLADescricao = @P1 AND (PLA LIKE '2%')";
                        if (rowDeb.titulo == "Despesas Operacionais")
                            PLA = "240000";
                        else if (!VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(comando, out PLA, rowDeb.titulo))
                            PLA = "213000";
                        if (PLA == "204000")
                            PLA = "204001";
                        continue;
                    }
                };
                DataSet1.Histórico_LançamentosRow rownovaH = dataSet1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                rownovaH.Codcon = rowDeb.Codcon;
                rownovaH.Crédito = (rowDeb.quadro == 1);
                rownovaH.Data_de_Lançamento = rowB.BALDataInicio;
                if (rowDeb.titulo == "")
                    rownovaH.Descrição = " ";
                else if (rowDeb.titulo.Length > 60)
                    rownovaH.Descrição = rowDeb.titulo.Substring(0, 60);
                else
                    rownovaH.Descrição = rowDeb.titulo;
                rownovaH.Mês_Competência = "042013";
                rownovaH.Tipopag = PLA;
                rownovaH.Valor_Pago = (float)rowDeb.valor;
                rownovaH.Versão = iV.Value;
                iV++;
                dataSet1.Histórico_Lançamentos.AddHistórico_LançamentosRow(rownovaH);

            }
            dataSet1.Histórico_LançamentosTableAdapter.Update(dataSet1.Histórico_Lançamentos);
            return Lidos;
        }

    }
}
