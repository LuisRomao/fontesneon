﻿namespace CorrecaoHistorico
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.button4 = new System.Windows.Forms.Button();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new CorrecaoHistorico.DataSet1();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodcon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrédito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDatadeLeitura = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipopag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescrição = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNúmeroDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMêsCompetência = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVersão = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(5, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(175, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "ANIMMA";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(175, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Ler";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(187, 60);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(56, 20);
            this.textBox3.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(284, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(175, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Apagar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(284, 35);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(175, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Gravar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.memoEdit1);
            this.panelControl1.Controls.Add(this.button4);
            this.panelControl1.Controls.Add(this.button1);
            this.panelControl1.Controls.Add(this.textBox1);
            this.panelControl1.Controls.Add(this.button3);
            this.panelControl1.Controls.Add(this.button2);
            this.panelControl1.Controls.Add(this.textBox3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1164, 97);
            this.panelControl1.TabIndex = 7;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(466, 6);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(686, 85);
            this.memoEdit1.TabIndex = 7;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(284, 64);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(175, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "Todos";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Histórico Lançamentos";
            this.gridControl1.DataSource = this.dataSet1BindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 97);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1164, 501);
            this.gridControl1.TabIndex = 8;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dataSet1BindingSource
            // 
            this.dataSet1BindingSource.DataSource = this.dataSet1;
            this.dataSet1BindingSource.Position = 0;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodcon,
            this.colValorPago,
            this.colCrédito,
            this.colDatadeLeitura,
            this.colTipopag,
            this.colDescrição,
            this.colNúmeroDocumento,
            this.colMêsCompetência,
            this.colVersão,
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor Pago", this.colValorPago, "{0:n2}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colCodcon
            // 
            this.colCodcon.FieldName = "Codcon";
            this.colCodcon.Name = "colCodcon";
            // 
            // colValorPago
            // 
            this.colValorPago.FieldName = "Valor Pago";
            this.colValorPago.Name = "colValorPago";
            this.colValorPago.Visible = true;
            this.colValorPago.VisibleIndex = 1;
            this.colValorPago.Width = 170;
            // 
            // colCrédito
            // 
            this.colCrédito.FieldName = "Crédito";
            this.colCrédito.Name = "colCrédito";
            this.colCrédito.Visible = true;
            this.colCrédito.VisibleIndex = 2;
            this.colCrédito.Width = 170;
            // 
            // colDatadeLeitura
            // 
            this.colDatadeLeitura.FieldName = "Data de Leitura";
            this.colDatadeLeitura.Name = "colDatadeLeitura";
            // 
            // colTipopag
            // 
            this.colTipopag.FieldName = "Tipopag";
            this.colTipopag.Name = "colTipopag";
            this.colTipopag.Visible = true;
            this.colTipopag.VisibleIndex = 3;
            this.colTipopag.Width = 170;
            // 
            // colDescrição
            // 
            this.colDescrição.FieldName = "Descrição";
            this.colDescrição.Name = "colDescrição";
            this.colDescrição.Visible = true;
            this.colDescrição.VisibleIndex = 4;
            this.colDescrição.Width = 170;
            // 
            // colNúmeroDocumento
            // 
            this.colNúmeroDocumento.FieldName = "Número Documento";
            this.colNúmeroDocumento.Name = "colNúmeroDocumento";
            this.colNúmeroDocumento.Width = 170;
            // 
            // colMêsCompetência
            // 
            this.colMêsCompetência.FieldName = "Mês Competência";
            this.colMêsCompetência.Name = "colMêsCompetência";
            this.colMêsCompetência.Visible = true;
            this.colMêsCompetência.VisibleIndex = 5;
            this.colMêsCompetência.Width = 186;
            // 
            // colVersão
            // 
            this.colVersão.FieldName = "Versão";
            this.colVersão.Name = "colVersão";
            this.colVersão.OptionsColumn.ReadOnly = true;
            this.colVersão.Visible = true;
            this.colVersão.VisibleIndex = 0;
            this.colVersão.Width = 58;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ok";
            this.gridColumn1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn1.Name = "gridColumn1";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 35);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 13);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "labelControl1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 598);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dataSet1BindingSource;
        private DataSet1 dataSet1;
        private DevExpress.XtraGrid.Columns.GridColumn colCodcon;
        private DevExpress.XtraGrid.Columns.GridColumn colValorPago;
        private DevExpress.XtraGrid.Columns.GridColumn colCrédito;
        private DevExpress.XtraGrid.Columns.GridColumn colDatadeLeitura;
        private DevExpress.XtraGrid.Columns.GridColumn colTipopag;
        private DevExpress.XtraGrid.Columns.GridColumn colDescrição;
        private DevExpress.XtraGrid.Columns.GridColumn colNúmeroDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn colMêsCompetência;
        private DevExpress.XtraGrid.Columns.GridColumn colVersão;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.Button button4;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}

