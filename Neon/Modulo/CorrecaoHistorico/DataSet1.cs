﻿namespace CorrecaoHistorico {
    
    
    public partial class DataSet1 {

        private DataSet1TableAdapters.BalanceteTableAdapter balanceteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Balancete
        /// </summary>
        public DataSet1TableAdapters.BalanceteTableAdapter BalanceteTableAdapter
        {
            get
            {
                if (balanceteTableAdapter == null)
                {
                    balanceteTableAdapter = new DataSet1TableAdapters.BalanceteTableAdapter();
                    balanceteTableAdapter.TrocarStringDeConexao();
                };
                return balanceteTableAdapter;
            }
        }

        private DataSet1TableAdapters.Histórico_LançamentosTableAdapter histórico_LançamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Histórico_Lançamentos
        /// </summary>
        public DataSet1TableAdapters.Histórico_LançamentosTableAdapter Histórico_LançamentosTableAdapter
        {
            get
            {
                if (histórico_LançamentosTableAdapter == null)
                {
                    histórico_LançamentosTableAdapter = new DataSet1TableAdapters.Histórico_LançamentosTableAdapter();
                    histórico_LançamentosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                };
                return histórico_LançamentosTableAdapter;
            }
        }

        private DataSet1TableAdapters.Saldos_e_PoupançaTableAdapter saldos_e_PoupançaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Saldos_e_Poupança
        /// </summary>
        public DataSet1TableAdapters.Saldos_e_PoupançaTableAdapter Saldos_e_PoupançaTableAdapter
        {
            get
            {
                if (saldos_e_PoupançaTableAdapter == null)
                {
                    saldos_e_PoupançaTableAdapter = new DataSet1TableAdapters.Saldos_e_PoupançaTableAdapter();
                    saldos_e_PoupançaTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                };
                return saldos_e_PoupançaTableAdapter;
            }
        }

        private DataSet1TableAdapters.DataTable1TableAdapter dataTable1TableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DataTable1
        /// </summary>
        public DataSet1TableAdapters.DataTable1TableAdapter DataTable1TableAdapter
        {
            get
            {
                if (dataTable1TableAdapter == null)
                {
                    dataTable1TableAdapter = new DataSet1TableAdapters.DataTable1TableAdapter();
                    dataTable1TableAdapter.TrocarStringDeConexao();
                };
                return dataTable1TableAdapter;
            }
        }
    }
}

namespace CorrecaoHistorico.DataSet1TableAdapters {
    
    
    public partial class Saldos_e_PoupançaTableAdapter {
    }
}
