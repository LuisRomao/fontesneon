﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace finnetfast
{
    public partial class Form1 : Form
    {
        private const string entrada = @"C:\FontesVS\neon\Modulo\finnetfast\bin\Release\431191108CON\entrada\Retorno.txt";

        public Form1()
        {
            InitializeComponent();
            string Conteudo = string.Format("Abertura: {0}\r\nComando:{1}\r\n", DateTime.Now, Application.ExecutablePath);
            System.IO.File.AppendAllText(entrada, Conteudo);            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            System.IO.File.AppendAllText(entrada, string.Format("Término: {0}\r\n", DateTime.Now));
            Close();
        }
    }
}
