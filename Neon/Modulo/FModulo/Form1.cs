using System;
using System.Windows.Forms;
using dllModulo;
using VirDB.Bancovirtual;

namespace FModulo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private Controlador _Controlador;

        private Controlador Controlador
        {
            get
            {
                if (_Controlador == null)
                    _Controlador = new Controlador();
                return _Controlador;
            }
        }

        
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            memoEdit1.Text += string.Format("Ligar...\r\n");
            Application.DoEvents();
            Controlador.Liga(true);
            memoEdit1.Text += string.Format("Ligado");
            Application.DoEvents();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Controlador.Desliga();            
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (Controlador.ProxyVirWeb != null)
            {
                memoEdit1.Text = string.Format("Status Proxy\r\n----------------\r\n{0}\r\n----------------\r\n", Controlador.ProxyVirWeb.StatusProxy());
                if (Controlador.strRetorno != null)
                    memoEdit1.Text += string.Format("\r\n{0}", Controlador.strRetorno.ToString());
            }
            else
            {
                memoEdit1.Text = string.Format("Ultimo Erro:{0}", Controlador.UltimoErro ?? "-ok-");

                memoEdit1.Text += string.Format("\r\nstatusModulo:\r\n  Vers�o:{0}\r\nErro:{1}",
                        moduloH.statusModulo.VesaoSt,
                        moduloH.statusModulo.UltimoSt);
            }
            if (Controlador.UltimoErro != null)
                memoEdit1.Text += string.Format("\r\n{0}", Controlador.UltimoErro);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Controlador.Desliga();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            BancoVirtual.Popular("QAS - SBC", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONH;User ID=NEONT;Password=TPC4P2DT;Connect Timeout=30");
            BancoVirtual.PopularFilial("QAS - SBC", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSAH;User ID=NEONT;Password=TPC4P2DT;Connect Timeout=30");
            //BancoVirtual.Popular("NEON SBC REAL", @"Data Source=cp.neonimoveis.com;Initial Catalog=NEON;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");            
            //BancoVirtual.PopularAcces("NEON Access ", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\newscc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\newscc\neon.mda;Jet OLEDB:Database Password=96333018");
            //BancoVirtual.PopularAccesH("NEON AccessH", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\newscc\hist.mdb ;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\newscc\neon.mda;Jet OLEDB:Database Password=96333018");
            //BancoVirtual.PopularInternet("INTERNET REAL1", @"Data Source=cp.neonimoveis.com;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 1);
            //BancoVirtual.PopularInternet("INTERNET REAL2", @"Data Source=cp.neonimoveis.com;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 2);
            //BancoVirtual.PopularFilial("NEON SA REAL", @"Data Source=cp.neonimoveis.com;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            //memoEdit1.Text += Controlador.TratarArquivosRetorno("d:\\gpssa", dllModulo.Controlador.TipoRetono.ROBO_BRADESCO);


            memoEdit1.Text += string.Format("* 25 * -RETORNO TIVIT CAIXA\r\n{0}", Controlador.TratarArquivosRetorno(@"C:\CAIXA\COBRANCA\", Controlador.TipoRetono.TIVIT));
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            
            BancoVirtual.Popular("QAS - SBC", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONH;User ID=NEONT;Password=TPC4P2DT;Connect Timeout=30");
            BancoVirtual.PopularFilial("QAS - SBC", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSAH;User ID=NEONT;Password=TPC4P2DT;Connect Timeout=30");
            memoEdit1.Text += new dllbanco.TributoEletronico().ProcessarDados();
        }

    }
}