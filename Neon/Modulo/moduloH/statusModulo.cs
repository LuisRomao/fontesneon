using System;
using System.Diagnostics;

namespace moduloH
{
    /// <summary>
    /// Status do m�dulo
    /// </summary>
    public class statusModulo : MarshalByRefObject
    {               
        
        /// <summary>
        /// 
        /// </summary>
        public static string VesaoSt = "";
        /// <summary>
        /// 
        /// </summary>
        public static string UltimoSt = "";
        /// <summary>
        /// 
        /// </summary>
        public static EventHandler TrocaModulo;

        /// <summary>
        /// delegate para callback 
        /// </summary>
        /// <returns></returns>
        public delegate string delegateCallBack();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tarefa"></param>
        public delegate void delegateCallBackTarefa(string Tarefa);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public delegate bool delegateEnviarImpostos();

        /// <summary>
        /// Callback (registrar um m�dodo a ser chamado
        /// </summary>
        public static delegateCallBack StatusAplicativoSt;

        /// <summary>
        /// 
        /// </summary>
        public static delegateEnviarImpostos EnviarImpostosSt;

        /// <summary>
        /// CallBack para chamar tarefa
        /// </summary>
        public static delegateCallBackTarefa CallBackTarefaAplicativoSt;

        /// <summary>
        /// Vers�o
        /// </summary>
        public string Vesao 
        {
            get 
            {
                return VesaoSt;
            }
            set 
            {
                VesaoSt = value;
            }
        }

        /// <summary>
        /// �ltimo Erro
        /// </summary>
        public string UltimoErro {
            get { return UltimoSt; }
            set
            {
            	UltimoSt = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ChamaMenu() 
        {
            if(TrocaModulo !=null)
                    TrocaModulo(this, new EventArgs());            
        }       

        /// <summary>
        /// 
        /// </summary>
        public string StatusAplicativo
        {
            get
            {
                return (StatusAplicativoSt == null) ? "-" : StatusAplicativoSt();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool EnviarImpostos()
        {
            return (EnviarImpostosSt == null) ? false : EnviarImpostosSt();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tarefa"></param>
        public void TarefaAplicativo(string Tarefa)
        {
            if (CallBackTarefaAplicativoSt != null)
                CallBackTarefaAplicativoSt(Tarefa);
            
        }
    }
}
