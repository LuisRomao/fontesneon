using System;
using System.ServiceProcess;
using dllModulo;

namespace SModulo
{
    /// <summary>
    /// 
    /// </summary>
    public partial class smodulo : ServiceBase
    {
        /// <summary>
        /// 
        /// </summary>
        public smodulo()
        {
            InitializeComponent();
        }


        private Controlador _Controlador;

        private Controlador Controlador
        {
            get
            {
                if (_Controlador == null)
                    _Controlador = new Controlador();
                return _Controlador;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            Controlador.Liga(true);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnStop()
        {
            Controlador.Desliga();
        }
       
    }
}
