/*
MR - 20/05/2014 13:00 - 13.2.9.3   - Processa retorno de pagamento eletr�nico pendente (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (20/05/2014 13:00) ***)
MR - 28/05/2014 12:30 - 13.2.9.8   - Publica��o de regras dos Equipamento (Altera��es indicadas por *** MRC - INICIO - RESERVA (28/05/2014 12:30) ***)
MR - 01/07/2014 19:00 - 13.2.9.22  - Redirecionamento de comprovantes para \\email\volume_1\publicacoes em subpastas de acordo com o tipo (Altera��es indicadas por *** MRC - INICIO (01/07/2014 19:00) ***)
MR - 12/08/2014 10:00 - 14.1.4.22  - Quebra de comprovantes de pagamento eletr�nico em arquivos indiviuais (Altera��es indicadas por *** MRC - INICIO (12/08/2014 10:00) ***)
MR - 12/11/2014 10:00 -            - Processa remessa e retorno de pagamento de tributos eletronico (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
LH - 24/01/2015       - 14.2.4.4   - Pega o destinat�rio dos e-mail do esta��es.xml
MR - 10/02/2015 12:00 -            - Tratamento de comprovante recebido pela NEXXERA, de pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO (10/02/2015 12:00) ***)
MR - 20/02/2015 20:00 -            - Chamada NEXXERA, intercalando com Chamada FINNET (Altera��es indicadas por *** MRC - INICIO (20/02/2015 20:00) ***)
MR - 10/03/2015 14:00 -            - Chamada ROBO BRADESCO por presen�a de arquivo de configura��o, indicando "N�o configurado" caso n�o exista, para SA e SB (Altera��es indicadas por *** MRC - INICIO - ROBO (10/03/2015 14:00) ***)
MR - 12/03/2015 12:00 -            - Chamada ROBO BRADESCO com tratamento para PRODUCAO/DESENVOLVIMENTO (Altera��es indicadas por *** MRC - INICIO - ROBO (12/03/2015 12:00) ***)
MR - 18/03/2015 10:00              - Alterado o nome da funcao ReGrasEquipamento para PublicaReGrasEquipamento para manter padr�o de nomenclatura (Altera��es indicadas por *** MRC - INICIO - RESERVA (18/03/2015 10:00) ***)
MR - 27/03/2015 09:30              - Chamada FINNET, NEXXERA por presen�a de arquivo de configura��o, em nova funcao ChamaVAN, indicando "N�o configurado" caso n�o exista, para SA e SB (Altera��es indicadas por *** MRC - INICIO (27/03/2015 09:30) ***)
                                     Tratamento dos retornos FINNET e NEXXERA com mesma fun��o mas com possibilidade de diret�rios diferentes
MR - 23/07/2015 11:00              - Configurado o diret�rio do ROBO para SA (Altera��es indicadas por *** MRC - INICIO (23/07/2015 11:00) ***)
MR - 18/08/2015 10:00              - Tratamento de comprovante recebido pela NEXXERA com nova nomenclatura (considera a nova e a anterior), de pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO (18/08/2015 10:00) ***)
LH - 18/09/2015 11:40 - 14.2.4.131 - Retorno via rob�
MR - 21/03/2016 20:00              - Verifica retorno da chamada de VAN assincrona antes de gerar remessa de pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
MR - 21/03/2016 20:00 -            - Verifica retorno da chamada de VAN assincrona antes de chamar outra van e antes de gerar remessa de pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
                                   - Registro de boletos separado para Bradesco e Itau 
                                   - Configurado o diret�rio ITAU (gera��o de arquivos automatica na rede, para envio e recebimento manual) 
MR - 01/04/2016 19:00 -            - Seta variavel de controle de retorno de VAN apenas se a mesma for iniciada (Altera��es indicadas por *** MRC - INICIO (01/04/2016 19:00) ***)
MR - 28/04/2016 12:00 -            - Seta variavel de controle de retorno de VAN apenas se a mesma for iniciada (Altera��es indicadas por *** MRC - INICIO (28/04/2016 12:00) ***)
                                   - NEXXERA como primeira van a ser chamada e como caminho default para pagto eletr�nico
MR - 27/05/2016 12:00 -            - Chamada da nova fun��o VerificaReservasSite que recolhe as reservas e cancelamentos do site (anteriormente feito em ReservaEQuipamento) antes de gerar novas grades para evitar duplicadade de grades alteradas que j� est�o reservadas (Altera��es indicadas por *** MRC - INICIO (27/05/2016 12:00) ***)
									 Concentra��o de todas as atividades referentes a Reserva logo no in�cio do processo
									 Chama uma vez a fun�ao APAGAR logo ap�s processar as reservas para que datas duplicadas por altera��o de horario seja apagadas logo (evitando reservas indevidas)
MR - 19/07/2016 15:00 -            - Chamada da NEXXERA intercala com a gera��o de arquivos e � chamada no final do processo, para tentar contornar problema de arquivos que somem durante a c�pia (Altera��es indicadas por *** MRC - INICIO (19/07/2016 15:00) ***)
                                     Retirada do tratamento FINNET (envio e retorno) 
MR - 08/08/2016 11:00 -            - Confer�ncia do envio de arquivos NEXXERA (intercalado com a sua chamada) e c�pia dos n�o enviados para a pasta de sa�da (Altera��es indicadas por *** MRC - INICIO (08/08/2016 15:00) ***)
*/

using System;
using System.Threading;
using System.Diagnostics;
using moduloH;
using System.IO;
using CompontesBasicos;
using VirDB.Bancovirtual;
using Internet;
using dllbanco;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.Runtime.InteropServices;
using VirExcepitionNeon;
using VirEnumeracoes;


namespace dllModulo
{
    /// <summary>
    /// Controlador
    /// </summary>
    public class Controlador : IDisposable
    {
        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (ProxyVirWeb != null)
            {
                ProxyVirWeb.Dispose();
                ProxyVirWeb = null;
            }
            if (TimerAutomatico != null)
            {
                TimerAutomatico.Dispose();
                TimerAutomatico = null;
            }
        }

        //*** MRC - INICIO (27/03/2015 09:30) ***
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString")]
        private static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);
        //*** MRC - TERMINO (27/03/2015 09:30) ***

        private DateTime HAtivacao;        
        
        /// <summary>
        /// Ultimo erro
        /// </summary>
        public string UltimoErro = "";

        private string _CaminhoLocal;
        private string CaminhoLocal
        {
            get
            {
                if (_CaminhoLocal == null)
                {
                    _CaminhoLocal = System.IO.Path.GetDirectoryName(Environment.CommandLine.Replace("\"", ""));
                    if (!Directory.Exists(_CaminhoLocal + @"\TMP"))
                        Directory.CreateDirectory(_CaminhoLocal + @"\TMP");
                    if (!Directory.Exists(_CaminhoLocal + @"\log"))
                        Directory.CreateDirectory(_CaminhoLocal + @"\log");
                };
                return _CaminhoLocal;
            }
        }

        /// <summary>
        /// Componente de mensagens com o site virweb
        /// </summary>
        public proxyvirweb.cVirtualProxy ProxyVirWeb;        

        private int EMP;

        private Timer TimerAutomatico;        

        enum TipoRetorno 
        {
            nao_retorna,
            mensagem,
            email
        }

        private DateTime UltimaTarefaDiaria;        
        private TipoRetorno retornodiario;
        private TipoRetorno retornochamada;
        private bool RelogioAutomatico = true;
        private int HoraTarefaDiaria;
        private int MinutoTarefaDiaria;
        
        private static string ReportaErro(Exception e,string Chamador)
        {
            string strErro = string.Format("Erro ({0})\r\n*************:",Chamador);
            while (e != null)
            {
                strErro += string.Format("{0}\r\n{1}\r\n", e.Message, e.StackTrace);
                e = e.InnerException;
            };
            return strErro + "\r\n*************";
        }

        private NeonNet _NeonNet;
        private NeonNet NeonNet
        {
            get
            {
                return _NeonNet ?? (_NeonNet = new NeonNet((EMP != 0) && (EMP != 4), EMP));
            }
        }
       
        private Robo.Bradesco _RoboBradesco;
        private Robo.Bradesco RoboBradesco
        {
            get
            {
                return _RoboBradesco ?? (_RoboBradesco = new Robo.Bradesco((EMP != 0) && (EMP != 4), EMP));
            }
        }                
        
        private enum TipoVan
        {
            /// <summary>
            /// Finnet
            /// </summary>
            FINNET = 1,
            /// <summary>
            /// NEXXERA
            /// </summary>
            NEXXERA = 2,
        }

        //*** MRC - INICIO (19/07/2016 15:00) ***
        //A chamada das WANs ocorre de forma assincrona ent�o n�o devemos tentar ler arquivos ou enviar durante a chamada
        private bool BloqueadoAguardaRetonoWAN = false;
        //Geracao de arquivos passa a intercalar com a chamada das WANs (caso o evento de retorno da chamada da WAN nao funcione) para tentar evitar de criar arquivos durante a chamada
        private bool par = false;
        //*** MRC - TERMINO (19/07/2016 15:00) ***

        private DateTime UltimaLimpesa;        
        private bool Rechamada = false;

        private string ApagarLog()
        {
            string retorno;
            if (DateTime.Today > UltimaLimpesa)
            {
                UltimaLimpesa = DateTime.Today;
                string[] ArquivosLimpar = Directory.GetFiles(CaminhoLocal + "\\log", "Tarefa_*.log");
                retorno = string.Format("{0} Limpesa:{1}\r\n", DateTime.Now, ArquivosLimpar.Length);
                foreach (string Arquivo in ArquivosLimpar)
                {
                    string NomeArquivo = System.IO.Path.GetFileName(Arquivo);
                    if (NomeArquivo.Length != 28)
                    {
                        retorno += string.Format("     Pulado:{0}\r\n", NomeArquivo);
                        continue;
                    }
                    try
                    {
                        DateTime DataHora = new DateTime(int.Parse(NomeArquivo.Substring(7, 4)), int.Parse(NomeArquivo.Substring(11, 2)), int.Parse(NomeArquivo.Substring(13, 2)));
                        if (DataHora.AddDays(15) < DateTime.Now)
                        {
                            File.Delete(Arquivo);
                            retorno += string.Format("     Log antigo apagado:{0}\r\n", NomeArquivo);
                        }
                        //else
                        //  retorno += string.Format("     Na validade:{0}\r\n", NomeArquivo);
                    }
                    catch (Exception e)
                    {
                        retorno += VirExceptionNeon.RelatorioDeErro(e, true, true, false);
                    }
                };
            }
            else
                retorno = string.Format("{0} Limpesa n�o efetuada\r\n", DateTime.Now);
            return retorno;
        }

        /// <summary>
        /// Relat�rio
        /// </summary>
        public StringBuilder strRetorno;

        private void AddRetorno(string Titulo, string conteudo = "",bool ChecarCancel = true)
        {
            strRetorno.AppendFormat("{0} - {1}\r\n", DateTime.Now,Titulo);
            if (conteudo != "")
            {
                string[] linhas = conteudo.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string linha in linhas)
                    strRetorno.AppendFormat("\t|{0}\r\n", linha);
            }
            if (ChecarCancel && Cancelar)
            {
                Cancelar = false;
                throw new Exception("Cancelado");
            }
        }

        private void retornarRelatorio(TipoRetorno tipoRetorno)
        {
            string PrimeiraLinha = string.Format("Termino:{0}\r\n", DateTime.Now);
            if (Rechamada)
            {
                if (NeonNet == null)
                    PrimeiraLinha = "PARCIAL\r\n";
                else
                    PrimeiraLinha = string.Format("PARCIAL {0}\r\n", NeonNet.Local());
            }
            File.WriteAllText(String.Format(@"{0}\log\Tarefa_{1:yyyyMMdd_HH_mm_ss}.log", CaminhoLocal, DateTime.Now), PrimeiraLinha + strRetorno);
            switch (tipoRetorno)
            {
                case TipoRetorno.mensagem:
                    ProxyVirWeb.RetornarCentral(strRetorno.ToString());
                    break;
                case TipoRetorno.email:
                    try
                    {
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;neon@vcommerce.com.br", PrimeiraLinha + strRetorno, "Chamada");
                    }
                    catch (Exception e)
                    {
                        File.WriteAllText(String.Format(@"{0}\log\Tarefa_{1:yyyyMMdd_HH_mm_ss}.log", CaminhoLocal, DateTime.Now), ReportaErro(e,"Erro ao enviar e-mail"));
                    }
                    break;
                case TipoRetorno.nao_retorna:
                default:
                    break;
            };
        }

        private bool RoboDesligado = false;

        private void Chamada(object oInformacaoDeEstado)
        {
            TipoRetorno tipoRetorno = retornochamada;
            if ((oInformacaoDeEstado != null) && (oInformacaoDeEstado is TipoRetorno))
                tipoRetorno = (TipoRetorno)oInformacaoDeEstado;
            try
            {
                if (Cancelar)
                {
                    strRetorno = new StringBuilder();
                    AddRetorno("===--> CANCELADO");
                }
                else if (Rechamada)
                    AddRetorno("===--> RECHAMADA");
                else
                {
                    Rechamada = true;
                    strRetorno = new StringBuilder();
                    AddRetorno("* 01 * -IN�CIO", VirExceptionNeon.CabecalhoRetorno());
                    FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST = null;
                    //if (EMP == 1)
                    //    AddRetorno("20-TRIBUTO ELETRONICO FOLHA", new EDIBoletosNeon.Arquivo().RemessaTributo(10000, CaminhoRoboBradescoSaida, dllImpostos.OrigemImposto.Folha));
                    AddRetorno("* 02 * -APAGAR LOG", ApagarLog());
                    /*
                    if (EMP == 1)
                    {
                        AddRetorno("20-TRIBUTO ELETRONICO FOLHA", new EDIBoletosNeon.Arquivo().RemessaTributo(10000, CaminhoRoboBradescoSaida, dllImpostos.OrigemImposto.Folha));
                        AddRetorno("21-TRIBUTO ELETRONICO NOTA", new EDIBoletosNeon.Arquivo().RemessaTributo(10000, CaminhoRoboBradescoSaida, dllImpostos.OrigemImposto.Nota));
                    }
                    AddRetorno("22-ROBO BRADESCO", RoboBradesco.Processa(CaminhoRoboBradesco, CaminhoRoboBradescoEntrada, CaminhoRoboBradescoSaida));
                    AddRetorno("23-RETORNO ROBO BRADESCO", TratarArquivosRetorno(CaminhoRoboBradescoEntrada, TipoRetono.ROBO_BRADESCO));
                    */

                    //*** MRC - INICIO (19/07/2016 15:00) ***
                    //if (!BloqueadoAguardaRetonoWAN)
                    //{
                    //    TipoVan TipoW = par ? TipoVan.NEXXERA : TipoVan.FINNET;
                    //    AddRetorno(string.Format("3-CHAMADA VAN ({0})", TipoW), ChamaVAN(TipoW));
                    //    par = !par;
                    //}
                    //*** MRC - TERMINO (19/07/2016 15:00) ***

                    AddRetorno("*** TAREFA CURTA ***");
                    AddRetorno("* -15 * -REGISTRO BRADESCO", NeonNet.PublicaRegistroBoletos("-15-REGISTRO BRADESCO", CaminhoRoboBradescoSaida, 1000, 237));
                    AddRetorno("* -16 * -REGISTRO ITAU", NeonNet.PublicaRegistroBoletos("-16-REGISTRO ITAU", CaminhoItauSaida, 1000, 341));
                    AddRetorno("* -17 * -REGISTRO CAIXA", NeonNet.PublicaRegistroBoletos("-17-REGISTRO CAIXA", CaminhoCaixaSaida, 1000, 104));
                    AddRetorno("* -18 * -REGISTRO SANTANDER", NeonNet.PublicaRegistroBoletos("-18-REGISTRO SANTANDER", CaminhoSanSaida, 1000, 33));
                    AddRetorno("* 03 * -RESERVAS SITE", NeonNet.VerificaReservasSite());
                    AddRetorno("* 04 * -GERA GRADE RESERVAS", NeonNet.GeraReserva());
                    AddRetorno("* 05 * -Publica EQUIPAMENTOS", NeonNet.PublicaEQuipamentos());
                    AddRetorno("* 06 * -Publica REGRAS EQUIPAMENTO", NeonNet.PublicaReGrasEquipamento());
                    AddRetorno("* 07 * -RESERVAS", NeonNet.ReservaEQuipamento());
                    AddRetorno("* 08 * -APAGAR", NeonNet.Apagar());
                    AddRetorno("* 09 * -CONDOM�NIOS", NeonNet.PublicaCondominios());
                    try
                    {
                        AddRetorno("* 10 * -APARTAMENTOS", NeonNet.PublicaApartamentos(EMP));
                    }
                    catch
                    {
                        AddRetorno("ERRO");
                    }                    
                    AddRetorno("* 11 * -BLOCOS", NeonNet.PublicaBlocos());
                    AddRetorno("* 12 * -rec ACORDOS", NeonNet.RecebAcordos());
                    AddRetorno("* 13 * -REGISTRO BRADESCO", NeonNet.PublicaRegistroBoletos("15-REGISTRO BRADESCO", CaminhoRoboBradescoSaida, 1000, 237));
                    AddRetorno("* 14 * -REGISTRO ITAU", NeonNet.PublicaRegistroBoletos("16-REGISTRO ITAU", CaminhoItauSaida, 1000, 341));
                    AddRetorno("* 15 * -REGISTRO CAIXA", NeonNet.PublicaRegistroBoletos("17-REGISTRO CAIXA", CaminhoCaixaSaida, 1000, 104));
                    AddRetorno("* 16 * -REGISTRO SANTANDER", NeonNet.PublicaRegistroBoletos("18-REGISTRO SANTANDER", CaminhoSanSaida, 1000, 33));
                    if (!BloqueadoAguardaRetonoWAN)
                        //ATEN��O o CaminhoNexxeraSaida est� usando o default o uso do CONRemessa_TEA est� desativado
                        AddRetorno("* 17 * -PAGAMENTO ELETRONICO", NeonNet.EmiteChequeEletronico("19-PAGAMENTO ELETRONICO",10000, CaminhoNexxeraSaida));
                    if (EMP == 1)
                    {
                        AddRetorno("* 18 * -TRIBUTO ELETRONICO FOLHA",NeonNet.RemessaTributo("20-TRIBUTO ELETRONICO FOLHA", 10000, CaminhoRoboBradescoSaida, OrigemImposto.Folha));
                        AddRetorno("* 19 * -TRIBUTO ELETRONICO NOTA", NeonNet.RemessaTributo("21-TRIBUTO ELETRONICO NOTA", 10000, CaminhoRoboBradescoSaida, OrigemImposto.Nota));
                    }
                    if (RoboDesligado)
                        AddRetorno("* 20 * -ROBO BRADESCO : DESLIGADO");
                    else
                        AddRetorno("* 21 * -ROBO BRADESCO", RoboBradesco.Processa(CaminhoRoboBradesco, CaminhoRoboBradescoEntrada, CaminhoRoboBradescoSaida));
                    AddRetorno("* 22 * -RETORNO ROBO BRADESCO", TratarArquivosRetorno(CaminhoRoboBradescoEntrada, TipoRetono.ROBO_BRADESCO));
                    if (!BloqueadoAguardaRetonoWAN)
                        AddRetorno("* 24 * -RETORNO NEXXERA", TratarArquivosRetorno(CaminhoNexxeraEntrada, TipoRetono.NEXXERA));
                    AddRetorno("* 23 * -RETORNO TIVIT CAIXA", TratarArquivosRetorno(CaminhoCaixaEntrada, TipoRetono.TIVIT));
                    AddRetorno("* 24 * -TMP -> SCC", NeonNet.CarregaTMP_para_SCC());
                    AddRetorno("* 25 * -APAGAR", NeonNet.Apagar());
                    /*
                    try
                    {
                        AddRetorno("* 28 * -COBRAN�A", NeonNet.CobrancaVelha());
                    }
                    catch (Exception ex)
                    {
                        AddRetorno("ERRO n�o tratado na cobran�a (28)");
                        AddRetorno(VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(ex, false, false, false));
                    }*/

                    AddRetorno("* 26 * -TRIBUTO ELETRONICO - RETORNO", new TributoEletronico().ProcessarDados());
                    if (!BloqueadoAguardaRetonoWAN)
                    {
                        AddRetorno(string.Format("* 27 * -CONFERE ENVIO VAN ({0})", TipoVan.NEXXERA), ConfereEnvioVAN(TipoVan.NEXXERA));
                        AddRetorno(string.Format("* 28 * -CHAMADA VAN ({0})", TipoVan.NEXXERA), ChamaVAN(TipoVan.NEXXERA));                                                    
                    }
                    AddRetorno("* 29 * -PAGAMENTO ELETRONICO - RETORNO", new PagamentoEletronico().ProcessarDados());
                    par = !par;
                    if (UltimaTarefaDiaria < DateTime.Now)
                    {
                        AddRetorno("*** TAREFA LONGA ***");
                        AddRetorno("* 30 * -INDICES", NeonNet.PublicaIndices());
                        Conciliacao.ConciliacaoSt.assistido = false;
                        AddRetorno("* 31 * -BOLETOS CAIXA", Conciliacao.ConciliacaoSt.AmarraBoletosCaixa());
                        AddRetorno("* 32 * -Francesinha", NeonNet.ProcessaFrancesinha(Francesinha.TipoRetorno.resumo));
                        AddRetorno("* 33 * -COBRAN�A", NeonNet.CobrancaNova());
                        int total = Conciliacao.ConciliacaoSt.CarregaPendentes();
                        Conciliacao.ConciliacaoSt.Consolidar(60, true);
                        AddRetorno("* 34 * -CONCILIA��O", string.Format("Pendentes: {5}\r\n Baixados: {0:n0}\r\n Antigos: {1:n0}\r\n Inativos {2:n0}\r\n Aguardo: {3:0}\r\n Cheques: {4}\r\n Descricao: {6}\r\n",
                                                                Conciliacao.ConciliacaoSt.baixados,
                                                                Conciliacao.ConciliacaoSt.antigos,
                                                                Conciliacao.ConciliacaoSt.inativo,
                                                                Conciliacao.ConciliacaoSt.aguardo,
                                                                Conciliacao.ConciliacaoSt.chequesNao,
                                                                total,
                                                                Conciliacao.ConciliacaoSt.descricaonaoencontrada));
                        AddRetorno("* 35 * -COMPLEMENTO", Conciliacao.ConciliacaoSt.Retorno);
                        //AddRetorno("* 38 * -VERIFICA PERIODICOS", "DESLIGADO");
                        AddRetorno("* 36 * -VERIFICA PERIODICOS", NeonNet.RevisaPeriodicos_WS());
                        AddRetorno("* 37 * -D�BITO AUTOM�TICO");
                        Conciliacao.ConciliacaoSt.VerificarDebitoAutomaticoPendente();                        
                        AddRetorno("Fim tarefa longa");
                        AjustaProximaTarefaLonga();
                    }
                    else
                        AddRetorno("TAREFA LONGA PULADA", string.Format("Pr�xima: {0:dd/MM/yyyy HH:mm:ss}\r\n", UltimaTarefaDiaria));
                    Rechamada = false;
                }
            }
            catch (Exception eg)
            {
                AddRetorno("Erro n�o tratado", VirExceptionNeon.RelatorioDeErro(eg, true, true, false), false);
                Rechamada = false;
            }
            finally
            {
                AddRetorno("Fim");
                retornarRelatorio(tipoRetorno);
            }
        }

        private DateTime AjustaProximaTarefaLonga()
        {
            if (DateTime.Now.TimeOfDay.Hours < 5)
                UltimaTarefaDiaria = DateTime.Today.AddHours(5);
            else
                if (DateTime.Now.TimeOfDay.Hours < 20)
                    UltimaTarefaDiaria = DateTime.Today.AddHours(20);
                else
                    UltimaTarefaDiaria = DateTime.Today.AddDays(1).AddHours(5);
            return UltimaTarefaDiaria;
        }

        private int EmailsPendentes;

        /// <summary>
        /// Chamado na ativa��o do servi�o
        /// </summary>
        public void Liga(bool OcultarMessageBox)
        {
            try
            {                
                ChamadaFinnet = DateTime.MinValue;
                retornodiario = TipoRetorno.email;
                retornochamada = TipoRetorno.email;                
                HoraTarefaDiaria = 20;
                MinutoTarefaDiaria = 0;
                UltimaTarefaDiaria = DateTime.MinValue;                
                AjustaProximaTarefaLonga();                
                
                VariaveisGlobais.OcultarMessageBox = OcultarMessageBox;
                HAtivacao = DateTime.Now;
                UltimoErro = string.Format("-- ok {0} --", HAtivacao);                
                if (ProxyVirWeb == null)
                {                    
                    ProxyVirWeb = new proxyvirweb.cVirtualProxy();
                    //System.Windows.Forms.MessageBox.Show("6");
                    ProxyVirWeb.TMP = CaminhoLocal + @"\TMP\";
                    EMP = dEstacao.dEstacaoSt.GetValor("EMP", 0);
                    //System.Windows.Forms.MessageBox.Show("7 EMP = "+EMP.ToString());
                    if (EMP == 1)
                    {                        
                        ProxyVirWeb.Cliente = proxyvirweb.Centra35.Clientes.NeonSBC;                        
                        BancoVirtual.Popular("NEON SBC REAL", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEON;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                        BancoVirtual.PopularAcces("NEON Access ", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\newscc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\newscc\neon.mda;Jet OLEDB:Database Password=96333018");
                        BancoVirtual.PopularAccesH("NEON AccessH", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\newscc\hist.mdb ;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\newscc\neon.mda;Jet OLEDB:Database Password=96333018");
                        BancoVirtual.PopularInternet("INTERNET REAL1", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 1);
                        BancoVirtual.PopularInternet("INTERNET REAL2", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 2);
                        BancoVirtual.PopularFilial("NEON SA REAL", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");                        
                    }
                    else
                        if (EMP == 3)
                        {
                            ProxyVirWeb.Cliente = proxyvirweb.Centra35.Clientes.NeonSA;
                            BancoVirtual.Popular("NEON SA REAL", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                            
                            BancoVirtual.PopularAcces("NEON Access", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon03\newsccSA\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=\\neon03\newsccSA\neon.MDA;Jet OLEDB:Database Password=7778");
                            BancoVirtual.PopularAccesH("NEON AccessH", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon03\newsccSA\hist.mdb ;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=\\neon03\newsccSA\neon.mda;Jet OLEDB:Database Password=7778");
                            BancoVirtual.PopularInternet("INTERNET REAL1", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 1);
                            BancoVirtual.PopularInternet("INTERNET REAL2", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 2);
                            BancoVirtual.PopularFilial("NEON SBC REAL", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEON;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                        }
                        else
                            if ((EMP == 0) || (EMP == 4))
                            {
                                ProxyVirWeb.Cliente = proxyvirweb.Centra35.Clientes.VirtualTeste;
                                BancoVirtual.Popular("QAS - SBC CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
                                BancoVirtual.Popular("QAS - SA CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");

                                BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
                                BancoVirtual.PopularAccesH("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\hist.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
                                BancoVirtual.PopularInternet("INTERNET Local 1", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=neoninternet;Persist Security Info=True;User ID=sa;Password=venus", 1);
                                BancoVirtual.PopularInternet("INTERNET Local 2", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=Neonimoveis_2;Persist Security Info=True;User ID=sa;Password=venus", 2);                                
                            }                    
                    ProxyVirWeb.Repositorio = dEstacao.dEstacaoSt.GetValor("Repositorio");
                    CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao = ((EMP != 0) && (EMP != 4));
                    //System.Windows.Forms.MessageBox.Show("11 Produ��o:" + FormPrincipalBase.strEmProducao.ToString());
                    new Framework.usuarioLogado.UsuarioLogadoNeon(30);
                    //System.Windows.Forms.MessageBox.Show("12");
                    ProxyVirWeb.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy;
                    //if(Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy == null)
                    //    System.Windows.Forms.MessageBox.Show("13 proxy: NULL");
                    //else
                    //    System.Windows.Forms.MessageBox.Show("13 proxy:" + Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy.Address.ToString());
                    ProxyVirWeb.Ativar(VirMSSQL.TableAdapter.STTableAdapter, VirOleDb.TableAdapter.STTableAdapter);
                    //System.Windows.Forms.MessageBox.Show("14");
                    ProxyVirWeb.StatusAplicativo = CallBackStatusAplicativo;
                    ProxyVirWeb.TarefaAplicativo = CallBackTarefaAplicativo;
                    ProxyVirWeb.TrocaModulo += TrocaModulo;                    
                };                                                
                TimerAutomatico = new Timer(Chamada, null, new TimeSpan(0, 10, 0), new TimeSpan(0, 15, 0));                
                VirExceptionNeon.VirExceptionNeonSt.TempoMaximoTrans = 30;
                EmailsPendentes = VirEmailNeon.EmailDiretoNeon.EmalST.Pendentes;
            }
            catch (Exception e)
            {
                UltimoErro = string.Format("Erro:{0}\r\n**********\r\n{1}\r\n**********\r\n", e.Message, e.StackTrace);
            };

            try
            {
                //RemotingConfiguration.Configure(string.Format(@"{0}\MonitorServer.config", CaminhoLocal), false);                
                FileVersionInfo VModuloH = FileVersionInfo.GetVersionInfo(String.Format(@"{0}\ModuloH.dll", CaminhoLocal));
                FileVersionInfo VdllModulo = FileVersionInfo.GetVersionInfo(String.Format(@"{0}\dllModulo.dll", CaminhoLocal));
                statusModulo.VesaoSt = string.Format("Processo   :{0}\r\nModuloH.dll:{1}\r\ndllModulo  :{2}\r\n",
                        Environment.CommandLine,
                        VModuloH.FileVersion,
                        VdllModulo.FileVersion);
                statusModulo.TrocaModulo = TrocaModulo;
                statusModulo.StatusAplicativoSt = CallBackStatusAplicativoLocal;
                statusModulo.CallBackTarefaAplicativoSt = CallBackTarefaAplicativo;
                statusModulo.EnviarImpostosSt = EnviarImpostos;
            }
            catch (Exception e)
            {
                UltimoErro += string.Format("Erro:{0}\r\n**********\r\n{1}\r\n**********\r\n", e.Message, e.StackTrace);
            };
            try
            {                
                statusModulo.UltimoSt = UltimoErro;
            }
            catch (Exception e)
            {
                UltimoErro += string.Format("Erro:{0}\r\n**********\r\n{1}\r\n**********\r\n", e.Message, e.StackTrace);
            };
        }        

        /// <summary>
        /// Desliga o loopMonitor
        /// </summary>
        public void Desliga()
        {
            VirExceptionNeon.VirExceptionNeonSt.DesligarLoop = true;
            VirEmailNeon.EmailDiretoNeon.EmalST.DesligarLoop = true;
        }

        /// <summary>
        /// Funcao que reporta o status do aplicativo para o proxy (� chamada indiretamente por CallBackStatusAplicativoLocal
        /// </summary>
        /// <returns></returns>
        public string CallBackStatusAplicativo()
        {
            return string.Format("Usu�rio: {10} - {11}\r\nAtiva��o:{0} Status:{1}\r\nUltimo Erro:{2}\r\nVers�o:\r\n{3} {4} {5} {6}\r\nNeonnet:{7}\r\nFinnet:{8} - {9}",                                  
                HAtivacao,
                DateTime.Now,
                UltimoErro ?? "-ok-",
                statusModulo.VesaoSt,
                Rechamada ? "Processo curto Ativo" : "Processo curto parado",                
                Cancelar ? "Off-line" : "On-line",
                RelogioAutomatico ? "RelogioAutomatico Ligado" : "RelogioAutomatico desligado",
                NeonNet.Local(),
                ChamadaFinnet == DateTime.MinValue ? "Parado" : string.Format("{0:dd/MM/yyyy HH:mm:ss} - {1}", ChamadaFinnet,LocaFinnnet),
                BloqueadoAguardaRetonoWAN ? "Aguardo":"Liberado",
                Environment.UserName, 
                Environment.MachineName
                );
        }

        private string LocaFinnnet = "";

        private bool Cancelar;        

        private string RetornaExepiton(Exception e)
        {
            string retorno = "";
            while (e != null)
            {
                retorno += string.Format("\r\nTipo: {0}\r\nErro: {1}\r\n{2}\r\n\r\n",e.GetType(), e.Message,e.StackTrace);
                e = e.InnerException;
            }
            return retorno;
        }

        /// <summary>
        /// CallBackTarefaAplicativo
        /// </summary>
        /// <param name="Parametro"></param>
        public void CallBackTarefaAplicativo(string Parametro)
        {
            bool tratado = false;
            try
            {
                if (Parametro.StartsWith("MANUALPERIODICO"))
                {
                    int n = int.Parse(Parametro.Substring(16).Trim());
                    ProxyVirWeb.RetornarCentral(string.Format("Chamada Manual - Peri�dicos {0}\r\n{1}",n, NeonNet.RevisaPeriodicos_WS(n)));
                    tratado = true;
                }

                if (Parametro.StartsWith("TesteWS"))
                {                    
                    ProxyVirWeb.RetornarCentral(string.Format("Teste WS \r\n{0}", NeonNet.Teste_WS()));
                    tratado = true;
                }

                if (Parametro.StartsWith("CHAMADAMANUAL"))
                {                    
                    ProxyVirWeb.RetornarCentral("Chamada Manual - Inicio"); 
                    Chamada(TipoRetorno.mensagem);
                    ProxyVirWeb.RetornarCentral("Chamada Manual - T�rmino"); 
                    tratado = true;
                }                

                if (Parametro.StartsWith("DESLIGAROBO"))
                {
                    RoboDesligado = true;
                    ProxyVirWeb.RetornarCentral("ROBO DESLIGADO");                    
                    tratado = true;
                }

                if (Parametro.StartsWith("RELIGAROBO"))
                {
                    RoboDesligado = false;
                    ProxyVirWeb.RetornarCentral("ROBO LIGADO");
                    tratado = true;
                }

                if (Parametro.StartsWith("CHAMADAROBO"))
                {
                    ProxyVirWeb.RetornarCentral("ROBO INICIO");
                    ProxyVirWeb.RetornarCentral(RoboBradesco.Processa(CaminhoRoboBradesco, CaminhoRoboBradescoEntrada, CaminhoRoboBradescoSaida));
                    ProxyVirWeb.RetornarCentral("ROBO FIM");
                    tratado = true;
                }

                if (Parametro.StartsWith("ENVIATRIBUTO"))
                {
                    /*
                    ProxyVirWeb.RetornarCentral("ENVIATRIBUTO INICIO");
                    ProxyVirWeb.RetornarCentral("20-TRIBUTO ELETRONICO FOLHA");
                    ProxyVirWeb.RetornarCentral(new EDIBoletosNeon.Arquivo().RemessaTributo(10000, CaminhoRoboBradescoSaida, dllImpostos.OrigemImposto.Folha));
                    ProxyVirWeb.RetornarCentral("21-TRIBUTO ELETRONICO NOTA");
                    ProxyVirWeb.RetornarCentral(new EDIBoletosNeon.Arquivo().RemessaTributo(10000, CaminhoRoboBradescoSaida, dllImpostos.OrigemImposto.Nota));
                    ProxyVirWeb.RetornarCentral(RoboBradesco.Processa(CaminhoRoboBradesco, CaminhoRoboBradescoEntrada, CaminhoRoboBradescoSaida));
                    ProxyVirWeb.RetornarCentral("ENVIATRIBUTO FIM");
                    tratado = true;*/
                }
                                              
                if (Parametro.StartsWith("NAODEBITOU"))
                {                    
                    ProxyVirWeb.RetornarCentral(string.Format("inicio n�o debitou"));
                    Conciliacao.ConciliacaoSt.VerificarDebitoAutomaticoPendente();                   
                    ProxyVirWeb.RetornarCentral(string.Format("fim n�odebitou"));
                    tratado = true;
                };

                if (Parametro.StartsWith("StatusCONCILIACAO"))
                {
                    ProxyVirWeb.RetornarCentral(string.Format("indice: {0:n0}", Conciliacao.ConciliacaoSt.indiceConciliar));
                    tratado = true;
                };
                if (Parametro.StartsWith("PROCESSOLONGO"))
                {
                    //ForcarPublicacaoDiaria = true;
                    UltimaTarefaDiaria = DateTime.MinValue;
                    ProxyVirWeb.RetornarCentral(string.Format("Processo Longo agendado:{0:HH:mm:ss}", DateTime.Now));
                    tratado = true;
                }
                
                
               

                if (Parametro.StartsWith("TESTEEMAIL"))
                {
                    try
                    {
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", "Teste e-mail", "teste e-mail");
                        ProxyVirWeb.RetornarCentral("Enviado");
                    }
                    catch (Exception ee) 
                    {
                        ProxyVirWeb.RetornarCentral(ReportaErro(ee, "Teste e-mail"));
                    }
                    tratado = true;
                }

                if (Parametro.StartsWith("AJUSTAHORARETORNO"))
                {
                    if (Parametro.Length == 21)
                    {
                        HoraTarefaDiaria = int.Parse(Parametro.Substring(17, 2));
                        MinutoTarefaDiaria = int.Parse(Parametro.Substring(19, 2));
                        UltimaTarefaDiaria = DateTime.MinValue;
                        ProxyVirWeb.RetornarCentral(string.Format("OK Novo hor�rio: {0:00}:{1:00}", HoraTarefaDiaria, MinutoTarefaDiaria));
                        tratado = true;
                    }

                }
                else if (Parametro.StartsWith("AJUSTARETORNO"))
                {
                    if (Parametro.Length == 15)
                    {
                        int tipo = int.Parse(Parametro.Substring(14, 1));
                        TipoRetorno NovoTipo = (TipoRetorno)tipo;
                        if (Parametro.Substring(13, 1) == "D")
                            retornodiario = NovoTipo;
                        else
                            retornochamada = NovoTipo;
                        ProxyVirWeb.RetornarCentral(string.Format("OK Tipo retorno {0} : {1}", Parametro.Substring(13, 1), NovoTipo));
                        tratado = true;
                    }

                }
                else if (Parametro.StartsWith("CONDOMINIOS"))
                {
                    ProxyVirWeb.RetornarCentral(string.Format("CONDOMINIOS Resultado:\r\n{0}", NeonNet.PublicaCondominios()));
                    tratado = true;
                }
                else if (Parametro.ToUpper() == "CANCELAR")
                {
                    Cancelar = true;
                    NeonNet.Cancelar = true;
                    tratado = true;
                    ProxyVirWeb.RetornarCentral("CANCELANDO...");
                }
                else if (Parametro.ToUpper() == "OFFLINE")
                {
                    Cancelar = true;
                    tratado = true;
                    ProxyVirWeb.RetornarCentral(string.Format("OFFLINE {0}", Rechamada ? "PROCESSO PENDENTE" : "processo ok"));
                }
                else if (Parametro.ToUpper() == "LIBERAR")
                {
                    if (Rechamada)
                        ProxyVirWeb.RetornarCentral("processo curto ativo");
                    else
                    {
                        Cancelar = false;
                        NeonNet.Cancelar = false;
                        tratado = true;
                    }
                }
                else if (Parametro.StartsWith("APARTAMENTOS"))
                {
                    ProxyVirWeb.RetornarCentral(string.Format("APARTAMENTOS Resultado:\r\n{0}", NeonNet.PublicaApartamentos(EMP)));
                }
                else if (Parametro.StartsWith("APAGAR"))
                {
                    ProxyVirWeb.RetornarCentral("INICIO APAGAR");
                    ProxyVirWeb.RetornarCentral(string.Format("APAGAR Resultado\r\n:{0}", NeonNet.Apagar()));
                    ProxyVirWeb.RetornarCentral("FIM APAGAR");
                    tratado = true;
                }
                else if (Parametro.ToUpper() == "LIGAAUTORELGIO")
                {
                    RelogioAutomatico = true;
                    tratado = true;
                }
                else if (Parametro.ToUpper() == "DESLIGAAUTORELGIO")
                {
                    RelogioAutomatico = false;
                    tratado = true;
                }

            }
            catch (Exception e)
            {
                string strErro = "Erro em tarefa";
                while (e != null)
                {
                    strErro += string.Format("{0}\r\n{1}\r\n", e.Message, e.StackTrace);
                    e = e.InnerException;
                };
                ProxyVirWeb.RetornarCentral(strErro);
            }

            if (tratado)
                return;

            if ((!tratado) && (Parametro == "ECO"))
                ProxyVirWeb.RetornarCentral("\r\nRetorno ECO\r\n");

                                    

            

            

            else if (Parametro.StartsWith("ARQUIVO "))
            {
                string pathVerificar = Parametro.Substring(8);
                string Retorno = string.Format("|{0}|: ", pathVerificar);
                Retorno += File.Exists(pathVerificar) ? File.GetCreationTime(pathVerificar).ToString() : "Arquivo n�o existe";
                ProxyVirWeb.RetornarCentral(Retorno);
            }
            else if (Parametro.StartsWith("INDICES"))
            {
                ProxyVirWeb.RetornarCentral(string.Format("EMP:{0}", EMP));
                Internet.NeonNet NeonNet = new Internet.NeonNet(true, EMP);
                ProxyVirWeb.RetornarCentral(string.Format("Resultado\r\n:{0}", NeonNet.PublicaIndices()));
            }
            




            else
                ProxyVirWeb.RetornarCentral(string.Format("Comando inv�lido<{0}>", Parametro));
        }

        /// <summary>
        /// Funcao que reporta o status do aplicativo para o moduloH, inclui staus do proxy
        /// </summary>
        /// <returns></returns>
        public string CallBackStatusAplicativoLocal()
        {
            if (ProxyVirWeb != null)
                return ProxyVirWeb.StatusProxy();
            else
                return CallBackStatusAplicativoLocal();
        }

        private void TrocaModulo(object sender, EventArgs e)
        {
            try
            {
                string CaminhoLocal = System.IO.Path.GetDirectoryName(Environment.CommandLine.Replace("\"", ""));
                string arquivoV = String.Format(@"{0}\Menu.exe", CaminhoLocal);
                string arquivoN = String.Format(@"{0}Menu.exe", dEstacao.dEstacaoSt.GetValor("Repositorio"));
                UltimoErro += string.Format("\r\narquivos: {0}-{1}\r\n", arquivoV, arquivoN);
                try
                {
                    if (File.Exists(arquivoN) && !File.Exists(arquivoV))
                        File.Copy(arquivoN, arquivoV);
                    else
                    {
                        FileVersionInfo VMenuV = FileVersionInfo.GetVersionInfo(arquivoV);
                        FileVersionInfo VMenuN = FileVersionInfo.GetVersionInfo(arquivoN);
                        if (VMenuN.FileVersion != VMenuV.FileVersion)
                            File.Copy(arquivoN, arquivoV, true);
                    }
                }
                catch (Exception ex)
                {
                    UltimoErro += string.Format("\r\nErro:{0}\r\n**********\r\n{1}\r\n**********\r\n", ex.Message, ex.StackTrace);
                }

                ProcessStartInfo info = new ProcessStartInfo(String.Format(@"{0}\menu.exe", CaminhoLocal), "MODULO");
                info.WorkingDirectory = CaminhoLocal;
                Process Pro = Process.Start(info);
            }
            catch (Exception ex)
            {
                UltimoErro += string.Format("\r\nErro:{0}\r\n**********\r\n{1}\r\n**********\r\n", ex.Message, ex.StackTrace);
            }
        }

        private string ServidorRetorno(TipoRetono tipoRetono)
        {            
            switch(tipoRetono)
            {
                case TipoRetono.ROBO_BRADESCO:
                case TipoRetono.NEXXERA:
                    switch (EMP)
                    {
                        case 1:
                        case 3:
                            return String.Format(@"{0}Bradepag\", IdentificaArquivo.IdentificaArquivoST.ServidorRetorno);
                        case 0: return @"C:\FontesVS\neon\Modulo\finnetfast\bin\Release\retorno\";
                        default: return "";
                    }                    
                case TipoRetono.TIVIT:
                    {
                        switch (EMP)
                        {
                            case 1:
                            case 3:
                                return String.Format(@"{0}CAIXA\", IdentificaArquivo.IdentificaArquivoST.ServidorRetorno);
                            case 0:
                                return String.Format(@"{0}CAIXA\", @"c:\");
                            default: return "";
                        }                        
                    }
            }
            return "";       
        }

        #region FINNET

        private string CaminhoFinnet
        {
            get 
            {
                switch (EMP)
                {
                    case 1: return @"D:\FINNET\";
                    case 0: return @"C:\FontesVS\neon\Modulo\finnetfast\bin\Release\";
                    default: return "";
                }
            }
        }

        private string CaminhoFinnetSaida
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\FINNET\431191108CON\SAIDA\";
                    case 0: return @"C:\FontesVS\neon\Modulo\finnetfast\bin\Release\";
                    default: return "";
                }
            }
        }

        private bool EnviarImpostos()
        {
            if (EMP == 3)
                return false;
            //string ArquivoGerado = dllImpostoNeon.dGPS.dGPSSt.GerarGPS(Tipo);
            string GeradoGPS = dllImpostoNeonProc.dGPS.dGPSSt.GerarGPS(TipoImposto.INSS);
            string GeradoPis = dllImpostoNeonProc.dGPS.dGPSSt.GerarGPS(TipoImposto.PIS);
            if (GeradoGPS != "")            
                File.Move(GeradoGPS, String.Format("{0}431191108CON\\SAIDA\\{1}", CaminhoFinnet, System.IO.Path.GetFileName(GeradoGPS)));                
            
            if (GeradoPis != "")        
                File.Move(GeradoPis, String.Format("{0}431191108CON\\SAIDA\\{1}", CaminhoFinnet, System.IO.Path.GetFileName(GeradoPis)));

            if ((GeradoPis != "") || (GeradoGPS != ""))
            {
                //ChamaFinnet();
                return true;
            }
            else
                return false;
        }

        private DateTime ChamadaFinnet;

        

        //*** MRC - INICIO (12/08/2014 10:00) ***
        /// <summary>
        /// Esta fun��o quebra as p�ginas do arquivo PDF contendo os comprovantes dos pagamentos eletr�nicos e salva cada p�gina individualmente com o n�mero do pagamento indicado no nome
        /// </summary>
        /// <param name="inputPath">Arquivo a ser quebrado em arquivos por p�gina</param>
        /// <param name="outputPath">Diret�rio de destino dos arquivos gerados</param>
        private void SplitAndSave(string inputPath, string outputPath)
        {
            FileInfo file = new FileInfo(inputPath);
            string name = file.Name.Substring(0, file.Name.LastIndexOf("."));

            using (PdfReader reader = new PdfReader(inputPath))
            {
                for (int pagenumber = 1; pagenumber <= reader.NumberOfPages; pagenumber++)
                {
                    string strTextPage = PdfTextExtractor.GetTextFromPage(reader, pagenumber);
                    string filename = "recibo_" + Convert.ToInt64(strTextPage.Substring(strTextPage.IndexOf("000000"), 16)).ToString() + ".pdf";

                    Document document = new Document();
                    PdfCopy copy = new PdfCopy(document, new FileStream(outputPath + "\\" + filename, FileMode.Create));
                    document.Open();
                    copy.AddPage(copy.GetImportedPage(reader, pagenumber));
                    document.Close();
                }
            }
        }
        //*** MRC - TERMINO (12/08/2014 10:00) ***

        /*
        private void ChamaFinnet()
        {
            if (EMP == 3)
                return;
            Process process = new System.Diagnostics.Process();

            //*** MRC - INICIO (20/02/2015 20:00) ***
            if (!par)
            {
                //FINNET
                //*** MRC - TERMINO (20/02/2015 20:00) ***
                process.StartInfo.Arguments = "\"D:\\FINNET\\CTCP.INI\" -p 431191108CON -r 5 -t 30 -m B";
                process.StartInfo.FileName = string.Format("{0}program\\finnetfast.exe", CaminhoFinnet);
                //*** MRC - INICIO (20/02/2015 20:00) ***
            }
            else
            {
                //NEXXERA
                process.StartInfo.Arguments = "/SE=neonimoveis15";
                process.StartInfo.FileName = string.Format("{0}Skyline.exe", CaminhoNexxera);
            }
            //*** MRC - TERMINO (20/02/2015 20:00) ***

            process.StartInfo.Verb = "Open";
            process.Exited += process_Exited;
            process.EnableRaisingEvents = true;
            //process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
            process.Start();
            ChamadaFinnet = DateTime.Now;
        }
        */

        #endregion
        
        /// <summary>
        /// A chamada VAN � sincrona mas seu retorno n�o.
        /// </summary>
        /// <param name="TipoVan">Tipo da VAN</param>
        /// <returns></returns>
        private string ChamaVAN(TipoVan TipoVan)
        {
            string retorno = "";
            Process process = new System.Diagnostics.Process();

            //Verifica qual Tipo de VAN vai chamar
            switch (TipoVan)
            {
                case TipoVan.FINNET:
                    //FINNET
                    //Verifica "ctcp.ini" n�o existente
                    if (!File.Exists(CaminhoFinnet + "ctcp.ini"))
                        return string.Format("Finnet: N�o configurado.\r\n");
                    else
                    {
                        //Seta parametros
                        process.StartInfo.Arguments = "\"D:\\FINNET\\CTCP.INI\" -p 431191108CON -r 5 -t 30 -m B";
                        process.StartInfo.FileName = string.Format("{0}program\\finnetfast.exe", CaminhoFinnet);
                        retorno = string.Format("Finnet: Chamada efetuada.\r\n");
                    }
                    break;
                case TipoVan.NEXXERA:
                    //NEXXERA
                    //Verifica "wtcm.ini" n�o existente
                    if (!File.Exists(CaminhoNexxera + "wtcm.ini"))
                        return string.Format("Nexxera: N�o configurado.\r\n");
                    else
                    {
                        //Pega parametros configurados
                        StringBuilder passIni = new StringBuilder(15);
                        string pass = "";
                        GetPrivateProfileString("Neon", "pass", "", passIni, 15, CaminhoNexxera + "wtcm.ini");
                        pass = passIni.ToString();

                        //Seta parametros
                        process.StartInfo.Arguments = string.Format("/SE={0}", pass);
                        process.StartInfo.FileName = string.Format("{0}Skyline.exe", CaminhoNexxera);
                        retorno = string.Format("Nexxera: Chamada efetuada.\r\n");
                    }
                    break;
                default:
                    return string.Format("Tipo de VAN n�o identificada.\r\n");
            }

            //Start do processo
            process.StartInfo.Verb = "Open";
            process.Exited += process_Exited;
            process.EnableRaisingEvents = true;
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
            process.Start();
            ChamadaFinnet = DateTime.Now;

            //*** MRC - INICIO (01/04/2016 19:00) ***
            BloqueadoAguardaRetonoWAN = true; //O retono do ChamaFinnet (ou ChamaVAN) � assincrono e destrava o tratamento do retorno que � s�ncrono                            
            //*** MRC - TERMINO (01/04/2016 19:00) ***

            //Retorna status
            return retorno;
        }
        //*** MRC - TERMINO (27/03/2015 09:30) ***

        //*** MRC - INICIO (08/08/2016 11:00) ***
        /// <summary>
        /// Confere o envio de arquivos da VAN
        /// </summary>
        /// <param name="TipoVan">Tipo da VAN</param>
        /// <returns></returns>
        private string ConfereEnvioVAN(TipoVan TipoVan)
        {
            string retorno = "";
            int intReenviados = 0;

            //Verifica qual Tipo de VAN vai chamar
            switch (TipoVan)
            {
                case TipoVan.NEXXERA:
                    //NEXXERA
                    //Verifica todos arquivos do dia (se ja houver)
                    string Referencia = @"C:\Modulo servidor\TMP\";
                    string strPathBackup = string.Format(@"{0}BKP_PE\{1:yyyyMMdd}\", Referencia, DateTime.Now);
                    DirectoryInfo Dir = new DirectoryInfo(strPathBackup);
                    if (Dir.Exists)
                    {
                        //Pega cada arquivo e verifica se nao esta na pasta dos enviados e nem na pasta dos que estao para envio
                        FileInfo[] Files = Dir.GetFiles("*");
                        foreach (FileInfo arq in Files)
                        {
                            if (!File.Exists(CaminhoNexxeraSaida + @"\" + arq.Name) && !File.Exists(CaminhoNexxeraSaida + @"\BACKUP\" + arq.Name))
                            {
                                File.Copy(arq.FullName, CaminhoNexxeraSaida + @"\" + arq.Name);
                                intReenviados++;
                            }
                        }
                    }
                    retorno = string.Format("Nexxera: Reenviados - {0}\r\n", intReenviados);
                    break;
                default:
                    return string.Format("Tipo de VAN n�o identificada ou sem confer�ncia de envio programada.\r\n");
            }

            //Retorna status
            return retorno;
        }
        //*** MRC - TERMINO (08/08/2016 11:00) ***

        /// <summary>
        /// Este processo ocorre pela chamada do retorno do processo da VAN. n�o � conveniente fazer nada porque pode ocorrer de forma simultanea com os outros 2 loops (site / virweb)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void process_Exited(object sender, EventArgs e)
        {
            BloqueadoAguardaRetonoWAN = false;
        }

        //*** MRC - INICIO - TRIBUTOS (20/02/2015 20:00) ***
        #region NEXXERA

        private string CaminhoNexxera
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\NEXXERA\";
                    //*** MRC - INICIO (27/03/2015 09:30) ***
                    case 3: return @"D:\NEXXERA\";
                    //*** MRC - TERMINO (27/03/2015 09:30) ***
                    case 0: return @"C:\FontesVS\Neon\Modulo\Nexxera\";
                    default: return "";
                }
            }
        }

        private string CaminhoNexxeraSaida
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\NEXXERA\SAIDA\";
                    //*** MRC - INICIO (27/03/2015 09:30) ***
                    case 3: return @"D:\NEXXERA\SAIDA\";
                    //*** MRC - TERMINO (27/03/2015 09:30) ***
                    case 0: return @"C:\FontesVS\Neon\Modulo\Nexxera\Saida\";
                    default: return "";
                }
            }
        }

        //*** MRC - INICIO (27/03/2015 09:30) ***
        private string CaminhoNexxeraEntrada
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\NEXXERA\ENTRADA\";
                    case 3: return @"D:\NEXXERA\ENTRADA\";
                    case 0: return @"C:\FontesVS\Neon\Modulo\Nexxera\Entrada\";
                    default: return "";
                }
            }
        }

        private string CaminhoCaixaEntrada
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"C:\CAIXA\COBRANCA\";
                    case 3: return @"C:\Caixa\COBRANCA\";
                    case 0: return @"C:\CAIXA\COBRANCA\";
                    default: return "";
                }
            }
        }


        //*** MRC - TERMINO (27/03/2015 09:30) ***

        #endregion
        //*** MRC - TERMINO - TRIBUTOS (20/02/2015 20:00) ***

        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
        #region ROBO_BRADESCO

        private string CaminhoRoboBradesco
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\ROBO_BRADESCO\";
                    //*** MRC - INICIO (23/07/2015 11:00) ***
                    case 3: return @"D:\ROBO_BRADESCO\";
                    //*** MRC - TERMINO (23/07/2015 11:00) ***
                    case 0: return @"C:\FontesVS\Neon\Modulo\Robo_Bradesco\";
                    default: return "";
                }
            }
        }

        private string CaminhoRoboBradescoSaida
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\ROBO_BRADESCO\SAIDA\";
                    //*** MRC - INICIO (23/07/2015 11:00) ***
                    case 3: return @"D:\ROBO_BRADESCO\SAIDA\";
                    //*** MRC - TERMINO (23/07/2015 11:00) ***
                    case 0: return @"C:\FontesVS\Neon\Modulo\Robo_Bradesco\Saida\";
                    default: return "";
                }
            }
        }

        //*** MRC - INICIO - ROBO (10/03/2015 14:00) ***
        private string CaminhoRoboBradescoEntrada
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\ROBO_BRADESCO\ENTRADA\";
                    //*** MRC - INICIO (23/07/2015 11:00) ***
                    case 3: return @"D:\ROBO_BRADESCO\ENTRADA\";
                    //*** MRC - TERMINO (23/07/2015 11:00) ***
                    case 0: return @"C:\FontesVS\Neon\Modulo\Robo_Bradesco\Entrada\";
                    default: return "";
                }
            }
        }
        //*** MRC - TERMINO - ROBO (10/03/2015 14:00) ***

        #endregion
        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***

        //*** MRC - INICIO (21/03/2016 20:00) ***
        #region ITAU

        /*
        private string CaminhoItau
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\ITAU\";
                    case 3: return @"D:\ITAU\";
                    case 0: return @"C:\FontesVS\Neon\Modulo\Itau\";
                    default: return "";
                }
            }
        }*/

        private string CaminhoItauSaida
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\ITAU\SAIDA\";
                    case 3: return @"D:\ITAU\SAIDA\";
                    case 0: return @"C:\FontesVS\Neon\Modulo\Itau\Saida\";
                    default: return "";
                }
            }
        }

        private string CaminhoCaixaSaida
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"C:\CAIXA\COBRANCA\";
                    case 3: return @"D:\Caixa\SAIDA\";
                    case 0: return @"C:\CAIXA\COBRANCA\";
                    default: return "";
                }
            }
        }

        private string CaminhoSanSaida
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\SAN\SAIDA\";
                    case 3: return @"D:\SAN\SAIDA\";
                    case 0: return @"C:\FontesVS\Neon\Modulo\SAN\Saida\";
                    default: return "";
                }
            }
        }

        /*
        private string CaminhoItauEntrada
        {
            get
            {
                switch (EMP)
                {
                    case 1: return @"D:\ITAU\ENTRADA\";
                    case 3: return @"D:\ITAU\ENTRADA\";
                    case 0: return @"C:\FontesVS\Neon\Modulo\Itau\Entrada\";
                    default: return "";
                }
            }
        }*/

        #endregion
        //*** MRC - TERMINO (21/03/2016 20:00) ***

        /// <summary>
        /// 
        /// </summary>
        public enum TipoRetono
        {
            /// <summary>
            /// 
            /// </summary>
            ROBO_BRADESCO,
            /// <summary>
            /// 
            /// </summary>
            NEXXERA,
            /// <summary>
            /// 
            /// </summary>
            TIVIT
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strPath"></param>
        /// <param name="tipoRetono"></param>
        /// <returns></returns>
        public string TratarArquivosRetorno(string strPath, TipoRetono tipoRetono)
        {
            string retorno = "";
            try
            {
                //Verifica se diretorio de entrada existe e esta acessivel
                if (!Directory.Exists(strPath))
                    return string.Format("Erro: Diret�rio de entrada de arquivos de retorno n�o existe ou n�o est� acess�vel: '{0}'", strPath);

                //Verifica se diretorio do servidor de retorno existe e esta acessivel
                if (!Directory.Exists(ServidorRetorno(tipoRetono)))
                    return string.Format("Erro: Diret�rio de servidor de retorno n�o existe ou n�o est� acess�vel: '{0}'", ServidorRetorno(tipoRetono));

                //Inicia variaveis
                IdentificaArquivo.IdentificaArquivoST.RetornoNoLog = true;
                IdentificaArquivo.IdentificaArquivoST.Log = "";
                bool comErros = false;

                if (tipoRetono == TipoRetono.TIVIT)
                {
                    string PastaBKP = string.Format("{0}{1:yyyy_MM_dd}\\", ServidorRetorno(tipoRetono), DateTime.Now);
                    string[] CaixasPostais = Directory.GetDirectories(strPath);
                    foreach (string CaixaPoatal in CaixasPostais)
                    {
                        string pastaretorno = string.Format("{0}\\RETORNO", CaixaPoatal);
                        if (Directory.Exists(pastaretorno))
                        {
                            string[] arquivosRet = Directory.GetFiles(pastaretorno);
                            foreach (string arquivoRet in arquivosRet)
                            {
                                if (System.IO.Path.GetExtension(arquivoRet) != ".RET")
                                {
                                    string NomeArquivo = string.Format("{0}Ignorado{1}", PastaBKP, System.IO.Path.GetFileName(arquivoRet));
                                    while (File.Exists(NomeArquivo))                                    
                                        NomeArquivo = string.Format("{0}Ignorado{1:HH-mm-ss_}{2}", PastaBKP, DateTime.Now, System.IO.Path.GetFileName(arquivoRet));                                    
                                    File.Move(arquivoRet, NomeArquivo);
                                    IdentificaArquivo.IdentificaArquivoST.Log += string.Format("\r\n{0} -> {1}\r\n", arquivoRet, NomeArquivo);
                                }
                                else
                                {
                                    VirExceptionNeon.VirExceptionNeonSt.TempoMaximoTrans = 5 * 60;
                                    if (!IdentificaArquivo.IdentificaArquivoST.ProcessaArquivo(arquivoRet))
                                        comErros = true;
                                    VirExceptionNeon.VirExceptionNeonSt.TempoMaximoTrans = 30;
                                    //Faz backup no servidor de retorno
                                    if (!Directory.Exists(PastaBKP))
                                        Directory.CreateDirectory(PastaBKP);
                                    string NomeArquivo = (string.Format("{0}{1}", PastaBKP, System.IO.Path.GetFileName(arquivoRet)));
                                    while (File.Exists(NomeArquivo))
                                        NomeArquivo = string.Format("{0}{1:HH-mm-ss_}{2}", PastaBKP, DateTime.Now, System.IO.Path.GetFileName(arquivoRet));
                                    File.Move(arquivoRet, NomeArquivo);
                                }
                            }
                        }
                    }
                }
                else
                {
                    //Verifica o destino do comprovante
                    string DestinoPDF = dEstacao.dEstacaoSt.GetValor("Destino PDF", @"\\email\volume_1\publicacoes\");

                    //Pega arquivos da pasta
                    string[] arquivos = Directory.GetFiles(strPath, "*.*", SearchOption.TopDirectoryOnly);

                    //Processa cada arquivo
                    foreach (string arquivo in arquivos)
                    {
                        //Verifica Tipo
                        switch (tipoRetono)
                        {
                            case TipoRetono.ROBO_BRADESCO:
                                //Verifica se arquivo recebido deve ser tratado        
                                bool Tratar = false;
                                string nomeArquivolido = System.IO.Path.GetFileName(arquivo).ToUpper();
                                if (nomeArquivolido.Substring(0, 2) == "PT")   //PT = Tributos
                                    Tratar = true;
                                else if ((nomeArquivolido.Substring(0, 2) == "CB") && (System.IO.Path.GetExtension(arquivo) == ".RET"))  //CB = Cobran�a
                                    Tratar = true;
                                else if ((nomeArquivolido.Substring(0, 2) == "CC") && (nomeArquivolido.Substring(6, 1) == "A") && (System.IO.Path.GetExtension(arquivo).ToUpper() == ".RET"))  //CC = Extrato
                                    Tratar = true;

                                if (Tratar)
                                {
                                    //Trata o arquivo
                                    VirExceptionNeon.VirExceptionNeonSt.TempoMaximoTrans = 5 * 60;
                                    if (!IdentificaArquivo.IdentificaArquivoST.ProcessaArquivo(arquivo))
                                        comErros = true;
                                    VirExceptionNeon.VirExceptionNeonSt.TempoMaximoTrans = 30;

                                    //Faz backup no servidor de retorno
                                    string NomeArquivo = (string.Format("{0}{1}", ServidorRetorno(tipoRetono), System.IO.Path.GetFileName(arquivo)));
                                    while (File.Exists(NomeArquivo))
                                    {
                                        NomeArquivo = string.Format("{0}{1:yyyy-MM-dd_HH-mm-ss_}{2}", ServidorRetorno(tipoRetono), DateTime.Now, System.IO.Path.GetFileName(arquivo));
                                    }
                                    File.Move(arquivo, NomeArquivo);
                                }
                                else
                                {

                                    string NomeArquivo = string.Format("{0}Ignorado\\{1}", strPath, System.IO.Path.GetFileName(arquivo));
                                    while (File.Exists(NomeArquivo))
                                    {
                                        NomeArquivo = string.Format("{0}Ignorado\\{1:yyyy-MM-dd_HH-mm-ss_}{2}", strPath, DateTime.Now, System.IO.Path.GetFileName(arquivo));
                                    }
                                    File.Move(arquivo, NomeArquivo);
                                    IdentificaArquivo.IdentificaArquivoST.Log += string.Format("\r\n{0} -> {1}\r\n", arquivo, NomeArquivo);
                                }
                                break;                            
                            case TipoRetono.NEXXERA:
                                //Verifica se arquivo recebido deve ser tratado
                                // * PG = Pagamento Eletr�nico
                                // * PAG_*.PDF = Comprovantes
                                // * comprovante_*.PDF = Comprovantes
                                if (System.IO.Path.GetFileName(arquivo).Substring(0, 2) == "PG")
                                {
                                    //Trata o arquivo
                                    VirExceptionNeon.VirExceptionNeonSt.TempoMaximoTrans = 5 * 60;
                                    if (!IdentificaArquivo.IdentificaArquivoST.ProcessaArquivo(arquivo))
                                        comErros = true;
                                    VirExceptionNeon.VirExceptionNeonSt.TempoMaximoTrans = 30;

                                    //Faz backup no servidor de retorno
                                    string NomeArquivo = (string.Format("{0}{1}", ServidorRetorno(tipoRetono), System.IO.Path.GetFileName(arquivo)));
                                    while (File.Exists(NomeArquivo))
                                    {
                                        NomeArquivo = string.Format("{0}{1:yyyy-MM-dd_HH-mm-ss_}{2}", ServidorRetorno(tipoRetono), DateTime.Now, System.IO.Path.GetFileName(arquivo));
                                    }
                                    File.Move(arquivo, NomeArquivo);
                                }
                                //*** MRC - INICIO (18/08/2015 10:00) ***
                                else if (
                                          arquivo.ToUpper().Contains("PDF")
                                             &&
                                          (
                                           System.IO.Path.GetFileName(arquivo).StartsWith("PAG_")
                                            ||
                                           System.IO.Path.GetFileName(arquivo).StartsWith("comprovante_")
                                          )
                                        )
                                {
                                    //*** MRC - TERMINO (18/08/2015 10:00) ***
                                    //*** MRC - INICIO (28/04/2016 12:00) ***
                                    try
                                    {
                                        //Seta destino e arquivo final do comprovante dependendo do tipo
                                        string DestinoComprovante = DestinoPDF + @"Financeiro\Recibos\Individual\";
                                        if (!Directory.Exists(DestinoComprovante))
                                            Directory.CreateDirectory(DestinoComprovante);
                                        DestinoComprovante = DestinoComprovante + "recibo_" + Convert.ToInt64(arquivo.Substring(arquivo.IndexOf("000000"), 16)).ToString() + ".pdf";

                                        //Salva comprovante individual com o numero de pagamento
                                        File.Copy(arquivo, DestinoComprovante);
                                        File.Delete(arquivo);
                                        IdentificaArquivo.IdentificaArquivoST.Log += string.Format("\r\nCopiado PDF {0} -> {1}\r\n", arquivo, DestinoComprovante);
                                    }
                                    catch (Exception ex)
                                    {
                                        IdentificaArquivo.IdentificaArquivoST.Log += string.Format("\r\nErro na c�pia do PDF {0}\r\n\r\n{1}", arquivo, RetornaExepiton(ex));
                                    }
                                    //*** MRC - INICIO (28/04/2016 12:00) ***
                                }
                                else
                                {
                                    //Ignora o arquivo
                                    File.Move(arquivo, strPath + @"IGNORADO\" + System.IO.Path.GetFileName(arquivo));
                                    IdentificaArquivo.IdentificaArquivoST.Log += string.Format("\r\nIgnorado {0} -> {1}\r\n", arquivo, strPath + @"IGNORADO\" + System.IO.Path.GetFileName(arquivo));
                                }
                                break;
                            default:
                                IdentificaArquivo.IdentificaArquivoST.Log += string.Format("\r\nTipo de Retorno n�o identificado ({0}).\r\n", tipoRetono);
                                comErros = true;
                                break;
                        }
                    }
                }
                //Registra log caso exista
                if (IdentificaArquivo.IdentificaArquivoST.Log != "")
                    File.WriteAllLines(String.Format(@"{0}\log\{1:yyyyMMdd_HH_mm_ss}.log", CaminhoLocal, DateTime.Now), new string[] { IdentificaArquivo.IdentificaArquivoST.Log });

                //Envia e-mail com log indicando erro
                if (comErros)
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("erros@virweb.com.br", IdentificaArquivo.IdentificaArquivoST.Log, string.Format("ERRO no tratamento de retorno ({0})", tipoRetono));

                //Seta o retorno                
                retorno += IdentificaArquivo.IdentificaArquivoST.Log == "" ? "Sem arquivos" : string.Format("Resultado tratamento de retorno {0}:\r\n{1}\r\n", tipoRetono, IdentificaArquivo.IdentificaArquivoST.Log);
            }
            catch (Exception ex)
            {
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("erros@virweb.com.br", RetornaExepiton(ex), string.Format("ERRO no tratamento de retorno ({0})", tipoRetono));
                retorno += string.Format("ERRO no tratamento de retorno ({0}) (Arquivo:{2}): {1}", tipoRetono, RetornaExepiton(ex), strPath);
            }

            //Retorna resultado do tratamento de retorno
            return retorno;
        }
        
        
    }
}
