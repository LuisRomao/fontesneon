namespace TesteMonitor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cMonitorModulo1 = new MonitorModulo.cMonitorModulo();
            this.SuspendLayout();
            // 
            // cMonitorModulo1
            // 
            this.cMonitorModulo1.CaixaAltaGeral = true;
            this.cMonitorModulo1.Doc = System.Windows.Forms.DockStyle.None;
            this.cMonitorModulo1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
       
            this.cMonitorModulo1.Location = new System.Drawing.Point(13, 35);
            this.cMonitorModulo1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cMonitorModulo1.Name = "cMonitorModulo1";
            this.cMonitorModulo1.Size = new System.Drawing.Size(763, 459);
            this.cMonitorModulo1.somenteleitura = false;
            this.cMonitorModulo1.TabIndex = 0;
            this.cMonitorModulo1.Titulo = null;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 525);
            this.Controls.Add(this.cMonitorModulo1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private MonitorModulo.cMonitorModulo cMonitorModulo1;

    }
}

