using System;
using System.Windows.Forms;


namespace TesteMonitor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //RemotingConfiguration.Configure("Client.exe.config",false);
            //moduloH.statusModulo statusModulo = (moduloH.statusModulo)Activator.GetObject(typeof(moduloH.statusModulo), "http://lapneon:8989/statusModulo.rem");
            //moduloH.statusModulo statusModulo = new moduloH.statusModulo();
            //MessageBox.Show(statusModulo.GetVesao());
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}