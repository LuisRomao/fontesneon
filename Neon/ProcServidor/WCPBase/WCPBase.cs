﻿using System;
using System.Collections.Generic;
using System.Text;
using CompontesBasicosProc;
using VirCrip;
using VirDB.Bancovirtual;
using VirExceptionProc;

namespace WCPBase
{
    /// <summary>
    /// Classe abstrata base parta todos os objetos Servidores de processos
    /// </summary>
    public abstract class WCPBase
    {
        private static List<string> _Usados;

        private static List<string> Usados
        {
            get
            {
                if (_Usados == null)
                    _Usados = new List<string>();
                else
                    if (_Usados.Count > 500)
                        _Usados.Clear();
                return _Usados;
            }
        }

        /// <summary>
        /// Retorno
        /// </summary>
        protected RetornoServidorProcessos Retorno;

        /// <summary>
        /// identifica a função no log
        /// </summary>
        protected abstract string Identificador13 { get; }

        /// <summary>
        /// Dados da chamada
        /// </summary>
        protected string DadosDaChamada;

        /// <summary>
        /// Valida a chamada
        /// </summary>
        /// <param name="Chave"></param>                
        /// <returns></returns>
        protected bool DecodificaChave(byte[] Chave)
        {
            VirMSSQL.TableAdapter.Servidor_De_Processos = true;
            Retorno = new RetornoServidorProcessos("OK","OK");
            AddLog("INICIO");
            if (!BancoVirtual.Configurado(TiposDeBanco.SQL))
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["AMBIENTE"] == "PRODUCAO")
                {
                    AddLog("PRODUÇÂO");
                    BancoVirtual.Popular("SBC", @"Data Source=177.190.193.218\NEON;Initial Catalog=Neon;Persist Security Info=True;User ID=NEONP;Connect Timeout=30;Password=TPC4P2DP");
                    BancoVirtual.PopularFilial("SA", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSA;Persist Security Info=True;User ID=NEONP;Connect Timeout=30;Password=TPC4P2DP");
                }
                else if (System.Web.Configuration.WebConfigurationManager.AppSettings["AMBIENTE"] == "TESTE")
                {
                    AddLog("TESTE");
                    BancoVirtual.Popular("QAS - SBC", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
                    BancoVirtual.PopularFilial("QAS - SA", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
                }
                else
                {
                    BancoVirtual.Popular("QAS CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonH  ;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
                    BancoVirtual.PopularFilial("QAS CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
                }
            }
            EMP = USU = 0;
            string Aberto = VirCripWS.Uncrip(Chave);
            if ((Aberto.Length != 42) || (Aberto.Substring(33, 9) != "<-*WCF*->"))
            {
                Retorno = new RetornoServidorProcessos("Erro","Erro no formato da chave");
                Retorno.ok = false;
                Retorno.TipoDeErro = VirTipoDeErro.efetivo_bug;
                return false;
            }
            if (Usados.Contains(Aberto))
            {
                Retorno = new RetornoServidorProcessos("Erro", "Erro no sequencial");
                Retorno.ok = false;
                Retorno.TipoDeErro = VirTipoDeErro.recuperavel;
                return false;
            }
            Usados.Add(Aberto);
            //yyyyMMddHHmmssEMPUUUUUUUURRRRRRRR<-*WCF*->
            DateTime DataHora = new DateTime(int.Parse(Aberto.Substring(0, 4)), int.Parse(Aberto.Substring(4, 2)), int.Parse(Aberto.Substring(6, 2)), int.Parse(Aberto.Substring(8, 2)), int.Parse(Aberto.Substring(10, 2)), int.Parse(Aberto.Substring(12, 2)));
            if (DataHora.AddDays(1).AddHours(2) < DateTime.Now)
                return false;
            EMP = int.Parse(Aberto.Substring(14, 3));
            USU = int.Parse(Aberto.Substring(17, 8));
            DadosDaChamada = string.Format("DADOS DA CHAMADA:\r\nEMP:{0}\r\nUSU:{1}\r\n",
                                            EMP,
                                            USU);
            AddLog(string.Format("DADOS DA CHAMADA: EMP:{0} USU:{1}",
                                            EMP,
                                            USU));
            return true;
        }

        /// <summary>
        /// EMP
        /// </summary>
        protected int EMP;

        /// <summary>
        /// USU
        /// </summary>
        protected int USU;

        private FrameworkProc.EMPTProc _EMPTProc1;

        /// <summary>
        /// Objeto EMPTProc gerado com dados da chave
        /// </summary>
        protected FrameworkProc.EMPTProc EMPTProc1
        {
            get
            {
                if(_EMPTProc1 == null)
                    _EMPTProc1 = new FrameworkProc.EMPTProc(EMP == 1 ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial, USU);
                return _EMPTProc1;
            }
        }        

        /// <summary>
        /// Função básica de teste
        /// </summary>
        /// <param name="Chave"></param>
        /// <returns></returns>
        public RetornoServidorProcessos Teste(byte[] Chave)
        {            
            if (DecodificaChave(Chave))
                Retorno.Mensagem = string.Format("Teste ok: EMP {0} USU {1}", EMP, USU);            
            return Retorno;
        }


        #region LOG
        private static int Contador = 1;
        private bool GravarLog = true;
        private StringBuilder LOG;
        private DateTime Abertura;

        /// <summary>
        /// Inclui informações no log
        /// </summary>
        /// <param name="Linha"></param>
        protected void AddLog(string Linha)
        {
            if (!GravarLog)
                return;
            if (LOG == null)
            {
                if ((System.Web.Configuration.WebConfigurationManager.AppSettings["LOG"] == null) || (System.Web.Configuration.WebConfigurationManager.AppSettings["LOG"] == ""))
                {
                    GravarLog = false;
                    return;
                }
                LOG = new StringBuilder();
                Abertura = DateTime.Now;
                LOG.AppendFormat("{0:dd/MM/yyyy HH:mm:ss} Criado Log\r\n", Abertura);
            }
            LOG.AppendFormat("{0:HH:mm:ss} {1}\r\n", DateTime.Now, Linha);
        }
        
        /// <summary>
        /// Grava o log
        /// </summary>
        protected void GravaLog()
        {
            if (!GravarLog)
                return;            
            TimeSpan deltatempo = DateTime.Now - Abertura;
            AddLog(string.Format("FIM: {0:hh\\:mm\\:ss\\.ff}", deltatempo));
            if (deltatempo.TotalSeconds > 10)
                identificadorStatusArquivo = "#" + identificadorStatusArquivo + "#";
            if(deltatempo.TotalSeconds > 50)
                identificadorStatusArquivo = identificadorStatusArquivo.ToUpper();
            if (deltatempo.TotalMinutes > 3)
                identificadorStatusArquivo = "###" + identificadorStatusArquivo + "###";
            string Arqvuivo = string.Format("{0}WCF{1}_{2:yyyy_MM_dd_mm_ss}_{3:0000000}_DEC_{4:00000}_{5:0}_{6}.log",
                                            System.Web.Configuration.WebConfigurationManager.AppSettings["LOG"],
                                            Identificador13,
                                            DateTime.Now,
                                            Contador++,
                                            deltatempo.TotalSeconds,
                                            deltatempo.Milliseconds / 100,
                                            identificadorStatusArquivo);
            if (identificadorStatusArquivo != "ok")
                System.IO.File.WriteAllText(Arqvuivo, LOG.ToString());
        } 
        #endregion

        private string identificadorStatusArquivo = "ok";

        /// <summary>
        /// Função para reportar erros
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected RetornoServidorProcessos ReportaErro(Exception e)
        {
            string retorno = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, true, true, true);
            Retorno.ok = false;
            Retorno.Mensagem = string.Format("{0}\r\n{1}", DadosDaChamada , retorno);
            Retorno.MensagemSimplificada = VirExceptionProc.VirExceptionProc.RelatorioDeErroUsuario(e);
            Retorno.TipoDeErro = VirExceptionProc.VirExceptionProc.IdentificaTipoDeErro(e);
            AddLog(string.Format("ERRO:\r\n{0}",retorno));
            identificadorStatusArquivo = "erro";
            return Retorno;            
        }

    }
}
