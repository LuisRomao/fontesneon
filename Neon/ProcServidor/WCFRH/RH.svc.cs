﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using dllSefipProc;
using DPProc;
using WCPBase;
using VirExceptionProc;
using CompontesBasicosProc;

namespace WCFRH
{    
    /// <summary>
    /// Classe com funções RH para servidor de processos
    /// </summary>
    public class RH : WCPBase.WCPBase ,IRH
    {
        /// <summary>
        /// identifica a função no log
        /// </summary>
        protected override string Identificador13
        {
            get { return "RH___________"; }
        }
        
        /// <summary>
        /// Grava os dados da SEFIP
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="DSefip"></param>
        /// <param name="Comp"></param>
        /// <returns></returns>
        public RetornoServidorProcessos SefipProc_gravanobancoProc(byte[] Chave, dSefip DSefip, int Comp)
        {            
            try
            {
                if (!DecodificaChave(Chave))
                    return Retorno;
                SefipProc SefipProc1 = new SefipProc(EMP, USU);
                if (SefipProc1.gravanobancoProc(DSefip, Comp))
                    AddLog("OK");
                else
                {
                    Retorno.ok = false;
                    Retorno.Mensagem = "Erro ao gravar: Checar o motivo";
                    Retorno.MensagemSimplificada = "Erro ao gravar";
                    Retorno.TipoDeErro = VirTipoDeErro.efetivo_bug;
                }
                return Retorno;
            }
            catch (Exception e)
            {                
                return ReportaErro(e);                
            }
            finally
            {
                GravaLog();
            }
        }

        /// <summary>
        /// Grava os salários
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="RHT"></param>
        /// <returns></returns>
        public RetornoServidorProcessos RH_GravaSalario(byte[] Chave, int RHT)
        {
            try
            {
                if (!DecodificaChave(Chave))
                    return Retorno;
                DPProc.Salarios Salarios1 = new Salarios();
                //FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(EMP == 1 ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial, USU);
                if (Salarios1.Grava(EMPTProc1, RHT))
                {
                    AddLog("OK");
                }
                else
                {
                    Retorno.ok = false;
                    Retorno.Mensagem = "Erro ao gravar: Checar o motivo";
                    Retorno.MensagemSimplificada = "Erro ao gravar";
                    Retorno.TipoDeErro = VirTipoDeErro.efetivo_bug;
                }                
                return Retorno;
            }
            catch (Exception e)
            {
                return ReportaErro(e);
            }
            finally
            {
                GravaLog();
            }
        }
    }
}
