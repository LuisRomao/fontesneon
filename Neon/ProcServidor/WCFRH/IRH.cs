﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using dllSefipProc;

namespace WCFRH
{    
    /// <summary>
    /// Classe com funções RH para servidor de processos
    /// </summary>
    [ServiceContract]
    public interface IRH
    {
        /// <summary>
        /// Função básica de teste
        /// </summary>
        /// <param name="Chave"></param>
        /// <returns></returns>
        [OperationContract]
        CompontesBasicosProc.RetornoServidorProcessos Teste(byte[] Chave);

        /// <summary>
        /// Grava os dados da SEFIP
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="DSefip"></param>
        /// <param name="Comp"></param>
        /// <returns></returns>
        [OperationContract]
        CompontesBasicosProc.RetornoServidorProcessos SefipProc_gravanobancoProc(byte[] Chave, dSefip DSefip, int Comp);

        /// <summary>
        /// Grava os salários
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="RHT"></param>
        /// <returns></returns>
        [OperationContract]
        CompontesBasicosProc.RetornoServidorProcessos RH_GravaSalario(byte[] Chave, int RHT);
        
    }    
}
