﻿using System.ServiceModel;
using CompontesBasicosProc;
using ContaPagarProc;
using ContaPagarProc.LoteRepasse;

namespace WCFPeriodicoNota
{
    /// <summary>
    /// Classe para Nota e periódicos no Servidor de Processos
    /// </summary>
    [ServiceContract]
    public interface IPeriodicoNota
    {
        /// <summary>
        /// Teste básico
        /// </summary>
        /// <param name="Chave"></param>
        /// <returns></returns>
        [OperationContract]
        RetornoServidorProcessos Teste(byte[] Chave);        

        /// <summary>
        /// Cadastra próximo periódico
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="PGF"></param>
        /// <returns></returns>
        [OperationContract]
        RetornoServidorProcessos periodico_CadastrarProximo(byte[] Chave, int PGF);

        /// <summary>
        /// Grava alterações das notas
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="DNOtAs"></param>
        /// <param name="Justificativa"></param>
        /// <param name="PGF"></param>
        /// <param name="JustificativaDestrava"></param>
        /// <returns></returns>
        [OperationContract]
        RetornoServidorProcessos dNOtAs_GravaNoBanco(byte[] Chave, dNOtAs DNOtAs, string Justificativa, int? PGF, string JustificativaDestrava);

        /// <summary>
        /// Gera os cheques do repasse
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="dLoteRepasse"></param>
        /// <param name="FRNCom"></param>
        /// <param name="FRNSem"></param>
        /// <returns></returns>
        [OperationContract]
        RetornoServidorProcessos LoteRepasse_GeraChequesRepasse(byte[] Chave, dLoteRepasse dLoteRepasse, int FRNCom, int FRNSem);
    }
}
