﻿using System;
using CompontesBasicosProc;
using ContaPagarProc;
using ContaPagarProc.LoteRepasse;
using VirExceptionProc;
using AbstratosNeon;


namespace WCFPeriodicoNota
{
    /// <summary>
    /// Classe para Nota e periódicos no Servidor de Processos
    /// </summary>
    public class PeriodicoNota : WCPBase.WCPBase, IPeriodicoNota
    {
        /// <summary>
        /// Identificação para o log
        /// </summary>
        protected override string Identificador13
        {
            get { return "PeriodicoNota"; }
        }

        /// <summary>
        /// Cadastra próximo periódico
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="PGF"></param>
        /// <returns></returns>
        public RetornoServidorProcessos periodico_CadastrarProximo(byte[] Chave, int PGF)
        {
            try
            {
                if (!DecodificaChave(Chave))
                    return Retorno;
                //PagamentoPeriodicoProc periodico = new PagamentoPeriodicoProc(PGF, false, new FrameworkProc.EMPTProc(EMP == 1 ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial, USU));
                PagamentoPeriodicoProc periodico = new PagamentoPeriodicoProc(PGF, false, EMPTProc1);
                if (periodico.Encontrado)
                {
                    if (periodico.CadastrarProximo())
                        Retorno.Mensagem = string.Format("Retorno ok Descrição{0}", periodico.Descricao);
                    else
                        Retorno.Mensagem = string.Format("Retorno branco. Descrição{0}", periodico.Descricao);
                }
                else
                {
                    Retorno.ok = false;
                    Retorno.TipoDeErro = VirTipoDeErro.efetivo_bug;
                    Retorno.Mensagem = string.Format("Periódico não encontrado PGF = {0}",PGF);
                }
                return Retorno;
            }
            catch (Exception e)
            {                
                return ReportaErro(e);                
            }
            finally
            {
                GravaLog();
            }
        }

        /// <summary>
        /// Grava alterações das notas
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="DNOtAs"></param>
        /// <param name="Justificativa"></param>
        /// <param name="PGF"></param>
        /// <param name="JustificativaDestrava"></param>
        /// <returns></returns>
        public RetornoServidorProcessos dNOtAs_GravaNoBanco(byte[] Chave, dNOtAs DNOtAs, string Justificativa, int? PGF, string JustificativaDestrava)
        {
            try
            {
                if (!DecodificaChave(Chave))
                    return Retorno;
                DadosDaChamada += string.Format("Justificativa:{0}\r\nPGF:{1}\r\nJustificativaDestrava:{2}\r\n", Justificativa, PGF.GetValueOrDefault(0), JustificativaDestrava);
                AddLog(string.Format("Dados complementares Justificativa:{0} PGF:{1} JustificativaDestrava:{2}", Justificativa, PGF.GetValueOrDefault(0), JustificativaDestrava));
                //FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(EMP == 1 ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial, USU);
                PagamentoPeriodicoProc PagamentoPeriodicoDestravar = null;
                if (PGF.HasValue)
                    PagamentoPeriodicoDestravar = new PagamentoPeriodicoProc(PGF.Value, false, EMPTProc1);
                DNOtAs.EMPTProc1 = EMPTProc1;
                if (DNOtAs.GravaNoBanco(Justificativa, PagamentoPeriodicoDestravar, JustificativaDestrava))                
                    AddLog("OK");                                    
                else
                {
                    Retorno.ok = false;
                    Retorno.Mensagem = "Erro ao gravar: Checar o motivo";
                    Retorno.MensagemSimplificada = "Erro ao gravar";
                    Retorno.TipoDeErro = VirTipoDeErro.efetivo_bug;                    
                }
                return Retorno;
            }
            catch (Exception e)
            {
                return ReportaErro(e);                
            }
            finally
            {
                GravaLog();
            }
        }

        /// <summary>
        /// Gera os cheques do Repasse
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="dLoteRepasse"></param>
        /// <param name="FRNCom"></param>
        /// <param name="FRNSem"></param>
        /// <returns></returns>
        public RetornoServidorProcessos LoteRepasse_GeraChequesRepasse(byte[] Chave, dLoteRepasse dLoteRepasse, int FRNCom,int FRNSem)
        {
            try
            {
                if (!DecodificaChave(Chave))
                    return Retorno;
                DadosDaChamada += string.Format("GeraChequesRepasse. Registros:{0} FRNCom:{1} FRNSem:{2}\r\n", dLoteRepasse.Importar.Count, FRNCom, FRNSem);                
                LoteRepasse LoteRepasse1 = new LoteRepasse(EMPTProc1, dLoteRepasse);
                ABS_Fornecedor FornecedorCom = ABS_Fornecedor.GetFornecedor(FRNCom);
                ABS_Fornecedor FornecedorSem = ABS_Fornecedor.GetFornecedor(FRNSem);
                if (LoteRepasse1.GeraChequesRepasse(FornecedorCom,FornecedorSem))
                    AddLog("OK");                
                else
                {
                    Retorno.ok = false;
                    Retorno.Mensagem = "Erro ao gerar: Checar o motivo";
                    Retorno.MensagemSimplificada = "Erro ao gerar";
                    Retorno.TipoDeErro = VirTipoDeErro.efetivo_bug;
                }
                return Retorno;
            }
            catch (Exception e)
            {
                try
                {
                    string ArqvuivoDados = string.Format("{0}WCF{1}_{2:yyyy_MM_dd_mm_ss}_LoteRepasse_GeraChequesRepasse_dLoteRepasse.xml",
                                                System.Web.Configuration.WebConfigurationManager.AppSettings["LOG"],
                                                Identificador13,
                                                DateTime.Now
                                                );
                    dLoteRepasse.WriteXml(ArqvuivoDados);
                }
                catch 
                {                    
                }
                return ReportaErro(e);
            }
            finally
            {
                GravaLog();
            }
        }
    }
}
