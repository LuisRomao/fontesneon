﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicosProc;


namespace TesteWCF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private SPeriodicoNota.PeriodicoNotaClient _Periodiconota;

        private SPeriodicoNota.PeriodicoNotaClient Periodiconota
        {
            get 
            { 
                if(_Periodiconota == null)
                    _Periodiconota = new SPeriodicoNota.PeriodicoNotaClient(chLocal.Checked ? "PeriodicoNotaLocal" : "PeriodicoNotaRemota_ProcH");
                return _Periodiconota;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {            
            string resposta = string.Format("{0:mm:ss} início\r\n",System.DateTime.Now);
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            Periodiconota.Close();
            _Periodiconota = null;
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            resposta += string.Format("{0:mm:ss}: |{1}|\r\n", System.DateTime.Now, Periodiconota.Teste(Criptografa(1, 30)));
            Periodiconota.Close();
            _Periodiconota = null;
            memoEdit1.Text = resposta;
        }

        private int staticR;

        private byte[] Criptografa(int EMP,int USU)
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}{3:00000000}<-*WCF*->", DateTime.Now, EMP, USU, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            int PGF = (int)spinPGF.Value;
            string retono = string.Format("Resultado:\r\n|{0}|\r\n|{1}|\r\n", Periodiconota.periodico_CadastrarProximo(Criptografa(1, 30), PGF),
                                                                              Periodiconota.periodico_CadastrarProximo(Criptografa(3,30), PGF));
            memoEdit1.Text = retono;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            string URL = "ProcH";
            //URL = "BasicHttpBinding_IService1";

            

            /*
            <endpoint address="http://localhost:65443/Service1.svc" binding="basicHttpBinding"
                bindingConfiguration="BasicHttpBinding_IService1" contract="ServiceReference1.IService1"
                name="BasicHttpBinding_IService1" />
            */

            ServiceReference1.Service1Client S1 = new ServiceReference1.Service1Client(URL);
            memoEdit1.Text += S1.GetData("S1 Chamada 1");            
            Application.DoEvents();
            memoEdit1.Text += S1.GetData("S1 Chamada 2");
            S1.Close();
            Application.DoEvents();
            ServiceReference1.Service1Client S2 = new ServiceReference1.Service1Client(URL);
            memoEdit1.Text += S2.GetData("S2 Chamada 1");            
            Application.DoEvents();
            memoEdit1.Text += S2.GetData("S2 Chamada 2");
            S2.Close();
        }

        private bool Parar;

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            Parar = false;
            for (int i = 1; i < spinEdit1.Value; i++)
            {
                if (Parar)
                    break;
                Application.DoEvents();
                string retono = string.Format("Resultado ({0}):\r\n|{1}|\r\n|{2}|\r\n", i,
                                                                                        Periodiconota.periodico_CadastrarProximo(Criptografa(1, 30), i),
                                                                                        Periodiconota.periodico_CadastrarProximo(Criptografa(3, 30), i));
                memoEdit1.Text = retono + memoEdit1.Text;
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Parar = true;
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            string URL = "WCFRHL";
            SRH.RHClient SRH1 = new SRH.RHClient(URL);
            RetornoServidorProcessos Retorno = SRH1.Teste(Criptografa(1, 1));                      
            memoEdit1.Text = Retorno.Mensagem;                        
            memoEdit1.Text += "\r\n";            
            SRH1.Close();            
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoLocal("Teste Blocqueio");
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update cheques set CHEFavorecido = 'teste' where che = @P1",spinEdit2.Value);
            simpleButtonC.Enabled = simpleButtonR.Enabled = true;
        }

        private void simpleButtonC_Click(object sender, EventArgs e)
        {
            simpleButtonC.Enabled = simpleButtonR.Enabled = false;
            VirMSSQL.TableAdapter.STTableAdapter.Commit();
        }

        private void simpleButtonR_Click(object sender, EventArgs e)
        {
            simpleButtonC.Enabled = simpleButtonR.Enabled = false;
            VirMSSQL.TableAdapter.STTableAdapter.RollBack("teste");
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            string URL = "PeriodicoNotaLocal";
            SPeriodicoNota.PeriodicoNotaClient SPeriodicoNota1 = new SPeriodicoNota.PeriodicoNotaClient(URL);
            RetornoServidorProcessos Retorno = SPeriodicoNota1.Teste(Criptografa(1, 1));
            memoEdit1.Text = Retorno.Mensagem;
            memoEdit1.Text += "\r\n";
            SPeriodicoNota1.Close();
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoLocal("Teste Blocqueio");
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update pagamentos set PAGValor = 0 where PAG = @P1", spinEdit3.Value);
            simpleButtonC.Enabled = simpleButtonR.Enabled = true;
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {   
         
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update pagamentos set PAGValor = 0 where PAG = @P1", spinEdit4.Value);
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoLocal("Teste Blocqueio");
            memoEdit1.Text = string.Format("Bloqueio de {0}\r\n",spinEdit3.Value);            
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update pagamentos set PAGValor = 0 where PAG = @P1", spinEdit3.Value);
            memoEdit1.Text += string.Format("OK\r\n");            
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            try
            {
                memoEdit1.Text += string.Format("Bloqueio de {0}\r\n", spinEdit4.Value);
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update pagamentos set PAGValor = 0 where PAG = @P1", spinEdit4.Value);
                memoEdit1.Text += string.Format("OK\r\n");
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                memoEdit1.Text += string.Format("commit\r\n");
            }
            catch (Exception ex)
            {
                memoEdit1.Text += string.Format("ERRO:\r\n{0}",VirExceptionProc.VirExceptionProc.RelatorioDeErro(ex,false,false,false));            
            }
        }

        private ClientWCFFtp.ClientFTP Emissor;

        private DateTime Inicio;

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            if (Emissor == null)
                Emissor = new ClientWCFFtp.ClientFTP(checkBoxProducao.Checked, 0, 0, checkBoxSinc.Checked);
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Inicio = DateTime.Now;

                foreach (string FileName in openFileDialog1.FileNames)
                    Emissor.Put(FileName,
                                textEdit1.Text == "" ? null:textEdit1.Text,
                                checkBoxAbsoluto.Checked,  
                                checkBoxEnv.Checked ? ClientWCFFtp.ClientFTP.Tipoenvio.envio_com_compressao : ClientWCFFtp.ClientFTP.Tipoenvio.envio_sem_compressao,
                                checkBoxEst.Checked ? ClientWCFFtp.ClientFTP.TipoArmazenagem.Armazenagem_com_compressao : ClientWCFFtp.ClientFTP.TipoArmazenagem.Armazenagem_sem_compressao);

                //FTPteste. FTPteste.FtpClient Teste = new FTPteste.FtpClient();
                
                
                
                if (!checkBoxSinc.Checked)
                {
                    progressBarControl1.Properties.Maximum = (int)Emissor.TotalPut;
                    progressBarControl1.Position = 0;
                    progressBarControl1.Visible = true;
                    //Monitorado1 = Emissor;
                    //timer1.Enabled = true;
                }
                else
                {
                    TimeSpan deltaTempo = DateTime.Now - Inicio;
                    memoEdit2.Text += string.Format("{0}\r\nTempo: {1:n1} segundos\r\n", openFileDialog1.FileName, deltaTempo.TotalSeconds);
                }
            }
        }

        private void simpleButton14_Click(object sender, EventArgs e)
        {
            if (Emissor == null)
                Emissor = new ClientWCFFtp.ClientFTP(checkBoxProducao.Checked, 0, 0, checkBoxSinc.Checked);

            foreach (ClientWCFFtp.DadosArquivo Arq in Emissor.List(textEdit1.Text, checkBoxAbsoluto.Checked, textEditMacara.Text))
                memoEdit2.Text += string.Format("\r\n{0,-80}\t{1,-30}\t{2,7:n0}\t{3:dd/MM/yyyy HH:mm:ss} ({4:n})", Arq.NomeCompleto, Arq.Nome, Arq.TamanhoStr, Arq.Data,Arq.Tamanho);
        }
    }
}
