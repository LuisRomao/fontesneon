﻿namespace TesteWCF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.spinPGF = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.chLocal = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButtonC = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonR = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.label2 = new System.Windows.Forms.Label();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.spinEdit3 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.spinEdit4 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.textEditMacara = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.checkBoxAbsoluto = new System.Windows.Forms.CheckBox();
            this.checkBoxProducao = new System.Windows.Forms.CheckBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.checkBoxSinc = new System.Windows.Forms.CheckBox();
            this.checkBoxEst = new System.Windows.Forms.CheckBox();
            this.checkBoxEnv = new System.Windows.Forms.CheckBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.spinPGF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chLocal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMacara.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(28, 28);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(259, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Teste Chamada PeriodicoNota";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(28, 57);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(132, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Cadastra Proximo";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(166, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "PGF:";
            // 
            // spinPGF
            // 
            this.spinPGF.EditValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.spinPGF.Location = new System.Drawing.Point(203, 59);
            this.spinPGF.Name = "spinPGF";
            this.spinPGF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinPGF.Size = new System.Drawing.Size(84, 20);
            this.spinPGF.TabIndex = 3;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(28, 164);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(259, 23);
            this.simpleButton3.TabIndex = 4;
            this.simpleButton3.Text = "Teste básico";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // memoEdit1
            // 
            this.memoEdit1.EditValue = " - ";
            this.memoEdit1.Location = new System.Drawing.Point(27, 209);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit1.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.memoEdit1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.memoEdit1.Size = new System.Drawing.Size(538, 472);
            this.memoEdit1.TabIndex = 5;
            // 
            // chLocal
            // 
            this.chLocal.EditValue = true;
            this.chLocal.Location = new System.Drawing.Point(317, 28);
            this.chLocal.Name = "chLocal";
            this.chLocal.Properties.Caption = "Local";
            this.chLocal.Size = new System.Drawing.Size(75, 19);
            this.chLocal.TabIndex = 6;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(401, 137);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(164, 50);
            this.simpleButton4.TabIndex = 7;
            this.simpleButton4.Text = "Parar";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(401, 81);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(83, 50);
            this.simpleButton5.TabIndex = 8;
            this.simpleButton5.Text = "Testar";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(490, 111);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Size = new System.Drawing.Size(75, 20);
            this.spinEdit1.TabIndex = 9;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(28, 687);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(259, 23);
            this.simpleButton6.TabIndex = 10;
            this.simpleButton6.Text = "Teste Chamada RH";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(583, 210);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(161, 23);
            this.simpleButton7.TabIndex = 11;
            this.simpleButton7.Text = "Trava CHE";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // spinEdit2
            // 
            this.spinEdit2.EditValue = new decimal(new int[] {
            983863,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(787, 213);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit2.Size = new System.Drawing.Size(100, 20);
            this.spinEdit2.TabIndex = 12;
            // 
            // simpleButtonC
            // 
            this.simpleButtonC.Enabled = false;
            this.simpleButtonC.Location = new System.Drawing.Point(630, 342);
            this.simpleButtonC.Name = "simpleButtonC";
            this.simpleButtonC.Size = new System.Drawing.Size(114, 23);
            this.simpleButtonC.TabIndex = 13;
            this.simpleButtonC.Text = "Commit";
            this.simpleButtonC.Click += new System.EventHandler(this.simpleButtonC_Click);
            // 
            // simpleButtonR
            // 
            this.simpleButtonR.Enabled = false;
            this.simpleButtonR.Location = new System.Drawing.Point(753, 342);
            this.simpleButtonR.Name = "simpleButtonR";
            this.simpleButtonR.Size = new System.Drawing.Size(114, 23);
            this.simpleButtonR.TabIndex = 14;
            this.simpleButtonR.Text = "RollBack";
            this.simpleButtonR.Click += new System.EventHandler(this.simpleButtonR_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(28, 716);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(259, 23);
            this.simpleButton8.TabIndex = 15;
            this.simpleButton8.Text = "Teste Chamada Periodiconota";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(750, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "CHE:";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(583, 239);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(161, 23);
            this.simpleButton9.TabIndex = 17;
            this.simpleButton9.Text = "Edita PAG";
            this.simpleButton9.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(750, 244);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "PAG:";
            // 
            // spinEdit3
            // 
            this.spinEdit3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit3.Location = new System.Drawing.Point(787, 241);
            this.spinEdit3.Name = "spinEdit3";
            this.spinEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit3.Size = new System.Drawing.Size(100, 20);
            this.spinEdit3.TabIndex = 19;
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(583, 268);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(161, 23);
            this.simpleButton10.TabIndex = 20;
            this.simpleButton10.Text = "Segundo PAG para deadlock";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(750, 273);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "PAG:";
            // 
            // spinEdit4
            // 
            this.spinEdit4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit4.Location = new System.Drawing.Point(787, 270);
            this.spinEdit4.Name = "spinEdit4";
            this.spinEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit4.Size = new System.Drawing.Size(100, 20);
            this.spinEdit4.TabIndex = 22;
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(583, 432);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(263, 23);
            this.simpleButton11.TabIndex = 23;
            this.simpleButton11.Text = "Segundo PAG para deadlock";
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(583, 461);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(263, 23);
            this.simpleButton12.TabIndex = 24;
            this.simpleButton12.Text = "Segundo PAG para deadlock";
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton12_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(923, 808);
            this.xtraTabControl1.TabIndex = 25;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.memoEdit1);
            this.xtraTabPage1.Controls.Add(this.simpleButton12);
            this.xtraTabPage1.Controls.Add(this.simpleButton1);
            this.xtraTabPage1.Controls.Add(this.simpleButton11);
            this.xtraTabPage1.Controls.Add(this.simpleButton2);
            this.xtraTabPage1.Controls.Add(this.spinEdit4);
            this.xtraTabPage1.Controls.Add(this.label1);
            this.xtraTabPage1.Controls.Add(this.label4);
            this.xtraTabPage1.Controls.Add(this.spinPGF);
            this.xtraTabPage1.Controls.Add(this.simpleButton10);
            this.xtraTabPage1.Controls.Add(this.simpleButton3);
            this.xtraTabPage1.Controls.Add(this.spinEdit3);
            this.xtraTabPage1.Controls.Add(this.chLocal);
            this.xtraTabPage1.Controls.Add(this.label3);
            this.xtraTabPage1.Controls.Add(this.simpleButton4);
            this.xtraTabPage1.Controls.Add(this.simpleButton9);
            this.xtraTabPage1.Controls.Add(this.simpleButton5);
            this.xtraTabPage1.Controls.Add(this.label2);
            this.xtraTabPage1.Controls.Add(this.spinEdit1);
            this.xtraTabPage1.Controls.Add(this.simpleButtonR);
            this.xtraTabPage1.Controls.Add(this.simpleButton8);
            this.xtraTabPage1.Controls.Add(this.simpleButtonC);
            this.xtraTabPage1.Controls.Add(this.simpleButton6);
            this.xtraTabPage1.Controls.Add(this.spinEdit2);
            this.xtraTabPage1.Controls.Add(this.simpleButton7);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(917, 780);
            this.xtraTabPage1.Text = "Geral";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.panelControl1);
            this.xtraTabPage2.Controls.Add(this.panelControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(917, 780);
            this.xtraTabPage2.Text = "FTP";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.memoEdit2);
            this.panelControl1.Controls.Add(this.progressBarControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 93);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(917, 687);
            this.panelControl1.TabIndex = 12;
            // 
            // memoEdit2
            // 
            this.memoEdit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit2.EditValue = "-";
            this.memoEdit2.Location = new System.Drawing.Point(2, 32);
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit2.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.memoEdit2.Properties.Appearance.Options.UseFont = true;
            this.memoEdit2.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit2.Size = new System.Drawing.Size(913, 653);
            this.memoEdit2.TabIndex = 12;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.progressBarControl1.Location = new System.Drawing.Point(2, 2);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(913, 30);
            this.progressBarControl1.TabIndex = 11;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.textEditMacara);
            this.panelControl2.Controls.Add(this.simpleButton14);
            this.panelControl2.Controls.Add(this.checkBoxAbsoluto);
            this.panelControl2.Controls.Add(this.checkBoxProducao);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.textEdit1);
            this.panelControl2.Controls.Add(this.simpleButton13);
            this.panelControl2.Controls.Add(this.checkBoxSinc);
            this.panelControl2.Controls.Add(this.checkBoxEst);
            this.panelControl2.Controls.Add(this.checkBoxEnv);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(917, 93);
            this.panelControl2.TabIndex = 13;
            // 
            // textEditMacara
            // 
            this.textEditMacara.EditValue = "*.*";
            this.textEditMacara.Location = new System.Drawing.Point(579, 36);
            this.textEditMacara.Name = "textEditMacara";
            this.textEditMacara.Size = new System.Drawing.Size(331, 20);
            this.textEditMacara.TabIndex = 16;
            // 
            // simpleButton14
            // 
            this.simpleButton14.Location = new System.Drawing.Point(498, 34);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(75, 23);
            this.simpleButton14.TabIndex = 15;
            this.simpleButton14.Text = "List";
            this.simpleButton14.Click += new System.EventHandler(this.simpleButton14_Click);
            // 
            // checkBoxAbsoluto
            // 
            this.checkBoxAbsoluto.AutoSize = true;
            this.checkBoxAbsoluto.Location = new System.Drawing.Point(377, 69);
            this.checkBoxAbsoluto.Name = "checkBoxAbsoluto";
            this.checkBoxAbsoluto.Size = new System.Drawing.Size(68, 17);
            this.checkBoxAbsoluto.TabIndex = 14;
            this.checkBoxAbsoluto.Text = "Absoluto";
            this.checkBoxAbsoluto.UseVisualStyleBackColor = true;
            // 
            // checkBoxProducao
            // 
            this.checkBoxProducao.AutoSize = true;
            this.checkBoxProducao.Location = new System.Drawing.Point(38, 11);
            this.checkBoxProducao.Name = "checkBoxProducao";
            this.checkBoxProducao.Size = new System.Drawing.Size(71, 17);
            this.checkBoxProducao.TabIndex = 13;
            this.checkBoxProducao.Text = "produção";
            this.checkBoxProducao.UseVisualStyleBackColor = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(38, 70);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(64, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "Nome remoto";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(117, 67);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(254, 20);
            this.textEdit1.TabIndex = 11;
            // 
            // simpleButton13
            // 
            this.simpleButton13.Location = new System.Drawing.Point(38, 34);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(75, 23);
            this.simpleButton13.TabIndex = 0;
            this.simpleButton13.Text = "Put";
            this.simpleButton13.Click += new System.EventHandler(this.simpleButton13_Click);
            // 
            // checkBoxSinc
            // 
            this.checkBoxSinc.AutoSize = true;
            this.checkBoxSinc.Checked = true;
            this.checkBoxSinc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSinc.Location = new System.Drawing.Point(134, 40);
            this.checkBoxSinc.Name = "checkBoxSinc";
            this.checkBoxSinc.Size = new System.Drawing.Size(66, 17);
            this.checkBoxSinc.TabIndex = 8;
            this.checkBoxSinc.Text = "sincrono";
            this.checkBoxSinc.UseVisualStyleBackColor = true;
            // 
            // checkBoxEst
            // 
            this.checkBoxEst.AutoSize = true;
            this.checkBoxEst.Location = new System.Drawing.Point(206, 47);
            this.checkBoxEst.Name = "checkBoxEst";
            this.checkBoxEst.Size = new System.Drawing.Size(97, 17);
            this.checkBoxEst.TabIndex = 10;
            this.checkBoxEst.Text = "Estocar Zipado";
            this.checkBoxEst.UseVisualStyleBackColor = true;
            // 
            // checkBoxEnv
            // 
            this.checkBoxEnv.AutoSize = true;
            this.checkBoxEnv.Location = new System.Drawing.Point(206, 27);
            this.checkBoxEnv.Name = "checkBoxEnv";
            this.checkBoxEnv.Size = new System.Drawing.Size(91, 17);
            this.checkBoxEnv.TabIndex = 9;
            this.checkBoxEnv.Text = "Enviar Zipado";
            this.checkBoxEnv.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 808);
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.spinPGF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chLocal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMacara.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SpinEdit spinPGF;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.CheckEdit chLocal;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonC;
        private DevExpress.XtraEditors.SimpleButton simpleButtonR;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SpinEdit spinEdit3;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.SpinEdit spinEdit4;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.CheckBox checkBoxEst;
        private System.Windows.Forms.CheckBox checkBoxEnv;
        private System.Windows.Forms.CheckBox checkBoxSinc;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.MemoEdit memoEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.CheckBox checkBoxProducao;
        private System.Windows.Forms.CheckBox checkBoxAbsoluto;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.TextEdit textEditMacara;
    }
}

