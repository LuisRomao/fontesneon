﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using VirDB.Bancovirtual;

namespace TesteWCF
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            BancoVirtual.Popular("QAS - SBC", @"Data Source=cp.neonimoveis.com;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
            BancoVirtual.Popular("QAS - SA", @"Data Source=cp.neonimoveis.com;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
