﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ClientWCFFtp
{
    /// <summary>
    /// Dados dos arquivos
    /// </summary>
    public class DadosArquivo
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Nome"></param>
        public DadosArquivo(string Nome)
        {
            FileInfo info = new FileInfo(Nome);
            Tamanho = info.Length;            
            Nome = info.Name;
            NomeCompleto = info.FullName;
            Data = info.LastWriteTime;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Tamanho"></param>
        /// <param name="Nome"></param>
        /// <param name="NomeCompleto"></param>
        /// <param name="Data"></param>
        public DadosArquivo(long Tamanho, string Nome,string NomeCompleto,DateTime Data)
        {            
            this.Tamanho = Tamanho;
            this.Nome = Nome;
            this.NomeCompleto = NomeCompleto;
            this.Data = Data;
        }

        
        /// <summary>
        /// Tamanho do arquivo
        /// </summary>        
        public long Tamanho;

        /// <summary>
        /// Nome Arquivo
        /// </summary>
        public string Nome;

        /// <summary>
        /// Nome completo
        /// </summary>
        public string NomeCompleto;

        /// <summary>
        /// Data do arquivo
        /// </summary>
        public DateTime Data;

        /// <summary>
        /// Tamanho em string
        /// </summary>
        public string TamanhoStr
        {
            get
            {
                if (Tamanho < 1024)
                    return string.Format("{0} b", Tamanho);
                else
                {
                    decimal TamanhoD = Math.Round(Tamanho / 1024M,1);
                    if (TamanhoD < 1024)
                        return string.Format("{0:n1} K", TamanhoD);
                    else
                    {
                        TamanhoD = Math.Round(TamanhoD / 1024M, 1);
                        if (TamanhoD < 1024)
                            return string.Format("{0:n1} M", TamanhoD);
                        else
                        {
                            TamanhoD = Math.Round(TamanhoD / 1024M, 1);
                            return string.Format("{0:n1} G", TamanhoD);
                        }
                    }
                }
            }
        }
    }
}
