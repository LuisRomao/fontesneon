﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace ClientWCFFtp
{
    /// <summary>
    /// Cliente WCFFtp
    /// </summary>
    public class ClientFTP : IDisposable
    {
        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (st != null)
            {
                st.Dispose();
                st = null;
            }
            if (stRetorno != null)
            {
                stRetorno.Dispose();
                stRetorno = null;
            }
        }

        private static int staticR;

        private int EMP=0;

        private int USU=0;

        private byte[] Criptografa()
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}{3:00000000}<-*WCF*->", DateTime.Now, EMP, USU, staticR++);            
            return VirCrip.VirCripWS.Crip(Aberto);
        }

        private class itAgenda
        {
            public string FileName;
            public Stream File; 
            public string NomeRemoto;
            public bool Absoluto;
            public TipoArmazenagem EstocarZipado;
            public Tipoenvio EnviarZipado;

            public itAgenda(string FileName, Stream File, string NomeRemoto,bool Absoluto, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado)
            {
                this.FileName = FileName;
                this.File = File;
                this.NomeRemoto = NomeRemoto;
                this.Absoluto = Absoluto;
                this.EnviarZipado = EnviarZipado;
                this.EstocarZipado = EstocarZipado;
            }
        }

        private List<itAgenda> Agenda;
       
        private bool Assincrono = false;

        private Ftp.FtpClient FtpClient;

        private Thread ThRecFTP;

        /// <summary>
        /// Cria um novo mecanismo de envio/rec de arquivos
        /// </summary>
        /// <param name="Producao"></param>
        /// <param name="USU"></param>
        /// <param name="EMP"></param>
        /// <param name="Sincrono"></param>
        public ClientFTP(bool Producao, int USU = 0, int EMP = 0, bool Sincrono = true)
        {
            Assincrono = !Sincrono;
            this.USU = USU;
            this.EMP = EMP;
            FtpClient = new Ftp.FtpClient(Producao ? "FtpP" : "FtpH");
            
            Agenda = new List<itAgenda>();
            if (!Sincrono)
            {
                parar = false;
                ThRecFTP = new Thread(LoopAssicrono);
                PutAtivo = true;
                ThRecFTP.Start();
            }

        }
      
        private const int TamanhoMaxPacote = 0xFFFF;

        /// <summary>
        /// para o processo
        /// </summary>
        public bool parar;

        private void LoopAssicrono()
        {
            while (!parar)
            {
                if (Agenda.Count > 0)
                {
                    ThRecFTP.Priority = ThreadPriority.Normal;
                    itAgenda DaVez = Agenda[0];
                    Agenda.Remove(DaVez);
                    if (DaVez.FileName != null)
                    {
                        if (DaVez.NomeRemoto == null)
                            DaVez.NomeRemoto = Path.GetFileName(DaVez.FileName);
                        using (FileStream fs = new FileStream(DaVez.FileName, FileMode.Open))
                        {
                            PutEfetivo(fs, DaVez.NomeRemoto,DaVez.Absoluto, DaVez.EnviarZipado, DaVez.EstocarZipado);
                        }
                    }
                    else
                        PutEfetivo(DaVez.File, DaVez.NomeRemoto,DaVez.Absoluto, DaVez.EnviarZipado, DaVez.EstocarZipado);
                }
                else
                {
                    Thread.Sleep(1000);
                    ThRecFTP.Priority = ThreadPriority.Lowest;
                }

            }
        }

        #region PUT

        /// <summary>
        /// Envia Arquivo
        /// </summary>
        /// <param name="FileName">Arquivo com path</param>
        /// <param name="Absoluto"></param>
        /// <param name="Pasta">pasta do servidor</param>
        /// <returns></returns>
        public bool PutPasta(string FileName, string Pasta,bool Absoluto = false)
        {
            if ((Pasta != "") && (!Pasta.EndsWith("\\")))
                Pasta += "\\";
            string ArquvidoRemoto = string.Format("{0}{1}",Pasta, Path.GetFileName(FileName));
            return Put(FileName, ArquvidoRemoto,Absoluto);
        }

        /// <summary>
        /// Envia Arquivo
        /// </summary>
        /// <param name="FileName">Nome do arquivo</param>
        /// <param name="NomeRemoto">Nome que será dado ao arquivo remoto, com a path a partir da referencia no webconfig</param>
        /// <param name="Absoluto"></param>
        /// <param name="EnviarZipado">Envia zipado</param>
        /// <param name="EstocarZipado">Guardar zipado</param>
        /// <returns></returns>
        public bool Put(string FileName, string NomeRemoto = null, bool Absoluto= false, Tipoenvio EnviarZipado = Tipoenvio.envio_com_compressao, TipoArmazenagem EstocarZipado = TipoArmazenagem.Armazenagem_sem_compressao)
        {
            if (Assincrono)
            {
                Agenda.Add(new itAgenda(FileName, null, NomeRemoto,Absoluto, EnviarZipado, EstocarZipado));
                return true;
            }
            else
            {
                bool retorno = false;
                if (NomeRemoto == null)
                    NomeRemoto = Path.GetFileName(FileName);
                using (FileStream fs = new FileStream(FileName, FileMode.Open))
                {
                    retorno = Put(fs, NomeRemoto,Absoluto, EnviarZipado, EstocarZipado);
                }
                return retorno;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="File"></param>
        /// <param name="NomeRemoto"></param>
        /// <param name="Absoluto"></param>
        /// <param name="EnviarZipado"></param>
        /// <param name="EstocarZipado"></param>
        /// <returns></returns>
        public bool Put (Stream File, string NomeRemoto,bool Absoluto = false, Tipoenvio EnviarZipado = Tipoenvio.envio_com_compressao, TipoArmazenagem EstocarZipado = TipoArmazenagem.Armazenagem_sem_compressao)
        {
            if (Assincrono)
            {
                Agenda.Add(new itAgenda(null,File,NomeRemoto,Absoluto, EnviarZipado,EstocarZipado));
                return true;
            }
            else
                return PutEfetivo(File, NomeRemoto, Absoluto, EnviarZipado, EstocarZipado);
        }

        /// <summary>
        /// Envia Arquivo
        /// </summary>
        /// <param name="File"></param>
        /// <param name="NomeRemoto"></param>
        /// <param name="Absoluto"></param>
        /// <param name="EnviarZipado"></param>
        /// <param name="EstocarZipado"></param>
        /// <returns></returns>
        private bool PutEfetivo(Stream File, string NomeRemoto, bool Absoluto, Tipoenvio EnviarZipado,TipoArmazenagem EstocarZipado)
        {
            if (EnviarZipado == Tipoenvio.envio_com_compressao)
            {
                MemoryStream MSZ = new MemoryStream();
                VirZIP.zip.Execute(File, MSZ, System.IO.Compression.CompressionMode.Compress);                
                if (MSZ.Length < File.Length)
                {
                    st = MSZ;
                    File.Close();                    
                }
                else
                {
                    EnviarZipado = Tipoenvio.envio_sem_compressao;
                    st = File;
                }                
            }
            else
                st = File;
            st.Position = 0;

            if (EnviarZipado == Tipoenvio.envio_sem_compressao)
                EstocarZipado = TipoArmazenagem.Armazenagem_sem_compressao;
            
            Saldo = TotalPut = st.Length;

            bool Retorno = false;
            Codigostr = null;
            try
            {
                if(Absoluto)
                    Codigostr = FtpClient.PutAbreABS(Criptografa(), NomeRemoto, Saldo, (Ftp.Tipoenvio)EnviarZipado, (Ftp.TipoArmazenagem)EstocarZipado);
                else
                    Codigostr = FtpClient.PutAbre(Criptografa(), NomeRemoto, Saldo, (Ftp.Tipoenvio)EnviarZipado, (Ftp.TipoArmazenagem)EstocarZipado);
                byte[] pacote;

                using (BinaryReader BR = new BinaryReader(st))
                {
                    int i = 1;
                    while (Saldo > 0)
                    {
                        int TamanhoPacote = Saldo > TamanhoMaxPacote ? TamanhoMaxPacote : (int)Saldo;
                        Saldo -= TamanhoPacote;
                        pacote = BR.ReadBytes(TamanhoPacote);
                        FtpClient.PutPacote(Codigostr, i, pacote);
                        i++;
                    }
                }
                
                Retorno = true;
            }
            finally
            {
                if (Codigostr != null)
                    FtpClient.PutFecha(Codigostr);
            }
            return Retorno;                       
        }


        #endregion

        #region GET


        /// <summary>
        /// Recebe um arquivo
        /// </summary>
        /// <param name="FileName">Nome do arquivo</param>
        /// <param name="NomeRemoto">Nome do arquivo remoto</param>        
        /// <param name="Absoluto"></param>
        /// <returns></returns>
        public bool Get(string FileName, string NomeRemoto = null, bool Absoluto = false)
        {
            bool retorno = false;
            if (NomeRemoto == null)
                NomeRemoto = Path.GetFileName(FileName);
            if (pathTMP == null)
                pathTMP = Path.GetDirectoryName(FileName);
            Stream fs = new FileStream(FileName, FileMode.Create);            
            retorno = Get(ref fs, NomeRemoto, Absoluto);            
            fs.Close();
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        public string pathTMP = null;

        private int numerador = 0;

        /// <summary>
        /// Recebe um arquivo
        /// </summary>
        /// <param name="File">Stream com o arquivo - pode ser null</param>
        /// <param name="NomeRemoto">Nome do arquivo remoto</param>  
        /// <param name="Absoluto"></param>
        /// <returns></returns>
        public bool Get(ref Stream File, string NomeRemoto, bool Absoluto = false)
        {
            string ArquivoTMP = null;
            if(Absoluto)
                Codigostr = FtpClient.GetAbreABS(out TotalPut, out zipado,Criptografa(), NomeRemoto);
            else
                Codigostr = FtpClient.GetAbre(out TotalPut, out zipado, Criptografa(), NomeRemoto);            
            if (TotalPut == 0)
            {
                UltimoErro = Codigostr;
                return false;
            }
            Saldo = TotalPut;

            if (File == null)
                st = new MemoryStream();
            else
                st = File;

            if (zipado)
            {
                stRetorno = st;
                if (TotalPut < 100000000)
                    st = new MemoryStream();
                else
                {                    
                    ArquivoTMP = string.Format("{0}\\{1:yyyyMMddHHmmss}_{2}.$$$",pathTMP, DateTime.Now, numerador++);                    
                    st = new FileStream(ArquivoTMP, FileMode.Create);
                }
            }          

            //if (Assincrono)
            //{
            //    Thread ThRecFTP = new Thread(Getth);
            //    PutAtivo = true;
            //    ThRecFTP.Start();                
            //    return true;
            //}
            //else
            //{
            try
            {
                ResultadoPut = false;
                int i = 1;
                while (Saldo > 0)
                {                    
                    byte[] pacote = FtpClient.GetPacote(Codigostr, i++);
                    if (pacote == null)
                        break;
                    Saldo -= pacote.Length;
                    using (MemoryStream ms = new MemoryStream(pacote))
                        ms.WriteTo(st);
                };                
                if (Saldo == 0)
                {
                    ResultadoPut = FtpClient.GetFecha(Codigostr);                  
                    if (zipado)
                    {
                        st.Position = 0;
                        VirZIP.zip.Execute(stRetorno, st, System.IO.Compression.CompressionMode.Decompress);
                        st.Close();                        
                        st = null;                        
                    }                    
                }
            }
            finally
            {
                PutAtivo = false;
                st = null;
                stRetorno = null;
                if((ArquivoTMP != null) && System.IO.File.Exists(ArquivoTMP))
                    System.IO.File.Delete(ArquivoTMP);
            }
            return ResultadoPut;                        
        }





        #endregion

        #region List

        /// <summary>
        /// Lista os arquivos
        /// </summary>
        /// <param name="Pasta"></param>
        /// <param name="Absoluta"></param>
        /// <param name="Mascara"></param>
        /// <returns></returns>
        public List<DadosArquivo> List(string Pasta = "", bool Absoluta = false, string Mascara = "*.*")
        {
            List<DadosArquivo> retorno = new List<DadosArquivo>();
            foreach (Ftp.DadosArquivo Arq in FtpClient.Lista(Pasta, Absoluta, Mascara))
                retorno.Add(new DadosArquivo(Arq.Tamanho,Arq.Nome,Arq.NomeCompleto,Arq.Data));
            return retorno;
        }
        #endregion

        private Stream st;
        private Stream stRetorno;
        private bool ResultadoPut;
        private string Codigostr;
        private bool zipado;

        /// <summary>
        /// 
        /// </summary>
        public long TotalPut;

        /// <summary>
        /// 
        /// </summary>
        public long Saldo;
                
        /// <summary>
        /// 
        /// </summary>
        public bool PutAtivo;

        /// <summary>
        /// 
        /// </summary>
        public string UltimoErro = "";
        

        /// <summary>
        /// 
        /// </summary>
        public enum Tipoenvio 
        {
            /// <summary>
            /// 
            /// </summary>
            envio_com_compressao = 1, 
            /// <summary>
            /// 
            /// </summary>
            envio_sem_compressao = 0 
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TipoArmazenagem 
        { 
            /// <summary>
            /// 
            /// </summary>
            Armazenagem_com_compressao = 1, 
            /// <summary>
            /// 
            /// </summary>
            Armazenagem_sem_compressao = 0 
        }
    }
}
