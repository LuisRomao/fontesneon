﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TesteWcfSVC
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IService1
    {
        private string Historico;

        public Service1()
        {
            Historico = string.Format("Criado {0:ddMMss HHmmss}\r\n",DateTime.Now);
        }

        private static int STint = 0;

        private int Estint = 0;

        public string GetData(string value)
        {
            DateTime Esperar = DateTime.Now.AddSeconds(3);
            while (Esperar > DateTime.Now) ;
            return string.Format("{4:mm:ss}\r\nHistorico:{0}\r\nChamador:{1}\r\nestática: {2}\r\nprivada: {3}\r\n\r\n",Historico, value, STint++,Estint++,System.DateTime.Now);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
