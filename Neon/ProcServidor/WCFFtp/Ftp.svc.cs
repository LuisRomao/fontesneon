﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace WCFFtp
{
    /// <summary>
    /// objeto base para WC
    /// </summary>
    public class Ftp : WCPBase.WCPBase, IFtp
    {

        private const int TamanhoMaxPacote = 0xFFFF;

        private static SortedList<string, ArquivoParcial> _Arqs;

        private int numerador = 0;

        private static SortedList<string, ArquivoParcial> Arqs
        {
            get { return _Arqs == null ? _Arqs = new SortedList<string, ArquivoParcial>() : _Arqs; }
        }

        private static string Caminho
        {
            get
            {
                return ConfigurationManager.AppSettings["publicacoesIND"];
            }
        }

        /// <summary>
        /// identifica a função no log
        /// </summary>
        protected override string Identificador13
        {
            get { return "FTP__________"; }
        }

        #region PUT

        private string PutAbre(string NomeTotalArquivo, long Tamanho, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado)
        {                        
            string Codigostr = string.Format("{0:yyyyMMddHHmmss}_{1}", DateTime.Now, numerador++);
            ArquivoParcial ArqP = new ArquivoParcial(Codigostr, NomeTotalArquivo, EnviarZipado, EstocarZipado);
            Arqs.Add(Codigostr, ArqP);
            string Pasta = Path.GetDirectoryName(NomeTotalArquivo);
            if (!Directory.Exists(Pasta))
                Directory.CreateDirectory(Pasta);
            ArqP.fs = new FileStream(ArqP.NomeTMP, FileMode.Create);
            return Codigostr;
        }

        /// <summary>
        /// inicia o processo de put
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="FileName"></param>
        /// <param name="Tamanho"></param>
        /// <param name="EnviarZipado"></param>
        /// <param name="EstocarZipado"></param>
        /// <returns></returns>
        public string PutAbre(byte[] Chave, string FileName, long Tamanho, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado)
        {
            if (!DecodificaChave(Chave))
                return string.Empty;
            string NomeTotalArquivo = string.Format("{0}{1}", Caminho, FileName);
            return PutAbre(NomeTotalArquivo, Tamanho, EnviarZipado, EstocarZipado);
            /*
            string Codigostr = string.Format("{0:yyyyMMddHHmmss}_{1}", DateTime.Now, numerador++);
            ArquivoParcial ArqP = new ArquivoParcial(Codigostr, NomeTotalArquivo, EnviarZipado, EstocarZipado);
            Arqs.Add(Codigostr, ArqP);
            string Pasta = Path.GetDirectoryName(NomeTotalArquivo);
            if (!Directory.Exists(Pasta))
                Directory.CreateDirectory(Pasta);
            ArqP.fs = new FileStream(ArqP.NomeTMP, FileMode.Create);
            return Codigostr;*/
        }

        /// <summary>
        /// inicia o processo de put
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="NomeTotalArquivo">"Nome total do arquivo com path remota"</param>
        /// <param name="Tamanho"></param>
        /// <param name="EnviarZipado"></param>
        /// <param name="EstocarZipado"></param>
        /// <returns></returns>
        public string PutAbreABS(byte[] Chave, string NomeTotalArquivo, long Tamanho, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado)
        {
            if (!DecodificaChave(Chave))
                return string.Empty;            
            return PutAbre(NomeTotalArquivo, Tamanho, EnviarZipado, EstocarZipado);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigostr"></param>
        /// <param name="Pacote"></param>
        /// <param name="dados"></param>
        /// <returns></returns>
        public int PutPacote(string Codigostr, int Pacote, byte[] dados)
        {
            ArquivoParcial ArqP = Arqs[Codigostr];
            if (ArqP == null)
                return -1;
            if (ArqP.nPacote != Pacote)
                return -1;
            ArqP.nPacote++;
            using (MemoryStream ms = new MemoryStream(dados))
                ms.WriteTo(ArqP.fs);
            return ArqP.nPacote;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigostr"></param>
        /// <returns></returns>
        public bool PutFecha(string Codigostr)
        {
            ArquivoParcial ArqP = Arqs[Codigostr];
            if (ArqP == null)
                return false;
            bool retorno = ArqP.Fecha();
            Arqs.RemoveAt(Arqs.IndexOfKey(Codigostr));
            return retorno;
        }

        #endregion

        #region GET

        private string GetAbre(out long Tamanho,bool zipado, string NomeTotalArquivo)
        {                        
            string Codigostr = string.Format("{0:yyyyMMddHHmmss}_{1}", DateTime.Now, numerador++);
            ArquivoParcial ArqP = new ArquivoParcial(Codigostr,
                                                     NomeTotalArquivo,
                                                     zipado ? Tipoenvio.envio_com_compressao : Tipoenvio.envio_sem_compressao,
                                                     zipado ? TipoArmazenagem.Armazenagem_com_compressao : TipoArmazenagem.Armazenagem_sem_compressao);
            Arqs.Add(Codigostr, ArqP);
            ArqP.fs = new FileStream(NomeTotalArquivo, FileMode.Open, FileAccess.Read, FileShare.Read);
            ArqP.BR = new BinaryReader(ArqP.fs);
            ArqP.Saldo = Tamanho = ArqP.fs.Length;
            return Codigostr;
        }

        /// <summary>
        /// Abre o get
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Tamanho"></param>
        /// <param name="zipado"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetAbre(out long Tamanho, out bool zipado, byte[] Chave, string FileName)
        {
            if (!DecodificaChave(Chave))
            {
                Tamanho = 0;
                zipado = false;
                return string.Empty;
            }
            zipado = true;
            string NomeTotalArquivo = string.Format("{0}{1}.vir", Caminho, FileName);
            if (!File.Exists(NomeTotalArquivo))
            {
                NomeTotalArquivo = string.Format("{0}{1}", Caminho, FileName);
                zipado = false;
                if (!File.Exists(NomeTotalArquivo))
                {
                    Tamanho = 0;
                    return "Arquivo não encontrado";
                }
            };
            return GetAbre(out Tamanho, zipado, NomeTotalArquivo);                        
        }

        /// <summary>
        /// Abre o get com nome absoluto
        /// </summary>
        /// <param name="Tamanho"></param>
        /// <param name="zipado"></param>
        /// <param name="Chave"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetAbreABS(out long Tamanho, out bool zipado, byte[] Chave, string FileName)
        {
            if (!DecodificaChave(Chave))
            {
                Tamanho = 0;
                zipado = false;
                return string.Empty;
            }
            zipado = true;
            string NomeTotalArquivo = string.Format("{0}.vir", FileName);
            if (!File.Exists(NomeTotalArquivo))
            {
                NomeTotalArquivo = FileName;
                zipado = false;
                if (!File.Exists(NomeTotalArquivo))
                {
                    Tamanho = 0;
                    return "Arquivo não encontrado";
                }
            };
            return GetAbre(out Tamanho, zipado, NomeTotalArquivo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigostr"></param>
        /// <param name="Pacote"></param>
        /// <returns></returns>
        public byte[] GetPacote(string Codigostr, int Pacote)
        {
            ArquivoParcial ArqP = Arqs[Codigostr];
            if (ArqP == null)
                return null;
            if (ArqP.nPacote != Pacote)
                return null;
            ArqP.nPacote++;
            int TamanhoPacote = ArqP.Saldo > TamanhoMaxPacote ? TamanhoMaxPacote : (int)ArqP.Saldo;
            byte[] dados = ArqP.BR.ReadBytes(TamanhoPacote);
            ArqP.Saldo -= TamanhoPacote;
            return dados;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigostr"></param>
        /// <returns></returns>
        public bool GetFecha(string Codigostr)
        {
            ArquivoParcial ArqP = Arqs[Codigostr];
            if (ArqP == null)
                return false;
            ArqP.fs.Close();
            Arqs.RemoveAt(Arqs.IndexOfKey(Codigostr));
            return (ArqP.Saldo == 0);
        }

        #endregion

        #region List
        /// <summary>
        /// Lista os arquivos de uma pasta
        /// </summary>
        /// <param name="Pasta"></param>
        /// <param name="Absoluta"></param>
        /// <param name="Mascara"></param>
        /// <returns></returns>
        public List<DadosArquivo> Lista(string Pasta, bool Absoluta = false, string Mascara = "*.*")
        {
            if (!Absoluta)
                Pasta = string.Format("{0}{1}", Caminho, Pasta);
            List<DadosArquivo> retorno = new List<DadosArquivo>();
            foreach (string Arquivo in Directory.GetFiles(Pasta, Mascara))
                retorno.Add(new DadosArquivo(Arquivo));
            return retorno;
        }
        #endregion       
    }
}
