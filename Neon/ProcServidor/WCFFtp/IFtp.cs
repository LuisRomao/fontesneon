﻿using CompontesBasicosProc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace WCFFtp
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface IFtp
    {

        /// <summary>
        /// Função básica de teste
        /// </summary>
        /// <param name="Chave"></param>
        /// <returns></returns>
        [OperationContract]
        RetornoServidorProcessos Teste(byte[] Chave);

        #region PUT
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="FileName"></param>
        /// <param name="Tamanho"></param>
        /// <param name="EnviarZipado"></param>
        /// <param name="EstocarZipado"></param>
        /// <returns></returns>
        [OperationContract]
        string PutAbre(byte[] Chave, string FileName, long Tamanho, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado);

        /// <summary>
        /// inicia o processo de put
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="NomeTotalArquivo">"Nome total do arquivo com path remota"</param>
        /// <param name="Tamanho"></param>
        /// <param name="EnviarZipado"></param>
        /// <param name="EstocarZipado"></param>
        /// <returns></returns>
        [OperationContract]
        string PutAbreABS(byte[] Chave, string NomeTotalArquivo, long Tamanho, Tipoenvio EnviarZipado, TipoArmazenagem EstocarZipado);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigostr"></param>
        /// <param name="Pacote"></param>
        /// <param name="dados"></param>
        /// <returns></returns>
        [OperationContract]
        int PutPacote(string Codigostr, int Pacote, byte[] dados);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigostr"></param>
        /// <returns></returns>
        [OperationContract]
        bool PutFecha(string Codigostr);
        #endregion

        #region GET

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Tamanho"></param>
        /// <param name="zipado"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        [OperationContract]
        string GetAbre(out long Tamanho, out bool zipado, byte[] Chave, string FileName);

        /// <summary>
        /// Abre o get com nome absoluto
        /// </summary>
        /// <param name="Tamanho"></param>
        /// <param name="zipado"></param>
        /// <param name="Chave"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        [OperationContract]
        string GetAbreABS(out long Tamanho, out bool zipado, byte[] Chave, string FileName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigostr"></param>
        /// <param name="Pacote"></param>
        /// <returns></returns>
        [OperationContract]
        byte[] GetPacote(string Codigostr, int Pacote);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigostr"></param>
        /// <returns></returns>
        [OperationContract]
        bool GetFecha(string Codigostr);
        #endregion

        #region List
        /// <summary>
        /// Lista os arquivos de uma pasta
        /// </summary>
        /// <param name="Pasta"></param>
        /// <param name="Absoluta"></param>
        /// <param name="Mascara"></param>
        /// <returns></returns>
        [OperationContract]
        List<DadosArquivo> Lista(string Pasta, bool Absoluta = false, string Mascara = "*.*");

        #endregion        

    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public enum Tipoenvio
    {
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        envio_com_compressao = 1,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        envio_sem_compressao = 0
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public enum TipoArmazenagem
    {
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Armazenagem_com_compressao = 1,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Armazenagem_sem_compressao = 0
    }

    /// <summary>
    /// Dados do arquivo
    /// </summary>
    [DataContract]
    public class DadosArquivo
    {        
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Nome"></param>
        public DadosArquivo(string Nome)
        {
            FileInfo info = new FileInfo(Nome);
            this.Tamanho = info.Length;
            this.Nome = info.Name;
            this.NomeCompleto = info.FullName;
            this.Data = info.LastWriteTime;            
        }
                
        /// <summary>
        /// Tamanho
        /// </summary>
        [DataMember]
        public long Tamanho;

        /// <summary>
        /// Nome
        /// </summary>
        [DataMember]
        public string Nome;

        /// <summary>
        /// Data
        /// </summary>
        [DataMember]
        public DateTime Data;

        /// <summary>
        /// Nome completo
        /// </summary>
        [DataMember]
        public string NomeCompleto;
    }
}
