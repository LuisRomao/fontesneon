using System;
using System.IO;
using System.Windows.Forms;
using FrameworkProc.datasets;

namespace Menu_Principal
{

    public partial class FormMenuPrincipal : CompontesBasicos.FormPrincipalBase
    {
        private void ChecaPasta(string pasta)
        {
            string pastaReal = string.Format("{0}\\{1}", Application.StartupPath,pasta);
            if (!Directory.Exists(pastaReal))
                Directory.CreateDirectory(pastaReal);
        }

        public FormMenuPrincipal()
        {            
            InitializeComponent();            
            try
            {
                //if (!Directory.Exists(Application.StartupPath + @"\TMP"))
                //    Directory.CreateDirectory(Application.StartupPath + @"\TMP");
                ChecaPasta("TMP");
                ChecaPasta("XML");
                ChecaPasta("MODELOS");
                string[] ArquivosTMP = System.IO.Directory.GetFiles(Application.StartupPath + @"\TMP");
                foreach (string arquivo in ArquivosTMP)
                    File.Delete(arquivo);
                ArquivosTMP = Directory.GetFiles(Application.StartupPath,"log*.txt");
                foreach (string arquivo in ArquivosTMP)
                    if (File.GetLastWriteTime(arquivo) < DateTime.Now.AddMonths(-1))
                        File.Delete(arquivo);
            }
            catch { }
            //if (System.IO.File.Exists(Application.StartupPath + @"\Neon.NEO"))
            //    URLHepl = "http://Neon04/Neonhelp";
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt != null)
            {
                nbgDP.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("DP") > 0);
                nbgJuridico.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("JUR�DICO") > 0);
                nbgCadastro.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CADASTRO") > 0);
                navBarCad2.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CADASTRO") > 0);
                nbgUtilitarios.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("DBA") > 0);
                nbgAcesso.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("GER�NCIA") > 0);
                nbgConfiguracao.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("TI") > 0);
                nbfBoletos.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BOLETOS") > 0);
                GrupCtrlIMP.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("IMPRESSOS") > 0);
                navBarInternet.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("INTERNET") > 0);
                navBarFiscal.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("FISCAL") > 0);
                navBarBalancete.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BALANCETE") > 0);
                //navBarCaixa.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.VerificaFuncionalidade("CAIXA") > 0);
                navBarNotas.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("NOTAS") > 0);
                navBarCobranca.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("PROTESTO") > 0);
                navBarFaturamento.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("FATURAMENTO") > 0);
                if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                {
                    VirEmailNeon.EmailDiretoNeon.EmalST.EmTeste();
                    VirEmailNeon.EmailDiretoNeon.EmalSTSemRetorno.EmTeste();
                }
            }
            else
            {
                for (int i = 1; i < NavBarControl_F.Groups.Count; i++)//   DevExpress.XtraNavBar.NavBarGroup Grupo in
                    NavBarControl_F.Groups[i].Visible = false;                    
            }            
        }

        //public object oberro;

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {            
            int pk = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
            
            Cadastros.Usuarios.cUsuariosCampos cUsuariosCampos = new Cadastros.Usuarios.cUsuariosCampos();
            cUsuariosCampos.Fill(CompontesBasicos.TipoDeCarga.pk, pk, false);
            cUsuariosCampos.SetaUsuarioUnico();            
            ShowMoludointerno(cUsuariosCampos, "Meu usu�rio", CompontesBasicos.EstadosDosComponentes.JanelasAtivas, false);
            EscondeMenu();
        }

        private void FormMenuPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt != null) && (VirEmailNeon.EmailDiretoNeon.EmalST.Pendentes > 0))
            {
                if (MessageBox.Show(string.Format("Existem {0} e-mails pendentes.\r\nFechar assim mesmo?", VirEmailNeon.EmailDiretoNeon.EmalST.Pendentes), "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes)
                {
                    e.Cancel = true;
                    return;
                }
            }
            string CaminhoServidor = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Repositorio");
            
            //    CaminhoServidor = @"\\email\Volume_1\Repositorio\";
            //    CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Repositorio", CaminhoServidor);
            //}
            //if (!System.IO.File.Exists(Application.StartupPath + @"\Neon.NEO"))
            //    CaminhoServidor = @"\\Neon02\Sistema NEON\";
            //else
            //    CaminhoServidor = @"\\10.135.1.151\Sistema NEON\";
            //if (System.IO.File.Exists(Application.StartupPath + @"\NeonL.NEO"))
            //    CaminhoServidor = @"c:\publicar\";
            bool copiar = false;
            string arquivo = CaminhoServidor + "menu.exe";
            string nomearquivoLocal = "c:\\sistema NEON\\menu.exe";
            if (!System.IO.File.Exists(arquivo))
                return;
            if (!System.IO.File.Exists(nomearquivoLocal))
                copiar = true;
            else
            {
                System.Diagnostics.FileVersionInfo VerServidor = System.Diagnostics.FileVersionInfo.GetVersionInfo(arquivo);
                System.Diagnostics.FileVersionInfo VerLocal = System.Diagnostics.FileVersionInfo.GetVersionInfo(nomearquivoLocal);
                if (VerServidor.FileVersion != VerLocal.FileVersion)
                        copiar = true;

            };
            if (copiar)
                try
                {
                    System.IO.File.Copy(arquivo, nomearquivoLocal, true);
                }
                catch {}
        }

        

        private void ChamaHelp(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {                        
            AbreHelp();
        }

        private void FormMenuPrincipal_OnCadastraModulo(object sender, CompontesBasicos.ModuleInfo Modulo, ref bool Abrir, ref bool Cadastrar)
        {
            if ((CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt != null) && (Modulo.ModuleTypestr == "Calendario.Agenda.cAgendaGrade"))
            {
                int Pendencias = (int)dUSUarios.dUSUariosSt.USUariosTableAdapter.ScalarQueryPendentes(CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU);
                if (Pendencias > 0)               
                    Abrir = true;                
            }

            
            

            /*
            if (Modulo.ModuleTypestr == "Calendario.Agenda.cAgendaGrade")
            {
                int Pendencias = (int)Framework.datasets.dUSUarios.dUSUariosSt.USUariosTableAdapter.ScalarQueryPendentes(CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU);
                if (Pendencias > 0)
                {
                    //MessageBox.Show(String.Format("ATEN��O: Existem {0} agendamentos em seu nome.", Pendencias));                    
                    Abrir = true;
                }
                
            }
            else 
             */
 /*
            if (Modulo.ModuleTypestr == "dllCaixa.cSaldosGeral")
            {
                string comando =
"SELECT     TOP (1) CON, CONStatus\r\n" +
"FROM         CONDOMINIOS\r\n" +
"WHERE     (CONAuditor1_USU = @P1) AND (CONStatus = 1) OR\r\n" +
"          (CONAuditor2_USU = @P1) AND (CONStatus = 1);";                
                if (VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado(comando,Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU))                                   
                    Abrir = true;                                               
            }*/
            
        }

        private void FormMenuPrincipal_Load(object sender, EventArgs e)
        {            
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt != null)
            {
                int EmailsParados = VirEmailNeon.EmailDiretoNeon.EmalST.Pendentes;
                bool AbrirMapa = (dUSUarios.dUSUariosStGerentes.USUarios.FindByUSU(Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU) != null);
                if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("Mapa") > 0)
                    AbrirMapa = true;                

                if ((Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU == 35)
                    ||
                    (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU == 41)
                    )
                    AbrirMapa = true;
                if (AbrirMapa)
                    ModuleInfoCollection.ShowModule("Calendario.StatusGeral.cStatusGeral", this);
            }
        }

        /*
        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime? DateTimeInicioTrans = VirMSSQL.TableAdapter.STTableAdapter.DateTimeInicioTrans;
            if (DateTime.Now.AddSeconds(-30) > DateTimeInicioTrans.GetValueOrDefault(DateTime.Now))
            {
                TimeSpan TS = (TimeSpan)(DateTime.Now - DateTimeInicioTrans.Value);
                string identificacoes = "";
                if (VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada != null)
                    foreach (string ideX in VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada.Values)
                        identificacoes += string.Format("\r\n\t{0}", ideX);
                else
                    identificacoes = "--";
                string DadosDoProblema = string.Format("{0}\r\nIdentifica��es das transa��es abertas ({1}):{2}\r\nTempo: {3:n4}s\r\nLOG\r\n{5}\r\nVers�o:{4}",                                                       
                                                       VirException.VirException.CabecalhoRetorno(),
                                                       VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada == null ? 0 : VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada.Count,
                                                       identificacoes,
                                                       TS.TotalSeconds,
                                                       VirException.VirException.VersaoAssembles(),
                                                       VirMSSQL.TableAdapter.STTableAdapter.Log
                                                       );
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;luis@zromao.com", DadosDoProblema, "ERRO EM TRANSA��O");
            }
            if (VirMSSQL.TableAdapter.STTableAdapter.LogVircatchRetornando != "")
            {
                string DadosDoProblema = string.Format("{0}\r\nLog:\r\n{1}\r\n{2}",
                                                       VirException.VirException.CabecalhoRetorno(),
                                                       VirMSSQL.TableAdapter.STTableAdapter.LogVircatchRetornando,                                                       
                                                       VirMSSQL.TableAdapter.STTableAdapter.UltimoErro == null ? "Ultimo erro null" : VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(VirMSSQL.TableAdapter.STTableAdapter.UltimoErro,true)                                                       
                                                       );
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;luis@zromao.com", DadosDoProblema, "LogVircatchRetornando");
                VirMSSQL.TableAdapter.STTableAdapter.LogVircatchRetornando = "";
            }
        }
        */
        

        private void PictureEdit_F_Click(object sender, EventArgs e)
        {
            //string geraerro = "a";
            //geraerro = geraerro.Substring(10, 2);
        }

        

        

        
    }
}

