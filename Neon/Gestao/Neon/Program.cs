//#define BANCOTESTE  
using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using CompontesBasicos;
using Login;
using VirDB.Bancovirtual;
using VirExcepitionNeon;



namespace Menu_Principal
{


    static class Program
    {

        [DllImport("User32.dll")]
        private static extern IntPtr FindWindow(String lpClassName, String lpWindowName);
        [DllImport("User32.dll")]
        private static extern Int32 SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern Int32 ShowWindow(IntPtr hWnd, int nCmdShow);
        
        /*
        static bool CarregaPonto()
        {
            if (FPonto.FPontoST.ShowDialog() == DialogResult.OK)
            {
                FPonto.FPontoST.ShowPonto();
                return true;
            }
            else
                return false;
        }*/
        /*
        static void DesligaPonto()
        {
            FPonto.FPontoST.DesligarLeitor();
            FPonto.FPontoST.Close();
        }*/
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //MessageBox.Show("ponto 1");
            bool Primeiro;
            string Titulo = "Neon - Administração de Condomínios";
            using (Mutex Mut = new Mutex(true, "MultNeon", out Primeiro))
            {                
                if (Primeiro)
                {                    
                    //VirExcepitionNeon.VirExceptionNeon TrataErro = new VirExcepitionNeon.VirExceptionNeon("Neon");
                    try
                    {
                        Application.EnableVisualStyles();
                        Application.SetCompatibleTextRenderingDefault(false);
                        bool QAS = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("QAS", false);                        
#if (DEBUG)
                        BancoVirtual.Popular("Local", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=venus");
                        BancoVirtual.Popular("copia SBC 1", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonSBC;Persist Security Info=True;User ID=sa;Password=venus");
                        BancoVirtual.Popular("copia SBC 2", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonSBC2;Persist Security Info=True;User ID=sa;Password=venus");
                        BancoVirtual.Popular("copia SBC 3", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonSBC3;Persist Security Info=True;User ID=sa;Password=venus");
                        //BancoVirtual.PopularEmailRet("EmailRet", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=EmailRet;Persist Security Info=True;User ID=sa;Password=venus");
                        BancoVirtual.Popular("NEON SBC (cuidado)", @"Data Source=NEON20\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Connect Timeout=30;Password=TPC4P2D");
                        //BancoVirtual.Popular("arvixe QAS", @"Data Source=neonimoveis.com;Initial Catalog=NeonMINI;Persist Security Info=True;User ID=saNeon;Password=TPC4P2D");
                        //BancoVirtual.Popular("Lap", @"Data Source=lapvirtual\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=venus");
                        //BancoVirtual.Popular("copia C", @"Data Source=lapvirtual\SQLEXPRESS;Initial Catalog=NeonReal;Persist Security Info=True;User ID=sa;Password=venus");
                        BancoVirtual.Popular("Lap SA 64 1", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NEONSA;Persist Security Info=True;User ID=sa;Password=venus");
                        BancoVirtual.Popular("Lap SA 64 2", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NEONSA2;Persist Security Info=True;User ID=sa;Password=venus");
                        BancoVirtual.Popular("Lap SA 64 3", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NEONSA3;Persist Security Info=True;User ID=sa;Password=venus");
                        //BancoVirtual.Popular("Lap", @"Data Source=lapvirtual\SQLEXPRESS;Initial Catalog=Neon;Integrated Security=True;User ID=sa;Password=VENUS");
                        //BancoVirtual.Popular("Servidor", @"Data Source=Servidor\SQLEXPRESS;Initial Catalog=Neon;Integrated Security=True;User ID=sa;Password=VENUS");
                        //BancoVirtual.Popular("Lapsa", @"Data Source=lapvirtual\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=venus");
                        //BancoVirtual.Popular("NEON40 (cuidado)", @"Data Source=NEON40\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=TPC4P2D");
                        //BancoVirtual.Popular("NEON (cuidado)", @"Data Source=NEON02\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=7778");
                        //*BancoVirtual.PopularAcces("NEON Access (cuidado)", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=G:\neon.mda;Jet OLEDB:Database Password=96333018");
                        //*BancoVirtual.PopularAcces("NEON Access SA (cuidado)", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\dados.mdb;Persist Security Info=True;Password=7778;User ID=CPD;Jet OLEDB:System database=G:\neon.mda;Jet OLEDB:Database Password=7778");
                        BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
                        BancoVirtual.PopularAcces("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
                        BancoVirtual.PopularAccesH("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\hist.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
                        BancoVirtual.PopularAccesH("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
                        BancoVirtual.PopularAccesH("LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\neon.MDA;Jet OLEDB:Database Password=7778");
                        //*BancoVirtual.PopularAccesH("NEON AccessH", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\hist.mdb ;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\neon.mda;Jet OLEDB:Database Password=96333018");
                        BancoVirtual.PopularInternet("INTERNET Local 1", @"Data Source=Lapvirtual8\sqlexpress;Initial Catalog=neoninternet;Persist Security Info=True;User ID=sa;Password=venus", 1);
                        BancoVirtual.PopularInternet("INTERNET Local 2", @"Data Source=Lapvirtual8\sqlexpress;Initial Catalog=Neonimoveis_2;Persist Security Info=True;User ID=sa;Password=venus", 2);
                        //FormPrincipalBase.MostrarTiposNaTab = true;
                        FormMenuPrincipal.strEmProducao = false;
#else

#endif
                        int EMP = dEstacao.dEstacaoSt.GetValor("EMP", 1);                        
                        if (QAS)
                        {
                            CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao = false;
                            Titulo = "AMBIENTE DE TESTE";
                            MessageBox.Show("ATENÇÃO: Ambiente de testes");
                            if (EMP == 1)
                            {
                                //BancoVirtual.Popular      ("QAS", @"Data Source=cp.neonimoveis.com;Initial Catalog=NeonH  ;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
                                //BancoVirtual.PopularFilial("QAS", @"Data Source=cp.neonimoveis.com;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
                                BancoVirtual.Popular("QAS CTI", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonH  ;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
                                BancoVirtual.PopularFilial("QAS CTI", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
                                BancoVirtual.PopularAcces("NEON Access T ", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\NEWSCC\dadosT.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\NEWSCC\neon.mda;Jet OLEDB:Database Password=96333018");
                                BancoVirtual.PopularAccesH("NEON AccessH T", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\NEWSCC\histT.mdb ;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\NEWSCC\neon.mda;Jet OLEDB:Database Password=96333018");
                                BancoVirtual.PopularInternet("QAS INTERNET 1", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_1H;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT", 1);
                                BancoVirtual.PopularInternet("QAS INTERNET 2", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_2H;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT", 2);
                                BancoVirtual.PopularInternet("Local INTERNET 1", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=sa;Password=venus", 1);
                                BancoVirtual.PopularInternet("Local INTERNET 2", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=sa;Password=venus", 2);
                            }
                            else
                            {
                                BancoVirtual.Popular("QAS", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
                                BancoVirtual.PopularFilial("QAS", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonH  ;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");                                
                                BancoVirtual.PopularAcces("NEON Access T ", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\NEWSCC\dadosT.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\NEWSCC\neon.mda;Jet OLEDB:Database Password=96333018");
                                BancoVirtual.PopularAccesH("NEON AccessH T", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\NEWSCC\histT.mdb ;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\NEWSCC\neon.mda;Jet OLEDB:Database Password=96333018");
                                BancoVirtual.PopularInternet("QAS INTERNET 1", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_1H;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT", 1);
                                BancoVirtual.PopularInternet("QAS INTERNET 2", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_2H;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT", 2);
                                BancoVirtual.PopularInternet("Local INTERNET 1", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=sa;Password=venus", 1);
                                BancoVirtual.PopularInternet("Local INTERNET 2", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=sa;Password=venus", 2);
                            }
                        }
                        else
                        {
                            CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao = true;
                            FormPrincipalBase.TituloPrincipal = string.Format("Neon - Administração de Condomínios");
                            if (EMP == 1)
                            {
                                //BancoVirtual.Popular("NEON SBC (cuidado)", @"Data Source=cp.neonimoveis.com;Initial Catalog=NEON  ;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                                //BancoVirtual.PopularFilial("NEON SA  (cuidado)", @"Data Source=cp.neonimoveis.com;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                                BancoVirtual.Popular("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEON  ;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                                BancoVirtual.PopularFilial("NEON SA  (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                                BancoVirtual.PopularAcces("NEON Access ", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\NEWSCC\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\NEWSCC\neon.mda;Jet OLEDB:Database Password=96333018");
                                BancoVirtual.PopularAccesH("NEON AccessH", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\neon02\NEWSCC\hist.mdb ;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=\\neon02\NEWSCC\neon.mda;Jet OLEDB:Database Password=96333018");
                            }
                            else
                            {
                                BancoVirtual.Popular("NEON SA  (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                                BancoVirtual.PopularFilial("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEON  ;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");                                
                                BancoVirtual.PopularAcces("NEON Access", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=G:\neon.MDA;Jet OLEDB:Database Password=7778");
                                BancoVirtual.PopularAccesH("NEON AccessH", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\hist.mdb ;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=G:\neon.mda;Jet OLEDB:Database Password=7778");

                            };

                            BancoVirtual.PopularInternet("INTERNET REAL1", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 1);
                            BancoVirtual.PopularInternet("INTERNET REAL2", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 2);
                        }
                        FormPrincipalBase.TituloPrincipal = Titulo;                        
                        Application.ThreadException += VirExceptionNeon.VirExceptionNeonSt.OnThreadException;
                        //Autenticacao Usuario                        
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo("pt-BR");
                        
                        bool logado = false;
                        
                        //int iPonto = dEstacao.dEstacaoSt.GetValor("PONTO", 0);
                        
                        fLogin FLogin = new fLogin();

                        
                        bool conectado = FLogin.Conecta();
                        
                        if (conectado)
                            //if (iPonto == 1)
                            //{
                            //    logado = CarregaPonto();
                            //}
                            //else
                            //{

                                logado = (FLogin.ShowDialog() == DialogResult.OK);
                            //}
                        else
                            MessageBox.Show("Servidor não responde");
                        FLogin.Dispose();
                        FLogin = null;

                        //new fLogin().ShowDialog();


                        //if (logado || !conectado)
                        //{
                        //FormMenuPrincipal FPrincipal = new FormMenuPrincipal();

                        //Application.Run(FPrincipal);                                                            
                        Application.Run(new FormMenuPrincipal());
                        //}
                        Mut.ReleaseMutex();
                    }
                    catch (Exception e)
                    {                        
                        VirExceptionNeon.VirExceptionNeonSt.NotificarErro(e);                        
                    }
                }
                else
                {                    
                    IntPtr HJanela = FindWindow(null, Titulo /*"Neon - Administração de Condomínios"*/);
                    if (HJanela != IntPtr.Zero)
                    {
                        SetForegroundWindow(HJanela);
                        ShowWindow(HJanela, 3);
                    }
                }
            }            
        }
    }
}