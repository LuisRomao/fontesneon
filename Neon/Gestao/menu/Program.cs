using System;
using System.Windows.Forms;

namespace menu
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                using (fMenu Tela = new fMenu())
                {
                            if (args.Length >= 1)
                                Tela.servico = (args[0].ToUpper() == "MODULO");
                            //if (args.Length >= 3)
                            //    Tela.CaminhoServidor = string.Format(@"{0}\", args[2]);
                            //if (args.Length >= 4)
                            //    Tela.CaminhoLocal = string.Format(@"{0}\", args[3]);
                            if (Tela.servico || ((args.Length >= 2) && (args[1].ToUpper() == "OCULTO")))
                                Tela.Efetivar();
                            else
                                Application.Run(Tela);
                }                                                    
            }
            catch 
            {            
            };
        }
    }
}