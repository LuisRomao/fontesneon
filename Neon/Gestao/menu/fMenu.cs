using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Data;
using System.IO;

namespace menu
{
    public partial class fMenu : Form
    {
        public fMenu()
        {
            InitializeComponent();
        }

        #region Dados da estacao
        private string ArquivoXML
        {
            get
            {
                //if (servico)
                //        CaminhoLocal = Environment.CurrentDirectory + "\\";
                //else
                //        CaminhoLocal = Application.StartupPath + "\\";                
                return CaminhoLocal + "XML\\Estacao.xml";
            }
        }

        private DataTable DadosEstacao
        {
            get
            {
                DataSet dEstacao = new DataSet("dEstacao");;
                DataTable Retorno;
                if (File.Exists(ArquivoXML))
                {                    
                    dEstacao.ReadXml(ArquivoXML);
                    Retorno = dEstacao.Tables["Dados"];                    
                }
                else
                {
                    Retorno = dEstacao.Tables.Add("Dados");                    
                    Retorno.Columns.Add("Chave", typeof(string));
                    Retorno.Columns.Add("Valor", typeof(string));
                };
                Retorno.PrimaryKey = new DataColumn[] { Retorno.Columns["Chave"] };
                return Retorno;
            }
        }

        private string GetValor(string Chave)
        {
            DataRow row = DadosEstacao.Rows.Find(Chave);
            return (row == null) ? null : row["Valor"].ToString();
        }

        private void SetValor(string Chave, string Valor)
        {
            DataTable Tabela = DadosEstacao;
            DataRow row = Tabela.Rows.Find(Chave);
            if (row == null)
                row = Tabela.Rows.Add(Chave, Valor);
            else
                row["Valor"] = Valor;
            Tabela.WriteXml(ArquivoXML);            
        }

        private String _CaminhoServidor;

        private String CaminhoServidor
        {
            get
            {
                if (_CaminhoServidor != null)
                    return _CaminhoServidor;
                _CaminhoServidor = GetValor("Repositorio");
                if (_CaminhoServidor == null)
                    RegistraLog("NULO");
                if (_CaminhoServidor == null)
                {
                    RegistraLog("Caminho Servidor nao presente em XML");
                    if (Directory.Exists(@"\\Neon02\Sistema NEON\"))
                    {
                        _CaminhoServidor = @"\\Neon02\Sistema NEON\";
                    }
                    else
                    {
                        RegistraLog(@"nao existe \\Neon02\Sistema NEON\");
                        if (Directory.Exists(@"\\10.135.1.151\Sistema NEON\"))
                            _CaminhoServidor = @"\\10.135.1.151\Sistema NEON\";
                        else
                        {
                            RegistraLog(@"nao existe \\10.135.1.151\Sistema NEON\");
                            if (Directory.Exists(@"\\neon04\Sistema NEON\"))
                                _CaminhoServidor = @"\\neon04\Sistema NEON\";
                            else
                            {
                                if (Directory.Exists(@"\\lapneon\e\repositorio\"))
                                    _CaminhoServidor = @"\\lapneon\e\repositorio\";
                            }
                        }
                    }
                    if (_CaminhoServidor != null)
                        SetValor("Repositorio", _CaminhoServidor);
                };
                RegistraLog(string.Format("Caminho Servidor (repositorio):{0}", _CaminhoServidor ?? ""));
                return _CaminhoServidor ?? "";
            }
        }

        private String _CaminhoLocal;

        private String CaminhoLocal 
        {
            get 
            {
                if (_CaminhoLocal == null) 
                {
                    _CaminhoLocal = Application.StartupPath + "\\";
                    if (!Directory.Exists(_CaminhoLocal + "XML"))
                        Directory.CreateDirectory(_CaminhoLocal + "XML");
                    if (!Directory.Exists(_CaminhoLocal + "PT-BR"))
                        Directory.CreateDirectory(_CaminhoLocal + "PT-BR");
                    if (!Directory.Exists(_CaminhoLocal + "IMAGENS"))
                        Directory.CreateDirectory(_CaminhoLocal + "IMAGENS");
                    if (!Directory.Exists(_CaminhoLocal + "ModelosHTML"))
                        Directory.CreateDirectory(_CaminhoLocal + "ModelosHTML");
                    RegistraLog("Caminho local: " +_CaminhoLocal);
                };
                return _CaminhoLocal;
            }
        } 

        #endregion

        #region LOG
        private string _Arqlog;

        private string Arqlog
        {
            get
            {
                return _Arqlog ?? (_Arqlog = string.Format(@"{0}log{1:yyyyMMdd_HH_mm_ss}.txt", CaminhoLocal, DateTime.Now));
            }
        }

        private void RegistraLog(string Texto)
        {
            System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} {1}\r\n", DateTime.Now, Texto));
        }

        #endregion

        public bool servico;

        
        //public String CaminhoLocal = @"c:\Sistema NEON\";

              
        
        private string Copiar(String Path,String ext)
        {
            string retorno = string.Format("{0}$.{1}\r\n", Path, ext);
            bool copiar;
            string nomearquivoLocal;
            if (!System.IO.Directory.Exists(CaminhoServidor + Path))
                return string.Format("{0}  Pasta n�o encontrada: {1}{2}\r\n",retorno,CaminhoServidor,Path);
            Array.ForEach(System.IO.Directory.GetFiles(CaminhoServidor + Path, String.Format("*.{0}", ext)),
            delegate(string arquivo)
            {
                retorno += string.Format("     Arquivo: {0} :", arquivo);
                copiar = false;
                nomearquivoLocal = CaminhoLocal + Path + System.IO.Path.GetFileName(arquivo);
                if (!System.IO.File.Exists(nomearquivoLocal))
                    copiar = true;
                else
                {
                    FileVersionInfo VerServidor = FileVersionInfo.GetVersionInfo(arquivo);
                    FileVersionInfo VerLocal = FileVersionInfo.GetVersionInfo(nomearquivoLocal);
                    if ((VerServidor.FileVersion == null) && (VerLocal.FileVersion == null))
                    {
                        if (System.IO.File.GetLastWriteTime(arquivo) != System.IO.File.GetLastWriteTime(nomearquivoLocal))
                            copiar = true;
                    }
                    else
                        if (VerServidor.FileVersion != VerLocal.FileVersion)
                            copiar = true;
                }
                if (copiar)
                    try
                    {
                        System.IO.File.Copy(arquivo, nomearquivoLocal, true);
                        retorno += "Copiado    <<<<<<--------- * * \r\n";
                    }
                    catch (Exception e)
                    {
                        retorno += string.Format("\r\nERRO:{0}\r\n**********{1}\r\n**********\r\n", e.Message,e.StackTrace);                        
                    }
                else
                    retorno += "Pulado\r\n";
            });
            return retorno;
        }
        

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            DateTime dinicio = DateTime.Now;
            Efetivar();
            while (((TimeSpan)(DateTime.Now - dinicio)).Seconds < 5)            
                Application.DoEvents();            
            Application.Exit();
        }

        System.ServiceProcess.ServiceController _ServicoModulo;
        System.ServiceProcess.ServiceController ServicoModulo 
        {
            get 
            {
                if (_ServicoModulo == null)                 
                    _ServicoModulo = new System.ServiceProcess.ServiceController("ModuloNeon");                                    
                return _ServicoModulo;
            }
        }

        

        public void Efetivar()         
        {
            try
            {
                RegistraLog(string.Format("In�cio <{0}>", servico ? "Servi�o" : "Aplicativo"));                
                if (servico)
                    if (ServicoModulo.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                    {
                        System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Parando servi�o\r\n", DateTime.Now));
                        ServicoModulo.Stop();
                        System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Parado\r\n", DateTime.Now));
                        DateTime DataEspera = DateTime.Now.AddMinutes(1);
                        while (DateTime.Now < DataEspera) ;
                        System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Fim espera\r\n", DateTime.Now));
                    }                               
                System.IO.File.AppendAllText(Arqlog, Copiar("", "dll"));
                System.IO.File.AppendAllText(Arqlog, Copiar("", "exe"));
                System.IO.File.AppendAllText(Arqlog, Copiar("", "pdb"));
                System.IO.File.AppendAllText(Arqlog, Copiar("", "config"));
                System.IO.File.AppendAllText(Arqlog, Copiar("pt-br\\", "dll"));
                System.IO.File.AppendAllText(Arqlog, Copiar("pt-br\\", "exe"));
                System.IO.File.AppendAllText(Arqlog, Copiar("imagens\\", "jpg"));
                System.IO.File.AppendAllText(Arqlog, Copiar("imagens\\", "gif"));
                System.IO.File.AppendAllText(Arqlog, Copiar("ModelosHTML\\", "htm"));
                System.IO.File.AppendAllText(Arqlog, Copiar("ModelosHTML\\", "ico"));
                System.IO.File.AppendAllText(Arqlog, Copiar("ModelosHTML\\", "gif"));
                System.IO.File.AppendAllText(Arqlog, Copiar("ModelosHTML\\", "jpg"));
                System.IO.File.AppendAllText(Arqlog, Copiar("Modelos\\", "xml"));
                System.IO.File.AppendAllText(Arqlog, Copiar("Modelos\\", "pdf"));
                System.IO.File.AppendAllText(Arqlog, Copiar("", "dic"));
                System.IO.File.AppendAllText(Arqlog, Copiar("", "aff"));
                if (servico)
                {
                    System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Refresh\r\n", DateTime.Now));
                    ServicoModulo.Refresh();
                    if (ServicoModulo.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Iniciando servi�o\r\n", DateTime.Now));
                        ServicoModulo.Start();
                        System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Iniciado\r\n", DateTime.Now));
                    }
                }
                else
                    if (System.IO.File.Exists(String.Format("{0}neon.exe", CaminhoLocal)))
                    {
                        System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Iniciando execut�vel\r\n", DateTime.Now));
                        System.IO.Directory.SetCurrentDirectory(CaminhoLocal);
                        System.Diagnostics.Process.Start(String.Format("{0}neon.exe", CaminhoLocal));
                    }
                    else
                    {
                        System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Execut�vel n�o encontrado: <{1}neon.exe>\r\n", DateTime.Now, CaminhoLocal));
                    }
            }
            catch (Exception e)
            {
                System.IO.File.AppendAllText(Arqlog, string.Format("ERRO:{0}\r\nStackTrace:\r\n**********\r\n{1}\r\n**********\r\n", e.Message, e.StackTrace));
            };
            System.IO.File.AppendAllText(Arqlog, string.Format("{0:HH:mm:ss} Fim", DateTime.Now));
            
        }

    }
}