using System;
using System.Threading;
using System.Windows.Forms;

namespace VirExcepitionNeon
{
    /// <summary>
    /// Classe para reportar erros
    /// </summary>
    public class VirExceptionNeon:VirException.VirException
    {      
        private static VirExceptionNeon virExceptionNeonSt;        

        /// <summary>
        /// Instancia ST
        /// </summary>
        public static VirExceptionNeon VirExceptionNeonSt {
            get 
            {                
                if (virExceptionNeonSt == null)
                {
            
                    virExceptionNeonSt = new VirExceptionNeon();
            
                    VirException.VirException.VirExceptionSt = virExceptionNeonSt;
            
                }
                return  virExceptionNeonSt;
            } 
        }

        /// <summary>
        /// Construtor
        /// </summary>        
        public VirExceptionNeon() 
        {            
            EMP = "NEON";
            
            CriaTimer();
            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_SMTP"></param>
        /// <param name="_Usuario"></param>
        /// <param name="_Senha"></param>
        /// <param name="_Remetente"></param>
        public VirExceptionNeon(string _SMTP, string _Usuario, string _Senha, string _Remetente)
            : base(_SMTP, _Usuario, _Senha, _Remetente)
        {
            CriaTimer();            
        }

        private void CriaTimer()
        {            
            Thread Thread1 = new Thread(new ThreadStart(LoopMonitor));         
            Thread1.Priority = ThreadPriority.Lowest;
            
            Thread1.Start();
            
            if (Application.ExecutablePath != null)
                Application.ApplicationExit += new EventHandler(Application_ApplicationExit);            
        }

        void Application_ApplicationExit(object sender, EventArgs e)
        {
            DesligarLoop = true;
        }      

        /// <summary>
        /// Enviar e.mail
        /// </summary>
        /// <param name="DadosDoErro">Dados do erro</param>
        /// <param name="Assunto">Assunto</param>
        protected override void EnviaEmail(string DadosDoErro,string Assunto)
        {
            //string Cabecalho = String.Format("EMP:{0}\r\n\r\n", EMP);
            //Cabecalho += String.Format("Usu�rio: {0} - {1}\r\n\r\n", Environment.UserName, Environment.MachineName);
            //DadosDoErro = Cabecalho + DadosDoErro;
            try
            {
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("Erros@virweb.com.br", DadosDoErro, Assunto);
            }
            catch
            {
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("Erros@virweb.com.br", DadosDoErro, "Erro no assunto");
            }
        }

        /// <summary>
        /// Tempo m�ximo de uma transa��o
        /// </summary>
        public int TempoMaximoTrans = 30;

        /// <summary>
        /// Desliga o Loop
        /// </summary>
        public bool DesligarLoop = false;

        private void LoopMonitor()
        {
            while (!DesligarLoop)
            {
                MonitoraTrans();
                Thread.Sleep(3 * 1000);                
            }
        }

        private DateTime? DispararVircat = null;

        //private void MonitoraTrans(object sender, EventArgs e)        
        private void MonitoraTrans()        
        {            
            try
            {                                
                DateTime? DateTimeInicioTrans = VirMSSQL.TableAdapter.STTableAdapter.DateTimeInicioTrans;                
                if (DateTime.Now.AddSeconds(-TempoMaximoTrans) > DateTimeInicioTrans.GetValueOrDefault(DateTime.Now))
                {
                    //Console.WriteLine("TESTE----------IF 1------------TESTE");
                    TimeSpan TS = (TimeSpan)(DateTime.Now - DateTimeInicioTrans.Value);
                    string identificacoes = "";
                    if (VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada != null)
                        foreach (string ideX in VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada.Values)
                            identificacoes += string.Format("\r\n\t{0}", ideX);
                    else
                        identificacoes = "--";
                    string DadosDoProblema = string.Format("TEMPO\r\n\r\n{0}\r\nIdentifica��es das transa��es abertas ({1}):{2}\r\nTempo: {3:n4}s\r\nLOG\r\n{5}\r\nVers�o:{4}",
                                                           VirException.VirException.CabecalhoRetorno(),
                                                           VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada == null ? 0 : VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada.Count,
                                                           identificacoes,
                                                           TS.TotalSeconds,
                                                           VirMSSQL.TableAdapter.STTableAdapter.Log,
                                                           VirException.VirException.VersaoAssembles()
                                                           );
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;luis@zromao.com", DadosDoProblema, "ERRO EM TRANSA��O");
                    Thread.Sleep(60 * 1000);
                }
                
                if (VirMSSQL.TableAdapter.STTableAdapter.VircatchRetornando)
                {

                    //Console.WriteLine("TESTE----------IF 2------------TESTE");
                    string identificacoes = "";
                    if (VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada != null)
                        foreach (string ideX in VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada.Values)
                            identificacoes += string.Format("\r\n\t{0}", ideX);
                    else
                        identificacoes = "--";
                    string DadosDoProblema = string.Format("ERRO TIPO VircatchRetornando\r\n\r\n{0}\r\nLog:\r\n----\r\n{1}----\r\n�ltimo erro:\r\n----\r\n{2}\r\n----\r\nIdentifica��es das transa��es abertas:({3}):{4}\r\n",
                                                           VirException.VirException.CabecalhoRetorno(),
                                                           VirMSSQL.TableAdapter.STTableAdapter.Log.ToString(),
                                                           VirExceptionNeon.RelatorioDeErro(VirMSSQL.TableAdapter.STTableAdapter.UltimoErro,true, true,false),
                                                           VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada == null ? 0 : VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada.Count,
                                                           identificacoes
                                                           );
                    //Console.WriteLine("TESTE----------IF 2 A------------TESTE");
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;luis@zromao.com", DadosDoProblema, "LogVircatchRetornando");
                    //Console.WriteLine("TESTE----------IF 2 b------------TESTE");
                    //VirMSSQL.TableAdapter.STTableAdapter.LogVircatchRetornando = "";
                    if (DispararVircat.HasValue)
                    {
                        if (DateTime.Now > DispararVircat.Value)
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(new Exception("Corre��o emergencial"));
                    }
                    else
                        DispararVircat = DateTime.Now.AddMinutes(2);
                }
                else
                    DispararVircat = null;
                
                if (VirMSSQL.TableAdapter.STTableAdapter.LogVircatchRetornando != "")
                {
                    //Console.WriteLine("TESTE----------IF COR------------TESTE");
                    string identificacoes = "";
                    if (VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada != null)
                        foreach (string ideX in VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada.Values)
                            identificacoes += string.Format("\r\n\t{0}", ideX);
                    else
                        identificacoes = "--";
                    string DadosDoProblema = string.Format("ERRO TIPO LogVircatchRetornando Corrigido\r\n\r\n{0}\r\nLog:\r\n-----\r\n{1}\r\n-----\r\nRelat�rio de erro:\r\n----\r\n{2}\r\n----\r\nIdentifica��es das transa��es abertas ({3}):{4}",
                                                           VirException.VirException.CabecalhoRetorno(),
                                                           VirMSSQL.TableAdapter.STTableAdapter.LogVircatchRetornando,
                                                           VirExceptionNeon.RelatorioDeErro(VirMSSQL.TableAdapter.STTableAdapter.UltimoErro, true,true,false),
                                                           VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada == null ? 0 : VirMSSQL.TableAdapter.STTableAdapter.IdentificadorChamada.Count,
                                                           identificacoes
                                                           );
                    //Console.WriteLine("TESTE----------IF C A------------TESTE");
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br;luis@zromao.com", DadosDoProblema, "LogVircatchRetornando Corrigido");
                
                    VirMSSQL.TableAdapter.STTableAdapter.LogVircatchRetornando = "";
                
                }
            }
            catch 
            {
                
            }
        }
    }
}
