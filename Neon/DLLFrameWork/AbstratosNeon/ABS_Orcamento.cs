﻿using System;
using System.Runtime.Serialization;
using Abstratos;
using VirEnumeracoesNeon;

namespace AbstratosNeon
{
    /// <summary>
    /// Classe abstrata para orçamento
    /// </summary>
    [DataContract(Name = "ABS_Orcamento", Namespace = "AbstratosNeon")]
    public abstract class ABS_Orcamento : ABS_Base
    {
        private static string NomeObjeto = "OrcamentoProc.Orcamento";

        /// <summary>
        /// Cria um objeto real orçamento (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Orcamento GetOrcamento(int ORG)
        {
            return (ABS_Orcamento)ContruaObjeto(NomeObjeto,
                                                new object[] { ORG },
                                                new Type[] { typeof(int) });
        }

        /// <summary>
        /// Indica se o orçamento foi encontrado no Banco
        /// </summary>
        public abstract bool real { get; }

        /// <summary>
        /// Status do orçamento
        /// </summary>
        public abstract ORCStatus Status { get; set; }
        
        /// <summary>
        /// Competência inicial
        /// </summary>
        public abstract ABS_Competencia CompI { get; set; }

        /// <summary>
        /// Competência final
        /// </summary>
        public abstract ABS_Competencia CompF { get; set; }

        /// <summary>
        /// Condomínio
        /// </summary>
        public abstract ABS_Condominio Condominio { get; set; }

}
}
