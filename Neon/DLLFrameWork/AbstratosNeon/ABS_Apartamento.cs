﻿using System;
using System.Collections.Generic;
using Abstratos;
using VirEnumeracoesNeon;
using FrameworkProc;

namespace AbstratosNeon
{
    /// <summary>
    /// Objeto abstrato para o Apartamento
    /// </summary>
    public abstract class ABS_Apartamento : ABS_Base
    {

        private static string NomeObjeto = "Cadastros.Apartamentos.Apartamento";
        private static string NomeObjetoProc = "CadastrosProc.ApartamentoProc";

        /// <summary>
        /// Cria um objeto real tipo Apartamento (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Apartamento GetApartamento(int APT)
        {
            return (ABS_Apartamento)ContruaObjeto(NomeObjeto,
                                                new object[] { APT },
                                                new Type[] { typeof(int) });
        }

        /// <summary>
        /// Cria um objeto real tipo ApartamentoProc (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Apartamento GetApartamentoProc(int APT, EMPTProc _EMPTProc1 = null)
        {
            return (ABS_Apartamento)ContruaObjeto(NomeObjetoProc,
                                                new object[] { APT , _EMPTProc1},
                                                new Type[] { typeof(int) , typeof(EMPTProc)});
        }

        /// <summary>
        /// Retorna os apartamentos de um condomínio
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public static SortedList<int,ABS_Apartamento> ABSGetApartamentos(int CON)
        {
            return (SortedList<int, ABS_Apartamento>)MetodoEstatico(NomeObjeto,
                                         "GetApartamentos",
                                         new object[] { CON },
                                         new Type[] { typeof(int) });
        }

        /// <summary>
        /// Carrega todos os apartamentos
        /// </summary>
        /// <returns></returns>
        public static SortedList<int, ABS_Apartamento> ABSGetApartamentosAtivos()
        {
            return (SortedList<int, ABS_Apartamento>)MetodoEstatico(NomeObjeto,
                                         "GetApartamentosAtivos");
        }

        /// <summary>
        /// Carrega todos os apartamentos
        /// </summary>
        /// <returns></returns>
        public static SortedList<int, ABS_Apartamento> GetApartamentosSeguro(bool? interno)
        {
            return (SortedList<int, ABS_Apartamento>)MetodoEstatico(NomeObjeto,
                                         "GetApartamentosSeguro",
                                         new object[] { interno },
                                         new Type[] { typeof(bool?) });
        }

        /// <summary>
        /// Indica se o Apartamento foi encontrado no Banco
        /// </summary>
        public abstract bool encontrado { get; }

        /// <summary>
        /// 
        /// </summary>
        protected ABS_Condominio _Condominio;

        /// <summary>
        /// Condominio
        /// </summary>
        public virtual ABS_Condominio Condominio 
        {
            get { return _Condominio; }
            set { _Condominio = value; }
        }

        /// <summary>
        /// APT
        /// </summary>
        public abstract int APT { get; }

        /// <summary>
        /// BLO
        /// </summary>
        public abstract int BLO { get; }

        /// <summary>
        /// CON
        /// </summary>
        public abstract int CON { get; }

        /// <summary>
        /// BLOCodigo
        /// </summary>
        public abstract string BLOCodigo { get; }

        /// <summary>
        /// BLONome
        /// </summary>
        public abstract string BLONome { get; }

        /// <summary>
        /// CONCodigo
        /// </summary>
        public abstract string CONCodigo { get; }

        /// <summary>
        /// Email
        /// </summary>
        public abstract string EmailResponsavel { get; }

        /// <summary>
        /// Aligado
        /// </summary>
        public abstract bool Alugado { get; }

        /// <summary>
        /// APTNumero
        /// </summary>
        public abstract string APTNumero { get; }

        /// <summary>
        /// APTSeguro
        /// </summary>
        public abstract bool APTSeguro { get; }

        /// <summary>
        /// Email
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract string PESEmail(bool Proprietario);

        /// <summary>
        /// Retorna se o proprietário ou inquilino quer e-mail
        /// </summary>
        /// <param name="FormaSolicitada">Forma solicitada, na ausencia usa a cadastrada</param>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract bool QuerEmail(FormaPagamento? FormaSolicitada, bool Proprietario);

        /// <summary>
        /// Retorna se o proprietário ou inquilino quer impresso em papel
        /// </summary>
        /// <param name="FormaSolicitada">Forma solicitada, na ausencia usa a cadastrada</param>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract bool QuerImpresso(FormaPagamento? FormaSolicitada, bool Proprietario);

        /// <summary>
        /// CpfCnpj
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract DocBacarios.CPFCNPJ PESCpfCnpj(bool Proprietario);

        /// <summary>
        /// PESNome
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract string PESNome(bool Proprietario);

        /// <summary>
        /// PESEndereço
        /// </summary>
        /// <param name="FormaSolicitada">Forma solicitada, na ausencia usa a cadastrada</param>
        /// <param name="Proprietario"></param>
        /// <param name="ForcaPapel"></param>
        /// <returns></returns>
        public abstract string PESEndereco(FormaPagamento? FormaSolicitada, bool Proprietario, bool ForcaPapel);

        /// <summary>
        /// PESCEP
        /// </summary>
        /// <param name="FormaSolicitada"></param>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract string PESCEP(FormaPagamento? FormaSolicitada, bool Proprietario);        

        /// <summary>
        /// Forma de pagamento
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <param name="ForcaPapel"></param>
        /// <param name="FormaSolicitada">Forma solicitada, na ausencia usa a cadastrada</param>
        /// <returns></returns>
        public abstract FormaPagamento FormaPagamentoEfetivo(FormaPagamento? FormaSolicitada, bool Proprietario, bool ForcaPapel);

        /// <summary>
        /// PES para INSS do corpo diretivo
        /// </summary>
        public abstract int PES_INSS { get; }

        /// <summary>
        /// Status da cobrança
        /// </summary>
        /// <returns></returns>
        public abstract APTStatusCobranca StatusCobranca { get; set; }

        /// <summary>
        /// Iquilino que paga o rateio
        /// </summary>
        public abstract TipoPagIP InquilinoPagaRateio { get; }

        /// <summary>
        /// Dia diferente do condominio
        /// </summary>
        public abstract bool APTDiaDiferente { get; }

        /// <summary>
        /// Vencimento do boleto de condominio
        /// </summary>
        /// <param name="competencia"></param>
        /// <returns></returns>
        public abstract DateTime DataVencimento(ABS_Competencia competencia);

        /// <summary>
        /// Usuario internet
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract string APTUsuarioInternet(bool Proprietario);

        /// <summary>
        /// senha internet
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract string APTSenhaInternet(bool Proprietario);

        /// <summary>
        /// Conta para debito automático
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public abstract ABS_ContaCorrente ContaDebito(bool Proprietario);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Proprietario"></param>
        public abstract void AbrecApartamento(bool? Proprietario);

        /// <summary>
        /// Dados do proprietário
        /// </summary>
        public abstract ABS_Pessoa Proprietario { get; }

        /// <summary>
        /// Dados do inquilino
        /// </summary>
        public abstract ABS_Pessoa Inquilino { get; }
    }    

    
}