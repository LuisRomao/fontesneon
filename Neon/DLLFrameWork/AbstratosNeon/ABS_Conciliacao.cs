﻿using System;
using Abstratos;
using VirEnumeracoesNeon;

namespace AbstratosNeon
{
    /// <summary>
    /// Objeto abstrato para Conciliacao
    /// </summary>
    public abstract class ABS_Conciliacao : ABS_Base
    {
        private static string NomeObjeto = "dllbanco.Conciliacao";

        /// <summary>
        /// Retorna uma instância
        /// </summary>
        /// <returns></returns>
        public static ABS_Conciliacao Conciliacao()
        {
            return (ABS_Conciliacao)ContruaObjeto(NomeObjeto, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CCD"></param>
        /// <param name="PGF"></param>
        /// <param name="Data"></param>
        /// <param name="competencia"></param>
        /// <returns></returns>
        public abstract statusconsiliado ConciliarDebitoAutomatico(int CCD, int PGF, DateTime Data, ABS_Competencia competencia);

    }
}
