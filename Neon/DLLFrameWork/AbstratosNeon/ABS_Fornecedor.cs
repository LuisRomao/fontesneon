﻿using System;
using Abstratos;
using DocBacarios;
using VirEnumeracoesNeon;

namespace AbstratosNeon
{
    /// <summary>
    /// Classe abstrata para Fornecedor
    /// </summary>
    public abstract class ABS_Fornecedor : ABS_Base
    {
        private static string NomeObjetoProc = "CadastrosProc.Fornecedores.FornecedorProc";
        private static string NomeObjeto = "Cadastros.Fornecedores.Fornecedor";

        /// <summary>
        /// Procura um fornecedor com este CNPJ
        /// </summary>
        /// <param name="CNPJ"></param>
        /// <param name="PodeInserir">Pode inserir o fornecedor se nao estive cadastrado</param>
        /// <returns>Objeto fornecedor ou null</returns>
        /// <remarks>Se tiver mais de um ja seleciona, não tiver insere</remarks>
        public static ABS_Fornecedor ABSGetFornecedor(CPFCNPJ CNPJ, bool PodeInserir)
        {
            return (ABS_Fornecedor)MetodoEstatico(NomeObjeto,
                                                  "GetFornecedor",
                                                  new object[] { CNPJ, PodeInserir },
                                                  new Type[] { typeof(CPFCNPJ), typeof(bool) });
        }

        /// <summary>
        /// Procura um fornecedor com este CNPJ
        /// </summary>
        /// <param name="CNPJ"></param>
        /// <param name="PodeInserir">Pode inserir o fornecedor se nao estive cadastrado</param>
        /// <returns>Objeto fornecedor ou null</returns>
        /// <remarks>Se tiver mais de um ja seleciona, não tiver insere</remarks>
        public static ABS_Fornecedor ABSGetFornecedorProc(CPFCNPJ CNPJ, bool PodeInserir)
        {
            return (ABS_Fornecedor)MetodoEstatico(NomeObjetoProc,
                                                  "GetFornecedorProc",
                                                  new object[] { CNPJ, PodeInserir },
                                                  new Type[] { typeof(CPFCNPJ), typeof(bool) });
        }

        /// <summary>
        /// Cria um objeto real tipo Fornecedor (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Fornecedor GetFornecedor(int FRN)
        {
            return (ABS_Fornecedor)ContruaObjeto(NomeObjeto,
                                                new object[] { FRN },
                                                new Type[] { typeof(int) });
        }

        /// <summary>
        /// Cria um objeto real tipo Fornecedor (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Fornecedor GetFornecedorProc(int FRN)
        {
            return (ABS_Fornecedor)ContruaObjeto(NomeObjetoProc,
                                                new object[] { FRN },
                                                new Type[] { typeof(int) });
        }

        /// <summary>
        /// Código interno
        /// </summary>
        public abstract int FRN { get; }

        /// <summary>
        /// Nome do Fornecedor
        /// </summary>
        public abstract string FRNNome { get; }

        /// <summary>
        /// Endereço do Fornecedor
        /// </summary>
        public abstract string Endereco(ModeloEndereco Mod);

        /// <summary>
        /// CNPJ
        /// </summary>
        public abstract CPFCNPJ CNPJ { get; }

        /// <summary>
        /// Conta para depósito
        /// </summary>
        public abstract ABS_ContaCorrente ABSConta { get; }


        /// <summary>
        /// Nome do modelo de impresso para procuração
        /// </summary>
        public abstract string ModeloProcuracao { get; }

        /// <summary>
        /// Tem ou não procuração cadastrada
        /// </summary>
        public abstract bool TemProcuracaoCadastrada { get; }

    }
}
