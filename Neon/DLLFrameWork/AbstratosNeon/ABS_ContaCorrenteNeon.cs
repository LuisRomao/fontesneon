﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abstratos;
using FrameworkProc;

namespace AbstratosNeon
{
    /// <summary>
    /// Conta Corrente de Condomínio
    /// </summary>
    public class ABS_ContaCorrenteNeon: ABS_Base
    {
        private static string NomeObjeto = "FrameworkProc.objetosNeon.ContaCorrenteNeon";

        /// <summary>
        /// Cria um objeto real tipo Condomínio (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_ContaCorrenteNeon GetContaCorrenteNeon(int CCT, EMPTProc _EMPTProc1 = null)
        {
            return (ABS_ContaCorrenteNeon)ContruaObjeto(NomeObjeto,
                                             new object[] { CCT, _EMPTProc1 },
                                                new Type[] { typeof(int), typeof(EMPTProc) });
        }
    }
}
