﻿/*
MR - 26/12/2014 10:00           - Inclusão dos campos adicionais para Tributo Eletrônico (Inscricao e CodigoBarra) na UnidadeResposta, não obrigatorios (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
*/

using System;
using System.Collections.Generic;
using VirEnumeracoesNeon;
using Abstratos;

namespace AbstratosNeon
{
    /// <summary>
    /// Carrega os dados de um arquivo
    /// </summary>
    public abstract class ABS_DadosPer : ABS_Base
    {
        

        private static string NomeObjeto(TiposArquivo Tipo)
        {
            switch (Tipo)
            {
                case TiposArquivo.FGTS:
                    return "dllSefip.fgts";
                default:
                    return "";
            }            
        }

        /// <summary>
        /// Unidade de resposta
        /// </summary>
        public class UnidadeResposta
        {
            /// <summary>
            /// Código do condomínio
            /// </summary>
            public int Codigo;

            /// <summary>
            /// Valor total
            /// </summary>
            public decimal Valor;

            /// <summary>
            /// Competência
            /// </summary>
            public ABS_Competencia Competencia;

            /// <summary>
            /// Vencimento
            /// </summary>
            public DateTime Vencimento;

            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
            /// <summary>
            /// Incrição
            /// </summary>
            public string Inscricao;

            /// <summary>
            /// Código de Barras
            /// </summary>
            public string CodigoBarras;

            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="_Codigo"></param>
            /// <param name="_Competencia"></param>
            /// <param name="_Valor"></param>
            /// <param name="_Vencimento"></param>
            /// <param name="_Inscricao"></param>
            /// <param name="_CodigoBarras"></param>
            public UnidadeResposta(int _Codigo, ABS_Competencia _Competencia, decimal _Valor, DateTime _Vencimento, string _Inscricao = "", string _CodigoBarras = "")
            {
                Codigo = _Codigo;
                Competencia = _Competencia;
                Valor = _Valor;
                Vencimento = _Vencimento;
                Inscricao = _Inscricao;
                CodigoBarras = _CodigoBarras; 
            }
            //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***
        }

        /// <summary>
        /// Resultados da carga
        /// </summary>
        public SortedList<int, UnidadeResposta> Resultado;

        /// <summary>
        /// Competência lida no arquivo
        /// </summary>
        public ABS_Competencia Competencia;

        /// <summary>
        /// Cria uma instância
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public static ABS_DadosPer GetDadosPer(TiposArquivo Tipo)
        {
            return (ABS_DadosPer)ContruaObjeto(NomeObjeto(Tipo), null);
        }

        /// <summary>
        /// Carrega os dados
        /// </summary>
        /// <returns>Carregado ou não</returns>
        public abstract bool Carrega();        
    }

    
}
