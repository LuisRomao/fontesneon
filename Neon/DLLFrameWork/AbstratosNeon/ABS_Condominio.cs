﻿using System;
using System.Collections.Generic;
using Abstratos;
using VirEnumeracoesNeon;
using FrameworkProc;

namespace AbstratosNeon
{
    /// <summary>
    /// Objeto abstrato para Condomínio
    /// </summary>
    public abstract class ABS_Condominio : ABS_Base
    {
        private static string NomeObjeto = "Cadastros.Condominios.Condominio";
        private static string NomeObjetoProc = "CadastrosProc.Condominios.CondominioProc";

        /// <summary>
        /// Cria um objeto real tipo Condomínio (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Condominio GetCondominio(int CON)
        {
            return (ABS_Condominio)ContruaObjeto(NomeObjeto,
                                                new object[] { CON },
                                                new Type[] { typeof(int) });
        }

        /// <summary>
        /// Cria um objeto real tipo Condomínio (real Proc)
        /// </summary>
        /// <returns></returns>
        public static ABS_Condominio GetCondominioProc(int CON, EMPTProc _EMPTProc1 = null)
        {
            return (ABS_Condominio)ContruaObjeto(NomeObjetoProc,
                                                new object[] { CON, _EMPTProc1 },
                                                new Type[] { typeof(int), typeof(EMPTProc) });
        }

        /// <summary>
        /// Retorna os condomínio
        /// </summary>
        /// <param name="Ativos"></param>
        /// <returns></returns>
        public static SortedList<int, ABS_Condominio> ABSGetCondominios(bool Ativos)
        {
            return (SortedList<int, ABS_Condominio>)MetodoEstatico(NomeObjeto,
                                         "GetCondominios",
                                         new object[] { Ativos },
                                         new Type[] { typeof(bool) });
        }

        /// <summary>
        /// Retorna os condomínio Proc
        /// </summary>
        /// <param name="Ativos"></param>
        /// <returns></returns>
        public static SortedList<int, ABS_Condominio> ABSGetCondominiosProc(bool Ativos)
        {
            return (SortedList<int, ABS_Condominio>)MetodoEstatico(NomeObjetoProc,
                                         "GetCondominiosProc",
                                         new object[] { Ativos },
                                         new Type[] { typeof(bool) });
        }

        /// <summary>
        /// Código interno
        /// </summary>
        public abstract int CON { get; }

        /// <summary>
        /// Nome do condomínio
        /// </summary>
        public abstract string Nome { get; }

        /// <summary>
        /// Código do condomínio
        /// </summary>
        public abstract string CONCodigo { get; }

        /// <summary>
        /// Identificação do bloco BLOCO Quadra torre etc
        /// </summary>
        public abstract string IdentificacaoBloco { get; }

        /// <summary>
        /// Identificação do apartamento: Apartamento, casa ,sala etc CONNomeApto
        /// </summary>
        public abstract string IdentificacaoApartamento { get; }


        /// <summary>
        /// CONOcultaSenha
        /// </summary>
        public abstract bool OcultaSenhaBoleto { get; }

        /// <summary>
        /// CON_PSC
        /// </summary>
        public abstract int? CodigoSeguroConteudo { get; }

        /// <summary>
        /// Codigo folha de pagamentos
        /// </summary>
        public abstract int? CONCodigoFolha1 { get; }

        /// <summary>
        /// Juros
        /// </summary>
        public abstract decimal CONValorJuros { get; }

        /// <summary>
        /// CONDivideSaldos
        /// </summary>
        public abstract bool CONDivideSaldos { get; }

        /// <summary>
        /// Dados da conta corrente padrão do condomínio
        /// </summary>
        public abstract ABS_ContaCorrente ABSConta { get; }

        /// <summary>
        /// Valor da segunda via, se nao for para cobrar é zero
        /// </summary>
        public abstract decimal CustoSegudaVia { get; }

        /// <summary>
        /// 
        /// </summary>
        public abstract bool CONJurosPrimeiroMes { get; }

        /// <summary>
        /// 
        /// </summary>
        public abstract bool CONJurosCompostos { get; }

        /// <summary>
        /// Tipo de malote
        /// </summary>
        public abstract int CONMalote { get; }

        /// <summary>
        /// Se tem procuração
        /// </summary>
        public abstract bool CONProcuracao { get; }

        /// <summary>
        /// Como são chamados os blocos: bloco, torre, quadra etc
        /// </summary>
        public abstract string CONNomeBloco { get; }

        /// <summary>
        /// Como são chamadas as unidades: apartamento, casa, sala etc
        /// </summary>
        public abstract string CONNomeApto { get; }

        /// <summary>
        /// Endereço do condomínio
        /// </summary>
        /// <param name="Mod"></param>
        /// <returns></returns>
        public abstract string Endereco(ModeloEndereco Mod);

        /// <summary>
        /// Lisata dos Apartamentos
        /// </summary>
        public abstract SortedList<int,ABS_Apartamento> Apartamentos { get; }

        /// <summary>
        /// Dados do sindico
        /// </summary>
        public abstract ABS_Pessoa Sindico { get; }

        /// <summary>
        /// CNPJ do condomínio
        /// </summary>
        public abstract DocBacarios.CPFCNPJ CNPJ { get; }

        /// <summary>
        /// Escritório de Advocacia
        /// </summary>
        public abstract ABS_Fornecedor EscritorioAdvocacia { get; }

        /// <summary>
        /// Status do condomínio
        /// </summary>
        public abstract CONStatus Status { get; }
    }
}
