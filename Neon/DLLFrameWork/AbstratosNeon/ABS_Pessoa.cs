﻿using System;
using VirEnumeracoesNeon;
using Abstratos;
using DocBacarios;

namespace AbstratosNeon
{
    /// <summary>
    /// Classe com dados de pessoa
    /// </summary>
    public abstract class ABS_Pessoa : ABS_Base
    {
        private static string NomeObjeto = "CadastrosProc.PessoaProc";

        /// <summary>
        /// Cria um objeto real tipo Condomínio (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Pessoa GetPessoa(int PES)
        {
            return (ABS_Pessoa)ContruaObjeto(NomeObjeto,
                                             new object[] { PES },
                                             new Type[] { typeof(int) });
        }

        /// <summary>
        /// Nome
        /// </summary>
        public abstract string Nome { get; }

        /// <summary>
        /// CPF
        /// </summary>
        public abstract CPFCNPJ CPF { get; }

        /// <summary>
        /// Endereço do condomínio
        /// </summary>
        /// <param name="Mod"></param>
        /// <returns></returns>
        public abstract string Endereco(ModeloEndereco Mod);
    }
}
