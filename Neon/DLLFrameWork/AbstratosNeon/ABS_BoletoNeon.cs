﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abstratos;

namespace AbstratosNeon
{
    /// <summary>
    /// Objeto abstrato para boleto Neon
    /// </summary>
    public abstract class ABS_BoletoNeon : ABS_Boleto
    {
        /// <summary>
        /// Apartamento do boleto
        /// </summary>
        protected ABS_Apartamento _Apartamento;

        /// <summary>
        /// Fornecedor - pode ser nulo
        /// </summary>
        protected ABS_Fornecedor _Fornecedor;

        /// <summary>
        /// Apartamento do boleto
        /// </summary>
        public virtual ABS_Apartamento Apartamento 
        {
            get { return _Apartamento; }
            set { _Apartamento = value; } 
        }

        /// <summary>
        /// Fornecedor
        /// </summary>
        public virtual ABS_Fornecedor Fornecedor
        {
            get { return _Fornecedor; }
            set { _Fornecedor = value; }
        }

        /// <summary>
        /// Condomino do boleto
        /// </summary>
        protected ABS_Condominio _Condominio;

        /// <summary>
        /// Condomino do boleto
        /// </summary>
        public virtual ABS_Condominio Condominio 
        {
            get { return _Condominio; }
            set { _Condominio = value; }
        }

        /// <summary>
        /// Inclui Custo de segunda via
        /// </summary>
        /// <param name="QuestionarUsuario">Questiona ou nao o usuário</param>
        /// <returns></returns>
        public abstract bool IncluicustoSegundaVia(bool QuestionarUsuario);

        /// <summary>
        /// Código do condominio
        /// </summary>
        public abstract int CON { get; }

        /// <summary>
        /// Código APT
        /// </summary>
        public abstract int? APT { get; }
    }
}
