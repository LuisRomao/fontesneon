﻿/*
13/05/2014 13.2.8.49  LH reativação do XML
22/09/2015 14.2.7.133 LH Rentabilidade
*/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Abstratos;
using VirEnumeracoesNeon;


namespace AbstratosNeon
{
    /// <summary>
    /// Objeto abstrato para o balancete
    /// </summary>
    [DataContract(Name = "ABS_balancete", Namespace = "AbstratosNeon")]
    public abstract class ABS_balancete : ABS_Base
    {
        
        private static string NomeObjeto = "Balancete.GrBalancete.balancete";        

        /// <summary>
        /// Cria um objeto real tipo balancete (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_balancete GetBalancete(int CON, ABS_Competencia comp,TipoBalancete Tipo)
        {
            return (ABS_balancete)ContruaObjeto(NomeObjeto, 
                                                new object[] { CON, comp, Tipo }, 
                                                new Type[] { typeof(int), typeof(ABS_Competencia), typeof(TipoBalancete) });            
        }

        /// <summary>
        /// Retorna a path do pdf com o balancete se estiver disponível
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="_comp"></param>
        /// <param name="CompetenciaBoleto">indica se a competência fornecida é a do balancete ou do boleto</param>
        /// <returns></returns>
        public static string ImpressoGravado(int CON, ABS_Competencia _comp, bool CompetenciaBoleto)
        {
            return (string)MetodoEstatico(NomeObjeto,
                                         "ImpressoGravado",
                                         new object[] {CON,_comp,CompetenciaBoleto},
                                         new Type[] { typeof(int), typeof(ABS_Competencia), typeof(bool) });   
        }

        

        /// <summary>
        /// Indica se o balancete foi encontrado no Banco
        /// </summary>
        public abstract bool encontrado { get; }

        /// <summary>
        /// Status do balancete
        /// </summary>
        public abstract BALStatus Status { get; }

        /// <summary>
        /// Competência
        /// </summary>        
        public abstract ABS_Competencia ABS_comp { get; }

        /// <summary>
        /// Data inicial
        /// </summary>        
        public abstract DateTime DataI { get; }

        /// <summary>
        /// Data Final
        /// </summary>                
        public abstract DateTime DataF { get; }               

        /// <summary>
        /// Todas as contas Lógicas
        /// </summary>
        public abstract SortedList<int, ContaLogica> ContasLogicas { get; }

        /// <summary>
        /// Lista dos créditos
        /// </summary>
        public abstract SortedList<string, TotCredito> TotCreditos { get; }

        /// <summary>
        /// Valore do Realizado X previsto
        /// </summary>
        public abstract TotPrevXRel totPrevXRel { get; }        

        /// <summary>
        /// Retora os débitos para o impresso.
        /// </summary>
        public abstract Dictionary<string,Dictionary<string, TotDebito>> TotDebitosGrupo { get; }

        /// <summary>
        /// Carrega os dados
        /// </summary>      
        /// <param name="Completo">alem de carregar as contas carrega os lançamentos</param>
        public abstract void CarregarDados(bool Completo);

        /// <summary>
        /// Carrega os dados para o balancete
        /// </summary>
        public abstract void CarregarDadosBalancete();

        /// <summary>
        /// Carrega os dados para o balancete
        /// </summary>
        /// <param name="TpClass">Tipo de Agrupamente</param>
        /// <param name="Forcar">Forçar recalculo</param>
        public abstract void CarregarDadosBalancete(ABS_solicitaAgrupamento TpClass, bool Forcar = false);

        /// <summary>
        /// Simula um boleto com o balancete (em tela)
        /// </summary>
        /// <param name="PBO"></param>
        /// <param name="ForcarBalancete">Coloca o balancete no boleto mesmo que não esteja fechado</param>
        /// <param name="ReCarregarDados">Carrega os dados do balancete</param>
        /// <returns></returns>
        public abstract bool simulaBoleto(int? PBO = null, bool ForcarBalancete = false, bool ReCarregarDados = true);                

        /// <summary>
        /// Uma conta lógica
        /// </summary>
        public class ContaLogica
        {
            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="CTLNome"></param>
            /// <param name="SaldoI"></param>
            /// <param name="SaldoF"></param>
            /// <param name="Creditos"></param>
            /// <param name="Debitos"></param>
            /// <param name="Transferencias"></param>
            /// <param name="Rentabilidade">Rentabilidade</param>
            public ContaLogica(string CTLNome, decimal SaldoI, decimal SaldoF,decimal Creditos,decimal Debitos,decimal Transferencias,decimal Rentabilidade)
            {
                this.CTLNome = CTLNome;
                this.SaldoI = SaldoI;
                this.SaldoF = SaldoF;
                this.Creditos = Creditos;
                this.Debitos = Debitos;
                this.Transferencias = Transferencias;
                this.Rentabilidade = Rentabilidade;
            }

            /// <summary>
            /// Nome
            /// </summary>
            public string CTLNome;
            /// <summary>
            /// Saldo Inicial
            /// </summary>
            public decimal SaldoI;
            /// <summary>
            /// Saldo Final
            /// </summary>
            public decimal SaldoF;
            /// <summary>
            /// Total de Créditos
            /// </summary>
            public decimal Creditos;
            /// <summary>
            /// Total de debitos
            /// </summary>
            public decimal Debitos;
            /// <summary>
            /// Total de transferências
            /// </summary>
            public decimal Transferencias;
            /// <summary>
            /// Total de Rentabilidade
            /// </summary>
            public decimal Rentabilidade;
        }

                

        /// <summary>
        /// Um Grupo de débitos
        /// </summary>
        public class TotDebito
        {
            /// <summary>
            /// String com as descrições dos débitos (pareado com strValores)
            /// </summary>
            public string Descricao;

            /// <summary>
            /// String com os valores dos débitos (Descricao)
            /// </summary>
            public string strValores;

            /// <summary>
            /// Equivale ao Descricao e strValores só que detalhado
            /// </summary>
            public Dictionary<string, decimal> Desc_Valor;

            /// <summary>
            /// Total do grupo
            /// </summary>
            public decimal Total;

            /// <summary>
            /// Construtor
            /// </summary>
            public TotDebito()
            { 
                Descricao = strValores = "";
                Desc_Valor = new Dictionary<string, decimal>();
                Total = 0;
            }
        }

        /// <summary>
        /// Um crédito
        /// </summary>
        public class TotCredito 
        {
            /// <summary>
            /// Valor recebito com vencimeto após o período do balancete
            /// </summary>
            public decimal Antecipado;
            /// <summary>
            /// no periodo
            /// </summary>
            public decimal Mes;
            /// <summary>
            /// Vencimento anterior ao período
            /// </summary>
            public decimal Atrasado; 
            /// <summary>
            /// Previsto para o período
            /// </summary>
            public decimal Previsto; 
            /// <summary>
            /// parte do previsto recebida
            /// </summary>
            public decimal Realizado; 
            /// <summary>
            /// Descrição do crédito
            /// </summary>
            public string Descricao;
            /// <summary>
            /// Plano de contas
            /// </summary>
            public string PLA;
            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="Antecipado"></param>
            /// <param name="Mes"></param>
            /// <param name="Atrasado"></param>
            /// <param name="Previsto"></param>
            /// <param name="Realizado"></param>
            /// <param name="Descricao"></param>
            /// <param name="PLA"></param>
            public TotCredito(decimal Antecipado, decimal Mes, decimal Atrasado, decimal Previsto, decimal Realizado, string Descricao, string PLA = null)
            {
                this.Antecipado = Antecipado;
                this.Atrasado = Atrasado;
                this.Descricao = Descricao;
                this.Mes = Mes;
                this.Previsto = Previsto;
                this.Realizado = Realizado;
                this.PLA = PLA;
            }

            /// <summary>
            /// Total recebido no período
            /// </summary>
            public decimal TotRec { get { return Antecipado+Mes+Atrasado;} }
            
        }

        /// <summary>
        /// Valore do Realizado X previsto
        /// </summary>
        public class TotPrevXRel
        {
            /// <summary>
            /// Valor Previsto
            /// </summary>
            public decimal Previsto;
            /// <summary>
            /// Valor (de previsto) recebido antes do perríodo
            /// </summary>
            public decimal RecAnterior;
            /// <summary>
            /// Valor (de previsto) recebido no perríodo
            /// </summary>
            public decimal RecPrevisto;
            /// <summary>
            /// Valor (do previsto) não redebito até o fim do período
            /// </summary>
            public decimal Inad;
            /// <summary>
            /// Inadimplencia / previsto (percentual)
            /// </summary>
            public decimal perInad;
        }
    }

    
}
