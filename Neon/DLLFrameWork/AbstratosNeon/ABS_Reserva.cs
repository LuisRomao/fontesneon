﻿/*
MR - 14/01/2016 12:30             - Formas de cobrar por menor ou maior de condomínio (Alterações indicadas por *** MRC - INICIO - RESERVA (14/01/2016 12:30) ***)
MR - 15/08/2016 14:00             - Tipos de grade para reservas (Alterações indicadas por *** MRC - INICIO (15/08/2016 14:00) ***)
MR - 15/08/2016 14:00 -           - Tipos de grade, tipos de cancelamento e tipos de cobrança para reservas (Alterações indicadas por *** MRC - INICIO (15/08/2016 14:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstratosNeon
{
    /// <summary>
    /// Classe para tratamento das reservas
    /// </summary>
    public abstract class ABS_Reserva
    {
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum EQPFormaCobrar
    {
        /// <summary>
        /// Valor fixo
        /// </summary>
        Valor = 0,
        /// <summary>
        /// Proporcional ao salário mínimo
        /// </summary>
        Salario = 1,
        /// <summary>
        /// Proporcional ao valor do condomínio
        /// </summary>
        Condominio = 2,
        //*** MRC - INICIO - RESERVA (14/01/2016 12:30) ***
        /// <summary>
        /// Proporcional ao menor valor do condomínio
        /// </summary>
        CondominioMenor = 3,
        /// <summary>
        /// Proporcional ao maior valor do condomínio
        /// </summary>
        CondominioMaior = 4
        //*** MRC - TERMINO - RESERVA (14/01/2016 12:30) ***
    }

    //*** MRC - INICIO (15/08/2016 14:00) ***
    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum EQPTipoGrade
    {
        /// <summary>
        /// Dias corridos
        /// </summary>
        DiasCorridos = 0,
        /// <summary>
        /// Meses completos
        /// </summary>
        MesesCompletos = 1
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum EQPTipoCancelamento
    {
        /// <summary>
        /// Antes da data reservada
        /// </summary>
        AntesDataReservada = 0,
        /// <summary>
        /// Após efetuar a reserva
        /// </summary>
        AposEfetuarReversa = 1
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum EQPTipoCobranca
    {
        /// <summary>
        /// Total (100%)
        /// </summary>
        Total = 0,
        /// <summary>
        /// IMPRParcial (50%)
        /// </summary>
        Parcial = 1
    }
    //*** MRC - TERMINO (15/08/2016 14:00) ***

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum GradeReserva
    {
        /// <summary>
        /// Todos os dias
        /// </summary>
        Todos = 0,
        /// <summary>
        /// 
        /// </summary>
        Domingo = 1,
        /// <summary>
        /// 
        /// </summary>
        Segunda = 2,
        /// <summary>
        /// 
        /// </summary>
        Terca = 3,
        /// <summary>
        /// 
        /// </summary>
        Quarta = 4,
        /// <summary>
        /// 
        /// </summary>
        Quinta =5,
        /// <summary>
        /// 
        /// </summary>
        Sexta = 6,
        /// <summary>
        /// 
        /// </summary>
        Sabado = 7,
        /// <summary>
        /// 
        /// </summary>
        Segunda_Sexta = 8,
        /// <summary>
        /// 
        /// </summary>
        Sabado_Domingo = 9,
        /// <summary>
        /// 
        /// </summary>
        Terca_Sexta =10
    }

    
}
