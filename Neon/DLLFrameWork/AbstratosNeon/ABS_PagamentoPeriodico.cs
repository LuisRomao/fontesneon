﻿using System;
using Abstratos;
using VirEnumeracoesNeon;



namespace AbstratosNeon
{
    /// <summary>
    /// Objeto abstrato para o PagamentoPeriodico
    /// </summary>
    public abstract class ABS_PagamentoPeriodico : ABS_Base
    {
        private static string NomeObjeto     = "ContaPagar.Follow.PagamentoPeriodico";
        private static string NomeObjetoProc = "ContaPagarProc.PagamentoPeriodicoProc";
                                                
        /// <summary>
        /// Cria um objeto real tipo PagamentoPeriodico (real)
        /// </summary>
        /// <param name="PGF"></param>
        /// <returns></returns>
        public static ABS_PagamentoPeriodico GetPagamentoPeriodico(int PGF)
        {
            return (ABS_PagamentoPeriodico)ContruaObjeto(NomeObjeto, new object[] { PGF }, new Type[] { typeof(int) });
        }

        /// <summary>
        /// Cria um objeto real tipo PagamentoPeriodico pegando eventualmente do pool
        /// </summary>
        /// <param name="PGF"></param>
        /// <returns></returns>
        public static ABS_PagamentoPeriodico GetPagamentoPeriodicoPool(int PGF)
        {
            return (ABS_PagamentoPeriodico)ContruaObjeto(NomeObjeto, new object[] { PGF, true }, new Type[] { typeof(int),typeof(bool) });
        }

        /// <summary>
        /// Cria um objeto real tipo PagamentoPeriodico (real)
        /// </summary>
        /// <param name="PGF"></param>
        /// <param name="_EMPTProc"></param>
        /// <returns></returns>
        public static ABS_PagamentoPeriodico GetPagamentoPeriodicoProc(int PGF, object _EMPTProc)
        {
            return (ABS_PagamentoPeriodico)ContruaObjeto(NomeObjetoProc, new object[] { PGF,false, _EMPTProc }, new Type[] { typeof(int) ,typeof(bool), typeof(object) });
        }

        /// <summary>
        /// Carrega todos os PGFs Ativos
        /// </summary>
        /// <returns></returns>
        public static int CarregaPGFAtivos()
        {
            return (int)MetodoEstatico(NomeObjeto, "CarregaPGFAtivos", null);
        }

        /// <summary>
        /// Verifica se tem linha mae ativa
        /// </summary>
        public abstract bool Encontrado { get; }

        /// <summary>
        /// PLA
        /// </summary>
        public abstract string PLA { get; }

        /// <summary>
        /// Descrição
        /// </summary>
        public abstract string Descricao { get; }

        /// <summary>
        /// Comando para impressora termica referente ao pagamento periódico
        /// </summary>
        /// <returns></returns>
        public abstract string LinhaDaCopia();

        /// <summary>
        /// Imprime se não tiver documento ou se for para forçar
        /// </summary>
        /// <param name="Forcar">Forçar a impressão</param>
        public abstract void Imprimir(bool Forcar);

        /// <summary>
        /// esta função deve ser chamada quando for detectado o lançamento na conta.
        /// </summary>
        /// <param name="DataPag"></param>
        /// <param name="ValorEfetivo"></param>
        /// <param name="CCD"></param>
        /// <param name="CompServ">Competência</param>
        /// <returns></returns>
        public abstract bool ConciliaDebitoAutomatico(DateTime DataPag, decimal ValorEfetivo, int CCD, ABS_Competencia CompServ = null);

        /// <summary>
        /// Função que atualiza o cadastro verificando o proximo evento e cadastrando se necessário
        /// </summary>
        /// <returns></returns>
        public abstract bool CadastrarProximo();

        /// <summary>
        /// ValorProximoDebitoProgramado
        /// </summary>
        /// <returns></returns>
        public abstract decimal ValorProximoDebitoProgramado();

        /// <summary>
        /// Proxima Competência
        /// </summary>
        public abstract Abstratos.ABS_Competencia ABS_ProximaCompetencia { get; }

        /// <summary>
        /// Status
        /// </summary>
        public abstract StatusPagamentoPeriodico Status { get; }

        /// <summary>
        /// Salva as alterações e justificativa
        /// </summary>
        /// <param name="justificativa"></param>
        /// <returns></returns>
        public abstract bool Salvar(String justificativa);
        
    }
    
}