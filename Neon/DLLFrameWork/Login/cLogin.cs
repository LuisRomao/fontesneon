using System;
using System.Data;
using System.Windows.Forms;
using CompontesBasicos;
using FrameworkProc.datasets;

namespace Login
{
    /// <summary>
    /// Componente de Login
    /// </summary>
    public partial class cLogin : ComponenteBaseBindingSource
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public cLogin()
        {
            InitializeComponent();
        }

        private bool Conectado = false;

        /// <summary>
        /// Conecta com o banco de dados
        /// </summary>
        /// <returns></returns>
        internal bool Conecta()
        {
            try
            {
                BindingSource_F.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
                Conectado = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void cLogin_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                if (!Conectado)
                    Conecta();
                grvUsuarios.Focus();
            }
        }

        private void grvUsuarios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                txtSenha.Focus();
            }
        }

        private void grvUsuarios_DoubleClick(object sender, EventArgs e)
        {
            txtSenha.Focus();
        }

        //public String _Usuario = "";

        private void btnLogin_Click(object sender, EventArgs e)
        {
            bool aprovado = false;
            if (txtSenha.Text.ToUpper() == "AZUL FLORESTA") {                
                CompontesBasicos.Bancovirtual.UsuarioLogado.Desenvolvimento = txtSenha.Text.ToUpper();
                aprovado = true;
            };


            DataRowView DRV = (DataRowView)grvUsuarios.GetRow(grvUsuarios.FocusedRowHandle);
            if ((DRV == null) || (txtSenha.Text.ToUpper() != grvUsuarios.GetFocusedRowCellDisplayText(colUSUSenha).ToString()))
                MessageBox.Show("A senha informada n�o confere com o usu�rio selecionado.", "Login", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                aprovado = true;                                
            }
            if (aprovado) {
                if (DRV != null)
                {
                    dUSUarios.USUariosRow rowUSU = (dUSUarios.USUariosRow)DRV.Row;
                    Framework.DSCentral.USU = rowUSU.USU;
                    Framework.DSCentral.USUNome = rowUSU.USUNome;
                    if(!rowUSU.IsUSU__USUfilialNull())
                        Framework.DSCentral.USUFilial = rowUSU.USU__USUfilial;
                    new Framework.usuarioLogado.UsuarioLogadoNeon(rowUSU.USU);
                };

                if (this.Parent is Form)
                    (this.Parent as Form).DialogResult = DialogResult.OK;
            }

        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                btnLogin.PerformClick();
            }
        }
    }
}