namespace Login
{
    partial class fLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cLogin1 = new Login.cLogin();
            this.SuspendLayout();
            // 
            // cLogin1
            // 
            this.cLogin1.Autofill = true;
            this.cLogin1.CaixaAltaGeral = true;
            this.cLogin1.Cursor = System.Windows.Forms.Cursors.Default;
            this.cLogin1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.cLogin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cLogin1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cLogin1.LinhaMae_F = null;
            this.cLogin1.Location = new System.Drawing.Point(0, 0);
            this.cLogin1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cLogin1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cLogin1.Name = "cLogin1";
            this.cLogin1.Size = new System.Drawing.Size(707, 468);
            this.cLogin1.somenteleitura = false;
            this.cLogin1.TabIndex = 0;
            this.cLogin1.TableAdapterPrincipal = null;
            this.cLogin1.Titulo = null;
            this.cLogin1.Load += new System.EventHandler(this.cLogin1_Load);
            // 
            // fLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 468);
            this.Controls.Add(this.cLogin1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Acesso ao Sistema";
            this.ResumeLayout(false);

        }

        #endregion

        private cLogin cLogin1;






    }
}