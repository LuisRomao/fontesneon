using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Login
{
    /// <summary>
    /// Formulário que contem o componente cLogin
    /// </summary>
    public partial class fLogin : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public fLogin()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Conecta
        /// </summary>
        /// <returns></returns>
        public bool Conecta()
        {
            return cLogin1.Conecta();
        }

        private void cLogin1_Load(object sender, EventArgs e)
        {

        }
    }
}