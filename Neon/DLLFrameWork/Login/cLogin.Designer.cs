namespace Login
{
    partial class cLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cLogin));
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.grdUsuarios = new DevExpress.XtraGrid.GridControl();
            this.grvUsuarios = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colUSUFoto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colUSULogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUSenha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnLogin = new DevExpress.XtraEditors.SimpleButton();
            this.txtSenha = new DevExpress.XtraEditors.TextEdit();
            this.LabMensagem = new System.Windows.Forms.Label();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.LabNome = new System.Windows.Forms.Label();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labLogin = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenha.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "USUarios";
            this.BindingSource_F.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            this.BindingSource_F.Filter = "(USULogin <> \'\') and (USUSenha <> \'\')";
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.StretchHorizontal;
            this.pictureEdit1.Size = new System.Drawing.Size(1589, 99);
            this.pictureEdit1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Info;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(0, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1589, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Para logar, clique na sua conta de usuário e informe sua senha.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grdUsuarios
            // 
            this.grdUsuarios.Cursor = System.Windows.Forms.Cursors.Default;
            this.grdUsuarios.DataSource = this.BindingSource_F;
            this.grdUsuarios.Dock = System.Windows.Forms.DockStyle.Left;
            this.grdUsuarios.Location = new System.Drawing.Point(0, 115);
            this.grdUsuarios.LookAndFeel.SkinName = "Money Twins";
            this.grdUsuarios.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdUsuarios.MainView = this.grvUsuarios;
            this.grdUsuarios.Name = "grdUsuarios";
            this.grdUsuarios.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1});
            this.grdUsuarios.Size = new System.Drawing.Size(193, 684);
            this.grdUsuarios.TabIndex = 6;
            this.grdUsuarios.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvUsuarios});
            // 
            // grvUsuarios
            // 
            this.grvUsuarios.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvUsuarios.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvUsuarios.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.Empty.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.EvenRow.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.EvenRow.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.EvenRow.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.FilterPanel.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.FilterPanel.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.grvUsuarios.Appearance.FixedLine.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.grvUsuarios.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.grvUsuarios.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.FooterPanel.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.GroupButton.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.GroupButton.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvUsuarios.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvUsuarios.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.GroupFooter.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.GroupFooter.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.GroupPanel.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.GroupPanel.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvUsuarios.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvUsuarios.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.GroupRow.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.GroupRow.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.GroupRow.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.grvUsuarios.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.grvUsuarios.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.grvUsuarios.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.grvUsuarios.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.HorzLine.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.OddRow.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.OddRow.Options.UseBorderColor = true;
            this.grvUsuarios.Appearance.OddRow.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.grvUsuarios.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.grvUsuarios.Appearance.Preview.Options.UseFont = true;
            this.grvUsuarios.Appearance.Preview.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.grvUsuarios.Appearance.Row.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.Row.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvUsuarios.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.RowSeparator.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.grvUsuarios.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grvUsuarios.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.grvUsuarios.Appearance.TopNewRow.Options.UseBackColor = true;
            this.grvUsuarios.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvUsuarios.Appearance.VertLine.Options.UseBackColor = true;
            this.grvUsuarios.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUSUFoto,
            this.colUSULogin,
            this.colUSUSenha});
            this.grvUsuarios.GridControl = this.grdUsuarios;
            this.grvUsuarios.Name = "grvUsuarios";
            this.grvUsuarios.OptionsBehavior.AllowIncrementalSearch = true;
            this.grvUsuarios.OptionsBehavior.Editable = false;
            this.grvUsuarios.OptionsView.EnableAppearanceEvenRow = true;
            this.grvUsuarios.OptionsView.EnableAppearanceOddRow = true;
            this.grvUsuarios.OptionsView.ShowColumnHeaders = false;
            this.grvUsuarios.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvUsuarios.OptionsView.ShowGroupPanel = false;
            this.grvUsuarios.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.grvUsuarios.OptionsView.ShowIndicator = false;
            this.grvUsuarios.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.grvUsuarios.PaintStyleName = "Skin";
            this.grvUsuarios.RowHeight = 30;
            this.grvUsuarios.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colUSULogin, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.grvUsuarios.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grvUsuarios.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grvUsuarios_KeyPress);
            this.grvUsuarios.DoubleClick += new System.EventHandler(this.grvUsuarios_DoubleClick);
            // 
            // colUSUFoto
            // 
            this.colUSUFoto.Caption = "USUFoto";
            this.colUSUFoto.ColumnEdit = this.repositoryItemPictureEdit1;
            this.colUSUFoto.FieldName = "USUFoto";
            this.colUSUFoto.Name = "colUSUFoto";
            this.colUSUFoto.OptionsColumn.AllowEdit = false;
            this.colUSUFoto.OptionsColumn.AllowFocus = false;
            this.colUSUFoto.OptionsColumn.AllowIncrementalSearch = false;
            this.colUSUFoto.OptionsColumn.AllowMove = false;
            this.colUSUFoto.OptionsColumn.FixedWidth = true;
            this.colUSUFoto.OptionsColumn.ReadOnly = true;
            this.colUSUFoto.Visible = true;
            this.colUSUFoto.VisibleIndex = 0;
            this.colUSUFoto.Width = 40;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.CustomHeight = 50;
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray;
            this.repositoryItemPictureEdit1.ReadOnly = true;
            this.repositoryItemPictureEdit1.ShowMenu = false;
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // colUSULogin
            // 
            this.colUSULogin.Caption = "USULogin";
            this.colUSULogin.FieldName = "USULogin";
            this.colUSULogin.Name = "colUSULogin";
            this.colUSULogin.OptionsColumn.AllowEdit = false;
            this.colUSULogin.OptionsColumn.AllowMove = false;
            this.colUSULogin.Visible = true;
            this.colUSULogin.VisibleIndex = 1;
            this.colUSULogin.Width = 60;
            // 
            // colUSUSenha
            // 
            this.colUSUSenha.Caption = "USUSenha";
            this.colUSUSenha.FieldName = "USUSenha";
            this.colUSUSenha.Name = "colUSUSenha";
            this.colUSUSenha.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnLogin);
            this.groupControl1.Controls.Add(this.txtSenha);
            this.groupControl1.Controls.Add(this.LabMensagem);
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Controls.Add(this.LabNome);
            this.groupControl1.Controls.Add(this.pictureEdit2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(193, 115);
            this.groupControl1.LookAndFeel.SkinName = "Money Twins";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(1396, 684);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "groupControl1";
            // 
            // btnLogin
            // 
            this.btnLogin.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnLogin.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(105)))), ((int)(((byte)(175)))));
            this.btnLogin.Appearance.Options.UseFont = true;
            this.btnLogin.Appearance.Options.UseForeColor = true;
            this.btnLogin.Image = ((System.Drawing.Image)(resources.GetObject("btnLogin.Image")));
            this.btnLogin.Location = new System.Drawing.Point(122, 218);
            this.btnLogin.LookAndFeel.SkinName = "Liquid Sky";
            this.btnLogin.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(97, 39);
            this.btnLogin.TabIndex = 6;
            this.btnLogin.Text = "Entrar";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtSenha
            // 
            this.txtSenha.Location = new System.Drawing.Point(70, 177);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 15F);
            this.txtSenha.Properties.Appearance.Options.UseFont = true;
            this.txtSenha.Properties.PasswordChar = '●';
            this.txtSenha.Size = new System.Drawing.Size(198, 34);
            this.txtSenha.StyleController = this.StyleController_F;
            this.txtSenha.TabIndex = 5;
            this.txtSenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSenha_KeyPress);
            // 
            // LabMensagem
            // 
            this.LabMensagem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabMensagem.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabMensagem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(105)))), ((int)(((byte)(175)))));
            this.LabMensagem.Location = new System.Drawing.Point(103, 32);
            this.LabMensagem.Name = "LabMensagem";
            this.LabMensagem.Size = new System.Drawing.Size(1268, 108);
            this.LabMensagem.TabIndex = 4;
            this.LabMensagem.Text = "Bem Vindo";
            this.LabMensagem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Location = new System.Drawing.Point(26, 146);
            this.groupControl3.LookAndFeel.SkinName = "Money Twins";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1343, 1);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "groupControl3";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Location = new System.Drawing.Point(26, 25);
            this.groupControl2.LookAndFeel.SkinName = "Money Twins";
            this.groupControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.ShowCaption = false;
            this.groupControl2.Size = new System.Drawing.Size(1343, 1);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "groupControl2";
            // 
            // LabNome
            // 
            this.LabNome.AutoSize = true;
            this.LabNome.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "USUNome", true));
            this.LabNome.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabNome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(101)))), ((int)(((byte)(181)))));
            this.LabNome.Location = new System.Drawing.Point(23, 8);
            this.LabNome.Name = "LabNome";
            this.LabNome.Size = new System.Drawing.Size(110, 14);
            this.LabNome.TabIndex = 1;
            this.LabNome.Text = "Nome do Usuário";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUFoto", true));
            this.pictureEdit2.Location = new System.Drawing.Point(7, 41);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(241)))), ((int)(((byte)(254)))));
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.LookAndFeel.SkinName = "Liquid Sky";
            this.pictureEdit2.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit2.Size = new System.Drawing.Size(90, 90);
            this.pictureEdit2.TabIndex = 0;
            // 
            // labLogin
            // 
            this.labLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(170)))), ((int)(((byte)(219)))));
            this.labLogin.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labLogin.ForeColor = System.Drawing.Color.White;
            this.labLogin.Location = new System.Drawing.Point(3112, 73);
            this.labLogin.Name = "labLogin";
            this.labLogin.Size = new System.Drawing.Size(86, 21);
            this.labLogin.TabIndex = 8;
            this.labLogin.Text = "L O G I N";
            // 
            // cLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labLogin);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.grdUsuarios);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureEdit1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cLogin";
            this.Size = new System.Drawing.Size(1589, 799);
            this.Load += new System.EventHandler(this.cLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenha.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdUsuarios;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView grvUsuarios;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUFoto;
        private DevExpress.XtraGrid.Columns.GridColumn colUSULogin;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUSenha;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private System.Windows.Forms.Label LabNome;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label LabMensagem;
        private DevExpress.XtraEditors.TextEdit txtSenha;
        private DevExpress.XtraEditors.SimpleButton btnLogin;
        private System.Windows.Forms.Label labLogin;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
    }
}
