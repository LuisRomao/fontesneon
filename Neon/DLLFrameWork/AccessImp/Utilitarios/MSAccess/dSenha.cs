﻿namespace AccessImp.Utilitarios.MSAccess {


    partial class dSenha
    {
        private dSenhaTableAdapters.ProprietárioTableAdapter proprietarioTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ProprietÃ¡rio
        /// </summary>
        public dSenhaTableAdapters.ProprietárioTableAdapter ProprietarioTableAdapter {
            get {
                if (proprietarioTableAdapter == null) {
                    proprietarioTableAdapter = new dSenhaTableAdapters.ProprietárioTableAdapter();
                    proprietarioTableAdapter.TrocarStringDeConexao();
                };
                return proprietarioTableAdapter;
            }
        }

        private dSenhaTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dSenhaTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dSenhaTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao();
                };
                return aPARTAMENTOSTableAdapter;
            }
        }

    }
}
