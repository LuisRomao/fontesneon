﻿using System;
using Framework.datasets;
using System.Text;
using System.IO;

namespace FrameworkProc
{
    /// <summary>
    /// Classe para Extensions
    /// </summary>
    public static class ExtensionNeon
    {
        /// <summary>
        /// Verifica se o dia é feriado
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool IsFeriado(this DateTime data)
        {
            return (dFERiados.dFERiadosSt.FERiados.FindByFERData(data) != null);
        }

        /// <summary>
        /// Verifica se o dia é útil
        /// </summary>
        /// <param name="data"></param>
        /// <param name="SabadosUtes"></param>
        /// <param name="FeriadosUteis"></param>
        /// <returns></returns>
        public static bool IsUtil(this DateTime data, bool SabadosUtes = false,bool FeriadosUteis=false)
        {
            if (data.DayOfWeek == DayOfWeek.Sunday)
                return false;
            if (!SabadosUtes && (data.DayOfWeek == DayOfWeek.Saturday))
                return false;
            if (!FeriadosUteis && IsFeriado(data))
                return false;
            return true;
        }

        /// <summary>
        /// Calcula o próximo dia útil mas NÃO altera a isntancia
        /// </summary>
        /// <param name="Data">Data base</param>
        /// <param name="Dias">Dias a pular</param>
        /// <param name="sentido"></param>
        /// <param name="SabadosUtes"></param>
        /// <param name="FeriadosUteis"></param>
        /// <returns></returns>        
        public static DateTime SomaDiasUteis(this DateTime Data, int Dias, Sentido sentido=Sentido.Frente, bool SabadosUtes = false, bool FeriadosUteis = false)
        {
            int passo = sentido == Sentido.Frente ? 1 : -1;
            while ((Dias > 0) || (!IsUtil(Data, SabadosUtes, FeriadosUteis)))
            {
                Data = Data.AddDays(passo);
                if (IsUtil(Data, SabadosUtes, FeriadosUteis))
                    Dias--;
            }
            return Data;
        }

        
        /// <summary>
        /// Limpa o string
        /// </summary>
        /// <param name="Entrada"></param>
        /// <param name="Tipos"></param>
        /// <remarks>o conteúdo da entrada é alterado</remarks>
        /// <returns></returns>
        [Obsolete("Usar o do CompontesBasicosProc")]
        public static string Limpa(this string Entrada,params TiposLimpesa[] Tipos)
        {
            bool RemoveBrancosDuplicados = false;
            bool RemoveQuebrasLinha = false;
            bool SoNumeros = false;
            bool RemoveAcentos = false;
            bool Maiusculas = false;
            bool PrimeiroCaracMaiusculo = false;
            bool NomeArquivo = false;
            foreach (TiposLimpesa Tipo in Tipos)
                if (Tipo == TiposLimpesa.RemoveBrancosDuplicados)
                    RemoveBrancosDuplicados = true;
                else if (Tipo == TiposLimpesa.RemoveQuebrasLinha)
                    RemoveQuebrasLinha = true;
                else if (Tipo == TiposLimpesa.SoNumeros)
                    SoNumeros = true;
                else if (Tipo == TiposLimpesa.RemoveAcentos)
                    RemoveAcentos = true;
                else if (Tipo == TiposLimpesa.Maiusculas)
                    Maiusculas = true;
                else if (Tipo == TiposLimpesa.PrimeiroCaracMaiusculo)
                    PrimeiroCaracMaiusculo = true;
                else if (Tipo == TiposLimpesa.NomeArquivo)
                {
                    NomeArquivo = true;
                    RemoveBrancosDuplicados = true;
                }
            StringBuilder SB = new StringBuilder();
            bool eBranco = false;
            bool InicioPal = true;
            char novoc;
            foreach (char c in Entrada)
            {
                if (RemoveAcentos)
                    switch (c)
                    {
                        case 'á':
                        case 'à':
                        case 'ã':
                        case 'â':
                            novoc = 'a';
                            break;
                        case 'é':
                        case 'è':
                        case 'ê':
                            novoc = 'e';
                            break;
                        case 'í':
                        case 'ì':
                        case 'î':
                            novoc = 'i';
                            break;
                        case 'ó':
                        case 'ò':
                        case 'õ':
                        case 'ô':
                            novoc = 'o';
                            break;
                        case 'ú':
                        case 'ù':
                        case 'û':
                        case 'ü':
                            novoc = 'u';
                            break;
                        case 'ç':
                            novoc = 'c';
                            break;
                        case 'Á':
                        case 'À':
                        case 'Ã':
                        case 'Â':
                            novoc = 'A';
                            break;
                        case 'É':
                        case 'È':
                        case 'Ê':
                            novoc = 'E';
                            break;
                        case 'Í':
                        case 'Ì':
                        case 'Î':
                            novoc = 'I';
                            break;
                        case 'Ó':
                        case 'Ò':
                        case 'Õ':
                        case 'Ô':
                            novoc = 'O';
                            break;
                        case 'Ú':
                        case 'Ù':
                        case 'Û':
                        case 'Ü':
                            novoc = 'U';
                            break;
                        case 'Ç':
                            novoc = 'C';
                            break;
                        default:
                            novoc = c;
                            break;
                    }
                else
                    novoc = c;
                if (PrimeiroCaracMaiusculo)
                {
                    if (c == ' ')
                        InicioPal = true;
                    else
                        if (InicioPal)
                    {
                        novoc = char.ToUpper(novoc);
                        InicioPal = false;
                    }
                }
                if (SoNumeros)
                    if (!char.IsDigit(novoc))
                        continue;
                if (NomeArquivo)
                    if ((c == '\\') || (c == '/') || (c == '.'))
                        novoc = '_';
                if (RemoveBrancosDuplicados)
                {
                    if (novoc == ' ')
                    {
                        if (eBranco)
                            continue;
                        else
                            eBranco = true;
                    }
                    else
                        eBranco = false;
                }
                if (RemoveQuebrasLinha)
                    if ((novoc == '\r') || (novoc == '\n'))
                        continue;
                SB.Append(novoc);
            }
            Entrada = SB.ToString();
            if (RemoveBrancosDuplicados)
                Entrada = Entrada.Trim();
            if (Maiusculas)
                return Entrada.ToUpper();
            return Entrada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="Tipos"></param>
        /// <returns></returns>
        [Obsolete("Usar o do CompontesBasicosProc")]
        public static string Limpa(this Stream Str,params TiposLimpesa[] Tipos)
        {
            Str.Position = 0;
            string strLido = new StreamReader(Str).ReadToEnd();
            return Limpa(strLido, Tipos);
        }
    }

    /// <summary>
    /// Sentido de arredondamento
    /// </summary>
    public enum Sentido
    {
        /// <summary>
        /// Usa datas superiores
        /// </summary>
        Frente,
        /// <summary>
        /// Usa datas anteriores
        /// </summary>
        Traz
    }

    /// <summary>
    /// Tipo de limpeza de string
    /// </summary>
    [Obsolete("Usar o do CompontesBasicosProc")]
    public enum TiposLimpesa
    {
        /// <summary>
        /// 
        /// </summary>
        RemoveBrancosDuplicados,
        /// <summary>
        /// 
        /// </summary>
        RemoveQuebrasLinha,
        /// <summary>
        /// 
        /// </summary>
        SoNumeros,
        /// <summary>
        /// 
        /// </summary>
        RemoveAcentos,
        /// <summary>
        /// 
        /// </summary>
        Maiusculas,
        /// <summary>
        /// 
        /// </summary>
        PrimeiroCaracMaiusculo,
        /// <summary>
        /// 
        /// </summary>
        NomeArquivo
    }
}