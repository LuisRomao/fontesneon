﻿using System;

namespace FrameworkProc.objetosNeon
{


    partial class dLOGGeral : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dLOGGeral(EMPTProc _EMPTProc1)
            : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        private dLOGGeralTableAdapters.LinhaLOgTableAdapter linhaLOgTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: LinhaLOg
        /// </summary>
        public dLOGGeralTableAdapters.LinhaLOgTableAdapter LinhaLOgTableAdapter
        {
            get
            {
                if (linhaLOgTableAdapter == null)
                {
                    linhaLOgTableAdapter = new dLOGGeralTableAdapters.LinhaLOgTableAdapter();
                    linhaLOgTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return linhaLOgTableAdapter;
            }
        }

        private dLOGGeralTableAdapters.LLOxLICTableAdapter lLOxLICTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: LLOxLIC
        /// </summary>
        public dLOGGeralTableAdapters.LLOxLICTableAdapter LLOxLICTableAdapter
        {
            get
            {
                if (lLOxLICTableAdapter == null)
                {
                    lLOxLICTableAdapter = new dLOGGeralTableAdapters.LLOxLICTableAdapter();
                    lLOxLICTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return lLOxLICTableAdapter;
            }
        }
    }
}
