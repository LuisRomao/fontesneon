﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FrameworkProc;
using VirMSSQL;


namespace FrameworkProc.objetosNeon
{
    /// <summary>
    /// Classe para LOG
    /// </summary>
    public class LOGGeralProc : IEMPTProc
    {
        private VirEnumeracoesNeon.LLOxXXX LLOxXXX;
        private int ID = 0;
        private bool Valido = false;

        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                {
                    if (TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                }
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        private dLOGGeral dLOGGeral1;

        #region Contrutores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="EMPTProc"></param>
        public LOGGeralProc(EMPTProc EMPTProc = null)
        {
            if (_EMPTProc1 == null)
            {
                if (TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
            dLOGGeral1 = new dLOGGeral(EMPTProc);
        }

        private void Carrega(bool Forcar = false)
        {
            if (ID == 0)
                return;
            if (Forcar || !Valido)
            {
                switch (LLOxXXX)
                {
                    case VirEnumeracoesNeon.LLOxXXX.LIC:
                        dLOGGeral1.LinhaLOgTableAdapter.FillByLIC(dLOGGeral1.LinhaLOg, ID);
                        break;
                    default: throw new NotImplementedException(string.Format("enumeração:{0} em LOGGeral.cs 79", LLOxXXX));
                }
                Valido = true;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_LLOxXXX"></param>
        /// <param name="_ID"></param>
        /// <param name="EMPTProc"></param>
        public LOGGeralProc(VirEnumeracoesNeon.LLOxXXX _LLOxXXX, int _ID, EMPTProc EMPTProc = null) : this(EMPTProc)
        {
            LLOxXXX = _LLOxXXX;
            ID = _ID;
            Carrega();            
        }
        #endregion

        #region Registrar
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo">Tipo de log</param>
        /// <param name="TipoLocal">Tipo com significado para tabela Ligada</param>
        /// <param name="Data"></param>
        /// <param name="Texto"></param>
        /// <param name="LLOxXXX"></param>
        /// <param name="ID"></param>
        /// <param name="EMPTProc"></param>
        public static void RegistraLogST(VirEnumeracoesNeon.LLOTipo Tipo, int TipoLocal, DateTime Data, string Texto, VirEnumeracoesNeon.LLOxXXX LLOxXXX, int ID, EMPTProc EMPTProc = null)
        {
            LOGGeralProc LOGGeralProc1 = new LOGGeralProc(EMPTProc);
            LOGGeralProc1.RegistraLog(Tipo, TipoLocal, Data, Texto, LLOxXXX, ID);
        }

        /// <summary>
        /// Registra no log
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="TipoLocal"></param>
        /// <param name="Data"></param>
        /// <param name="Texto"></param>
        public void RegistraLog(VirEnumeracoesNeon.LLOTipo Tipo, int TipoLocal, DateTime Data, string Texto)
        {
            RegistraLog(Tipo, TipoLocal, Data, Texto, LLOxXXX, ID);
        }

        /// <summary>
        /// Da entrada em um log que aponta apenas uma tabela
        /// </summary>
        /// <param name="Tipo">Tipo de log</param>
        /// <param name="TipoLocal">Tipo com significado para tabela Ligada</param>
        /// <param name="Data"></param>
        /// <param name="Texto"></param>
        /// <param name="LLOxXXX">Tabela</param>
        /// <param name="ID">ID da tabela</param>        
        public void RegistraLog(VirEnumeracoesNeon.LLOTipo Tipo, int TipoLocal, DateTime Data, string Texto, VirEnumeracoesNeon.LLOxXXX LLOxXXX , int ID)
        {
            SortedList<VirEnumeracoesNeon.LLOxXXX, int> Dados = new SortedList<VirEnumeracoesNeon.LLOxXXX, int>();
            Dados.Add(LLOxXXX, ID);
            RegistraLog(Tipo, TipoLocal, Data, Texto, Dados);
        }

        /// <summary>
        /// Da entrada em um log que aponta múltiplas tabelas
        /// </summary>
        /// <param name="Tipo">Tipo de log</param>
        /// <param name="TipoLocal">Tipo com significado para tabela Ligada</param>
        /// <param name="Data"></param>
        /// <param name="Texto"></param>
        /// <param name="LigacoesXXX">Ligações tabela / ID</param>        
        public void RegistraLog(VirEnumeracoesNeon.LLOTipo Tipo, int TipoLocal, DateTime Data, string Texto, SortedList<VirEnumeracoesNeon.LLOxXXX, int> LigacoesXXX)
        {
            dLOGGeral.LinhaLOgRow Nova = dLOGGeral1.LinhaLOg.NewLinhaLOgRow();
            Nova.LLOData = Data;
            Nova.LLOTexto = Texto;
            Nova.LLOTipo = (int)Tipo;
            Nova.LLOTipoLocal = TipoLocal;
            Nova.LLO_USU = EMPTProc1.USU;
            dLOGGeral1.LinhaLOg.AddLinhaLOgRow(Nova);
            try
            {
                EMPTProc1.AbreTrasacaoSQL("RegistraLog LOGGeral.cs 52", dLOGGeral1.LinhaLOgTableAdapter);
                dLOGGeral1.LinhaLOgTableAdapter.Update(Nova);
                foreach (VirEnumeracoesNeon.LLOxXXX LLOxXXX in LigacoesXXX.Keys)
                {
                    switch (LLOxXXX)
                    {
                        case VirEnumeracoesNeon.LLOxXXX.LIC:
                            EMPTProc1.EmbarcaEmTrans(dLOGGeral1.LLOxLICTableAdapter);
                            dLOGGeral.LLOxLICRow NovaLLOxLIC = dLOGGeral1.LLOxLIC.NewLLOxLICRow();
                            NovaLLOxLIC.LLO = Nova.LLO;
                            NovaLLOxLIC.LIC = LigacoesXXX[LLOxXXX];
                            dLOGGeral1.LLOxLIC.AddLLOxLICRow(NovaLLOxLIC);
                            dLOGGeral1.LLOxLICTableAdapter.Update(NovaLLOxLIC);
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Não implementado tipo: {0}", LLOxXXX));
                    }
                }
                EMPTProc1.Commit();
            }
            catch (Exception ex)
            {
                EMPTProc1.Vircatch(ex);
            }
            Valido = false;
        }

        #endregion

        /// <summary>
        /// Relatório de log
        /// </summary>
        /// <returns></returns>
        public string Relatorio()
        {
            Carrega();
            string Recuo = "    ";
            string linhaDIV    = "**********************************************";
            string linhaPontos = "..............................................";
            string linhaDIV2 = string.Format("{0}{1}", Recuo,linhaPontos.Substring(Recuo.Length));
            StringBuilder SB = new StringBuilder();
            SB.AppendLine(linhaDIV);
            foreach (dLOGGeral.LinhaLOgRow row in dLOGGeral1.LinhaLOg)
            {                
                SB.AppendFormat("{0:dd/MM/yyyy HH:mm:ss} - {1}\r\n",row.LLOData,row.USUNome);
                string[] linhas = row.LLOTexto.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                //if (linhas.Length > 1)
                //    SB.AppendLine(linhaDIV2);
                foreach (string Linha in linhas)
                    SB.AppendFormat("{0}{1}\r\n", Recuo, Linha);
                //if (linhas.Length > 1)
                //    SB.AppendLine(linhaDIV2);
                SB.AppendLine(linhaDIV);
            }
            return SB.ToString();
        }

    }
}
