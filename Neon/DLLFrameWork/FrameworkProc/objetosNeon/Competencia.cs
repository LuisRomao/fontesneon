using System;
using System.Collections.Generic;
using System.Text;
using Abstratos;
using FrameworkProc;

namespace Framework.objetosNeon
{
    /// <summary>
    /// Objeto compet�ncia
    /// </summary>
    public class Competencia:ABS_Competencia
    {


        #region Opeardores

        /// <summary>
        /// Retorna se igual
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (!(obj is Competencia))
                return false;
            return (this == (Competencia)obj);
        }

        /// <summary>
        /// Retorna GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// operador ==
        /// </summary>
        public static bool operator ==(Competencia a, Competencia b)
        {
            if (object.Equals(a, null) && (object.Equals(b,null)))
                return true;
            if (object.Equals(a, null) || (object.Equals(b, null)))
                return false;
            try
            {
                return ((a.Mes == b.Mes) && (a.Ano == b.Ano));
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// operador !=
        /// </summary>
        public static bool operator !=(Competencia a, Competencia b)
        {
            if (object.Equals(a, null) && (object.Equals(b, null)))
                return false;
            if (object.Equals(a, null) || (object.Equals(b, null)))
                return true;
            try
            {
                return ((a.Mes != b.Mes) || (a.Ano != b.Ano));
            }
            catch
            {
                return true;
            }
        }

        
        /// <summary>
        /// Operador ++
        /// </summary>
        public static Competencia operator ++(Competencia Comp){
            Competencia Manobra = Comp.CloneCompet(1);            
            return Manobra;
        }

        /// <summary>
        /// Operador --
        /// </summary>
        public static Competencia operator --(Competencia Comp)
        {
            Competencia Manobra = Comp.CloneCompet(-1);
            return Manobra;
        }

        /// <summary>
        /// operador Menor
        /// </summary>
        public static bool operator <(Competencia a, Competencia b)
        {
            if (a.Ano < b.Ano)
                return true;
            else if (a.Ano == b.Ano)
                return a.Mes < b.Mes;
            return false;

        }

        /// <summary>
        /// operador >
        /// </summary>
        public static bool operator >(Competencia a, Competencia b)
        {
            if (a.Ano > b.Ano)
                return true;
            else if (a.Ano == b.Ano)
                return a.Mes > b.Mes;
            return false;

        }

        /// <summary>
        /// operador menor ou igual
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// </summary>
        public static bool operator <=(Competencia a, Competencia b)
        {
            if (a.Ano < b.Ano)
                return true;
            else if (a.Ano == b.Ano)
                return a.Mes <= b.Mes;
            return false;

        }

        /// <summary>
        /// operador >=
        /// </summary>
        public static bool operator >=(Competencia a, Competencia b)
        {
            if (a.Ano > b.Ano)
                return true;
            else if (a.Ano == b.Ano)
                return a.Mes >= b.Mes;
            return false;

        }

        /// <summary>
        /// operado -
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int operator -(Competencia a, Competencia b)
        {
            return (a.Ano - b.Ano) * 12 + (a.Mes - b.Mes); 
        }

        #endregion

        

        /// <summary>
        /// Indica se a compet�ncia ainda permite inclus�o de itens no boleto
        /// </summary>
        /// <returns>retorna true se a compet�ncia foi gerada e n�o emitida</returns>
        public bool Aberta()
        {
            return this > Ultima(CON);             
        }


        /// <summary>
        /// �ltima cometencia emitida
        /// </summary>
        public static Competencia Ultima(int CON) {
            //VirDB.VirtualTableAdapter Ta = new Framework.Buscas.dBuscasTableAdapters.PrevisaoBOletosTableAdapter();

            //Ta.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
             
            string Comando = "SELECT     TOP (1) PBOCompetencia\r\n" +
                             "FROM         PrevisaoBOletos\r\n" +
                             "WHERE     (PBO_CON = @P1) AND (PBOStatus in (1,2,4,5))\r\n" +
                             "ORDER BY PBOCompetencia DESC;";
            //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQL(ComandoAjuste, VirDB.VirtualTableAdapter.ComandosBusca.NonQuery);

            
            //System.Collections.ArrayList retorno = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLLinha(Comando, CON);
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Ultima Comp", true);
            int? PBOCompetencia = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(Comando, CON);
            //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                
            //    (System.Collections.ArrayList)Ta.BuscaSQL(Comando, VirDB.VirtualTableAdapter.ComandosBusca.ArrayList, new object[] { CON });
            Competencia CompRetorno;
            if (!PBOCompetencia.HasValue)
                CompRetorno = new Competencia(DateTime.Today.Month, DateTime.Today.Year).Add(-1);
            else
                CompRetorno = new Competencia(PBOCompetencia.Value);
            CompRetorno.CON = CON;
            return CompRetorno;
        }

        /// <summary>
        /// Copia os campos de outra instancia
        /// </summary>
        /// <param name="Origem">Instancia para c�pia</param>
        public void CopiaValores(Competencia Origem) {
            this.Ano = Origem.Ano;
            this.Mes = Origem.Mes;
            if(Origem.CON > 0)
               this.CON = Origem.CON;
        }



        #region Construtores
        /// <summary>
        /// Soma meses na cometencia
        /// </summary>
        /// <param name="meses">meses a serem somados (pode ser negativo)</param>
        /// <returns>compet�ncia calculada (this)</returns>
        public Competencia Add(int meses)
        {
            Mes += meses;
            AjustaCompMes();
            return this;
        }

        /// <summary>
        /// Constructor - competencia na data atual
        /// </summary>
        public Competencia():base(DateTime.Today.Month, DateTime.Today.Year)
        {
            //Mes = DateTime.Today.Month;
            //Ano = DateTime.Today.Year;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="data"></param>
        public Competencia(DateTime data) : base(data.Month, data.Year)
        {
            //Mes = data.Month;
            //Ano = data.Year;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_mes">mes</param>
        /// <param name="_ano">ano</param>
        public Competencia(int _mes, int _ano):base(_mes,_ano)
        {
            //Mes = _mes;
            //Ano = _ano;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_mes">mes</param>
        /// <param name="_ano">ano</param>
        /// <param name="_CON">Condominio CON</param>
        public Competencia(int _mes, int _ano, int _CON):base(_mes,_ano)
        {
            //Mes = _mes;
            //Ano = _ano;
            CON = _CON;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CompetenciaAAAAMM">Competicia na forma numerica do banco</param>        
        public Competencia(int CompetenciaAAAAMM)
        {
            CompetenciaBind = CompetenciaAAAAMM;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CompetenciaAAAAMM"></param>
        /// <param name="_CON">CON</param>
        /// <param name="Nulo">Usar null</param>
        public Competencia(int CompetenciaAAAAMM, int _CON, object Nulo)
        {
            CompetenciaBind = CompetenciaAAAAMM;
            CON = _CON;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <remarks>Utiliza o periodo da cometencia do cadastro do condominio</remarks>
        /// <param name="_data">Data</param>
        /// <param name="_CON">Condominio</param>
        /// <returns>Gera uma competencia a partir de uma data</returns>
        public Competencia(DateTime _data, int _CON)
        {
            //throw new Exception("N�o usar este contrutor campo DiaCotabilidade n�o populado");
            Mes = _data.Month;
            Ano = _data.Year;
            CON = _CON;
            //Int16 diadecorte;
            //Framework.Buscas.dBuscasTableAdapters.CONDOMINIOSTableAdapter TA = new Framework.Buscas.dBuscasTableAdapters.CONDOMINIOSTableAdapter();
            //TA.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
            //object retorno = TA.DiaCotabilidade(_CON);
            //if ((retorno == null)||(retorno == DBNull.Value))
            //    diadecorte = 32;
            //else
            //    diadecorte = (Int16)retorno;
            int diaC = DiaCorteCopetencia;

            if ((diaC != 1) && (_data.Day >= diaC))
                Add(1);
        } 
        #endregion

        private int DiaCorteCopetencia 
        {
            get 
            {
                if (CON <= 0)
                    return 1;
                dCompetencia.CONDOMINIOSRow row = DCompetencia.CONDOMINIOS.FindByCON(CON);
                if (row == null)
                    return 1;
                else
                    return row.CONDiaContabilidade;
                //object retorno = Framework.Buscas.dBuscas.CONDOMINIOSTableAdapter.DiaCotabilidade(CON);
                //if ((retorno == null) || (retorno == DBNull.Value))
                //    return 1;
                //else
                //    return (int)((Int16)retorno);
            }
        }

        /// <summary>
        /// Competencia no formato MM/yyyy
        /// </summary>
        /// <remarks>override do ToString padrao</remarks>
        /// <returns>MM/yyyy</returns>
        public override string ToString()
        {
            return Mes.ToString("00")+"/"+Ano.ToString();
        }

        /// <summary>
        /// Competencia no formato MMyyyy (somente numeros)
        /// </summary>
        /// <remarks>formato usado no banco access</remarks>
        /// <returns>MMyyyyy</returns>
        public string ToStringN()
        {
            return Mes.ToString("00") + Ano.ToString();
        }

        
        /// <summary>
        /// Data de vencimento do apartamentos
        /// </summary>
        /// <remarks>
        /// se nao existe um valor particular para o apt, retorna a data padrao
        /// Para APT=0 retorna data padrao
        /// </remarks>
        /// <param name="APT">APT</param>
        /// <returns>Data ajustada para APTDiaDiferente</returns>
        [Obsolete("usar do objeto ABS_Apartamento que tem uma performance superior")]
        public DateTime DataVencimenot(Int32 APT) {
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("DataVencimenot APT", true);
            if (cON == 0)
                return new DateTime(Ano, Mes, 1);
            else
            {
                int APTDiaDiferenteREF = 0;
                int APTDiaCondominio;
                string comandoBusca = "select APTDiaDiferente,APTDiaDiferenteREF,APTDiaCondominio from AParTamentos where APT = @P1";
                //CompontesBasicos.Performance.Performance.PerformanceST.Registra("DataVencimenot", true);
                System.Collections.ArrayList DadosAPT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLLinha(comandoBusca, APT);
                //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                if ((DadosAPT.Count == 3) && (DadosAPT[0] != DBNull.Value) && (DadosAPT[2] != DBNull.Value) && ((bool)DadosAPT[0] == true))
                {
                    if (DadosAPT[1] != DBNull.Value)
                        APTDiaDiferenteREF = (int)DadosAPT[1];
                    APTDiaCondominio = (Int16)DadosAPT[2];
                    Competencia CompRef = new Competencia(Mes, Ano).Add(APTDiaDiferenteREF);
                    //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                    return CompRef.DataNaCompetencia(APTDiaCondominio);
                }
                else
                {
                    //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                    return VencimentoPadrao;
                }
            }
        }

        /// <summary>
        /// Data dentro do mes da competecia
        /// </summary>
        /// <param name="dia">dia</param>
        /// <param name="reg">Regime calendario ou competencia</param>
        /// <returns>data</returns>        
        /// <remarks>se o dia for maior do que o numero de dias do m�s, retorna o ultimo dia do m�s</remarks>
        public DateTime DataNaCompetencia(int dia,Regime reg) 
        {
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("DataNaCompetencia dia reg", true);
            Competencia compRef;
            int diaC = DiaCorteCopetencia;
            if ((diaC != 1) && (reg == Regime.balancete) && (dia >= DiaCorteCopetencia))
                compRef = CloneCompet(-1);
            else
                compRef = this;

            if (DateTime.DaysInMonth(compRef.Ano, compRef.Mes) >= dia)
            {
                //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                return new DateTime(compRef.Ano, compRef.Mes, dia);                
            }
            else
            {
                //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                return new DateTime(compRef.Ano, compRef.Mes, DateTime.DaysInMonth(compRef.Ano, compRef.Mes));
            }
        }

        /// <summary>
        /// Periodo do balancete
        /// </summary>
        /// <param name="dIni"></param>
        /// <param name="dFim"></param>
        public void PeriodoBalancete(out DateTime dIni,out DateTime dFim) 
        {
            CalculaDatasBalancete();
            dIni = _DataInicioBalancete.Value;
            dFim = _DataFimBalancete.Value;
            /*
            int diaC = DiaCorteCopetencia;
            if (diaC == 1)
                dIni = new DateTime(Ano, Mes, 1);
            else
            {
                Competencia CompRef = CloneCompet(-1);
                if (diaC > DateTime.DaysInMonth(CompRef.Ano, CompRef.Mes))
                    diaC = DateTime.DaysInMonth(CompRef.Ano, CompRef.Mes);
                dIni = new DateTime(CompRef.Ano, CompRef.Mes, diaC);
            }
            dFim = dIni.AddMonths(1).AddDays(-1);
            */
        }        

        /// <summary>
        /// Primeiro dia do balancete
        /// </summary>
        public DateTime DataInicioBalancete
        {
            get
            {
                CalculaDatasBalancete();
                return _DataInicioBalancete.Value;
            }
        }

        /// <summary>
        /// �ltimo dia do balancete
        /// </summary>
        public DateTime DataFimBalancete
        {
            get
            {
                CalculaDatasBalancete();
                return _DataFimBalancete.Value;
            }
        }
        
        private DateTime? _DataInicioBalancete;
        private DateTime? _DataFimBalancete;

        private void CalculaDatasBalancete()
        {
            if (_DataInicioBalancete.HasValue)
                return;
            int diaC = DiaCorteCopetencia;
            if (diaC == 1)
                _DataInicioBalancete = new DateTime(Ano, Mes, 1);
            else
            {
                Competencia CompRef = CloneCompet(-1);
                if (diaC > DateTime.DaysInMonth(CompRef.Ano, CompRef.Mes))
                    diaC = DateTime.DaysInMonth(CompRef.Ano, CompRef.Mes);
                _DataInicioBalancete = new DateTime(CompRef.Ano, CompRef.Mes, diaC);
            }
            _DataFimBalancete = _DataInicioBalancete.Value.AddMonths(1).AddDays(-1);
        }

        

        /// <summary>
        /// Data dentro do mes da competecia
        /// </summary>
        /// <param name="dia">dia</param>
        /// <returns>data</returns>
        /// <remarks>se o dia for maior do que o numero de dias do m�s, retorna o ultimo dia do m�s</remarks>
        public DateTime DataNaCompetencia(int dia) 
        {
            return DataNaCompetencia(dia, Regime.mes);
        }

        /// <summary>
        /// 
        /// </summary>
        public enum Regime 
        {
            /// <summary>
            /// 
            /// </summary>
            mes,
            /// <summary>
            /// 
            /// </summary>
            balancete
        }

        /// <summary>
        /// Condominio
        /// </summary>
        private Int32 cON = 0;

        /// <summary>
        /// Condom�nio
        /// </summary>
        /// <remarks>N�o obrigat�rio</remarks>
        /// <value>Usado para o c�lculo de datas</value>
        public Int32 CON
        {
            get { return cON; }
            set {                
                vencimentoPadraoOriginal = DateTime.MinValue;                
                cON = value; 
            }
        }

        /// <summary>
        /// For�a o recalculo das datas de vencimento.
        /// </summary>
        public static void RecalculaVencimentos()
        {
            dCompetencia = null;
        }

        private static dCompetencia dCompetencia = null;

        private static dCompetencia DCompetencia
        {
            get 
            {
                if (dCompetencia == null)
                {
                    dCompetencia = new dCompetencia();
                    dCompetencia.CONDOMINIOSTableAdapter.Fill(dCompetencia.CONDOMINIOS);
                }
                return dCompetencia;
            }
        }
        

        /// <summary>
        /// Data de vecimento
        /// </summary>
        /// <remarks>E iniciado com a data do cadastro mas pode ser alterado</remarks>
        private DateTime vencimentoPadrao = DateTime.MinValue;

        /// <summary>
        /// Data de vencimento
        /// </summary>
        /// <remarks>E iniciado com a data do cadastro mas pode ser alterado</remarks>
        public DateTime VencimentoPadrao {
            get {
                
                if (vencimentoPadrao == DateTime.MinValue)
                    try
                    {
                        vencimentoPadrao = CalculaVencimento();
                    }
                    catch 
                    { }
                return vencimentoPadrao;
            }
            set {
                vencimentoPadrao = value;
            }
        }

        /// <summary>
        /// Vencimento calculado para esta competencia
        /// </summary>
        /// <remarks>A fun��o desta vari�vel � evitar o recalculo da fun�ao CalculaVencimento()</remarks>
        private DateTime vencimentoPadraoOriginal = DateTime.MinValue;

        /// <summary>
        /// Vencimento calculado para esta competencia
        /// </summary>
        /// <remarks>N�o pode ser alterado. Este valor � o default do vencimentoPadrao que pode ser alterado</remarks>
        private DateTime VencimentoPadraoOriginal {
            get {
                if (vencimentoPadraoOriginal == DateTime.MinValue)
                    vencimentoPadraoOriginal = CalculaVencimento();
                return vencimentoPadraoOriginal;
            }
        }

        



        private static Int32 nmes;
        private static Int32 nano;

        /// <summary>
        /// Funcao estatica que ajusta a variavel estatica mes
        /// </summary>
        /// <returns></returns>
        private static void AjustaMes()
        {
            while ((nmes < 1) || (nmes > 12))
            {
                if (nmes < 1)
                {
                    nano--;
                    nmes += 12;
                };
                if (nmes > 12)
                {
                    nano++;
                    nmes -= 12;
                }
            }
        }

        /*
        /// <summary>
        /// Indica se o vencimento ocorre no mes anterior ao da compet�ncia
        /// </summary>
        /// <remarks>Ocorre para vencimento posterio so dia 15</remarks>
        private bool DataDeVencimentoNoMesAnteriorCompetencia;// = false;
        */

        /// <summary>
        /// Calcula a data de vencimento padrao
        /// </summary>
        public DateTime CalculaVencimento(string RefVencto, bool CompV, string TipoVencto, int DiaVencto)
        {
            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("CalculaVencimento 1", true);
            
            
            Competencia CompManobra = this.CloneCompet();
            if (RefVencto != "I")
                CompManobra--;
            if (!CompV)
                CompManobra++;
            //DataDeVencimentoNoMesAnteriorCompetencia = false;
            if ((RefVencto == "I") && (TipoVencto == "C") && (DiaVencto > 15))
            {
                CompManobra--;
                //DataDeVencimentoNoMesAnteriorCompetencia = true;
            }
            
            //AjustaMes();

            Int16 ii;
            DateTime ultimo;
            if (RefVencto == "I")
            {

                if (TipoVencto == "C")                                    
                    ultimo = CompManobra.DataNaCompetencia(DiaVencto);                                    
                else
                {
                    ultimo = CompManobra.DataNaCompetencia(1);
                    for (ii = 0; ultimo.AddDays(1).Month <= ultimo.Month; ultimo = ultimo.AddDays(1))

                        if (ultimo.IsUtil())
                        {
                            ii++;
                            if (ii >= DiaVencto)
                                break;
                        };
                    //Caso tenha parado no ultimo dia do mes mas este nao seja util ele volta at� encontrar um
                    while (!ultimo.IsUtil())
                        ultimo = ultimo.AddDays(-1);

                }
                
                
            }
            else
            {
                ultimo = new DateTime(CompManobra.Ano, CompManobra.Mes, 1).AddMonths(1);

                if (TipoVencto == "C")
                    if (DiaVencto >= (ultimo.AddDays(-1)).Day)
                        ultimo = new DateTime(CompManobra.Ano, CompManobra.Mes, 1);
                    else
                        ultimo = ultimo.AddDays(-DiaVencto);
                else
                {
                    //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Parte final", true);
                    ultimo = ultimo.AddDays(-1);
                    for (ii = 0; ultimo.AddDays(-1).Month >= ultimo.Month; ultimo = ultimo.AddDays(-1))
                        if (ultimo.IsUtil())
                        {
                            ii++;
                            if (ii >= DiaVencto)
                                break;
                        };
                    while (!ultimo.IsUtil())
                        ultimo = ultimo.AddDays(1);
                    //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                };
                    
                

            };
            //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
            return ultimo;
        }

        /// <summary>
        /// Calcula a data de vencimento padrao
        /// </summary>
        /// <remarks> Utilizar o m�todo nao est�tico </remarks>
        public static DateTime CalculaVencimentoST(int _nano, int _nmes, string RefVencto, bool CompV, string TipoVencto, int DiaVencto, bool PostergaFeriado)
        {
            nano = _nano;
            nmes = _nmes;
            if (RefVencto != "I")
                nmes--;
            if (!CompV)
                nmes++;

            if ((RefVencto == "I") && (DiaVencto > 15))
            {
                nmes--;
                
            }
            AjustaMes();

            Int16 ii;
            DateTime ultimo;
            if (RefVencto == "I")
            {
                ultimo = new DateTime(nano, nmes, 1);
                if (TipoVencto == "C")
                {
                    ultimo = ultimo.AddDays(DiaVencto - 1);                    
                }
                else
                    for (ii = 1; (ii < DiaVencto) || (!ultimo.IsUtil()); ultimo = ultimo.AddDays(1))
                        if (ultimo.IsUtil())
                            ii++;
                DateTime PrimeiroDiaDoProximoMes = new DateTime(nano, nmes, 1).AddMonths(1);
                if (ultimo >= PrimeiroDiaDoProximoMes)
                    ultimo = PrimeiroDiaDoProximoMes.AddDays(-1);
            }
            else
            {
                if (nmes == 12)
                    ultimo = new DateTime(nano + 1, 1, 1);
                else
                    ultimo = new DateTime(nano, nmes + 1, 1);
                //ndia = ultimo.Day;
                if (TipoVencto == "C")
                    ultimo = ultimo.AddDays(-DiaVencto);
                else
                    for (ii = 1; (ii <= DiaVencto) || (!ultimo.IsUtil()); ultimo = ultimo.AddDays(-1))
                        if (ultimo.IsUtil())
                            ii++;
                if (ultimo < new DateTime(nano, nmes, 1))
                    ultimo = new DateTime(nano, nmes, 1);

            };
            if (PostergaFeriado)
                while (!ultimo.IsUtil())
                    ultimo = ultimo.AddDays(1);
            return ultimo;
        }


        /// <summary>
        /// Calcula a data de vencimento padrao
        /// </summary>
        /// <remarks>Evite utilizar, use a propriedade VencimentoPadraoOriginal</remarks>        
        /// <remarks>usa o ano,o mes e CON das propriedadeos o objeto</remarks>        
        /// <returns>vencimento</returns>
        public DateTime CalculaVencimento()
        {
            try
            {
                //CompontesBasicos.Performance.Performance.PerformanceST.Registra("CalculaVencimento");

                dCompetencia.CONDOMINIOSRow row = DCompetencia.CONDOMINIOS.FindByCON(CON);

                try
                {
                    
                    return CalculaVencimento(row.CONReferenciaVencimento, row.CONCompetenciaVencida, row.CONTipoVencimento, row.CONDiaVencimento);
                    
                }
                catch 
                {
                    string Erro = "Erro no cadastro do condom�nio:";
                    if (row == null)
                        Erro += " 'C�digo do condom�nio n�o encontrado (CON=" + CON.ToString() + ")'";
                    else if (row.IsCONReferenciaVencimentoNull())
                        Erro += " 'Refer�ncia de vencimento'";
                    else if (row.IsCONCompetenciaVencidaNull())
                        Erro += " 'Compet�ncia vencida ou n�o!'";
                    else if (row.IsCONTipoVencimentoNull())
                        Erro += " 'Dias corridos ou dias �teis'";
                    else if (row.IsCONDiaVencimentoNull())
                        Erro += " 'Dia de vencimento'";
                    else
                        Erro += string.Format(" 'N�o identificado. Dados: CON:{0} Ref:{1} Comp. Vencida:{2} Dias Corridos:{3} Dia:{4}'",
                                                                CON,
                                                                row.CONReferenciaVencimento,
                                                                row.CONCompetenciaVencida,
                                                                row.CONTipoVencimento,
                                                                row.CONDiaVencimento);


                    //System.Windows.Forms.MessageBox.Show(Erro + Environment.NewLine + e1.Message);
                    //VirException.VirException.VirExceptionSt.NotificarErro(new Exception(Erro, e1));
                    //VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.NotificarErro(new Exception(Erro,e1));

                    return DateTime.MinValue;
                }
            }
            catch
            {
                return DateTime.MinValue;
            }
            finally
            {
                //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
            }
        }




       


        


        /// <summary>
        /// Calcula a data de vencimento padrao
        /// </summary>
        /// <param name="_nano">ano</param>
        /// <param name="_nmes">mes</param>
        /// <param name="CON">CON</param>
        public static DateTime CalculaVencimento(int _nano, int _nmes, int CON) 
        {
            
            //Framework.Buscas.dBuscasTableAdapters.CONDOMINIOSTableAdapter TA = new Framework.Buscas.dBuscasTableAdapters.CONDOMINIOSTableAdapter();
            //TA.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
            System.Collections.ArrayList DadocCondominio = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLLinha(
                "SELECT CONReferenciaVencimento,CONCompetenciaVencida, CONTipoVencimento,   CONDiaVencimento, \r\n" +
                "CONPostergaFeriado\r\n"+
                "FROM         CONDOMINIOS\r\n"+
                "WHERE     (CON = "+CON.ToString()+")");



            try
            {                
                return CalculaVencimentoST(_nano, _nmes, (string)DadocCondominio[0], (bool)DadocCondominio[1], (string)DadocCondominio[2], (Int16)DadocCondominio[3], (bool)DadocCondominio[4]);
            }
            catch {
                string Erro = "Erro no cadastro do condom�nio:";
                if(DadocCondominio.Count == 0)
                    Erro += " 'C�digo do condom�nio n�o encontrado (CON="+CON.ToString()+")'";
                else if(DadocCondominio[0] == null)
                    Erro += " 'Refer�ncia de vencimento'";
                else if (DadocCondominio[1] == null)
                    Erro += " 'Compet�ncia vencida ou n�o!'";
                else if (DadocCondominio[2] == null)
                    Erro += " 'Dias corridos ou dias �teis'";
                else if (DadocCondominio[3] == null)
                    Erro += " 'Dia de vencimento'";
                else if (DadocCondominio[4] == null)
                    Erro += " 'Postergar no caso de feriados?'";
                //System.Windows.Forms.MessageBox.Show(Erro);
                return DateTime.MinValue;
            } 
            
        }

        

        /// <summary>
        /// O mesmos que Ultima(CON).Add(1)
        /// </summary>
        /// <returns>Proxima compet�ncia</returns>
        public static Competencia Proxima(Int32 CON) {
            return Ultima(CON).Add(1);
        }

        /// <summary>
        /// Cria um objeto competencia a partir de um string
        /// </summary>
        /// <param name="entrada">string</param>
        /// <returns>competencia</returns>
        public static Competencia Parse(string entrada) {
            try
            {
                entrada = entrada.Replace("/", "");                
                if (entrada.Length == 6)
                {
                    int mes = int.Parse(entrada.Substring(0, 2));
                    int ano = int.Parse(entrada.Substring(2, 4));
                    return new Competencia(mes, ano);
                }
                else
                    return null;
            }
            catch {
                return null;
            }

        }

        /// <summary>
        /// Cria um objeto competencia a partir de um string
        /// </summary>
        /// <param name="entrada">string</param>
        /// <param name="CON">Condominio</param>
        /// <returns>competencia</returns>
        public static Competencia Parse(string entrada,int CON)
        {
            Competencia Retorno = Parse(entrada);
            if (Retorno != null)
                Retorno.CON = CON;
            return Retorno;
        }

        /*
        public static Competencia Ultima(Int32 CON) {
            Buscas.dBuscasTableAdapters.PrevisaoBOletosTableAdapter PrevisaoBOletosTableAdapter = new Framework.Buscas.dBuscasTableAdapters.PrevisaoBOletosTableAdapter();
            PrevisaoBOletosTableAdapter.TrocarStringDeConexao();
            Buscas.dBuscas.PrevisaoBOletosDataTable PrevisaoBOletosDataTable = PrevisaoBOletosTableAdapter.GetUltimaCompetencia(CON);
            if (PrevisaoBOletosDataTable.Rows.Count == 1)
            {
                Buscas.dBuscas.PrevisaoBOletosRow PrevisaoBOletosRow = (Buscas.dBuscas.PrevisaoBOletosRow)PrevisaoBOletosDataTable.Rows[0];
                return new Competencia(PrevisaoBOletosRow.PBOCompetenciames, PrevisaoBOletosRow.PBOCompetenciaano);
            }
            else
                return new Competencia(DateTime.Today.Month, DateTime.Today.Year).Add(-1);
        } 
        */

        #region ICloneable Members

        /// <summary>
        /// Clonagem
        /// </summary>
        /// <remarks>Implementa��o da interface</remarks>
        /// <returns>Objeto clone</returns>
        public override object Clone()
        {
            return new Competencia(this.Mes,this.Ano,this.CON);
        }

        /// <summary>
        /// Clone da compet�ncia (Tipado)
        /// </summary>
        /// <remarks>O mesmo que Close s� que j� tipado</remarks>
        /// <returns>Clone da compet�ncia</returns>
        public Competencia CloneCompet()
        {
            return new Competencia(this.Mes, this.Ano, this.CON);
        }

        /// <summary>
        /// Clona e soma meses na compet�ncia clonada
        /// </summary>
        /// <param name="meses">meses a serem somados (pode ser negativo)</param>
        /// <returns>compet�ncia calculada</returns>
        public Competencia CloneCompet(int meses)
        {
            Competencia CompRetorno = this.CloneCompet();
            CompRetorno.Add(meses);
            return CompRetorno;
        }

        /// <summary>
        /// Clona e soma meses na compet�ncia clonada
        /// </summary>
        /// <param name="meses">meses a serem somados (pode ser negativo)</param>
        /// <returns>compet�ncia calculada</returns>
        public override ABS_Competencia ABS_CloneCompet(int meses) { return CloneCompet(meses); }

        #endregion

        /// <summary>
        /// Diferen�a em meses 
        /// </summary>
        /// <param name="ReferenciaBind">referencia no formato aaaaMM</param>
        /// <returns>meses</returns>
        public int DeltaMeses(int ReferenciaBind)
        {
            int refAno = ReferenciaBind / 100;
            int refmes = ReferenciaBind % 100;
            return 12 * (Ano - refAno) + (Mes - refmes);
        }

        /// <summary>
        /// Diferen�a em meses entre as competencias
        /// </summary>
        /// <param name="ReferenciaBindI">referencia no formato aaaaMM</param>
        /// <param name="ReferenciaBindF">referencia no formato aaaaMM</param>
        /// <returns></returns>
        public static int DeltaMeses(int ReferenciaBindI, int ReferenciaBindF)
        {                        
            return 12 * (ReferenciaBindF / 100 - ReferenciaBindI / 100) + (ReferenciaBindF % 100 - ReferenciaBindI % 100);
        }
    }
}
