﻿using CompontesBasicosProc;
using System;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using VirMSSQL;

namespace FrameworkProc.objetosNeon
{
    /// <summary>
    /// Conta corrente
    /// </summary>
    //public class ContaCorrenteNeon : ContaCorrente, IEMPTProc
    public class ContaCorrenteNeon : ContaCorrente, IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                {
                    if (TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                }
                return _EMPTProc1;
            }
            set => _EMPTProc1 = value;
        }

        private dContaCorrente _dContaCorrente;
        private dContaCorrente.ContaCorrenteGeralRow rowCCG;
        private dContaCorrente.ContaCorrenTeRow linhaMae;

        /// <summary>
        /// Linha mãe
        /// </summary>
        public dContaCorrente.ContaCorrenTeRow LinhaMae => linhaMae;
        
        private void DeFRNParaCCG(int FRN)
        {
            if (dContaCorrente.FORNECEDORESTableAdapter.Fill(dContaCorrente.FORNECEDORES, FRN) == 1)
            {
                try
                {
                    dContaCorrente.FORNECEDORESRow FRNrow = dContaCorrente.FORNECEDORES[0];
                    if (!FRNrow.IsFRNAgenciaCreditoNull() && !FRNrow.IsFRNContaCreditoNull() && !FRNrow.IsFRNContaDgCreditoNull() && !FRNrow.IsFRNBancoCreditoNull())
                    {                        
                        CarregaDados(FRNrow.FRNContaTipoCredito == 1 ? TipoConta.CC : TipoConta.Poupanca,
                                     int.Parse(FRNrow.FRNBancoCredito),
                                     int.Parse(FRNrow.FRNAgenciaCredito),
                                     FRNrow.IsFRNAgenciaDgCreditoNull() ? null : (int?)int.Parse(FRNrow.FRNAgenciaDgCredito),
                                     int.Parse(FRNrow.FRNContaCredito),
                                     FRNrow.FRNContaDgCredito,
                                     null,
                                     "",
                                     FRNrow.FRNNome,
                                     new DocBacarios.CPFCNPJ(FRNrow.FRNCnpj));
                    }
                    if (validaAgencia() && validaConta())
                    {
                        FRNrow.FRN_CCG = Salvar(TipoChave.CCG);
                        dContaCorrente.FORNECEDORESTableAdapter.Update(FRNrow);
                    }
                }
                catch 
                {
                    
                }
            }                        
        }

        private void DeCCTParaCCG()
        {
            if ((rowCCG != null) || (linhaMae == null))
                return;            
            TabelaCCG = dContaCorrente.ContaCorrenteGeralTableAdapter.GetDataByConta(BCO, Agencia, NumeroConta, (int)Tipo);
            if (TabelaCCG.Count == 0)
                Salvar(TipoChave.CCG);
            else
                rowCCG = TabelaCCG[0];
            if (linhaMae.IsCCT_CCGNull() || (linhaMae.CCT_CCG != rowCCG.CCG))
            {
                linhaMae.CCT_CCG = rowCCG.CCG;
                dContaCorrente.ContaCorrenTeTableAdapter.Update(linhaMae);
            }
        }

        private dContaCorrente.ContaCorrenTeDataTable TabelaCCT;
        private dContaCorrente.ContaCorrenteGeralDataTable TabelaCCG;

        #region Construtores

        /// <summary>
        /// Construtor
        /// </summary>
        public ContaCorrenteNeon(EMPTProc _EMPTProc = null) : base()
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
        }

        /*
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="TipoCh"></param>
        public ContaCorrenteNeon(int Chave, TipoChave TipoCh) : this(Chave, TipoCh, false)
        {
        }*/

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="TipoCh"></param>
        /// <param name="UsarCCG">Cadastra na tabela CCG se nao estiver</param>
        public ContaCorrenteNeon(int Chave, TipoChave TipoCh, bool UsarCCG = false, EMPTProc _EMPTProc = null) : this(_EMPTProc)
        {
            switch (TipoCh)
            {
                case TipoChave.CON:
                    TabelaCCT = dContaCorrente.ContaCorrenTeTableAdapter.GetDataByCON(Chave);
                    break;
                case TipoChave.CCT:
                    TabelaCCT = dContaCorrente.ContaCorrenTeTableAdapter.GetDataByCCT(Chave);
                    break;
                case TipoChave.CCG:
                    TabelaCCG = dContaCorrente.ContaCorrenteGeralTableAdapter.GetDataByCCG(Chave);
                    break;
                case TipoChave.FRN:
                    TabelaCCG = dContaCorrente.ContaCorrenteGeralTableAdapter.GetDataByFRN(Chave);
                    if (TabelaCCG.Count == 0)
                    {
                        DeFRNParaCCG(Chave);
                        TabelaCCG = dContaCorrente.ContaCorrenteGeralTableAdapter.GetDataByFRN(Chave);
                    }
                    break;
            }

            if ((TabelaCCT != null) && (TabelaCCT.Count == 1))
            {
                linhaMae = TabelaCCT[0];
                CarregaCCTrow();
            }
            else if ((TabelaCCG != null) && (TabelaCCG.Count == 1))
            {
                rowCCG = TabelaCCG[0];
                CarregaDados((TipoConta)rowCCG.CCGTipo,
                             rowCCG.CCG_BCO,
                             rowCCG.CCGAgencia,
                             rowCCG.IsCCGAgenciaDgNull() ? (int?)null : rowCCG.CCGAgenciaDg,
                             rowCCG.CCGConta,
                             rowCCG.CCGContaDg,
                             null,
                             string.Empty,
                             rowCCG.IsCCGTitularNull() ? string.Empty : rowCCG.CCGTitular,
                             rowCCG.IsCCGCPFNull() ? null : new DocBacarios.CPFCNPJ(rowCCG.CCGCPF));
                Encontrado = true;
            }
            else
                if ((TipoCh == TipoChave.CCT) && (!ChecaDadosAntigos(Chave)))
                throw new Exception(string.Format("Erro no cadastro da conta corrente\r\nChave: {0} Tipo: {1}", Chave, Tipo));
            if (UsarCCG && (rowCCG == null))
                DeCCTParaCCG();
        }

        public ContaCorrenteNeon(int _CCT, EMPTProc _EMPTProc = null) : this(_EMPTProc)
        {
            if (dContaCorrente.ContaCorrenTeTableAdapter.FillByCCT(dContaCorrente.ContaCorrenTe, _CCT) == 1)
            {
                linhaMae = dContaCorrente.ContaCorrenTe[0];
                CarregaCCTrow();
                Encontrado = true;
            }
        }

        /// <summary>
        /// Construtor para carga em lote
        /// </summary>
        /// <param name="_rowCCT"></param>
        public ContaCorrenteNeon(dContaCorrente.ContaCorrenTeRow _rowCCT) : this(((dContaCorrente)_rowCCT.Table.DataSet).EMPTProc1)
        {
            linhaMae = _rowCCT;
            CarregaCCTrow();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_BCO">Banco</param>
        /// <param name="_Agencia">Agência</param>
        /// <param name="_Conta">Conta</param>
        /// <param name="_Tipo">Tipo de conta</param>
        public ContaCorrenteNeon(int _BCO,int _Agencia,int _Conta,TipoConta _Tipo, EMPTProc _EMPTProc = null) : this(_EMPTProc)
        {
            int nContas = dContaCorrente.ContaCorrenTeTableAdapter.FillByAgConta(dContaCorrente.ContaCorrenTe,_BCO,_Agencia,_Conta,(int)_Tipo);
            if (nContas <= 0)
                Encontrado = false;
            else
            {
                Encontrado = true;
                if (nContas == 1)
                    linhaMae = dContaCorrente.ContaCorrenTe[0];                                    
                else 
                {
                    foreach (dContaCorrente.ContaCorrenTeRow candidato in dContaCorrente.ContaCorrenTe)
                        if (candidato.CCTAtiva)
                        {
                            linhaMae = candidato;
                            break;
                        }
                    if(linhaMae == null)
                        linhaMae = dContaCorrente.ContaCorrenTe[0];
                }
                CarregaCCTrow();
            }

        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_BCO">Banco</param>
        /// <param name="_CNR">CNR</param>
        public ContaCorrenteNeon(int _BCO, int _CNR, EMPTProc _EMPTProc = null) : this(_EMPTProc)
        {
            int nContas = dContaCorrente.ContaCorrenTeTableAdapter.FillByCNR(dContaCorrente.ContaCorrenTe, _BCO, _CNR);
            if (nContas <= 0)
                Encontrado = false;
            else
            {
                Encontrado = true;
                if (nContas == 1)
                    linhaMae = dContaCorrente.ContaCorrenTe[0];
                else
                {
                    foreach (dContaCorrente.ContaCorrenTeRow candidato in dContaCorrente.ContaCorrenTe)
                        if (candidato.CCTAtiva)
                        {
                            linhaMae = candidato;
                            break;
                        }
                    if (linhaMae == null)
                        linhaMae = dContaCorrente.ContaCorrenTe[0];
                }
                CarregaCCTrow();
            }

        }

        #endregion

        private void CarregaCCTrow()
        {
            int? CNR = null;
            if (!linhaMae.IsCCTCNRNull())
                CNR = linhaMae.CCTCNR;
            TipoConta tipoConta = TipoConta.CC;
            
            switch ((CCTTipo)linhaMae.CCTTipo)
            {
                case CCTTipo.ContaCorrete:                
                case CCTTipo.ContaCorrete_com_Oculta:
                case CCTTipo.ContaDaADM:
                    tipoConta = TipoConta.CC;
                    break;
                case CCTTipo.Poupanca:
                    tipoConta = TipoConta.Poupanca;
                    break;
                case CCTTipo.ContaCorreteOculta:
                case CCTTipo.Aplicacao:                
                default:
                    throw new Exception("Tipo de conta inválida para crédito");
            }
            CarregaDados(tipoConta,
                         linhaMae.CCT_BCO,
                         linhaMae.CCTAgencia,
                         linhaMae.CCTAgenciaDg,
                         linhaMae.CCTConta,
                         linhaMae.CCTContaDg,
                         CNR,
                         linhaMae.IsCCTCaixaPostalNull() ? string.Empty : linhaMae.CCTCaixaPostal,
                         linhaMae.IsCONNomeNull() ? string.Empty : linhaMae.CONNome,
                         linhaMae.IsCONCnpjNull() ? null : new DocBacarios.CPFCNPJ(linhaMae.CONCnpj));
            DebitoAutomatico = linhaMae.CCTDebitoAutomatico;
            RateioCredito = linhaMae.CCTRateioCredito;
            PagamentoEletronico = linhaMae.CCTPagamentoEletronico;
            Encontrado = true;            
        }

        private bool ChecaDadosAntigos(int CON)
        {
            dContaCorrente.CONDOMINIOSRow rowCON = dContaCorrente.CONDOMINIOSTableAdapter.GetData(CON)[0];
            int CONAgencia = int.Parse(rowCON.CONAgencia);
            int CONDigitoAgencia = int.Parse(rowCON.CONDigitoAgencia);
            int CONConta = int.Parse(rowCON.CONConta);
            int CONCodigoCNR = 0;
            if (!rowCON.IsCONCodigoCNRNull())
                int.TryParse(rowCON.CONCodigoCNR, out CONCodigoCNR);
            if ((CONAgencia != Agencia) || (CONDigitoAgencia != AgenciaDg) || (CONConta != NumeroConta) || (rowCON.CONDigitoConta != NumeroDg) || (rowCON.CON_BCO != BCO) || (CONCodigoCNR != CNR.GetValueOrDefault(0)))
            {
                CarregaDados(TipoConta.CC,
                             rowCON.CON_BCO,
                             CONAgencia,
                             CONDigitoAgencia,
                             CONConta,
                             rowCON.CONDigitoConta,
                             CONCodigoCNR,
                             string.Empty);
                return true;
            }
            return false;
        }

        private dContaCorrente dContaCorrente => _dContaCorrente ?? (_dContaCorrente = new dContaCorrente(EMPTProc1)); 
        

        /// <summary>
        /// Salva no banco de dados
        /// </summary>        
        public int Salvar(TipoChave TipoDeChave)
        {
            if (!validaConta())
                throw new Exception("Conta Corrente inválida");
            switch (TipoDeChave)
            {
                case TipoChave.CON:
                    throw new NotImplementedException("TipoChave = CON");
                case TipoChave.CCT:
                    throw new NotImplementedException("TipoChave = CCT");
                case TipoChave.CCG:
                    if (rowCCG == null)
                    {
                        int Cadastrados = dContaCorrente.ContaCorrenteGeralTableAdapter.FillByConta(dContaCorrente.ContaCorrenteGeral, BCO, Agencia, NumeroConta, (int)Tipo);
                        if (Cadastrados == 1)
                        {
                            rowCCG = dContaCorrente.ContaCorrenteGeral[0];
                            if ((!rowCCG.IsCCGCPFNull())
                                  &&
                                ((CPF_CNPJ != null) && (rowCCG.CCGCPF != CPF_CNPJ.ToString())
                                   ||
                                  (CPF_CNPJ == null)
                                )

                               )
                                throw new Exception(string.Format("Conta já está cadastrada com outro CPF/CNPJ: {0}", rowCCG.CCGCPF));
                        }
                        else
                            rowCCG = dContaCorrente.ContaCorrenteGeral.NewContaCorrenteGeralRow();
                    }
                    rowCCG.CCGAgencia = Agencia;
                    if (AgenciaDg.HasValue)
                        rowCCG.CCGAgenciaDg = AgenciaDg.Value;
                    rowCCG.CCGConta = NumeroConta;
                    rowCCG.CCGContaDg = NumeroDg;
                    rowCCG.CCG_BCO = BCO;
                    rowCCG.CCGTipo = (int)Tipo;
                    if (Titular != string.Empty)
                        rowCCG.CCGTitular = Titular;
                    else
                        rowCCG.SetCCGTitularNull();
                    if (CPF_CNPJ != null)
                        rowCCG.CCGCPF = CPF_CNPJ.ToString();
                    else
                        rowCCG.SetCCGCPFNull();
                    if (rowCCG.RowState == System.Data.DataRowState.Detached)
                        dContaCorrente.ContaCorrenteGeral.AddContaCorrenteGeralRow(rowCCG);
                    dContaCorrente.ContaCorrenteGeralTableAdapter.EmbarcaEmTransST();
                    dContaCorrente.ContaCorrenteGeralTableAdapter.Update(rowCCG);
                    return rowCCG.CCG;
                default:
                    throw new NotImplementedException(string.Format("ContaCorrenteNeon.cs 130 Tipo de chave não tratado: {0}", Tipo));
            }
        }

        /// <summary>
        /// Chave
        /// </summary>
        public int? CCG => rowCCG == null ? (int?)null : rowCCG.CCG;         

        /// <summary>
        /// CCT
        /// </summary>
        public int? CCT => linhaMae == null ? (int?)null : linhaMae.CCT; 
        
        

        /// <summary>
        /// Tipo da chave
        /// </summary>
        public enum TipoChave
        {
            /// <summary>
            /// Condominio
            /// </summary>
            CON,
            /// <summary>
            /// Conta Corrente
            /// </summary>
            CCT,
            /// <summary>
            /// Conta Corrente Geral
            /// </summary>
            CCG,
            /// <summary>
            /// Fornecedor
            /// </summary>
            FRN            
        }
    }
}
