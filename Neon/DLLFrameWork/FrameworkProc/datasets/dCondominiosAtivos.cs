﻿/*
LH - 23/4/2014        13.2.8.29 - FIX faltava campo copiaCON
MR - 03/12/2014 10:00           - Inclusão do campo com cod. de comunicacao para tributos, usando no envio de arquivo para Bradesco (CONCodigoComunicacaoTributos) no CONDOMINIOSTableAdapter
MR - 26/01/2014 16:00           - Inclusão do campo de segundo gerente nos Fills (CONAuditor2_USU) no CONDOMINIOSTableAdapter
MR - 15/03/2016 10:00           - Inclusão do campo de tamanho de fonte para o quadro de inadimplentes nos Fills (CONInaTamanhoFonte) no CONDOMINIOSTableAdapter
*/

namespace FrameworkProc.datasets
{

    using FrameworkProc;
    using Framework;
    using VirEnumeracoesNeon;

    partial class dCondominiosAtivos : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        static private dCondominiosAtivos _dCondominiosAtivosST;
        static private dCondominiosAtivos _dCondominiosAtivosSTF;

        /// <summary>
        /// DataSet com os condomínios ativos
        /// </summary>
        static public dCondominiosAtivos dCondominiosAtivosST
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    DSCentral.RegistrarErro("dCondominiosAtivos.cs 24");
                if (_dCondominiosAtivosST == null)
                {
                    _dCondominiosAtivosST = new dCondominiosAtivos();
                    _dCondominiosAtivosST.SoAtivos = true;
                }
                if (_dCondominiosAtivosST.CONDOMINIOS.Count == 0)
                    _dCondominiosAtivosST.CarregaCondominios();
                return _dCondominiosAtivosST;
            }
            set
            {
                _dCondominiosAtivosST = value;
            }
        }

        /// <summary>
        /// DataSet com os condomínios ativos
        /// </summary>
        static public dCondominiosAtivos dCondominiosAtivosSTF
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    DSCentral.RegistrarErro("dCondominiosAtivos.cs 44");
                if (_dCondominiosAtivosSTF == null)
                {
                    _dCondominiosAtivosSTF = new dCondominiosAtivos(new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Filial));
                    _dCondominiosAtivosSTF.SoAtivos = true;
                }
                if (_dCondominiosAtivosSTF.CONDOMINIOS.Count == 0)
                    _dCondominiosAtivosSTF.CarregaCondominios();
                return _dCondominiosAtivosSTF;
            }
        }

        /// <summary>
        /// DataSet com os condomínios ativos
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        static public dCondominiosAtivos dCondominiosAtivosSTX(EMPTipo EMPTipo)
        {
            if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                DSCentral.RegistrarErro("dCondominiosAtivos.cs 64");
            return EMPTipo == EMPTipo.Local ? dCondominiosAtivosST : dCondominiosAtivosSTF;
        }

        static private dCondominiosAtivos _dCondominiosTodosST;
        static private dCondominiosAtivos _dCondominiosTodosSTF;

        /// <summary>
        /// DataSet com todos os condomínios
        /// </summary>
        static public dCondominiosAtivos dCondominiosTodosST
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    DSCentral.RegistrarErro("dCondominiosAtivos.cs 79");
                if (_dCondominiosTodosST == null)
                {
                    _dCondominiosTodosST = new dCondominiosAtivos();
                    _dCondominiosTodosST.SoAtivos = false;
                }
                if (_dCondominiosTodosST.CONDOMINIOS.Count == 0)
                    _dCondominiosTodosST.CarregaCondominios();
                return _dCondominiosTodosST;
            }
            set
            {
                _dCondominiosTodosST = value;
            }
        }

        /// <summary>
        /// DataSet com todos os condomínios da filial
        /// </summary>
        static public dCondominiosAtivos dCondominiosTodosSTF
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    DSCentral.RegistrarErro("dCondominiosAtivos.cs 99");
                if (_dCondominiosTodosSTF == null)
                {
                    _dCondominiosTodosSTF = new dCondominiosAtivos(new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Filial));
                    _dCondominiosTodosSTF.SoAtivos = false;
                }
                if (_dCondominiosTodosSTF.CONDOMINIOS.Count == 0)
                    _dCondominiosTodosSTF.CarregaCondominios();
                return _dCondominiosTodosSTF;
            }
        }

        /// <summary>
        /// DataSet com todos os condomínios
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        static public dCondominiosAtivos dCondominiosTodosSTX(EMPTipo EMPTipo)
        {
            if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                DSCentral.RegistrarErro("dCondominiosAtivos.cs 119");
            return EMPTipo == EMPTipo.Local ? dCondominiosTodosST : dCondominiosTodosSTF;
        }



        /// <summary>
        /// Condominios ativos
        /// </summary>
        /// <param name="EMPTProc"></param>
        public dCondominiosAtivos(EMPTProc EMPTProc)
            : this()
        {
            if (EMPTProc == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                {
                    throw new System.Exception("Erro em Servidor de processos");
                }
                EMPTProc = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = EMPTProc;
        }

        /// <summary>
        /// Altera o Status do condominio
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="novoStatus"></param>
        /// <param name="Motivo"></param>
        /// <returns></returns>
        public bool AlteraStatus(int CON, CONStatus novoStatus, string Motivo)
        {
            StatusCONdominiosTableAdapter.EmbarcaEmTransST();
            StatusCONdominiosTableAdapter.Fill(StatusCONdominios, CON);
            StatusCONdominiosRow row = StatusCONdominios[0];
            if (row.CONStatus == (int)novoStatus)
                return false;
            row.CONStatus = (int)novoStatus;
            row.CONHistorico = string.Format("{0}{1}",
                                              row.IsCONHistoricoNull() ? "" : row.CONHistorico + "\r\n\r\n",
                                              Motivo);
            StatusCONdominiosTableAdapter.Update(row);
            CONDOMINIOSRow rowCON = CONDOMINIOS.FindByCON(CON);
            if (rowCON != null)
                rowCON.CONStatus = (int)novoStatus;
            return true;
        }

        private dCondominiosAtivosTableAdapters.StatusCONdominiosTableAdapter statusCONdominiosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: STATUSCONDOMINIOS
        /// </summary>
        public dCondominiosAtivosTableAdapters.StatusCONdominiosTableAdapter StatusCONdominiosTableAdapter
        {
            get
            {
                if (statusCONdominiosTableAdapter == null)
                {
                    statusCONdominiosTableAdapter = new dCondominiosAtivosTableAdapters.StatusCONdominiosTableAdapter();
                    statusCONdominiosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return statusCONdominiosTableAdapter;
            }
        }

        private bool SoAtivos;



        /// <summary>
        /// Encontra o condomínio pelo CODCON
        /// </summary>
        /// <param name="codcon"></param>
        /// <returns></returns>
        public CONDOMINIOSRow BuscaPorCodCon(string codcon)
        {
            int indice;
            if (DVCODCON == null)
            {
                DVCODCON = new System.Data.DataView(CONDOMINIOS);
                DVCODCON.Sort = "CONCODIGO";
            }
            indice = DVCODCON.Find(codcon);
            if (indice >= 0)
                return (CONDOMINIOSRow)DVCODCON[indice].Row;
            //foreach (CONDOMINIOSRow rowCandidato in CONDOMINIOS)
            //    if(rowCandidato.CONCodigo == codcon)
            //        return rowCandidato;
            return null;
        }

        private System.Data.DataView DVCODCON;
        private System.Data.DataView DVFolha1;
        private System.Data.DataView DVFolha2;

        /// <summary>
        /// Encontra o condomínio pelo código da folha
        /// </summary>
        /// <param name="Cod"></param>
        /// <returns></returns>
        public CONDOMINIOSRow BuscaPorCodFolha(int Cod)
        {
            int indice;
            if (DVFolha1 == null)
            {
                DVFolha1 = new System.Data.DataView(CONDOMINIOS);
                DVFolha1.Sort = "CONCodigoFolha1";
            }
            indice = DVFolha1.Find(Cod);
            if (indice >= 0)
                return (CONDOMINIOSRow)DVFolha1[indice].Row;
            return null;
        }

        /// <summary>
        /// Encontra o condomínio pelo código da folha alternativo
        /// </summary>
        /// <param name="Cod"></param>
        /// <returns></returns>
        public CONDOMINIOSRow BuscaPorCodFolha(string Cod)
        {
            int indice;
            if (DVFolha2 == null)
            {
                DVFolha2 = new System.Data.DataView(CONDOMINIOS);
                DVFolha2.Sort = "CONCodigoFolha2A";
            }
            indice = DVFolha2.Find(Cod);
            if (indice >= 0)
                return (CONDOMINIOSRow)DVFolha2[indice].Row;
            return null;
        }

        private dCondominiosAtivosTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;

        /// <summary>
        /// TableAdapter
        /// </summary>
        public dCondominiosAtivosTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new FrameworkProc.datasets.dCondominiosAtivosTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dCondominiosAtivosTableAdapters.SindicosTableAdapter sindicosTableAdapter;

        /// <summary>
        /// Sindicos TableAdapter
        /// </summary>
        public dCondominiosAtivosTableAdapters.SindicosTableAdapter SindicosTableAdapter
        {
            get
            {
                if (sindicosTableAdapter == null)
                {
                    sindicosTableAdapter = new FrameworkProc.datasets.dCondominiosAtivosTableAdapters.SindicosTableAdapter();
                    sindicosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return sindicosTableAdapter;
            }
        }

        /// <summary>
        /// Carrega os condominios
        /// </summary>

        public void CarregaCondominios()
        {
            if (!SoAtivos)
                CONDOMINIOSTableAdapter.FillByTodos(CONDOMINIOS);
            else
                CONDOMINIOSTableAdapter.FillByAtivos(CONDOMINIOS, EMPTProc1.EMP);
        }

        /// <summary>
        /// Carrega os síndicos
        /// </summary>
        /// <param name="refresh">Recarga</param>
        public void CarregaSindicos(bool refresh)
        {
            if ((!refresh) && (Sindicos.Rows.Count > 0))
                return;
            SindicosTableAdapter.Fill(Sindicos);
        }
    }
}
