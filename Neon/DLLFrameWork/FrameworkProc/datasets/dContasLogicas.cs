﻿namespace FrameworkProc.datasets
{


    public partial class dContasLogicas
    {
        private static dContasLogicas _dContasLogicasSt;

        /// <summary>
        /// dataset estático:dContasLogicas
        /// </summary>
        public static dContasLogicas dContasLogicasSt
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    Framework.DSCentral.RegistrarErro("dContasLogicas.cs 16");
                if (_dContasLogicasSt == null)
                {
                    _dContasLogicasSt = new dContasLogicas();
                    _dContasLogicasSt.ContaCorrenTeTableAdapter.Fill(_dContasLogicasSt.ContaCorrenTe);
                }
                return _dContasLogicasSt;
            }
        }

        /// <summary>
        /// Limpa o pool das contas que sejam atualizadas na RAM
        /// </summary>
        public static void LimpaSt()
        {
            _dContasLogicasSt = null;
        }

        private dContasLogicasTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dContasLogicasTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dContasLogicasTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao();
                };
                return conTasLogicasTableAdapter;
            }
        }

        /// <summary>
        /// Cria e poula um dContasLogica
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="Ativas"></param>
        /// <returns></returns>
        public static dContasLogicas GetdContasLogicas(int CON, bool Ativas = true)
        {
            dContasLogicas retorno = new dContasLogicas();
            if (Ativas)
                retorno.ConTasLogicasTableAdapter.FillByCONAtivos(retorno.ConTasLogicas, CON);
            else
                retorno.ConTasLogicasTableAdapter.FillByCON(retorno.ConTasLogicas, CON);
            return retorno;
        }

        private dContasLogicasTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dContasLogicasTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dContasLogicasTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }
    }
}
