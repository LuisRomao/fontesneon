﻿namespace FrameworkProc.datasets {


    /// <summary>
    /// dataset INPC
    /// </summary>    
    partial class dINPC
    {
        private static dINPC _dINPCSt;

        /// <summary>
        /// dataset estático:dINPC
        /// </summary>
        public static dINPC dINPCSt
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new System.Exception("uso de dINPCSt em servidor de processos");
                if (_dINPCSt == null)
                {
                    _dINPCSt = new dINPC();
                    _dINPCSt.INPCTableAdapter.Fill(_dINPCSt.INPC);
                }
                return _dINPCSt;
            }
        }

        private dINPCTableAdapters.INPCTableAdapter iNPCTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: INPC
        /// </summary>
        public dINPCTableAdapters.INPCTableAdapter INPCTableAdapter
        {
            get
            {
                if (iNPCTableAdapter == null)
                {
                    iNPCTableAdapter = new dINPCTableAdapters.INPCTableAdapter();
                    iNPCTableAdapter.TrocarStringDeConexao();
                };
                return iNPCTableAdapter;
            }
        }
        
        private dINPC.INPCRow maxrowINP;
        private dINPC.INPCRow penultimarowINP;
        private dINPC.INPCRow rowINP;

        private Framework.objetosNeon.Competencia _UltimaComp;

        private Framework.objetosNeon.Competencia UltimaComp
        {
            get
            {
                if (_UltimaComp == null)
                {
                    maxrowINP = INPC[0];
                    _UltimaComp = new Framework.objetosNeon.Competencia(maxrowINP.INPmes, maxrowINP.INPano);
                    foreach (INPCRow rowINP in _dINPCSt.INPC)
                    {
                        Framework.objetosNeon.Competencia Candidato = new Framework.objetosNeon.Competencia(rowINP.INPmes, rowINP.INPano);
                        if (Candidato > _UltimaComp)
                        {
                            _UltimaComp = Candidato;
                            maxrowINP = rowINP;
                        }
                    }
                };
                return _UltimaComp;
            }
        }    

        /// <summary>
        /// Busca o INPC
        /// </summary>
        /// <param name="inpc">retorna o inpc</param>
        /// <param name="indice">retorna o indice</param>
        /// <param name="Comp">competencia (mes/ano)</param>
        public void buscaIndices(ref float inpc, ref float indice, Framework.objetosNeon.Competencia Comp)
        {
            rowINP = INPC.FindByINPanoINPmes(Comp.Ano, Comp.Mes);
            if (rowINP != null)
            {
                inpc = rowINP.INPInpc;
                indice = rowINP.INPIndice;
            }
            else
            {
                Framework.objetosNeon.Competencia CompAnt = UltimaComp.CloneCompet(-1);
                penultimarowINP = INPC.FindByINPanoINPmes(CompAnt.Ano, CompAnt.Mes);
                double meses = 12 * (Comp.Ano - maxrowINP.INPano) + (Comp.Mes - maxrowINP.INPmes);
                indice = (float)(maxrowINP.INPIndice * System.Math.Pow((double)penultimarowINP.INPInpc, meses));
                inpc = penultimarowINP.INPInpc;

            }
        }

    }
}
