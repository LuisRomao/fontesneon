﻿using
System;

namespace Framework.datasets
{
    
    
    public partial class dFERiados {
        private dFERiadosTableAdapters.FERiadosTableAdapter fERiadosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FERiados
        /// </summary>
        public dFERiadosTableAdapters.FERiadosTableAdapter FERiadosTableAdapter
        {
            get
            {
                if (fERiadosTableAdapter == null)
                {
                    fERiadosTableAdapter = new dFERiadosTableAdapters.FERiadosTableAdapter();
                    fERiadosTableAdapter.TrocarStringDeConexao();
                };
                return fERiadosTableAdapter;
            }
        }

        private static dFERiados _dFERiadosSt;

        /// <summary>
        /// dataset estático:dFERiados
        /// </summary>
        public static dFERiados dFERiadosSt
        {
            get
            {
                if (_dFERiadosSt == null)
                {
                    _dFERiadosSt = new dFERiados();
                    _dFERiadosSt.FERiadosTableAdapter.Fill(_dFERiadosSt.FERiados);
                    /*
                    if (_dFERiadosSt.FERiados.Count == 0)
                    {
                        System.Data.DataTable TFeriados = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLTabela("select data from feriados where data > @P1", new System.DateTime(2011, 1, 1));
                        foreach (System.Data.DataRow Linha in TFeriados.Rows)
                            _dFERiadosSt.FERiados.AddFERiadosRow((System.DateTime)Linha[0]);
                        _dFERiadosSt.FERiadosTableAdapter.Update(_dFERiadosSt.FERiados);
                        _dFERiadosSt.FERiados.AcceptChanges();
                    }*/
                }
                return _dFERiadosSt;
            }
        }

        

        /// <summary>
        /// Verifica se o dia é útil
        /// </summary>
        /// <param name="data"></param>
        /// <param name="sabados"></param>
        /// <param name="feriados"></param>
        /// <returns></returns>
        [Obsolete("Usar extensão da data")]
        public static bool IsUtil(DateTime data, Sabados sabados = Sabados.naoUteis, Feriados feriados = Feriados.naoUteis)
        {
            if (data.DayOfWeek == DayOfWeek.Sunday)
                return false;
            if ((sabados == Sabados.naoUteis) && (data.DayOfWeek == DayOfWeek.Saturday))
                return false;
            if ((feriados == Feriados.naoUteis) && IsFeriado(data))
                return false;            
            return true;
        }

        /// <summary>
        /// Verifica se o dia é feriado
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Obsolete("Usar extensão da data")]
        public static bool IsFeriado(DateTime data)
        {
            return (dFERiadosSt.FERiados.FindByFERData(data) != null);
        }

        /// <summary>
        /// Sentido de calculo
        /// </summary>
        public enum Sentido
        {
            /// <summary>
            /// 
            /// </summary>
            avanca,
            /// <summary>
            /// 
            /// </summary>
            retorna
        }

        /// <summary>
        /// Contar ou não o sabado
        /// </summary>
        [Obsolete("Usar extensão da data")]
        public enum Sabados 
        {
            /// <summary>
            /// 
            /// </summary>
            Uteis,
            /// <summary>
            /// 
            /// </summary>
            naoUteis 
        }

        /// <summary>
        /// Levar em conta os feriados ou nao
        /// </summary>
        [Obsolete("Usar extensão da data")]
        public enum Feriados 
        {
            /// <summary>
            /// útil 
            /// </summary>
            Uteis, 
            /// <summary>
            /// não útil
            /// </summary>
            naoUteis 
        }

        /// <summary>
        /// Calcula o próximo dia útil
        /// </summary>
        /// <param name="Data">Data base</param>
        /// <param name="Dias">Dias a pular</param>
        /// <param name="sentido"></param>
        /// <param name="sabados"></param>
        /// <param name="feriados"></param>
        /// <returns></returns>
        [Obsolete("Usar extensão da data SomaDiasUteis")]
        public static DateTime DiasUteis(DateTime Data, int Dias, Sentido sentido,Sabados sabados = Sabados.naoUteis,Feriados feriados = Feriados.naoUteis)
        {
            int passo = sentido == Sentido.avanca ? 1 : -1;
            while ((Dias > 0) || (!IsUtil(Data, sabados, feriados)))
            {
                Data = Data.AddDays(passo);
                if(IsUtil(Data,sabados,feriados))
                    Dias--;
            }
            return Data;
        }
    }
}
