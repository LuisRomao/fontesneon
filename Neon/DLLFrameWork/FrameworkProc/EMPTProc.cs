﻿using System;
using VirMSSQL;
using VirDB.Bancovirtual;
using CompontesBasicosProc;

namespace FrameworkProc
{
    /// <summary>
    /// Objeto para acesso a 2 bancos Local e Filial e para servidor de processos. Carrega as informações que viriam dos objetos estáticos
    /// </summary>
    /// <remarks>As operações de transação devem ser feitas via este objeto no caso de acesso a filial e processo</remarks>
    public class EMPTProc
    {
        /// <summary>
        /// EMPLocal
        /// </summary>
        /// <remarks>No servidor de processo é 1 por convenção</remarks>
        public static int EMPLocal
        {
            get
            {
                if (TableAdapter.Servidor_De_Processos)
                    return 1;
                else
                    return Framework.DSCentral.EMP;
            }

        }

        private static EMPTProc eMPTProcStLocal;

        private static EMPTProc eMPTProcStFilial;

        /// <summary>
        /// EMPTProc estático local
        /// </summary>
        public static EMPTProc EMPTProcStLocal
        {
            get
            {
                if (eMPTProcStLocal == null)
                    eMPTProcStLocal = new EMPTProc(EMPTipo.Local, Framework.DSCentral.USU);
                return eMPTProcStLocal;
            }
        }

        /// <summary>
        /// EMPTProcStFilial estático filial
        /// </summary>
        public static EMPTProc EMPTProcStFilial
        {
            get
            {
                if (eMPTProcStFilial == null)
                    eMPTProcStFilial = new EMPTProc(EMPTipo.Filial, Framework.DSCentral.USUFilial);
                return eMPTProcStFilial;
            }
        }

        /// <summary>
        /// EMPTProc estático para um EMP
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public static EMPTProc EMPTProcStX(int EMP)
        {
            return EMPTProcStX(Framework.DSCentral.EMPTipo(EMP));
        }

        /// <summary>
        /// EMPTProc estático para um Tipo
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public static EMPTProc EMPTProcStX(EMPTipo Tipo)
        {
            return Tipo == EMPTipo.Local ? EMPTProcStLocal : EMPTProcStFilial;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Tipo"></param>
        /// <param name="_USU">Setagem obrigatória em caso de processso</param>
        public EMPTProc(EMPTipo _Tipo,int? _USU = null)
        {
            this._Tipo = _Tipo;
            if (_USU.HasValue)
                USU = _USU.Value;
            else
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("USU não especificado em Servidor de Processos");
            }
        }

        private EMPTipo _Tipo;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTipo Tipo { get { return _Tipo; } }

        private int? uSU;
        private string uSUNome;

        /// <summary>
        /// Usuario logado
        /// </summary>
        public int USU 
        {
            get 
            {
                if (uSU.HasValue)
                    return uSU.Value;
                if (!VirMSSQL.TableAdapter.Servidor_De_Processos)
                    return Framework.DSCentral.USUX(Tipo);
                throw new Exception("USU não definido - Servidor de processos");
            }
            set { uSU = value; }
        }

        /// <summary>
        /// Nome Usuario logado
        /// </summary>
        public string USUNome
        {
            get 
            {
                if (uSUNome == null)
                    uSUNome = STTA.BuscaEscalar_string("select USUNome from USUarios where USU = @P1", USU);
                return uSUNome;
            }
        }


        private FrameworkProc.datasets.dCondominiosAtivos dCondominiosAtivos;
        private FrameworkProc.datasets.dCondominiosAtivos DCondominiosAtivos
        {
            get 
            {
                if (dCondominiosAtivos == null)
                {
                    if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    {
                        dCondominiosAtivos = new datasets.dCondominiosAtivos(this);
                        dCondominiosAtivos.CarregaCondominios();
                    }
                    else
                    {
                        dCondominiosAtivos = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosSTX(Tipo);
                    };
                }
                return dCondominiosAtivos;
            }
        }

        /// <summary>
        /// Condominio
        /// </summary>
        public FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON(int CON)
        {
            return DCondominiosAtivos.CONDOMINIOS.FindByCON(CON);            
        }

        #region Bloco MSSQL
        /// <summary>
        /// Equivalente ao TableAdapter.STTableAdapter no caso de processos
        /// </summary>
        private VirMSSQL.TableAdapter _TableAdapterTRANS;

        /// <summary>
        /// TableAdapter estático ou local no caso de processo
        /// </summary>
        public VirMSSQL.TableAdapter STTA
        {
            get
            {
                if (_TableAdapterTRANS == null)
                    if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                        _TableAdapterTRANS = (VirMSSQL.TableAdapter)(new TableAdapter(TBanco, true));
                    else
                        _TableAdapterTRANS = VirMSSQL.TableAdapter.ST(TBanco);
                return _TableAdapterTRANS;
            }
        }

        /// <summary>
        /// Tipo de banco de dados
        /// </summary>
        public TiposDeBanco TBanco { get { return Tipo == EMPTipo.Local ? TiposDeBanco.SQL : TiposDeBanco.Filial; } }

        /// <summary>
        /// Abertura de transação
        /// </summary>
        /// <param name="Identificador"></param>
        /// <param name="VirtualTableAdapters"></param>
        /// <returns></returns>
        public VirMSSQL.TableAdapter AbreTrasacaoSQL(string Identificador, params TableAdapter[] VirtualTableAdapters)
        {
            TiposDeBanco Tipo = VirtualTableAdapters.Length == 0 ? TiposDeBanco.SQL : VirtualTableAdapters[0].Tipo;
            foreach (TableAdapter TA in VirtualTableAdapters)
                if (TA.Tipo != Tipo)
                    throw new Exception("AbreTrasacaoSQL com tipos difierentes");
            if (Tipo.EstaNoGrupo(TiposDeBanco.SQL, TiposDeBanco.Filial))
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                {
                    STTA.AbreTrasacaoLocal(Identificador, VirtualTableAdapters);
                    return STTA;
                }
                else
                    return VirMSSQL.TableAdapter.AbreTrasacaoSQL(Tipo, Identificador, VirtualTableAdapters);
            }
            else
                throw new Exception(string.Format("AbreTrasacaoSQL com tipo inválido:{0}", Tipo));
        }

        /// <summary>
        /// Commit
        /// </summary>
        /// <returns></returns>
        public bool Commit()
        {
            return STTA.Commit();
        }

        /// <summary>
        /// Vircatch
        /// </summary>
        /// <param name="e"></param>
        public void Vircatch(Exception e)
        {
            STTA.Vircatch(e);
        }

        /// <summary>
        /// Embarca em transação se esta existir
        /// </summary>
        /// <param name="VirtualTableAdapters"></param>
        /// <returns></returns>
        public bool EmbarcaEmTrans(params TableAdapter[] VirtualTableAdapters)
        {
            //VirMSSQL.TableAdapter TA = VirMSSQL.TableAdapter.Servidor_De_Processos ? GetTableAdapterTRANS(TBanco, false) : STTA;
            if (STTA.GetSubTrans == 0)
                return false;
            STTA.AcolheTrans(VirtualTableAdapters);
            return true;
        } 
        #endregion

        /// <summary>
        /// Codigo da empresa
        /// </summary>
        public int EMP
        {
            get
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    return Tipo == EMPTipo.Local ? 1 : 3;
                else
                    return Tipo == EMPTipo.Local ? Framework.DSCentral.EMP : Framework.DSCentral.EMPF;
            }
        }
    }

    /// <summary>
    /// Tipo de filial
    /// </summary>
    public enum EMPTipo
    {
        /// <summary>
        /// Local
        /// </summary>
        Local,
        /// <summary>
        /// Filial
        /// </summary>
        Filial
    }        
}
