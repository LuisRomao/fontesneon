﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkProc
{
    /// <summary>
    /// Interface para os objetos que trabalham em 2 bancos ou no servidor de processos
    /// </summary>
    public interface IEMPTProc
    {        
        /// <summary>
        /// Classe que contem os dados referente ao banco os dados para o servidor de processos que seriam estátidos
        /// </summary>
        EMPTProc EMPTProc1{get;set;}
    }
}
