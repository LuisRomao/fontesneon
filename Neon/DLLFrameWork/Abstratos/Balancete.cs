﻿using System;
using System.Collections.Generic;
using System.Text;
using Framework.objetosNeon;

namespace Abstratos
{
    public class Balancete : ABS_Base
    {
        /// <summary>
        /// Objeto abstrato para o balancete
        /// </summary>
        public abstract class ABS_balancete
        {
            private static string NomeObjeto = "Balancete.GrBalancete.balancete";

            /// <summary>
            /// Cria um objeto real tipo balancete (real)
            /// </summary>
            /// <returns></returns>
            public static ABS_balancete GetBalancete(int CON, Competencia comp)
            {
                return (ABS_balancete)ContruaObjeto(NomeObjeto,new object[]{ CON, comp},new Type[]{typeof(int),typeof(Competencia)});
            }

            /// <summary>
            /// Indica se o balancete foi encontrado no Banco
            /// </summary>
            public abstract bool encontrado { get; }

            public abstract BALStatus Status { get; }

            /// <summary>
            /// Competência
            /// </summary>
            public abstract Competencia comp { get; }

            /// <summary>
            /// Data inicial
            /// </summary>
            public abstract DateTime DataI { get; }

            /// <summary>
            /// Data Final
            /// </summary>
            public abstract DateTime DataF { get; }

            /// <summary>
            /// Todas as contas Lógicas
            /// </summary>
            public SortedList<int, ContaLogica> ContasLogicas;
        }

        /// <summary>
        /// Uma conta lógica
        /// </summary>
        public class ContaLogica
        {
            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="CTLNome"></param>
            /// <param name="SaldoI"></param>
            /// <param name="SaldoF"></param>
            public ContaLogica(string CTLNome, decimal SaldoI, decimal SaldoF)
            {
                this.CTLNome = CTLNome;
                this.SaldoI = SaldoI;
                this.SaldoF = SaldoF;
            }

            /// <summary>
            /// Nome
            /// </summary>
            public string CTLNome;
            /// <summary>
            /// Saldo Inicial
            /// </summary>
            public decimal SaldoI;
            /// <summary>
            /// Saldo Final
            /// </summary>
            public decimal SaldoF;
        }

        public enum BALStatus
        {
            NaoIniciado = 0,
            Producao = 1,
            Termindo = 2,
            ParaPublicar = 3,
            Publicado = 4,
            FechadoDefinitivo = 5
        }
    }
}
