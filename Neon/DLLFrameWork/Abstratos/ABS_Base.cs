﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Abstratos
{
    /// <summary>
    /// Classe abstrata base.
    /// </summary>
    public abstract class ABS_Base
    {
                
        /// <summary>
        /// Constroi um objeto a partir de seu nome. Isso evita a carga da dll desnecessariamente
        /// </summary>
        /// <param name="Nome">Nome completo do objeto (com namespace)</param>
        /// <param name="parametros">Parametos do costrutor</param>
        /// <param name="TiposParametros">Tipos dos parametros. Não é obrigatório mas permite a utilização de null nos parâmetos</param>
        /// <returns>Objeto contruído</returns>
        protected static object ContruaObjeto(string Nome, object[] parametros,Type[] TiposParametros = null)
        {
            if (Nome != null)
            {
                Type Tipo = BuscaType(Nome);
                if (Tipo == null)
                    return null;
                if (parametros.Length == 0)
                {
                    System.Reflection.ConstructorInfo constructorInfoObj = Tipo.GetConstructor(Type.EmptyTypes);
                    if (constructorInfoObj == null)
                        return null;
                    return constructorInfoObj.Invoke(null);
                }
                else
                {
                    Type[] Tipos;
                    if (TiposParametros != null)
                        Tipos = TiposParametros;
                    else
                    {
                        Tipos = new Type[parametros.Length];
                        for (int i = 0; i < parametros.Length; i++)
                            Tipos[i] = parametros[i].GetType();
                    }
                    System.Reflection.ConstructorInfo constructorInfoObjP = Tipo.GetConstructor(Tipos);
                    if (constructorInfoObjP == null)
                        return null;
                    return constructorInfoObjP.Invoke(parametros);
                }
            }
            else
                return null;
        }

        /// <summary>
        /// Localiza um tipo pelo nome
        /// </summary>
        /// <param name="NomeComponente"></param>
        /// <returns></returns>
        private static Type BuscaType(string NomeComponente)
        {

            int PosPonto = NomeComponente.IndexOf('.');
            if (PosPonto <= 0)
                return null;
            System.Reflection.Assembly Ass;
            try
            {
                Ass = System.Reflection.Assembly.Load(NomeComponente.Substring(0, PosPonto));
            }
            catch
            {                
                Ass = System.Reflection.Assembly.LoadFrom(@"C:\FontesVS\Geral Releases\DLLS\" + NomeComponente.Substring(0, PosPonto) + ".dll");
            }
            return Ass.GetType(NomeComponente);

        }
    }
}
