﻿using System;
using CodBar;

namespace dllImpostoNeon.impressos
{
    class impGuiaGPS : Impress
    {
        public impGuiaGPS()
            : base()
        {
            NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de cópias de Cheque");
            Mascara = AbreForm();
            int LarguraUtilColuna1 = 850;
            int LarguraUtilColuna2 = 490;
            int LarguraUtilColuna3 = 490;
            int AlturaUtilLinhas = 60;
            int YbaseQuadro = 135;
            int AlturaUtilQuadro = 740;
            int espBordaQuadro = 10;
            int espLinhaInterna = 5;
            //Quadro
            Mascara += CodCaixa(0,
                                YbaseQuadro,
                                2 * espBordaQuadro + 2 * espLinhaInterna + LarguraUtilColuna1 + LarguraUtilColuna2 + LarguraUtilColuna3,
                                AlturaUtilQuadro + 2 * espBordaQuadro,
                                espBordaQuadro,
                                espBordaQuadro);
            //linhas verticais            
            Mascara += CodLinha(espBordaQuadro + LarguraUtilColuna1, YbaseQuadro + espBordaQuadro, espLinhaInterna, AlturaUtilQuadro);
            int AlturaLinha2 = 9 * AlturaUtilLinhas + 8 * espLinhaInterna;
            Mascara += CodLinha(espBordaQuadro + LarguraUtilColuna1 + espLinhaInterna + LarguraUtilColuna2,
                                YbaseQuadro + espBordaQuadro + AlturaUtilQuadro - AlturaLinha2,
                                espLinhaInterna,
                                AlturaLinha2);
            //Linha Vertical (toquinho)
            Mascara += CodLinha(espBordaQuadro + 280, YbaseQuadro + espBordaQuadro + 210 + espLinhaInterna, espLinhaInterna, 309 - 210 - espLinhaInterna);
            //linhas horizontais Coluna 1            
            int YlinhaQ1_3 = YbaseQuadro + espBordaQuadro + 210;
            int YlinhaQ1_2 = YbaseQuadro + espBordaQuadro + 309;
            int YlinhaQ1_1 = YbaseQuadro + espBordaQuadro + 520;
            Mascara += CodLinha(espBordaQuadro, YbaseQuadro + espBordaQuadro + 70, LarguraUtilColuna1, espLinhaInterna);
            Mascara += CodLinha(espBordaQuadro, YlinhaQ1_3, LarguraUtilColuna1, espLinhaInterna);
            Mascara += CodLinha(espBordaQuadro, YlinhaQ1_2, LarguraUtilColuna1, espLinhaInterna);
            Mascara += CodLinha(espBordaQuadro, YlinhaQ1_1, LarguraUtilColuna1, espLinhaInterna);
            //linhas horizontais Coluna 2 e 3                        
            for (int i = 1; i <= 9; i++)
                Mascara += CodLinha(espBordaQuadro + LarguraUtilColuna1 + espLinhaInterna,
                                    YbaseQuadro + espBordaQuadro + AlturaUtilQuadro - (i * (AlturaUtilLinhas + espLinhaInterna)),
                                    2 * LarguraUtilColuna2 + espLinhaInterna,
                                    espLinhaInterna);
            //Coluna 1
            int LarguraLogo = 240;
            Mascara += CodTXT(espBordaQuadro + LarguraLogo, YbaseQuadro + espBordaQuadro + 650, "MINISTÉRIO DA PREVIDÊNCIA SOCIAL", 1);
            Mascara += CodTXT(espBordaQuadro + LarguraLogo, YbaseQuadro + espBordaQuadro + 620, "Instituto Nacional do Seguro Social", 1);
            Mascara += CodTXT(espBordaQuadro + LarguraLogo, YbaseQuadro + espBordaQuadro + 590, "GUIA DA PREVIDENCIA SOCIAL - GPS", 1);

            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_1 -  30, "01 NOME OU RAZÃO SOCIAL/ FONE/ ENDEREÇO", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_1 -  70, "%NOME%", 2); 
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_1 - 120, "%TELEFONE%", 1);
            int alturatextoNF = 18;
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_1 - 120 - 1 * alturatextoNF, "%NF1%", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_1 - 120 - 2 * alturatextoNF, "%NF2%", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_1 - 120 - 3 * alturatextoNF, "%NF3%", 1);

            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_2 - 30, "02 VENCIMENTO", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_2 - 50, "(Uso exclusivo do INSS)", 0);

            int alturalinhatextoat = 18;
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_3 - 30, "ATENÇÃO:", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_3 - 45 - 1 * alturalinhatextoat, "É vedada a utilização de GPS para recolhimento de receita de valor", 0);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_3 - 45 - 2 * alturalinhatextoat, "inferior ao estipulado em Resolução publicada pelo INSS. A receita", 0);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_3 - 45 - 3 * alturalinhatextoat, "que resultar valor inferior deverá ser adicionada à contribuição ou", 0);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_3 - 45 - 4 * alturalinhatextoat, "importância correspondente nos meses subsequentes, até que o total", 0);
            Mascara += CodTXT(espBordaQuadro + 5, YlinhaQ1_3 - 45 - 5 * alturalinhatextoat, "seja igual ou superior ao valor mínimo fixado.", 0);

            //Coluna 2
            int referenciaY = YbaseQuadro + espBordaQuadro + AlturaUtilQuadro - 30;
            int referenciaX = espBordaQuadro + LarguraUtilColuna1 + espLinhaInterna + 5;
            Mascara += CodTXT(referenciaX, referenciaY - 0 * (AlturaUtilLinhas + espLinhaInterna), "03 CÓDIGO DE PAGAMENTO", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 1 * (AlturaUtilLinhas + espLinhaInterna), "04 COMPETÊNCIA", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 2 * (AlturaUtilLinhas + espLinhaInterna), "05 IDENTIFICADOR", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 3 * (AlturaUtilLinhas + espLinhaInterna), "06 VALOR DO INSS", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 4 * (AlturaUtilLinhas + espLinhaInterna), "07", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 5 * (AlturaUtilLinhas + espLinhaInterna), "08", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 6 * (AlturaUtilLinhas + espLinhaInterna), "09 VALOR DE OUTRAS ENTIDADES", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 7 * (AlturaUtilLinhas + espLinhaInterna), "10 ATM, MULTA E JUROS", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 8 * (AlturaUtilLinhas + espLinhaInterna), "11 TOTAL", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 9 * (AlturaUtilLinhas + espLinhaInterna), "AUTENTICAÇÃO BANCÁRIA", 1);

            //Coluna 3
            referenciaY = YbaseQuadro + espBordaQuadro + AlturaUtilQuadro - 60;
            referenciaX += LarguraUtilColuna2 + espLinhaInterna;
            Mascara += CodTXT(referenciaX, referenciaY - 0 * (AlturaUtilLinhas + espLinhaInterna), "%CODIGO%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 1 * (AlturaUtilLinhas + espLinhaInterna), "%PERIODO%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 2 * (AlturaUtilLinhas + espLinhaInterna), "%CNPJ%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 3 * (AlturaUtilLinhas + espLinhaInterna), "%VALOR%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 8 * (AlturaUtilLinhas + espLinhaInterna), "%VALOR%", 3);            

            Mascara += FechaForm(2);
        }
    }
}
