﻿using System;
using CodBar;

namespace dllImpostoNeon.impressos
{
    class impGuiaDarf : Impress
    {
        public impGuiaDarf()
            : base()
        {
            NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de cópias de Cheque");                    
            Mascara = AbreForm();
            int LarguraUtilColuna1 = 850;
            int LarguraUtilColuna2 = 490;
            int LarguraUtilColuna3 = 490;
            int AlturaUtilLinhas = 60;
            int YbaseQuadro = 135;            
            int AlturaUtilQuadro = 740;            
            int espBordaQuadro = 10;
            int espLinhaInterna = 5;
            //Quadro
            Mascara += CodCaixa(0, 
                                YbaseQuadro, 
                                2 * espBordaQuadro + 2 * espLinhaInterna + LarguraUtilColuna1 + LarguraUtilColuna2 + LarguraUtilColuna3 , 
                                AlturaUtilQuadro + 2 * espBordaQuadro, 
                                espBordaQuadro, 
                                espBordaQuadro);            
            //linhas verticais            
            Mascara += CodLinha(espBordaQuadro + LarguraUtilColuna1 , YbaseQuadro + espBordaQuadro, espLinhaInterna, AlturaUtilQuadro);
            int AlturaLinha2 = 9 * AlturaUtilLinhas + 8 * espLinhaInterna;
            Mascara += CodLinha(espBordaQuadro + LarguraUtilColuna1 + espLinhaInterna + LarguraUtilColuna2 , 
                                YbaseQuadro + espBordaQuadro + AlturaUtilQuadro - AlturaLinha2, 
                                espLinhaInterna, 
                                AlturaLinha2 );
            //linhas horizontais Coluna 1
            Mascara += CodLinha(espBordaQuadro,YbaseQuadro + espBordaQuadro +  70,LarguraUtilColuna1,espLinhaInterna);         
            Mascara += CodLinha(espBordaQuadro,YbaseQuadro + espBordaQuadro + 210,LarguraUtilColuna1,espLinhaInterna);         
            Mascara += CodLinha(espBordaQuadro,YbaseQuadro + espBordaQuadro + 335,LarguraUtilColuna1,espLinhaInterna);         
            Mascara += CodLinha(espBordaQuadro,YbaseQuadro + espBordaQuadro + 460,LarguraUtilColuna1,espLinhaInterna);         
            //linhas horizontais Coluna 2 e 3                        
            for (int i = 1; i <= 9; i++)
                Mascara += CodLinha(espBordaQuadro + LarguraUtilColuna1 + espLinhaInterna, 
                                    YbaseQuadro + espBordaQuadro + AlturaUtilQuadro - (i * (AlturaUtilLinhas + espLinhaInterna)), 
                                    2 * LarguraUtilColuna2 + espLinhaInterna, 
                                    espLinhaInterna);
            
            //Coluna 1
            int LarguraLogo = 240;
            Mascara += CodTXT(espBordaQuadro + LarguraLogo, YbaseQuadro + espBordaQuadro + 650, "MINISTÉRIO DA FAZENDA", 2);
            Mascara += CodTXT(espBordaQuadro + LarguraLogo, YbaseQuadro + espBordaQuadro + 620, "SECRETARIA DA RECEITA FEDERAL", 1);
            Mascara += CodTXT(espBordaQuadro + LarguraLogo, YbaseQuadro + espBordaQuadro + 590, "Documento de Arrecadacao de Receitas Federais", 0);
            Mascara += CodTXT(espBordaQuadro + LarguraLogo, YbaseQuadro + espBordaQuadro + 470, "DARF", 6);

            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 430, "01 NOME / TELEFONE",1);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 380, "%NOME%", 2); // 378
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 348, "%TELEFONE%", 1);

            int alturatextoNF = 18;
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 298, "%DESCRICAOCODIGO%", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 298 - 1 * alturatextoNF, "%NF1%", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 298 - 2 * alturatextoNF, "%NF2%", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 298 - 3 * alturatextoNF, "%NF3%", 1);

            int alturalinhatextoat = 18;
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 180, "ATENÇÃO:", 1);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 85 + 5 * alturalinhatextoat, "E vedado o recolhimento de tributos e contribuições administrados", 0);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 85 + 4 * alturalinhatextoat, "pela Secretaria da Receita Federal cujo valor total seja inferior", 0);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 85 + 3 * alturalinhatextoat, "a R$ 10,00. Ocorrendo tal situação, adicione esse valor ao", 0);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 85 + 2 * alturalinhatextoat, "tributo/contribuição de mesmo código de periodos subsequentes, até", 0);
            Mascara += CodTXT(espBordaQuadro + 5, YbaseQuadro + espBordaQuadro + 85 + 1 * alturalinhatextoat, "que o total seja igual ou superior a R$ 10,00.", 0);

            //Coluna 2
            int referenciaY = YbaseQuadro + espBordaQuadro + AlturaUtilQuadro - 30;
            int referenciaX = espBordaQuadro + LarguraUtilColuna1 + espLinhaInterna + 5;
            Mascara += CodTXT(referenciaX, referenciaY - 0 * (AlturaUtilLinhas + espLinhaInterna), "02 PERÍODO DE APURAÇÃO", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 1 * (AlturaUtilLinhas + espLinhaInterna), "03 NÚMERO DO CPF OU CNPJ", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 2 * (AlturaUtilLinhas + espLinhaInterna), "04 CÓDIGO DA RECEITA", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 3 * (AlturaUtilLinhas + espLinhaInterna), "05 NÚMERO DE REFERÊNCIA", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 4 * (AlturaUtilLinhas + espLinhaInterna), "06 DATA DE VENCIMENTO", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 5 * (AlturaUtilLinhas + espLinhaInterna), "07 VALOR DO PRINCIPAL", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 6 * (AlturaUtilLinhas + espLinhaInterna), "08 VALOR DA MULTA", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 7 * (AlturaUtilLinhas + espLinhaInterna), "09 VALOR DOS JUROS e/ou", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 7 * (AlturaUtilLinhas + espLinhaInterna) - 30, "ENCARGOS DL -1.025/69", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 8 * (AlturaUtilLinhas + espLinhaInterna), "10 VALOR TOTAL", 1);
            Mascara += CodTXT(referenciaX, referenciaY - 9 * (AlturaUtilLinhas + espLinhaInterna), "AUTENTICAÇÃO BANCÁRIA", 1);
            
            //Coluna 3
            referenciaY = YbaseQuadro + espBordaQuadro + AlturaUtilQuadro - 60;
            referenciaX += LarguraUtilColuna2 + espLinhaInterna;
            Mascara += CodTXT(referenciaX, referenciaY - 0 * (AlturaUtilLinhas + espLinhaInterna), "%PERIODO%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 1 * (AlturaUtilLinhas + espLinhaInterna), "%CNPJ%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 2 * (AlturaUtilLinhas + espLinhaInterna), "%CODIGO%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 4 * (AlturaUtilLinhas + espLinhaInterna), "%VENCIMENTO%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 5 * (AlturaUtilLinhas + espLinhaInterna), "%VALOR%", 3);
            Mascara += CodTXT(referenciaX, referenciaY - 8 * (AlturaUtilLinhas + espLinhaInterna), "%VALOR%", 3);
            
            Mascara += FechaForm(2);
        }
    }
}
