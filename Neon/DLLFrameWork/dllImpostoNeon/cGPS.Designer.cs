namespace dllImpostoNeon
{
    partial class cGPS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cGPS));
            this.colGPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSNomeClientePagador = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSCEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSUF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSCidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSCNPJ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSDataPagto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSValorINSS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSValorMulta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSOutras = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSValorTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSCodigoReceita = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSIdentificador = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSAno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSNomeIdentificador = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboBoxStatus = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colGPSComentario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGPSAutenticacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSDATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPS_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPS_CCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboBoxTipo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colGPSValorJuros = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSApuracao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.colGPSCodComunicacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSDigAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSDigConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.colNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGPSOrigem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboBoxOrigem = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ch_13 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.colEMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCONCodigoFolha1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxOrigem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch_13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6)});
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6});
            this.BarManager_F.MaxItemId = 30;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Caption = "Liberar";
            this.BtnAlterar_F.Enabled = false;
            this.BtnAlterar_F.Hint = "Liberar";
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            this.BtnAlterar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnAlterar_F_ItemClick);
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // barButtonAtualizar
            // 
            this.barButtonAtualizar.Hint = "Atualizar";
            this.barButtonAtualizar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonAtualizar.ImageOptions.Image")));
            this.barButtonAtualizar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonAtualizar.ImageOptions.LargeImage")));
            this.barButtonAtualizar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // GridControl_F
            // 
            this.GridControl_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Location = new System.Drawing.Point(0, 77);
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ComboBoxStatus,
            this.repositoryItemMemoExEdit1,
            this.ComboBoxTipo,
            this.ComboBoxOrigem,
            this.repositoryItemImageComboBox1});
            this.GridControl_F.Size = new System.Drawing.Size(1438, 353);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseBorderColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNumero,
            this.colGPS,
            this.colGPSNomeClientePagador,
            this.colGPSEndereco,
            this.colGPSCEP,
            this.colGPSUF,
            this.colGPSCidade,
            this.colGPSBairro,
            this.colGPSCNPJ,
            this.colGPSDataPagto,
            this.colGPSValorINSS,
            this.colGPSValorMulta,
            this.colGPSOutras,
            this.colGPSValorTotal,
            this.colGPSCodigoReceita,
            this.colGPSIdentificador,
            this.colGPSAno,
            this.colGPSMes,
            this.colGPSNomeIdentificador,
            this.colGPSAgencia,
            this.colGPSConta,
            this.colGPSStatus,
            this.colGPSComentario,
            this.colGPSAutenticacao,
            this.colGPSDATAI,
            this.colGPSDATAA,
            this.colGPSI_USU,
            this.colGPSA_USU,
            this.colGPS_CON,
            this.colGPS_CCD,
            this.colGPSTipo,
            this.colGPSValorJuros,
            this.colGPSVencimento,
            this.colGPSApuracao,
            this.colGPSCodComunicacao,
            this.colGPSDigAgencia,
            this.colGPSDigConta,
            this.colGPSOrigem,
            this.colEMP,
            this.colCONCodigoFolha1});
            this.GridView_F.CustomizationFormBounds = new System.Drawing.Rectangle(426, 335, 208, 464);
            this.GridView_F.GroupCount = 1;
            this.GridView_F.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "GPS", null, "(N�mero de guias = {0})")});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.CheckBoxSelectorColumnWidth = 15;
            this.GridView_F.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsSelection.MultiSelect = true;
            this.GridView_F.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.GridView_F.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.False;
            this.GridView_F.OptionsSelection.ShowCheckBoxSelectorInGroupRow = DevExpress.Utils.DefaultBoolean.True;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupedColumns = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGPSStatus, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGPSTipo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGPSNomeClientePagador, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNumero, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGPSOrigem, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridView_F_RowCellStyle);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "GPS";
            this.BindingSource_F.DataSource = typeof(dllImpostoNeonProc.dGPS);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // colGPS
            // 
            this.colGPS.Caption = "GPS";
            this.colGPS.FieldName = "GPS";
            this.colGPS.Name = "colGPS";
            this.colGPS.OptionsColumn.ReadOnly = true;
            this.colGPS.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colGPS.Width = 90;
            // 
            // colGPSNomeClientePagador
            // 
            this.colGPSNomeClientePagador.Caption = "Condom�nio";
            this.colGPSNomeClientePagador.FieldName = "GPSNomeClientePagador";
            this.colGPSNomeClientePagador.Name = "colGPSNomeClientePagador";
            this.colGPSNomeClientePagador.OptionsColumn.ReadOnly = true;
            this.colGPSNomeClientePagador.Visible = true;
            this.colGPSNomeClientePagador.VisibleIndex = 6;
            this.colGPSNomeClientePagador.Width = 229;
            // 
            // colGPSEndereco
            // 
            this.colGPSEndereco.Caption = "Endere�o";
            this.colGPSEndereco.FieldName = "GPSEndereco";
            this.colGPSEndereco.Name = "colGPSEndereco";
            this.colGPSEndereco.OptionsColumn.ReadOnly = true;
            // 
            // colGPSCEP
            // 
            this.colGPSCEP.Caption = "CEP";
            this.colGPSCEP.FieldName = "GPSCEP";
            this.colGPSCEP.Name = "colGPSCEP";
            this.colGPSCEP.OptionsColumn.ReadOnly = true;
            // 
            // colGPSUF
            // 
            this.colGPSUF.Caption = "UF";
            this.colGPSUF.FieldName = "GPSUF";
            this.colGPSUF.Name = "colGPSUF";
            this.colGPSUF.OptionsColumn.ReadOnly = true;
            // 
            // colGPSCidade
            // 
            this.colGPSCidade.Caption = "Cidade";
            this.colGPSCidade.FieldName = "GPSCidade";
            this.colGPSCidade.Name = "colGPSCidade";
            this.colGPSCidade.OptionsColumn.ReadOnly = true;
            // 
            // colGPSBairro
            // 
            this.colGPSBairro.Caption = "Bairro";
            this.colGPSBairro.FieldName = "GPSBairro";
            this.colGPSBairro.Name = "colGPSBairro";
            this.colGPSBairro.OptionsColumn.ReadOnly = true;
            // 
            // colGPSCNPJ
            // 
            this.colGPSCNPJ.Caption = "CNPJ";
            this.colGPSCNPJ.FieldName = "GPSCNPJ";
            this.colGPSCNPJ.Name = "colGPSCNPJ";
            this.colGPSCNPJ.OptionsColumn.ReadOnly = true;
            // 
            // colGPSDataPagto
            // 
            this.colGPSDataPagto.Caption = "Para";
            this.colGPSDataPagto.FieldName = "GPSDataPagto";
            this.colGPSDataPagto.Name = "colGPSDataPagto";
            this.colGPSDataPagto.OptionsColumn.ReadOnly = true;
            this.colGPSDataPagto.Visible = true;
            this.colGPSDataPagto.VisibleIndex = 7;
            this.colGPSDataPagto.Width = 85;
            // 
            // colGPSValorINSS
            // 
            this.colGPSValorINSS.Caption = "Pricipal";
            this.colGPSValorINSS.DisplayFormat.FormatString = "n2";
            this.colGPSValorINSS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGPSValorINSS.FieldName = "GPSValorPrincipal";
            this.colGPSValorINSS.Name = "colGPSValorINSS";
            this.colGPSValorINSS.OptionsColumn.ReadOnly = true;
            this.colGPSValorINSS.Visible = true;
            this.colGPSValorINSS.VisibleIndex = 11;
            this.colGPSValorINSS.Width = 77;
            // 
            // colGPSValorMulta
            // 
            this.colGPSValorMulta.Caption = "Multa";
            this.colGPSValorMulta.DisplayFormat.FormatString = "n2";
            this.colGPSValorMulta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGPSValorMulta.FieldName = "GPSValorMulta";
            this.colGPSValorMulta.Name = "colGPSValorMulta";
            this.colGPSValorMulta.OptionsColumn.ReadOnly = true;
            this.colGPSValorMulta.Visible = true;
            this.colGPSValorMulta.VisibleIndex = 12;
            this.colGPSValorMulta.Width = 56;
            // 
            // colGPSOutras
            // 
            this.colGPSOutras.Caption = "Outras";
            this.colGPSOutras.DisplayFormat.FormatString = "n2";
            this.colGPSOutras.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGPSOutras.FieldName = "GPSOutras";
            this.colGPSOutras.Name = "colGPSOutras";
            this.colGPSOutras.OptionsColumn.ReadOnly = true;
            this.colGPSOutras.Visible = true;
            this.colGPSOutras.VisibleIndex = 14;
            this.colGPSOutras.Width = 57;
            // 
            // colGPSValorTotal
            // 
            this.colGPSValorTotal.Caption = "Total";
            this.colGPSValorTotal.DisplayFormat.FormatString = "n2";
            this.colGPSValorTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGPSValorTotal.FieldName = "GPSValorTotal";
            this.colGPSValorTotal.Name = "colGPSValorTotal";
            this.colGPSValorTotal.OptionsColumn.ReadOnly = true;
            this.colGPSValorTotal.Visible = true;
            this.colGPSValorTotal.VisibleIndex = 15;
            this.colGPSValorTotal.Width = 62;
            // 
            // colGPSCodigoReceita
            // 
            this.colGPSCodigoReceita.Caption = "C�digo";
            this.colGPSCodigoReceita.FieldName = "GPSCodigoReceita";
            this.colGPSCodigoReceita.Name = "colGPSCodigoReceita";
            this.colGPSCodigoReceita.OptionsColumn.ReadOnly = true;
            this.colGPSCodigoReceita.Visible = true;
            this.colGPSCodigoReceita.VisibleIndex = 10;
            this.colGPSCodigoReceita.Width = 62;
            // 
            // colGPSIdentificador
            // 
            this.colGPSIdentificador.Caption = "Identificador";
            this.colGPSIdentificador.FieldName = "GPSIdentificador";
            this.colGPSIdentificador.Name = "colGPSIdentificador";
            this.colGPSIdentificador.OptionsColumn.ReadOnly = true;
            this.colGPSIdentificador.Visible = true;
            this.colGPSIdentificador.VisibleIndex = 16;
            this.colGPSIdentificador.Width = 84;
            // 
            // colGPSAno
            // 
            this.colGPSAno.Caption = "Ano";
            this.colGPSAno.FieldName = "GPSAno";
            this.colGPSAno.Name = "colGPSAno";
            this.colGPSAno.OptionsColumn.ReadOnly = true;
            this.colGPSAno.Visible = true;
            this.colGPSAno.VisibleIndex = 18;
            this.colGPSAno.Width = 39;
            // 
            // colGPSMes
            // 
            this.colGPSMes.Caption = "M�s";
            this.colGPSMes.FieldName = "GPSMes";
            this.colGPSMes.Name = "colGPSMes";
            this.colGPSMes.OptionsColumn.ReadOnly = true;
            this.colGPSMes.Visible = true;
            this.colGPSMes.VisibleIndex = 17;
            this.colGPSMes.Width = 39;
            // 
            // colGPSNomeIdentificador
            // 
            this.colGPSNomeIdentificador.Caption = "Nome Identificador";
            this.colGPSNomeIdentificador.FieldName = "GPSNomeIdentificador";
            this.colGPSNomeIdentificador.Name = "colGPSNomeIdentificador";
            this.colGPSNomeIdentificador.OptionsColumn.ReadOnly = true;
            // 
            // colGPSAgencia
            // 
            this.colGPSAgencia.Caption = "Ag�ncia";
            this.colGPSAgencia.FieldName = "GPSAgencia";
            this.colGPSAgencia.Name = "colGPSAgencia";
            this.colGPSAgencia.OptionsColumn.ReadOnly = true;
            // 
            // colGPSConta
            // 
            this.colGPSConta.Caption = "Conta";
            this.colGPSConta.FieldName = "GPSConta";
            this.colGPSConta.Name = "colGPSConta";
            this.colGPSConta.OptionsColumn.ReadOnly = true;
            // 
            // colGPSStatus
            // 
            this.colGPSStatus.Caption = "Status";
            this.colGPSStatus.ColumnEdit = this.ComboBoxStatus;
            this.colGPSStatus.FieldName = "GPSStatus";
            this.colGPSStatus.Name = "colGPSStatus";
            this.colGPSStatus.OptionsColumn.ReadOnly = true;
            this.colGPSStatus.Visible = true;
            this.colGPSStatus.VisibleIndex = 4;
            this.colGPSStatus.Width = 128;
            // 
            // ComboBoxStatus
            // 
            this.ComboBoxStatus.AutoHeight = false;
            this.ComboBoxStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxStatus.Name = "ComboBoxStatus";
            // 
            // colGPSComentario
            // 
            this.colGPSComentario.Caption = "Coment�rio";
            this.colGPSComentario.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colGPSComentario.FieldName = "GPSComentario";
            this.colGPSComentario.Name = "colGPSComentario";
            this.colGPSComentario.OptionsColumn.ReadOnly = true;
            this.colGPSComentario.Visible = true;
            this.colGPSComentario.VisibleIndex = 19;
            this.colGPSComentario.Width = 76;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            // 
            // colGPSAutenticacao
            // 
            this.colGPSAutenticacao.Caption = "Autenticacao";
            this.colGPSAutenticacao.FieldName = "GPSAutenticacao";
            this.colGPSAutenticacao.Name = "colGPSAutenticacao";
            this.colGPSAutenticacao.OptionsColumn.ReadOnly = true;
            this.colGPSAutenticacao.Visible = true;
            this.colGPSAutenticacao.VisibleIndex = 20;
            this.colGPSAutenticacao.Width = 125;
            // 
            // colGPSDATAI
            // 
            this.colGPSDATAI.Caption = "Inclus�o";
            this.colGPSDATAI.CustomizationCaption = "Data de inclus�o";
            this.colGPSDATAI.FieldName = "GPSDATAI";
            this.colGPSDATAI.Name = "colGPSDATAI";
            this.colGPSDATAI.OptionsColumn.ReadOnly = true;
            this.colGPSDATAI.Visible = true;
            this.colGPSDATAI.VisibleIndex = 21;
            // 
            // colGPSDATAA
            // 
            this.colGPSDATAA.Caption = "Altera��o";
            this.colGPSDATAA.FieldName = "GPSDATAA";
            this.colGPSDATAA.Name = "colGPSDATAA";
            this.colGPSDATAA.OptionsColumn.ReadOnly = true;
            // 
            // colGPSI_USU
            // 
            this.colGPSI_USU.Caption = "Inclus�o";
            this.colGPSI_USU.CustomizationCaption = "Operador Inclus�o";
            this.colGPSI_USU.FieldName = "GPSI_USU";
            this.colGPSI_USU.Name = "colGPSI_USU";
            this.colGPSI_USU.OptionsColumn.ReadOnly = true;
            // 
            // colGPSA_USU
            // 
            this.colGPSA_USU.Caption = "Altera��o";
            this.colGPSA_USU.FieldName = "GPSA_USU";
            this.colGPSA_USU.Name = "colGPSA_USU";
            this.colGPSA_USU.OptionsColumn.ReadOnly = true;
            // 
            // colGPS_CON
            // 
            this.colGPS_CON.Caption = "CON";
            this.colGPS_CON.FieldName = "GPS_CON";
            this.colGPS_CON.Name = "colGPS_CON";
            this.colGPS_CON.OptionsColumn.ReadOnly = true;
            // 
            // colGPS_CCD
            // 
            this.colGPS_CCD.Caption = "CCD";
            this.colGPS_CCD.FieldName = "GPS_CCD";
            this.colGPS_CCD.Name = "colGPS_CCD";
            this.colGPS_CCD.OptionsColumn.ReadOnly = true;
            // 
            // colGPSTipo
            // 
            this.colGPSTipo.Caption = "Tipo";
            this.colGPSTipo.ColumnEdit = this.ComboBoxTipo;
            this.colGPSTipo.FieldName = "GPSTipo";
            this.colGPSTipo.Name = "colGPSTipo";
            this.colGPSTipo.OptionsColumn.ReadOnly = true;
            this.colGPSTipo.Visible = true;
            this.colGPSTipo.VisibleIndex = 8;
            // 
            // ComboBoxTipo
            // 
            this.ComboBoxTipo.AutoHeight = false;
            this.ComboBoxTipo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxTipo.Name = "ComboBoxTipo";
            // 
            // colGPSValorJuros
            // 
            this.colGPSValorJuros.Caption = "Juros";
            this.colGPSValorJuros.DisplayFormat.FormatString = "n2";
            this.colGPSValorJuros.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGPSValorJuros.FieldName = "GPSValorJuros";
            this.colGPSValorJuros.Name = "colGPSValorJuros";
            this.colGPSValorJuros.OptionsColumn.ReadOnly = true;
            this.colGPSValorJuros.Visible = true;
            this.colGPSValorJuros.VisibleIndex = 13;
            this.colGPSValorJuros.Width = 52;
            // 
            // colGPSVencimento
            // 
            this.colGPSVencimento.Caption = "Vencimento";
            this.colGPSVencimento.FieldName = "GPSVencimento";
            this.colGPSVencimento.Name = "colGPSVencimento";
            this.colGPSVencimento.OptionsColumn.ReadOnly = true;
            this.colGPSVencimento.Visible = true;
            this.colGPSVencimento.VisibleIndex = 9;
            // 
            // colGPSApuracao
            // 
            this.colGPSApuracao.Caption = "Apura��o";
            this.colGPSApuracao.FieldName = "GPSApuracao";
            this.colGPSApuracao.Name = "colGPSApuracao";
            this.colGPSApuracao.OptionsColumn.ReadOnly = true;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Pagar em Cheque";
            this.barButtonItem1.Id = 24;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Marcar como Pago";
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Enviar Arquivos";
            this.barButtonItem3.Hint = "Enviar Arquivos";
            this.barButtonItem3.Id = 26;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Re-Agendar";
            this.barButtonItem4.Hint = "Re-Agendar";
            this.barButtonItem4.Id = 27;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // colGPSCodComunicacao
            // 
            this.colGPSCodComunicacao.Caption = "C�d. Comunica��o";
            this.colGPSCodComunicacao.FieldName = "GPSCodComunicacao";
            this.colGPSCodComunicacao.Name = "colGPSCodComunicacao";
            // 
            // colGPSDigAgencia
            // 
            this.colGPSDigAgencia.Caption = "D�g. Ag�ncia";
            this.colGPSDigAgencia.FieldName = "GPSDigAgencia";
            this.colGPSDigAgencia.Name = "colGPSDigAgencia";
            // 
            // colGPSDigConta
            // 
            this.colGPSDigConta.Caption = "D�g. Conta";
            this.colGPSDigConta.FieldName = "GPSDigConta";
            this.colGPSDigConta.Name = "colGPSDigConta";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Comprovantes";
            this.barButtonItem5.Hint = "Comprovantes";
            this.barButtonItem5.Id = 28;
            this.barButtonItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.Image")));
            this.barButtonItem5.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.LargeImage")));
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // colNumero
            // 
            this.colNumero.Caption = "N�mero";
            this.colNumero.FieldName = "GPS";
            this.colNumero.Name = "colNumero";
            this.colNumero.Visible = true;
            this.colNumero.VisibleIndex = 5;
            this.colNumero.Width = 90;
            // 
            // colGPSOrigem
            // 
            this.colGPSOrigem.Caption = "Origem";
            this.colGPSOrigem.ColumnEdit = this.ComboBoxOrigem;
            this.colGPSOrigem.FieldName = "GPSOrigem";
            this.colGPSOrigem.Name = "colGPSOrigem";
            this.colGPSOrigem.OptionsColumn.ReadOnly = true;
            this.colGPSOrigem.Visible = true;
            this.colGPSOrigem.VisibleIndex = 3;
            this.colGPSOrigem.Width = 70;
            // 
            // ComboBoxOrigem
            // 
            this.ComboBoxOrigem.AutoHeight = false;
            this.ComboBoxOrigem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxOrigem.Name = "ComboBoxOrigem";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.ch_13);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1438, 77);
            this.panelControl1.TabIndex = 5;
            // 
            // ch_13
            // 
            this.ch_13.Location = new System.Drawing.Point(20, 51);
            this.ch_13.MenuManager = this.BarManager_F;
            this.ch_13.Name = "ch_13";
            this.ch_13.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ch_13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ch_13.Properties.Appearance.Options.UseBackColor = true;
            this.ch_13.Properties.Appearance.Options.UseFont = true;
            this.ch_13.Properties.Caption = "D�cimo Terceiro";
            this.ch_13.Size = new System.Drawing.Size(146, 19);
            this.ch_13.TabIndex = 3;
            this.ch_13.CheckedChanged += new System.EventHandler(this.ch_13_CheckedChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(20, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(88, 16);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Compet�ncia:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(249, 25);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(106, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Calcular";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(114, 24);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 0;
            this.cCompet1.Titulo = null;
            // 
            // colEMP
            // 
            this.colEMP.Caption = "EMP";
            this.colEMP.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colEMP.FieldName = "EMP";
            this.colEMP.Name = "colEMP";
            this.colEMP.Visible = true;
            this.colEMP.VisibleIndex = 1;
            this.colEMP.Width = 49;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("SBC", 1, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("SA", 3, -1)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colCONCodigoFolha1
            // 
            this.colCONCodigoFolha1.Caption = "C�digo Folha";
            this.colCONCodigoFolha1.FieldName = "CONCodigoFolha1";
            this.colCONCodigoFolha1.Name = "colCONCodigoFolha1";
            this.colCONCodigoFolha1.Visible = true;
            this.colCONCodigoFolha1.VisibleIndex = 2;
            this.colCONCodigoFolha1.Width = 78;
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Reiniciar Remessa";
            this.barButtonItem6.Id = 29;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // cGPS
            // 
            this.Autofill = false;
            this.BindingSourcePrincipal = this.BindingSource_F;
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGPS";
            this.Size = new System.Drawing.Size(1438, 490);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxOrigem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch_13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colGPS;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSNomeClientePagador;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSCEP;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSUF;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSCidade;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSCNPJ;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSDataPagto;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSValorINSS;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSValorMulta;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSOutras;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSValorTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSCodigoReceita;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSIdentificador;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSAno;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSMes;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSNomeIdentificador;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSConta;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSComentario;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSAutenticacao;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSDATAA;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSA_USU;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboBoxStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colGPS_CON;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboBoxTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colGPS_CCD;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSValorJuros;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSApuracao;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSCodComunicacao;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSDigAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSDigConta;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraGrid.Columns.GridColumn colNumero;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboBoxOrigem;
        private DevExpress.XtraGrid.Columns.GridColumn colGPSOrigem;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoFolha1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraEditors.CheckEdit ch_13;
    }
}
