namespace dllImpostoNeon
{
    partial class cGPSCampos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cGPSCampos));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.INSS = new DevExpress.XtraEditors.CalcEdit();
            this.Multa = new DevExpress.XtraEditors.CalcEdit();
            this.Outros = new DevExpress.XtraEditors.CalcEdit();
            this.Codigo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.dataPara = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelOutrosJuros = new DevExpress.XtraEditors.LabelControl();
            this.comboTipo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.Apuracao = new DevExpress.XtraEditors.DateEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.Total = new DevExpress.XtraEditors.CalcEdit();
            this.Vencimento = new DevExpress.XtraEditors.DateEdit();
            this.chVencido = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.LIdentificador = new DevExpress.XtraEditors.LabelControl();
            this.LNome = new DevExpress.XtraEditors.LabelControl();
            this.Identificador = new DevExpress.XtraEditors.TextEdit();
            this.Nome = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.INSS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Multa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Outros.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Codigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataPara.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataPara.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Apuracao.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Apuracao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Total.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vencimento.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vencimento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVencido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Identificador.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nome.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
           
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.CompetenciaBind = 200910;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
           
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(86, 129);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 1;
            this.cCompet1.Titulo = null;
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(22, 103);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.Size = new System.Drawing.Size(308, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 0;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;            
            // 
            // INSS
            // 
            this.INSS.Location = new System.Drawing.Point(86, 214);
            this.INSS.Name = "INSS";
            this.INSS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.INSS.Properties.DisplayFormat.FormatString = "n2";
            this.INSS.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.INSS.Size = new System.Drawing.Size(134, 20);
            this.INSS.TabIndex = 4;
            this.INSS.EditValueChanged += new System.EventHandler(this.INSS_EditValueChanged);
            // 
            // Multa
            // 
            this.Multa.Location = new System.Drawing.Point(86, 240);
            this.Multa.Name = "Multa";
            this.Multa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Multa.Properties.DisplayFormat.FormatString = "n2";
            this.Multa.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Multa.Size = new System.Drawing.Size(134, 20);
            this.Multa.TabIndex = 5;
            this.Multa.EditValueChanged += new System.EventHandler(this.Multa_EditValueChanged);
            // 
            // Outros
            // 
            this.Outros.Location = new System.Drawing.Point(86, 266);
            this.Outros.Name = "Outros";
            this.Outros.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Outros.Properties.DisplayFormat.FormatString = "n2";
            this.Outros.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Outros.Size = new System.Drawing.Size(134, 20);
            this.Outros.TabIndex = 6;
            this.Outros.EditValueChanged += new System.EventHandler(this.Outros_EditValueChanged);
            // 
            // Codigo
            // 
            this.Codigo.Location = new System.Drawing.Point(86, 188);
            this.Codigo.Name = "Codigo";
            this.Codigo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Codigo.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2100 - 2100", 2100, -1)});
            this.Codigo.Size = new System.Drawing.Size(244, 20);
            this.Codigo.TabIndex = 3;
            this.Codigo.EditValueChanged += new System.EventHandler(this.Codigo_EditValueChanged);
            // 
            // dataPara
            // 
            this.dataPara.EditValue = null;
            this.dataPara.Location = new System.Drawing.Point(86, 159);
            this.dataPara.Name = "dataPara";
            this.dataPara.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dataPara.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataPara.Size = new System.Drawing.Size(121, 20);
            this.dataPara.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(18, 136);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 13);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Compet�ncia";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(47, 191);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(33, 13);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "C�digo";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(57, 217);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 13);
            this.labelControl3.TabIndex = 9;
            this.labelControl3.Text = "Valor";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(54, 243);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(26, 13);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Multa";
            // 
            // LabelOutrosJuros
            // 
            this.LabelOutrosJuros.Location = new System.Drawing.Point(47, 269);
            this.LabelOutrosJuros.Name = "LabelOutrosJuros";
            this.LabelOutrosJuros.Size = new System.Drawing.Size(33, 13);
            this.LabelOutrosJuros.TabIndex = 11;
            this.LabelOutrosJuros.Text = "Outros";
            // 
            // comboTipo
            // 
            this.comboTipo.Location = new System.Drawing.Point(86, 47);
            this.comboTipo.Name = "comboTipo";
            this.comboTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboTipo.Properties.EditValueChanged += new System.EventHandler(this.comboTipo_Properties_EditValueChanged);
            this.comboTipo.Size = new System.Drawing.Size(244, 20);
            this.comboTipo.TabIndex = 12;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(60, 50);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(20, 13);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Tipo";
            // 
            // Apuracao
            // 
            this.Apuracao.EditValue = null;
            this.Apuracao.Location = new System.Drawing.Point(202, 133);
            this.Apuracao.Name = "Apuracao";
            this.Apuracao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Apuracao.Properties.ReadOnly = true;
            this.Apuracao.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Apuracao.Size = new System.Drawing.Size(128, 20);
            this.Apuracao.TabIndex = 14;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(37, 162);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(43, 13);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Data Ag.";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(26, 313);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(55, 13);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Vencimento";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(56, 339);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(24, 13);
            this.labelControl9.TabIndex = 17;
            this.labelControl9.Text = "Total";
            // 
            // Total
            // 
            this.Total.Location = new System.Drawing.Point(86, 336);
            this.Total.Name = "Total";
            this.Total.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Total.Properties.DisplayFormat.FormatString = "n2";
            this.Total.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Total.Properties.ReadOnly = true;
            this.Total.Size = new System.Drawing.Size(134, 20);
            this.Total.TabIndex = 18;
            // 
            // Vencimento
            // 
            this.Vencimento.EditValue = null;
            this.Vencimento.Location = new System.Drawing.Point(87, 310);
            this.Vencimento.Name = "Vencimento";
            this.Vencimento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Vencimento.Properties.ReadOnly = true;
            this.Vencimento.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Vencimento.Size = new System.Drawing.Size(133, 20);
            this.Vencimento.TabIndex = 19;
            // 
            // chVencido
            // 
            this.chVencido.Location = new System.Drawing.Point(213, 159);
            this.chVencido.Name = "chVencido";
            this.chVencido.Properties.Caption = "Vencido";
            this.chVencido.Size = new System.Drawing.Size(117, 19);
            this.chVencido.TabIndex = 20;
            this.chVencido.CheckedChanged += new System.EventHandler(this.chVencido_CheckedChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(37, 76);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(43, 13);
            this.labelControl5.TabIndex = 21;
            this.labelControl5.Text = "Cod DTM";
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(87, 73);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Size = new System.Drawing.Size(100, 20);
            this.spinEdit1.TabIndex = 22;
            this.spinEdit1.EditValueChanged += new System.EventHandler(this.spinEdit1_EditValueChanged);
            // 
            // LIdentificador
            // 
            this.LIdentificador.Location = new System.Drawing.Point(20, 394);
            this.LIdentificador.Name = "LIdentificador";
            this.LIdentificador.Size = new System.Drawing.Size(61, 13);
            this.LIdentificador.TabIndex = 23;
            this.LIdentificador.Text = "Identificador";
            this.LIdentificador.Visible = false;
            // 
            // LNome
            // 
            this.LNome.Location = new System.Drawing.Point(53, 420);
            this.LNome.Name = "LNome";
            this.LNome.Size = new System.Drawing.Size(27, 13);
            this.LNome.TabIndex = 24;
            this.LNome.Text = "Nome";
            this.LNome.Visible = false;
            // 
            // Identificador
            // 
            this.Identificador.Location = new System.Drawing.Point(87, 391);
            this.Identificador.MenuManager = this.BarManager_F;
            this.Identificador.Name = "Identificador";
            
            this.Identificador.Size = new System.Drawing.Size(133, 20);
            
            this.Identificador.TabIndex = 25;
            this.Identificador.Visible = false;
            this.Identificador.Validating += new System.ComponentModel.CancelEventHandler(this.Identificador_Validating);
            // 
            // Nome
            // 
            this.Nome.Location = new System.Drawing.Point(87, 417);
            this.Nome.MenuManager = this.BarManager_F;
            this.Nome.Name = "Nome";
            this.Nome.Properties.MaxLength = 40;
            
            this.Nome.Size = new System.Drawing.Size(243, 20);
            
            this.Nome.TabIndex = 26;
            this.Nome.Visible = false;
            // 
            // cGPSCampos
            // 
            this.Controls.Add(this.Nome);
            this.Controls.Add(this.Identificador);
            this.Controls.Add(this.LNome);
            this.Controls.Add(this.LIdentificador);
            this.Controls.Add(this.spinEdit1);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.chVencido);
            this.Controls.Add(this.Vencimento);
            this.Controls.Add(this.Total);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.Apuracao);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.comboTipo);
            this.Controls.Add(this.LabelOutrosJuros);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.dataPara);
            this.Controls.Add(this.Codigo);
            this.Controls.Add(this.Outros);
            this.Controls.Add(this.Multa);
            this.Controls.Add(this.INSS);
            this.Controls.Add(this.lookupCondominio_F1);
            this.Controls.Add(this.cCompet1);
           
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGPSCampos";
            this.Size = new System.Drawing.Size(346, 454);
            this.Controls.SetChildIndex(this.cCompet1, 0);
            this.Controls.SetChildIndex(this.lookupCondominio_F1, 0);
            this.Controls.SetChildIndex(this.INSS, 0);
            this.Controls.SetChildIndex(this.Multa, 0);
            this.Controls.SetChildIndex(this.Outros, 0);
            this.Controls.SetChildIndex(this.Codigo, 0);
            this.Controls.SetChildIndex(this.dataPara, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.LabelOutrosJuros, 0);
            this.Controls.SetChildIndex(this.comboTipo, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.Apuracao, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.labelControl8, 0);
            this.Controls.SetChildIndex(this.labelControl9, 0);
            this.Controls.SetChildIndex(this.Total, 0);
            this.Controls.SetChildIndex(this.Vencimento, 0);
            this.Controls.SetChildIndex(this.chVencido, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.spinEdit1, 0);
            this.Controls.SetChildIndex(this.LIdentificador, 0);
            this.Controls.SetChildIndex(this.LNome, 0);
            this.Controls.SetChildIndex(this.Identificador, 0);
            this.Controls.SetChildIndex(this.Nome, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.INSS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Multa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Outros.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Codigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataPara.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataPara.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Apuracao.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Apuracao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Total.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vencimento.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vencimento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVencido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Identificador.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nome.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.DateEdit dataPara;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CalcEdit INSS;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CalcEdit Multa;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CalcEdit Outros;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ImageComboBoxEdit Codigo;
        /// <summary>
        /// 
        /// </summary>
        public Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl LabelOutrosJuros;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ImageComboBoxEdit comboTipo;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.DateEdit Apuracao;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CalcEdit Total;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.DateEdit Vencimento;
        private DevExpress.XtraEditors.CheckEdit chVencido;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.LabelControl LIdentificador;
        private DevExpress.XtraEditors.LabelControl LNome;
        private DevExpress.XtraEditors.TextEdit Identificador;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit Nome;
    }
}
