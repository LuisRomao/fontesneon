using System;
using System.ComponentModel;
using VirEnumeracoes;

namespace dllImpostoNeon
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cGPSCampos : CompontesBasicos.ComponenteBaseDialog
    {
        ImpostoNeon Imp = new ImpostoNeon();

        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ CNPJIdentificador;

        /// <summary>
        /// 
        /// </summary>
        public cGPSCampos()
        {
            InitializeComponent();
            ImpostoNeon.VirEnumTipoImposto.CarregaEditorDaGrid(comboTipo);
            Imp.Competencia = cCompet1.Comp;
            //AjustaDatas();
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            if (Imp.Tipo != 0)
            {
                Imp.Competencia = cCompet1.Comp;
                AjustaDatas();
            }
        }

        private void comboTipo_Properties_EditValueChanged(object sender, EventArgs e)
        {
            TipoImposto Tipo = (TipoImposto)comboTipo.EditValue;
            Imp.Tipo = Tipo;
            LabelOutrosJuros.Text = "Juros";
            switch (Tipo)
            {
                case TipoImposto.COFINS:
                    break;
                case TipoImposto.CSLL:
                    break;
                case TipoImposto.INSS:
                    Apuracao.Visible = false;
                    LabelOutrosJuros.Text = "Outros";
                    Codigo.Properties.Items.Clear();
                    Codigo.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2100: Empresas em Geral � CNPJ",(object)2100));
                    Codigo.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2631: Contribui��o Retida sobre a NF/Fatura da Empresa Prestadora de Servi�o � CNPJ", (object)2631));
                    break;
                case TipoImposto.IR:
                    Apuracao.Visible = true;
                    Codigo.Properties.Items.Clear();
                    Codigo.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem("1708: Remunera��o de servi�os prestados por pessoa jur�dica", (object)1708));
                    Codigo.SelectedIndex = 0;
                    break;
                case TipoImposto.ISSQN:
                    break;
                case TipoImposto.PIS:
                    Apuracao.Visible = true;
                    Codigo.Properties.Items.Clear();
                    Codigo.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem("8301: PIS/Pasep - Folha de Sal�rios", (object)8301));
                    Codigo.SelectedIndex = 0;
                    break;
                case TipoImposto.PIS_COFINS_CSLL:
                    break;
                default:
                    break;
            };
            AjustaDatas();
        }

        private void AjustaDatas() {
            Vencimento.DateTime = Imp.Vencimento;
            if (chVencido.Checked)
                dataPara.Properties.MaxValue = DateTime.MinValue;
            else
                dataPara.Properties.MaxValue = Imp.VencimentoEfetivo;
            Apuracao.DateTime = Imp.Apuracao;
        }

        private void INSS_EditValueChanged(object sender, EventArgs e)
        {
            AjustaTotal();
        }

        private void AjustaTotal() {
            Total.Value = INSS.Value + Multa.Value + Outros.Value;
        }

        private void Multa_EditValueChanged(object sender, EventArgs e)
        {
            AjustaTotal();
        }

        private void Outros_EditValueChanged(object sender, EventArgs e)
        {
            AjustaTotal();
        }

        private void chVencido_CheckedChanged(object sender, EventArgs e)
        {
            AjustaDatas();
        }

        private void spinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (spinEdit1.EditValue != null)
                if (spinEdit1.Text.Length > 0) {
                    int CON = 0;
                    int codDTM = (int)((decimal)spinEdit1.EditValue);
                    if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select con from condominios where CONCodigoFolha1 = @P1 or CONCodigoFolha2 = @P2", out CON, codDTM, codDTM))
                        lookupCondominio_F1.CON_sel = CON;
                }

        }

        private void Identificador_Validating(object sender, CancelEventArgs e)
        {
            CNPJIdentificador = new DocBacarios.CPFCNPJ(Identificador.Text);
            if (CNPJIdentificador.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                Identificador.Text = CNPJIdentificador.ToString();
            else
                Identificador.ErrorText = "CNPJ inv�lido";
        }

        private void Codigo_EditValueChanged(object sender, EventArgs e)
        {
            LNome.Visible = LIdentificador.Visible = Identificador.Visible = Nome.Visible = ((int)Codigo.EditValue == 2631);
        }
    }
}

