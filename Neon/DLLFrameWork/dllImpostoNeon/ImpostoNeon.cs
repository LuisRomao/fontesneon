/*
LH - 17/12/2014         14.1.4.101 - Novo construtor
MR - 26/12/2014 10:00 - 14.2.4.1   - Tratamento do novo tipo de imposto FGTS (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
*/
using System;
using CodBar;
using dllImpostos;
using dllImpostoNeonProc;
using Framework.objetosNeon;
using dllVirEnum;
using System.Drawing;
using VirEnumeracoes;

namespace dllImpostoNeon
{
    /// <summary>
    /// Classe para calculo de impostos
    /// </summary>
    public class ImpostoNeon:ImpostoNeonProc
    {
        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Tipo"></param>        
        public ImpostoNeon(TipoImposto _Tipo)
            : base(_Tipo)
        {
        }


        /// <summary>
        /// Construtor padr�o N�o usar
        /// </summary>        
        public ImpostoNeon()
        {
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Tipo"></param>
        /// <param name="_DataNota"></param>
        /// <param name="_DataPagamento"></param>
        /// <param name="_ValorBase"></param>
        /// <param name="_Competencia"></param>
        public ImpostoNeon(TipoImposto _Tipo, DateTime _DataNota, DateTime _DataPagamento, decimal _ValorBase, Competencia _Competencia = null)
            : base(_Tipo,_DataNota,_DataPagamento,_ValorBase,_Competencia)
        {            
        }
        #endregion

        /*
        private Competencia competencia;

        /// <summary>
        /// 
        /// </summary>
        public Competencia Competencia
        {
            get
            {
                if (competencia != null)
                    return competencia;
                else
                    return new Competencia(Apuracao);
            }
            set
            {
                competencia = value;
            }
        }*/

        

        /*
        /// <summary>
        /// determina se determinado dia � feriado
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        protected override bool IsFeriado(DateTime Data)
        {
            return Framework.datasets.dFERiados.IsFeriado(Data);
            
            //return Framework.objetosNeon.Competencia.IsFeriado(Data);
        }*/

        /*
        /// <summary>
        /// 
        /// </summary>
        public enum TipoImpresso 
        {
            /// <summary>
            /// 
            /// </summary>
            Termica,
            /// <summary>
            /// 
            /// </summary>
            Impressora
        }
        */
        /*
        /// <summary>
        /// Guias
        /// </summary>
        public enum guias 
        {
            /// <summary>
            /// 
            /// </summary>
            DARF,
            /// <summary>
            /// 
            /// </summary>
            GPS,
            /// <summary>
            /// 
            /// </summary>
            Nulo
        }*/

        /*
        /// <summary>
        /// Guia para recolhimento
        /// </summary>
        public guias Guia 
        {
            get 
            {
                switch (Tipo)
                {
                    case TipoImposto.COFINS:
                    case TipoImposto.CSLL:
                    case TipoImposto.IR:
                    case TipoImposto.PIS:
                    case TipoImposto.PIS_COFINS_CSLL:
                        return guias.DARF;
                    case TipoImposto.INSS:
                    case TipoImposto.INSSpf:
                    case TipoImposto.INSSpfEmp:
                    case TipoImposto.INSSpfRet:
                        return guias.GPS;
                    case TipoImposto.ISSQN:
                    case TipoImposto.ISS_SA:
                    default:
                        return guias.Nulo;
                }
            }
        }*/

        private Impress mascaraGuia
        {
            get 
            {
                switch (Tipo)
                {
                    case TipoImposto.COFINS:                        
                    case TipoImposto.CSLL:
                    case TipoImposto.IR:
                    case TipoImposto.IRPF:
                    case TipoImposto.PIS:
                    case TipoImposto.PIS_COFINS_CSLL:
                        return new impressos.impGuiaDarf();
                    case TipoImposto.INSS:                        
                    case TipoImposto.INSSpf:                        
                    case TipoImposto.INSSpfEmp:                       
                    case TipoImposto.INSSpfRet:
                        return new impressos.impGuiaGPS();                          
                    case TipoImposto.ISSQN:
                    case TipoImposto.ISS_SA:         
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                                 
                    case TipoImposto.FGTS:
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                                 
                    default:
                        return null;
                }
            }
        } 

        
        /*
        private string mascaraINSS =
"\r\n" +
"OC,Fr\r\n" +
"Q20,0\r\n" +
"q600\r\n" +
"N\r\n" +
//"GG450,20,\"INSSxB\"\r\n" +
"X8,10,10,600,1500\r\n" +
"A560,220,1,3,1,1,N,\"MINISTERIO DA PREVIDENCIA SOCIAL\"\r\n" +
"A530,210,1,3,1,1,N,\"Instituto Nacional do Seguro Social\"\r\n" +
"A480,230,1,3,1,1,N,\"GUIA DA PREVIDENCIA SOCIAL - GPS\"\r\n" +
"LO430,10,5,694\r\n" +
"A428,20,1,3,1,1,N,\"01 NOME OU RAZAO SOCIAL/ FONE/ ENDERECO\"\r\n" +
"A388,20,1,4,1,1,N,\"%NOME%\"\r\n" +
"A360,20,1,3,1,1,N,\"%TELEFONE%\"\r\n" +
"A320,20,1,3,1,1,N,\"%NF1%\"\r\n" +
"A300,20,1,3,1,1,N,\"%NF2%\"\r\n" +
"A280,20,1,3,1,1,N,\"%NF3%\"\r\n" + 
"LO260,10,5,694\r\n" +
"A250,20,1,3,1,1,N,\"02 VENCIMENTO\"\r\n" +
"A230,20,1,1,1,1,N,\"(Uso exclusivo do INSS)\"\r\n" +
"LO180,250,80,5\r\n" +
"LO180,10,5,694\r\n" +
"A178,20,1,3,1,1,N,\"ATENCAO:\"\r\n" +
"A148,25,1,1,1,1,N,\"E vedada a utilizacao de GPS para recolhimento de receita de valor\"\r\n" +
"A136,25,1,1,1,1,N,\"inferior ao estipulado em Resolucao publicada pelo INSS. A receita\"\r\n" +
"A124,25,1,1,1,1,N,\"que resultar valor inferior devera ser adicionada a contribuicao ou\"\r\n" +
"A112,25,1,1,1,1,N,\"importancia correspondente nos meses subsequentes, ate que o total\"\r\n" +
"A100,25,1,1,1,1,N,\"seja igual ou superior ao valor minimo fixado.\"\r\n" +
"LO80,10,5,694\r\n" +
"LO8,700,584,5\r\n" +
"LO121,1100,474,5\r\n" +
"A590,710,1,3,1,1,N,\"03 CODIGO DE PAGAMENTO\"\r\n" +
"A590,1200,1,4,1,1,N,\"%CODIGO%\"\r\n" +
"LO541,700,5,800\r\n" +
"A537,710,1,3,1,1,N,\"04 COMPETENCIA\"\r\n" +
"A537,1200,1,4,1,1,N,\"%PERIODO%\"\r\n" +
"LO488,700,5,800\r\n" +
"A484,710,1,3,1,1,N,\"05 IDENTIFICADOR\"\r\n" +
"A484,1150,1,4,1,1,N,\"%CNPJ%\"\r\n" +
"LO435,700,5,800\r\n" +
"A431,710,1,3,1,1,N,\"06 VALOR DO INSS\"\r\n" +
"A431,1200,1,4,1,1,N,\"%VALOR%\"\r\n" +
"LO382,700,5,800\r\n" +
"A378,710,1,3,1,1,N,\"07\"\r\n" +
"LO329,700,5,800\r\n" +
"A325,710,1,3,1,1,N,\"08\"\r\n" +
"LO276,700,5,800\r\n" +
"A276,710,1,3,1,1,N,\"09 VALOR DE OUTRAS ENTIDADES\"\r\n" +
"LO223,700,5,800\r\n" +
"A219,710,1,3,1,1,N,\"10 ATM, MULTA E JUROS\"\r\n" +
"LO170,700,5,800\r\n" +
"A166,710,1,3,1,1,N,\"11 TOTAL\"\r\n" +
"A166,1200,1,4,1,1,N,\"%VALOR%\"\r\n" +
"LO117,700,5,800\r\n" +
"A113,710,1,3,1,1,N,\"AUTENTICACAO BANCARIA\"\r\n" +
"P2\r\n";*/

        /*
        private Impress _ImpressoDARF;

        private Impress ImpressoDARF 
        {
            get 
            {
                if (_ImpressoDARF == null) 
                {
                    _ImpressoDARF = mascaraGuia; //mascaraGuia                    
                    string NomeImpressora = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de c�pias de Cheque");                    
                };
                return _ImpressoDARF;
            }
        }*/

        /// <summary>
        /// 
        /// </summary>
        public string NFs = "";

        /// <summary>
        /// 
        /// </summary>
        public string NomeTomador = "";

        /// <summary>
        /// 
        /// </summary>
        public string NomePrestador = "";

        /// <summary>
        /// 
        /// </summary>
        public string Telefone;

        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ cpfcnpjTomador;

        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ cpfcnpjPrestador;

        /// <summary>
        /// 
        /// </summary>
        public bool GuiaDoPrestador {
            get { return Guia_Nome_Prestador(Tipo); }
        }
        

        /// <summary>
        /// Imprime 
        /// </summary>
        /// <returns></returns>
        public bool Imprime()
        {            
            Impress ImpressoGuia = mascaraGuia;
            if (ImpressoGuia == null)
                return false;
            ImpressoGuia.Reset();
            if (Guia == guias.DARF)
                ImpressoGuia.SetCampo("%PERIODO%", Apuracao.ToString("dd/MM/yyyy"));
            else
                if (Guia == guias.GPS)
                    ImpressoGuia.SetCampo("%PERIODO%", Apuracao.ToString("MM/yyyy"));
            ImpressoGuia.SetCampo("%CODIGO%", Codigo);
            ImpressoGuia.SetCampo("%DESCRICAOCODIGO%", DescCodigo);
            ImpressoGuia.SetCampo("%VENCIMENTO%", Vencimento.ToString("dd/MM/yyyy"));
            ImpressoGuia.SetCampo("%VALOR%", ValorImposto.ToString("n2"));
            if (GuiaDoPrestador)
            {
                ImpressoGuia.SetCampo("%NOME%", NomePrestador);
                ImpressoGuia.SetCampo("%CNPJ%", cpfcnpjPrestador.ToString());
            }
            else
            {
                ImpressoGuia.SetCampo("%NOME%", NomeTomador);
                ImpressoGuia.SetCampo("%CNPJ%", cpfcnpjTomador.ToString());
            }
            ImpressoGuia.SetCampo("%TELEFONE%", Telefone);

            string NF2 = "";
            string NF3 = "";
            int corte = 48;
            if (NFs.Length > 48)
            {
                while ((NFs[corte] != ' ') && (corte > 30))
                    corte--;
                NF2 = NFs.Substring(corte).Trim();
                NFs = NFs.Substring(0, corte).Trim();
                if (NF2.Length > 48)
                {
                    corte = 48;
                    while ((NF2[corte] != ' ') && (corte > 30))
                        corte--;
                    NF3 = NF2.Substring(corte).Trim();
                    NF2 = NF2.Substring(0, corte).Trim();
                    if (NF3.Length > 48)
                        NF3 = NF3.Substring(0, 48).Trim();
                };
            };
            ImpressoGuia.SetCampo("%NF1%", NFs);
            ImpressoGuia.SetCampo("%NF2%", NF2);
            ImpressoGuia.SetCampo("%NF3%", NF3);
            return ImpressoGuia.ImprimeDOC();

        }

        /// <summary>
        /// Efetua o auto teste
        /// </summary>
        /// <returns></returns>
        public static string AutoTesteNeon()
        {
            Falha = false;           
            Relatorio = dllImpostos.Imposto.AutoTeste() + "\r\n\r\nRELAT�RIO DE TESTE NEON \r\n---------------------------\r\n\r\n";
            ImpostoNeon Imposto1 = new ImpostoNeon(TipoImposto.IR);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 1), new DateTime(2008, 12, 1), new DateTime(2008, 12, 30),10000,150);
            return Relatorio;
        }

        private static VirEnum virEnumTipoImposto;

        /// <summary>
        /// Descri��o dos tipos de impostos
        /// </summary>
        public static VirEnum VirEnumTipoImposto
        {
            get
            {
                if (virEnumTipoImposto == null)
                {
                    virEnumTipoImposto = new VirEnum(typeof(TipoImposto));
                    virEnumTipoImposto.GravaNomes(TipoImposto.PIS_COFINS_CSLL, "Contribui��e Sociais", Color.Aqua);
                    virEnumTipoImposto.GravaNomes(TipoImposto.INSS, "INSS", Color.FromArgb(255, 200, 200));
                    virEnumTipoImposto.GravaNomes(TipoImposto.INSSpfRet, "INSS - Autonomo prestador", Color.Lime);
                    virEnumTipoImposto.GravaNomes(TipoImposto.INSSpfEmp, "INSS - Autonomo tomador", Color.Lavender);
                    virEnumTipoImposto.GravaNomes(TipoImposto.INSSpf, "INSS - Autonomo", Color.Black);
                    virEnumTipoImposto.GravaNomes(TipoImposto.ISSQN, "ISS", Color.Yellow);
                    virEnumTipoImposto.GravaNomes(TipoImposto.IR, "IR - PJ", Color.Salmon);
                    virEnumTipoImposto.GravaNomes(TipoImposto.IRPF, "IR - PF", Color.Tomato);
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    virEnumTipoImposto.GravaNomes(TipoImposto.FGTS, "FGTS", Color.Violet);
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                }
                return virEnumTipoImposto;
            }
        }

        //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
        private static VirEnum virEnumOrigemImposto;

        /// <summary>
        /// Descri��o das origens de impostos
        /// </summary>
        public static VirEnum VirEnumOrigemImposto
        {
            get
            {
                if (virEnumOrigemImposto == null)
                {
                    virEnumOrigemImposto = new VirEnum(typeof(OrigemImposto));
                    virEnumOrigemImposto.GravaNomes(OrigemImposto.Folha, "Folha");
                    virEnumOrigemImposto.GravaNomes(OrigemImposto.Nota, "Nota");
                }
                return virEnumOrigemImposto;
            }
        }
        
    }
}
