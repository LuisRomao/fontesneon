/*
MR - 12/11/2014 10:00 -           - Inclus�o das colunas cod. de comunicacao, dig. agencia e dig. conta como ocultas no grid
                                    Na fun��o do bot�o "Enviar Arquivos" foi incluido o processo para envio de arquivos no novo layout simplesmente setando um novo status, neste primeiro momento processa os dois - antigo e novo (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 10/12/2014 12:00 -           - Inclus�o de coluna para sele��o de linhas no grid por grupo (todas do grupo e individual)
                                    Inclus�o de coluna N�mero (comprovante = GPS) pintado de azul caso exista (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
                                    Inclus�o de bot�o para visualizar e imprimir comprovantes selecionados na coluna de sele��o (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
                                    Revisado "Excluir" para permitir pagamentos por sele��o vida coluna (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
                                    Revisado "Liberar" para permitir pagamentos por sele��o vida coluna (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
                                    Revisado "Re-Agendar" para permitir pagamentos por sele��o vida coluna (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
                                    Ignora linha selecionada de cabecalho de grupo ao processar linhas selecionadas em "Pagar em Cheque" e "Marcar como pago", por�m n�o revisados (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
MR - 12/01/2015 12:00 -           - Corre��o na sele��o de linhas para ignorar apenas o cabe�alho (estava ignorando a primeira linha quando i=0)
                                    Desativado o envio de arquivo eletr�nico pelo processo antido (FINNET) (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/01/2015 12:00) ***)
MR - 29/01/2015 12:00 -           - Corrigido o bug ao gerar comprovante "arquivo em uso" (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
MR - 27/04/2015 12:00 -           - Inclus�o do campo de origem do imposto, folha ou nota, no grid
MR - 18/08/2015 10:00 -           - Cancelamento de tributo j� enviado ao banco (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (18/08/2015 10:00) ***)
                                    Corre��o de bug na exibicao de comprovante quando nenhuma linha est� selecionada
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Framework;
using CompontesBasicosProc;
using CompontesBasicos;
using iTextSharp.text;
using iTextSharp.text.pdf;
using dllImpostoNeonProc;
using VirEnumeracoesNeon;
using VirEnumeracoes;

namespace dllImpostoNeon
{
    /// <summary>
    /// cGPS
    /// </summary>
    public partial class cGPS : CompontesBasicos.ComponenteGradeNavegador
    {
        //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
        #region Variaveis
            private string DestinoPDF;
        #endregion
        //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***

        /// <summary>
        /// Construtor
        /// </summary>
        public cGPS()
        {
            InitializeComponent();
            GridView_F.OptionsBehavior.Editable = true;
            BindingSource_F.DataSource = dGPS.dGPSSt;
            //dGPS.dGPSSt.GPSTableAdapter.Fill(dGPS.dGPSSt.GPS);
            Framework.Enumeracoes.VirEnumStatusArquivo.CarregaEditorDaGrid(colGPSStatus);
            ImpostoNeon.VirEnumTipoImposto.CarregaEditorDaGrid(colGPSTipo);
            //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
            ImpostoNeon.VirEnumOrigemImposto.CarregaEditorDaGrid(colGPSOrigem);
            //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***

            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
            //Seta o destino dos comprovantes
            DestinoPDF = dEstacao.dEstacaoSt.GetValor("Destino PDF");
            if (DestinoPDF == "")
            {
                DestinoPDF = @"\\email\volume_1\publicacoes\";
                dEstacao.dEstacaoSt.SetValor("Destino PDF", DestinoPDF);
            }
            DestinoPDF = DestinoPDF + @"Financeiro\Recibos\Individual\";            
            //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
        }

        //esta duplicado no cDAR
        
        /// <summary>
        /// ExcluirRegistro
        /// </summary>
        protected override void ExcluirRegistro_F()
        {
            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
            //Pega as linhas selecionadas
            List<dGPS.GPSRow> selecionadas = new List<dGPS.GPSRow>();
            foreach (int i in GridView_F.GetSelectedRows())
            {
                //Ignora linha selecionada de cabecalho de grupo
                if (i >= 0)
                {
                    dGPS.GPSRow rowSel = (dGPS.GPSRow)GridView_F.GetDataRow(i);
                    if (rowSel != null)
                        selecionadas.Add(rowSel);
                }
            }

            //Processa cada selecao
            foreach (dGPS.GPSRow rowSel in selecionadas)
            {
            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                if (Enumeracoes.StatusArquivocancelaveis.Contains((StatusArquivo)rowSel.GPSStatus)) 
                {
                    string comentario="";
                    if (VirInput.Input.Execute("Motivo (" + rowSel.GPSNomeClientePagador + ")" , ref comentario,true))
                    {
                        //*** MRC - INICIO - TRIBUTOS (18/08/2015 10:00) ***
                        rowSel.GPSComentario = (rowSel.IsGPSComentarioNull() ? "" : rowSel.GPSComentario) + String.Format("{0:dd/MM/yy HH:mm:ss} - Exclus�o feita por {2} - Motivo: {1}\r\n", DateTime.Now, comentario, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome); ;
                        //*** MRC - TERMINO - TRIBUTOS (18/08/2015 10:00) ***
                        rowSel.GPSStatus = (int)StatusArquivo.Excluido;
                        dGPS.dGPSSt.GPSTableAdapter.Update(rowSel);
                    }
                }
                //*** MRC - INICIO - TRIBUTOS (18/08/2015 10:00) ***
                else if (Colecoes.StatusArquivoCancelaveisBanco.Contains((StatusArquivo)rowSel.GPSStatus))
                {
                    string comentario = "";
                    if (VirInput.Input.Execute("Motivo (" + rowSel.GPSNomeClientePagador + ")", ref comentario, true))
                    {
                        rowSel.GPSComentario = (rowSel.IsGPSComentarioNull() ? "" : rowSel.GPSComentario) + String.Format("{0:dd/MM/yy HH:mm:ss} - Exclus�o no banco programada por {2} - Motivo: {1}\r\n", DateTime.Now, comentario, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome);
                        rowSel.GPSStatus = (int)StatusArquivo.Aguardando_Envio_Exclusao;
                        dGPS.dGPSSt.GPSTableAdapter.Update(rowSel);
                    }
                }
                //*** MRC - TERMINO - TRIBUTOS (18/08/2015 10:00) ***
            }

            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
            RefreshDados();
            //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***            
        }

        /// <summary>
        /// RefreshDados
        /// </summary>
        public override void RefreshDados()
        {
            //dGPS.dGPSSt.GPSTableAdapter.Fill(dGPS.dGPSSt.GPS);
        }

        /// <summary>
        /// Libera
        /// </summary>
        /// <param name="LinhaMae"></param>
        /// <param name="Comentario"></param>
        private void Libera(dGPS.GPSRow LinhaMae,string Comentario) {
            if (Comentario != null)
                LinhaMae.GPSComentario = (LinhaMae.IsGPSComentarioNull() ? "" : LinhaMae.GPSComentario + "\r\n") + Comentario;
            LinhaMae.GPSStatus = (int)StatusArquivo.ParaEnviar;
            dGPS.dGPSSt.GPSTableAdapterX(LinhaMae.EMP).Update(LinhaMae);            
        }

        /// <summary>
        /// Alterar
        /// </summary>
        protected override void Alterar()
        {
            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
            //Pega as linhas selecionadas
            List<dGPS.GPSRow> selecionadas = new List<dGPS.GPSRow>();
            foreach (int i in GridView_F.GetSelectedRows())
            {
                //Ignora linha selecionada de cabecalho de grupo
                if (i >= 0)
                {
                    dGPS.GPSRow rowSel = (dGPS.GPSRow)GridView_F.GetDataRow(i);
                    if (rowSel != null)
                        selecionadas.Add(rowSel);
                }
            }

            //Processa cada selecao
            foreach (dGPS.GPSRow rowSel in selecionadas)
            {
            //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
                switch ((StatusArquivo)rowSel.GPSStatus)
                {
                    case StatusArquivo.Digitado:
                    case StatusArquivo.Liberar:
                        if (MessageBox.Show("Confirma a libera��o: Valor = " + rowSel.GPSValorTotal.ToString("0.00") + "?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            Libera(rowSel, DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " - Liberado por " + Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome);
                        break;
                    case StatusArquivo.Pre_Autorizado:
                    case StatusArquivo.Autorizado:
                    case StatusArquivo.Aguardando_Retorno:
                    case StatusArquivo.Inconsistente:
                    case StatusArquivo.Agendado:
                    case StatusArquivo.Com_operacao:
                    case StatusArquivo.Efetuado:
                    case StatusArquivo.Nao_Efetuado:
                    case StatusArquivo.Excluido:
                    case StatusArquivo.A_Agendar:
                    case StatusArquivo.ParaEnviar:
                        break;
                    case StatusArquivo.Liberar_Gerente:
                        bool aprovado = false;
                        if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.VerificaFuncionalidade("LiberaPagamento") > 0)
                            aprovado = true;
                        else
                        {
                            System.Data.DataRow rowGerentes = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("SELECT CONAuditor1_USU, CONAuditor2_USU FROM CONDOMINIOS where con = @P1", rowSel.GPS_CON);
                            if (rowGerentes != null)
                            {
                                if (rowGerentes["CONAuditor1_USU"] != DBNull.Value)
                                    if ((int)rowGerentes["CONAuditor1_USU"] == Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU)
                                        aprovado = true;
                                if (rowGerentes["CONAuditor2_USU"] != DBNull.Value)
                                    if ((int)rowGerentes["CONAuditor2_USU"] == Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU)
                                        aprovado = true;
                            };
                        };
                        if (aprovado && MessageBox.Show("Confirma a libera��o: Valor = " + rowSel.GPSValorTotal.ToString("0.00") + "?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            Libera(rowSel, DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " - Liberado por " + Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome);
                        break;
                    case StatusArquivo.Liberar_Gerencia:
                        if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.VerificaFuncionalidade("LiberaPagamento") > 0)
                            Libera(rowSel, DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " - Liberado por " + Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome);
                        break;
                    default:
                        break;
                }
            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
            }

            RefreshDados();
            //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
        }

        /// <summary>
        /// BtnIncluir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {                                   
            cGPSCampos GPSCampos = new cGPSCampos();
            if (GPSCampos.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                if (GPSCampos.lookupCondominio_F1.CON_sel == -1)
                    return;
                if (GPSCampos.dataPara.DateTime.Date == DateTime.MinValue)
                    return;
                if(Framework.DSCentral.FiltrarEMP){
                    Framework.DSCentral.FiltrarEMP = false;
                    FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CarregaCondominios();
                }; 
                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(GPSCampos.lookupCondominio_F1.CON_sel);
                if (rowCON == null)
                    return;
                if (rowCON.IsCONCepNull()) {
                    MessageBox.Show("CEP n�o cadastrado");
                    return;
                };
                string CEP = "0";
                foreach (char c in rowCON.CONCep)
                    if (char.IsDigit(c))
                        CEP += c;
                if (rowCON.IsCONCnpjNull()) {
                    MessageBox.Show("CNPJ n�o cadastrado");
                    return;
                };
                DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(rowCON.CONCnpj);
                if (Cnpj.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                {
                    MessageBox.Show("CNPJ inv�lido(" + Cnpj.Tipo + ")");
                    return;
                };
                if ((int)GPSCampos.Codigo.EditValue == 2631)
                {
                    if ((GPSCampos.CNPJIdentificador == null) || (GPSCampos.CNPJIdentificador.Tipo != DocBacarios.TipoCpfCnpj.CNPJ) || (GPSCampos.Nome.Text == ""))
                    {
                        MessageBox.Show("CNPJ/Nome do identificador inv�lido");
                        return;
                    }
                }
                string comentario = "";
                if (GPSCampos.INSS.Value + GPSCampos.Multa.Value + GPSCampos.Outros.Value > 5000) {
                    if (MessageBox.Show("Confirma valor superior a 5.000,00?", "Valor elevado", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        comentario = "Valor confirmado";
                    else
                        return;
                };
                ImpostoNeon NovoImposto = new ImpostoNeon((TipoImposto)GPSCampos.comboTipo.EditValue);
                NovoImposto.Competencia = GPSCampos.cCompet1.Comp;                
                dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();
                novalinha.GPSTipo = (int)GPSCampos.comboTipo.EditValue;
                novalinha.GPSVencimento = NovoImposto.Vencimento;
                novalinha.GPSNomeClientePagador = rowCON.CONNome;
                novalinha.GPS_CON = rowCON.CON;
                novalinha.GPSEndereco = rowCON.CONEndereco;
                novalinha.GPSCEP = int.Parse(CEP);
                novalinha.GPSUF = rowCON.CIDUf;
                novalinha.GPSCidade = (rowCON.CIDNome.Length > 20) ? rowCON.CIDNome.Substring(0, 20) : rowCON.CIDNome;
                novalinha.GPSBairro = rowCON.CONBairro;
                novalinha.GPSCNPJ = Cnpj.Valor;
                novalinha.GPSDataPagto = GPSCampos.dataPara.DateTime.Date;
                novalinha.GPSValorPrincipal = GPSCampos.INSS.Value;
                novalinha.GPSValorMulta = GPSCampos.Multa.Value;
                if ((TipoImposto)GPSCampos.comboTipo.EditValue == TipoImposto.INSS)
                {
                    novalinha.GPSValorJuros = 0;
                    novalinha.GPSOutras = GPSCampos.Outros.Value;
                }
                else 
                {
                    novalinha.GPSValorJuros = GPSCampos.Outros.Value;
                    novalinha.GPSOutras = 0;
                };
                novalinha.GPSValorTotal = GPSCampos.INSS.Value + GPSCampos.Multa.Value + GPSCampos.Outros.Value;
                novalinha.GPSCodigoReceita = (int)GPSCampos.Codigo.EditValue;
                if ((int)GPSCampos.Codigo.EditValue == 2631)
                {
                    novalinha.GPSIdentificador = GPSCampos.CNPJIdentificador.Valor;
                }
                else
                    novalinha.GPSIdentificador = Cnpj.Valor;
                novalinha.GPSAno = GPSCampos.cCompet1.Comp.Ano;
                novalinha.GPSMes = GPSCampos.cCompet1.Comp.Mes;
                if ((int)GPSCampos.Codigo.EditValue == 2631)
                    novalinha.GPSNomeIdentificador = GPSCampos.Nome.Text;
                else
                    novalinha.GPSNomeIdentificador = rowCON.CONNome;
                novalinha.GPSAgencia = int.Parse(rowCON.CONAgencia);
                novalinha.GPSConta = int.Parse(rowCON.CONConta);
                if (novalinha.GPSValorTotal > 20000)
                    novalinha.GPSStatus = (int)StatusArquivo.Liberar_Gerencia;
                else if (novalinha.GPSValorTotal > 15000)
                    novalinha.GPSStatus = (int)StatusArquivo.Liberar_Gerente;
                else
                    novalinha.GPSStatus = (int)StatusArquivo.Liberar;
                novalinha.GPSI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                novalinha.GPSDATAI = DateTime.Now;
                if (comentario != "")
                    novalinha.GPSComentario = comentario;
                dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllimpostoneon cGPS 231", dGPS.dGPSSt.GPSTableAdapter);
                    dGPS.dGPSSt.GPSTableAdapter.Update(novalinha);
                    novalinha.AcceptChanges();
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                    throw new Exception("virch - " + ex.Message, ex);
                }
            };
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string comando1 =
"UPDATE    PAGamentos\r\n" +
"SET              PAGTipo = 4\r\n" +
"FROM         GPS INNER JOIN\r\n" +
"                      PAGamentos ON GPS.GPS = PAGamentos.PAG_GPS INNER JOIN\r\n" +
"                      CHEques ON PAGamentos.PAG_CHE = CHEques.CHE\r\n" +
"WHERE     (GPS.GPS = @P1) AND (CHEques.CHETipo = 6) AND (CHEques.CHEStatus = 12);";
            string comandoconvertecheque =
"UPDATE    CHEques\r\n" +
"SET              CHETipo = 4, CHEStatus = 1\r\n" +
"FROM         GPS INNER JOIN\r\n" +
"                      PAGamentos ON GPS.GPS = PAGamentos.PAG_GPS INNER JOIN\r\n" +
"                      CHEques ON PAGamentos.PAG_CHE = CHEques.CHE\r\n" +
"WHERE     (GPS.GPS = @P1) AND (CHEques.CHETipo = 6) AND (CHEques.CHEStatus = 12);";
            //System.Collections.ArrayList PodeConverter = new System.Collections.ArrayList(new StatusArquivo[] { StatusArquivo.Inconsistente, StatusArquivo.Nao_Efetuado, StatusArquivo.Agendado, StatusArquivo.Aguardando_Retorno });
            System.Collections.ArrayList PodeConverter = new System.Collections.ArrayList(new StatusArquivo[] { StatusArquivo.Inconsistente, StatusArquivo.Nao_Efetuado, StatusArquivo.Aguardando_Retorno });

            foreach (int i in GridView_F.GetSelectedRows())
            {
                //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                //Ignora linha selecionada de cabecalho de grupo
                if (i >= 0)
                {
                //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
                    dGPS.GPSRow rowSel = (dGPS.GPSRow)GridView_F.GetDataRow(i);
                    if (rowSel != null)
                    {
                        //if ((rowSel.GPSTipo != 7) || (rowSel.GPSMes != 6) || (rowSel.GPSAno != 2011))
                        //    continue;
                        if (rowSel.GPSVencimento < DateTime.Today)
                            continue;
                        StatusArquivo status = (StatusArquivo)rowSel.GPSStatus;
                        if (PodeConverter.Contains(status))
                        {
                            try
                            {
                                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllimpostoNeon cGPS - 293",dGPS.dGPSSt.GPSTableAdapterX(rowSel.EMP));
                                if ((VirMSSQL.TableAdapter.ST(rowSel.EMP == DSCentral.EMP ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial).ExecutarSQLNonQuery(comando1, rowSel.GPS) == 1) && (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoconvertecheque, rowSel.GPS) == 1))
                                {
                                    rowSel.GPSStatus = (int)StatusArquivo.VirouCheque;
                                    rowSel.GPSComentario = string.Format("{0}{1:dd/MM/yyyy HH:mm} Convertido para cheque - {2}",(rowSel.IsGPSComentarioNull() ? "" : rowSel.GPSComentario + "\r\n"), DateTime.Now , Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome);
                                    dGPS.dGPSSt.GPSTableAdapterX(rowSel.EMP).Update(rowSel);                                    
                                }
                                VirMSSQL.TableAdapter.CommitSQL(rowSel.EMP == DSCentral.EMP ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial);
                            }
                            catch(Exception ex)
                            {
                                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                            }
                        }
                    }
                //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                }                
                //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Collections.ArrayList PodeConverter = new System.Collections.ArrayList(new StatusArquivo[] { StatusArquivo.Agendado, StatusArquivo.Aguardando_Retorno });
            foreach (int i in GridView_F.GetSelectedRows())
            {
                //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                //Ignora linha selecionada de cabecalho de grupo
                if (i >= 0)
                {
                //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
                    dGPS.GPSRow rowSel = (dGPS.GPSRow)GridView_F.GetDataRow(i);
                    if (rowSel != null)
                    {
                        StatusArquivo status = (StatusArquivo)rowSel.GPSStatus;
                        if (PodeConverter.Contains(status))
                        {
                            
                                rowSel.GPSStatus = (int)StatusArquivo.Efetuado;
                                rowSel.GPSComentario = string.Format("{0}{1:dd/MM/yyyy HH:mm} Marcado com pago manualmente - {2}", (rowSel.IsGPSComentarioNull() ? "" : rowSel.GPSComentario + "\r\n"), DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome);
                                dGPS.dGPSSt.GPSTableAdapterX(rowSel.EMP).Update(rowSel);
                                
                        }
                    }
                //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                }
                //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
            }
        }

        //private bool CanalConfigurado = false;
        //private string URIremota = "";

        /*
        private void ConfiguraCanal()
        {
            string Servidor = "";
            if (CompontesBasicos.FormPrincipalBase.strEmProducao)
                Servidor = "Neon20";
            else
                Servidor = "Lapvirtual8";
            if (CanalConfigurado)
                return;
            ChannelServices.RegisterChannel(new System.Runtime.Remoting.Channels.Http.HttpChannel(), false);
            string URIremota = string.Format("http://{0}:8989/statusModulo.rem", Servidor);
            WellKnownClientTypeEntry WKCTE = new WellKnownClientTypeEntry(typeof(moduloH.statusModulo), URIremota);
            RemotingConfiguration.RegisterWellKnownClientType(WKCTE);            
            CanalConfigurado = true;            
        }*/

        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
        private bool SetarEnvioImpostos()
        {
            int intCont = 0;            
            intCont = dGPS.dGPSSt.SetarEnvioGPS(LinhasSelecionadas());            
            return (intCont == 0) ? false : true;
        }
        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (VirInput.Input.Senha("Senha").ToString().Trim() == "CUIDADO")
            {
                //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
                //*** MRC - INICIO - TRIBUTOS (12/01/2015 12:00) ***
                //Layout Antigo
                //ConfiguraCanal();            
                //MessageBox.Show(string.Format("Enviar Impostos (antigo): {0}\r\n", new moduloH.statusModulo().EnviarImpostos() ? "Enviado" : "Nada para enviar"));
                //*** MRC - TERMINO - TRIBUTOS (12/01/2015 12:00) ***
                //Layout Novo
                MessageBox.Show(string.Format("Enviar Impostos: {0}\r\n", SetarEnvioImpostos() ? "Enviado" : "Nada para enviar"));
                //Atualiza o grid
                //dGPS.dGPSSt.GPSTableAdapter.Fill(dGPS.dGPSSt.GPS);
                //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
                Carrega();
            }
            else
            {
                MessageBox.Show("Senha incorreta");
            }
        }

        private List<dGPS.GPSRow> LinhasSelecionadas()
        {
            List<dGPS.GPSRow> selecionadas = new List<dGPS.GPSRow>();
            foreach (int i in GridView_F.GetSelectedRows())
            {
                //Ignora linha selecionada de cabecalho de grupo
                if (i >= 0)
                {
                    dGPS.GPSRow rowSel = (dGPS.GPSRow)GridView_F.GetDataRow(i);
                    if (rowSel != null)
                        selecionadas.Add(rowSel);
                }
            }
            return selecionadas;
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Seta status permitidos para reagendamento e nova data
            System.Collections.ArrayList PodeReagendar = new System.Collections.ArrayList(new StatusArquivo[] { StatusArquivo.Digitado, StatusArquivo.Inconsistente, StatusArquivo.Nao_Efetuado, StatusArquivo.ParaEnviar });
            DateTime? NovaData = DateTime.Today.AddDays(1);
            //NovaData = VirInput.Input.Execute("Nova Data", ref NovaData);
            if (!VirInput.Input.Execute("Nova Data", ref NovaData,DateTime.Today,null))
                return;

            /*
            //Pega as linhas selecionadas
            List<dGPS.GPSRow> selecionadas = new List<dGPS.GPSRow>();
            foreach (int i in GridView_F.GetSelectedRows())
            {
                //Ignora linha selecionada de cabecalho de grupo
                if (i >= 0)
                {
                    dGPS.GPSRow rowSel = (dGPS.GPSRow)GridView_F.GetDataRow(i);
                    if (rowSel != null)
                        selecionadas.Add(rowSel);
                }
            }*/

            //Processa cada selecao
            foreach (dGPS.GPSRow rowSel in LinhasSelecionadas())
            {            
                StatusArquivo status = (StatusArquivo)rowSel.GPSStatus;
                //if (PodeReagendar.Contains(status))
                if (status.EstaNoGrupo(StatusArquivo.Digitado, StatusArquivo.Inconsistente, StatusArquivo.Nao_Efetuado, StatusArquivo.ParaEnviar,StatusArquivo.Aguardando_Retorno))
                {
                    if (rowSel.GPSVencimento < NovaData)
                        continue;
                    
                        rowSel.GPSStatus = (int)StatusArquivo.ParaEnviar;
                        rowSel.GPSComentario = string.Format("{0}{1:dd/MM/yyyy HH:mm} Re-Agendado manualmente - {2}", (rowSel.IsGPSComentarioNull() ? "" : rowSel.GPSComentario + "\r\n"), DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome);
                        rowSel.GPSDataPagto = NovaData.Value;
                        dGPS.dGPSSt.GPSTableAdapterX(rowSel.EMP).Update(rowSel);                        
                    
                }
            }
        }

        //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
        private bool MergePDF(SortedList<int, dGPS.GPSRow> lstArquivos, string strArquivo)
        {
            //Inicia variaveis
            PdfReader reader = null;
            Document sourceDocument = null;
            PdfCopy pdfCopyProvider = null;
            PdfImportedPage importedPage;

            try
            {
                //Cria um novo documento
                sourceDocument = new Document();
                pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(strArquivo, System.IO.FileMode.Create));

                //Abre o docuemnto
                sourceDocument.Open();

                //Varre cada arquivo a ser inserido no novo documento
                foreach (dGPS.GPSRow rowSel in lstArquivos.Values)
                {
                    //Insere
                    reader = new PdfReader(DestinoPDF + @"\recibo_tributo_" + rowSel.GPS.ToString() + ".pdf");
                    importedPage = pdfCopyProvider.GetImportedPage(reader, 1);
                    pdfCopyProvider.AddPage(importedPage);
                    reader.Close();
                }

                //Salva o documento
                sourceDocument.Close();
            }
            catch { return false; }
            return true;
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Cria lista dos arquivos de comprovantes a serem impressos
            SortedList<int, dGPS.GPSRow> lstArquivos = new SortedList<int, dGPS.GPSRow>();
            int diferenciador = 0;
            //Pega as linhas selecionadas
            int SemCodigo = -1;
            bool booFaltaComprovante = false;
            foreach (int i in GridView_F.GetSelectedRows())
            {
                //Ignora linha selecionada de cabecalho de grupo
                if (i >= 0)
                {
                    //Pega a linha
                    dGPS.GPSRow rowSel = (dGPS.GPSRow)GridView_F.GetDataRow(i);
                    if (rowSel != null)
                    {
                        //Verifica se arquivo de comprovante foi gerado e insere na lista
                        int codigo = rowSel.IsCONCodigoFolha1Null() ? SemCodigo-- : rowSel.CONCodigoFolha1 * 1000 + diferenciador++;
                        if (System.IO.File.Exists(DestinoPDF + @"\recibo_tributo_" + rowSel.GPS.ToString() + ".pdf"))
                            lstArquivos.Add(codigo, rowSel);
                        else
                            booFaltaComprovante = true;
                    }
                }
            }

            //*** MRC - INICIO - TRIBUTOS (18/08/2015 10:00) ***
            //Verifica se nao tem nenhum comprovante pra exibir (nao selecionou nada ou nao encontrou arquivo)
            if (lstArquivos.Count == 0)
            {
                //Avisa caso nao tenha encontrado comprovantes selecionados
                if (booFaltaComprovante)
                    MessageBox.Show("ATEN��O: Comprovantes selecionados n�o foram encontrados.");   
                else
                    MessageBox.Show("ATEN��O: Nenhum comprovante foi selecionado.");
                return;
            }
            //*** MRC - TERMINO - TRIBUTOS (18/08/2015 10:00) ***

            //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***
            //Define nome do arquivo de comprovante (que nao esteja em uso)
            string Path = System.Windows.Forms.Application.StartupPath + @"\TMP\";
            string strArquivo = String.Format("{0}Comprovante.pdf", Path);
            if (!System.IO.Directory.Exists(Path))
                System.IO.Directory.CreateDirectory(Path);
            if (System.IO.File.Exists(strArquivo))
                try
                { System.IO.File.Delete(strArquivo); }
                catch
                { strArquivo = String.Format("{0}Comprovante_{1:ddMMyyhhmmss}.pdf", Path, DateTime.Now); };
            System.Threading.Thread.Sleep(3000);
            //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***

            //Junta arquivos das linhas selecionadas
            if (MergePDF(lstArquivos, strArquivo))
            {
                //Avisa caso nao tenha encontrado algum comprovante
                if (booFaltaComprovante)
                    MessageBox.Show("ATEN��O: Alguns comprovantes selecionados n�o foram encontrados.");   

                //Abre o documento 
                System.Diagnostics.Process.Start(strArquivo);

                //Verifica se aponta as impressoes
                if (MessageBox.Show("Deseja apontar a impress�o do(s) comprovante(s)?", "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    foreach (dGPS.GPSRow rowSel in lstArquivos.Values)
                        Framework.DSCentral.IMPressoesTableAdapter.Incluir(rowSel.GPS_CON, 159, 1, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Tributo No " + rowSel.GPS, false);
            }       
            else
                MessageBox.Show("ATEN��O: Ocorreu um erro ao exibir os comprovantes. Verifique se n�o existe nenhum comprovante aberto.");     
        }

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            //Para a coluna de n�mero de gps, pinta de azul aqueles que possuem comprovante
            /*
            if (e.Column == colNumero)
            {
                DevExpress.XtraGrid.Views.Grid.GridView xGrid = null;
                xGrid = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                object val = xGrid.GetRowCellValue(e.RowHandle, e.Column);
                if (val != null)
                    if (System.IO.File.Exists(DestinoPDF + @"\recibo_tributo_" + Convert.ToString(val) + ".pdf"))
                        e.Appearance.ForeColor = Color.Blue;
                    else
                        e.Appearance.ForeColor = Color.Black;
            }*/
        }

        private void cGPS_Load(object sender, EventArgs e)
        {

        }

        private int Carrega()
        {
            int Total = 0;
            if (ch_13.Checked)
            {
                Total = dGPS.dGPSSt.GPSTableAdapter.FillByCompet(dGPS.dGPSSt.GPS, cCompet1.Comp.Ano, 13);
                Total += dGPS.dGPSSt.GPSTableAdapterF.FillByCompet(dGPS.dGPSSt.GPS, cCompet1.Comp.Ano, 13);
            }
            else
            {
                Total = dGPS.dGPSSt.GPSTableAdapter.FillByCompet(dGPS.dGPSSt.GPS, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
                Total += dGPS.dGPSSt.GPSTableAdapterF.FillByCompet(dGPS.dGPSSt.GPS, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
            }
            return Total;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Carrega();            
        }

        private void BtnAlterar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string comando = "update condominios set CONSequencialRemessaTributos = 99998 where con = @P1";
            //Processa cada selecao
            foreach (dGPS.GPSRow rowSel in LinhasSelecionadas())
            {
                StatusArquivo status = (StatusArquivo)rowSel.GPSStatus;
                if (status == StatusArquivo.Inconsistente)
                {
                    VirMSSQL.TableAdapter.ST(rowSel.EMP == DSCentral.EMP ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial).ExecutarSQLNonQuery(comando, rowSel.GPS_CON);
                }
            }
        }

        private void ch_13_CheckedChanged(object sender, EventArgs e)
        {
            cCompet1.Enabled = !ch_13.Checked;
        }

        /*
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dGPS.GPSRow rowSel = (dGPS.GPSRow)GridView_F.GetFocusedDataRow();
            if ((rowSel == null) || (rowSel.GPSTipo != (int)TipoImposto.INSS))
                return;

        }*/



    }
}

