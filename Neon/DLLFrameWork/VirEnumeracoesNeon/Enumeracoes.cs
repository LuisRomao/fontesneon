﻿using System;
using System.Collections.Generic;

namespace VirEnumeracoesNeon
{
    /// <summary>
    /// Status Balancete
    /// </summary>
    public enum BALStatus
    {
        /// <summary>
        /// Erro
        /// </summary>
        Erro = -1,
        /// <summary>
        /// Não iniciado
        /// </summary>
        NaoIniciado = 0,
        /// <summary>
        /// Em produção
        /// </summary>
        Producao = 1,
        /// <summary>
        /// Terminado
        /// </summary>
        Termindo = 2,
        /// <summary>
        /// Para Publicar
        /// </summary>
        ParaPublicar = 3,
        /// <summary>
        /// Publicado
        /// </summary>
        Publicado = 4,
        /// <summary>
        /// 
        /// </summary>
        FechadoDefinitivo = 5,
        /// <summary>
        /// Enviado para o condomínio
        /// </summary>
        Enviado = 6
    }

    /// <summary>
    /// Gropos fixos de despesas
    /// </summary>
    public enum GrupoDebito
    {
        /// <summary>
        /// Gerais
        /// </summary>
        gerais = 0,
        /// <summary>
        /// Mão de Obra
        /// </summary>
        MaoObra = 1,
        /// <summary>
        /// Administrativas
        /// </summary>
        Administrativas = 2,
        /// <summary>
        /// Administradora
        /// </summary>
        Administradora = 3,
        /// <summary>
        /// Bancárias
        /// </summary>
        Bancarias = 4,
        /// <summary>
        /// Honorários
        /// </summary>
        Honorarios = 5
    }

    /// <summary>
    /// Tipo de balancete
    /// </summary>
    public enum TipoBalancete
    {
        /// <summary>
        /// Pasta
        /// </summary>
        Balancete = 0,
        /// <summary>
        /// Boleto
        /// </summary>
        Boleto = 1,
        /// <summary>
        /// Acumulado
        /// </summary>
        Acumulado = 2,
        /// <summary>
        /// Para simulacao
        /// </summary>
        Simulador = 3,
        /// <summary>
        /// Site Grafico
        /// </summary>
        SiteGrafico = 4,
    }

    /// <summary>
    /// Formas de pagamento
    /// </summary>
    public enum FormaPagamento
    {
        /// <summary>
        /// Via correio (somente)
        /// </summary>
        Correio = 2,
        /// <summary>
        /// Entrega no condomínio (somente)
        /// </summary>
        EntreigaPessoal = 4,
        /// <summary>
        /// Correio E e-mail
        /// </summary>
        CorreioEemail = 12,
        /// <summary>
        /// No condomínio e e-mail
        /// </summary>
        EntreigaPessoalEemail = 14,
        /// <summary>
        /// Por e-mail - via correio quando necessário
        /// </summary>
        email_Correio = 22,
        /// <summary>
        /// Por e-mail - no condominio quando necessário
        /// </summary>
        email_EntreigaPessoal = 24,
        /// <summary>
        /// Não é gravado no banco, é setado na emissão do boleto
        /// </summary>
        Dentro_DeSuper = 0,
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum APTStatusCobranca
    {
        /// <summary>
        /// Extra
        /// </summary>
        Extra = 3,
        /// <summary>
        /// Novo
        /// </summary>
        Novo = 1,
        /// <summary>
        /// Carta simples
        /// </summary>
        PrimeiraCarta = 2,
        /// <summary>
        /// Último aviso
        /// </summary>
        UltimoAviso = 4,
        /// <summary>
        /// Cobrança jurídica - código mantido por causa do site
        /// </summary>
        juridico = -1,
        /// <summary>
        /// Sem cobrança
        /// </summary>
        SemCobranca = 0,
        /// <summary>
        /// Este status se aplica as linhas mas nao ao apartamento!!
        /// </summary>
        Terminada = 5
    }

    /// <summary>
    /// Tipos de pagamento P/I
    /// </summary>
    public enum TipoPagIP
    {
        /// <summary>
        /// Inqluilino nunca paga
        /// </summary>
        NaoPaga = 0,
        /// <summary>
        /// Inqluilino sempre paga
        /// </summary>
        Paga = 1,
        /// <summary>
        /// Segue o plano de contas
        /// </summary>
        SeguePLA = 2
    }

    /// <summary>
    /// Tipos de importador
    /// </summary>
    public enum TiposArquivo
    {
        /// <summary>
        /// Arquivo de impressão de guia FGTS
        /// </summary>
        FGTS,
        /// <summary>
        /// RTF da darf
        /// </summary>
        DARF
    }

    /// <summary>
    /// Status
    /// </summary>
    public enum StatusPagamentoPeriodico
    {
        /// <summary>
        /// inativo
        /// </summary>
        Inativo = 0,
        /// <summary>
        /// O documento nao foi cadastrado
        /// </summary>
        AtivadoSemDOC = 1,
        /// <summary>
        /// Ativado com documento
        /// </summary>
        Ativado = 2,
        /// <summary>
        /// Encerrado
        /// </summary>
        Encerrado = 3,
    }

    /// <summary>
    /// Tipos de documento
    /// </summary>
    public enum TiposDOC
    {
        /// <summary>
        /// 
        /// </summary>
        Descritivo = 0,
        /// <summary>
        /// 
        /// </summary>
        Contrato = 1,
        /// <summary>
        /// 
        /// </summary>
        Apolice = 2,
        /// <summary>
        /// 
        /// </summary>
        Outro = 3
    }

    /// <summary>
    /// ModeloEndereco
    /// </summary>
    public enum ModeloEndereco
    {
        /// <summary>
        /// Uma linha com endereço completo
        /// </summary>
        UmaLinha = 1,
        /// <summary>
        /// 
        /// </summary>
        DuasLinhas = 2,
        /// <summary>
        /// 
        /// </summary>
        TresLinhas = 3,
        /// <summary>
        /// Uma linha com endereço e CEP sem cidade e bairro
        /// </summary>
        UmaLinha_End_CEP = 4,
        /// <summary>
        /// Uma linha só com o endereço 
        /// </summary>
        UmaLinha_End = 5,
    }

    /// <summary>
    /// Status do boleto
    /// </summary>
    public enum StatusBoleto
    {
        /// <summary>
        /// Boleto com erro
        /// </summary>
        Erro = -3,
        /// <summary>
        /// Boteto antigo nao classificado
        /// </summary>
        Antigo = -2,
        /// <summary>
        /// Boteto cancelado
        /// </summary>
        Cancelado = -1,
        /// <summary>
        /// Boleto válido
        /// </summary>
        Valido = 0,
        /// <summary>
        /// Este boleto foi dividido - este não é válido
        /// </summary>
        OriginalDividido = 1,
        /// <summary>
        /// Parte do boleto - resultado da divisao (válido)
        /// </summary>
        Parcial = 2,
        /// <summary>
        /// Boleto original incluido em acordo
        /// </summary>
        OriginalAcordo = 3,
        /// <summary>
        /// boleto parcial incluído em acordo
        /// </summary>
        ParcialAcordo = 4,
        /// <summary>
        /// Boleto gerado em acordo
        /// </summary>
        Acordo = 5,
        /// <summary>
        /// Boleto aguardando registro
        /// </summary>
        [Obsolete("status foi substituído pelo StatusRegistro", true)]
        AguardandoRegistro = 6,
        /// <summary>
        /// Boleto aguardando cancelamento do registro
        /// </summary>
        [Obsolete("status inválido", true)]
        AguardandoCancelamentoRegistro = 7,
        /// <summary>
        /// Boleto em produção retido sem registro
        /// </summary>
        [Obsolete("status inválido usar EmProducao", true)]
        EmProducaoSemRegistro = 8,
        /// <summary>
        /// Boleto em produção retido com registro
        /// </summary>
        [Obsolete("status inválido usar EmProducao", true)]
        EmProducaoComRegistro = 9,
        /// <summary>
        /// é um boleto com registro mais não foi registrado (usado com o super)
        /// </summary>
        [Obsolete("status inválido usar equivalente no status do registro", true)]
        ComregistroNaoRegistrado = 10,
        /// <summary>
        /// Boleto em produção que foi dividido. (este é válido)
        /// </summary>
        [Obsolete("status inválido usar EmProducao", true)]
        EmProducaoDivididoSemRegistro = 11,
        /// <summary>
        /// Boleto em produção que foi dividido. (este é válido)
        /// </summary>
        [Obsolete("status inválido usar EmProducao", true)]
        EmProducaoDivididoComRegistro = 12,
        /// <summary>
        /// Boleto ainda em produção - pode ser editado ou dividido
        /// </summary>
        EmProducao = 13,
        /// <summary>
        /// Boleto para adiantamento e pagamento duplicado
        /// </summary>
        Extra = 14,
        /// <summary>
        /// Boleto saldo para incluir em outro boleto
        /// </summary>
        Saldinho = 15,
        /// <summary>
        /// Boleto para acumular no próximo
        /// </summary>
        Acumulado = 16,
    }

    /// <summary>
    /// Define o estado do boleto
    /// </summary>
    public enum Statusparcela
    {
        /// <summary>
        /// Boleto esta pago
        /// </summary>
        Paga,
        /// <summary>
        /// Boleto não vencido
        /// </summary>
        Aberto,
        /// <summary>
        /// Boleto vencido a até 3 dias
        /// </summary>
        AbertoTolerancia3,
        /// <summary>
        /// Boleto vencido a menos de 30 dias
        /// </summary>
        AtrasadoTolerancia30,
        /// <summary>
        /// boleto vencido a mais de 30 dias
        /// </summary>
        Atrasado
    }

    /// <summary>
    /// Tamanhos de fontes para quadro de inadimplência
    /// </summary>
    public enum InaTamanhoFonte
    {
        /// <summary>
        /// Pequena = 6
        /// </summary>
        Pequena = 6,
        /// <summary>
        /// Media = 7 
        /// </summary>
        Media = 7,
        /// <summary>
        /// Grande = 8
        /// </summary>
        Grande = 8
    }

    /// <summary>
    /// Tipo de conta do condomínio para pagamento eletrônico
    /// </summary>
    public enum TipoContaPE
    {
        /// <summary>
        /// Conta Própria no CNPJ do condomínio
        /// </summary>
        Propria = 0,
        /// <summary>
        /// Conta Própria no CNPJ da Neon 
        /// </summary>
        PropriaNeon = 1,
        /// <summary>
        /// Conta Pool no CNPJ da Neon
        /// </summary>
        PoolNeon = 2
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum ARLStatusN
    {
        /// <summary>
        /// 
        /// </summary>
        Nova = -1,
        /// <summary>
        /// 
        /// </summary>
        Baixado = 0,
        /// <summary>
        /// 
        /// </summary>
        CondominioInativo = 1,
        /// <summary>
        /// 
        /// </summary>
        ContaIncorreta = 2,
        /// <summary>
        /// 
        /// </summary>
        ValorIncorreto = 3,
        /// <summary>
        /// 
        /// </summary>
        ErroNoCalculoDoBoleto = 4,
        /// <summary>
        /// 
        /// </summary>
        Duplicidade = 5,
        /// <summary>
        /// 
        /// </summary>
        BoletoNaoEncontrado = 6,
        /// <summary>
        /// 
        /// </summary>
        ContaNaoEncontrada = 7,
        /// <summary>
        /// 
        /// </summary>
        ErronaBaixa = 8,
        /// <summary>
        /// 
        /// </summary>
        BoletoCancelado = 9,
        /// <summary>
        /// 
        /// </summary>
        baixadoMultaAgendada = 10,
        /// <summary>
        /// 
        /// </summary>
        baixadoComMulta = 11,
        /// <summary>
        /// 
        /// </summary>
        CanceladoManualmente = 12,
        /// <summary>
        /// 
        /// </summary>
        CreditoNaoIdentificado = 13,
        /// <summary>
        /// 
        /// </summary>
        DuplicidadeConfirmada = 14,
        /// <summary>
        /// O código de retorno não é tratado
        /// </summary>
        RetornoNaoHomologado = 15,
        /// <summary>
        /// Retorno de rejeição de registro
        /// </summary>
        RegistroRejeitado = 16,
        /// <summary>
        /// Retorno de confirmação de registro
        /// </summary>
        RegistroConfirmado = 17,
        /// <summary>
        /// O registro veio como rejeitado mas ja foi tratado
        /// </summary>
        RegistoRejeitadoJaTratado = 18,
        /// <summary>
        /// Pago em cheque aguardando compensação
        /// </summary>
        PagoEmChequeAguardo = 19,
        /// <summary>
        /// Cheque ok
        /// </summary>
        PagoEmChequeOK = 20,
        /// <summary>
        /// 
        /// </summary>
        ChequeNaoEncontrado = 21,
        /// <summary>
        /// Pagamento extra, pode ser adiantamento ou duplicidade
        /// </summary>
        PagamentoExtra = 22,
        /// <summary>
        /// 
        /// </summary>
        TributoNaoEncontrado = 23,
        /// <summary>
        /// 
        /// </summary>
        BaixadoTarifa = 24,
        /// <summary>
        /// 
        /// </summary>
        SuperBoletoVencido = 25,
        /// <summary>
        /// O Registro foi feito mas o DA foi recusado
        /// </summary>
        DebitoAutomaticoRecusado = 26
    }

    /// <summary>
    /// Tipo de assinatura no corpo diretivo
    /// </summary>
    public enum AssinaCheque
    {
        /// <summary>
        /// 
        /// </summary>
        NaoAssina = 0,
        /// <summary>
        /// 
        /// </summary>
        Assina = 1,
        /// <summary>
        /// 
        /// </summary>
        Conjunto = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum INSSSindico
    {
        /// <summary>
        /// 
        /// </summary>            
        NaoRecolhe = 0,
        /// <summary>
        /// 
        /// </summary>
        ParteCond = 1,
        /// <summary>
        /// 
        /// </summary>
        ParteCondSindico = 2
    }

    /// <summary>
    /// TiposRepass
    /// </summary>
    public enum TiposRepass
    {
        /// <summary>
        /// 
        /// </summary>
        Administrativas = 0,
        /// <summary>
        /// 
        /// </summary>
        Malotes = 1,
        /// <summary>
        /// 
        /// </summary>
        Impressos = 2,
        /// <summary>
        /// 
        /// </summary>
        Assembleias = 3,
        /// <summary>
        /// 
        /// </summary>
        Xerox = 4
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum PBOStatus
    {
        /// <summary>
        /// BODs gerados
        /// </summary>
        Preparado = 0,
        /// <summary>
        /// Bloqueado
        /// </summary>
        Bloqueado = 1,
        /// <summary>
        /// Aguradando registro
        /// </summary>
        Registrando = 2,
        /// <summary>
        /// Não usar
        /// </summary>
        [Obsolete("Status descontinuado, não aguardamos mais o registro para imprimir", true)]
        Imprimir = 3,
        /// <summary>
        /// OK
        /// </summary>
        ok = 4,
        /// <summary>
        /// Aguardando retorno do registro
        /// </summary>
        aguardaRetorno = 5
    }

    /// <summary>
    /// Enumeração para campo TipoLancamentoNeon
    /// </summary>
    public enum TipoLancamentoNeon
    {
        /// <summary>
        /// não definido
        /// </summary>
        NaoIdentificado = 0,
        /// <summary>
        /// 
        /// </summary>
        Cheque = 1,
        /// <summary>
        /// 
        /// </summary>
        DespesasBancarias = 2,
        /// <summary>
        /// 
        /// </summary>
        Estorno = 3,
        /// <summary>
        /// 
        /// </summary>
        PelaDescricao = 4,
        /// <summary>
        /// 
        /// </summary>
        Salarios = 5,
        /// <summary>
        /// 
        /// </summary>
        ChequeDevolvido_Terceiro = 6,
        /// <summary>
        /// 
        /// </summary>
        ChequeDevolvido_Condominio = 7,
        /// <summary>
        /// 
        /// </summary>
        deposito = 8,
        /// <summary>
        /// 
        /// </summary>
        creditodecobranca = 9,
        /// <summary>
        /// 
        /// </summary>
        resgate = 10,
        /// <summary>
        /// 
        /// </summary>
        debitoautomatico = 11,
        /// <summary>
        /// 
        /// </summary>
        rentabilidade = 12,
        /// <summary>
        /// 
        /// </summary>
        Tributo = 13,
        /// <summary>
        /// Pagamento Eletrônico (cheque / doc)
        /// </summary>
        PagamentoEletronico = 14,
        /// <summary>
        /// 
        /// </summary>
        TransferenciaInterna = 15,
        /// <summary>
        /// Soma na conta na aplicação afetando o total e o aplicado
        /// </summary>
        RendimentoContaInvestimento = 16,
        /// <summary>
        /// Credito de cobrança manual
        /// </summary>
        creditodecobrancaManual = 17,
    }

    /// <summary>
    /// Status do orçamento
    /// </summary>
    public enum ORCStatus
    {
        /// <summary>
        /// A princípio não será usado
        /// </summary>
        Cancelado = -1,
        /// <summary>
        /// Fase de cadastramento
        /// </summary>
        Cadastro = 0,
        /// <summary>
        /// Ativo
        /// </summary>
        Ativo = 1,
        /// <summary>
        /// Terminado
        /// </summary>
        Terminado = 2,
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum CCTTipo
    {
        /// <summary>
        /// 
        /// </summary>
        ContaCorrete = 0,
        /// <summary>
        /// 
        /// </summary>
        Poupanca = 1,
        /// <summary>
        /// 
        /// </summary>
        Aplicacao = 2,
        /// <summary>
        /// 
        /// </summary>
        ContaCorrete_com_Oculta = 3,
        /// <summary>
        /// 
        /// </summary>
        ContaCorreteOculta = 4,
        /// <summary>
        /// 
        /// </summary>
        ContaPool = 5,
        /// <summary>
        /// 
        /// </summary>
        ContaDaADM = 6
    }

    /// <summary>
    /// Enumeração para campo statusconsiliado
    /// </summary>        
    public enum statusconsiliado
    {
        /// <summary>
        /// Registro legado - não usar
        /// </summary>
        Antigo = 0,
        /// <summary>
        /// Ainda não tratado
        /// </summary>
        Aguardando = 1,
        /// <summary>
        /// Tratado
        /// </summary>
        Automatico = 2,
        /// <summary>
        /// outra filial
        /// </summary>
        Filial = 3,
        /// <summary>
        /// condomínio inativo
        /// </summary>
        Inativo = 4,
        /// <summary>
        /// Aguarda conta
        /// </summary>
        AguardaConta = 5,
        /// <summary>
        /// Manual
        /// </summary>
        Manual = 6,
        /// <summary>
        /// Depósito
        /// </summary>
        Deposito = 7,
        /// <summary>
        /// Cheque não encontrado
        /// </summary>            
        ChequeNaoEncontrado = 8,
        /// <summary>
        /// Tributo não encontrado
        /// </summary>
        TributoNaoEncontrado = 9
    }

    /// <summary>
    /// Fase do periodico
    /// </summary>
    public enum FasePeriodico
    {
        /// <summary>
        /// 
        /// </summary>
        ComNotaprov,
        /// <summary>
        /// 
        /// </summary>
        AguardaEmissaoCheque,
        /// <summary>
        /// 
        /// </summary>
        CadastrarProxima,
    }

    /// <summary>
    /// 
    /// </summary>
    public enum StatusArquivo
    {
        /// <summary>
        /// 
        /// </summary>
        Digitado = 0,
        /// <summary>
        /// 
        /// </summary>
        Pre_Autorizado = 1,
        /// <summary>
        /// 
        /// </summary>
        Autorizado = 2,
        /// <summary>
        /// 
        /// </summary>
        Aguardando_Retorno = 3,
        /// <summary>
        /// 
        /// </summary>
        Inconsistente = 4,
        /// <summary>
        /// 
        /// </summary>
        Agendado = 5,
        /// <summary>
        /// 
        /// </summary>
        Com_operacao = 6,
        /// <summary>
        /// 
        /// </summary>
        Efetuado = 11,
        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
        /// <summary>
        /// 
        /// </summary>
        Confirmado_Banco = 17,
        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
        /// <summary>
        /// 
        /// </summary>
        Nao_Efetuado = 12,
        /// <summary>
        /// 
        /// </summary>
        Excluido = 13,
        /// <summary>
        /// 
        /// </summary>
        A_Agendar = 16,
        /// <summary>
        /// 
        /// </summary>
        ParaEnviar = 100,
        /// <summary>
        /// 
        /// </summary>
        Liberar_Gerente = 101,
        /// <summary>
        /// 
        /// </summary>
        Liberar_Gerencia = 102,
        /// <summary>
        /// 
        /// </summary>
        Liberar = 103,
        /// <summary>
        /// 
        /// </summary>
        VirouCheque = 104,
        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
        /// <summary>
        /// 
        /// </summary>
        Aguardando_Envio = 105,
        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
        //*** MRC - INICIO - TRIBUTOS (18/08/2015 10:00) ***
        /// <summary>
        /// 
        /// </summary>
        Aguardando_Envio_Exclusao = 106,
        /// <summary>
        /// 
        /// </summary>
        Aguardando_Retorno_Exclusao = 107
        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
    }

    /// <summary>
    /// Enumeração para processamento de cancelamento de pagamento eletrônico 
    /// </summary>
    public enum PECancelamentoStatus
    {
        /// <summary>
        /// Cancelamento processado
        /// </summary>
        ProcessadoCancelamentoPE = 0,
        /// <summary>
        /// Pagamento eletrônico cancelado e aguardando o envio de arquivo para o banco pelo processo automatico
        /// </summary>
        AguardaEnvioCancelamentoPE = 1,
        /// <summary>
        /// Cancelamento de Pagamento eletrônico enviado e aguardando confirmação de arquivo recebido pelo banco
        /// </summary>
        AguardaConfirmacaoCancelamentoBancoPE = 2,
    }

    /// <summary>
    /// TipoPagPeriodico
    /// </summary>
    public enum TipoPagPeriodico
    {
        /// <summary>
        /// Cheque em papel
        /// </summary>
        Cheque = 0,
        /// <summary>
        /// Cheque síndico
        /// </summary>
        ChequeSindico = 1,
        /// <summary>
        /// Cheque para fazer depósito
        /// </summary>
        ChequeDeposito = 2,
        /// <summary>
        /// Cheque para pagar boleto
        /// </summary>
        Boleto = 3,
        /// <summary>
        /// Débito automático
        /// </summary>
        DebitoAutomatico = 4,
        /// <summary>
        /// Débito automatico com conta
        /// </summary>
        DebitoAutomaticoConta = 5,
        /// <summary>
        /// Conta
        /// </summary>
        Conta = 6,
        /// <summary>
        /// Pis - Folha
        /// </summary>
        FolhaPIS = 7,
        /// <summary>
        /// FGTS
        /// </summary>
        FolhaFGTS = 8,
        /// <summary>
        /// FGTS eletrônico
        /// </summary>
        Folha = 9,
        /// <summary>
        /// Folha - cheque
        /// </summary>
        FolhaPISEletronico = 10,
        /// <summary>
        /// Pis - Folha Eletrônico
        /// </summary>
        FolhaFGTSEletronico = 11,
        /// <summary>
        /// Folha - eletrônico
        /// </summary>
        FolhaEletronico = 12,
        /// <summary>
        /// IR - Folha
        /// </summary>
        FolhaIR = 13,
        /// <summary>
        /// IR - Folha Eletrônico
        /// </summary>
        FolhaIREletronico = 14,
        /// <summary>
        /// Crédito em conta corrente ou poupanca - Pagamento Eletrônico
        /// </summary>
        PECreditoConta = 20,
        /// <summary>
        /// Transferência (DOC ou TED) - Pagamento Eletrônico
        /// </summary>
        PETransferencia = 21,
        /// <summary>
        /// Ordem de Pagamento - Pagamento Eletrônico
        /// </summary>
        PEOrdemPagamento = 22,
        /// <summary>
        /// Titulo Bancário (Boleto) - Pagamento Eletrônico
        /// </summary>
        PETituloBancario = 23,
        //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
        /// <summary>
        /// Titulo Bancário Agrupável (Várias notas, um único boleto) - Pagamento Eletrônico
        /// </summary>
        PETituloBancarioAgrupavel = 24,
        //*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) ***
        /// <summary>
        /// Adiantamento de salários
        /// </summary>
        Adiantamento = 25,
    }

    /// <summary>
    /// Staus dos itens da agenda
    /// </summary>        
    public enum AGBStatus
    {
        /// <summary>
        /// 
        /// </summary>        
        Agendado = 0,
        /// <summary>
        /// 
        /// </summary>
        Aviso = 1,
        /// <summary>
        /// 
        /// </summary>
        Atraso = 2,
        /// <summary>
        /// 
        /// </summary>
        Vencido = 3,
        /// <summary>
        /// ok
        /// </summary>
        Ok = 4
    }

    /// <summary>
    /// Tipos de agenda
    /// </summary>
    public enum TiposTAG
    {
        /// <summary>
        /// MANDATO_DO_SINDICO
        /// </summary>
        MANDATO_DO_SINDICO = 3,
        /// <summary>
        /// 
        /// </summary>
        BOLETOS_CONTRATOS = 5,
        /// <summary>
        /// 
        /// </summary>
        Contas = 6,
        /// <summary>
        /// 
        /// </summary>
        Contratos = 10,
        /// <summary>
        /// 
        /// </summary>
        Debito_Automatico = 11
    }

    /// <summary>
    /// Tipos de Pagamento (Todos)
    /// </summary>
    public enum PAGTipo
    {
        /// <summary>
        /// Cheque a ser retirado na Neon
        /// </summary>
        cheque = 0,
        /// <summary>
        /// O sindigo efetua o pagamento
        /// </summary>
        sindico = 1,
        /// <summary>
        /// Boleto bancário
        /// </summary>
        boleto = 2,
        /// <summary>
        /// Será feito depósito
        /// </summary>
        deposito = 3,
        /// <summary>
        /// Guia de Recolhimento (Cheque)
        /// </summary>
        Guia = 4,
        /// <summary>
        /// Honorarios
        /// </summary>
        Honorarios = 5,
        /// <summary>
        /// Guia Eletrônica Folha
        /// </summary>
        GuiaEletronica = 6,
        /// <summary>
        /// Caixinha
        /// </summary>
        Caixinha = 7,
        /// <summary>
        /// Debito automático (não emite cheque)
        /// </summary>
        DebitoAutomatico = 8,
        /// <summary>
        /// Conta (emite cheque)
        /// </summary>
        Conta = 9,
        /// <summary>
        /// Conta com valor para acumular
        /// </summary>
        Acumular = 10,
        /// <summary>
        /// Crédito em conta corrente ou poupanca - Pagamento Eletrônico
        /// </summary>
        PECreditoConta = 20,
        /// <summary>
        /// Transferência (DOC ou TED) - Pagamento Eletrônico
        /// </summary>
        PETransferencia = 21,
        /// <summary>
        /// Ordem de Pagamento - Pagamento Eletrônico
        /// </summary>
        PEOrdemPagamento = 22,
        /// <summary>
        /// Titulo Bancário (Boleto) - Pagamento Eletrônico
        /// </summary>
        PETituloBancario = 23,
        //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
        /// <summary>
        /// Titulo Bancário Agrupável (Várias notas, um único boleto) - Pagamento Eletrônico
        /// </summary>
        PETituloBancarioAgrupavel = 24,
        /// <summary>
        /// Trasnferencia física de valor arrecadado
        /// </summary>
        TrafArrecadado = 25,
        /// <summary>
        /// Cheque emitido direto pelo síndico
        /// </summary>
        ChequeSindico = 26,
        /// <summary>
        /// Guia de Recolhimento (Pagamento Eletrônico)
        /// </summary>
        PEGuia = 27,
        /// <summary>
        /// Folha de pagamento (pode virar cheque)
        /// </summary>
        Folha = 28,
        /// <summary>
        /// Pagamento de conta de consumo. Obs: é tratado como tributos como o PEGuia
        /// </summary>
        PEContaConsumo = 29
    }

    /// <summary>
    /// 
    /// </summary>
    public enum NOAStatus
    {
        /// <summary>
        /// 
        /// </summary>
        NaoEncontrada = -1,
        /// <summary>
        /// 
        /// </summary>
        Cadastrada = 0,
        /// <summary>
        /// 
        /// </summary>
        Parcial = 1,
        /// <summary>
        /// 
        /// </summary>
        Liquidada = 2,
        /// <summary>
        /// 
        /// </summary>
        SemPagamentos = 3,
        /// <summary>
        /// Nota cancelada
        /// </summary>
        NotaCancelada = 4,
        /// <summary>
        /// A nota real ainda não chegou
        /// </summary>
        NotaProvisoria = 5,
        /// <summary>
        /// Nota com pagamento acumulado na próxima
        /// </summary>
        NotaAcumulada = 6,
        /// <summary>
        /// Nota com pagamento acumulado na próxima com valor a ajustar
        /// </summary>
        CadastradaAjustarValor = 7,
    }

    /// <summary>
    /// Tipos de nota
    /// </summary>
    public enum NOATipo
    {
        /// <summary>
        /// Avulsa
        /// </summary>
        Avulsa = 0,
        /// <summary>
        /// Da folha de pagamentos
        /// </summary>
        Folha = 1,
        /// <summary>
        /// Importado de arquiovo XML
        /// </summary>
        ImportacaoXML = 2,
        /// <summary>
        /// Gerado pelo Pagamento Periódico
        /// </summary>
        PagamentoPeriodico = 3,
        /// <summary>
        /// Repasse
        /// </summary>
        Repasse = 4,
        /// <summary>
        /// 
        /// </summary>
        NotaParcial = 5,
        /// <summary>
        /// 
        /// </summary>
        Recibo = 6,
        /// <summary>
        /// 
        /// </summary>
        ReciboParcial = 7,
        /// <summary>
        /// 
        /// </summary>
        TransferenciaF = 8
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum CHEStatus
    {
        /// <summary>
        /// 
        /// </summary>
        Antigo = -2,
        /// <summary>
        /// 
        /// </summary>
        NaoEncontrado = -1,
        /// <summary>
        /// 
        /// </summary>
        Cancelado = 0,
        /// <summary>
        /// 
        /// </summary>
        Cadastrado = 1,
        /// <summary>
        /// 
        /// </summary>
        AguardaRetorno = 2,
        /// <summary>
        /// 
        /// </summary>
        Aguardacopia = 3,
        /// <summary>
        /// 
        /// </summary>
        Retornado = 4,
        /// <summary>
        /// 
        /// </summary>
        EnviadoBanco = 5,
        /// <summary>
        /// 
        /// </summary>
        Retirado = 6,
        /// <summary>
        /// 
        /// </summary>
        Compensado = 7,
        /// <summary>
        /// 
        /// </summary>
        CompensadoAguardaCopia = 8,
        /// <summary>
        /// 
        /// </summary>
        PagamentoCancelado = 9,
        /// <summary>
        /// 
        /// </summary>
        DebitoAutomatico = 10,
        /// <summary>
        /// 
        /// </summary>
        Caixinha = 11,
        /// <summary>
        /// 
        /// </summary>
        Eletronico = 12,
        /// <summary>
        /// 
        /// </summary>
        ComSindico = 13,
        /// <summary>
        /// 
        /// </summary>
        BaixaManual = 14,
        //*** MRC - INICIO - PAG-FOR (2) ***    
        /// <summary>
        /// Pagamento eletrônico emitido e aguardando o envio de arquivo para o banco pelo processo automatico
        /// </summary>
        AguardaEnvioPE = 15,
        /// <summary>
        /// Pagamento eletrônico enviado e aguardando confirmação de arquivo recebido pelo banco
        /// </summary>
        AguardaConfirmacaoBancoPE = 16,
        /// <summary>
        /// Pagamento eletrônico efetuado e confirmado pelo banco (antes de compensar no processo de baixa)
        /// </summary>
        PagamentoConfirmadoBancoPE = 17,
        /// <summary>
        /// Pagamento eletrônico excluído do site pelo síndico (não aprovado)
        /// </summary>
        PagamentoNaoAprovadoSindicoPE = 18,
        //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
        /// <summary>
        /// Pagamento eletrônico com inconsistência
        /// </summary>
        PagamentoInconsistentePE = 19,
        //*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) ***
        //*** MRC - TERMINO - PAG-FOR (2) ***
        /// <summary>
        /// O cheque não pode ser emitido ainda.
        /// </summary>
        CadastradoBloqueado = 20,
        /// <summary>
        /// O banco não efetivou o pagamento por falta de saldo ou coisa do tipo
        /// </summary>
        PagamentoNaoEfetivadoPE = 21,
    }

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum CONStatus
    {
        /// <summary>
        /// Condominio que saiu
        /// </summary>
        Inativo = 0,
        /// <summary>
        /// Condominio ativo
        /// </summary>
        Ativado = 1,
        /// <summary>
        /// Desativado para edição
        /// </summary>
        Desativado = 2,
        /// <summary>
        /// Condominio sendo cadastrado
        /// </summary>
        Implantacao = 3
    }

    /// <summary>
    /// Planos Padrao
    /// </summary>
    public enum PLAPadrao
    {
        /// <summary>
        /// nao definido
        /// </summary>
        NaoDefinido = 0,
        /// <summary>
        /// Acordo
        /// </summary>
        acordo = 111000,
        /// <summary>
        /// Condominio
        /// </summary>
        Condominio = 101000,
        /// <summary>
        /// Fundo de reserva
        /// </summary>
        FundoReserva = 110000,
        /// <summary>
        /// honorarios
        /// </summary>
        honorarios = 119000,
        /// <summary>
        /// Multa por atraso
        /// </summary>
        MultasAtraso = 120001,
        /// <summary>
        /// seguro conteudo credito
        /// </summary>
        segurocosnteudoC = 910001,
        /// <summary>
        /// seguro conteudo débito
        /// </summary>
        segurocosnteudoD = 920001,
        /// <summary>
        /// Crédito Antecipado
        /// </summary>
        PagamentoExtraC = 910002,
        /// <summary>
        /// Desconto Crédito Antecipado
        /// </summary>
        PagamentoExtraD = 920002,
        /// <summary>
        /// Cobrança ativa credito
        /// </summary>
        CobrancaAtivaC = 910003,
        /// <summary>
        /// Cobrança ativa débito
        /// </summary>
        CobrancaAtivaD = 920003,
        /// <summary>
        /// Referência a outro boleto
        /// </summary>
        Saldinho = 800001,
        /// <summary>
        /// Credito na identificado
        /// </summary>
        CreditoNaoIdentificado = 198000,
        /// <summary>
        /// Taxa no faturamento
        /// </summary>
        TaxaDeADM = 230001,
        /// <summary>
        /// Repasse
        /// </summary>
        RepasseDespAdmin = 240000,
        /// <summary>
        /// Repasse
        /// </summary>
        RepasseImpressos = 240007,
        /// <summary>
        /// Repasse
        /// </summary>
        RepasseConducao = 240004,
        /// <summary>
        /// Repasse
        /// </summary>
        RepasseCopias = 240001
    }

    /// <summary>
    /// Tipos de código de barras
    /// </summary>
    public enum TipoCodigoBarras
    {
        /// <summary>
        /// Cópia de cheque
        /// </summary>
        Cheque = 1,
        /// <summary>
        /// Cópia de cheque - Segunda Via
        /// </summary>
        ChequeSegundaVia = 91,
        /// <summary>
        /// Holerite
        /// </summary>
        Holerite = 2,
        /// <summary>
        /// Balancete
        /// </summary>
        Balancete = 11,
    }

    /// <summary>
    /// Enumeração dos ramos de atividades do sistema
    /// </summary>
    public enum RAVPadrao
    {
        /// <summary>
        /// Escritório de advocacia
        /// </summary>
        EscritorioAdvogado = -1,
        /// <summary>
        /// Imobiliária locação
        /// </summary>
        Imobiliaria = -2,
        /// <summary>
        /// Seguradora
        /// </summary>
        Seguradora = -3,
        /// <summary>
        /// Corretora
        /// </summary>
        Corretora = -4,
        /// <summary>
        /// Cartório
        /// </summary>
        Cartorio = -5,
        /// <summary>
        /// 
        /// </summary>
        ContaConsumo = -6
    }



    /// <summary>
    /// Tipos de unidade
    /// </summary>
    public enum APTTipo
    {
        /// <summary>
        /// Inativa
        /// </summary>
        Inativa = 0,
        /// <summary>
        /// Apartamento
        /// </summary>
        Apartamento = 1,
        /// <summary>
        /// Casa
        /// </summary>
        Casa = 2,
        /// <summary>
        /// Vaga de garagem
        /// </summary>
        Vaga = 3,
        /// <summary>
        /// Box
        /// </summary>
        Box = 4,
        /// <summary>
        /// Apartamento do zelador
        /// </summary>
        Zelador = 5,
        /// <summary>
        /// Unidade virtual como do Síndico profissional
        /// </summary>
        Virtual = 6,
        /// <summary>
        /// Unidade pertencente ao condomínio
        /// </summary>
        DoCondominio = 7,
        /// <summary>
        /// Unidade da Construtora
        /// </summary>
        DaConstrutora = 8,
        /// <summary>
        /// Locação
        /// </summary>
        Locacao = 9,
    }

    /// <summary>
    /// Tipos de registro no log
    /// </summary>
    public enum LLOTipo
    {
        /// <summary>
        /// Registro que perde a finalidade com o tempo e pode ser apagado
        /// </summary>
        Temporario = 0,
        /// <summary>
        /// Registro feito pelo usuário
        /// </summary>
        Usuario = 1,
        /// <summary>
        /// Registro feito pelo sistema
        /// </summary>
        Sistema = 2,
        /// <summary>
        /// Para efeito de log, não mostra ao usuário
        /// </summary>
        Log = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum TiposAPTDOC
    {
        /// <summary>
        /// 
        /// </summary>
        escritura = 0,
        /// <summary>
        /// 
        /// </summary>
        advertencia = 1,
        /// <summary>
        /// 
        /// </summary>
        outros = 2,
        /// <summary>
        /// Documento assinado autorizando o débito automático
        /// </summary>
        debito_automatico = 3,
    }

    /// <summary>
    /// Tabela que usa o log
    /// </summary>
    public enum LLOxXXX
    {
        /// <summary>
        /// Linha de cobrança
        /// </summary>
        LIC
    }

    /// <summary>
    /// Objeto estático para converesão de códigos PLA
    /// </summary>
    public static class PadraoPLA
    {
        /// <summary>
        /// Código PLA
        /// </summary>
        /// <param name="PLA"></param>
        /// <returns></returns>
        public static string PLAPadraoCodigo(PLAPadrao PLA)
        {
            return string.Format("{0:000000}", (int)PLA);
        }
    }



    /// <summary>
    /// Coleções de tipos
    /// </summary>
    public class Colecoes
    {
        private static List<PLAPadrao> _PLAsRepasse;

        /// <summary>
        /// Lista de PLAs de repasse
        /// </summary>
        public static List<PLAPadrao> PLAsRepasse { get => _PLAsRepasse ?? (_PLAsRepasse = new List<PLAPadrao> { PLAPadrao.RepasseConducao, PLAPadrao.RepasseCopias, PLAPadrao.RepasseDespAdmin, PLAPadrao.RepasseImpressos }); }  
        

        private static List<StatusArquivo> _StatusArquivoCancelaveisBanco;

        /// <summary>
        /// 
        /// </summary>
        public static List<StatusArquivo> StatusArquivoCancelaveisBanco
        {
            get
            {
                if (_StatusArquivoCancelaveisBanco == null)
                {
                    _StatusArquivoCancelaveisBanco = new List<StatusArquivo>();
                    _StatusArquivoCancelaveisBanco.Add(StatusArquivo.Agendado);
                };
                return _StatusArquivoCancelaveisBanco;
            }
        }

        private static List<int> _CodigosRAVReservados;

        /// <summary>
        /// 
        /// </summary>
        public static List<int> CodigosRAVReservados
        {
            get
            {
                if (_CodigosRAVReservados == null)
                {
                    _CodigosRAVReservados = new List<int>();
                    foreach (int Valor in Enum.GetValues(typeof(RAVPadrao)))
                        _CodigosRAVReservados.Add(Valor);
                };
                return _CodigosRAVReservados;
            }
        }
    }
}
