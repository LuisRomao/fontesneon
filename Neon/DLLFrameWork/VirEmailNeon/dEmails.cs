﻿namespace VirEmailNeon {
    
    
    public partial class dEmails {
        private dEmailsTableAdapters.EMaiLTableAdapter eMaiLTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EMaiL
        /// </summary>
        public dEmailsTableAdapters.EMaiLTableAdapter EMaiLTableAdapter
        {
            get
            {
                if (eMaiLTableAdapter == null)
                {
                    eMaiLTableAdapter = new dEmailsTableAdapters.EMaiLTableAdapter();
                    eMaiLTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.EmailREt);
                };
                return eMaiLTableAdapter;
            }
        }

        private dEmailsTableAdapters.EMailAnexoTableAdapter eMailAnexoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EMailAnexo
        /// </summary>
        public dEmailsTableAdapters.EMailAnexoTableAdapter EMailAnexoTableAdapter
        {
            get
            {
                //string geraerro = "X";
                //geraerro = geraerro.Substring(4, 4);
                if (eMailAnexoTableAdapter == null)
                {
                    eMailAnexoTableAdapter = new dEmailsTableAdapters.EMailAnexoTableAdapter();
                    eMailAnexoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.EmailREt);
                };
                return eMailAnexoTableAdapter;
            }
        }
    }
}
