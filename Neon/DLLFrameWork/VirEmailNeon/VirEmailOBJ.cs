﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace VirEmailNeon
{
    /// <summary>
    /// Objeto serializavel que contem um e-mail
    /// </summary>
    [Serializable()] 
    internal class VirEmailOBJ : ISerializable 
    {
        internal string Destinatario;
        internal string ConteudoEmail;
        internal string Subject;
        internal int? BOL;
        internal Attachment[] Anexos;

        private string[] NAnexo;
        
        private byte[][] bAnexo
        {
            get 
            {
                byte[][]  retorno = new byte[Anexos.Length][];
                for (int i = 0; i < Anexos.Length; i++)
                {
                    retorno[i] = new byte[Anexos[i].ContentStream.Length];
                    Anexos[i].ContentStream.Read(retorno[i], 0, (int)Anexos[i].ContentStream.Length);
                } 
                return retorno;
            }
            set
            {
                Anexos = new Attachment[value.Length];
                for (int i = 0; i < value.Length; i++)
                {
                    MemoryStream MS = new MemoryStream(value[i]);
                    Anexos[i] = new Attachment(MS, NAnexo[i]);
                }
            }
        }

        internal string ArquivoBIN;

        #region Serialização
        /// <summary>
        /// Metodo GetObjectData para serializar 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="ctxt"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("Destinatario", Destinatario);
            info.AddValue("ConteudoEmail", ConteudoEmail);
            info.AddValue("Subject", Subject);
            info.AddValue("BOL", BOL);
            info.AddValue("bAnexo", bAnexo);
            info.AddValue("NAnexo", NAnexo);
        }

        /// <summary>
        /// Construtor para Deserialize
        /// </summary>
        /// <param name="info"></param>
        /// <param name="ctxt"></param>
        public VirEmailOBJ(SerializationInfo info, StreamingContext ctxt)
        {
            Destinatario = (string)info.GetValue("Destinatario", typeof(string));
            ConteudoEmail = (string)info.GetValue("ConteudoEmail", typeof(string));
            Subject = (string)info.GetValue("Subject", typeof(string));
            BOL = (int?)info.GetValue("BOL", typeof(int?));
            NAnexo = (string[])info.GetValue("NAnexo", typeof(string[]));
            bAnexo = (byte[][])info.GetValue("bAnexo", typeof(byte[][]));
        } 
        #endregion

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Destinatario"></param>
        /// <param name="_ConteudoEmail"></param>
        /// <param name="_Subject"></param>
        /// <param name="_BOL"></param>
        /// <param name="_Anexos"></param>
        public VirEmailOBJ(string _Destinatario, string _ConteudoEmail, string _Subject, int? _BOL, params Attachment[] _Anexos)
        {
            Destinatario = _Destinatario;
            ConteudoEmail = _ConteudoEmail;
            Subject = _Subject;
            BOL = _BOL;
            Anexos = _Anexos;            
            NAnexo = new string[Anexos.Length];
            for (int i = 0; i < Anexos.Length; i++)            
                NAnexo[i] = Anexos[i].Name;             
        }

        private static string _CaminhoLocal;
        private static string CaminhoLocal
        {
            get
            {
                if (_CaminhoLocal == null)
                {
                    _CaminhoLocal = string.Format("{0}\\EML", Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")));
                    if (!Directory.Exists(_CaminhoLocal))
                        Directory.CreateDirectory(_CaminhoLocal);        
            
                };
                return _CaminhoLocal;
            }
        }        

        /// <summary>
        /// Salva em formato bin
        /// </summary>
        /// <returns></returns>
        public string SalvarBIN()
        {
            string NomeBase = string.Format("{0:yyyy_MM_dd_HH_mm_ss_fff_}", DateTime.Now);           
            for (int i = 0; ;i++ )
            {
                ArquivoBIN = string.Format("{0}\\{1}{2}.bin", CaminhoLocal, NomeBase, i);
                if (!File.Exists(ArquivoBIN))
                    break;
            }            
            
            BinaryFormatter bformatter = new BinaryFormatter();
            using (FileStream file1 = new FileStream(ArquivoBIN, FileMode.Create))
            {
                bformatter.Serialize(file1, this);
            }

            return ArquivoBIN;
        }

        /// <summary>
        /// Apaga o arquivo retido
        /// </summary>
        public void ApagarArquivo()
        {
            if (File.Exists(ArquivoBIN))
                File.Delete(ArquivoBIN);
        }

        /// <summary>
        /// Recupera um email gravado
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public static VirEmailOBJ Recupera(string NomeArquivo = null)
        {
            if (NomeArquivo == null)
            {
                string[] Candidatos = Directory.GetFiles(CaminhoLocal);
                if (Candidatos.Length == 0)
                    return null;
                NomeArquivo = Candidatos[0];
            }
            using (FileStream stream = File.Open(NomeArquivo, FileMode.Open))
            {
                BinaryFormatter bformatter = new BinaryFormatter();

                VirEmailOBJ retorno = (VirEmailOBJ)bformatter.Deserialize(stream);
                stream.Close();
                retorno.ArquivoBIN = NomeArquivo;
                return retorno;
            };
        }

        internal static int nPendentes;

        /// <summary>
        /// Recupera vários emails
        /// </summary>
        /// <param name="maximo"></param>
        /// <returns></returns>
        public static List<VirEmailOBJ> Recuperas(int? maximo = null)
        {
            //string[] Candidatos = Directory.GetFiles(CaminhoLocal,"*.bin");
            List<string> Candidatos = new List<string>(Directory.GetFiles(CaminhoLocal, "*.bin"));
            Candidatos.Sort();
            nPendentes = Candidatos.Count;
            int nretornar = Candidatos.Count;
            if (maximo.HasValue && (maximo.Value < nretornar))
                nretornar = maximo.Value;
            List<VirEmailOBJ> retorno = new List<VirEmailOBJ>();
            for (int i = 0; i < nretornar; i++)
                try
                {
                    retorno.Add(Recupera(Candidatos[i]));
                }
                catch 
                {
                    string Informacoes = string.Format("Arquivo zerado\r\n:UserName:{0}\r\nMachineName:{1}\r\nArquivo{2}", System.Environment.UserName, System.Environment.MachineName, Candidatos[i]);
                    if (EmailDiretoNeon.EmalST.EnviarArquivos("luis@virweb.com.br", Informacoes, "Arquivo zerado", Candidatos[i]))
                        File.Delete(Candidatos[i]);
                }
            return retorno;
        }
    }
}
