/*
05/05/2014 LH - 13.2.8.47 - Grava��o do campo BOL na tabela de e-mails
*/

using System;
using System.Collections.Generic;
using System.Net.Mail;
using VirEmail;
using System.Threading;
using System.Text;


namespace VirEmailNeon
{
    /// <summary>
    /// Componete para envio de e.mals direto ao smtp (neon)
    /// </summary>
    public class EmailDiretoNeon : EmailDireto
    {
        /// <summary>
        /// Consulta o usu�rio
        /// </summary>
        /// <param name="Destinatario"></param>
        /// <param name="DestinatarioDefault"></param>
        /// <returns></returns>
        protected override bool ConsultaUsuario(ref string Destinatario, string DestinatarioDefault)
        {                
            Destinatario = DestinatarioDefault;
            if (!VirInput.Input.Execute("E.mail:", ref Destinatario))
            {
                Destinatario = DestinatarioDefault = "";
                return false;
            }
            else
                return true;        
        }

        private static EmailDiretoNeon _EmalSTSemRetorno;

        private static EmailDiretoNeon _EmalST;

        /// <summary>
        /// Inst�ncia est�tica
        /// </summary>
        public static EmailDiretoNeon EmalST
        {
            get
            {
                if (_EmalST == null)
                {
                    _EmalST = new EmailDiretoNeon();
                    _EmalST.Ativar();
                }
                return _EmalST;
            }
        }

        /// <summary>
        /// Componente est�tico para envio com caixa sem retorno
        /// </summary>
        public static EmailDiretoNeon EmalSTSemRetorno
        {
            get
            {
                if (_EmalSTSemRetorno == null)
                {
                    _EmalSTSemRetorno = new EmailDiretoNeon();
                    _EmalSTSemRetorno.Remetente = "Boletos@neonimoveis.com.br";
                }
                return _EmalSTSemRetorno;
            }
        }

        /// <summary>
        /// Coloca em teste (servidor locaweb)
        /// </summary>
        public void EmTeste()
        {
            GerarLog = true;
            GravaLog("EM TESTE");
            //this.SMTP = "smtp.virweb.com.br";
            //this.Usuario = "luis@virweb.com.br";
            //this.Senha = "natalivenus42";
            //this.Remetente = "luis@virweb.com.br";
            this.Copia = "virtual@virweb.com.br";
        }

        /// <summary>
        /// Construtor, j� ajusta os variaves SMTP,Usuario,Senha e Rementente
        /// </summary>
        public EmailDiretoNeon()
        {
            //this.SMTP = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.ServidorSMTP;
            //this.Usuario = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.EmailNetCredenciais.UserName;
            //this.Senha = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.EmailNetCredenciais.Password;
            //this.Remetente = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.EmailRemetente;            
            //Porta = 587;
        }

        /// <summary>
        /// Gera um log das atividades
        /// </summary>
        public bool GerarLog = true;

        private StringBuilder log;

        /// <summary>
        /// Formas de envio
        /// </summary>
        public enum FormaEnvio
        {
            /// <summary>
            /// 
            /// </summary>
            Direto,
            /// <summary>
            /// 
            /// </summary>
            ViaServidor,
            /// <summary>
            /// 
            /// </summary>
            Retidos
        }

        /*
        public class virEmailArquivo
        {
            public string Destinatario="1";
            public string ConteudoEmail = "2";
            public string Subject = "3";
            public int? BOL = 4;
            //private Attachment At;
            public byte[][] bAnexo;
            //public Attachment[] datas = new Attachment[2];

            public virEmailArquivo()
            {
                bAnexo = new byte[3][];
                for (int i = 0; i < 3; i++)
                {
                    Attachment Anexo = new Attachment(@"d:\XML\PPFI15052014.pdf");
                    bAnexo[i] = new byte[Anexo.ContentStream.Length];
                    //Anexo.ContentStream.Write(bAnexo, 0, (int)Anexo.ContentStream.Length);
                    Anexo.ContentStream.Read(bAnexo[i], 0, (int)Anexo.ContentStream.Length);
                }                
            }

            public void SalvarXML(string NomeArquivo)
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(virEmailArquivo));
                System.IO.StreamWriter file = new System.IO.StreamWriter(string.Format("{0}.xml",NomeArquivo));
                serializer.Serialize(file, this);                
            }
            
        }
        */

        /*
        public static void Teste()
        {
            Attachment[] Anexos = new Attachment[] { new Attachment(@"d:\XML\PPFI15052014.pdf"), new Attachment(@"d:\XML\PPFI15052014.pdf"), new Attachment(@"d:\XML\PPFI15052014.pdf") };
            for (int i = 0; i < 10; i++)
                EmalST.Enviar(FormaEnvio.ViaServidor, "luis@virweb.com.br", "ViaServidor", "Assunto via servidor " + i.ToString(), Anexos);            
        }*/

        private bool Reter(string Destinatario, string ConteudoEmail, string Subject, int? BOL, params Attachment[] Anexos)
        {
            try
            {
                VirEmailOBJ EmalReter = new VirEmailOBJ(Destinatario, ConteudoEmail, Subject, BOL, Anexos);
                EmalReter.SalvarBIN();
                if (!Afogado)
                    GravaLog("Reter libera loop de espera");
                Afogado = true;
            }
            catch (Exception e)
            {
                UltimoErro = e;
                return false;
            };
            return true;

            /*
            try
            {
                dEmails dEmails1 = new dEmails();
                VirMSSQL.TableAdapter.AbreTrasacaoSQL(VirDB.Bancovirtual.TiposDeBanco.EmailREt,"EmaiDiretoNeon Reter 94",dEmails1.EMaiLTableAdapter, dEmails1.EMailAnexoTableAdapter);
                dEmails.EMaiLRow EMLrow = dEmails1.EMaiL.NewEMaiLRow();
                EMLrow.EMLConteudo = ConteudoEmail;
                EMLrow.EMLSubject = Subject;
                EMLrow.EMLTo = Destinatario;
                if (BOL.HasValue)
                    EMLrow.EML_BOL = BOL.Value;
                dEmails1.EMaiL.AddEMaiLRow(EMLrow);
                dEmails1.EMaiLTableAdapter.Update(EMLrow);
                EMLrow.AcceptChanges();
                foreach (Attachment Anexo in datas)
                {
                    dEmails.EMailAnexoRow EMArow = dEmails1.EMailAnexo.NewEMailAnexoRow();
                    EMArow.EMA_EML = EMLrow.EML;
                    byte[] bAnexo = new byte[Anexo.ContentStream.Length];
                    //Anexo.ContentStream.Write(bAnexo, 0, (int)Anexo.ContentStream.Length);
                    Anexo.ContentStream.Read(bAnexo, 0, (int)Anexo.ContentStream.Length);
                    EMArow.EMAConteudo = bAnexo;
                    EMArow.EMANome = Anexo.Name;
                    dEmails1.EMailAnexo.AddEMailAnexoRow(EMArow);
                }
                dEmails1.EMailAnexoTableAdapter.Update(dEmails1.EMailAnexo);
                VirMSSQL.TableAdapter.CommitSQL(VirDB.Bancovirtual.TiposDeBanco.EmailREt);
            }
            catch (Exception e)
            {
                UltimoErro = e;
                VirMSSQL.TableAdapter.VircatchSQL(e,VirDB.Bancovirtual.TiposDeBanco.EmailREt);
                return false;
            }
            */

        }

        #region Enviar
        /// <summary>
        /// Envia E-mail
        /// </summary>
        /// <param name="Forma"></param>
        /// <param name="Destinatario"></param>
        /// <param name="ConteudoEmail"></param>
        /// <param name="Subject"></param>
        /// <param name="datas"></param>
        /// <returns></returns>
        public bool Enviar(FormaEnvio Forma, string Destinatario, string ConteudoEmail, string Subject, params Attachment[] datas)
        {
            return Enviar(Forma, Destinatario, ConteudoEmail, Subject, null, datas);
        }

        /// <summary>
        /// Envia E-mail
        /// </summary>
        /// <param name="Forma"></param>
        /// <param name="Destinatario"></param>
        /// <param name="ConteudoEmail"></param>
        /// <param name="Subject"></param>
        /// <param name="BOL"></param>
        /// <param name="datas"></param>
        /// <returns></returns>
        public bool Enviar(FormaEnvio Forma, string Destinatario, string ConteudoEmail, string Subject, int? BOL, params Attachment[] datas)
        {
            switch (Forma)
            {
                case FormaEnvio.ViaServidor:
                    return Reter(Destinatario, ConteudoEmail, Subject, null, datas);
                case FormaEnvio.Direto:
                    if (_Enviar(true, Destinatario, ConteudoEmail, Subject, datas))
                        return true;
                    else
                    {
                        if ((Destinatario == null) || (Destinatario == ""))
                            Destinatario = UltimoEndereco;
                        if ((Destinatario != null) && (Destinatario != ""))
                            Reter(Destinatario, ConteudoEmail, Subject, null, datas);
                        return false;
                    }
                case FormaEnvio.Retidos:
                    if (Destinatario == "")
                        return true;
                    else
                        return _Enviar(false, Destinatario, ConteudoEmail, Subject, datas);
            }
            return false;
        }

        /// <summary>
        /// Envia e-mail Direto
        /// </summary>
        /// <param name="Destinatario"></param>
        /// <param name="ConteudoEmail"></param>
        /// <param name="Subject"></param>
        /// <param name="datas"></param>
        /// <returns></returns>
        public override bool Enviar(string Destinatario, string ConteudoEmail, string Subject, params System.Net.Mail.Attachment[] datas)
        {
            return Enviar(FormaEnvio.Direto, Destinatario, ConteudoEmail, Subject, datas);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Forma"></param>
        /// <param name="Destinatario"></param>
        /// <param name="NomeArquivo"></param>
        /// <param name="Impresso"></param>
        /// <param name="ConteudoEmail"></param>
        /// <param name="Subject"></param>
        /// <param name="_DestinatarioDefault"></param>
        /// <returns></returns>
        public bool Enviar(FormaEnvio Forma, string Destinatario, string NomeArquivo, DevExpress.XtraReports.UI.XtraReport Impresso, string ConteudoEmail, string Subject, string _DestinatarioDefault = "")
        {
            return Enviar(Forma, Destinatario, NomeArquivo, Impresso, ConteudoEmail, Subject, null, _DestinatarioDefault = "");
        }

        /// <summary>
        /// Envia e-mail Direto
        /// </summary>
        /// <param name="Forma"></param>
        /// <param name="Destinatario"></param>
        /// <param name="NomeArquivo"></param>
        /// <param name="Impresso"></param>
        /// <param name="ConteudoEmail"></param>
        /// <param name="Subject"></param>
        /// <param name="BOL"></param>
        /// <param name="_DestinatarioDefault"></param>
        /// <param name="Arquivos">Arquivos para anexar</param>
        /// <returns></returns>
        public bool Enviar(FormaEnvio Forma, string Destinatario, string NomeArquivo, DevExpress.XtraReports.UI.XtraReport Impresso, string ConteudoEmail, string Subject, int? BOL, string _DestinatarioDefault = "", params string[] Arquivos)
        {
            switch (Forma)
            {
                case FormaEnvio.ViaServidor:
                    if ((Arquivos == null) || (Arquivos.Length == 0))
                        return Reter(Destinatario, ConteudoEmail, Subject, BOL, GeraPDF(Impresso, NomeArquivo));
                    else
                    {
                        System.Net.Mail.Attachment[] Novodatas = new System.Net.Mail.Attachment[Arquivos.Length + 1];
                        Novodatas[0] = GeraPDF(Impresso, NomeArquivo);
                        for (int i = 0; i < Arquivos.Length; i++)
                            Novodatas[i + 1] = new Attachment(Arquivos[i]);
                        return Reter(Destinatario, ConteudoEmail, Subject, BOL, Novodatas);
                    }
                case FormaEnvio.Direto:
                    if ((Arquivos != null) && (Arquivos.Length != 0))
                        throw new NotImplementedException("N�o implementado com anexos");
                    return (Enviar(Destinatario, NomeArquivo, Impresso, ConteudoEmail, Subject, _DestinatarioDefault));
            }
            return false;

        }

        #endregion
        

        private bool Ativo = false;

        /// <summary>
        /// Desligar o loop da thread
        /// </summary>
        public bool DesligarLoop = false;

        private void GravaLog(string Gravacao)
        {
            if (!GerarLog)
                return;
            if (log == null)
            {
                log = new StringBuilder(string.Format("Data:{0}\r\nUsu�rio: {1} - {2}\r\nAplicativo:{3}\r\n",
                                                           DateTime.Now,
                                                           Environment.UserName,
                                                           Environment.MachineName,
                                                           System.Windows.Forms.Application.ExecutablePath ?? Environment.CommandLine));
            }
            log.AppendFormat("\r\n{0:HH:mm:ss fff} - {1} ", DateTime.Now,Gravacao);
            //Console.WriteLine("\r\n************{0:HH:mm:ss fff} - {1} ", DateTime.Now, Gravacao);
        }

        private void Ativar()
        {            
            GravaLog("Ativar");
            if (Ativo)
                return;
            Ativo = true;
            //new Thread(new ThreadStart(DescarregarLocal)).Start();
            Thread Thread1 = new Thread(new ThreadStart(DescarregarLocal));
            Thread1.Priority = ThreadPriority.Lowest;
            Thread1.Start();
            if (System.Windows.Forms.Application.ExecutablePath != null)
                System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
        }        

        void Application_ApplicationExit(object sender, EventArgs e)
        {
            DesligarLoop = true;
        }

        private static bool Afogado;

        /// <summary>
        /// N�mero de e-mail pendentes
        /// </summary>
        public int Pendentes;

        private void RegistraNoHistoricoBoleto(int BOL,string Mensagem,string Destinatario)
        {
            string MensagemCompleta = string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} - {1} ({2})", DateTime.Now, Mensagem, Destinatario);         
            VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.SQL);
            TA.ExecutarSQLNonQuery("update boletos set BOLJustificativa = BOLJustificativa + @P1 where BOL = @P2", MensagemCompleta, BOL);
            GravaLog(string.Format("REGISTRA BOL {0} {1}", BOL, Mensagem));
        }

        private void DescarregarLocal()
        {
            GravaLog("DescarregarLocal 14");
            while (!DesligarLoop)
            {
                try
                {
                    List<VirEmailOBJ> Retidos = VirEmailOBJ.Recuperas(200);
                    Pendentes = VirEmailOBJ.nPendentes;
                    GravaLog(string.Format("Pendentes:{0} / {1}", Retidos.Count, Pendentes));
                    Afogado = (Pendentes > 0);
                    if (Pendentes > 0)
                        AbreLote();
                    foreach (VirEmailOBJ Retido in Retidos)
                    {
                        Pendentes--;
                        if (DesligarLoop)
                            break;
                        if (Retido.Subject != null)
                            Retido.Subject = Retido.Subject.Replace(Environment.NewLine, " ");
                        if (Enviar(FormaEnvio.Retidos, Retido.Destinatario, Retido.ConteudoEmail, Retido.Subject, Retido.Anexos))
                        {
                            if (Retido.BOL.HasValue)
                                RegistraNoHistoricoBoleto(Retido.BOL.Value, "Envio de e-mail ok", Retido.Destinatario);
                            Retido.ApagarArquivo();
                            GravaLog(string.Format("{0}: ok {1}", Retido.ArquivoBIN, Retido.Destinatario));
                        }
                        else
                        {
                            Afogado = false;
                            string MenErro = "";
                            Exception Ex = UltimoErro;
                            while (Ex != null)
                            {
                                MenErro += string.Format("{0}\r\n{1}", Ex.Message,Ex.StackTrace);
                                Ex = Ex.InnerException;
                                if(Ex!=null)
                                    MenErro += "\r\n";
                            }
                            GravaLog(string.Format("{0}: ERRO {1} - {2}", Retido.ArquivoBIN, Retido.Destinatario, UltimoErro == null ? "???" : MenErro));
                            if ( (
                                  (UltimoErro == null)
                                  ||
                                  MenErro.Contains("need fully-qualified address")
                                  ||
                                  MenErro.Contains("Endere�o inv�lido")
                                  ||
                                  MenErro.Contains("Mailbox unavailable")
                                  ||
                                  MenErro.Contains("O cliente ou servidor s� � configurado para Endere�os de email com partes locais ASCII")
                                  ||
                                  MenErro.Contains("A cadeia de caracteres especificada n�o est� no formato necess�rio para um endere�o de email")
                                  ||
                                  MenErro.Contains("Erro de sintaxe em par�metros ou argumentos")
                                  ||
                                  MenErro.Contains("Caixa de correio n�o dispon�vel")
                                  ||
                                  MenErro.Contains("A valid address is required")
                                  ||
                                  MenErro.Contains("The client or server is only configured for E-mail addresses with ASCII")
                                  ||
                                  MenErro.Contains("O �ndice estava fora dos limites da matriz")
                                  ||
                                  MenErro.Contains("Index was outside the bounds of the array")
                                  ||
                                  MenErro.Contains("Bad recipient address syntax")
                                  ||
                                  MenErro.Contains("Syntax error in parameters or arguments")
                                )
                                &&
                                (
                                  (UltimoErro == null)
                                  ||
                                  !MenErro.Contains("you are sending too many mails")
                                )
                               )
                            {                                
                                if (Retido.BOL.HasValue)
                                {                                    
                                    try
                                    {
                                        RegistraNoHistoricoBoleto(Retido.BOL.Value, string.Format("e-mail N�O ENVIADO. Erro: {0}", UltimoErro.Message), Retido.Destinatario);
                                    }
                                    catch (Exception Ex2)
                                    {
                                        GravaLog(string.Format("Erro no registro do hist�rico BOL = {0}\r\nERRO {1}\r\n�ltimo erro: {2}", Retido.BOL.Value,Ex2.Message,UltimoEndereco));
                                    }
                                }
                                Retido.ApagarArquivo();
                            }
                            else
                                GravaLog(string.Format("N�O EST� NA LISTA - tentar novamente"));
                        }
                    }
                }
                catch (Exception ex)
                {
                    GravaLog(string.Format("Erro: {0}\r\n",ex.Message));
                    UltimoErro = ex;
                    Afogado = false;
                    string ArquivoLOG = string.Format("{0}\\EML\\LOG.log", System.IO.Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")));
                    System.IO.File.WriteAllText(ArquivoLOG, log.ToString());
                }
                finally
                {
                    FechaLote();
                }
                Thread.Sleep(3 * 1000);
                if (!Afogado && !DesligarLoop)
                {
                    GravaLog("ESPERA");
                    DateTime Esperar = DateTime.Now.AddMinutes(2);
                    while (!DesligarLoop && (DateTime.Now < Esperar) && !Afogado)
                        Thread.Sleep(3 * 1000);
                }                
            }
            if (GerarLog)
            {
                GravaLog("FIM");
                if (VirEmailOBJ.nPendentes > 0)
                    Enviar("luis@virweb.com.br", log.ToString(), VirEmailOBJ.nPendentes == 0 ? "LOG dos e-mails" : "PENDENTE LOG dos e-mails");
                try
                {
                    string ArquivoLOG = string.Format("{0}\\EML\\LOG.log", System.IO.Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")));
                    System.IO.File.WriteAllText(ArquivoLOG, log.ToString());
                }
                catch { }
            }
        }
    
        /*
        /// <summary>
        /// Envia os e-mails parados
        /// </summary>
        /// <returns></returns>
        public string Descarregar()
        {
                   
            int enviados = 0;
            int total = 0;
            DateTime TempoMaximo = DateTime.Now.AddMinutes(10);
            try
            {
                //AbreLote();
                dEmails dEmails1 = new dEmails();
                total = dEmails1.EMaiLTableAdapter.Fill(dEmails1.EMaiL);
                //dEmails1.EMailAnexoTableAdapter.FillTOP(dEmails1.EMailAnexo);
                List<dEmails.EMaiLRow> apagarEML = new List<dEmails.EMaiLRow>();
                //List<int> apagarEMA = new List<int>();
                foreach (dEmails.EMaiLRow rowEML in dEmails1.EMaiL)
                {
                    if (DateTime.Now > TempoMaximo)
                        break;
                    dEmails1.EMailAnexoTableAdapter.FillByEML(dEmails1.EMailAnexo,rowEML.EML);
                    dEmails.EMailAnexoRow[] EMAs = rowEML.GetEMailAnexoRows();
                    System.Net.Mail.Attachment[] at1 = new System.Net.Mail.Attachment[EMAs.Length];
                    int i = 0;
                    foreach (dEmails.EMailAnexoRow EMA in EMAs)
                    {
                        System.IO.MemoryStream MS = new System.IO.MemoryStream(EMA.EMAConteudo);
                        at1[i++] = new Attachment(MS, EMA.EMANome);
                    }
                    if (Enviar(FormaEnvio.Retidos, rowEML.EMLTo, rowEML.EMLConteudo, rowEML.EMLSubject, at1))
                    {
                        if (!rowEML.IsEML_BOLNull())
                        {
                            Abstratos.ABS_Boleto Boleto = Abstratos.ABS_Boleto.GetBoleto(rowEML.EML_BOL);
                            if (Boleto.Encontrado)
                                Boleto.GravaJustificativa("Envio de e-mail ok", "");
                        }
                        enviados++;
                        apagarEML.Add(rowEML);
                        //foreach (dEmails.EMailAnexoRow EMA in EMAs)
                        //    apagarEMA.Add(EMA);
                    }
                    else
                    {
                        if (
                            UltimoErro.Message.Contains("need fully-qualified address")
                              ||
                            UltimoErro.Message.Contains("Endere�o inv�lido")
                              ||
                            UltimoErro.Message.Contains("O cliente ou servidor s� � configurado para Endere�os de email com partes locais ASCII")
                              ||
                            UltimoErro.Message.Contains("A cadeia de caracteres especificada n�o est� no formato necess�rio para um endere�o de email")
                              ||
                            UltimoErro.Message.Contains("Erro de sintaxe em par�metros ou argumentos")
                              ||
                            UltimoErro.Message.Contains("Caixa de correio n�o dispon�vel")
                           )
                        {
                            if (!rowEML.IsEML_BOLNull())
                            {
                                Abstratos.ABS_Boleto Boleto = Abstratos.ABS_Boleto.GetBoleto(rowEML.EML_BOL);
                                if (Boleto.Encontrado)
                                    Boleto.GravaJustificativa(string.Format("e-mail N�O ENVIADO. Erro: {0}", UltimoErro.Message), "");
                            }
                            apagarEML.Add(rowEML);
                            //foreach (dEmails.EMailAnexoRow EMA in EMAs)
                            //    apagarEMA.Add(EMA);
                        }
                        else
                            break;
                    }
                }
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL(VirDB.Bancovirtual.TiposDeBanco.EmailREt, "emailDiretoNeon 262", dEmails1.EMaiLTableAdapter, dEmails1.EMailAnexoTableAdapter);
                    //foreach (dEmails.EMailAnexoRow rowEMA in apagarEMA)
                    //    rowEMA.Delete();
                    //dEmails1.EMailAnexoTableAdapter.Update(dEmails1.EMailAnexo);
                    foreach (dEmails.EMaiLRow rowEML in apagarEML)
                    {
                        dEmails1.EMailAnexoTableAdapter.Apagar(rowEML.EML);
                        rowEML.Delete();
                    }
                    dEmails1.EMaiLTableAdapter.Update(dEmails1.EMaiL);
                    VirMSSQL.TableAdapter.CommitSQL(VirDB.Bancovirtual.TiposDeBanco.EmailREt);
                }
                catch (Exception e)
                {
                    UltimoErro = e;
                    VirMSSQL.TableAdapter.VircatchSQL(e, VirDB.Bancovirtual.TiposDeBanco.EmailREt);
                    return string.Format("Erro:{0}\r\n\r\nResultado parcial: E-mails enviados {1}/{2}\r\n", e.Message, enviados, total);
                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return string.Format("Erro na carga:{0}\r\n\r\n", ex.Message);
            }
            finally
            {
                //FechaLote();
            }
            return string.Format("E-mails enviados {0}/{1}",enviados,total);
        }
        */
    }
}
