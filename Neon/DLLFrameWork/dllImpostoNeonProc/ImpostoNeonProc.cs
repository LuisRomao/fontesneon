﻿using System;
using dllImpostos;
using Framework.objetosNeon;
using FrameworkProc;
using VirEnumeracoes;

namespace dllImpostoNeonProc
{
    /// <summary>
    /// 
    /// </summary>
    public class ImpostoNeonProc : Imposto
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Tipo"></param>
        public ImpostoNeonProc(TipoImposto _Tipo)
            : base(_Tipo)
        { 
        }

        /// <summary>
        /// 
        /// </summary>
        public ImpostoNeonProc()
            : base()
        {
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Tipo"></param>
        /// <param name="_DataNota"></param>
        /// <param name="_DataPagamento"></param>
        /// <param name="_ValorBase"></param>
        /// <param name="_Competencia"></param>
        public ImpostoNeonProc(TipoImposto _Tipo, DateTime _DataNota, DateTime _DataPagamento, decimal _ValorBase, Competencia _Competencia = null)
            : base(_Tipo)
        {
            DataNota = _DataNota;
            DataPagamento = _DataPagamento;
            ValorBase = _ValorBase;
            Competencia = _Competencia;
        }

        /// <summary>
        /// 
        /// </summary>
        protected Competencia competencia;

        /// <summary>
        /// 
        /// </summary>
        public Competencia Competencia
        {
            get
            {
                if (competencia != null)
                    return competencia;
                else
                    return new Competencia(Apuracao);
            }
            set
            {
                competencia = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override DateTime Apuracao
        {
            get
            {
                if (competencia == null)
                    return base.Apuracao;
                else
                {
                    DateTime Manobra;
                    Manobra = new DateTime(competencia.Ano, competencia.Mes, 1);
                    Manobra = Manobra.AddMonths(1).AddDays(-1);
                    return Manobra;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override DateTime Vencimento
        {
            get
            {
                if (competencia == null)
                    return base.Vencimento;
                else
                {
                    //DateTime Manobra;
                    int MesReal = (competencia.Mes == 13 ? 12 : competencia.Mes);
                    switch (TipoVenc)
                    {
                        case TipoVencimento.ProximaQuizena:
                            return base.Vencimento;
                        case TipoVencimento.Dia20doProximoMes:
                            return new DateTime(competencia.Ano, MesReal, 20).AddMonths(1);
                        case TipoVencimento.Dia25doProximoMes:
                            return new DateTime(competencia.Ano, MesReal, 25).AddMonths(1);
                        case TipoVencimento.Dia15doProximoMes:
                            return new DateTime(competencia.Ano, MesReal, 15).AddMonths(1);
                        default: throw new NotImplementedException();
                    }
                }
            }
        }

        /// <summary>
        /// Verifica se a data é feriado
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        protected override bool IsFeriado(DateTime Data)
        {
            return Data.IsFeriado();
        }
    }
}
