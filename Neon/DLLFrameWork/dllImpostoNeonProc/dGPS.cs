﻿/*
MR - 12/11/2014 10:00           - Inclusão do campo cod. de comunicacao, dig. agencia e dig. conta (GPSCodComunicacao, GPSDigAgencia, GPSDigConta) no GPSTableAdapter
                                - Ajustes em GPSTableAdapter.FillByEnviar para retornar apenas os registros no layout antigo, sem cod. de comunicacao setado
                                - Incluido GPSTableAdapter.FillBySetarEnvio para setar o envio dos registros no layout novo, com cod. de comunicacao setado
                                - Incluido GPSTableAdapter.FillByGPS para tratar GPS recebido em arquivo de retorno
                                - Incluido GPSTableAdapter.AlteraStatus semelhante ao GPSTableAdapter.AjustaStatus, porem incluindo informacao no comentario
                                - Incluido GPSTableAdapter.GravaObs que salva informacao no comentario
                                - Incluida a funcao SetarEnvioGPS (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 12/11/2014 10:00           - Inclusão do campo codigo barra para FGTS (GPSCodigoBarra) no GPSTableAdapter
                                  Tratamento do novo tipo de imposto FGTS (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
MR - 27/04/2015 12:00           - Inclusão do campo de origem do imposto, folha ou nota (GPSOrigem) no GPSTableAdapter
MR - 18/08/2015 10:00           - Inclusão do campo de identificacao do documento recebido pelo banco (GPSIDDocumento) no GPSTableAdapter
                                  Alterada a query GPSTableAdapter.AlteraStatus para salvar tambem o campo GPSIDDocumento
*/

using System;
using System.Collections.Generic;
using VirEnumeracoesNeon;
using VirEnumeracoes;

namespace dllImpostoNeonProc
{


    partial class dGPS
    {
        private static dGPS _dGPSSt;

        /// <summary>
        /// dataset estático:dGPS
        /// </summary>
        public static dGPS dGPSSt
        {
            get
            {
                if (_dGPSSt == null)
                {
                    _dGPSSt = new dGPS();
                    //_dGPSSt.GPSTableAdapter.Fill(_dGPSSt.GPS);
                }
                return _dGPSSt;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Ultimoerro;

        private dGPSTableAdapters.GPSTableAdapter gPSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: GPS
        /// </summary>
        public dGPSTableAdapters.GPSTableAdapter GPSTableAdapter
        {
            get
            {
                if (gPSTableAdapter == null)
                {
                    gPSTableAdapter = new dGPSTableAdapters.GPSTableAdapter();
                    gPSTableAdapter.TrocarStringDeConexao();
                };
                return gPSTableAdapter;
            }
        }

        private dGPSTableAdapters.GPSTableAdapter gPSTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: GPS
        /// </summary>
        public dGPSTableAdapters.GPSTableAdapter GPSTableAdapterF
        {
            get
            {
                if (gPSTableAdapterF == null)
                {
                    gPSTableAdapterF = new dGPSTableAdapters.GPSTableAdapter();
                    gPSTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    gPSTableAdapterF.ClearBeforeFill = false;
                };
                return gPSTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public dGPSTableAdapters.GPSTableAdapter GPSTableAdapterX(int EMP)
        {
            return (EMP == Framework.DSCentral.EMP) ? GPSTableAdapter : GPSTableAdapterF;
        }

        /// <summary>
        /// TratarRetorno
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public bool TratarRetorno(string NomeArquivo, TipoImposto Tipo)
        {
            ArquivosTXT.cTabelaTXT Ret = new ArquivosTXT.cTabelaTXT(Retorno, ArquivosTXT.ModelosTXT.layout);
            Ret.RegistraMapa(Retorno.IDColumn, Tipo == TipoImposto.INSS ? 322 : 352, 10);
            Ret.RegistraMapa(Retorno.StatusColumn, 185, 2);
            Ret.RegistraMapa(Retorno.AutenticacaoColumn, 437, 27);
            Ret.Carrega(NomeArquivo, "");
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllImpostoNeon dGPS - 61", GPSTableAdapter);
                foreach (RetornoRow Retrow in Retorno)
                {
                    GPSTableAdapter.AjustaStatus(Retrow.Status, Retrow.Autenticacao, Retrow.ID);
                };
                return VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                Ultimoerro = e.Message;
                return false;
            };

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public string GerarGPS(TipoImposto Tipo)
        {
            GPSTableAdapter.FillByEnviar(GPS, (int)Tipo);
            if (GPS.Count == 0)
                return "";
            else
            {
                string Diretorio = "c:\\sistema neon\\tmp\\";
                if (!System.IO.Directory.Exists(Diretorio))
                    System.IO.Directory.CreateDirectory(Diretorio);
                string Nome = "";
                switch (Tipo)
                {
                    case TipoImposto.INSS:
                    case TipoImposto.PIS:
                    case TipoImposto.COFINS:
                    case TipoImposto.CSLL:
                    case TipoImposto.IR:
                    case TipoImposto.PIS_COFINS_CSLL:
                        ArquivosTXT.cTabelaTXT ArquivoGPS = new ArquivosTXT.cTabelaTXT(GPS, ArquivosTXT.ModelosTXT.layout);
                        ArquivoGPS.Formatodata = ArquivosTXT.FormatoData.yyyyMMdd;
                        if (Tipo == TipoImposto.INSS)
                            ArquivoGPS.RegistraMapa("ID", 1, 1, ArquivosTXT.TipoMapa.detalhe, ArquivosTXT.TiposDeCampo.normal, "7");
                        else
                            ArquivoGPS.RegistraMapa("ID", 1, 1, ArquivosTXT.TipoMapa.detalhe, ArquivosTXT.TiposDeCampo.normal, "2");
                        ArquivoGPS.RegistraMapa(GPS.GPSNomeClientePagadorColumn, 2, 40);
                        ArquivoGPS.RegistraMapa(GPS.GPSEnderecoColumn, 42, 40);
                        ArquivoGPS.RegistraMapa(GPS.GPSCEPColumn, 82, 8);//acertar
                        ArquivoGPS.RegistraMapa(GPS.GPSUFColumn, 90, 2);
                        ArquivoGPS.RegistraMapa(GPS.GPSCidadeColumn, 92, 20);
                        ArquivoGPS.RegistraMapa(GPS.GPSBairroColumn, 112, 20);
                        ArquivoGPS.RegistraMapa("TipoInsc", 132, 1, ArquivosTXT.TipoMapa.detalhe, "2");
                        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        if (Tipo == TipoImposto.INSS)
                            ArquivoGPS.RegistraMapa(GPS.GPSCNPJColumn, 133, 15);
                        else
                            ArquivoGPS.RegistraMapa(GPS.GPSCNPJColumn, 133, 14);
                        ArquivoGPS.RegistraMapa(GPS.GPSDataPagtoColumn, 168, 8);
                        ArquivoGPS.RegistraMapa("Auto", 176, 1, ArquivosTXT.TipoMapa.detalhe, "S");
                        ArquivoGPS.RegistraMapa(GPS.GPSValorPrincipalColumn, 177, 15);
                        if (Tipo == TipoImposto.INSS)
                        {
                            ArquivoGPS.RegistraMapa(GPS.GPSValorMultaColumn, 192, 15);
                            ArquivoGPS.RegistraMapa(GPS.GPSOutrasColumn, 207, 15);
                            ArquivoGPS.RegistraMapa(GPS.GPSValorTotalColumn, 222, 15);
                            ArquivoGPS.RegistraMapa(GPS.GPSCodigoReceitaColumn, 237, 4);
                            ArquivoGPS.RegistraMapa("TipoID", 241, 2, ArquivosTXT.TipoMapa.detalhe, ArquivosTXT.TiposDeCampo.normal, "01");
                            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                            ArquivoGPS.RegistraMapa(GPS.GPSIdentificadorColumn, 243, 14);
                            ArquivoGPS.RegistraMapa(GPS.GPSAnoColumn, 257, 4);
                            ArquivoGPS.RegistraMapa(GPS.GPSMesColumn, 261, 2);
                            ArquivoGPS.RegistraMapa(GPS.GPSNomeIdentificadorColumn, 263, 40);
                            ArquivoGPS.RegistraMapa(GPS.GPSColumn, 303, 10);
                            ArquivoGPS.RegistraMapa("UsoEmpresa", 322, 30, ArquivosTXT.TipoMapa.detalhe, "");
                            ArquivoGPS.RegistraMapa("Reserva", 383, 39, ArquivosTXT.TipoMapa.detalhe, "");
                        }
                        else
                        {

                            ArquivoGPS.RegistraMapa(GPS.GPSValorJurosColumn, 192, 15);
                            ArquivoGPS.RegistraMapa(GPS.GPSValorMultaColumn, 207, 15);
                            ArquivoGPS.RegistraMapa("Reserva", 222, 15, ArquivosTXT.TipoMapa.detalhe, "");
                            ArquivoGPS.RegistraMapa(GPS.GPSVencimentoColumn, 237, 8);
                            ArquivoGPS.RegistraMapa(GPS.GPSCodigoReceitaColumn, 245, 4);
                            ArquivoGPS.RegistraMapa(GPS.GPSApuracaoColumn, 249, 8);
                            ArquivoGPS.RegistraMapa("Percentual", 257, 4, ArquivosTXT.TipoMapa.detalhe, 0);
                            ArquivoGPS.RegistraMapa("Referencia", 261, 17, ArquivosTXT.TipoMapa.detalhe, "");
                            ArquivoGPS.RegistraMapa("Receita", 278, 15, ArquivosTXT.TipoMapa.detalhe, 0);
                            ArquivoGPS.RegistraMapa(GPS.GPSNomeIdentificadorColumn, 293, 40);
                            ArquivoGPS.RegistraMapa(GPS.GPSColumn, 333, 10);
                            ArquivoGPS.RegistraMapa("TipoInsc1", 348, 1, ArquivosTXT.TipoMapa.detalhe, "2");
                            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                            ArquivoGPS.RegistraMapa(GPS.GPSIdentificadorColumn, 349, 14);
                            ArquivoGPS.RegistraMapa("Reserva1", 373, 49, ArquivosTXT.TipoMapa.detalhe, "");
                        }
                        ArquivoGPS.RegistraMapa(GPS.GPSAgenciaColumn, 422, 4);
                        ArquivoGPS.RegistraMapa(GPS.GPSContaColumn, 426, 7);
                        ArquivoGPS.RegistraMapa("NumeroConvenio", 433, 8, ArquivosTXT.TipoMapa.detalhe, 29847);
                        ArquivoGPS.RegistraMapa("Reserva2", 441, 23, ArquivosTXT.TipoMapa.detalhe, "");
                        Nome = String.Format("{0}{1}_{2:yyyyMMddHHmmss}.rem", Diretorio, Tipo == TipoImposto.INSS ? "GPS" : "DARF", System.DateTime.Now);
                        ArquivoGPS.Salva(Nome);
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllimpostoNeon dGPS - 159", GPSTableAdapter);
                            foreach (dGPS.GPSRow row in GPS)
                                row.GPSStatus = (int)StatusArquivo.Aguardando_Retorno;
                            GPSTableAdapter.Update(GPS);
                            GPS.AcceptChanges();
                            VirMSSQL.TableAdapter.STTableAdapter.Commit();
                            return Nome;
                        }
                        catch (Exception e)
                        {
                            VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                            return "";
                        };
                    case TipoImposto.ISSQN:
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                                 
                    case TipoImposto.FGTS:
                        //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                                 
                        return "";
                };
                return Nome;
            }
        }

        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
        /// <summary>
        /// Setar Envio de GPS
        /// </summary>
        /// <returns></returns>
        public int SetarEnvioGPS(List<dGPS.GPSRow> Linhas)
        {

            foreach (dGPS.GPSRow row in Linhas)
                {
                    if ((StatusArquivo)row.GPSStatus == StatusArquivo.ParaEnviar)
                    {
                        row.GPSStatus = (int)StatusArquivo.Aguardando_Envio;
                        GPSTableAdapterX(row.EMP).Update(GPS);
                    }
                }
                return GPS.Count;
            
        }
        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***

        /*
        public string GerarDARF()
        {
            DARTableAdapter.FillByEnviar(DAR);
            if (DAR.Count == 0)
                return "";
            else
            {
                string Diretorio = "c:\\enviar\\";
                string Nome = "";
                ArquivosTXT.cTabelaTXT ArquivoDARF = new ArquivosTXT.cTabelaTXT(DAR, ArquivosTXT.ModelosTXT.layout);
                ArquivoDARF.Formatodata = ArquivosTXT.FormatoData.yyyyMMdd;
                
                
                
                

                Nome = Diretorio + "DARF_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".rem";
                if (!ArquivoDARF.Salva(Nome))
                    return false;
                try
                {
                    VirMSSQL.TableAdapter.STTableAdapter.IniciaTrasacaoST(DARTableAdapter);
                    foreach (dGPS.DARRow row in DAR)
                        row.DARStatus = (int)StatusArquivo.Aguardando_Retorno;
                    GPSTableAdapter.Update(DAR);
                    GPS.AcceptChanges();
                    VirMSSQL.TableAdapter.STTableAdapter.FimTrasacao(true);
                    //copy
                    //chamada
                    return Nome;
                }
                catch
                {
                    VirMSSQL.TableAdapter.STTableAdapter.FimTrasacao(false);
                    return "";
                }
            }
        }

        */
    }
}
