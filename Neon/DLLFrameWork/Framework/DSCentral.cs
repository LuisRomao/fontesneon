﻿namespace Framework {

    partial class DSCentral
    {
        private static DSCentralTableAdapters.PLAnocontasTableAdapter pLAnocontasTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public static DSCentralTableAdapters.PLAnocontasTableAdapter PLAnocontasTableAdapter
        {
            get {
                if (pLAnocontasTableAdapter == null) {
                    pLAnocontasTableAdapter = new Framework.DSCentralTableAdapters.PLAnocontasTableAdapter();
                    pLAnocontasTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return DSCentral.pLAnocontasTableAdapter; 
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        public static bool ReceberDoAccess = false;
        private static int eMP = 0;

        /// <summary>
        /// Mostrar condomínios somente da filial
        /// </summary>
        public static bool FiltrarEMP = true;

        /// <summary>
        /// Filial do banco de dados
        /// </summary>
        public static int EMP
        {
            get {
                if (VirMSSQL.TableAdapter.STTableAdapter.Configurado())
                {
                    if (eMP == 0)
                        VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select EMP from EMPRESAS where EMPAtiva = 1", out eMP);
                    return DSCentral.eMP;
                }
                else
                    return -1;
            }            
        }

        /// <summary>
        /// EMP da filial
        /// </summary>
        public static int EMPF
        {
            get
            {
                if (VirMSSQL.TableAdapter.STTableAdapter.Configurado())
                    return DSCentral.EMP == 1 ? 3 : 1;
                else
                    return -1;
            }
        }

        private static DSCentral dCentral;

        private DSCentralTableAdapters.EMPRESASTableAdapter eMPRESASTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public DSCentralTableAdapters.EMPRESASTableAdapter EMPRESASTableAdapter {
            get {
                if (eMPRESASTableAdapter == null) {
                    eMPRESASTableAdapter = new Framework.DSCentralTableAdapters.EMPRESASTableAdapter();
                    eMPRESASTableAdapter.TrocarStringDeConexao();
                };
                return eMPRESASTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EMPRESASDataTable EMPRESASST {
            get {               
                if (EMPRESAS.Rows.Count == 0)
                    EMPRESASTableAdapter.Fill(EMPRESAS,EMP);
                return EMPRESAS;
            }
        }

        /// <summary>
        /// Dados da empresa padrao
        /// </summary>
        public static EMPRESASRow EMProw
        {
            get 
            {
                return DCentral.EMPRESASST[0];
            }
        }

        private static DSCentralTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private static DSCentralTableAdapters.IMPressoesTableAdapter iMPressoesTableAdapter;
        private static DSCentralTableAdapters.TiPoImpressaoTableAdapter tiPoImpressaoTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public static DSCentralTableAdapters.TiPoImpressaoTableAdapter TiPoImpressaoTableAdapter
        {
            get {
                if (tiPoImpressaoTableAdapter == null) {
                    tiPoImpressaoTableAdapter = new DSCentralTableAdapters.TiPoImpressaoTableAdapter();
                    tiPoImpressaoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return DSCentral.tiPoImpressaoTableAdapter; 
            }
        } 

        /// <summary>
        /// 
        /// </summary>
        public static DSCentralTableAdapters.IMPressoesTableAdapter IMPressoesTableAdapter
        {
            get {
                if (iMPressoesTableAdapter == null) {
                    iMPressoesTableAdapter = new Framework.DSCentralTableAdapters.IMPressoesTableAdapter();
                    iMPressoesTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return DSCentral.iMPressoesTableAdapter; 
            }
        }
 
        /// <summary>
        /// 
        /// </summary>
        public static DSCentralTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get {
                if (cONDOMINIOSTableAdapter == null) 
                {
                    cONDOMINIOSTableAdapter = new Framework.DSCentralTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return DSCentral.cONDOMINIOSTableAdapter; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DSCentral.PLAnocontasDataTable PlanoDeContas {
            get {
                if(PLAnocontas.Rows.Count == 0)
                    PLAnocontasTableAdapter.Fill(DCentral.PLAnocontas);
                return PLAnocontas;
            }
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="PLA"></param>
        /// <returns></returns>
        public PLAnocontasRow ImportaPLA(string PLA) {
            PLAnocontasRow retorno = PlanoDeContas.FindByPLA(PLA);
            if (retorno == null) {
                string comando = "SELECT Descricao, [Descrição Resumida] as DescricaoRes, [Tipo despesa] as proprietario FROM Plano_contas WHERE (TipoPag = @P1)";
                System.Data.DataTable DT = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLTabela(comando, PLA);
                if (DT.Rows.Count > 0) {
                    System.Data.DataRow DR = DT.Rows[0];
                    retorno = PLAnocontas.NewPLAnocontasRow();
                    retorno.PLA = PLA;
                    retorno.PLADescricao = DR["Descricao"].ToString();
                    retorno.PLADescricaoLivre = false;
                    retorno.PLADescricaoRes = DR["DescricaoRes"].ToString();
                    retorno.PLAEVE = false;
                    retorno.PLAProprietario = (DR["proprietario"].ToString() != "P");
                    PLAnocontas.AddPLAnocontasRow(retorno);
                    PLAnocontasTableAdapter.Update(retorno);
                    retorno.AcceptChanges();
                    
                }
            }
            return retorno;
        }
        */
        /// <summary>
        /// 
        /// </summary>
        public static DSCentral DCentral
        {
            get {
                if (dCentral == null)
                {
                    dCentral = new DSCentral();
                    VirDB.VirtualTableAdapter.AjustaAutoInc(dCentral);                    
                };
                return DSCentral.dCentral; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void fill_CON()
        {
            if (CONDOMINIOS.Rows.Count == 0)
            {
                DSCentral.CONDOMINIOSTableAdapter.Fill(CONDOMINIOS);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void fill_TPI() {
            if (TiPoImpressao.Rows.Count == 0) {
                DSCentral.TiPoImpressaoTableAdapter.Fill(TiPoImpressao);
            }
        }
    }
}
