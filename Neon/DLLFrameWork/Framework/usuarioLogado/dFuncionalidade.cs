﻿namespace Framework.usuarioLogado {

    /// <summary>
    /// dataset para a funcionalidades do usuários
    /// </summary>
    partial class dFuncionalidade
    {
        private dFuncionalidadeTableAdapters.FuncionalidadesTableAdapter funcionalidadesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Funcionalidades
        /// </summary>
        public dFuncionalidadeTableAdapters.FuncionalidadesTableAdapter FuncionalidadesTableAdapter
        {
            get
            {
                if (funcionalidadesTableAdapter == null)
                {
                    funcionalidadesTableAdapter = new dFuncionalidadeTableAdapters.FuncionalidadesTableAdapter();
                    funcionalidadesTableAdapter.TrocarStringDeConexao();
                };
                return funcionalidadesTableAdapter;
            }
        }
    }
}
