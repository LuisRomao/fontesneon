using System;
using System.Collections.Generic;
using System.Text;
using CompontesBasicos.Bancovirtual;
using FrameworkProc.datasets;
using FrameworkProc;

namespace Framework.usuarioLogado
{
    /// <summary>
    /// Classe de login para NEON
    /// </summary>
    public class UsuarioLogadoNeon : CompontesBasicos.Bancovirtual.UsuarioLogado
    {
        private System.Net.NetworkCredential _EmailNetCredenciais;
        //private System.Net.NetworkCredential _NetCredenciais;
        private System.Net.WebProxy _Proxy = null;
        private string _ServidorSMTP;
        //private string _ServidorProxy = "";
        private string _EmailRemetente;

        //public object erro;
        //private string errostr;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="USU"></param>
        public UsuarioLogadoNeon(int USU) {
            USUreserva = USU;
            System.Data.DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select EMPSMTP,EMPSMTPusu,EMPSMTPpass from EMPRESAS where EMP = @P1", Framework.DSCentral.EMP);
            _ServidorSMTP = (string)DR["EMPSMTP"];
            _EmailNetCredenciais = new System.Net.NetworkCredential((string)DR["EMPSMTPusu"], (string)DR["EMPSMTPpass"]);
            if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
            {
                
                if (Framework.DSCentral.EMP == 1)
                {                                                            
                    //_ServidorSMTP = "smtp.neonimoveis.com";
                    //_EmailNetCredenciais = new System.Net.NetworkCredential("neon@neonimoveis.com", "neon123");
                    //_EmailNetCredenciais = new System.Net.NetworkCredential("sistema@neonimoveis.com", "  ");
                    //_EmailNetCredenciais = new System.Net.NetworkCredential("sistema@neonimoveis1.com", "Sis_08%10*");
                    _EmailRemetente = "neon@neonimoveis.com.br";                    
                }
                else if (Framework.DSCentral.EMP == 4)
                {                    
                    //_ServidorSMTP = "10.135.1.20";
                    //_NetCredenciais = new System.Net.NetworkCredential("publicacoes", "neon01");
                    _EmailRemetente = "neon@neonimoveis.com.br";                    
                }
                else if (Framework.DSCentral.EMP == 3)
                {                    
                    //_ServidorSMTP = "smtp.neonimoveis.com";
                    //_EmailNetCredenciais = new System.Net.NetworkCredential("neonsa@neonimoveis.com", "neon123");
                    _EmailRemetente = "neonsa@neonimoveis.com.br"; 
                };
               
            }
            else
            {                
                //_ServidorSMTP = "smtp.neonimoveis.com";
                //_EmailNetCredenciais = new System.Net.NetworkCredential("sistema@neonimoveis1.com", "Sis_08%10*");
                //_EmailNetCredenciais = new System.Net.NetworkCredential("neon@neonimoveis.com", "neon123");
                _EmailRemetente = "neon@neonimoveis.com.br"; 
            }

            CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt = this;
            DSCentral.USU = USU;
        }

        private dUSUarios.USUariosRow _rowUsuarioLogado;
        /// <summary>
        /// Usuario logado
        /// </summary>
        public dUSUarios.USUariosRow rowUsuarioLogado
        {
            get 
            {
                if (_rowUsuarioLogado == null)
                {
                    _rowUsuarioLogado = dUSUarios.dUSUariosSt.USUarios.FindByUSU(USUreserva);
                }
                return _rowUsuarioLogado;
            }
            set 
            { 
                _rowUsuarioLogado = value;
                if (_rowUsuarioLogado != null)
                    USUreserva = _rowUsuarioLogado.USU;
            }
        }
   

        private int USUreserva;

        /// <summary>
        /// Le e grava os dados da classe m�e
        /// </summary>
        public static UsuarioLogadoNeon UsuarioLogadoStNEON
        {
            get
            {                
                return (UsuarioLogadoNeon)CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt;
            }
        }


        #region override

        /// <summary>
        /// C�digo do usu�rio logado
        /// </summary>
        public override int USU
        {
            get { 
                return rowUsuarioLogado.USU; 
            }
        }

        /// <summary>
        /// Nome do usu�rio logado
        /// </summary>
        public override string USUNome
        {
            get { 
                return rowUsuarioLogado.USUNome; 
            }
        }

        
        /// <summary>
        /// Credenciais
        /// </summary>
        public override System.Net.NetworkCredential EmailNetCredenciais
        {
            get {return _EmailNetCredenciais ; }
        }

        /// <summary>
        /// proxy
        /// </summary>
        public override System.Net.WebProxy Proxy
        {
            get { return _Proxy; }
        }
       
        /// <summary>
        /// Busca o nome de um usu�rio
        /// </summary>
        /// <param name="USU"></param>
        /// <returns></returns>
        public override string BuscaUSUNome(object USU)
        {
            //return datasets.dUSUarios.dUSUariosSt.BuscaUSUNome(USU);
            return dUSUarios.dUSUariosStTodos.BuscaUSUNome(USU);
        }

        /// <summary>
        /// Remetente
        /// </summary>
        public override string EmailRemetente
        {
            get { return _EmailRemetente; }
        }

        /// <summary>
        /// Servidor SMTP
        /// </summary>
        public override string ServidorSMTP
        {
            get { return _ServidorSMTP; }
        }

        #endregion

        #region Funcionalidades
        private dFuncionalidade _dFuncionalidade;

        private dFuncionalidade DFuncionalidade
        {
            get
            {
                if (_dFuncionalidade == null)
                {
                    _dFuncionalidade = new dFuncionalidade();
                    _dFuncionalidade.FuncionalidadesTableAdapter.Fill(_dFuncionalidade.Funcionalidades, rowUsuarioLogado.USU);
                };
                return _dFuncionalidade;
            }
        }

        /// <summary>
        /// Verifica se o usuario logado possui acesso � funcionalidade
        /// </summary>
        /// <param name="Funcionalidade">Funcionalidade a pesquisar</param>
        /// <returns>Nivel de acesso</returns>
        public override int VerificaFuncionalidade(string Funcionalidade)
        {
            if (desenvolvimento)
                return 1000;
            if (rowUsuarioLogado == null)
                return 0;
            else
            {
                dFuncionalidade.FuncionalidadesRow rowFuncionalidades = DFuncionalidade.Funcionalidades.FindByFUNNome(Funcionalidade);
                if (rowFuncionalidades == null)
                    return 0;
                else
                    return rowFuncionalidades.FUNPERNivel;
            };
        }
        
        #endregion
    }
}
