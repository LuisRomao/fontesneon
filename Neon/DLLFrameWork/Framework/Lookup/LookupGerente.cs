﻿using FrameworkProc.datasets;
using System;
using System.ComponentModel;
using VirDB.Bancovirtual;

namespace Framework.Lookup
{    
    /// <summary>
    /// 
    /// </summary>
    public partial class LookupGerente : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public static int? GetGerente(int CON)
        {
            int USU;
            if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONAuditor1_USU from CONdominios where CON = @P1", out USU, CON))
                return USU;
            else
                return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public static int? GetAssistente(int CON)
        {
            int USU;
            if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONAuditor2_USU from CONdominios where CON = @P1", out USU, CON))
                return USU;
            else
                return null;
        }

        /// <summary>
        /// Construtor
        /// </summary>        
        public LookupGerente()
        {
            InitializeComponent();            
        }

        private int uSUSel = -1;

        private bool block = false;
        
        /// <summary>
        /// Usuário selecionado
        /// </summary>
        public int USUSel
        {
            get
            {
                return uSUSel;
            }
            set
            {
                uSUSel = value;
                lookUpEdit1.EditValue = value;
            }
        }

        /// <summary>
        /// Nome Usuário selecionado
        /// </summary>
        public string USUNomeSel
        {
            get
            {
                dUSUarios.USUariosRow USUrow = dUSUarios.dUSUariosStTodos.USUarios.FindByUSU(uSUSel);
                return USUrow == null ? "" : USUrow.USUNome;
            }            
        }

        /// <summary>
        /// Tipo do componete
        /// </summary>
        [Category("Virtual Software")]
        [Description("Tipo usuário")]
        public TipoUsuario Tipo
        {
            get => _Tipo;
            set
            {
                bool Mudar = _Tipo != value;
                _Tipo = value;
                if (Mudar)
                    MudaTipo();
            }
        }

        #region Eventos
        /// <summary>
        /// Evento que ocorre quando o gerente / assistente é alterado
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento na alteração das opções")]
        public event EventHandler alterado;

        /// <summary>
        /// Evento que ocorre quando o gerente é alterado
        /// </summary>
        /// <param name="e"></param>
        protected virtual void Onalterado(EventArgs e)
        {
            if (alterado != null)
                alterado(this, e);
        }

        #endregion

        /// <summary>
        /// Tipo de usuario do componente
        /// </summary>
        public enum TipoUsuario
        {
            /// <summary>
            /// 
            /// </summary>
            gerente ,
            /// <summary>
            /// 
            /// </summary>
            assistente
        }

        private TipoUsuario _Tipo = TipoUsuario.gerente;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void LookupGerente_Load(object sender, EventArgs e)
        {
            MudaTipo();            
        }

        private void MudaTipo()
        {
            label_F.Text = (Tipo == TipoUsuario.assistente) ? "Assistente" : "Gerente";
            if ((!this.DesignMode) && (BancoVirtual.Configurado(TiposDeBanco.SQL)))
            {
                if (Tipo == TipoUsuario.gerente)
                    uSUariosBindingSource.DataSource = dUSUarios.dUSUariosStGerentes;
                else
                    uSUariosBindingSource.DataSource = dUSUarios.dUSUariosStAssistentes;
            };
        }

        private void lookUpEdit1_Properties_EditValueChanged(object sender, EventArgs e)
        {
            if (block)
                return;
            try
            {
                block = true;
                USUSel = (int)lookUpEdit1.EditValue;
                Onalterado(e);
            }
            finally
            {
                block = false;
            }
        }

        private void lookUpEdit1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            USUSel = -1;
        }

    }
}
