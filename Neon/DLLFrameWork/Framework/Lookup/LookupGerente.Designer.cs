﻿namespace Framework.Lookup
{
    partial class LookupGerente
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label_F = new System.Windows.Forms.Label();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
     
            // 
            // label_F
            // 
            this.label_F.AutoSize = true;
            this.label_F.Location = new System.Drawing.Point(0, 3);
            this.label_F.Name = "label_F";
            this.label_F.Size = new System.Drawing.Size(46, 13);
            this.label_F.TabIndex = 4;
            this.label_F.Text = "Gerente";
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Location = new System.Drawing.Point(68, 0);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Undo)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USU Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit1.Properties.DataSource = this.uSUariosBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "USUNome";
            this.lookUpEdit1.Properties.NullText = "--";
            this.lookUpEdit1.Properties.ShowHeader = false;
            this.lookUpEdit1.Properties.ValueMember = "USU";
            this.lookUpEdit1.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit1_Properties_ButtonClick);
            this.lookUpEdit1.Properties.EditValueChanged += new System.EventHandler(this.lookUpEdit1_Properties_EditValueChanged);
            this.lookUpEdit1.Size = new System.Drawing.Size(216, 20);
            this.lookUpEdit1.TabIndex = 5;
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // LookupGerente
            // 
            this.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lookUpEdit1);
            this.Controls.Add(this.label_F);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "LookupGerente";
            this.Size = new System.Drawing.Size(290, 20);
            this.Load += new System.EventHandler(this.LookupGerente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        /// <summary>
        /// 
        /// </summary>
        protected System.Windows.Forms.Label label_F;
        /// <summary>
        /// 
        /// </summary>
        protected System.Windows.Forms.BindingSource uSUariosBindingSource;
    }
}
