﻿/*
LH - 30/01/2014        14.2.4.7  - Ajuste dos condominios ocultos
LH - 03/11/2015        15.1.7.31 - Recurso de filial
*/

using FrameworkProc;

namespace Framework.Lookup {

    /// <summary>
    /// Componente para busca de apartamentos
    /// </summary>
    partial class dComponente_F
    {
        private static dComponente_F _dComponente_FSt;
        private static dComponente_F _dComponente_FStOculto;
        //[System.Obsolete("Usar Filial")]
        //private static dComponente_F _dComponente_FStTotal;
        //[System.Obsolete("Usar Filial")]
        //private static dComponente_F _dComponente_FStTotalOculto;

        
        private static dComponente_F _dComponente_FFilialSt;        
        private static dComponente_F _dComponente_FFilialStOculto;

        /// <summary>
        /// dataset estático:dComponente_F
        /// </summary>
        public static dComponente_F dComponente_FSt
        {
            get
            {                
                if (_dComponente_FSt == null)
                    _dComponente_FSt = new dComponente_F(EMPTipo.Local, false);
                _dComponente_FSt.CarregaCON();                
                return _dComponente_FSt;
            }
        }

        /// <summary>
        /// dataset estático:dComponente_F
        /// </summary>
        public static dComponente_F dComponente_FFilialSt
        {
            get
            {
                if (_dComponente_FFilialSt == null)
                    _dComponente_FFilialSt = new dComponente_F(EMPTipo.Filial, false);
                _dComponente_FFilialSt.CarregaCON();
                return _dComponente_FFilialSt;
            }
        }

        /*
        /// <summary>
        /// dataset estático:dComponente_F com SA e SBC
        /// </summary>        
        public static dComponente_F dComponente_FStTotal
        {
            get
            {
                //System.Windows.Forms.MessageBox.Show("REF");
                if (_dComponente_FStTotal == null)
                    _dComponente_FStTotal = new dComponente_F(false);
                _dComponente_FStTotal.CarregaCON();
                return _dComponente_FStTotal;
            }
        }*/

        /// <summary>
        /// dataset estático:dComponente_F
        /// </summary>
        public static dComponente_F dComponente_FStOculto
        {
            get
            {                
                if (_dComponente_FStOculto == null)
                    _dComponente_FStOculto = new dComponente_F(EMPTipo.Local, true);
                _dComponente_FStOculto.CarregaCON();
                return _dComponente_FStOculto;
            }
        }

        /// <summary>
        /// dataset estático:dComponente_F
        /// </summary>
        public static dComponente_F dComponente_FFilialStOculto
        {
            get
            {                
                if (_dComponente_FFilialStOculto == null)
                    _dComponente_FFilialStOculto = new dComponente_F(EMPTipo.Filial, true);
                _dComponente_FFilialStOculto.CarregaCON();
                return _dComponente_FFilialStOculto;
            }
        }

        /*
        /// <summary>
        /// dataset estático:dComponente_F com SA e SBC
        /// </summary>        
        public static dComponente_F dComponente_FStTotalOculto
        {
            get
            {
                //System.Windows.Forms.MessageBox.Show("REF");
                if (_dComponente_FStTotalOculto == null)
                    _dComponente_FStTotalOculto = new dComponente_F(true);
                _dComponente_FStTotalOculto.CarregaCON();
                return _dComponente_FStTotalOculto;
            }
        }*/

        
        
        private bool Ocultos;
        private EMPTipo EMPTipo;

        /*
        /// <summary>
        /// Tipo de filial
        /// </summary>
        public enum EMPTipo 
        {
            /// <summary>
            /// Local
            /// </summary>
            Local, 
            /// <summary>
            /// Filial
            /// </summary>
            Filial 
        }*/

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPT">Filial</param>
        /// <param name="_Ocultos">Mostrar Ocultos</param>
        public dComponente_F(EMPTipo _EMPT, bool _Ocultos)
            : this()
        {            
            EMPTipo = _EMPT;
            Ocultos = _Ocultos;
        }


        /// <summary>
        /// Construtor
        /// </summary>        
        /// <param name="_Ocultos">Com os ocultos</param>               
        public dComponente_F(bool _Ocultos):this()            
        {            
            Ocultos = _Ocultos;
            EMPTipo = EMPTipo.Local;
        }

        /// <summary>
        /// Carrega os condomínios a partir do banco
        /// </summary>        
        internal void CarregaCON()
        {
            if ((CONDOMINIOS.Rows.Count == 0) && (VirDB.Bancovirtual.BancoVirtual.Configurado(VirDB.Bancovirtual.TiposDeBanco.SQL)))
            {
                int EMP = Framework.DSCentral.EMP;
                if (EMPTipo == EMPTipo.Filial)
                    EMP = EMP == 1 ? 3 : 1;
                dComponente_FTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapterX = (EMPTipo == EMPTipo.Local ? CONDOMINIOSTableAdapter : CONDOMINIOSTableAdapterF);

                if (!Framework.DSCentral.FiltrarEMP)
                {
                    if (Ocultos)
                        CONDOMINIOSTableAdapterX.Fill(CONDOMINIOS);
                    else
                        CONDOMINIOSTableAdapterX.FillLivre(CONDOMINIOS);
                }
                else
                {
                    if (Ocultos)
                        CONDOMINIOSTableAdapterX.FillByEMP(CONDOMINIOS, EMP);
                    else
                        CONDOMINIOSTableAdapterX.FillByEMPLivre(CONDOMINIOS, EMP);
                }
            };
        }

        #region TableAdapters

        private dComponente_FTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS (Filial)
        /// </summary>
        public dComponente_FTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapterF
        {
            get
            {
                if (cONDOMINIOSTableAdapterF == null)
                {
                    cONDOMINIOSTableAdapterF = new dComponente_FTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                return cONDOMINIOSTableAdapterF;
            }
        }

        private dComponente_FTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dComponente_FTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dComponente_FTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dComponente_FTableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BLOCOS
        /// </summary>
        public dComponente_FTableAdapters.BLOCOSTableAdapter BLOCOSTableAdapter
        {
            get
            {
                if (bLOCOSTableAdapter == null)
                {
                    bLOCOSTableAdapter = new dComponente_FTableAdapters.BLOCOSTableAdapter();
                    bLOCOSTableAdapter.TrocarStringDeConexao();
                    bLOCOSTableAdapter.ClearBeforeFill = false;
                };
                return bLOCOSTableAdapter;
            }
        }

        private dComponente_FTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dComponente_FTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dComponente_FTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao();
                    aPARTAMENTOSTableAdapter.ClearBeforeFill = false;
                };
                return aPARTAMENTOSTableAdapter;
            }
        }
        
        #endregion

    }
}
