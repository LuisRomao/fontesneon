﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using VirDB.Bancovirtual;
using FrameworkProc.datasets;

namespace Framework.Lookup
{
    /// <summary>
    /// Componente para seleção de usuários.
    /// </summary>
    public partial class LookupUSUAtivos : LookupGerente
    {
        /// <summary>
        /// 
        /// </summary>
        public LookupUSUAtivos()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void LookupGerente_Load(object sender, EventArgs e)
        {
            if ((!this.DesignMode) && (VirDB.Bancovirtual.BancoVirtual.Configurado(TiposDeBanco.SQL)))
            {
                if (SoUsuariosAtivos)
                    uSUariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
                else
                    uSUariosBindingSource.DataSource = dUSUarios.dUSUariosStTodos;
                //lkpCodCon_F.Properties.NullText = "";                
            };
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Somente os usuários ativos")]
        public bool SoUsuariosAtivos
        {
            get { return soUsuariosAtivos; }
            set { soUsuariosAtivos = value; }
        }

        private bool soUsuariosAtivos = true;
    }
}
