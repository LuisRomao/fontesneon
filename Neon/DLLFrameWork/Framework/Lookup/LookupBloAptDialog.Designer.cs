﻿namespace Framework.Lookup
{
    partial class LookupBloAptDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LookupBloAptDialog));
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.CON_selrow = null;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(3, 39);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.NivelCONOculto = 2;
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(461, 57);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 4;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = null;
            // 
            // LookupBloAptDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lookupBlocosAptos_F1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "LookupBloAptDialog";
            this.Size = new System.Drawing.Size(467, 111);
            this.Controls.SetChildIndex(this.lookupBlocosAptos_F1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LookupBlocosAptos_F lookupBlocosAptos_F1;
    }
}
