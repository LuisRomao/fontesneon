﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace Framework.Lookup
{
    /// <summary>
    /// Diálogo para selecionar APT
    /// </summary>
    public partial class LookupBloAptDialog : ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor padrão
        /// </summary>
        public LookupBloAptDialog()
        {
            InitializeComponent();
        }

        private int? CONTravado;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON"></param>
        public LookupBloAptDialog(int CON):this()
        {            
            CONTravado = CON;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if(CONTravado.HasValue)
                lookupBlocosAptos_F1.TravaCondominio(CONTravado.Value);       
        }

        /// <summary>
        /// Apartamento selecionado
        /// </summary>
        public int? APT_Sel
        {
            get { return lookupBlocosAptos_F1.APT_Sel == -1 ? (int?)null : lookupBlocosAptos_F1.APT_Sel; }
        }

        /// <summary>
        /// BLOCO selecionado
        /// </summary>
        public int? BLO_Sel
        {
            get { return lookupBlocosAptos_F1.BLO_Sel == -1 ? (int?)null : lookupBlocosAptos_F1.BLO_Sel; }
        }

        /// <summary>
        /// Condominio selecionado
        /// </summary>
        public int? CON_Sel
        {
            get { return lookupBlocosAptos_F1.CON_sel == -1 ? (int?)null : lookupBlocosAptos_F1.CON_sel; }
        }

        /// <summary>
        /// Permite fechar se o apartamento for escolhido
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            if (!base.CanClose())
                return false;
            else
                if (CONTravado.HasValue && (CONTravado.Value != CON_Sel))
                    return false;
                else
                    return APT_Sel.HasValue;
        
        }
    }
}
