using System;
using System.ComponentModel;
using System.Data;
using VirDB.Bancovirtual;
using FrameworkProc;

namespace Framework.Lookup
{
    /// <summary>
    /// 
    /// </summary>
    public partial class LookupCondominio_F : CompontesBasicos.ComponenteBaseBindingSource
    {

        //private bool total;
        private int nivelCONOculto = 2;

        /// <summary>
        /// Nivel oculto
        /// </summary>
        [Category("Virtual Software")]
        [Description("Nivel de condominios ocultos")]
        public int NivelCONOculto
        {
            get
            {
                return nivelCONOculto;
            }
            set
            {
                nivelCONOculto = value;
            }
        }

        /*
        /// <summary>
        /// Mostrar condominos da filial
        /// </summary>
        [Category("Virtual Software")]
        [Description("SBC e SA")]        
        public bool Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
            }
        }*/

        private EMPTipo _Filial;

        /// <summary>
        /// Mostrar condominos locais ou da filial
        /// </summary>
        [Category("Virtual Software")]
        [Description("Local ou Filial")]
        [DefaultValue(EMPTipo.Local)]
        public EMPTipo Filial 
        { 
            get
            {
                return _Filial;
            } 
            set
            {
                bool Recarga = (_Filial != value);
                _Filial = value;
                if (Recarga)
                    EMPAlterado();
            } 
        }

        /// <summary>
        /// Altera��o na empresa
        /// </summary>
        virtual protected void EMPAlterado()
        {
            if (dataSetCondominios != null)
            {
                dataSetCondominios = null;
                this.lkpNome_F.EditValue = null;
                this.lkpCodCon_F.EditValue = null;
                CON_selrow = null;
                dComponenteFBindingSource.DataSource = DataSetCondominios;
            }
        }

        #region Eventos
        /// <summary>
        /// Evento que ocorre quando o condom�nio � alterado
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento na altera��o das op��es")]
        public event EventHandler alterado;

        /// <summary>
        /// Evento que ocorre quando o condom�nio � alterado
        /// </summary>
        /// <param name="e"></param>
        protected void Onalterado(EventArgs e)
        {
            if (alterado != null)
                alterado(this, e);
        }

        #endregion

        /// <summary>
        /// Construtor
        /// </summary>
        public LookupCondominio_F()
        {            
            InitializeComponent();
        }

        private Framework.Lookup.dComponente_F dataSetCondominios;

        private Framework.Lookup.dComponente_F DataSetCondominios
        {
            get
            {
                if (dataSetCondominios == null)
                {
                    if (Filial == EMPTipo.Local)
                    {
                        if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CONDOCULTOS") >= NivelCONOculto)
                            dataSetCondominios = dComponente_F.dComponente_FStOculto;
                        else
                            dataSetCondominios = dComponente_F.dComponente_FSt;
                    }
                    else
                    {
                        if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CONDOCULTOS") >= NivelCONOculto)
                            dataSetCondominios = dComponente_F.dComponente_FFilialStOculto;
                        else
                            dataSetCondominios = dComponente_F.dComponente_FFilialSt;
                    }

                };
                return dataSetCondominios;
            }
        }

        private void LookupCondominio_F_Load(object sender, EventArgs e)
        {
            
            if ((!this.DesignMode) && (VirDB.Bancovirtual.BancoVirtual.Configurado(TiposDeBanco.SQL)))
            {                
                dComponenteFBindingSource.DataSource = DataSetCondominios;
                lkpCodCon_F.Properties.NullText = "";
                lkpNome_F.Properties.NullText = "";
            };
        }

        #region Variaveis de estado

        /// <summary>
        /// Linha do condom�nio selecionado
        /// </summary>
        protected dComponente_F.CONDOMINIOSRow cON_sel = null;

        private int _con;

        /// <summary>
        /// Condom�nio selecionado
        /// </summary>
        /// <remarks>pode ser null</remarks>
        public dComponente_F.CONDOMINIOSRow CON_selrow {
            set
            {
                cON_sel = value;
                if (cON_sel != null)
                    _con = cON_sel.CON;
                else
                    _con = -1;
            }
            get 
            {
                if ((cON_sel != null) && (cON_sel.RowState == DataRowState.Detached) && (_con != -1))
                {
                    cON_sel = dComponente_F.dComponente_FSt.CONDOMINIOS.FindByCON(_con);
                }
                return cON_sel;
            }            
        }

        /// <summary>
        /// Condom�nio selecionado
        /// </summary>
        public Int32 CON_sel
        {
            get
            {
                return (CON_selrow == null) ? -1 : CON_selrow.CON;
            }
            set
            {
                _con = value;
                if (lkpCodCon_F != null)
                    lkpCodCon_F.EditValue = value;
            }            
        }

        
        #endregion       

        /// <summary>
        /// Bloqueio para evitar re-entrada
        /// </summary>
        protected bool bloc=false;

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnalteradoInterno() 
        { 
        }

        /// <summary>
        /// Trava um condom�nio
        /// </summary>
        /// <param name="CON"></param>
        public void TravaCondominio(int CON)
        {
            CON_sel = CON;
            lkpCodCon_F.Properties.ReadOnly = true;
            lkpNome_F.Properties.ReadOnly = true;
            OnalteradoInterno();
        }

        

        private void lkpCondominio_F_EditValueChanged(object sender, EventArgs e)
        {
            if (!bloc)
            {
                try
                {
                    bloc = true;
                    DevExpress.XtraEditors.LookUpEdit Chamador = (DevExpress.XtraEditors.LookUpEdit)(sender);
                    object oCON = Chamador.GetColumnValue("CON");
                    if (oCON != null)
                    {
                        Int32 _CON = Convert.ToInt32(oCON);
                        
                        this.lkpNome_F.EditValue = _CON;
                        this.lkpCodCon_F.EditValue = _CON;
                        if (_CON > 0)
                            CON_selrow = DataSetCondominios.CONDOMINIOS.FindByCON(_CON);
                        else
                            CON_selrow = null;

                    }
                    else
                    {
                        this.lkpNome_F.EditValue = null;                        
                        this.lkpCodCon_F.EditValue = null;                        
                        cON_sel = null;
                        
                        
                    };
                    OnalteradoInterno();
                    Onalterado(e);
                    
                }
                finally {
                    bloc = false;
                }
            }
        }

        

        private void lkpCodCon_F_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1)
            {                
                this.lkpCodCon_F.EditValue = null;                
                this.lkpNome_F.EditValue = null;                
                if (lkpCodCon_F.IsPopupOpen)
                {
                    this.lkpCodCon_F.Text = "";
                    this.lkpCodCon_F.ClosePopup();                    
                };
                this.lkpNome_F.ClosePopup();
                dComponente_F.dComponente_FSt.APARTAMENTOS.Clear();
                dComponente_F.dComponente_FSt.BLOCOS.Clear();
                dComponente_F.dComponente_FSt.CONDOMINIOS.Clear();
                DataSetCondominios.CarregaCON();
            };
        }
    }
}