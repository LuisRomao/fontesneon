namespace Framework.Lookup
{
    partial class ctrLookupCondominio_F
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lkpCodCon_F = new DevExpress.XtraEditors.LookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dComponente_F = new Framework.Lookup.dComponente_F();
            this.lkpNome_F = new DevExpress.XtraEditors.LookUpEdit();
            this.label_F = new System.Windows.Forms.Label();
            this.cONDOMINIOSTableAdapter = new Framework.Lookup.dComponente_FTableAdapters.CONDOMINIOSTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCodCon_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dComponente_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpNome_F.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lkpCodCon_F
            // 
            this.lkpCodCon_F.Location = new System.Drawing.Point(67, 0);
            this.lkpCodCon_F.Name = "lkpCodCon_F";
            this.lkpCodCon_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpCodCon_F.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CODCON", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lkpCodCon_F.Properties.DataSource = this.cONDOMINIOSBindingSource;
            this.lkpCodCon_F.Properties.DisplayMember = "CONCodigo";
            this.lkpCodCon_F.Properties.ValueMember = "CONCodigo";
            this.lkpCodCon_F.Size = new System.Drawing.Size(96, 20);
            this.lkpCodCon_F.TabIndex = 0;
            this.lkpCodCon_F.EditValueChanged += new System.EventHandler(this.lkpCondominio_F_EditValueChanged);
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dComponente_F;
            // 
            // dComponente_F
            // 
            this.dComponente_F.DataSetName = "dComponente_F";
            this.dComponente_F.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lkpNome_F
            // 
            this.lkpNome_F.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lkpNome_F.Location = new System.Drawing.Point(169, 0);
            this.lkpNome_F.Name = "lkpNome_F";
            this.lkpNome_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpNome_F.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "Nome", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lkpNome_F.Properties.DataSource = this.cONDOMINIOSBindingSource;
            this.lkpNome_F.Properties.DisplayMember = "CONNome";
            this.lkpNome_F.Properties.ValueMember = "CON";
            this.lkpNome_F.Size = new System.Drawing.Size(139, 20);
            this.lkpNome_F.TabIndex = 2;
            this.lkpNome_F.EditValueChanged += new System.EventHandler(this.lkpCondominio_F_EditValueChanged);
            // 
            // label_F
            // 
            this.label_F.AutoSize = true;
            this.label_F.Location = new System.Drawing.Point(-3, 7);
            this.label_F.Name = "label_F";
            this.label_F.Size = new System.Drawing.Size(64, 13);
            this.label_F.TabIndex = 3;
            this.label_F.Text = "Condomínio";
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            // 
            // ctrLookupCondominio_F
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label_F);
            this.Controls.Add(this.lkpNome_F);
            this.Controls.Add(this.lkpCodCon_F);
            this.Name = "ctrLookupCondominio_F";
            this.Size = new System.Drawing.Size(308, 20);
            this.Load += new System.EventHandler(this.uctLookupCondominio_F_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lkpCodCon_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dComponente_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpNome_F.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private dComponente_F dComponente_F;
        private Framework.Lookup.dComponente_FTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        protected DevExpress.XtraEditors.LookUpEdit lkpCodCon_F;
        protected System.Windows.Forms.Label label_F;
        protected DevExpress.XtraEditors.LookUpEdit lkpNome_F;
    }
}
