using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using VirDB.Bancovirtual;

namespace Framework.Lookup
{
    /// <summary>
    /// Componente para escolha de bloco/apartamento
    /// </summary>
    public partial class LookupBlocosAptos_F : Framework.Lookup.LookupCondominio_F
    {
        #region Eventos
        /// <summary>
        /// Evento que ocorre quando o BLOCO � alterado
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento na altera��o do bloco")]
        public event EventHandler alteradoBLOCO;

        /// <summary>
        /// Evento que ocorre quando o BLOCO � alterado
        /// </summary>
        /// <param name="e"></param>
        private void OnalteradoBLOCO(EventArgs e)
        {
            if (alteradoBLOCO != null)
                alteradoBLOCO(this, e);
            else
                //Compatibilidade (quando s� existia o evento alterado)
                if (alteradoAPT == null)
                    Onalterado(e);
        }

        /// <summary>
        /// Evento que ocorre quando o apartamento � alterado
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento que ocorre quando o apartamento � alterado")]
        public event EventHandler alteradoAPT;

        /// <summary>
        /// Evento que ocorre quando o apartamento � alterado
        /// </summary>
        /// <param name="e"></param>
        private void OnalteradoAPT(EventArgs e)
        {
            if (alteradoAPT != null)
                alteradoAPT(this, e);
            else
            {
                //Compatibilidade
                Onalterado(e);
            }
        }
        
        #endregion
        
        /// <summary>
        /// Construtor
        /// </summary>
        public LookupBlocosAptos_F()
        {            
            InitializeComponent();            
            if (VirDB.Bancovirtual.BancoVirtual.Configurado(TiposDeBanco.SQL))
            {
                dComponenteFBindingSourceBLOCO.DataSource = dComponente_F.dComponente_FSt;
                ApartamentosBindingSource.DataSource = dComponente_F.dComponente_FSt;
            };            
        }

        #region Variaveis de estado
        private dComponente_F.BLOCOSRow blo_Sel = null;
        private dComponente_F.APARTAMENTOSRow apt_Sel = null;

        private int? _blo = null;
        private int? _apt = null;

        /// <summary>
        /// Dados do apartamento selecionado
        /// </summary>
        /// <remarks>pode ser null</remarks>
        public dComponente_F.APARTAMENTOSRow APT_Selrow {
            get 
            {
                if ((apt_Sel != null) && (apt_Sel.RowState == DataRowState.Detached))
                    apt_Sel = null;
                if((apt_Sel == null) && (_apt.HasValue))
                {
                    apt_Sel = dComponente_F.dComponente_FSt.APARTAMENTOS.FindByAPT(_apt.Value);
                    if (apt_Sel == null)
                    {
                        if (BLO_Selrow != null)
                            apt_Sel = dComponente_F.dComponente_FSt.APARTAMENTOS.FindByAPT(_apt.Value);
                    }
                }
                return apt_Sel;
            }
        }

        /// <summary>
        /// Bloco selecionado
        /// </summary>
        /// <remarks>pode ser null</remarks>
        public dComponente_F.BLOCOSRow BLO_Selrow {
            get 
            {
                if ((blo_Sel != null) && (blo_Sel.RowState == DataRowState.Detached))
                    blo_Sel = null;
                if ((blo_Sel == null) && (_blo.HasValue))
                {
                    blo_Sel = dComponente_F.dComponente_FSt.BLOCOS.FindByBLO(_blo.Value);
                    if (blo_Sel == null)
                    {
                        dComponente_F.dComponente_FSt.BLOCOSTableAdapter.Fill(dComponente_F.dComponente_FSt.BLOCOS, CON_sel);                        
                        if (cON_sel.GetAPARTAMENTOSRows().Length == 0)
                            dComponente_F.dComponente_FSt.APARTAMENTOSTableAdapter.FillByCON(dComponente_F.dComponente_FSt.APARTAMENTOS, CON_sel);
                        blo_Sel = dComponente_F.dComponente_FSt.BLOCOS.FindByBLO(_blo.Value);
                    }
                }
                return blo_Sel;
            }
        }

        /// <summary>
        /// Bloco selecionado
        /// </summary>
        public Int32 BLO_Sel
        {
            get
            {
                return _blo.GetValueOrDefault(-1);
            }
            set {
                if (!VirDB.Bancovirtual.BancoVirtual.Configurado(TiposDeBanco.SQL))
                    return;
                blo_Sel = dComponente_F.dComponente_FSt.BLOCOS.FindByBLO(value);                
                if (blo_Sel != null) {
                    CON_sel = blo_Sel.BLO_CON;
                    lkpBLOCodigo_F.EditValue = value;
                    _blo = value;
                }
                else{
                    object oCON = dComponente_F.dComponente_FSt.BLOCOSTableAdapter.BucasCON(value);
                    CON_sel = (oCON == null) ? -1 : (int)oCON;
                    lkpBLOCodigo_F.EditValue = value;
                }                
            }
        }

        /// <summary>
        /// Apartamento selecionado
        /// </summary>
        public Int32 APT_Sel
        {
            get
            {
                return _apt.GetValueOrDefault(-1);
            }
            set
            {
                if (!VirDB.Bancovirtual.BancoVirtual.Configurado(TiposDeBanco.SQL))
                    return;
                apt_Sel = dComponente_F.dComponente_FSt.APARTAMENTOS.FindByAPT(value);
                
                if (apt_Sel != null)
                {
                    BLO_Sel = apt_Sel.APT_BLO;
                    lkpApartamento.EditValue = value;
                    _apt = value;
                }
                else
                {
                    _apt = null;
                    object oBLO = dComponente_F.dComponente_FSt.APARTAMENTOSTableAdapter.BuscaBLO(value);
                    if (oBLO == null)
                    {
                        BLO_Sel = -1;
                        _blo = null;
                    }
                    else
                    {
                        BLO_Sel = (int)oBLO;
                        lkpApartamento.EditValue = value;
                        
                    }
                }
            }
        }
        
        #endregion
             

        private void CondominioAlterado() {            
            //if (!bloc)
            //{


                blo_Sel = null;
                apt_Sel = null;

                if (cON_sel != null)
                {
                    dComponente_F.BLOCOSRow[] BlocosDoCondominio = cON_sel.GetBLOCOSRows();
                    if (BlocosDoCondominio.Length == 0)
                    {                        
                        dComponente_F.dComponente_FSt.BLOCOSTableAdapter.Fill(dComponente_F.dComponente_FSt.BLOCOS, CON_sel);
                        BlocosDoCondominio = cON_sel.GetBLOCOSRows();
                        if (cON_sel.GetAPARTAMENTOSRows().Length == 0)
                            dComponente_F.dComponente_FSt.APARTAMENTOSTableAdapter.FillByCON(dComponente_F.dComponente_FSt.APARTAMENTOS, CON_sel);
                    };

                    ApartamentosBindingSource.Filter = dComponenteFBindingSourceBLOCO.Filter = "BLO_CON = " + CON_sel.ToString();

                    if (BlocosDoCondominio.Length == 1)
                    {

                        blo_Sel = BlocosDoCondominio[0];
                        lkpBLOCodigo_F.EditValue = blo_Sel.BLO;
                        lkpBLOCodigo_F.Enabled = false;
                        lkpBLONome_F.EditValue = blo_Sel.BLO;
                        lkpBLOCodigo_F.Enabled = false;
                        lkpBLONome_F.Enabled = false;
                        lkpApartamento.Enabled = true;
                    }
                    else
                    {
                        //blocBLOCO = true;
                        lkpBLOCodigo_F.Enabled = true;
                        lkpBLOCodigo_F.EditValue = null;
                        lkpBLONome_F.Enabled = true;
                        //lkpBLONome_F.EditValue = null;                        
                        lkpApartamento.EditValue = null;
                        lkpApartamento.Enabled = true;
                        //blocBLOCO = false;
                    }
                }
                else{
                    lkpBLOCodigo_F.EditValue = null;
                    lkpBLOCodigo_F.Enabled = false;
                    //lkpBLONome_F.EditValue = null;
                    //lkpBLOCodigo_F.Enabled = false;
                    lkpBLONome_F.Enabled = false;                    
                    lkpApartamento.Enabled = false;
                }

            //};
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnalteradoInterno()
        {
            CondominioAlterado();
        }

                
        private void LookupBlocosAptos_F_Load(object sender, EventArgs e)
        {
            
            //dComponenteFBindingSourceBLOCO.DataSource = dComponente_F.dComponente_FSt;
            //dComponenteFBindingSource1.DataSource = dComponente_F.dComponente_FSt;
            lkpBLOCodigo_F.Enabled = false;
            lkpBLONome_F.Enabled = false;
            lkpApartamento.Enabled = false;        
        }


        private bool travaFiltroAPT = false;

        private void lkpBlocos_F_EditValueChanged(object sender, EventArgs e)
        {
            
            if (!blocBLOCO)
            {
                try
                {
                    blocBLOCO = true;
                    DevExpress.XtraEditors.LookUpEdit Chamador = (DevExpress.XtraEditors.LookUpEdit)(sender);
                    object oBLO = Chamador.GetColumnValue("BLO");
                    if (oBLO != null)
                    {
                        Int32 _BLO = Convert.ToInt32(oBLO);

                        this.lkpBLOCodigo_F.EditValue = _BLO;
                        this.lkpBLONome_F.EditValue = _BLO;
                        if (_BLO > 0)
                        {
                            blo_Sel = dComponente_F.dComponente_FSt.BLOCOS.FindByBLO(_BLO);
                            _blo = blo_Sel.BLO;
                        }
                        else
                        {
                            blo_Sel = null;
                            _blo = null;
                        }   
                        if (!travaFiltroAPT)
                        {
                            apt_Sel = null;
                            _apt = null;
                            ApartamentosBindingSource.Filter = "APT_BLO = " + BLO_Sel.ToString();
                        }

                    }
                    else
                    {
                        this.lkpBLOCodigo_F.EditValue = null;
                        //this.lkpBLOCodigo_F.Text = "";
                        this.lkpBLONome_F.EditValue = null;
                        //this.lkpBLONome_F.Text = "";
                        if (lkpBLOCodigo_F.IsPopupOpen)
                        {
                            this.lkpBLOCodigo_F.Text = "";
                            this.lkpBLOCodigo_F.ClosePopup();
                        };
                        if (lkpBLONome_F.IsPopupOpen)
                        {
                            this.lkpBLONome_F.Text = "";
                            this.lkpBLONome_F.ClosePopup();
                        };
                        //lkpApartamento.EditValue = null;
                        this.blo_Sel = null;
                        this.apt_Sel = null;
                        _apt = null;
                        _blo = null;

                    };

                    OnalteradoBLOCO(e);
                    if(!travaFiltroAPT)
                        lkpApartamento.EditValue = null;
                }
                finally
                {
                    blocBLOCO = false;
                };
            };
            
        }

        private bool BlocAPT = false;

        private void lkpApartamento_EditValueChanged(object sender, EventArgs e)
        {
            if (BlocAPT)
                return;

            BlocAPT = true;

            object oAPT = this.lkpApartamento.GetColumnValue("APT_BLO");
            if (oAPT != null)
            {
                int _APT = Convert.ToInt32(this.lkpApartamento.GetColumnValue("APT"));
                travaFiltroAPT = true;
                this.lkpBLOCodigo_F.EditValue = Convert.ToInt32(oAPT);
                travaFiltroAPT = false;

                if (_APT > 0)
                {
                    apt_Sel = dComponente_F.dComponente_FSt.APARTAMENTOS.FindByAPT(_APT);
                    _apt = _APT;
                }
                else
                {
                    apt_Sel = null;
                    _apt = null;
                }
            }
            else
            {
                this.lkpApartamento.EditValue = null;
                apt_Sel = null;
                if (lkpApartamento.IsPopupOpen)
                {
                    this.lkpApartamento.Text = "";
                    this.lkpApartamento.ClosePopup();
                };
            }    
            OnalteradoAPT(e);
            BlocAPT = false;  
        }

        private bool blocBLOCO = false;

        private void lkpBLOCodigo_F_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {            
            if (e.Button.Index == 1)            
                CondominioAlterado();                           
        }

        private void lkpApartamento_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            
            if (e.Button.Index == 1)
            {
                this.lkpApartamento.EditValue = null;
                
                if (this.lkpApartamento.IsPopupOpen)
                {
                    this.lkpApartamento.Text = "";
                    this.lkpApartamento.ClosePopup();
                };
                
            };
            
        }
    }
}

