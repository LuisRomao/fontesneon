using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Framework.Lookup
{
    public partial class ctrLookupCondominio_F : UserControl
    {
        public ctrLookupCondominio_F()
        {
            InitializeComponent();
        }

        private void uctLookupCondominio_F_Load(object sender, EventArgs e)
        {
            cONDOMINIOSTableAdapter.Fill(dComponente_F.CONDOMINIOS);
        }

        private void lkpCondominio_F_EditValueChanged(object sender, EventArgs e)
        {
            if (sender is DevExpress.XtraEditors.LookUpEdit)
            {
                if (((DevExpress.XtraEditors.LookUpEdit)(sender)) == lkpCodCon_F)
                    lkpNome_F.EditValue = ((DevExpress.XtraEditors.LookUpEdit)(sender)).EditValue;
                else
                    lkpCodCon_F.EditValue = ((DevExpress.XtraEditors.LookUpEdit)(sender)).EditValue; ;
            }
        }
    }
}