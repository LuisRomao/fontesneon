namespace Framework.Lookup
{
    partial class LookupBlocosAptos_F
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lkpBLOCodigo_F = new DevExpress.XtraEditors.LookUpEdit();
            this.dComponenteFBindingSourceBLOCO = new System.Windows.Forms.BindingSource(this.components);
            this.labBloco_F = new System.Windows.Forms.Label();
            this.lkpBLONome_F = new DevExpress.XtraEditors.LookUpEdit();
            this.labApartamento_F = new System.Windows.Forms.Label();
            this.lkpApartamento = new DevExpress.XtraEditors.LookUpEdit();
            this.ApartamentosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lkpCodCon_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpNome_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBLOCodigo_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dComponenteFBindingSourceBLOCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBLONome_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpApartamento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApartamentosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lkpCodCon_F
            // 
            // 
            // lkpNome_F
            // 
            this.lkpNome_F.Size = new System.Drawing.Size(330, 20);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // lkpBLOCodigo_F
            // 
            this.lkpBLOCodigo_F.Location = new System.Drawing.Point(67, 26);
            this.lkpBLOCodigo_F.Name = "lkpBLOCodigo_F";
            this.lkpBLOCodigo_F.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lkpBLOCodigo_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Undo)});
            this.lkpBLOCodigo_F.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLO", "BLO", 38, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLOCodigo", "BLOCodigo", 58, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpBLOCodigo_F.Properties.DataSource = this.dComponenteFBindingSourceBLOCO;
            this.lkpBLOCodigo_F.Properties.DisplayMember = "BLOCodigo";
            this.lkpBLOCodigo_F.Properties.NullText = "";
            this.lkpBLOCodigo_F.Properties.ValueMember = "BLO";
            this.lkpBLOCodigo_F.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpBLOCodigo_F_Properties_ButtonClick);
            this.lkpBLOCodigo_F.Size = new System.Drawing.Size(96, 20);
            this.lkpBLOCodigo_F.TabIndex = 4;
            this.lkpBLOCodigo_F.EditValueChanged += new System.EventHandler(this.lkpBlocos_F_EditValueChanged);
            // 
            // dComponenteFBindingSourceBLOCO
            // 
            this.dComponenteFBindingSourceBLOCO.DataMember = "BLOCOS";
            this.dComponenteFBindingSourceBLOCO.DataSource = typeof(Framework.Lookup.dComponente_F);
            // 
            // labBloco_F
            // 
            this.labBloco_F.AutoSize = true;
            this.labBloco_F.Location = new System.Drawing.Point(-3, 29);
            this.labBloco_F.Name = "labBloco_F";
            this.labBloco_F.Size = new System.Drawing.Size(32, 13);
            this.labBloco_F.TabIndex = 5;
            this.labBloco_F.Text = "Bloco";
            // 
            // lkpBLONome_F
            // 
            this.lkpBLONome_F.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lkpBLONome_F.Location = new System.Drawing.Point(169, 26);
            this.lkpBLONome_F.Name = "lkpBLONome_F";
            this.lkpBLONome_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpBLONome_F.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLO", "BLO", 38, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLONome", "BLONome", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpBLONome_F.Properties.DataSource = this.dComponenteFBindingSourceBLOCO;
            this.lkpBLONome_F.Properties.DisplayMember = "BLONome";
            this.lkpBLONome_F.Properties.NullText = "";
            this.lkpBLONome_F.Properties.ValueMember = "BLO";
            this.lkpBLONome_F.Size = new System.Drawing.Size(174, 20);
            this.lkpBLONome_F.TabIndex = 6;
            this.lkpBLONome_F.EditValueChanged += new System.EventHandler(this.lkpBlocos_F_EditValueChanged);
            // 
            // labApartamento_F
            // 
            this.labApartamento_F.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labApartamento_F.AutoSize = true;
            this.labApartamento_F.Location = new System.Drawing.Point(349, 29);
            this.labApartamento_F.Name = "labApartamento_F";
            this.labApartamento_F.Size = new System.Drawing.Size(70, 13);
            this.labApartamento_F.TabIndex = 7;
            this.labApartamento_F.Tag = "";
            this.labApartamento_F.Text = "Apartamento";
            // 
            // lkpApartamento
            // 
            this.lkpApartamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lkpApartamento.Location = new System.Drawing.Point(416, 26);
            this.lkpApartamento.Name = "lkpApartamento";
            this.lkpApartamento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Undo)});
            this.lkpApartamento.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("APT", "APT", 38, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLOCodigo", "Bloco", 30, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("APTNumero", "N�mero", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("APT_BLO", "Name7", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.lkpApartamento.Properties.DataSource = this.ApartamentosBindingSource;
            this.lkpApartamento.Properties.DisplayMember = "APTNumero";
            this.lkpApartamento.Properties.NullText = "";
            this.lkpApartamento.Properties.SortColumnIndex = 1;
            this.lkpApartamento.Properties.ValueMember = "APT";
            this.lkpApartamento.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpApartamento_Properties_ButtonClick);
            this.lkpApartamento.Size = new System.Drawing.Size(83, 20);
            this.lkpApartamento.TabIndex = 8;
            this.lkpApartamento.EditValueChanged += new System.EventHandler(this.lkpApartamento_EditValueChanged);
            // 
            // ApartamentosBindingSource
            // 
            this.ApartamentosBindingSource.DataMember = "APARTAMENTOS";
            this.ApartamentosBindingSource.DataSource = typeof(Framework.Lookup.dComponente_F);
            // 
            // LookupBlocosAptos_F
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.lkpApartamento);
            this.Controls.Add(this.labApartamento_F);
            this.Controls.Add(this.lkpBLONome_F);
            this.Controls.Add(this.lkpBLOCodigo_F);
            this.Controls.Add(this.labBloco_F);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "LookupBlocosAptos_F";
            this.Size = new System.Drawing.Size(533, 50);
            this.Load += new System.EventHandler(this.LookupBlocosAptos_F_Load);
            this.Controls.SetChildIndex(this.lkpCodCon_F, 0);
            this.Controls.SetChildIndex(this.lkpNome_F, 0);
            this.Controls.SetChildIndex(this.labBloco_F, 0);
            this.Controls.SetChildIndex(this.lkpBLOCodigo_F, 0);
            this.Controls.SetChildIndex(this.lkpBLONome_F, 0);
            this.Controls.SetChildIndex(this.labApartamento_F, 0);
            this.Controls.SetChildIndex(this.lkpApartamento, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lkpCodCon_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpNome_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBLOCodigo_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dComponenteFBindingSourceBLOCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBLONome_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpApartamento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApartamentosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private System.Windows.Forms.Label labBloco_F;
        private System.Windows.Forms.Label labApartamento_F;
        private System.Windows.Forms.BindingSource dComponenteFBindingSourceBLOCO;
        private DevExpress.XtraEditors.LookUpEdit lkpBLOCodigo_F;
        private DevExpress.XtraEditors.LookUpEdit lkpBLONome_F;
        private DevExpress.XtraEditors.LookUpEdit lkpApartamento;
        /// <summary>
        /// BindingSource dos apartamentos filtrados pelo componente
        /// </summary>
        public System.Windows.Forms.BindingSource ApartamentosBindingSource;

    }
}
