namespace Framework.Lookup
{
    /// <summary>
    /// Componente para sele��o de condom�nio
    /// </summary>
    /// <remarks>� o componente pai do seletor de apartamentos</remarks>
    partial class LookupCondominio_F
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lkpCodCon_F = new DevExpress.XtraEditors.LookUpEdit();
            this.dComponenteFBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lkpNome_F = new DevExpress.XtraEditors.LookUpEdit();
            this.label_F = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCodCon_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dComponenteFBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpNome_F.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // lkpCodCon_F
            // 
            this.lkpCodCon_F.Location = new System.Drawing.Point(67, 0);
            this.lkpCodCon_F.Name = "lkpCodCon_F";
            this.lkpCodCon_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Undo)});
            this.lkpCodCon_F.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CON", "", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CODCON", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", 60, "Nome")});
            this.lkpCodCon_F.Properties.DataSource = this.dComponenteFBindingSource;
            this.lkpCodCon_F.Properties.DisplayMember = "CONCodigo";
            this.lkpCodCon_F.Properties.NullText = "";
            this.lkpCodCon_F.Properties.PopupWidth = 300;
            this.lkpCodCon_F.Properties.SortColumnIndex = 1;
            this.lkpCodCon_F.Properties.ValueMember = "CON";
            this.lkpCodCon_F.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpCodCon_F_Properties_ButtonClick);
            this.lkpCodCon_F.Size = new System.Drawing.Size(96, 20);
            this.lkpCodCon_F.TabIndex = 0;
            this.lkpCodCon_F.EditValueChanged += new System.EventHandler(this.lkpCondominio_F_EditValueChanged);
            // 
            // dComponenteFBindingSource
            // 
            this.dComponenteFBindingSource.DataMember = "CONDOMINIOS";
            this.dComponenteFBindingSource.DataSource = typeof(Framework.Lookup.dComponente_F);
            // 
            // lkpNome_F
            // 
            this.lkpNome_F.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lkpNome_F.Location = new System.Drawing.Point(169, 0);
            this.lkpNome_F.Name = "lkpNome_F";
            this.lkpNome_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpNome_F.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CON", "", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "Nome", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lkpNome_F.Properties.DataSource = this.dComponenteFBindingSource;
            this.lkpNome_F.Properties.DisplayMember = "CONNome";
            this.lkpNome_F.Properties.NullText = "";
            this.lkpNome_F.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.lkpNome_F.Properties.ValueMember = "CON";
            this.lkpNome_F.Size = new System.Drawing.Size(139, 20);
            this.lkpNome_F.TabIndex = 2;
            this.lkpNome_F.TabStop = false;
            this.lkpNome_F.EditValueChanged += new System.EventHandler(this.lkpCondominio_F_EditValueChanged);
            // 
            // label_F
            // 
            this.label_F.AutoSize = true;
            this.label_F.Location = new System.Drawing.Point(-3, 3);
            this.label_F.Name = "label_F";
            this.label_F.Size = new System.Drawing.Size(62, 13);
            this.label_F.TabIndex = 3;
            this.label_F.Text = "Condom�nio";
            // 
            // LookupCondominio_F
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BindingSourcePrincipal = this.dComponenteFBindingSource;
            this.Controls.Add(this.label_F);
            this.Controls.Add(this.lkpNome_F);
            this.Controls.Add(this.lkpCodCon_F);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "LookupCondominio_F";
            this.Size = new System.Drawing.Size(308, 20);
            this.Load += new System.EventHandler(this.LookupCondominio_F_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCodCon_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dComponenteFBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpNome_F.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// bucador de condom�nio CONcodigo
        /// </summary>
        protected DevExpress.XtraEditors.LookUpEdit lkpCodCon_F;
        /// <summary>
        /// bucador de condom�nio CONNome
        /// </summary>
        protected DevExpress.XtraEditors.LookUpEdit lkpNome_F;
        private System.Windows.Forms.Label label_F;
        private System.Windows.Forms.BindingSource dComponenteFBindingSource;
    }
}
