namespace Framework.Lookup
{
    partial class LookupBlocosAptos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lkpBLOCodigo_F = new DevExpress.XtraEditors.LookUpEdit();
            this.labBloco_F = new System.Windows.Forms.Label();
            this.lkpBLONome_F = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCodCon_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpNome_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBLOCodigo_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBLONome_F.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lkpCodCon_F
            // 
            // 
            // lkpNome_F
            // 
            this.lkpNome_F.Size = new System.Drawing.Size(157, 20);
            // 
            // lkpBLOCodigo_F
            // 
            this.lkpBLOCodigo_F.Location = new System.Drawing.Point(67, 26);
            this.lkpBLOCodigo_F.Name = "lkpBLOCodigo_F";
            this.lkpBLOCodigo_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpBLOCodigo_F.Size = new System.Drawing.Size(96, 20);
            this.lkpBLOCodigo_F.TabIndex = 4;
            // 
            // labBloco_F
            // 
            this.labBloco_F.AutoSize = true;
            this.labBloco_F.Location = new System.Drawing.Point(-3, 33);
            this.labBloco_F.Name = "labBloco_F";
            this.labBloco_F.Size = new System.Drawing.Size(34, 13);
            this.labBloco_F.TabIndex = 5;
            this.labBloco_F.Text = "Bloco";
            // 
            // lkpBLONome_F
            // 
            this.lkpBLONome_F.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lkpBLONome_F.Location = new System.Drawing.Point(169, 26);
            this.lkpBLONome_F.Name = "lkpBLONome_F";
            this.lkpBLONome_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpBLONome_F.Size = new System.Drawing.Size(157, 20);
            this.lkpBLONome_F.TabIndex = 6;
            // 
            // LookupBlocosAptos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.lkpBLONome_F);
            this.Controls.Add(this.lkpBLOCodigo_F);
            this.Controls.Add(this.labBloco_F);
            this.Name = "LookupBlocosAptos";
            this.Size = new System.Drawing.Size(326, 46);
            this.Controls.SetChildIndex(this.lkpCodCon_F, 0);
            this.Controls.SetChildIndex(this.lkpNome_F, 0);
            this.Controls.SetChildIndex(this.label_F, 0);
            this.Controls.SetChildIndex(this.labBloco_F, 0);
            this.Controls.SetChildIndex(this.lkpBLOCodigo_F, 0);
            this.Controls.SetChildIndex(this.lkpBLONome_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lkpCodCon_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpNome_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBLOCodigo_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBLONome_F.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected DevExpress.XtraEditors.LookUpEdit lkpBLOCodigo_F;
        protected System.Windows.Forms.Label labBloco_F;
        protected DevExpress.XtraEditors.LookUpEdit lkpBLONome_F;

    }
}
