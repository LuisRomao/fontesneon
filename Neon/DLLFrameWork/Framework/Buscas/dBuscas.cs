﻿namespace Framework.Buscas {


    partial class dBuscas
    {
        partial class ChavesAPTDataTable
        {
        }

        private static dBuscasTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public static dBuscasTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dBuscasTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private static dBuscas edBuscas;
        private static dBuscasTableAdapters.CONDOMINIOSTableAdapter eCONDOMINIOSTableAdapter;
        private static dBuscasTableAdapters.ChavesAPTTableAdapter eChavesAPTTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public static dBuscasTableAdapters.ChavesAPTTableAdapter EChavesAPTTableAdapter
        {
            get {
                if (eChavesAPTTableAdapter == null) {
                    eChavesAPTTableAdapter = new Framework.Buscas.dBuscasTableAdapters.ChavesAPTTableAdapter();
                    eChavesAPTTableAdapter.TrocarStringDeConexao();
                };
                return eChavesAPTTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static dBuscasTableAdapters.CONDOMINIOSTableAdapter ECONDOMINIOSTableAdapter
        {
            get {
                if (eCONDOMINIOSTableAdapter == null)
                {
                    eCONDOMINIOSTableAdapter = new Framework.Buscas.dBuscasTableAdapters.CONDOMINIOSTableAdapter();
                    eCONDOMINIOSTableAdapter.TrocarStringDeConexao();
                }
                return eCONDOMINIOSTableAdapter; 
            }
            //set {  }
        } 

        /// <summary>
        /// 
        /// </summary>
        public static void Fill(){
            ECONDOMINIOSTableAdapter.FillTodos(EdBuscas.CONDOMINIOS);
            EChavesAPTTableAdapter.Fill(EdBuscas.ChavesAPT);
        }

        /// <summary>
        /// 
        /// </summary>
        public static dBuscas EdBuscas
        {
            get {
                if (dBuscas.edBuscas == null)
                {
                    edBuscas = new dBuscas();
                    Fill();
                }
                return dBuscas.edBuscas; 
            }
            //set {  }
        }
    }
}
