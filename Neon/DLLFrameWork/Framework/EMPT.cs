﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirMSSQL;
using VirDB.Bancovirtual;
using FrameworkProc;

namespace Framework
{
    /// <summary>
    /// Objeto para acesso a 2 bancos Local e Filial
    /// </summary>
    public class EMPT:EMPTProc
    {
        /*
        /// <summary>
        /// Tipo de filial
        /// </summary>
        public enum EMPTipo
        {
            /// <summary>
            /// Local
            /// </summary>
            Local,
            /// <summary>
            /// Filial
            /// </summary>
            Filial
        }*/

        //private EMPTipo _Tipo = EMPTipo.Local;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Tipo"></param>
        public EMPT(EMPTipo _Tipo):base(_Tipo)
        {
//            this._Tipo = _Tipo;
        }


//        public EMPTipo Tipo { get { return _Tipo; } }
        /*
        /// <summary>
        /// Tipo de banco de dados
        /// </summary>
        public TiposDeBanco TBanco { get { return Tipo == EMPTipo.Local ? TiposDeBanco.SQL : TiposDeBanco.Filial; } }
        */
        /// <summary>
        /// Codigo EMP
        /// </summary>        
        public override int EMP { get { return Tipo == EMPTipo.Local ? DSCentral.EMP : DSCentral.EMPF; } }

        /*
        /// <summary>
        /// TableAdapter estático
        /// </summary>
        public VirMSSQL.TableAdapter STTA { get { return TableAdapter.ST(Tipo == EMPTipo.Local ? TiposDeBanco.SQL : TiposDeBanco.Filial); } }


        /// <summary>
        /// Vircatch
        /// </summary>
        /// <param name="e"></param>
        public void Vircatch(Exception e)
        {
            STTA.Vircatch(e);
        }

        /// <summary>
        /// Commit
        /// </summary>
        /// <returns></returns>
        public bool Commit()
        {
            return STTA.Commit();
        }*/

    }
}
