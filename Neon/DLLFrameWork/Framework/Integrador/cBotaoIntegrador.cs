﻿using System;
using System.ComponentModel;

namespace Framework.Integrador
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cBotaoIntegrador : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cBotaoIntegrador()
        {
            InitializeComponent();
        }

        private TiposIntegra tipo;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("tipo de integração")]
        public TiposIntegra Tipo
        {
            get { return tipo;}
            set 
            { 
                tipo = value;
                switch (tipo)
                {
                    case TiposIntegra.Cheque:
                        simpleButton1.ImageIndex = 0;
                        simpleButton1.Text = "Cheque";
                        break;
                    case TiposIntegra.Nota:
                        simpleButton1.ImageIndex = 1;
                        simpleButton1.Text = "Nota";
                        break;
                    case TiposIntegra.Periodicos:
                        simpleButton1.ImageIndex = 2;
                        simpleButton1.Text = "Periódico";
                        break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Chave = -1;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (Chave != -1)
                Integrador.AbreComponente(Tipo, Chave, false, false);
        }
    }

}
