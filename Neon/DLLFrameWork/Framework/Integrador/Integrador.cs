﻿using System;
using CompontesBasicos;
using FrameworkProc;

namespace Framework.Integrador
{
    /// <summary>
    /// objeto de integração
    /// </summary>
    public static class Integrador
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="TipoI"></param>
        /// <param name="Chave"></param>
        /// <param name="SomenteLeitura"></param>
        /// <param name="PopUp"></param>
        /// <returns></returns>
        public static object AbreComponente(TiposIntegra TipoI,int Chave,bool SomenteLeitura,bool PopUp)
        {            
            bool ChamarComPkNoConstructor = false;
            string NomeTipo = "";
            string Titulo = "";
            switch (TipoI)
            {
                case TiposIntegra.Cheque: NomeTipo = "dllCheques.cCheque";
                    ChamarComPkNoConstructor = true;
                    Titulo = "Cheque";
                    break;
                case TiposIntegra.Nota: 
                    NomeTipo = "ContaPagar.cNotasCampos";
                    Titulo = "Nota";
                    break;
                case TiposIntegra.Periodicos: 
                    NomeTipo = "ContaPagar.Follow.cPagamentosPeriodicosCampos"; 
                    break;    
            }
            System.Type Tipo = CompontesBasicos.ModuleInfo.BuscaType(NomeTipo);
            if (!Tipo.IsSubclassOf(typeof(ComponenteCamposBase)))
                ChamarComPkNoConstructor = true;
            if(!ChamarComPkNoConstructor)
                return FormPrincipalBase.FormPrincipal.ShowMoludoCampos(Tipo, Titulo, Chave, SomenteLeitura, null, PopUp ? EstadosDosComponentes.PopUp : EstadosDosComponentes.JanelasAtivas);
            else
            {                
                System.Reflection.ConstructorInfo Construtor = Tipo.GetConstructor(new Type[] { typeof(int),typeof(EMPTProc) });                
                ComponenteBase Componente = (ComponenteBase)Construtor.Invoke(new object[] { Chave , null});
                Componente.Titulo = Titulo;
                Componente.somenteleitura = SomenteLeitura;
                Componente.VirShowModulo(PopUp ? EstadosDosComponentes.PopUp : EstadosDosComponentes.JanelasAtivas);
                return Componente;
            }
        }
    }

}