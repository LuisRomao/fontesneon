/*
MR - 28/04/2014 09:45 - 13.2.8.34 - Criacao de nova enumeracao para Tipos de Pagamento dos periodicos (Todos os �teis - para exibi��o nos combos) (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 29/04/2014 15:25 -           - Inclus�o de nova enumera��o de status para processo de cancelamento de pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***)
MR - 30/07/2014 16:00 -           - Tratamento de novo tipo de pagamento eletr�nico para titulo agrup�vel - varias notas um �nico boleto (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***)
MR - 05/08/2014 11:00 -           - Inclus�o de nova enumera��o de tipo de conta do condominio para pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (05/08/2014 11:00) ***)
MR - 12/11/2014 10:00 -           - Inclus�o de novo tipo de status de envio de arquivo na enumeracao StatusArquivo para tributos (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 12/11/2014 10:00 -           - Inclus�o de novo tipo de status de retorno de arquivo na enumeracao ARLStatusN para tributos (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 12/11/2014 10:00 -           - Inclus�o de novo tipo de lancamento neon na enumeracao TipoLancamentoNeon para tributos (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 12/11/2014 10:00 -           - Inclus�o de novo status de consolidado na enumeracao statusconsiliado para tributos (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
LH - 01/12/2014 13:46 - 14.1.4.94 - AGBStatus - CHESTATUS
LH - 01/12/2014 14:00 - 14.1.4.96 - Altera��o no funcionamento dos pagamentos peri�dicos com boleto
MR - 18/08/2015 10:00 -           - Cancelamento de tributo j� enviado ao banco (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (18/08/2015 10:00) ***)
MR - 14/01/2016 12:30             - Formas de cobrar por menor ou maior de condom�nio (Altera��es indicadas por *** MRC - INICIO - RESERVA (14/01/2016 12:30) ***)
MR - 15/03/2016 10:00 -           - Inclus�o de nova enumera��o com tamanhos de fonte para quadro de inadimpl�ncia (Altera��es indicadas por *** MRC - INICIO (15/03/2016 10:00) ***)
MR - 01/04/2016 19:00 -           - Inclus�o de novo tipo de status de retorno de arquivo na enumeracao ARLStatusN para tarifas (Altera��es indicadas por *** MRC - INICIO (01/04/2016 19:00) ***)
MR - 15/08/2016 14:00 -           - Inclus�o de novas enumera��es para reserva: tipos de grade, tipos de cancelamento e tipos de cobran�a (Altera��es indicadas por *** MRC - INICIO (15/08/2016 14:00) ***)
*/

using System;
using System.Drawing;
using dllVirEnum;
using AbstratosNeon;
using VirEnumeracoesNeon;
using Framework.objetosNeon;

namespace Framework
{    

    /// <summary>
    /// Enumera��es
    /// </summary>
    public static class Enumeracoes
    {
        static private VirEnum virEnumORCStatus;

        /// <summary>
        /// VirEnum para os tipos de malote
        /// </summary>
        static public VirEnum VirEnumORCStatus
        {
            get
            {
                if (virEnumORCStatus == null)
                {
                    virEnumORCStatus = new VirEnum(typeof(ORCStatus));
                    virEnumORCStatus.GravaNomes(ORCStatus.Cancelado, "Cancelado", Color.LightGray);
                    virEnumORCStatus.GravaNomes(ORCStatus.Cadastro , "Sendo Cadastrado", Color.Yellow);
                    virEnumORCStatus.GravaNomes(ORCStatus.Ativo, "Ativo", Color.Lime);
                    virEnumORCStatus.GravaNomes(ORCStatus.Terminado, "Terminado", Color.Aqua);                    
                }
                return virEnumORCStatus;
            }
        }

        static private VirEnum virEnumTiposMalote;

        /// <summary>
        /// VirEnum para os tipos de malote
        /// </summary>
        static public VirEnum VirEnumTiposMalote
        {
            get
            {
                if (virEnumTiposMalote == null)
                {
                    virEnumTiposMalote = new VirEnum(typeof(malote.TiposMalote));
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.Segunda_Manha, "M Segunda");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.SegundaQuartaManha, "M Seg-Quarta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.SegundaQuintaManha, "M Seg-Quinta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.Segunda_Tarde, "T Segunda");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.SegundaQuartaTarde, "T Seg-Quarta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.SegundaQuintaTarde, "T Seg-Quinta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.TercaQuintaManha, "M Ter_Quinta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.TercaSextaManha, "M Ter-Sexta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.TercaSextaTarde, "T Ter-Sexta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.QuartaManha, "M Quarta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.QuintaManha, "M Quinta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.QuintaTarde, "T Quinta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.SextaTarde, "T Sexta");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.TercaManha, "M Ter�a");
                    virEnumTiposMalote.GravaNomes(malote.TiposMalote.TercaTarde, "T Ter�a");
                }
                return virEnumTiposMalote;
            }
        }

        private static VirEnum virEnumPAGTipo;

        /// <summary>
        /// VirEnum para PAGTipo (Todos)
        /// </summary>
        public static VirEnum VirEnumPAGTipo
        {
            get
            {
                if (virEnumPAGTipo == null)
                {
                    virEnumPAGTipo = new VirEnum(typeof(PAGTipo));
                    virEnumPAGTipo.GravaNomes(PAGTipo.boleto, "Boleto banc�rio");
                    virEnumPAGTipo.GravaNomes(PAGTipo.cheque, "Carteira",Color.FromArgb(200, 200, 200));
                    virEnumPAGTipo.GravaNomes(PAGTipo.deposito, "Dep�sito em conta", Color.PapayaWhip);
                    virEnumPAGTipo.GravaNomes(PAGTipo.sindico, "Ser� pago pelo s�ndico");
                    virEnumPAGTipo.GravaNomes(PAGTipo.Folha, "Folha - Cr�dito em Conta", Color.Aqua);
                    virEnumPAGTipo.GravaNomes(PAGTipo.Guia, "Guia de recolhimento - Cheque");
                    virEnumPAGTipo.GravaNomes(PAGTipo.PEGuia, "Guia de recolhimento - Eletr�nico");                    
                    virEnumPAGTipo.GravaNomes(PAGTipo.Honorarios, "Honor�rios");
                    virEnumPAGTipo.GravaNomes(PAGTipo.Conta, "Conta(�gua, luz etc)");
                    virEnumPAGTipo.GravaNomes(PAGTipo.GuiaEletronica, "Tributo eletr�nico (Guia)");
                    virEnumPAGTipo.GravaNomes(PAGTipo.DebitoAutomatico, "D�bito autom�tico");
                    virEnumPAGTipo.GravaNomes(PAGTipo.PECreditoConta, "Pagamento Eletr�nico - Cr�dito em Conta (Corrente ou Poupan�a)", Color.Aqua);
                    virEnumPAGTipo.GravaNomes(PAGTipo.PETransferencia, "Pagamento Eletr�nico - Transfer�ncia (DOC ou TED)", Color.Aqua);
                    virEnumPAGTipo.GravaNomes(PAGTipo.PEOrdemPagamento, "Pagamento Eletr�nico - Ordem de Pagamento", Color.Aqua);
                    virEnumPAGTipo.GravaNomes(PAGTipo.PETituloBancario, "Pagamento Eletr�nico - T�tulo Banc�rio", Color.Aqua);
                    virEnumPAGTipo.GravaNomes(PAGTipo.PETituloBancarioAgrupavel, "Pagamento Eletr�nico - T�tulo Banc�rio Agrup�vel", Color.Aqua);
                    virEnumPAGTipo.GravaNomes(PAGTipo.TrafArrecadado, "Transfer�ncia f�sica de valor arrecadado");
                    virEnumPAGTipo.GravaNomes(PAGTipo.ChequeSindico, "Cheque S�ndico");
                }
                return virEnumPAGTipo;
            }
        }

        private static VirEnum virEnumPAGTipoUtil;

        //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
        /// <summary>
        /// VirEnum para PAGTipo (Todos os �teis - para exibi��o nos combos)
        /// </summary>
        public static VirEnum VirEnumPAGTipoUtil
        {
            get
            {
                if (virEnumPAGTipoUtil == null)
                {
                    virEnumPAGTipoUtil = new VirEnum(typeof(PAGTipo));
                    virEnumPAGTipoUtil.CarregarDefault = false;
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.boleto, "Boleto banc�rio");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.cheque, "Carteira");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.deposito, "Dep�sito em conta");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.sindico, "Ser� pago pelo s�ndico");
                    //virEnumPAGTipoUtil.GravaNomes(PAGTipo.Guia, "Guia de recolhimento");
                    //virEnumPAGTipoUtil.GravaNomes(PAGTipo.Honorarios, "Honor�rios");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.Conta, "Conta(�gua, luz etc)");
                    //virEnumPAGTipoUtil.GravaNomes(PAGTipo.GuiaEletronica, "Tributo eletr�nico (Guia)");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.DebitoAutomatico, "D�bito autom�tico");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.PECreditoConta, "Pagamento Eletr�nico - Cr�dito em Conta (Corrente ou Poupan�a)");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.PETituloBancario, "Pagamento Eletr�nico - T�tulo Banc�rio");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.PETituloBancarioAgrupavel, "Pagamento Eletr�nico - T�tulo Banc�rio Agrup�vel");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.TrafArrecadado, "Transferencia f�sica de valor arrecadado");
                    virEnumPAGTipoUtil.GravaNomes(PAGTipo.ChequeSindico, "Cheque S�ndico");
                }
                return virEnumPAGTipoUtil;
            }
        }
        //*** MRC - TERMINO - PAG-FOR (28/04/2014 09:45) ***

        private static VirEnum virEnumPAGTipoGUIAS;

        /// <summary>
        /// VirEnum para PAGTipo (Para as guias)
        /// </summary>
        public static VirEnum VirEnumPAGTipoGUIAS
        {
            get 
            {
                if (virEnumPAGTipoGUIAS == null)
                {
                    virEnumPAGTipoGUIAS = new VirEnum(typeof(PAGTipo));
                    virEnumPAGTipoGUIAS.CarregarDefault = false;
                    virEnumPAGTipoGUIAS.GravaNomes(PAGTipo.Guia, "Guia de recolhimento - Cheque");
                    virEnumPAGTipoGUIAS.GravaNomes(PAGTipo.PEGuia, "Guia de recolhimento - Eletr�nico");                    
                }
                return virEnumPAGTipoGUIAS;
            }
        }

        private static VirEnum virEnumPAGTipoSemPE;

        /// <summary>
        /// VirEnum para PAGTipo (sem Pagamento Eletr�nico)
        /// </summary>
        public static VirEnum VirEnumPAGTipoSemPE
        {
            get
            {
                if (virEnumPAGTipoSemPE == null)
                {
                    virEnumPAGTipoSemPE = new VirEnum(typeof(PAGTipo));
                    virEnumPAGTipoSemPE.CarregarDefault = false;
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.boleto, "Boleto banc�rio");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.cheque, "Carteira");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.deposito, "Dep�sito em conta");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.sindico, "Ser� pago pelo s�ndico");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.Guia, "Guia de recolhimento");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.Honorarios, "Honor�rios");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.Conta, "Conta(�gua, luz etc)");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.GuiaEletronica, "Tributo eletr�nico (Guia)");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.DebitoAutomatico, "D�bito autom�tico");
                    //virEnumPAGTipoSemPE.GravaNomes(PAGTipo.TrafArrecadado, "Transferencia f�sica de valor arrecadado");
                    virEnumPAGTipoSemPE.GravaNomes(PAGTipo.ChequeSindico, "Cheque S�ndico");
                }
                return virEnumPAGTipoSemPE;
            }
        }

        private static VirEnum virEnumNOATipo;

        /// <summary>
        /// VirEnum para NOATipo
        /// </summary>
        public static VirEnum VirEnumNOATipo
        {
            get
            {
                if (virEnumNOATipo == null)
                {
                    virEnumNOATipo = new VirEnum(typeof(NOATipo));
                    virEnumNOATipo.GravaNomes(NOATipo.Avulsa, "Nota Avulsa");
                    virEnumNOATipo.GravaNomes(NOATipo.Folha, "Folha de pagamento");
                    virEnumNOATipo.GravaNomes(NOATipo.ImportacaoXML, "Imp. DTM");
                    virEnumNOATipo.GravaNomes(NOATipo.PagamentoPeriodico, "Pagamento Peri�dico");
                    virEnumNOATipo.GravaNomes(NOATipo.NotaParcial, "Nota Parcial");
                    virEnumNOATipo.GravaNomes(NOATipo.Recibo, "Recibo");
                    virEnumNOATipo.GravaNomes(NOATipo.ReciboParcial, "Recibo Parcial");
                    virEnumNOATipo.GravaNomes(NOATipo.Repasse, "Repasse");
                    virEnumNOATipo.GravaNomes(NOATipo.TransferenciaF, "Transfer�ncia entre contas");
                }
                return virEnumNOATipo;
            }
        }

        private static VirEnum virEnumNOAStatus;

        /// <summary>
        /// 
        /// </summary>
        public static VirEnum VirEnumNOAStatus
        {
            get
            {
                if (virEnumNOAStatus == null)
                {
                    virEnumNOAStatus = new VirEnum(typeof(NOAStatus));
                    virEnumNOAStatus.GravaNomes(NOAStatus.Cadastrada, "Nota cadastrada", Color.FromArgb(255, 255, 100));//amarelo
                    virEnumNOAStatus.GravaNomes(NOAStatus.Parcial, "Nota incompleta", Color.FromArgb(255, 200, 200));//vermelho
                    virEnumNOAStatus.GravaNomes(NOAStatus.Liquidada, "Pagamentos efetuados", Color.FromArgb(200, 255, 255));//verde
                    virEnumNOAStatus.GravaNomes(NOAStatus.SemPagamentos, "Sem Pagamentos", Color.Silver);
                    virEnumNOAStatus.GravaNomes(NOAStatus.NotaCancelada, "Nota Cancelada", Color.FromArgb(200, 200, 200)); //cinza
                    virEnumNOAStatus.GravaNomes(NOAStatus.NotaProvisoria, "Nota Provis�ria", Color.DarkGoldenrod); // marrom    
                    virEnumNOAStatus.GravaNomes(NOAStatus.NotaAcumulada, "Nota Acumulada", Color.Orange); // laranja
                    virEnumNOAStatus.GravaNomes(NOAStatus.CadastradaAjustarValor, "Nota Acumulada sem valor", Color.Orange); // laranja
                }
                return virEnumNOAStatus;
            }
        }
                     
        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumFormaPagamento : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumFormaPagamento(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumFormaPagamento _virEnumFormaPagamentoSt;

            /// <summary>
            /// VirEnum est�tico para campo FormaPagamento
            /// </summary>
            public static virEnumFormaPagamento virEnumFormaPagamentoSt
            {
                get
                {
                    if (_virEnumFormaPagamentoSt == null)
                    {
                        _virEnumFormaPagamentoSt = new virEnumFormaPagamento(typeof(FormaPagamento));
                        _virEnumFormaPagamentoSt.GravaNomes(FormaPagamento.Correio, "Boleto enviado por correio");                        
                        _virEnumFormaPagamentoSt.GravaNomes(FormaPagamento.EntreigaPessoal, "Boleto entregue no condom�nio");
                        _virEnumFormaPagamentoSt.GravaNomes(FormaPagamento.CorreioEemail, "Boleto enviado por correio com e-mail");
                        _virEnumFormaPagamentoSt.GravaNomes(FormaPagamento.EntreigaPessoalEemail, "Boleto entregue no condom�nio com e-mail");
                        _virEnumFormaPagamentoSt.GravaNomes(FormaPagamento.email_Correio, "Boleto enviado por e-mail e documentos entregues no condom�nio");
                        _virEnumFormaPagamentoSt.GravaNomes(FormaPagamento.email_EntreigaPessoal, "Boleto enviado por e-mail e documentos via correio");
                    }
                    return _virEnumFormaPagamentoSt;
                }
            }
        }

        

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumCONStatus : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumCONStatus(Type Tipo)
                :
                base(Tipo)
            {
                UsarShort = true;
            }

            private static virEnumCONStatus _virEnumCONStatusSt;

            /// <summary>
            /// VirEnum est�tico para campo CONStatus
            /// </summary>
            public static virEnumCONStatus virEnumCONStatusSt
            {
                get
                {
                    if (_virEnumCONStatusSt == null)
                    {
                        _virEnumCONStatusSt = new virEnumCONStatus(typeof(CONStatus));
                        _virEnumCONStatusSt.GravaNomes(CONStatus.Inativo, "INATIVO");
                        _virEnumCONStatusSt.GravaNomes(CONStatus.Ativado, "ATIVADO");
                        _virEnumCONStatusSt.GravaNomes(CONStatus.Desativado, "DESATIVADO");
                        _virEnumCONStatusSt.GravaNomes(CONStatus.Implantacao, "IMPLANTA��O");
                        _virEnumCONStatusSt.GravaCor(CONStatus.Inativo, Color.Red);
                        _virEnumCONStatusSt.GravaCor(CONStatus.Ativado, Color.Lime);
                        _virEnumCONStatusSt.GravaCor(CONStatus.Desativado, Color.Yellow);
                        _virEnumCONStatusSt.GravaCor(CONStatus.Implantacao, Color.Aqua);
                    }
                    return _virEnumCONStatusSt;
                }
            }
        }


        

        

        private static dllVirEnum.VirEnum virEnumAGBStatus;

        /// <summary>
        /// VirEnum para AGBStatus
        /// </summary>
        public static dllVirEnum.VirEnum VirEnumAGBStatus
        {
            get
            {
                if (virEnumAGBStatus == null)
                {
                    virEnumAGBStatus = new dllVirEnum.VirEnum(typeof(AGBStatus));
                    virEnumAGBStatus.GravaNomes(AGBStatus.Agendado, "Agendado");
                    virEnumAGBStatus.GravaNomes(AGBStatus.Aviso, "Aviso");
                    virEnumAGBStatus.GravaNomes(AGBStatus.Vencido, "Vencido");
                    virEnumAGBStatus.GravaNomes(AGBStatus.Atraso, "Atraso");
                    virEnumAGBStatus.GravaNomes(AGBStatus.Ok, "Ok");
                    virEnumAGBStatus.GravaCor(AGBStatus.Agendado, Color.White);
                    virEnumAGBStatus.GravaCor(AGBStatus.Aviso, Color.Yellow);
                    virEnumAGBStatus.GravaCor(AGBStatus.Atraso, Color.Red);
                    virEnumAGBStatus.GravaCor(AGBStatus.Vencido, Color.Black);
                    virEnumAGBStatus.GravaCor(AGBStatus.Ok, Color.Silver);
                }
                return virEnumAGBStatus;
            }
        }




        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumTipoLancamentoNeon : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumTipoLancamentoNeon(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumTipoLancamentoNeon _virEnumTipoLancamentoNeonSt;

            /// <summary>
            /// VirEnum est�tico para campo TipoLancamentoNeon
            /// </summary>
            public static virEnumTipoLancamentoNeon virEnumTipoLancamentoNeonSt
            {
                get
                {
                    if (_virEnumTipoLancamentoNeonSt == null)
                    {
                        _virEnumTipoLancamentoNeonSt = new virEnumTipoLancamentoNeon(typeof(TipoLancamentoNeon));
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.NaoIdentificado, "??");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.Cheque, "Cheque");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.ChequeDevolvido_Condominio, "Cheque devolvido");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.ChequeDevolvido_Terceiro, "Cheque devolvido");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.creditodecobranca, "Cr�dito de combran�a");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.creditodecobrancaManual, "Cr�dito de combran�a Manual");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.debitoautomatico, "D�bito autom�tico");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.deposito, "Dep�sito");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.DespesasBancarias, "Despesas Bancarias");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.Estorno, "Estorno");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.PelaDescricao, "???");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.rentabilidade, "Rentabilidade");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.resgate, "Resgate");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.Salarios, "Sal�rios");                        
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.Tributo, "Tributo");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.PagamentoEletronico, "Pag. Eletr�nico");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.TransferenciaInterna, "Transfer�ncia Interna");
                        _virEnumTipoLancamentoNeonSt.GravaNomes(TipoLancamentoNeon.RendimentoContaInvestimento, "Rentabilidade Interna");
                    }
                    return _virEnumTipoLancamentoNeonSt;
                }
            }

        }

        

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumstatusconsiliado : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumstatusconsiliado(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumstatusconsiliado _virEnumstatusconsiliadoSt;

            /// <summary>
            /// VirEnum est�tico para campo statusconsiliado
            /// </summary>
            public static virEnumstatusconsiliado virEnumstatusconsiliadoSt
            {
                get
                {
                    if (_virEnumstatusconsiliadoSt == null)
                    {
                        _virEnumstatusconsiliadoSt = new virEnumstatusconsiliado(typeof(statusconsiliado));
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.Antigo, "Antigo");
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.Aguardando, "Para processar");
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.Automatico, "Processado (Autom�tico)");
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.Manual, "Processado (Manual)");
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.Filial, "Outra Filial");
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.Inativo, "Condom�no inativo");
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.AguardaConta, "Aguarda Conta");
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.ChequeNaoEncontrado, "Cheque n�o cadastrado");
                        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
                        _virEnumstatusconsiliadoSt.GravaNomes(statusconsiliado.TributoNaoEncontrado, "Tributo n�o cadastrado");
                        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
                    }
                    return _virEnumstatusconsiliadoSt;
                }
            }
        }

        /*
        /// <summary>
        /// Enumera��o para campo 
        /// </summary>
        [Obsolete("tranferido para AbstratosNeon")]
        public enum APTStatusCobranca
        {
            /// <summary>
            /// Cobran�a antiga n�o jur�dica
            /// </summary>
            AntigoENSU = -2,
            /// <summary>
            /// Cobran�a antiga jur�dica
            /// </summary>
            AntigoJ = -1,
            /// <summary>
            /// Sem cobran�a
            /// </summary>
            SemCobranca = 0             
        }*/

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumAPTStatusCobranca : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumAPTStatusCobranca(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumAPTStatusCobranca _virEnumAPTStatusCobrancaSt;

            /// <summary>
            /// VirEnum est�tico para campo APTStatusCobranca
            /// </summary>
            public static virEnumAPTStatusCobranca virEnumAPTStatusCobrancaSt
            {
                get
                {
                    if (_virEnumAPTStatusCobrancaSt == null)
                    {
                        _virEnumAPTStatusCobrancaSt = new virEnumAPTStatusCobranca(typeof(APTStatusCobranca));
                        _virEnumAPTStatusCobrancaSt.GravaNomes(APTStatusCobranca.SemCobranca, "Sem Cobran�a");
                        _virEnumAPTStatusCobrancaSt.GravaNomes(APTStatusCobranca.juridico, "Jur�dico");
                        _virEnumAPTStatusCobrancaSt.GravaNomes(APTStatusCobranca.Extra, "Extra Judicial");
                        _virEnumAPTStatusCobrancaSt.GravaNomes(APTStatusCobranca.Novo, "Nova");
                        _virEnumAPTStatusCobrancaSt.GravaNomes(APTStatusCobranca.PrimeiraCarta, "Carta Simples");
                        _virEnumAPTStatusCobrancaSt.GravaNomes(APTStatusCobranca.UltimoAviso, "�ltimo aviso");
                        _virEnumAPTStatusCobrancaSt.GravaNomes(APTStatusCobranca.SemCobranca, "ok");
                    }
                    return _virEnumAPTStatusCobrancaSt;
                }
            }
        }

        private static dllVirEnum.VirEnum virEnumTiposAPTDOC;

        /// <summary>
        /// VirEnum para TiposAPTDOC
        /// </summary>
        public static dllVirEnum.VirEnum VirEnumTiposAPTDOC
        {
            get
            {
                if (virEnumTiposAPTDOC == null)
                {
                    virEnumTiposAPTDOC = new dllVirEnum.VirEnum(typeof(TiposAPTDOC));
                    virEnumTiposAPTDOC.GravaNomes(TiposAPTDOC.advertencia, "Advert�ncia");
                    virEnumTiposAPTDOC.GravaNomes(TiposAPTDOC.escritura, "Escritura/Contrato");
                    virEnumTiposAPTDOC.GravaNomes(TiposAPTDOC.outros, "Outros");
                    virEnumTiposAPTDOC.GravaNomes(TiposAPTDOC.debito_automatico, "D�bito Autom�tico");
                }
                return virEnumTiposAPTDOC;
            }
        }



        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumPBOStatus : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumPBOStatus(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumPBOStatus _virEnumPBOStatusSt;

            /// <summary>
            /// VirEnum est�tico para campo PBOStatus
            /// </summary>
            public static virEnumPBOStatus virEnumPBOStatusSt
            {
                get
                {
                    if (_virEnumPBOStatusSt == null)
                    {
                        _virEnumPBOStatusSt = new virEnumPBOStatus(typeof(PBOStatus));
                        _virEnumPBOStatusSt.GravaNomes(PBOStatus.Preparado, "Preparado", Color.Yellow);
                        _virEnumPBOStatusSt.GravaNomes(PBOStatus.Bloqueado, "Bloqueado", Color.Red);
                        _virEnumPBOStatusSt.GravaNomes(PBOStatus.Registrando, "Aguarda registro", Color.Violet);
                        _virEnumPBOStatusSt.GravaNomes(PBOStatus.aguardaRetorno, "Aguarda retorno registro", Color.SpringGreen);
                        _virEnumPBOStatusSt.GravaNomes(PBOStatus.ok, "Ok", Color.Aqua);
                    }
                    return _virEnumPBOStatusSt;
                }
            }
        }

        

        

        

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumCHEStatus : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumCHEStatus(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumCHEStatus _virEnumCHEStatusSt;

            /// <summary>
            /// VirEnum est�tico para campo CHEStatus
            /// </summary>
            public static virEnumCHEStatus virEnumCHEStatusSt
            {
                get
                {
                    if (_virEnumCHEStatusSt == null)
                    {
                        _virEnumCHEStatusSt = new virEnumCHEStatus(typeof(CHEStatus));
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.NaoEncontrado, "Cheque n�o encontrado");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.Cancelado, "Cancelado");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.Cadastrado, "Cadastrado",Color.Aqua);
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.AguardaRetorno, "Aguarda retorno");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.Aguardacopia, "Aguarda C�pia");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.Retornado, "Retornado");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.Retirado, "Retirado pelo favorecido");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.EnviadoBanco, "Enviado para o banco");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.Compensado, "Compensado");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.CompensadoAguardaCopia, "Compensado sem o retorno da c�pia");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.PagamentoCancelado, "Pagamento Cancelado");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.Eletronico, "Pagamento Eletr�nico");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.Caixinha, "Pagamento via caixinha");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.DebitoAutomatico, "D�bito autom�tico agendado");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.BaixaManual, "Baixa Manual");                        
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.AguardaEnvioPE, "Aguarda envio para o Banco", Color.Silver);
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.AguardaConfirmacaoBancoPE, "Aguarda confirma��o de recebimento pelo Banco", Color.Yellow);
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.PagamentoConfirmadoBancoPE, "Pagamento Eletr�nico efetuado e confirmado pelo Banco");
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.PagamentoNaoAprovadoSindicoPE, "Pagamento Eletr�nico n�o aprovado pelo S�ndico",Color.MediumPurple);
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.PagamentoInconsistentePE, "Pagamento Eletr�nico com inconsist�ncia", Color.Red);
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.CadastradoBloqueado, "Aguardando a Nota", Color.DarkGoldenrod);
                        _virEnumCHEStatusSt.GravaNomes(CHEStatus.PagamentoNaoEfetivadoPE, "Pagamento n�o efetuado", Color.Violet); //
                    }
                    return _virEnumCHEStatusSt;
                }
            }
        }

        
        
        

        private static VirEnum virEnumTiposRepass;

        /// <summary>
        /// VirEnum para TiposRepass
        /// </summary>
        public static VirEnum VirEnumTiposRepass
        {
            get
            {
                if (virEnumTiposRepass == null)
                {
                    virEnumTiposRepass = new VirEnum(typeof(TiposRepass));
                    virEnumTiposRepass.GravaNomes(TiposRepass.Administrativas, "Despesa Administrativas");
                    virEnumTiposRepass.GravaNomes(TiposRepass.Assembleias, "Assemb�ias");
                    virEnumTiposRepass.GravaNomes(TiposRepass.Impressos, "Impressos");
                    virEnumTiposRepass.GravaNomes(TiposRepass.Malotes, "Malotes");
                    virEnumTiposRepass.GravaNomes(TiposRepass.Xerox, "Xerox");
                }
                return virEnumTiposRepass;
            }
        }

        

        static private VirEnum virEnumINSSSindico;

        /// <summary>
        /// Recolhimento do INSS
        /// </summary>
        static public VirEnum VirEnumINSSSindico
        {
            get
            {
                if (virEnumINSSSindico == null)
                {
                    virEnumINSSSindico = new VirEnum(typeof(INSSSindico));
                    virEnumINSSSindico.GravaNomes(INSSSindico.NaoRecolhe, "N�o recolhe");
                    virEnumINSSSindico.GravaNomes(INSSSindico.ParteCond, "20%");
                    virEnumINSSSindico.GravaNomes(INSSSindico.ParteCondSindico, "31%");
                }
                return virEnumINSSSindico;
            }
        }

        

        static private VirEnum virEnumAssinaCheque;

        /// <summary>
        /// Tipo de assinatura no corpo diretivo
        /// </summary>
        static public VirEnum VirEnumAssinaCheque
        {
            get
            {
                if (virEnumAssinaCheque == null)
                {
                    virEnumAssinaCheque = new VirEnum(typeof(AssinaCheque));
                    virEnumAssinaCheque.GravaNomes(AssinaCheque.NaoAssina, "N�o");
                    virEnumAssinaCheque.GravaNomes(AssinaCheque.Assina, "Assina");
                    virEnumAssinaCheque.GravaNomes(AssinaCheque.Conjunto, "Conjunto");
                }
                return virEnumAssinaCheque;
            }
        }

        static private VirEnum virEnumStatusArquivo;

        /// <summary>
        /// 
        /// </summary>
        static public VirEnum VirEnumStatusArquivo
        {
            get
            {
                if (virEnumStatusArquivo == null)
                {
                    virEnumStatusArquivo = new VirEnum(typeof(StatusArquivo));
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.A_Agendar, "Agendar");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Agendado, "Agendado");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Aguardando_Retorno, "Aguardando Retorno");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Autorizado, "Autorizado");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Com_operacao, "Com Opera��o");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Digitado, "Digitado");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Efetuado, "Efetuado");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Excluido, "Excluido");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Inconsistente, "Inconsistente");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Nao_Efetuado, "N�o Efetuado");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.ParaEnviar, "Para Enviar");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Pre_Autorizado, "Pre Autorizado");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Liberar_Gerente, "Liberar - Gerente");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Liberar_Gerencia, "Liberar - Gerencia");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Liberar, "Liberar");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.VirouCheque, "Cheque");
                    //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Aguardando_Envio, "Aguardando Envio");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Confirmado_Banco, "Confirmado Banco");
                    //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***
                    //*** MRC - INICIO - TRIBUTOS (18/08/2015 10:00) ***
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Aguardando_Envio_Exclusao, "Aguardando Envio (Exclus�o)");
                    virEnumStatusArquivo.GravaNomes(StatusArquivo.Aguardando_Retorno_Exclusao, "Aguardando Retorno (Exclus�o)");
                    //*** MRC - TERMINO - TRIBUTOS (18/08/2015 10:00) ***
                }
                return virEnumStatusArquivo;
            }
        }

        

        private static System.Collections.ArrayList _StatusArquivocancelaveis;

        /// <summary>
        /// 
        /// </summary>
        public static System.Collections.ArrayList StatusArquivocancelaveis
        {
            get
            {
                if (_StatusArquivocancelaveis == null)
                {
                    _StatusArquivocancelaveis = new System.Collections.ArrayList();
                    _StatusArquivocancelaveis.Add(StatusArquivo.Digitado);
                    _StatusArquivocancelaveis.Add(StatusArquivo.Liberar);
                    _StatusArquivocancelaveis.Add(StatusArquivo.Liberar_Gerencia);
                    _StatusArquivocancelaveis.Add(StatusArquivo.Liberar_Gerente);
                    _StatusArquivocancelaveis.Add(StatusArquivo.ParaEnviar);
                };
                return _StatusArquivocancelaveis;
            }
        }

        
        
        

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumEQPFormaCobrar : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumEQPFormaCobrar(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumEQPFormaCobrar _virEnumEQPFormaCobrarSt;

            /// <summary>
            /// VirEnum est�tico para campo EQPFormaCobrar
            /// </summary>
            public static virEnumEQPFormaCobrar virEnumEQPFormaCobrarSt
            {
                get
                {
                    if (_virEnumEQPFormaCobrarSt == null)
                    {
                        _virEnumEQPFormaCobrarSt = new virEnumEQPFormaCobrar(typeof(EQPFormaCobrar));
                        _virEnumEQPFormaCobrarSt.GravaNomes(EQPFormaCobrar.Valor, "Valor fixo: R$ x");
                        _virEnumEQPFormaCobrarSt.GravaNomes(EQPFormaCobrar.Salario, "x% do Sal�rio");
                        _virEnumEQPFormaCobrarSt.GravaNomes(EQPFormaCobrar.Condominio, "x% do Condom�nio");
                        _virEnumEQPFormaCobrarSt.GravaNomes(EQPFormaCobrar.CondominioMenor, "x% do Menor Condom�nio");
                        _virEnumEQPFormaCobrarSt.GravaNomes(EQPFormaCobrar.CondominioMaior, "x% do Maior Condom�nio");
                    }
                    return _virEnumEQPFormaCobrarSt;
                }
            }
        }

        //*** MRC - INICIO (15/08/2016 14:00) ***
        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumEQPTipoGrade: dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumEQPTipoGrade(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumEQPTipoGrade _virEnumEQPTipoGradeSt;

            /// <summary>
            /// VirEnum est�tico para campo EQPTipoGrade
            /// </summary>
            public static virEnumEQPTipoGrade virEnumEQPTipoGradeSt
            {
                get
                {
                    if (_virEnumEQPTipoGradeSt == null)
                    {
                        _virEnumEQPTipoGradeSt = new virEnumEQPTipoGrade(typeof(EQPTipoGrade));
                        _virEnumEQPTipoGradeSt.GravaNomes(EQPTipoGrade.DiasCorridos, "Dias corridos");
                        _virEnumEQPTipoGradeSt.GravaNomes(EQPTipoGrade.MesesCompletos, "Meses completos");
                    }
                    return _virEnumEQPTipoGradeSt;
                }
            }
        }

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumEQPTipoCancelamento : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumEQPTipoCancelamento(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumEQPTipoCancelamento _virEnumEQPTipoCancelamentoSt;

            /// <summary>
            /// VirEnum est�tico para campo EQPTipoCancelamento
            /// </summary>
            public static virEnumEQPTipoCancelamento virEnumEQPTipoCancelamentoSt
            {
                get
                {
                    if (_virEnumEQPTipoCancelamentoSt == null)
                    {
                        _virEnumEQPTipoCancelamentoSt = new virEnumEQPTipoCancelamento(typeof(EQPTipoCancelamento));
                        _virEnumEQPTipoCancelamentoSt.GravaNomes(EQPTipoCancelamento.AntesDataReservada, "Antes da data reservada");
                        _virEnumEQPTipoCancelamentoSt.GravaNomes(EQPTipoCancelamento.AposEfetuarReversa, "Ap�s efetuar a reserva");
                    }
                    return _virEnumEQPTipoCancelamentoSt;
                }
            }
        }

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumEQPTipoCobranca : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumEQPTipoCobranca(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumEQPTipoCobranca _virEnumEQPTipoCobrancaSt;

            /// <summary>
            /// VirEnum est�tico para campo EQPTipoCobranca
            /// </summary>
            public static virEnumEQPTipoCobranca virEnumEQPTipoCobrancaSt
            {
                get
                {
                    if (_virEnumEQPTipoCobrancaSt == null)
                    {
                        _virEnumEQPTipoCobrancaSt = new virEnumEQPTipoCobranca(typeof(EQPTipoCobranca));
                        _virEnumEQPTipoCobrancaSt.GravaNomes(EQPTipoCobranca.Total, "Total (100%)");
                        _virEnumEQPTipoCobrancaSt.GravaNomes(EQPTipoCobranca.Parcial, "Parcial (50%)");
                    }
                    return _virEnumEQPTipoCobrancaSt;
                }
            }
        }
        //*** MRC - TERMINO (15/08/2016 14:00) ***

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumBALStatus : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumBALStatus(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumBALStatus _virEnumBALStatusSt;

            /// <summary>
            /// VirEnum est�tico para campo BALStatus
            /// </summary>
            public static virEnumBALStatus virEnumBALStatusSt
            {
                get
                {
                    if (_virEnumBALStatusSt == null)
                    {
                        _virEnumBALStatusSt = new virEnumBALStatus(typeof(BALStatus));
                        _virEnumBALStatusSt.GravaNomes(BALStatus.NaoIniciado, "N�o Iniciado", Color.OldLace);
                        _virEnumBALStatusSt.GravaNomes(BALStatus.Producao, "Em Produ��o", Color.Wheat);
                        _virEnumBALStatusSt.GravaNomes(BALStatus.Termindo, "Terminado", Color.Yellow);
                        _virEnumBALStatusSt.GravaNomes(BALStatus.ParaPublicar, "Para Publicar", Color.DeepSkyBlue);
                        _virEnumBALStatusSt.GravaNomes(BALStatus.Publicado, "Publicado", Color.Lime);
                        _virEnumBALStatusSt.GravaNomes(BALStatus.FechadoDefinitivo, "Fechado Definitivo", Color.Pink);
                        _virEnumBALStatusSt.GravaNomes(BALStatus.Enviado , "Enviado", Color.Silver);                        
                    }
                    return _virEnumBALStatusSt;
                }
            }


        }


        /// <summary>
        /// virEnum para GradeREserva
        /// </summary>
        public class virEnumGradeReserva : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumGradeReserva(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumGradeReserva _virEnumGradeReservaSt;

            /// <summary>
            /// VirEnum est�tico para campo GradeReserva
            /// </summary>
            public static virEnumGradeReserva virEnumGradeReservaSt
            {
                get
                {
                    if (_virEnumGradeReservaSt == null)
                    {
                        _virEnumGradeReservaSt = new virEnumGradeReserva(typeof(GradeReserva));
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Todos, "Todos os dias");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Domingo, "Domingo");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Segunda, "Segunda");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Terca, "Ter�a");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Quarta, "Quarta");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Quinta, "Quinta");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Sexta, "Sexta");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Sabado, "S�bado");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Segunda_Sexta, "Segunda a sexta");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Sabado_Domingo, "Fim de semana");
                        _virEnumGradeReservaSt.GravaNomes(GradeReserva.Terca_Sexta, "Ter�a a sexta");
                    }
                    return _virEnumGradeReservaSt;
                }
            }

            
        }

        
        


        private static VirEnum virEnumTipoPagPeriodico;

        /// <summary>
        /// VirEnum para TipoPagPeriodico (Todos)
        /// </summary>
        public static VirEnum VirEnumTipoPagPeriodico
        {
            get
            {
                if (virEnumTipoPagPeriodico == null)
                {
                    virEnumTipoPagPeriodico = new VirEnum(typeof(TipoPagPeriodico));
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.Cheque, "Cheque -> Carteira");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.ChequeSindico, "Cheque -> S�ndico");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.ChequeDeposito, "Cheque -> Dep�sito");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.Boleto, "Boletos");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.DebitoAutomatico, "D�bito autom�tico SEM conta");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.DebitoAutomaticoConta, "D�bito autom�tico com conta");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.Conta, "Conta (�gual, luz etc)");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.Folha, "Folha");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.FolhaFGTS, "FGTS");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.FolhaPIS, "PIS (Folha)");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.PECreditoConta, "Pagamento Eletr�nico - Cr�dito em Conta (Corrente ou Poupan�a)");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.PETransferencia, "Pagamento Eletr�nico - Transfer�ncia (DOC ou TED)");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.PEOrdemPagamento, "Pagamento Eletr�nico - Ordem de Pagamento");
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.PETituloBancario, "Pagamento Eletr�nico - T�tulo Banc�rio");
                    //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.PETituloBancarioAgrupavel, "Pagamento Eletr�nico - T�tulo Banc�rio Agrup�vel");
                    //*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) ***
                    virEnumTipoPagPeriodico.GravaNomes(TipoPagPeriodico.Adiantamento, "Adiantamento");
                }
                return virEnumTipoPagPeriodico;
            }
        }


        /*
        //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
        /// <summary>
        /// Tipos de peri�dicos (Todos os �teis - para exibi��o nos combos)
        /// </summary>
        public enum TipoPagPeriodicoUtil
        {
           /// <summary>
           /// Cheque em papel
           /// </summary>
           Cheque = 0,
           /// <summary>
           /// Cheque s�ndico
           /// </summary>
           ChequeSindico = 1,
           /// <summary>
           /// Cheque para fazer dep�sito
           /// </summary>
           ChequeDeposito = 2,
           /// <summary>
           /// Cheque para pagar boleto
           /// </summary>
           Boleto = 3,
           /// <summary>
           /// D�bito autom�tico
           /// </summary>
           DebitoAutomatico = 4,
           /// <summary>
           /// D�bito automatico com conta
           /// </summary>
           DebitoAutomaticoConta = 5,
           /// <summary>
           /// Conta
           /// </summary>
           Conta = 6,
           /// <summary>
           /// Pis - Folha
           /// </summary>
           FolhaPIS = 7,
           /// <summary>
           /// FGTS
           /// </summary>
           FolhaFGTS = 8,
           /// <summary>
           /// FGTS eletr�nico
           /// </summary>
           Folha = 9,
           /// <summary>
           /// Folha - cheque
           /// </summary>
           FolhaPISEletronico = 10,
           /// <summary>
           /// Pis - Folha Eletr�nico
           /// </summary>
           FolhaFGTSEletronico = 11,
           /// <summary>
           /// Folha - eletr�nico
           /// </summary>
           FolhaEletronico = 12,
           /// <summary>
           /// IR - Folha
           /// </summary>
           FolhaIR = 13,
           /// <summary>
           /// IR - Folha Eletr�nico
           /// </summary>
           FolhaIREletronico = 14,
           /// <summary>
           /// Cr�dito em conta corrente ou poupanca - Pagamento Eletr�nico
           /// </summary>
           PECreditoConta = 20,
           /// <summary>
           /// Titulo Banc�rio (Boleto) - Pagamento Eletr�nico
           /// </summary>
           PETituloBancario = 23,
           //*** MRC - INICIO - PAG-FOR (30/07/2014 16:00) ***
           /// <summary>
           /// Titulo Banc�rio Agrup�vel (V�rias notas, um �nico boleto) - Pagamento Eletr�nico
           /// </summary>
           PETituloBancarioAgrupavel = 24,
           //*** MRC - TERMINO - PAG-FOR (30/07/2014 16:00) ***
        }*/

        private static VirEnum virEnumTipoPagPeriodicoUtil;

        
        /// <summary>
        /// VirEnum para TipoPagPeriodico (Todos os �teis - para exibi��o nos combos)
        /// </summary>
        public static VirEnum VirEnumTipoPagPeriodicoUtil
        {
           get
           {
              if (virEnumTipoPagPeriodicoUtil == null)
              {
                    virEnumTipoPagPeriodicoUtil = new VirEnum(typeof(TipoPagPeriodico));
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.Cheque, "Cheque -> Carteira");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.ChequeSindico, "Cheque -> S�ndico");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.ChequeDeposito, "Cheque -> Dep�sito");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.Boleto, "Boletos");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.DebitoAutomatico, "D�bito autom�tico SEM conta");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.DebitoAutomaticoConta, "D�bito autom�tico com conta");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.Conta, "Conta (�gual, luz etc)");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.Folha, "Folha");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.FolhaFGTS, "FGTS");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.FolhaPIS, "PIS (Folha)");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.PECreditoConta, "Pagamento Eletr�nico - Cr�dito em Conta (Corrente ou Poupan�a)");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.PETituloBancario, "Pagamento Eletr�nico - T�tulo Banc�rio");
                    virEnumTipoPagPeriodicoUtil.GravaNomes(TipoPagPeriodico.PETituloBancarioAgrupavel, "Pagamento Eletr�nico - T�tulo Banc�rio Agrup�vel");                 
              }
              return virEnumTipoPagPeriodicoUtil;
           }
        }
        

            /*
        /// <summary>
        /// Tipos de peri�dicos (sem Pagamento Eletr�nico)
        /// </summary>
        public enum TipoPagPeriodicoSemPE
        {
            /// <summary>
            /// Cheque em papel
            /// </summary>
            Cheque = 0,
            /// <summary>
            /// Cheque s�ndico
            /// </summary>
            ChequeSindico = 1,
            /// <summary>
            /// Cheque para fazer dep�sito
            /// </summary>
            ChequeDeposito = 2,
            /// <summary>
            /// Cheque para pagar boleto
            /// </summary>
            Boleto = 3,
            /// <summary>
            /// D�bito autom�tico
            /// </summary>
            DebitoAutomatico = 4,
            /// <summary>
            /// D�bito automatico com conta
            /// </summary>
            DebitoAutomaticoConta = 5,
            /// <summary>
            /// Conta
            /// </summary>
            Conta = 6,
            /// <summary>
            /// Pis - Folha
            /// </summary>
            FolhaPIS = 7,
            /// <summary>
            /// FGTS
            /// </summary>
            FolhaFGTS = 8,
            /// <summary>
            /// FGTS eletr�nico
            /// </summary>
            Folha = 9,
            /// <summary>
            /// Folha - cheque
            /// </summary>
            FolhaPISEletronico = 10,
            /// <summary>
            /// Pis - Folha Eletr�nico
            /// </summary>
            FolhaFGTSEletronico = 11,
            /// <summary>
            /// Folha - eletr�nico
            /// </summary>
            FolhaEletronico = 12,
            /// <summary>
            /// IR - Folha
            /// </summary>
            FolhaIR = 13,
            /// <summary>
            /// IR - Folha Eletr�nico
            /// </summary>
            FolhaIREletronico = 14,
        }*/

        private static VirEnum virEnumTipoPagPeriodicoSemPE;

        
        /// <summary>
        /// VirEnum para TipoPagPeriodico (sem Pagamento Eletr�nico)
        /// </summary>
        public static VirEnum VirEnumTipoPagPeriodicoSemPE
        {
            get
            {
                if (virEnumTipoPagPeriodicoSemPE == null)
                {
                    virEnumTipoPagPeriodicoSemPE = new VirEnum(typeof(TipoPagPeriodico));
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.Cheque, "Cheque -> Carteira");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.ChequeSindico, "Cheque -> S�ndico");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.ChequeDeposito, "Cheque -> Dep�sito");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.Boleto, "Boletos");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.DebitoAutomatico, "D�bito autom�tico SEM conta");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.DebitoAutomaticoConta, "D�bito autom�tico com conta");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.Conta, "Conta (�gua, luz etc)");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.Folha, "Folha");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.FolhaFGTS, "FGTS");
                    virEnumTipoPagPeriodicoSemPE.GravaNomes(TipoPagPeriodico.FolhaPIS, "PIS (Folha)");
                }
                return virEnumTipoPagPeriodicoSemPE;
            }
        }

        

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumAPTTipo : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padr�o
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumAPTTipo(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumAPTTipo _virEnumAPTTipoSt;

            /// <summary>
            /// VirEnum est�tico para campo APTTipo
            /// </summary>
            public static virEnumAPTTipo virEnumAPTTipoSt
            {
                get
                {
                    if (_virEnumAPTTipoSt == null)
                    {
                        _virEnumAPTTipoSt = new virEnumAPTTipo(typeof(APTTipo));
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.Apartamento, "Apartamento",Color.Aqua);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.Casa, "Casa", Color.DeepSkyBlue);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.Box, "Box / Dep�sito", Color.Yellow);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.DaConstrutora, "Unidade da construtora", Color.LightBlue);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.DoCondominio, "Unidade do condom�nio", Color.PaleTurquoise);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.Inativa, "Inativa", Color.Gainsboro);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.Locacao, "Loca��o / antena", Color.Goldenrod);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.Vaga, "Vaga", Color.Gold);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.Virtual, "Virtual", Color.Violet);
                        _virEnumAPTTipoSt.GravaNomes(APTTipo.Zelador, "Zelador", Color.Fuchsia);
                    }
                    return _virEnumAPTTipoSt;
                }
            }
        }


        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumARLStatusN : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumARLStatusN(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumARLStatusN _virEnumARLStatusNSt;

            /// <summary>
            /// VirEnum est�tico para campo ARLStatusN
            /// </summary>
            public static virEnumARLStatusN virEnumARLStatusNSt
            {
                get
                {
                    if (_virEnumARLStatusNSt == null)
                    {
                        _virEnumARLStatusNSt = new virEnumARLStatusN(typeof(ARLStatusN));
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.Baixado, "Baixado ok", Color.Green);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.Duplicidade, "DUPLICIDADE", Color.Red);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.baixadoComMulta, "Baixado com multa", Color.Green);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.RegistroConfirmado, "Registro do boleto confirmado", Color.Green);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.baixadoMultaAgendada, "Baixado com multa agendada", Color.Blue);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.BoletoCancelado, "Boleto cancelado", Color.LightPink);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.BoletoNaoEncontrado, "Boleto n�o encontrado");
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.CanceladoManualmente, "Cancelado Manualmente");
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.CondominioInativo, "Condom�nio inativo", Color.Yellow);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.ContaIncorreta, "Conta incorreta", Color.LightPink);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.ContaNaoEncontrada, "Conta n�o encontrada", Color.LightPink);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.CreditoNaoIdentificado, "Cr�dito n�o identificado");
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.DuplicidadeConfirmada, "Duplicidade confirmada", Color.Aqua);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.ErronaBaixa, "Erro na baixa", Color.Coral);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.ErroNoCalculoDoBoleto, "Erro no c�lculo do boleto", Color.Coral);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.Nova, "Para processar", Color.White);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.RegistoRejeitadoJaTratado, "Registro do boleto n�o confirmado", Color.Coral);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.RegistroRejeitado, "Registro do boleto n�o confirmado", Color.Coral);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.ValorIncorreto, "Valor incorreto", Color.LightPink);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.RetornoNaoHomologado, "???", Color.Coral);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.SuperBoletoVencido, "Super-Boleto fora da data/Valor", Color.LightPink);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.DebitoAutomaticoRecusado, "D�bito autom�tico recusado", Color.Violet);
                        _virEnumARLStatusNSt.GravaNomes(ARLStatusN.PagoEmChequeAguardo, "Pag. Cheque Aguarda compensar", Color.LemonChiffon);
                    }
                    return _virEnumARLStatusNSt;
                }
            }
        }        

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumRAVPadrao : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padr�o
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumRAVPadrao(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumRAVPadrao _virEnumRAVPadraoSt;

            /// <summary>
            /// VirEnum est�tico para campo RAVPadrao
            /// </summary>
            public static virEnumRAVPadrao virEnumRAVPadraoSt
            {
                get
                {
                    if (_virEnumRAVPadraoSt == null)
                    {
                        _virEnumRAVPadraoSt = new virEnumRAVPadrao(typeof(RAVPadrao));
                        _virEnumRAVPadraoSt.GravaNomes(RAVPadrao.EscritorioAdvogado, "* Escrit�rio de advocacia",Color.Aqua);
                        _virEnumRAVPadraoSt.GravaNomes(RAVPadrao.Imobiliaria, "* Imobili�ria Loca��o", Color.Chartreuse);
                        _virEnumRAVPadraoSt.GravaNomes(RAVPadrao.Seguradora, "* Seguradora", Color.Yellow);
                        _virEnumRAVPadraoSt.GravaNomes(RAVPadrao.Corretora, "* Corretora", Color.DarkOrange);
                        _virEnumRAVPadraoSt.GravaNomes(RAVPadrao.Cartorio, "* Cart�rio", Color.Plum);
                        _virEnumRAVPadraoSt.GravaNomes(RAVPadrao.ContaConsumo, "* D�bito Autom�tico", Color.Bisque);
                    }
                    return _virEnumRAVPadraoSt;
                }
            }
        }

        static private VirEnum virEnumTipoContaPE;

        /// <summary>
        /// VirEnum para TipoContaPE
        /// </summary>
        static public VirEnum VirEnumTipoContaPE
        {
           get
           {
              if (virEnumTipoContaPE == null)
              {
                 virEnumTipoContaPE = new VirEnum(typeof(TipoContaPE));
                 virEnumTipoContaPE.GravaNomes(TipoContaPE.Propria, "Conta Pr�pria no CNPJ do condominio");
                 virEnumTipoContaPE.GravaNomes(TipoContaPE.PropriaNeon, "Conta Pr�pria no CNPJ da Neon");
                 virEnumTipoContaPE.GravaNomes(TipoContaPE.PoolNeon, "Conta Pool no CNPJ da Neon");
              }
              return virEnumTipoContaPE;
           }
        }
        //*** MRC - TERMINO - PAG-FOR (05/08/2014 11:00) ***

        //*** MRC - INICIO (15/03/2016 10:00) ***
        

        static private VirEnum virEnumInaTamanhoFonte;

        /// <summary>
        /// VirEnum para InaTamanhoFonte
        /// </summary>
        static public VirEnum VirEnumInaTamanhoFonte
        {
            get
            {
                if (virEnumInaTamanhoFonte == null)
                {
                    virEnumInaTamanhoFonte = new VirEnum(typeof(InaTamanhoFonte));
                    virEnumInaTamanhoFonte.GravaNomes(InaTamanhoFonte.Pequena, "Pequena");
                    virEnumInaTamanhoFonte.GravaNomes(InaTamanhoFonte.Media, "M�dia");
                    virEnumInaTamanhoFonte.GravaNomes(InaTamanhoFonte.Grande, "Grande");
                }
                return virEnumInaTamanhoFonte;
            }
        }
        //*** MRC - TERMINO (15/03/2016 10:00) ***
    }
}
