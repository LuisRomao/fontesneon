using System;
using FrameworkProc;

namespace Framework
{
    /// <summary>
    /// Classe para calculo de dias �teis
    /// </summary>
    public class cDiaUtil : ICloneable
    {
        /// <summary>
        /// Sentido de ajuste
        /// </summary>
        public Sentido Sentido;
        private DateTime _Data;
        /// <summary>
        /// Data �til
        /// </summary>
        public DateTime Data
        {
            get
            {
                return _Data;
            }
            set
            {
                _Data = Ajusta(value, Sentido,TrataFeriadoComoUtil);
            }
        }

        /// <summary>
        /// Trata feriados como dia util
        /// </summary>
        public bool TrataFeriadoComoUtil = false;

        private static System.Collections.ArrayList _DiasInuteis;

        /// <summary>
        /// Dias da semana n�o uteis
        /// </summary>
        public static System.Collections.ArrayList DiasInuteis
        {
            get
            {
                if (_DiasInuteis == null)
                {
                    _DiasInuteis = new System.Collections.ArrayList(new DayOfWeek[] { DayOfWeek.Saturday, DayOfWeek.Sunday });
                };
                return _DiasInuteis;
            }
        }

        /// <summary>
        /// Retorna se uma data � �til
        /// </summary>
        /// <param name="Entrada"></param>
        /// <param name="TrataFeriadoComoUtil"></param>
        /// <returns></returns>
        public static bool IsUtil(DateTime Entrada, bool TrataFeriadoComoUtil)
        {
            if(DiasInuteis.Contains(Entrada.DayOfWeek))
                return false;
            if (!TrataFeriadoComoUtil && IsFeriado(Entrada))
                return false;
            return true;
        }

        /// <summary>
        /// Retorna se uma data � �til
        /// </summary>
        /// <param name="Entrada"></param>
        /// <returns></returns>
        public bool IsUtil(DateTime Entrada)
        {
            if (DiasInuteis.Contains(Entrada.DayOfWeek))
                return false;
            if (!TrataFeriadoComoUtil && IsFeriado(Entrada))
                return false;
            return true;
        }

        private static int Passo(Sentido S)
        {
            return S == Sentido.Frente ? 1 : -1;
        }

        /// <summary>
        /// Ajusta uma data para um dia �til
        /// </summary>
        /// <param name="Entrada">Data base</param>
        /// <param name="Sentido">Sentido de ajuste</param>
        /// <param name="TrataFeriadoComoUtil"></param>
        /// <returns></returns>
        public static DateTime Ajusta(DateTime Entrada, Sentido Sentido, bool TrataFeriadoComoUtil)
        {
            while (!IsUtil(Entrada,TrataFeriadoComoUtil))
            {
                Entrada = Entrada.AddDays(Passo(Sentido));
            }
            return Entrada;
        }

        /// <summary>
        /// Ajusta uma data para um dia �til
        /// </summary>
        /// <param name="Entrada"></param>
        /// <param name="Sentido"></param>
        /// <returns></returns>
        public static DateTime Ajusta(DateTime Entrada, Sentido Sentido)
        {
            return Ajusta(Entrada, Sentido, false);
        }

        /// <summary>
        /// Retorna se a data � feriado, se TrataFeriadoComoUtil retorna sempre falso
        /// </summary>
        /// <param name="Dia"></param>
        /// <returns></returns>
        public static bool IsFeriado(DateTime Dia)
        {
            //Buscar no database
            return false;
        }

        /// <summary>
        /// Retorna se a estancia � feriado
        /// </summary>
        /// <returns></returns>
        public bool IsFeriado()
        {
            return IsFeriado(Data);
        }

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="DataBase">Data base</param>
        /// <param name="_Sentido">Sentido</param>
        public cDiaUtil(DateTime DataBase, Sentido _Sentido)
        {
            Sentido = _Sentido;
            Data = DataBase;
        }

        /// <summary>
        /// construtor
        /// </summary>
        /// <param name="DataBase"></param>
        /// <param name="_Sentido"></param>
        /// <param name="_TrataFeriadoComoUtil"></param>
        public cDiaUtil(DateTime DataBase, Sentido _Sentido, bool _TrataFeriadoComoUtil)
        {
            Sentido = _Sentido;
            Data = DataBase;
            TrataFeriadoComoUtil = _TrataFeriadoComoUtil;
        } 
        #endregion

        /// <summary>
        /// Retorna data
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Data.ToString();
        }

        /// <summary>
        /// Retorna data
        /// </summary>
        /// <param name="Mascara">mascara da data</param>
        /// <returns></returns>
        public string ToString(string Mascara)
        {
            return Data.ToString(Mascara);
        }

        /// <summary>
        /// Soma dias corridos � data e ajusta no final pelo sentido da instancia. Nao altera _Data da instancia
        /// </summary>
        /// <param name="Dias"></param>
        /// <returns></returns>
        public DateTime AddDiasCorridos(int Dias)
        {
            DateTime Manobra = _Data;
            Manobra.AddDays(Dias);
            Manobra = Ajusta(Manobra, Sentido,TrataFeriadoComoUtil);
            return Manobra;
        }

        /// <summary>
        /// Soma dias �teis. Nao altera _Data da instancia
        /// </summary>
        /// <param name="Dias"></param>
        /// <returns></returns>
        public DateTime AddDiasUteis(int Dias)
        {
            Sentido S = Sentido.Frente;
            int Faltam = Dias;
            if (Dias < 0)
            {
                Faltam = -Dias;
                S = Sentido.Traz;
            };
            DateTime Manobra = _Data;
            while (Faltam > 0)
            {
                Manobra = Manobra.AddDays(Passo(S));
                if (IsUtil(Manobra))
                    Faltam--;
            }
            return Manobra;
        }

        /// <summary>
        /// Calcula o numero de dias utens entra as datas. Di para Df - se Di > Df o resultado � negativo
        /// </summary>
        /// <param name="Di"></param>
        /// <param name="Df"></param>
        /// <returns></returns>
        public static int DiasUtes(DateTime Di, DateTime Df)
        {
            int resultado = 0;
            cDiaUtil cDi = new cDiaUtil(Di, Sentido.Frente);
            cDiaUtil cDf = new cDiaUtil(Df, Sentido.Frente);
            while (cDi < cDf)
            {
                cDi++;
                resultado++;
            }
            while (cDi > cDf)
            {
                cDi--;
                resultado--;
            }
            return resultado;
        }


        #region Opeardores

        /// <summary>
        /// Retorna se igual
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (!(obj is cDiaUtil))
                return false;
            return (this == (cDiaUtil)obj);
        }

        /// <summary>
        /// Retorna GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// operador ==
        /// </summary>
        public static bool operator ==(cDiaUtil a, cDiaUtil b)
        {
            if (object.Equals(a, null) && (object.Equals(b, null)))
                return true;
            if (object.Equals(a, null) || (object.Equals(b, null)))
                return false;
            try
            {
                return (a.Data == b.Data);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// operador !=
        /// </summary>
        public static bool operator !=(cDiaUtil a, cDiaUtil b)
        {
            if (object.Equals(a, null) && (object.Equals(b, null)))
                return false;
            if (object.Equals(a, null) || (object.Equals(b, null)))
                return true;
            try
            {
                return (a.Data != b.Data);
            }
            catch
            {
                return true;
            }
        }


        /// <summary>
        /// Operador ++
        /// </summary>
        public static cDiaUtil operator ++(cDiaUtil _cDiaUtil)
        {
            return _cDiaUtil.ClonecDiaUtil(1);
        }

        /// <summary>
        /// Operador --
        /// </summary>
        public static cDiaUtil operator --(cDiaUtil _cDiaUtil)
        {
            return _cDiaUtil.ClonecDiaUtil(-1);
        }

        /// <summary>
        /// operador Menor
        /// </summary>
        public static bool operator <(cDiaUtil a, cDiaUtil b)
        {
            return (a.Data < b.Data);
        }

        /// <summary>
        /// operador >
        /// </summary>
        public static bool operator >(cDiaUtil a, cDiaUtil b)
        {
            return (a.Data > b.Data);
        }

        /// <summary>
        /// operador menor ou igual
        /// </summary>
        public static bool operator <=(cDiaUtil a, cDiaUtil b)
        {
            return (a.Data < b.Data);
        }

        /// <summary>
        /// operador >=
        /// </summary>
        public static bool operator >=(cDiaUtil a, cDiaUtil b)
        {
            return (a.Data > b.Data);

        }
        #endregion


        #region ICloneable Members

        /// <summary>
        /// Clonagem
        /// </summary>
        /// <remarks>Implementa��o da interface</remarks>
        /// <returns>Objeto clone</returns>
        public object Clone()
        {
            return new cDiaUtil(this.Data, this.Sentido);
        }

        /// <summary>
        /// Clone da cDiaUtil (Tipado)
        /// </summary>
        /// <remarks>O mesmo que Close s� que j� tipado</remarks>
        /// <returns>Clone da cDiaUtil</returns>
        public cDiaUtil ClonecDiaUtil()
        {
            return new cDiaUtil(this.Data, this.Sentido);
        }

        /// <summary>
        /// Clona e soma dias na cDiaUtil clonada
        /// </summary>
        ///<param name="diasUteis"></param>
        /// <returns></returns>
        public cDiaUtil ClonecDiaUtil(int diasUteis)
        {
            return new cDiaUtil(AddDiasUteis(diasUteis), Sentido, TrataFeriadoComoUtil); ;
        }

        #endregion


    }

    /*
    /// <summary>
    /// Sentido de arredondamento
    /// </summary>
    public enum Sentido
    {
        /// <summary>
        /// Usa datas superiores
        /// </summary>
        Frente,
        /// <summary>
        /// Usa datas anteriores
        /// </summary>
        Traz
    }*/
}
