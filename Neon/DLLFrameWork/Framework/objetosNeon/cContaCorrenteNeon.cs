﻿using DocBacarios;
using CompontesBasicos;
using FrameworkProc.objetosNeon;
using System.Windows.Forms;

namespace Framework.objetosNeon
{
    /// <summary>
    /// Editor de conta corrente
    /// </summary>
    public partial class cContaCorrenteNeon : CompontesBasicos.ComponenteBaseDialog
    {
        private CPFCNPJ CPF;
        /// <summary>
        /// Objeto interno
        /// </summary>
        public ContaCorrenteNeon contaCorrenteNeon1;

        /// <summary>
        /// Construtor
        /// </summary>
        public cContaCorrenteNeon(bool somenteleitura = false)
        {
            InitializeComponent();
            dBANCOS.BANCOSTableAdapter.Fill(dBANCOS.BANCOS);
            this.somenteleitura = somenteleitura;
            Editaveis(!somenteleitura);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_ContaCorrenteNeon"></param>
        /// <param name="somenteleitura"></param>
        public cContaCorrenteNeon(ContaCorrenteNeon _ContaCorrenteNeon, bool somenteleitura) : this(somenteleitura)
        {
            ContaCorrenteNeon1 = _ContaCorrenteNeon;
            AjustaTela();
        }

        private void AjustaTela()
        {
            if (ContaCorrenteNeon1.BCO != 0)
                lookBCO.EditValue = ContaCorrenteNeon1.BCO;
            spinAgencia.Value = ContaCorrenteNeon1.Agencia;
            spinAgenciaDg.Value = ContaCorrenteNeon1.AgenciaDg.GetValueOrDefault(0);
            spinConta.Value = ContaCorrenteNeon1.NumeroConta;
            textEditContaDg.EditValue = ContaCorrenteNeon1.NumeroDg;
            textEditTitular.Text = contaCorrenteNeon1.Titular;
            textEditCPF.Text = contaCorrenteNeon1.CPF_CNPJ != null ? contaCorrenteNeon1.CPF_CNPJ.ToString() : string.Empty;
            CPF = new CPFCNPJ(textEditCPF.Text);
        }

        private void Editaveis(bool Editavel)
        {
            lookBCO.ReadOnly = spinConta.ReadOnly = spinAgencia.ReadOnly = spinAgenciaDg.ReadOnly = textEditContaDg.ReadOnly = textEditTitular.ReadOnly = textEditCPF.ReadOnly = !Editavel;
        }

        private void lookBCO_EditValueChanged(object sender, System.EventArgs e)
        {
            spinAgenciaDg.Visible = (!lookBCO.EditValue.EstaNoGrupo(341));
        }

        private bool RecuperaTela()
        {
            if (!ValidaDados())
                return false;
            ContaCorrenteNeon1.BCO = (int)lookBCO.EditValue;
            ContaCorrenteNeon1.Agencia = (int)spinAgencia.Value;
            ContaCorrenteNeon1.AgenciaDg = (int)spinAgenciaDg.Value;
            ContaCorrenteNeon1.NumeroConta = (int)spinConta.Value;
            ContaCorrenteNeon1.NumeroDg = (textEditContaDg.EditValue != null) ? textEditContaDg.EditValue.ToString() : null;
            contaCorrenteNeon1.Titular = textEditTitular.Text.Trim() == string.Empty ? string.Empty : textEditTitular.Text;
            contaCorrenteNeon1.CPF_CNPJ = (CPF.Tipo != TipoCpfCnpj.INVALIDO) ? CPF : null;
            if (!ValidaDadosConta())
                return false;
            return true;
        }

        private void textEdit2_Properties_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!ValidaCPF())
            {
                textEditCPF.ErrorText = "CPF/CNPJ inválido";
                e.Cancel = true;
            }
            else
                textEditCPF.Text = CPF.ToString();
        }

        private bool ValidaCPF()
        {
            CPF = new CPFCNPJ(textEditCPF.Text);
            return (CPF.Tipo != TipoCpfCnpj.INVALIDO) || (textEditCPF.Text.Trim() == string.Empty);
        }

        private bool ValidaDados()
        {
            if (lookBCO.EditValue == null)
            {
                MessageBox.Show("Banco não definido");
                lookBCO.Focus();
                lookBCO.BackColor = System.Drawing.Color.FromArgb(250, 210, 210);
                return false;
            }
            if (textEditContaDg.EditValue == null)
            {
                MessageBox.Show("Dígito da conta não definido");
                textEditContaDg.Focus();
                textEditContaDg.BackColor = System.Drawing.Color.FromArgb(250, 210, 210);
                return false;
            }
            if (!ValidaCPF())
            {
                MessageBox.Show("CPF/CNPJ inválido");
                textEditCPF.Focus();
                textEditCPF.BackColor = System.Drawing.Color.FromArgb(250, 210, 210);
                return false;
            }
            return true;
        }

        private bool ValidaDadosConta()
        {
            switch (lookBCO.EditValue)
            {
                case 341:
                case 33:
                
                    if (!ContaCorrenteNeon1.validaConta())
                    {
                        MessageBox.Show("Agência / Conta inválida");
                        spinAgencia.Focus();
                        spinAgencia.BackColor = spinAgenciaDg.BackColor = System.Drawing.Color.FromArgb(250, 210, 210);
                        spinConta.BackColor = textEditContaDg.BackColor = System.Drawing.Color.FromArgb(250, 210, 210);
                        return false;
                    };
                    break;
                case 237:
                case 1:
                    if (!ContaCorrenteNeon1.validaAgencia())
                    {
                        MessageBox.Show("Agência inválida");
                        spinAgencia.Focus();
                        spinAgencia.BackColor = spinAgenciaDg.BackColor = System.Drawing.Color.FromArgb(250, 210, 210);
                        return false;
                    };
                    if (!ContaCorrenteNeon1.validaConta())
                    {
                        MessageBox.Show("Conta inválida");
                        spinConta.Focus();
                        spinConta.BackColor = textEditContaDg.BackColor = System.Drawing.Color.FromArgb(250, 210, 210);
                        return false;
                    };
                    break;
            }

            return true;
        }

        private void VoltaCor(object sender, System.EventArgs e)
        {
            ((DevExpress.XtraEditors.BaseEdit)sender).BackColor = System.Drawing.Color.White;
        }

        /// <summary>
        /// Fecha a tela
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if (Resultado == DialogResult.OK)
            {
                if (!RecuperaTela())
                {
                    Resultado = DialogResult.Cancel;
                    return;
                }
                ContaCorrenteNeon1.Salvar(ContaCorrenteNeon.TipoChave.CCG);
            }
            base.FechaTela(Resultado);
        }

        /// <summary>
        /// Coloca valore iniciais
        /// </summary>
        /// <param name="Titular"></param>
        /// <param name="CPF"></param>
        public void SetDefaults(string Titular, CPFCNPJ CPF = null)
        {
            textEditTitular.Text = Titular;
            if (CPF != null)
            {
                textEditCPF.Text = CPF.ToString();
                this.CPF = CPF;
            }
        }

        /// <summary>
        /// Trava o banco
        /// </summary>
        /// <param name="BCO"></param>
        public void TravaBanco(int BCO)
        {
            lookBCO.EditValue = BCO;
            lookBCO.Enabled = false;
        }

        /// <summary>
        /// chave CCG
        /// </summary>
        public int? CCG { get { return ContaCorrenteNeon1.CCG; } }

        /// <summary>
        /// Conta Corrente
        /// </summary>
        public ContaCorrenteNeon ContaCorrenteNeon1
        {
            get { return contaCorrenteNeon1 ?? (contaCorrenteNeon1 = new ContaCorrenteNeon()); }
            set { contaCorrenteNeon1 = value; }
        }
    }
}
