using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Framework.objetosNeon
{
    /// <summary>
    /// Componente visual para a compet�ncia
    /// </summary>
    public partial class cCompetData : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Evendo que ocorre quando a compet�ncia ou a data s�o alterados
        /// </summary>
        [Category("Virtual Software")]
        [Description("Altera��o compet�ncia/Data")]
        public event EventHandler alterado;

        /// <summary>
        /// Evendo que ocorre quando a compet�ncia ou a data s�o alterados
        /// </summary>
        /// <param name="e">Par�metros padr�o par eventos</param>
        protected virtual void Onalterado(EventArgs e)
        {
            if (alterado != null)
                alterado(this, e);
        }

        /// <summary>
        /// Propriedade que indica se a data � com ou sem o condom�nio
        /// </summary>
        [Category("Virtual Software")]
        public bool Comcondominio
        {
            get { return chComCondominio.Checked; }
            set { chComCondominio.Checked = value; }
        }

        
        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public cCompetData()
        {
            InitializeComponent();
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = chComCondominio.Checked ? PgCompetencia : pgData;
            Onalterado(new EventArgs());
        }

        private void cCompetData_Load(object sender, EventArgs e)
        {
            dateEdit1.Properties.MinValue = DateTime.Today;
            dateEdit1.DateTime = DateTime.Today.AddDays(7);
            dateEdit1.Properties.MaxValue = DateTime.Today.AddMonths(3);
        }

        /// <summary>
        /// Ajusta Competencia
        /// </summary>
        /// <param name="Origem">Instancia para c�pia</param>
        public void SetCompetencia(Competencia Origem)
        {
            cCompet1.SetCompetencia(Origem);          
        }

        /// <summary>
        /// Ajusta Competencia
        /// </summary>
        
        public void SetCompetencia(int _Mes,int _Ano)
        {
            cCompet1.SetCompetencia(new Competencia(_Mes,_Ano));
        }

        /// <summary>
        /// Ajusta Competencia
        /// </summary>

        public void SetCompetencia(int _Mes, int _Ano, int _CON)
        {
            cCompet1.SetCompetencia(new Competencia(_Mes, _Ano, _CON));
        }

        /// <summary>
        /// Retorna a competencia
        /// </summary>
        /// <remarks>No caso de n�o ser com o condom�nio a competencia � calculado pelo mes da data.</remarks>
        /// <returns>competencia</returns>
        public Competencia GetCompetencia()
        {
            if (Comcondominio)
                return cCompet1.GetCompetencia();
            else
                return new Competencia(dateEdit1.DateTime.Month, dateEdit1.DateTime.Year, cCompet1.Comp.CON);
        }
    }
}

