using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Framework.objetosNeon
{
    

    /// <summary>
    /// 
    /// </summary>
    public partial class InputCompetencia :  CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// Solicita uma compet�ncia do usu�rio
        /// </summary>
        /// <param name="compdefault">default</param>
        /// <returns></returns>
        public static Competencia stInput(Competencia compdefault = null)
        {
            InputCompetencia Imp = new InputCompetencia();
            if (compdefault != null)
            {
                Imp.cCompet1.Comp = compdefault;
                Imp.cCompet1.AjustaTela();
            }
            if (Imp.ShowEmPopUp() == DialogResult.OK)
                return Imp.cCompet1.Comp;
            else
                return null;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public InputCompetencia()
        {
            InitializeComponent();
        }       
    }
}
