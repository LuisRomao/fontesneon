﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.objetosNeon
{
    /// <summary>
    /// Classe para agrupar os planos de conta no balancete
    /// </summary>
    public class AgrupadorBalancete
    {
        /// <summary>
        /// Enumeração para campo 
        /// </summary>
        public enum MBDDetalhamento
        {
            /// <summary>
            /// Linha única com o total
            /// </summary>
            UmaLinha = 0,
            /// <summary>
            /// Uma linha para cada plano de contas
            /// </summary>
            PlanoDeContas = 1,
            /// <summary>
            /// Pela descrição
            /// </summary>
            Descricao = 2
        }

        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumMBDDetalhamento : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumMBDDetalhamento(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumMBDDetalhamento _virEnumMBDDetalhamentoSt;

            /// <summary>
            /// VirEnum estático para campo MBDDetalhamento
            /// </summary>
            public static virEnumMBDDetalhamento virEnumMBDDetalhamentoSt
            {
                get
                {
                    if (_virEnumMBDDetalhamentoSt == null)
                    {
                        _virEnumMBDDetalhamentoSt = new virEnumMBDDetalhamento(typeof(MBDDetalhamento));
                        _virEnumMBDDetalhamentoSt.GravaNomes(MBDDetalhamento.UmaLinha, "Linha única com o Total");
                        _virEnumMBDDetalhamentoSt.GravaNomes(MBDDetalhamento.PlanoDeContas, "Descrição do plano de contas");
                        _virEnumMBDDetalhamentoSt.GravaNomes(MBDDetalhamento.Descricao, "Descrição Livre");
                    }
                    return _virEnumMBDDetalhamentoSt;
                }
            }
        }

        /// <summary>
        /// Tipo de agrupamento 
        /// </summary>
        public enum TipoClassificacaoDesp
        {
            /// <summary>
            /// Em branco
            /// </summary>
            EmBanco = -1,
            /// <summary>
            /// Padrão por plano de contas
            /// </summary>
            PlanoContas = 0,
            /// <summary>
            /// Grupos Padrão
            /// </summary>
            Grupos = 1,
            /// <summary>
            /// Por Contas lógicas
            /// </summary>
            ContasLogicas = 2,
            /// <summary>
            /// Cadastrado na tabela MBA
            /// </summary>
            CadastroMBA = 3            
        }

        /// <summary>
        /// Objeto para solicitar agrupamento
        /// </summary>
        public class solicitaAgrupamento : AbstratosNeon.ABS_solicitaAgrupamento
        {
            /// <summary>
            /// Tipo de agrupamento
            /// </summary>
            public TipoClassificacaoDesp Tipo;

            /// <summary>
            /// MBA - não pode ser nulo para TipoClassificacaoDesp.CadastroMBA
            /// </summary>
            public int? MBA;

            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="_Tipo">Tipo</param>
            /// <param name="_MBA">MBA - não pode ser nulo para TipoClassificacaoDesp.CadastroMBA</param>
            public solicitaAgrupamento(TipoClassificacaoDesp _Tipo, int? _MBA = null)
            {
                Tipo = _Tipo;
                if (_Tipo == TipoClassificacaoDesp.CadastroMBA)
                {
                    if (!_MBA.HasValue)
                        throw new Exception("Chamada de TipoClassificacaoDesp.CadastroMBA sem valor para MBA");
                    MBA = _MBA;
                }
            }

            #region Operadores
            /// <summary>
            /// GethashCode
            /// </summary>
            /// <returns></returns>
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            /// <summary>
            /// Retorna se igual
            /// </summary>
            public override bool Equals(object obj)
            {
                if (obj == null)
                    return false;
                if (!(obj is solicitaAgrupamento))
                    return false;
                return (this == (solicitaAgrupamento)obj);
            }

            /// <summary>
            /// operador ==
            /// </summary>
            public static bool operator ==(solicitaAgrupamento a, solicitaAgrupamento b)
            {
                if (object.Equals(a, null) && (object.Equals(b, null)))
                    return true;
                if (object.Equals(a, null) || (object.Equals(b, null)))
                    return false;
                try
                {
                    return ((a.Tipo == b.Tipo) && (a.MBA == b.MBA));
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// operador !=
            /// </summary>
            public static bool operator !=(solicitaAgrupamento a, solicitaAgrupamento b)
            {
                if (object.Equals(a, null) && (object.Equals(b, null)))
                    return false;
                if (object.Equals(a, null) || (object.Equals(b, null)))
                    return true;
                try
                {
                    return ((a.Tipo != b.Tipo) || (a.MBA != b.MBA));
                }
                catch
                {
                    return true;
                }
            } 
            #endregion
        }

        /// <summary>
        /// Retorno de ClassificaDespesa
        /// </summary>
        public class retornoClassificacao
        {
            /// <summary>
            /// Grupo
            /// </summary>
            public string Grupo;
            /// <summary>
            /// Título
            /// </summary>
            public string Titulo;
            /// <summary>
            /// Descrição
            /// </summary>
            public string Descricao;
            /// <summary>
            /// Ordem
            /// </summary>
            public int Ordem;

            /// <summary>
            /// ordem dos grupos
            /// </summary>
            public int nGrupo;

            /// <summary>
            /// ordem dos SG
            /// </summary>
            public int nSuperGrupo;

            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="_Grupo"></param>
            /// <param name="_Titulo"></param>
            /// <param name="_Ordem"></param>
            /// <param name="_Descricao"></param>
            /// <param name="_nGrupo"></param>
            /// <param name="_nSuperGrupo"></param>
            public retornoClassificacao(string _Grupo, string _Titulo, string _Descricao, int _Ordem, int _nSuperGrupo, int _nGrupo)
            {
                Grupo = _Grupo;
                Titulo = _Titulo;
                Descricao = _Descricao;
                Ordem = _Ordem;
                nGrupo = _nGrupo;
                nSuperGrupo = _nSuperGrupo;
            }
        }

        /// <summary>
        /// ClassificaDespesa
        /// </summary>
        /// <param name="TClas"></param>
        /// <param name="PLA"></param>
        /// <param name="Descricao"></param>
        /// <returns></returns>
        public static retornoClassificacao ClassificaDespesa(solicitaAgrupamento TClas, string PLA, string Descricao)
        {            
            retornoClassificacao Retorno_Ordem_Grupo;
            switch (TClas.Tipo)
            {
                case TipoClassificacaoDesp.Grupos:
                default:
                    int planonumerico = 0;
                    int.TryParse(PLA, out planonumerico);
                    if (planonumerico == 230000)
                        Retorno_Ordem_Grupo = new retornoClassificacao("", "Advocatícias", Descricao, 50,0,5);                    
                    else if ((planonumerico == 240005) || (planonumerico == 240006))
                        Retorno_Ordem_Grupo = new retornoClassificacao("", "Bancárias", Descricao, 40,0,4);                                            
                    else if ((planonumerico < 219999) || (planonumerico >= 250000) || (planonumerico == 230009))
                        Retorno_Ordem_Grupo = new retornoClassificacao("", "Despesas Gerais", Descricao, 0,0,0);                                                                
                    else if ((planonumerico >= 220000) && (planonumerico < 230000))
                        Retorno_Ordem_Grupo = new retornoClassificacao("", "Mão de obra", Descricao, 10,0,1);                   
                    else if ((planonumerico > 230000) && (planonumerico < 230003))
                        Retorno_Ordem_Grupo = new retornoClassificacao("", "Administradora", Descricao, 30,0,3);                    
                    else //if ((planonumerico > 230003) && (planonumerico < 250000)) 
                        Retorno_Ordem_Grupo = new retornoClassificacao("", "Administrativas", Descricao, 20,0,2);                               
                    break;
                case TipoClassificacaoDesp.PlanoContas:
                    string PLAGrupo = string.Format("{0}000", PLA.Substring(0, 3));
                    Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(PLAGrupo);
                    if (PLArow == null)
                        PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(PLA);
                    Retorno_Ordem_Grupo = new retornoClassificacao("", PLArow == null ? PLA : PLArow.PLADescricaoRes, Descricao, int.Parse(PLAGrupo), 0, int.Parse(PLAGrupo));                    
                    break;
                case TipoClassificacaoDesp.CadastroMBA:
                    dAgrupadorBalancete.PLAModeloRow row = dAgrupadorBalancete.dAgrupadorBalanceteSt.PLAModelo.FindByMBAPLA(TClas.MBA.Value, PLA);
                    if (row == null)
                        throw new Exception(string.Format("Conta não classificada: {0}",PLA));
                    MBDDetalhamento Detalhamento = (MBDDetalhamento)row.MBDDetalhamento;
                    switch (Detalhamento)
                    {
                        case MBDDetalhamento.Descricao:
                            if(!row.MBDxPLADescricaoLivre)
                                Descricao = (row.IsMBDxPLADescricaoNull() || (row.MBDxPLADescricao == "")) ? row.PLADescricaoRes : row.MBDxPLADescricao;
                            break;
                        case MBDDetalhamento.PlanoDeContas:
                            Descricao = (row.IsMBDxPLADescricaoNull() || (row.MBDxPLADescricao == "")) ? row.PLADescricaoRes : row.MBDxPLADescricao;
                            break;
                        case MBDDetalhamento.UmaLinha:
                            Descricao = row.MBDTitulo;
                            break;
                    }
                    Retorno_Ordem_Grupo = new retornoClassificacao(row.MBGTitulo, row.MBDTitulo, Descricao, row.MBGOrdem * 100000000 + row.MBDOrdem * 10000 + row.MBDxPLAOrdem, row.MBGOrdem, row.MBDOrdem);                    
                    break;
            }
            return Retorno_Ordem_Grupo;
        }
    }
}
