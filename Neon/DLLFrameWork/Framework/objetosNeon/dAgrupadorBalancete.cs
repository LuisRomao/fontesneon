﻿namespace Framework.objetosNeon {
    
    
    public partial class dAgrupadorBalancete 
    {
        private static dAgrupadorBalancete _dAgrupadorBalanceteSt;

        /// <summary>
        /// dataset estático:dAgrupadorBalancete
        /// </summary>
        public static dAgrupadorBalancete dAgrupadorBalanceteSt
        {
            get
            {
                if (_dAgrupadorBalanceteSt == null)
                {
                    _dAgrupadorBalanceteSt = new dAgrupadorBalancete();
                    _dAgrupadorBalanceteSt.PLAModeloTableAdapter.Fill(_dAgrupadorBalanceteSt.PLAModelo);
                }
                return _dAgrupadorBalanceteSt;
            }
        }

        /// <summary>
        /// Carrega novamente os dados
        /// </summary>
        public static void ResetSt()
        {
            _dAgrupadorBalanceteSt = null;
        }

        private dAgrupadorBalanceteTableAdapters.PLAModeloTableAdapter pLAModeloTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PLAModelo
        /// </summary>
        public dAgrupadorBalanceteTableAdapters.PLAModeloTableAdapter PLAModeloTableAdapter
        {
            get
            {
                if (pLAModeloTableAdapter == null)
                {
                    pLAModeloTableAdapter = new dAgrupadorBalanceteTableAdapters.PLAModeloTableAdapter();
                    pLAModeloTableAdapter.TrocarStringDeConexao();
                };
                return pLAModeloTableAdapter;
            }
        }
    }
}
