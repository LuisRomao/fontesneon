/*
LH - 27/01/2015 16:00  14.2.4.9          - Novo malote - Ter�a de manh�
*/

using System;
using dllVirEnum;

namespace Framework.objetosNeon
{
    

    /// <summary>
    /// Classe Malote
    /// </summary>
    public class malote
    {
        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON">condom�nio</param>
        /// <param name="referencia">refer�ncia</param>
        public malote(int CON, DateTime referencia)
        {
            _CON = CON;
            object oTipo = dMALote.MALoteTableAdapter.TipoDeMaloteDoCondominio(_CON);
            if (oTipo == null)
            {
                string CONCodigo = "";
                VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CONCodigo from condominios where CON = @P1", out CONCodigo, CON);
                throw new Exception("Malote n�o cadastrado para " + CONCodigo);
            }
            Tipo = (TiposMalote)oTipo;
            dataMalote = referencia.Date;
            iniciaValores();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Tipo">Tipo de malote</param>
        /// <param name="referencia">Data</param>        
        public malote(TiposMalote Tipo, DateTime referencia)
        {
            this.Tipo = Tipo;
            //int passo = (SentidoReferenica == SentidoMalote.ida) ? -1 : 1;
            dataMalote = referencia.Date;
            iniciaValores();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="referencia"></param>
        /// <param name="CON"></param>
        public malote(TiposMalote Tipo, DateTime referencia, int CON)
        {
            this.Tipo = Tipo;
            _CON = CON;
            dataMalote = referencia.Date;
            iniciaValores();
        }


        /// <summary>
        /// Primerio malote ap�a a data/hora solicitados
        /// </summary>
        /// <param name="CON">Condom�nio</param>
        /// <param name="Data">Data e hora</param>
        /// <returns></returns>
        public static malote PrimeiroMaloteApos(int CON, DateTime Data)
        {
            malote mal = new malote(CON, Data);
            while (mal.dataEnvio < Data)
                mal++;
            return mal;
        }

        /// <summary>
        /// Malote que estar� de volta na data/hora solicitados
        /// </summary>
        /// <param name="CON">Condominio</param>
        /// <param name="DataDisponivel">Data e hora solicitados</param>
        /// <returns></returns>
        public static malote MalotePara(int CON, DateTime DataDisponivel)
        {
            try
            {
                return MalotePara((TiposMalote)Framework.datasets.dMALote.dMALoteSt.MALoteTableAdapter.TipoDeMaloteDoCondominio(CON), SentidoMalote.volta, DataDisponivel, 0);
            }
            catch
            {
                return null;
            }

        }


        /// <summary>
        /// O malote que atende a data e hora de referencia com folga de n malotes
        /// </summary>
        /// <param name="Tipo">Tipo de malote</param>
        /// <param name="Sentido">Se o malote vai ou volta</param>
        /// <param name="referencia">Data de refer�ncia de c�lculo</param>
        /// <param name="Malotes">Quantidade de malotes de folga. </param>
        /// <returns></returns>
        public static malote MalotePara(TiposMalote Tipo, SentidoMalote Sentido, DateTime referencia, int Malotes)
        {
            try
            {
                malote Retorno = new malote(Tipo, referencia);
                if (Sentido == SentidoMalote.volta)
                {
                    while (Retorno.dataRetorno > referencia)
                        Retorno--;

                    for (int i = 0; i < Malotes; i++)
                        Retorno--;
                }
                else
                {
                    while (Retorno.dataEnvio < referencia)
                        Retorno++;

                    for (int i = 0; i < Malotes; i++)
                        Retorno++;
                }
                return Retorno;
            }
            catch
            {
                return null;
            }

        }
        
        #endregion


        #region Enumera��es
        /// <summary>
        /// Manha/Tarde
        /// </summary>
        public enum Periodo { 
            /// <summary>
            /// Manh�
            /// </summary>
            Manha,
            /// <summary>
            /// Tarde
            /// </summary>
            Tarde,
            /// <summary>
            /// Errado
            /// </summary>
            Erro
        } 

        /// <summary>
        /// Tipos de malote
        /// </summary>
        public enum TiposMalote
        {
            /// <summary>
            /// Segunda e quinta de manha
            /// </summary>
            SegundaQuintaManha = 0,
            /// <summary>
            /// Segunda e quinta de tarde
            /// </summary>
            SegundaQuintaTarde = 1,
            /// <summary>
            /// Ter�a e sexta de manha
            /// </summary>
            TercaSextaManha = 2,
            /// <summary>
            /// Ter�a e sexta de tarde
            /// </summary>
            TercaSextaTarde = 3,
            /// <summary>
            /// Quinta
            /// </summary>
            QuintaTarde = 4,
            /// <summary>
            /// Segunda e quarta Manha
            /// </summary>
            SegundaQuartaManha = 5,
            /// <summary>
            /// Segunda e quarta Tarde
            /// </summary>
            SegundaQuartaTarde = 6,
            /// <summary>
            /// 
            /// </summary>
            SextaTarde = 7,
            /// <summary>
            /// 
            /// </summary>
            Segunda_Manha = 8,
            /// <summary>
            /// 
            /// </summary>
            QuintaManha = 9,
            /// <summary>
            /// 
            /// </summary>
            Segunda_Tarde = 10,
            /// <summary>
            /// 
            /// </summary>
            TercaQuintaManha = 11,
            /// <summary>
            /// 
            /// </summary>
            QuartaManha = 12,
            /// <summary>
            /// Ter�a de Manh�
            /// </summary>
            TercaManha = 13,
            /// <summary>
            /// Ter�a Tarde
            /// </summary>
            TercaTarde = 14,
        }

        static private VirEnum virEnumTiposMalote;

        /// <summary>
        /// VirEnum para os tipos de malote
        /// </summary>
        static public VirEnum VirEnumTiposMalote
        {
            get
            {
                if (virEnumTiposMalote == null)
                {
                    virEnumTiposMalote = new VirEnum(typeof(TiposMalote));
                    virEnumTiposMalote.GravaNomes(TiposMalote.Segunda_Manha, "M Segunda");
                    virEnumTiposMalote.GravaNomes(TiposMalote.SegundaQuartaManha, "M Seg-Quarta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.SegundaQuintaManha, "M Seg-Quinta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.Segunda_Tarde, "T Segunda");
                    virEnumTiposMalote.GravaNomes(TiposMalote.SegundaQuartaTarde, "T Seg-Quarta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.SegundaQuintaTarde, "T Seg-Quinta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.TercaQuintaManha, "M Ter_Quinta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.TercaSextaManha, "M Ter-Sexta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.TercaSextaTarde, "T Ter-Sexta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.QuartaManha, "M Quarta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.QuintaManha, "M Quinta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.QuintaTarde, "T Quinta");                                        
                    virEnumTiposMalote.GravaNomes(TiposMalote.SextaTarde, "T Sexta");
                    virEnumTiposMalote.GravaNomes(TiposMalote.TercaManha, "M Ter�a");
                    virEnumTiposMalote.GravaNomes(TiposMalote.TercaTarde, "T Ter�a");
                }
                return virEnumTiposMalote;
            }
        }


        /// <summary>
        /// Sentido do malote (ida/volta)
        /// </summary>
        public enum SentidoMalote
        {
            /// <summary>
            /// Ida
            /// </summary>
            ida,
            /// <summary>
            /// volta
            /// </summary>
            volta
        } 
        #endregion

        

        private static bool IsDiaUtil(DateTime Data) {
            return (Data.DayOfWeek != DayOfWeek.Saturday) && (Data.DayOfWeek != DayOfWeek.Sunday);
        }

        /// <summary>
        /// Retorna se � dia de malote
        /// </summary>
        /// <param name="Tipo">tipo de malote</param>
        /// <param name="referencia"></param>
        /// <returns></returns>
        public static bool IsDiaDeMalote(TiposMalote Tipo, DateTime referencia) {            
            return IsDiaDeMalote(Tipo, referencia.DayOfWeek);
        }

        /// <summary>
        /// Retorna se � dia de malote
        /// </summary>
        /// <param name="Tipo">Tipo de malote</param>
        /// <param name="referencia"></param>
        /// <returns></returns>
        public static bool IsDiaDeMalote(TiposMalote Tipo, DayOfWeek referencia) {
            switch (Tipo) { 
                case TiposMalote.SegundaQuintaManha:
                case TiposMalote.SegundaQuintaTarde:
                    return (referencia == DayOfWeek.Monday) || (referencia == DayOfWeek.Thursday);
                case TiposMalote.TercaManha:
                case TiposMalote.TercaTarde:
                    return referencia == DayOfWeek.Tuesday;
                case TiposMalote.TercaSextaManha:
                case TiposMalote.TercaSextaTarde:
                    return (referencia == DayOfWeek.Tuesday) || (referencia == DayOfWeek.Friday);
                case TiposMalote.QuintaTarde: 
                    return (referencia == DayOfWeek.Thursday); 
                case TiposMalote.SegundaQuartaManha:
                case TiposMalote.SegundaQuartaTarde:
                    return (referencia == DayOfWeek.Monday) || (referencia == DayOfWeek.Wednesday);
                case TiposMalote.SextaTarde:
                    return (referencia == DayOfWeek.Friday);
                case TiposMalote.Segunda_Manha:
                case TiposMalote.Segunda_Tarde:
                    return (referencia == DayOfWeek.Monday);
                case TiposMalote.QuintaManha:
                    return (referencia == DayOfWeek.Thursday);
                case TiposMalote.TercaQuintaManha:
                    return (referencia == DayOfWeek.Tuesday) || (referencia == DayOfWeek.Thursday);
                case TiposMalote.QuartaManha:
                    return (referencia == DayOfWeek.Wednesday);
                default: 
                    throw new NotImplementedException(string.Format("Malote n�o tratado: {0}", Tipo));
            }
        }

        
        /// <summary>
        /// Retorna o periodo de um determinado tipo de malote
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public static Periodo GetPeriodo(TiposMalote Tipo) {
            switch (Tipo) { 
                case TiposMalote.SegundaQuintaManha:
                case TiposMalote.TercaManha: 
                case TiposMalote.SegundaQuartaManha:
                case TiposMalote.Segunda_Manha:
                case TiposMalote.QuintaManha:
                case TiposMalote.TercaQuintaManha:
                case TiposMalote.QuartaManha:                
                case TiposMalote.TercaSextaManha:
                    return Periodo.Manha;
                case TiposMalote.SegundaQuintaTarde:
                case TiposMalote.TercaSextaTarde:
                case TiposMalote.QuintaTarde:
                case TiposMalote.SegundaQuartaTarde:
                case TiposMalote.SextaTarde:
                case TiposMalote.Segunda_Tarde:
                case TiposMalote.TercaTarde:
                    return Periodo.Tarde;
                default: return Periodo.Erro;                                    
            }
        }

        private TiposMalote Tipo;
        private DateTime dataMalote;
        private DateTime dataRetorno;
        private DateTime dataEnvio;

        /// <summary>
        /// Data do malote
        /// </summary>
        public DateTime DataMalote {
            get {
                return dataMalote;
            }
        }

        /// <summary>
        /// Data e hora em que os documentos ficam disponives
        /// </summary>
        public DateTime DataRetorno {
            get { 
                return dataRetorno; 
            }
        }

        /// <summary>
        /// Data e hora m�ximo de envio de documentos pelo malote
        /// </summary>
        public DateTime DataEnvio {
            get {
                return dataEnvio;
            }
        }

        #region Operadores       
        /// <summary>
        /// Retorna se igual
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (!(obj is malote))
                return false;
            return (this == (malote)obj);
        }

        /// <summary>
        /// Retorna GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// operador ==
        /// </summary>
        public static bool operator ==(malote a, malote b)
        {
            if (object.Equals(a, null) && (object.Equals(b, null)))
                return true;
            if (object.Equals(a, null) || (object.Equals(b, null)))
                return false;
            try
            {
                return ((a.dataMalote == b.dataMalote) && (a.Tipo == b.Tipo));
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// operador !=
        /// </summary>
        public static bool operator !=(malote a, malote b)
        {
            if (object.Equals(a, null) && (object.Equals(b, null)))
                return false;
            if (object.Equals(a, null) || (object.Equals(b, null)))
                return true;
            try
            {
                return ((a.dataMalote != b.dataMalote) || (a.Tipo != b.Tipo));
            }
            catch
            {
                return true;
            }
        }

        /// <summary>
        /// Operador ++
        /// </summary>
        public static malote operator ++(malote MAL)
        {
            int Pulo = 2;
            malote Manobra = new malote(MAL.Tipo, MAL.dataMalote.AddDays(Pulo),MAL.CON);
            while (MAL == Manobra) {
                Manobra = new malote(MAL.Tipo, MAL.dataMalote.AddDays(Pulo++), MAL.CON);
            };
            return Manobra;
        }

        /// <summary>
        /// Operador --
        /// </summary>
        public static malote operator --(malote MAL)
        {
            malote Manobra = new malote(MAL.Tipo, MAL.dataMalote.AddDays(-1), MAL.CON);
            return Manobra;
        }
        
        #endregion

        /// <summary>
        /// Representa��o em string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0:dd/MM/yy (dddd)} - {1}", DataMalote, ((GetPeriodo(Tipo) == Periodo.Manha) ? "Manh�" : "Tarde"));
        }

        /// <summary>
        /// Representa��o em string
        /// </summary>
        /// <param name="modelo">modelo:s = dd/mm/yy (ddd) - M ou ss = dd/mm ddd M </param>
        /// <returns></returns>
        public string ToString(string modelo)
        {
            if(modelo == "s")
                return String.Format("{0:dd/MM/yy (ddd)} - {1}", DataMalote, ((GetPeriodo(Tipo) == Periodo.Manha) ? "M" : "T"));
            else if (modelo == "ss")
                return String.Format("{0:dd/MM ddd} {1}", DataMalote, ((GetPeriodo(Tipo) == Periodo.Manha) ? "M" : "T"));
            else
                return ToString();
        }

        private int _CON;
        /// <summary>
        /// Condominio
        /// </summary>
        public int CON {
            get { return _CON; }
        }

        private datasets.dMALote _dMALote;
        private datasets.dMALote dMALote {
            get {
                if (_dMALote == null)
                    _dMALote = new Framework.datasets.dMALote();
                return _dMALote;
            }
        }

        private datasets.dMALote.MALoteRow _rowMAL;
        /// <summary>
        /// Linha mestra
        /// </summary>
        public datasets.dMALote.MALoteRow rowMAL {
            get {
                if(_rowMAL == null){
                    if (CON != 0) {
                        int lidos = dMALote.MALoteTableAdapter.FillByCON(dMALote.MALote, CON, dataMalote);
                        if (lidos > 0)
                            _rowMAL = dMALote.MALote[0];
                        else {
                            _rowMAL = dMALote.MALote.NewMALoteRow();
                            _rowMAL.MAL_CON = CON;
                            _rowMAL.MALData = dataMalote;
                            _rowMAL.MALExtra = false;
                            dMALote.MALote.AddMALoteRow(_rowMAL);
                            dMALote.MALoteTableAdapter.Update(_rowMAL);
                            _rowMAL.AcceptChanges();
                        }
                    }
                };
                return _rowMAL;
            }
        }

        private void iniciaValores() {
            while (!IsDiaDeMalote(Tipo, DataMalote))
                dataMalote = dataMalote.AddDays(-1);
            switch (GetPeriodo(Tipo))
            {
                case Periodo.Manha:
                    dataRetorno = dataMalote.AddHours(13);                    
                    dataEnvio = dataMalote.AddDays(-1).AddHours(17);
                    break;
                case Periodo.Tarde:
                    dataRetorno = dataMalote.AddDays(1).AddHours(9);
                    dataEnvio = dataMalote.AddHours(11);
                    break;
            }
            while (!IsDiaUtil(dataRetorno))
                dataRetorno = dataRetorno.AddDays(1);
            while (!IsDiaUtil(dataEnvio))
                dataEnvio = dataEnvio.AddDays(-1);
        }

        

        
    }

    /// <summary>
    /// Componente combo para malotes
    /// </summary>
    [Obsolete("Usar um ImageComboBoxEdit e carregar o virenum")]
    public class ComboMalotes : DevExpress.XtraEditors.ImageComboBoxEdit
    {
        /// <summary>
        /// Carrega os dados
        /// </summary>
        protected override void InitializeDefault()
        {
            base.InitializeDefault();
            if (!DesignMode)
            {
                Properties.Items.Clear();
                foreach (datasets.dMALote.TiposMaloteRow rowT in datasets.dMALote.dMALoteSt.TiposMalote)
                    Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(rowT.Descricao, (object)rowT.Codigo));
            }
        }
    }
}
