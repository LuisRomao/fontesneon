namespace Framework.objetosNeon
{
    /// <summary>
    /// Imagens para proprietario e inquilino
    /// </summary>
    partial class ImagensPI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImagensPI));
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // ImagensPI
            // 
            this.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("$this.ImageStream")));
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
    }
}
