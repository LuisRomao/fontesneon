using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Framework.objetosNeon
{
    /// <summary>
    /// Componente grafico para Competencia
    /// </summary>
    public partial class cCompet : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Objeto Competeincia
        /// </summary>
        public Competencia Comp;
        private Competencia compMinima;
        private Competencia compMaxima;

        private bool _PermiteNulo;

        /// <summary>
        /// Permite nulo
        /// </summary>
        [Category("Virtual Software")]
        [Description("Permite compet�ncia nula")]
        public bool PermiteNulo 
        {
            get 
            {
                return _PermiteNulo;
            }
            set 
            {
                _PermiteNulo = value;
                if (!_PermiteNulo)
                    IsNull = false;
                AjustaTela();
            }
        }

        private bool _IsNull;

        /// <summary>
        /// � nulo
        /// </summary>
        public bool IsNull 
        {
            get 
            {
                return _IsNull;
            }
            set 
            {
                if (PermiteNulo)
                {
                    if (_IsNull != value)
                    {
                        _IsNull = value;
                        AjustaTela();
                        DisparaEventoOnChange();
                    }
                }
            }
        }

        /// <summary>
        /// Compet�ncia em forma int para o banco de dados
        /// </summary>
        [Bindable(true)]
        [Category("Virtual Software")]
        [Description("Compet�ncia em forma int para o banco de dados")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]        
        public object CompetenciaBind
        {
            get
            {
                if (IsNull)
                    return DBNull.Value;
                else
                    return Comp.CompetenciaBind;
            }
            set
            {
                if (value is Int32)
                {
                    IsNull = false;
                    Comp.CompetenciaBind = (Int32)value;
                }
                else
                    IsNull = true;
                AjustaTela();
            }
        }

        /// <summary>
        /// Condom�nio
        /// </summary>
        [Bindable(true)]
        [Category("Virtual Software")]
        [Description("Condominio")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] 
        public object CON
        {
            get
            {
                if (Comp.CON == 0)
                    return DBNull.Value;
                else
                    return Comp.CON;
            }
            set
            {
                if (value is Int32)
                {
                    Comp.CON = (Int32)value;
                }
                else
                    Comp.CON = 0;                
            }
        }

        /// <summary>
        /// Botoes
        /// </summary>
        protected enum FormaBotoes 
        {
            /// <summary>
            /// mais menos
            /// </summary>
            maismenos,
            /// <summary>
            /// mais menos del
            /// </summary>
            maismenosdel,
            /// <summary>
            /// ativar
            /// </summary>
            ativar
        }

        /// <summary>
        /// Ajusta a tela
        /// </summary>
        /// <param name="Forma"></param>
        protected void AjustaBotao(FormaBotoes Forma)
        {
            switch (Forma)
            {
                case FormaBotoes.maismenos:
                    buttonEdit1.Properties.Buttons[0].Visible = true;
                    buttonEdit1.Properties.Buttons[1].Visible = true;
                    buttonEdit1.Properties.Buttons[2].Visible = false;
                    buttonEdit1.Properties.Buttons[3].Visible = false;
                    anoCompet.Enabled = mesCompet.Enabled = true;
                    break;
                case FormaBotoes.maismenosdel:
                    buttonEdit1.Properties.Buttons[0].Visible = true;
                    buttonEdit1.Properties.Buttons[1].Visible = true;
                    buttonEdit1.Properties.Buttons[2].Visible = true;
                    buttonEdit1.Properties.Buttons[3].Visible = false;
                    anoCompet.Enabled = mesCompet.Enabled = true;
                    break;
                case FormaBotoes.ativar:
                    buttonEdit1.Properties.Buttons[0].Visible = false;
                    buttonEdit1.Properties.Buttons[1].Visible = false;
                    buttonEdit1.Properties.Buttons[2].Visible = false;
                    buttonEdit1.Properties.Buttons[3].Visible = true;
                    anoCompet.Enabled = mesCompet.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Minima Competencia
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Competencia CompMinima
        {
            get
            {
                return compMinima ?? (compMinima = new Competencia(1, 2000));                
            }
            set { 
                compMinima = value;
                AjustaTela();
            }
        }

        /// <summary>
        /// M�xima Competencia
        /// </summary>
        [Category("Virtual Software")]
        [Description("Compet�ncia m�xima")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Competencia CompMaxima
        {
            get { 
                return compMaxima ?? (compMaxima = new Competencia().Add(48)); 
            }
            set
            {
                compMaxima = value;
                AjustaTela();
            }
        }

        private bool Editando=false;

        /// <summary>
        /// Ajusta Competencia
        /// </summary>
        /// <param name="Origem">Instancia para c�pia</param>
        public void SetCompetencia(Competencia Origem)
        {
            Comp.CopiaValores(Origem);
            AjustaTela();
        }





        /// <summary>
        /// Ajusta a tela para a competencia Comp
        /// </summary>
        public void AjustaTela() {
            Editando = true;
            if (PermiteNulo)
            {
                if (IsNull)
                    AjustaBotao(FormaBotoes.ativar);
                else
                    AjustaBotao(FormaBotoes.maismenosdel);
            }
            else
                AjustaBotao(FormaBotoes.maismenos);
            if (IsNull)
            {
                mesCompet.Text = anoCompet.Text = string.Empty;
                mesCompet.EditValue = anoCompet.EditValue = null;
            }
            else
            {
                if ((Comp == null) || (Comp < CompMinima))
                {
                    if (Comp == null)
                        Comp = CompMinima.CloneCompet();
                    Comp.Ano = CompMinima.Ano;
                    Comp.Mes = CompMinima.Mes;
                }
                else if (Comp > CompMaxima)
                {
                    Comp.Ano = CompMaxima.Ano;
                    Comp.Mes = CompMaxima.Mes;
                }
                mesCompet.Value = Comp.Mes;
                anoCompet.Value = Comp.Ano;
            }
            Editando = false;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public cCompet()
        {
            InitializeComponent();
            DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.Never;
            Validated += GravaCompetenciaBind;
            Comp = new Competencia(DateTime.Now.Month, DateTime.Now.Year);            
            GravaCompetenciaBind();
            AjustaTela();
        }       

        private void GravaCompetenciaBind(object sender, EventArgs e)
        {
            GravaCompetenciaBind();
        }

        /// <summary>
        /// Bind Compet�ncia
        /// </summary>
        public void GravaCompetenciaBind()
        {
            Binding B = DataBindings["CompetenciaBind"];
            if (B != null)
                B.WriteValue();
        }


        private void spinEdit2_Spin(object sender, DevExpress.XtraEditors.Controls.SpinEventArgs e)
        {
            
            Comp.Add(e.IsSpinUp ? 1 : -1);
            AjustaTela();
        }

        private void anoCompet_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Right:
                    Comp.Add(1);
                    AjustaTela();
                    DisparaEventoOnChange();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Left:
                    Comp.Add(-1);
                    AjustaTela();
                    DisparaEventoOnChange();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    IsNull = false;                                        
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Undo:
                    IsNull = true;
                    break;
            }
        }

        private void mesCompet_EditValueChanged(object sender, EventArgs e)
        {
            
            if (!Editando)
            {
                Editando = true;
                Comp.Mes = (int)mesCompet.Value;
                Comp.Ano = (int)anoCompet.Value;
                Editando = false;
                AjustaTela();
                DisparaEventoOnChange();
            }
        }

        /// <summary>
        /// Retorna a competencia (Clone)
        /// </summary>
        /// <remarks>Retorna um clone</remarks>
        /// <returns>Cometencia</returns>
        public Competencia GetCompetencia()
        {
            return Comp.CloneCompet();
        }


        /// <summary>
        /// Evento que ocorre ap�s a atera��o da compet�ncia
        /// </summary>        
        [Category("Virtual Software")]
        [Description("Alter��o da compet�ncia")]
        public event EventHandler OnChange;

        private void DisparaEventoOnChange() {
            if (OnChange != null)
                OnChange(this, new EventArgs());
        }

        /// <summary>
        /// Seta ou le se � somente leitura
        /// </summary>
        public bool ReadOnly {
            get {
                return mesCompet.Properties.ReadOnly;
            }
            set {
                buttonEdit1.Visible = !value;
                mesCompet.Properties.ReadOnly = anoCompet.Properties.ReadOnly = value;
            }
        }

    }
}

