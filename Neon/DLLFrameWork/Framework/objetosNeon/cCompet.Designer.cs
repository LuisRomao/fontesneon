namespace Framework.objetosNeon
{
    partial class cCompet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.mesCompet = new DevExpress.XtraEditors.SpinEdit();
            this.anoCompet = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mesCompet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.anoCompet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
           
            // 
            // mesCompet
            // 
            this.mesCompet.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mesCompet.Location = new System.Drawing.Point(0, 4);
            this.mesCompet.Name = "mesCompet";
            this.mesCompet.Properties.DisplayFormat.FormatString = "00";
            this.mesCompet.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.mesCompet.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.mesCompet.Properties.Mask.EditMask = "00";
            this.mesCompet.Properties.MaxValue = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.mesCompet.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mesCompet.Size = new System.Drawing.Size(19, 20);
            this.mesCompet.TabIndex = 0;
            this.mesCompet.EditValueChanged += new System.EventHandler(this.mesCompet_EditValueChanged);
            // 
            // anoCompet
            // 
            this.anoCompet.EditValue = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.anoCompet.Location = new System.Drawing.Point(31, 4);
            this.anoCompet.Name = "anoCompet";
            this.anoCompet.Properties.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.anoCompet.Properties.IsFloatValue = false;
            this.anoCompet.Properties.Mask.EditMask = "f0";
            this.anoCompet.Properties.MaxValue = new decimal(new int[] {
            2020,
            0,
            0,
            0});
            this.anoCompet.Properties.MinValue = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.anoCompet.Size = new System.Drawing.Size(30, 20);
            this.anoCompet.TabIndex = 1;
            this.anoCompet.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.anoCompet_ButtonClick);
            this.anoCompet.EditValueChanged += new System.EventHandler(this.mesCompet_EditValueChanged);
            this.anoCompet.Spin += new DevExpress.XtraEditors.Controls.SpinEventHandler(this.spinEdit2_Spin);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl1.Location = new System.Drawing.Point(19, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(12, 25);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "/";
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.Location = new System.Drawing.Point(62, 4);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Left),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Undo, "", 1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null)});
            this.buttonEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.buttonEdit1.Size = new System.Drawing.Size(55, 20);
            this.buttonEdit1.TabIndex = 3;
            this.buttonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.anoCompet_ButtonClick);
            this.buttonEdit1.Spin += new DevExpress.XtraEditors.Controls.SpinEventHandler(this.spinEdit2_Spin);
            // 
            // cCompet
            // 
            this.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Appearance.Options.UseBackColor = true;
            this.Controls.Add(this.buttonEdit1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.anoCompet);
            this.Controls.Add(this.mesCompet);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCompet";
            this.Size = new System.Drawing.Size(117, 24);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mesCompet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.anoCompet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SpinEdit mesCompet;
        private DevExpress.XtraEditors.SpinEdit anoCompet;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
    }
}
