using System;
using VirEnumeracoesNeon;

namespace Framework.objetosNeon
{
    /// <summary>
    /// Classe representante de um apartamento
    /// </summary>
    [Obsolete("Usar ABS_Apartamento ou Cadastros.Apartamentos.Apartamento")]
    public class Apartamento
    {
        private bool _Encontrado = false;
        private string _CONCodigo = "";
        private string _BLOCodigo = "";
        private string _APTNumero = "";
        private int _CON;
        private int _BLO;
        private int _APT = 0;
        private bool? _APTSeguro = null;
        //private int? _CON_PSC;

        /// <summary>
        /// Retorna se o apartamento foi encontrado, ou seja, se os dads fornecidos no contrutor s�o v�lidos
        /// </summary>
        public bool Encontrado {
            get {
                if (_Encontrado)
                    return true;
                if (APT == 0)
                    return false;
                return VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("select APT from apartamentos where APT = @P1", APT);
            }
        }

        /// <summary>
        /// Email do inquilino ou propriet�rio
        /// </summary>
        /// <returns></returns>
        public string EmailResponsavel()
        {
            if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                return "virtual@virweb.com.br";
            string Comando;
            if (Alugado())
                Comando =
"SELECT    PESSOAS.PESEmail\r\n" +
"FROM         APARTAMENTOS INNER JOIN\r\n" +
"                      PESSOAS ON APARTAMENTOS.APTInquilino_PES = PESSOAS.PES\r\n" +
"WHERE     (APARTAMENTOS.APT = @P1);";
            else
                Comando =
"SELECT    PESSOAS.PESEmail\r\n" +
"FROM         APARTAMENTOS INNER JOIN\r\n" +
"                      PESSOAS ON APARTAMENTOS.APTProprietario_PES = PESSOAS.PES\r\n" +
"WHERE     (APARTAMENTOS.APT = @P1);";
            string Retorno;
            if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(Comando, out Retorno, APT))
                return Retorno;
            else
                return "";
        }

        /// <summary>
        /// BLO
        /// </summary>
        public int BLO {
            get{
                ChecaDados();
                return _BLO;
            }
        }

        /// <summary>
        /// CON
        /// </summary>
        public int CON
        {
            get
            {
                ChecaDados();
                return _CON;
            }
        }

        /// <summary>
        /// APT
        /// </summary>
        public int APT {
            get {
                return _APT;
            }
        }

        /// <summary>
        /// Retorna se o apartamento est� alugado
        /// </summary>
        /// <returns>Se est� alugado</returns>
        public bool Alugado() {
            string ComandoAlugado = "SELECT APTInquilino_PES FROM APARTAMENTOS WHERE (APT = @P1)";
            int PES;
            return (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(ComandoAlugado,out PES, APT));                            
        }

        /// <summary>
        /// Status da cobran�a
        /// </summary>
        /// <returns></returns>
        public APTStatusCobranca StatusCobranca()
        {
            return (APTStatusCobranca)VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select APTStatusCobranca from apartamentos where APT = @P1",APT);
        }

        private void ChecaDados(){
            if (_CONCodigo == "") {
                string Busca = "SELECT CONDOMINIOS.CON, CONDOMINIOS.CONCodigo, BLOCOS.BLO, BLOCOS.BLOCodigo, APARTAMENTOS.APTNumero\r\n" +
                               "FROM         CONDOMINIOS INNER JOIN\r\n"+
                               "BLOCOS ON CONDOMINIOS.CON = BLOCOS.BLO_CON INNER JOIN\r\n"+
                               "APARTAMENTOS ON BLOCOS.BLO = APARTAMENTOS.APT_BLO\r\n" +
                               "WHERE     (APARTAMENTOS.APT = @P1)";
                System.Data.DataRow Resultado = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(Busca, APT);
                _CONCodigo = Resultado["CONCodigo"].ToString();
                _BLOCodigo = Resultado["BLOCodigo"].ToString();
                _APTNumero = Resultado["APTNumero"].ToString();
                _CON = (int)Resultado["CON"];
                _BLO = (int)Resultado["BLO"];
            };
        }

        /// <summary>
        /// CONCodigo do apartamento
        /// </summary>
        public string CONCodigo {
            get {
                ChecaDados();
                return _CONCodigo;
            }
        }

        /// <summary>
        /// BLOCodigo do apartamento
        /// </summary>
        public string BLOCodigo {
            get {
                ChecaDados();
                return _BLOCodigo;
            }
        }

        /// <summary>
        /// APTNumero do apartamento
        /// </summary>
        public string APTNumero {
            get {
                ChecaDados();
                return _APTNumero;
            }
        }

        /*
        public int? CON_PSC
        {
            get 
        }*/

        /// <summary>
        /// Indica se o apartamento tem seguro conteudo
        /// </summary>
        public bool APTSeguro
        {
            get 
            {
                
                if (!_APTSeguro.HasValue)                                    
                    _APTSeguro = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_bool("Select APTSeguro from AParTamentos where APT = @P1",APT);                                    
                return _APTSeguro.Value;
            }
            /*set 
            {
 
            }*/
        }

        #region Construtores
        /// <summary>
        /// Construtor principal
        /// </summary>
        /// <param name="APT">APT</param>
        public Apartamento(int APT)
        {
            _APT = APT;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="__CONCodigo">CONClodigo</param>
        /// <param name="__BLOCodigo">BLOCodigo</param>
        /// <param name="__APTNumeo">APTNumero</param>
        public Apartamento(string __CONCodigo, string __BLOCodigo, string __APTNumeo)
        {
            string Busca = "SELECT CON,BLO,APT\r\n" +
                           "FROM CONDOMINIOS INNER JOIN\r\n" +
                           "BLOCOS ON CON = BLO_CON INNER JOIN\r\n" +
                           "APARTAMENTOS ON BLO = APT_BLO\r\n" +
                           "WHERE (CONCodigo = @P1 and BLOCodigo = @P2 and APTNumero = @P3)";
            System.Data.DataRow Resultado = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(Busca, __CONCodigo, __BLOCodigo, __APTNumeo);
            if (Resultado != null)
            {
                _Encontrado = true;
                _CONCodigo = __CONCodigo;
                _BLOCodigo = __BLOCodigo;
                _APTNumero = __APTNumeo;
                _CON = (int)Resultado["CON"];
                _BLO = (int)Resultado["BLO"];
                _APT = (int)Resultado["APT"];
            }
        } 
        #endregion

    }
}
