﻿namespace Framework.objetosNeon
{
    partial class cContaCorrenteNeon
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditContaDg = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.spinAgencia = new DevExpress.XtraEditors.SpinEdit();
            this.spinAgenciaDg = new DevExpress.XtraEditors.SpinEdit();
            this.spinConta = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lookBCO = new DevExpress.XtraEditors.LookUpEdit();
            this.bANCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dBANCOS = new Framework.datasets.dBANCOS();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTitular = new DevExpress.XtraEditors.TextEdit();
            this.textEditCPF = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditContaDg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinAgencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinAgenciaDg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinConta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookBCO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBANCOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTitular.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCPF.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl1.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl1.Location = new System.Drawing.Point(37, 132);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 19);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Agência";
            // 
            // textEditContaDg
            // 
            this.textEditContaDg.Location = new System.Drawing.Point(262, 186);
            this.textEditContaDg.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textEditContaDg.MenuManager = this.BarManager_F;
            this.textEditContaDg.Name = "textEditContaDg";
            this.textEditContaDg.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditContaDg.Properties.Appearance.Options.UseFont = true;
            this.textEditContaDg.Properties.MaxLength = 1;
            this.textEditContaDg.Size = new System.Drawing.Size(25, 26);
            this.textEditContaDg.TabIndex = 4;
            this.textEditContaDg.Leave += new System.EventHandler(this.VoltaCor);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl2.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl2.Location = new System.Drawing.Point(37, 189);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 19);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Conta";
            // 
            // spinAgencia
            // 
            this.spinAgencia.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinAgencia.Location = new System.Drawing.Point(132, 129);
            this.spinAgencia.MenuManager = this.BarManager_F;
            this.spinAgencia.Name = "spinAgencia";
            this.spinAgencia.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinAgencia.Properties.Appearance.Options.UseFont = true;
            this.spinAgencia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.spinAgencia.Properties.IsFloatValue = false;
            this.spinAgencia.Properties.Mask.EditMask = "N00";
            this.spinAgencia.Size = new System.Drawing.Size(125, 26);
            this.spinAgencia.TabIndex = 1;
            this.spinAgencia.Leave += new System.EventHandler(this.VoltaCor);
            // 
            // spinAgenciaDg
            // 
            this.spinAgenciaDg.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinAgenciaDg.Location = new System.Drawing.Point(263, 129);
            this.spinAgenciaDg.MenuManager = this.BarManager_F;
            this.spinAgenciaDg.Name = "spinAgenciaDg";
            this.spinAgenciaDg.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinAgenciaDg.Properties.Appearance.Options.UseFont = true;
            this.spinAgenciaDg.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.spinAgenciaDg.Properties.IsFloatValue = false;
            this.spinAgenciaDg.Properties.Mask.EditMask = "N00";
            this.spinAgenciaDg.Size = new System.Drawing.Size(24, 26);
            this.spinAgenciaDg.TabIndex = 2;
            this.spinAgenciaDg.Leave += new System.EventHandler(this.VoltaCor);
            // 
            // spinConta
            // 
            this.spinConta.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinConta.Location = new System.Drawing.Point(132, 186);
            this.spinConta.MenuManager = this.BarManager_F;
            this.spinConta.Name = "spinConta";
            this.spinConta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinConta.Properties.Appearance.Options.UseFont = true;
            this.spinConta.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.spinConta.Properties.IsFloatValue = false;
            this.spinConta.Properties.Mask.EditMask = "N00";
            this.spinConta.Size = new System.Drawing.Size(125, 26);
            this.spinConta.TabIndex = 3;
            this.spinConta.Leave += new System.EventHandler(this.VoltaCor);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl3.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl3.Location = new System.Drawing.Point(37, 75);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(49, 19);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Banco";
            // 
            // lookBCO
            // 
            this.lookBCO.Location = new System.Drawing.Point(132, 72);
            this.lookBCO.MenuManager = this.BarManager_F;
            this.lookBCO.Name = "lookBCO";
            this.lookBCO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookBCO.Properties.Appearance.Options.UseFont = true;
            this.lookBCO.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookBCO.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BCO", "Código", 20, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BCONome", "Nome", 74, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookBCO.Properties.DataSource = this.bANCOSBindingSource;
            this.lookBCO.Properties.DisplayMember = "BCONome";
            this.lookBCO.Properties.KeyMember = "BCO";
            this.lookBCO.Properties.ValueMember = "BCO";
            this.lookBCO.Size = new System.Drawing.Size(155, 26);
            this.lookBCO.TabIndex = 0;
            this.lookBCO.EditValueChanged += new System.EventHandler(this.lookBCO_EditValueChanged);
            this.lookBCO.Leave += new System.EventHandler(this.VoltaCor);
            // 
            // bANCOSBindingSource
            // 
            this.bANCOSBindingSource.DataMember = "BANCOS";
            this.bANCOSBindingSource.DataSource = this.dBANCOS;
            // 
            // dBANCOS
            // 
            this.dBANCOS.DataSetName = "dBANCOS";
            this.dBANCOS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl4.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl4.Location = new System.Drawing.Point(37, 246);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(55, 19);
            this.labelControl4.TabIndex = 14;
            this.labelControl4.Text = "Titular";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl5.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl5.Location = new System.Drawing.Point(37, 303);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(82, 19);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "CPF/CNPJ";
            // 
            // textEditTitular
            // 
            this.textEditTitular.Location = new System.Drawing.Point(132, 243);
            this.textEditTitular.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textEditTitular.MenuManager = this.BarManager_F;
            this.textEditTitular.Name = "textEditTitular";
            this.textEditTitular.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditTitular.Properties.Appearance.Options.UseFont = true;
            this.textEditTitular.Properties.MaxLength = 35;
            this.textEditTitular.Size = new System.Drawing.Size(543, 26);
            this.textEditTitular.TabIndex = 5;
            // 
            // textEditCPF
            // 
            this.textEditCPF.Location = new System.Drawing.Point(132, 300);
            this.textEditCPF.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textEditCPF.MenuManager = this.BarManager_F;
            this.textEditCPF.Name = "textEditCPF";
            this.textEditCPF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditCPF.Properties.Appearance.Options.UseFont = true;
            this.textEditCPF.Properties.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit2_Properties_Validating);
            this.textEditCPF.Size = new System.Drawing.Size(543, 26);
            this.textEditCPF.TabIndex = 6;
            this.textEditCPF.Leave += new System.EventHandler(this.VoltaCor);
            // 
            // cContaCorrenteNeon
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textEditCPF);
            this.Controls.Add(this.textEditTitular);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.lookBCO);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.spinConta);
            this.Controls.Add(this.spinAgenciaDg);
            this.Controls.Add(this.spinAgencia);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textEditContaDg);
            this.Controls.Add(this.labelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "cContaCorrenteNeon";
            this.Size = new System.Drawing.Size(709, 356);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.textEditContaDg, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.spinAgencia, 0);
            this.Controls.SetChildIndex(this.spinAgenciaDg, 0);
            this.Controls.SetChildIndex(this.spinConta, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.lookBCO, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.textEditTitular, 0);
            this.Controls.SetChildIndex(this.textEditCPF, 0);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditContaDg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinAgencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinAgenciaDg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinConta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookBCO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBANCOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTitular.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCPF.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditContaDg;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit spinAgencia;
        private DevExpress.XtraEditors.SpinEdit spinAgenciaDg;
        private DevExpress.XtraEditors.SpinEdit spinConta;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lookBCO;
        private System.Windows.Forms.BindingSource bANCOSBindingSource;
        private datasets.dBANCOS dBANCOS;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit textEditTitular;
        private DevExpress.XtraEditors.TextEdit textEditCPF;
    }
}
