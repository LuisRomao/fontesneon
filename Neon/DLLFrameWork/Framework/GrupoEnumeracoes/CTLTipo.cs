﻿using System;
using System.Drawing;

namespace Framework
{

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum CTLTipo
    {
        /// <summary>
        /// 
        /// </summary>
        Caixa = 0,
        /// <summary>
        /// 
        /// </summary>
        Fundo = 1,
        /// <summary>
        /// 
        /// </summary>
        Rateio = 2,
        /// <summary>
        /// 
        /// </summary>
        Terceiro = 3,
        /// <summary>
        /// 
        /// </summary>
        Provisao = 4,
        /// <summary>
        /// 
        /// </summary>
        CreditosAnteriores = 5
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumCTLTipo : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumCTLTipo(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumCTLTipo _virEnumCTLTipoSt;

        /// <summary>
        /// VirEnum estático para campo CTLTipo
        /// </summary>
        public static virEnumCTLTipo virEnumCTLTipoSt
        {
            get
            {
                if (_virEnumCTLTipoSt == null)
                {
                    _virEnumCTLTipoSt = new virEnumCTLTipo(typeof(CTLTipo));
                    _virEnumCTLTipoSt.GravaNomes(CTLTipo.Caixa, "Caixa", Color.White);
                    _virEnumCTLTipoSt.GravaNomes(CTLTipo.Fundo, "Fundo de reserva", Color.Yellow);
                    _virEnumCTLTipoSt.GravaNomes(CTLTipo.Rateio, "Rateio", Color.Lime);
                    _virEnumCTLTipoSt.GravaNomes(CTLTipo.Terceiro, "Terceiros", Color.Pink);
                    _virEnumCTLTipoSt.GravaNomes(CTLTipo.Provisao, "Provisão", Color.Aqua);
                }
                return _virEnumCTLTipoSt;
            }
        }
    }
}