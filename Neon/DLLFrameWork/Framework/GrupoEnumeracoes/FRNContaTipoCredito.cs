﻿using System;
using System.Drawing;

namespace Framework
{

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum FRNContaTipoCredito
    {
        /// <summary>
        /// 
        /// </summary>
        ContaCorrete = 1,
        /// <summary>
        /// 
        /// </summary>
        Poupanca = 2
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumFRNContaTipoCredito : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumFRNContaTipoCredito(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumFRNContaTipoCredito _virEnumFRNContaTipoCreditoSt;

        /// <summary>
        /// VirEnum estático para campo FRNContaTipoCredito
        /// </summary>
        public static virEnumFRNContaTipoCredito virEnumFRNContaTipoCreditoSt
        {
            get
            {
                if (_virEnumFRNContaTipoCreditoSt == null)
                {
                    _virEnumFRNContaTipoCreditoSt = new virEnumFRNContaTipoCredito(typeof(FRNContaTipoCredito));
                    _virEnumFRNContaTipoCreditoSt.GravaNomes(FRNContaTipoCredito.ContaCorrete, "Conta Corrente");
                    _virEnumFRNContaTipoCreditoSt.GravaCor(FRNContaTipoCredito.ContaCorrete, System.Drawing.Color.White);
                    _virEnumFRNContaTipoCreditoSt.GravaNomes(FRNContaTipoCredito.Poupanca, "Poupança");
                    _virEnumFRNContaTipoCreditoSt.GravaCor(FRNContaTipoCredito.Poupanca, System.Drawing.Color.Yellow);
                }
                return _virEnumFRNContaTipoCreditoSt;
            }
        }
    }
}