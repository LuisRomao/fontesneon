﻿using System;
using System.Drawing;
using VirEnumeracoesNeon;

namespace Framework
{

    

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumCCTTipo : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumCCTTipo(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumCCTTipo _virEnumCCTTipoSt;

        /// <summary>
        /// VirEnum estático para campo CCTTipo
        /// </summary>
        public static virEnumCCTTipo virEnumCCTTipoSt
        {
            get
            {
                if (_virEnumCCTTipoSt == null)
                {
                    _virEnumCCTTipoSt = new virEnumCCTTipo(typeof(CCTTipo));
                    _virEnumCCTTipoSt.GravaNomes(CCTTipo.ContaCorrete, "Conta Corrente");
                    _virEnumCCTTipoSt.GravaCor(CCTTipo.ContaCorrete, System.Drawing.Color.White);
                    _virEnumCCTTipoSt.GravaNomes(CCTTipo.Poupanca, "Poupança");
                    _virEnumCCTTipoSt.GravaCor(CCTTipo.Poupanca, System.Drawing.Color.Yellow);
                    _virEnumCCTTipoSt.GravaNomes(CCTTipo.Aplicacao, "Aplicação");
                    _virEnumCCTTipoSt.GravaCor(CCTTipo.Aplicacao, System.Drawing.Color.Aqua);
                    _virEnumCCTTipoSt.GravaNomes(CCTTipo.ContaCorrete_com_Oculta, "C.C. com Aplicação");
                    _virEnumCCTTipoSt.GravaCor(CCTTipo.ContaCorrete_com_Oculta, System.Drawing.Color.Pink);
                    _virEnumCCTTipoSt.GravaNomes(CCTTipo.ContaCorreteOculta, "Conta Oculta");
                    _virEnumCCTTipoSt.GravaCor(CCTTipo.ContaCorreteOculta, System.Drawing.Color.PaleVioletRed);
                    _virEnumCCTTipoSt.GravaNomes(CCTTipo.ContaPool, "Conta Pool");
                    _virEnumCCTTipoSt.GravaCor(CCTTipo.ContaPool, System.Drawing.Color.Lime);
                    _virEnumCCTTipoSt.GravaNomes(CCTTipo.ContaDaADM, "Conta da ADM", Color.Khaki);
                }
                return _virEnumCCTTipoSt;
            }
        }
    }
}