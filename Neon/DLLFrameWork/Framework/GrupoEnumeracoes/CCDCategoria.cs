﻿using System;
using System.Drawing;

namespace Framework
{

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum CCDCategoria
    {
        /// <summary>
        /// 
        /// </summary>
        Cheque = 101,
        /// <summary>
        /// 
        /// </summary>
        DebitoAutomatico102 = 102,
        /// <summary>
        /// 
        /// </summary>        
        Estorno = 103,
        /// <summary>
        /// 
        /// </summary>
        DebitoAutomatico104 = 104,
        /// <summary>
        /// 
        /// </summary>
        Aplicacoes = 106,
        /// <summary>
        /// 
        /// </summary>
        DebitoAutomatico112 = 112,
        /// <summary>
        /// 
        /// </summary>
        Tarifas105 = 105,
        /// <summary>
        /// 
        /// </summary>
        Tarifas110 = 110,
        /// <summary>
        /// I.R. sobre os rendimentos
        /// </summary>
        IR_Rendimentos = 111,
        /// <summary>
        /// 
        /// </summary>
        Salarios = 113,
        /// <summary>
        /// 
        /// </summary>
        TransFContas117 = 117,
        /// <summary>
        /// 
        /// </summary>
        ChequeDevolvido118 = 118,
        /// <summary>
        /// 
        /// </summary>
        ChequeDevolvido119 = 119,
        /// <summary>
        /// 
        /// </summary>
        Deposito = 201,
        /// <summary>
        /// 
        /// </summary>
        Cobranca = 202,
        /// <summary>
        /// 
        /// </summary>
        Estorno203 = 203,
        /// <summary>
        /// 
        /// </summary>
        Estorno204 = 204,
        /// <summary>
        /// TED e DOC
        /// </summary>
        TED_DOC = 209,
        /// <summary>
        /// 
        /// </summary>       
        Estorno215 = 215,
        /// <summary>
        /// 
        /// </summary>
        TranInt_Rentabilidade = 205,
        /// <summary>
        /// 
        /// </summary>
        Resgate = 206,
        /// <summary>
        /// 
        /// </summary>
        TransFContas213 = 213,
        /// <summary>
        /// 
        /// </summary>
        Rendimento = 214,
        /// <summary>
        /// 
        /// </summary>
        PagamentoFornecedores = 217,
        /// <summary>
        /// 
        /// </summary>
        JurosContaInterna = 1,
        /// <summary>
        /// 
        /// </summary>
        Outros = 0
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumCCDCategoria : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumCCDCategoria(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumCCDCategoria _virEnumCCDCategoriaSt;

        /// <summary>
        /// VirEnum estático para campo CCDCategoria
        /// </summary>
        public static virEnumCCDCategoria virEnumCCDCategoriaSt
        {
            get
            {
                if (_virEnumCCDCategoriaSt == null)
                {
                    _virEnumCCDCategoriaSt = new virEnumCCDCategoria(typeof(CCDCategoria));
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Cheque, "101 - Cheque");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Cheque, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.DebitoAutomatico102, "102 - Débito Automático");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.DebitoAutomatico102, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Estorno, "103 - Estorno");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Estorno, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.DebitoAutomatico104, "104 - Débito Automático");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.DebitoAutomatico104, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.DebitoAutomatico112, "112 - Débito Automático");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.DebitoAutomatico112, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Tarifas105, "105 - Tarifa Bancária");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Tarifas105, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Aplicacoes, "Aplicações", Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Tarifas110, "110 - Tarifa Bancária");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Tarifas110, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Salarios, "113 - Cheque");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Salarios, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.ChequeDevolvido118, "118 - Cheque Devolvido");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.ChequeDevolvido118, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.ChequeDevolvido119, "119 - Cheque Devolvido");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.ChequeDevolvido119, System.Drawing.Color.Lime);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Deposito, "201 - Depósito");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Deposito, System.Drawing.Color.Pink);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Cobranca, "202 - Cobrança");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Cobranca, System.Drawing.Color.Pink);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Estorno203, "203 - Estorno");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Estorno203, System.Drawing.Color.Pink);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Estorno204, "204 - Estorno");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Estorno204, System.Drawing.Color.Pink);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Estorno215, "215 - Estorno");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Estorno215, System.Drawing.Color.Pink);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Resgate, "206 - Resgate");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Resgate, System.Drawing.Color.Pink);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Rendimento, "214 - Rendimento");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Rendimento, System.Drawing.Color.Pink);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.Outros, "Outros");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.Outros, System.Drawing.Color.White);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.TranInt_Rentabilidade, "Rentabilidade");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.TranInt_Rentabilidade, System.Drawing.Color.White);
                    _virEnumCCDCategoriaSt.GravaNomes(CCDCategoria.JurosContaInterna, "Rentabilidade Conta Oculta");
                    _virEnumCCDCategoriaSt.GravaCor(CCDCategoria.JurosContaInterna, System.Drawing.Color.Pink);

                }
                return _virEnumCCDCategoriaSt;
            }
        }
    }
}