﻿using System;
using System.Drawing;

namespace Framework
{


    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum PSCStatus
    {
        /// <summary>
        /// 
        /// </summary>
        Canelado = 0,
        /// <summary>
        /// 
        /// </summary>
        Ativo = 1
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumPSCStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumPSCStatus(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumPSCStatus _virEnumPSCStatusSt;

        /// <summary>
        /// VirEnum estático para campo PSCStatus
        /// </summary>
        public static virEnumPSCStatus virEnumPSCStatusSt
        {
            get
            {
                if (_virEnumPSCStatusSt == null)
                {
                    _virEnumPSCStatusSt = new virEnumPSCStatus(typeof(PSCStatus));
                    _virEnumPSCStatusSt.GravaNomes(PSCStatus.Canelado,"Cancelado",Color.Red);
                    _virEnumPSCStatusSt.GravaNomes(PSCStatus.Ativo, "Ativo", Color.Lime);                   
                }
                return _virEnumPSCStatusSt;
            }
        }
    }
}