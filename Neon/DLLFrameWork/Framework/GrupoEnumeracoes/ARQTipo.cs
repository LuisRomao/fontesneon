﻿using System;
using System.Drawing;

namespace Framework
{

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum ARQTipo
    {
        /// <summary>
        /// 
        /// </summary>
        Arquivo = 0,
        /// <summary>
        /// 
        /// </summary>
        Manual = 1
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumARQTipo : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumARQTipo(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumARQTipo _virEnumARQTipoSt;

        /// <summary>
        /// VirEnum estático para campo ARQTipo
        /// </summary>
        public static virEnumARQTipo virEnumARQTipoSt
        {
            get
            {
                if (_virEnumARQTipoSt == null)
                {
                    _virEnumARQTipoSt = new virEnumARQTipo(typeof(ARQTipo));
                    _virEnumARQTipoSt.GravaNomes(ARQTipo.Arquivo, "Arquivo");
                    _virEnumARQTipoSt.GravaCor(ARQTipo.Arquivo, System.Drawing.Color.White);
                    _virEnumARQTipoSt.GravaNomes(ARQTipo.Manual, "Manual");
                    _virEnumARQTipoSt.GravaCor(ARQTipo.Arquivo, System.Drawing.Color.White);

                }
                return _virEnumARQTipoSt;
            }
        }
    }
}