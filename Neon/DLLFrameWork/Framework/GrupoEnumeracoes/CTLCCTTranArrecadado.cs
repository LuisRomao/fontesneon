﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.GrupoEnumeracoes
{
    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum CTLCCTTranArrecadado
    {
        /// <summary>
        /// Não gera cheque
        /// </summary>
        Nao = 0,
        /// <summary>
        /// gera cheque para o último dia do balancete
        /// </summary>
        NoPeriodo = 1,
        /// <summary>
        /// gera cheque para o primeiro dia do próximo balancete
        /// </summary>
        NoProximoPeriodo =2,
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumCTLCCTTranArrecadado : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumCTLCCTTranArrecadado(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumCTLCCTTranArrecadado _virEnumCTLCCTTranArrecadadoSt;

        /// <summary>
        /// VirEnum estático para campo CTLCCTTranArrecadado
        /// </summary>
        public static virEnumCTLCCTTranArrecadado virEnumCTLCCTTranArrecadadoSt
        {
            get
            {
                if (_virEnumCTLCCTTranArrecadadoSt == null)
                {
                    _virEnumCTLCCTTranArrecadadoSt = new virEnumCTLCCTTranArrecadado(typeof(CTLCCTTranArrecadado));
                    _virEnumCTLCCTTranArrecadadoSt.GravaNomes(CTLCCTTranArrecadado.Nao, " - ");
                    _virEnumCTLCCTTranArrecadadoSt.GravaNomes(CTLCCTTranArrecadado.NoPeriodo, "Tranfere dentro do período");
                    _virEnumCTLCCTTranArrecadadoSt.GravaNomes(CTLCCTTranArrecadado.NoProximoPeriodo, "Tranfere no próximo balancete");
                }
                return _virEnumCTLCCTTranArrecadadoSt;
            }
        }
    }
}
