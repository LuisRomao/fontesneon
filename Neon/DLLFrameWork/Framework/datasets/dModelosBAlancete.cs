﻿namespace Framework.datasets {
    
    
    public partial class dModelosBAlancete 
    {
        private static dModelosBAlancete _dModelosBAlanceteSt;

        /// <summary>
        /// dataset estático:dModelosBAlancete
        /// </summary>
        public static dModelosBAlancete dModelosBAlanceteSt
        {
            get
            {
                if (_dModelosBAlanceteSt == null)
                {
                    _dModelosBAlanceteSt = new dModelosBAlancete();
                    _dModelosBAlanceteSt.ModeloBAlanceteTableAdapter.Fill(_dModelosBAlanceteSt.ModeloBAlancete);
                }
                return _dModelosBAlanceteSt;
            }
        }

        private dModelosBAlanceteTableAdapters.ModeloBAlanceteTableAdapter modeloBAlanceteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ModeloBAlancete
        /// </summary>
        public dModelosBAlanceteTableAdapters.ModeloBAlanceteTableAdapter ModeloBAlanceteTableAdapter
        {
            get
            {
                if (modeloBAlanceteTableAdapter == null)
                {
                    modeloBAlanceteTableAdapter = new dModelosBAlanceteTableAdapters.ModeloBAlanceteTableAdapter();
                    modeloBAlanceteTableAdapter.TrocarStringDeConexao();
                };
                return modeloBAlanceteTableAdapter;
            }
        }
    }
}
