using System;

namespace Framework.datasets
{
    /// <summary>
    /// 
    /// </summary>
    public enum ATVTipo
    {
        /// <summary>
        /// Aviso gen�rico
        /// </summary>
        Gererico = -1,
        /// <summary>
        /// Condom�nio desativado
        /// </summary>
        CondominioDesativado = 0,
        /// <summary>
        /// Cheque n�o encontrado
        /// </summary>
        ChequeNaoEncontrado = 1,
        /// <summary>
        /// Cheque em duplicidade
        /// </summary>
        ChequeDuplicidade = 2,
        /// <summary>
        /// Cheque com valor errado
        /// </summary>
        ChequeValor = 3,
        /// <summary>
        /// Debito autom�tico sem documento
        /// </summary>
        DebitoAutomaticoDocumento = 4,
        /// <summary>
        /// SEFIP para retificar
        /// </summary>
        SEFIPRetificar = 5,
        /// <summary>
        /// ISS
        /// </summary>
        ISSRetificar = 6,
        /// <summary>
        /// Sindico com INSS iregurar
        /// </summary>
        INSSSindico = 7,
        /// <summary>
        /// Retorno de tributo
        /// </summary>
        RetornoDeTributo = 8,
        /// <summary>
        /// Debito Automatico agendado Nao Ocorreu
        /// </summary>
        DebitoAutomaticoNaoOcorreu =9,
        /// <summary>
        /// Debito Automatico Nao Previsto
        /// </summary>
        DebitoAutomaticoNaoPrevisto = 10,
        /// <summary>
        /// O d�bito autom�tico ocorreu com o valor incorreto
        /// </summary>
        DebitoAutomaticoComValorIncorreto = 11,
        /// <summary>
        /// Compet�ncia do d�bito autom�tico pulou.
        /// </summary>
        DebitoAutomaticoPulado = 12,
        /// <summary>
        /// Comunica��o interna RTF
        /// </summary>
        ComunicacaoInternaRTF = 13,
        /// <summary>
        /// Debito autom�tico n�o ocorreu
        /// </summary>
        DebitoAutomaticoNaoOcorreu1 = 14
    }

    /// <summary>
    /// virEnum para campo ATV
    /// </summary>
    public class virEnumATVTipo : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumATVTipo(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumATVTipo _virEnumATVTipoSt;

        /// <summary>
        /// VirEnum est�tico para campo ATVTipo
        /// </summary>
        public static virEnumATVTipo virEnumATVTipoSt
        {
            get
            {
                if (_virEnumATVTipoSt == null)
                {
                    _virEnumATVTipoSt = new virEnumATVTipo(typeof(ATVTipo));
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.ChequeDuplicidade, "Cheque baixado em duplicidade");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.ChequeNaoEncontrado, "Cheque n�o Encontrado");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.ChequeValor, "Valor de cheque Incorreto");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.CondominioDesativado, "Condom�nio Desativado");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.DebitoAutomaticoDocumento, "D�bito Autom�tico sem a conta");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.INSSSindico, "INSS de s�ndico irregular");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.ISSRetificar, "ISS cadastrado ap�s fechamento");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.RetornoDeTributo, "Retorno de tributo");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.SEFIPRetificar, "Retificar SEFIP");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.Gererico, "Gen�rico");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.DebitoAutomaticoNaoOcorreu, "d�bito autom�tico agendado n�o ocorreu");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.DebitoAutomaticoNaoPrevisto, "d�bito autom�tico n�o programado");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.DebitoAutomaticoComValorIncorreto, "d�bito com valor incorreto");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.DebitoAutomaticoPulado, "d�bito autom�tico pulou uma compet�ncia");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.ComunicacaoInternaRTF, "Comunica��o Interna");
                    _virEnumATVTipoSt.GravaNomes(ATVTipo.DebitoAutomaticoNaoOcorreu1, "d�bito autom�tico n�o ocorreu");
                }
                return _virEnumATVTipoSt;
            }
        }
    }

    
}
