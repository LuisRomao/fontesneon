using System;

namespace Framework.datasets
{
    /// <summary>
    /// 
    /// </summary>
    public enum AGGStatus
    {
        /// <summary>
        /// 
        /// </summary>
        NaoIniciado = 0,
        /// <summary>
        /// 
        /// </summary>
        EmProducao = 1,
        /// <summary>
        /// 
        /// </summary>
        Terminado = 2
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumAGGStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumAGGStatus(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumAGGStatus _virEnumAGGStatusSt;

        /// <summary>
        /// VirEnum est�tico para campo AGGStatus
        /// </summary>
        public static virEnumAGGStatus virEnumAGGStatusSt
        {
            get
            {
                if (_virEnumAGGStatusSt == null)
                {
                    _virEnumAGGStatusSt = new virEnumAGGStatus(typeof(AGGStatus));
                    _virEnumAGGStatusSt.GravaNomes(AGGStatus.NaoIniciado, "N�o Iniciado");
                    _virEnumAGGStatusSt.GravaCor(AGGStatus.NaoIniciado, System.Drawing.Color.LightGray);
                    _virEnumAGGStatusSt.GravaNomes(AGGStatus.EmProducao, "Em produ��o");
                    _virEnumAGGStatusSt.GravaCor(AGGStatus.EmProducao, System.Drawing.Color.Yellow);
                    _virEnumAGGStatusSt.GravaNomes(AGGStatus.Terminado, "Terminado");
                    _virEnumAGGStatusSt.GravaCor(AGGStatus.Terminado, System.Drawing.Color.Lime);

                }
                return _virEnumAGGStatusSt;
            }
        }
    }
}
