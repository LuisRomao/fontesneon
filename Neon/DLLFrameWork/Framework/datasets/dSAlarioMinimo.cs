﻿namespace Framework.datasets {


    partial class dSAlarioMinimo
    {
        /// <summary>
        /// Valor do salario minimo na competencia
        /// </summary>
        /// <param name="Comp"></param>
        /// <returns></returns>
        public static decimal Valor(Framework.objetosNeon.Competencia Comp)
        {
            dSAlarioMinimo.SAlarioMinimoRow rowSAM;
            if (sAlarioMinimoTableAdapter.FillByComp(dSAlarioMinimoSt.SAlarioMinimo, Comp.CompetenciaBind) == 1)
            {
                rowSAM = dSAlarioMinimoSt.SAlarioMinimo[0];
                return rowSAM.SAMValor;
            }
            else
                return 0;
        }



        private static dSAlarioMinimo _dSAlarioMinimoSt;

        /// <summary>
        /// dataset estático:dSAlarioMinimo
        /// </summary>
        public static dSAlarioMinimo dSAlarioMinimoSt
        {
            get
            {
                if (_dSAlarioMinimoSt == null)
                    _dSAlarioMinimoSt = new dSAlarioMinimo();
                return _dSAlarioMinimoSt;
            }
        }

        private static dSAlarioMinimoTableAdapters.SAlarioMinimoTableAdapter sAlarioMinimoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SAlarioMinimo
        /// </summary>
        public static dSAlarioMinimoTableAdapters.SAlarioMinimoTableAdapter SAlarioMinimoTableAdapter
        {
            get
            {
                if (sAlarioMinimoTableAdapter == null)
                {
                    sAlarioMinimoTableAdapter = new dSAlarioMinimoTableAdapters.SAlarioMinimoTableAdapter();
                    sAlarioMinimoTableAdapter.TrocarStringDeConexao();
                };
                return sAlarioMinimoTableAdapter;
            }
        }
    }
}
