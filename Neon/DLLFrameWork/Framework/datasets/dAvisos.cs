﻿namespace Framework.datasets
{


    public partial class dAvisos
    {
        private dAvisosTableAdapters.TipoAVisoTableAdapter tipoAVisoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TipoAViso
        /// </summary>
        public dAvisosTableAdapters.TipoAVisoTableAdapter TipoAVisoTableAdapter
        {
            get
            {
                if (tipoAVisoTableAdapter == null)
                {
                    tipoAVisoTableAdapter = new dAvisosTableAdapters.TipoAVisoTableAdapter();
                    tipoAVisoTableAdapter.TrocarStringDeConexao();
                };
                return tipoAVisoTableAdapter;
            }
        }

        private dAvisosTableAdapters.AVIsosTableAdapter aVIsosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: AVIsos
        /// </summary>
        public dAvisosTableAdapters.AVIsosTableAdapter AVIsosTableAdapter
        {
            get
            {
                if (aVIsosTableAdapter == null)
                {
                    aVIsosTableAdapter = new dAvisosTableAdapters.AVIsosTableAdapter();
                    aVIsosTableAdapter.TrocarStringDeConexao();
                };
                return aVIsosTableAdapter;
            }
        }
    }
}
