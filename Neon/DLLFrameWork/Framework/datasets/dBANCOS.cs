﻿namespace Framework.datasets {


    partial class dBANCOS
    {
        private static dBANCOS _dBANCOSSt;

        /// <summary>
        /// dataset estático:dBANCOS
        /// </summary>
        public static dBANCOS dBANCOSSt
        {
            get
            {
                if (_dBANCOSSt == null)
                {
                    _dBANCOSSt = new dBANCOS();
                    _dBANCOSSt.BANCOSTableAdapter.Fill(_dBANCOSSt.BANCOS);
                }
                return _dBANCOSSt;
            }
        }

        private dBANCOSTableAdapters.BANCOSTableAdapter bANCOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BANCOS
        /// </summary>
        public dBANCOSTableAdapters.BANCOSTableAdapter BANCOSTableAdapter
        {
            get
            {
                if (bANCOSTableAdapter == null)
                {
                    bANCOSTableAdapter = new dBANCOSTableAdapters.BANCOSTableAdapter();
                    bANCOSTableAdapter.TrocarStringDeConexao();
                };
                return bANCOSTableAdapter;
            }
        }
    }
}
