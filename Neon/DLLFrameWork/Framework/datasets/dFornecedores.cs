﻿using System;

namespace Framework.datasets
{

    [Obsolete("O cadastro de fornecedores foi unificado. Não utilizar mais desta forma")]
    partial class dFornecedores
    {
        /// <summary>
        /// Coleção com os dFornecedores. Um para cada tipo
        /// </summary>
        static private System.Collections.SortedList _dFornecedoresST;

        /// <summary>
        /// Coleção com os dFornecedores. Um para cada tipo
        /// </summary>
        static private System.Collections.SortedList dFornecedoresST
        {
            get
            {
                if (_dFornecedoresST == null)
                    _dFornecedoresST = new System.Collections.SortedList();
                return _dFornecedoresST;
            }
        }



        /// <summary>
        /// Fornece um dFornecedores para o tipo solicitado
        /// </summary>
        /// <param name="Tipo">Tipo de fornecedor</param>
        /// <returns></returns>        
        [Obsolete("O cadastro de fornecedores foi unificado. Não utilizar mais desta forma")]
        static public dFornecedores GetdFornecedoresST(string Tipo)
        {
            if (!dFornecedoresST.ContainsKey(Tipo))
            {
                dFornecedores novodFornecedores = new dFornecedores();
                novodFornecedores.FORNECEDORESTableAdapter.Fill(novodFornecedores.FORNECEDORES, Tipo);
                dFornecedoresST.Add(Tipo, novodFornecedores);
            }
            return (dFornecedores)dFornecedoresST[Tipo];
        }

        /// <summary>
        /// Carrega novamente os dados
        /// </summary>
        /// <param name="Tipo"></param>
        [Obsolete("O cadastro de fornecedores foi unificado. Não utilizar mais desta forma")]        
        static public void RefreshDados(string Tipo)
        {
            if (!dFornecedoresST.ContainsKey(Tipo))
                GetdFornecedoresST(Tipo);
            else
            {
                dFornecedores novodFornecedores = (dFornecedores)dFornecedoresST[Tipo];
                novodFornecedores.FORNECEDORESTableAdapter.Fill(novodFornecedores.FORNECEDORES, Tipo);
            }
        }

        private dFornecedoresTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;

        /// <summary>
        /// Table adapter para todos os fornecedores
        /// </summary>
        public dFornecedoresTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new Framework.datasets.dFornecedoresTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao();
                };
                return fORNECEDORESTableAdapter;
            }
        }
    }
}
