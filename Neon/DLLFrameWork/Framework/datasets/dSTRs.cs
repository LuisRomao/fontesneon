﻿namespace Framework.datasets {


    partial class dSTRs
    {
        /// <summary>
        /// Sigla padrão
        /// </summary>
        private string Sigla;

       
        

        /// <summary>
        /// Inclui linha
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Text"></param>
        public void Add(int Chave, string Text) {
            STRs.AddSTRsRow(Chave, Text, Sigla);
            STRsTableAdapter.Update(STRs);
            STRs.AcceptChanges();
        }

        /// <summary>
        /// Remove linha
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Text"></param>
        public void Dell(int Chave, string Text)
        {
            STRsRow Matar = null;
            foreach (STRsRow row in STRs)
            {
                if ((row.STRTexto == Text) && (row.STRChave == Chave))
                    Matar = row;
            };
            if (Matar != null) {
                Matar.Delete();
                STRsTableAdapter.Update(STRs);
                STRs.AcceptChanges();
            }
        }

        /// <summary>
        /// Carrega a tabela
        /// </summary>
        /// <param name="_Sigla"></param>
        public void Fill(string _Sigla) {
            Sigla = _Sigla;
            STRsTableAdapter.Fill(STRs, Sigla);
        }

        private dSTRsTableAdapters.STRsTableAdapter sTRsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: STRs
        /// </summary>
        public dSTRsTableAdapters.STRsTableAdapter STRsTableAdapter
        {
            get
            {
                if (sTRsTableAdapter == null)
                {
                    sTRsTableAdapter = new dSTRsTableAdapters.STRsTableAdapter();
                    sTRsTableAdapter.TrocarStringDeConexao();
                };
                return sTRsTableAdapter;
            }
        }
    }
}
