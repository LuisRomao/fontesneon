﻿namespace Framework.datasets {


    partial class dEmpresas
    {
        private static dEmpresas _dEmpresasSt;

        /// <summary>
        /// dataset estático:dEmpresas
        /// </summary>
        public static dEmpresas dEmpresasSt
        {
            get
            {
                if (_dEmpresasSt == null)
                {
                    _dEmpresasSt = new dEmpresas();
                    _dEmpresasSt.EMPRESASTableAdapter.Fill(_dEmpresasSt.EMPRESAS);
                }
                return _dEmpresasSt;
            }
        }

        private dEmpresasTableAdapters.EMPRESASTableAdapter eMPRESASTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EMPRESAS
        /// </summary>
        public dEmpresasTableAdapters.EMPRESASTableAdapter EMPRESASTableAdapter
        {
            get
            {
                if (eMPRESASTableAdapter == null)
                {
                    eMPRESASTableAdapter = new dEmpresasTableAdapters.EMPRESASTableAdapter();
                    eMPRESASTableAdapter.TrocarStringDeConexao();
                };
                return eMPRESASTableAdapter;
            }
        }
    }
}
