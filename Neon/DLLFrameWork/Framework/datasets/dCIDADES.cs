﻿namespace Framework.datasets {

    /// <summary>
    /// Cadastro das cidades
    /// </summary>
    partial class dCIDADES
    {
        private static dCIDADES _dCIDADESSt;

        /// <summary>
        /// dataset estático:dCIDADES
        /// </summary>
        public static dCIDADES dCIDADESSt
        {
            get
            {
                if (_dCIDADESSt == null)
                {
                    _dCIDADESSt = new dCIDADES();
                    _dCIDADESSt.CIDADESTableAdapter.Fill(_dCIDADESSt.CIDADES);
                }
                return _dCIDADESSt;
            }
        }

        /// <summary>
        /// Refresh dos dados
        /// </summary>
        /// <returns></returns>
        public int RefreshDados() {
            return CIDADESTableAdapter.Fill(CIDADES);
        }

        private dCIDADESTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CIDADES
        /// </summary>
        public dCIDADESTableAdapters.CIDADESTableAdapter CIDADESTableAdapter
        {
            get
            {
                if (cIDADESTableAdapter == null)
                {
                    cIDADESTableAdapter = new dCIDADESTableAdapters.CIDADESTableAdapter();
                    cIDADESTableAdapter.TrocarStringDeConexao();
                };
                return cIDADESTableAdapter;
            }
        }
    }
}
