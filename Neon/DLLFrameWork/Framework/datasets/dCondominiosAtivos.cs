﻿/*
LH - 23/4/2014        13.2.8.29 - FIX faltava campo copiaCON
MR - 03/12/2014 10:00           - Inclusão do campo com cod. de comunicacao para tributos, usando no envio de arquivo para Bradesco (CONCodigoComunicacaoTributos) no CONDOMINIOSTableAdapter
MR - 26/01/2014 16:00           - Inclusão do campo de segundo gerente nos Fills (CONAuditor2_USU) no CONDOMINIOSTableAdapter
*/

namespace Framework.datasets {

    using FrameworkProc;

    partial class dCondominiosAtivos
    {
        static private dCondominiosAtivos _dCondominiosAtivosST;
        static private dCondominiosAtivos _dCondominiosAtivosSTF;

        /// <summary>
        /// DataSet com os condomínios ativos
        /// </summary>
        static public dCondominiosAtivos dCondominiosAtivosST
        {
            get
            {
                if (_dCondominiosAtivosST == null)
                {
                    _dCondominiosAtivosST = new dCondominiosAtivos();
                    _dCondominiosAtivosST.SoAtivos = true;
                }
                if (_dCondominiosAtivosST.CONDOMINIOS.Count == 0)
                    _dCondominiosAtivosST.CarregaCondominios();
                return _dCondominiosAtivosST;
            }
        }

        /// <summary>
        /// DataSet com os condomínios ativos
        /// </summary>
        static public dCondominiosAtivos dCondominiosAtivosSTF
        {
            get
            {
                if (_dCondominiosAtivosSTF == null)
                {
                    _dCondominiosAtivosSTF = new dCondominiosAtivos(EMPTProc.EMPTipo.Filial);
                    _dCondominiosAtivosSTF.SoAtivos = true;
                }
                if (_dCondominiosAtivosSTF.CONDOMINIOS.Count == 0)
                    _dCondominiosAtivosSTF.CarregaCondominios();
                return _dCondominiosAtivosSTF;
            }
        }

        /// <summary>
        /// DataSet com os condomínios ativos
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        static public dCondominiosAtivos dCondominiosAtivosSTX(EMPTProc.EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTProc.EMPTipo.Local ? dCondominiosAtivosST : dCondominiosAtivosSTF;                                        
        }

        static private dCondominiosAtivos _dCondominiosTodosST;
        static private dCondominiosAtivos _dCondominiosTodosSTF;

        /// <summary>
        /// DataSet com os condomínios ativos
        /// </summary>
        static public dCondominiosAtivos dCondominiosTodosST
        {
            get
            {
                if (_dCondominiosTodosST == null)
                {
                    _dCondominiosTodosST = new dCondominiosAtivos();
                    _dCondominiosTodosST.SoAtivos = false;
                }
                if (_dCondominiosTodosST.CONDOMINIOS.Count == 0)
                    _dCondominiosTodosST.CarregaCondominios();
                return _dCondominiosTodosST;
            }
        }

        /// <summary>
        /// DataSet com os condomínios ativos
        /// </summary>
        static public dCondominiosAtivos dCondominiosTodosSTF
        {
            get
            {
                if (_dCondominiosTodosSTF == null)
                {
                    _dCondominiosTodosSTF = new dCondominiosAtivos(EMPTProc.EMPTipo.Filial);
                    _dCondominiosTodosSTF.SoAtivos = false;
                }
                if (_dCondominiosTodosSTF.CONDOMINIOS.Count == 0)
                    _dCondominiosTodosSTF.CarregaCondominios();
                return _dCondominiosTodosSTF;
            }
        }

        /// <summary>
        /// DataSet com os condomínios TODOS
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        static public dCondominiosAtivos dCondominiosTodosSTX(EMPTProc.EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTProc.EMPTipo.Local ? dCondominiosTodosST : dCondominiosTodosSTF;
        }

        private EMPTProc _EMPTipo;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTipo
        {
            get
            {
                if (_EMPTipo == null)
                    _EMPTipo = new EMPTProc(EMPTProc.EMPTipo.Local);
                return _EMPTipo;
            }
        }

        /// <summary>
        /// Condominios ativos
        /// </summary>
        /// <param name="_EMPTipo"></param>
        public dCondominiosAtivos(EMPTProc.EMPTipo _EMPTipo)
            : this()
        {
            this._EMPTipo = new EMPTProc(_EMPTipo);
        }

        /// <summary>
        /// Altera o Status do condominio
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="novoStatus"></param>
        /// <param name="Motivo"></param>
        /// <returns></returns>
        public bool AlteraStatus(int CON, Enumeracoes.CONStatus novoStatus,string Motivo)
        {
            StatusCONdominiosTableAdapter.EmbarcaEmTransST();
            StatusCONdominiosTableAdapter.Fill(StatusCONdominios, CON);
            StatusCONdominiosRow row = StatusCONdominios[0];
            if (row.CONStatus == (int)novoStatus)
                return false;
            row.CONStatus = (int)novoStatus;
            row.CONHistorico = string.Format("{0}{1}",
                                              row.IsCONHistoricoNull() ? "" : row.CONHistorico + "\r\n\r\n",                                                                                            
                                              Motivo);
            StatusCONdominiosTableAdapter.Update(row);
            CONDOMINIOSRow rowCON = CONDOMINIOS.FindByCON(CON);
            if (rowCON != null)
                rowCON.CONStatus = (int)novoStatus;
            return true;
        }

        private dCondominiosAtivosTableAdapters.StatusCONdominiosTableAdapter statusCONdominiosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: STATUSCONDOMINIOS
        /// </summary>
        public dCondominiosAtivosTableAdapters.StatusCONdominiosTableAdapter StatusCONdominiosTableAdapter
        {
            get
            {
                if (statusCONdominiosTableAdapter == null)
                {
                    statusCONdominiosTableAdapter = new dCondominiosAtivosTableAdapters.StatusCONdominiosTableAdapter();
                    statusCONdominiosTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return statusCONdominiosTableAdapter;
            }
        }

        private bool SoAtivos;
    
        

        /// <summary>
        /// Encontra o condomínio pelo CODCON
        /// </summary>
        /// <param name="codcon"></param>
        /// <returns></returns>
        public CONDOMINIOSRow BuscaPorCodCon (string codcon){            
            int indice;
            if (DVCODCON == null)
            {
                DVCODCON = new System.Data.DataView(CONDOMINIOS);
                DVCODCON.Sort = "CONCODIGO";
            }
            indice = DVCODCON.Find(codcon);
            if (indice >= 0)
                return (CONDOMINIOSRow)DVCODCON[indice].Row;
            //foreach (CONDOMINIOSRow rowCandidato in CONDOMINIOS)
            //    if(rowCandidato.CONCodigo == codcon)
            //        return rowCandidato;
            return null;
        }

        private System.Data.DataView DVCODCON;
        private System.Data.DataView DVFolha1;
        private System.Data.DataView DVFolha2;

        /// <summary>
        /// Encontra o condomínio pelo código da folha
        /// </summary>
        /// <param name="Cod"></param>
        /// <returns></returns>
        public CONDOMINIOSRow BuscaPorCodFolha(int Cod)
        {
            int indice;
            if (DVFolha1 == null)
            {
                DVFolha1 = new System.Data.DataView(CONDOMINIOS);
                DVFolha1.Sort = "CONCodigoFolha1";
            }
            indice = DVFolha1.Find(Cod);
            if (indice >= 0)
                return (CONDOMINIOSRow)DVFolha1[indice].Row;            
            return null;            
        }

        /// <summary>
        /// Encontra o condomínio pelo código da folha alternativo
        /// </summary>
        /// <param name="Cod"></param>
        /// <returns></returns>
        public CONDOMINIOSRow BuscaPorCodFolha(string Cod)
        {
            int indice;                        
            if (DVFolha2 == null)
            {
                DVFolha2 = new System.Data.DataView(CONDOMINIOS);
                DVFolha2.Sort = "CONCodigoFolha2A";
            }
            indice = DVFolha2.Find(Cod);
            if (indice >= 0)
                return (CONDOMINIOSRow)DVFolha2[indice].Row;
            return null;
        }

        private dCondominiosAtivosTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;

        /// <summary>
        /// TableAdapter
        /// </summary>
        public dCondominiosAtivosTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter {
            get {
                if (cONDOMINIOSTableAdapter == null) {
                    cONDOMINIOSTableAdapter = new Framework.datasets.dCondominiosAtivosTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dCondominiosAtivosTableAdapters.SindicosTableAdapter sindicosTableAdapter;

        /// <summary>
        /// Sindicos TableAdapter
        /// </summary>
        public dCondominiosAtivosTableAdapters.SindicosTableAdapter SindicosTableAdapter
        {
            get
            {
                if (sindicosTableAdapter == null)
                {
                    sindicosTableAdapter = new Framework.datasets.dCondominiosAtivosTableAdapters.SindicosTableAdapter();
                    sindicosTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return sindicosTableAdapter;
            }
        }

        /// <summary>
        /// Carrega os condominios
        /// </summary>
        
        public void CarregaCondominios() {
            if (!SoAtivos)
                CONDOMINIOSTableAdapter.FillByTodos(CONDOMINIOS);
            else
                CONDOMINIOSTableAdapter.FillByAtivos(CONDOMINIOS, EMPTipo.EMP);                        
        }

        /// <summary>
        /// Carrega os síndicos
        /// </summary>
        /// <param name="refresh">Recarga</param>
        public void CarregaSindicos(bool refresh)
        {
            if ((!refresh) && (Sindicos.Rows.Count > 0))
                return;
            SindicosTableAdapter.Fill(Sindicos);
        }
    }
}
