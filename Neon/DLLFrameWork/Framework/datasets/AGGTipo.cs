using System;

namespace Framework.datasets
{
    /// <summary>
    /// Tipo de AGG, se refere ao pr�prio campo AGG
    /// </summary>
    public enum AGGTipo
    {
        /// <summary>
        /// 
        /// </summary>
        SEFIP = 0,
        /// <summary>
        /// 
        /// </summary>
        ISS = 1
    }

    /// <summary>
    /// virEnum para campo AGG
    /// </summary>
    public class virEnumAGGTipo : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumAGGTipo(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumAGGTipo _virEnumAGGTipoSt;

        /// <summary>
        /// VirEnum est�tico para campo AGGTipo
        /// </summary>
        public static virEnumAGGTipo virEnumAGGTipoSt
        {
            get
            {
                if (_virEnumAGGTipoSt == null)
                {
                    _virEnumAGGTipoSt = new virEnumAGGTipo(typeof(AGGTipo));
                    _virEnumAGGTipoSt.GravaNomes(AGGTipo.SEFIP, "SEFIP");
                    _virEnumAGGTipoSt.GravaCor(AGGTipo.SEFIP, System.Drawing.Color.White);
                    _virEnumAGGTipoSt.GravaNomes(AGGTipo.ISS, "ISS");
                    _virEnumAGGTipoSt.GravaCor(AGGTipo.ISS, System.Drawing.Color.White);

                }
                return _virEnumAGGTipoSt;
            }
        }
    }
}
