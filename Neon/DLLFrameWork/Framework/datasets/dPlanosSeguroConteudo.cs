﻿namespace Framework.datasets {
    
    
    public partial class dPlanosSeguroConteudo 
    {
        private static dPlanosSeguroConteudo _dPlanosSeguroConteudoSt;

        /// <summary>
        /// dataset estático:dPlanosSeguroConteudo
        /// </summary>
        public static dPlanosSeguroConteudo dPlanosSeguroConteudoSt
        {
            get
            {
                if (_dPlanosSeguroConteudoSt == null)
                {
                    _dPlanosSeguroConteudoSt = new dPlanosSeguroConteudo();
                    _dPlanosSeguroConteudoSt.PlanosSeguroConteudoTableAdapter.Fill(_dPlanosSeguroConteudoSt.PlanosSeguroConteudo);
                }
                return _dPlanosSeguroConteudoSt;
            }
        }

        private dPlanosSeguroConteudoTableAdapters.PlanosSeguroConteudoTableAdapter planosSeguroConteudoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PlanosSeguroConteudo
        /// </summary>
        public dPlanosSeguroConteudoTableAdapters.PlanosSeguroConteudoTableAdapter PlanosSeguroConteudoTableAdapter
        {
            get
            {
                if (planosSeguroConteudoTableAdapter == null)
                {
                    planosSeguroConteudoTableAdapter = new dPlanosSeguroConteudoTableAdapters.PlanosSeguroConteudoTableAdapter();
                    planosSeguroConteudoTableAdapter.TrocarStringDeConexao();
                };
                return planosSeguroConteudoTableAdapter;
            }
        }
    }
}
