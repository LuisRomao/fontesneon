﻿namespace Framework.datasets {


    partial class dUSUarios
    {
        #region Construtores estáticos
        private static dUSUarios _dUSUariosSt;

        /// <summary>
        /// dataset estático:dUSUarios
        /// </summary>
        public static dUSUarios dUSUariosSt
        {
            get
            {
                if (_dUSUariosSt == null)
                {
                    _dUSUariosSt = new dUSUarios();

                };
                if ((!_dUSUariosSt.DesignMode) && (_dUSUariosSt.USUarios.Rows.Count == 0))
                    _dUSUariosSt.USUariosTableAdapter.Fill(_dUSUariosSt.USUarios);
                return _dUSUariosSt;
            }
        }

        private static dUSUarios _dUSUariosStTodos;

        /// <summary>
        /// dataset estático:dUSUarios
        /// </summary>
        public static dUSUarios dUSUariosStTodos
        {
            get
            {
                if (_dUSUariosStTodos == null)
                {
                    _dUSUariosStTodos = new dUSUarios();

                };
                if ((!_dUSUariosStTodos.DesignMode) && (_dUSUariosStTodos.USUarios.Rows.Count == 0))
                    _dUSUariosStTodos.USUariosTableAdapter.FillByCompleto(_dUSUariosStTodos.USUarios);
                return _dUSUariosStTodos;
            }
        }

        private static dUSUarios _dUSUariosStTodosF;

        /// <summary>
        /// dataset estático:dUSUarios (Filial)
        /// </summary>
        public static dUSUarios dUSUariosStTodosF
        {
            get
            {
                if (_dUSUariosStTodosF == null)
                {
                    _dUSUariosStTodosF = new dUSUarios(FrameworkProc.EMPTProc.EMPTipo.Filial);

                };
                if (_dUSUariosStTodosF.USUarios.Rows.Count == 0)
                    _dUSUariosStTodosF.USUariosTableAdapter.FillByCompleto(_dUSUariosStTodosF.USUarios);
                return _dUSUariosStTodosF;
            }
        }

        /// <summary>
        /// dataset estático:dUSUarios (Filial / Local)
        /// </summary>
        public static dUSUarios dUSUariosStTodosX(FrameworkProc.EMPTProc.EMPTipo EMPTipo)
        {
            return EMPTipo == FrameworkProc.EMPTProc.EMPTipo.Local ? dUSUariosStTodos : dUSUariosStTodosF;
        }

        private static dUSUarios _dUSUariosStGerentes;

        /// <summary>
        /// dataset estático:dUSUariosGerentes
        /// </summary>
        public static dUSUarios dUSUariosStGerentes
        {
            get
            {
                if (_dUSUariosStGerentes == null)
                {
                    _dUSUariosStGerentes = new dUSUarios();

                };
                if ((!_dUSUariosStGerentes.DesignMode) && (_dUSUariosStGerentes.USUarios.Rows.Count == 0))
                    _dUSUariosStGerentes.USUariosTableAdapter.FillByGerentes(_dUSUariosStGerentes.USUarios);
                return _dUSUariosStGerentes;
            }
        }

        /// <summary>
        /// Usuario no banco da filial
        /// </summary>
        /// <param name="USU"></param>
        /// <returns></returns>
        public static int USUequivalente(int USU)
        {
            USUariosRow rowUSU = dUSUariosStTodos.USUarios.FindByUSU(USU);
            if (!rowUSU.IsUSU__USUfilialNull())
                return rowUSU.USU__USUfilial;
            System.Data.DataRow DRLocal = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select * from usuarios where USU = @P1", USU);
            DRLocal["usu__USUfilial"] = USU;
            DRLocal["USU_CID"] = System.DBNull.Value;
            DRLocal["USUInclusao_USU"] = System.DBNull.Value;
            DRLocal["USUAtualizacao_USU"] = System.DBNull.Value;
            DRLocal["USU_PER"] = System.DBNull.Value;
            DRLocal["USU"] = System.DBNull.Value;
            VirMSSQL.TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Filial).Insert("USUarios", DRLocal, false, false);
            int? USUF = VirMSSQL.TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Filial).BuscaEscalar_int("Select USU from usuarios where usu__USUfilial = @P1", USU);
            rowUSU.USU__USUfilial = USUF.Value;
            dUSUariosStTodos.USUariosTableAdapter.Update(rowUSU);
            return USUF.Value;
        }

        /// <summary>
        /// Usuario logado
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public static int USUAtual(FrameworkProc.EMPTProc.EMPTipo EMPTipo = FrameworkProc.EMPTProc.EMPTipo.Local)
        {
            if (EMPTipo == FrameworkProc.EMPTProc.EMPTipo.Local)
                return Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
            else
                return Framework.datasets.dUSUarios.USUequivalente(Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU);
        }

        #endregion

        private FrameworkProc.EMPTProc _EMPTipo;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public FrameworkProc.EMPTProc EMPTipo
        {
            get
            {
                if (_EMPTipo == null)
                    _EMPTipo = new FrameworkProc.EMPTProc(FrameworkProc.EMPTProc.EMPTipo.Local);
                return _EMPTipo;
            }
        }

        /// <summary>
        /// Construitor com EMPTipo
        /// </summary>
        /// <param name="_EMPTipo"></param>
        public dUSUarios(FrameworkProc.EMPTProc.EMPTipo _EMPTipo)
            : this()
        {
            this._EMPTipo = new FrameworkProc.EMPTProc(_EMPTipo);
        }

        private dUSUariosTableAdapters.USUariosTableAdapter uSUariosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: USUarios
        /// </summary>
        public dUSUariosTableAdapters.USUariosTableAdapter USUariosTableAdapter
        {
            get
            {
                if (uSUariosTableAdapter == null)
                {
                    uSUariosTableAdapter = new dUSUariosTableAdapters.USUariosTableAdapter();
                    uSUariosTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                    
                };
                return uSUariosTableAdapter;
            }
        }

        /// <summary>
        /// Retorna o nome do usuário
        /// </summary>
        /// <param name="USU"></param>
        /// <returns>Nome</returns>
        public string BuscaUSUNome(object USU)
        {
            if ((USU == null) || !(USU is int))
                return "!";
            else
            {
                USUariosRow rowUSU = USUarios.FindByUSU((int)USU);
                return (rowUSU != null) ? rowUSU.USUNome : "-";
            }
        }

    }
}
