﻿namespace Framework.datasets {


    partial class dASSunto
    {
        private static dASSunto _dASSuntoSt;

        /// <summary>
        /// dataset estático:dASSunto
        /// </summary>
        public static dASSunto dASSuntoSt
        {
            get
            {
                if (_dASSuntoSt == null)
                {
                    _dASSuntoSt = new dASSunto();
                    _dASSuntoSt.ASSuntoTableAdapter.Fill(_dASSuntoSt.ASSunto);
                }
                return _dASSuntoSt;
            }
        }

        private dASSuntoTableAdapters.ASSuntoTableAdapter aSSuntoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ASSunto
        /// </summary>
        public dASSuntoTableAdapters.ASSuntoTableAdapter ASSuntoTableAdapter
        {
            get
            {
                if (aSSuntoTableAdapter == null)
                {
                    aSSuntoTableAdapter = new dASSuntoTableAdapters.ASSuntoTableAdapter();
                    aSSuntoTableAdapter.TrocarStringDeConexao();
                };
                return aSSuntoTableAdapter;
            }
        }
    }
}
