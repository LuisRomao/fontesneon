﻿using System;
using System.Collections.Generic;
using FrameworkProc;
using VirEnumeracoesNeon;

namespace Framework.datasets {


    partial class dPLAnocontas
    {
        private System.Data.DataView DVConTasLogicas_CON_TIPO;

        private Dictionary<CTLTipo, string> CTLsPadrao;

        /// <summary>
        /// Localiza a CTL padrão do condomínio
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="Tipo">Tipo padrão</param>
        /// <returns>CTL</returns>
        public int CTLPadrao(int CON,CTLTipo Tipo)
        {
            if (CTLsPadrao == null)
            {
                CTLsPadrao = new Dictionary<CTLTipo, string>();
                CTLsPadrao.Add(CTLTipo.Caixa, "CAIXA");
                CTLsPadrao.Add(CTLTipo.CreditosAnteriores, "Créditos não identificados/Antecipados");
                CTLsPadrao.Add(CTLTipo.Fundo, "Fundo de Reserva");                
                ConTasLogicasTableAdapter.Fill(ConTasLogicas);
                DVConTasLogicas_CON_TIPO = new System.Data.DataView(ConTasLogicas);
                DVConTasLogicas_CON_TIPO.Sort = "CTL_CON,CTLTipo";                
            }
            if ((Tipo != CTLTipo.Caixa) && (Tipo != CTLTipo.CreditosAnteriores) && (Tipo != CTLTipo.Fundo))
                throw new Exception(string.Format("CTL {0} não é padrão",Tipo));
            ConTasLogicasRow rowCTL;
            
            int i = DVConTasLogicas_CON_TIPO.Find(new object[] {CON,(int)Tipo});
            
            if (i == -1)
            {
                rowCTL = ConTasLogicas.NewConTasLogicasRow();
                rowCTL.CTL_CON = CON;
                rowCTL.CTLAtiva = true;
                rowCTL.CTLDATAI = DateTime.Now;
                rowCTL.CTLTipo = (int)Tipo;
                rowCTL.CTLTitulo = CTLsPadrao[Tipo];
                ConTasLogicas.AddConTasLogicasRow(rowCTL);
                ConTasLogicasTableAdapter.Update(rowCTL);
                rowCTL.AcceptChanges();
            }
            else
            {
                rowCTL = (ConTasLogicasRow)DVConTasLogicas_CON_TIPO[i].Row;
                if (!rowCTL.CTLAtiva)
                {
                    rowCTL.CTLAtiva = true;
                    ConTasLogicasTableAdapter.Update(rowCTL);
                    rowCTL.AcceptChanges();
                }
            };
            return rowCTL.CTL;
        }

        #region Static
        private static dPLAnocontas _dPLAnocontasSt;
        private static dPLAnocontas _dPLAnocontasStF;
        private static dPLAnocontas _dPLAnocontasSt25;
        private static dPLAnocontas _dPLAnocontasSt25F;
        private static dPLAnocontas _dPLAnocontasStReceitas;
        private static dPLAnocontas _dPLAnocontasSt56;
        private static dPLAnocontas _dPLAnocontasStEVE;

        private dPLAnocontasTableAdapters.SubPLanoTableAdapter subPLanoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SubPLano
        /// </summary>
        public dPLAnocontasTableAdapters.SubPLanoTableAdapter SubPLanoTableAdapter
        {
            get
            {
                if (subPLanoTableAdapter == null)
                {
                    subPLanoTableAdapter = new dPLAnocontasTableAdapters.SubPLanoTableAdapter();
                    subPLanoTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return subPLanoTableAdapter;
            }
        }

        /// <summary>
        /// dataset estático:dPLAnocontas
        /// </summary>
        public static dPLAnocontas dPLAnocontasSt
        {
            get
            {
                if (_dPLAnocontasSt == null)
                {                    
                    _dPLAnocontasSt = new dPLAnocontas();
                    _dPLAnocontasSt.PLAnocontasTableAdapter.Fill(_dPLAnocontasSt.PLAnocontas);
                    _dPLAnocontasSt.SubPLanoTableAdapter.Fill(_dPLAnocontasSt.SubPLano);
                }
                return _dPLAnocontasSt;
            }
        }

        /// <summary>
        /// dataset estático:dPLAnocontas (FILIAL)
        /// </summary>
        public static dPLAnocontas dPLAnocontasStF
        {
            get
            {
                if (_dPLAnocontasStF == null)
                {
                    _dPLAnocontasStF = new dPLAnocontas(FrameworkProc.EMPTipo.Filial);
                    _dPLAnocontasStF.PLAnocontasTableAdapter.Fill(_dPLAnocontasStF.PLAnocontas);
                    _dPLAnocontasStF.SubPLanoTableAdapter.Fill(_dPLAnocontasStF.SubPLano);
                }
                return _dPLAnocontasStF;
            }
        }

        /// <summary>
        /// dataset estático:dPLAnocontas
        /// </summary>
        public static dPLAnocontas dPLAnocontasStX(EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTipo.Local ? dPLAnocontasSt : dPLAnocontasStF;
        }

        /// <summary>
        /// dataset estático:dPLAnocontas com filtro para 2 e 5
        /// </summary>
        public static dPLAnocontas dPLAnocontasSt25
        {
            get
            {
                if (_dPLAnocontasSt25 == null)
                {
                    _dPLAnocontasSt25 = new dPLAnocontas();
                    _dPLAnocontasSt25.PLAnocontasTableAdapter.Fill25(_dPLAnocontasSt25.PLAnocontas);
                    _dPLAnocontasSt25.SubPLanoTableAdapter.Fill25(_dPLAnocontasSt25.SubPLano);
                }
                return _dPLAnocontasSt25;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static dPLAnocontas dPLAnocontasSt25F
        {
            get
            {
                if (_dPLAnocontasSt25F == null)
                {
                    _dPLAnocontasSt25F = new dPLAnocontas(FrameworkProc.EMPTipo.Filial);
                    _dPLAnocontasSt25F.PLAnocontasTableAdapter.Fill25(_dPLAnocontasSt25F.PLAnocontas);
                    _dPLAnocontasSt25F.SubPLanoTableAdapter.Fill25(_dPLAnocontasSt25F.SubPLano);
                }
                return _dPLAnocontasSt25F;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public static dPLAnocontas dPLAnocontasSt25X(EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTipo.Local ? dPLAnocontasSt25 : dPLAnocontasSt25F;
        }

        /// <summary>
        /// dataset estático:dPLAnocontas com filtro para 5 e 6
        /// </summary>
        public static dPLAnocontas dPLAnocontasSt56
        {
            get
            {
                if (_dPLAnocontasSt56 == null)
                {
                    _dPLAnocontasSt56 = new dPLAnocontas();
                    _dPLAnocontasSt56.PLAnocontasTableAdapter.Fill56(_dPLAnocontasSt56.PLAnocontas);
                    _dPLAnocontasSt56.SubPLanoTableAdapter.Fill56(_dPLAnocontasSt56.SubPLano);
                }
                return _dPLAnocontasSt56;
            }
        }

        private dPLAnocontasTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dPLAnocontasTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dPLAnocontasTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return conTasLogicasTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static dPLAnocontas dPLAnocontasStEVE
        {
            get 
            {
                if (_dPLAnocontasStEVE == null)
                {
                    _dPLAnocontasStEVE = new dPLAnocontas();
                    _dPLAnocontasStEVE.PLAnocontasTableAdapter.FillByEVE(_dPLAnocontasStEVE.PLAnocontas);
                    _dPLAnocontasStEVE.SubPLanoTableAdapter.FillByEVE(_dPLAnocontasStEVE.SubPLano);
                }
                return _dPLAnocontasStEVE;
            }
        }

        /// <summary>
        /// dataset estático:dPLAnocontas com filtro para receitas 1XXXXX
        /// </summary>
        public static dPLAnocontas dPLAnocontasStReceitas
        {
            get
            {
                if (_dPLAnocontasStReceitas == null)
                {
                    _dPLAnocontasStReceitas = new dPLAnocontas();
                    _dPLAnocontasStReceitas.PLAnocontasTableAdapter.FillReceitas(_dPLAnocontasStReceitas.PLAnocontas);
                }
                return _dPLAnocontasStReceitas;
            }
        }

        /// <summary>
        /// Planos Padrao
        /// </summary>
        /// <param name="PLA"></param>
        /// <returns></returns>
        public static PLAPadrao PlaPadrao(string PLA)
        {
            try
            {
                return (PLAPadrao)(int.Parse(PLA));
            }
            catch
            {
                return PLAPadrao.NaoDefinido;
            }

            /*
            if (PLA == "111000")
                return PLAPadrao.acordo;
            else if (PLA == "119000")
                return PLAPadrao.honorarios;
            else
                return PLAPadrao.NaoDefinido;*/
        }

        /*
        /// <summary>
        /// Planos Padrao
        /// </summary>
        /// <param name="PLA"></param>
        /// <returns></returns>
        public static string PLAPadraoCodigo(PLAPadrao PLA)
        {
            return string.Format("{0:000000}", (int)PLA);            
        }*/

        #endregion

        private EMPTProc _EMPTipo;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTipo
        {
            get
            {
                if (_EMPTipo == null)
                    _EMPTipo = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTipo;
            }
        }

        /// <summary>
        /// Construitor com EMPTipo
        /// </summary>
        /// <param name="_EMPTipo"></param>
        public dPLAnocontas(EMPTipo _EMPTipo)
            : this()
        {
            this._EMPTipo = new EMPTProc(_EMPTipo);
        }

        /// <summary>
        /// Retorna a descrição de uma conta
        /// </summary>
        /// <param name="PLA">PLA</param>
        /// <returns>PLADescrição</returns>
        /// <remarks>retona "" se a conta nao existir</remarks>
        public string GetDescricao(string PLA)
        {
            PLAnocontasRow LinhaProcurada = PLAnocontas.FindByPLA(PLA);
            if (LinhaProcurada == null)
                return "";
            else
                return LinhaProcurada.PLADescricao;
        }

        private dPLAnocontasTableAdapters.PLAnocontasTableAdapter pLAnocontasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PLAnocontas
        /// </summary>
        public dPLAnocontasTableAdapters.PLAnocontasTableAdapter PLAnocontasTableAdapter
        {
            get
            {
                if (pLAnocontasTableAdapter == null)
                {
                    pLAnocontasTableAdapter = new dPLAnocontasTableAdapters.PLAnocontasTableAdapter();
                    pLAnocontasTableAdapter.TrocarStringDeConexao(EMPTipo.TBanco);
                };
                return pLAnocontasTableAdapter;
            }
        }
    }

    /*
    /// <summary>
    /// Planos Padrao
    /// </summary>
    public enum PLAPadrao
    {
        /// <summary>
        /// nao definido
        /// </summary>
        NaoDefinido = 0,
        /// <summary>
        /// Acordo
        /// </summary>
        acordo       = 111000,
        /// <summary>
        /// Condominio
        /// </summary>
        Condominio   = 101000,
        /// <summary>
        /// Fundo de reserva
        /// </summary>
        FundoReserva = 110000,
        /// <summary>
        /// honorarios
        /// </summary>
        honorarios = 119000,
        /// <summary>
        /// Multa por atraso
        /// </summary>
        MultasAtraso = 120001,
        /// <summary>
        /// seguro conteudo credito
        /// </summary>
        segurocosnteudoC = 910001,
        /// <summary>
        /// seguro conteudo débito
        /// </summary>
        segurocosnteudoD = 920001,
        /// <summary>
        /// Crédito Antecipado
        /// </summary>
        PagamentoExtraC = 910002,
        /// <summary>
        /// Desconto Crédito Antecipado
        /// </summary>
        PagamentoExtraD = 920002,
        /// <summary>
        /// Referência a outro boleto
        /// </summary>
        Saldinho = 800001,
        /// <summary>
        /// Credito na identificado
        /// </summary>
        CreditoNaoIdentificado = 198000,
    }
    */
}
