﻿using Framework.objetosNeon;
using System;


namespace Framework.datasets {
    
    /// <summary>
    /// dataset dAGendaGeral
    /// </summary>
    public partial class dAGendaGeral {

        private static dAGendaGeral _dAGendaGeralSt;

        /// <summary>
        /// dataset estático:dAGendaGeral
        /// </summary>
        public static dAGendaGeral dAGendaGeralSt
        {
            get
            {
                if (_dAGendaGeralSt == null)
                    _dAGendaGeralSt = new dAGendaGeral();
                return _dAGendaGeralSt;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static dAGendaGeral dAGendaGeralStFill
        {
            get 
            {
                dAGendaGeralSt.AGendaGeralTableAdapter.Fill(dAGendaGeralSt.AGendaGeral);
                return dAGendaGeralSt;
            }
        }

        private dAGendaGeralTableAdapters.AGendaGeralTableAdapter aGendaGeralTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: AGendaGeral
        /// </summary>
        public dAGendaGeralTableAdapters.AGendaGeralTableAdapter AGendaGeralTableAdapter
        {
            get
            {
                if (aGendaGeralTableAdapter == null)
                {
                    aGendaGeralTableAdapter = new dAGendaGeralTableAdapters.AGendaGeralTableAdapter();
                    aGendaGeralTableAdapter.TrocarStringDeConexao();
                };
                return aGendaGeralTableAdapter;
            }
        }

        #region Funções Get/Set

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="Texto"></param>
        public void GravaObs(AGGTipo Tipo, string Texto)
        {
            AGendaGeralRow AGGrow = GetAGGrow(Tipo);
            GravaHistorico(AGGrow, string.Format("Anotação:\r\n{0}\r\n", Texto));
            AGendaGeralTableAdapter.Update(AGGrow);
            AGGrow.AcceptChanges();
        }
        
        
        /// <summary>
        /// Grava texto no histórico - não grava no banco so no campo
        /// </summary>
        /// <param name="AGGrow"></param>
        /// <param name="Texto"></param>       
        private static void GravaHistorico(AGendaGeralRow AGGrow,string Texto)
        {
            AGGrow.AGGHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} - {1}\r\n{2}\r\n", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome, Texto);
        }
        
        /// <summary>
        /// Get competencia
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public Competencia GetAGBCompetencia(AGGTipo Tipo)
        {
            return new Competencia(GetAGGrow(Tipo).AGGCompetencia);
        }

        /// <summary>
        /// Get status
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public AGGStatus GetAGBStatus(AGGTipo Tipo)
        {
            return (AGGStatus)GetAGGrow(Tipo).AGGSTatus;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public string GetAGBHistorico(AGGTipo Tipo)
        {
            return GetAGGrow(Tipo).AGGHistorico;
        }

        /// <summary>
        /// Get row
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        private AGendaGeralRow GetAGGrow(AGGTipo Tipo)
        {
            AGendaGeralRow AGGrow = null;
            if (AGendaGeralTableAdapter.FillBy(AGendaGeral,(int)Tipo) == 1)
                AGGrow = AGendaGeral[0];
            if (AGGrow == null)
            {
                Competencia comp = (new Competencia().Add(-1));
                AGGrow = AGendaGeral.NewAGendaGeralRow();
                AGGrow.AGG = (int)Tipo;
                AGGrow.AGGCompetencia = comp.CompetenciaBind;
                AGGrow.AGGSTatus = (int)AGGStatus.NaoIniciado;
                AGGrow.AGGHistorico = string.Format("{0:dd/MM/yyyy HH:mm:ss} - Cadastro inicial Competência: {1}\r\n", DateTime.Now, comp);
                AGendaGeral.AddAGendaGeralRow(AGGrow);
                AGendaGeralTableAdapter.Update(AGGrow);
                AGGrow.AcceptChanges();
            }
            return AGGrow;
        }

        /// <summary>
        /// Set Competencia
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="comp"></param>
        public void SetAGBCompetencia(AGGTipo Tipo, Competencia comp)
        {
            AGendaGeralRow AGGrow = GetAGGrow(Tipo);
            if (AGGrow.AGGCompetencia != comp.CompetenciaBind)
            {              
                AGGrow.AGGCompetencia = comp.CompetenciaBind;
                GravaHistorico(AGGrow, string.Format("Alteração de competência para: {0}", comp));
                AGendaGeralTableAdapter.Update(AGGrow);
                AGGrow.AcceptChanges();
            }
        }

        /// <summary>
        /// set status
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="status"></param>
        public void SetAGBStatus(AGGTipo Tipo, AGGStatus status)
        {
            AGendaGeralRow AGGrow = GetAGGrow(Tipo);
            if (AGGrow.AGGSTatus != (int)status)
            {
                AGGrow.AGGSTatus = (int)status;
                GravaHistorico(AGGrow, string.Format("Alteração de status para: {0}", virEnumAGGStatus.virEnumAGGStatusSt.Descritivo(status)));
                AGendaGeralTableAdapter.Update(AGGrow);
                AGGrow.AcceptChanges();
            }
        } 
        #endregion
    }
}