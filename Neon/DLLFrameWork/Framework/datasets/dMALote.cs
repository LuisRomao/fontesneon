﻿namespace Framework.datasets {


    partial class dMALote
    {
        private static dMALote _dMALoteSt;

        /// <summary>
        /// dataset estático:dMALote
        /// </summary>
        public static dMALote dMALoteSt
        {
            get
            {
                if (_dMALoteSt == null)
                {
                    _dMALoteSt = new dMALote();
                    foreach (object o in System.Enum.GetValues(typeof(objetosNeon.malote.TiposMalote)))
                        _dMALoteSt.TiposMalote.AddTiposMaloteRow((int)o, objetosNeon.malote.VirEnumTiposMalote.Descritivo(o));                                        
                }
                return _dMALoteSt;
            }
        }


        private dMALoteTableAdapters.MALoteTableAdapter mALoteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: MALote
        /// </summary>
        public dMALoteTableAdapters.MALoteTableAdapter MALoteTableAdapter
        {
            get
            {
                if (mALoteTableAdapter == null)
                {
                    mALoteTableAdapter = new dMALoteTableAdapters.MALoteTableAdapter();
                    mALoteTableAdapter.TrocarStringDeConexao();
                };
                return mALoteTableAdapter;
            }
        }
    }
}
