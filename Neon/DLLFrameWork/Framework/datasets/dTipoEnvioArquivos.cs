﻿namespace Framework.datasets {
    
    
    partial class dTipoEnvioArquivos
    {
        private static dTipoEnvioArquivos _dTipoEnvioArquivosSt;

        /// <summary>
        /// dataset estático:dTipoEnvioArquivos
        /// </summary>
        public static dTipoEnvioArquivos dTipoEnvioArquivosSt
        {
            get
            {
                if (_dTipoEnvioArquivosSt == null)
                {
                    _dTipoEnvioArquivosSt = new dTipoEnvioArquivos();
                    _dTipoEnvioArquivosSt.TipoEnvioArquivosTableAdapter.Fill(_dTipoEnvioArquivosSt.TipoEnvioArquivos);
                }
                return _dTipoEnvioArquivosSt;
            }
        }

        private dTipoEnvioArquivosTableAdapters.TipoEnvioArquivosTableAdapter tipoEnvioArquivosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TipoEnvioArquivos
        /// </summary>
        public dTipoEnvioArquivosTableAdapters.TipoEnvioArquivosTableAdapter TipoEnvioArquivosTableAdapter
        {
            get
            {
                if (tipoEnvioArquivosTableAdapter == null)
                {
                    tipoEnvioArquivosTableAdapter = new dTipoEnvioArquivosTableAdapters.TipoEnvioArquivosTableAdapter();
                    tipoEnvioArquivosTableAdapter.TrocarStringDeConexao();
                };
                return tipoEnvioArquivosTableAdapter;
            }
        }
    }
}
