using System;

namespace Framework.datasets
{
    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum ATVClasse
    {
        /// <summary>
        /// 
        /// </summary>
        Alerta = 0,
        /// <summary>
        /// 
        /// </summary>
        Aviso = 1,
        /// <summary>
        /// 
        /// </summary>
        Informacao = 2
    }


    /// <summary>
    /// virEnum para campo ATVClasse
    /// </summary>
    public class virEnumATVClasse : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumATVClasse(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumATVClasse _virEnumATVClasseSt;

        /// <summary>
        /// VirEnum estático para campo ATVClasse
        /// </summary>
        public static virEnumATVClasse virEnumATVClasseSt
        {
            get
            {
                if (_virEnumATVClasseSt == null)
                {
                    _virEnumATVClasseSt = new virEnumATVClasse(typeof(ATVClasse));
                    _virEnumATVClasseSt.GravaNomes(ATVClasse.Alerta, "Alerta");
                    _virEnumATVClasseSt.GravaCor(ATVClasse.Alerta, System.Drawing.Color.Red);                
                    _virEnumATVClasseSt.GravaNomes(ATVClasse.Aviso, "Aviso");
                    _virEnumATVClasseSt.GravaCor(ATVClasse.Aviso, System.Drawing.Color.Yellow);              
                    _virEnumATVClasseSt.GravaNomes(ATVClasse.Informacao, "Informativo");
                    _virEnumATVClasseSt.GravaCor(ATVClasse.Informacao, System.Drawing.Color.LightGray);
                }
                return _virEnumATVClasseSt;
            }
        }
    }
}
