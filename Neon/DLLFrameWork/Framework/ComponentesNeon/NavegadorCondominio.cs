/*
LH - 30/01/2014        14.2.4.7 - Ajuste dos condominios ocultos
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Framework.ComponentesNeon
{
    /// <summary>
    /// Componente base parar navegadores baseados nos condomínios
    /// </summary>
    public partial class NavegadorCondominio : CompontesBasicos.ComponeteGradeNavegadorPaiCampos
    {
        /// <summary>
        /// Mostrar condominos da filial
        /// </summary>
        [Category("Virtual Software")]
        [Description("Nivel de condominios ocultos")]
        public int NivelCONOculto
        {
            get
            {
                return lookupCondominio_F1.NivelCONOculto;
            }
            set
            {
                lookupCondominio_F1.NivelCONOculto = value;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public NavegadorCondominio()
        {
            InitializeComponent();          
        }

        /// <summary>
        /// Condomínio selecionado
        /// </summary>
        public int CON {
            get 
            {
                return lookupCondominio_F1.CON_sel;
            }
            set
            {
                lookupCondominio_F1.CON_sel = value;
            }
        }

        /// <summary>
        /// Evento na alteração do condomínio
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento na alteração do condomínio")]
        public event EventHandler CondominioAlterado;

        private void OnCondominioAlterado(EventArgs e)
        {
            if (CondominioAlterado != null)
                CondominioAlterado(this, e);
        }


        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            OnCondominioAlterado(new EventArgs());
        }

        /// <summary>
        /// Inclusão de nova linha
        /// </summary>
        /// <param name="sender">Chamador</param>
        /// <param name="e"></param>
        /// <remarks>Cancela a chamada se nenhum condomínio estiver selecionado</remarks>
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(CON != -1)
                base.BtnIncluir_F_ItemClick(sender, e);
        }
    }
}

