using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Framework.ComponentesNeon
{
    /// <summary>
    /// Componente Campos para registros que dependem de condomínios
    /// </summary>
    public partial class CamposCondominio : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public CamposCondominio()
        {
            InitializeComponent();
            cargaFinal += new EventHandler(CamposCondominio_cargaFinal);
        }

        /// <summary>
        /// Grade chamadora
        /// </summary>        
        private NavegadorCondominio GradeChamadoraNeon {
            get {
                return (GradeChamadora == null ? null :(NavegadorCondominio)GradeChamadora);
            }
        }

        void CamposCondominio_cargaFinal(object sender, EventArgs e)
        {            
            if ((LinhaMae_F.RowState == DataRowState.Detached) && (tabela != null))
            {
                string nomeCampo = tabela.Columns[0].ColumnName + "_CON";
                if(LinhaMae_F[nomeCampo] == DBNull.Value)
                    LinhaMae_F[nomeCampo] = GradeChamadoraNeon.CON;
            }
        }
    }
}

