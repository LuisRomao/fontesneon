﻿namespace Framework.Importador {


    partial class DSImportador
    {
        private DSImportadorTableAdapters.BLOAPTTableAdapter bLOAPTTableAdapter;
        /// <summary>
        /// Table adapter único da instancia
        /// </summary>
        public DSImportadorTableAdapters.BLOAPTTableAdapter BLOAPTTableAdapter {
            get {
                if (bLOAPTTableAdapter == null) {
                    bLOAPTTableAdapter = new Framework.Importador.DSImportadorTableAdapters.BLOAPTTableAdapter();
                    bLOAPTTableAdapter.TrocarStringDeConexao();
                };
                return bLOAPTTableAdapter;
            }
        }
    }
}
