using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;

namespace Framework.Importador
{
    

    /// <summary>
    /// Objeto para importa��o de dados em forma de planilha
    /// </summary>
    public partial class cImportador : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Dataset de busca (um para a instancia)
        /// </summary>
        private DSImportador _dsImpartador;

        private DSImportador dsImpartador {
            get {
                if (_dsImpartador == null)
                    _dsImpartador = new DSImportador();
                return _dsImpartador;
            }
        }

        private ConjuntoColunasImp _conjuntoColunasImp;

        private ConjuntoColunasImp conjuntoColunasImp {
            get {
                if (_conjuntoColunasImp == null)
                    _conjuntoColunasImp = new ConjuntoColunasImp();
                return _conjuntoColunasImp;
            }
        }

        /// <summary>
        /// DataTable que vai conter os dados lidos
        /// </summary>
        public DataTable _DataTable;

        private int ColunaCODCON = -1;
        private int ColunaBLOCO = 0;
        private int ColunaAPARTAMENTO = 1;
        private int ColunaPI = -1;
        

        /// <summary>
        /// Construitor padr�o
        /// </summary>
        public cImportador()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor com setagem de condom�nio
        /// </summary>
        /// <param name="CON"></param>
        public cImportador(int CON)
        {
            InitializeComponent();
            SetaCON(CON);
        }

        private void CriaColuna(DataColumn NovaColuna,bool SomenteLeitura) {
            DevExpress.XtraGrid.Columns.GridColumn DvnovaColuna = gridView1.Columns.Add();
            DvnovaColuna.FieldName = NovaColuna.ColumnName;
            DvnovaColuna.Visible = true;
            DvnovaColuna.Width = 160;
            DvnovaColuna.OptionsColumn.AllowEdit = !SomenteLeitura;
            if (SomenteLeitura)
                DvnovaColuna.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public bool SetaCON(int CON) {
            if (dsImpartador.BLOAPTTableAdapter.Fill(dsImpartador.BLOAPT, CON) == 0)
                return false;
            else
            {
                CONPadrao = CON;
                return true;
            }
        }

        /// <summary>
        /// Carega os dados da �rea de transfer�ncia
        /// </summary>
        public bool Carrega() {
            Char[] QuebraDeLinha = new Char[2] { '\r', '\n' };
            Char[] QuebraDeColuna = new Char[1] { '\t' };
            gridView1.Columns.Clear();
            

            if (Clipboard.GetData(System.Windows.Forms.DataFormats.Text) == null)
                return false;
            string Clip = Clipboard.GetData(System.Windows.Forms.DataFormats.Text).ToString();
            String[] Linhas = Clip.Split(QuebraDeLinha,StringSplitOptions.RemoveEmptyEntries);            
            if (Linhas.Length == 0)
                return false;
            _DataTable = new DataTable();
            int colunas;
            string[] Campos = Linhas[0].Split(QuebraDeColuna);

            CriaColuna(_DataTable.Columns.Add("Coluna 1"),false);            
            CriaColuna(_DataTable.Columns.Add("Coluna 2"),false);
            //CriaColuna
            DataRow rowTitulo = _DataTable.Rows.Add(new string[2] { "BLOCO", "APARTAMENTO" });
            colunas = 2;
            
            

            
            foreach (string Linha in Linhas)
            {                
                string[] Campos1 = Linha.Split(QuebraDeColuna);
                for (int i = colunas; i < Campos1.Length; i++, colunas++)
                    CriaColuna(_DataTable.Columns.Add("Coluna " + ((int)(i + 1)).ToString()),false);
                _DataTable.Rows.Add(Campos1);                
            };
            CriaColuna(_DataTable.Columns.Add("Status"),true);
            _DataTable.Columns.Add("OK", typeof(bool));
            _DataTable.Columns.Add("APT", typeof(int));
            rowTitulo["Status"] = " - ";
            rowTitulo["OK"] = false;
            foreach (ItenColunaImp Iten in conjuntoColunasImp)
                _DataTable.Columns.Add(Iten.Nome,Iten.Tipo);
            gridControl1.DataSource = _DataTable;
            ValidarDados();
            return true;
        }

        private void ValidarDados()
        {
            bool primeira = true;
            foreach (DataRow DR in _DataTable.Rows)
            {
                if (!primeira)
                    ValidarRow(DR);
                primeira = false;
            }
        }

        /// <summary>
        /// C�digo do condom�nio
        /// </summary>
        /// <remarks>Quando este valor � informado a coluna CODCON � omitida</remarks>
        public int CONPadrao = 0;

        //private string BLOCodigo;
        //private int BLO;
        //private string APTNumero;
        //private int APT;

        private StatusLinha BuscaBLOAPT(string BLOCodigo, string APTNumero) {
            StatusLinha retorno = new StatusLinha();
            foreach (DSImportador.BLOAPTRow rowBLOAPT in dsImpartador.BLOAPT) {
                if(BLOCodigo == rowBLOAPT.BLOCodigo){
                    retorno.BLOencontrado = true;
                    retorno.BLO = rowBLOAPT.BLO;
                };
                if (APTNumero == rowBLOAPT.APTNumero)                
                    retorno.APTNumeroexiste = true;

                if ((BLOCodigo == rowBLOAPT.BLOCodigo) && (APTNumero == rowBLOAPT.APTNumero))
                {
                    retorno.APTencontrado = true;
                    retorno.APT = rowBLOAPT.APT;
                    break;
                };
            };
            return retorno;
        }

   

        private void ValidarRow(DataRow DR)
        {
            if (DR["Status"].ToString() == " - ")
                return;
            DR.RowError = "";
            DR.ClearErrors();
            DR["Status"] = "";
            DR["OK"] = false;
            string BLOCodigo = "";
            string APTNumero = "";
        
            //Validar colunas extra
            foreach (ItenColunaImp Iten in conjuntoColunasImp)
            {
                if (Iten.Coluna != -1)
                {
                    if (Iten.Tipo == typeof(decimal))
                    {
                        decimal retorno;
                        if (!decimal.TryParse(DR[Iten.Coluna].ToString(), out retorno))
                        {
                            DR.SetColumnError(Iten.Coluna, "Valor inv�lido");
                            DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                            DR.RowError = "Valores inv�lidos";
                        }
                        else {
                            if ((Iten.Minimo != null) && (retorno < (decimal)Iten.Minimo)) {
                                DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�nimo = '"+Iten.Minimo.ToString()+"')");
                                DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                DR.RowError = "Valores inv�lidos";
                            }
                            if ((Iten.Maximo != null) && (retorno > (decimal)Iten.Maximo))
                            {
                                DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�ximo = '" + Iten.Maximo.ToString() + "')");
                                DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                DR.RowError = "Valores inv�lidos";
                            }
                        };
                        if (DR.RowError == "")
                            DR[Iten.Nome] = retorno;
                        else
                            DR[Iten.Nome] = DBNull.Value;

                    }
                    else {
                        if (Iten.Tipo == typeof(double))
                        {
                            double retorno;
                            if (!double.TryParse(DR[Iten.Coluna].ToString(), out retorno))
                            {
                                DR.SetColumnError(Iten.Coluna, "Valor inv�lido");
                                DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                DR.RowError = "Valores inv�lidos";
                            }
                            else
                            {
                                if ((Iten.Minimo != null) && (retorno < (double)Iten.Minimo))
                                {
                                    DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�nimo = '" + Iten.Minimo.ToString() + "')");
                                    DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                    DR.RowError = "Valores inv�lidos";
                                }
                                if ((Iten.Maximo != null) && (retorno > (double)Iten.Maximo))
                                {
                                    DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�ximo = '" + Iten.Maximo.ToString() + "')");
                                    DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                    DR.RowError = "Valores inv�lidos";
                                }
                            };
                            if (DR.RowError == "")
                                DR[Iten.Nome] = retorno;
                            else
                                DR[Iten.Nome] = DBNull.Value;

                        }
                        else
                        {
                            if (Iten.Tipo == typeof(string))
                            {
                                string retorno =DR[Iten.Coluna].ToString();
                                if ((Iten.Minimo != null) && (retorno.Length < (int)Iten.Minimo))
                                {
                                    DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�nimo = '" + Iten.Minimo.ToString() + "' caracteres)");
                                    DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                    DR.RowError = "Valores inv�lidos";
                                }
                                if ((Iten.Maximo != null) && (retorno.Length > (int)Iten.Maximo))
                                {
                                    DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�ximo = '" + Iten.Maximo.ToString() + "') caracteres");
                                    DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                    DR.RowError = "Valores inv�lidos";
                                };
                                if (DR.RowError == "")
                                    DR[Iten.Nome] = retorno;
                                else
                                    DR[Iten.Nome] = DBNull.Value;
                            }
                            else
                            {
                                if (Iten.Tipo == typeof(int))
                                {
                                    int retorno;
                                    if (!int.TryParse(DR[Iten.Coluna].ToString(), out retorno))
                                    {
                                        DR.SetColumnError(Iten.Coluna, "Valor inv�lido");
                                        DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                        DR.RowError = "Valores inv�lidos";
                                    }
                                    else
                                    {
                                        if ((Iten.Minimo != null) && (retorno < (int)Iten.Minimo))
                                        {
                                            DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�nimo = '" + Iten.Minimo.ToString() + "')");
                                            DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                            DR.RowError = "Valores inv�lidos";
                                        }
                                        if ((Iten.Maximo != null) && (retorno > (int)Iten.Maximo))
                                        {
                                            DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�ximo = '" + Iten.Maximo.ToString() + "')");
                                            DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                                            DR.RowError = "Valores inv�lidos";
                                        }
                                    };
                                    if (DR.RowError == "")
                                        DR[Iten.Nome] = retorno;
                                    else
                                        DR[Iten.Nome] = DBNull.Value;
                                }
                                else
                                {
                                    MessageBox.Show("Tipo '" + Iten.Tipo.ToString() + "' n�o implementado");
                                }
                            }
                        }
                    }
                }
            };

            //if ((ColunaBLOCO == -1) || (ColunaAPARTAMENTO == -1))
            if (ColunaAPARTAMENTO == -1)
                return;
            if (CONPadrao == 0)
            {
                if (ColunaCODCON == -1)
                    return;
                else 
                {
                    
                }
            }
            else
            {
                //if (DR[ColunaBLOCO].ToString() == "")
                //{
                //    DR["Status"] = "Bloco n�o definido";
                //    DR.SetColumnError(ColunaBLOCO, "Bloco n�o definido");
                //    DR.RowError = "Apartamento n�o encontrado";
                //}
                //else                 
                if (ColunaBLOCO == -1)
                    BLOCodigo = "SB";
                else
                {
                    BLOCodigo = DR[ColunaBLOCO].ToString().ToUpper();
                };

                if (DR[ColunaAPARTAMENTO].ToString() == "")
                {
                    DR["Status"] += ("|" + "Apartamento n�o definido");
                    DR.SetColumnError(ColunaAPARTAMENTO, "Apartamento n�o definido");
                    DR.RowError = "Apartamento n�o encontrado";
                }
                else                
                    APTNumero = DR[ColunaAPARTAMENTO].ToString();                
                if(DR.RowError == ""){
                    StatusLinha statusLinha = BuscaBLOAPT(BLOCodigo,APTNumero);

                    //Tentativa de corrigir os dados completando com zeros
                    if (!statusLinha.APTencontrado)
                    {
                        string NumeroAlternativo = DR[ColunaAPARTAMENTO].ToString().PadLeft(4,'0');

                        string BlocoAlternativo = BLOCodigo;
                        if (BlocoAlternativo == "")
                            BlocoAlternativo = "SB";
                        while (BlocoAlternativo.Length < 2)
                            BlocoAlternativo = "0" + BlocoAlternativo;
                        StatusLinha statusAlternativo = BuscaBLOAPT(BLOCodigo, NumeroAlternativo);
                        if (statusAlternativo.APTencontrado)
                        {
                            statusLinha = statusAlternativo;
                            DR[ColunaAPARTAMENTO] = NumeroAlternativo;
                        }
                        else
                        {
                            statusAlternativo = BuscaBLOAPT(BlocoAlternativo, APTNumero);
                            if (statusAlternativo.APTencontrado)
                            {
                                statusLinha = statusAlternativo;
                                if(ColunaBLOCO != -1)
                                    DR[ColunaBLOCO] = BlocoAlternativo;
                            }
                            else
                            {
                                statusAlternativo = BuscaBLOAPT(BlocoAlternativo, NumeroAlternativo);
                                if (statusAlternativo.APTencontrado)
                                {
                                    statusLinha = statusAlternativo;
                                    DR[ColunaAPARTAMENTO] = NumeroAlternativo;
                                    if (ColunaBLOCO != -1)
                                       DR[ColunaBLOCO] = BlocoAlternativo;
                                }
                            }
                        }
                    }
                    if(statusLinha.APTencontrado){
                        DR["Status"] += "OK";
                        DR["OK"] = true;
                        DR["APT"] = statusLinha.APT;
                    }
                    else{
                        DR["Status"] += ("|" + "Apartamento n�o encontrado");
                        DR.RowError = "Apartamento n�o encontrado";
                        if ((!statusLinha.BLOencontrado) && (ColunaBLOCO >= 0))
                            DR.SetColumnError(ColunaBLOCO, "Bloco n�o encontrado");
                        if ((!statusLinha.APTNumeroexiste) && (ColunaAPARTAMENTO >= 0))
                            DR.SetColumnError(ColunaAPARTAMENTO, "Apartamento n�o encontrado");
                    }
                }
            }
            
            /*
            if (DR[ColunaProduto].ToString() == "Produto")
                DR["Status"] = "";
            else
            {
                
                string produto = DR[ColunaProduto].ToString().Trim().ToUpper();
                EDI.Componentes.Produtos.dProdutos.PRODUTOSNOVORow rowPRO = FormPrincipal.dProdutos.PRODUTOSNOVO.FindByPRO(produto);
                if (rowPRO == null)
                {
                    DR.RowError = "Erro no produto";
                    DR.SetColumnError(ColunaProduto, "C�digo inv�lido");
                    DR["Status"] = "Erro no produto";
                }
                else
                {
                    bool duplicado = false;
                    int loc = 0;
                    if (cPedidos != null)
                    {
                        loc = cPedidos.bgvProdutos.LocateByValue(0, colITP_PRO, produto);
                        duplicado = (loc >= 0);
                    };
                    if (duplicado)
                    {
                        {
                            DR.RowError = "Erro no produto";
                            DR.SetColumnError(ColunaProduto, "C�digo Duplicado: Linha " + (loc + 1).ToString());
                            DR["Status"] = "C�digo Duplicado: Linha " + (loc + 1).ToString();
                        }
                    }
                    else

                        if (VerificaDuplicacao(produto, DR))
                        {
                            DR.RowError = "Erro no produto";
                            DR.SetColumnError(ColunaProduto, "C�digo Duplicado");
                            DR["Status"] = "C�digo Duplicado";
                        }
                        else
                            if (ValidaQuantidade(DR, rowPRO.PROQUANTIDADE) && ValidaDesconto(DR, ColunaDesconto1) && ValidaDesconto(DR, ColunaDesconto2) && ValidaDesconto(DR, ColunaDesconto3) && ValidaDesconto(DR, ColunaDesconto4))
                                DR["Status"] = "OK";

                }
            }*/
        }

        private void gridView1_CustomRowCellEditForEditing(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if ((e.RowHandle == 0) && (e.Column.Caption != "Status"))            
                e.RepositoryItem = repositoryItemComboBox1;            
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {            
            if ((e.RowHandle == 0) && (e.Column.Caption != "Status"))
            {
                e.Appearance.BackColor = Color.Blue;
                e.Appearance.ForeColor = Color.White;
            };
        }

        private void AjustaColunas()
        {
            ColunaCODCON = ColunaPI = ColunaBLOCO = ColunaAPARTAMENTO = -1;
            foreach (ItenColunaImp Iten in conjuntoColunasImp) {
                Iten.Coluna = -1;
            };
            for (int i = 0; i < _DataTable.Columns.Count; i++)
                switch (_DataTable.Rows[0].ItemArray[i].ToString())
                {
                    case "CODCON": ColunaCODCON = i;
                        break;
                    case "BLOCO": ColunaBLOCO = i;
                        break;
                    case "APARTAMENTO": ColunaAPARTAMENTO = i;
                        break;
                    case "P/I": ColunaPI = i;
                        break;
                    default:
                        foreach (ItenColunaImp Iten in conjuntoColunasImp)
                        {
                            if(Iten.Titulo == _DataTable.Rows[0].ItemArray[i].ToString())
                                Iten.Coluna = i;
                        };
                        break;
                    
                };
            ValidarDados();

        }

        private void repositoryItemComboBox1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.ValidateEditor();
            Validate();
            AjustaColunas();
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            ValidarRow(((DataRowView)e.Row).Row);
        }

        /// <summary>
        /// Cria uma coluna de importa��o
        /// </summary>
        /// <param name="_Nome">Nome da coluna</param>
        /// <param name="_Titulo">T�tulo da coluna</param>
        /// <param name="_Tipo">Tipo de dado - usar typeOf()</param>        
        public void AddColunaImp(string _Nome, string _Titulo, Type _Tipo)
        {
            AddColunaImp(_Nome, _Titulo, _Tipo,null,null);
        }

        /// <summary>
        /// Cria uma coluna de importa��o
        /// </summary>
        /// <param name="_Nome">Nome da coluna</param>
        /// <param name="_Titulo">T�tulo da coluna</param>
        /// <param name="_Tipo">Tipo de dado - usar typeOf()</param>
        /// <param name="_Maximo">Valor m�ximo</param>
        /// <param name="_Minimo">Valor m�nimi</param>
        public void AddColunaImp(string _Nome, string _Titulo, Type _Tipo, object _Maximo, object _Minimo)
        {
            if (_Tipo == typeof(string)) {
                if ((_Maximo != null) && (_Maximo.GetType() != typeof(int)))
                {
                    _Maximo = null;
                };
                if ((_Minimo != null) && (_Minimo.GetType() != typeof(int)))
                {
                    _Minimo = null;
                };
            }
            else
            {
                if ((_Maximo != null) && (_Maximo.GetType() != _Tipo))
                {
                    _Maximo = null;
                };
                if ((_Minimo != null) && (_Minimo.GetType() != _Tipo))
                {
                    _Minimo = null;
                };
            }
            conjuntoColunasImp.Add(_Nome, _Titulo, _Tipo,_Maximo,_Minimo);
            DevExpress.XtraEditors.Controls.ComboBoxItem ItemCombo = new DevExpress.XtraEditors.Controls.ComboBoxItem(_Titulo);
            repositoryItemComboBox1.Items.Add(ItemCombo);
            
        }


        /// <summary>
        /// Apaga o resistro das colunas de importa��o
        /// </summary>
        public void ClearColunaImp()
        {
            repositoryItemComboBox1.Items.Clear();
            conjuntoColunasImp.Clear();
            DevExpress.XtraEditors.Controls.ComboBoxItem ItemCombo = new DevExpress.XtraEditors.Controls.ComboBoxItem("BLOCO");
            repositoryItemComboBox1.Items.Add(ItemCombo);
            ItemCombo = new DevExpress.XtraEditors.Controls.ComboBoxItem("APARTAMENTO");
            repositoryItemComboBox1.Items.Add(ItemCombo);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.OK);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.Cancel);
        }

    }

    /// <summary>
    /// Objeto para retorno do status da linha
    /// </summary>
    internal class StatusLinha
    {
        public bool BLOencontrado = false;
        public bool APTNumeroexiste = false;
        public bool APTencontrado = false;
        public int BLO = 0;
        public int APT = 0;
    }

    internal class ItenColunaImp {
        public string Nome;
        public string Titulo;
        public Type Tipo;
        public object Maximo;
        public object Minimo;
        public int Coluna;        
        public ItenColunaImp(string _Nome, string _Titulo, Type _Tipo,object _Maximo, object _Minimo)
        {
            Nome = _Nome;
            Titulo = _Titulo;
            Tipo = _Tipo;
            Maximo = _Maximo;
            Minimo = _Minimo;
            Coluna = -1;
        }
    }

    internal class ConjuntoColunasImp : ArrayList
    {
        public void Add(string _Nome, string _Titulo, Type _Tipo, object _Maximo, object _Minimo) {
            this.Add(new ItenColunaImp(_Nome, _Titulo, _Tipo, _Maximo, _Minimo));
        }

        public void Add(string _Nome, string _Titulo, Type _Tipo)
        {
            Add(_Nome, _Titulo, _Tipo,null,null);
        }
    }
}

