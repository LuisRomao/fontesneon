﻿using System;
using Framework.datasets;
using Framework.objetosNeon;

namespace Framework.Alarmes
{
   
    /// <summary>
    /// objeto de aviso
    /// </summary>
    public class Aviso
    {
        /// <summary>
        /// Linha mae
        /// </summary>
        public dAvisos.AVIsosRow LinhaMae;

        /// <summary>
        /// 
        /// </summary>
        private dAvisos _dAvisos1;

        /// <summary>
        /// 
        /// </summary>
        public dAvisos dAvisos1
        {
            get { return _dAvisos1 ?? (_dAvisos1 = new dAvisos()); }
            set { _dAvisos1 = value; }
        }

        /// <summary>
        /// Registro encontrado
        /// </summary>
        public bool Encontrado
        {
            get { return LinhaMae != null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ATVTipo Tipo
        {
            get 
            {
                if (LinhaMae == null)
                    return ATVTipo.Gererico;
                else
                    return (ATVTipo)LinhaMae.AVI_TAV;
            }
        }
         /// <summary>
         /// 
         /// </summary>
        public ATVClasse Classe { 
            get 
            { 
                return (LinhaMae == null) ? ATVClasse.Alerta : (ATVClasse)LinhaMae.AVI_TAV; 
            } 
        }

        /// <summary>
        /// Construtor para aviso existentes
        /// </summary>
        /// <param name="AVI"></param>
        public Aviso(int AVI)
        {            
            dAvisos1.TipoAVisoTableAdapter.Fill(dAvisos1.TipoAViso);            
            VirMSSQL.TableAdapter.EmbarcaEmTrans(dAvisos1.AVIsosTableAdapter);
            if(dAvisos1.AVIsosTableAdapter.FillByAVI(dAvisos1.AVIsos, AVI) == 1)
                LinhaMae = dAvisos1.AVIsos[0];
            
        }

        /// <summary>
        /// Construtor para recuperar aviso
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="Chave"></param>
        public Aviso(ATVTipo Tipo, int Chave)
        {
            dAvisos1.TipoAVisoTableAdapter.Fill(dAvisos1.TipoAViso);
            VirMSSQL.TableAdapter.EmbarcaEmTrans(dAvisos1.AVIsosTableAdapter);
            if (dAvisos1.AVIsosTableAdapter.FillByChave(dAvisos1.AVIsos,(int)Tipo,Chave) >= 1)
                LinhaMae = dAvisos1.AVIsos[0];            
        }

        private void InicarValores(ATVTipo Tipo,int CON,int Chave,Competencia comp,string Descricao,string Historico,int USU)
        {
            dAvisos1.TipoAVisoTableAdapter.Fill(dAvisos1.TipoAViso);
            LinhaMae = dAvisos1.AVIsos.NewAVIsosRow();
            if (CON > 0)
                LinhaMae.AVI_CON = CON;
            LinhaMae.AVI_TAV = (int)Tipo;
            if (Chave > 0)
                LinhaMae.AVIChave = Chave;
            if (comp != null)
                LinhaMae.AVIComp = comp.CompetenciaBind;
            LinhaMae.AVIDataI = DateTime.Now;
            LinhaMae.AVIDescricao = Descricao;
            if (Tipo != ATVTipo.ComunicacaoInternaRTF)
                LinhaMae.AVIHistorico = string.Format("{0:dd/MM/yyyy HH:mm:ss} - Cadastrado\r\n{1}\r\n", DateTime.Now, Historico);
            else
                LinhaMae.AVIHistorico = Historico;
            LinhaMae.AVIResp_USU = USU != 0 ? USU : dAvisos1.TipoAViso.FindByTAV((int)Tipo).TAV_USU;
            LinhaMae.AVIStatus = 0;
            dAvisos1.AVIsos.AddAVIsosRow(LinhaMae);
            dAvisos1.AVIsosTableAdapter.Update(LinhaMae);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="CON"></param>
        /// <param name="Chave"></param>
        /// <param name="comp"></param>
        /// <param name="Descricao"></param>
        /// <param name="Historico"></param>
        public Aviso(ATVTipo Tipo, int CON, int Chave, Competencia comp, string Descricao,string Historico)
        {
            InicarValores(Tipo, CON, Chave, comp, Descricao, Historico,0);                     
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Descricao"></param>
        /// <param name="Historico"></param>
        /// <param name="USU"></param>
        public Aviso(string Descricao, string Historico,int USU)
        {
            InicarValores(ATVTipo.ComunicacaoInternaRTF, 0, 0, null, Descricao, Historico, USU);
        }

        /// <summary>
        /// Reedita o alarme
        /// </summary>
        /// <param name="NovoTipo"></param>
        /// <param name="NovoResp"></param>
        /// <param name="Historico"></param>
        public void ReEdita(ATVTipo NovoTipo,int NovoResp,string Historico)
        {
            if (!Encontrado)
                throw new Exception("Aviso não encontrado");
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("FrameWork Alarmes Aviso 137",dAvisos1.TipoAVisoTableAdapter);
                LinhaMae.AVI_TAV = (int)NovoTipo;
                LinhaMae.AVIResp_USU = NovoResp;
                LinhaMae.SetAVINotificadoNull();
                LinhaMae.AVIHistorico += Historico;
                dAvisos1.AVIsosTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.VircatchSQL(e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Delete()
        {
            if (!Encontrado)
                throw new Exception("Aviso não encontrado");
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Framework Alarmes Aviso 158", dAvisos1.AVIsosTableAdapter);
                LinhaMae.Delete();
                dAvisos1.AVIsosTableAdapter.Update(dAvisos1.AVIsos);
                dAvisos1.AVIsos.AcceptChanges();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
            }
        }
    }
}
