﻿using System.Windows.Forms;
using FrameworkProc.datasets;

namespace Framework.Alarmes
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cDestinatarios : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// construtor padrao
        /// </summary>
        public cDestinatarios()
        {
            InitializeComponent();            
        }

        private System.Collections.ArrayList SBCs;
        private System.Collections.ArrayList SAs;
        private System.Collections.ArrayList Gerentes;

        /// <summary>
        /// construtor
        /// </summary>
        /// <param name="SBC"></param>
        /// <param name="SA"></param>
        /// <param name="Virtual"></param>
        public cDestinatarios(bool SBC,bool SA,bool Virtual)
        {
            InitializeComponent();
            SBCs = new System.Collections.ArrayList();
            SAs = new System.Collections.ArrayList();
            Gerentes = new System.Collections.ArrayList();
            if (SBC)
                chGSBC.Items.Add(new DevExpress.XtraEditors.Controls.CheckedListBoxItem(-1, "* TODOS-SBC", CheckState.Unchecked, true));
            if (SA)
                chGSBC.Items.Add(new DevExpress.XtraEditors.Controls.CheckedListBoxItem(-3, "* TODOS-SA", CheckState.Unchecked, false));
            chGSBC.Items.Add(new DevExpress.XtraEditors.Controls.CheckedListBoxItem(0, "* Gerentes", CheckState.Unchecked, true));
            foreach (dUSUarios.USUariosRow rowUSU in dUSUarios.dUSUariosSt.USUarios)
            {
                DevExpress.XtraEditors.Controls.CheckedListBoxItem it = new DevExpress.XtraEditors.Controls.CheckedListBoxItem(rowUSU.USU, rowUSU.USUNome, CheckState.Unchecked, true);
                chGSBC.Items.Add(it);
                SBCs.Add(it);
                if (dUSUarios.dUSUariosStGerentes.USUarios.FindByUSU(rowUSU.USU) != null)
                    Gerentes.Add(it);
            }
            chGSBC.SortOrder = SortOrder.Ascending;
        }

        /// <summary>
        /// Retorno
        /// </summary>
        public System.Collections.ArrayList Selecionados;

        /// <summary>
        /// FechaTela
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            Selecionados = new System.Collections.ArrayList();
            foreach (DevExpress.XtraEditors.Controls.CheckedListBoxItem it in chGSBC.Items)
                if (it.CheckState == CheckState.Checked)
                    Selecionados.Add(it.Value);
            base.FechaTela(Resultado);
        }

        private bool trava = false;

        private void chGSBC_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            if (trava)
                return;
            try
            {
                trava = true;
                DevExpress.XtraEditors.Controls.CheckedListBoxItem it = (DevExpress.XtraEditors.Controls.CheckedListBoxItem)chGSBC.Items[e.Index];
                if (it != null)
                {
                    int Valor = (int)it.Value;
                    if (Valor == -1)
                    {                        
                        foreach (DevExpress.XtraEditors.Controls.CheckedListBoxItem itx in SBCs)
                            itx.CheckState = it.CheckState;
                    }
                    if (Valor == 0)
                    {
                        foreach (DevExpress.XtraEditors.Controls.CheckedListBoxItem itx in Gerentes)
                            itx.CheckState = it.CheckState;
                    }
                }
            }
            finally
            {
                trava = false;
            }
        }

    }
}
