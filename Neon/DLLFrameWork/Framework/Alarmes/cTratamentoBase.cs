﻿using CompontesBasicos;
using Framework.datasets;

namespace Framework.Alarmes
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cTratamentoBase : ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor padrão, não usar, somente para carga em designer time
        /// </summary>
        public cTratamentoBase()
        {
            InitializeComponent();            
        }

        //public dAvisos.AVIsosRow AVIrow;

        //protected dAvisos dAvisos1;

        /// <summary>
        /// 
        /// </summary>
        protected Aviso Aviso1;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="AVI">Aviso</param>
        public cTratamentoBase(int AVI)
        {
            InitializeComponent();            
            Aviso1 = new Aviso(AVI);
            Abortar_Por_Erro = !Aviso1.Encontrado;            
            if (!Abortar_Por_Erro)
            {
                if (Aviso1.Classe == ATVClasse.Aviso)
                {
                    pictureEdit2.Location = pictureEdit1.Location;
                    pictureEdit1.Visible = false;
                    pictureEdit2.Visible = true;
                }
                labelTipo.Text = virEnumATVTipo.virEnumATVTipoSt.Descritivo(Aviso1.LinhaMae.AVI_TAV);
            }
        }
    }
}
