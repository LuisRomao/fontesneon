﻿using CompontesBasicos;
using Framework.datasets;
using FrameworkProc.datasets;
using System;
using System.Data;
using System.Windows.Forms;
using VirEnumeracoesNeon;

namespace Framework.Alarmes
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cMeusAlarmes : ComponenteBase
    {
        private readonly dAvisos dAvisos1;

        private bool block = false;

        private bool ManterAberta = false;

        /// <summary>
        /// 
        /// </summary>
        public cMeusAlarmes()
        {
            InitializeComponent();
            dAvisos1 = new dAvisos();
            radioGroup1.SelectedIndex = 0;            
            dAvisosBindingSource.DataSource = dAvisos1;
            dUSUariosBindingSource.DataSource = dUSUarios.dUSUariosStTodos;
            cONDOMINIOSBindingSource.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST;
            datasets.virEnumATVTipo.virEnumATVTipoSt.CarregaEditorDaGrid(colAVI_TAV);
            //datasets.virEnumATVClasse.virEnumATVClasseSt.CarregaEditorDaGrid(colatvc
            //timer1.Interval = 30 * 60 * 1000;
            timer1.Interval = 1 * 1000;
            //if (!FormPrincipalBase.strEmProducao)
              //  timer1.Interval = 5 * 60 * 1000;
            //Verificacao();
            timer1.Enabled = true;
            simpleButton3.Enabled = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("GERÊNCIA") > 1);
        }

        /// <summary>
        /// Chamar pelo mapa geral
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            int Gerente = (int)args[0];
            radioGroup1.SelectedIndex = 1;
            lookupGerente1.USUSel = Gerente;
            Verificacao();
            gridView1.ActiveFilterString = "Vencido = True";
            return null;
        }

        /// <summary>
        /// Esta função deve ser eliminada referncia OneNote Remover
        /// </summary>
        /// <param name="chave"></param>
        private bool RemoveLinhaDuplicada(int chave)
        {
            bool retorno = false;
            string comando1 =
"SELECT     PGF, NOACompet\r\n" +
"FROM         CHEques INNER JOIN\r\n" +
"                      PAGamentos ON CHEques.CHE = PAGamentos.PAG_CHE INNER JOIN\r\n" +
"                      NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA INNER JOIN\r\n" +
"                      PaGamentosFixos ON NOtAs.NOA_PGF = PaGamentosFixos.PGF\r\n" +
"WHERE     (CHE = @P1);";

            string comando2 =
"SELECT     CHE, CHEStatus, PAG, NOA, CHE_CCD, CHEValor\r\n" +
"FROM         CHEques INNER JOIN\r\n" +
"                      PAGamentos ON CHEques.CHE = PAGamentos.PAG_CHE INNER JOIN\r\n" +
"                      NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA INNER JOIN\r\n" +
"                      PaGamentosFixos ON NOtAs.NOA_PGF = PaGamentosFixos.PGF\r\n" +
"WHERE     (PGF = @P1) AND (NOACompet = @P2) ORDER BY CHEStatus;";

            DataTable DT1 = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comando1, chave);
            if ((DT1 != null) && (DT1.Rows.Count == 1))
            {
                int PGF = (int)(DT1.Rows[0]["PGF"]);
                int nCompet = (int)(DT1.Rows[0]["NOACompet"]);
                DataTable DT2 = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comando2, PGF, nCompet);
                if ((DT2 != null) && (DT2.Rows.Count == 2))
                {
                    DataRow DR7 = DT2.Rows[0];
                    DataRow DR10 = DT2.Rows[1];
                    if (((int)DR7["CHEStatus"] == 7) &&
                        ((int)DR10["CHEStatus"] == 10) &&
                        ((decimal)DR7["CHEValor"] == (decimal)DR10["CHEValor"]) &&
                        (DR7["CHE_CCD"] != DBNull.Value) &&
                        (DR10["CHE_CCD"] == DBNull.Value))
                    {
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("Framework Alarmes - cMeusAlarmes 95");
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from PAGamentos where PAG = @P1", DR10["PAG"]);
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from CHEques where CHE = @P1", DR10["CHE"]);
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete from NOtAs where NOA = @P1", DR10["NOA"]);
                            VirMSSQL.TableAdapter.CommitSQL();
                            retorno = true;
                        }
                        catch (Exception e)
                        {
                            VirMSSQL.TableAdapter.VircatchSQL(e);
                        }
                        /*delete from PAGamentos where PAG = 301160
delete from CHEques where CHE = 245857
delete from NOtAs where NOA = 213321
delete from AVIsos where AVI =*/
                    }
                    //int PGF = (int)(DT1.Rows[0]["PGF"]);
                    //int nCompet = (int)(DT1.Rows[0]["NOACompet"]);
                }
            }
            return retorno;
        }

        private bool Verificacao()
        {            
            if (block)
                return true;
            else
            {
                try
                {
                    block = true;
                    int AVIanterior = -1;
                    dAvisos.AVIsosRow AVIrowAnterior = (dAvisos.AVIsosRow)gridView1.GetFocusedDataRow();
                    if (AVIrowAnterior != null)
                        AVIanterior = AVIrowAnterior.AVI;
                    dAvisos1.AVIsos.Clear();
                    dAvisos1.TipoAVisoTableAdapter.Fill(dAvisos1.TipoAViso);
                    bool reserva = dAvisos1.AVIsosTableAdapter.ClearBeforeFill;
                    using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                    {
                        ESP.Motivo.Text = "Carregando Dados";
                        Application.DoEvents();
                        try
                        {
                            dAvisos1.AVIsos.Clear();
                            dAvisos1.AVIsosTableAdapter.ClearBeforeFill = false;
                            switch (radioGroup1.SelectedIndex)
                            {
                                case 0:                                    
                                    dAvisos1.AVIsosTableAdapter.FillByUSU_9(dAvisos1.AVIsos, FormPrincipalBase.USULogado);
                                    dAvisos1.AVIsosTableAdapter.FillByUSU_10(dAvisos1.AVIsos, FormPrincipalBase.USULogado);
                                    dAvisos1.AVIsosTableAdapter.FillByUSU_14(dAvisos1.AVIsos, FormPrincipalBase.USULogado);
                                    dAvisos1.AVIsosTableAdapter.FillByUSU_X(dAvisos1.AVIsos, FormPrincipalBase.USULogado);
                                    break;
                                case 1:
                                    if (lookupGerente1.USUSel == -1)
                                    {                                       
                                        dAvisos1.AVIsosTableAdapter.FillByALARMES_9(dAvisos1.AVIsos);
                                        dAvisos1.AVIsosTableAdapter.FillByALARMES_10(dAvisos1.AVIsos);
                                        dAvisos1.AVIsosTableAdapter.FillByALARMES_14(dAvisos1.AVIsos);
                                        dAvisos1.AVIsosTableAdapter.FillByALARMES_X(dAvisos1.AVIsos);
                                    }
                                    else
                                    {                                        
                                        dAvisos1.AVIsosTableAdapter.FillByGerente_9(dAvisos1.AVIsos, lookupGerente1.USUSel);
                                        dAvisos1.AVIsosTableAdapter.FillByGerente_10(dAvisos1.AVIsos, lookupGerente1.USUSel);
                                        dAvisos1.AVIsosTableAdapter.FillByGerente_14(dAvisos1.AVIsos, lookupGerente1.USUSel);
                                        dAvisos1.AVIsosTableAdapter.FillByGerente_X(dAvisos1.AVIsos, lookupGerente1.USUSel);
                                    }
                                    break;
                                case 2:
                                    if (lookupGerente1.USUSel == -1)
                                    {                                     
                                        dAvisos1.AVIsosTableAdapter.FillByALARMES_9(dAvisos1.AVIsos);
                                        dAvisos1.AVIsosTableAdapter.FillByALARMES_10(dAvisos1.AVIsos);
                                        dAvisos1.AVIsosTableAdapter.FillByALARMES_14(dAvisos1.AVIsos);
                                        dAvisos1.AVIsosTableAdapter.FillByALARMES_X(dAvisos1.AVIsos);
                                    }
                                    else
                                    {                                     
                                        dAvisos1.AVIsosTableAdapter.FillByAssistente_9(dAvisos1.AVIsos, lookupGerente1.USUSel);
                                        dAvisos1.AVIsosTableAdapter.FillByAssistente_10(dAvisos1.AVIsos, lookupGerente1.USUSel);
                                        dAvisos1.AVIsosTableAdapter.FillByAssistente_14(dAvisos1.AVIsos, lookupGerente1.USUSel);
                                        dAvisos1.AVIsosTableAdapter.FillByAssistente_X(dAvisos1.AVIsos, lookupGerente1.USUSel);
                                    }
                                    break;
                            };
                        }
                        finally
                        {
                            dAvisos1.AVIsosTableAdapter.ClearBeforeFill = reserva;
                        }
                        ESP.AtivaGauge(dAvisos1.AVIsos.Count);
                        ESP.Motivo.Text = "Atualizando...";                        
                        Application.DoEvents();
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Geral");
                        foreach (dAvisos.AVIsosRow row in dAvisos1.AVIsos)
                        {
                            DateTime DataLimite = row.AVIDataI;
                            DataLimite.AddDays(row.TAVdiasaviso + 1);
                            row.Vencido = DataLimite < DateTime.Today;
                        }
                        dAvisos1.AVIsos.AcceptChanges();

                        System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
                        System.Collections.ArrayList Abrir = new System.Collections.ArrayList();
                        //if (radioGroup1.SelectedIndex == 0)
                        //{
                        if (!ManterAberta)
                            TabVisivel(dAvisos1.AVIsos.Count != 0);
                        ManterAberta = true;
                        int i=0;
                        DateTime prox = DateTime.Now.AddSeconds(3);
                        foreach (dAvisos.AVIsosRow row in dAvisos1.AVIsos)
                        {
                            i++;
                            if (DateTime.Now > prox)
                            {
                                ESP.Gauge(i);
                                ESP.Motivo.Text = string.Format("Atualizando {0}/{1}", i, dAvisos1.AVIsos.Count);
                                Application.DoEvents();
                                prox = DateTime.Now.AddSeconds(3);
                                if (ESP.Abortar)
                                    break;
                            }
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("loop");
                            ATVTipo Tipo = (ATVTipo)row.AVI_TAV;
                            switch (Tipo)
                            {
                                case ATVTipo.DebitoAutomaticoNaoOcorreu:
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("case 1");
                                    //int iCHEStatus;
                                    //VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CHEStatus from CHEques where CHE = @P1", out iCHEStatus, row.AVIChave);
                                    //Referência OneNote: corrigir - deveria usar a enumeração
                                    if (row.CHEStatus != 10)
                                        Apagar.Add(row);
                                    break;
                                case ATVTipo.DebitoAutomaticoNaoPrevisto:
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("case 2");
                                    //int iStatusConsolidadoCCD = 1;
                                    //if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CCDConsolidado from contacorrentedetalhe where ccd = @P1", out iStatusConsolidadoCCD, row.AVIChave))
                                    //if (iStatusConsolidadoCCD != 1)
                                    if (row.CCDConsolidado != 1)
                                        Apagar.Add(row);
                                    break;
                                case ATVTipo.DebitoAutomaticoNaoOcorreu1:
                                    /*if (!PoolPGFCriado)
                                    {
                                        PoolPGFCriado = true;
                                        AbstratosNeon.ABS_PagamentoPeriodico.CarregaPGFAtivos();
                                    }*/
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("case 3");
                                    //AbstratosNeon.ABS_PagamentoPeriodico per = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodicoPool(row.AVIChave);                                                                
                                    Framework.objetosNeon.Competencia CompAlarme = new Framework.objetosNeon.Competencia(row.AVIComp);
                                    //Framework.objetosNeon.Competencia CompProxima = (Framework.objetosNeon.Competencia)per.ABS_ProximaCompetencia;
                                    Framework.objetosNeon.Competencia CompProxima = new Framework.objetosNeon.Competencia(row.PGFProximaCompetencia);
                                    if (CompProxima > CompAlarme)
                                    {
                                        AbstratosNeon.ABS_PagamentoPeriodico per = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(row.AVIChave);
                                        per.Salvar(string.Format("Alarme removido - Competência do alarme {0} - Próxima competência {1}", CompAlarme, CompProxima));
                                        Apagar.Add(row);
                                    }
                                    else if ((row.PGFStatus != (int)StatusPagamentoPeriodico.Ativado) && (row.PGFStatus != (int)StatusPagamentoPeriodico.AtivadoSemDOC))
                                    {
                                        AbstratosNeon.ABS_PagamentoPeriodico per1 = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(row.AVIChave);
                                        per1.Salvar("Alarme removido - Periódico desativado");
                                        Apagar.Add(row);
                                    }
                                    break;
                            }
                            CompontesBasicos.Performance.Performance.PerformanceST.Registra("loop");
                            if ((row.AVIResp_USU == FormPrincipalBase.USULogado) && (row.IsAVINotificadoNull()))
                            {
                                if (Tipo == ATVTipo.ComunicacaoInternaRTF)
                                {
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("RTF");
                                    ComponenteBase EditorRTF = (ComponenteBase)ModuleInfo.ContruaObjeto("maladireta.EditorRTF.cGeraRTF", row.AVIHistorico);
                                    EditorRTF.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
                                    Apagar.Add(row);
                                }
                                else
                                {
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Alerta");
                                    if (Abrir.Count < 2)
                                    {
                                        cTrataAVIbase Alerta = new cTrataAVIbase(row);
                                        Abrir.Add(Alerta);
                                        row.AVINotificado = DateTime.Now;
                                        dAvisos1.AVIsosTableAdapter.Update(row);
                                        row.AcceptChanges();
                                    }
                                }

                            }
                        }
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Apagando");
                        ESP.AtivaGauge(Apagar.Count);
                        i = 0;
                        ESP.Motivo.Text = string.Format("Apagando {0}/{1}", i, Apagar);
                        Application.DoEvents();
                        foreach (dAvisos.AVIsosRow row in Apagar)
                        {
                            i++;
                            row.Delete();
                            if (DateTime.Now > prox)
                            {
                                ESP.Gauge(i);
                                ESP.Espere(string.Format("Apagando {0}/{1}", i, Apagar));
                                Application.DoEvents();
                                prox = DateTime.Now.AddSeconds(3);
                                if (ESP.Abortar)
                                    break;
                            }
                        }
                        ESP.Espere(string.Format("Gravação"));
                        Application.DoEvents();
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("update");
                        dAvisos1.AVIsosTableAdapter.Update(dAvisos1.AVIsos);
                        dAvisos1.AVIsos.AcceptChanges();
                        //}
                        if (AVIanterior != -1)                        
                            gridView1.FocusedRowHandle = gridView1.LocateByValue("AVI", AVIanterior);
                        
                        foreach (cTrataAVIbase Alerta in Abrir)
                            Alerta.ShowEmPopUp();
                        //string relatorio = CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();
                        //Clipboard.SetText(relatorio);
                        //MessageBox.Show(relatorio);
                    }
                    return true;
                }
                finally
                {
                    block = false;
                }
            }
        }
    

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false; 
            //if (radioGroup1.SelectedIndex == 0)
            Verificacao();
                             
        }

        

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if ((e.Column == colAVIComp) && (e.Value != null) && (e.Value != DBNull.Value)) 
            {
                objetosNeon.Competencia comp = new objetosNeon.Competencia((int)e.Value);
                e.DisplayText = comp.ToString();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (block)
                return;
            else
            {
                try
                {
                    block = true;
                    dAvisos.AVIsosRow AVIrow = (dAvisos.AVIsosRow)gridView1.GetFocusedDataRow();
                    if(AVIrow == null)
                        return;
                    string Anotacao = "";
                    if(VirInput.Input.Execute("Anotação:",ref Anotacao,true) && (Anotacao.Trim() != ""))
                    {
                        AVIrow.AVIHistorico += string.Format("{0:dd/MM/yyyy HH:mm:ss} - {1}:\r\n{2}\r\n",DateTime.Now,CompontesBasicos.FormPrincipalBase.USULogadoNome,Anotacao.Trim());
                        dAvisos1.AVIsosTableAdapter.Update(AVIrow);
                        AVIrow.AcceptChanges();
                    }
                }
                finally
                {
                    block = false;
                }
            }
        }

        private void Abre()
        {
            if (block)
                return;
            else
            {
                try
                {
                    block = true;
                    dAvisos.AVIsosRow AVIrow = (dAvisos.AVIsosRow)gridView1.GetFocusedDataRow();
                    if (AVIrow == null)
                        return;
                    cTrataAVIbase Alerta = new cTrataAVIbase(AVIrow);
                    Alerta.ShowEmPopUp();
                }
                finally
                {
                    block = false;
                }
            }
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            Abre();            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Verificacao();
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dAvisos.AVIsosRow row = (dAvisos.AVIsosRow)gridView1.GetDataRow(e.FocusedRowHandle);
            if (row == null)
                return;
            if (row.TipoAVisoRow.TAV_USU == CompontesBasicos.FormPrincipalBase.USULogado)
                colAVIResp_USU.OptionsColumn.ReadOnly = false;
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dAvisos.AVIsosRow row = (dAvisos.AVIsosRow)gridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            int USUAnteriro = (int)row["AVIResp_USU", DataRowVersion.Original];
            string de = dUSUarios.dUSUariosStTodos.USUarios.FindByUSU(USUAnteriro).USUNome;
            string para = dUSUarios.dUSUariosStTodos.USUarios.FindByUSU(row.AVIResp_USU).USUNome;
            row.AVIHistorico += string.Format("{0:dd/MM/yyyy} - {1} Responsável alterado: {2} -> {3}\r\n",
                                                  DateTime.Now,
                                                  CompontesBasicos.FormPrincipalBase.USULogadoNome,
                                                  de,
                                                  para);
            dAvisos1.AVIsosTableAdapter.Update(row);
            row.AcceptChanges();
        }

       

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {            
            Abre();
        }

        private void Retorno(ComponenteBase Sender, DialogResult Modo, params object[] dados)
        {
            if (dados.Length == 0)
                return;
            string strRTF = dados[0].ToString();
            if (Modo == System.Windows.Forms.DialogResult.OK)
            {
                cDestinatarios dest = new cDestinatarios(true, true, true);
                if (dest.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    foreach (int xUSU in dest.Selecionados)
                    {
                        string Descricao = string.Format("{0:dd/MM/yyyy HH:mm} - {1}", DateTime.Now, CompontesBasicos.FormPrincipalBase.USULogadoNome);
                        new Aviso(Descricao, strRTF, xUSU);
                    }
                }                
            }
        }


        private void simpleButton3_Click(object sender, EventArgs e)
        {            
            del_retornodechamada delRetono = new del_retornodechamada(Retorno);
            ComponenteBase EditorRTF = (ComponenteBase)ModuleInfo.ContruaObjeto("maladireta.EditorRTF.cGeraRTF", delRetono, "");
            EditorRTF.VirShowModulo(EstadosDosComponentes.JanelasAtivas);            
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (radioGroup1.SelectedIndex)
            {
                case 0:
                    lookupGerente1.Enabled = false;
                    break;
                case 1:
                    lookupGerente1.Enabled = true;
                    lookupGerente1.Tipo = Lookup.LookupGerente.TipoUsuario.gerente;
                    break;
                case 2:
                    lookupGerente1.Enabled = true;
                    lookupGerente1.Tipo = Lookup.LookupGerente.TipoUsuario.assistente;
                    break;
            } 
            dAvisos1.AVIsos.Clear();
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colAVI_TAV)
            {
                dAvisos.AVIsosRow AVIrow = (dAvisos.AVIsosRow)gridView1.GetDataRow(e.RowHandle);
                if (AVIrow == null)
                    return;
                switch (AVIrow.AVI_TAV)
                {
                    case 9:
                        e.Appearance.BackColor = System.Drawing.Color.Pink;
                        break;
                    case 10:
                        e.Appearance.BackColor = System.Drawing.Color.Cornsilk;
                        break;
                    case 14:
                        e.Appearance.BackColor = System.Drawing.Color.Aqua;
                        break;
                    default:
                        break;
                }
                e.Appearance.ForeColor = System.Drawing.Color.Black;
            }
        }
    }
}
