﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using Framework.datasets;

namespace Framework.Alarmes
{
    /// <summary>
    /// Componete popup para chamar as telas de tratamento
    /// </summary>
    public partial class cTrataAVIbase : ComponenteBase
    {
        private dAvisos.AVIsosRow LinhaMae;

        private ATVTipo TipoAVI
        {
            //Podemos confiar que LinhaMae é populado pelo construtor
            get { return (ATVTipo)LinhaMae.AVI_TAV; }
        }

        private System.Collections.ArrayList AvisosDetalhes
        {
            get 
            { 
                return new System.Collections.ArrayList(
                                   new ATVTipo[] { 
                                                   ATVTipo.DebitoAutomaticoComValorIncorreto, 
                                                   ATVTipo.DebitoAutomaticoPulado,
                                                   ATVTipo.DebitoAutomaticoDocumento
                                                 }
                                                       ); 
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rowAVI"></param>
        public cTrataAVIbase(dAvisos.AVIsosRow rowAVI)
        {
            InitializeComponent();
            //dAvisos1 = (dAvisos)rowAVI.Table.DataSet;
            LinhaMae = rowAVI;
            aVIsosRowBindingSource.DataSource = rowAVI;
            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            ATVClasse Classe = (ATVClasse)rowAVI.TipoAVisoRow.TAVClasse;
            switch (Classe) 
            {
                case ATVClasse.Alerta:
                    xtraTabControl1.SelectedTabPage = xtraTabPage1;
                    break;
                case ATVClasse.Aviso:
                    xtraTabControl1.SelectedTabPage = xtraTabPage2;
                    if (AvisosDetalhes.Contains((ATVTipo)LinhaMae.AVI_TAV))
                        simpleButtonDetalhes.Visible = true;
                        //simpleButtonAviso.Text = "";                    
                    break;
                case ATVClasse.Informacao:
                    xtraTabControl1.SelectedTabPage = xtraTabPage3;
                    break;
            }
        }

        
        /*
        static public cTrataAVIbase Construtor(dAvisos.AVIsosRow rowAVI)
        {
            cTrataAVIbase retorno = new cTrataAVIbase(rowAVI);
            return retorno;
        }*/

        private void Tratar()
        {
            FechaTela(DialogResult.OK);
            string nomeTipo = "Framework.Alarmes.cTratamentoBase";
            switch (TipoAVI)
            {
                case ATVTipo.Gererico:
                    break;
                case ATVTipo.CondominioDesativado:
                    break;
                case ATVTipo.ChequeNaoEncontrado:
                    break;
                case ATVTipo.ChequeDuplicidade:
                    break;
                case ATVTipo.ChequeValor:
                    break;
                
                    
                case ATVTipo.SEFIPRetificar:
                    break;
                case ATVTipo.ISSRetificar:
                    break;
                case ATVTipo.INSSSindico:
                    break;
                case ATVTipo.RetornoDeTributo:
                    break;
                case ATVTipo.DebitoAutomaticoNaoPrevisto:
                case ATVTipo.DebitoAutomaticoNaoOcorreu:                
                case ATVTipo.DebitoAutomaticoComValorIncorreto:
                case ATVTipo.DebitoAutomaticoNaoOcorreu1:
                    nomeTipo = "ContaPagar.ContasAgLuz.cTratamentoDebAut";
                    break;

                case ATVTipo.DebitoAutomaticoDocumento:    
                case ATVTipo.DebitoAutomaticoPulado:
                    Integrador.Integrador.AbreComponente(Integrador.TiposIntegra.Periodicos, LinhaMae.AVIChave, false, false);
                    return;                    
                default:
                    break;
            }

            Type Tipo = CompontesBasicos.ModuleInfo.BuscaType(nomeTipo);
            System.Reflection.ConstructorInfo constructorInfoObj = Tipo.GetConstructor(new Type[] { typeof(int) });
            if (constructorInfoObj == null)
                return;
            cTratamentoBase cTratador = (constructorInfoObj.Invoke(new object[] { LinhaMae.AVI }) as cTratamentoBase);
            if (!cTratador.Abortar_Por_Erro)
                cTratador.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Tratar();            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.Cancel);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {            
                FechaTela(DialogResult.OK);
                LinhaMae.Delete();
                dAvisos dAvisos1 = (dAvisos)LinhaMae.Table.DataSet;
                dAvisos1.AVIsosTableAdapter.Update(dAvisos1.AVIsos);
                dAvisos1.AcceptChanges();            
        }

        private void simpleButtonDetalhes_Click(object sender, EventArgs e)
        {
            Tratar();            
        }
    }
}
