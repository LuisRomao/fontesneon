﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.objetosNeon;

namespace Framework.Abstratos
{
    /// <summary>
    /// Objeto abstrato para o balancete
    /// </summary>
    public abstract class ABS_balancete
    {
        /// <summary>
        /// Cria um objeto real tipo balancete (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_balancete GetBalancete(int CON,Competencia comp)
        {
            return (ABS_balancete)CompontesBasicos.ModuleInfo.ContruaObjeto("Balancete.GrBalancete.balancete",CON,comp); 
        }

        /// <summary>
        /// Indica se o balance já é o novo. deve ser eliminado após a conversão de todos
        /// </summary>
        public abstract bool Antigo { get; }

        /// <summary>
        /// Indica se o balancete foi encontrado no Banco
        /// </summary>
        public abstract bool encontrado { get; }
        
        /// <summary>
        /// Competência
        /// </summary>
        public abstract Competencia comp { get; }

        /// <summary>
        /// Data inicial
        /// </summary>
        public abstract DateTime DataI { get; }

        /// <summary>
        /// Data Final
        /// </summary>
        public abstract DateTime DataF { get; }

        /// <summary>
        /// Todas as contas Lógicas
        /// </summary>
        public  SortedList<int,ContaLogica> ContasLogicas;
    }

    /// <summary>
    /// Uma conta lógica
    /// </summary>
    public class ContaLogica
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CTLNome"></param>
        /// <param name="SaldoI"></param>
        /// <param name="SaldoF"></param>
        public ContaLogica(string CTLNome, decimal SaldoI, decimal SaldoF)
        {
            this.CTLNome = CTLNome;
            this.SaldoI = SaldoI;
            this.SaldoF = SaldoF;
        }

        /// <summary>
        /// Nome
        /// </summary>
        public string CTLNome;
        /// <summary>
        /// Saldo Inicial
        /// </summary>
        public decimal SaldoI;
        /// <summary>
        /// Saldo Final
        /// </summary>
        public decimal SaldoF;
    }
}
