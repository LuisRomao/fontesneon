﻿using System;
using System.Windows.Forms;
using CodBar;

namespace CodBarNeon
{    
    /// <summary>
    /// Classe abstrata para impressão em impressora térmica
    /// </summary>
    public abstract class ImpressNeon:Impress
    {
        private static string _NomeImpressoraSt = "";

        /// <summary>
        /// Nome da impressora
        /// </summary>
        public override string NomeImpressora
        {
            get
            {
                if (_NomeImpressoraSt == "")
                {
                    _NomeImpressoraSt = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de cópias de Cheque");
                    if (_NomeImpressoraSt == "")
                    {
                        PrintDialog PD = new PrintDialog();
                        PD.ShowDialog();
                        _NomeImpressoraSt = PD.PrinterSettings.PrinterName;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de cópias de Cheque", _NomeImpressoraSt);
                    }
                }
                return _NomeImpressoraSt;
            }
            set
            {
                _NomeImpressoraSt = value;
            }
        }
        
        private static LinguagemCodBar _LinguagemCB = LinguagemCodBar.Indefinida;

        /// <summary>
        /// 
        /// </summary>
        private static LinguagemCodBar LinguagemCB
        {
            get
            {
                if (_LinguagemCB == LinguagemCodBar.Indefinida)
                {
                    try
                    {
                        _LinguagemCB = (LinguagemCodBar)Enum.Parse(typeof(CodBar.LinguagemCodBar), CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Linguagem codbar"));
                    }
                    catch
                    {
                        _LinguagemCB = CodBar.LinguagemCodBar.EPL2;
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Linguagem codbar", LinguagemCB.ToString());
                    }                    
                }
                return _LinguagemCB;
            }
        }

        public ImpressNeon()
            : base(LinguagemCB)
        { 
        }
    }
}
