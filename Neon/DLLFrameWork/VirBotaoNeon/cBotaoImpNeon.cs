using System;
using System.ComponentModel;
using dllBotao;

namespace VirBotaoNeon
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cBotaoImpNeon : cBotaoImp
    {
        /// <summary>
        /// 
        /// </summary>
        public cBotaoImpNeon()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Nome Arquivo Anexo")]
        public string NomeArquivoAnexo
        {
            get
            {
                return nomeArquivoAnexo;
            }
            set
            {
                nomeArquivoAnexo = value;
            }
        }

        private string nomeArquivoAnexo;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Corpo do e.mail")]
        public string CorpoEmail
        {
            get
            {
                return corpoEmail;
            }
            set
            {
                corpoEmail = value;
            }
        }

        private string corpoEmail;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Assunto do e.mail")]
        public string AssuntoEmail
        {
            get
            {
                return assuntoEmail;
            }
            set
            {
                assuntoEmail = value;
            }
        }

        private string assuntoEmail;
        /// <summary>
        /// 
        /// </summary>
        public string EmailDefault;
        /// <summary>
        /// 
        /// </summary>
        protected override void GeraEmail()
        {
            if (Impresso != null)
                if(!VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", NomeArquivoAnexo, Impresso, CorpoEmail, AssuntoEmail, EmailDefault))
                    NotificarErro(VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro ?? new Exception("Erro no envio do e.mail"));
        }
    }
}
