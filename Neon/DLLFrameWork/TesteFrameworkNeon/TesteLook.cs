﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using VirEmail;
using VirEmailNeon;
using Framework;
using FrameworkProc;


namespace TesteFrameworkNeon
{
    public partial class TesteLook : CompontesBasicos.ComponenteBase
    {
        public TesteLook()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(lookupBlocosAptos_F1.APT_Selrow.APTNumero);
            MessageBox.Show(lookupCondominio_F1.CON_selrow.CONCodigo);
            MessageBox.Show(lookupBlocosAptos_F1.BLO_Selrow.BLOCodigo);
            MessageBox.Show(lookupGerente1.USUNomeSel);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Framework.Lookup.dComponente_F.dComponente_FSt.CONDOMINIOS.Clear();
            Framework.Lookup.dComponente_F.dComponente_FSt.BLOCOS.Clear();
            Framework.Lookup.dComponente_F.dComponente_FSt.APARTAMENTOS.Clear();
            //Framework.Lookup.dComponente_F.dComponente
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            OpenFileDialog Arquivos = new OpenFileDialog();
            Arquivos.Multiselect = true;

            
            if (Arquivos.ShowDialog() == DialogResult.OK)
            {
                int i = 0;
                System.Net.Mail.Attachment[] at1 = new System.Net.Mail.Attachment[Arquivos.FileNames.Length];
                foreach (string Arq in Arquivos.FileNames)
                {
                    at1[i++] = new System.Net.Mail.Attachment(Arq);                    
                }
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, "luis@virweb.com.br", "Teste l1\r\nl2", "Teste", at1);
            }

        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("Pendentes: {0}", VirEmailNeon.EmailDiretoNeon.EmalST.Pendentes));
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            Attachment[] Anexos = new Attachment[] { new Attachment(@"d:\XML\PPFI15052014.pdf"), new Attachment(@"d:\XML\PPFI15052014.pdf"), new Attachment(@"d:\XML\PPFI15052014.pdf") };
            for (int i = 0; i < 10; i++)
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, "luis@virweb.com.br", "ViaServidor", "Assunto via servidor " + i.ToString(), Anexos);            
        }

        private void checkEdit1_Click(object sender, EventArgs e)
        {
            EmailDiretoNeon.EmalST.GerarLog = checkEdit1.Checked;
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            string comando = 
"SELECT        TOP (5000) dbo.BOLetos.BOL, dbo.BOLetos.BOLVencto, dbo.CONDOMINIOS.CONCodigo, dbo.BLOCOS.BLOCodigo, aptnumero,boljustificativa\r\n" + 
"FROM            dbo.BOLetos INNER JOIN\r\n" + 
"                         dbo.APARTAMENTOS ON dbo.BOLetos.BOL_APT = dbo.APARTAMENTOS.APT INNER JOIN\r\n" + 
"                         dbo.BLOCOS ON dbo.APARTAMENTOS.APT_BLO = dbo.BLOCOS.BLO INNER JOIN\r\n" + 
"                         dbo.CONDOMINIOS ON dbo.BOLetos.BOL_CON = dbo.CONDOMINIOS.CON AND dbo.BLOCOS.BLO_CON = dbo.CONDOMINIOS.CON\r\n" + 
"ORDER BY dbo.BOLetos.BOL DESC;";
            DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(comando);
            MessageBox.Show("Total " + DT.Rows.Count.ToString());
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            VirMSSQL.TableAdapter.AbreTrasacaoSQL("teste");
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update boletos set boljustificativa = 'mod' where bol in (22439180,22439188)");
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            VirMSSQL.TableAdapter.CommitSQL();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("Pendentes: {0}", VirEmailNeon.EmailDiretoNeon.EmalST.Pendentes));
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            lookupCondominio_F2.Filial = lookupCondominio_F2.Filial == EMPTipo.Filial ? EMPTipo.Local : EMPTipo.Filial;
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            FrameworkProc.datasets.dCondominiosAtivos x = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(EMPTipo.Local);
            x = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosSTX(EMPTipo.Local);
            x = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(EMPTipo.Filial);
            x = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosSTX(EMPTipo.Filial);
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            lookupGerente2.Tipo = lookupGerente3.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.gerente;
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            lookupGerente2.Tipo = lookupGerente3.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.assistente;
        }
    }
}
