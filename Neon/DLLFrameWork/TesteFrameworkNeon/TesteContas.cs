﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace TesteFrameworkNeon
{
    public partial class TesteContas : CompontesBasicos.ComponenteBase
    {
        public TesteContas()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Framework.objetosNeon.cContaCorrenteNeon cContaCorrenteNeon1 = new Framework.objetosNeon.cContaCorrenteNeon();
            if (cContaCorrenteNeon1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                MessageBox.Show("ok");
                spinEdit1.Value = cContaCorrenteNeon1.contaCorrenteNeon1.CCG.Value;
            }
            else
                MessageBox.Show("cancelado");
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            FrameworkProc.objetosNeon.ContaCorrenteNeon ContaCorrenteNeon1 = new FrameworkProc.objetosNeon.ContaCorrenteNeon((int)spinEdit1.Value, FrameworkProc.objetosNeon.ContaCorrenteNeon.TipoChave.CCG);
            Framework.objetosNeon.cContaCorrenteNeon cContaCorrenteNeon1 = new Framework.objetosNeon.cContaCorrenteNeon(ContaCorrenteNeon1, true);
            //cContaCorrenteNeon1.somenteleitura = true;            
            if (cContaCorrenteNeon1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                MessageBox.Show("ok");
            else
                MessageBox.Show("cancelado");
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            FrameworkProc.objetosNeon.ContaCorrenteNeon ContaCorrenteNeon1 = new FrameworkProc.objetosNeon.ContaCorrenteNeon((int)spinEdit1.Value, FrameworkProc.objetosNeon.ContaCorrenteNeon.TipoChave.CCG);
            Framework.objetosNeon.cContaCorrenteNeon cContaCorrenteNeon1 = new Framework.objetosNeon.cContaCorrenteNeon(ContaCorrenteNeon1, false);
            //cContaCorrenteNeon1.somenteleitura = false;
            if (cContaCorrenteNeon1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                MessageBox.Show("ok");
            else
                MessageBox.Show("cancelado");
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Framework.objetosNeon.cContaCorrenteNeon cContaCorrenteNeon1 = new Framework.objetosNeon.cContaCorrenteNeon();
            cContaCorrenteNeon1.TravaBanco(237);
            cContaCorrenteNeon1.SetDefaults("Nome", new DocBacarios.CPFCNPJ(01124173000184));
            if (cContaCorrenteNeon1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                MessageBox.Show("ok");
                spinEdit1.Value = cContaCorrenteNeon1.contaCorrenteNeon1.CCG.Value;
            }
            else
                MessageBox.Show("cancelado");
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            SortedList<string, int> Testados = new SortedList<string, int>();
            DataTable Tabela = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela("SELECT CCT, CCT_BCO, CCTAgencia, CCTAgenciaDg, CCTConta, CCTContaDg, CCTTitulo FROM dbo.ContaCorrenTe where CCTAtiva = 1 AND (CCTAplicacao = 0)");
            foreach (DataRow row in Tabela.Rows)
            {
                int Banco = (int)row["CCT_BCO"];
                //if (Banco == 237)
                // if (Banco == 341)
                if (Banco == 104)
                    try
                    {
                        FrameworkProc.objetosNeon.ContaCorrenteNeon Conta = new FrameworkProc.objetosNeon.ContaCorrenteNeon((int)row["CCT"], FrameworkProc.objetosNeon.ContaCorrenteNeon.TipoChave.CCT);
                        bool resultado = Conta.validaConta();
                        if (!resultado)
                        {
                            memoEdit1.Text += string.Format("CCT = {0} Banco {1} {2} |{3}| / |{4}| {5}\r\n", row["CCT"], row["CCT_BCO"], row["CCTConta"], row["CCTContaDg"], resultado, row["CCTTitulo"]);
                            memoEdit1.Text += string.Format("ERRO {0}\r\n", Conta);
                        }
                        else
                        {
                            memoEdit1.Text += string.Format("OK {0}\r\n", Conta);
                            if (Testados.ContainsKey((string)row["CCTContaDg"]))
                                Testados[(string)row["CCTContaDg"]] = Testados[(string)row["CCTContaDg"]] + 1;
                            else
                                Testados.Add((string)row["CCTContaDg"], 1);
                        }
                        Application.DoEvents();
                    }
                    catch (Exception ex)
                    {
                        memoEdit1.Text += string.Format("CCT = {0} Banco {1} |{2}| |{3}| {4}\r\n", row["CCT"], row["CCT_BCO"], row["CCTConta"], row["CCTContaDg"], ex.Message);
                    }
            }
            memoEdit1.Text += "\r\n\r\n Testados: ";
            foreach (string tes in Testados.Keys)
                memoEdit1.Text += string.Format("{0} ({1}) ", tes, Testados[tes]);
            memoEdit1.Text += string.Format("\r\n\r\n----------------------------------------\r\n\r\n");

            foreach (DataRow row in Tabela.Rows)
            {
                int Banco = (int)row["CCT_BCO"];
                //if (Banco == 237)
                if (Banco == 341)
                    //if (Banco == 104)
                    try
                    {
                        FrameworkProc.objetosNeon.ContaCorrenteNeon Conta = new FrameworkProc.objetosNeon.ContaCorrenteNeon((int)row["CCT"], FrameworkProc.objetosNeon.ContaCorrenteNeon.TipoChave.CCT);
                        bool resultado = Conta.validaConta();
                        if (!resultado)
                            memoEdit1.Text += string.Format("CCT = {0} Banco {1} {2} |{3}| / |{4}| {5}\r\n", row["CCT"], row["CCT_BCO"], row["CCTConta"], row["CCTContaDg"], resultado, row["CCTTitulo"]);
                        else
                        {
                            if (Testados.ContainsKey((string)row["CCTContaDg"]))
                                Testados[(string)row["CCTContaDg"]] = Testados[(string)row["CCTContaDg"]] + 1;
                            else
                                Testados.Add((string)row["CCTContaDg"], 1);
                        }
                        Application.DoEvents();
                    }
                    catch (Exception ex)
                    {
                        memoEdit1.Text += string.Format("CCT = {0} Banco {1} |{2}| |{3}| {4}\r\n", row["CCT"], row["CCT_BCO"], row["CCTConta"], row["CCTContaDg"], ex.Message);
                    }
            }
            memoEdit1.Text += "\r\n\r\n Testados: ";
            foreach (string tes in Testados.Keys)
                memoEdit1.Text += string.Format("{0} ({1}) ", tes, Testados[tes]);
            memoEdit1.Text += string.Format("\r\n\r\n----------------------------------------\r\n\r\n");



            SortedList<int, int> TestadosAG = new SortedList<int, int>();
            foreach (DataRow row in Tabela.Rows)
            {
                int Banco = (int)row["CCT_BCO"];
                if (Banco == 237)
                {
                    //try
                    //{
                    FrameworkProc.objetosNeon.ContaCorrenteNeon Conta = new FrameworkProc.objetosNeon.ContaCorrenteNeon((int)row["CCT"], FrameworkProc.objetosNeon.ContaCorrenteNeon.TipoChave.CCT);
                    bool resultado = Conta.validaAgencia();
                    if (!resultado)
                        memoEdit1.Text += string.Format("CCT = {0} Banco {1} |{2}| / |{3}| {4}\r\n", row["CCT"], row["CCT_BCO"], row["CCTAgencia"], row["CCTAgenciaDg"], resultado, row["CCTTitulo"]);

                    else
                    {
                        if (TestadosAG.ContainsKey((int)row["CCTAgenciaDg"]))
                            TestadosAG[(int)row["CCTAgenciaDg"]] = TestadosAG[(int)row["CCTAgenciaDg"]] + 1;
                        else
                            TestadosAG.Add((int)row["CCTAgenciaDg"], 1);
                    }
                    Application.DoEvents();
                    //}
                    //catch (Exception ex)
                    // {
                    //     memoEdit1.Text += string.Format("CCT = {0} Banco {1} |{2}| |{3}| {4}\r\n", row["CCT"], row["CCT_BCO"], row["CCTAgencia"], row["CCTAgenciaDg"], ex.Message);
                    // }
                }
            }
            memoEdit1.Text += "\r\n\r\n Testados: ";
            foreach (int tes in TestadosAG.Keys)
                memoEdit1.Text += string.Format("{0} ({1}) ", tes, TestadosAG[tes]);
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            string Comando =
"SELECT        FRN, FRNTipo, FRNCnpj, FRNNome, FRNBancoCredito, FRNAgenciaCredito, FRNAgenciaDgCredito, FRNContaCredito, FRNContaDgCredito, FRNContaTipoCredito, FRN_CCG\r\n" +
"FROM            dbo.FORNECEDORES\r\n" +
"WHERE        (FRNContaCredito IS NOT NULL) and (FRNBancocredito = '001') order by FRNNome;";
            SortedList<string, int> Testados = new SortedList<string, int>();
            DataTable Tabela = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(Comando);
            FrameworkProc.objetosNeon.ContaCorrenteNeon Conta = null;
            foreach (DataRow row in Tabela.Rows)
            {
                int Banco = int.Parse((string)row["FRNBancoCredito"]);
                //if (Banco == 237)
                // if (Banco == 341)
                if (Banco == 1)
                    try
                    {
                        Conta = new FrameworkProc.objetosNeon.ContaCorrenteNeon((int)row["FRN"], FrameworkProc.objetosNeon.ContaCorrenteNeon.TipoChave.FRN);
                        bool resultado = Conta.validaConta();
                        if (!resultado)
                        {                            
                            memoEdit1.Text += string.Format("ERRO {0} {1} {2} {3}\r\n", row["FRNCnpj"], row["FRNAgenciaDgCredito"], row["FRNContaCredito"], row["FRNContaDgCredito"]);
                        }
                        else
                        {
                            if (Conta.CPF_CNPJ == null)
                                memoEdit1.Text += string.Format("OK {0} {1}\r\n",Conta, "SEM CNPJ");
                            else
                            {
                                memoEdit1.Text += string.Format("OK {0} {1}\r\n", Conta, Conta.CPF_CNPJ);
                                if (Testados.ContainsKey(Conta.NumeroDg))
                                    Testados[Conta.NumeroDg] = Testados[Conta.NumeroDg] + 1;
                                else
                                    Testados.Add(Conta.NumeroDg, 1);
                            }
                        }
                        Application.DoEvents();
                    }
                    catch (Exception ex)
                    {
                        memoEdit1.Text += string.Format("{0} {1}\r\n", Conta, ex.Message);
                    }
            }
            memoEdit1.Text += "\r\n\r\n Testados: ";
            foreach (string tes in Testados.Keys)
                memoEdit1.Text += string.Format("{0} ({1}) ", tes, Testados[tes]);
            memoEdit1.Text += string.Format("\r\n\r\n----------------------------------------\r\n\r\n");

        }
    }
}
