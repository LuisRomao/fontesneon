﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Collections.Generic;
using Abstratos;

namespace TesteFrameworkNeon
{
    public partial class FSerializar : Form
    {
        public FSerializar()
        {
            InitializeComponent();
        }

        private void simpleButton16_Click(object sender, EventArgs e)
        {
            try
            {

                Person oPerson = new Person();

                oPerson.FirstName = "NOME1";

                

                oPerson.Propriedade = "Propriedade";



                //XmlSerializer oSerialiser = new XmlSerializer(typeof(Person));

                Stream oStream = new FileStream(@"C:\lixo\xmlFile.xml", FileMode.Create);

                DataContractSerializer ser = new DataContractSerializer(typeof(Person));
                ser.WriteObject(oStream, oPerson);


                //oSerialiser.Serialize(oStream, oPerson);

                oStream.Close();

                MessageBox.Show("The job is done");

            }

            catch (ApplicationException caught)
            {
                MessageBox.Show(caught.Source);
            }
        }

        private void simpleButton17_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dialogo = new OpenFileDialog();
            if (dialogo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Stream oStream = new FileStream(dialogo.FileName, FileMode.Open);
                DataContractSerializer ser = new DataContractSerializer(typeof(Person));
                Person PRecuperado = (Person)ser.ReadObject(oStream);
                MessageBox.Show(PRecuperado.ToString());
                oStream.Close();
            }
        }

       

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            FrameworkProc.datasets.dCondominiosAtivos X = new FrameworkProc.datasets.dCondominiosAtivos();
            gridControl1.DataSource = X;
            gridControl1.DataMember = "CONDOMINIOS";
            X.CONDOMINIOSTableAdapter.FillByTodos(X.CONDOMINIOS);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            VirEmailNeon.EmailDiretoNeon X = VirEmailNeon.EmailDiretoNeon.EmalST;
        }
    }

    

    [DataContract(Name = "TesteComp", Namespace = "www.virweb.com.br")]
    public class TesteComp : Abstratos.ABS_Competencia
    {
        public TesteComp()
        { 
        }

        public override ABS_Competencia ABS_CloneCompet(int meses)
        {
            throw new NotImplementedException();
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }

    [DataContract(Name="TesteBalancete",Namespace="www.virweb.com.br")]
    public class Person
    {
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", FirstName, LastName, Propriedade);
        }

        private string _FirstName;

        [DataMember]
        private string LastName;

        private string _LNao;

        public string Propriedade;

        public int Numero = 4;

        public string LNao { get { return _LNao; } set { _LNao = value; } }

        [DataMember]
        public string FirstName
        {

            get { return _FirstName; }

            set { _FirstName = value; }

        }

        

        public Person()
        {
            LastName = "Sobrenome 1";
        }

        /*
        public Person(string FirstName, string LastName)
        {

            this.FirstName = FirstName;

            this.LastName = LastName;

        }*/

        //void ISerializable.GetObjectData(SerializationInfo oInfo, StreamingContext oContext)
        //{
            /*
            oInfo.AddValue("FirstName", this.FirstName);

            oInfo.AddValue("LastName", this.LastName);

            oInfo.AddValue("FirstName2", this.FirstName);

            oInfo.AddValue("Teste", "Valor Teste");

            oInfo.AddValue("Teste2", 2);

            //oInfo.AddValue("Propriedade", "TRAVADO");*/
        //}

    }

    
}
