using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VirExcepitionNeon;
using VirDB.Bancovirtual;

namespace TesteFrameworkNeon
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            BancoVirtual.Popular("QAS - SBC CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
            BancoVirtual.Popular("QAS - SA CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
            BancoVirtual.Popular("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEON  ;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");


            BancoVirtual.PopularFilial("SBC QAS", @"Data Source=cp.neonimoveis.com;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
            BancoVirtual.PopularFilial("SA QAS", @"Data Source=cp.neonimoveis.com;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");
            //BancoVirtual.PopularFilial("NEON SA (cuidado)", @"Data Source=cp.neonimoveis.com;Initial Catalog=NEONSA  ;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            
            VirDB.Bancovirtual.BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
            VirDB.Bancovirtual.BancoVirtual.PopularAcces("SBC LOCAL 2", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");            
            Login.fLogin NovoLg = new Login.fLogin();
            //Application.ThreadException += VirExceptionNeon.VirExceptionNeonSt.OnThreadException;
            if (NovoLg.ShowDialog() == DialogResult.OK)
                Application.Run(new Principal());
            else
                //Application.Run(new Form1());
                Application.Run(new FSerializar());
            
            
        }
    }
}