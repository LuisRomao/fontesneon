﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Framework.objetosNeon;

namespace TesteFrameworkNeon
{
    public partial class FTestaPLA : Form
    {
        public FTestaPLA()
        {
            InitializeComponent();
        }

        private Framework.datasets.dPLAnocontas DS;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DS = new Framework.datasets.dPLAnocontas();
            bindingSource1.DataSource = DS;
            Framework.datasets.dPLAnocontasTableAdapters.SubPLanoTableAdapter TA = new Framework.datasets.dPLAnocontasTableAdapters.SubPLanoTableAdapter();
            TA.Fill(DS.SubPLano);
        }
    }
}
