namespace TesteFrameworkNeon
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit3 = new DevExpress.XtraEditors.SpinEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.cCompet3 = new Framework.objetosNeon.cCompet();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.cCompet2 = new Framework.objetosNeon.cCompet();
            this.testeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dTestecomp = new TesteFrameworkNeon.dTestecomp();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.spinEdit6 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit5 = new DevExpress.XtraEditors.SpinEdit();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colc1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colc2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colc3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.spinEdit4 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.SAno = new DevExpress.XtraEditors.SpinEdit();
            this.Smes = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.cImportador1 = new Framework.Importador.cImportador();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTestecomp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Smes.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(49, 28);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(100, 28);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            327,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(49, 84);
            this.spinEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Size = new System.Drawing.Size(133, 22);
            this.spinEdit1.TabIndex = 1;
            // 
            // spinEdit2
            // 
            this.spinEdit2.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(49, 116);
            this.spinEdit2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit2.Size = new System.Drawing.Size(133, 22);
            this.spinEdit2.TabIndex = 2;
            // 
            // spinEdit3
            // 
            this.spinEdit3.EditValue = new decimal(new int[] {
            2007,
            0,
            0,
            0});
            this.spinEdit3.Location = new System.Drawing.Point(49, 148);
            this.spinEdit3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.spinEdit3.Name = "spinEdit3";
            this.spinEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit3.Size = new System.Drawing.Size(133, 22);
            this.spinEdit3.TabIndex = 3;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(215, 87);
            this.dateEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(133, 22);
            this.dateEdit1.TabIndex = 4;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(248, 119);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(100, 28);
            this.simpleButton2.TabIndex = 7;
            this.simpleButton2.Text = "simpleButton2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.Location = new System.Drawing.Point(389, 123);
            this.dateEdit2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit2.Size = new System.Drawing.Size(133, 22);
            this.dateEdit2.TabIndex = 8;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(1029, 180);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(100, 28);
            this.simpleButton3.TabIndex = 10;
            this.simpleButton3.Text = "GeraData";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(765, 28);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(100, 28);
            this.simpleButton4.TabIndex = 12;
            this.simpleButton4.Text = "simpleButton4";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(264, 32);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.Size = new System.Drawing.Size(411, 25);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 5;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = "Framework.Lookup.LookupCondominio_F - ";            
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1288, 784);
            this.xtraTabControl1.TabIndex = 13;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.simpleButton15);
            this.xtraTabPage1.Controls.Add(this.cCompet3);
            this.xtraTabPage1.Controls.Add(this.simpleButton14);
            this.xtraTabPage1.Controls.Add(this.simpleButton13);
            this.xtraTabPage1.Controls.Add(this.cCompet2);
            this.xtraTabPage1.Controls.Add(this.textBox2);
            this.xtraTabPage1.Controls.Add(this.simpleButton12);
            this.xtraTabPage1.Controls.Add(this.textBox1);
            this.xtraTabPage1.Controls.Add(this.spinEdit6);
            this.xtraTabPage1.Controls.Add(this.spinEdit5);
            this.xtraTabPage1.Controls.Add(this.bindingNavigator1);
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.cCompet1);
            this.xtraTabPage1.Controls.Add(this.button3);
            this.xtraTabPage1.Controls.Add(this.button2);
            this.xtraTabPage1.Controls.Add(this.button1);
            this.xtraTabPage1.Controls.Add(this.simpleButton11);
            this.xtraTabPage1.Controls.Add(this.simpleButton10);
            this.xtraTabPage1.Controls.Add(this.spinEdit4);
            this.xtraTabPage1.Controls.Add(this.simpleButton9);
            this.xtraTabPage1.Controls.Add(this.simpleButton8);
            this.xtraTabPage1.Controls.Add(this.simpleButton7);
            this.xtraTabPage1.Controls.Add(this.lookupBlocosAptos_F1);
            this.xtraTabPage1.Controls.Add(this.memoEdit1);
            this.xtraTabPage1.Controls.Add(this.SAno);
            this.xtraTabPage1.Controls.Add(this.Smes);
            this.xtraTabPage1.Controls.Add(this.simpleButton6);
            this.xtraTabPage1.Controls.Add(this.simpleButton5);
            this.xtraTabPage1.Controls.Add(this.simpleButton1);
            this.xtraTabPage1.Controls.Add(this.simpleButton4);
            this.xtraTabPage1.Controls.Add(this.spinEdit1);
            this.xtraTabPage1.Controls.Add(this.simpleButton3);
            this.xtraTabPage1.Controls.Add(this.dateEdit2);
            this.xtraTabPage1.Controls.Add(this.spinEdit2);
            this.xtraTabPage1.Controls.Add(this.simpleButton2);
            this.xtraTabPage1.Controls.Add(this.spinEdit3);
            this.xtraTabPage1.Controls.Add(this.dateEdit1);
            this.xtraTabPage1.Controls.Add(this.lookupCondominio_F1);
            this.xtraTabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1282, 753);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // simpleButton15
            // 
            this.simpleButton15.Location = new System.Drawing.Point(1069, 38);
            this.simpleButton15.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(100, 28);
            this.simpleButton15.TabIndex = 41;
            this.simpleButton15.Text = "simpleButton15";
            this.simpleButton15.Click += new System.EventHandler(this.simpleButton15_Click);
            // 
            // cCompet3
            // 
            this.cCompet3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet3.Appearance.Options.UseBackColor = true;
            this.cCompet3.CaixaAltaGeral = true;
            this.cCompet3.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet3.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet3.IsNull = false;
            this.cCompet3.Location = new System.Drawing.Point(1052, 276);
            this.cCompet3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cCompet3.Name = "cCompet3";
            this.cCompet3.PermiteNulo = false;
            this.cCompet3.ReadOnly = false;
            this.cCompet3.Size = new System.Drawing.Size(156, 30);
            this.cCompet3.somenteleitura = false;
            this.cCompet3.TabIndex = 40;
            this.cCompet3.Titulo = "Framework.objetosNeon.cCompet - ";
            // 
            // simpleButton14
            // 
            this.simpleButton14.Location = new System.Drawing.Point(264, 587);
            this.simpleButton14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(100, 28);
            this.simpleButton14.TabIndex = 39;
            this.simpleButton14.Text = "simpleButton14";
            this.simpleButton14.Click += new System.EventHandler(this.simpleButton14_Click);
            // 
            // simpleButton13
            // 
            this.simpleButton13.Location = new System.Drawing.Point(657, 127);
            this.simpleButton13.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(100, 28);
            this.simpleButton13.TabIndex = 38;
            this.simpleButton13.Text = "GuiaGPS";
            this.simpleButton13.Click += new System.EventHandler(this.simpleButton13_Click);
            // 
            // cCompet2
            // 
            this.cCompet2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet2.Appearance.Options.UseBackColor = true;
            this.cCompet2.CaixaAltaGeral = true;
            this.cCompet2.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.testeBindingSource, "c2", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompet2.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet2.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet2.IsNull = false;
            this.cCompet2.Location = new System.Drawing.Point(420, 638);
            this.cCompet2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cCompet2.Name = "cCompet2";
            this.cCompet2.PermiteNulo = false;
            this.cCompet2.ReadOnly = false;
            this.cCompet2.Size = new System.Drawing.Size(156, 30);
            this.cCompet2.somenteleitura = false;
            this.cCompet2.TabIndex = 37;
            this.cCompet2.Tag = "100";
            this.cCompet2.Titulo = "Framework.objetosNeon.cCompet - ";
            // 
            // testeBindingSource
            // 
            this.testeBindingSource.DataMember = "Teste";
            this.testeBindingSource.DataSource = this.dTestecomp;
            // 
            // dTestecomp
            // 
            this.dTestecomp.DataSetName = "dTestecomp";
            this.dTestecomp.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(264, 692);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(132, 22);
            this.textBox2.TabIndex = 36;
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(215, 518);
            this.simpleButton12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(100, 28);
            this.simpleButton12.TabIndex = 35;
            this.simpleButton12.Text = "simpleButton12";
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton12_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(101, 679);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 22);
            this.textBox1.TabIndex = 34;
            this.textBox1.Tag = "20";
            this.textBox1.Validating += new System.ComponentModel.CancelEventHandler(this.textBox1_Validating);
            // 
            // spinEdit6
            // 
            this.spinEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.testeBindingSource, "c2", true));
            this.spinEdit6.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit6.Location = new System.Drawing.Point(101, 634);
            this.spinEdit6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.spinEdit6.Name = "spinEdit6";
            this.spinEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit6.Size = new System.Drawing.Size(133, 22);
            this.spinEdit6.TabIndex = 33;
            // 
            // spinEdit5
            // 
            this.spinEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.testeBindingSource, "c1", true));
            this.spinEdit5.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit5.Location = new System.Drawing.Point(101, 583);
            this.spinEdit5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.spinEdit5.Name = "spinEdit5";
            this.spinEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit5.Size = new System.Drawing.Size(133, 22);
            this.spinEdit5.TabIndex = 32;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.BindingSource = this.testeBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(1282, 27);
            this.bindingNavigator1.TabIndex = 31;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 24);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(65, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.testeBindingSource;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl1.Location = new System.Drawing.Point(657, 470);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(533, 246);
            this.gridControl1.TabIndex = 29;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colc1,
            this.colc2,
            this.colc3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            // 
            // colc1
            // 
            this.colc1.FieldName = "c1";
            this.colc1.Name = "colc1";
            this.colc1.Visible = true;
            this.colc1.VisibleIndex = 0;
            // 
            // colc2
            // 
            this.colc2.FieldName = "c2";
            this.colc2.Name = "colc2";
            this.colc2.Visible = true;
            this.colc2.VisibleIndex = 1;
            // 
            // colc3
            // 
            this.colc3.FieldName = "c3";
            this.colc3.Name = "colc3";
            this.colc3.Visible = true;
            this.colc3.VisibleIndex = 2;
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.testeBindingSource, "c1", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(420, 580);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = true;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(156, 30);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 28;
            this.cCompet1.Tag = "100";
            this.cCompet1.Titulo = "Framework.objetosNeon.cCompet - ";
            this.cCompet1.Validating += new System.ComponentModel.CancelEventHandler(this.cCompet1_Validating);
            this.cCompet1.Validated += new System.EventHandler(this.cCompet1_Validated);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(49, 127);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(343, 28);
            this.button3.TabIndex = 27;
            this.button3.Text = "acertar CON";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(49, 80);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(343, 28);
            this.button2.TabIndex = 26;
            this.button2.Text = "abrir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(49, 28);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(343, 28);
            this.button1.TabIndex = 25;
            this.button1.Text = "Gerar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(911, 364);
            this.simpleButton11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(252, 28);
            this.simpleButton11.TabIndex = 24;
            this.simpleButton11.Text = "Auto teste imposto";
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(603, 364);
            this.simpleButton10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(100, 28);
            this.simpleButton10.TabIndex = 23;
            this.simpleButton10.Text = "simpleButton10";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // spinEdit4
            // 
            this.spinEdit4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit4.Location = new System.Drawing.Point(584, 401);
            this.spinEdit4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.spinEdit4.Name = "spinEdit4";
            this.spinEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit4.Size = new System.Drawing.Size(133, 22);
            this.spinEdit4.TabIndex = 22;
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(412, 470);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(100, 28);
            this.simpleButton9.TabIndex = 21;
            this.simpleButton9.Text = "A";
            this.simpleButton9.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(412, 434);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(100, 28);
            this.simpleButton8.TabIndex = 20;
            this.simpleButton8.Text = "B";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(412, 399);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(100, 28);
            this.simpleButton7.TabIndex = 19;
            this.simpleButton7.Text = "C";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.CON_selrow = null;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(419, 254);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(565, 57);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 18;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = "Framework.Lookup.LookupBlocosAptos_F - ";            
            this.lookupBlocosAptos_F1.alteradoBLOCO += new System.EventHandler(this.lookupBlocosAptos_F1_alteradoBLOCO);
            this.lookupBlocosAptos_F1.alteradoAPT += new System.EventHandler(this.lookupBlocosAptos_F1_alteradoAPT);
            this.lookupBlocosAptos_F1.alterado += new System.EventHandler(this.lookupBlocosAptos_F1_alterado_1);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(12, 116);
            this.memoEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Size = new System.Drawing.Size(572, 423);
            this.memoEdit1.TabIndex = 17;
            this.memoEdit1.UseOptimizedRendering = true;
            // 
            // SAno
            // 
            this.SAno.EditValue = new decimal(new int[] {
            2008,
            0,
            0,
            0});
            this.SAno.Location = new System.Drawing.Point(1029, 116);
            this.SAno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SAno.Name = "SAno";
            this.SAno.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SAno.Size = new System.Drawing.Size(133, 22);
            this.SAno.TabIndex = 16;
            // 
            // Smes
            // 
            this.Smes.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Smes.Location = new System.Drawing.Point(1029, 148);
            this.Smes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Smes.Name = "Smes";
            this.Smes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Smes.Size = new System.Drawing.Size(133, 22);
            this.Smes.TabIndex = 15;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(909, 39);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(100, 28);
            this.simpleButton6.TabIndex = 14;
            this.simpleButton6.Text = "Importar pop";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(909, 4);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(100, 28);
            this.simpleButton5.TabIndex = 13;
            this.simpleButton5.Text = "Importar";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.cImportador1);
            this.xtraTabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1282, 753);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // cImportador1
            // 
            this.cImportador1.CaixaAltaGeral = true;
            this.cImportador1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.cImportador1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cImportador1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cImportador1.Location = new System.Drawing.Point(0, 0);
            this.cImportador1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cImportador1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cImportador1.Name = "cImportador1";
            this.cImportador1.Size = new System.Drawing.Size(1282, 753);
            this.cImportador1.somenteleitura = false;
            this.cImportador1.TabIndex = 0;
            this.cImportador1.Titulo = "Framework.Importador.cImportador - ";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.memoEdit2);
            this.xtraTabPage3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1282, 753);
            this.xtraTabPage3.Text = "xtraTabPage3";
            // 
            // memoEdit2
            // 
            this.memoEdit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit2.Location = new System.Drawing.Point(0, 0);
            this.memoEdit2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit2.Properties.Appearance.Options.UseFont = true;
            this.memoEdit2.Properties.WordWrap = false;
            this.memoEdit2.Size = new System.Drawing.Size(1282, 753);
            this.memoEdit2.TabIndex = 18;
            this.memoEdit2.UseOptimizedRendering = true;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1282, 753);
            this.xtraTabPage4.Text = "Teste serializar";
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1288, 784);
            this.Controls.Add(this.xtraTabControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTestecomp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Smes.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraEditors.SpinEdit spinEdit3;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.DateEdit dateEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private Framework.Importador.cImportador cImportador1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SpinEdit SAno;
        private DevExpress.XtraEditors.SpinEdit Smes;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private Framework.Lookup.LookupBlocosAptos_F lookupBlocosAptos_F1;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SpinEdit spinEdit4;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.MemoEdit memoEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private Framework.objetosNeon.cCompet cCompet1;
        private System.Windows.Forms.BindingSource testeBindingSource;
        private dTestecomp dTestecomp;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colc1;
        private DevExpress.XtraGrid.Columns.GridColumn colc2;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private DevExpress.XtraEditors.SpinEdit spinEdit6;
        private DevExpress.XtraEditors.SpinEdit spinEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colc3;
        private System.Windows.Forms.TextBox textBox1;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private System.Windows.Forms.TextBox textBox2;
        private Framework.objetosNeon.cCompet cCompet2;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private Framework.objetosNeon.cCompet cCompet3;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private Framework.objetosNeon.ImagensPI imagensPI1;
    }
}

