using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TesteFrameworkNeon
{
    public partial class Principal : CompontesBasicos.FormPrincipalBase
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            FTestaPLA F = new FTestaPLA();
            F.ShowDialog();
        }
    }
}

