﻿namespace TesteFrameworkNeon
{
    partial class TesteLook
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.lookupCondominio_F2 = new Framework.Lookup.LookupCondominio_F();
            this.cCompet2 = new Framework.objetosNeon.cCompet();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.lookupGerente1 = new Framework.Lookup.LookupGerente();
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.lookupGerente2 = new Framework.Lookup.LookupGerente();
            this.lookupGerente3 = new Framework.Lookup.LookupGerente();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(383, 95);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Usa";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(383, 141);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 2;
            this.simpleButton2.Text = "limpa";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(21, 371);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(288, 23);
            this.simpleButton3.TabIndex = 5;
            this.simpleButton3.Text = "e-mail - enviar";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(21, 396);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(288, 23);
            this.simpleButton4.TabIndex = 6;
            this.simpleButton4.Text = "e-mail - enviar retidos";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(21, 425);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(288, 23);
            this.simpleButton5.TabIndex = 9;
            this.simpleButton5.Text = "e-mail - teste";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(325, 398);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Log de e-mail";
            this.checkEdit1.Size = new System.Drawing.Size(75, 19);
            this.checkEdit1.TabIndex = 10;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(424, 371);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(122, 23);
            this.simpleButton6.TabIndex = 11;
            this.simpleButton6.Text = "ler";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(424, 401);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(122, 23);
            this.simpleButton8.TabIndex = 13;
            this.simpleButton8.Text = "grava trans";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(424, 430);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(122, 23);
            this.simpleButton9.TabIndex = 14;
            this.simpleButton9.Text = "Commit";
            this.simpleButton9.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(21, 489);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(288, 23);
            this.simpleButton7.TabIndex = 15;
            this.simpleButton7.Text = "ligar thread email";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(700, 23);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(75, 23);
            this.simpleButton10.TabIndex = 17;
            this.simpleButton10.Text = "Troca EMP";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // lookupCondominio_F2
            // 
            this.lookupCondominio_F2.Autofill = true;
            this.lookupCondominio_F2.CaixaAltaGeral = true;
            this.lookupCondominio_F2.CON_sel = -1;
            this.lookupCondominio_F2.CON_selrow = null;
            this.lookupCondominio_F2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F2.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F2.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F2.LinhaMae_F = null;
            this.lookupCondominio_F2.Location = new System.Drawing.Point(364, 23);
            this.lookupCondominio_F2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F2.Name = "lookupCondominio_F2";
            this.lookupCondominio_F2.NivelCONOculto = 2;
            this.lookupCondominio_F2.Size = new System.Drawing.Size(308, 20);
            this.lookupCondominio_F2.somenteleitura = false;
            this.lookupCondominio_F2.TabIndex = 16;
            this.lookupCondominio_F2.TableAdapterPrincipal = null;
            this.lookupCondominio_F2.Titulo = null;
            // 
            // cCompet2
            // 
            this.cCompet2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet2.Appearance.Options.UseBackColor = true;
            this.cCompet2.CaixaAltaGeral = true;
            this.cCompet2.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet2.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet2.IsNull = false;
            this.cCompet2.Location = new System.Drawing.Point(399, 334);
            this.cCompet2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet2.Name = "cCompet2";
            this.cCompet2.PermiteNulo = false;
            this.cCompet2.ReadOnly = false;
            this.cCompet2.Size = new System.Drawing.Size(117, 24);
            this.cCompet2.somenteleitura = false;
            this.cCompet2.TabIndex = 8;
            this.cCompet2.Titulo = null;
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(399, 304);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 7;
            this.cCompet1.Titulo = null;
            // 
            // lookupGerente1
            // 
            this.lookupGerente1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente1.Appearance.Options.UseBackColor = true;
            this.lookupGerente1.CaixaAltaGeral = true;
            this.lookupGerente1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente1.Location = new System.Drawing.Point(64, 309);
            this.lookupGerente1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente1.Name = "lookupGerente1";
            this.lookupGerente1.Size = new System.Drawing.Size(290, 20);
            this.lookupGerente1.somenteleitura = false;
            this.lookupGerente1.TabIndex = 4;
            this.lookupGerente1.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.gerente;
            this.lookupGerente1.Titulo = null;
            this.lookupGerente1.USUSel = -1;
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.CON_selrow = null;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(35, 192);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.NivelCONOculto = 2;
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(424, 46);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 3;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = null;
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(35, 23);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(308, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 0;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(613, 274);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(75, 23);
            this.simpleButton11.TabIndex = 18;
            this.simpleButton11.Text = "simpleButton11";
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // lookupGerente2
            // 
            this.lookupGerente2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente2.Appearance.Options.UseBackColor = true;
            this.lookupGerente2.CaixaAltaGeral = true;
            this.lookupGerente2.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente2.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente2.Location = new System.Drawing.Point(503, 166);
            this.lookupGerente2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente2.Name = "lookupGerente2";
            this.lookupGerente2.Size = new System.Drawing.Size(290, 20);
            this.lookupGerente2.somenteleitura = false;
            this.lookupGerente2.TabIndex = 19;
            this.lookupGerente2.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.assistente;
            this.lookupGerente2.Titulo = null;
            this.lookupGerente2.USUSel = -1;
            // 
            // lookupGerente3
            // 
            this.lookupGerente3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente3.Appearance.Options.UseBackColor = true;
            this.lookupGerente3.CaixaAltaGeral = true;
            this.lookupGerente3.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente3.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente3.Location = new System.Drawing.Point(503, 205);
            this.lookupGerente3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente3.Name = "lookupGerente3";
            this.lookupGerente3.Size = new System.Drawing.Size(290, 20);
            this.lookupGerente3.somenteleitura = false;
            this.lookupGerente3.TabIndex = 20;
            this.lookupGerente3.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.gerente;
            this.lookupGerente3.Titulo = null;
            this.lookupGerente3.USUSel = -1;
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(812, 166);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(112, 23);
            this.simpleButton12.TabIndex = 21;
            this.simpleButton12.Text = "Gerente";
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton12_Click);
            // 
            // simpleButton13
            // 
            this.simpleButton13.Location = new System.Drawing.Point(812, 202);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(112, 23);
            this.simpleButton13.TabIndex = 22;
            this.simpleButton13.Text = "Assistente";
            this.simpleButton13.Click += new System.EventHandler(this.simpleButton13_Click);
            // 
            // TesteLook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton13);
            this.Controls.Add(this.simpleButton12);
            this.Controls.Add(this.lookupGerente3);
            this.Controls.Add(this.lookupGerente2);
            this.Controls.Add(this.simpleButton11);
            this.Controls.Add(this.simpleButton10);
            this.Controls.Add(this.lookupCondominio_F2);
            this.Controls.Add(this.simpleButton7);
            this.Controls.Add(this.simpleButton9);
            this.Controls.Add(this.simpleButton8);
            this.Controls.Add(this.simpleButton6);
            this.Controls.Add(this.checkEdit1);
            this.Controls.Add(this.simpleButton5);
            this.Controls.Add(this.cCompet2);
            this.Controls.Add(this.cCompet1);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.lookupGerente1);
            this.Controls.Add(this.lookupBlocosAptos_F1);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.lookupCondominio_F1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TesteLook";
            this.Size = new System.Drawing.Size(1143, 549);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private Framework.Lookup.LookupBlocosAptos_F lookupBlocosAptos_F1;
        private Framework.Lookup.LookupGerente lookupGerente1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private Framework.objetosNeon.cCompet cCompet1;
        private Framework.objetosNeon.cCompet cCompet2;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F2;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private Framework.Lookup.LookupGerente lookupGerente2;
        private Framework.Lookup.LookupGerente lookupGerente3;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
    }
}
