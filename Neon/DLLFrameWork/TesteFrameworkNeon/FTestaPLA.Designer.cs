﻿namespace TesteFrameworkNeon
{
    partial class FTestaPLA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMBA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBDCred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBDTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBDDetalhamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBDOrdem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBDMostraZerado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBDxPLADescricaoLivre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBDxPLAOrdem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBDxPLADescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBGTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBGOrdem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLADescricaoRes = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(31, 13);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.DataSource = this.bindingSource1;
            this.gridControl1.Location = new System.Drawing.Point(31, 65);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1012, 643);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "SubPLano";
            this.bindingSource1.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMBA,
            this.colPLA,
            this.colMBDCred,
            this.colMBDTitulo,
            this.colMBDDetalhamento,
            this.colMBDOrdem,
            this.colMBDMostraZerado,
            this.colMBDxPLADescricaoLivre,
            this.colMBDxPLAOrdem,
            this.colMBDxPLADescricao,
            this.colMBGTitulo,
            this.colMBGOrdem,
            this.colPLADescricaoRes});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colMBA
            // 
            this.colMBA.FieldName = "MBA";
            this.colMBA.Name = "colMBA";
            this.colMBA.OptionsColumn.ReadOnly = true;
            this.colMBA.Visible = true;
            this.colMBA.VisibleIndex = 0;
            // 
            // colPLA
            // 
            this.colPLA.FieldName = "PLA";
            this.colPLA.Name = "colPLA";
            this.colPLA.Visible = true;
            this.colPLA.VisibleIndex = 1;
            // 
            // colMBDCred
            // 
            this.colMBDCred.FieldName = "MBDCred";
            this.colMBDCred.Name = "colMBDCred";
            this.colMBDCred.Visible = true;
            this.colMBDCred.VisibleIndex = 2;
            // 
            // colMBDTitulo
            // 
            this.colMBDTitulo.FieldName = "MBDTitulo";
            this.colMBDTitulo.Name = "colMBDTitulo";
            this.colMBDTitulo.Visible = true;
            this.colMBDTitulo.VisibleIndex = 3;
            // 
            // colMBDDetalhamento
            // 
            this.colMBDDetalhamento.FieldName = "MBDDetalhamento";
            this.colMBDDetalhamento.Name = "colMBDDetalhamento";
            this.colMBDDetalhamento.Visible = true;
            this.colMBDDetalhamento.VisibleIndex = 4;
            // 
            // colMBDOrdem
            // 
            this.colMBDOrdem.FieldName = "MBDOrdem";
            this.colMBDOrdem.Name = "colMBDOrdem";
            this.colMBDOrdem.Visible = true;
            this.colMBDOrdem.VisibleIndex = 5;
            // 
            // colMBDMostraZerado
            // 
            this.colMBDMostraZerado.FieldName = "MBDMostraZerado";
            this.colMBDMostraZerado.Name = "colMBDMostraZerado";
            this.colMBDMostraZerado.Visible = true;
            this.colMBDMostraZerado.VisibleIndex = 6;
            // 
            // colMBDxPLADescricaoLivre
            // 
            this.colMBDxPLADescricaoLivre.FieldName = "MBDxPLADescricaoLivre";
            this.colMBDxPLADescricaoLivre.Name = "colMBDxPLADescricaoLivre";
            this.colMBDxPLADescricaoLivre.Visible = true;
            this.colMBDxPLADescricaoLivre.VisibleIndex = 7;
            // 
            // colMBDxPLAOrdem
            // 
            this.colMBDxPLAOrdem.FieldName = "MBDxPLAOrdem";
            this.colMBDxPLAOrdem.Name = "colMBDxPLAOrdem";
            this.colMBDxPLAOrdem.Visible = true;
            this.colMBDxPLAOrdem.VisibleIndex = 8;
            // 
            // colMBDxPLADescricao
            // 
            this.colMBDxPLADescricao.FieldName = "MBDxPLADescricao";
            this.colMBDxPLADescricao.Name = "colMBDxPLADescricao";
            this.colMBDxPLADescricao.Visible = true;
            this.colMBDxPLADescricao.VisibleIndex = 9;
            // 
            // colMBGTitulo
            // 
            this.colMBGTitulo.FieldName = "MBGTitulo";
            this.colMBGTitulo.Name = "colMBGTitulo";
            this.colMBGTitulo.Visible = true;
            this.colMBGTitulo.VisibleIndex = 10;
            // 
            // colMBGOrdem
            // 
            this.colMBGOrdem.FieldName = "MBGOrdem";
            this.colMBGOrdem.Name = "colMBGOrdem";
            this.colMBGOrdem.Visible = true;
            this.colMBGOrdem.VisibleIndex = 11;
            // 
            // colPLADescricaoRes
            // 
            this.colPLADescricaoRes.FieldName = "PLADescricaoRes";
            this.colPLADescricaoRes.Name = "colPLADescricaoRes";
            this.colPLADescricaoRes.Visible = true;
            this.colPLADescricaoRes.VisibleIndex = 12;
            // 
            // FTestaPLA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 747);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.simpleButton1);
            this.Name = "FTestaPLA";
            this.Text = "FTestaPLA";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn colMBA;
        private DevExpress.XtraGrid.Columns.GridColumn colPLA;
        private DevExpress.XtraGrid.Columns.GridColumn colMBDCred;
        private DevExpress.XtraGrid.Columns.GridColumn colMBDTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colMBDDetalhamento;
        private DevExpress.XtraGrid.Columns.GridColumn colMBDOrdem;
        private DevExpress.XtraGrid.Columns.GridColumn colMBDMostraZerado;
        private DevExpress.XtraGrid.Columns.GridColumn colMBDxPLADescricaoLivre;
        private DevExpress.XtraGrid.Columns.GridColumn colMBDxPLAOrdem;
        private DevExpress.XtraGrid.Columns.GridColumn colMBDxPLADescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colMBGTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colMBGOrdem;
        private DevExpress.XtraGrid.Columns.GridColumn colPLADescricaoRes;
    }
}