using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Framework.objetosNeon;
using System.Runtime.Serialization;
using System.IO;
using System.Xml.Serialization;
using dllImpostoNeonProc;
using VirEnumeracoes;

namespace TesteFrameworkNeon
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Competencia C1 = new Competencia(1, 2000);
            MessageBox.Show("Original : " + C1.ToString());
            C1++;
            MessageBox.Show("Apos ++ : " + C1.ToString());
            MessageBox.Show("Antes : " + C1.ToString());
            MessageBox.Show("C1++ : " + (C1++).ToString());
            MessageBox.Show("Depois : " + C1.ToString());

            MessageBox.Show("Antes : " + C1.ToString());
            MessageBox.Show("++C1 : " + (++C1).ToString());
            MessageBox.Show("Depois : " + C1.ToString());

            MessageBox.Show("Antes : " + C1.ToString());
            MessageBox.Show("C1++ : " + (C1++).ToString());
            MessageBox.Show("Depois : " + C1.ToString());

            int A = 1;
            MessageBox.Show("Antes : " + A.ToString());
            MessageBox.Show("++A : " + (++A).ToString());
            MessageBox.Show("Depois : " + A.ToString());

            MessageBox.Show("Antes : " + A.ToString());
            MessageBox.Show("A++ : " + (A++).ToString());
            MessageBox.Show("Depois : " + A.ToString());

        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            /*
            memoEdit1.Text = "";
            string busca = "SELECT CONDOMINIOS.CON,APARTAMENTOS.APT,CONDOMINIOS.CONCodigo,BLOCOS.BLOCodigo, APARTAMENTOS.APTNumero FROM         CONDOMINIOS INNER JOIN  BLOCOS ON CONDOMINIOS.CON = BLOCOS.BLO_CON INNER JOIN  APARTAMENTOS ON BLOCOS.BLO = APARTAMENTOS.APT_BLO WHERE     (APARTAMENTOS.APTDiaDiferente = 1) AND (CONDOMINIOS.CONStatus = 1) AND (CONDOMINIOS.CON_EMP = 3) ORDER BY CONDOMINIOS.CONCodigo, BLOCOS.BLOCodigo, APARTAMENTOS.APTNumero";
            DataTable Diferentes = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(busca);
            foreach (DataRow row in Diferentes.Rows)
            {
                Competencia Comp = new Competencia((int)Smes.Value, (int)SAno.Value, (int)row["CON"]);
                DateTime D1 = Comp.VencimentoPadrao;
                DateTime D2 = Comp.DataVencimenot((int)row["APT"]);
                TimeSpan delsta = D2 - D1;

                memoEdit1.Text += row["CONCodigo"].ToString() + (char)9 + "'" + row["BLOCodigo"] + (char)9 + "'" + row["APTNumero"] + (char)9 + " Padrao: " + (char)9 + D1.ToString("dd/MM/yyyy") + (char)9 + " -> " + (char)9 + D2.ToString("dd/MM/yyyy") + (char)9 + "Dias:" + (char)9 + delsta.Days + "\r\n";
            };
            */

            

        }

        private void lookupBlocosAptos_F1_alterado(object sender, EventArgs e)
        {
            MessageBox.Show("alterado");
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            cImportador1.SetaCON(327);
            cImportador1.ClearColunaImp();
            cImportador1.AddColunaImp("Leitura", "Leitura do G�s", typeof(decimal), 10.5, 0.5);
            cImportador1.AddColunaImp("Nome", "Nome", typeof(string), 20, 1);
            cImportador1.AddColunaImp("Int", "Inteiro", typeof(int), 10, 1);
            cImportador1.Carrega();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            //Framework.Importador.cImportador Novo = new Framework.Importador.cImportador(327);
            //Novo.AddColunaImp("Leitura", "Leitura do G�s", typeof(decimal), 10.5, 0.5);
            //Novo.AddColunaImp("Nome", "Nome", typeof(string), 20, 1);
            //Novo.AddColunaImp("Int", "Inteiro", typeof(int), 10, 1);
            //Novo.Carrega();
            //if (Novo.ShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            //{
            //    MessageBox.Show("OK");
            //};
            //Framework.Importador.cImportador.ShowModuloSTA(CompontesBasicos.EstadosDosComponentes.PopUp, Novo);             
        }

        private void lookupBlocosAptos_F1_alterado_1(object sender, EventArgs e)
        {
            memoEdit1.Text += "C   " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
        }

        private void lookupBlocosAptos_F1_alteradoAPT(object sender, EventArgs e)
        {
            memoEdit1.Text += "  A " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
        }

        private void lookupBlocosAptos_F1_alteradoBLOCO(object sender, EventArgs e)
        {
            memoEdit1.Text += " B  " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            //C
            int i = Convert.ToInt32(spinEdit4.EditValue);
            memoEdit1.Text += "T1 " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
            lookupBlocosAptos_F1.CON_sel = i;
            memoEdit1.Text += "T2 " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            //B
            int i = Convert.ToInt32(spinEdit4.EditValue);
            memoEdit1.Text += "T1 " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
            lookupBlocosAptos_F1.BLO_Sel = i;
            memoEdit1.Text += "T2 " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            //A
            int i = Convert.ToInt32(spinEdit4.EditValue);
            memoEdit1.Text += "T1 " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
            lookupBlocosAptos_F1.APT_Sel = i;
            memoEdit1.Text += "T2 " + lookupBlocosAptos_F1.CON_sel.ToString() + " " + lookupBlocosAptos_F1.BLO_Sel.ToString() + " " + lookupBlocosAptos_F1.APT_Sel.ToString() + "\r\n";
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            /*
            int ii = 5;
            int j = ii++;
            memoEdit2.Text += "Ap�s o ii++ ii="+ii.ToString()+" j="+j.ToString()+"\r\n";

            ii = 5;
            j = ++ii;

            memoEdit2.Text += "Ap�s o ++ii ii=" + ii.ToString() + " j=" + j.ToString() + "\r\n";

            Competencia Compi = new Competencia(1, 2000);
            Competencia Compj = Compi++;
            memoEdit2.Text += "Ap�s o Compi++ Compi=" + Compi.ToString() + " Compj=" + Compj.ToString() + "\r\n";

            Compi = new Competencia(1, 2000);
            Compj = ++Compi;
            memoEdit2.Text += "Ap�s o ++Compi Compi=" + Compi.ToString() + " Compj=" + Compj.ToString() + "\r\n";

            malote Malotei = new malote(malote.TiposMalote.SegundaManha, DateTime.Today);
            malote Malotej = Malotei++;

            memoEdit2.Text += "Ap�s o Malotei++ Malotei=" + Malotei.ToString() + " Malotej=" + Malotej.ToString() + "\r\n";

            Malotei = new malote(malote.TiposMalote.SegundaManha, DateTime.Today);
            Malotej = ++Malotei;

            memoEdit2.Text += "Ap�s o ++Malotei Malotei=" + Malotei.ToString() + " Malotej=" + Malotej.ToString() + "\r\n";

            //return;
            string titulo = "";
            string sm = "";
            string st = "";
            string tm = "";
            string tt = "";
            string smi = "";
            string sti = "";
            string tmi = "";
            string tti = "";
            string q = "";
            string q15p = "";
            //string  = "";
            //string tti = "";
            //string tti = "";
            string Formato = "dd/MM dddd";
            memoEdit2.Text += "\r\nTESTE DE MALOTE \r\n\r\n";
            DateTime Dia = new DateTime(2008,8,1,13,30,0);
            int Largura = 35;
            for (int i = 0; i < 8; i++) {
                titulo += Dia.ToString(Formato).PadRight(Largura);
                sm += malote.MalotePara(malote.TiposMalote.SegundaManha, malote.SentidoMalote.volta, Dia, 0).ToString().PadRight(Largura); ;
                st += malote.MalotePara(malote.TiposMalote.SegundaTarde, malote.SentidoMalote.volta, Dia, 0).ToString().PadRight(Largura);
                tm += malote.MalotePara(malote.TiposMalote.TercaManha, malote.SentidoMalote.volta, Dia, 0).ToString().PadRight(Largura);
                tt += malote.MalotePara(malote.TiposMalote.TercaTarde, malote.SentidoMalote.volta, Dia, 0).ToString().PadRight(Largura);
                smi += malote.MalotePara(malote.TiposMalote.SegundaManha, malote.SentidoMalote.ida, Dia, 0).ToString().PadRight(Largura);
                sti += malote.MalotePara(malote.TiposMalote.SegundaTarde, malote.SentidoMalote.ida, Dia, 0).ToString().PadRight(Largura);
                tmi += malote.MalotePara(malote.TiposMalote.TercaManha, malote.SentidoMalote.ida, Dia, 0).ToString().PadRight(Largura);
                tti += malote.MalotePara(malote.TiposMalote.TercaTarde, malote.SentidoMalote.ida, Dia, 0).ToString().PadRight(Largura);
                Dia = Dia.AddDays(1);
            };
            memoEdit2.Text += "         " + titulo + "\r\n--------------VOLTA(13:30 do dia)\r\n";
            memoEdit2.Text += "Se/qui M " + sm + "\r\n";
            memoEdit2.Text += "Se/qui T " + st + "\r\n";
            memoEdit2.Text += "Te/Sex M " + tm + "\r\n";
            memoEdit2.Text += "Te/Sex T " + tt + "\r\n";

            memoEdit2.Text += "\r\n\r\n\r\n         " + titulo + "\r\n--------------IDA\r\n";
            memoEdit2.Text += "Se/qui M " + smi + "\r\n";
            memoEdit2.Text += "Se/qui T " + sti + "\r\n";
            memoEdit2.Text += "Te/Sex M " + tmi + "\r\n";
            memoEdit2.Text += "Te/Sex T " + tti + "\r\n";
            */
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = dllImpostoNeon.ImpostoNeon.AutoTesteNeon();
            dllImpostoNeon.ImpostoNeon Imp = new dllImpostoNeon.ImpostoNeon(TipoImposto.IR);
            Imp.DataPagamento = new DateTime(2010, 7, 28);
            Imp.ValorBase = 1000;
            //Imp.Imprime(dllImpostoNeon.ImpostoNeon.TipoImpresso.Termica,"Nome Teste","(11) 32225277","01.000.000/0001-84","NF:2000 NF:20001");
            //Imp = new dllImpostoNeon.ImpostoNeon(dllImpostos.TipoImposto.PIS_COFINS_CSLL);
            //Imp.DataPagamento = new DateTime(2010, 7, 28);
            //Imp.ValorBase = 1000;
            //Imp.Imprime(dllImpostoNeon.ImpostoNeon.TipoImpresso.Termica, "Nome Teste", "(11) 32225277", "01.000.000/0001-84", "NF:2000 NF:20001 xxx NF:2000 NF:20001 xxx NF:2000 NF:20001 xxx NF:2000 NF:20001 xxx NF:2000 NF:20001 xxx ");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Gerado;
            Gerado = dGPS.dGPSSt.GerarGPS(TipoImposto.INSS);
            if (Gerado != "")
                MessageBox.Show("Gerou: " + Gerado);
            //Gerado = dllImpostoNeon.dGPS.dGPSSt.GerarDARF();
            //if (Gerado != "")
            //    MessageBox.Show("Gerou: " + Gerado);


            /*
            System.IO.StreamReader A1 = new System.IO.StreamReader(Gerado);
            System.IO.StreamReader A2 = new System.IO.StreamReader(@"\\neon01\Folha\Virtual\GPS\abr09\GPS_20090413164200.rem");
            string linha1 = A1.ReadLine();
            string linha2 = A2.ReadLine();
            int i = 1;
            while (linha1 != null) {
                if (linha1.Substring(1, 301) != linha2.Substring(1, 301))
                {
                    MessageBox.Show("linha " + i.ToString() + "\r\n" + linha1.Substring(1, 301) + "\r\n" + linha2.Substring(1, 301));
                    break;
                };
                if (linha1.Substring(312) != linha2.Substring(312))
                {
                    MessageBox.Show("2 linha " + i.ToString() + "\r\n" + linha1.Substring(312) + "\r\n" + linha2.Substring(312));
                    break;
                }
                linha1 = A1.ReadLine();
                linha2 = A2.ReadLine();
                i++;
            }
            */
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dllImpostoNeon.cGPS novo = new dllImpostoNeon.cGPS();
            novo.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
        }

        private void button3_Click(object sender, EventArgs e)
        {

            dGPS.dGPSSt.GPSTableAdapter.Fill(dGPS.dGPSSt.GPS);
            foreach (dGPS.GPSRow row in dGPS.dGPSSt.GPS)
            {
                int CON = 0;
                DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(row.GPSCNPJ);
                if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select CON from Condominios where conCNPJ = @P1", out CON, cnpj.ToString()))
                    if (CON != 0)
                    {

                        row.GPS_CON = CON;
                        dGPS.dGPSSt.GPSTableAdapter.Update(row);
                        row.AcceptChanges();
                    }
            };

        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            cCompet1.IsNull = !cCompet1.IsNull;

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void cCompet1_Validated(object sender, EventArgs e)
        {


        }

        private void cCompet1_Validating(object sender, CancelEventArgs e)
        {

        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {

        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            dllImpostoNeon.ImpostoNeon IGPS = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSS);
            IGPS.DataNota = IGPS.DataPagamento = DateTime.Today;
            IGPS.ValorBase = 100;
            //IGPS.Imprime(dllImpostoNeon.ImpostoNeon.TipoImpresso.Termica, "Condomino XXX", "3222 7766", "01.122.111/0001-11");
        }

        private void simpleButton14_Click(object sender, EventArgs e)
        {
            cCompet nova = new cCompet();
            nova.Parent = this;
        }

        private void simpleButton15_Click(object sender, EventArgs e)
        {
            Competencia comp = new Competencia();
            MessageBox.Show(comp.CalculaVencimento().ToString());
        }

        private void simpleButton16_Click(object sender, EventArgs e)
        {
            

        }

    }



    



}