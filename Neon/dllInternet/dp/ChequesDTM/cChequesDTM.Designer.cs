namespace DP.ChequesDTM
{
    partial class cChequesDTM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cChequesDTM));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.BotAtCond = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.edtArquivo = new DevExpress.XtraEditors.ButtonEdit();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.TabPageCheques = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.dChequesDTMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dChequesDTM = new DP.ChequesDTM.dChequesDTM();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCHEFavorecido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpCONCodigo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bindingSourceCondominiosAtivos = new System.Windows.Forms.BindingSource(this.components);
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpCONNome = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colCHEIdaData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHERetornoData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumeroNota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONProcuracao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHE_BCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.LaConta = new System.Windows.Forms.Label();
            this.lookUpFOR1 = new DevExpress.XtraEditors.LookUpEdit();
            this.fornecedoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpFOR2 = new DevExpress.XtraEditors.LookUpEdit();
            this.ComboPAGTipoDefault = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.chAgrupa = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.SubPLanoPLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textEditDescritivo = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEditPLA1 = new DevExpress.XtraEditors.LookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpEditPLA2 = new DevExpress.XtraEditors.LookUpEdit();
            this.BotaoGerar = new DevExpress.XtraEditors.SimpleButton();
            this.BotPadrao = new DevExpress.XtraEditors.SimpleButton();
            this.PadraoValor = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.PadraoVenc = new DevExpress.XtraEditors.DateEdit();
            this.PadraoFav = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TabPageImportados = new DevExpress.XtraTab.XtraTabPage();
            label4 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtArquivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.TabPageCheques.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dChequesDTMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dChequesDTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpCONCodigo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCondominiosAtivos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpCONNome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboPAGTipoDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAgrupa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubPLanoPLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescritivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PadraoValor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PadraoVenc.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PadraoVenc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PadraoFav.Properties)).BeginInit();
            this.TabPageImportados.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(221, 8);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(87, 13);
            label4.TabIndex = 44;
            label4.Text = "F. pagamento:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(222, 50);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(84, 13);
            label2.TabIndex = 47;
            label2.Text = "Compet�ncia:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.BotAtCond);
            this.groupControl1.Controls.Add(this.simpleButton5);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.simpleButton3);
            this.groupControl1.Controls.Add(this.edtArquivo);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "The Asphalt World";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1407, 94);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = ":: Cadastro em lote ::";
            // 
            // BotAtCond
            // 
            this.BotAtCond.Enabled = false;
            this.BotAtCond.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BotAtCond.ImageOptions.Image")));
            this.BotAtCond.Location = new System.Drawing.Point(387, 23);
            this.BotAtCond.Name = "BotAtCond";
            this.BotAtCond.Size = new System.Drawing.Size(168, 66);
            this.BotAtCond.TabIndex = 10;
            this.BotAtCond.Text = "Atualizar condom�nios";
            this.BotAtCond.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.Location = new System.Drawing.Point(251, 23);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(130, 66);
            this.simpleButton5.TabIndex = 9;
            this.simpleButton5.Text = "Limpar Lista";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumBlue;
            this.label1.Location = new System.Drawing.Point(640, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome do Arquivo (XML)";
            this.label1.Visible = false;
            // 
            // simpleButton3
            // 
            this.simpleButton3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.ImageOptions.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(3, 23);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(242, 66);
            this.simpleButton3.TabIndex = 8;
            this.simpleButton3.Text = "Um cheque para cada condom�nio";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // edtArquivo
            // 
            this.edtArquivo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtArquivo.Location = new System.Drawing.Point(643, 40);
            this.edtArquivo.Name = "edtArquivo";
            this.edtArquivo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.edtArquivo.Size = new System.Drawing.Size(810, 20);
            this.edtArquivo.TabIndex = 1;
            this.edtArquivo.ToolTipController = this.ToolTipController_F;
            this.edtArquivo.Visible = false;
            this.edtArquivo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.edtArquivo_ButtonClick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "xml";
            this.openFileDialog.Filter = "XML|*.xml";
            this.openFileDialog.Title = "Selecione um arquivo";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1513, 368);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 94);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.TabPageCheques;
            this.xtraTabControl1.Size = new System.Drawing.Size(1407, 696);
            this.xtraTabControl1.TabIndex = 5;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TabPageImportados,
            this.TabPageCheques});
            // 
            // TabPageCheques
            // 
            this.TabPageCheques.Controls.Add(this.gridControl2);
            this.TabPageCheques.Controls.Add(this.panelControl1);
            this.TabPageCheques.Name = "TabPageCheques";
            this.TabPageCheques.Size = new System.Drawing.Size(1401, 668);
            this.TabPageCheques.Text = "Cheques para gerar";
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "NovosCheques";
            this.gridControl2.DataSource = this.dChequesDTMBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 216);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpCONCodigo,
            this.repositoryItemLookUpCONNome,
            this.repositoryItemImageComboBox1});
            this.gridControl2.Size = new System.Drawing.Size(1401, 452);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // dChequesDTMBindingSource
            // 
            this.dChequesDTMBindingSource.DataSource = this.dChequesDTM;
            this.dChequesDTMBindingSource.Position = 0;
            // 
            // dChequesDTM
            // 
            this.dChequesDTM.DataSetName = "dChequesDTM";
            this.dChequesDTM.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gainsboro;
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseBorderColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.gridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCHEFavorecido,
            this.colCHEValor,
            this.colCHEVencimento,
            this.colCONCodigo,
            this.colCONNome,
            this.colCHEIdaData,
            this.colCHERetornoData,
            this.colNumeroNota,
            this.colErro,
            this.colCONProcuracao,
            this.colCHE_BCO,
            this.colPAGTipo});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.NewItemRowText = "NOVO";
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView2_RowCellStyle);
            this.gridView2.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView2_RowUpdated);
            // 
            // colCHEFavorecido
            // 
            this.colCHEFavorecido.Caption = "Favorecido";
            this.colCHEFavorecido.FieldName = "CHEFavorecido";
            this.colCHEFavorecido.Name = "colCHEFavorecido";
            this.colCHEFavorecido.Visible = true;
            this.colCHEFavorecido.VisibleIndex = 4;
            this.colCHEFavorecido.Width = 420;
            // 
            // colCHEValor
            // 
            this.colCHEValor.Caption = "Valor";
            this.colCHEValor.DisplayFormat.FormatString = "n2";
            this.colCHEValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCHEValor.FieldName = "CHEValor";
            this.colCHEValor.Name = "colCHEValor";
            this.colCHEValor.OptionsColumn.FixedWidth = true;
            this.colCHEValor.Visible = true;
            this.colCHEValor.VisibleIndex = 5;
            this.colCHEValor.Width = 112;
            // 
            // colCHEVencimento
            // 
            this.colCHEVencimento.Caption = "Vencimento";
            this.colCHEVencimento.FieldName = "CHEVencimento";
            this.colCHEVencimento.Name = "colCHEVencimento";
            this.colCHEVencimento.OptionsColumn.FixedWidth = true;
            this.colCHEVencimento.Visible = true;
            this.colCHEVencimento.VisibleIndex = 8;
            this.colCHEVencimento.Width = 92;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.ColumnEdit = this.repositoryItemLookUpCONCodigo;
            this.colCONCodigo.FieldName = "CON";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.FixedWidth = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            this.colCONCodigo.Width = 72;
            // 
            // repositoryItemLookUpCONCodigo
            // 
            this.repositoryItemLookUpCONCodigo.AutoHeight = false;
            this.repositoryItemLookUpCONCodigo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpCONCodigo.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CON Codigo", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CON Nome", 62, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpCONCodigo.DataSource = this.bindingSourceCondominiosAtivos;
            this.repositoryItemLookUpCONCodigo.DisplayMember = "CONCodigo";
            this.repositoryItemLookUpCONCodigo.Name = "repositoryItemLookUpCONCodigo";
            this.repositoryItemLookUpCONCodigo.NullText = " --";
            this.repositoryItemLookUpCONCodigo.ShowHeader = false;
            this.repositoryItemLookUpCONCodigo.ValueMember = "CON";
            // 
            // bindingSourceCondominiosAtivos
            // 
            this.bindingSourceCondominiosAtivos.DataMember = "CONDOMINIOS";
            this.bindingSourceCondominiosAtivos.DataSource = typeof(FrameworkProc.datasets.dCondominiosAtivos);
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Condom�nio";
            this.colCONNome.ColumnEdit = this.repositoryItemLookUpCONNome;
            this.colCONNome.FieldName = "CON";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.FixedWidth = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 1;
            this.colCONNome.Width = 235;
            // 
            // repositoryItemLookUpCONNome
            // 
            this.repositoryItemLookUpCONNome.AutoHeight = false;
            this.repositoryItemLookUpCONNome.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpCONNome.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CON Nome", 62, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpCONNome.DataSource = this.bindingSourceCondominiosAtivos;
            this.repositoryItemLookUpCONNome.DisplayMember = "CONNome";
            this.repositoryItemLookUpCONNome.Name = "repositoryItemLookUpCONNome";
            this.repositoryItemLookUpCONNome.NullText = " --";
            this.repositoryItemLookUpCONNome.ShowHeader = false;
            this.repositoryItemLookUpCONNome.ValueMember = "CON";
            // 
            // colCHEIdaData
            // 
            this.colCHEIdaData.Caption = "Ida";
            this.colCHEIdaData.FieldName = "CHEIdaData";
            this.colCHEIdaData.Name = "colCHEIdaData";
            this.colCHEIdaData.OptionsColumn.AllowEdit = false;
            this.colCHEIdaData.OptionsColumn.FixedWidth = true;
            this.colCHEIdaData.Visible = true;
            this.colCHEIdaData.VisibleIndex = 6;
            // 
            // colCHERetornoData
            // 
            this.colCHERetornoData.Caption = "Retorno";
            this.colCHERetornoData.FieldName = "CHERetornoData";
            this.colCHERetornoData.Name = "colCHERetornoData";
            this.colCHERetornoData.OptionsColumn.AllowEdit = false;
            this.colCHERetornoData.OptionsColumn.FixedWidth = true;
            this.colCHERetornoData.Visible = true;
            this.colCHERetornoData.VisibleIndex = 7;
            // 
            // colNumeroNota
            // 
            this.colNumeroNota.Caption = "Nota";
            this.colNumeroNota.FieldName = "NumeroNota";
            this.colNumeroNota.Name = "colNumeroNota";
            this.colNumeroNota.OptionsColumn.FixedWidth = true;
            this.colNumeroNota.Visible = true;
            this.colNumeroNota.VisibleIndex = 3;
            // 
            // colErro
            // 
            this.colErro.FieldName = "Erro";
            this.colErro.Name = "colErro";
            this.colErro.Visible = true;
            this.colErro.VisibleIndex = 9;
            this.colErro.Width = 94;
            // 
            // colCONProcuracao
            // 
            this.colCONProcuracao.FieldName = "CONProcuracao";
            this.colCONProcuracao.Name = "colCONProcuracao";
            this.colCONProcuracao.Width = 108;
            // 
            // colCHE_BCO
            // 
            this.colCHE_BCO.FieldName = "CHE_BCO";
            this.colCHE_BCO.Name = "colCHE_BCO";
            this.colCHE_BCO.Width = 113;
            // 
            // colPAGTipo
            // 
            this.colPAGTipo.Caption = "Tipo Pagamento";
            this.colPAGTipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colPAGTipo.FieldName = "PAGTipo";
            this.colPAGTipo.Name = "colPAGTipo";
            this.colPAGTipo.Visible = true;
            this.colPAGTipo.VisibleIndex = 2;
            this.colPAGTipo.Width = 151;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(label2);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(label4);
            this.panelControl1.Controls.Add(this.ComboPAGTipoDefault);
            this.panelControl1.Controls.Add(this.chAgrupa);
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.BotaoGerar);
            this.panelControl1.Controls.Add(this.BotPadrao);
            this.panelControl1.Controls.Add(this.PadraoValor);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.PadraoVenc);
            this.panelControl1.Controls.Add(this.PadraoFav);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1401, 216);
            this.panelControl1.TabIndex = 1;
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(314, 43);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 46;
            this.cCompet1.TabStop = false;
            this.cCompet1.Titulo = "Framework.objetosNeon.cCompet - Framework.objetosNeon.cCompet - ";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.LaConta);
            this.groupControl2.Controls.Add(this.lookUpFOR1);
            this.groupControl2.Controls.Add(this.lookUpFOR2);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl2.Location = new System.Drawing.Point(2, 90);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1397, 51);
            this.groupControl2.TabIndex = 45;
            this.groupControl2.Text = "FORNECEDOR";
            // 
            // LaConta
            // 
            this.LaConta.AutoSize = true;
            this.LaConta.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LaConta.ForeColor = System.Drawing.Color.MediumBlue;
            this.LaConta.Location = new System.Drawing.Point(708, 26);
            this.LaConta.Name = "LaConta";
            this.LaConta.Size = new System.Drawing.Size(40, 13);
            this.LaConta.TabIndex = 31;
            this.LaConta.Text = "Conta";
            this.LaConta.Visible = false;
            // 
            // lookUpFOR1
            // 
            this.lookUpFOR1.Location = new System.Drawing.Point(5, 23);
            this.lookUpFOR1.Name = "lookUpFOR1";
            this.lookUpFOR1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.lookUpFOR1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "CNPJ", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome")});
            this.lookUpFOR1.Properties.DataSource = this.fornecedoresBindingSource;
            this.lookUpFOR1.Properties.DisplayMember = "FRNCnpj";
            this.lookUpFOR1.Properties.NullText = "CNPJ";
            this.lookUpFOR1.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.OnlyInPopup;
            this.lookUpFOR1.Properties.ValueMember = "FRN";
            this.lookUpFOR1.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpFOR2_ButtonClick);
            this.lookUpFOR1.Size = new System.Drawing.Size(164, 20);
            this.lookUpFOR1.TabIndex = 30;
            this.lookUpFOR1.TabStop = false;
            this.lookUpFOR1.EditValueChanged += new System.EventHandler(this.lookUpFOR1_EditValueChanged);
            // 
            // fornecedoresBindingSource
            // 
            this.fornecedoresBindingSource.DataMember = "FRNLookup";
            this.fornecedoresBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // lookUpFOR2
            // 
            this.lookUpFOR2.Location = new System.Drawing.Point(175, 23);
            this.lookUpFOR2.Name = "lookUpFOR2";
            this.lookUpFOR2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.lookUpFOR2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "FRN Nome", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "FRN Cnpj", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpFOR2.Properties.DataSource = this.fornecedoresBindingSource;
            this.lookUpFOR2.Properties.DisplayMember = "FRNNome";
            this.lookUpFOR2.Properties.NullText = " --";
            this.lookUpFOR2.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.lookUpFOR2.Properties.ShowHeader = false;
            this.lookUpFOR2.Properties.ValueMember = "FRN";
            this.lookUpFOR2.Size = new System.Drawing.Size(527, 20);
            this.lookUpFOR2.TabIndex = 29;
            this.lookUpFOR2.TabStop = false;
            this.lookUpFOR2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpFOR2_ButtonClick);
            this.lookUpFOR2.EditValueChanged += new System.EventHandler(this.lookUpFOR1_EditValueChanged);
            // 
            // ComboPAGTipoDefault
            // 
            this.ComboPAGTipoDefault.Location = new System.Drawing.Point(314, 5);
            this.ComboPAGTipoDefault.Name = "ComboPAGTipoDefault";
            this.ComboPAGTipoDefault.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboPAGTipoDefault.Size = new System.Drawing.Size(160, 20);
            this.ComboPAGTipoDefault.TabIndex = 14;
            this.ComboPAGTipoDefault.EditValueChanged += new System.EventHandler(this.ComboPAGTipoDefault_EditValueChanged);
            // 
            // chAgrupa
            // 
            this.chAgrupa.EditValue = true;
            this.chAgrupa.Location = new System.Drawing.Point(480, 6);
            this.chAgrupa.Name = "chAgrupa";
            this.chAgrupa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chAgrupa.Properties.Appearance.Options.UseFont = true;
            this.chAgrupa.Properties.Caption = "Permite Agrupar";
            this.chAgrupa.Size = new System.Drawing.Size(121, 19);
            this.chAgrupa.TabIndex = 13;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lookUpEdit1);
            this.groupControl3.Controls.Add(this.textEditDescritivo);
            this.groupControl3.Controls.Add(this.lookUpEditPLA1);
            this.groupControl3.Controls.Add(this.lookUpEditPLA2);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl3.Location = new System.Drawing.Point(2, 141);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1397, 73);
            this.groupControl3.TabIndex = 11;
            this.groupControl3.Text = "Classifica��o";
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Enabled = false;
            this.lookUpEdit1.Location = new System.Drawing.Point(5, 46);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLCodigo", "C�digo", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLDescricao", "Descri��o", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SPLValorISS", "ISS (%)", 15, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.lookUpEdit1.Properties.DataSource = this.SubPLanoPLAnocontasBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "Descritivo";
            this.lookUpEdit1.Properties.PopupWidth = 400;
            this.lookUpEdit1.Properties.ValueMember = "SPL";
            this.lookUpEdit1.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit1_Properties_ButtonClick);
            this.lookUpEdit1.Size = new System.Drawing.Size(164, 20);
            this.lookUpEdit1.TabIndex = 20;
            // 
            // SubPLanoPLAnocontasBindingSource
            // 
            this.SubPLanoPLAnocontasBindingSource.DataMember = "SubPLano";
            this.SubPLanoPLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // textEditDescritivo
            // 
            this.textEditDescritivo.Location = new System.Drawing.Point(175, 46);
            this.textEditDescritivo.Name = "textEditDescritivo";
            this.textEditDescritivo.Properties.MaxLength = 50;
            this.textEditDescritivo.Size = new System.Drawing.Size(527, 20);
            this.textEditDescritivo.TabIndex = 18;
            this.textEditDescritivo.EditValueChanged += new System.EventHandler(this.textEditDescritivo_EditValueChanged);
            this.textEditDescritivo.Validating += new System.ComponentModel.CancelEventHandler(this.textEditDescritivo_Validating);
            // 
            // lookUpEditPLA1
            // 
            this.lookUpEditPLA1.Location = new System.Drawing.Point(5, 23);
            this.lookUpEditPLA1.Name = "lookUpEditPLA1";
            this.lookUpEditPLA1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditPLA1.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA1.Properties.DisplayMember = "PLA";
            this.lookUpEditPLA1.Properties.NullText = "Plano de contas";
            this.lookUpEditPLA1.Properties.ValueMember = "PLA";
            this.lookUpEditPLA1.Size = new System.Drawing.Size(164, 20);
            this.lookUpEditPLA1.TabIndex = 0;
            this.lookUpEditPLA1.EditValueChanged += new System.EventHandler(this.lookUpEditPLA1_EditValueChanged);
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // lookUpEditPLA2
            // 
            this.lookUpEditPLA2.Location = new System.Drawing.Point(175, 23);
            this.lookUpEditPLA2.Name = "lookUpEditPLA2";
            this.lookUpEditPLA2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditPLA2.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA2.Properties.DisplayMember = "PLADescricao";
            this.lookUpEditPLA2.Properties.NullText = " --";
            this.lookUpEditPLA2.Properties.ValueMember = "PLA";
            this.lookUpEditPLA2.Size = new System.Drawing.Size(527, 20);
            this.lookUpEditPLA2.TabIndex = 1;
            this.lookUpEditPLA2.TabStop = false;
            this.lookUpEditPLA2.EditValueChanged += new System.EventHandler(this.lookUpEditPLA1_EditValueChanged);
            // 
            // BotaoGerar
            // 
            this.BotaoGerar.Enabled = false;
            this.BotaoGerar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BotaoGerar.ImageOptions.Image")));
            this.BotaoGerar.Location = new System.Drawing.Point(736, 5);
            this.BotaoGerar.Name = "BotaoGerar";
            this.BotaoGerar.Size = new System.Drawing.Size(128, 79);
            this.BotaoGerar.TabIndex = 10;
            this.BotaoGerar.Text = "Gerar Cheques";
            this.BotaoGerar.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // BotPadrao
            // 
            this.BotPadrao.Enabled = false;
            this.BotPadrao.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BotPadrao.ImageOptions.Image")));
            this.BotPadrao.Location = new System.Drawing.Point(607, 5);
            this.BotPadrao.Name = "BotPadrao";
            this.BotPadrao.Size = new System.Drawing.Size(123, 79);
            this.BotPadrao.TabIndex = 6;
            this.BotPadrao.Text = "Aplicar Padr�o";
            this.BotPadrao.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // PadraoValor
            // 
            this.PadraoValor.Location = new System.Drawing.Point(116, 47);
            this.PadraoValor.Name = "PadraoValor";
            this.PadraoValor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PadraoValor.Properties.DisplayFormat.FormatString = "n2";
            this.PadraoValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PadraoValor.Properties.EditFormat.FormatString = "c2";
            this.PadraoValor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PadraoValor.Properties.Precision = 2;
            this.PadraoValor.Size = new System.Drawing.Size(100, 20);
            this.PadraoValor.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(5, 50);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(72, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Valor Padr�o";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(5, 29);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(105, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Favorecido Padr�o";
            // 
            // PadraoVenc
            // 
            this.PadraoVenc.EditValue = null;
            this.PadraoVenc.Location = new System.Drawing.Point(116, 5);
            this.PadraoVenc.Name = "PadraoVenc";
            this.PadraoVenc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PadraoVenc.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.PadraoVenc.Size = new System.Drawing.Size(100, 20);
            this.PadraoVenc.TabIndex = 2;
            // 
            // PadraoFav
            // 
            this.PadraoFav.Location = new System.Drawing.Point(116, 26);
            this.PadraoFav.Name = "PadraoFav";
            this.PadraoFav.Properties.MaxLength = 70;
            this.PadraoFav.Size = new System.Drawing.Size(358, 20);
            this.PadraoFav.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(70, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Data Padr�o";
            // 
            // TabPageImportados
            // 
            this.TabPageImportados.Controls.Add(this.gridControl1);
            this.TabPageImportados.Name = "TabPageImportados";
            this.TabPageImportados.PageVisible = false;
            this.TabPageImportados.Size = new System.Drawing.Size(1513, 368);
            this.TabPageImportados.Text = "Dados Importados";
            // 
            // cChequesDTM
            // 
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.groupControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cChequesDTM";
            this.Size = new System.Drawing.Size(1407, 790);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtArquivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.TabPageCheques.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dChequesDTMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dChequesDTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpCONCodigo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCondominiosAtivos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpCONNome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fornecedoresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFOR2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboPAGTipoDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAgrupa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubPLanoPLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescritivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PadraoValor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PadraoVenc.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PadraoVenc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PadraoFav.Properties)).EndInit();
            this.TabPageImportados.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.ButtonEdit edtArquivo;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage TabPageImportados;
        private DevExpress.XtraTab.XtraTabPage TabPageCheques;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource dChequesDTMBindingSource;
        private dChequesDTM dChequesDTM;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEFavorecido;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit PadraoVenc;
        private DevExpress.XtraEditors.TextEdit PadraoFav;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton BotPadrao;
        private DevExpress.XtraEditors.CalcEdit PadraoValor;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEIdaData;
        private DevExpress.XtraGrid.Columns.GridColumn colCHERetornoData;
        private DevExpress.XtraEditors.SimpleButton BotaoGerar;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA2;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private DevExpress.XtraEditors.TextEdit textEditDescritivo;
        private DevExpress.XtraEditors.CheckEdit chAgrupa;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboPAGTipoDefault;
        private System.Windows.Forms.BindingSource fornecedoresBindingSource;
        private System.Windows.Forms.BindingSource bindingSourceCondominiosAtivos;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpCONCodigo;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colNumeroNota;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LookUpEdit lookUpFOR1;
        private DevExpress.XtraEditors.LookUpEdit lookUpFOR2;
        private DevExpress.XtraGrid.Columns.GridColumn colErro;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.BindingSource SubPLanoPLAnocontasBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCONProcuracao;
        private DevExpress.XtraGrid.Columns.GridColumn colCHE_BCO;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGTipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.SimpleButton BotAtCond;
        private System.Windows.Forms.Label LaConta;
        private Framework.objetosNeon.cCompet cCompet1;
    }
}
