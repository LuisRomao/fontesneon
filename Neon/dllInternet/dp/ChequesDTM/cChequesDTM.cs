using AbstratosNeon;
using ContaPagarProc;
using dllCheques;
using Framework;
using Framework.objetosNeon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using VirMSSQL;
using VirEnumeracoesNeon;


namespace DP.ChequesDTM
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cChequesDTM : CompontesBasicos.ComponenteBase
    {

        private SortedList<int, ABS_Condominio> _Condominios;

        private dNOtAs _dNOtAs;

        private string FavoritoAuto = "";

        private Cadastros.Fornecedores.Fornecedor fornecedor;

        private bool SemafaroFOR = false;

        private bool SemafaroPLA = false;

        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup;

        /// <summary>
        /// 
        /// </summary>
        public cChequesDTM()
        {
            InitializeComponent();
            pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas;
            bindingSourceCondominiosAtivos.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST;
            dFornecedoresLookup = new CadastrosProc.Fornecedores.dFornecedoresLookup();
            dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
            fornecedoresBindingSource.DataSource = dFornecedoresLookup;
            PopulaPadrao();
            Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(colPAGTipo);
            ComboPAGTipoDefault.EditValue = (int)PAGTipo.cheque;
            SubPLanoPLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25;
            cCompet1.Comp = new Competencia();
            //pLAnocontasBindingSource.Filter = "(PLA like '2%') or (PLA like '5%')";
        }

        private void AplicaPadrao(dChequesDTM.NovosChequesRow rowNChe)
        {
            if (rowNChe.IsCONMaloteNull() || (rowNChe.IsCONProcuracaoNull()))
            {
                try
                {
                    //AbstratosNeon.ABS_Condominio Condominio = AbstratosNeon.ABS_Condominio.GetCondominio(rowNChe.CON);
                    //Cadastros.Condominios.Condominio Condominio = new Cadastros.Condominios.Condominio(rowNChe.CON);
                    //Condominio.Conta(null).
                    //FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(rowNChe.CON);
                    //Cadastros.Condominios.Condominio Condominio = new Cadastros.Condominios.Condominio()
                    rowNChe.CONMalote = Condominios[rowNChe.CON].CONMalote;
                    rowNChe.CONProcuracao = Condominios[rowNChe.CON].CONProcuracao;
                }
                catch
                {
                    rowNChe.Erro = string.Format("Erro no cadastro do condom�nio (malote)");
                    return;
                }
            }
            rowNChe.Erro = "Ok";
            if (PadraoValor.EditValue != null)
                rowNChe.CHEValor = PadraoValor.Value;

            if (PadraoFav.Text != "")
                rowNChe.CHEFavorecido = PadraoFav.Text;

            if (PAGTipoPadrao.HasValue)
            {
                PAGTipo TipoEfetivo = PAGTipo.cheque;
                switch (PAGTipoPadrao.Value)
                {
                    case PAGTipo.cheque:
                    case PAGTipo.sindico:
                        TipoEfetivo = PAGTipoPadrao.Value;
                        break;
                    case PAGTipo.boleto:
                        if (Condominios[rowNChe.CON].ABSConta.PagamentoEletronico)
                            TipoEfetivo = PAGTipo.PETituloBancario;
                        else
                            TipoEfetivo = PAGTipo.boleto;
                        break;
                    case PAGTipo.deposito:
                        if (Condominios[rowNChe.CON].ABSConta.PagamentoEletronico)
                            TipoEfetivo = PAGTipo.PECreditoConta;
                        else
                            TipoEfetivo = PAGTipo.deposito;
                        break;
                }

                rowNChe.PAGTipo = (int)TipoEfetivo;

                if (PadraoVenc.DateTime != null)
                {
                    if (PadraoVenc.DateTime != DateTime.MinValue)
                        rowNChe.CHEVencimento = PadraoVenc.DateTime;
                    if (!rowNChe.IsCHEVencimentoNull())
                    {
                        malote M1 = null;
                        malote M2 = null;
                        bool RetornaNaData;
                        DateTime[] Datas = Cheque.CalculaDatas(TipoEfetivo, rowNChe.CONProcuracao, (malote.TiposMalote)rowNChe.CONMalote, rowNChe.CHEVencimento, DateTime.Now, ref M1, ref M2, out RetornaNaData);
                        if (Datas == null)
                        {
                            rowNChe.Erro = string.Format("Erro no cadastro do condom�nio (malote)");
                            return;
                        };
                        rowNChe.CHEIdaData = Datas[0];
                        rowNChe.CHERetornoData = Datas[1];
                    }
                }
            }
        }

        private void ComboPAGTipoDefault_EditValueChanged(object sender, EventArgs e)
        {
            BotPadrao.Enabled = PAGTipoPadrao.HasValue;
        }

        private void edtArquivo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            /*
            dChequesDTM.NovosCheques.Clear();
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {                                    
                    this.edtArquivo.EditValue = openFileDialog.FileName;
                    this.edtArquivo.ErrorText = "";
                    if (!File.Exists(this.openFileDialog.FileName))
                    {                        
                        this.edtArquivo.ErrorText = "O arquivo informado n�o existe ou n�o � um arquivo no formato v�lido (XML).";
                        MessageBox.Show("O arquivo informado n�o existe ou n�o � um arquivo no formato v�lido (XML).", "Cheques DTM", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    };
                    DataSet DS = new DataSet("DSLeitura");
                    DS.ReadXmlSchema(openFileDialog.FileName);
                    if (DS.Tables.Count != 1)
                        throw new Exception("Arquivo inv�lido!N�mero de tabelas = " + DS.Tables.Count.ToString());
                    if (!DS.Tables[0].Columns.Contains("COD_EMPRES"))
                        throw new Exception("Arquivo inv�lido! Coluna 'COD_EMPRES' n�o encontrado");
                    DS.Tables[0].Columns["COD_EMPRES"].DataType = typeof(int);
                    foreach (DataColumn DC in DS.Tables[0].Columns)
                    {
                        if (DC.ColumnName.StartsWith("VALOR") || DC.ColumnName.StartsWith("REFER"))
                            DC.DataType = typeof(decimal);
                        if (DC.ColumnName.StartsWith("VALOR") || DC.ColumnName.StartsWith("REFER"))
                            DC.DataType = typeof(decimal);
                    }
                    DS.ReadXml(openFileDialog.FileName);                    
                    gridControl1.DataSource = DS.Tables[0];
                    try {
                        gridView2.BeginDataUpdate();
                        //int CON = 0;
                        foreach (DataRow DR in DS.Tables[0].Rows) {
                            if (dChequesDTM.CONDOMINIOSTableAdapter.Fill(dChequesDTM.CONDOMINIOS, (int)DR["COD_EMPRES"]) == 1)
                            {
                                dChequesDTM.CONDOMINIOSRow rowCON = dChequesDTM.CONDOMINIOS[0];
                                if ((rowCON.CON_EMP != Framework.DSCentral.EMP) || (rowCON.CONStatus != 1))
                                    continue;
                                dChequesDTM.NovosChequesRow NovosLinha = null;
                                foreach (dChequesDTM.NovosChequesRow rowTeste in dChequesDTM.NovosCheques)
                                    if (rowTeste.CON == rowCON.CON)
                                    {
                                        NovosLinha = rowTeste;
                                        break;
                                    }                                
                                if (NovosLinha == null)
                                {
                                    NovosLinha = dChequesDTM.NovosCheques.NewNovosChequesRow();
                                    NovosLinha.CON = rowCON.CON;
                                    //NovosLinha.CONCodigo = rowCON.CONCodigo;
                                    //NovosLinha.CONNome = rowCON.CONNome;
                                    NovosLinha.CHE_BCO = rowCON.CON_BCO;
                                    if (!rowCON.IsCONProcuracaoNull())
                                        NovosLinha.CONProcuracao = rowCON.CONProcuracao;
                                    if (!rowCON.IsCONMaloteNull())
                                        NovosLinha.CONMalote = rowCON.CONMalote;
                                    NovosLinha.CHEValor = 0;
                                };
                                foreach (DataColumn DC in DS.Tables[0].Columns)
                                    if (DC.ColumnName.StartsWith("VALOR"))
                                        NovosLinha.CHEValor += (decimal)DR[DC];
                                if (NovosLinha.RowState == DataRowState.Detached)
                                    dChequesDTM.NovosCheques.AddNovosChequesRow(NovosLinha);
                            }
                            else {
                                if (dChequesDTM.CONDOMINIOS.Rows.Count == 0)
                                    MessageBox.Show("Nenhum condom�nio encontrado com o c�digo " + ((int)DR["COD_EMPRES"]).ToString());
                                else
                                    MessageBox.Show("Mais de um condom�nio encontrado com o c�digo " + ((int)DR["COD_EMPRES"]).ToString());
                            }
                        };                        
                    }
                    finally {                        
                        gridView2.EndDataUpdate();
                    };
                    xtraTabControl1.SelectedTabPage = TabPageCheques;
                
            }
            */
        }

        private void EnableGerar()
        {
            BotaoGerar.Enabled = ((lookUpEditPLA1.Text != "") && (textEditDescritivo.Text != ""));
        }

        private void GeraCheques()
        {
            if (lookUpEditPLA1.Text == "")
            {
                MessageBox.Show("Classifica��o inv�lida");
                return;
            }
            if (lookUpEdit1.EditValue == null)
            {
                MessageBox.Show("Classifica��o da prefeitura inv�lida");
                return;
            }
            int contador = 0;
            int? FRN = null;
            //if(lookUpFOR1.EditValue == null)
            if (fornecedor != null)
                FRN = fornecedor.FRN;
            try
            {
                TableAdapter.AbreTrasacaoSQL("DP cChequesDTM - 283",
                                                                       dNOtAs.NOtAsTableAdapter,
                                                                       dNOtAs.PAGamentosTableAdapter);
                foreach (dChequesDTM.NovosChequesRow rowNChe in dChequesDTM.NovosCheques)
                {

                    if (!rowNChe.IsCHEVencimentoNull() && !rowNChe.IsCHEValorNull() && !rowNChe.IsCHEFavorecidoNull())
                    {
                        if (rowNChe.CHEValor == 0)
                            continue;
                        if (rowNChe.IsErroNull())
                            continue;
                        if ((rowNChe.Erro != "") && (rowNChe.Erro != null) && (rowNChe.Erro != "Ok"))
                            continue;
                        int Nnota = rowNChe.IsNumeroNotaNull() ? 0 : rowNChe.NumeroNota;
                        if ((Nnota != 0) && (FRN.HasValue))
                        {
                            string ErrojaCadatrada = NotaJaCadastrada(Nnota, FRN.Value);
                            if (ErrojaCadatrada != "")
                            {
                                rowNChe.Erro = ErrojaCadatrada;
                                continue;
                            }
                        }
                        PAGTipo pagTipo = (PAGTipo)(rowNChe.IsPAGTipoNull() ? ComboPAGTipoDefault.EditValue : rowNChe.PAGTipo);
                        string DadosPag = ""; // COLOCAR DADOS DA CONTA                        
                        int? CCG = null;
                        int? CCT = null;
                        switch (pagTipo)
                        {
                            case PAGTipo.deposito:
                                DadosPag = fornecedor.Conta.ToString();
                                break;
                            case PAGTipo.PECreditoConta:
                                Cadastros.Condominios.Condominio Condominio1 = (Cadastros.Condominios.Condominio)Condominios[rowNChe.CON];
                                CCT = Condominio1.Conta.CCT;
                                CCG = fornecedor.Conta.CCG;
                                break;
                            case PAGTipo.PETituloBancarioAgrupavel:
                            case PAGTipo.PETituloBancario:
                                Cadastros.Condominios.Condominio Condominio2 = (Cadastros.Condominios.Condominio)Condominios[rowNChe.CON];
                                CCT = Condominio2.Conta.CCT;
                                break;
                        }
                        int? NOA = dNOtAs.IncluiNotaSimples_Efetivo(Nnota,
                                                             DateTime.Today,
                                                             rowNChe.CHEVencimento,
                                                             cCompet1.Comp,
                                                             NOATipo.ImportacaoXML,
                                                             rowNChe.CHEFavorecido,
                                                             rowNChe.CON,
                                                             pagTipo,
                                                             DadosPag,
                                                             chAgrupa.Checked,
                                                             lookUpEditPLA1.Text,
                                                             (int)lookUpEdit1.EditValue,
                                                             textEditDescritivo.Text,
                                                             rowNChe.CHEValor,
                                                             FRN,
                                                             false,
                                                             dNOtAs.TipoChave.Nenhuma,
                                                             null,
                                                             null,
                                                             CCT,
                                                             null,
                                                             CCG);
                        if (NOA.HasValue)
                            rowNChe.Erro = "Gerado";
                        else
                            rowNChe.Erro = "Erro no cadastro simplificado da nota";
                        contador++;
                    };
                };

                VirMSSQL.TableAdapter.ST().Commit();
                MessageBox.Show("Cheques cadastrados: " + contador.ToString());
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                MessageBox.Show("Erro na geracao dos cheques\r\n Os cheques n�o foram gravados");
                throw new Exception("Erro na geracao dos cheques\r\n Os cheques n�o foram gravados", e);
            }
        }

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dChequesDTM.NovosChequesRow linha = (dChequesDTM.NovosChequesRow)gridView2.GetDataRow(e.RowHandle);
            if (linha == null)
                return;
            DateTime Agora = DateTime.Now;
            DateTime Hoje = DateTime.Today;

            Hoje = Agora.Date;
            int Horadecorte = ((Agora.Hour > 10) ? 24 : 11);
            if (linha.IsCHEVencimentoNull())
                return;
            if (e.Column == colCHEVencimento)
                if (linha.CHEVencimento < Hoje)
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.White;
                }
                else
                    if (linha.CHEVencimento == Hoje)
                {
                    e.Appearance.BackColor = Color.Yellow;
                    e.Appearance.ForeColor = Color.Black;
                }
            if (e.Column == colCHEIdaData)
            {
                if (linha.IsCHEIdaDataNull())
                {
                    e.Appearance.ForeColor = Color.SaddleBrown;
                }
                else
                {
                    if (linha.CHEIdaData < Agora)
                    {
                        e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                    }
                    else
                        if (linha.CHEIdaData <= Hoje.AddHours(Horadecorte))
                    {
                        e.Appearance.BackColor = Color.Yellow;
                        e.Appearance.ForeColor = Color.Black;
                    }
                }
            }

            if (e.Column == colCHERetornoData)
                if (linha.IsCHERetornoDataNull())
                {
                    e.Appearance.ForeColor = Color.SaddleBrown;
                }
                else
                {
                    if (linha.CHERetornoData < Agora)
                    {
                        e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                    }
                    else
                        if (linha.CHERetornoData <= Hoje.AddHours(17))
                    {
                        e.Appearance.BackColor = Color.Yellow;
                        e.Appearance.ForeColor = Color.Black;
                    }
                }
            if (e.Column == colErro)
            {
                if (linha.IsErroNull())
                    return;
                if (linha.Erro == "Ok")
                {
                    e.Appearance.BackColor = Color.FromArgb(255, 255, 100);

                }
                else
                    if (linha.Erro == "Gerado")
                    e.Appearance.BackColor = Color.Lime;
                else
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.White;
                }
            }
            if (e.Column == colPAGTipo)
            {
                if (!linha.IsPAGTipoNull())
                    e.Appearance.BackColor = Enumeracoes.VirEnumPAGTipo.GetCor((PAGTipo)linha.PAGTipo);
            }
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dChequesDTM.NovosChequesRow rowNChe = (dChequesDTM.NovosChequesRow)(((DataRowView)e.Row).Row);
            if (rowNChe != null)
                AplicaPadrao(rowNChe);
        }

        /*
        private bool IncluiFRN(string CNPJAlvo)
        {            
            DocBacarios.CPFCNPJ cpfcnpj = new DocBacarios.CPFCNPJ(CNPJAlvo);
            Cadastros.Fornecedores.Fornecedor Fornecedor = Cadastros.Fornecedores.Fornecedor.GetFornecedor(cpfcnpj, true);
            if (Fornecedor != null)
            {
                dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
                return true;
            }
            else
                return false;
            //int FRN = -1;
            //System.Collections.SortedList CamposNovos = new System.Collections.SortedList();
            //CamposNovos.Add("FRNCnpj", CNPJAlvo);
            //CamposNovos.Add("FRNTipo", "FSR");
            //FRN = ShowModuloAdd(typeof(Cadastros.Fornecedores.cFornecedoresCampos), CamposNovos);
            /*
            if (FRN > 0)
            {
                try
                {
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = false;
                    Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresDataTable Fornecedores = Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fornecedores;
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.FillByFRN(Fornecedores, FRN);
                    SemafaroFOR = true;
                    lookUpFOR1.EditValue = lookUpFOR2.EditValue = FRN;
                    //LinhaMae.NOA_FRN = FRN;
                    SemafaroFOR = false;
                    //AjustaCNPJ(FRN);
                    //NotaJaCadastrada();
                }
                finally
                {
                    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.ClearBeforeFill = true;
                };
                return true;
            }

        }*/

        private void lookUpEdit1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                if (lookUpEditPLA1.EditValue == null)
                    return;
                ContaPagar.cNovoSPL novo = new ContaPagar.cNovoSPL(string.Format("{0} - {1}", lookUpEditPLA1.EditValue, lookUpEditPLA2.EditValue));
                novo.Cadastre(lookUpEditPLA1.EditValue.ToString());
            }
        }

        private void lookUpEditPLA1_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroPLA)
            {
                try
                {
                    SemafaroPLA = true;
                    if (sender == lookUpEditPLA1)
                        lookUpEditPLA2.EditValue = lookUpEditPLA1.EditValue;
                    if (sender == lookUpEditPLA2)
                        lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue;
                    if ((lookUpEditPLA2.EditValue == null) || (lookUpEditPLA2.EditValue == DBNull.Value))
                    {
                        lookUpEdit1.Enabled = false;
                        //PLArow = null;
                    }
                    else
                    {
                        string PLA = lookUpEditPLA2.EditValue.ToString();
                        //PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas.FindByPLA(PLA);
                        SubPLanoPLAnocontasBindingSource.Filter = string.Format("SPL_PLA = {0}", PLA);
                        lookUpEdit1.Enabled = true;
                        if (SubPLanoPLAnocontasBindingSource.Count == 0)
                            lookUpEdit1.EditValue = null;
                        else if (SubPLanoPLAnocontasBindingSource.Count == 1)
                        {
                            DataRowView DRV = (DataRowView)SubPLanoPLAnocontasBindingSource[0];
                            Framework.datasets.dPLAnocontas.SubPLanoRow rowsub = (Framework.datasets.dPLAnocontas.SubPLanoRow)DRV.Row;
                            lookUpEdit1.EditValue = rowsub.SPL;
                        }
                        else
                            lookUpEdit1.EditValue = null;
                    }
                    EnableGerar();
                }
                finally
                {
                    SemafaroPLA = false;
                }
            }
            /*
            if (!SemafaroPLA)
            {
                SemafaroPLA = true;
                if (sender == lookUpEditPLA1)
                    lookUpEditPLA2.EditValue = lookUpEditPLA1.EditValue;
                if (sender == lookUpEditPLA2)
                {
                    lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue;

                };
                string PLA = lookUpEditPLA2.EditValue.ToString();
                
                SubPLanoPLAnocontasBindingSource.Filter = string.Format("SPL_PLA = {0}", PLA);
                lookUpEdit1.Enabled = true;
                SemafaroPLA = false;
                EnableGerar();
            }
            */
        }

        private void lookUpFOR1_EditValueChanged(object sender, EventArgs e)
        {
            if (!SemafaroFOR)
            {
                int? novoFRN = null;
                try
                {
                    SemafaroFOR = true;
                    if (sender == lookUpFOR1)
                        lookUpFOR2.EditValue = lookUpFOR1.EditValue;
                    if (sender == lookUpFOR2)
                        lookUpFOR1.EditValue = lookUpFOR2.EditValue;
                    if (lookUpFOR1.EditValue != null)
                        novoFRN = (int)lookUpFOR1.EditValue;
                    else
                        novoFRN = null;
                    if ((PadraoFav.Text == "") || (PadraoFav.Text == FavoritoAuto))
                        FavoritoAuto = PadraoFav.Text = lookUpFOR2.Text;
                    if (novoFRN.HasValue)
                    {
                        if ((fornecedor == null) || (fornecedor.FRN != novoFRN.Value))
                        {
                            fornecedor = new Cadastros.Fornecedores.Fornecedor(novoFRN.Value);
                            PopulaPadrao();
                        }
                    }
                    else if (fornecedor != null)
                    {
                        fornecedor = null;
                        PopulaPadrao();
                    }
                }
                finally
                {
                    SemafaroFOR = false;
                }
            }
        }

        /*
        private void lookUpFOR1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                fornecedor = Cadastros.Fornecedores.Fornecedor.NovoFornecedor();
                if (fornecedor != null)
                    dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
            }
            //IncluiFRN("");
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Close)
            {
                fornecedor = null;
                lookUpFOR1.EditValue = null;
                if (PadraoFav.Text == FavoritoAuto)
                    FavoritoAuto = PadraoFav.Text = "";
            }

        }*/

        //private Cadastros.Fornecedores.Fornecedor Fornecedor;

        private void lookUpFOR2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit Chamador = (DevExpress.XtraEditors.LookUpEdit)sender;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
            {
                if (Chamador.EditValue == null)
                    return;
                int FRN = (int)Chamador.EditValue;
                fornecedor = new Cadastros.Fornecedores.Fornecedor(FRN);
                if (fornecedor.Editar(true))
                    dFornecedoresLookup.FRNLookupTableAdapter.Fill(dFornecedoresLookup.FRNLookup);
                //Cadastros.Fornecedores.cFornecedoresCampos EditorFRN = new Cadastros.Fornecedores.cFornecedoresCampos();
                //EditorFRN.Fill(CompontesBasicos.TipoDeCarga.pk, FRN, false);
                //if (EditorFRN.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                // {
                //    Cadastros.Fornecedores.dFornecedoresGrade.FornecedoresDataTable Fornecedores = Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fornecedores;
                //    Cadastros.Fornecedores.dFornecedoresGrade.FFornecedoresTableAdapter.Fill(Fornecedores, "FRN");
                //}
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                fornecedor = Cadastros.Fornecedores.Fornecedor.NovoFornecedor();
                if (fornecedor != null)
                {
                    dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
                    lookUpFOR2.EditValue = fornecedor.FRN;
                    lookUpFOR1.EditValue = fornecedor.FRN;
                }
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                fornecedor = null;
                lookUpFOR2.EditValue = null;
                lookUpFOR1.EditValue = null;
            }
        }

        private string NotaJaCadastrada(int Nota, int FRN)
        {
            string comandoBusca =
"SELECT     NOtAs.NOADATAI, CONDOMINIOS.CONCodigo\r\n" +
"FROM         NOtAs INNER JOIN\r\n" +
"                      CONDOMINIOS ON NOtAs.NOA_CON = CONDOMINIOS.CON\r\n" +
"WHERE     (NOtAs.NOANumero = @P1) AND (NOtAs.NOA_FRN = @P2);";
            DateTime DataInc;
            if (TableAdapter.ST().BuscaEscalar(comandoBusca, out DataInc, Nota, FRN))
                return string.Format("Nota j� cadastrada em {0:dd/MM/yyyy hh:mm}", DataInc);
            else
                return "";
        }

        private void Padrao()
        {
            foreach (dChequesDTM.NovosChequesRow rowNChe in dChequesDTM.NovosCheques)
                AplicaPadrao(rowNChe);
        }

        private void PopulaPadrao()
        {
            ComboPAGTipoDefault.Properties.Items.Clear();
            ComboPAGTipoDefault.Properties.Items.Add("Carteira", PAGTipo.cheque, -1);
            ComboPAGTipoDefault.Properties.Items.Add("S�ndico", PAGTipo.sindico, -1);
            ComboPAGTipoDefault.Properties.Items.Add("Boleto", PAGTipo.boleto, -1);
            if ((fornecedor != null) && (fornecedor.Conta.Encontrado))
            {
                ComboPAGTipoDefault.Properties.Items.Add("Dep�sito em conta", PAGTipo.deposito, -1);
                LaConta.Visible = true;
                LaConta.Text = fornecedor.Conta.ToString();
            }
            else
                LaConta.Visible = false;
            BotPadrao.Enabled = PAGTipoPadrao.HasValue;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (ComboPAGTipoDefault.SelectedItem == null)
                ComboPAGTipoDefault.ErrorText = "obrigat�rio";
            else
                Padrao();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            _Condominios = null;
            BotAtCond.Enabled = false;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            //dChequesDTM.CONDOMINIOSTableAdapter.FillTodos(dChequesDTM.CONDOMINIOS, Framework.DSCentral.EMP);
            //SortedList<int, ABS_Condominio> Condominios = ABS_Condominio.ABSGetCondominios(true);
            dChequesDTM.NovosCheques.Clear();
            //foreach (dChequesDTM.CONDOMINIOSRow rowCON in dChequesDTM.CONDOMINIOS)                        
            foreach (ABS_Condominio Condominio in Condominios.Values)
            {
                try
                {
                    dChequesDTM.NovosChequesRow NovosLinha = dChequesDTM.NovosCheques.NewNovosChequesRow();
                    NovosLinha.CON = Condominio.CON;                    
                    NovosLinha.CHE_BCO = Condominio.ABSConta.BCO;
                    NovosLinha.CONProcuracao = Condominio.CONProcuracao;
                    NovosLinha.CONMalote = Condominio.CONMalote;
                    NovosLinha.CHEValor = 0;
                    dChequesDTM.NovosCheques.AddNovosChequesRow(NovosLinha);
                }
                catch (Exception ex)
                {

                }                
            };
            xtraTabControl1.SelectedTabPage = TabPageCheques;

        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            GeraCheques();
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            decimal multiplicador = 0;
            if (VirInput.Input.Execute("Multiplicador", ref multiplicador, 6))
                if (multiplicador != 0)
                    foreach (dChequesDTM.NovosChequesRow rowNChe in dChequesDTM.NovosCheques)
                    {
                        rowNChe.CHEValor = Math.Round(rowNChe.CHEValor * multiplicador, 2);
                    }
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            dChequesDTM.NovosCheques.Clear();
        }

        private void textEditDescritivo_EditValueChanged(object sender, EventArgs e)
        {
            EnableGerar();
        }

        private void textEditDescritivo_Validating(object sender, CancelEventArgs e)
        {
            string Corrigido = "";
            bool primeiro = true;
            for (int i = 0; i < textEditDescritivo.Text.Length; i++)
            {
                if (primeiro)
                    Corrigido += char.ToUpper(textEditDescritivo.Text[i]);
                else
                    Corrigido += char.ToLower(textEditDescritivo.Text[i]);
                if (textEditDescritivo.Text[i] != ' ')
                    primeiro = false;
            }
            if (textEditDescritivo.Text != Corrigido)
                textEditDescritivo.Text = Corrigido;
        }

        private SortedList<int, ABS_Condominio> Condominios
        {
            get
            {
                if (_Condominios == null)
                    using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                    {
                        ESP.Espere("Carregando Condom�nios");
                        Application.DoEvents();
                        _Condominios = ABS_Condominio.ABSGetCondominios(true);
                        BotAtCond.Enabled = true;
                    }
                return _Condominios;
            }
        }

        private dNOtAs dNOtAs
        {
            get
            {
                if (_dNOtAs == null)
                    _dNOtAs = new dNOtAs();
                return _dNOtAs;
            }
        }

        private PAGTipo? PAGTipoPadrao
        {
            get
            {
                if (ComboPAGTipoDefault.SelectedItem != null)
                    return (PAGTipo)ComboPAGTipoDefault.EditValue;
                else
                    return null;
            }
        }
    }
}

