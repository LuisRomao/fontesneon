﻿/*
MR - 12/11/2014 10:00           - Inclusão do campo com cod. de comunicacao para tributos, usando no envio de arquivo para Bradesco (CONCodigoComunicacaoTributos) no CONDOMINIOS
                                  Inclusão do campo cod. de comunicacao, dig. agencia e dig. conta (CodComunicacao, DigAgencia, DigConta) no PIS
MR - 04/12/2014 10:00           - Inclusão do campo cod. de comunicacao, dig. agencia e dig. conta (CodComunicacao, DigAgencia, DigConta) no INSS
*/

using dllVirEnum;
using System.Data;
using System.Drawing;
using Framework;
using FrameworkProc;
using DPProc;
using VirEnumeracoesNeon;


namespace DP.Automacao_Bancaria
{
    partial class dAutomacaoBancaria
    {
        partial class RHTarefaDetalheDataTable
        {
        }

        partial class SaldoCCDataTable
        {
        }

        private static dAutomacaoBancaria _dAutomacaoBancariaSt;

        /// <summary>
        /// dataset estático:dAutomacaoBancaria
        /// </summary>
        public static dAutomacaoBancaria dAutomacaoBancariaSt
        {
            get
            {
                if (_dAutomacaoBancariaSt == null)
                    _dAutomacaoBancariaSt = new dAutomacaoBancaria();
                return _dAutomacaoBancariaSt;
            }
        }

        private dAutomacaoBancariaTableAdapters.SaldoCCTableAdapter saldoCCTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoCC
        /// </summary>
        public dAutomacaoBancariaTableAdapters.SaldoCCTableAdapter SaldoCCTableAdapter
        {
            get
            {
                if (saldoCCTableAdapter == null)
                {
                    saldoCCTableAdapter = new dAutomacaoBancariaTableAdapters.SaldoCCTableAdapter();
                    saldoCCTableAdapter.TrocarStringDeConexao();
                    saldoCCTableAdapter.ClearBeforeFill = false;
                };
                return saldoCCTableAdapter;
            }
        }

        private dAutomacaoBancariaTableAdapters.SaldoCCTableAdapter saldoCCTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoCC (Filial)
        /// </summary>
        public dAutomacaoBancariaTableAdapters.SaldoCCTableAdapter SaldoCCTableAdapterF
        {
            get
            {
                if (saldoCCTableAdapterF == null)
                {
                    saldoCCTableAdapterF = new dAutomacaoBancariaTableAdapters.SaldoCCTableAdapter();
                    saldoCCTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    saldoCCTableAdapterF.ClearBeforeFill = false;
                };
                return saldoCCTableAdapterF;
            }
        }
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoCC (Local/Filial)
        /// </summary>
        public dAutomacaoBancariaTableAdapters.SaldoCCTableAdapter SaldoCCTableAdapterX(EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTipo.Local ? SaldoCCTableAdapter : SaldoCCTableAdapterF;
        }


        private dAutomacaoBancariaTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dAutomacaoBancariaTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dAutomacaoBancariaTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dAutomacaoBancariaTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS (Filial)
        /// </summary>
        public dAutomacaoBancariaTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapterF
        {
            get
            {
                if (cONDOMINIOSTableAdapterF == null)
                {
                    cONDOMINIOSTableAdapterF = new dAutomacaoBancariaTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                return cONDOMINIOSTableAdapterF;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public dAutomacaoBancariaTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapterX(EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTipo.Local ? CONDOMINIOSTableAdapter : CONDOMINIOSTableAdapterF;
        }

        private dAutomacaoBancariaTableAdapters.FUnCionarioTableAdapter fUnCionarioTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FUnCionario
        /// </summary>
        public dAutomacaoBancariaTableAdapters.FUnCionarioTableAdapter FUnCionarioTableAdapter
        {
            get
            {
                if (fUnCionarioTableAdapter == null)
                {
                    fUnCionarioTableAdapter = new dAutomacaoBancariaTableAdapters.FUnCionarioTableAdapter();
                    fUnCionarioTableAdapter.TrocarStringDeConexao();
                };
                return fUnCionarioTableAdapter;
            }
        }

        private dAutomacaoBancariaTableAdapters.FUnCionarioTableAdapter fUnCionarioTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: FUnCionario (Filial)
        /// </summary>
        public dAutomacaoBancariaTableAdapters.FUnCionarioTableAdapter FUnCionarioTableAdapterF
        {
            get
            {
                if (fUnCionarioTableAdapterF == null)
                {
                    fUnCionarioTableAdapterF = new dAutomacaoBancariaTableAdapters.FUnCionarioTableAdapter();
                    fUnCionarioTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    fUnCionarioTableAdapterF.ClearBeforeFill = false;
                };
                return fUnCionarioTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public dAutomacaoBancariaTableAdapters.FUnCionarioTableAdapter FUnCionarioTableAdapterX(EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTipo.Local ? FUnCionarioTableAdapter : FUnCionarioTableAdapterF;
        }

        private dAutomacaoBancariaTableAdapters.RHTarefaTableAdapter rHTarefaTableAdapter;
        private dAutomacaoBancariaTableAdapters.RHTarefaTableAdapter rHTarefaTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefa
        /// </summary>
        public dAutomacaoBancariaTableAdapters.RHTarefaTableAdapter RHTarefaTableAdapter
        {
            get
            {
                if (rHTarefaTableAdapter == null)
                {
                    rHTarefaTableAdapter = new dAutomacaoBancariaTableAdapters.RHTarefaTableAdapter();
                    rHTarefaTableAdapter.TrocarStringDeConexao();
                };
                return rHTarefaTableAdapter;
            }
        }


        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefa (Filial)
        /// </summary>
        public dAutomacaoBancariaTableAdapters.RHTarefaTableAdapter RHTarefaTableAdapterF
        {
            get
            {
                if (rHTarefaTableAdapterF == null)
                {
                    rHTarefaTableAdapterF = new dAutomacaoBancariaTableAdapters.RHTarefaTableAdapter();
                    rHTarefaTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    rHTarefaTableAdapterF.ClearBeforeFill = false;
                };
                return rHTarefaTableAdapterF;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefa 
        /// </summary>
        public dAutomacaoBancariaTableAdapters.RHTarefaTableAdapter RHTarefaTableAdapterX(int EMPX)
        {
            return EMPX == DSCentral.EMP ? RHTarefaTableAdapter : RHTarefaTableAdapterF;
        }

        private dAutomacaoBancariaTableAdapters.RHTarefaDetalheTableAdapter rHTarefaDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefaDetalhe
        /// </summary>
        public dAutomacaoBancariaTableAdapters.RHTarefaDetalheTableAdapter RHTarefaDetalheTableAdapter
        {
            get
            {
                if (rHTarefaDetalheTableAdapter == null)
                {
                    rHTarefaDetalheTableAdapter = new dAutomacaoBancariaTableAdapters.RHTarefaDetalheTableAdapter();
                    rHTarefaDetalheTableAdapter.TrocarStringDeConexao();
                };
                return rHTarefaDetalheTableAdapter;
            }
        }

        private dAutomacaoBancariaTableAdapters.RHTarefaDetalheTableAdapter rHTarefaDetalheTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefaDetalhe (Filial)
        /// </summary>
        public dAutomacaoBancariaTableAdapters.RHTarefaDetalheTableAdapter RHTarefaDetalheTableAdapterF
        {
            get
            {
                if (rHTarefaDetalheTableAdapterF == null)
                {
                    rHTarefaDetalheTableAdapterF = new dAutomacaoBancariaTableAdapters.RHTarefaDetalheTableAdapter();
                    rHTarefaDetalheTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    rHTarefaDetalheTableAdapterF.ClearBeforeFill = false;
                };
                return rHTarefaDetalheTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public dAutomacaoBancariaTableAdapters.RHTarefaDetalheTableAdapter RHTarefaDetalheTableAdapterX(EMPTipo EMPTipo)
        {
            return EMPTipo == FrameworkProc.EMPTipo.Local ? RHTarefaDetalheTableAdapter : RHTarefaDetalheTableAdapterF;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public dAutomacaoBancariaTableAdapters.RHTarefaDetalheTableAdapter RHTarefaDetalheTableAdapterX(int EMP)
        {
            return EMP == Framework.DSCentral.EMP ? RHTarefaDetalheTableAdapter : RHTarefaDetalheTableAdapterF;
        }

        private dAutomacaoBancariaTableAdapters.RHTarefaSubdetalheTableAdapter rHTarefaSubdetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefaSubdetalhe
        /// </summary>
        public dAutomacaoBancariaTableAdapters.RHTarefaSubdetalheTableAdapter RHTarefaSubdetalheTableAdapter
        {
            get
            {
                if (rHTarefaSubdetalheTableAdapter == null)
                {
                    rHTarefaSubdetalheTableAdapter = new dAutomacaoBancariaTableAdapters.RHTarefaSubdetalheTableAdapter();
                    rHTarefaSubdetalheTableAdapter.TrocarStringDeConexao();
                };
                return rHTarefaSubdetalheTableAdapter;
            }
        }

        private dAutomacaoBancariaTableAdapters.RHTarefaSubdetalheTableAdapter rHTarefaSubdetalheTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefaSubdetalhe (Filial)
        /// </summary>
        public dAutomacaoBancariaTableAdapters.RHTarefaSubdetalheTableAdapter RHTarefaSubdetalheTableAdapterF
        {
            get
            {
                if (rHTarefaSubdetalheTableAdapterF == null)
                {
                    rHTarefaSubdetalheTableAdapterF = new dAutomacaoBancariaTableAdapters.RHTarefaSubdetalheTableAdapter();
                    rHTarefaSubdetalheTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    rHTarefaSubdetalheTableAdapterF.ClearBeforeFill = false;
                };
                return rHTarefaSubdetalheTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMPTipo"></param>
        /// <returns></returns>
        public dAutomacaoBancariaTableAdapters.RHTarefaSubdetalheTableAdapter RHTarefaSubdetalheTableAdapterX(EMPTipo EMPTipo)
        {
            return EMPTipo == EMPTipo.Local ? RHTarefaSubdetalheTableAdapter : RHTarefaSubdetalheTableAdapterF;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public dAutomacaoBancariaTableAdapters.RHTarefaSubdetalheTableAdapter RHTarefaSubdetalheTableAdapterX(int EMP)
        {
            return EMP == Framework.DSCentral.EMP ? RHTarefaSubdetalheTableAdapter : RHTarefaSubdetalheTableAdapterF;
        }

        /// <summary>
        /// Força a recarga dos dados
        /// </summary>
        internal void RecarregarCondominios()
        {
            CONDOMINIOS.Clear();
            _CONDOMINIOSF = null;
        }

        internal void RecarregarFuncionarios(bool Forcar)
        {
            if (!Forcar && (FUnCionario.Count > 0))
                return;
            FUnCionario.Clear();
            FUnCionarioTableAdapter.FillcomEMP(FUnCionario);
            FUnCionarioTableAdapterF.FillcomEMP(FUnCionario);
        }

        internal void CompletaCampos()
        {
            PopulaSaldos();
            foreach (RHTarefaDetalheRow RHDrow in RHTarefaDetalhe)
            {
                if (RHDrow.SaldoCCRowParent != null)
                {
                    //RHDrow.ValSaldoCC = RHDrow.SaldoCCRowParent.SCCValorApF;
                    if (RHDrow.SaldoCCRowParent.CCTTipo == (int)CCTTipo.ContaCorrete_com_Oculta)
                        RHDrow.ValSaldoCC = RHDrow.SaldoCCRowParent.SCCValorF + RHDrow.SaldoCCRowParent.SCCValorApF;
                    else
                        RHDrow.ValSaldoCC = RHDrow.SaldoCCRowParent.SCCValorF;
                }
                DataRowState RowStateOrig = RHDrow.RowState;
                RHDrow.Total = 0;
                RHDrow.ValorAcumulado = 0;
                foreach (RHTarefaSubdetalheRow RHSrow in RHDrow.GetRHTarefaSubdetalheRows())
                {

                    RHDrow.Total += RHSrow.RHSValor;
                    if (RHSrow.IsCHEStatusNull())
                        RHDrow.ValorAcumulado += RHSrow.RHSValor;
                    RowStateOrig = RHSrow.RowState;
                    FUnCionarioRow FUCrow = RHSrow.FUnCionarioRow;
                    if (!FUCrow.IsFUCAgenciaNull())
                        RHSrow.FUCAgencia = FUCrow.FUCAgencia;
                    if (!FUCrow.IsFUCBancoNull())
                        RHSrow.FUCBanco = FUCrow.FUCBanco;
                    if (!FUCrow.IsFUCContaNull())
                        RHSrow.FUCConta = FUCrow.FUCConta;
                    if (!FUCrow.IsFUCCpfNull())
                    {
                        DocBacarios.CPFCNPJ cpf = new DocBacarios.CPFCNPJ(FUCrow.FUCCpf);
                        RHSrow.FUCCpf = cpf.ToString();
                    };
                    RHSrow.FUCNome = FUCrow.FUCNome;
                    RHSrow.FUCRegistro = FUCrow.FUCRegistro;
                    if (!FUCrow.IsFUCTipoContaNull())
                        RHSrow.FUCTipoConta = FUCrow.FUCTipoConta;
                    if (RowStateOrig == DataRowState.Unchanged)
                        RHSrow.AcceptChanges();
                }
                if (RowStateOrig == DataRowState.Unchanged)
                    RHDrow.AcceptChanges();
            }
        }

        private void PopulaSaldos(bool forcar = false)
        {
            if ((SaldoCC.Count != 0) && (!forcar))
                return;
            SaldoCC.Clear();
            SaldoCCTableAdapter.Fill(SaldoCC);
            SaldoCCTableAdapterF.Fill(SaldoCC);
        }

        /// <summary>
        /// Busca o condominio pelos codigos da folha
        /// </summary>
        /// <param name="CodFolha"></param>
        /// <param name="CodFolha2"></param>
        /// <param name="Usar2Bancos"></param>
        /// <param name="CadastroFolha"></param>
        /// <returns></returns>
        internal dAutomacaoBancaria.CONDOMINIOSRow BuscaCondominio(int? CodFolha, string CodFolha2, bool Usar2Bancos, string CadastroFolha = "")
        {
            dAutomacaoBancaria.CONDOMINIOSRow retorno = null;
            if (CONDOMINIOS.Rows.Count == 0)
                CONDOMINIOSTableAdapter.Fill(CONDOMINIOS);
            retorno = BuscaCondominio(CodFolha, CodFolha2, CONDOMINIOS);


            if (Usar2Bancos)
            {
                if (retorno == null)
                {
                    retorno = BuscaCondominioFilial(CodFolha, CodFolha2);
                }
                if (retorno == null)
                {
                    cNovoCodigo cNovo = new cNovoCodigo(CodFolha, CodFolha2, this, CadastroFolha);
                    if (cNovo.ShowEmPopUp() == System.Windows.Forms.DialogResult.OK)
                        return cNovo.resultado;
                }
            }
            return retorno;
        }


        internal RHTarefaSubdetalheRow Busca_rowRHS()
        {

            return null;
        }

        private DataView dV_FUnCionario_EMP_CON_Registro;

        private DataView DV_FUnCionario_EMP_CON_Registro
        {
            get
            {
                if (dV_FUnCionario_EMP_CON_Registro == null)
                {
                    dV_FUnCionario_EMP_CON_Registro = new DataView(FUnCionario);
                    dV_FUnCionario_EMP_CON_Registro.Sort = "EMP,FUC_CON,FUCRegistro";
                };
                return dV_FUnCionario_EMP_CON_Registro;
            }
        }

        internal FUnCionarioRow Busca_rowFUnCionario(int EMP, int CON, string Registro)
        {
            int ind = DV_FUnCionario_EMP_CON_Registro.Find(new object[] { EMP, CON, Registro });
            return (ind < 0) ? null : (FUnCionarioRow)(dV_FUnCionario_EMP_CON_Registro[ind].Row);
        }




        private DataView dV_RHTarefaDetalhe_RHD_RHT_RHD_CON;

        private DataView DV_RHTarefaDetalhe_RHD_RHT_RHD_CON
        {
            get
            {
                if (dV_RHTarefaDetalhe_RHD_RHT_RHD_CON == null)
                {
                    dV_RHTarefaDetalhe_RHD_RHT_RHD_CON = new DataView(RHTarefaDetalhe);
                    dV_RHTarefaDetalhe_RHD_RHT_RHD_CON.Sort = "RHD_RHT,RHD_CON";
                };
                return dV_RHTarefaDetalhe_RHD_RHT_RHD_CON;
            }
        }

        /*
        internal TMP_SalarioRow Busca_rowFolha(int TMPFolha, int EMP, int CON__EMP)
        {
            int ind = DV_TMP_Salario_TMPFolha_EMP_CON__EMP.Find(new object[] { TMPFolha, EMP, CON__EMP });
            return (ind < 0) ? null : (TMP_SalarioRow)(DV_TMP_Salario_TMPFolha_EMP_CON__EMP[ind].Row);
        }*/

        internal RHTarefaDetalheRow Busca_rowRHD(int RHT, int CON__EMP)
        {
            int ind = DV_RHTarefaDetalhe_RHD_RHT_RHD_CON.Find(new object[] { RHT, CON__EMP });
            return (ind < 0) ? null : (RHTarefaDetalheRow)(DV_RHTarefaDetalhe_RHD_RHT_RHD_CON[ind].Row);
        }

        private CONDOMINIOSDataTable _CONDOMINIOSF;

        internal CONDOMINIOSDataTable CONDOMINIOSF
        {
            get
            {
                if (_CONDOMINIOSF == null)
                {
                    _CONDOMINIOSF = new CONDOMINIOSDataTable();
                    CONDOMINIOSTableAdapterF.Fill(_CONDOMINIOSF);
                    //_CONDOMINIOS_F = CONDOMINIOSTableAdapterF.GetData();
                }
                return _CONDOMINIOSF;
            }
        }

        internal CONDOMINIOSDataTable CONDOMINIOSX(int EMP)
        {
            if (EMP == DSCentral.EMP)
                return CONDOMINIOS;
            else
                return CONDOMINIOSF;
        }

        internal void CarregaCondominos(bool Forcar = false)
        {
            if (CONDOMINIOS.Count == 0 || Forcar)
                CONDOMINIOSTableAdapter.Fill(CONDOMINIOS);
            if (CONDOMINIOSF.Count == 0 || Forcar)
                CONDOMINIOSTableAdapterF.Fill(CONDOMINIOSF);
        }

        private dAutomacaoBancaria.CONDOMINIOSRow BuscaCondominioFilial(int? CodFolha, string CodFolha2)
        {
            return BuscaCondominio(CodFolha, CodFolha2, CONDOMINIOSF);
        }

        private dAutomacaoBancaria.CONDOMINIOSRow BuscaCondominio(int? CodFolha, string CodFolha2, CONDOMINIOSDataTable Tabela)
        {
            foreach (dAutomacaoBancaria.CONDOMINIOSRow rowCond in Tabela)
            {
                if (CodFolha.HasValue)
                    if (!rowCond.IsCONCodigoFolha1Null() && (rowCond.CONCodigoFolha1 == CodFolha.Value))
                        return rowCond;
                if (CodFolha2 != null)
                    if (!rowCond.IsCONCodigoFolha2ANull() && (rowCond.CONCodigoFolha2A == CodFolha2))
                        return rowCond;
            }
            return null;
        }

        partial class DETREMESSADPDataTable
        {
        }

        partial class INSSDataTable
        {
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        internal enum nTipoPag {
            /// <summary>
            /// 
            /// </summary>
            Invalido = 0,
            /// <summary>
            /// 
            /// </summary>
            Cheque = 1,
            /// <summary>
            /// 
            /// </summary>
            CreditoConta = 2,                        
            /// <summary>
            /// 
            /// </summary>
            Carta = 3,            
            /// <summary>
            /// 
            /// </summary>
            NaoEmitir = 4
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rowCond"></param>
        /// <returns></returns>
        public string CondominioUsuInternto(CONDOMINIOSRow rowCond)
        {
            string Manobra = "";
            if (!rowCond.IsCONCodigoFolha1Null())
                Manobra = rowCond.CONCodigoFolha1.ToString();
            if (!rowCond.IsCONCodigoFolha2ANull())
                Manobra += (Manobra == "" ? "" : " - ") + rowCond.CONCodigoFolha2A;
            Manobra += (Manobra == "" ? "" : " ") + rowCond.CONCodigo.ToString();
            if (!rowCond.IsCONProcuracaoNull())
                Manobra += (Manobra == "" ? "" : " - ") + (rowCond.CONProcuracao ? "P" : "N");
            return Manobra;
        }

        private VirEnum virEnumnTipoPag;

        /// <summary>
        /// 
        /// </summary>
        public VirEnum VirEnumnTipoPag
        {
            get
            {
                if (virEnumnTipoPag == null)
                {
                    virEnumnTipoPag = new VirEnum(typeof(nTipoPag));
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoConta, "Crédito Conta", Color.Lime);
                    virEnumnTipoPag.GravaNomes(nTipoPag.Cheque, "Cheque", Color.Aqua);
                    //virEnumnTipoPag.GravaNomes(nTipoPag.Carta, "Carta", Color.Salmon);                    
                    virEnumnTipoPag.GravaNomes(nTipoPag.Invalido, "Inválido", Color.Red);
                    virEnumnTipoPag.GravaNomes(nTipoPag.NaoEmitir, "Não emitir", Color.LightPink);
                    virEnumnTipoPag.GravaNomes(nTipoPag.ChequeEletronico, "Cheque eletrônico", Color.Gold);
                }
                return virEnumnTipoPag;
            }
        }

        private VirEnum virEnumnTipoPagPTRB;

        /// <summary>
        /// 
        /// </summary>
        public VirEnum VirEnumnTipoPagPTRB
        {
            get
            {
                if (virEnumnTipoPagPTRB == null)
                {
                    virEnumnTipoPagPTRB = new VirEnum(typeof(nTipoPag));
                    virEnumnTipoPagPTRB.CarregarDefault = false;
                    virEnumnTipoPagPTRB.GravaNomes(nTipoPag.Cheque, "Cheque", System.Drawing.Color.Aqua);
                    virEnumnTipoPagPTRB.GravaNomes(nTipoPag.Invalido, "Inválido", System.Drawing.Color.Red);
                    virEnumnTipoPagPTRB.GravaNomes(nTipoPag.CreditoConta, "Elet. Bradesco", System.Drawing.Color.Lime);
                    //virEnumnTipoPagPTRB.GravaNomes(nTipoPag.EletronicoItau, "Elet. Itaú");
                    //virEnumnTipoPagPTRB.GravaNomes(nTipoPag.ChequeFilial, "Cheque Filial");
                    virEnumnTipoPagPTRB.GravaNomes(nTipoPag.NaoEmitir, "Não emitir");
                }
                return virEnumnTipoPagPTRB;
            }
        }

    }
}
