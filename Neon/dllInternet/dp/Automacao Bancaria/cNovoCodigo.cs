﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using Framework;
using FrameworkProc;

namespace DP.Automacao_Bancaria
{
    /// <summary>
    /// Associar código da folha
    /// </summary>
    public partial class cNovoCodigo : ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cNovoCodigo()
        {
            InitializeComponent();
        }

        private int? CodFolha;
        private string CodFolha2;
        private dAutomacaoBancaria dChamador;
        /// <summary>
        /// Resultado
        /// </summary>
        internal dAutomacaoBancaria.CONDOMINIOSRow resultado;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CodFolha"></param>
        /// <param name="CodFolha2"></param>
        /// <param name="dChamador"></param>
        /// <param name="CadastroFolha"></param>
        public cNovoCodigo(int? CodFolha, string CodFolha2, dAutomacaoBancaria dChamador,string CadastroFolha)
            : this()
        {
            LbCadastroFolha.Text = CadastroFolha;
            if (CodFolha.HasValue)
            {
                textEditCodigo.Text = CodFolha.ToString();
                this.CodFolha = CodFolha;
                this.CodFolha2 = null;
            }
            else
            {
                textEditCodigo.Text = CodFolha2;
                this.CodFolha = null;
                this.CodFolha2 = CodFolha2;
            }            
            this.dChamador = dChamador;
        }

        /// <summary>
        /// Fecha Tela
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if (Resultado == DialogResult.OK)
            {
                dAutomacaoBancariaTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapterX;
                if (lookupCondominio_F1.CON_sel > 0)
                {
                    if (lookupCondominio_F1.Filial == EMPTipo.Local)
                    {
                        resultado = dChamador.CONDOMINIOS.FindByCON(lookupCondominio_F1.CON_sel);
                        CONDOMINIOSTableAdapterX = dChamador.CONDOMINIOSTableAdapter;
                    }
                    else
                    {
                        resultado = dChamador.CONDOMINIOSF.FindByCON(lookupCondominio_F1.CON_sel);
                        CONDOMINIOSTableAdapterX = dChamador.CONDOMINIOSTableAdapterF;
                    }
                    if (CodFolha.HasValue)
                    {
                        if (!resultado.IsCONCodigoFolha1Null())
                        {
                            string Mensagem = string.Format("Este condomíno já está ligado ao código {0}\r\nDeseja substituir?",resultado.CONCodigoFolha1);
                            if (MessageBox.Show(Mensagem, "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                return;
                        }
                        resultado.CONCodigoFolha1 = CodFolha.Value;
                    }
                    if (!string.IsNullOrEmpty(CodFolha2))
                    {
                        if (!resultado.IsCONCodigoFolha2ANull())
                        {
                            string Mensagem = string.Format("Este condomíno já está ligado ao código {0}\r\nDeseja substituir?", resultado.CONCodigoFolha2A);
                            if (MessageBox.Show(Mensagem, "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                return;
                        }
                        resultado.CONCodigoFolha2A = CodFolha2;
                    }
                    CONDOMINIOSTableAdapterX.Update(resultado);
                }
            }
            base.FechaTela(Resultado);
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lookupCondominio_F1.Filial = (bool)radioGroup1.EditValue ? EMPTipo.Local : EMPTipo.Filial;
        }
    }
}
