/*
LH - 16/04/2014 -                 - troca o campo CONCodigoFolha2 por CONCodigoFolha2A
                                    remo��o de CONCodigoBancoCS
LH - 25/04/2014       - 13.2.8.31 - FIX inclus�o do campo PAGPermiteAgrupar
LH - 12/05/2014       - 13.2.8.47 - FIX erro com o CONCodigoFolha2A
MR - 12/11/2014 10:00 -           - Renomeada a pasta PIS para DARF, pois trata PIS e IR - apenas nome da pasta, no c�digo mantive as fun��es com a nomenclatura PIS
                                    Inclus�o da coluna cod. de comunicacao como oculta na aba do DARF - antiga PIS
                                    Salvando os dados adicionais de cod. de comunicacao, dig. agencia e dig. conta na tabela GPS (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 04/12/2014 10:00 -           - Solicita Data para Pagamento, permitindo escolher uma data diferente da data seguinte (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***)
                                    Solicita Senha antes da data de pagamento (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***)
                                    Solicita libera��o apenas para valores acima de 15000,00 e n�o tratar n�vel de libera��o (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***)
                                    Inclus�o da coluna cod. de comunicacao como oculta na aba do GPS
                                    Salvando os dados adicionais de cod. de comunicacao, dig. agencia e dig. conta na tabela GPS (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***)
MR - 10/12/2014 12:00 -           - Trata acentua��o ao verificar o tipo de arquivo para GPS e PIS (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
MR - 29/01/2015 12:00 -           - Seta tipos de pagamento considerando o apenas Tributo Eletr�nico Brandesco novo onde o CONCodigoComunicacaoTributos � obrigatorio (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
                                    Faz consist�ncias necess�rias para Tributo Eletr�nico Brandesco novo antes de setar envio
                                    No caso das DARFs, faz a separa��o de GPSTipo com PIS e IR (antes setava sempre como PIS)
                                    Retorno do Trib. Eletr. Bradesco apenas como yellow no grid de pagamentos
*/

using CompontesBasicosProc;
using CompontesBasicos;
using ContaPagar.Follow;
using ContaPagarProc;
using DevExpress.XtraReports.UI;
using dllCheques;
using dllChequesProc;
using dllImpostoNeon;
using dllImpostoNeonProc;
using DocBacarios;
using DPProc;
using Framework;
using FrameworkProc;
using FrameworkProc.datasets;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using VirMSSQL;
using VirEnumeracoes;
using VirEnumeracoesNeon;

namespace DP.Automacao_Bancaria
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAutomacaoBancaria : ComponenteBaseBindingSource
    {
        private int? _AgenciaCAIXA;
        private int? _AgenciaCAIXADV;

        private SortedList<int, int> _Apontar;

        private SortedList<int, int> _ApontarF;
               
        Boolean _CancelarExtracao = false;
               
        private CPFCNPJ _cnpjEmissor;
        private int? _CodigoCompromisso;
        private int? _ContaCAIXA;
        private string _ContaCAIXADV;
        private int? _convenioCAIXA;

        private dCheque _dCheque;
        private dNOtAs _dNOtAs;

        private DP.impressos.ImpRecSal _ImpRecSal;
        private int? _OperacaoContaCAIXA;
        private int? _ParametroTr;        

        //public bool UsarServidorDeProcessos = false;

        private SPeriodicoNota.PeriodicoNotaClient _Periodiconota;


        private dllImpresso.ImpProtocolo _Protocolo;

        bool BaixaManualLiberada = false;

        private int ContaApontamento = 126;       

        private string ConteudoTotal;

        private dAutomacaoBancaria dAutomacaoBancariaTarefas;
        int Inicio;

        private string[] Linhas;

        int Tamanho;
        int tamanhototal;


        //private TipoPagPeriodico? PeriodicoCarregado;



        //private DateTime ProximaMostra;







        /*
        private static int staticR;

        
        private byte[] Criptografa(int EMP, int USU)
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}{3:00000000}<-*WCF*->", DateTime.Now, EMP, USU, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }*/

        //private StringBuilder RelatorioDeTeste;



        bool Terminado;

        private string UltimoArquivo;

        private SortedList<int, int> USUEquivalentes;

        /// <summary>
        /// 
        /// </summary>
        public cAutomacaoBancaria()
        {
            InitializeComponent();
            textEdit1.EnterMoveNextControl = false;
            if (!DesignMode)
            {
                dAutomacaoBancaria.VirEnumnTipoPagPTRB.CarregaEditorDaGrid(colTipoPag1);
                dAutomacaoBancaria.VirEnumnTipoPagPTRB.CarregaEditorDaGrid(colTipoPag);
                pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas;
                uSUariosBindingSource.DataSource = dUSUarios.dUSUariosStTodos;
                virEnumRHTStatus.virEnumRHTStatusSt.CarregaEditorDaGrid(colRHTStatus);
                virEnumTipoBancaria.virEnumTipoBancariaSt.CarregaEditorDaGrid(colRHTTipo);                
                Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
            }
            dAutomacaoBancariaTarefas = new dAutomacaoBancaria();
            gridControlTarefas.DataSource = dAutomacaoBancariaTarefas;
            dataCorte.DateTime = DateTime.Today.AddMonths(-2);
        }

        /*
        private void Abortar(string men)
        {
            MessageBox.Show(men, "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            this.ViewPagamentos.BeginDataUpdate();
            dAutomacaoBancaria.RHTarefaSubdetalhe.Clear();
            dAutomacaoBancaria.RHTarefaDetalhe.Clear();
            this.ViewPagamentos.EndDataUpdate();
            dAutomacaoBancaria.RecarregarCondominios();
            Application.DoEvents();
        }*/

        private void AddApontamentos(int CON, int Q, int EMP)
        {

            if (ApontarX(EMP).ContainsKey(CON))
                ApontarX(EMP)[CON] = ApontarX(EMP)[CON] + Q;
            else
                ApontarX(EMP).Add(CON, Q);
        }


        private void AjustaGPS()
        {
            System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
            foreach (dAutomacaoBancaria.INSSRow row in dAutomacaoBancaria.INSS)
            {
                if (row.Banco != 237)
                    Apagar.Add(row);
                string manobra = "0";
                foreach (char c in row.CEP)
                    if (char.IsDigit(c))
                        manobra += c;
                row.nCEP = int.Parse(manobra);
                //DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(row.Campo5);
                //if (Cnpj.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                //{
                //    MessageBox.Show("CNPJ inv�lido(" + row.Campo3 + ") para " + row.Condominio);
                //    row.nCNPJ = 0;
                //}
                //else
                //    row.nCNPJ = Cnpj.Valor;
                //row.Apuracao = row.Campo6.AddDays(-row.Campo6.Day);
            };

            foreach (dAutomacaoBancaria.INSSRow row in Apagar)
                row.Delete();
        }



        private void AjustaPis()
        {
            System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
            foreach (dAutomacaoBancaria.PISRow row in dAutomacaoBancaria.PIS)
            {
                if (row.Banco != 237)
                    Apagar.Add(row);
                string manobra = "0";
                foreach (char c in row.CEP)
                    if (char.IsDigit(c))
                        manobra += c;
                row.nCEP = int.Parse(manobra);
                DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(row.Campo3);
                if (Cnpj.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                {
                    MessageBox.Show("CNPJ inv�lido(" + row.Campo3 + ") para " + row.Condominio);
                    row.nCNPJ = 0;
                }
                else
                    row.nCNPJ = Cnpj.Valor;
                row.Apuracao = row.Campo6.AddDays(-row.Campo6.Day);
            };

            foreach (dAutomacaoBancaria.PISRow row in Apagar)
                row.Delete();
        }

        internal enum StatusRHT
        {
            NaoAplicavel = 0,
            ok = 1,
            aguardo = 2,
            alerta = 3,
            vencido = 4,
            vencidoDefinitivo = 5,
            cancelado = 6
        }

        private void AjustaStatusRHT(dAutomacaoBancaria.RHTarefaRow RHTrow,bool Cheque,bool Compensado)
        {
            if (Cheque)
            {
                if (Compensado)
                {
                    if (RHTrow.StatusCheque == (int)StatusRHT.NaoAplicavel)
                        RHTrow.StatusCheque = (int)StatusRHT.ok;
                }
                else
                {
                    DateTime DataAvisoCheque = RHTrow.RHTDataCheque.SomaDiasUteis(3);
                    DateTime DataLimiteCheque = RHTrow.RHTDataCheque.SomaDiasUteis(4);
                    DateTime DataLimiteDefCheque = RHTrow.RHTDataCheque.SomaDiasUteis(10);
                    StatusRHT novo = StatusRHT.aguardo;
                    if (DateTime.Today >= DataLimiteDefCheque)
                        novo = StatusRHT.vencidoDefinitivo;
                    else if (DateTime.Today >= DataLimiteCheque)
                        novo = StatusRHT.vencido;
                    else if (DateTime.Today >= DataAvisoCheque)
                        novo = StatusRHT.alerta;
                    if (RHTrow.StatusCheque < (int)novo)
                        RHTrow.StatusCheque = (int)novo;
                }
            }
            else
            {
                if (Compensado)
                {
                    if (RHTrow.StatusCredito == (int)StatusRHT.NaoAplicavel)
                        RHTrow.StatusCredito = (int)StatusRHT.ok;
                }
                else
                {
                    DateTime DataAvisoCredito = RHTrow.RHTDataCredito.SomaDiasUteis(1);
                    DateTime DataLimiteCredito = RHTrow.RHTDataCredito.SomaDiasUteis(2);
                    DateTime DataLimiteDefCheque = RHTrow.RHTDataCheque.SomaDiasUteis(4);
                    StatusRHT novo = StatusRHT.aguardo;
                    if (DateTime.Today >= DataLimiteDefCheque)
                        novo = StatusRHT.vencidoDefinitivo;
                    else if (DateTime.Today >= DataLimiteCredito)
                        novo = StatusRHT.vencido;
                    else if (DateTime.Today >= DataAvisoCredito)
                        novo = StatusRHT.alerta;
                    if (RHTrow.StatusCredito < (int)novo)
                        RHTrow.StatusCredito = (int)novo;
                }
            }
        }

        private bool AjustaStatus()
        {
            //dAutomacaoBancariaTarefas.BuscaCHEStatusTableAdapter.Fill(dAutomacaoBancariaTarefas.BuscaCHEStatus);
            //dAutomacaoBancariaTarefas.BuscaCHEStatusTableAdapterF.Fill(dAutomacaoBancariaTarefas.BuscaCHEStatus);
            foreach (dAutomacaoBancaria.RHTarefaRow RHTrow in dAutomacaoBancariaTarefas.RHTarefa)
            {
                RHTrow.USU = RHTrow.EMP == DSCentral.EMP ? RHTrow.RHTI_USU : USUEquivalente(RHTrow.RHTI_USU);
                RHTrow.NCondominios = RHTrow.GetRHTarefaDetalheRows().Length;
                RHTrow.StatusCheque = RHTrow.StatusCredito = (int)StatusRHT.NaoAplicavel;
                RHTrow.AcceptChanges();
                RHTStatus NovoStatus = (RHTStatus)RHTrow.RHTStatus;
                bool TemNGerada = false;
                bool TemGerada = false;
                bool TemCancelada = false;
                bool TemComcluido = false;
                
                

                foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in RHTrow.GetRHTarefaDetalheRows())
                {
                    decimal TotalD = 0;
                    int QTDD = 0;
                    foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in RHDrow.GetRHTarefaSubdetalheRows())
                    //foreach (dAutomacaoBancaria.BuscaCHEStatusRow rowBusca in RHDrow.GetBuscaCHEStatusRows())
                    {
                        TotalD += RHSrow.RHSValor;
                        QTDD += 1;
                        if (RHSrow.IsCHEStatusNull())
                            TemNGerada = true;
                        else
                            switch ((CHEStatus)RHSrow.CHEStatus)
                            {
                                case CHEStatus.Antigo:
                                case CHEStatus.NaoEncontrado:
                                case CHEStatus.Cancelado:
                                case CHEStatus.PagamentoCancelado:
                                case CHEStatus.PagamentoNaoAprovadoSindicoPE:
                                case CHEStatus.PagamentoInconsistentePE:
                                case CHEStatus.PagamentoNaoEfetivadoPE:
                                    TemCancelada = true;
                                    break;
                                case CHEStatus.Cadastrado:
                                case CHEStatus.AguardaRetorno:
                                case CHEStatus.Aguardacopia:
                                case CHEStatus.Retornado:
                                case CHEStatus.EnviadoBanco:
                                case CHEStatus.Retirado:
                                case CHEStatus.DebitoAutomatico:
                                case CHEStatus.Caixinha:
                                case CHEStatus.Eletronico:
                                case CHEStatus.ComSindico:
                                case CHEStatus.AguardaEnvioPE:
                                case CHEStatus.AguardaConfirmacaoBancoPE:
                                case CHEStatus.PagamentoConfirmadoBancoPE:
                                case CHEStatus.CadastradoBloqueado:
                                    TemGerada = true;
                                    AjustaStatusRHT(RHTrow, (nTipoPag)RHSrow.RHSTipoPag == nTipoPag.Cheque, false);
                                    //checa a data passando para aguardo, aviso ou vencido
                                    break;
                                case CHEStatus.Compensado:
                                case CHEStatus.CompensadoAguardaCopia:
                                case CHEStatus.BaixaManual:
                                    TemComcluido = true;
                                    AjustaStatusRHT(RHTrow, (nTipoPag)RHSrow.RHSTipoPag == nTipoPag.Cheque, true);
                                    //se for naplicavael vai para ok
                                    break;
                                default:
                                    throw new NotImplementedException(string.Format("CheStatus n�o tratado: {0} em cAutomacaoBancaria.cs 3245", (CHEStatus)RHSrow.CHEStatus));
                            }
                    }
                    RHDrow.Total = TotalD;
                    RHDrow.QTD = QTDD;
                }
                if (TemNGerada)
                {
                    if (TemGerada || TemCancelada || TemComcluido)
                        NovoStatus = RHTStatus.GeradaParcial;
                    else
                        NovoStatus = RHTStatus.Cadastrada;
                }
                else
                {
                    if (TemGerada)
                        NovoStatus = RHTStatus.Gerada;
                    else if (TemComcluido)
                        NovoStatus = RHTStatus.Concluida;
                    else if (TemCancelada)
                        NovoStatus = RHTStatus.Cancelada;
                }
                if (RHTrow.RHTStatus != (int)NovoStatus)
                    RHTrow.RHTStatus = (int)NovoStatus;

                if (RHTrow.RowState != DataRowState.Unchanged)
                {
                    if (RHTrow.EMP == DSCentral.EMP)
                        dAutomacaoBancariaTarefas.RHTarefaTableAdapter.Update(RHTrow);
                    else
                        dAutomacaoBancariaTarefas.RHTarefaTableAdapterF.Update(RHTrow);
                }
            }
            bool Pendentes = false;
            foreach (dAutomacaoBancaria.RHTarefaRow RHTrow in dAutomacaoBancariaTarefas.RHTarefa)
                if (((RHTStatus)RHTrow.RHTStatus).EstaNoGrupo(RHTStatus.Cadastrada, RHTStatus.GeradaParcial))
                    Pendentes = true;
            return Pendentes;
        }

        private SortedList<int, int> ApontarX(int EMP)
        {
            return EMP == DSCentral.EMP ? Apontar : ApontarF;
        }

        private void BaxaTodos(object sender, EventArgs e)
        {
            bool Liberado = false;
            string Leitura = string.Empty;
            if (VirInput.Input.Execute("DIGITE 'BAIXAR TODOS'", ref Leitura, false))
                if (Leitura.ToUpper().Trim() == "BAIXAR TODOS")
                    Liberado = true;
            if (!Liberado)
                return;

            foreach (impressos.dRecSal.HOLeritesRow rowHOL in dRecSal.HOLerites)
                if (rowHOL.IsHOLRetornoNull())
                {
                    rowHOL.HOLRetorno = DateTime.Today;
                    dRecSal.HOLeritesTableAdapterX(rowHOL.EMP).Update(rowHOL);
                    rowHOL.AcceptChanges();
                }
        }





        private void btnIniciarExtracao_Click(object sender, EventArgs e)
        {
            this.edtArquivo.ErrorText = string.Empty;
            if (this.edtArquivo.Text.Trim() == string.Empty)
            {
                this.edtArquivo.ErrorText = "Informe ou selecione um arquivo no formato RTF.";
                MessageBox.Show("Informe ou selecione um arquivo no formato RTF.", "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.btnIniciarExtracao.Enabled = false;
            this.labZoom.Enabled = false;
            this.edtZoomRTF.Enabled = false;
            this.btnMaisZoom.Enabled = false;
            this.btnMenosZoom.Enabled = false;
            this.pgbExtracao.Visible = true;
            this.btnCancelarExtracao.Visible = true;

            if (edtTipoArquivo.Text == "PAGAMENTOS")
                ExtrairPagamentos();
            else
                    if (edtTipoArquivo.Text == "GPS")
                ExtrairGPS();
            else
                        if (edtTipoArquivo.Text == "PIS")
                ExtrairPIS();
            else
                ExtrairDemonstrativo();

            this.btnIniciarExtracao.Enabled = true;
            this.labZoom.Enabled = true;
            this.edtZoomRTF.Enabled = true;
            this.btnMaisZoom.Enabled = true;
            this.btnMenosZoom.Enabled = true;
            this.pgbExtracao.Visible = false;
            this.btnCancelarExtracao.Visible = false;

        }

        private void btnMaisZoom_Click(object sender, EventArgs e)
        {
            this.edtZoomRTF.Text = Convert.ToDouble((Convert.ToDouble(this.edtZoomRTF.Text) + 0.1)).ToString("n1");

            this.ArquivoRTF.ZoomFactor = float.Parse(this.edtZoomRTF.Text);
        }

        private void btnMenosZoom_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(this.edtZoomRTF.Text) > 0.5)
                this.edtZoomRTF.Text = Convert.ToDouble((Convert.ToDouble(this.edtZoomRTF.Text) - 0.1)).ToString("n1");

            this.ArquivoRTF.ZoomFactor = float.Parse(this.edtZoomRTF.Text);
        }

        private void btnRemessaGPS_Click(object sender, EventArgs e)
        {
            NovoTradutoGPS();
        }



        private void btnRemessaPIS_Click(object sender, EventArgs e)
        {
            NovoTradutoPIS();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in dAutomacaoBancaria.RHTarefaSubdetalhe)
                    if (RHSrow.EMP == DSCentral.EMP)
                        RHSrow.RHSValor = 0;
                dAutomacaoBancaria.RHTarefaSubdetalheTableAdapter.Update(dAutomacaoBancaria.RHTarefaSubdetalhe);
                foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in dAutomacaoBancaria.RHTarefaSubdetalhe)
                    if (RHSrow.EMP == DSCentral.EMPF)
                        RHSrow.RHSValor = 0;
                dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterF.Update(dAutomacaoBancaria.RHTarefaSubdetalhe);
            }
        }





        private void CarregarParaImprimir(DP.impressos.dRecSal.HOLeritesRow Linha)
        {
            impressos.dRecSal.RecibosRow NovaLinha = ImpRecSal.dRecSal.Recibos.NewRecibosRow();
            NovaLinha.OK = true;
            dAutomacaoBancaria.CONDOMINIOSRow rowCond = dAutomacaoBancaria.CONDOMINIOSX(Linha.EMP).FindByCON(Linha.HOL_CON);
            if (rowCond == null)
            {
                dAutomacaoBancaria.CONDOMINIOSDataTable DT = dAutomacaoBancaria.CONDOMINIOSTableAdapterX(DSCentral.EMPTipo(Linha.EMP)).GetDataByCONAntigo(Linha.HOL_CON);
                if (DT.Count > 0)
                    rowCond = DT[0];
            }
            if (rowCond == null)
            {
                MessageBox.Show("Condom�nio n�o encontrado");
                return;
            };

            NovaLinha.CONNome = rowCond.CONNome;
            NovaLinha.CONEndereco = rowCond.CONEndereco;
            NovaLinha.CON = rowCond.CON;
            NovaLinha.EMP = rowCond.CON_EMP;
            NovaLinha.UsoInterno = dAutomacaoBancaria.CondominioUsuInternto(rowCond);
            NovaLinha.DuasVias = Linha.HOLDuasvias;
            NovaLinha.HOL = Linha.HOL;
            NovaLinha.BOLEndereco = Linha.HOLEndereco;
            NovaLinha.BOLNome = Linha.HOLNome;

            NovaLinha.Cod = Linha.HOLCod;
            NovaLinha.Descricao = Linha.HOLDescricao;
            NovaLinha.Ref = Linha.HOLRef;
            NovaLinha.Venc = Linha.HOLVenc;
            NovaLinha.Descontos = Linha.HOLDescontos;
            NovaLinha.Q0 = Linha.HOLQ0;
            NovaLinha.Q1 = Linha.HOLQ1;
            NovaLinha.Q2 = Linha.HOLQ2;
            NovaLinha.Q3 = Linha.HOLQ3;
            NovaLinha.QOutros = Linha.HOLQOutros;
            NovaLinha.Qb1 = Linha.HOLb1;
            NovaLinha.Qb2 = Linha.HOLb2;
            NovaLinha.Qb3 = Linha.HOLb3;
            NovaLinha.Qb4 = Linha.HOLb4;
            NovaLinha.Qb5 = Linha.HOLb5;
            NovaLinha.Qtot1 = Linha.HOLQTot1;
            NovaLinha.Qtot2 = Linha.HOLQTot2;
            NovaLinha.Qtot3 = Linha.HOLQTot1 - Linha.HOLQTot2;
            ImpRecSal.dRecSal.Recibos.AddRecibosRow(NovaLinha);
        }

        private void CarregaTitulosHol()
        {
            if (ChNR.Checked)
            {
                dRecSal.HOleritTituloTableAdapter.Fill(dRecSal.HOleritTitulo);
                dRecSal.HOleritTituloTableAdapterF.Fill(dRecSal.HOleritTitulo);
            }
            else
            {
                dRecSal.HOleritTituloTableAdapter.FillByTODOS(dRecSal.HOleritTitulo);
                dRecSal.HOleritTituloTableAdapterF.FillByTODOS(dRecSal.HOleritTitulo);
            }
        }

        private void cAutomacaoBancaria_cargaFinal(object sender, EventArgs e)
        {
            textEdit1.EnterMoveNextControl = false;
        }

        private void cAutomacaoBancaria_cargaPrincipal(object sender, EventArgs e)
        {
            CarregaTarefas();
        }

        private void cAutomacaoBancaria_Load(object sender, EventArgs e)
        {
            this.edtTipoArquivo.Text = string.Empty;
            this.edtArquivo.Focus();
            this.Dock = DockStyle.Fill;
            OcultarTabPageDados();
            pgbExtracao.Visible = false;
            btnCancelarExtracao.Visible = false;
            Application.DoEvents();
            //TabPagePagamentos.PageVisible = true;
            bool Pendentes = AjustaStatus();

            //TabControlAutomacao.SelectedTabPage = Pendentes ? TabTarefas : TabPageRTF;
            timerAviso.Enabled = Pendentes;
            //ValidaPagamentos();                
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            ImpRecSal.Apontamento(-1, -1);
            ImpRecSal.CreateDocument();
            switch (cBotaoImpBol1.BotaoClicado)
            {
                case dllImpresso.Botoes.Botao.PDF_frente:
                    ImpRecSal.ComRemetente = false;
                    break;
                case dllImpresso.Botoes.Botao.email:
                    break;
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.imprimir:
                    ImpRecSal.ComRemetente = true;
                    ImpRecSal.Print();
                    foreach (int CON in Apontar.Keys)
                        Framework.DSCentral.IMPressoesTableAdapter.Incluir(CON, ContaApontamento, Apontar[CON], DateTime.Now, DSCentral.USU, "Autom�tico DP", false);
                    foreach (int CON in ApontarF.Keys)
                        Framework.DSCentral.IMPressoesTableAdapterF.Incluir(CON, ContaApontamento, ApontarF[CON], DateTime.Now, DSCentral.USUFilial, "Autom�tico DP", false);
                    break;
                case dllImpresso.Botoes.Botao.imprimir_frente:
                    ImpRecSal.ComRemetente = false;
                    ImpRecSal.Print();
                    break;
                case dllImpresso.Botoes.Botao.nulo:
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    ImpRecSal.ComRemetente = true;
                    ImpRecSal.ShowPreviewDialog();
                    break;
                default:
                    break;
            }
        }

        private void cBotaoImpBol2_clicado(object sender, EventArgs e)
        {
            if (ImpRecSal.dRecSal.Recibos.Rows.Count == 0)
                return;
            Protocolo.bindingSource1.Sort = "PESNome";
            Protocolo.SetDocumento("Demonstrativo de pagamento");
            Protocolo.Apontamento(ImpRecSal.dRecSal.Recibos[0].CON, ContaApontamento);
            string Condominio = ImpRecSal.dRecSal.Recibos[0].CONNome;
            Protocolo.SetNome(Condominio);
            Protocolo.dImpProtocolo.DadosProt.Clear();
            foreach (impressos.dRecSal.RecibosRow row in ImpRecSal.dRecSal.Recibos)
            {
                if (Condominio != row.CONNome)
                {
                    Protocolo.CreateDocument();
                    Protocolo.ShowPreviewDialog();
                    Protocolo.dImpProtocolo.DadosProt.Clear();
                    Protocolo.Apontamento(row.CON, ContaApontamento);
                    Condominio = row.CONNome;
                };
                Protocolo.dImpProtocolo.DadosProt.AddDadosProtRow(string.Empty, row.BOLNome, "Entrega Pessoal", string.Empty, string.Empty, string.Empty, "Bloco", (int)FormaPagamento.EntreigaPessoal, 0);
            };
            Protocolo.CreateDocument();
            switch (cBotaoImpBol2.BotaoClicado)
            {
                case dllImpresso.Botoes.Botao.PDF_frente:
                    break;
                case dllImpresso.Botoes.Botao.email:
                    break;
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.imprimir_frente:
                    Protocolo.Print();
                    break;
                case dllImpresso.Botoes.Botao.nulo:
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    Protocolo.ShowPreviewDialog();
                    break;
                default:
                    break;
            }



        }

        private void cBotaoImpBol3_clicado(object sender, EventArgs e)
        {
            ImpRecSal.dRecSal.Recibos.Clear();
            foreach (DP.impressos.dRecSal.HOLeritesRow Linha in dRecSal.HOLerites)
                CarregarParaImprimir(Linha);
            ImpRecSal.Apontamento(-1, -1);
            ImpRecSal.CreateDocument();
            switch (cBotaoImpBol3.BotaoClicado)
            {
                case dllImpresso.Botoes.Botao.PDF_frente:
                    ImpRecSal.ComRemetente = false;
                    break;
                case dllImpresso.Botoes.Botao.email:
                    break;
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.imprimir:
                    ImpRecSal.ComRemetente = true;
                    ImpRecSal.Print();
                    foreach (int CON in Apontar.Keys)
                        Framework.DSCentral.IMPressoesTableAdapter.Incluir(CON, ContaApontamento, Apontar[CON], DateTime.Now, DSCentral.USU, "Autom�tico DP", false);
                    foreach (int CON in ApontarF.Keys)
                        Framework.DSCentral.IMPressoesTableAdapterF.Incluir(CON, ContaApontamento, ApontarF[CON], DateTime.Now, DSCentral.USUFilial, "Autom�tico DP", false);
                    break;
                case dllImpresso.Botoes.Botao.imprimir_frente:
                    ImpRecSal.ComRemetente = false;
                    ImpRecSal.Print();
                    break;
                case dllImpresso.Botoes.Botao.nulo:
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    ImpRecSal.ComRemetente = true;
                    ImpRecSal.ShowPreviewDialog();
                    break;
                default:
                    break;
            }
        }

        private void cBotaoImpBol4_clicado(object sender, EventArgs e)
        {
            ImpRecSal.dRecSal.Recibos.Clear();
            if (LinhaMae != null)
            {
                CarregarParaImprimir(LinhaMae);
                ImpRecSal.Apontamento(LinhaMae.HOL_CON, ContaApontamento);
                ImpRecSal.CreateDocument();
                //ImpRecSal.ShowPreviewDialog();
                ImpRecSal.Print();
                switch (cBotaoImpBol4.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.PDF_frente:
                        ImpRecSal.ComRemetente = false;
                        break;
                    case dllImpresso.Botoes.Botao.email:
                        break;
                    case dllImpresso.Botoes.Botao.imprimir:
                        ImpRecSal.ComRemetente = true;
                        ImpRecSal.Print();
                        break;
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                        ImpRecSal.ComRemetente = false;
                        ImpRecSal.Print();
                        break;
                    case dllImpresso.Botoes.Botao.nulo:
                        break;
                    case dllImpresso.Botoes.Botao.pdf:
                        break;
                    case dllImpresso.Botoes.Botao.botao:
                    case dllImpresso.Botoes.Botao.tela:
                        ImpRecSal.ComRemetente = true;
                        ImpRecSal.ShowPreviewDialog();
                        break;
                    default:
                        break;
                }
            }
        }





        private void ChNR_CheckedChanged(object sender, EventArgs e)
        {
            //dRecSal.HOLerites.Clear();
            CarregaTitulosHol();
        }

        private void CorrecAuto(object sender, EventArgs e)
        {
            decimal AjusteMaximo = 10;
            if (VirInput.Input.Execute("Valor m�ximo:", ref AjusteMaximo))
                foreach (dAutomacaoBancaria.INSSRow row in dAutomacaoBancaria.INSS)
                {
                    if (row.IsErroValorNull())
                        continue;
                    if ((row.ErroValor != 0) && (Math.Abs(row.ErroValor) < AjusteMaximo))
                        if (row.IsCampo6Null())
                            row.NEON += row.ErroValor;
                        else
                            row.Campo6 += row.ErroValor;
                }
        }

        /*
        [Obsolete]
        private dNOtAs.NOtAsRow GetrowNOA(dAutomacaoBancaria.SalariosRow rowsal)
        { 
            if(NoasList == null)
                NoasList = new System.Collections.SortedList();
            if (NoasList.ContainsKey(rowsal.CON__EMP))
                return (dNOtAs.NOtAsRow)NoasList[rowsal.CON__EMP];
            else{
                        dNOtAs.NOtAsRow rowNOA = dNOtAs.NOtAs.NewNOtAsRow();
                        rowNOA.NOA_CON = rowsal.CON__EMP;
                        switch (tipoBancaria)
                        {
                            case TipoBancaria.Pagamento:
                                rowNOA.NOA_PLA = "220001";
                                rowNOA.NOAServico = "Sal�rio";
                                break;
                            case TipoBancaria.Adiantamento:
                                rowNOA.NOA_PLA = "220002";
                                rowNOA.NOAServico = "Adiantamento";
                                break;
                            case TipoBancaria.DecimoTerceiro:
                                rowNOA.NOA_PLA = "220017";
                                rowNOA.NOAServico = "13.Sal�rio";
                                break;
                            default:
                                break;
                        }    
                        
                        rowNOA.NOADataEmissao = DateTime.Today;
                        rowNOA.NOACompet = rowNOA.NOADataEmissao.Year * 100 + rowNOA.NOADataEmissao.Month;
                        rowNOA.NOAAguardaNota = false;
                        rowNOA.NOADATAI = DateTime.Now;
                        rowNOA.NOAI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                        rowNOA.NOANumero = 0;
                        
                        rowNOA.NOAStatus = (int)NOAStatus.Cadastrada;
                        rowNOA.NOATipo = (int)NOATipo.Folha;
                        rowNOA.NOATotal = 0;
                        rowNOA.NOADefaultPAGTipo = (int)PAGTipo.cheque;
                        dNOtAs.NOtAs.AddNOtAsRow(rowNOA);
                        dNOtAs.NOtAsTableAdapter.Update(rowNOA);
                        rowNOA.AcceptChanges();
                        return rowNOA;
            }
        }
        */



        private void CriaCheques(DateTime DataParaDebito, TipoPagPeriodico TipoPadrao)
        {
            //NoasList = null;
            int contador = 0;
            dAutomacaoBancaria.RHTarefaSubdetalheRow ultimaLinha = null;
            try
            {
                contador = 0;
                //ajustar
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("DP cAutomacaoBancaria - 1617",
                                                                        dNOtAs.NOtAsTableAdapter,
                                                                        dNOtAs.PAGamentosTableAdapter);
                foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in dAutomacaoBancaria.RHTarefaSubdetalhe)
                    if ((nTipoPag)RHSrow.RHSTipoPag == nTipoPag.Cheque)
                        if (RHSrow.RHSValor > 0)
                        {
                            contador++;
                            ultimaLinha = RHSrow;
                            /*
                            PagamentoPeriodico PagamentoPeriodico = Periodicos[rowsal.CON__EMP];


                            if (!PagamentoPeriodico.Encontrado)
                                PagamentoPeriodico.CadastraPGF(rowsal.CON__EMP,
                                                               TipoPadrao,
                                                               DataParaDebito,
                                                               rowsal.FUNValor,new Framework.objetosNeon.Competencia() );

                            

                            PagamentoPeriodico.CadastraProximoPagamento(DataParaDebito, DataParaDebito, rowsal.FUNValor, new Framework.objetosNeon.Competencia());
                            */



                            dAutomacaoBancaria.RHTarefaDetalheRow RHDrow = RHSrow.RHTarefaDetalheRow;
                            Cheque NovoCheque = new Cheque(DataParaDebito, RHSrow.FUCNome, RHDrow.RHD_CON, PAGTipo.sindico, false, 0);

                            /*
                            dNOtAs.NOtAsRow rowNOA = GetrowNOA(rowsal);
                            rowNOA.NOATotal += rowsal.FUNValor;
                            dNOtAs.NOtAsTableAdapter.Update(rowNOA);
                            rowNOA.AcceptChanges();

                            dNOtAs.PAGamentosRow rowPag = dNOtAs.PAGamentos.NewPAGamentosRow();
                            rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                            rowPag.PAG_NOA = rowNOA.NOA;
                            rowPag.PAGDATAI = DateTime.Now;
                            rowPag.PAGI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                            rowPag.PAGTipo = (int)PAGTipo.sindico;
                            rowPag.PAGValor = rowsal.FUNValor;
                            rowPag.PAGVencimento = DataParaDebito;
                            rowPag.PAGPermiteAgrupar = true;
                            dNOtAs.PAGamentos.AddPAGamentosRow(rowPag);
                            dNOtAs.PAGamentosTableAdapter.Update(rowPag);
                            rowPag.AcceptChanges();
                            NovoCheque.AjustarTotalPelasParcelas();
                            */
                        };

                VirMSSQL.TableAdapter.ST().Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.ST().Vircatch(e);
                try
                {
                    MessageBox.Show(string.Format("Erro na geracao dos cheques\r\n Dados:\r\nContador = {0}\r\nCondominio:{1}\r\nNominal:{2})\r\n Os cheques n�o foram gravados\r\n{3}",
                                         contador,
                                         ultimaLinha.RHTarefaDetalheRow.CONCodigo,
                                         ultimaLinha.FUCNome,
                                         e.Message));
                }
                catch
                {
                    MessageBox.Show(string.Format("Erro na geracao dos cheques\r\n Dados:\r\nContador = {0}\r\n Os cheques n�o foram gravados\r\n{1}",
                                         contador,
                                         e.Message));
                }
                throw new Exception("Erro Gerando cheques: " + e.Message, e);
            }
        }







        private void edtArquivo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                dAutomacaoBancaria.RHTarefaSubdetalhe.Clear();
                dAutomacaoBancaria.RHTarefaDetalhe.Clear();
                OcultarTabPageDados();
                this.edtTipoArquivo.Text = string.Empty;
                this.edtZoomRTF.Text = "1,0";
                //this.Validate();
                this.edtArquivo.EditValue = openFileDialog.FileName;
                this.Validate();
                this.ArquivoRTF.Clear();
                this.edtArquivo.ErrorText = string.Empty;
                if ((!File.Exists(this.openFileDialog.FileName)) || (new FileInfo(this.openFileDialog.FileName).Extension != ".rtf"))
                {
                    //this.Cursor = Cursors.Default;
                    this.edtArquivo.ErrorText = "O arquivo informado n�o existe ou n�o � um arquivo no formato v�lido (RTF).";
                    MessageBox.Show("O arquivo informado n�o existe ou n�o � um arquivo no formato v�lido (RTF).", "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                try
                {
                    this.ArquivoRTF.LoadFile(openFileDialog.FileName);
                    UltimoArquivo = Path.GetFileName(openFileDialog.FileName);
                    ConteudoTotal = this.ArquivoRTF.Text;
                    if ((ConteudoTotal.IndexOf("RELACAO BANCARIA / CAIXA") >= 0)
                          ||
                        (ConteudoTotal.IndexOf("RELA��O BANC�RIA / CAIXA") >= 0)
                          ||
                        (ConteudoTotal.IndexOf("RELA++O BANC+RIA / CAIXA") >= 0)
                       )
                        edtTipoArquivo.Text = "PAGAMENTOS";
                    else
                        //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                        if ((ConteudoTotal.IndexOf("GUIA DA PREVIDENCIA SOCIAL - GPS") >= 0)
                             ||
                            (ConteudoTotal.IndexOf("GUIA DA PREVID�NCIA SOCIAL - GPS") >= 0)
                             ||
                            (ConteudoTotal.IndexOf("GUIA DA PREVID+NCIA SOCIAL - GPS") >= 0)
                           )
                    //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
                    {
                        this.edtTipoArquivo.Text = "GPS";
                        this.TabPageGPS.PageVisible = true;
                    }
                    else
                            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                            if ((ConteudoTotal.IndexOf("MINISTERIO DA FAZENDA") >= 0)
                                  ||
                                (ConteudoTotal.IndexOf("MINIST�RIO DA FAZENDA") >= 0)
                                  ||
                                (ConteudoTotal.IndexOf("MINIST+RIO DA FAZENDA") >= 0)
                               )
                    //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
                    {
                        this.edtTipoArquivo.Text = "PIS";
                        this.TabPagePIS.PageVisible = true;

                        //this.pgbPIS.Visible = false;
                    }
                    else
                    {
                        this.edtTipoArquivo.Text = "FOLHA";
                        TabControlAutomacao.MakePageVisible(TabRecibos);
                        TabRecibos.PageVisible = true;
                    }

                    Application.DoEvents();

                    this.Cursor = Cursors.Default;
                }
                catch (Exception Erro)
                {


                    MessageBox.Show("Ocorreu um erro ao tentar ler o arquivo " + openFileDialog.FileName + ".\r\n\r\n" + Erro.Message.ToUpper(), "GPS", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    //this.ArquivoRTF.Clear();

                    this.edtTipoArquivo.Text = string.Empty;
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

            }
        }








        private void ExtrairDemonstrativo()
        {
            int LCompetencia = 0; //era 1            
            int CCompetencia = 35;
            int LCodFolha2 = 2;
            int LCNPJ = 3;
            int LNome = 5;
            int LitensI = 8;
            int LitensF = 24;
            int LDepositoRod = 25;
            int LBancoRod = 26;
            int LAgenciaRod = 27;
            int LContinua = 28;  //era 27
            int LResumo = 30;
            int LTotais1 = 26;
            int LTotais2 = 28;
            //int n = 0;
            int i = 0;
            dAutomacaoBancaria.CONDOMINIOSTableAdapter.Fill(dAutomacaoBancaria.CONDOMINIOS);
            if (this.ArquivoRTF.Lines.Length > 0)
                try
                {
                    
                    ImpRecSal.dRecSal.Recibos.Clear();
                    recibosBindingSource.DataSource = ImpRecSal.dRecSal;
                    tamanhototal = ConteudoTotal.Length;
                    this.pgbExtracao.Maximum = tamanhototal;
                    this.pgbExtracao.Value = 0;
                    Inicio = 0;
                    Tamanho = 0;
                    this.pgbExtracao.Value = 0;

                    this.btnCancelarExtracao.Visible = true;
                    this.pgbExtracao.Visible = true;

                    Linhas = ConteudoTotal.Split('\n');

                    //Ajuste de layout
                    if (Linhas[1].Trim().Length != 0)
                    {
                        LCodFolha2 = 1;
                        LCNPJ = 2;
                        LNome = 4;                        
                        LitensF = 23;
                        LDepositoRod = 24;
                        LBancoRod = 25;
                        LAgenciaRod = 26;
                        LContinua = 26;
                        LResumo = 28;
                        LTotais1 = 24;
                        LTotais2 = 26;
                    }
                    if (GetParte(0, 0, 38, 1) != "/")
                        CCompetencia = 29;

                    int Grupos = (int)Math.Truncate((decimal)(Linhas.Length / 33));

                    decimal vTot1;
                    decimal vTot2;
                    decimal v1;
                    decimal v2;
                    string UltimoErro = string.Empty;

                    dAutomacaoBancaria.CONDOMINIOSRow rowCond = null;



                    for (i = 0; i < Grupos; i++)
                    {
                        //if (i == 944)
                        //    MessageBox.Show("este");
                        vTot1 = vTot2 = 0;
                        int? CodFolha = null;
                        int tryCodFolha;
                        string CodFolha2 = null;
                        impressos.dRecSal.RecibosRow NovaLinha = ImpRecSal.dRecSal.Recibos.NewRecibosRow();
                        NovaLinha.OK = true;
                        CodFolha2 = GetParte(i, LCodFolha2, 2, 3);
                        if (int.TryParse(CodFolha2, out tryCodFolha))
                            CodFolha = tryCodFolha;

                        rowCond = dAutomacaoBancaria.BuscaCondominio(CodFolha, CodFolha2, true);
                        if (rowCond == null)
                        {
                            NovaLinha.OK = false;
                            UltimoErro = "Condom�nio com c�digo " + CodFolha2 + " n�o encontrado\r\n Recibo n�mero " + Grupos.ToString();
                            MessageBox.Show("Condom�nio com c�digo " + CodFolha2 + " n�o encontrado.", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Cursor = Cursors.Default;
                            return;
                        }

                        NovaLinha.UsoInterno = dAutomacaoBancaria.CondominioUsuInternto(rowCond);
                        NovaLinha.CONNome = rowCond.CONNome;
                        NovaLinha.CONEndereco = rowCond.CONEndereco;
                        NovaLinha.CON = rowCond.CON;
                        NovaLinha.EMP = rowCond.CON_EMP;
                        NovaLinha.DuasVias = ((!rowCond.IsCONCodigoFolha1Null()) && (rowCond.CONCodigoFolha1 == CodFolha));
                        AddApontamentos(rowCond.CON, 2, rowCond.CON_EMP);
                        if (rowCond.IsCONCnpjNull() || (rowCond.CONCnpj != GetParte(i, LCNPJ, 35, 20)))
                        {
                            NovaLinha.OK = false;
                            UltimoErro = string.Format("Erro no CNPJ (Encontrado:<{0}> Cadastro:<{1}>)\r\n Recibo n�mero {2} - {3}\r\n Os recibos n�o ser�o gravados/impressos", GetParte(i, LCNPJ, 35, 20), rowCond.IsCONCnpjNull() ? string.Empty : rowCond.CONCnpj, Grupos, rowCond.CONNome);
                        }

                        //else {
                        //    NovaLinha.OK = false;
                        //    UltimoErro = string.Format("Erro no c�digo do condom�nio\r\n{0}\r\n Recibo n�mero:{1}", GetParte(i, 2, 2, 100), i);                            
                        //}
                        NovaLinha.BOLEndereco = GetParte(i, LCNPJ, 2, 33);
                        NovaLinha.BOLNome = GetParte(i, LNome, 11, 33);
                        string Codigo = "C�d.\r\n";
                        string Descricao = "Descri��o\r\n";
                        string Referencia = "Ref.\r\n";
                        string Vencimentos = "Vencimentos\r\n";
                        string Descontos = "Descontos\r\n";
                        bool Continua = true;

                        while (Continua)
                        {
                            for (int j = LitensI; j <= LitensF; j++)
                            {
                                string strCod = GetParte(i, j, 2, 4);
                                string strDes = GetParte(i, j, 6, 30);
                                string strRef = GetParte(i, j, 35, 8);
                                string strv1_ = GetParte(i, j, 43, 14);
                                string strv2_ = GetParte(i, j, 57, 14);
                                if ((strCod != " ") ||
                                   (strDes != " ") ||
                                   (strRef != " ") ||
                                   (strv1_ != " ") ||
                                   (strv2_ != " "))
                                {

                                    Codigo += "\r\n" + strCod;
                                    Descricao += "\r\n" + strDes;
                                    Referencia += "\r\n" + strRef;
                                    if (decimal.TryParse(strv1_, out v1))
                                        vTot1 += v1;
                                    if (decimal.TryParse(strv2_, out v2))
                                        vTot2 += v2;
                                    Vencimentos += "\r\n" + strv1_;
                                    Descontos += "\r\n" + strv2_;
                                }

                            };
                            if (GetParte(i, LContinua, 58, 8) == "CONTINUA")
                                i++;
                            else
                                Continua = false;
                        };
                        NovaLinha.Cod = Codigo;
                        NovaLinha.Descricao = Descricao;
                        NovaLinha.Ref = Referencia;
                        NovaLinha.Venc = Vencimentos;
                        NovaLinha.Descontos = Descontos;
                        NovaLinha.Q0 = GetParte(i, LCompetencia, CCompetencia, 10);
                        NovaLinha.Q1 = GetParte(i, LCodFolha2, 1, 33);
                        NovaLinha.Q2 = GetParte(i, LCNPJ, 1, 33) + "   " + GetParte(i, LCNPJ, 35, 20);

                        NovaLinha.Q3 = GetParte(i, LNome, 1, 70);
                        string StrOutros = GetParte(i, LDepositoRod, 2, 46);
                        StrOutros += "\r\n" + GetParte(i, LBancoRod, 2, 46);
                        StrOutros += "\r\n" + GetParte(i, LAgenciaRod, 2, 46);
                        NovaLinha.QOutros = StrOutros;
                        NovaLinha.Qb1 = GetParte(i, LResumo, 13, 12);
                        NovaLinha.Qb2 = GetParte(i, LResumo, 26, 12);
                        NovaLinha.Qb3 = GetParte(i, LResumo, 38, 12);
                        NovaLinha.Qb4 = GetParte(i, LResumo, 50, 12);
                        NovaLinha.Qb5 = GetParte(i, LResumo, 67, 2);
                        try
                        {
                            NovaLinha.Qtot1 = decimal.Parse(GetParte(i, LTotais1, 43, 14));
                            NovaLinha.Qtot2 = decimal.Parse(GetParte(i, LTotais1, 57, 14));
                            NovaLinha.Qtot3 = decimal.Parse(GetParte(i, LTotais2, 57, 14));                            
                        }
                        catch
                        {
                            NovaLinha.Qtot1 = 0;
                            NovaLinha.Qtot2 = 0;
                            NovaLinha.Qtot3 = 0;
                            NovaLinha.OK = false;
                        };
                        if ((NovaLinha.Qtot1 != vTot1) || (NovaLinha.Qtot2 != vTot2) || (NovaLinha.Qtot3 != (vTot1 - vTot2)))
                        {
                            NovaLinha.OK = false;
                            UltimoErro = "Valores n�o batem\r\n Recibo n�mero " + Grupos.ToString();
                        }
                        ImpRecSal.dRecSal.Recibos.AddRecibosRow(NovaLinha);
                    }

                    this.btnCancelarExtracao.Visible = false;
                    this.pgbExtracao.Visible = false;

                    this.Cursor = Cursors.Default;
                    this.TabControlAutomacao.SelectedTabPage = TabRecibos;
                    if (UltimoErro != string.Empty)
                        MessageBox.Show(UltimoErro);
                }
                catch (Exception Erro)
                {
                    //this.Cursor = Cursors.Default;
                    //MessageBox.Show("Ocorreu uma excess�o ao tentar extrair os dados do arquivo " + this.openFileDialog.FileName + ".\r\n\r\n" + Erro.Message + "\r\n(" + n.ToString() + ")", "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.btnCancelarExtracao.Visible = false;
                    this.pgbExtracao.Visible = false;
                    throw new Exception("Erro na extra��o dos dados do Holerite\r\nArquivo:" + this.openFileDialog.FileName + "\r\nGrupo:" + i.ToString() + "\r\n", Erro);
                }
        }

        private void ExtrairGPS()
        {
            if (this.ArquivoRTF.Lines.Length > 0)
                using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                {
                    Esp.Motivo.Text = "Carregando RTF";
                    Application.DoEvents();
                    //dAutomacaoBancaria.EnforceConstraints = false;
                    Framework.objetosNeon.Competencia comp = null;

                    this.dAutomacaoBancaria.INSS.Clear();

                    //ConteudoTotal = this.ArquivoRTF.Text;
                    tamanhototal = ConteudoTotal.Length;
                    this.pgbExtracao.Maximum = tamanhototal;
                    this.pgbExtracao.Value = 0;
                    Inicio = 0;
                    Tamanho = 0;
                    this.pgbExtracao.Value = 0;

                    //this.ViewGPS.BeginDataUpdate();

                    String sLinha = string.Empty;

                    String CampoDecimal = string.Empty;

                    String sCONErro = string.Empty;

                    //DataRowView RowGPS = null;
                    dAutomacaoBancaria.INSSRow RowGPS = null;

                    this.btnCancelarExtracao.Visible = true;
                    this.pgbExtracao.Visible = true;

                    this.dAutomacaoBancaria.CONDOMINIOS.Clear();
                    //dAutomacaoBancaria.CONDOMINIOSTableAdapter.Fill(this.dAutomacaoBancaria.CONDOMINIOS);

                    Terminado = false;
                    int nGuias = 0;
                    while (!Terminado)
                    {
                        Application.DoEvents();

                        if (this._CancelarExtracao)
                        {
                            this.Cursor = Cursors.Default;
                            this.dAutomacaoBancaria.INSS.Clear();
                            this.btnCancelarExtracao.Visible = false;
                            this.pgbExtracao.Visible = false;
                            //this.ViewGPS.EndDataUpdate();

                            return;
                        }

                        this.pgbExtracao.Value = Inicio;

                        //Inicio de uma GPS
                        //sLinha = this.ArquivoRTF.Lines[iLinha];
                        //sLinha = ProximaLinha();
                        if (sLinha.IndexOf("3-Cod.pagto") >= 0)
                        {
                            string sLinhaPag = sLinha;
                            string sLinhacompet = ProximaLinha(2);
                            string sLinhaIDENT = ProximaLinha(2);
                            string sLinhaValor = ProximaLinha(2);

                            //Extraindo codigo do condominio na GPS e verificando
                            //se o cadastro do mesmo existe na tabela condominios
                            //atraves do campo CONCodigoFolha1

                            String sCondominio = sLinhaValor.Substring(1, 45);

                            //String sCodigoFolha1 = Convert.ToInt32(sLinhaValor.Substring(1, 4).Replace(" ", "")).ToString();
                            string CNPJ = sLinhaIDENT.Substring(61, 18).Replace(" ", string.Empty);

                            //DataRowView DRV = ((DataRowView)(this.iNSSBindingSource.AddNew()));
                            //RowGPS = (dAutomacaoBancaria.INSSRow)DRV.Row;
                            RowGPS = dAutomacaoBancaria.INSS.NewINSSRow();
                            RowGPS.BeginEdit();
                            //RowGPS.SEFIP = 0;
                            RowGPS.NEON = 0;
                            Int32 CodFolha = 0;
                            RowGPS.TipoPag = (int)nTipoPag.Invalido;
                            string strCodFolha = sLinhaValor.Substring(1, 4).Trim();
                            if (!Int32.TryParse(strCodFolha, out CodFolha))
                                RowGPS["Erro"] = "C�digo folha inv�lido:<" + sLinhaValor.Substring(1, 4) + ">";
                            else
                            {
                                dAutomacaoBancaria.CONDOMINIOSRow CONrow = dAutomacaoBancaria.BuscaCondominio(CodFolha, null, true);
                                if (CONrow == null)
                                {
                                    MessageBox.Show(string.Format("Nenhum Condom�nio encontrado com o C�digo {0}", CodFolha));
                                    RowGPS["Erro"] = "Nenhum Condom�nio encontrado com o C�digo " + CodFolha.ToString();
                                    RowGPS["BANCO"] = 0;
                                    return;
                                }
                                else
                                {
                                    RowGPS["CON"] = CONrow.CON;
                                    //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
                                    //if (!CONrow.IsCONCodigoComunicacaoTributosNull())
                                    RowGPS["CodComunicacao"] = "117045";
                                    //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***    
                                    if (CONrow.IsCONCnpjNull())
                                    {
                                        MessageBox.Show(string.Format("Erro:\r\nCondom�nio {0} - {1} sem CNPJ", CONrow.CONNome, CONrow.CONCodigo));
                                        return;
                                    }
                                    RowGPS.nCNPJ = new DocBacarios.CPFCNPJ(CONrow.CONCnpj).Valor;
                                    DocBacarios.CPFCNPJ cnpjLido = new DocBacarios.CPFCNPJ(CNPJ);
                                    if (cnpjLido.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                                        RowGPS["Erro"] = "CNPJ inv�lido:" + CNPJ;
                                    else
                                        if (CONrow.IsCONCnpjNull())
                                        RowGPS["Erro"] = "CNPJ n�o cadastrado!";
                                    else
                                            if (sLinhaPag.Substring(61, 18).Replace(" ", string.Empty) != "2631")
                                        if (cnpjLido.Valor != new DocBacarios.CPFCNPJ(CONrow.CONCnpj).Valor)
                                            RowGPS["Erro"] = "CNPJ n�o confere:" + CNPJ + " x " + CONrow.CONCnpj;
                                    ;
                                    try
                                    {
                                        RowGPS.EMP = CONrow.CON_EMP;
                                        RowGPS["Agencia"] = int.Parse(CONrow.CONAgencia);
                                        RowGPS["Conta"] = int.Parse(CONrow.CONConta);
                                        //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
                                        RowGPS["DigAgencia"] = CONrow.CONDigitoAgencia;
                                        RowGPS["DigConta"] = CONrow.CONDigitoConta;
                                        //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) *** 
                                        RowGPS["BANCO"] = CONrow.CON_BCO;
                                        if (CONrow.CONCodigoFolha1 != CodFolha)
                                            RowGPS.TipoPag = (int)nTipoPag.NaoEmitir;
                                        else
                                            if ((CONrow.CON_BCO == 237) && (CONrow.CONPagEletronico))
                                            RowGPS.TipoPag = (int)nTipoPag.CreditoConta;
                                        else
                                            RowGPS.TipoPag = (int)nTipoPag.Cheque;
                                        //if ((!CONrow.IsCONPagEletronicoNull()) && (CONrow.CONPagEletronico))
                                        //{
                                        //    if (CONrow.CON_BCO == 237)
                                        //        RowGPS.TipoPag = (int)nTipoPag.EletronicoBradesco;
                                        //    else
                                        //        RowGPS.TipoPag = (int)nTipoPag.EletronicoItau;
                                        //}
                                        //else
                                        //    if (CONrow.CON_EMP != Framework.DSCentral.EMP)
                                        //        RowGPS.TipoPag = (int)nTipoPag.ChequeFilial;
                                        //    else
                                        //        RowGPS.TipoPag = (int)nTipoPag.Cheque;
                                        //*** MRC - TERMINO - TRIBUTOS (29/01/2015 12:00) ***
                                    }
                                    catch
                                    {
                                        RowGPS.Erro = "Erro no Banco/Ag�ncia/Conta do condom�nio";
                                        RowGPS["Agencia"] = RowGPS["Conta"] = RowGPS.Banco = 0;
                                    }
                                }
                            };

                            //Campo 3-Cod.pagto
                            RowGPS["Campo3"] = sLinhaPag.Substring(61, 18).Replace(" ", string.Empty);

                            //Campo 4-Competenc.
                            string Compet = sLinhacompet.Substring(61, 18).Replace(" ", string.Empty);
                            if (comp == null)
                            {
                                int mes = int.Parse(Compet.Substring(0, 2));
                                int ano = int.Parse(Compet.Substring(3, 4));
                                comp = new Framework.objetosNeon.Competencia(mes, ano);
                            };
                            RowGPS["Campo4"] = Compet;
                            RowGPS["MES"] = Compet.Substring(0, 2);
                            RowGPS["ANO"] = Compet.Substring(3, 4);

                            //Campo 5-Identific.
                            RowGPS["Campo5"] = sLinhaIDENT.Substring(61, 18).Replace(" ", string.Empty);

                            //Campo 6-Valor INSS + Dados da Empresa
                            RowGPS["Condominio"] = sLinhaValor.Substring(1, 4).Replace(" ", string.Empty);
                            //RowGPS["Endereco"] = drFoundRows[0]["CONEndereco"];
                            //RowGPS["Bairro"] = drFoundRows[0]["CONBairro"];
                            //RowGPS["Cidade"] = drFoundRows[0]["CIDNome"];
                            //RowGPS["UF"] = drFoundRows[0]["CIDUF"];
                            //RowGPS["CEP"] = drFoundRows[0]["CONCEP"];
                            //RowGPS["Campo1"] = drFoundRows[0]["CONNome"];
                            sLinha = ProximaLinha(null);
                            RowGPS["Endereco"] = sLinha.Substring(2, 44).Trim();
                            sLinha = ProximaLinha(null);
                            RowGPS["Bairro"] = sLinha.Substring(18, 28).Trim();
                            sLinha = ProximaLinha(null);
                            RowGPS["CEP"] = sLinha.Substring(2, 8).Trim();
                            RowGPS["Cidade"] = sLinha.Substring(12, 21).Trim();
                            RowGPS["UF"] = sLinha.Substring(34, 2).Trim();
                            RowGPS["Campo1"] = sLinhaValor.Substring(6, 39).Trim();
                            CampoDecimal = sLinhaValor.Substring(61, 18).Replace(" ", string.Empty);
                            CampoDecimal = (CampoDecimal.Trim() == string.Empty ? "0" : CampoDecimal);
                            RowGPS["Campo6"] = CampoDecimal;

                            //Campo 7 e 8 n�o importado

                            //Campo 9-Val.Outros 
                            sLinha = ProximaLinha(3);
                            CampoDecimal = sLinha.Substring(61, 18).Replace(" ", string.Empty);
                            CampoDecimal = (CampoDecimal.Trim() == string.Empty ? "0" : CampoDecimal);
                            RowGPS["Campo9"] = CampoDecimal;

                            //Campo 10-At.Monet/Juros/multa
                            sLinha = ProximaLinha(3);
                            CampoDecimal = sLinha.Substring(61, 18).Replace(" ", string.Empty);
                            CampoDecimal = (CampoDecimal.Trim() == string.Empty ? "0" : CampoDecimal);
                            RowGPS["Campo10"] = CampoDecimal;

                            //Campo 11-Total
                            sLinha = ProximaLinha(2);
                            CampoDecimal = sLinha.Substring(61, 18).Replace(" ", string.Empty);
                            CampoDecimal = (CampoDecimal.Trim() == string.Empty ? "0" : CampoDecimal);
                            RowGPS["Campo11"] = CampoDecimal;

                            //Verifica se deu erro e seta como invalido para correcao
                            if (!RowGPS.IsErroNull())
                                RowGPS.TipoPag = (int)nTipoPag.Invalido;

                            RowGPS.EndEdit();
                            Console.WriteLine(string.Format("Guia = {0} EMP {1} con {2}", nGuias, RowGPS.EMP, RowGPS.CON));
                            dAutomacaoBancaria.INSS.AddINSSRow(RowGPS);
                            nGuias++;
                            //if (nGuias == 245)
                            //    MessageBox.Show("E");
                            //try
                            //{
                            //    dAutomacaoBancaria.EnforceConstraints = true;
                            //    dAutomacaoBancaria.EnforceConstraints = false;
                            // }
                            //catch (System.Data.ConstraintException ex)
                            //{
                            //    MessageBox.Show("Erros na importa��o dos dados... corrigir o cadastro na folha.\r\n" + ex.Message + nGuias.ToString());
                            //}
                            //}
                        }
                        sLinha = ProximaLinha(null);
                    }

                    //this.ViewGPS.EndDataUpdate();
                    //try
                    //{
                    //    dAutomacaoBancaria.EnforceConstraints = true;
                    //}
                    //catch (System.Data.ConstraintException ex)
                    //{
                    //    MessageBox.Show("Erros na importa��o dos dados... corrigir o cadastro na folha.\r\n"+ex.Message);
                    //}

                    this.btnCancelarExtracao.Visible = false;
                    this.pgbExtracao.Visible = false;

                    this.Cursor = Cursors.Default;
                    this.TabControlAutomacao.SelectedTabPage = TabPageGPS;
                    if (sCONErro.Trim() != string.Empty)
                    //MessageBox.Show("O processo de extra��o de dados do arquivo " + this.openFileDialog.FileName + " foi conclu�do com sucesso.", "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //else
                    {
                        MessageBox.Show("O processo de extra��o de dados do arquivo " + this.openFileDialog.FileName + " foi conclu�do, mais n�o foi poss�vel localizar o registro de um ou mais condom�nios.\r\nVerifique os dados dos condom�nios abaixo e tente gerar o arquivo novamente.\r\n\r\nCondom�nio(s) n�o localizado(s): \r\n" + sCONErro, "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        using (StreamWriter swFile = new StreamWriter("c:\\logdeerro.txt", true))
                        {
                            swFile.WriteLine("----------------");
                            swFile.WriteLine("  " + DateTime.Now.ToString());
                            swFile.WriteLine("----------------");
                            swFile.WriteLine(sCONErro.ToString());
                            swFile.Close();
                        }
                    }
                    //try
                    //{
                    /*
                    ViewGPS.BeginDataUpdate();
                    dllSefip.Sefip sefip1 = new dllSefip.Sefip(comp, false);
                    Esp.Motivo.Text = "Carregando Notas";
                    Application.DoEvents();
                    Esp.AtivaGauge(sefip1.dSefip.Sefip.Rows.Count);
                    int i = 0;
                    DateTime prox = DateTime.Now;
                    foreach (dllSefip.dSefip.SefipRow rowlocal in sefip1.dSefip.Sefip)
                    {
                        i++;
                        if (prox <= DateTime.Now)
                        {
                            Esp.Gauge(i);
                            prox = DateTime.Now.AddSeconds(3);
                        }
                        dAutomacaoBancaria.INSSRow rowINSS = dAutomacaoBancaria.INSS.FindBynCNPJ(rowlocal.CONCNPJ);
                        if (rowINSS == null)
                        {
                            rowINSS = dAutomacaoBancaria.INSS.NewINSSRow();
                            rowINSS.nCNPJ = rowlocal.CONCNPJ;
                            rowINSS.NEON = 0;
                            //Int32 CodFolha = 0;
                            rowINSS.CON = rowlocal.CON;
                            rowINSS.Campo1 = rowlocal.CONNome;
                            //rowINSS.Banco
                            //rowINSS.Agencia
                            //rowINSS.Conta
                            dAutomacaoBancaria.CONDOMINIOSRow CONrow = dAutomacaoBancaria.CONDOMINIOS.FindByCON(rowlocal.CON);
                            rowINSS.TipoPag = (int)nTipoPag.Invalido;
                            if ((!CONrow.IsCONPagEletronicoNull()) && (CONrow.CONPagEletronico))
                            {
                                if (CONrow.CON_BCO == 237)
                                    rowINSS.TipoPag = (int)nTipoPag.EletronicoBradesco;
                                else
                                    rowINSS.TipoPag = (int)nTipoPag.EletronicoItau;
                            }
                            else
                                if (CONrow.CON_EMP != Framework.DSCentral.EMP)
                                    rowINSS.TipoPag = (int)nTipoPag.ChequeFilial;
                                else
                                    rowINSS.TipoPag = (int)nTipoPag.Cheque;
                            if (rowINSS.TipoPag == (int)nTipoPag.EletronicoBradesco)
                            {
                                try
                                {
                                    rowINSS.Banco = CONrow.CON_BCO;
                                    rowINSS.Agencia = int.Parse(CONrow.CONAgencia);
                                    rowINSS.Conta = int.Parse(CONrow.CONConta);
                                    rowINSS.CEP = CONrow.CONCep;
                                    rowINSS.Endereco = CONrow.CONEndereco;
                                    rowINSS.UF = CONrow.CIDUf;
                                    rowINSS.Cidade = CONrow.CIDNome;
                                    rowINSS.Bairro = CONrow.CONBairro;
                                }
                                catch
                                {
                                    rowINSS.TipoPag = (int)nTipoPag.Invalido;
                                    rowINSS.Erro = "Erro no cadastro n�o permite pagamento eletr�nico";
                                }
                            };
                            rowINSS.Campo3 = "2100";
                            rowINSS.Campo4 = comp.ToString();
                            rowINSS.ANO = comp.Ano.ToString("0000");
                            rowINSS.MES = comp.Mes.ToString("00");
                            rowINSS.Campo5 = rowlocal.CONCNPJ.ToString();

                            dAutomacaoBancaria.INSS.AddINSSRow(rowINSS);

                        }


                        foreach (dllSefip.dSefip.INSSPagoRow rowPAG in rowlocal.GetINSSPagoRows())

                            rowINSS.NEON += rowPAG.PAGValor;


                    }
                    string Arquivo = @"\\NEON02/folha/virtual/gps.txt";
                    if (!File.Exists(Arquivo))
                    {
                        OpenFileDialog OF = new OpenFileDialog();
                        if (OF.ShowDialog() != DialogResult.OK)
                        {
                            return;
                        };
                        Arquivo = OF.FileName;
                    }
                    Esp.Motivo.Text = "Lendo SEFIP";
                    Application.DoEvents();
                    dllSefip.Guias Guias1 = new Guias(Arquivo);
                    Esp.Motivo.Text = "Carregando SEFIP";
                    Application.DoEvents();
                    //int i = 0;
                    foreach (Par par in Guias1.guias)
                    {
                        //i++;
                        dAutomacaoBancaria.INSSRow rowINSS = dAutomacaoBancaria.INSS.FindBynCNPJ(par.cnpj.Valor);
                        if (rowINSS == null)
                        {
                            MessageBox.Show(string.Format("Guia sem par: CNPJ:{0} Valor:{1:n2}",par.cnpj,par.valor));
                        }
                        else
                        {
                            rowINSS.SEFIP = par.valor;

                        };
                    }
                    */
                    //}
                    //finally 
                    //{
                    //    ViewGPS.EndDataUpdate();
                    // }
                }
;
        }

        private void ExtrairPagamentos()
        {
            FolhaPag.cFolhaPag cNovaFolhaPag = new FolhaPag.cFolhaPag(this);
            if (cNovaFolhaPag.ExtrairPagamentos(this, UltimoArquivo, ConteudoTotal))
                cNovaFolhaPag.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
            else
                MessageBox.Show("Cancelado");
        }

        /*
        /// <summary>
        /// Le os dados do RTF e carrega na tabela (RAM) Sal�rios. Cuidado com os campos PGF__EMP e CON__EMP que tem c�digos da tabela Local/Filial
        /// </summary>
        private void ExtrairPagamentos()
        {
            SortedList Alternativas = new SortedList();
            Alternativas.Add(TipoBancaria.Pagamento, "Pagamento");
            Alternativas.Add(TipoBancaria.Adiantamento, "Adiantamento");
            Alternativas.Add(TipoBancaria.DecimoTerceiro, "d�cimo terceiro");
            Alternativas.Add(TipoBancaria.Avulso, "Avulso");
            object Selecionado;
            if (!VirInput.Input.Execute("Tipo de pagamento", Alternativas, out Selecionado, VirInput.Formularios.cRadioCh.TipoSelecao.radio))
                return;
            imageComboTipo.EditValue = (int)Selecionado;
            tipoBancaria = (TipoBancaria)Selecionado;
            try
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Carregando condom�nios");
                    dAutomacaoBancaria.RecarregarCondominios();
                    ESP.Espere("Carregando funcion�rios");
                    dAutomacaoBancaria.RecarregarFuncionarios(true);
                }
                
                dAutomacaoBancaria.RHTarefaSubdetalhe.Clear();
                dAutomacaoBancaria.RHTarefaDetalhe.Clear();
                rowmaeRHT = rowmaeRHTF = null;
                bool NaoTemBanco = true;
                String sLinha = "";
                String sLinhaBusca;
                String sLinhadet = "";
                String sLinhaag = "";
                Int32 FUNIDBanco;
                String FUNBanco;
                string FUNAgencia; //formato 0000-0
                string TipoBanco;
                nTipoPag nTipoPag;
                TipoBancaria tipoBancariaRTF = TipoBancaria.Avulso;
                Periodicos = new SortedList<int, PagamentoPeriodico>();
                this.ViewPagamentos.BeginDataUpdate();
                tamanhototal = ConteudoTotal.Length;
                if (ConteudoTotal.Contains("A DE PAGAMENTO\n"))
                    tipoBancariaRTF = TipoBancaria.Pagamento;
                else if (ConteudoTotal.Contains("A DE ADIANTAMENTO\n"))
                    tipoBancariaRTF = TipoBancaria.Adiantamento;
                else if (ConteudoTotal.Contains("A DE 13"))
                    tipoBancariaRTF = TipoBancaria.DecimoTerceiro;
                else
                {
                    MessageBox.Show("Banc�ria n�o identificada!!\r\nTratada como Avulsa");
                    tipoBancariaRTF = TipoBancaria.Avulso;
                };

                if(tipoBancaria != tipoBancariaRTF)
                {
                    if (MessageBox.Show("Arquivo RTF com tipo diferente do informado\r\nConfirma assim mesmo ?", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return;
                }
                                
                
                //imageComboTipo.EditValue = (int)tipoBancaria;
                this.pgbExtracao.Maximum = tamanhototal;
                this.pgbExtracao.Value = 0;
                Inicio = 0;
                Tamanho = 0;
                Terminado = false;
                Competencia comp = null;
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    CarregaDadosPeriodicos(ESP);
                    ESP.Espere("Carregando arquivo");
                    ESP.AtivaGauge(tamanhototal);
                    Application.DoEvents();
                    if (ESP.GaugeTempo(0))
                        return;
                    while (!Terminado)
                    {
                        _CancelarExtracao = ESP.GaugeTempo(Inicio);
                        if (this._CancelarExtracao)
                        {
                            Abortar("Cancelado");
                            return;
                        }
                        this.pgbExtracao.Value = Inicio;
                        sLinha = ProximaLinha(ESP);

                        //Competencia
                        if ((comp == null) && sLinha.Contains("DATAMACE") && sLinha.Contains("G.I.P.") && sLinha.Contains("REF:"))
                        {
                            int mes;
                            int ano;
                            int i = sLinha.IndexOf("REF:");
                            if (int.TryParse(sLinha.Substring(i + 4, 2), out mes) && (int.TryParse(sLinha.Substring(i + 7, 4), out ano)))
                            {
                                comp = new Framework.objetosNeon.Competencia(mes, ano);
                                //rowmaeRHT.RHTCompetencia = rowmaeRHTF.RHTCompetencia = comp.CompetenciaBind;
                                cCompet1.Comp = comp;
                                cCompet1.AjustaTela();
                            }
                        }

                        //Cabecalho Relatorio
                        if ((sLinha.IndexOf("RELACAO BANCARIA / CAIXA") >= 0)
                              ||
                            (sLinha.IndexOf("RELA��O BANC�RIA / CAIXA") >= 0)
                              ||
                            (sLinha.IndexOf("RELA++O BANC+RIA / CAIXA") >= 0)
                            )
                        {
                            //Verificando se existem funcionarios na pagina do relatorio
                            bool ProximoBanco = false;
                            while ((!ProximoBanco) && (!Terminado))
                            {
                                sLinhaag = "";
                                NaoTemBanco = false;
                                do
                                {
                                    sLinhaBusca = ProximaLinha(ESP);
                                    //Refer�ncia OneNote: Funcionario em grupo separado 11.1.7.3 11/11/2011
                                    //if (sLinhaBusca.IndexOf("REGISTRO NOME                                           C.P.F.           VALOR") >= 0)
                                    if (sLinhaBusca.IndexOf("REGISTRO NOME") >= 0)
                                    {
                                        NaoTemBanco = true;
                                        ProximoBanco = false;
                                        sLinhaBusca = ProximaLinha(ESP);
                                    }
                                    else
                                    {
                                        if ((sLinhaBusca.IndexOf("AGENCIA:") >= 0)
                                              ||
                                            (sLinhaBusca.IndexOf("AG�NCIA:") >= 0)
                                              ||
                                            (sLinhaBusca.IndexOf("AG+NCIA:") >= 0)
                                           )
                                        {
                                            if (sLinhaBusca.Contains("BANCO: 00000-NAO CADASTRADO")
                                                  ||
                                                sLinhaBusca.Contains("BANCO: 00000-N�O CADASTRADO")
                                                  ||
                                                sLinhaBusca.Contains("BANCO: 00000-N+O CADASTRADO")
                                               )
                                            {
                                                NaoTemBanco = true;
                                                ProximoBanco = false;
                                            }
                                            else
                                            {
                                                sLinhaag = sLinhaBusca;
                                                NaoTemBanco = false;
                                            }
                                            do
                                            {
                                                sLinhaBusca = ProximaLinha(ESP);
                                            } while ((sLinhaBusca.IndexOf("REGISTRO NOME                              C.CORRENTE   C.P.F.           VALOR") != 0)
                                                      &&
                                                     (sLinhaBusca.IndexOf("REGISTRO NOME                               C O N T A   C.P.F.           VALOR") != 0)
                                                      &&
                                                     (sLinhaBusca.IndexOf("REGISTRO NOME                              C O N T A    C.P.F.            VALOR") != 0)
                                                      &&
                                                     (sLinhaBusca.IndexOf("REGISTRO NOME                              C O N T A    C.P.F.             VALOR") != 0)
                                                      &&
                                                     (!Terminado));
                                            sLinhaBusca = ProximaLinha(ESP);
                                        }
                                    };
                                    if ((sLinhaBusca.IndexOf("TOTAL DESTE BANCO") >= 0) || (sLinhaBusca.IndexOf("TOTAL DA EMPRESA") >= 0))
                                        ProximoBanco = true;
                                } while ((!NaoTemBanco) && (sLinhaag == "") && (!ProximoBanco) && (!Terminado));


                                if ((ProximoBanco) || (Terminado))
                                {
                                    sLinha = ProximaLinha(ESP);
                                }
                                else
                                {

                                    sLinhadet = ProximaLinha(ESP);

                                    if (sLinhadet.Trim() != "")
                                    {
                                        //Localizando registro do condom�nio para colher informa��es
                                        string CONCodigoFolha2A = sLinha.Substring(49, 3);
                                        string CadastroFolha = sLinha.Substring(49);
                                        if (CONCodigoFolha2A == " - ") //modelo antigo
                                        {
                                            CONCodigoFolha2A = sLinha.Substring(46, 3);
                                            CadastroFolha = sLinha.Substring(46);
                                        }
                                        int CONCodigoFolha = 0;
                                        if (!int.TryParse(CONCodigoFolha2A, out CONCodigoFolha))                                                                                                                                
                                                CONCodigoFolha = -1;
                                        
                                        dAutomacaoBancaria.CONDOMINIOSRow rowCON = dAutomacaoBancaria.BuscaCondominio(CONCodigoFolha, CONCodigoFolha2A, true, CadastroFolha);

                                        
                                        

                                        if (rowCON != null)
                                        {
                                            FrameworkProc.EMPTProc EMPTProc1 = new EMPTProc(rowCON.CON_EMP == DSCentral.EMP ? EMPTipo.Local : EMPTipo.Filial,
                                                                                            rowCON.CON_EMP == DSCentral.EMP ? Framework.DSCentral.USU : Framework.DSCentral.USUFilial);

                                            if (rowCON.IsCONAgenciaNull())
                                            {
                                                Abortar(string.Format("Erro fatal: Cadastro de condom�nio incompleto (ag�ncia)\r\nCondom�nio: {0}", rowCON.CONCodigo));
                                                return;
                                            }
                                            if (rowCON.IsCONCnpjNull())
                                            {
                                                Abortar(string.Format("Erro fatal: Cadastro de condom�nio incompleto (CNPJ)\r\nCondom�nio: {0}", rowCON.CONCodigo));
                                                return;
                                            }

                                            //Dados do Condominio                                            
                                            string CodFolha = rowCON.CONCodigoFolha1.ToString().PadLeft(3, '0');

                                            //Dados do Funcionario
                                            if (NaoTemBanco)
                                            {
                                                FUNIDBanco = 0;
                                                FUNBanco = FUNAgencia = TipoBanco = " ";
                                                nTipoPag = nTipoPag.Cheque;
                                            }
                                            else
                                            {
                                                sLinhaag = sLinhaag.ToUpper();
                                                FUNIDBanco = Convert.ToInt32(sLinhaag.Substring(7, 5));
                                                FUNBanco = FUNIDBanco.ToString() + "-" + sLinhaag.Substring(13, sLinhaag.IndexOf(" ", 13)).Trim();
                                                FUNAgencia = sLinhaag.Substring(53, 6);
                                                //corre��o para quando n�o vem o separador do digito da agencia - este problema pareceu nos RTF da datamace em 28/09/2011                                                
                                                if ((FUNAgencia[5] == ' ') && (FUNAgencia.IndexOf('-') == -1))
                                                    FUNAgencia = string.Format("{0}-{1}", FUNAgencia.Substring(0, 4), FUNAgencia.Substring(4, 1));
                                                TipoBanco = sLinhaag.Substring(13, 30).Trim().ToUpper();
                                                nTipoPag = GetnTipoPag(TipoBanco);
                                                if (nTipoPag == nTipoPag.Invalido)
                                                    MessageBox.Show("Tipo de pagamento n�o definido para: " + TipoBanco);
                                            }

                                            //Gerando registro de funcionario
                                            sLinhadet = sLinhadet.ToUpper();
                                            //Refer�ncia OneNote: Quebra inesperada 11.1.7.2 17/10/2011
                                            while ((sLinhadet.Substring(0, 10) != "----------") && (sLinhadet.Substring(0, 10) != "          ") && (sLinhadet.Substring(0, 10) != "\fDATAMACE "))
                                            {
                                                string FUNRegistro = sLinhadet.Substring(0, 9).Trim();
                                                String FUNNome = sLinhadet.Substring(9, 32).Trim();
                                                string FUNConta = sLinhadet.Substring(43, 13).Trim();
                                                String FUNCPF = sLinhadet.Substring(56, 11).Trim();
                                                decimal FUNValor = decimal.Parse(sLinhadet.Substring(67, 12).Trim());
                                                tipoconta FUNTipoConta = tipoconta.invalida;

                                                if ((FUNConta.IndexOf(".") == 3))// && (FUNConta.IndexOf("-") == 7))
                                                    FUNTipoConta = tipoconta.ContaCorrente;
                                                else
                                                    if ((FUNConta.IndexOf(":") == 3) && (FUNConta.IndexOf("-") == 7))
                                                    FUNTipoConta = tipoconta.ContaSalario;
                                                
                                                //int RHT = rowCON.CON_EMP == Framework.DSCentral.EMP ? rowmaeRHT.RHT : rowmaeRHTF.RHT;                                                
                                                int RHT = NovarowmaeRHT(rowCON.CON_EMP, comp).RHT;
                                                //rHTarefaDetalheBindingSource.Filter = string.Format("RHD_RHT = {0}", RHT);
                                                //rHTarefaSubdetalheBindingSource.Filter = string.Format("RHD_RHT = {0}", RHT);
                                                //rHTarefaSubdetalheBindingSource.Filter = string.Format("");
                                                dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = dAutomacaoBancaria.Busca_rowRHD(RHT, rowCON.CON);
                                                if (rowRHD == null)
                                                {
                                                    rowRHD = dAutomacaoBancaria.RHTarefaDetalhe.NewRHTarefaDetalheRow();
                                                    rowRHD.RHD_CON = rowCON.CON;
                                                    rowRHD.RHD_RHT = RHT;                                                    
                                                    rowRHD.EMP = rowCON.CON_EMP;
                                                    rowRHD.Total = 0;
                                                    rowRHD.CONAgencia = rowCON.CONAgencia;
                                                    rowRHD.CONBanco = rowCON.CON_BCO.ToString("000");
                                                    rowRHD.CONCNPJ = rowCON.CONCnpj;
                                                    rowRHD.CONCodigo = rowCON.CONCodigo;
                                                    rowRHD.CONNome = rowCON.CONNome;
                                                    rowRHD.CONCodigoFolha1 = rowCON.CONCodigoFolha1;
                                                    rowRHD.CONConta = rowCON.CONConta;
                                                    rowRHD.CONDigitoAgencia = rowCON.CONDigitoAgencia;
                                                    rowRHD.CONDigitoConta = rowCON.CONDigitoConta;
                                                    if (!rowCON.IsCONCodigoBancoCCNull())
                                                        rowRHD.CONRazaoCC = rowCON.CONCodigoBancoCC;
                                                    dAutomacaoBancaria.RHTarefaDetalhe.AddRHTarefaDetalheRow(rowRHD);
                                                    dAutomacaoBancaria.RHTarefaDetalheTableAdapterX(EMPTProc1.Tipo).Update(rowRHD);
                                                }
                                                dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS = dAutomacaoBancaria.Busca_rowRHS();
                                                dAutomacaoBancaria.FUnCionarioRow rowFUC = dAutomacaoBancaria.Busca_rowFUnCionario(rowCON.CON_EMP, rowCON.CON, FUNRegistro);
                                                CPFCNPJ cpf = new CPFCNPJ(FUNCPF);
                                                if (rowFUC == null)
                                                {
                                                    rowFUC = dAutomacaoBancaria.FUnCionario.NewFUnCionarioRow();
                                                    rowFUC.FUC_CON = rowCON.CON;
                                                    rowFUC.FUCAgencia = FUNAgencia;
                                                    rowFUC.FUCAtivo = true;
                                                    rowFUC.FUCBanco = FUNIDBanco;
                                                    rowFUC.FUCConta = FUNConta;
                                                    if (cpf.Tipo == TipoCpfCnpj.CPF)
                                                        rowFUC.FUCCpf = cpf.Valor;
                                                    rowFUC.FUCNome = FUNNome;
                                                    //rowFUC.FUCPIS = 
                                                    rowFUC.FUCRegistro = FUNRegistro;
                                                    rowFUC.FUCTipoConta = (int)FUNTipoConta;
                                                }
                                                else
                                                {
                                                    if (rowFUC.FUCAgencia != FUNAgencia)
                                                        rowFUC.FUCAgencia = FUNAgencia;
                                                    if (!rowFUC.FUCAtivo)
                                                        rowFUC.FUCAtivo = true;
                                                    if (rowFUC.FUCBanco != FUNIDBanco)
                                                        rowFUC.FUCBanco = FUNIDBanco;
                                                    if (rowFUC.FUCConta != FUNConta)
                                                        rowFUC.FUCConta = FUNConta;
                                                    if ((cpf.Tipo == TipoCpfCnpj.CPF) && (rowFUC.FUCCpf != cpf.Valor))
                                                        rowFUC.FUCCpf = cpf.Valor;
                                                    if (rowFUC.FUCNome != FUNNome)
                                                        rowFUC.FUCNome = FUNNome;
                                                    if (rowFUC.FUCTipoConta != (int)FUNTipoConta)
                                                        rowFUC.FUCTipoConta = (int)FUNTipoConta;
                                                }
                                                if (rowFUC.RowState == DataRowState.Detached)
                                                    dAutomacaoBancaria.FUnCionario.AddFUnCionarioRow(rowFUC);
                                                if (rowFUC.RowState != DataRowState.Unchanged)
                                                    dAutomacaoBancaria.FUnCionarioTableAdapterX(EMPTProc1.Tipo).Update(rowFUC);


                                                if (rowRHS == null)
                                                {
                                                    rowRHS = dAutomacaoBancaria.RHTarefaSubdetalhe.NewRHTarefaSubdetalheRow();
                                                    rowRHS.EMP = rowCON.CON_EMP;
                                                    rowRHS.RHS_FUC = rowFUC.FUC;
                                                    rowRHS.RHS_RHD = rowRHD.RHD;
                                                    rowRHS.RHSTipoPag = (int)nTipoPag;
                                                    rowRHS.RHSValor = FUNValor;
                                                    rowRHS.CONNome = rowCON.CONNome;
                                                    rowRHS.RowError = "";
                                                    rowRHS.strStatus = "Carregando";
                                                    dAutomacaoBancaria.RHTarefaSubdetalhe.AddRHTarefaSubdetalheRow(rowRHS);
                                                    rowRHD.Total += FUNValor;
                                                    dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterX(EMPTProc1.Tipo).Update(rowRHS);
                                                    if (TipoPagPeriodico.HasValue)
                                                    {
                                                        if (!Periodicos.ContainsKey(rowRHD.RHD))
                                                        {
                                                            PagamentoPeriodico per = new PagamentoPeriodico(rowCON.CON, TipoPagPeriodico.Value, EMPTProc1, true);
                                                            Periodicos.Add(rowRHD.RHD, per);
                                                            Console.WriteLine(string.Format("=========> RHD = {0:00000} encontrado:{1}", rowRHD.RHD,per.Encontrado));
                                                        }
                                                    }
                                                }
                                                else
                                                    rowRHS.RHSValor += FUNValor;
                                                sLinhadet = "";
                                                while (sLinhadet.Length < 10)
                                                    sLinhadet = ProximaLinha(ESP).ToUpper();
                                            }

                                        }
                                        else
                                        {
                                            string men = string.Format("N�o foi poss�vel localizar o cadastro do condom�nio {0}.\r\nPoss�velmente este c�digo n�o esteja atribuido a nenhum dos condom�nios existentes.",
                                                CONCodigoFolha != -1 ? CONCodigoFolha.ToString() : CONCodigoFolha2A);
                                            Abortar(men);
                                            return;
                                        }
                                    }
                                };
                            };
                            dAutomacaoBancaria.RHTarefaTableAdapter.Update(rowmaeRHT);
                            dAutomacaoBancaria.RHTarefaTableAdapterF.Update(rowmaeRHTF);
                        }
                    };
                    ESP.Espere("Completando Campos");
                    dAutomacaoBancaria.CompletaCampos();
                    CarregaHistoricoPeriodicos();
                }

                TabPagePagamentos.PageVisible = true;
                this.TabControlAutomacao.SelectedTabPage = TabPagePagamentos;
                ValidaPagamentos();
                //MessageBox.Show("O processo de extra��o de dados do arquivo " + this.openFileDialog.FileName + " foi conclu�do com sucesso.", "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                this.ViewPagamentos.EndDataUpdate();
                this.ViewPagamentos.RefreshData();

            }
            //CompontesBasicos.Performance.Performance.PerformanceST.Relatorio(true,true);
        }
        */

        private void ExtrairPIS()
        {
            int n = 0;
            dAutomacaoBancaria.CONDOMINIOSTableAdapter.Fill(dAutomacaoBancaria.CONDOMINIOS);
            if (this.ArquivoRTF.Lines.Length > 0)
                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    this.dAutomacaoBancaria.PIS.Clear();

                    //ConteudoTotal = this.ArquivoRTF.Text;
                    tamanhototal = ConteudoTotal.Length;
                    this.pgbExtracao.Maximum = tamanhototal;
                    this.pgbExtracao.Value = 0;
                    Inicio = 0;
                    Tamanho = 0;
                    this.pgbExtracao.Value = 0;

                    //this.ViewPIS.BeginDataUpdate();

                    //String sLinhaAnt = "";
                    String sLinhaBusca = string.Empty;

                    //String sCONErro = "";

                    //DataRowView RowPIS = null;
                    dAutomacaoBancaria.PISRow RowPIS = null;

                    this.btnCancelarExtracao.Visible = true;
                    this.pgbExtracao.Visible = true;

                    //this.dAutomacaoBancaria.CONDOMINIOS.Clear();
                    //this.cONDOMINIOSTableAdapter.Fill(this.dAutomacaoBancaria.CONDOMINIOS);

                    sLinhaBusca = string.Empty;

                    Terminado = false;
                    while (!Terminado)
                    {
                        Application.DoEvents();

                        if (this._CancelarExtracao)
                        {
                            this.Cursor = Cursors.Default;
                            this.dAutomacaoBancaria.PIS.Clear();
                            this.btnCancelarExtracao.Visible = false;
                            this.pgbExtracao.Visible = false;
                            //this.ViewPIS.EndDataUpdate();

                            return;
                        }

                        this.pgbExtracao.Value = Inicio;

                        //sLinhaAnt = sLinhaBusca;
                        sLinhaBusca = ProximaLinha(null);
                        String sLinhaData = sLinhaBusca;

                        //Inicio de uma DARF
                        if (sLinhaBusca.IndexOf("MINISTERIO DA FAZENDA") >= 0)
                        {
                            n++;

                            String sLinhaCNPJ = ProximaLinha(4);
                            String sLinhaCodigo = ProximaLinha(3);
                            String sLinhaCondominio = ProximaLinha(3);
                            String sLinhaCEP = ProximaLinha(1);
                            String sVencimento = ProximaLinha(2);
                            String sLinhaValor = ProximaLinha(3);
                            String sLinhaTotal = ProximaLinha(9);

                            //Extraindo codigo do condominio na DARF e verificando
                            //se o cadastro do mesmo existe na tabela condominios
                            //atraves do campo CONCodigoFolha1
                            String sCondominio = sLinhaCondominio.Substring(6, 33).Trim();
                            String sCodigoFolha1 = sLinhaCondominio.Substring(2, 3);

                            //DataRow[] drFoundRows;

                            //DataRowView DRV = ((DataRowView)(this.pISBindingSource.AddNew()));
                            //RowPIS = (dAutomacaoBancaria.PISRow)DRV.Row;
                            RowPIS = dAutomacaoBancaria.PIS.NewPISRow();
                            RowPIS.TipoPag = (int)nTipoPag.Invalido;
                            Int32 CodFolha = 0;
                            if (!Int32.TryParse(sCodigoFolha1, out CodFolha))
                                RowPIS.Erro = "C�digo folha inv�lido:<" + sCodigoFolha1 + ">";
                            else
                            {
                                //Console.Write("Busca CodFolha {0}\r\n", CodFolha);
                                //if(CodFolha == 270)
                                //    Console.Write("******Busca CodFolha {0}\r\n", CodFolha);
                                dAutomacaoBancaria.CONDOMINIOSRow CONrow = dAutomacaoBancaria.BuscaCondominio(CodFolha, null, true);
                                if (CONrow == null)
                                {
                                    RowPIS.Erro = "Nenhum Condom�nio encontrado com o C�digo " + CodFolha.ToString();
                                    RowPIS.Banco = 0;
                                }
                                else
                                {
                                    RowPIS.CON = CONrow.CON;
                                    RowPIS.EMP = CONrow.CON_EMP;
                                    RowPIS.CodComunicacao = "117045";
                                    string CNPJ = sLinhaCNPJ.Substring(59, 18);
                                    DocBacarios.CPFCNPJ cnpjLido = new DocBacarios.CPFCNPJ(CNPJ);
                                    if (cnpjLido.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                                        RowPIS.Erro = "CNPJ inv�lido:" + CNPJ;
                                    else
                                        if (CONrow.IsCONCnpjNull())
                                        RowPIS.Erro = "CNPJ n�o cadastrado!";
                                    else
                                            if (cnpjLido.Valor != new DocBacarios.CPFCNPJ(CONrow.CONCnpj).Valor)
                                        RowPIS.Erro = "CNPJ n�o confere:" + CNPJ + " x " + CONrow.CONCnpj;
                                    ;
                                    try
                                    {
                                        RowPIS.Agencia = int.Parse(CONrow.CONAgencia);
                                        RowPIS.Conta = int.Parse(CONrow.CONConta);
                                        RowPIS.DigAgencia = CONrow.CONDigitoAgencia;
                                        RowPIS.DigConta = CONrow.CONDigitoConta;
                                    }
                                    catch
                                    {
                                        RowPIS.Erro = "Erro na Ag�ncia/Conta do condom�nio";
                                        RowPIS.Agencia = RowPIS.Conta = 0;
                                    }
                                    if (CONrow.IsCON_BCONull())
                                    {
                                        RowPIS.Erro = "Erro no Banco do condom�nio";
                                        RowPIS.Banco = 0;
                                    }
                                    else
                                    {
                                        RowPIS.Banco = CONrow.CON_BCO;
                                        if (CONrow.IsCONGPSEletronicoNull())
                                        {
                                            CONrow.CONGPSEletronico = false;
                                            if (CONrow.CON_EMP == DSCentral.EMP)
                                                dAutomacaoBancaria.CONDOMINIOSTableAdapter.Update(CONrow);
                                            else
                                                dAutomacaoBancaria.CONDOMINIOSTableAdapterF.Update(CONrow);
                                        }
                                        if ((CONrow.CON_BCO == 237) && (CONrow.CONGPSEletronico))
                                            RowPIS.TipoPag = (int)nTipoPag.CreditoConta;
                                        else
                                            RowPIS.TipoPag = (int)nTipoPag.Cheque;
                                    }
                                    //02-Periodo de Apuracao
                                    RowPIS.Campo2 = DateTime.Parse(sLinhaData.Substring(66, 10));

                                    //03-Numero do CPF ou CNPJ
                                    RowPIS.Campo3 = sLinhaCNPJ.Substring(59, 18);

                                    //04-CODIGO DA RECEITA
                                    RowPIS.Campo4 = sLinhaCodigo.Substring(65, 4);

                                    //06-DATA VENCTO E 01-DADOS DA EMPRESA
                                    RowPIS.Condominio = sCodigoFolha1;// sCondominio;
                                    RowPIS.Endereco = CONrow.CONEndereco;
                                    RowPIS.Bairro = CONrow.CONBairro;
                                    RowPIS.Cidade = CONrow.CIDNome;
                                    RowPIS.UF = CONrow.CIDUf;

                                    RowPIS.CEP = sLinhaCEP.Substring(2, 9);
                                    RowPIS.Campo1 = sCondominio;
                                    RowPIS.Campo6 = DateTime.Parse(sVencimento.Substring(64, 10).Trim());

                                    //07-VALOR DO PRINCIPAL
                                    RowPIS.Campo7 = decimal.Parse(sLinhaValor.Substring(64, 11));

                                    //10-VALOR TOTAL                                
                                    RowPIS.Campo10 = decimal.Parse(sLinhaTotal.Substring(64, 11));

                                    //Verifica se deu erro e seta como invalido para correcao
                                    if (!RowPIS.IsErroNull())
                                        RowPIS.TipoPag = (int)nTipoPag.Invalido;
                                }
                            };
                            dAutomacaoBancaria.PIS.AddPISRow(RowPIS);
                        }
                    }

                    this.btnCancelarExtracao.Visible = false;
                    this.pgbExtracao.Visible = false;

                    this.Cursor = Cursors.Default;
                    this.TabControlAutomacao.SelectedTabPage = TabPagePIS;
                    ViewPIS.RefreshData();

                }
                catch (Exception Erro)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Ocorreu uma excess�o ao tentar extrair os dados do arquivo " + this.openFileDialog.FileName + ".\r\n\r\n" + Erro.Message + "\r\n(" + n.ToString() + ")", "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.btnCancelarExtracao.Visible = false;
                    this.pgbExtracao.Visible = false;
                }
        }





        private string GetParte(int Grupo, int Linha, int inicio, int Tamanho)
        {            
            if (Tamanho > Linhas[Linha + Grupo * 33].Length - inicio)
                Tamanho = Linhas[Linha + Grupo * 33].Length - inicio;
            //Console.Write("Resultado: {0}\r\n", (Tamanho <= 0) ? " " : Linhas[Linha + Grupo * 33].Substring(inicio, Tamanho).Trim());
            return (Tamanho <= 0) ? " " : Linhas[Linha + Grupo * 33].Substring(inicio, Tamanho).Trim();
        }




        private dNOtAs.NOtAsRow GetrowNOAGPS(dAutomacaoBancaria.INSSRow rowGPS)
        {
            dNOtAs.NOtAsRow rowNOA = dNOtAs.NOtAs.NewNOtAsRow();
            rowNOA.NOA_CON = rowGPS.CON;
            rowNOA.NOA_PLA = "220016";
            rowNOA.NOAServico = "INSS - " + rowGPS.MES + "/" + rowGPS.ANO;

            rowNOA.NOADataEmissao = DateTime.Today;
            rowNOA.NOACompet = rowNOA.NOADataEmissao.Year * 100 + rowNOA.NOADataEmissao.Month;
            rowNOA.NOAAguardaNota = false;
            rowNOA.NOADATAI = DateTime.Now;
            rowNOA.NOAI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
            rowNOA.NOANumero = 0;

            rowNOA.NOAStatus = (int)NOAStatus.Cadastrada;
            rowNOA.NOATipo = (int)NOATipo.Folha;
            rowNOA.NOATotal = 0;
            rowNOA.NOADefaultPAGTipo = (int)PAGTipo.cheque;
            dNOtAs.NOtAs.AddNOtAsRow(rowNOA);
            dNOtAs.NOtAsTableAdapter.Update(rowNOA);
            rowNOA.AcceptChanges();
            return rowNOA;
        }

        private dNOtAs.NOtAsRow GetrowNOAPis(dAutomacaoBancaria.PISRow rowPIS)
        {
            dNOtAs.NOtAsRow rowNOA = dNOtAs.NOtAs.NewNOtAsRow();
            rowNOA.NOA_CON = rowPIS.CON;
            rowNOA.NOA_PLA = "220006";
            rowNOA.NOAServico = "PIS sobre pagamento 8301";

            rowNOA.NOADataEmissao = DateTime.Today;
            rowNOA.NOACompet = rowNOA.NOADataEmissao.Year * 100 + rowNOA.NOADataEmissao.Month;
            rowNOA.NOAAguardaNota = false;
            rowNOA.NOADATAI = DateTime.Now;
            rowNOA.NOAI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
            rowNOA.NOANumero = 0;

            rowNOA.NOAStatus = (int)NOAStatus.Cadastrada;
            rowNOA.NOATipo = (int)NOATipo.Folha;
            rowNOA.NOATotal = 0;
            rowNOA.NOADefaultPAGTipo = (int)PAGTipo.cheque;
            dNOtAs.NOtAs.AddNOtAsRow(rowNOA);
            dNOtAs.NOtAsTableAdapter.Update(rowNOA);
            rowNOA.AcceptChanges();
            return rowNOA;
        }





        private void gridView4_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

            if (dAutomacaoBancaria.CONDOMINIOS.Rows.Count == 0)
                dAutomacaoBancaria.CONDOMINIOSTableAdapter.Fill(dAutomacaoBancaria.CONDOMINIOS);
            DataRow LinhaMae = gridView4.GetDataRow(e.FocusedRowHandle);
            if (LinhaMae != null)
            {
                dRecSal.HOLerites.Clear();
                DP.impressos.dRecSal.HOleritTituloRow rowHOT = (DP.impressos.dRecSal.HOleritTituloRow)LinhaMae;
                if (ChNR.Checked)
                    dRecSal.HOLeritesTableAdapterX(rowHOT.EMP).FillAguardo(dRecSal.HOLerites, rowHOT.HOT);
                else
                    dRecSal.HOLeritesTableAdapterX(rowHOT.EMP).FillByHOT(dRecSal.HOLerites, rowHOT.HOT);
            }
        }

        private void gridView4_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            DP.impressos.dRecSal.HOleritTituloRow HOTrow = (DP.impressos.dRecSal.HOleritTituloRow)gridView4.GetDataRow(e.RowHandle);
            if (HOTrow == null)
                return;
            e.Appearance.BackColor = HOTrow.EMP == Framework.DSCentral.EMP ? System.Drawing.Color.PaleGreen : System.Drawing.Color.LemonChiffon;
            e.Appearance.ForeColor = Color.Black;
        }

        private void gridView6_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if ((e.RowHandle >= 0) && (e.Column == gridColumn3))
            {
                dAutomacaoBancaria.RHTarefaRow rowRHT = (dAutomacaoBancaria.RHTarefaRow)gridView6.GetDataRow(e.RowHandle);
                if (((RHTStatus)(rowRHT.RHTStatus)).EstaNoGrupo(RHTStatus.Cadastrada, RHTStatus.Gerada, RHTStatus.GeradaParcial))
                    e.RepositoryItem = repositoryItemButtonDelete;
            }
        }

        private void gridView6_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            //colRHTStatus
            if (e == null)
                return;
            if (e.RowHandle < 0)
                return;
            if (e.Column == null)
                return;
            dAutomacaoBancaria.RHTarefaRow RHTrow = (dAutomacaoBancaria.RHTarefaRow)gridView6.GetDataRow(e.RowHandle);
            if (RHTrow == null)
                return;
            if (e.Column == colRHTStatus)
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = virEnumRHTStatus.virEnumRHTStatusSt.GetCor(RHTrow.RHTStatus);
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colEMP2)
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = RHTrow.EMP == Framework.DSCentral.EMP ? System.Drawing.Color.PaleGreen : System.Drawing.Color.LemonChiffon;
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column.EstaNoGrupo(colRHTDataCheque, colRHTDataCredito))
            {
                StatusRHT Status = StatusRHT.NaoAplicavel;
                if ((e.Column == colRHTDataCheque) && (!RHTrow.IsStatusChequeNull()))
                    Status = (StatusRHT)RHTrow.StatusCheque;
                if ((e.Column == colRHTDataCredito) && (!RHTrow.IsStatusCreditoNull()))
                    Status = (StatusRHT)RHTrow.StatusCredito;                
                switch (Status)
                {
                    case StatusRHT.NaoAplicavel:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.White;
                        e.Appearance.ForeColor = Color.White;
                        break;
                    case StatusRHT.aguardo:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.White;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusRHT.alerta:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Yellow;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusRHT.vencido:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Pink;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusRHT.vencidoDefinitivo:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                        break;
                }                
            }            
        }





        private void NovoTradutoGPS()
        {
            //Esta fun��o deve ser desligada. s� � utilizada para o 13
            if (dAutomacaoBancaria.INSS.Count == 0)
                return;

            bool avisar = false;
            int Errados = 0;
            int Eletronicos = 0;
            int Cheques = 0;
            //DateTime MaiorData = DateTime.Today;

            foreach (dAutomacaoBancaria.INSSRow row in dAutomacaoBancaria.INSS)
                if (!row.IsErroNull() || (nTipoPag)row.TipoPag == nTipoPag.Invalido)
                    Errados++;
            ;
            if (Errados > 0)
                if (MessageBox.Show("Existem " + Errados.ToString() + " pagamentos que ser�o ignorados.\r\nConfirma assim mesmo?", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return;

            //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
            if (VirInput.Input.Senha("Senha GPS") != "CUIDADO-GPS")
            {
                MessageBox.Show("Senha incorreta");
                return;
            };

            DateTime Agendamento = DateTime.Today.AddDays(1);
            while ((Agendamento.DayOfWeek == DayOfWeek.Saturday) || (Agendamento.DayOfWeek == DayOfWeek.Sunday))
                Agendamento = Agendamento.AddDays(1);
            DateTime? DataAgendarSel = Agendamento;
            if (VirInput.Input.Execute("Data de Pagamento", ref DataAgendarSel, DateTime.Today, DateTime.Today.AddDays(30)))
            {
                if (!Framework.datasets.dFERiados.IsUtil(DataAgendarSel.Value, Framework.datasets.dFERiados.Sabados.naoUteis, Framework.datasets.dFERiados.Feriados.naoUteis))
                {
                    MessageBox.Show("Data de Pagamento inv�lida (n�o � dia �til)");
                    return;
                }
            }
            else
                return;
            Agendamento = Convert.ToDateTime(DataAgendarSel);
            //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***

            dGPS.dGPSSt.GPS.Clear();
            try
            {
                //TableAdapter.ST().IniciaTrasacaoST(dGPS.dGPSSt.GPSTableAdapter, dNOtAs.CHEquesTableAdapter, dNOtAs.NOtAsTableAdapter, dNOtAs.PAGamentosTableAdapter);
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("DP cAutomacaoBancaria - 2904", dGPS.dGPSSt.GPSTableAdapter, dNOtAs.NOtAsTableAdapter, dNOtAs.PAGamentosTableAdapter);

                DateTime Vencimento = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 20);

                foreach (dAutomacaoBancaria.INSSRow row in dAutomacaoBancaria.INSS)
                {
                    if ((row.Campo11 + row.NEON) <= 0)
                        continue;

                    if (row.EMP != DSCentral.EMP)
                        continue;

                    Console.WriteLine(string.Format("CON = {0} {1}", row.CON, row.EMP));
                    if (row.CON == 522)
                        MessageBox.Show("verificar");

                    FrameworkProc.EMPTipo EMPTipo = row.EMP == DSCentral.EMP ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial;

                    switch ((nTipoPag)row.TipoPag)
                    {
                        case nTipoPag.CreditoConta:
                            //Insere Tributo na tabela GPS
                            if (row.Campo6 == 0)
                                continue;
                            dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();
                            string CEP = "0";
                            foreach (char c in row.CEP)
                                if (char.IsDigit(c))
                                    CEP += c;
                            novalinha.EMP = row.EMP;
                            DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(row.Campo5);
                            novalinha.GPSTipo = (int)TipoImposto.INSS;
                            novalinha.GPSNomeClientePagador = row.Campo1;
                            novalinha.GPS_CON = row.CON;
                            novalinha.GPSEndereco = row.Endereco;
                            novalinha.GPSCEP = int.Parse(CEP);
                            novalinha.GPSUF = row.UF;
                            novalinha.GPSCidade = (row.Cidade.Length > 20) ? row.Cidade.Substring(0, 20) : row.Cidade;
                            novalinha.GPSBairro = row.Bairro;
                            //novalinha.GPSCNPJ = Cnpj.Valor;
                            novalinha.GPSCNPJ = row.nCNPJ;
                            novalinha.GPSDataPagto = Agendamento;
                            novalinha.GPSValorPrincipal = row.Campo6; //+row.NEON;
                            novalinha.GPSValorMulta = row.Campo10;
                            novalinha.GPSValorJuros = 0;
                            novalinha.GPSOutras = row.Campo9;
                            novalinha.GPSValorTotal = row.Campo11 + row.NEON;
                            novalinha.GPSCodigoReceita = int.Parse(row.Campo3);
                            novalinha.GPSIdentificador = Cnpj.Valor;
                            novalinha.GPSAno = int.Parse(row.ANO);
                            novalinha.GPSMes = int.Parse(row.MES);
                            dllImpostoNeon.ImpostoNeon novoImp = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSS);
                            novoImp.Competencia = new Framework.objetosNeon.Competencia(novalinha.GPSMes, novalinha.GPSAno);
                            //novalinha.GPSVencimento = novoImp.Vencimento;
                            novalinha.GPSVencimento = Vencimento;
                            novalinha.GPSNomeIdentificador = row.Campo1;

                            //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
                            FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(row.CON);
                            if (rowCON.IsCON_BCONull())
                                throw new Exception(string.Format("Condom�nio sem Banco: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                            if (rowCON.CON_BCO != 237)
                                throw new Exception(string.Format("Conta principal do Condom�nio n�o � Bradesco, portanto n�o pode fazer Pag. Eletr. Bradesco: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                            if (rowCON.IsCONAgenciaNull())
                                throw new Exception(string.Format("Condom�nio sem Ag�ncia: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                            novalinha.GPSAgencia = int.Parse(rowCON.CONAgencia);
                            if (rowCON.IsCONDigitoAgenciaNull())
                                throw new Exception(string.Format("Condom�nio sem D�gito Ag�ncia: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                            novalinha.GPSDigAgencia = rowCON.CONDigitoAgencia;
                            if (rowCON.IsCONContaNull())
                                throw new Exception(string.Format("Condom�nio sem Conta: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                            novalinha.GPSConta = int.Parse(rowCON.CONConta);
                            if (rowCON.IsCONDigitoContaNull())
                                throw new Exception(string.Format("Condom�nio sem D�gito Conta: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                            novalinha.GPSDigConta = rowCON.CONDigitoConta;
                            novalinha.GPSCodComunicacao = "117045";
                            //*** MRC - TERMINO - TRIBUTOS (29/01/2015 12:00) ***)

                            //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
                            //if (row.Campo11 > 20000)
                            //{
                            //    novalinha.GPSStatus = (int)StatusArquivo.Liberar_Gerencia;
                            //    avisar = true;
                            //}
                            //else
                            //    if (row.Campo11 > 15000)
                            //    {
                            //        novalinha.GPSStatus = (int)StatusArquivo.Liberar_Gerente;
                            //        avisar = true;
                            //    }
                            //    else
                            if (row.Campo11 > 15000)
                            {
                                novalinha.GPSStatus = (int)StatusArquivo.Liberar;
                                avisar = true;
                            }
                            else
                                novalinha.GPSStatus = (int)StatusArquivo.ParaEnviar;
                            //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***                            
                            novalinha.GPSOrigem = (int)OrigemImposto.Folha;
                            novalinha.GPSI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                            novalinha.GPSDATAI = DateTime.Now;
                            dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
                            dGPS.dGPSSt.GPSTableAdapterX(novalinha.EMP).Update(novalinha);
                            Eletronicos++;
                            break;
                        case nTipoPag.Cheque:
                            //Insere cheque
                            if (row.Campo6 == 0)
                                continue;
                            //NoasList = null;
                            //if(DateTime.Today > new DateTime(2011,4,1))
                            //{
                            //    MessageBox.Show("Erro na data");
                            //    throw new Exception("Erro na data");
                            //}
                            Cheque NovoCheque = new Cheque(Vencimento, "INSS", row.CON, PAGTipo.Guia, false, 0, null, string.Empty, false, new FrameworkProc.EMPTProc(EMPTipo));

                            /*
                            dNOtAs.NOtAsRow rowNOA = GetrowNOAGPS(row);
                            rowNOA.NOATotal += row.Campo11;
                            dNOtAs.NOtAsTableAdapter.Update(rowNOA);
                            rowNOA.AcceptChanges();

                            dNOtAs.PAGamentosRow rowPag = dNOtAs.PAGamentos.NewPAGamentosRow();
                            rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                            rowPag.PAG_NOA = rowNOA.NOA;
                            rowPag.PAGDATAI = DateTime.Now;
                            rowPag.PAGI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                            rowPag.PAGTipo = (int)PAGTipo.Guia;
                            rowPag.PAGValor = row.Campo11;
                            //usar o objeto do imposto
                            //DateTime USAROIMPOSTO
                            rowPag.PAGVencimento = Vencimento;
                            rowPag.PAGPermiteAgrupar = true;
                            dNOtAs.PAGamentos.AddPAGamentosRow(rowPag);
                            dNOtAs.PAGamentosTableAdapter.Update(rowPag);
                            rowPag.AcceptChanges();
                            NovoCheque.AjustarTotalPelasParcelas();*/
                            Cheques++;
                            break;
                    };
                };
                dGPS.dGPSSt.GPSTableAdapter.Update(dGPS.dGPSSt.GPS);
                dGPS.dGPSSt.GPS.AcceptChanges();
                dAutomacaoBancaria.INSS.Clear();
                VirMSSQL.TableAdapter.ST().Commit();
                string men = string.Empty;
                if (Eletronicos > 0)
                    men += "Eletr�nicos: " + Eletronicos.ToString() + "\r\n";
                if (Cheques > 0)
                    men += "Cheques: " + Cheques.ToString() + "\r\n";
                MessageBox.Show(men);
                if (avisar)
                    MessageBox.Show("ATEN��O\r\n\r\nExistem guias retidas aguardando libera��o!!!");
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
                string errostr = string.Format("Erro no condom�nio: \r\n\r\n{0}", e.Message);
                MessageBox.Show(errostr);
                //throw e;
                //*** MRC - TERMINO - TRIBUTOS (29/01/2015 12:00) ***)
            }
        }

        /*
        int nArquivo;        
        //String Diretorio;

        private string GeraNomeDeArquivoGPS() {
            while (System.IO.File.Exists(Diretorio + nArquivo.ToString() + ".txt"))
                nArquivo++;
            return Diretorio + nArquivo.ToString() + ".txt";
        }
        */

        private void NovoTradutoPIS()
        {
            if (dAutomacaoBancaria.PIS.Count == 0)
                return;

            bool avisar = false;
            int Errados = 0;
            int Eletronicos = 0;
            int Cheques = 0;
            //ViewPIS.CloseEditor();
            //ViewPIS.ValidateEditor();
            ViewPIS.UpdateCurrentRow();

            DateTime PeriodoApuracao = dAutomacaoBancaria.PIS[0].Campo2;

            ImpostoNeon imposto_PISTMP = new ImpostoNeon(TipoImposto.PIS);
            imposto_PISTMP.DataBase = PeriodoApuracao;

            ImpostoNeon imposto_IRTMP = new ImpostoNeon(TipoImposto.IR);
            imposto_IRTMP.DataBase = PeriodoApuracao;

            DateTime VencimentoEfetivoUnificado = imposto_PISTMP.VencimentoEfetivo;
            DateTime VencimentoEfetivoUnificado1 = imposto_IRTMP.VencimentoEfetivo;
            if (VencimentoEfetivoUnificado1 < VencimentoEfetivoUnificado)
                VencimentoEfetivoUnificado = VencimentoEfetivoUnificado1;

            //int codigo = 0;

            //ImpostoNeon imposto;

            foreach (dAutomacaoBancaria.PISRow row in dAutomacaoBancaria.PIS)
            {
                if (!row.IsErroNull() || (nTipoPag)row.TipoPag == nTipoPag.Invalido)
                    Errados++;
                if (PeriodoApuracao != dAutomacaoBancaria.PIS[0].Campo2)
                {
                    MessageBox.Show(string.Format("Cacelado.\r\nExistem guias de diferentes compet�ncias no arquivo! {0:dd/MM/yyyy} e {1:dd/MM/yyyy}", PeriodoApuracao, row.Campo2));
                    return;
                }
            };
            if (Errados > 0)
                if (MessageBox.Show("Existem " + Errados.ToString() + " pagamentos que ser�o ignorados.\r\nConfirma assim mesmo?", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return;

            //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
            if (VirInput.Input.Senha("Senha PIS") != "CUIDADO-PIS")
            {
                MessageBox.Show("Senha incorreta");
                return;
            };

            DateTime Agendamento = DateTime.Today.AddDays(1);
            if (Agendamento > VencimentoEfetivoUnificado)
                Agendamento = DateTime.Today;
            while ((Agendamento.DayOfWeek == DayOfWeek.Saturday) || (Agendamento.DayOfWeek == DayOfWeek.Sunday))
                Agendamento = Agendamento.AddDays(1);
            DateTime? DataAgendarSel = Agendamento;
            if (VirInput.Input.Execute("Data de Pagamento", ref DataAgendarSel, DateTime.Today, DateTime.Today.AddDays(30)))
            {
                //if (!Framework.datasets.dFERiados.IsUtil(DataAgendarSel.Value, Framework.datasets.dFERiados.Sabados.naoUteis, Framework.datasets.dFERiados.Feriados.naoUteis))                
                if (!DataAgendarSel.Value.IsUtil())
                {
                    MessageBox.Show("Data de Pagamento inv�lida (n�o � dia �til)");
                    return;
                }
                if (DataAgendarSel > VencimentoEfetivoUnificado)
                {
                    MessageBox.Show("Data de Pagamento posterior ao vencimento efetivo unificado (" + VencimentoEfetivoUnificado.ToString("dd/MM/yyyy") + "). Pagamento deve ser efetuado com a data de hoje.");
                    return;
                }
            }
            else
                return;
            Agendamento = Convert.ToDateTime(DataAgendarSel);
            //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
            dAutomacaoBancaria.PIS.WriteXml(@"c:\sistema neon\reservaPIS.xml");
            dGPS.dGPSSt.GPS.Clear();
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("DP cAutomacaoBancaria - 3531");
                //VirMSSQL.TableAdapter.AbreTrasacaoSQL("DP cAutomacaoBancaria - 2693", dGPS.dGPSSt.GPSTableAdapter, dNOtAs.NOtAsTableAdapter, dNOtAs.PAGamentosTableAdapter);
                dGPS.dGPSSt.GPSTableAdapter.EmbarcaEmTransST();
                //dNOtAs.NOtAsTableAdapter.EmbarcaEmTransST();
                //dNOtAs.PAGamentosTableAdapter.EmbarcaEmTransST();
                dGPS.dGPSSt.GPSTableAdapterF.EmbarcaEmTransST();
                //dNOtAs.NOtAsTableAdapterF.EmbarcaEmTransST();
                //dNOtAs.PAGamentosTableAdapterF.EmbarcaEmTransST();

                //int contadorTeste = 4;                
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.AtivaGauge(dAutomacaoBancaria.PIS.Count);
                    foreach (dAutomacaoBancaria.PISRow row in dAutomacaoBancaria.PIS)
                    {
                        ESP.Gauge();
                        if (row.CON == 489)
                            System.Console.Write("------->{0} {1}\r\n", row.CON, row.Condominio);
                        System.Console.Write("------->{0} {1}\r\n", row.CON, row.Condominio);
                        if (row.Campo10 <= 0)
                            continue;

                        int codigo = int.Parse(row.Campo4);

                        ImpostoNeon imposto = (codigo == 8301) ? imposto_PISTMP : imposto_IRTMP;

                        //Framework.TipoPagPeriodico TipoPadrao;
                        //DateTime dataPagar;

                        //pis novo
                        /*
                        if (codigo == 8301) //pis
                        {

                            switch ((nTipoPag)row.TipoPag)
                            {
                                case nTipoPag.EletronicoBradesco:
                                    TipoPadrao = dPagamentosPeriodicos.TipoPagPeriodico.FolhaPISEletronico;
                                    dataPagar = Agendamento;
                                    break;
                                case nTipoPag.Cheque:
                                    TipoPadrao = dPagamentosPeriodicos.TipoPagPeriodico.FolhaPIS;
                                    dataPagar = VencimentoEfetivoUnificado;
                                    break;
                                default:
                                    continue;
                            }
                        }
                        else
                        {
                            switch ((nTipoPag)row.TipoPag)
                            {
                                case nTipoPag.EletronicoBradesco:
                                    TipoPadrao = dPagamentosPeriodicos.TipoPagPeriodico.FolhaIREletronico;
                                    dataPagar = Agendamento;
                                    break;
                                case nTipoPag.Cheque:
                                    TipoPadrao = dPagamentosPeriodicos.TipoPagPeriodico.FolhaIR;
                                    dataPagar = VencimentoEfetivoUnificado;
                                    break;
                                default:
                                    continue;
                            }
                        }

                        PagamentoPeriodico PagamentoPeriodico = new PagamentoPeriodico(row.CON, TipoPadrao);
                                                                                  

                        if(!PagamentoPeriodico.Encontrado)
                            PagamentoPeriodico.CadastraPGF(row.CON, 
                                                           TipoPadrao, 
                                                           VencimentoEfetivoUnificado,
                                                           row.Campo10,imposto.Competencia);

                        //MessageBox.Show("checar a competencia / Tipo alternativo");

                        PagamentoPeriodico.CadastraProximoPagamento(dataPagar,Agendamento, row.Campo10, imposto.Competencia);

                        //contadorTeste--;
                        //if(contadorTeste == 0)
                        //    break;
                        continue;
                        */

                        switch ((nTipoPag)row.TipoPag)
                        {
                            case nTipoPag.CreditoConta:
                                //Insere Tributo na tabela GPS
                                dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();
                                string CEP = "0";
                                foreach (char c in row.CEP)
                                    if (char.IsDigit(c))
                                        CEP += c;
                                DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(row.Campo3);
                                //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***
                                //novalinha.GPSTipo = (int)TipoImposto.PIS;
                                novalinha.GPSTipo = (int.Parse(row.Campo4) == 8301) ? (int)TipoImposto.PIS : (int)TipoImposto.IR;
                                //*** MRC - TERMINO - TRIBUTOS (29/01/2015 12:00) ***
                                novalinha.GPSNomeClientePagador = row.Campo1;
                                novalinha.GPS_CON = row.CON;
                                novalinha.EMP = row.EMP;
                                novalinha.GPSApuracao = row.Campo2;
                                novalinha.GPSAno = imposto.Competencia.Ano;
                                novalinha.GPSMes = imposto.Competencia.Mes;
                                novalinha.GPSVencimento = row.Campo6;
                                novalinha.GPSEndereco = row.Endereco;
                                novalinha.GPSCEP = int.Parse(CEP);
                                novalinha.GPSUF = row.UF;
                                novalinha.GPSCidade = (row.Cidade.Length > 20) ? row.Cidade.Substring(0, 20) : row.Cidade;
                                novalinha.GPSBairro = row.Bairro;
                                novalinha.GPSCNPJ = Cnpj.Valor;
                                novalinha.GPSDataPagto = Agendamento;
                                novalinha.GPSValorJuros = row.Campo9;
                                novalinha.GPSValorMulta = row.Campo8;
                                novalinha.GPSOutras = 0;
                                novalinha.GPSValorPrincipal = row.Campo7;
                                novalinha.GPSValorTotal = row.Campo10;
                                novalinha.GPSCodigoReceita = int.Parse(row.Campo4);
                                novalinha.GPSIdentificador = Cnpj.Valor;
                                novalinha.GPSNomeIdentificador = row.Campo1;

                                //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
                                EMPTipo EMPTipo1 = row.EMP == DSCentral.EMP ? EMPTipo.Local : EMPTipo.Filial;
                                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosSTX(EMPTipo1).CONDOMINIOS.FindByCON(row.CON);
                                if (rowCON.IsCON_BCONull())
                                    throw new Exception(string.Format("Condom�nio sem Banco: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                                if (rowCON.CON_BCO != 237)
                                    throw new Exception(string.Format("Conta principal do Condom�nio n�o � Bradesco, portanto n�o pode fazer Pag. Eletr. Bradesco: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                                if (rowCON.IsCONAgenciaNull())
                                    throw new Exception(string.Format("Condom�nio sem Ag�ncia: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                                novalinha.GPSAgencia = int.Parse(rowCON.CONAgencia);
                                if (rowCON.IsCONDigitoAgenciaNull())
                                    throw new Exception(string.Format("Condom�nio sem D�gito Ag�ncia: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                                novalinha.GPSDigAgencia = rowCON.CONDigitoAgencia;
                                if (rowCON.IsCONContaNull())
                                    throw new Exception(string.Format("Condom�nio sem Conta: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                                novalinha.GPSConta = int.Parse(rowCON.CONConta);
                                if (rowCON.IsCONDigitoContaNull())
                                    throw new Exception(string.Format("Condom�nio sem D�gito Conta: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                                novalinha.GPSDigConta = rowCON.CONDigitoConta;
                                novalinha.GPSCodComunicacao = "117045";
                                //*** MRC - TERMINO - TRIBUTOS (29/01/2015 12:00) ***)

                                //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
                                //if (row.Campo10 > 20000)
                                //{
                                //    novalinha.GPSStatus = (int)StatusArquivo.Liberar_Gerencia;
                                //    avisar = true;
                                //}
                                //else
                                //    if (row.Campo10 > 15000)
                                //    {
                                //        novalinha.GPSStatus = (int)StatusArquivo.Liberar_Gerente;
                                //        avisar = true;
                                //    }
                                //    else
                                if (row.Campo10 > 15000)
                                {
                                    novalinha.GPSStatus = (int)StatusArquivo.Liberar;
                                    avisar = true;
                                }
                                else
                                    novalinha.GPSStatus = (int)StatusArquivo.ParaEnviar;
                                //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
                                novalinha.GPSOrigem = (int)OrigemImposto.Folha;
                                novalinha.GPSI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                                novalinha.GPSDATAI = DateTime.Now;
                                dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
                                dGPS.dGPSSt.GPSTableAdapterX(novalinha.EMP).Update(novalinha);
                                Eletronicos++;
                                break;
                            case nTipoPag.Cheque:
                                TipoPagPeriodico TipoPeriodico = imposto.Tipo == TipoImposto.IR ? TipoPagPeriodico.FolhaIR : TipoPagPeriodico.FolhaPIS;
                                EMPTProc EMPTProc1 = new EMPTProc(row.EMP == DSCentral.EMP ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial,
                                                                  row.EMP == DSCentral.EMP ? Framework.DSCentral.USU : Framework.DSCentral.USUFilial);
                                PagamentoPeriodico PagamentoPeriodico = new PagamentoPeriodico(row.CON, TipoPeriodico, EMPTProc1);
                                if (!PagamentoPeriodico.Encontrado)
                                    PagamentoPeriodico.CadastraPGF(row.CON,
                                                                   TipoPeriodico,
                                                                   row.Campo6,
                                                                   row.Campo10, new Framework.objetosNeon.Competencia());
                                PagamentoPeriodico.CadastraProximoPagamento(row.Campo6, Agendamento, row.Campo10, imposto.Competencia);
                                /*


                                //Insere cheque
                                Cheque NovoCheque = new Cheque(Agendamento, "SECRETARIA DA RECEITA FEDERAL", row.CON, PAGTipo.Guia, false, 0);                            

                                dNOtAs.NOtAsRow rowNOA = GetrowNOAPis(row);
                                rowNOA.NOATotal += row.Campo10;
                                dNOtAs.NOtAsTableAdapter.Update(rowNOA);
                                rowNOA.AcceptChanges();

                                dNOtAs.PAGamentosRow rowPag = dNOtAs.PAGamentos.NewPAGamentosRow();
                                rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                                rowPag.PAG_NOA = rowNOA.NOA;
                                rowPag.PAGDATAI = DateTime.Now;
                                rowPag.PAGI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                                rowPag.PAGTipo = (int)PAGTipo.Guia;
                                rowPag.PAGValor = row.Campo10;
                                rowPag.PAGVencimento = Agendamento;
                                rowPag.PAGPermiteAgrupar = true;
                                dNOtAs.PAGamentos.AddPAGamentosRow(rowPag);
                                dNOtAs.PAGamentosTableAdapter.Update(rowPag);
                                rowPag.AcceptChanges();
                                NovoCheque.AjustarTotalPelasParcelas();*/
                                Cheques++;
                                break;
                        };
                        //break;
                    };
                };
                dAutomacaoBancaria.PIS.Clear();
                VirMSSQL.TableAdapter.CommitSQLDuplo();
                string men = string.Empty;
                if (Eletronicos > 0)
                    men += "Eletr�nicos: " + Eletronicos.ToString() + "\r\n";
                if (Cheques > 0)
                    men += "Cheques: " + Cheques.ToString() + "\r\n";
                MessageBox.Show(men);
                if (avisar)
                    MessageBox.Show("ATEN��O\r\n\r\nExistem guias retidas aguardando libera��o!!!");
            }
            catch (Exception e)
            {
                TableAdapter.VircatchSQL(e);
                //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
                string errostr = string.Format("Erro no condom�nio: \r\n\r\n{0}", e.Message);
                MessageBox.Show(errostr);
                //throw e;
                //*** MRC - TERMINO - TRIBUTOS (29/01/2015 12:00) ***)
            }
        }



        private void OcultarTabPageDados()
        {
            //Ocultando TABPAGES de Dados            
            foreach (DevExpress.XtraTab.XtraTabPage Tab in TabControlAutomacao.TabPages)
                if (!Tab.EstaNoGrupo(TabPageRTF, TabRetornoRec, TabTarefas))
                    Tab.PageVisible = false;
        }

        private string ProximaLinha(CompontesBasicos.Espera.cEspera ESP)
        {
            if (ESP != null)
                ESP.GaugeTempo(Inicio);
            if (Terminado)
                return string.Empty;
            else
            {
                Inicio += Tamanho;
                if (Inicio != 0)
                    for (; (Inicio < tamanhototal) && (ConteudoTotal[Inicio] != Char.Parse("\n")); Inicio++) ;
                else
                    Inicio = -1;
                if ((Inicio + 1) >= tamanhototal)
                {
                    Terminado = true;
                    return string.Empty;
                }
                else
                {
                    Inicio++;
                    for (Tamanho = 1; (Inicio + Tamanho < tamanhototal) && ConteudoTotal[Inicio + Tamanho] != Char.Parse("\n"); Tamanho++) ;
                    return ConteudoTotal.Substring(Inicio, Tamanho);
                };
            };

        }

        private string ProximaLinha(int linhas, CompontesBasicos.Espera.cEspera ESP = null)
        {
            for (int i = 1; i < (linhas); i++)
                ProximaLinha(ESP);
            return ProximaLinha(ESP);
        }






        private void repositoryItemButtonDelete_Click(object sender, EventArgs e)
        {
            int CancelamentoNaoFeito = 0;
            dAutomacaoBancaria.RHTarefaRow RHTrow = (dAutomacaoBancaria.RHTarefaRow)gridView6.GetFocusedDataRow();
            if (RHTrow == null)
                return;
            if (MessageBox.Show("Confirma o cancelamento de todos os cheque gerados por esta tarefa?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Levantando dados");
                    int EMPapagar = RHTrow.EMP;
                    int nApagar = dAutomacaoBancariaTarefas.RHTarefaSubdetalheTableAdapterX(EMPapagar).FillByRHT(dAutomacaoBancariaTarefas.RHTarefaSubdetalhe, RHTrow.RHT);
                    int i = 0;
                    ESP.AtivaGauge(nApagar);
                    List<dAutomacaoBancaria.RHTarefaSubdetalheRow> ApagarS = new List<dAutomacaoBancaria.RHTarefaSubdetalheRow>();
                    List<dAutomacaoBancaria.RHTarefaDetalheRow> ApagarD = new List<dAutomacaoBancaria.RHTarefaDetalheRow>();
                    List<int> CancelarCHEs = new List<int>();
                    bool CancelaDelRHT = false;
                    foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in RHTrow.GetRHTarefaDetalheRows())
                    {
                        bool CancelaDelRHD = false;
                        foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in RHDrow.GetRHTarefaSubdetalheRows())
                        {
                            i++;
                            ESP.GaugeTempo(i);
                            if (RHSrow.IsRHS_CHENull())
                                ApagarS.Add(RHSrow);
                            else
                            {
                                if (!CancelarCHEs.Contains(RHSrow.RHS_CHE))
                                    CancelarCHEs.Add(RHSrow.RHS_CHE);
                                CancelaDelRHD = CancelaDelRHT = true;
                            }
                        }

                        if (!CancelaDelRHD)
                            ApagarD.Add(RHDrow);
                    }
                    ESP.Espere("Apagando 1/2");
                    i = 0;
                    ESP.AtivaGauge(ApagarS.Count);
                    foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in ApagarS)
                    {
                        i++;
                        ESP.GaugeTempo(i);
                        RHSrow.Delete();
                        dAutomacaoBancariaTarefas.RHTarefaSubdetalheTableAdapterX(EMPapagar).Update(RHSrow);
                    };
                    ESP.Espere("Apagando 2/2");
                    i = 0;
                    ESP.AtivaGauge(ApagarD.Count);
                    foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in ApagarD)
                    {
                        i++;
                        ESP.GaugeTempo(i);
                        RHDrow.Delete();
                        dAutomacaoBancariaTarefas.RHTarefaDetalheTableAdapterX(EMPapagar).Update(RHDrow);
                    }
                    if (!CancelaDelRHT)
                    {
                        RHTrow.Delete();
                        dAutomacaoBancariaTarefas.RHTarefaTableAdapterX(EMPapagar).Update(RHTrow);
                    }
                    ESP.Espere("Cancelando Cheques");
                    i = 0;
                    ESP.AtivaGauge(CancelarCHEs.Count);
                    foreach (int CHE in CancelarCHEs)
                    {
                        i++;
                        ESP.GaugeTempo(i);
                        Cheque Cheque = new Cheque(CHE, ChequeProc.TipoChave.CHE, EMPTProc.EMPTProcStX(RHTrow.EMP));
                        if (Cheque.CHEStatus.EstaNoGrupo(CHEStatus.Compensado, CHEStatus.CompensadoAguardaCopia))
                            CancelamentoNaoFeito++;
                        else
                            if ((Cheque.Tipo.EstaNoGrupo(PAGTipo.cheque, PAGTipo.ChequeSindico)) && (Cheque.CHEStatus.EstaNoGrupo(CHEStatus.Retornado, CHEStatus.ComSindico)))
                            CancelamentoNaoFeito++;
                        else
                            Cheque.CancelarCheque("Cancelada a tarefa pelo DP");
                    }
                }
                dAutomacaoBancaria.RHTarefaSubdetalhe.Clear();
                dAutomacaoBancaria.RHTarefaDetalhe.Clear();
                dAutomacaoBancaria.RHTarefa.Clear();
                if (CancelamentoNaoFeito > 0)
                    MessageBox.Show(string.Format("Existem cheques n�o cancelados, verificar caso a caso: Total = {0}", CancelamentoNaoFeito));
            }
            AjustaStatus();
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                dAutomacaoBancaria.RHTarefaRow RHTrow = (dAutomacaoBancaria.RHTarefaRow)gridView6.GetFocusedDataRow();
                if (RHTrow == null)
                    return;
                FolhaPag.cFolhaPag TarefaReaberta = new FolhaPag.cFolhaPag(RHTrow.EMP, RHTrow.RHT, this);
                TarefaReaberta.VirShowModulo(EstadosDosComponentes.JanelasAtivas);

                /*
                dAutomacaoBancaria.RHTarefaSubdetalhe.Clear();
                dAutomacaoBancaria.RHTarefaDetalhe.Clear();
                dAutomacaoBancaria.RHTarefa.Clear();
                cCompet1.Comp.CompetenciaBind = RHTrow.RHTCompetencia;
                cCompet1.AjustaTela();
                imageComboTipo.EditValue = RHTrow.RHTTipo;
                tipoBancaria = (TipoBancaria)imageComboTipo.EditValue;
                dateCreditoConta.DateTime = RHTrow.RHTDataCredito;
                dateDataCheque.DateTime = RHTrow.RHTDataCheque;
                lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue = RHTrow.RHT_PLA;
                if (RHTrow.EMP == Framework.DSCentral.EMP)
                {
                    dAutomacaoBancaria.RHTarefaTableAdapter.FillByRHT(dAutomacaoBancaria.RHTarefa,RHTrow.RHT);
                    rowmaeRHT = dAutomacaoBancaria.RHTarefa[0];
                    rowmaeRHTF = null;
                    dAutomacaoBancaria.RHTarefaDetalheTableAdapter.FillByRHT(dAutomacaoBancaria.RHTarefaDetalhe, RHTrow.RHT);
                    dAutomacaoBancaria.RHTarefaSubdetalheTableAdapter.FillByRHT(dAutomacaoBancaria.RHTarefaSubdetalhe, RHTrow.RHT);
                }
                else
                {
                    dAutomacaoBancaria.RHTarefaTableAdapterF.FillByRHT(dAutomacaoBancaria.RHTarefa, RHTrow.RHT);
                    rowmaeRHTF = dAutomacaoBancaria.RHTarefa[0];
                    //rowmaeRHTF = RHTrow;
                    rowmaeRHT = null;
                    dAutomacaoBancaria.RHTarefaDetalheTableAdapterF.FillByRHT(dAutomacaoBancaria.RHTarefaDetalhe, RHTrow.RHT);
                    dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterF.FillByRHT(dAutomacaoBancaria.RHTarefaSubdetalhe, RHTrow.RHT);
                }
                //rHTarefaDetalheBindingSource.Filter = string.Format("RHD_RHT = {0}", RHTrow.RHT);
                //rHTarefaSubdetalheBindingSource.Filter = string.Format("RHD_RHT = {0}", RHTrow.RHT);
                dateCreditoConta.DateTime = RHTrow.RHTDataCredito;
                dateDataCheque.DateTime = RHTrow.RHTDataCheque;
                //ESP.Espere("Carregando funcion�rios");
                dAutomacaoBancaria.RecarregarFuncionarios(false);
                dAutomacaoBancaria.CompletaCampos();
                ValidaPagamentos();
                trocaTipo(ESP, true);
                CarregaHistoricoPeriodicos();
                TabPagePagamentos.PageVisible = true;
                TabControlAutomacao.SelectedTabPage = TabPagePagamentos;
                */
            }
        }





        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string Titulo = string.Empty;
            VirInput.Input.Execute("T�tulo:", ref Titulo, false);
            if (Titulo == string.Empty)
                return;
            impressos.dRecSal.HOleritTituloRow rowHOT = null;
            impressos.dRecSal.HOleritTituloRow rowHOTF = null;
            impressos.dRecSal.HOleritTituloRow rowHOTX;
            //VirMSSQL.TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.SQL).AbreTrasacaoLocal("HOT 1");
            //VirMSSQL.TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Filial).AbreTrasacaoLocal("HOT 1");
            //int HOT = TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.SQL).IncluirAutoInc(Comando, Titulo, DateTime.Now);
            //int HOTF = TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Filial).IncluirAutoInc(Comando, Titulo, DateTime.Now);
            //VirMSSQL.TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.SQL).Commit();
            //VirMSSQL.TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Filial).Commit();
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.AtivaGauge(ImpRecSal.dRecSal.Recibos.Count);
                Application.DoEvents();
                int i = 0;
                DateTime Prox = DateTime.Now.AddSeconds(3);
                foreach (impressos.dRecSal.RecibosRow rowREC in ImpRecSal.dRecSal.Recibos)
                {
                    i++;
                    if (DateTime.Now > Prox)
                    {
                        ESP.Gauge(i);
                        Application.DoEvents();
                        Prox = DateTime.Now.AddSeconds(3);
                    }
                    if (rowREC.IsHOLNull())
                    {
                        if (rowREC.EMP == Framework.DSCentral.EMP)
                        {
                            if (rowHOT == null)
                            {
                                rowHOT = ImpRecSal.dRecSal.HOleritTitulo.AddHOleritTituloRow(Titulo, DateTime.Now, rowREC.EMP, 0);
                                ImpRecSal.dRecSal.HOleritTituloTableAdapter.Update(rowHOT);
                            }
                            rowHOTX = rowHOT;
                        }
                        else
                        {
                            if (rowHOTF == null)
                            {
                                rowHOTF = ImpRecSal.dRecSal.HOleritTitulo.AddHOleritTituloRow(Titulo, DateTime.Now, rowREC.EMP, 0);
                                ImpRecSal.dRecSal.HOleritTituloTableAdapterF.Update(rowHOTF);
                            }
                            rowHOTX = rowHOTF;
                        }
                        impressos.dRecSal.HOLeritesRow NovaLinha = ImpRecSal.dRecSal.HOLerites.NewHOLeritesRow();
                        if (rowREC.IsCONNull())
                        {
                            MessageBox.Show("Condom�nio n�o encontrado");
                            rowREC.RowError = "Condominio n�o encontrado";
                            break;
                        }
                        NovaLinha.HOL_CON = rowREC.CON;
                        NovaLinha.HOLb1 = rowREC.Qb1;
                        NovaLinha.HOLb2 = rowREC.Qb2;
                        NovaLinha.HOLb3 = rowREC.Qb3;
                        NovaLinha.HOLb4 = rowREC.Qb4;
                        NovaLinha.HOLb5 = rowREC.Qb5;
                        NovaLinha.HOLCod = rowREC.Cod;
                        NovaLinha.HOLDescontos = rowREC.Descontos;
                        NovaLinha.HOLDescricao = rowREC.Descricao;
                        NovaLinha.HOLDuasvias = rowREC.DuasVias;
                        NovaLinha.HOLEndereco = rowREC.BOLEndereco;
                        NovaLinha.HOLNome = rowREC.BOLNome;
                        NovaLinha.HOLQ0 = rowREC.Q0;
                        NovaLinha.HOLQ1 = rowREC.Q1;
                        NovaLinha.HOLQ2 = rowREC.Q2;
                        NovaLinha.HOLQ3 = rowREC.Q3;
                        NovaLinha.HOLQOutros = rowREC.QOutros;
                        NovaLinha.HOLRef = rowREC.Ref;
                        NovaLinha.HOLVenc = rowREC.Venc;
                        NovaLinha.HOLQTot1 = rowREC.Qtot1;
                        NovaLinha.HOLQTot2 = rowREC.Qtot2;
                        NovaLinha.HOL_HOT = rowHOTX.HOT;
                        NovaLinha.EMP = rowHOTX.EMP;
                        ImpRecSal.dRecSal.HOLerites.AddHOLeritesRow(NovaLinha);
                        ImpRecSal.dRecSal.HOLeritesTableAdapterX(rowREC.EMP).Update(NovaLinha);
                        NovaLinha.AcceptChanges();
                        rowREC.HOL = NovaLinha.HOL;
                    }
                }
            }
            cBotaoImpBol1.Enabled = cBotaoImpBol2.Enabled = true;
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            impressos.dRecSal.HOLeritesRow rowHOL = LinhaMae;
            if (rowHOL != null)
            {
                if (!BaixaManualLiberada)
                {
                    string Leitura = string.Empty;
                    if (VirInput.Input.Execute("DIGITE 'BAIXA MANUAL'", ref Leitura, false))
                        if (Leitura.ToUpper().Trim() == "BAIXA MANUAL")
                            BaixaManualLiberada = true;
                }
                if (BaixaManualLiberada)
                {
                    rowHOL.HOLRetorno = DateTime.Today;
                    dRecSal.HOLeritesTableAdapterX(rowHOL.EMP).Update(rowHOL);
                }
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            CarregaTarefas(true, false);
        }



        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (dAutomacaoBancaria.CONDOMINIOS.Rows.Count == 0)
                dAutomacaoBancaria.CONDOMINIOSTableAdapter.Fill(dAutomacaoBancaria.CONDOMINIOS);
            dRecSal.HOLeritesTableAdapter.FillDEATE(dRecSal.HOLerites, (int)spinDE.Value, (int)spinATE.Value);
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            foreach (impressos.dRecSal.HOLeritesRow rowHOL in dRecSal.HOLerites)
                if (!rowHOL.IsHOLRetornoNull())
                    if (rowHOL.HOLRetorno.TimeOfDay.TotalMilliseconds == 0)
                    {
                        rowHOL.SetHOLRetornoNull();
                        dRecSal.HOLeritesTableAdapterX(rowHOL.EMP).Update(rowHOL);
                    }
        }






        /*
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if(!System.IO.Directory.Exists(@"J:\virtual\retornoRES\"))
                System.IO.Directory.CreateDirectory(@"J:\virtual\retornoRES\");
            this.dAutomacaoBancaria.CONDOMINIOS.Clear();
            this.cONDOMINIOSTableAdapter.Fill(this.dAutomacaoBancaria.CONDOMINIOS);
            string lote;
            string CodigoBancoCS;
            string codigoretorno;
            string cartao;
            // listBox1.Items.Clear();
            int con;
            int nArq = 0;
            foreach (string Arquivo in System.IO.Directory.GetFiles("J:\\virtual\\retorno")) 
            {
                if (Arquivo.StartsWith("J:\\virtual\\retorno\\SA") && (Arquivo.EndsWith(".RET")))
                {
                    
                    using (StreamReader sr = new StreamReader(Arquivo)) 
                    {
                        string linha = sr.ReadLine();
                        lote = linha.Substring(68,5);
                        CodigoBancoCS = linha.Substring(1, 8);

                        //DocBacarios.CPFCNPJ obCnpj = new DocBacarios.CPFCNPJ(CNPJ);
                        //CNPJ = obCnpj.ToString(true);
                        DataRow[] rowCondominio = dAutomacaoBancaria.CONDOMINIOS.Select("CONCodigoBancoCS='" + CodigoBancoCS + "'");
                        if (rowCondominio.Length != 1){
                            //listBox1.Items.Add("****XXXXXX****** CS:" + CodigoBancoCS + " Linha:" + linha);                            
                        }
                        else{
                            con=((dAutomacaoBancaria.CONDOMINIOSRow)rowCondominio[0]).CON;
                            //listBox1.Items.Add("LOTE:"+lote+" CS:"+CodigoBancoCS+" arquivo:"+Arquivo);
                            while (((linha = sr.ReadLine()) != null) && (linha[0] == Char.Parse("1")))
                            {
                                int nLote = int.Parse(lote);
                                if (nLote == 0) { 
                                    lote = linha.Substring(364, 7);
                                    nLote = int.Parse(lote);
                                }; 
                                codigoretorno = linha.Substring(278, 2);
                                cartao = linha.Substring(379, 7);
                                //listBox1.Items.Add("       CARTAO:" + cartao + " RETORNO:" + codigoretorno);
                                detremessadpTableAdapter1.AjustaStatus(codigoretorno,int.Parse(lote),con ,int.Parse(cartao));
                            }
                        }
                    };
                    string Destino = @"J:\virtual\retornoRES\" + Path.GetFileName(Arquivo);
                    if (File.Exists(Destino))
                        File.Delete(Destino);
                    File.Move(Arquivo, Destino);
                    nArq++;
                }
            }
            if (nArq == 0)
                MessageBox.Show("Nenhum arquivo processado");
            else
                MessageBox.Show(nArq.ToString()+" arquivos processados");
            this.remessasPendentesTableAdapter.Fill(this.dAutomacaoBancaria.RemessasPendentes);
        }
        */

        private string StringArquivo(string nome)
        {
            string retorno = string.Empty;
            foreach (Char carac in nome)
                if (Char.IsLetterOrDigit(carac) || Char.IsWhiteSpace(carac))
                    retorno += carac;
            return retorno;
        }



        private void TabControlAutomacao_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page == TabRetornoRec)
                CarregaTitulosHol();

        }

        /*
        private void RemessaCartaoSalarioBradesco(DateTime DataParaDebito)
        {
            /*** ARQUIVO BRADESCO - CARTAO SALARIO 
             *** Existe um outro componente chamado transferencia cartao salario 
             *** onde possui este mesmo codigo. As alteracoes nesta rotina deverao
             *** ser repetidas no outro componente tambem ***

            
            String Diretorio = @"J:\Virtual\Cartao Salario\" + DateTime.Now.ToString("MMMyy") + "\\";
                        

            if (!ExisteDiretorio(Diretorio))
            {
                Diretorio = @"C:\Virtual\Cartao Salario\" + DateTime.Now.ToString("MMMyy") + "\\";
                if (!ExisteDiretorio(Diretorio))
                    return;
            }

            if (!ExisteDiretorio(Diretorio))
                return;
            
            //Gerando Numero do Cartao Salario

            DateTime horageracao = DateTime.Now;

            
            dAutomacaoBancaria.REMESSASDPRow novalinha = (dAutomacaoBancaria.REMESSASDPRow)this.dAutomacaoBancaria.REMESSASDP.NewRow();
            //dAutomacaoBancaria.REMESSASDPRow novalinha = (dAutomacaoBancaria.REMESSASDPRow)this.dAutomacaoBancaria.REMESSASDP.AddREMESSASDPRow(0, horageracao);
            novalinha.REMStatus = 0;
            novalinha.REMDataHora = horageracao;
            this.dAutomacaoBancaria.REMESSASDP.AddREMESSASDPRow(novalinha);
            novalinha.EndEdit();

            this.rEMESSASDPTableAdapter.Update(this.dAutomacaoBancaria.REMESSASDP);
            this.dAutomacaoBancaria.AcceptChanges();
            //Int32 NUMCartaoSalario = ((dAutomacaoBancaria.REMESSASDPRow)(this.dAutomacaoBancaria.REMESSASDP.Rows[0])).REM;
            Int32 NUMCartaoSalario = novalinha.REM;
            String Arquivo;
            int numeroenvio = 0;
            do
            {
                numeroenvio++;
                Arquivo = Diretorio +
                             "SA" +
                             horageracao.ToString("ddMM") +
                             numeroenvio.ToString("00") +
                             ".REM";
            }
            while (File.Exists(Arquivo));
            
                   

            this.salariosBindingSource.RemoveFilter();
            this.salariosBindingSource.RemoveSort();

            this.salariosBindingSource.Filter = "FUNIDBanco='237' And FUNTipoConta='CART�O SAL�RIO' and FUNValor>0";
            this.salariosBindingSource.Sort = "CONCodigo ASC";

            if (this.salariosBindingSource.Count > 0)
            {
                this.labPagamentos.Text = "Gerando remessa CART�O SAL�RIO";
                this.pgbPagamentos.Value = 0;

                this.labPagamentos.Visible = true;
                this.pgbPagamentos.Visible = true;

                this.pgbPagamentos.Maximum = this.salariosBindingSource.Count;
               
                using (StreamWriter swArquivo = new StreamWriter(Arquivo))
                {
                    String sCODCON = "";

                    Int32 iSeqRegistro = 0;

                    Decimal nVrTotal = 0;

                    for (Int32 iPosicao = 0; iPosicao < this.salariosBindingSource.Count; iPosicao++)
                    {

                        this.pgbPagamentos.Value++;
                        
                        this.salariosBindingSource.Position = iPosicao;

                        DataRowView drvCARTAO = ((DataRowView)(this.salariosBindingSource.Current));



                        dAutomacaoBancaria.SalariosRow RowSalarios = (dAutomacaoBancaria.SalariosRow)drvCARTAO.Row;

                   //     if (RowSalarios.FUNValor == 0)
                     //       continue;

                        if (sCODCON.Trim() != drvCARTAO["CONCodigo"].ToString().Trim())
                        {
                            sCODCON = drvCARTAO["CONCodigo"].ToString();

                            nVrTotal = 0;

                            iSeqRegistro = 1;

                            String sIdHeader = "0";
                            String sCODBancoCS = FuncoesDP.CampoNumerico(drvCARTAO["CONRazaoCS"], 8);
                            String sTipoInscr = "2";

                            //CNPJ
                            String sCNPJAux = FuncoesDP.CampoNumerico(drvCARTAO["CONCnpj"], 15);

                            if (sCNPJAux.Trim() == "000000000000000")
                            {
                                MessageBox.Show("O campo CNPJ do condom�nio " + sCODCON + " n�o foi informado ou est� incorreto.\r\nVerifique o valor do campo e tente gerar o arquivo novamente.", "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                swArquivo.Close();
                                return;
                            }

                            String sCNPJBase = FuncoesDP.CampoNumerico(sCNPJAux.Substring(0, 9), 9);
                            String sCNPJFilial = FuncoesDP.CampoNumerico(sCNPJAux.Substring(9, 4), 4);
                            String sCNPJControle = FuncoesDP.CampoNumerico(sCNPJAux.Substring(13, 2), 2);

                            String sRazaoSocial = FuncoesDP.CampoAlfaNumerico(drvCARTAO["CONNome"].ToString(), 40);
                            String sIdServico = "70";
                            String sOrigemArquivo = "1";
                            String sNumRemessa = FuncoesDP.CampoNumerico(NUMCartaoSalario, 5);
                            String sReservaH1 = FuncoesDP.CampoAlfaNumerico(" ", 5);
                            String sDtGravacao = DateTime.Now.ToString("yyyyMMdd");
                            String sHrGravacao = DateTime.Now.ToString("HHmmss");
                            String sReservaH2 = FuncoesDP.CampoAlfaNumerico(" ", 5);
                            String sReservaH3 = FuncoesDP.CampoAlfaNumerico(" ", 3);
                            String sReservaH4 = FuncoesDP.CampoAlfaNumerico(" ", 80);
                            String sReservaH5 = FuncoesDP.CampoAlfaNumerico(" ", 88);
                            String sAgenciaDeb = FuncoesDP.CampoNumerico(drvCARTAO["CONAgencia"], 5);
                            String sContaDeb = FuncoesDP.CampoNumerico(drvCARTAO["CONConta"], 7);
                            String sdContaDeb = FuncoesDP.CampoNumerico(drvCARTAO["CONDigitoConta"], 1);
                            String sReservaH6 = FuncoesDP.CampoAlfaNumerico(" ", 194);
                            String sReservaH7 = FuncoesDP.CampoAlfaNumerico(" ", 19);
                            String sSeqHeader = FuncoesDP.CampoNumerico(iSeqRegistro, 6);

                            swArquivo.WriteLine(
                                                sIdHeader +
                                                sCODBancoCS +
                                                sTipoInscr +
                                                sCNPJBase +
                                                sCNPJFilial +
                                                sCNPJControle +
                                                sRazaoSocial +
                                                sIdServico +
                                                sOrigemArquivo +
                                                sNumRemessa +
                                                sReservaH1 +
                                                sDtGravacao +
                                                sHrGravacao +
                                                sReservaH2 +
                                                sReservaH3 +
                                                sReservaH4 +
                                                sReservaH5 +
                                                sAgenciaDeb +
                                                sContaDeb +
                                                sdContaDeb +
                                                sReservaH6 +
                                                sReservaH7 +
                                                sSeqHeader
                                             );
                        }

                        /*** Registro Transa��o ***/
        /*** Primeiro registro de transacao � extraido da linha atual ( Mesma linha do HEADER)

        iSeqRegistro++;

        String sIdTransacao = "1";
        String sReservaT1 = FuncoesDP.CampoAlfaNumerico(" ", 277);
        String sReservaT2 = FuncoesDP.CampoAlfaNumerico(" ", 02);
        String sReservaT3 = FuncoesDP.CampoAlfaNumerico(" ", 02);
        String sReservaT4 = FuncoesDP.CampoAlfaNumerico(" ", 02);
        String sReservaT5 = FuncoesDP.CampoAlfaNumerico(" ", 02);
        String sReservaT6 = FuncoesDP.CampoAlfaNumerico(" ", 02);
        String sTipoMotivo = "0"; //Inclusao de Pagto
        String sNomeFunc = FuncoesDP.CampoAlfaNumerico(drvCARTAO["FUNNome"], 26);

        //CPF... nao existe no Layout oficial do banco mais existe no MS-ACCESS
        String sCPF = drvCARTAO["FUNCPF"].ToString();
        sCPF = sCPF.Substring(0, 9) + "0000" + sCPF.Substring(9, 2);

        String sReservaT7 = FuncoesDP.CampoAlfaNumerico(sCPF, 32);
        String sReservaT8 = FuncoesDP.CampoAlfaNumerico(" ", 02);
        String sReservaT9 = FuncoesDP.CampoAlfaNumerico(" ", 15);
        String sNumPagto = FuncoesDP.CampoNumerico(NUMCartaoSalario, 7);
        String sDataPagto = DataParaDebito.ToString("yyyyMMdd");
        String sNumCartao = FuncoesDP.CampoNumerico(drvCARTAO["FUNConta"], 7);
        String sValorCred = FuncoesDP.CampoNumerico(drvCARTAO["FUNValor"], 15);
        String sReservaT10 = FuncoesDP.CampoNumerico(0, 15);
        String sReservaT11 = FuncoesDP.CampoAlfaNumerico(" ", 8);
        String sReservaT12 = FuncoesDP.CampoAlfaNumerico(" ", 8);
        String sNumNovoCartao = FuncoesDP.CampoNumerico(0, 7);
        String sReservaT13 = FuncoesDP.CampoAlfaNumerico(" ", 36);
        String sReservaT14 = FuncoesDP.CampoAlfaNumerico(" ", 19);
        String sSeqTransacao = FuncoesDP.CampoNumerico(iSeqRegistro, 6);

        swArquivo.WriteLine(
                            sIdTransacao +
                            sReservaT1 +
                            sReservaT2 +
                            sReservaT3 +
                            sReservaT4 +
                            sReservaT5 +
                            sReservaT6 +
                            sTipoMotivo +
                            sNomeFunc +
                            sReservaT7 +
                            sReservaT8 +
                            sReservaT9 +
                            sNumPagto +
                            sDataPagto +
                            sNumCartao +
                            sValorCred +
                            sReservaT10 +
                            sReservaT11 +
                            sReservaT12 +
                            sNumNovoCartao +
                            sReservaT13 +
                            sReservaT14 +
                            sSeqTransacao
                         );
        //Registra no banco de dados
        dAutomacaoBancaria.DETREMESSADPRow novalinhadet = dAutomacaoBancaria.DETREMESSADP.NewDETREMESSADPRow();                        
        novalinhadet.DRE_CON = RowSalarios.CON;
        novalinhadet.DRECartao = int.Parse(sNumCartao);
        novalinhadet.DRE_REM = NUMCartaoSalario;
        novalinhadet.DREStatus = "--";
        novalinhadet.EndEdit();
        this.dAutomacaoBancaria.DETREMESSADP.AddDETREMESSADPRow(novalinhadet);

        this.detremessadpTableAdapter1.Update(dAutomacaoBancaria.DETREMESSADP);
        this.dAutomacaoBancaria.AcceptChanges();

        //Acumulando Total da Transacao
        nVrTotal += Convert.ToDecimal(drvCARTAO["FUNValor"]);

        /*** Registro TRAILER **
        Boolean bPrintTrailer = false;

        if (iPosicao == (this.salariosBindingSource.Count - 1))
            bPrintTrailer = true;
        else
        {
            this.salariosBindingSource.Position++;

            DataRowView _D = ((DataRowView)(salariosBindingSource.Current));

            if (sCODCON != _D["CONCodigo"].ToString())
                bPrintTrailer = true;

            this.salariosBindingSource.Position--;
        }


        if (bPrintTrailer)
        {
            iSeqRegistro++;

            String sIdTrailer = "9";
            String sQtdeRegistro = FuncoesDP.CampoNumerico(iSeqRegistro, 6);
            String sValorTotal = FuncoesDP.CampoNumerico(nVrTotal, 17);
            String sReservaTr1 = FuncoesDP.CampoAlfaNumerico(" ", 451);
            String sReservaTr2 = FuncoesDP.CampoAlfaNumerico(" ", 19);
            String sSeqTrailer = FuncoesDP.CampoNumerico(iSeqRegistro, 6);

            swArquivo.WriteLine(
                                sIdTrailer +
                                sQtdeRegistro +
                                sValorTotal +
                                sReservaTr1 +
                                sReservaTr2 +
                                sSeqTrailer
                             );
        }

    }
    swArquivo.Flush();
    swArquivo.Close();

    this.labPagamentos.Visible = false;
    this.pgbPagamentos.Visible = false;
}
};

this.salariosBindingSource.RemoveFilter();
this.salariosBindingSource.RemoveSort();
}
*/





        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string Original = textEdit1.Text;
                if (Original.Length > 4)
                {
                    string Limpo = (Original[0] == '0') ? Original.Substring(1, Original.Length - 2) : Original.Substring(0, Original.Length - 1);
                    int EMP = int.Parse(Limpo.Substring(0, 1));
                    int Tipo = int.Parse(Limpo.Substring(1, 2));
                    int HOL = int.Parse(Limpo.Substring(3));
                    if ((Tipo == 1) || ((Tipo == 2)))
                    {
                        impressos.dRecSal.HOLeritesRow rowHOL = dRecSal.HOLerites.FindByHOL(HOL);
                        if (rowHOL == null)
                        {
                            string Comando =
"SELECT     HOLRetorno\r\n" +
"FROM         HOLerites\r\n" +
"WHERE     (HOL = @P1);";
                            DataRow rowRet = TableAdapter.ST(EMP == DSCentral.EMP ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial).BuscaSQLRow(Comando, HOL);
                            if (rowRet == null)
                                throw new Exception("Holerite nao encotrado\r\nCB:" + Original);
                            if (rowRet["HOLRetorno"] == DBNull.Value)
                                dRecSal.HOLeritesTableAdapterX(EMP).Baixa(HOL);
                        }
                        else
                        {
                            rowHOL.HOLRetorno = DateTime.Now;
                            dRecSal.HOLeritesTableAdapterX(EMP).Update(rowHOL);
                            rowHOL.AcceptChanges();
                        }
                    }
                }
                textEdit1.Text = string.Empty;
            }
        }



        private void timerAviso_Tick(object sender, EventArgs e)
        {
            timerAviso.Enabled = false;
            MessageBox.Show("ATEN��O: Tarefas Pendentes");
        }



        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            rEMESSASDPTableAdapter.Apagar(DateTime.Today.AddMonths(-2));
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            if (dAutomacaoBancaria.PIS.Count != 0)
                dAutomacaoBancaria.PIS.Clear();
            else
                dAutomacaoBancaria.PIS.ReadXml(@"c:\sistema neon\reservaPIS.xml");
        }





        private void txtZoomRTF_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) && (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) && e.KeyCode != Keys.Back && e.KeyCode != Keys.OemPeriod)
            {
                if (e.KeyCode == Keys.OemPeriod)
                    this.edtZoomRTF.Text += ",";

                e.SuppressKeyPress = true;
            }
        }

        private void txtZoomRTF_Validated(object sender, EventArgs e)
        {
            if (Convert.ToDouble(this.edtZoomRTF.Text) == 0)
                this.edtZoomRTF.Text = "0,5";
        }

        private int USUEquivalente(int USUF)
        {
            if (USUEquivalentes == null)
                USUEquivalentes = new SortedList<int, int>();
            if (!USUEquivalentes.ContainsKey(USUF))
            {
                int? USULocal = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select USU from usuarios where USU__USUfilial = @P1", USUF);
                if (USULocal.HasValue)
                    USUEquivalentes.Add(USUF, USULocal.Value);
            }
            return USUEquivalentes[USUF];
        }


        /*
        private void ViewPagamentos_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e == null)
                return;
            if (e.RowHandle < 0)
                return;
            if (e.Column == null)
                return;
            dAutomacaoBancaria.RHTarefaSubdetalheRow row = (dAutomacaoBancaria.RHTarefaSubdetalheRow)ViewPagamentos.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if ((e.Column == colFUNTipoConta) && (!row.IsFUCTipoContaNull()))
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = virEnumtipoconta.virEnumtipocontaSt.GetCor(row.FUCTipoConta);
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colstrStatus)
            {
                if (row.strStatus.StartsWith("X"))
                    e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Red;
                else if (row.strStatus.StartsWith("!"))
                    e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Yellow;
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colFUNnTipoPag)
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = dAutomacaoBancaria.VirEnumnTipoPag.GetCor(row.RHSTipoPag);
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colFUNNome)
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = row.EMP == Framework.DSCentral.EMP ? System.Drawing.Color.PaleGreen : System.Drawing.Color.LemonChiffon;
                e.Appearance.ForeColor = Color.Black;
            }
        }

        private void ViewPagamentos_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e == null)
                return;
            if (e.RowHandle < 0)
                return;
            dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow = (dAutomacaoBancaria.RHTarefaSubdetalheRow)ViewPagamentos.GetDataRow(e.RowHandle);
            ValidaPagamento(RHSrow);
            dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterX(RHSrow.EMP).Update(RHSrow);
            SomaRHDValor(RHSrow.RHTarefaDetalheRow);
        }*/

        private void ViewPIS_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView xGrid = null;
            object val = null;
            try
            {
                if (e.RowHandle < 0)
                    return;
                if ((e.Column == colTipoPag) || (e.Column == colTipoPag1))
                {
                    xGrid = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                    val = xGrid.GetRowCellValue(e.RowHandle, e.Column);
                    if (val != null)
                    {
                        switch ((nTipoPag)val)
                        {
                            case nTipoPag.Invalido:
                                e.Appearance.BackColor = Color.Red;
                                e.Appearance.BackColor2 = Color.Red;
                                break;
                            case nTipoPag.Cheque:
                                e.Appearance.BackColor = Color.Aqua;
                                e.Appearance.BackColor2 = Color.Aqua;
                                break;
                            case nTipoPag.CreditoConta:
                                //case nTipoPag.CreditoContaItau:
                                //case nTipoPag.CreditoContaUnibanco:
                                //case nTipoPag.CartaoSalarioBradesco:
                                //case nTipoPag.CreditoContaSalarioBradesco:
                                //case nTipoPag.Carta:
                                //case nTipoPag.EletronicoItau:
                                e.Appearance.BackColor = Color.Lime;
                                e.Appearance.BackColor2 = Color.Lime;
                                break;
                            //case nTipoPag.EletronicoBradesco:
                            //    e.Appearance.BackColor = Color.Yellow;
                            //    e.Appearance.BackColor2 = Color.Yellow;
                            //    break;
                            //case nTipoPag.ChequeFilial:
                            //    e.Appearance.BackColor = Color.Salmon;
                            //    e.Appearance.BackColor2 = Color.Salmon;
                            //    break;
                            case nTipoPag.NaoEmitir:
                                e.Appearance.BackColor = Color.Silver;
                                e.Appearance.BackColor2 = Color.Silver;
                                break;

                            default:
                                break;
                        }
                        e.Appearance.ForeColor = Color.Black;
                    };
                }
            }
            catch (Exception Ex)
            {
                if (xGrid != null)
                    if (val == null)
                        MessageBox.Show(Ex.Message + "\r\n" + e.RowHandle.ToString() + "\r\n Sem valor");
                    else
                        MessageBox.Show(Ex.Message + "\r\n" + e.RowHandle.ToString() + "\r\n" + val.ToString());
            }
        }



        private void ViewPIS_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            dAutomacaoBancaria.PISRow rowDARF = (dAutomacaoBancaria.PISRow)ViewPIS.GetDataRow(e.RowHandle);
            if (rowDARF == null)
                return;

            nTipoPag NovoTipo = (nTipoPag)rowDARF.TipoPag;
            bool Gravar = false;
            switch (NovoTipo)
            {
                case nTipoPag.Cheque:
                    Gravar = true;
                    break;
                case nTipoPag.CreditoConta:
                    if (rowDARF.Banco == 237)
                        Gravar = true;
                    else
                        rowDARF.TipoPag = (int)nTipoPag.Cheque;
                    break;
            }
            if (Gravar)
            {
                dAutomacaoBancaria.CONDOMINIOSRow rowCON;
                if (rowDARF.EMP == DSCentral.EMP)
                {
                    rowCON = dAutomacaoBancaria.CONDOMINIOS.FindByCON(rowDARF.CON);
                    rowCON.CONGPSEletronico = NovoTipo == nTipoPag.CreditoConta;
                    dAutomacaoBancaria.CONDOMINIOSTableAdapter.Update(rowCON);
                }
                else
                {
                    rowCON = dAutomacaoBancaria.CONDOMINIOSF.FindByCON(rowDARF.CON);
                    rowCON.CONGPSEletronico = NovoTipo == nTipoPag.CreditoConta;
                    dAutomacaoBancaria.CONDOMINIOSTableAdapterF.Update(rowCON);
                }

            }

        }

        private int AgenciaCAIXA
        { get { return _AgenciaCAIXA.Value; } }
        private int AgenciaCAIXADV
        { get { return _AgenciaCAIXADV.Value; } }

        private SortedList<int, int> Apontar
        {
            get
            {
                if (_Apontar == null)
                    _Apontar = new SortedList<int, int>();
                return _Apontar;
            }
        }

        private SortedList<int, int> ApontarF
        {
            get
            {
                if (_ApontarF == null)
                    _ApontarF = new SortedList<int, int>();
                return _ApontarF;
            }
        }

        private CPFCNPJ cnpjEmissor
        { get { return _cnpjEmissor ?? (_cnpjEmissor = new CPFCNPJ(Framework.DSCentral.EMP == 1 ? "55.055.651/0001-70" : "06.888.260/0001-21")); } }
        private int CodigoCompromisso
        { get { return _CodigoCompromisso.Value; } }
        private int ContaCAIXA
        { get { return _ContaCAIXA.Value; } }
        private string ContaCAIXADV
        { get { return _ContaCAIXADV; } }
        private int convenioCAIXA
        {
            get
            {
                if (!_convenioCAIXA.HasValue)
                {
                    int resposta;
                    VirInput.Input.Execute("Convenio CAIXA", out resposta);
                    _convenioCAIXA = resposta;
                    VirInput.Input.Execute("Parametro transmiss�o", out resposta);
                    _ParametroTr = resposta;
                    VirInput.Input.Execute("Ag�ncia", out resposta);
                    _AgenciaCAIXA = resposta;
                    VirInput.Input.Execute("D�gito Ag�ncia", out resposta);
                    _AgenciaCAIXADV = resposta;
                    VirInput.Input.Execute("Opera��o Conta", out resposta);
                    _OperacaoContaCAIXA = resposta;
                    VirInput.Input.Execute("Conta", out resposta);
                    _ContaCAIXA = resposta;
                    string respostastr = string.Empty;
                    VirInput.Input.Execute("D�gito Conta", ref respostastr);
                    _ContaCAIXADV = respostastr;
                    VirInput.Input.Execute("Codigo Compromisso", out resposta);
                    _CodigoCompromisso = resposta;
                }
                return _convenioCAIXA.Value;
            }
        }

        private dCheque dCheque
        {
            get
            {
                if (_dCheque == null)
                    _dCheque = new dCheque();
                return _dCheque;
            }
        }

        private dNOtAs dNOtAs
        {
            get
            {
                if (_dNOtAs == null)
                    _dNOtAs = new dNOtAs();
                return _dNOtAs;
            }
        }

        private DP.impressos.ImpRecSal ImpRecSal
        {
            get
            {
                if (_ImpRecSal == null)
                {
                    _ImpRecSal = new DP.impressos.ImpRecSal();
                    VirMSSQL.TableAdapter.AjustaAutoInc(_ImpRecSal.dRecSal);
                }
                return _ImpRecSal;
            }
        }



        impressos.dRecSal.HOLeritesRow LinhaMae
        {
            get
            {
                DataRowView DRV = (DataRowView)gridView3.GetRow(gridView3.FocusedRowHandle);
                if (DRV == null)
                    return null;
                else
                    return (impressos.dRecSal.HOLeritesRow)DRV.Row;
            }
        }
        private int OperacaoContaCAIXA
        { get { return _OperacaoContaCAIXA.Value; } }
        private int ParametroTr
        { get { return _ParametroTr.Value; } }

        private SPeriodicoNota.PeriodicoNotaClient Periodiconota
        {
            get
            {
                if (_Periodiconota == null)
                    _Periodiconota = new SPeriodicoNota.PeriodicoNotaClient();
                return _Periodiconota;
            }
        }


        private dllImpresso.ImpProtocolo Protocolo
        {
            get
            {
                if (_Protocolo == null)
                    _Protocolo = new dllImpresso.ImpProtocolo();
                return _Protocolo;
            }
        }

        internal void CarregaTarefas(bool AjustarStatus = false, bool ApagarRTF = false)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.Espere("Carregando dados");
                dAutomacaoBancariaTarefas.RHTarefaSubdetalhe.Clear();
                dAutomacaoBancariaTarefas.RHTarefaDetalhe.Clear();
                dAutomacaoBancariaTarefas.RHTarefa.Clear();
                if (checkEditVivos.Checked)
                {
                    dAutomacaoBancariaTarefas.RHTarefaTableAdapter.FillVivos(dAutomacaoBancariaTarefas.RHTarefa);
                    dAutomacaoBancariaTarefas.RHTarefaTableAdapterF.FillVivos(dAutomacaoBancariaTarefas.RHTarefa);
                    dAutomacaoBancariaTarefas.RHTarefaDetalheTableAdapter.FillVivos(dAutomacaoBancariaTarefas.RHTarefaDetalhe);
                    dAutomacaoBancariaTarefas.RHTarefaDetalheTableAdapterF.FillVivos(dAutomacaoBancariaTarefas.RHTarefaDetalhe);
                    dAutomacaoBancariaTarefas.RHTarefaSubdetalheTableAdapter.FillVivos(dAutomacaoBancariaTarefas.RHTarefaSubdetalhe);
                    dAutomacaoBancariaTarefas.RHTarefaSubdetalheTableAdapterF.FillVivos(dAutomacaoBancariaTarefas.RHTarefaSubdetalhe);
                }
                else
                {
                    dAutomacaoBancariaTarefas.RHTarefaTableAdapter.FillcomEMP(dAutomacaoBancariaTarefas.RHTarefa, dataCorte.DateTime);
                    dAutomacaoBancariaTarefas.RHTarefaTableAdapterF.FillcomEMP(dAutomacaoBancariaTarefas.RHTarefa, dataCorte.DateTime);
                    dAutomacaoBancariaTarefas.RHTarefaDetalheTableAdapter.FillcomEMP(dAutomacaoBancariaTarefas.RHTarefaDetalhe, dataCorte.DateTime);
                    dAutomacaoBancariaTarefas.RHTarefaDetalheTableAdapterF.FillcomEMP(dAutomacaoBancariaTarefas.RHTarefaDetalhe, dataCorte.DateTime);
                    dAutomacaoBancariaTarefas.RHTarefaSubdetalheTableAdapter.FillByCorte(dAutomacaoBancariaTarefas.RHTarefaSubdetalhe, dataCorte.DateTime);
                    dAutomacaoBancariaTarefas.RHTarefaSubdetalheTableAdapterF.FillByCorte(dAutomacaoBancariaTarefas.RHTarefaSubdetalhe, dataCorte.DateTime);
                }
                if (AjustarStatus)
                    AjustaStatus();
                if (ApagarRTF)
                {
                    ArquivoRTF.Clear();
                    TabControlAutomacao.SelectedTabPage = TabTarefas;
                }
            }
        }

        #region Pontes Folha
        private void repositoryItemButtonEdit2_Click(object sender, EventArgs e)
        {
            if (gridControlTarefas.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControlTarefas.FocusedView;
                dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = (dAutomacaoBancaria.RHTarefaDetalheRow)GV.GetFocusedDataRow();
                if (!rowRHD.IsRHD_NOANull())
                {
                    ContaPagar.cNotasCampos Novo = new ContaPagar.cNotasCampos(rowRHD.RHD_NOA,
                                                                               string.Format("{0} - Nota ({1:n2})", rowRHD.CONCodigoFolha1, rowRHD.IsTotalNull() ? 0 : rowRHD.Total),
                                                                               EMPTProc.EMPTProcStX(rowRHD.EMP));
                }
            }
        }

        private void gridView7_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column.Name == gridColumnNota.Name)
                {
                    DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                    dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = (dAutomacaoBancaria.RHTarefaDetalheRow)GV.GetDataRow(e.RowHandle);
                    if (!rowRHD.IsRHD_NOANull())
                        e.RepositoryItem = repositoryItemButtonNota;
                }
                if (e.Column.Name == gridColumnPeriodico.Name)
                {
                    DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                    dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = (dAutomacaoBancaria.RHTarefaDetalheRow)GV.GetDataRow(e.RowHandle);
                    if ((!rowRHD.IsNOA_PGFNull()) && (rowRHD.EMP == DSCentral.EMP))
                        e.RepositoryItem = repositoryItemButtonPeriodico;
                }
            }
        }        

        private void repositoryItemButtonPeriodico_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (gridControlTarefas.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControlTarefas.FocusedView;
                dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = (dAutomacaoBancaria.RHTarefaDetalheRow)GV.GetFocusedDataRow();
                if (!rowRHD.IsRHD_NOANull())
                {
                    cPagamentosPeriodicosCampos Novo = new cPagamentosPeriodicosCampos(rowRHD.NOA_PGF,
                                                                                       string.Format("{0} - Peri�dico", rowRHD.CONCodigoFolha1),
                                                                                       EMPTProc.EMPTProcStX(rowRHD.EMP));
                }
            }
        }

        #endregion

        private void checkEditVivos_CheckedChanged(object sender, EventArgs e)
        {
            dataCorte.Visible = !checkEditVivos.Checked;
        }
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    internal class virEnumTipoBancaria : dllVirEnum.VirEnum
    {

        private static virEnumTipoBancaria _virEnumTipoBancariaSt;

        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumTipoBancaria(Type Tipo) :
            base(Tipo)
        {
        }

        /// <summary>
        /// VirEnum est�tico para campo TipoBancaria
        /// </summary>
        public static virEnumTipoBancaria virEnumTipoBancariaSt
        {
            get
            {
                if (_virEnumTipoBancariaSt == null)
                {
                    _virEnumTipoBancariaSt = new virEnumTipoBancaria(typeof(TipoBancaria));
                    _virEnumTipoBancariaSt.GravaNomes(TipoBancaria.Adiantamento, "Adiantamento de sal�rio");
                    _virEnumTipoBancariaSt.GravaNomes(TipoBancaria.DecimoTerceiro, "13�");
                    _virEnumTipoBancariaSt.GravaNomes(TipoBancaria.Pagamento, "Sal�rio");
                    _virEnumTipoBancariaSt.GravaNomes(TipoBancaria.Avulso, "Avulso");
                }
                return _virEnumTipoBancariaSt;
            }
        }
    }

    /// <summary>
    /// Enumera��o para campo 
    /// </summary>
    internal enum RHTStatus
    {
        Antiga = -1,
        Cadastrada = 0,
        GeradaParcial = 1,
        Gerada = 2,
        Cancelada = 3,
        Concluida = 4
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumRHTStatus : dllVirEnum.VirEnum
    {

        private static virEnumRHTStatus _virEnumRHTStatusSt;

        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumRHTStatus(Type Tipo) :
            base(Tipo)
        {
        }

        /// <summary>
        /// VirEnum est�tico para campo RHTStatus
        /// </summary>
        public static virEnumRHTStatus virEnumRHTStatusSt
        {
            get
            {
                if (_virEnumRHTStatusSt == null)
                {
                    _virEnumRHTStatusSt = new virEnumRHTStatus(typeof(RHTStatus));
                    _virEnumRHTStatusSt.GravaNomes(RHTStatus.Cadastrada, "Carregada", Color.White);
                    _virEnumRHTStatusSt.GravaNomes(RHTStatus.GeradaParcial, "Pendente", Color.Yellow);
                    _virEnumRHTStatusSt.GravaNomes(RHTStatus.Gerada, "Gerada", Color.Lime);
                    _virEnumRHTStatusSt.GravaNomes(RHTStatus.Cancelada, "Cancelada", Color.Gray);
                    _virEnumRHTStatusSt.GravaNomes(RHTStatus.Concluida, "Conclu�da", Color.Aqua);
                    _virEnumRHTStatusSt.GravaNomes(RHTStatus.Antiga, "Antiga", Color.Linen);
                }
                return _virEnumRHTStatusSt;
            }
        }
    }
}

