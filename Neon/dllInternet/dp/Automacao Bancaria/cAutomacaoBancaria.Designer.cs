namespace DP.Automacao_Bancaria
{
    partial class cAutomacaoBancaria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cAutomacaoBancaria));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONCodigo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigoFolha11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCNPJ1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQTD1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHD_NOA1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnNota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_PGF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPeriodico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlTarefas = new DevExpress.XtraGrid.GridControl();
            this.dAutomacaoBancariaBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRHT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHTI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource();
            this.colRHTTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colRHTCompetencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCompetenciaEdit1 = new CompontesBasicos.RepositoryItemCompetenciaEdit();
            this.colRHTDataI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHTDataCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHTDataCheque = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHTArquivo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHT_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHTStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colEMP2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNCondominios = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonDelete = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemImageComboBox9 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemButtonNota = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonPeriodico = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRHSValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rHTarefaDetalheBindingSource = new System.Windows.Forms.BindingSource();
            this.dAutomacaoBancaria = new DP.Automacao_Bancaria.dAutomacaoBancaria();
            this.TabControlAutomacao = new DevExpress.XtraTab.XtraTabControl();
            this.TabTarefas = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dataCorte = new DevExpress.XtraEditors.DateEdit();
            this.checkEditVivos = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.TabPageRTF = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.ArquivoRTF = new System.Windows.Forms.RichTextBox();
            this.ToolStripRTF = new System.Windows.Forms.ToolStrip();
            this.btnIniciarExtracao = new System.Windows.Forms.ToolStripButton();
            this.btnCancelarExtracao = new System.Windows.Forms.ToolStripButton();
            this.pgbExtracao = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.labZoom = new System.Windows.Forms.ToolStripLabel();
            this.edtZoomRTF = new System.Windows.Forms.ToolStripTextBox();
            this.btnMaisZoom = new System.Windows.Forms.ToolStripButton();
            this.btnMenosZoom = new System.Windows.Forms.ToolStripButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.edtTipoArquivo = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.edtArquivo = new DevExpress.XtraEditors.ButtonEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.TabPageGPS = new DevExpress.XtraTab.XtraTabPage();
            this.gridGPS = new DevExpress.XtraGrid.GridControl();
            this.iNSSBindingSource = new System.Windows.Forms.BindingSource();
            this.ViewGPS = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCampo1GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo3GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo4GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo5GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo6GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo9GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo10GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo11GPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoPag1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colErro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpCODCON = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource();
            this.colCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNEON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrevisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSEFIP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErroValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodComunicacao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMP5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.TabPagePIS = new DevExpress.XtraTab.XtraTabPage();
            this.tableLayoutPanelPIS = new System.Windows.Forms.TableLayoutPanel();
            this.ToolStripDados = new System.Windows.Forms.ToolStrip();
            this.btnRemessaPIS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.gridPIS = new DevExpress.XtraGrid.GridControl();
            this.pISBindingSource = new System.Windows.Forms.BindingSource();
            this.ViewPIS = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCampo1PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo3PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo4PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo5PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo6PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo7PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo8PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo9PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCampo10PIS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBanco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoPag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colErro1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodComunicacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMP3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCON1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TabRecibos = new DevExpress.XtraTab.XtraTabPage();
            this.gridRecibos = new DevExpress.XtraGrid.GridControl();
            this.recibosBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONNome2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQtot1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQtot2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQtot3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repOK = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoImpBol2 = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.TabRetornoRec = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.dRecSalBindingSource = new System.Windows.Forms.BindingSource();
            this.dRecSal = new DP.impressos.dRecSal();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHOL_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOLNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOLRetorno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOLQ0 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOLDuasvias = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigoFolha1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigoFolha2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMP6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.cBotaoImpBol4 = new dllImpresso.Botoes.cBotaoImpBol();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ChNR = new DevExpress.XtraEditors.CheckEdit();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHOTTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOTDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAguardo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMP4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cBotaoImpBol3 = new dllImpresso.Botoes.cBotaoImpBol();
            this.spinATE = new DevExpress.XtraEditors.SpinEdit();
            this.spinDE = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.rHTarefaSubdetalheBindingSource = new System.Windows.Forms.BindingSource();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource();
            this.remessasPendentesBindingSource = new System.Windows.Forms.BindingSource();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.rEMESSASDPTableAdapter = new DP.Automacao_Bancaria.dAutomacaoBancariaTableAdapters.REMESSASDPTableAdapter();
            this.rEMESSASDPBindingSource = new System.Windows.Forms.BindingSource();
            this.detremessadpTableAdapter1 = new DP.Automacao_Bancaria.dAutomacaoBancariaTableAdapters.DETREMESSADPTableAdapter();
            this.remessasPendentesTableAdapter = new DP.Automacao_Bancaria.dAutomacaoBancariaTableAdapters.RemessasPendentesTableAdapter();
            this.dImpProtocolo1 = new dllImpresso.dImpProtocolo();
            this.timerAviso = new System.Windows.Forms.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTarefas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAutomacaoBancariaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCompetenciaEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonPeriodico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rHTarefaDetalheBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAutomacaoBancaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlAutomacao)).BeginInit();
            this.TabControlAutomacao.SuspendLayout();
            this.TabTarefas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataCorte.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataCorte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVivos.Properties)).BeginInit();
            this.TabPageRTF.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.ToolStripRTF.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtTipoArquivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtArquivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.TabPageGPS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridGPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNSSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewGPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCODCON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            this.TabPagePIS.SuspendLayout();
            this.tableLayoutPanelPIS.SuspendLayout();
            this.ToolStripDados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPIS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pISBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewPIS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox7)).BeginInit();
            this.TabRecibos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRecibos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recibosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.TabRetornoRec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRecSalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRecSal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChNR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinATE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinDE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rHTarefaSubdetalheBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.remessasPendentesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEMESSASDPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpProtocolo1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView7
            // 
            this.gridView7.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView7.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView7.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView7.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView7.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView7.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView7.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView7.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView7.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView7.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView7.Appearance.Empty.Options.UseBackColor = true;
            this.gridView7.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView7.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView7.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView7.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView7.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView7.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView7.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView7.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView7.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView7.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView7.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView7.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView7.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView7.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView7.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView7.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView7.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView7.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView7.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView7.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView7.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView7.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView7.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView7.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView7.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView7.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView7.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView7.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView7.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView7.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView7.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView7.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView7.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView7.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView7.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView7.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView7.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView7.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView7.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView7.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView7.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView7.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView7.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView7.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView7.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView7.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView7.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView7.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView7.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView7.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView7.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView7.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView7.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView7.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView7.Appearance.Preview.Options.UseBackColor = true;
            this.gridView7.Appearance.Preview.Options.UseFont = true;
            this.gridView7.Appearance.Preview.Options.UseForeColor = true;
            this.gridView7.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView7.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView7.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.Row.Options.UseBackColor = true;
            this.gridView7.Appearance.Row.Options.UseBorderColor = true;
            this.gridView7.Appearance.Row.Options.UseForeColor = true;
            this.gridView7.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView7.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView7.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView7.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView7.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView7.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView7.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView7.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView7.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCodigo1,
            this.colCONNome3,
            this.colTotal1,
            this.colCONCodigoFolha11,
            this.colCONCNPJ1,
            this.colQTD1,
            this.colRHD_NOA1,
            this.gridColumnNota,
            this.colRHD,
            this.colNOA_PGF,
            this.gridColumnPeriodico});
            this.gridView7.GridControl = this.gridControlTarefas;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsCustomization.AllowGroup = false;
            this.gridView7.OptionsDetail.ShowDetailTabs = false;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.EnableAppearanceOddRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.PaintStyleName = "Flat";
            this.gridView7.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView7_CustomRowCellEdit);
            // 
            // colCONCodigo1
            // 
            this.colCONCodigo1.Caption = "CODCON";
            this.colCONCodigo1.FieldName = "CONCodigo";
            this.colCONCodigo1.Name = "colCONCodigo1";
            this.colCONCodigo1.OptionsColumn.ReadOnly = true;
            this.colCONCodigo1.Visible = true;
            this.colCONCodigo1.VisibleIndex = 1;
            // 
            // colCONNome3
            // 
            this.colCONNome3.Caption = "Nome";
            this.colCONNome3.FieldName = "CONNome";
            this.colCONNome3.Name = "colCONNome3";
            this.colCONNome3.OptionsColumn.ReadOnly = true;
            this.colCONNome3.Visible = true;
            this.colCONNome3.VisibleIndex = 3;
            this.colCONNome3.Width = 327;
            // 
            // colTotal1
            // 
            this.colTotal1.Caption = "Total";
            this.colTotal1.DisplayFormat.FormatString = "n2";
            this.colTotal1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotal1.FieldName = "Total";
            this.colTotal1.Name = "colTotal1";
            this.colTotal1.OptionsColumn.ReadOnly = true;
            this.colTotal1.Visible = true;
            this.colTotal1.VisibleIndex = 5;
            this.colTotal1.Width = 100;
            // 
            // colCONCodigoFolha11
            // 
            this.colCONCodigoFolha11.Caption = "C�diigo";
            this.colCONCodigoFolha11.FieldName = "CONCodigoFolha1";
            this.colCONCodigoFolha11.Name = "colCONCodigoFolha11";
            this.colCONCodigoFolha11.OptionsColumn.ReadOnly = true;
            this.colCONCodigoFolha11.Visible = true;
            this.colCONCodigoFolha11.VisibleIndex = 0;
            // 
            // colCONCNPJ1
            // 
            this.colCONCNPJ1.Caption = "CNPJ";
            this.colCONCNPJ1.FieldName = "CONCNPJ";
            this.colCONCNPJ1.Name = "colCONCNPJ1";
            this.colCONCNPJ1.OptionsColumn.ReadOnly = true;
            this.colCONCNPJ1.Visible = true;
            this.colCONCNPJ1.VisibleIndex = 2;
            this.colCONCNPJ1.Width = 137;
            // 
            // colQTD1
            // 
            this.colQTD1.Caption = "Pagamentos";
            this.colQTD1.DisplayFormat.FormatString = "n0";
            this.colQTD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQTD1.FieldName = "QTD";
            this.colQTD1.Name = "colQTD1";
            this.colQTD1.OptionsColumn.ReadOnly = true;
            this.colQTD1.Visible = true;
            this.colQTD1.VisibleIndex = 4;
            // 
            // colRHD_NOA1
            // 
            this.colRHD_NOA1.Caption = "NOA";
            this.colRHD_NOA1.FieldName = "RHD_NOA";
            this.colRHD_NOA1.Name = "colRHD_NOA1";
            this.colRHD_NOA1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnNota
            // 
            this.gridColumnNota.Caption = "gridColumn4";
            this.gridColumnNota.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnNota.Name = "gridColumnNota";
            this.gridColumnNota.OptionsColumn.ShowCaption = false;
            this.gridColumnNota.Visible = true;
            this.gridColumnNota.VisibleIndex = 6;
            this.gridColumnNota.Width = 25;
            // 
            // colRHD
            // 
            this.colRHD.FieldName = "RHD";
            this.colRHD.Name = "colRHD";
            // 
            // colNOA_PGF
            // 
            this.colNOA_PGF.Caption = "PGF";
            this.colNOA_PGF.FieldName = "NOA_PGF";
            this.colNOA_PGF.Name = "colNOA_PGF";
            // 
            // gridColumnPeriodico
            // 
            this.gridColumnPeriodico.Caption = "gridColumn4";
            this.gridColumnPeriodico.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnPeriodico.Name = "gridColumnPeriodico";
            this.gridColumnPeriodico.OptionsColumn.ShowCaption = false;
            this.gridColumnPeriodico.Visible = true;
            this.gridColumnPeriodico.VisibleIndex = 7;
            this.gridColumnPeriodico.Width = 25;
            // 
            // gridControlTarefas
            // 
            this.gridControlTarefas.DataMember = "RHTarefa";
            this.gridControlTarefas.DataSource = this.dAutomacaoBancariaBindingSource;
            this.gridControlTarefas.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode4.LevelTemplate = this.gridView7;
            gridLevelNode5.LevelTemplate = this.gridView1;
            gridLevelNode5.RelationName = "FK_RHTRHTarefaSubdetalhe_RHTarefaDetalhe";
            gridLevelNode6.RelationName = "RHTarefaDetalhe_BuscaCHEStatus";
            gridLevelNode4.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode5,
            gridLevelNode6});
            gridLevelNode4.RelationName = "FK_RHTarefaDetalhe_RHTarefa";
            this.gridControlTarefas.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode4});
            this.gridControlTarefas.Location = new System.Drawing.Point(0, 54);
            this.gridControlTarefas.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.gridControlTarefas.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControlTarefas.MainView = this.gridView6;
            this.gridControlTarefas.Name = "gridControlTarefas";
            this.gridControlTarefas.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpUSU,
            this.repositoryItemImageComboBox4,
            this.repositoryItemImageComboBox5,
            this.repositoryItemImageComboBox6,
            this.repositoryItemButtonEdit1,
            this.repositoryItemCompetenciaEdit1,
            this.repositoryItemButtonDelete,
            this.repositoryItemImageComboBox9,
            this.repositoryItemButtonNota,
            this.repositoryItemButtonPeriodico,
            this.repositoryItemImageComboBox1});
            this.gridControlTarefas.ShowOnlyPredefinedDetails = true;
            this.gridControlTarefas.Size = new System.Drawing.Size(1394, 636);
            this.gridControlTarefas.TabIndex = 1;
            this.gridControlTarefas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6,
            this.gridView1,
            this.gridView7});
            // 
            // dAutomacaoBancariaBindingSource
            // 
            this.dAutomacaoBancariaBindingSource.DataSource = typeof(DP.Automacao_Bancaria.dAutomacaoBancaria);
            this.dAutomacaoBancariaBindingSource.Position = 0;
            // 
            // gridView6
            // 
            this.gridView6.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView6.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView6.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView6.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
            this.gridView6.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
            this.gridView6.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView6.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView6.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView6.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.gridView6.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView6.Appearance.Empty.Options.UseBackColor = true;
            this.gridView6.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(232)))), ((int)(((byte)(220)))));
            this.gridView6.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(232)))), ((int)(((byte)(220)))));
            this.gridView6.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView6.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView6.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView6.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView6.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView6.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView6.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.gridView6.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView6.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView6.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView6.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView6.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView6.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView6.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView6.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView6.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView6.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView6.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView6.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView6.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView6.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
            this.gridView6.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
            this.gridView6.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView6.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView6.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView6.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.gridView6.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView6.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView6.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView6.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
            this.gridView6.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
            this.gridView6.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView6.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView6.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView6.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
            this.gridView6.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
            this.gridView6.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView6.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView6.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView6.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(223)))), ((int)(((byte)(220)))));
            this.gridView6.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(195)))), ((int)(((byte)(191)))));
            this.gridView6.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(100)))), ((int)(((byte)(111)))));
            this.gridView6.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView6.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView6.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView6.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.HorzLine.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(100)))), ((int)(((byte)(111)))));
            this.gridView6.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView6.Appearance.HorzLine.Options.UseBorderColor = true;
            this.gridView6.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.gridView6.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.gridView6.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView6.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView6.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView6.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(253)))), ((int)(((byte)(247)))));
            this.gridView6.Appearance.Preview.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(253)))), ((int)(((byte)(247)))));
            this.gridView6.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView6.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(100)))), ((int)(((byte)(111)))));
            this.gridView6.Appearance.Preview.Options.UseBackColor = true;
            this.gridView6.Appearance.Preview.Options.UseBorderColor = true;
            this.gridView6.Appearance.Preview.Options.UseFont = true;
            this.gridView6.Appearance.Preview.Options.UseForeColor = true;
            this.gridView6.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.gridView6.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.gridView6.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.Row.Options.UseBackColor = true;
            this.gridView6.Appearance.Row.Options.UseBorderColor = true;
            this.gridView6.Appearance.Row.Options.UseForeColor = true;
            this.gridView6.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.gridView6.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView6.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView6.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(208)))), ((int)(((byte)(205)))));
            this.gridView6.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView6.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView6.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView6.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView6.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(188)))), ((int)(((byte)(184)))));
            this.gridView6.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRHT,
            this.colRHTI_USU,
            this.colRHTTipo,
            this.colRHTCompetencia,
            this.colRHTDataI,
            this.colRHTDataCredito,
            this.colRHTDataCheque,
            this.colRHTArquivo,
            this.colRHT_PLA,
            this.colRHTStatus,
            this.colEMP2,
            this.gridColumn2,
            this.gridColumn3,
            this.colNCondominios});
            this.gridView6.GridControl = this.gridControlTarefas;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsDetail.ShowDetailTabs = false;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.EnableAppearanceOddRow = true;
            this.gridView6.OptionsView.ShowAutoFilterRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.PaintStyleName = "Flat";
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRHTDataI, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView6.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView6_RowCellStyle);
            this.gridView6.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView6_CustomRowCellEdit);
            // 
            // colRHT
            // 
            this.colRHT.FieldName = "RHT";
            this.colRHT.Name = "colRHT";
            this.colRHT.OptionsColumn.ReadOnly = true;
            // 
            // colRHTI_USU
            // 
            this.colRHTI_USU.Caption = "Autor";
            this.colRHTI_USU.ColumnEdit = this.LookUpUSU;
            this.colRHTI_USU.FieldName = "USU";
            this.colRHTI_USU.Name = "colRHTI_USU";
            this.colRHTI_USU.OptionsColumn.ReadOnly = true;
            this.colRHTI_USU.Visible = true;
            this.colRHTI_USU.VisibleIndex = 1;
            this.colRHTI_USU.Width = 159;
            // 
            // LookUpUSU
            // 
            this.LookUpUSU.AutoHeight = false;
            this.LookUpUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpUSU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USU Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpUSU.DataSource = this.uSUariosBindingSource;
            this.LookUpUSU.DisplayMember = "USUNome";
            this.LookUpUSU.Name = "LookUpUSU";
            this.LookUpUSU.ValueMember = "USU";
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colRHTTipo
            // 
            this.colRHTTipo.Caption = "Tipo";
            this.colRHTTipo.ColumnEdit = this.repositoryItemImageComboBox5;
            this.colRHTTipo.FieldName = "RHTTipo";
            this.colRHTTipo.Name = "colRHTTipo";
            this.colRHTTipo.OptionsColumn.ReadOnly = true;
            this.colRHTTipo.Visible = true;
            this.colRHTTipo.VisibleIndex = 4;
            this.colRHTTipo.Width = 76;
            // 
            // repositoryItemImageComboBox5
            // 
            this.repositoryItemImageComboBox5.AutoHeight = false;
            this.repositoryItemImageComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox5.Name = "repositoryItemImageComboBox5";
            // 
            // colRHTCompetencia
            // 
            this.colRHTCompetencia.Caption = "Compet�ncia";
            this.colRHTCompetencia.ColumnEdit = this.repositoryItemCompetenciaEdit1;
            this.colRHTCompetencia.FieldName = "RHTCompetencia";
            this.colRHTCompetencia.Name = "colRHTCompetencia";
            this.colRHTCompetencia.OptionsColumn.AllowEdit = false;
            this.colRHTCompetencia.OptionsColumn.ReadOnly = true;
            this.colRHTCompetencia.Visible = true;
            this.colRHTCompetencia.VisibleIndex = 5;
            this.colRHTCompetencia.Width = 76;
            // 
            // repositoryItemCompetenciaEdit1
            // 
            this.repositoryItemCompetenciaEdit1.AutoHeight = false;
            this.repositoryItemCompetenciaEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions6),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Left, "", -1, true, true, true, editorButtonImageOptions7),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right)});
            this.repositoryItemCompetenciaEdit1.Mask.EditMask = "00/0000";
            this.repositoryItemCompetenciaEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.repositoryItemCompetenciaEdit1.Name = "repositoryItemCompetenciaEdit1";
            // 
            // colRHTDataI
            // 
            this.colRHTDataI.Caption = "Inclus�o";
            this.colRHTDataI.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.colRHTDataI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colRHTDataI.FieldName = "RHTDataI";
            this.colRHTDataI.Name = "colRHTDataI";
            this.colRHTDataI.OptionsColumn.ReadOnly = true;
            this.colRHTDataI.Visible = true;
            this.colRHTDataI.VisibleIndex = 2;
            this.colRHTDataI.Width = 126;
            // 
            // colRHTDataCredito
            // 
            this.colRHTDataCredito.Caption = "Data de Cr�dito";
            this.colRHTDataCredito.FieldName = "RHTDataCredito";
            this.colRHTDataCredito.Name = "colRHTDataCredito";
            this.colRHTDataCredito.OptionsColumn.ReadOnly = true;
            this.colRHTDataCredito.Visible = true;
            this.colRHTDataCredito.VisibleIndex = 6;
            this.colRHTDataCredito.Width = 87;
            // 
            // colRHTDataCheque
            // 
            this.colRHTDataCheque.Caption = "Data Cheque";
            this.colRHTDataCheque.FieldName = "RHTDataCheque";
            this.colRHTDataCheque.Name = "colRHTDataCheque";
            this.colRHTDataCheque.OptionsColumn.ReadOnly = true;
            this.colRHTDataCheque.Visible = true;
            this.colRHTDataCheque.VisibleIndex = 7;
            this.colRHTDataCheque.Width = 73;
            // 
            // colRHTArquivo
            // 
            this.colRHTArquivo.Caption = "RTF";
            this.colRHTArquivo.FieldName = "RHTArquivo";
            this.colRHTArquivo.Name = "colRHTArquivo";
            this.colRHTArquivo.OptionsColumn.ReadOnly = true;
            this.colRHTArquivo.Visible = true;
            this.colRHTArquivo.VisibleIndex = 8;
            this.colRHTArquivo.Width = 195;
            // 
            // colRHT_PLA
            // 
            this.colRHT_PLA.Caption = "Conta";
            this.colRHT_PLA.FieldName = "RHT_PLA";
            this.colRHT_PLA.Name = "colRHT_PLA";
            this.colRHT_PLA.OptionsColumn.ReadOnly = true;
            this.colRHT_PLA.Visible = true;
            this.colRHT_PLA.VisibleIndex = 9;
            this.colRHT_PLA.Width = 68;
            // 
            // colRHTStatus
            // 
            this.colRHTStatus.Caption = "Status";
            this.colRHTStatus.ColumnEdit = this.repositoryItemImageComboBox4;
            this.colRHTStatus.FieldName = "RHTStatus";
            this.colRHTStatus.Name = "colRHTStatus";
            this.colRHTStatus.OptionsColumn.ReadOnly = true;
            this.colRHTStatus.Visible = true;
            this.colRHTStatus.VisibleIndex = 10;
            this.colRHTStatus.Width = 145;
            // 
            // repositoryItemImageComboBox4
            // 
            this.repositoryItemImageComboBox4.AutoHeight = false;
            this.repositoryItemImageComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox4.Name = "repositoryItemImageComboBox4";
            // 
            // colEMP2
            // 
            this.colEMP2.ColumnEdit = this.repositoryItemImageComboBox6;
            this.colEMP2.FieldName = "EMP";
            this.colEMP2.Name = "colEMP2";
            this.colEMP2.OptionsColumn.ReadOnly = true;
            this.colEMP2.Visible = true;
            this.colEMP2.VisibleIndex = 0;
            this.colEMP2.Width = 52;
            // 
            // repositoryItemImageComboBox6
            // 
            this.repositoryItemImageComboBox6.AutoHeight = false;
            this.repositoryItemImageComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox6.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("SBC", 1, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("SA", 3, -1)});
            this.repositoryItemImageComboBox6.Name = "repositoryItemImageComboBox6";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 11;
            this.gridColumn2.Width = 29;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ShowCaption = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 12;
            this.gridColumn3.Width = 29;
            // 
            // colNCondominios
            // 
            this.colNCondominios.Caption = "Condom�nios";
            this.colNCondominios.FieldName = "NCondominios";
            this.colNCondominios.Name = "colNCondominios";
            this.colNCondominios.OptionsColumn.ReadOnly = true;
            this.colNCondominios.Visible = true;
            this.colNCondominios.VisibleIndex = 3;
            this.colNCondominios.Width = 79;
            // 
            // repositoryItemButtonDelete
            // 
            this.repositoryItemButtonDelete.AutoHeight = false;
            this.repositoryItemButtonDelete.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonDelete.Name = "repositoryItemButtonDelete";
            this.repositoryItemButtonDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonDelete.Click += new System.EventHandler(this.repositoryItemButtonDelete_Click);
            // 
            // repositoryItemImageComboBox9
            // 
            this.repositoryItemImageComboBox9.AutoHeight = false;
            this.repositoryItemImageComboBox9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox9.Name = "repositoryItemImageComboBox9";
            // 
            // repositoryItemButtonNota
            // 
            this.repositoryItemButtonNota.AutoHeight = false;
            editorButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions8.Image")));
            this.repositoryItemButtonNota.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions8, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemButtonNota.Name = "repositoryItemButtonNota";
            this.repositoryItemButtonNota.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonNota.Click += new System.EventHandler(this.repositoryItemButtonEdit2_Click);
            // 
            // repositoryItemButtonPeriodico
            // 
            this.repositoryItemButtonPeriodico.AutoHeight = false;
            editorButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions9.Image")));
            this.repositoryItemButtonPeriodico.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions9, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemButtonPeriodico.Name = "repositoryItemButtonPeriodico";
            this.repositoryItemButtonPeriodico.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonPeriodico.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonPeriodico_ButtonClick);
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(159)))), ((int)(((byte)(69)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(152)))), ((int)(((byte)(49)))));
            this.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(176)))), ((int)(((byte)(84)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.gridView1.Appearance.Preview.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(177)))), ((int)(((byte)(90)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseBorderColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRHSValor,
            this.colCHEStatus});
            this.gridView1.GridControl = this.gridControlTarefas;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PaintStyleName = "Flat";
            // 
            // colRHSValor
            // 
            this.colRHSValor.Caption = "Valor";
            this.colRHSValor.DisplayFormat.FormatString = "n2";
            this.colRHSValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRHSValor.FieldName = "RHSValor";
            this.colRHSValor.Name = "colRHSValor";
            this.colRHSValor.OptionsColumn.AllowEdit = false;
            this.colRHSValor.OptionsColumn.ReadOnly = true;
            this.colRHSValor.Visible = true;
            this.colRHSValor.VisibleIndex = 0;
            this.colRHSValor.Width = 94;
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.Caption = "Status Cheque";
            this.colCHEStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.OptionsColumn.AllowEdit = false;
            this.colCHEStatus.OptionsColumn.ReadOnly = true;
            this.colCHEStatus.Visible = true;
            this.colCHEStatus.VisibleIndex = 1;
            this.colCHEStatus.Width = 120;
            // 
            // rHTarefaDetalheBindingSource
            // 
            this.rHTarefaDetalheBindingSource.DataMember = "RHTarefaDetalhe";
            this.rHTarefaDetalheBindingSource.DataSource = this.dAutomacaoBancaria;
            // 
            // dAutomacaoBancaria
            // 
            this.dAutomacaoBancaria.DataSetName = "dAutomacaoBancaria";
            this.dAutomacaoBancaria.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TabControlAutomacao
            // 
            this.TabControlAutomacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlAutomacao.Location = new System.Drawing.Point(0, 0);
            this.TabControlAutomacao.Name = "TabControlAutomacao";
            this.TabControlAutomacao.PaintStyleName = "PropertyView";
            this.TabControlAutomacao.SelectedTabPage = this.TabTarefas;
            this.TabControlAutomacao.Size = new System.Drawing.Size(1396, 711);
            this.TabControlAutomacao.TabIndex = 0;
            this.TabControlAutomacao.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TabTarefas,
            this.TabPageRTF,
            this.TabPageGPS,
            this.TabPagePIS,
            this.TabRecibos,
            this.TabRetornoRec});
            this.TabControlAutomacao.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.TabControlAutomacao_SelectedPageChanged);
            // 
            // TabTarefas
            // 
            this.TabTarefas.Controls.Add(this.gridControlTarefas);
            this.TabTarefas.Controls.Add(this.panelControl2);
            this.TabTarefas.Name = "TabTarefas";
            this.TabTarefas.Size = new System.Drawing.Size(1394, 690);
            this.TabTarefas.Text = "Tarefas";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.dataCorte);
            this.panelControl2.Controls.Add(this.checkEditVivos);
            this.panelControl2.Controls.Add(this.simpleButton4);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1394, 54);
            this.panelControl2.TabIndex = 0;
            // 
            // dataCorte
            // 
            this.dataCorte.EditValue = null;
            this.dataCorte.Location = new System.Drawing.Point(131, 28);
            this.dataCorte.Name = "dataCorte";
            this.dataCorte.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dataCorte.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dataCorte.Size = new System.Drawing.Size(100, 20);
            this.dataCorte.TabIndex = 2;
            this.dataCorte.Visible = false;
            // 
            // checkEditVivos
            // 
            this.checkEditVivos.EditValue = true;
            this.checkEditVivos.Location = new System.Drawing.Point(131, 5);
            this.checkEditVivos.Name = "checkEditVivos";
            this.checkEditVivos.Properties.Caption = "Tarefas em aberto";
            this.checkEditVivos.Size = new System.Drawing.Size(270, 19);
            this.checkEditVivos.TabIndex = 1;
            this.checkEditVivos.CheckedChanged += new System.EventHandler(this.checkEditVivos_CheckedChanged);
            // 
            // simpleButton4
            // 
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(5, 5);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(120, 35);
            this.simpleButton4.TabIndex = 0;
            this.simpleButton4.Text = "Atualizar";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // TabPageRTF
            // 
            this.TabPageRTF.Controls.Add(this.groupControl2);
            this.TabPageRTF.Controls.Add(this.ToolStripRTF);
            this.TabPageRTF.Controls.Add(this.groupControl1);
            this.TabPageRTF.Name = "TabPageRTF";
            this.TabPageRTF.Size = new System.Drawing.Size(1463, 690);
            this.TabPageRTF.Text = "Arquivo RTF";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.ArquivoRTF);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 68);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1463, 595);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = ":: Pr�-Visualiza��o ::";
            // 
            // ArquivoRTF
            // 
            this.ArquivoRTF.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ArquivoRTF.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ArquivoRTF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ArquivoRTF.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ArquivoRTF.Location = new System.Drawing.Point(2, 20);
            this.ArquivoRTF.Name = "ArquivoRTF";
            this.ArquivoRTF.ReadOnly = true;
            this.ArquivoRTF.Size = new System.Drawing.Size(1459, 573);
            this.ArquivoRTF.TabIndex = 4;
            this.ArquivoRTF.Text = "";
            // 
            // ToolStripRTF
            // 
            this.ToolStripRTF.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ToolStripRTF.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnIniciarExtracao,
            this.btnCancelarExtracao,
            this.pgbExtracao,
            this.toolStripSeparator1,
            this.labZoom,
            this.edtZoomRTF,
            this.btnMaisZoom,
            this.btnMenosZoom});
            this.ToolStripRTF.Location = new System.Drawing.Point(0, 663);
            this.ToolStripRTF.Name = "ToolStripRTF";
            this.ToolStripRTF.ShowItemToolTips = false;
            this.ToolStripRTF.Size = new System.Drawing.Size(1463, 27);
            this.ToolStripRTF.TabIndex = 4;
            // 
            // btnIniciarExtracao
            // 
            this.btnIniciarExtracao.Image = global::DP.Properties.Resources.btnIniciarExtracao;
            this.btnIniciarExtracao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnIniciarExtracao.Name = "btnIniciarExtracao";
            this.btnIniciarExtracao.Size = new System.Drawing.Size(95, 24);
            this.btnIniciarExtracao.Text = "Extrair Dados";
            this.btnIniciarExtracao.Click += new System.EventHandler(this.btnIniciarExtracao_Click);
            // 
            // btnCancelarExtracao
            // 
            this.btnCancelarExtracao.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnCancelarExtracao.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCancelarExtracao.Image = global::DP.Properties.Resources.btnCancelarExtracao;
            this.btnCancelarExtracao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelarExtracao.Name = "btnCancelarExtracao";
            this.btnCancelarExtracao.Size = new System.Drawing.Size(23, 24);
            this.btnCancelarExtracao.Text = "toolStripButton1";
            // 
            // pgbExtracao
            // 
            this.pgbExtracao.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.pgbExtracao.Name = "pgbExtracao";
            this.pgbExtracao.Size = new System.Drawing.Size(100, 24);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // labZoom
            // 
            this.labZoom.Name = "labZoom";
            this.labZoom.Size = new System.Drawing.Size(39, 24);
            this.labZoom.Text = "Zoom";
            // 
            // edtZoomRTF
            // 
            this.edtZoomRTF.AutoSize = false;
            this.edtZoomRTF.Name = "edtZoomRTF";
            this.edtZoomRTF.Size = new System.Drawing.Size(30, 25);
            this.edtZoomRTF.Text = "1,0";
            this.edtZoomRTF.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.edtZoomRTF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtZoomRTF_KeyDown);
            this.edtZoomRTF.Validated += new System.EventHandler(this.txtZoomRTF_Validated);
            // 
            // btnMaisZoom
            // 
            this.btnMaisZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMaisZoom.Image = global::DP.Properties.Resources.btnMaisZoom;
            this.btnMaisZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMaisZoom.Name = "btnMaisZoom";
            this.btnMaisZoom.Size = new System.Drawing.Size(23, 24);
            this.btnMaisZoom.Text = "toolStripButton1";
            this.btnMaisZoom.Click += new System.EventHandler(this.btnMaisZoom_Click);
            // 
            // btnMenosZoom
            // 
            this.btnMenosZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMenosZoom.Image = global::DP.Properties.Resources.btnMenosZoom;
            this.btnMenosZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMenosZoom.Name = "btnMenosZoom";
            this.btnMenosZoom.Size = new System.Drawing.Size(23, 24);
            this.btnMenosZoom.Text = "toolStripButton2";
            this.btnMenosZoom.Click += new System.EventHandler(this.btnMenosZoom_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.edtTipoArquivo);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.edtArquivo);
            this.groupControl1.Controls.Add(this.pictureEdit1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "The Asphalt World";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1463, 68);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = ":: Datamace ::";
            // 
            // edtTipoArquivo
            // 
            this.edtTipoArquivo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtTipoArquivo.EditValue = "PAGAMENTO";
            this.edtTipoArquivo.Enabled = false;
            this.edtTipoArquivo.Location = new System.Drawing.Point(1364, 40);
            this.edtTipoArquivo.Name = "edtTipoArquivo";
            this.edtTipoArquivo.Properties.Appearance.BackColor = System.Drawing.Color.MistyRose;
            this.edtTipoArquivo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtTipoArquivo.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.edtTipoArquivo.Properties.Appearance.Options.UseBackColor = true;
            this.edtTipoArquivo.Properties.Appearance.Options.UseFont = true;
            this.edtTipoArquivo.Properties.Appearance.Options.UseForeColor = true;
            this.edtTipoArquivo.Properties.AutoHeight = false;
            this.edtTipoArquivo.Properties.ReadOnly = true;
            this.edtTipoArquivo.Size = new System.Drawing.Size(94, 22);
            this.edtTipoArquivo.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MediumBlue;
            this.label2.Location = new System.Drawing.Point(1361, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tipo do Arquivo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumBlue;
            this.label1.Location = new System.Drawing.Point(45, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome do Arquivo";
            // 
            // edtArquivo
            // 
            this.edtArquivo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtArquivo.Location = new System.Drawing.Point(48, 40);
            this.edtArquivo.Name = "edtArquivo";
            editorButtonImageOptions10.Image = global::DP.Properties.Resources.edtArquivo;
            this.edtArquivo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions10, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.edtArquivo.Size = new System.Drawing.Size(1310, 22);
            this.edtArquivo.TabIndex = 1;
            this.edtArquivo.ToolTipController = this.ToolTipController_F;
            this.edtArquivo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.edtArquivo_ButtonClick);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureEdit1.EditValue = global::DP.Properties.Resources.pictureEdit1;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 20);
            this.pictureEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(43, 46);
            this.pictureEdit1.TabIndex = 0;
            // 
            // TabPageGPS
            // 
            this.TabPageGPS.Controls.Add(this.gridGPS);
            this.TabPageGPS.Controls.Add(this.panelControl5);
            this.TabPageGPS.Name = "TabPageGPS";
            this.TabPageGPS.Size = new System.Drawing.Size(1463, 690);
            this.TabPageGPS.Text = "GPS";
            // 
            // gridGPS
            // 
            this.gridGPS.DataSource = this.iNSSBindingSource;
            this.gridGPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridGPS.Location = new System.Drawing.Point(0, 0);
            this.gridGPS.MainView = this.ViewGPS;
            this.gridGPS.Name = "gridGPS";
            this.gridGPS.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox3,
            this.LookUpCODCON});
            this.gridGPS.Size = new System.Drawing.Size(1463, 655);
            this.gridGPS.TabIndex = 0;
            this.gridGPS.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ViewGPS});
            // 
            // iNSSBindingSource
            // 
            this.iNSSBindingSource.DataMember = "INSS";
            this.iNSSBindingSource.DataSource = this.dAutomacaoBancaria;
            // 
            // ViewGPS
            // 
            this.ViewGPS.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.ViewGPS.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.ViewGPS.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.ViewGPS.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.ViewGPS.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.ViewGPS.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.ViewGPS.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.ViewGPS.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.ViewGPS.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.ViewGPS.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.ViewGPS.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ViewGPS.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.ViewGPS.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.ViewGPS.Appearance.Empty.Options.UseBackColor = true;
            this.ViewGPS.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.ViewGPS.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.ViewGPS.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.ViewGPS.Appearance.EvenRow.Options.UseBackColor = true;
            this.ViewGPS.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.ViewGPS.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.ViewGPS.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.ViewGPS.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.ViewGPS.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.ViewGPS.Appearance.FilterPanel.Options.UseBackColor = true;
            this.ViewGPS.Appearance.FilterPanel.Options.UseForeColor = true;
            this.ViewGPS.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.ViewGPS.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.ViewGPS.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.ViewGPS.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ViewGPS.Appearance.FocusedRow.Options.UseBackColor = true;
            this.ViewGPS.Appearance.FocusedRow.Options.UseForeColor = true;
            this.ViewGPS.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.FooterPanel.Options.UseBackColor = true;
            this.ViewGPS.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.ViewGPS.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.ViewGPS.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.ViewGPS.Appearance.GroupButton.Options.UseBackColor = true;
            this.ViewGPS.Appearance.GroupButton.Options.UseBorderColor = true;
            this.ViewGPS.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.ViewGPS.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.ViewGPS.Appearance.GroupFooter.Options.UseBackColor = true;
            this.ViewGPS.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.ViewGPS.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.ViewGPS.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.ViewGPS.Appearance.GroupPanel.Options.UseBackColor = true;
            this.ViewGPS.Appearance.GroupPanel.Options.UseForeColor = true;
            this.ViewGPS.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.ViewGPS.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ViewGPS.Appearance.GroupRow.Options.UseBackColor = true;
            this.ViewGPS.Appearance.GroupRow.Options.UseFont = true;
            this.ViewGPS.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.ViewGPS.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.ViewGPS.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.ViewGPS.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.ViewGPS.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.ViewGPS.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.ViewGPS.Appearance.HorzLine.Options.UseBackColor = true;
            this.ViewGPS.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.ViewGPS.Appearance.OddRow.Options.UseBackColor = true;
            this.ViewGPS.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.ViewGPS.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.ViewGPS.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.ViewGPS.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.ViewGPS.Appearance.Preview.Options.UseBackColor = true;
            this.ViewGPS.Appearance.Preview.Options.UseFont = true;
            this.ViewGPS.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.ViewGPS.Appearance.Row.Options.UseBackColor = true;
            this.ViewGPS.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ViewGPS.Appearance.RowSeparator.Options.UseBackColor = true;
            this.ViewGPS.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.ViewGPS.Appearance.VertLine.Options.UseBackColor = true;
            this.ViewGPS.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCampo1GPS,
            this.colCampo3GPS,
            this.colCampo4GPS,
            this.colCampo5GPS,
            this.colCampo6GPS,
            this.colCampo9GPS,
            this.colCampo10GPS,
            this.colCampo11GPS,
            this.colTipoPag1,
            this.colErro,
            this.colCON,
            this.colCondominio,
            this.colNEON,
            this.colPrevisto,
            this.colSEFIP,
            this.colErroValor,
            this.colCodComunicacao1,
            this.colEMP5});
            this.ViewGPS.GridControl = this.gridGPS;
            this.ViewGPS.Name = "ViewGPS";
            this.ViewGPS.OptionsBehavior.AllowIncrementalSearch = true;
            this.ViewGPS.OptionsView.ColumnAutoWidth = false;
            this.ViewGPS.OptionsView.EnableAppearanceEvenRow = true;
            this.ViewGPS.OptionsView.EnableAppearanceOddRow = true;
            this.ViewGPS.OptionsView.ShowAutoFilterRow = true;
            this.ViewGPS.OptionsView.ShowGroupPanel = false;
            this.ViewGPS.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCondominio, DevExpress.Data.ColumnSortOrder.Descending)});
            this.ViewGPS.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.ViewPIS_RowCellStyle);
            // 
            // colCampo1GPS
            // 
            this.colCampo1GPS.Caption = "Raz�o Social";
            this.colCampo1GPS.FieldName = "Campo1";
            this.colCampo1GPS.Name = "colCampo1GPS";
            this.colCampo1GPS.OptionsColumn.AllowEdit = false;
            this.colCampo1GPS.OptionsColumn.FixedWidth = true;
            this.colCampo1GPS.Visible = true;
            this.colCampo1GPS.VisibleIndex = 3;
            this.colCampo1GPS.Width = 89;
            // 
            // colCampo3GPS
            // 
            this.colCampo3GPS.Caption = "C�d. Pagto";
            this.colCampo3GPS.FieldName = "Campo3";
            this.colCampo3GPS.Name = "colCampo3GPS";
            this.colCampo3GPS.OptionsColumn.AllowEdit = false;
            this.colCampo3GPS.OptionsColumn.FixedWidth = true;
            this.colCampo3GPS.Width = 72;
            // 
            // colCampo4GPS
            // 
            this.colCampo4GPS.Caption = "Comp.";
            this.colCampo4GPS.FieldName = "Campo4";
            this.colCampo4GPS.Name = "colCampo4GPS";
            this.colCampo4GPS.OptionsColumn.AllowEdit = false;
            this.colCampo4GPS.OptionsColumn.FixedWidth = true;
            this.colCampo4GPS.Width = 62;
            // 
            // colCampo5GPS
            // 
            this.colCampo5GPS.Caption = "Identifica��o";
            this.colCampo5GPS.FieldName = "Campo5";
            this.colCampo5GPS.Name = "colCampo5GPS";
            this.colCampo5GPS.OptionsColumn.AllowEdit = false;
            this.colCampo5GPS.OptionsColumn.FixedWidth = true;
            this.colCampo5GPS.Width = 111;
            // 
            // colCampo6GPS
            // 
            this.colCampo6GPS.Caption = "Vr. INSS";
            this.colCampo6GPS.DisplayFormat.FormatString = "N2";
            this.colCampo6GPS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCampo6GPS.FieldName = "Campo6";
            this.colCampo6GPS.Name = "colCampo6GPS";
            this.colCampo6GPS.OptionsColumn.FixedWidth = true;
            this.colCampo6GPS.Visible = true;
            this.colCampo6GPS.VisibleIndex = 4;
            this.colCampo6GPS.Width = 87;
            // 
            // colCampo9GPS
            // 
            this.colCampo9GPS.Caption = "Vr. Outros";
            this.colCampo9GPS.DisplayFormat.FormatString = "N2";
            this.colCampo9GPS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCampo9GPS.FieldName = "Campo9";
            this.colCampo9GPS.Name = "colCampo9GPS";
            this.colCampo9GPS.OptionsColumn.FixedWidth = true;
            this.colCampo9GPS.OptionsFilter.AllowFilter = false;
            this.colCampo9GPS.Visible = true;
            this.colCampo9GPS.VisibleIndex = 5;
            this.colCampo9GPS.Width = 68;
            // 
            // colCampo10GPS
            // 
            this.colCampo10GPS.Caption = "Juros/Multa";
            this.colCampo10GPS.DisplayFormat.FormatString = "N2";
            this.colCampo10GPS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCampo10GPS.FieldName = "Campo10";
            this.colCampo10GPS.Name = "colCampo10GPS";
            this.colCampo10GPS.OptionsColumn.FixedWidth = true;
            this.colCampo10GPS.OptionsFilter.AllowFilter = false;
            this.colCampo10GPS.Width = 70;
            // 
            // colCampo11GPS
            // 
            this.colCampo11GPS.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colCampo11GPS.AppearanceCell.Options.UseFont = true;
            this.colCampo11GPS.Caption = "Folha";
            this.colCampo11GPS.DisplayFormat.FormatString = "N2";
            this.colCampo11GPS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCampo11GPS.FieldName = "Campo11";
            this.colCampo11GPS.Name = "colCampo11GPS";
            this.colCampo11GPS.OptionsColumn.AllowEdit = false;
            this.colCampo11GPS.OptionsColumn.FixedWidth = true;
            this.colCampo11GPS.OptionsColumn.ReadOnly = true;
            this.colCampo11GPS.Visible = true;
            this.colCampo11GPS.VisibleIndex = 6;
            this.colCampo11GPS.Width = 94;
            // 
            // colTipoPag1
            // 
            this.colTipoPag1.Caption = "Tipo Pag";
            this.colTipoPag1.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colTipoPag1.FieldName = "TipoPag";
            this.colTipoPag1.Name = "colTipoPag1";
            this.colTipoPag1.OptionsColumn.FixedWidth = true;
            this.colTipoPag1.Visible = true;
            this.colTipoPag1.VisibleIndex = 0;
            this.colTipoPag1.Width = 93;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colErro
            // 
            this.colErro.Caption = "Erro";
            this.colErro.FieldName = "Erro";
            this.colErro.Name = "colErro";
            this.colErro.OptionsColumn.ReadOnly = true;
            this.colErro.Visible = true;
            this.colErro.VisibleIndex = 11;
            this.colErro.Width = 251;
            // 
            // colCON
            // 
            this.colCON.Caption = "CODCON";
            this.colCON.ColumnEdit = this.LookUpCODCON;
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.FixedWidth = true;
            this.colCON.OptionsColumn.ReadOnly = true;
            this.colCON.Visible = true;
            this.colCON.VisibleIndex = 2;
            this.colCON.Width = 67;
            // 
            // LookUpCODCON
            // 
            this.LookUpCODCON.AutoHeight = false;
            this.LookUpCODCON.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpCODCON.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CODCON", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "Nome", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpCODCON.DataSource = this.cONDOMINIOSBindingSource;
            this.LookUpCODCON.DisplayMember = "CONCodigo";
            this.LookUpCODCON.Name = "LookUpCODCON";
            this.LookUpCODCON.ValueMember = "CON";
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.AllowNew = false;
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dAutomacaoBancaria;
            // 
            // colCondominio
            // 
            this.colCondominio.Caption = "DTM";
            this.colCondominio.FieldName = "Condominio";
            this.colCondominio.Name = "colCondominio";
            this.colCondominio.OptionsColumn.FixedWidth = true;
            this.colCondominio.OptionsColumn.ReadOnly = true;
            this.colCondominio.Visible = true;
            this.colCondominio.VisibleIndex = 1;
            this.colCondominio.Width = 54;
            // 
            // colNEON
            // 
            this.colNEON.Caption = "Local";
            this.colNEON.DisplayFormat.FormatString = "n2";
            this.colNEON.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNEON.FieldName = "NEON";
            this.colNEON.Name = "colNEON";
            this.colNEON.OptionsColumn.ReadOnly = true;
            this.colNEON.Visible = true;
            this.colNEON.VisibleIndex = 7;
            this.colNEON.Width = 84;
            // 
            // colPrevisto
            // 
            this.colPrevisto.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colPrevisto.AppearanceCell.Options.UseFont = true;
            this.colPrevisto.Caption = "Total Previsto";
            this.colPrevisto.DisplayFormat.FormatString = "n2";
            this.colPrevisto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrevisto.FieldName = "Previsto";
            this.colPrevisto.Name = "colPrevisto";
            this.colPrevisto.OptionsColumn.ReadOnly = true;
            this.colPrevisto.Visible = true;
            this.colPrevisto.VisibleIndex = 8;
            this.colPrevisto.Width = 82;
            // 
            // colSEFIP
            // 
            this.colSEFIP.Caption = "Sefip";
            this.colSEFIP.DisplayFormat.FormatString = "n2";
            this.colSEFIP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSEFIP.FieldName = "SEFIP";
            this.colSEFIP.Name = "colSEFIP";
            this.colSEFIP.OptionsColumn.ReadOnly = true;
            this.colSEFIP.Visible = true;
            this.colSEFIP.VisibleIndex = 9;
            this.colSEFIP.Width = 83;
            // 
            // colErroValor
            // 
            this.colErroValor.Caption = "Erro";
            this.colErroValor.DisplayFormat.FormatString = "n2";
            this.colErroValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colErroValor.FieldName = "ErroValor";
            this.colErroValor.Name = "colErroValor";
            this.colErroValor.OptionsColumn.ReadOnly = true;
            this.colErroValor.Visible = true;
            this.colErroValor.VisibleIndex = 10;
            this.colErroValor.Width = 68;
            // 
            // colCodComunicacao1
            // 
            this.colCodComunicacao1.Caption = "C�d. Comunica��o";
            this.colCodComunicacao1.FieldName = "CodComunicacao";
            this.colCodComunicacao1.Name = "colCodComunicacao1";
            // 
            // colEMP5
            // 
            this.colEMP5.FieldName = "EMP";
            this.colEMP5.Name = "colEMP5";
            this.colEMP5.Visible = true;
            this.colEMP5.VisibleIndex = 12;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.simpleButton8);
            this.panelControl5.Controls.Add(this.simpleButton7);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(0, 655);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1463, 35);
            this.panelControl5.TabIndex = 5;
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(130, 7);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(142, 23);
            this.simpleButton8.TabIndex = 1;
            this.simpleButton8.Text = "Gerar";
            this.simpleButton8.Click += new System.EventHandler(this.btnRemessaGPS_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(6, 6);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(118, 23);
            this.simpleButton7.TabIndex = 0;
            this.simpleButton7.Text = "Corre��o autom�tica";
            this.simpleButton7.Click += new System.EventHandler(this.CorrecAuto);
            // 
            // TabPagePIS
            // 
            this.TabPagePIS.Controls.Add(this.tableLayoutPanelPIS);
            this.TabPagePIS.Controls.Add(this.gridPIS);
            this.TabPagePIS.Name = "TabPagePIS";
            this.TabPagePIS.Size = new System.Drawing.Size(1463, 690);
            this.TabPagePIS.Text = "DARF";
            // 
            // tableLayoutPanelPIS
            // 
            this.tableLayoutPanelPIS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(241)))), ((int)(((byte)(254)))));
            this.tableLayoutPanelPIS.ColumnCount = 2;
            this.tableLayoutPanelPIS.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanelPIS.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelPIS.Controls.Add(this.ToolStripDados, 0, 0);
            this.tableLayoutPanelPIS.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanelPIS.Location = new System.Drawing.Point(0, 664);
            this.tableLayoutPanelPIS.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelPIS.Name = "tableLayoutPanelPIS";
            this.tableLayoutPanelPIS.RowCount = 1;
            this.tableLayoutPanelPIS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPIS.Size = new System.Drawing.Size(1463, 26);
            this.tableLayoutPanelPIS.TabIndex = 4;
            // 
            // ToolStripDados
            // 
            this.ToolStripDados.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ToolStripDados.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStripDados.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRemessaPIS,
            this.toolStripSplitButton1});
            this.ToolStripDados.Location = new System.Drawing.Point(0, 1);
            this.ToolStripDados.Name = "ToolStripDados";
            this.ToolStripDados.Size = new System.Drawing.Size(202, 25);
            this.ToolStripDados.TabIndex = 1;
            this.ToolStripDados.Text = "toolStrip2";
            // 
            // btnRemessaPIS
            // 
            this.btnRemessaPIS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemessaPIS.Name = "btnRemessaPIS";
            this.btnRemessaPIS.Size = new System.Drawing.Size(84, 22);
            this.btnRemessaPIS.Text = "Gerar Arquivo";
            this.btnRemessaPIS.Click += new System.EventHandler(this.btnRemessaPIS_Click);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(32, 22);
            this.toolStripSplitButton1.Text = "toolStripSplitButton1";
            this.toolStripSplitButton1.ButtonClick += new System.EventHandler(this.toolStripSplitButton1_ButtonClick);
            // 
            // gridPIS
            // 
            this.gridPIS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPIS.DataSource = this.pISBindingSource;
            this.gridPIS.Location = new System.Drawing.Point(-1, -1);
            this.gridPIS.MainView = this.ViewPIS;
            this.gridPIS.Name = "gridPIS";
            this.gridPIS.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox2,
            this.repositoryItemImageComboBox7});
            this.gridPIS.Size = new System.Drawing.Size(1465, 690);
            this.gridPIS.TabIndex = 0;
            this.gridPIS.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ViewPIS});
            // 
            // pISBindingSource
            // 
            this.pISBindingSource.DataMember = "PIS";
            this.pISBindingSource.DataSource = this.dAutomacaoBancaria;
            // 
            // ViewPIS
            // 
            this.ViewPIS.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.ViewPIS.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ViewPIS.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.ViewPIS.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.ViewPIS.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.ViewPIS.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.ViewPIS.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.ViewPIS.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.ViewPIS.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ViewPIS.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.ViewPIS.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.ViewPIS.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.ViewPIS.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.ViewPIS.Appearance.Empty.Options.UseBackColor = true;
            this.ViewPIS.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.EvenRow.Options.UseBackColor = true;
            this.ViewPIS.Appearance.EvenRow.Options.UseForeColor = true;
            this.ViewPIS.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.ViewPIS.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ViewPIS.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.ViewPIS.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.ViewPIS.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.ViewPIS.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.ViewPIS.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.ViewPIS.Appearance.FilterPanel.Options.UseBackColor = true;
            this.ViewPIS.Appearance.FilterPanel.Options.UseForeColor = true;
            this.ViewPIS.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.ViewPIS.Appearance.FixedLine.Options.UseBackColor = true;
            this.ViewPIS.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.ViewPIS.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.FocusedCell.Options.UseBackColor = true;
            this.ViewPIS.Appearance.FocusedCell.Options.UseForeColor = true;
            this.ViewPIS.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.ViewPIS.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.ViewPIS.Appearance.FocusedRow.Options.UseBackColor = true;
            this.ViewPIS.Appearance.FocusedRow.Options.UseForeColor = true;
            this.ViewPIS.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.ViewPIS.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ViewPIS.Appearance.FooterPanel.Options.UseBackColor = true;
            this.ViewPIS.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.ViewPIS.Appearance.FooterPanel.Options.UseForeColor = true;
            this.ViewPIS.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.ViewPIS.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.ViewPIS.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.GroupButton.Options.UseBackColor = true;
            this.ViewPIS.Appearance.GroupButton.Options.UseBorderColor = true;
            this.ViewPIS.Appearance.GroupButton.Options.UseForeColor = true;
            this.ViewPIS.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.ViewPIS.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.ViewPIS.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.GroupFooter.Options.UseBackColor = true;
            this.ViewPIS.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.ViewPIS.Appearance.GroupFooter.Options.UseForeColor = true;
            this.ViewPIS.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.ViewPIS.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.GroupPanel.Options.UseBackColor = true;
            this.ViewPIS.Appearance.GroupPanel.Options.UseForeColor = true;
            this.ViewPIS.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.ViewPIS.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.ViewPIS.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ViewPIS.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.GroupRow.Options.UseBackColor = true;
            this.ViewPIS.Appearance.GroupRow.Options.UseBorderColor = true;
            this.ViewPIS.Appearance.GroupRow.Options.UseFont = true;
            this.ViewPIS.Appearance.GroupRow.Options.UseForeColor = true;
            this.ViewPIS.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.ViewPIS.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.ViewPIS.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ViewPIS.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.ViewPIS.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.ViewPIS.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.ViewPIS.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.ViewPIS.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.ViewPIS.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.ViewPIS.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.ViewPIS.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.ViewPIS.Appearance.HorzLine.Options.UseBackColor = true;
            this.ViewPIS.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.ViewPIS.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.OddRow.Options.UseBackColor = true;
            this.ViewPIS.Appearance.OddRow.Options.UseForeColor = true;
            this.ViewPIS.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.ViewPIS.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.ViewPIS.Appearance.Preview.Options.UseBackColor = true;
            this.ViewPIS.Appearance.Preview.Options.UseForeColor = true;
            this.ViewPIS.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.ViewPIS.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.ViewPIS.Appearance.Row.Options.UseBackColor = true;
            this.ViewPIS.Appearance.Row.Options.UseForeColor = true;
            this.ViewPIS.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.ViewPIS.Appearance.RowSeparator.Options.UseBackColor = true;
            this.ViewPIS.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.ViewPIS.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.ViewPIS.Appearance.SelectedRow.Options.UseBackColor = true;
            this.ViewPIS.Appearance.SelectedRow.Options.UseForeColor = true;
            this.ViewPIS.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.ViewPIS.Appearance.VertLine.Options.UseBackColor = true;
            this.ViewPIS.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCampo1PIS,
            this.colCampo3PIS,
            this.colCampo4PIS,
            this.colCampo5PIS,
            this.colCampo6PIS,
            this.colCampo7PIS,
            this.colCampo8PIS,
            this.colCampo9PIS,
            this.colCampo10PIS,
            this.colBanco,
            this.colAgencia,
            this.colConta,
            this.colTipoPag,
            this.colErro1,
            this.colCodComunicacao,
            this.colEMP3,
            this.colCON1});
            this.ViewPIS.GridControl = this.gridPIS;
            this.ViewPIS.Name = "ViewPIS";
            this.ViewPIS.OptionsView.ColumnAutoWidth = false;
            this.ViewPIS.OptionsView.EnableAppearanceEvenRow = true;
            this.ViewPIS.OptionsView.EnableAppearanceOddRow = true;
            this.ViewPIS.OptionsView.ShowAutoFilterRow = true;
            this.ViewPIS.OptionsView.ShowGroupPanel = false;
            this.ViewPIS.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.ViewPIS_RowCellStyle);
            this.ViewPIS.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.ViewPIS_RowUpdated);
            // 
            // colCampo1PIS
            // 
            this.colCampo1PIS.Caption = "Raz�o Social";
            this.colCampo1PIS.FieldName = "Campo1";
            this.colCampo1PIS.Name = "colCampo1PIS";
            this.colCampo1PIS.OptionsColumn.FixedWidth = true;
            this.colCampo1PIS.Visible = true;
            this.colCampo1PIS.VisibleIndex = 2;
            this.colCampo1PIS.Width = 107;
            // 
            // colCampo3PIS
            // 
            this.colCampo3PIS.Caption = "CNPJ/CPF";
            this.colCampo3PIS.FieldName = "Campo3";
            this.colCampo3PIS.Name = "colCampo3PIS";
            this.colCampo3PIS.OptionsColumn.FixedWidth = true;
            this.colCampo3PIS.Visible = true;
            this.colCampo3PIS.VisibleIndex = 3;
            this.colCampo3PIS.Width = 107;
            // 
            // colCampo4PIS
            // 
            this.colCampo4PIS.Caption = "C�d. Receita";
            this.colCampo4PIS.FieldName = "Campo4";
            this.colCampo4PIS.Name = "colCampo4PIS";
            this.colCampo4PIS.OptionsColumn.FixedWidth = true;
            this.colCampo4PIS.Visible = true;
            this.colCampo4PIS.VisibleIndex = 4;
            this.colCampo4PIS.Width = 82;
            // 
            // colCampo5PIS
            // 
            this.colCampo5PIS.Caption = "N�mero Ref.";
            this.colCampo5PIS.FieldName = "Campo5";
            this.colCampo5PIS.Name = "colCampo5PIS";
            this.colCampo5PIS.OptionsColumn.FixedWidth = true;
            this.colCampo5PIS.Visible = true;
            this.colCampo5PIS.VisibleIndex = 5;
            this.colCampo5PIS.Width = 81;
            // 
            // colCampo6PIS
            // 
            this.colCampo6PIS.Caption = "Dt Vencto";
            this.colCampo6PIS.DisplayFormat.FormatString = "d";
            this.colCampo6PIS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCampo6PIS.FieldName = "Campo6";
            this.colCampo6PIS.Name = "colCampo6PIS";
            this.colCampo6PIS.OptionsColumn.FixedWidth = true;
            this.colCampo6PIS.Visible = true;
            this.colCampo6PIS.VisibleIndex = 6;
            // 
            // colCampo7PIS
            // 
            this.colCampo7PIS.Caption = "Vr Principal";
            this.colCampo7PIS.DisplayFormat.FormatString = "N2";
            this.colCampo7PIS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCampo7PIS.FieldName = "Campo7";
            this.colCampo7PIS.Name = "colCampo7PIS";
            this.colCampo7PIS.OptionsColumn.FixedWidth = true;
            this.colCampo7PIS.Visible = true;
            this.colCampo7PIS.VisibleIndex = 7;
            // 
            // colCampo8PIS
            // 
            this.colCampo8PIS.Caption = "Vr Multa";
            this.colCampo8PIS.DisplayFormat.FormatString = "N2";
            this.colCampo8PIS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCampo8PIS.FieldName = "Campo8";
            this.colCampo8PIS.Name = "colCampo8PIS";
            this.colCampo8PIS.OptionsColumn.FixedWidth = true;
            this.colCampo8PIS.Visible = true;
            this.colCampo8PIS.VisibleIndex = 8;
            this.colCampo8PIS.Width = 65;
            // 
            // colCampo9PIS
            // 
            this.colCampo9PIS.Caption = "Vr Juros";
            this.colCampo9PIS.DisplayFormat.FormatString = "N2";
            this.colCampo9PIS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCampo9PIS.FieldName = "Campo9";
            this.colCampo9PIS.Name = "colCampo9PIS";
            this.colCampo9PIS.OptionsColumn.FixedWidth = true;
            this.colCampo9PIS.Visible = true;
            this.colCampo9PIS.VisibleIndex = 9;
            this.colCampo9PIS.Width = 66;
            // 
            // colCampo10PIS
            // 
            this.colCampo10PIS.Caption = "Vr Total";
            this.colCampo10PIS.DisplayFormat.FormatString = "N2";
            this.colCampo10PIS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCampo10PIS.FieldName = "Campo10";
            this.colCampo10PIS.Name = "colCampo10PIS";
            this.colCampo10PIS.OptionsColumn.FixedWidth = true;
            this.colCampo10PIS.OptionsColumn.ReadOnly = true;
            this.colCampo10PIS.Visible = true;
            this.colCampo10PIS.VisibleIndex = 10;
            // 
            // colBanco
            // 
            this.colBanco.Caption = "Banco";
            this.colBanco.FieldName = "Banco";
            this.colBanco.Name = "colBanco";
            this.colBanco.OptionsColumn.FixedWidth = true;
            this.colBanco.Visible = true;
            this.colBanco.VisibleIndex = 1;
            this.colBanco.Width = 52;
            // 
            // colAgencia
            // 
            this.colAgencia.Caption = "Ag�ncia";
            this.colAgencia.FieldName = "Agencia";
            this.colAgencia.Name = "colAgencia";
            this.colAgencia.OptionsColumn.FixedWidth = true;
            this.colAgencia.Width = 58;
            // 
            // colConta
            // 
            this.colConta.Caption = "Conta";
            this.colConta.FieldName = "Conta";
            this.colConta.Name = "colConta";
            this.colConta.OptionsColumn.FixedWidth = true;
            this.colConta.Width = 101;
            // 
            // colTipoPag
            // 
            this.colTipoPag.Caption = "Tipo de pg.";
            this.colTipoPag.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colTipoPag.FieldName = "TipoPag";
            this.colTipoPag.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colTipoPag.Name = "colTipoPag";
            this.colTipoPag.OptionsColumn.FixedWidth = true;
            this.colTipoPag.Visible = true;
            this.colTipoPag.VisibleIndex = 12;
            this.colTipoPag.Width = 72;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colErro1
            // 
            this.colErro1.Caption = "Erro";
            this.colErro1.FieldName = "Erro";
            this.colErro1.Name = "colErro1";
            this.colErro1.Visible = true;
            this.colErro1.VisibleIndex = 11;
            this.colErro1.Width = 208;
            // 
            // colCodComunicacao
            // 
            this.colCodComunicacao.Caption = "C�d. Comunica��o";
            this.colCodComunicacao.FieldName = "CodComunicacao";
            this.colCodComunicacao.Name = "colCodComunicacao";
            // 
            // colEMP3
            // 
            this.colEMP3.ColumnEdit = this.repositoryItemImageComboBox7;
            this.colEMP3.FieldName = "EMP";
            this.colEMP3.Name = "colEMP3";
            this.colEMP3.Visible = true;
            this.colEMP3.VisibleIndex = 0;
            this.colEMP3.Width = 48;
            // 
            // repositoryItemImageComboBox7
            // 
            this.repositoryItemImageComboBox7.AutoHeight = false;
            this.repositoryItemImageComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox7.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("SBC", 1, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("SA", 3, -1)});
            this.repositoryItemImageComboBox7.Name = "repositoryItemImageComboBox7";
            // 
            // colCON1
            // 
            this.colCON1.FieldName = "CON";
            this.colCON1.Name = "colCON1";
            // 
            // TabRecibos
            // 
            this.TabRecibos.Controls.Add(this.gridRecibos);
            this.TabRecibos.Controls.Add(this.panelControl3);
            this.TabRecibos.Name = "TabRecibos";
            this.TabRecibos.Size = new System.Drawing.Size(1463, 690);
            this.TabRecibos.Text = "Recibos";
            // 
            // gridRecibos
            // 
            this.gridRecibos.DataSource = this.recibosBindingSource;
            this.gridRecibos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRecibos.Location = new System.Drawing.Point(0, 37);
            this.gridRecibos.MainView = this.gridView2;
            this.gridRecibos.Name = "gridRecibos";
            this.gridRecibos.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repOK});
            this.gridRecibos.Size = new System.Drawing.Size(1463, 653);
            this.gridRecibos.TabIndex = 1;
            this.gridRecibos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // recibosBindingSource
            // 
            this.recibosBindingSource.DataMember = "Recibos";
            this.recibosBindingSource.DataSource = typeof(DP.impressos.dRecSal);
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gainsboro;
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseBorderColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.gridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONNome2,
            this.colBOLNome,
            this.colQtot1,
            this.colQtot2,
            this.colQtot3,
            this.colOK});
            this.gridView2.GridControl = this.gridRecibos;
            this.gridView2.GroupCount = 1;
            this.gridView2.GroupFormat = "{0}: [#image]{1}      Recibos: {2}";
            this.gridView2.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "BOLNome", null, "{0}")});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONNome2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCONNome2
            // 
            this.colCONNome2.Caption = "Condom�nio";
            this.colCONNome2.FieldName = "CONNome";
            this.colCONNome2.Name = "colCONNome2";
            // 
            // colBOLNome
            // 
            this.colBOLNome.Caption = "Nome";
            this.colBOLNome.FieldName = "BOLNome";
            this.colBOLNome.Name = "colBOLNome";
            this.colBOLNome.Visible = true;
            this.colBOLNome.VisibleIndex = 0;
            this.colBOLNome.Width = 518;
            // 
            // colQtot1
            // 
            this.colQtot1.Caption = "Vencimentos";
            this.colQtot1.DisplayFormat.FormatString = "n2";
            this.colQtot1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQtot1.FieldName = "Qtot1";
            this.colQtot1.Name = "colQtot1";
            this.colQtot1.OptionsColumn.FixedWidth = true;
            this.colQtot1.Visible = true;
            this.colQtot1.VisibleIndex = 1;
            this.colQtot1.Width = 85;
            // 
            // colQtot2
            // 
            this.colQtot2.Caption = "Descontos";
            this.colQtot2.DisplayFormat.FormatString = "n2";
            this.colQtot2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQtot2.FieldName = "Qtot2";
            this.colQtot2.Name = "colQtot2";
            this.colQtot2.OptionsColumn.FixedWidth = true;
            this.colQtot2.Visible = true;
            this.colQtot2.VisibleIndex = 2;
            this.colQtot2.Width = 85;
            // 
            // colQtot3
            // 
            this.colQtot3.Caption = "L�quido";
            this.colQtot3.DisplayFormat.FormatString = "n2";
            this.colQtot3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQtot3.FieldName = "Qtot3";
            this.colQtot3.Name = "colQtot3";
            this.colQtot3.OptionsColumn.FixedWidth = true;
            this.colQtot3.Visible = true;
            this.colQtot3.VisibleIndex = 3;
            this.colQtot3.Width = 85;
            // 
            // colOK
            // 
            this.colOK.Caption = "OK";
            this.colOK.ColumnEdit = this.repOK;
            this.colOK.FieldName = "OK";
            this.colOK.Name = "colOK";
            this.colOK.Visible = true;
            this.colOK.VisibleIndex = 4;
            // 
            // repOK
            // 
            this.repOK.AllowFocused = false;
            this.repOK.AutoHeight = false;
            this.repOK.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repOK.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repOK.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("SIM", true, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("N�O", false, -1)});
            this.repOK.Name = "repOK";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Controls.Add(this.cBotaoImpBol2);
            this.panelControl3.Controls.Add(this.cBotaoImpBol1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1463, 37);
            this.panelControl3.TabIndex = 2;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "Gravar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cBotaoImpBol2
            // 
            this.cBotaoImpBol2.Enabled = false;
            this.cBotaoImpBol2.ItemComprovante = false;
            this.cBotaoImpBol2.Location = new System.Drawing.Point(212, 2);
            this.cBotaoImpBol2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol2.Name = "cBotaoImpBol2";
            this.cBotaoImpBol2.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol2.TabIndex = 1;
            this.cBotaoImpBol2.Titulo = "Protocolo";
            this.cBotaoImpBol2.clicado += new System.EventHandler(this.cBotaoImpBol2_clicado);
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.Enabled = false;
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(86, 2);
            this.cBotaoImpBol1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol1.TabIndex = 0;
            this.cBotaoImpBol1.Titulo = "Recibos";
            this.cBotaoImpBol1.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // TabRetornoRec
            // 
            this.TabRetornoRec.Controls.Add(this.gridControl2);
            this.TabRetornoRec.Controls.Add(this.panelControl4);
            this.TabRetornoRec.Name = "TabRetornoRec";
            this.TabRetornoRec.Size = new System.Drawing.Size(1394, 690);
            this.TabRetornoRec.Text = "Retorno Recibos";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.dRecSalBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 155);
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookUpEdit2});
            this.gridControl2.Size = new System.Drawing.Size(1394, 535);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // dRecSalBindingSource
            // 
            this.dRecSalBindingSource.DataMember = "HOLerites";
            this.dRecSalBindingSource.DataSource = this.dRecSal;
            // 
            // dRecSal
            // 
            this.dRecSal.DataSetName = "dRecSal";
            this.dRecSal.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView3.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView3.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView3.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView3.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView3.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView3.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.Empty.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.gridView3.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView3.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView3.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView3.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView3.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView3.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView3.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.gridView3.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView3.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView3.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView3.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.gridView3.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView3.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView3.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView3.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView3.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView3.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView3.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView3.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.gridView3.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gainsboro;
            this.gridView3.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView3.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView3.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView3.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.gridView3.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView3.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView3.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView3.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView3.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.gridView3.Appearance.Preview.Options.UseBackColor = true;
            this.gridView3.Appearance.Preview.Options.UseFont = true;
            this.gridView3.Appearance.Preview.Options.UseForeColor = true;
            this.gridView3.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView3.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView3.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.Row.Options.UseBackColor = true;
            this.gridView3.Appearance.Row.Options.UseBorderColor = true;
            this.gridView3.Appearance.Row.Options.UseForeColor = true;
            this.gridView3.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.gridView3.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView3.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView3.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.gridView3.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView3.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.gridView3.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHOL_CON,
            this.colHOLNome,
            this.colHOLRetorno,
            this.colHOL,
            this.colHOLQ0,
            this.gridColumn1,
            this.colHOLDuasvias,
            this.colCONCodigoFolha1,
            this.colCONCodigoFolha2,
            this.colEMP6});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.GroupCount = 1;
            this.gridView3.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "", null, "Recibos: {0}")});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.Editable = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.EnableAppearanceOddRow = true;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHOL_CON, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHOLNome, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colHOL_CON
            // 
            this.colHOL_CON.Caption = "Condom�nio";
            this.colHOL_CON.FieldName = "CONNome";
            this.colHOL_CON.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colHOL_CON.Name = "colHOL_CON";
            // 
            // colHOLNome
            // 
            this.colHOLNome.Caption = "Nome";
            this.colHOLNome.FieldName = "HOLNome";
            this.colHOLNome.Name = "colHOLNome";
            this.colHOLNome.Visible = true;
            this.colHOLNome.VisibleIndex = 4;
            this.colHOLNome.Width = 1041;
            // 
            // colHOLRetorno
            // 
            this.colHOLRetorno.Caption = "Retorno";
            this.colHOLRetorno.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colHOLRetorno.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colHOLRetorno.FieldName = "HOLRetorno";
            this.colHOLRetorno.Name = "colHOLRetorno";
            this.colHOLRetorno.Visible = true;
            this.colHOLRetorno.VisibleIndex = 5;
            this.colHOLRetorno.Width = 193;
            // 
            // colHOL
            // 
            this.colHOL.Caption = "C�d.";
            this.colHOL.FieldName = "HOL";
            this.colHOL.Name = "colHOL";
            this.colHOL.Width = 65;
            // 
            // colHOLQ0
            // 
            this.colHOLQ0.Caption = "T�tulo";
            this.colHOLQ0.FieldName = "HOLQ0";
            this.colHOLQ0.Name = "colHOLQ0";
            this.colHOLQ0.Visible = true;
            this.colHOLQ0.VisibleIndex = 3;
            this.colHOLQ0.Width = 167;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "CODCON";
            this.gridColumn1.FieldName = "CONCodigo";
            this.gridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 103;
            // 
            // colHOLDuasvias
            // 
            this.colHOLDuasvias.FieldName = "HOLDuasvias";
            this.colHOLDuasvias.Name = "colHOLDuasvias";
            // 
            // colCONCodigoFolha1
            // 
            this.colCONCodigoFolha1.Caption = "Cod. Folha";
            this.colCONCodigoFolha1.FieldName = "CONCodigoFolha1";
            this.colCONCodigoFolha1.Name = "colCONCodigoFolha1";
            this.colCONCodigoFolha1.Visible = true;
            this.colCONCodigoFolha1.VisibleIndex = 0;
            this.colCONCodigoFolha1.Width = 62;
            // 
            // colCONCodigoFolha2
            // 
            this.colCONCodigoFolha2.Caption = "Cod. Folha";
            this.colCONCodigoFolha2.FieldName = "CONCodigoFolha2";
            this.colCONCodigoFolha2.Name = "colCONCodigoFolha2";
            this.colCONCodigoFolha2.Visible = true;
            this.colCONCodigoFolha2.VisibleIndex = 1;
            this.colCONCodigoFolha2.Width = 62;
            // 
            // colEMP6
            // 
            this.colEMP6.FieldName = "EMP";
            this.colEMP6.Name = "colEMP6";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.DataSource = this.cONDOMINIOSBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "CONNome";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ValueMember = "CON";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.DataSource = this.cONDOMINIOSBindingSource;
            this.repositoryItemLookUpEdit2.DisplayMember = "CONCodigo";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.ValueMember = "CON";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.cBotaoImpBol4);
            this.panelControl4.Controls.Add(this.simpleButton6);
            this.panelControl4.Controls.Add(this.simpleButton3);
            this.panelControl4.Controls.Add(this.simpleButton2);
            this.panelControl4.Controls.Add(this.labelControl1);
            this.panelControl4.Controls.Add(this.ChNR);
            this.panelControl4.Controls.Add(this.gridControl3);
            this.panelControl4.Controls.Add(this.cBotaoImpBol3);
            this.panelControl4.Controls.Add(this.spinATE);
            this.panelControl4.Controls.Add(this.spinDE);
            this.panelControl4.Controls.Add(this.simpleButton5);
            this.panelControl4.Controls.Add(this.textEdit1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1394, 155);
            this.panelControl4.TabIndex = 1;
            // 
            // cBotaoImpBol4
            // 
            this.cBotaoImpBol4.ItemComprovante = false;
            this.cBotaoImpBol4.Location = new System.Drawing.Point(413, 60);
            this.cBotaoImpBol4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol4.Name = "cBotaoImpBol4";
            this.cBotaoImpBol4.Size = new System.Drawing.Size(309, 26);
            this.cBotaoImpBol4.TabIndex = 14;
            this.cBotaoImpBol4.Titulo = "Imprimir Selecionado";
            this.cBotaoImpBol4.clicado += new System.EventHandler(this.cBotaoImpBol4_clicado);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(413, 121);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(140, 23);
            this.simpleButton6.TabIndex = 13;
            this.simpleButton6.Text = "CANCELAR Baixas";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(582, 92);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(140, 23);
            this.simpleButton3.TabIndex = 12;
            this.simpleButton3.Text = "Baixar TODOS";
            this.simpleButton3.Click += new System.EventHandler(this.BaxaTodos);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(413, 92);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(140, 23);
            this.simpleButton2.TabIndex = 11;
            this.simpleButton2.Text = "Baixar Selecionado";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click_1);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(428, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(95, 13);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "C�digo de barras";
            // 
            // ChNR
            // 
            this.ChNR.EditValue = true;
            this.ChNR.Location = new System.Drawing.Point(5, 1);
            this.ChNR.Name = "ChNR";
            this.ChNR.Properties.Caption = "Mostrar somente os n�o retornados";
            this.ChNR.Size = new System.Drawing.Size(396, 19);
            this.ChNR.TabIndex = 9;
            this.ChNR.CheckedChanged += new System.EventHandler(this.ChNR_CheckedChanged);
            // 
            // gridControl3
            // 
            this.gridControl3.DataMember = "HOleritTitulo";
            this.gridControl3.DataSource = this.dRecSal;
            this.gridControl3.Location = new System.Drawing.Point(5, 26);
            this.gridControl3.MainView = this.gridView4;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(396, 123);
            this.gridControl3.TabIndex = 8;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHOTTitulo,
            this.colHOTDATAI,
            this.colAguardo,
            this.colEMP4,
            this.colHOT});
            this.gridView4.GridControl = this.gridControl3;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.Editable = false;
            this.gridView4.OptionsDetail.EnableMasterViewMode = false;
            this.gridView4.OptionsSelection.InvertSelection = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHOTDATAI, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView4_RowStyle);
            this.gridView4.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView4_FocusedRowChanged);
            // 
            // colHOTTitulo
            // 
            this.colHOTTitulo.Caption = "T�tulo";
            this.colHOTTitulo.FieldName = "HOTTitulo";
            this.colHOTTitulo.Name = "colHOTTitulo";
            this.colHOTTitulo.Visible = true;
            this.colHOTTitulo.VisibleIndex = 1;
            this.colHOTTitulo.Width = 223;
            // 
            // colHOTDATAI
            // 
            this.colHOTDATAI.Caption = "Data";
            this.colHOTDATAI.FieldName = "HOTDATAI";
            this.colHOTDATAI.Name = "colHOTDATAI";
            this.colHOTDATAI.Visible = true;
            this.colHOTDATAI.VisibleIndex = 0;
            this.colHOTDATAI.Width = 67;
            // 
            // colAguardo
            // 
            this.colAguardo.Caption = "Aguardo";
            this.colAguardo.FieldName = "Aguardo";
            this.colAguardo.Name = "colAguardo";
            this.colAguardo.OptionsColumn.ReadOnly = true;
            this.colAguardo.Visible = true;
            this.colAguardo.VisibleIndex = 2;
            this.colAguardo.Width = 53;
            // 
            // colEMP4
            // 
            this.colEMP4.FieldName = "EMP";
            this.colEMP4.Name = "colEMP4";
            // 
            // colHOT
            // 
            this.colHOT.FieldName = "HOT";
            this.colHOT.Name = "colHOT";
            // 
            // cBotaoImpBol3
            // 
            this.cBotaoImpBol3.ItemComprovante = false;
            this.cBotaoImpBol3.Location = new System.Drawing.Point(602, 31);
            this.cBotaoImpBol3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol3.Name = "cBotaoImpBol3";
            this.cBotaoImpBol3.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol3.TabIndex = 7;
            this.cBotaoImpBol3.Titulo = "Imp Todos";
            this.cBotaoImpBol3.clicado += new System.EventHandler(this.cBotaoImpBol3_clicado);
            // 
            // spinATE
            // 
            this.spinATE.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinATE.Location = new System.Drawing.Point(525, 37);
            this.spinATE.Name = "spinATE";
            this.spinATE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinATE.Size = new System.Drawing.Size(58, 20);
            this.spinATE.TabIndex = 6;
            // 
            // spinDE
            // 
            this.spinDE.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinDE.Location = new System.Drawing.Point(461, 37);
            this.spinDE.Name = "spinDE";
            this.spinDE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinDE.Size = new System.Drawing.Size(58, 20);
            this.spinDE.TabIndex = 5;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(413, 34);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(42, 23);
            this.simpleButton5.TabIndex = 4;
            this.simpleButton5.Text = "Cod.";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(525, 5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(118, 20);
            this.textEdit1.TabIndex = 0;
            this.textEdit1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEdit1_KeyDown);
            // 
            // rHTarefaSubdetalheBindingSource
            // 
            this.rHTarefaSubdetalheBindingSource.DataMember = "RHTarefaSubdetalhe";
            this.rHTarefaSubdetalheBindingSource.DataSource = this.dAutomacaoBancaria;
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // remessasPendentesBindingSource
            // 
            this.remessasPendentesBindingSource.DataMember = "RemessasPendentes";
            this.remessasPendentesBindingSource.DataSource = this.dAutomacaoBancaria;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "rtf";
            this.openFileDialog.Filter = "Formato Rich Text|*.rtf";
            this.openFileDialog.Title = "Selecione um arquivo";
            // 
            // rEMESSASDPTableAdapter
            // 
            this.rEMESSASDPTableAdapter.ClearBeforeFill = true;
            this.rEMESSASDPTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("rEMESSASDPTableAdapter.GetNovosDados")));
            this.rEMESSASDPTableAdapter.TabelaDataTable = null;
            // 
            // rEMESSASDPBindingSource
            // 
            this.rEMESSASDPBindingSource.DataMember = "REMESSASDP";
            this.rEMESSASDPBindingSource.DataSource = this.dAutomacaoBancaria;
            // 
            // detremessadpTableAdapter1
            // 
            this.detremessadpTableAdapter1.ClearBeforeFill = true;
            this.detremessadpTableAdapter1.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("detremessadpTableAdapter1.GetNovosDados")));
            this.detremessadpTableAdapter1.TabelaDataTable = null;
            // 
            // remessasPendentesTableAdapter
            // 
            this.remessasPendentesTableAdapter.ClearBeforeFill = true;
            this.remessasPendentesTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("remessasPendentesTableAdapter.GetNovosDados")));
            this.remessasPendentesTableAdapter.TabelaDataTable = null;
            // 
            // dImpProtocolo1
            // 
            this.dImpProtocolo1.DataSetName = "dImpProtocolo";
            this.dImpProtocolo1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // timerAviso
            // 
            this.timerAviso.Interval = 1000;
            this.timerAviso.Tick += new System.EventHandler(this.timerAviso_Tick);
            // 
            // cAutomacaoBancaria
            // 
            this.Controls.Add(this.TabControlAutomacao);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cAutomacaoBancaria";
            this.Size = new System.Drawing.Size(1396, 711);
            this.cargaPrincipal += new System.EventHandler(this.cAutomacaoBancaria_cargaPrincipal);
            this.cargaFinal += new System.EventHandler(this.cAutomacaoBancaria_cargaFinal);
            this.Load += new System.EventHandler(this.cAutomacaoBancaria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTarefas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAutomacaoBancariaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCompetenciaEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonPeriodico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rHTarefaDetalheBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAutomacaoBancaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlAutomacao)).EndInit();
            this.TabControlAutomacao.ResumeLayout(false);
            this.TabTarefas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataCorte.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataCorte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVivos.Properties)).EndInit();
            this.TabPageRTF.ResumeLayout(false);
            this.TabPageRTF.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.ToolStripRTF.ResumeLayout(false);
            this.ToolStripRTF.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtTipoArquivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtArquivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.TabPageGPS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridGPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNSSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewGPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCODCON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.TabPagePIS.ResumeLayout(false);
            this.tableLayoutPanelPIS.ResumeLayout(false);
            this.tableLayoutPanelPIS.PerformLayout();
            this.ToolStripDados.ResumeLayout(false);
            this.ToolStripDados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPIS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pISBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewPIS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox7)).EndInit();
            this.TabRecibos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridRecibos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recibosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.TabRetornoRec.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRecSalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRecSal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChNR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinATE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinDE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rHTarefaSubdetalheBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.remessasPendentesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEMESSASDPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpProtocolo1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl TabControlAutomacao;
        private DevExpress.XtraTab.XtraTabPage TabPageRTF;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.ButtonEdit edtArquivo;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.RichTextBox ArquivoRTF;
        private System.Windows.Forms.ToolStrip ToolStripRTF;
        private System.Windows.Forms.ToolStripButton btnMaisZoom;
        private System.Windows.Forms.ToolStripTextBox edtZoomRTF;
        private System.Windows.Forms.ToolStripButton btnMenosZoom;
        private System.Windows.Forms.ToolStripLabel labZoom;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private DevExpress.XtraEditors.TextEdit edtTipoArquivo;
        private dAutomacaoBancaria dAutomacaoBancaria;
        private System.Windows.Forms.ToolStripButton btnIniciarExtracao;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private System.Windows.Forms.ToolStripButton btnCancelarExtracao;
        private System.Windows.Forms.ToolStripProgressBar pgbExtracao;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private DP.Automacao_Bancaria.dAutomacaoBancariaTableAdapters.REMESSASDPTableAdapter rEMESSASDPTableAdapter;
        private System.Windows.Forms.BindingSource rEMESSASDPBindingSource;
        private DevExpress.XtraTab.XtraTabPage TabPageGPS;
        private DevExpress.XtraGrid.GridControl gridGPS;
        private DevExpress.XtraGrid.Views.Grid.GridView ViewGPS;
        private System.Windows.Forms.BindingSource iNSSBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo1GPS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo3GPS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo4GPS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo5GPS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo6GPS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo9GPS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo10GPS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo11GPS;
        private DevExpress.XtraTab.XtraTabPage TabPagePIS;
        private DevExpress.XtraGrid.GridControl gridPIS;
        private DevExpress.XtraGrid.Views.Grid.GridView ViewPIS;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPIS;
        private System.Windows.Forms.ToolStrip ToolStripDados;
        private System.Windows.Forms.ToolStripButton btnRemessaPIS;
        private System.Windows.Forms.BindingSource pISBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo1PIS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo3PIS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo4PIS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo5PIS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo6PIS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo7PIS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo8PIS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo9PIS;
        private DevExpress.XtraGrid.Columns.GridColumn colCampo10PIS;
        private DP.Automacao_Bancaria.dAutomacaoBancariaTableAdapters.DETREMESSADPTableAdapter detremessadpTableAdapter1;
        private System.Windows.Forms.BindingSource remessasPendentesBindingSource;
        private DP.Automacao_Bancaria.dAutomacaoBancariaTableAdapters.RemessasPendentesTableAdapter remessasPendentesTableAdapter;
        private DevExpress.XtraTab.XtraTabPage TabRecibos;
        private DevExpress.XtraGrid.GridControl gridRecibos;
        private System.Windows.Forms.BindingSource recibosBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome2;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLNome;
        private DevExpress.XtraGrid.Columns.GridColumn colQtot1;
        private DevExpress.XtraGrid.Columns.GridColumn colQtot2;
        private DevExpress.XtraGrid.Columns.GridColumn colQtot3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private dllImpresso.dImpProtocolo dImpProtocolo1;
        private DevExpress.XtraGrid.Columns.GridColumn colOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repOK;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraTab.XtraTabPage TabRetornoRec;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.BindingSource dRecSalBindingSource;
        private DP.impressos.dRecSal dRecSal;
        private DevExpress.XtraGrid.Columns.GridColumn colHOL_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colHOLNome;
        private DevExpress.XtraGrid.Columns.GridColumn colHOLRetorno;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBanco;
        private DevExpress.XtraGrid.Columns.GridColumn colAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colConta;
        private DevExpress.XtraGrid.Columns.GridColumn colHOL;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SpinEdit spinATE;
        private DevExpress.XtraEditors.SpinEdit spinDE;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol3;
        private DevExpress.XtraGrid.Columns.GridColumn colHOLQ0;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colHOTTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colHOTDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colAguardo;
        private DevExpress.XtraEditors.CheckEdit ChNR;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoPag;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoPag1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraGrid.Columns.GridColumn colErro;
        private DevExpress.XtraGrid.Columns.GridColumn colErro1;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCondominio;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpCODCON;
        private DevExpress.XtraGrid.Columns.GridColumn colNEON;
        private DevExpress.XtraGrid.Columns.GridColumn colPrevisto;
        private DevExpress.XtraGrid.Columns.GridColumn colSEFIP;
        private DevExpress.XtraGrid.Columns.GridColumn colErroValor;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraGrid.Columns.GridColumn colCodComunicacao;
        private DevExpress.XtraGrid.Columns.GridColumn colCodComunicacao1;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private System.Windows.Forms.BindingSource rHTarefaDetalheBindingSource;
        private System.Windows.Forms.BindingSource rHTarefaSubdetalheBindingSource;
        private DevExpress.XtraTab.XtraTabPage TabTarefas;
        private DevExpress.XtraGrid.GridControl gridControlTarefas;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colRHT;
        private DevExpress.XtraGrid.Columns.GridColumn colRHTI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colRHTTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colRHTCompetencia;
        private DevExpress.XtraGrid.Columns.GridColumn colRHTDataI;
        private DevExpress.XtraGrid.Columns.GridColumn colRHTDataCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colRHTDataCheque;
        private DevExpress.XtraGrid.Columns.GridColumn colRHTArquivo;
        private DevExpress.XtraGrid.Columns.GridColumn colRHT_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colRHTStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpUSU;
        private System.Windows.Forms.BindingSource uSUariosBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox5;
        private System.Windows.Forms.Timer timerAviso;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private CompontesBasicos.RepositoryItemCompetenciaEdit repositoryItemCompetenciaEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP3;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox7;
        private DevExpress.XtraGrid.Columns.GridColumn colCON1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP4;
        private DevExpress.XtraGrid.Columns.GridColumn colHOT;
        private DevExpress.XtraGrid.Columns.GridColumn colHOLDuasvias;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoFolha1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoFolha2;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol4;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonDelete;
        private DevExpress.XtraGrid.Columns.GridColumn colNCondominios;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP5;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP6;
        private DevExpress.XtraEditors.CheckEdit checkEditVivos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome3;
        private DevExpress.XtraGrid.Columns.GridColumn colTotal1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoFolha11;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCNPJ1;
        private System.Windows.Forms.BindingSource dAutomacaoBancariaBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colQTD1;
        private DevExpress.XtraGrid.Columns.GridColumn colRHD_NOA1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnNota;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonNota;
        private DevExpress.XtraGrid.Columns.GridColumn colRHD;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PGF;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPeriodico;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonPeriodico;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colRHSValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.DateEdit dataCorte;
    }
}
