using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DP.Arquivos_Remessa
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cParametrosGeracaoArquivo : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cParametrosGeracaoArquivo()
        {
            InitializeComponent();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (this.Parent is Form)
                (this.Parent as Form).DialogResult = DialogResult.Yes;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if (this.Parent is Form)
                (this.Parent as Form).DialogResult = DialogResult.Cancel;
        }

        private void chkCreditoConta_EditValueChanged(object sender, EventArgs e)
        {
            dteCreditoConta.Enabled = chkCreditoConta.Checked;
        }

        

        private void cParametrosGeracaoArquivo_Load(object sender, EventArgs e)
        {
            
            dteCreditoConta.DateTime = DateTime.Now;
        }
    }
}

