namespace DP.Arquivos_Remessa
{
    partial class fParametrosGeracaoArquivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fParametrosGeracaoArquivo));
            this.cParametrosGeracaoArquivo1 = new DP.Arquivos_Remessa.cParametrosGeracaoArquivo();
            this.SuspendLayout();
            // 
            // cParametrosGeracaoArquivo1
            // 
            this.cParametrosGeracaoArquivo1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
           
            this.cParametrosGeracaoArquivo1.Location = new System.Drawing.Point(0, 0);
            this.cParametrosGeracaoArquivo1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cParametrosGeracaoArquivo1.Name = "cParametrosGeracaoArquivo1";
            this.cParametrosGeracaoArquivo1.Size = new System.Drawing.Size(224, 204);
            this.cParametrosGeracaoArquivo1.TabIndex = 0;
            this.cParametrosGeracaoArquivo1.Titulo = null;
            // 
            // fParametrosGeracaoArquivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 203);
            this.ControlBox = false;
            this.Controls.Add(this.cParametrosGeracaoArquivo1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fParametrosGeracaoArquivo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Parāmetros - Arquivo de Remessa";
            this.ResumeLayout(false);

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public cParametrosGeracaoArquivo cParametrosGeracaoArquivo1;

    }
}