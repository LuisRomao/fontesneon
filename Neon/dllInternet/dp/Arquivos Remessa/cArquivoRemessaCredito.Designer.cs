namespace DP.Arquivos_Remessa
{
    partial class cArquivoRemessaCredito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cArquivoRemessaCredito));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.FileTreeList = new DevExpress.XtraTreeList.TreeList();
            this.colArquivo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTamanho = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDataCriacao = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.ImageList = new System.Windows.Forms.ImageList(this.components);
            this.TabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.RichTextFile = new System.Windows.Forms.RichTextBox();
            this.TabPageDados = new DevExpress.XtraTab.XtraTabPage();
            this.grdDados = new DevExpress.XtraGrid.GridControl();
            this.grvDados = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBanco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContaCorrente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCPF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colTipoCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tspArquivoRemessa = new System.Windows.Forms.ToolStrip();
            this.btnAtualizar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExtrairDados = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnImprimir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnArquivoRemessa = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.sepCancelar = new System.Windows.Forms.ToolStripSeparator();
            this.dArquivoRemessaCredito = new DP.Arquivos_Remessa.dArquivoRemessaCredito();
            this.cONDOMINIOSTableAdapter = new DP.Arquivos_Remessa.dArquivoRemessaCreditoTableAdapters.CONDOMINIOSTableAdapter();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FileTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.TabPageDados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            this.tspArquivoRemessa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dArquivoRemessaCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "Folha";
            this.BindingSource_F.DataSource = this.dArquivoRemessaCredito;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
        
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.FileTreeList);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(682, 131);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = ":: Selecione o arquivo gerado pelo programa da folha ::";
            // 
            // FileTreeList
            // 
            this.FileTreeList.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.FileTreeList.Appearance.Empty.Options.UseBackColor = true;
            this.FileTreeList.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.EvenRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.EvenRow.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.EvenRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.FileTreeList.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.FocusedCell.Options.UseBackColor = true;
            this.FileTreeList.Appearance.FocusedCell.Options.UseForeColor = true;
            this.FileTreeList.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.FileTreeList.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.FileTreeList.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.FileTreeList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.FocusedRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.FooterPanel.Options.UseBackColor = true;
            this.FileTreeList.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.FooterPanel.Options.UseForeColor = true;
            this.FileTreeList.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.GroupButton.Options.UseBackColor = true;
            this.FileTreeList.Appearance.GroupButton.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.FileTreeList.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.FileTreeList.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.GroupFooter.Options.UseBackColor = true;
            this.FileTreeList.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.GroupFooter.Options.UseForeColor = true;
            this.FileTreeList.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.FileTreeList.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.FileTreeList.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.FileTreeList.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.FileTreeList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.FileTreeList.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.FileTreeList.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.HorzLine.Options.UseBackColor = true;
            this.FileTreeList.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.OddRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.OddRow.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.OddRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.FileTreeList.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.FileTreeList.Appearance.Preview.Options.UseFont = true;
            this.FileTreeList.Appearance.Preview.Options.UseForeColor = true;
            this.FileTreeList.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.Row.Options.UseBackColor = true;
            this.FileTreeList.Appearance.Row.Options.UseForeColor = true;
            this.FileTreeList.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.FileTreeList.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.FileTreeList.Appearance.SelectedRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.SelectedRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.FileTreeList.Appearance.TreeLine.Options.UseBackColor = true;
            this.FileTreeList.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.VertLine.Options.UseBackColor = true;
            this.FileTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colArquivo,
            this.colTamanho,
            this.colDataCriacao});
            this.FileTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileTreeList.Location = new System.Drawing.Point(2, 24);
            this.FileTreeList.LookAndFeel.SkinName = "The Asphalt World";
            this.FileTreeList.LookAndFeel.UseDefaultLookAndFeel = false;
            this.FileTreeList.Name = "FileTreeList";
            this.FileTreeList.OptionsBehavior.Editable = false;
            this.FileTreeList.OptionsBehavior.ImmediateEditor = false;
            this.FileTreeList.OptionsMenu.EnableFooterMenu = false;
            this.FileTreeList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.FileTreeList.OptionsSelection.UseIndicatorForSelection = true;
            this.FileTreeList.OptionsView.EnableAppearanceEvenRow = true;
            this.FileTreeList.OptionsView.EnableAppearanceOddRow = true;
            this.FileTreeList.OptionsView.ShowHorzLines = false;
            this.FileTreeList.OptionsView.ShowRoot = false;
            this.FileTreeList.SelectImageList = this.ImageList;
            this.FileTreeList.Size = new System.Drawing.Size(678, 105);
            this.FileTreeList.TabIndex = 0;
            this.FileTreeList.GetSelectImage += new DevExpress.XtraTreeList.GetSelectImageEventHandler(this.treeList1_GetSelectImage);
            // 
            // colArquivo
            // 
            this.colArquivo.Caption = "Arquivo";
            this.colArquivo.FieldName = "Arquivo";
            this.colArquivo.MinWidth = 33;
            this.colArquivo.Name = "colArquivo";
            this.colArquivo.Visible = true;
            this.colArquivo.VisibleIndex = 0;
            this.colArquivo.Width = 352;
            // 
            // colTamanho
            // 
            this.colTamanho.Caption = "Tamanho";
            this.colTamanho.FieldName = "Tamanho";
            this.colTamanho.Name = "colTamanho";
            this.colTamanho.Visible = true;
            this.colTamanho.VisibleIndex = 1;
            this.colTamanho.Width = 110;
            // 
            // colDataCriacao
            // 
            this.colDataCriacao.Caption = "Data da Cria��o";
            this.colDataCriacao.FieldName = "Data Criacao";
            this.colDataCriacao.Name = "colDataCriacao";
            this.colDataCriacao.Visible = true;
            this.colDataCriacao.VisibleIndex = 2;
            this.colDataCriacao.Width = 110;
            // 
            // ImageList
            // 
            this.ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList.ImageStream")));
            this.ImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList.Images.SetKeyName(0, "rtf4.jpg");
            // 
            // TabControl
            // 
            this.TabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl.Location = new System.Drawing.Point(0, 135);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedTabPage = this.xtraTabPage1;
            this.TabControl.Size = new System.Drawing.Size(682, 448);
            this.TabControl.TabIndex = 1;
            this.TabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.TabPageDados});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.RichTextFile);
            this.xtraTabPage1.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.Image")));
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(676, 417);
            this.xtraTabPage1.Text = "1 - Arquivo RTF";
            // 
            // RichTextFile
            // 
            this.RichTextFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichTextFile.Location = new System.Drawing.Point(0, 0);
            this.RichTextFile.Name = "RichTextFile";
            this.RichTextFile.ReadOnly = true;
            this.RichTextFile.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            
            this.RichTextFile.Size = new System.Drawing.Size(676, 417);
            
            this.RichTextFile.TabIndex = 0;
            this.RichTextFile.Text = "";
            // 
            // TabPageDados
            // 
            this.TabPageDados.Controls.Add(this.grdDados);
            this.TabPageDados.Image = ((System.Drawing.Image)(resources.GetObject("TabPageDados.Image")));
            this.TabPageDados.Name = "TabPageDados";
            this.TabPageDados.PageVisible = false;
            this.TabPageDados.Size = new System.Drawing.Size(676, 417);
            this.TabPageDados.Text = "2 - Dados Extra�dos";
            // 
            // grdDados
            // 
            this.grdDados.DataSource = this.BindingSource_F;
            this.grdDados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDados.Location = new System.Drawing.Point(0, 0);
            this.grdDados.MainView = this.grvDados;
            this.grdDados.Name = "grdDados";
            this.grdDados.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1});
            this.grdDados.Size = new System.Drawing.Size(676, 417);
            this.grdDados.TabIndex = 0;
            this.grdDados.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDados});
            // 
            // grvDados
            // 
            this.grvDados.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.grvDados.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.grvDados.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.grvDados.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.grvDados.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvDados.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvDados.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.grvDados.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.grvDados.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.grvDados.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.grvDados.Appearance.Empty.Options.UseBackColor = true;
            this.grvDados.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.EvenRow.Options.UseBackColor = true;
            this.grvDados.Appearance.EvenRow.Options.UseBorderColor = true;
            this.grvDados.Appearance.EvenRow.Options.UseForeColor = true;
            this.grvDados.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.grvDados.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.grvDados.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.grvDados.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.grvDados.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.grvDados.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.FilterPanel.Options.UseBackColor = true;
            this.grvDados.Appearance.FilterPanel.Options.UseForeColor = true;
            this.grvDados.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.grvDados.Appearance.FixedLine.Options.UseBackColor = true;
            this.grvDados.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.grvDados.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grvDados.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grvDados.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.grvDados.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.grvDados.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grvDados.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grvDados.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.grvDados.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grvDados.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.FooterPanel.Options.UseBackColor = true;
            this.grvDados.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.grvDados.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvDados.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.GroupButton.Options.UseBackColor = true;
            this.grvDados.Appearance.GroupButton.Options.UseBorderColor = true;
            this.grvDados.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvDados.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvDados.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.GroupFooter.Options.UseBackColor = true;
            this.grvDados.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.grvDados.Appearance.GroupFooter.Options.UseForeColor = true;
            this.grvDados.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.grvDados.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.GroupPanel.Options.UseBackColor = true;
            this.grvDados.Appearance.GroupPanel.Options.UseForeColor = true;
            this.grvDados.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvDados.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.grvDados.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.GroupRow.Options.UseBackColor = true;
            this.grvDados.Appearance.GroupRow.Options.UseBorderColor = true;
            this.grvDados.Appearance.GroupRow.Options.UseForeColor = true;
            this.grvDados.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.grvDados.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.grvDados.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.grvDados.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.grvDados.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.grvDados.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.grvDados.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.grvDados.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.grvDados.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.grvDados.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.grvDados.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.HorzLine.Options.UseBackColor = true;
            this.grvDados.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.OddRow.Options.UseBackColor = true;
            this.grvDados.Appearance.OddRow.Options.UseBorderColor = true;
            this.grvDados.Appearance.OddRow.Options.UseForeColor = true;
            this.grvDados.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.grvDados.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.grvDados.Appearance.Preview.Options.UseFont = true;
            this.grvDados.Appearance.Preview.Options.UseForeColor = true;
            this.grvDados.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.grvDados.Appearance.Row.Options.UseBackColor = true;
            this.grvDados.Appearance.Row.Options.UseForeColor = true;
            this.grvDados.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.grvDados.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.grvDados.Appearance.RowSeparator.Options.UseBackColor = true;
            this.grvDados.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.grvDados.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grvDados.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grvDados.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grvDados.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.grvDados.Appearance.TopNewRow.Options.UseBackColor = true;
            this.grvDados.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.grvDados.Appearance.VertLine.Options.UseBackColor = true;
            this.grvDados.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCondominio,
            this.colBanco,
            this.colRegistro,
            this.colNome,
            this.colContaCorrente,
            this.colCPF,
            this.colValor,
            this.colTipoCredito});
            this.grvDados.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grvDados.GridControl = this.grdDados;
            this.grvDados.GroupCount = 3;
            this.grvDados.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", null, "")});
            this.grvDados.Name = "grvDados";
            this.grvDados.OptionsBehavior.AllowIncrementalSearch = true;
            this.grvDados.OptionsPrint.AutoWidth = false;
            this.grvDados.OptionsPrint.ExpandAllGroups = false;
            this.grvDados.OptionsView.EnableAppearanceEvenRow = true;
            this.grvDados.OptionsView.EnableAppearanceOddRow = true;
            this.grvDados.PaintStyleName = "Skin";
            this.grvDados.PreviewFieldName = "BancoDeb";
            this.grvDados.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCondominio, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBanco, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTipoCredito, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.grvDados.RowCountChanged += new System.EventHandler(this.grvDados_RowCountChanged);
            // 
            // colCondominio
            // 
            this.colCondominio.Caption = "Condom�nio";
            this.colCondominio.FieldName = "Condominio";
            this.colCondominio.Name = "colCondominio";
            this.colCondominio.OptionsColumn.AllowEdit = false;
            this.colCondominio.OptionsColumn.ReadOnly = true;
            this.colCondominio.Visible = true;
            this.colCondominio.VisibleIndex = 0;
            this.colCondominio.Width = 77;
            // 
            // colBanco
            // 
            this.colBanco.Caption = "Banco";
            this.colBanco.FieldName = "Banco";
            this.colBanco.Name = "colBanco";
            this.colBanco.OptionsColumn.AllowEdit = false;
            this.colBanco.OptionsColumn.ReadOnly = true;
            this.colBanco.Visible = true;
            this.colBanco.VisibleIndex = 0;
            this.colBanco.Width = 77;
            // 
            // colRegistro
            // 
            this.colRegistro.Caption = "Registro";
            this.colRegistro.FieldName = "Registro";
            this.colRegistro.Name = "colRegistro";
            this.colRegistro.OptionsColumn.AllowEdit = false;
            this.colRegistro.OptionsColumn.ReadOnly = true;
            this.colRegistro.Visible = true;
            this.colRegistro.VisibleIndex = 0;
            this.colRegistro.Width = 96;
            // 
            // colNome
            // 
            this.colNome.Caption = "Nome";
            this.colNome.FieldName = "Nome";
            this.colNome.Name = "colNome";
            this.colNome.OptionsColumn.AllowEdit = false;
            this.colNome.OptionsColumn.ReadOnly = true;
            this.colNome.Visible = true;
            this.colNome.VisibleIndex = 1;
            // 
            // colContaCorrente
            // 
            this.colContaCorrente.Caption = "Conta Corrente";
            this.colContaCorrente.FieldName = "ContaCorrente";
            this.colContaCorrente.Name = "colContaCorrente";
            this.colContaCorrente.OptionsColumn.AllowEdit = false;
            this.colContaCorrente.OptionsColumn.ReadOnly = true;
            this.colContaCorrente.Visible = true;
            this.colContaCorrente.VisibleIndex = 2;
            // 
            // colCPF
            // 
            this.colCPF.Caption = "CPF";
            this.colCPF.FieldName = "CPF";
            this.colCPF.Name = "colCPF";
            this.colCPF.OptionsColumn.AllowEdit = false;
            this.colCPF.OptionsColumn.ReadOnly = true;
            this.colCPF.Visible = true;
            this.colCPF.VisibleIndex = 3;
            // 
            // colValor
            // 
            this.colValor.Caption = "Valor";
            this.colValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValor.FieldName = "Valor";
            this.colValor.Name = "colValor";
            this.colValor.OptionsColumn.AllowIncrementalSearch = false;
            this.colValor.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colValor.Visible = true;
            this.colValor.VisibleIndex = 4;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.DisplayFormat.FormatString = "N2";
            this.repositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colTipoCredito
            // 
            this.colTipoCredito.Caption = "Tipo";
            this.colTipoCredito.FieldName = "TipoCredito";
            this.colTipoCredito.Name = "colTipoCredito";
            this.colTipoCredito.OptionsColumn.AllowEdit = false;
            this.colTipoCredito.OptionsColumn.ReadOnly = true;
            this.colTipoCredito.Visible = true;
            this.colTipoCredito.VisibleIndex = 0;
            // 
            // tspArquivoRemessa
            // 
            this.tspArquivoRemessa.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tspArquivoRemessa.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAtualizar,
            this.toolStripSeparator2,
            this.btnExtrairDados,
            this.toolStripSeparator3,
            this.btnImprimir,
            this.toolStripSeparator1,
            this.btnArquivoRemessa,
            this.btnCancelar,
            this.progressBar,
            this.sepCancelar});
            this.tspArquivoRemessa.Location = new System.Drawing.Point(0, 577);
            this.tspArquivoRemessa.Name = "tspArquivoRemessa";
            this.tspArquivoRemessa.Size = new System.Drawing.Size(682, 27);
            this.tspArquivoRemessa.TabIndex = 2;
            this.tspArquivoRemessa.Text = "toolStrip1";
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(140, 24);
            this.btnAtualizar.Text = "Procurar Arquivo";
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // btnExtrairDados
            // 
            this.btnExtrairDados.Image = ((System.Drawing.Image)(resources.GetObject("btnExtrairDados.Image")));
            this.btnExtrairDados.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExtrairDados.Name = "btnExtrairDados";
            this.btnExtrairDados.Size = new System.Drawing.Size(118, 24);
            this.btnExtrairDados.Text = "Extrair Dados";
            this.btnExtrairDados.Click += new System.EventHandler(this.btnExtrairDados_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Enabled = false;
            this.btnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btnImprimir.Image")));
            this.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(86, 24);
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // btnArquivoRemessa
            // 
            this.btnArquivoRemessa.Enabled = false;
            this.btnArquivoRemessa.Image = ((System.Drawing.Image)(resources.GetObject("btnArquivoRemessa.Image")));
            this.btnArquivoRemessa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnArquivoRemessa.Name = "btnArquivoRemessa";
            this.btnArquivoRemessa.Size = new System.Drawing.Size(183, 24);
            this.btnArquivoRemessa.Text = "Gerar Arquivo Remessa";
            this.btnArquivoRemessa.Click += new System.EventHandler(this.btnArquivoRemessa_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(23, 20);
            this.btnCancelar.Text = "toolStripButton1";
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // progressBar
            // 
            this.progressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 22);
            this.progressBar.Step = 1;
            this.progressBar.Value = 10;
            this.progressBar.Visible = false;
            // 
            // sepCancelar
            // 
            this.sepCancelar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.sepCancelar.Name = "sepCancelar";
            this.sepCancelar.Size = new System.Drawing.Size(6, 27);
            this.sepCancelar.Visible = false;
            // 
            // dArquivoRemessaCredito
            // 
            this.dArquivoRemessaCredito.DataSetName = "dArquivoRemessaCredito";
            this.dArquivoRemessaCredito.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            this.cONDOMINIOSTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("cONDOMINIOSTableAdapter.GetNovosDados")));
            this.cONDOMINIOSTableAdapter.TabelaDataTable = null;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dArquivoRemessaCredito;
            // 
            // cArquivoRemessaCredito
            // 
            this.Autofill = false;
            this.Controls.Add(this.tspArquivoRemessa);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.groupControl1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.Name = "cArquivoRemessaCredito";
            this.Size = new System.Drawing.Size(682, 604);
            this.Load += new System.EventHandler(this.cArquivoRemessaCredito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FileTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.TabPageDados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            this.tspArquivoRemessa.ResumeLayout(false);
            this.tspArquivoRemessa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dArquivoRemessaCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraTreeList.TreeList FileTreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colArquivo;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTamanho;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDataCriacao;
        private System.Windows.Forms.ImageList ImageList;
        private DevExpress.XtraTab.XtraTabControl TabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.RichTextBox RichTextFile;
        private DevExpress.XtraTab.XtraTabPage TabPageDados;
        private DevExpress.XtraGrid.GridControl grdDados;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDados;
        private System.Windows.Forms.ToolStrip tspArquivoRemessa;
        private DevExpress.XtraGrid.Columns.GridColumn colCondominio;
        private DevExpress.XtraGrid.Columns.GridColumn colBanco;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colNome;
        private DevExpress.XtraGrid.Columns.GridColumn colContaCorrente;
        private DevExpress.XtraGrid.Columns.GridColumn colCPF;
        private DevExpress.XtraGrid.Columns.GridColumn colValor;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoCredito;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnArquivoRemessa;
        private System.Windows.Forms.ToolStripButton btnAtualizar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnExtrairDados;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private dArquivoRemessaCredito dArquivoRemessaCredito;
        private System.Windows.Forms.ToolStripButton btnImprimir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator sepCancelar;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private DP.Arquivos_Remessa.dArquivoRemessaCreditoTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
    }
}
