using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraTreeList.Nodes;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using DP.Virtual;

namespace DP.Arquivos_Remessa
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cArquivoRemessaCredito : CompontesBasicos.ComponenteBaseBindingSource
    {
        #region Variaveis
        //String _DirLeitura;// = @Properties.Settings.Default.DiretorioLeituraRTF;
        //String _DirGravacao;// = @Properties.Settings.Default.DiretorioGravacaoRemessa;
        Boolean _Cancelar = false;
        #endregion
        
        /// <summary>
        /// 
        /// </summary>
        public cArquivoRemessaCredito()
        {
            InitializeComponent();
        }

        /*
        private void CarregarArquivos()
        {
            if (!Directory.Exists(_DirLeitura))
            {
                try
                {
                    Directory.CreateDirectory(_DirLeitura);
                }
                catch (Exception Erro)
                {
                    MessageBox.Show("N�o foi poss�vel encontrar o diret�rio " + _DirLeitura + ", o qual deve conter os arquivos de cr�dito gerado pelo programa da folha.\r\n" +
                                    "Tente criar o diret�rio manualmente pelo windows explorer ou entre em contato com a Virtual Software para maiores esclarecimentos.\r\n\r\n" +
                                    "Foi encontrado o seguinte erro ao tentar criar o diret�rio: " + Erro.Message.ToUpper(),
                                    "Carregar Arquivo de Cr�dito", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            FileTreeList.ClearNodes();
            
            TreeListNode node;
            FileInfo fi;
            try
            {
                string[] root = Directory.GetFiles(_DirLeitura);
                foreach (string s in root)
                {
                    fi = new FileInfo(s);

                    if (fi.Extension.ToUpper().IndexOf("RTF") >= 0)
                    {
                        node = FileTreeList.AppendNode(new object[] { fi.Name, (fi.Length / 1024) + " KB", fi.CreationTime.ToShortDateString() + " " + fi.CreationTime.ToShortTimeString() }, null);
                        node.HasChildren = false;
                    }
                }
            }
            catch (Exception Erro)
            {
                FileTreeList.ClearNodes();
                MessageBox.Show("Ocorreu uma excess�o ao tentar procurar por arquivos novos./r/n/r/n" + Erro.Message.ToUpper(), "Procurar Arquivos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            btnExtrairDados.Enabled = (FileTreeList.Nodes.Count > 0);
        }
        */

        private void cArquivoRemessaCredito_Load(object sender, EventArgs e)
        {
            this.TabControl.SelectedTabPageIndex = 0;
            this.Dock = DockStyle.Fill;
            //CarregarArquivos();
        }

        private void treeList1_GetSelectImage(object sender, DevExpress.XtraTreeList.GetSelectImageEventArgs e)
        {
            e.NodeImageIndex = 0;
        }
        /*
        private void FileTreeList_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
            {
                String _File = _DirLeitura + e.Node.GetDisplayText("Arquivo");

                if (File.Exists(_File))
                {
                    RichTextFile.Clear();
                    RichTextFile.LoadFile(_File);
                }
            }
        }
        */

        private void btnExtrairDados_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                _Cancelar = false;

                HabilitarBotoes(false);

                this.dArquivoRemessaCredito.Folha.Clear();

                this.dArquivoRemessaCredito.CONDOMINIOS.Clear();

                this.cONDOMINIOSTableAdapter.Fill(this.dArquivoRemessaCredito.CONDOMINIOS);

                Int32 _iLinha = 0;

                String _CodConFolha = "";
                String _CodBanco = "";
                String _Banco = "";
                String _Agencia = "";
                String _dAgencia = "";

                String _sLinha = "";

                String _Registro = "";
                String _Nome = "";
                String _CC = "";
                String _CPF = "";
                String _Valor = "";
                String _TipoCredito = "";

                String _CodBancoDeb = "";
                String _BancoDeb = "";
                String _AgenciaDeb = "";
                String _dAgenciaDeb = "";
                String _CCDeb = "";
                String _dCCDeb = "";

                String _CONCodigo = "";
                String _CONNome = "";
                String _CONCnpj = "";

                this.grvDados.BeginDataUpdate();

                this.progressBar.Value = 0;
                this.progressBar.Maximum = RichTextFile.Lines.Length;

                for (_iLinha = 0; _iLinha < RichTextFile.Lines.Length; _iLinha++)
                {
                    if (_Cancelar == true)
                        return;
                    
                    this.progressBar.Value = _iLinha;

                    Application.DoEvents();

                    _sLinha = RichTextFile.Lines[_iLinha].ToUpper();

                    //Cabecalho
                    if (_sLinha.IndexOf("DATAMACE") >= 0)
                    {
                        //Condominio
                        _iLinha += 2;
                        _sLinha = RichTextFile.Lines[_iLinha].ToUpper();
                        _CodConFolha = _sLinha.Substring(49, 3);

                        //Banco 
                        _iLinha += 5;
                        _sLinha = RichTextFile.Lines[_iLinha].ToUpper();
                        _CodBanco = _sLinha.Substring(7, 5);
                        _Banco = _sLinha.Substring(13, _sLinha.IndexOf(" ", 13)).Trim();
                        _Agencia = _sLinha.Substring(53, 4);
                        _dAgencia = _sLinha.Substring(58, 1);

                        //Informacoes do Cadastro de Condominio
                        String _CONFiltro = "(CONCodigoFolha1 = " + _CodConFolha + ") or (CONCodigoFolha2 = " + _CodConFolha + ")";
                        DataRow[] _CONRow = this.dArquivoRemessaCredito.CONDOMINIOS.Select(_CONFiltro);

                        if (_CONRow.Length == 1)
                        {
                            _CodBancoDeb = _CONRow[0]["CON_BCO"].ToString();
                            _BancoDeb = _CONRow[0]["CON_BCO"].ToString() + " - " + _CONRow[0]["BCONome"].ToString() + "   AG: " + _CONRow[0]["CONAgencia"].ToString() + "-" + _CONRow[0]["CONDigitoAgencia"].ToString() + "   CC: " + _CONRow[0]["CONConta"].ToString() + "-" + _CONRow[0]["CONDigitoConta"].ToString();
                            _AgenciaDeb = _CONRow[0]["CONAgencia"].ToString();
                            _dAgenciaDeb = _CONRow[0]["CONDigitoAgencia"].ToString();
                            _CCDeb = _CONRow[0]["CONConta"].ToString();
                            _dCCDeb = _CONRow[0]["CONDigitoConta"].ToString();
                            _CONCodigo = _CONRow[0]["CONCodigo"].ToString();
                            _CONNome = _CONRow[0]["CONNome"].ToString();
                            _CONCnpj = _CONRow[0]["CONCnpj"].ToString();
                        }
                        else
                            if (_CONRow.Length > 1)
                            {
                                MessageBox.Show("Foi encontrado mais de um condom�nio com o C�DIGO DATAMACE " + _CodConFolha + ".", "C�digo Datamace Duplicado", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                                Cursor = Cursors.Default;

                                dArquivoRemessaCredito.Folha.Clear();

                                dArquivoRemessaCredito.CONDOMINIOS.Clear();

                                Application.DoEvents();

                                HabilitarBotoes(true);

                                return;
                            }
                            else
                            {
                                MessageBox.Show("N�o foi poss�vel localizar o cadastro do condom�nio " + _CodConFolha + ".\r\nPoss�velmente este c�digo n�o esteja atribuido a nenhum dos condom�nios existentes.", "Condom�nio n�o encontrado", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                                Cursor = Cursors.Default;

                                dArquivoRemessaCredito.Folha.Clear();

                                Application.DoEvents();

                                HabilitarBotoes(true);

                                return;
                            }
                    }

                    if (_sLinha.IndexOf("REGISTRO") >= 0)
                    {
                        _iLinha += 2;
                        _sLinha = RichTextFile.Lines[_iLinha].ToUpper();

                        if (_sLinha.Trim() != "")
                        {
                            while (_sLinha.Substring(0, 10) != "----------")
                            {
                                _Registro = _sLinha.Substring(0, 11).Trim();
                                _Nome = _sLinha.Substring(11, 32).Trim();
                                _CC = _sLinha.Substring(43, 13).Trim();
                                _CPF = _sLinha.Substring(56, 11).Trim();
                                _Valor = _sLinha.Substring(67, 12).Trim();

                                if (_CC.Trim() != "")
                                {
                                    if (Convert.ToInt32(_CodBanco) == 341)
                                        _TipoCredito = "CR�DITO EM C/C";
                                    else
                                    {
                                        if (_CC.IndexOf(".") > 0)
                                            _TipoCredito = "CR�DITO EM C/C";
                                        else
                                            _TipoCredito = "CART�O SAL�RIO";
                                    }
                                }
                                else
                                    _TipoCredito = "CHEQUE";


                                DataRowView _DRV = ((DataRowView)(this.BindingSource_F.AddNew()));

                                _DRV.BeginEdit();
                                _DRV["CodFolha"] = _CodConFolha;
                                _DRV["Condominio"] = _CodConFolha + " - " + _CONCodigo + " [ " + _BancoDeb + " ] ";
                                _DRV["CodBanco"] = _CodBanco;
                                _DRV["Banco"] = _CodBanco + " - " + _Banco + "    AG: " + _Agencia + "-" + _dAgencia;
                                _DRV["Agencia"] = _Agencia;
                                _DRV["DigitoAgencia"] = _dAgencia;
                                _DRV["Registro"] = _Registro;
                                _DRV["Nome"] = _Nome;
                                _DRV["CPF"] = _CPF;
                                _DRV["ContaCorrente"] = _CC;
                                _DRV["Valor"] = _Valor;
                                _DRV["TipoCredito"] = _TipoCredito;
                                _DRV["BancoDeb"] = _BancoDeb;
                                _DRV["ContaDeb"] = _CCDeb;
                                _DRV["DigitoContaDeb"] = _dCCDeb;
                                _DRV["AgenciaDeb"] = _AgenciaDeb;
                                _DRV["DigitoAgenciaDeb"] = _dAgenciaDeb;
                                _DRV["CONCodigo"] = _CONCodigo;
                                _DRV["CONNome"] = _CONNome;
                                _DRV["CONCNPJ"] = _CONCnpj;
                                _DRV.EndEdit();

                                _iLinha++;
                                _sLinha = RichTextFile.Lines[_iLinha].ToUpper();
                            }
                        }
                    }
                }

                this.grvDados.EndDataUpdate();

                HabilitarBotoes(true);

                this.btnImprimir.Enabled = (this.BindingSource_F.Count > 0);

                this.TabPageDados.PageVisible = (this.BindingSource_F.Count > 0);

                this.TabControl.SelectedTabPageIndex = (this.BindingSource_F.Count > 0 ? 1 : 0);

                Cursor = Cursors.Default;
                
                Application.DoEvents();

                MessageBox.Show("Extra��o de dados conclu�da.", "Extra��o de Dados", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro ao tentar extrair os dados do arquivo " + FileTreeList.FocusedNode.GetDisplayText("Arquivo") + ".\r\n\r\nErro: " + Erro.Message, "Extra��o de Dados", MessageBoxButtons.OK, MessageBoxIcon.Error);

                dArquivoRemessaCredito.Folha.Clear();

                HabilitarBotoes(true);

                Cursor = Cursors.Default;

                Application.DoEvents();
            }
        }

        private void HabilitarBotoes(Boolean Habilitar)
        {
            this.btnImprimir.Enabled = Habilitar;
            this.btnArquivoRemessa.Enabled = Habilitar;
            this.btnAtualizar.Enabled = Habilitar;
            this.btnExtrairDados.Enabled = Habilitar;

            this.sepCancelar.Visible = (!Habilitar);
            this.progressBar.Visible = (!Habilitar);
            this.btnCancelar.Visible = (!Habilitar);
        }
        
        /*
        private Boolean VerificaPathArquivo()
        {
            if (!Directory.Exists(_DirGravacao))
            {
                try
                {
                    Directory.CreateDirectory(_DirGravacao);

                    return true;
                }
                catch
                {
                    MessageBox.Show("A pasta para guardar os arquivos de remessa gerados n�o foi encontrada.\r\nVerifique se a pasta " + _DirGravacao + " existe e tente gerar novamente.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
                return true;
        }
        */

        private void btnArquivoRemessa_Click(object sender, EventArgs e)
        {
            this.Validate();

            using (fParametrosGeracaoArquivo _Form = new fParametrosGeracaoArquivo())
            {
                if (_Form.ShowDialog() == DialogResult.Yes)
                {
                    if (_Form.cParametrosGeracaoArquivo1.chkCreditoConta.Checked)
                    {
                        //RemessaCreditoContaBradesco(_Form.cParametrosGeracaoArquivo1.dteCreditoConta.DateTime);                        
                    }
                    
                    //if (_Form.cParametrosGeracaoArquivo1.chkCartaoSalario.Checked)
                      //  RemessaCartaoSalarioBradesco(_Form.cParametrosGeracaoArquivo1.dteCartaoSalario.DateTime);

                    MessageBox.Show("Gera��o do(s) arquivo(s) de remessa conclu�da.", "Remessa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        /*
        private void RemessaCreditoContaItau(DateTime DataParaDebito)
        {
            *** ARQUIVO ITAU - CREDITO EM CONTA ***
            if (!VerificaPathArquivo())
                return;

            String _FileCC = "";

            for (Int32 _IdArquivo = 1; _IdArquivo <= 10; _IdArquivo++)
            {
                if (_IdArquivo == 10)
                {
                    MessageBox.Show("O n�mero m�ximo(9) para gera��o de arquivo remessa j� foi atingindo.\r\nProcesso cancelado.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    _FileCC = _DirGravacao + "S" + _IdArquivo.ToString() + DateTime.Now.ToString("ddMMyy") + ".TXT";

                    if (File.Exists(_FileCC))
                    {
                        if (DialogResult.Yes == MessageBox.Show("O arquivo " + _FileCC + " j� existe. Deseja substituir o arquivo?", "Arquivo Remessa", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            try
                            {
                                File.Delete(_FileCC);
                            }
                            catch
                            {
                                MessageBox.Show("O arquivo " + _FileCC + " j� existe e n�o foi poss�vel sobrescreve-lo.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    else
                        break;
                }
            }

            BindingSource_F.RemoveFilter();
            BindingSource_F.RemoveSort();

            BindingSource_F.Filter = "CodBanco='00341' And TipoCredito='CR�DITO EM C/C'";
            BindingSource_F.Sort = "CONCODIGO ASC";

            if (BindingSource_F.Count > 0)
            {
                using (SqlConnection _connSQL = new SqlConnection(Properties.Settings.Default.SQLServerConnection))
                {
                    using (StreamWriter _DataCC = File.CreateText(_FileCC))
                    {
                        String _CODCON = "";
                        
                        for (Int32 _Pos = 0; _Pos < BindingSource_F.Count; _Pos++)
                        {
                            BindingSource_F.Position = _Pos;

                            if (_CODCON != ((DataRowView)(BindingSource_F.Current))["CONCodigo"].ToString())
                            {
                                //Header Arquivo
                                const String haCodBanco = "341";
                                const String haCodLote = "0000";
                                const String haHeader = "0";
                                String haCompl1 = FuncoesDP.CampoAlfaNumerico(" ", 6);
                                const String haLayoutArq = "080";
                                const String haTipoInscr = "2";

                                String haCNPJ = FuncoesDP.CampoNumerico(((DataRowView)(BindingSource_F.Current))["CONCnpj"].ToString(), 14);

                                if (haCNPJ.Trim() == "00000000000000")
                                {
                                    MessageBox.Show("O campo CNPJ do condom�nio " + ((DataRowView)(BindingSource_F.Current))["CONCodigo"].ToString() + " n�o foi informado ou est� incorreto.\r\nVerifique o valor do campo e tente gerar o arquivo novamente.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    _DataCC.Close();
                                    return;
                                }

                                String haCompl2 = FuncoesDP.CampoAlfaNumerico(" ", 20);
                                String haAgencia = FuncoesDP.CampoNumerico(((DataRowView)(BindingSource_F.Current))["AgenciaDeb"], 5);
                                String haCompl3 = " ";
                                String haConta = FuncoesDP.CampoNumerico(((DataRowView)(BindingSource_F.Current))["ContaDeb"], 12);
                                String haCompl4 = " ";
                                String haDigitoConta = FuncoesDP.CampoNumerico(((DataRowView)(BindingSource_F.Current))["DigitoContaDeb"], 1);
                                String haRazaoSocial = FuncoesDP.CampoAlfaNumerico(((DataRowView)(BindingSource_F.Current))["CONNome"].ToString(), 30);
                                String haNomeBanco = FuncoesDP.CampoAlfaNumerico("BANCO ITAU", 30);
                                String haCompl5 = FuncoesDP.CampoAlfaNumerico(" ", 10);
                                String haTipoArquivo = "1"; //1=Remessa | 2=Retorno
                                String haDataArquivo = DateTime.Now.ToString("ddMMyyyy");
                                String haHoraArquivo = DateTime.Now.ToString("HHmmss");
                                String haCompl6 = FuncoesDP.CampoNumerico("0", 9);
                                String haDensidade = FuncoesDP.CampoNumerico("0", 5);
                                String haCompl7 = FuncoesDP.CampoAlfaNumerico(" ", 69);

                                _DataCC.WriteLine(haCodBanco +
                                                  haCodLote +
                                                  haHeader +
                                                  haCompl1 +
                                                  haLayoutArq +
                                                  haTipoInscr +
                                                  haCNPJ +
                                                  haCompl2 +
                                                  haAgencia +
                                                  haCompl3 +
                                                  haConta +
                                                  haCompl4 +
                                                  haDigitoConta +
                                                  haRazaoSocial +
                                                  haNomeBanco +
                                                  haCompl5 +
                                                  haTipoArquivo +
                                                  haDataArquivo +
                                                  haHoraArquivo +
                                                  haCompl6 +
                                                  haDensidade +
                                                  haCompl7);

                                //Header Lote
                                //const String hlCodBanco = "347";
                                //const String hlCodLote = "0001";
                                //const String hlHeader = "1";
                                //const String hlTipoOper = "C";
                                //const String hlTipoPagto = "30"; //Salarios
                                //const String hlFormaPagto = "30"; //Credito CC no ITAU 
                                //const String hlLayout = "040"; //Credito CC no ITAU 
                                //String hlCompl1 = " ";
                                
                                /*** Pegar do Header do Arquivo
                                 * const String haTipoInscr = "2";
                                 * String haCNPJ = FuncoesDP.CampoNumerico(((DataRowView)(BindingSource_F.Current))["CONCnpj"].ToString(), 14);
                                 * String haCompl2 = FuncoesDP.CampoAlfaNumerico(" ", 20);
                                 * String haAgencia = FuncoesDP.CampoNumerico(((DataRowView)(BindingSource_F.Current))["AgenciaDeb"], 5);
                                 * String haCompl3 = " ";
                                 * String haConta = FuncoesDP.CampoNumerico(((DataRowView)(BindingSource_F.Current))["ContaDeb"], 12);
                                 * String haCompl4 = " ";
                                 * String haDigitoConta = FuncoesDP.CampoNumerico(((DataRowView)(BindingSource_F.Current))["DigitoContaDeb"], 1);
                                 * String haRazaoSocial = FuncoesDP.CampoAlfaNumerico(((DataRowView)(BindingSource_F.Current))["CONNome"].ToString(), 30);
                                 ***

                                String hlFinalidadeLote = FuncoesDP.CampoAlfaNumerico(" ", 30);
                                String hlHistoricoCC = FuncoesDP.CampoAlfaNumerico(" ", 10);
                            }
                        }
                    }
                }
            }
        }
        */
        /*
        private void RemessaCartaoSalarioBradesco(DateTime DataParaDebito)
        {
            /*** ARQUIVO BRADESCO - CARTAO SALARIO 
             *** Existe um outro componente chamado transferencia cartao salario 
             *** onde possui este mesmo codigo. As alteracoes nesta rotina deverao
             *** ser refletidas no outro componente tambem *** 

            if (!VerificaPathArquivo())
                return;

            String _FileCC = "";

            for (Int32 _IdArquivo = 1; _IdArquivo <= 10; _IdArquivo++)
            {
                if (_IdArquivo == 10)
                {
                    MessageBox.Show("O n�mero m�ximo(9) para gera��o de arquivo remessa j� foi atingindo.\r\nProcesso cancelado.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    _FileCC = _DirGravacao + "SA" + DateTime.Today.Day.ToString("00") + DateTime.Today.Month.ToString("00") + "0" + _IdArquivo.ToString() + ".REM";

                    if (File.Exists(_FileCC))
                    {
                        if (DialogResult.Yes == MessageBox.Show("O arquivo " + _FileCC + " j� existe. Deseja substituir o arquivo?", "Arquivo Remessa", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            try
                            {
                                File.Delete(_FileCC);
                            }
                            catch
                            {
                                MessageBox.Show("O arquivo " + _FileCC + " j� existe e n�o foi poss�vel sobrescreve-lo.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    else
                        break;
                }
            }

            this.BindingSource_F.RemoveFilter();
            this.BindingSource_F.RemoveSort();

            this.BindingSource_F.Filter = "FUNBanco='237-BRADESCO' And TipoCredito='CART�O SAL�RIO'";
            this.BindingSource_F.Sort = "CONCODIGO ASC";

            if (this.BindingSource_F.Count > 0)
            {
                using (StreamWriter _DataCC = new StreamWriter(_FileCC))
                {
                    String _CODCON = "";

                    Int32 _SeqRegistro = 0;

                    Decimal _VrTotal = 0;

                    for (Int32 _Pos = 0; _Pos < this.BindingSource_F.Count; _Pos++)
                    {
                        this.BindingSource_F.Position = _Pos;

                        DataRowView _drvCARTAO = ((DataRowView)(this.BindingSource_F.Current));

                        if (_CODCON != ((DataRowView)(this.BindingSource_F.Current))["CONCodigo"].ToString())
                        {
                            //Variavel de Controle
                            _CODCON = ((DataRowView)(this.BindingSource_F.Current))["CONCodigo"].ToString();

                            //Posicionando no registro do condominio
                            Int32 _PosicaoReg = this.cONDOMINIOSBindingSource.Find("CONCodigo", _CODCON);

                            if (_PosicaoReg < 0)
                            {
                                MessageBox.Show("N�o foi poss�vel achar o registro do condom�nio " + _CODCON + ".", "Registro do Condom�nio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else
                                this.cONDOMINIOSBindingSource.Position = _PosicaoReg;

                            DataRowView _drvCON = ((DataRowView)(this.cONDOMINIOSBindingSource.Current));

                            //Inicializando Total Valores
                            _VrTotal = 0;

                            /*** Registro HEADER ***
                            _SeqRegistro = 1;

                            String _IdHeader = "0";

                            //Codigo Fornecido pelo banco
                            Object _CodBancoCS = _drvCON["CONCodigoBancoCC"];

                            if (_CodBancoCS == null)
                            {
                                MessageBox.Show("O par�metro C�DIGO FORNECIDO PELO BANCO PARA CONTA SAL�RIO n�o foi informado para o condom�nio " + ((DataRowView)(BindingSource_F.Current))["CONCodigo"] + ".\r\nInforme o par�metro e tente gerar o arquivo novamente.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                _DataCC.Close();
                                return;
                            }
                            else
                                _CodBancoCS = FuncoesDP.CampoNumerico(_CodBancoCS, 8);

                            String _TipoInscricao = "2";

                            String _CNPJAux = FuncoesDP.CampoNumerico(_drvCON["CONCnpj"].ToString(), 15);

                            if (_CNPJAux.Trim() == "000000000000000")
                            {
                                MessageBox.Show("O campo CNPJ do condom�nio " + _CODCON + " n�o foi informado ou est� incorreto.\r\nVerifique o valor do campo e tente gerar o arquivo novamente.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                _DataCC.Close();
                                return;
                            }

                            String _CNPJBase = FuncoesDP.CampoNumerico(_CNPJAux.Substring(0, 9), 9);
                            String _CNPJFilial = FuncoesDP.CampoNumerico(_CNPJAux.Substring(9, 4), 4);
                            String _CNPJControle = FuncoesDP.CampoNumerico(_CNPJAux.Substring(13, 2), 2);
                            String _RazaoSocial = FuncoesDP.CampoAlfaNumerico(_drvCON["CONNome"].ToString(), 40);
                            String _IdServico = "70";
                            String _OrigemArquivo = "1";
                            String _RazaoCS = "00002";
                            String _ReservaH1 = FuncoesDP.CampoAlfaNumerico(" ", 5);
                            String _DataGravacao = DateTime.Now.ToString("yyyyMMdd");
                            String _HoraGravacao = DateTime.Now.ToString("hhmmss");
                            String _ReservaH2 = FuncoesDP.CampoAlfaNumerico(" ", 5);
                            String _ReservaH3 = FuncoesDP.CampoAlfaNumerico(" ", 3);
                            String _ReservaH4 = FuncoesDP.CampoAlfaNumerico(" ", 80);
                            String _ReservaH5 = FuncoesDP.CampoAlfaNumerico(" ", 88);
                            String _AgenciaDeb = FuncoesDP.CampoNumerico(_drvCON["CONAgencia"], 5);
                            String _ContaDeb = FuncoesDP.CampoNumerico(_drvCON["CONConta"], 7);
                            String _dContaDeb = FuncoesDP.CampoNumerico(_drvCON["CONDigitoConta"], 1);
                            String _ReservaH6 = FuncoesDP.CampoAlfaNumerico(" ", 194);
                            String _ReservaH7 = FuncoesDP.CampoAlfaNumerico(" ", 19);
                            String _SeqHeader = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                            _DataCC.WriteLine(
                                                _IdHeader +
                                                _CodBancoCS +
                                                _TipoInscricao +
                                                _CNPJBase +
                                                _CNPJFilial +
                                                _CNPJControle +
                                                _RazaoSocial +
                                                _IdServico +
                                                _OrigemArquivo +
                                                _RazaoCS +
                                                _ReservaH1 +
                                                _DataGravacao +
                                                _HoraGravacao +
                                                _ReservaH2 +
                                                _ReservaH3 +
                                                _ReservaH4 +
                                                _ReservaH5 +
                                                _AgenciaDeb +
                                                _ContaDeb +
                                                _dContaDeb +
                                                _ReservaH6 +
                                                _ReservaH7 +
                                                _SeqHeader
                                             );

                        }

                        /*** Registro Transa��o ***
                        /*** Primeiro registro de transacao � extraido da linha atual ( Mesma linha do HEADER)*
                        _SeqRegistro++;

                        String _IdTransacao = "1";
                        String _ReservaT1 = FuncoesDP.CampoAlfaNumerico(" ", 277);
                        String _ReservaT2 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT3 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT4 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT5 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT6 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _TipoMotivo = "0"; //Inclusao de Pagto
                        String _NomeFunc = FuncoesDP.CampoAlfaNumerico(_drvCARTAO["Nome"].ToString(), 26);

                        String _CPF = _drvCARTAO["CPF"].ToString();
                        _CPF = _CPF.Substring(0, 9) + "0000" + _CPF.Substring(9, 2);
                        String _ReservaT7 = FuncoesDP.CampoAlfaNumerico(_CPF, 32);

                        String _ReservaT8 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT9 = FuncoesDP.CampoAlfaNumerico(" ", 15);
                        String _NumPagto = FuncoesDP.CampoNumerico(1, 7);
                        String _DataPagto = DataParaDebito.ToString("yyyyMMdd");
                        String _NumCartao = FuncoesDP.CampoNumerico(Convert.ToInt32(_drvCARTAO["ContaCorrente"]), 7);
                        String _ValorCred = FuncoesDP.CampoNumerico(_drvCARTAO["Valor"], 15);
                        String _ReservaT10 = FuncoesDP.CampoNumerico(0, 15);
                        String _ReservaT11 = FuncoesDP.CampoAlfaNumerico(" ", 8);
                        String _ReservaT12 = FuncoesDP.CampoAlfaNumerico(" ", 8);
                        String _NumNovoCartao = FuncoesDP.CampoNumerico(0, 7);
                        String _ReservaT13 = FuncoesDP.CampoAlfaNumerico(" ", 36);
                        String _ReservaT14 = FuncoesDP.CampoAlfaNumerico(" ", 19);
                        String _SeqTransacao = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                        _DataCC.WriteLine(
                                            _IdTransacao +
                                            _ReservaT1 +
                                            _ReservaT2 +
                                            _ReservaT3 +
                                            _ReservaT4 +
                                            _ReservaT5 +
                                            _ReservaT6 +
                                            _TipoMotivo +
                                            _NomeFunc +
                                            _ReservaT7 +
                                            _ReservaT8 +
                                            _ReservaT9 +
                                            _NumPagto +
                                            _DataPagto +
                                            _NumCartao +
                                            _ValorCred +
                                            _ReservaT10 +
                                            _ReservaT11 +
                                            _ReservaT12 +
                                            _NumNovoCartao +
                                            _ReservaT13 +
                                            _ReservaT14 +
                                            _SeqTransacao
                                         );


                        //Acumulando Total da Transacao
                        _VrTotal += Convert.ToDecimal(((DataRowView)(BindingSource_F.Current))["Valor"]);

                        /*** Registro TRAILER ***
                        Boolean _PrintTrailer = false;

                        if (_Pos == (BindingSource_F.Count - 1))
                            _PrintTrailer = true;
                        else
                        {
                            this.BindingSource_F.Position++;

                            DataRowView _D = ((DataRowView)(BindingSource_F.Current));

                            if (_CODCON != _D["CONCodigo"].ToString())
                                _PrintTrailer = true;

                            this.BindingSource_F.Position--;
                        }

                        if (_PrintTrailer)
                        {
                            _SeqRegistro++;

                            String _IdTrailer = "9";
                            String _QtdeRegistro = FuncoesDP.CampoNumerico(_SeqRegistro, 6);
                            String _ValorTotal = FuncoesDP.CampoNumerico(_VrTotal, 17);
                            String _ReservaTr1 = FuncoesDP.CampoAlfaNumerico(" ", 451);
                            String _ReservaTr2 = FuncoesDP.CampoAlfaNumerico(" ", 19);
                            String _SeqTrailer = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                            _DataCC.WriteLine(
                                                _IdTrailer +
                                                _QtdeRegistro +
                                                _ValorTotal +
                                                _ReservaTr1 +
                                                _ReservaTr2 +
                                                _SeqTrailer
                                             );
                        }
                    }

                    _DataCC.Flush();
                    _DataCC.Close();

                }
            }

            this.BindingSource_F.RemoveFilter();
            this.BindingSource_F.RemoveSort();
        }
        */

        /*
        private void RemessaCreditoContaBradesco(DateTime DataParaDebito)
        {
            throw new Exception("CANCELADO: RemessaCreditoContaBradesco");
            /*** ARQUIVO BRADESCO - CREDITO EM C/C 

            if (!VerificaPathArquivo())
                return;

            String _FileCC = "";

            for (Int32 _IdArquivo = 1; _IdArquivo <= 10; _IdArquivo++)
            {
                _FileCC = _DirGravacao + "FP" + DateTime.Today.Day.ToString("00") + DateTime.Today.Month.ToString("00") + _IdArquivo.ToString() + ".REM";

                if (File.Exists(_FileCC))
                {
                    try
                    {
                        File.Delete(_FileCC);
                    }
                    catch
                    {
                        MessageBox.Show("O arquivo " + _FileCC + " j� existe e n�o foi poss�vel sobrescreve-lo.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                    break;

            }

            this.BindingSource_F.RemoveFilter();
            this.BindingSource_F.RemoveSort();

            this.BindingSource_F.Filter = "CodBanco='00237' And TipoCredito='CR�DITO EM C/C'";
            this.BindingSource_F.Sort = "CONCODIGO ASC";

            if (this.BindingSource_F.Count > 0)
            {
                using (StreamWriter _DataCC = File.CreateText(_FileCC))
                {
                    String _CODCON = "";

                    Decimal _VrTotal = 0;

                    Int32 _SeqRegistro = 0;

                    String _RazaoCC = "";

                    for (Int32 _Pos = 0; _Pos < this.BindingSource_F.Count; _Pos++)
                    {
                        this.BindingSource_F.Position = _Pos;

                        DataRowView _drvDados = ((DataRowView)(this.BindingSource_F.Current));

                        if (_CODCON != ((DataRowView)(BindingSource_F.Current))["CONCodigo"].ToString())
                        {
                            //Variavel de Controle
                            _CODCON = ((DataRowView)(BindingSource_F.Current))["CONCodigo"].ToString();

                            //Posicionando no registro do condominio
                            Int32 _PosicaoReg = this.cONDOMINIOSBindingSource.Find("CONCodigo", _CODCON);

                            if (_PosicaoReg < 0)
                            {
                                MessageBox.Show("N�o foi poss�vel achar o registro do condom�nio " + _CODCON + ".", "Registro do Condom�nio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else
                                this.cONDOMINIOSBindingSource.Position = _PosicaoReg;

                            DataRowView _drvCON = ((DataRowView)(this.cONDOMINIOSBindingSource.Current));

                            //Inicializando Total Valores
                            _VrTotal = 0;

                            /*** Registro HEADER ***

                            String _IdHeader = "0";
                            String _IdFita = "1";
                            String _IdFitaExtenso = FuncoesDP.CampoAlfaNumerico("REMESSA", 7);
                            String _IdServico = "03";
                            String _IdServicoExtenso = FuncoesDP.CampoAlfaNumerico("CREDITO C/C", 15);
                            String _AgenciaDeb = FuncoesDP.CampoNumerico(_drvCON["CONAgencia"], 5);
                            _RazaoCC = "00705";
                            String _ContaDeb = FuncoesDP.CampoNumerico(_drvCON["CONConta"], 7);
                            String _dContaDeb = FuncoesDP.CampoNumerico(_drvCON["CONDigitoConta"], 1);
                            String _IdCrec = " ";
                            String _ReservaH1 = " ";

                            //Codigo Fornecido pelo banco
                            Object _CodBancoCC = _drvCON["CONCodigoBancoCC"];

                            if (_CodBancoCC == null)
                            {
                                MessageBox.Show("O par�metro C�DIGO FORNECIDO PELO BANCO PARA CONTA SAL�RIO n�o foi informado para o condom�nio " + ((DataRowView)(BindingSource_F.Current))["CONCodigo"] + ".\r\nInforme o par�metro e tente gerar o arquivo novamente.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                _DataCC.Close();
                                return;
                            }
                            else
                                _CodBancoCC = FuncoesDP.CampoNumerico(_CodBancoCC, 5);

                            String _RazaoSocial = FuncoesDP.CampoAlfaNumerico(_drvCON["CONNome"].ToString(), 25);
                            const String _NumBancoCamara = "237";
                            String _NomeBanco = FuncoesDP.CampoAlfaNumerico("BRADESCO", 15);
                            String _DataFita = DateTime.Today.ToString("ddMMyyyy");
                            String _Densidade = FuncoesDP.CampoNumerico("01600", 5);
                            String _UnidDensidade = "BPI";
                            String _DataDebito = DataParaDebito.ToString("ddMMyyyy");
                            String _IDMoeda = " ";
                            String _IDSeculo = "N";
                            String _ReservaH2 = FuncoesDP.CampoAlfaNumerico(" ", 74);
                            String _SeqHeader = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                            _DataCC.WriteLine(
                                                _IdHeader +
                                                _IdFita +
                                                _IdFitaExtenso +
                                                _IdServico +
                                                _IdServicoExtenso +
                                                _AgenciaDeb +
                                                _RazaoCC +
                                                _ContaDeb +
                                                _dContaDeb +
                                                _IdCrec +
                                                _ReservaH1 +
                                                _CodBancoCC +
                                                _RazaoSocial +
                                                _NumBancoCamara +
                                                _NomeBanco +
                                                _DataFita +
                                                _Densidade +
                                                _UnidDensidade +
                                                _DataDebito +
                                                _IDMoeda +
                                                _IDSeculo +
                                                _ReservaH2 +
                                                _SeqHeader
                                             );
                        }

                        /*** Registro Transa��o ***
                        /*** Primeiro registro de transacao � extraido da linha atual ( Mesma linha do HEADER)*
                        _SeqRegistro++;

                        String _IdTransacao = "1";
                        String _ReservaT1 = FuncoesDP.CampoAlfaNumerico(" ", 61);
                        String _AgenciaCred = FuncoesDP.CampoNumerico(_drvDados["Agencia"], 5);

                        //_RazaoCC - Ja tem o valor qdo executado rotina do header

                        String _CCAux = _drvDados["ContaCorrente"].ToString();
                        String _ContaCred = FuncoesDP.CampoNumerico(_CCAux.Substring(0, (_CCAux.Length - 1)), 7);
                        String _dContaCred = FuncoesDP.CampoNumerico(_CCAux.Substring((_CCAux.Length - 1), 1), 1);
                        String _ReservaT2 = FuncoesDP.CampoAlfaNumerico(" ", 2);
                        String _NomeFunc = FuncoesDP.CampoAlfaNumerico(_drvDados["Nome"].ToString(), 38);
                        String _RegistroFunc = FuncoesDP.CampoNumerico(_drvDados["Registro"], 6);
                        String _ValorCred = FuncoesDP.CampoNumerico(_drvDados["Valor"], 13);
                        const String _IdTipoServico = "298";
                        String _ReservaT3 = FuncoesDP.CampoAlfaNumerico(" ", 8);
                        String _ReservaT4 = FuncoesDP.CampoAlfaNumerico(" ", 44);
                        String _SeqTransacao = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                        _DataCC.WriteLine(
                                            _IdTransacao +
                                            _ReservaT1 +
                                            _AgenciaCred +
                                            _RazaoCC +
                                            _ContaCred +
                                            _dContaCred +
                                            _ReservaT2 +
                                            _NomeFunc +
                                            _RegistroFunc +
                                            _ValorCred +
                                            _IdTipoServico +
                                            _ReservaT3 +
                                            _ReservaT4 +
                                            _SeqTransacao
                                          );

                        //Acumulando Total da Transacao
                        _VrTotal += Convert.ToDecimal(_drvDados["Valor"]);

                        /*** Registro TRAILER ***
                        Boolean _PrintTrailer = false;

                        if (_Pos == (BindingSource_F.Count - 1))
                            _PrintTrailer = true;
                        else
                        {
                            this.BindingSource_F.Position++;

                            DataRowView _D = ((DataRowView)(BindingSource_F.Current));

                            if (_CODCON != _D["CONCodigo"].ToString())
                                _PrintTrailer = true;

                            this.BindingSource_F.Position--;
                        }

                        if (_PrintTrailer)
                        {
                            _SeqRegistro++;

                            const String _IdTrailer = "9";
                            String _VrTotalTransacao = FuncoesDP.CampoNumerico(_VrTotal, 13);
                            String _ReservaTL1 = FuncoesDP.CampoAlfaNumerico(" ", 180);
                            String _SeqTrailer = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                            _DataCC.WriteLine(
                                                _IdTrailer +
                                                _VrTotalTransacao +
                                                _ReservaTL1 +
                                                _SeqTrailer
                                             );
                        }
                    }

                    _DataCC.Flush();
                    _DataCC.Close();
                }

            }

            BindingSource_F.RemoveFilter();
            BindingSource_F.RemoveSort();
        }
        */
        

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            //CarregarArquivos();
        }

        private void grvDados_RowCountChanged(object sender, EventArgs e)
        {
            this.btnArquivoRemessa.Enabled = (this.grvDados.DataRowCount > 0);
            this.btnImprimir.Enabled = (this.grvDados.DataRowCount > 0);
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            this.grdDados.ShowPrintPreview();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            _Cancelar = true;
        }
    }
}