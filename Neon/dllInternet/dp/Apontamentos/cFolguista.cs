using System;
using System.Windows.Forms;

namespace DP.Apontamentos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cFolguista : Form
    {
        /// <summary>
        /// 
        /// </summary>
        public int Resposta;
        //public posEscala Escala;
        //public int Fase;

        //private DevExpress.XtraEditors.TimeEdit[,] Hs;
        //private SortedList botoes;

        /// <summary>
        /// 
        /// </summary>
        public cFolguista()
        {
            InitializeComponent();
            /*
            Hs = new DevExpress.XtraEditors.TimeEdit[,] { { H1, H2, H3, H4 }, { H1, H2, H3, H4 } };
            botoes = new SortedList();
            botoes.Add(posEscala.e8_1, e8_1);
            botoes.Add(posEscala.e8_2, e8_2);
            botoes.Add(posEscala.e8_3, e8_3);
            botoes.Add(posEscala.e8_4, e8_4);
            botoes.Add(posEscala.e8_5, e8_5);
            botoes.Add(posEscala.e8_6, e8_6);
            botoes.Add(posEscala.e8_7, e8_7);
            botoes.Add(posEscala.e8_8, e8_8);
            botoes.Add(posEscala.e5_1, e5_1);
            botoes.Add(posEscala.e5_2, e5_2);
            botoes.Add(posEscala.e5_3, e5_3);
            botoes.Add(posEscala.e5_4, e5_4);
            botoes.Add(posEscala.e5_5, e5_5);
            botoes.Add(posEscala.e2_1_6, e2_1_6);
            botoes.Add(posEscala.e2_2_6, e2_2_6);
            botoes.Add(posEscala.e2_1_18, e2_1_18);
            botoes.Add(posEscala.e2_2_18, e2_2_18);
            */ 
        }

        
        /*
        private void AjustaH(int linha,int[] horas) {
            for (int i = 0; i < 4; i++)
                if (horas[i] == -1)
                    Hs[linha,i].EditValue = null;
                else
                    Hs[linha,i].Time = DateTime.Today + new TimeSpan(horas[i], 0, 0);
        }*/

        private void Ok()
        {
            if (chManual.Checked)
            {
                BotaoOk.Enabled = true;                
            }
            else
                DialogResult = DialogResult.OK;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //Escala = (posEscala)botoes[botoes.IndexOfValue(sender)];
            //int[4] mod1 = { 6, 11, 12, 14 };
            /*
            switch (Escala)
            {
                case posEscala.e8_1:
                    //AjustaH(0 ,mod1);
                    //AjustaH(1, mod1);
                    //AjustaH(2, new int[] { 14, 11, 12, 14 });
                    //AjustaH(3, new int[] { 14, 11, 12, 14 });
                    Resposta = 1;
                    break;
                case posEscala.e8_2:
                    Resposta = 2;
                    break;
                case posEscala.e8_3:
                    Resposta = 2;
                    break;
                case posEscala.e8_4:
                    Resposta = 4;
                    break;
                case posEscala.e8_5:
                    Resposta = 5;
                    break;
                case posEscala.e8_6:
                    Resposta = 6;
                    break;
                case posEscala.e8_7:
                    break;
                case posEscala.e8_8:
                    break;
                case posEscala.e5_1:
                    break;
                case posEscala.e5_2:
                    break;
                case posEscala.e5_3:
                    break;
                case posEscala.e5_4:
                    break;
                case posEscala.e5_5:
                    break;
                case posEscala.e2_1_6:
                    break;
                case posEscala.e2_2_6:
                    break;
                case posEscala.e2_1_18:
                    break;
                case posEscala.e2_2_18:
                    break;
                default:
                    break;
            }*/
            Resposta = 1;
            Ok();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Resposta = 2;
            Ok();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Resposta = 3;
            Ok();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Resposta = 4;
            Ok();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            Resposta = 5;
            Ok();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            Resposta = 6;
            Ok();
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            Resposta = 7;
            Ok();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            Resposta = 8;
            Ok();
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            Resposta = 11;
            Ok();
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            Resposta = 12;
            Ok();
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            Resposta = 13;
            Ok();
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            Resposta = 14;
            Ok();
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            Resposta = 15;
            Ok();
        }

        private void simpleButton14_Click(object sender, EventArgs e)
        {
            Resposta = 21;
            Ok();
        }

        private void simpleButton15_Click(object sender, EventArgs e)
        {
            Resposta = 22;
            Ok();
        }

        private void simpleButton16_Click(object sender, EventArgs e)
        {
            Resposta = 31;
            Ok();
        }

        private void simpleButton17_Click(object sender, EventArgs e)
        {
            Resposta = 32;
            Ok();
        }

        private void BotaoOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// 
        /// </summary>
        public int[] mascara;

        /// <summary>
        /// 
        /// </summary>
        public int fase;

        /// <summary>
        /// 
        /// </summary>
        public int jornada=8;

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.SimpleButton Botao = (DevExpress.XtraEditors.SimpleButton)sender;
            Resposta = 0;
            if (Botao.Text.StartsWith("4x2"))
                mascara = new int[] { -1, -1, -1, -1, 0, 0 };
            if (Botao.Text.StartsWith("5x1"))
                mascara = new int[] {-1,-1,-1,-1,-1, 0};
            if (Botao.Text.StartsWith("6x1"))
                mascara = new int[] { -1, -1, -1, -1, -1, -1, 0 };
            if (Botao.Text.StartsWith("6x2"))
                mascara = new int[] { -1, -1, -1, -1, -1, -1, 0, 0 };
            if (Botao.Text.StartsWith("4x1"))
                mascara = new int[] { -1, -1, -1, -1, 0 };
            fase = int.Parse((string)Botao.Tag);
            DialogResult = DialogResult.OK;
        }

        private void simpleButton21_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.SimpleButton Botao = (DevExpress.XtraEditors.SimpleButton)sender;
            Resposta = 0;
            mascara = new int[] { 19, 0};
            jornada = 12;
            fase = int.Parse((string)Botao.Tag);
            DialogResult = DialogResult.OK;
        }

        private void simpleButton23_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.SimpleButton Botao = (DevExpress.XtraEditors.SimpleButton)sender;
            Resposta = 0;
            mascara = new int[] { 7, 0 };
            jornada = 12;
            fase = int.Parse((string)Botao.Tag);
            DialogResult = DialogResult.OK;
        }

        private void e2_1_18_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.SimpleButton Botao = (DevExpress.XtraEditors.SimpleButton)sender;
            Resposta = 0;
            mascara = new int[] { 18, 0 };
            jornada = 12;
            fase = int.Parse((string)Botao.Tag);
            DialogResult = DialogResult.OK;
        }

        private void e2_1_6_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.SimpleButton Botao = (DevExpress.XtraEditors.SimpleButton)sender;
            Resposta = 0;
            mascara = new int[] { 6, 0 };
            jornada = 12;
            fase = int.Parse((string)Botao.Tag);
            DialogResult = DialogResult.OK;
        }

        private void simpleButton24_Click(object sender, EventArgs e)
        {
            Resposta = -1;
            DialogResult = DialogResult.OK;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public enum posEscala 
    { 
        /// <summary>
        /// 
        /// </summary>
        e8_1,
        /// <summary>
        /// 
        /// </summary>
        e8_2,
        /// <summary>
        /// 
        /// </summary>
        e8_3,
        /// <summary>
        /// 
        /// </summary>
        e8_4,
        /// <summary>
        /// 
        /// </summary>
        e8_5,
        /// <summary>
        /// 
        /// </summary>
        e8_6,
        /// <summary>
        /// 
        /// </summary>
        e8_7,
        /// <summary>
        /// 
        /// </summary>
        e8_8,
        /// <summary>
        /// 
        /// </summary>
        e5_1,
        /// <summary>
        /// 
        /// </summary>
        e5_2,
        /// <summary>
        /// 
        /// </summary>
        e5_3,
        /// <summary>
        /// 
        /// </summary>
        e5_4,
        /// <summary>
        /// 
        /// </summary>
        e5_5,
        /// <summary>
        /// 
        /// </summary>
        e2_1_6,
        /// <summary>
        /// 
        /// </summary>
        e2_2_6,
        /// <summary>
        /// 
        /// </summary>
        e2_1_18,
        /// <summary>
        /// 
        /// </summary>
        e2_2_18
    }
}