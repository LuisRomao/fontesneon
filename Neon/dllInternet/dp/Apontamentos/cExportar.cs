using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DP.Apontamentos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cExportar : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public cExportar()
        {
            InitializeComponent();
            tMPApontamentoDTMBindingSource.DataSource = DP.impressos.dGravarDTM.dGravarDTMSt;
        }
    }
}

