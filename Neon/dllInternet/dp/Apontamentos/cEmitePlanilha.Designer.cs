namespace DP.Apontamentos
{
    partial class cEmitePlanilha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chSoplan = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.BTGrava = new DevExpress.XtraEditors.SimpleButton();
            this.BotCarregar = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.cBotaoImpBol3 = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.funcionariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHorario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCargo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPLP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLP_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPEtiqueta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPHorario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPCompetenciaAno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPCompetenciaMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPDataRetorno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPDataApontamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPApontamento_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLPI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcopiaPLP_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chSoplan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.funcionariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chSoplan);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.BTGrava);
            this.panelControl1.Controls.Add(this.BotCarregar);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1568, 66);
            this.panelControl1.TabIndex = 0;
            // 
            // chSoplan
            // 
            this.chSoplan.EditValue = true;
            this.chSoplan.Location = new System.Drawing.Point(86, 35);
            this.chSoplan.Name = "chSoplan";
            this.chSoplan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chSoplan.Properties.Appearance.Options.UseFont = true;
            this.chSoplan.Properties.Caption = "Somente planilhas";
            this.chSoplan.Size = new System.Drawing.Size(137, 19);
            this.chSoplan.TabIndex = 12;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(548, 5);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(146, 24);
            this.simpleButton3.TabIndex = 11;
            this.simpleButton3.Text = "Exporta";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // BTGrava
            // 
            this.BTGrava.Enabled = false;
            this.BTGrava.Location = new System.Drawing.Point(304, 6);
            this.BTGrava.Name = "BTGrava";
            this.BTGrava.Size = new System.Drawing.Size(92, 24);
            this.BTGrava.TabIndex = 10;
            this.BTGrava.Text = "Grava";
            this.BTGrava.Click += new System.EventHandler(this.simpleButton2_Click_1);
            // 
            // BotCarregar
            // 
            this.BotCarregar.Location = new System.Drawing.Point(5, 35);
            this.BotCarregar.Name = "BotCarregar";
            this.BotCarregar.Size = new System.Drawing.Size(75, 23);
            this.BotCarregar.TabIndex = 2;
            this.BotCarregar.Text = "Carregar";
            this.BotCarregar.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 13);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(88, 16);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Compet�ncia:";
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(99, 5);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 0;
            this.cCompet1.Titulo = null;
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // cBotaoImpBol3
            // 
            this.cBotaoImpBol3.ItemComprovante = false;
            this.cBotaoImpBol3.Location = new System.Drawing.Point(5, 42);
            this.cBotaoImpBol3.Name = "cBotaoImpBol3";
            this.cBotaoImpBol3.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol3.TabIndex = 8;
            this.cBotaoImpBol3.Titulo = "Imp Selecionados";
            this.cBotaoImpBol3.clicado += new System.EventHandler(this.cBotaoImpBol3_clicado);
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(5, 10);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol1.TabIndex = 9;
            this.cBotaoImpBol1.Titulo = "Imp Todos";
            this.cBotaoImpBol1.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Funcionarios";
            this.gridControl1.DataSource = this.funcionariosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1448, 507);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // funcionariosBindingSource
            // 
            this.funcionariosBindingSource.DataSource = typeof(DP.impressos.dPonto);
            this.funcionariosBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNome,
            this.colCondominio,
            this.colHorario,
            this.colRegistro,
            this.colCargo,
            this.colEMP});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colNome
            // 
            this.colNome.Caption = "Nome";
            this.colNome.FieldName = "Nome";
            this.colNome.Name = "colNome";
            this.colNome.Visible = true;
            this.colNome.VisibleIndex = 1;
            this.colNome.Width = 273;
            // 
            // colCondominio
            // 
            this.colCondominio.Caption = "Condom�nio";
            this.colCondominio.FieldName = "Condominio";
            this.colCondominio.Name = "colCondominio";
            this.colCondominio.Visible = true;
            this.colCondominio.VisibleIndex = 2;
            this.colCondominio.Width = 252;
            // 
            // colHorario
            // 
            this.colHorario.Caption = "Hor�rio";
            this.colHorario.FieldName = "Horario";
            this.colHorario.Name = "colHorario";
            this.colHorario.Visible = true;
            this.colHorario.VisibleIndex = 3;
            this.colHorario.Width = 157;
            // 
            // colRegistro
            // 
            this.colRegistro.Caption = "Registro";
            this.colRegistro.FieldName = "Registro";
            this.colRegistro.Name = "colRegistro";
            this.colRegistro.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Registro", "{0:n0}")});
            this.colRegistro.Visible = true;
            this.colRegistro.VisibleIndex = 0;
            this.colRegistro.Width = 112;
            // 
            // colCargo
            // 
            this.colCargo.Caption = "Cargo";
            this.colCargo.FieldName = "Cargo";
            this.colCargo.Name = "colCargo";
            this.colCargo.Visible = true;
            this.colCargo.VisibleIndex = 4;
            this.colCargo.Width = 175;
            // 
            // colEMP
            // 
            this.colEMP.FieldName = "EMP";
            this.colEMP.Name = "colEMP";
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "Feriados";
            this.gridControl2.DataSource = this.funcionariosBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.gridControl2.Location = new System.Drawing.Point(1448, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(114, 507);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colData});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.NewItemRowText = "Nova data";
            this.gridView2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colData
            // 
            this.colData.Caption = "Feriados";
            this.colData.FieldName = "Data";
            this.colData.Name = "colData";
            this.colData.Visible = true;
            this.colData.VisibleIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 66);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1568, 535);
            this.xtraTabControl1.TabIndex = 3;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.gridControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1562, 507);
            this.xtraTabPage1.Text = "Arquivo XML";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl3);
            this.xtraTabPage2.Controls.Add(this.panelControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1319, 705);
            this.xtraTabPage2.Text = "Gravado";
            // 
            // gridControl3
            // 
            this.gridControl3.DataMember = "PLanilhaPonto";
            this.gridControl3.DataSource = this.funcionariosBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 86);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(1319, 619);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            this.gridControl3.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            // 
            // gridView3
            // 
            this.gridView3.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView3.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView3.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView3.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView3.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView3.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView3.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView3.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView3.Appearance.Empty.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView3.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView3.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView3.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView3.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView3.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView3.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView3.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView3.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView3.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView3.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView3.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView3.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView3.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView3.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView3.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView3.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView3.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView3.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView3.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView3.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView3.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView3.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView3.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView3.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView3.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView3.Appearance.Preview.Options.UseBackColor = true;
            this.gridView3.Appearance.Preview.Options.UseFont = true;
            this.gridView3.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.Row.Options.UseBackColor = true;
            this.gridView3.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView3.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView3.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView3.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPLP,
            this.colPLP_CON,
            this.colPLPRegistro,
            this.colPLPNome,
            this.colPLPEtiqueta,
            this.colPLPHorario,
            this.colPLPCompetenciaAno,
            this.colPLPCompetenciaMes,
            this.colPLPDATAI,
            this.colPLPDataRetorno,
            this.colPLPDataApontamento,
            this.colPLPApontamento_USU,
            this.colPLPI_USU,
            this.colcopiaPLP_CON,
            this.colCidade});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.Editable = false;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.EnableAppearanceOddRow = true;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPLP, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView3_RowCellStyle);
            // 
            // colPLP
            // 
            this.colPLP.Caption = "C�digo";
            this.colPLP.FieldName = "PLP";
            this.colPLP.Name = "colPLP";
            this.colPLP.OptionsColumn.ReadOnly = true;
            this.colPLP.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PLP", "{0:n0}")});
            this.colPLP.Visible = true;
            this.colPLP.VisibleIndex = 1;
            this.colPLP.Width = 69;
            // 
            // colPLP_CON
            // 
            this.colPLP_CON.Caption = "CODCON";
            this.colPLP_CON.FieldName = "CONCodigo";
            this.colPLP_CON.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPLP_CON.Name = "colPLP_CON";
            this.colPLP_CON.Visible = true;
            this.colPLP_CON.VisibleIndex = 2;
            this.colPLP_CON.Width = 74;
            // 
            // colPLPRegistro
            // 
            this.colPLPRegistro.Caption = "Registro";
            this.colPLPRegistro.FieldName = "PLPRegistro";
            this.colPLPRegistro.Name = "colPLPRegistro";
            this.colPLPRegistro.Visible = true;
            this.colPLPRegistro.VisibleIndex = 4;
            this.colPLPRegistro.Width = 93;
            // 
            // colPLPNome
            // 
            this.colPLPNome.Caption = "Nome";
            this.colPLPNome.FieldName = "PLPNome";
            this.colPLPNome.Name = "colPLPNome";
            this.colPLPNome.Visible = true;
            this.colPLPNome.VisibleIndex = 5;
            this.colPLPNome.Width = 122;
            // 
            // colPLPEtiqueta
            // 
            this.colPLPEtiqueta.Caption = "Etiqueta";
            this.colPLPEtiqueta.FieldName = "PLPEtiqueta";
            this.colPLPEtiqueta.Name = "colPLPEtiqueta";
            // 
            // colPLPHorario
            // 
            this.colPLPHorario.Caption = "Hor�rio";
            this.colPLPHorario.FieldName = "PLPHorario";
            this.colPLPHorario.Name = "colPLPHorario";
            this.colPLPHorario.Visible = true;
            this.colPLPHorario.VisibleIndex = 6;
            this.colPLPHorario.Width = 129;
            // 
            // colPLPCompetenciaAno
            // 
            this.colPLPCompetenciaAno.Caption = "Ano";
            this.colPLPCompetenciaAno.FieldName = "PLPCompetenciaAno";
            this.colPLPCompetenciaAno.Name = "colPLPCompetenciaAno";
            this.colPLPCompetenciaAno.Width = 58;
            // 
            // colPLPCompetenciaMes
            // 
            this.colPLPCompetenciaMes.Caption = "Mes";
            this.colPLPCompetenciaMes.FieldName = "PLPCompetenciaMes";
            this.colPLPCompetenciaMes.Name = "colPLPCompetenciaMes";
            this.colPLPCompetenciaMes.Width = 53;
            // 
            // colPLPDATAI
            // 
            this.colPLPDATAI.Caption = "Inclus�o";
            this.colPLPDATAI.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.colPLPDATAI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPLPDATAI.FieldName = "PLPDATAI";
            this.colPLPDATAI.Name = "colPLPDATAI";
            this.colPLPDATAI.Visible = true;
            this.colPLPDATAI.VisibleIndex = 7;
            this.colPLPDATAI.Width = 95;
            // 
            // colPLPDataRetorno
            // 
            this.colPLPDataRetorno.Caption = "Retorno";
            this.colPLPDataRetorno.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.colPLPDataRetorno.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPLPDataRetorno.FieldName = "PLPDataRetorno";
            this.colPLPDataRetorno.Name = "colPLPDataRetorno";
            this.colPLPDataRetorno.Visible = true;
            this.colPLPDataRetorno.VisibleIndex = 8;
            this.colPLPDataRetorno.Width = 97;
            // 
            // colPLPDataApontamento
            // 
            this.colPLPDataApontamento.Caption = "Apontamento";
            this.colPLPDataApontamento.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.colPLPDataApontamento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPLPDataApontamento.FieldName = "PLPDataApontamento";
            this.colPLPDataApontamento.Name = "colPLPDataApontamento";
            this.colPLPDataApontamento.Visible = true;
            this.colPLPDataApontamento.VisibleIndex = 9;
            this.colPLPDataApontamento.Width = 95;
            // 
            // colPLPApontamento_USU
            // 
            this.colPLPApontamento_USU.Caption = "Resp Apontamento";
            this.colPLPApontamento_USU.FieldName = "USUNomeIAp";
            this.colPLPApontamento_USU.Name = "colPLPApontamento_USU";
            // 
            // colPLPI_USU
            // 
            this.colPLPI_USU.Caption = "Resp. inclus�o";
            this.colPLPI_USU.FieldName = "USUNomeI";
            this.colPLPI_USU.Name = "colPLPI_USU";
            // 
            // colcopiaPLP_CON
            // 
            this.colcopiaPLP_CON.Caption = "Condom�nio";
            this.colcopiaPLP_CON.FieldName = "CONNome";
            this.colcopiaPLP_CON.Name = "colcopiaPLP_CON";
            this.colcopiaPLP_CON.Visible = true;
            this.colcopiaPLP_CON.VisibleIndex = 3;
            this.colcopiaPLP_CON.Width = 129;
            // 
            // colCidade
            // 
            this.colCidade.Caption = "Cidade";
            this.colCidade.FieldName = "Cidade";
            this.colCidade.Name = "colCidade";
            this.colCidade.Visible = true;
            this.colCidade.VisibleIndex = 10;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.memoEdit1);
            this.panelControl2.Controls.Add(this.cBotaoImpBol3);
            this.panelControl2.Controls.Add(this.cBotaoImpBol1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1319, 86);
            this.panelControl2.TabIndex = 1;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(141, 9);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Size = new System.Drawing.Size(643, 59);
            this.memoEdit1.TabIndex = 10;
            // 
            // cEmitePlanilha
            // 
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cEmitePlanilha";
            this.Size = new System.Drawing.Size(1568, 601);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chSoplan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.funcionariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.SimpleButton BotCarregar;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource funcionariosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCondominio;
        private DevExpress.XtraGrid.Columns.GridColumn colHorario;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistro;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol3;
        private DevExpress.XtraGrid.Columns.GridColumn colCargo;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colData;
        private DevExpress.XtraEditors.SimpleButton BTGrava;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colPLP;
        private DevExpress.XtraGrid.Columns.GridColumn colPLP_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPNome;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPEtiqueta;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPHorario;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPCompetenciaAno;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPCompetenciaMes;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPDataRetorno;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPDataApontamento;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPApontamento_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPLPI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colcopiaPLP_CON;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.Columns.GridColumn colCidade;
        private DevExpress.XtraEditors.CheckEdit chSoplan;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP;
    }
}
