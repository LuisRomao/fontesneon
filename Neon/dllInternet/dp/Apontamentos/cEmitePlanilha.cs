using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DP.Automacao_Bancaria;
using DP.impressos;
using DevExpress.XtraReports.UI;
using CompontesBasicosProc;

namespace DP.Apontamentos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cEmitePlanilha : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cEmitePlanilha()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                funcionariosBindingSource.DataSource = ImpPonto.dPonto;                
            }
            
        }

        private impressos.ImpPonto _ImpPonto;

        private impressos.ImpPonto ImpPonto
        {
            get
            {
                if (_ImpPonto == null)
                {
                    _ImpPonto = new DP.impressos.ImpPonto();
                    VirMSSQL.TableAdapter.AjustaAutoInc(_ImpPonto.dPonto);
                }
                return _ImpPonto;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            BTGrava.Enabled=false;
            ImpPonto.dPonto.PLanilhaPonto.Clear();
            if (chSoplan.Checked)
            {
                dPonto.PLanilhaPontoTableAdapter.FillBymesAnoPlanilha(ImpPonto.dPonto.PLanilhaPonto, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
                dPonto.PLanilhaPontoTableAdapterF.FillBymesAnoPlanilha(ImpPonto.dPonto.PLanilhaPonto, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
            }
            else
            {
                impressos.dPonto.PLanilhaPontoTableAdapter.FillBymesAno(ImpPonto.dPonto.PLanilhaPonto, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
                impressos.dPonto.PLanilhaPontoTableAdapterF.FillBymesAno(ImpPonto.dPonto.PLanilhaPonto, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
            }
            ImpPonto.dPonto.ResumoTableAdapter.Fill(ImpPonto.dPonto.Resumo, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
            ImpPonto.dPonto.ResumoTableAdapterF.Fill(ImpPonto.dPonto.Resumo, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
            if (ImpPonto.dPonto.PLanilhaPonto.Count == 0)
            {
                string Arquivo;
                string diretorio;
                if (System.IO.Directory.Exists(@"\\neon02\Folha\Virtual\PLANILHA_PONTO"))
                    diretorio = @"\\neon02\Folha\Virtual\PLANILHA_PONTO\";
                else
                    diretorio = @"C:\virtual\PLANILHA_PONTO\";
                Arquivo = diretorio + cCompet1.Comp.ToStringN() + ".xml";
                if (!System.IO.File.Exists(Arquivo))
                    if (!System.IO.File.Exists(diretorio + cCompet1.Comp.ToStringN()))
                    {
                        MessageBox.Show("Arquivo n�o encontrado: " + Arquivo);
                        return;
                    };


                DataSet DSLeitura = new DataSet();
                DSLeitura.ReadXmlSchema(Arquivo);
                DataTable T = DSLeitura.Tables["Registro"];
                T.Columns["COD_EMPRES"].DataType = typeof(string);
                T.Columns["REGISTRO"].DataType = typeof(int);
                DSLeitura.ReadXml(Arquivo);
                int? CodCondFolha1;
                string CodCondFolha2;
                string NomeCondominio;
                string Endereco = "";
                bool ComErros = false;
                dAutomacaoBancaria.dAutomacaoBancariaSt.CONDOMINIOS.Clear();
                System.Collections.ArrayList Feriados = new System.Collections.ArrayList();
                foreach (impressos.dPonto.FeriadosRow rowF in ImpPonto.dPonto.Feriados)
                    Feriados.Add(rowF.Data);
                ImpPonto.CargaInicial(cCompet1.Comp, Feriados);
                ImpPonto.dPonto.Funcionarios.Clear();
                dAutomacaoBancaria dAutomacaoBancaria1 = new dAutomacaoBancaria();
                foreach (DataRow DR in T.Rows)
                {
                    //if (contador++ > 10)
                    //    break;
                    CodCondFolha2 = (string)DR["COD_EMPRES"];
                    CodCondFolha1 = null;
                    int try1;
                    if (int.TryParse(CodCondFolha2, out try1))
                    {
                        CodCondFolha1 = try1;
                        CodCondFolha2 = CodCondFolha1.Value.ToString("000");
                    }
                    //dAutomacaoBancaria.CONDOMINIOSRow rowCond = dAutomacaoBancaria.dAutomacaoBancariaSt.BuscaCondominio(CodCondFolha1,CodCondFolha2,false);
                    dAutomacaoBancaria.CONDOMINIOSRow rowCond = dAutomacaoBancaria1.BuscaCondominio(CodCondFolha1, CodCondFolha2, true);
                    int CON;
                    if (rowCond == null)
                    {
                        NomeCondominio = string.Format("Condom�nio n�o identificado ({0})",CodCondFolha2);
                        ComErros = true;
                        CON = -1;
                    }
                    else
                    {
                        NomeCondominio = CodCondFolha2 + " - " + rowCond.CONNome;
                        Endereco = rowCond.CONEndereco;
                        CON = rowCond.CON;
                    }
                    DP.impressos.dPonto.FuncionariosRow NovaLinha = ImpPonto.dPonto.Funcionarios.NewFuncionariosRow();
                    NovaLinha.CON = CON;
                    NovaLinha.EMP = rowCond.CON_EMP;
                    NovaLinha.CONFolha = CodCondFolha2;
                    NovaLinha.Registro = (int)DR["REGISTRO"];
                    NovaLinha.Nome = (string)DR["NOME"];
                    NovaLinha.Cargo = (string)DR["CARGO"];
                    NovaLinha.Condominio = NomeCondominio;
                    NovaLinha.Etiqueta = NomeCondominio + "\r\n" +
                        Endereco + "\r\n" +
                        ((int)DR["REGISTRO"]).ToString() + " " + (string)DR["NOME"] + " - " + (string)DR["CARGO"] + "\r\n" +
                        cCompet1.Comp.ToString() + " - " + (string)DR["HOR_DESCRI"];
                    //NovaLinha.CompetenciaHorario = cCompet1.Comp.ToString() +" - "+ (string)DR["HOR_DESCRI"];
                    NovaLinha.Horario = (string)DR["HOR_DESCRI"];
                    ImpPonto.dPonto.Funcionarios.AddFuncionariosRow(NovaLinha);
                    if (rowCond == null)
                        NovaLinha.RowError = "Condom�nio n�o encontrado";
                };
                //funcionariosBindingSource.DataSource = ImpPonto.dPonto;
                if (ComErros)
                    MessageBox.Show("ATEN��O\r\n\r\nErro na carga");
                BTGrava.Enabled = true;
                xtraTabControl1.SelectedTabPage = xtraTabPage1;
            }
            else {
                System.Collections.ArrayList Feriados = new System.Collections.ArrayList();
                foreach (impressos.dPonto.FeriadosRow rowF in ImpPonto.dPonto.Feriados)
                    Feriados.Add(rowF.Data);
                ImpPonto.CargaInicial(cCompet1.Comp, Feriados);
                xtraTabControl1.SelectedTabPage = xtraTabPage2;
            }
            cBotaoImpBol1.Enabled = cBotaoImpBol3.Enabled = true;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //int[] Selecionados = gridView1.GetSelectedRows();
            //foreach(DP.impressos.dPonto.FuncionariosRow
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            ImpPonto.FilterString = "";
            ImpPonto.MemImp.Text = memoEdit1.Text;
            ImpPonto.BandaMemImp.Visible = (memoEdit1.Text.Trim() != "");
            switch (cBotaoImpBol1.BotaoClicado)
            {
                case dllImpresso.Botoes.Botao.PDF_frente:
                    break;
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.imprimir_frente:
                    foreach (impressos.dPonto.ResumoRow ResRow in ImpPonto.dPonto.Resumo)
                    {
                        ImpPonto.FilterString = "PLP_CON = "+ResRow.PLP_CON;
                        ImpPonto.CreateDocument(false);
                        if (ImpPonto.Pages.Count > 0)
                        {                            
                            Framework.DSCentral.IMPressoesTableAdapter.Incluir(ResRow.PLP_CON, 37, ImpPonto.Pages.Count, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "A - Planilha de ponto", false);        
                            ImpPonto.Print();
                        }
                    }
                    break;                                    
                case dllImpresso.Botoes.Botao.email:                                        
                    break;                                 
                case dllImpresso.Botoes.Botao.nulo:
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    ImpPonto.CreateDocument(true);
                    ImpPonto.ShowPreviewDialog();
                    break;
                default:
                    break;
            }
        }

        private DateTime[] DecodificaHorario(string horario) {
            DateTime[] Retorno = new DateTime[4];
            try
            {
                for (int i = 0; i < 4; i++)
                {
                    int hora = int.Parse(horario.Substring(6 * i, 2));
                    int minuto = int.Parse(horario.Substring(3 + 6 * i, 2));
                    Retorno[i] = DateTime.Today + new TimeSpan(hora, minuto, 0);
                }
            }
            catch {
                return null;
            }
            
            return Retorno;
        }

        System.Collections.ArrayList _FilhotesAbertos;
        System.Collections.ArrayList FilhotesAbertos {
            get {
                if (_FilhotesAbertos == null)
                    _FilhotesAbertos = new System.Collections.ArrayList();
                return _FilhotesAbertos;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filhote"></param>
        public void UnRegistraFilhote(cApontamentoHoras Filhote) {
            FilhotesAbertos.Remove(Filhote);
            if(FilhotesAbertos.Count == 0)
                BotCarregar.Enabled = cCompet1.Enabled = true;
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {            
            //DESLIGADO
            /*
            impressos.dPonto.PLanilhaPontoRow LinhaMae = (impressos.dPonto.PLanilhaPontoRow)gridView3.GetFocusedDataRow();
            if (LinhaMae != null)
            {
                cApontamentoHoras NovaTela = new cApontamentoHoras();
                NovaTela.Competencia = cCompet1.Comp.CloneCompet();
                FilhotesAbertos.Add(NovaTela);
                BotCarregar.Enabled = cCompet1.Enabled = false;
                NovaTela.Chamador = this;
                NovaTela.titulo = LinhaMae.PLPNome;
                NovaTela.registro.Value = LinhaMae.PLPRegistro / 10;
                if (!LinhaMae.IsPLPCONFolhaNull())
                    NovaTela.CondFolha.Text = LinhaMae.PLPCONFolha.ToString("000");
                NovaTela.RowChamadora = LinhaMae;
                
                dPlaPonto.DadosGeraisRow NovaLinha = NovaTela.dPlaPonto.DadosGerais.NewDadosGeraisRow();
                NovaTela.LinhaGeral = NovaLinha;
                NovaTela.dadosGeraisBindingSource.DataSource = NovaLinha;
                DateTime[] Horario = DecodificaHorario(LinhaMae.PLPHorario);


                if (Horario != null)
                {
                    NovaLinha.H1 = Horario[0];
                    NovaLinha.H4 = Horario[3];
                    NovaLinha.H2 = Horario[1];
                    NovaLinha.H3 = Horario[2];
                    NovaLinha.SH1 = Horario[0];
                    NovaLinha.SH4 = Horario[3];
                    NovaLinha.SH2 = Horario[1];
                    NovaLinha.SH3 = Horario[2];
                    if ((Horario[2] != DateTime.MinValue) && (Horario[0] != DateTime.MinValue))
                    {
                        if (Horario[3] < Horario[0])
                            Horario[3] = Horario[3].AddDays(1);
                        TimeSpan Jornada = (Horario[3] - Horario[0]);
                        NovaLinha.Turno = (Jornada.TotalHours == 8);
                    };
                }
                else
                {
                    NovaLinha.Turno = true;
                }

                NovaLinha.maracarAlmoco = false;
                NovaLinha.Ex188 = 0;
                NovaLinha.Ex013 = NovaLinha.Ex146 = NovaLinha.Ex148 = NovaLinha.Ex173 = 0;
                NovaLinha.Ex176 = NovaLinha.Ex180 = NovaLinha.Ex181 = NovaLinha.Ex421 = 0;
                NovaLinha.Ex422 = NovaLinha.Ex543 = NovaLinha.Ex1 = NovaLinha.Ex2 = 0;
                NovaLinha.NEx1 = NovaLinha.NEx2 = 0;
                NovaLinha.PLR = LinhaMae.PLP;
                NovaTela.dPlaPonto.DadosGerais.AddDadosGeraisRow(NovaLinha);
                NovaTela.dadosGeraisBindingSource.Position = 0;
                NovaTela.Padrao(true);
                NovaTela.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            };
            
            */
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("DP cEmiteplanilha - 278");
                    impressos.dPonto.PLanilhaPontoTableAdapter.EmbarcaEmTransST();
                    impressos.dPonto.PLanilhaPontoTableAdapterF.EmbarcaEmTransST();
                    ESP.AtivaGauge(ImpPonto.dPonto.Funcionarios.Count);
                    int i = 0;
                    DateTime prox = DateTime.Now.AddSeconds(3);
                    Application.DoEvents();
                    foreach (impressos.dPonto.FuncionariosRow Fun in ImpPonto.dPonto.Funcionarios)
                    {
                        i++;
                        if (DateTime.Now > prox)
                        {
                            ESP.Gauge(i);
                            prox = DateTime.Now.AddSeconds(3);
                            Application.DoEvents();
                        }
                        if (Fun.CON != -1)
                        {
                            impressos.dPonto.PLanilhaPontoRow NovaLinha = ImpPonto.dPonto.PLanilhaPonto.NewPLanilhaPontoRow();
                            NovaLinha.PLP_CON = Fun.CON;
                            NovaLinha.PLPCompetenciaAno = cCompet1.Comp.Ano;
                            NovaLinha.PLPCompetenciaMes = cCompet1.Comp.Mes;
                            NovaLinha.PLPDATAI = DateTime.Now;
                            NovaLinha.PLPEtiqueta = Fun.Etiqueta;
                            NovaLinha.PLPHorario = Fun.Horario;
                            NovaLinha.PLPI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                            NovaLinha.PLPNome = Fun.Nome;
                            NovaLinha.PLPRegistro = Fun.Registro;
                            int CONFolha;
                            if (int.TryParse(Fun.CONFolha, out CONFolha))
                                NovaLinha.PLPCONFolha = CONFolha;
                            ImpPonto.dPonto.PLanilhaPonto.AddPLanilhaPontoRow(NovaLinha);
                            impressos.dPonto.PLanilhaPontoTableAdapterX(Fun.EMP).Update(NovaLinha);
                        }
                    };
                    VirMSSQL.TableAdapter.CommitSQLDuplo();
                    MessageBox.Show("Gravado");
                    ImpPonto.dPonto.PLanilhaPonto.Clear();
                    dPonto.PLanilhaPontoTableAdapter.FillBymesAnoPlanilha(ImpPonto.dPonto.PLanilhaPonto, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
                    dPonto.PLanilhaPontoTableAdapterF.FillBymesAnoPlanilha(ImpPonto.dPonto.PLanilhaPonto, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                    throw ex;
                }
            }
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void cBotaoImpBol3_clicado(object sender, EventArgs e)
        {
            foreach (impressos.dPonto.PLanilhaPontoRow Linha in ImpPonto.dPonto.PLanilhaPonto)
                Linha.Imprimir = false;
            ImpPonto.MemImp.Text = memoEdit1.Text;
            ImpPonto.BandaMemImp.Visible = (memoEdit1.Text.Trim() != "");
            foreach (int linha in gridView3.GetSelectedRows()) {
                impressos.dPonto.PLanilhaPontoRow LinhaSel = (impressos.dPonto.PLanilhaPontoRow)gridView3.GetDataRow(linha);
                LinhaSel.Imprimir = true;
                if ((cBotaoImpBol3.BotaoClicado == dllImpresso.Botoes.Botao.imprimir) || (cBotaoImpBol3.BotaoClicado == dllImpresso.Botoes.Botao.imprimir_frente) || (cBotaoImpBol3.BotaoClicado == dllImpresso.Botoes.Botao.botao))
                      Framework.DSCentral.IMPressoesTableAdapter.Incluir(LinhaSel.PLP_CON, 37, 1, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "A - Planilha de ponto", false);
            };
            ImpPonto.FilterString = "[Imprimir] = True";

            //ImpPonto.FilterString


            switch (cBotaoImpBol3.BotaoClicado)
            {
                case dllImpresso.Botoes.Botao.PDF_frente:
                    break;
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.imprimir_frente:
                    ImpPonto.CreateDocument(false);
                    ImpPonto.Print();
                    break;
                case dllImpresso.Botoes.Botao.email:
                    break;
                case dllImpresso.Botoes.Botao.nulo:
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    ImpPonto.CreateDocument(true);
                    ImpPonto.ShowPreviewDialog();
                    break;
                default:
                    break;
            };



        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            cExportar Exp = new cExportar();
            if (Exp.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) != DialogResult.OK)
                return;
            
            if (DP.impressos.dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.Count == 0)
                return;
            ArquivosTXT.cTabelaTXT TXT = new ArquivosTXT.cTabelaTXT(DP.impressos.dGravarDTM.dGravarDTMSt.TMPApontamentoDTM, ArquivosTXT.ModelosTXT.layout);
            TXT.RegistraMapa(dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.TMPEmpresaColumn, 1, 3);
            TXT.RegistraMapa(dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.TMPRegistroColumn, 4, 6);
            TXT.RegistraMapa(dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.TMPEventoColumn, 10, 3);
            TXT.RegistraMapa(dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.TMPReferenciaColumn, 13, 5);
            TXT.RegistraMapa(dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.TMPValorColumn, 18, 11);
            System.Windows.Forms.SaveFileDialog SF = new SaveFileDialog();
            SF.DefaultExt = "TXT";
            SF.FileName = @"\\neon02\Folha\DTM\Apontamentos\Teste.txt";            
            if (SF.ShowDialog() == DialogResult.OK)
            {
                TXT.Salva(SF.FileName);                
                DP.impressos.dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.Clear();
                DP.impressos.dGravarDTM.dGravarDTMSt.TMPApontamentoDTMTableAdapter.Limpa();
            };
            
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            ImpPonto.dPonto.PLanilhaPonto.Clear();
            ImpPonto.dPonto.Resumo.Clear();
            ImpPonto.dPonto.Funcionarios.Clear();
            BTGrava.Enabled = false;
        }

        

        private void gridView3_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            if (e.Column.EstaNoGrupo(colPLP_CON))
            {
                impressos.dPonto.PLanilhaPontoRow row = (impressos.dPonto.PLanilhaPontoRow)gridView3.GetDataRow(e.RowHandle);
                if (row == null)
                    return;
                if (!row.IsCON_EMPNull())
                {
                    e.Appearance.BackColor = e.Appearance.BackColor2 = row.CON_EMP == Framework.DSCentral.EMP ? System.Drawing.Color.PaleGreen : System.Drawing.Color.LemonChiffon;
                    e.Appearance.ForeColor = Color.Black;
                }
                else
                {
                    e.Appearance.BackColor = Color.Pink;
                    e.Appearance.ForeColor = Color.Black;
                }
            }
        }

        /*
        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            
            DataSet DS = new DataSet("DS");
            DS.Tables.Add(TableAdapter.ST().BuscaSQLTabela("SELECT PLanilhaPonto.* FROM PLanilhaPonto"));
            DS.Tables[0].TableName = "PLanilhaPonto";
            //DS.WriteXml("C:\\dados.xml");
            //MessageBox.Show("ok");
            
            //DataSet novoDS = new DataSet();
            //novoDS.Tables.Add("PLanilhaPonto");
            DataTable Tabela = DS.Tables[0];
            Tabela.Clear();
            DS.ReadXml("C:\\dados.xml");
            
            

            foreach (DataRow DR in Tabela.Rows) 
            {
                //TableAdapter.ST().Cone.Open();
                if (!TableAdapter.ST().EstaCadastrado("select PLP from PLanilhaPonto where PLP = @P1", DR["PLP"]))
                {
                    //TableAdapter.ST().IniciaTrasacao();
                    //TableAdapter.ST().ExecutarSQLNonQuery("SET IDENTITY_INSERT PLanilhaPonto ON;");
                    //TableAdapter.ST().Insert("PLanilhaPonto", DR, true, false);
                    //String comando = "insert into PLanilhaPonto (PLP,PLP_CON,PLPRegistro,PLPNome,PLPEtiqueta,PLPHorario,PLPCompetenciaAno,PLPCompetenciaMes,PLPDATAI,PLPI_USU,PLPCONFolha) values(@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11)";
                    TableAdapter.ST().Insert(DR, true, false);
                    //TableAdapter.ST().ExecutarSQLNonQuery("SET IDENTITY_INSERT PLanilhaPonto OFF;");
                    //VirMSSQL.TableAdapter.ST().Commit();
                }
                //TableAdapter.ST().Cone.Close();
            }
        }
        */ 
    }
}

