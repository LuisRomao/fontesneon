namespace DP.Apontamentos
{
    partial class cFolguista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.e8_1 = new DevExpress.XtraEditors.SimpleButton();
            this.e8_2 = new DevExpress.XtraEditors.SimpleButton();
            this.e8_4 = new DevExpress.XtraEditors.SimpleButton();
            this.e8_3 = new DevExpress.XtraEditors.SimpleButton();
            this.e8_6 = new DevExpress.XtraEditors.SimpleButton();
            this.e8_5 = new DevExpress.XtraEditors.SimpleButton();
            this.e8_8 = new DevExpress.XtraEditors.SimpleButton();
            this.e8_7 = new DevExpress.XtraEditors.SimpleButton();
            this.e5_1 = new DevExpress.XtraEditors.SimpleButton();
            this.e5_2 = new DevExpress.XtraEditors.SimpleButton();
            this.e5_3 = new DevExpress.XtraEditors.SimpleButton();
            this.e5_4 = new DevExpress.XtraEditors.SimpleButton();
            this.e5_5 = new DevExpress.XtraEditors.SimpleButton();
            this.e2_1_6 = new DevExpress.XtraEditors.SimpleButton();
            this.e2_2_6 = new DevExpress.XtraEditors.SimpleButton();
            this.e2_1_18 = new DevExpress.XtraEditors.SimpleButton();
            this.e2_2_18 = new DevExpress.XtraEditors.SimpleButton();
            this.chManual = new DevExpress.XtraEditors.CheckEdit();
            this.BotaoOk = new DevExpress.XtraEditors.SimpleButton();
            this.H4 = new DevExpress.XtraEditors.TimeEdit();
            this.H3 = new DevExpress.XtraEditors.TimeEdit();
            this.H2 = new DevExpress.XtraEditors.TimeEdit();
            this.H1 = new DevExpress.XtraEditors.TimeEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton21 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton22 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton25 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton26 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton27 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton28 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton29 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton30 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton31 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton32 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton33 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton34 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton35 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton36 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton37 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.chManual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // e8_1
            // 
            this.e8_1.Location = new System.Drawing.Point(12, 12);
            this.e8_1.Name = "e8_1";
            this.e8_1.Size = new System.Drawing.Size(25, 23);
            this.e8_1.TabIndex = 0;
            this.e8_1.Tag = "0";
            this.e8_1.Text = "6";
            this.e8_1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // e8_2
            // 
            this.e8_2.Location = new System.Drawing.Point(12, 41);
            this.e8_2.Name = "e8_2";
            this.e8_2.Size = new System.Drawing.Size(25, 23);
            this.e8_2.TabIndex = 1;
            this.e8_2.Tag = "1";
            this.e8_2.Text = "6";
            this.e8_2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // e8_4
            // 
            this.e8_4.Location = new System.Drawing.Point(12, 99);
            this.e8_4.Name = "e8_4";
            this.e8_4.Size = new System.Drawing.Size(25, 23);
            this.e8_4.TabIndex = 3;
            this.e8_4.Tag = "3";
            this.e8_4.Text = "14";
            this.e8_4.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // e8_3
            // 
            this.e8_3.Location = new System.Drawing.Point(12, 70);
            this.e8_3.Name = "e8_3";
            this.e8_3.Size = new System.Drawing.Size(25, 23);
            this.e8_3.TabIndex = 2;
            this.e8_3.Tag = "2";
            this.e8_3.Text = "14";
            this.e8_3.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // e8_6
            // 
            this.e8_6.Location = new System.Drawing.Point(12, 157);
            this.e8_6.Name = "e8_6";
            this.e8_6.Size = new System.Drawing.Size(25, 23);
            this.e8_6.TabIndex = 5;
            this.e8_6.Tag = "5";
            this.e8_6.Text = "22";
            this.e8_6.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // e8_5
            // 
            this.e8_5.Location = new System.Drawing.Point(12, 128);
            this.e8_5.Name = "e8_5";
            this.e8_5.Size = new System.Drawing.Size(25, 23);
            this.e8_5.TabIndex = 4;
            this.e8_5.Tag = "4";
            this.e8_5.Text = "22";
            this.e8_5.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // e8_8
            // 
            this.e8_8.Location = new System.Drawing.Point(12, 215);
            this.e8_8.Name = "e8_8";
            this.e8_8.Size = new System.Drawing.Size(25, 23);
            this.e8_8.TabIndex = 7;
            this.e8_8.Tag = "7";
            this.e8_8.Text = "F";
            this.e8_8.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // e8_7
            // 
            this.e8_7.Location = new System.Drawing.Point(12, 186);
            this.e8_7.Name = "e8_7";
            this.e8_7.Size = new System.Drawing.Size(25, 23);
            this.e8_7.TabIndex = 6;
            this.e8_7.Tag = "6";
            this.e8_7.Text = "F";
            this.e8_7.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // e5_1
            // 
            this.e5_1.Location = new System.Drawing.Point(43, 12);
            this.e5_1.Name = "e5_1";
            this.e5_1.Size = new System.Drawing.Size(25, 23);
            this.e5_1.TabIndex = 8;
            this.e5_1.Tag = "0";
            this.e5_1.Text = "6";
            this.e5_1.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // e5_2
            // 
            this.e5_2.Location = new System.Drawing.Point(43, 41);
            this.e5_2.Name = "e5_2";
            this.e5_2.Size = new System.Drawing.Size(25, 23);
            this.e5_2.TabIndex = 9;
            this.e5_2.Tag = "1";
            this.e5_2.Text = "14";
            this.e5_2.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // e5_3
            // 
            this.e5_3.Location = new System.Drawing.Point(43, 70);
            this.e5_3.Name = "e5_3";
            this.e5_3.Size = new System.Drawing.Size(25, 23);
            this.e5_3.TabIndex = 10;
            this.e5_3.Tag = "2";
            this.e5_3.Text = "22";
            this.e5_3.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // e5_4
            // 
            this.e5_4.Location = new System.Drawing.Point(43, 99);
            this.e5_4.Name = "e5_4";
            this.e5_4.Size = new System.Drawing.Size(25, 23);
            this.e5_4.TabIndex = 11;
            this.e5_4.Tag = "3";
            this.e5_4.Text = "F";
            this.e5_4.Click += new System.EventHandler(this.simpleButton12_Click);
            // 
            // e5_5
            // 
            this.e5_5.Location = new System.Drawing.Point(43, 128);
            this.e5_5.Name = "e5_5";
            this.e5_5.Size = new System.Drawing.Size(25, 23);
            this.e5_5.TabIndex = 12;
            this.e5_5.Tag = "4";
            this.e5_5.Text = "F";
            this.e5_5.Click += new System.EventHandler(this.simpleButton13_Click);
            // 
            // e2_1_6
            // 
            this.e2_1_6.Location = new System.Drawing.Point(74, 12);
            this.e2_1_6.Name = "e2_1_6";
            this.e2_1_6.Size = new System.Drawing.Size(25, 23);
            this.e2_1_6.TabIndex = 13;
            this.e2_1_6.Tag = "0";
            this.e2_1_6.Text = "6";
            this.e2_1_6.Click += new System.EventHandler(this.e2_1_6_Click);
            // 
            // e2_2_6
            // 
            this.e2_2_6.Location = new System.Drawing.Point(74, 41);
            this.e2_2_6.Name = "e2_2_6";
            this.e2_2_6.Size = new System.Drawing.Size(25, 23);
            this.e2_2_6.TabIndex = 14;
            this.e2_2_6.Tag = "1";
            this.e2_2_6.Text = "F";
            this.e2_2_6.Click += new System.EventHandler(this.e2_1_6_Click);
            // 
            // e2_1_18
            // 
            this.e2_1_18.Location = new System.Drawing.Point(105, 12);
            this.e2_1_18.Name = "e2_1_18";
            this.e2_1_18.Size = new System.Drawing.Size(25, 23);
            this.e2_1_18.TabIndex = 15;
            this.e2_1_18.Tag = "0";
            this.e2_1_18.Text = "18";
            this.e2_1_18.Click += new System.EventHandler(this.e2_1_18_Click);
            // 
            // e2_2_18
            // 
            this.e2_2_18.Location = new System.Drawing.Point(105, 41);
            this.e2_2_18.Name = "e2_2_18";
            this.e2_2_18.Size = new System.Drawing.Size(25, 23);
            this.e2_2_18.TabIndex = 16;
            this.e2_2_18.Tag = "1";
            this.e2_2_18.Text = "F";
            this.e2_2_18.Click += new System.EventHandler(this.e2_1_18_Click);
            // 
            // chManual
            // 
            this.chManual.Location = new System.Drawing.Point(10, 342);
            this.chManual.Name = "chManual";
            this.chManual.Properties.Caption = "Fora do padr�o";
            this.chManual.Size = new System.Drawing.Size(162, 19);
            this.chManual.TabIndex = 17;
            this.chManual.Visible = false;
            // 
            // BotaoOk
            // 
            this.BotaoOk.Enabled = false;
            this.BotaoOk.Location = new System.Drawing.Point(177, 344);
            this.BotaoOk.Name = "BotaoOk";
            this.BotaoOk.Size = new System.Drawing.Size(260, 23);
            this.BotaoOk.TabIndex = 18;
            this.BotaoOk.Text = "OK";
            this.BotaoOk.Visible = false;
            this.BotaoOk.Click += new System.EventHandler(this.BotaoOk_Click);
            // 
            // H4
            // 
            this.H4.EditValue = null;
            this.H4.Location = new System.Drawing.Point(384, 271);
            this.H4.Name = "H4";
            this.H4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.H4.Properties.Appearance.Options.UseTextOptions = true;
            this.H4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.H4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)});
            this.H4.Properties.DisplayFormat.FormatString = "HH:mm";
            this.H4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.H4.Properties.EditFormat.FormatString = "HH:mm";
            this.H4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.H4.Properties.Mask.BeepOnError = true;
            this.H4.Properties.Mask.EditMask = "00:00";
            this.H4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.H4.Properties.NullText = "__:__";
            this.H4.Size = new System.Drawing.Size(65, 21);
            this.H4.TabIndex = 22;
            // 
            // H3
            // 
            this.H3.EditValue = null;
            this.H3.Location = new System.Drawing.Point(319, 271);
            this.H3.Name = "H3";
            this.H3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.H3.Properties.Appearance.Options.UseTextOptions = true;
            this.H3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.H3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)});
            this.H3.Properties.DisplayFormat.FormatString = "HH:mm";
            this.H3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.H3.Properties.EditFormat.FormatString = "HH:mm";
            this.H3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.H3.Properties.Mask.BeepOnError = true;
            this.H3.Properties.Mask.EditMask = "00:00";
            this.H3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.H3.Properties.NullText = "__:__";
            this.H3.Size = new System.Drawing.Size(65, 21);
            this.H3.TabIndex = 21;
            // 
            // H2
            // 
            this.H2.EditValue = null;
            this.H2.Location = new System.Drawing.Point(254, 271);
            this.H2.Name = "H2";
            this.H2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.H2.Properties.Appearance.Options.UseTextOptions = true;
            this.H2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.H2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)});
            this.H2.Properties.DisplayFormat.FormatString = "HH:mm";
            this.H2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.H2.Properties.EditFormat.FormatString = "HH:mm";
            this.H2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.H2.Properties.Mask.BeepOnError = true;
            this.H2.Properties.Mask.EditMask = "00:00";
            this.H2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.H2.Properties.NullText = "__:__";
            this.H2.Size = new System.Drawing.Size(65, 21);
            this.H2.TabIndex = 20;
            // 
            // H1
            // 
            this.H1.EditValue = null;
            this.H1.Location = new System.Drawing.Point(189, 271);
            this.H1.Name = "H1";
            this.H1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.H1.Properties.Appearance.Options.UseTextOptions = true;
            this.H1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.H1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)});
            this.H1.Properties.DisplayFormat.FormatString = "HH:mm";
            this.H1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.H1.Properties.EditFormat.FormatString = "HH:mm";
            this.H1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.H1.Properties.Mask.BeepOnError = true;
            this.H1.Properties.Mask.EditMask = "00:00";
            this.H1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.H1.Properties.NullText = "__:__";
            this.H1.Size = new System.Drawing.Size(65, 21);
            this.H1.TabIndex = 19;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(284, 12);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(46, 23);
            this.simpleButton1.TabIndex = 23;
            this.simpleButton1.Tag = "0";
            this.simpleButton1.Text = "5x1 1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(284, 41);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(46, 23);
            this.simpleButton2.TabIndex = 24;
            this.simpleButton2.Tag = "1";
            this.simpleButton2.Text = "5x1 2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(284, 70);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(46, 23);
            this.simpleButton3.TabIndex = 25;
            this.simpleButton3.Tag = "2";
            this.simpleButton3.Text = "5x1 3";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(284, 99);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(46, 23);
            this.simpleButton4.TabIndex = 26;
            this.simpleButton4.Tag = "3";
            this.simpleButton4.Text = "5x1 4";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(284, 128);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(46, 23);
            this.simpleButton5.TabIndex = 27;
            this.simpleButton5.Tag = "4";
            this.simpleButton5.Text = "5x1 5";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(284, 157);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(46, 23);
            this.simpleButton6.TabIndex = 28;
            this.simpleButton6.Tag = "5";
            this.simpleButton6.Text = "5x1 F";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(336, 157);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(46, 23);
            this.simpleButton7.TabIndex = 34;
            this.simpleButton7.Tag = "5";
            this.simpleButton7.Text = "6x2 6";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(336, 128);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(46, 23);
            this.simpleButton8.TabIndex = 33;
            this.simpleButton8.Tag = "4";
            this.simpleButton8.Text = "6x2 5";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(336, 99);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(46, 23);
            this.simpleButton9.TabIndex = 32;
            this.simpleButton9.Tag = "3";
            this.simpleButton9.Text = "6x2 4";
            this.simpleButton9.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(336, 70);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(46, 23);
            this.simpleButton10.TabIndex = 31;
            this.simpleButton10.Tag = "2";
            this.simpleButton10.Text = "6x2 3";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(336, 41);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(46, 23);
            this.simpleButton11.TabIndex = 30;
            this.simpleButton11.Tag = "1";
            this.simpleButton11.Text = "6x2 2";
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(336, 12);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(46, 23);
            this.simpleButton12.TabIndex = 29;
            this.simpleButton12.Tag = "0";
            this.simpleButton12.Text = "6x2 1";
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton13
            // 
            this.simpleButton13.Location = new System.Drawing.Point(336, 215);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(46, 23);
            this.simpleButton13.TabIndex = 36;
            this.simpleButton13.Tag = "7";
            this.simpleButton13.Text = "6x2 F";
            this.simpleButton13.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton14
            // 
            this.simpleButton14.Location = new System.Drawing.Point(336, 186);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(46, 23);
            this.simpleButton14.TabIndex = 35;
            this.simpleButton14.Tag = "6";
            this.simpleButton14.Text = "6x2 F";
            this.simpleButton14.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton15
            // 
            this.simpleButton15.Location = new System.Drawing.Point(440, 128);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(46, 23);
            this.simpleButton15.TabIndex = 41;
            this.simpleButton15.Tag = "4";
            this.simpleButton15.Text = "4x1 F";
            this.simpleButton15.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton16
            // 
            this.simpleButton16.Location = new System.Drawing.Point(440, 99);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(46, 23);
            this.simpleButton16.TabIndex = 40;
            this.simpleButton16.Tag = "3";
            this.simpleButton16.Text = "4x1 4";
            this.simpleButton16.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton17
            // 
            this.simpleButton17.Location = new System.Drawing.Point(440, 70);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(46, 23);
            this.simpleButton17.TabIndex = 39;
            this.simpleButton17.Tag = "2";
            this.simpleButton17.Text = "4x1 3";
            this.simpleButton17.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton18
            // 
            this.simpleButton18.Location = new System.Drawing.Point(440, 41);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(46, 23);
            this.simpleButton18.TabIndex = 38;
            this.simpleButton18.Tag = "1";
            this.simpleButton18.Text = "4x1 2";
            this.simpleButton18.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton19
            // 
            this.simpleButton19.Location = new System.Drawing.Point(440, 12);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(46, 23);
            this.simpleButton19.TabIndex = 37;
            this.simpleButton19.Tag = "0";
            this.simpleButton19.Text = "4x1 1";
            this.simpleButton19.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton20
            // 
            this.simpleButton20.Location = new System.Drawing.Point(167, 41);
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Size = new System.Drawing.Size(25, 23);
            this.simpleButton20.TabIndex = 45;
            this.simpleButton20.Tag = "1";
            this.simpleButton20.Text = "F";
            this.simpleButton20.Click += new System.EventHandler(this.simpleButton21_Click);
            // 
            // simpleButton21
            // 
            this.simpleButton21.Location = new System.Drawing.Point(167, 12);
            this.simpleButton21.Name = "simpleButton21";
            this.simpleButton21.Size = new System.Drawing.Size(25, 23);
            this.simpleButton21.TabIndex = 44;
            this.simpleButton21.Tag = "0";
            this.simpleButton21.Text = "19";
            this.simpleButton21.Click += new System.EventHandler(this.simpleButton21_Click);
            // 
            // simpleButton22
            // 
            this.simpleButton22.Location = new System.Drawing.Point(136, 41);
            this.simpleButton22.Name = "simpleButton22";
            this.simpleButton22.Size = new System.Drawing.Size(25, 23);
            this.simpleButton22.TabIndex = 43;
            this.simpleButton22.Tag = "1";
            this.simpleButton22.Text = "F";
            this.simpleButton22.Click += new System.EventHandler(this.simpleButton23_Click);
            // 
            // simpleButton23
            // 
            this.simpleButton23.Location = new System.Drawing.Point(136, 12);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(25, 23);
            this.simpleButton23.TabIndex = 42;
            this.simpleButton23.Tag = "0";
            this.simpleButton23.Text = "7";
            this.simpleButton23.Click += new System.EventHandler(this.simpleButton23_Click);
            // 
            // simpleButton24
            // 
            this.simpleButton24.Location = new System.Drawing.Point(499, 12);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(111, 23);
            this.simpleButton24.TabIndex = 46;
            this.simpleButton24.Tag = "1";
            this.simpleButton24.Text = "Ferias";
            this.simpleButton24.Click += new System.EventHandler(this.simpleButton24_Click);
            // 
            // simpleButton25
            // 
            this.simpleButton25.Location = new System.Drawing.Point(388, 186);
            this.simpleButton25.Name = "simpleButton25";
            this.simpleButton25.Size = new System.Drawing.Size(46, 23);
            this.simpleButton25.TabIndex = 53;
            this.simpleButton25.Tag = "6";
            this.simpleButton25.Text = "6x1 F";
            this.simpleButton25.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton26
            // 
            this.simpleButton26.Location = new System.Drawing.Point(388, 157);
            this.simpleButton26.Name = "simpleButton26";
            this.simpleButton26.Size = new System.Drawing.Size(46, 23);
            this.simpleButton26.TabIndex = 52;
            this.simpleButton26.Tag = "5";
            this.simpleButton26.Text = "6x1 6";
            this.simpleButton26.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton27
            // 
            this.simpleButton27.Location = new System.Drawing.Point(388, 128);
            this.simpleButton27.Name = "simpleButton27";
            this.simpleButton27.Size = new System.Drawing.Size(46, 23);
            this.simpleButton27.TabIndex = 51;
            this.simpleButton27.Tag = "4";
            this.simpleButton27.Text = "6x1 5";
            this.simpleButton27.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton28
            // 
            this.simpleButton28.Location = new System.Drawing.Point(388, 99);
            this.simpleButton28.Name = "simpleButton28";
            this.simpleButton28.Size = new System.Drawing.Size(46, 23);
            this.simpleButton28.TabIndex = 50;
            this.simpleButton28.Tag = "3";
            this.simpleButton28.Text = "6x1 4";
            this.simpleButton28.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton29
            // 
            this.simpleButton29.Location = new System.Drawing.Point(388, 70);
            this.simpleButton29.Name = "simpleButton29";
            this.simpleButton29.Size = new System.Drawing.Size(46, 23);
            this.simpleButton29.TabIndex = 49;
            this.simpleButton29.Tag = "2";
            this.simpleButton29.Text = "6x1 3";
            this.simpleButton29.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton30
            // 
            this.simpleButton30.Location = new System.Drawing.Point(388, 41);
            this.simpleButton30.Name = "simpleButton30";
            this.simpleButton30.Size = new System.Drawing.Size(46, 23);
            this.simpleButton30.TabIndex = 48;
            this.simpleButton30.Tag = "1";
            this.simpleButton30.Text = "6x1 2";
            this.simpleButton30.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton31
            // 
            this.simpleButton31.Location = new System.Drawing.Point(388, 12);
            this.simpleButton31.Name = "simpleButton31";
            this.simpleButton31.Size = new System.Drawing.Size(46, 23);
            this.simpleButton31.TabIndex = 47;
            this.simpleButton31.Tag = "0";
            this.simpleButton31.Text = "6x1 1";
            this.simpleButton31.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton32
            // 
            this.simpleButton32.Location = new System.Drawing.Point(232, 157);
            this.simpleButton32.Name = "simpleButton32";
            this.simpleButton32.Size = new System.Drawing.Size(46, 23);
            this.simpleButton32.TabIndex = 59;
            this.simpleButton32.Tag = "5";
            this.simpleButton32.Text = "4x2 F";
            this.simpleButton32.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton33
            // 
            this.simpleButton33.Location = new System.Drawing.Point(232, 128);
            this.simpleButton33.Name = "simpleButton33";
            this.simpleButton33.Size = new System.Drawing.Size(46, 23);
            this.simpleButton33.TabIndex = 58;
            this.simpleButton33.Tag = "4";
            this.simpleButton33.Text = "4x2 F";
            this.simpleButton33.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton34
            // 
            this.simpleButton34.Location = new System.Drawing.Point(232, 99);
            this.simpleButton34.Name = "simpleButton34";
            this.simpleButton34.Size = new System.Drawing.Size(46, 23);
            this.simpleButton34.TabIndex = 57;
            this.simpleButton34.Tag = "3";
            this.simpleButton34.Text = "4x2 4";
            this.simpleButton34.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton35
            // 
            this.simpleButton35.Location = new System.Drawing.Point(232, 70);
            this.simpleButton35.Name = "simpleButton35";
            this.simpleButton35.Size = new System.Drawing.Size(46, 23);
            this.simpleButton35.TabIndex = 56;
            this.simpleButton35.Tag = "2";
            this.simpleButton35.Text = "4x2 3";
            this.simpleButton35.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton36
            // 
            this.simpleButton36.Location = new System.Drawing.Point(232, 41);
            this.simpleButton36.Name = "simpleButton36";
            this.simpleButton36.Size = new System.Drawing.Size(46, 23);
            this.simpleButton36.TabIndex = 55;
            this.simpleButton36.Tag = "1";
            this.simpleButton36.Text = "4x2 2";
            this.simpleButton36.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton37
            // 
            this.simpleButton37.Location = new System.Drawing.Point(232, 12);
            this.simpleButton37.Name = "simpleButton37";
            this.simpleButton37.Size = new System.Drawing.Size(46, 23);
            this.simpleButton37.TabIndex = 54;
            this.simpleButton37.Tag = "0";
            this.simpleButton37.Text = "4x2 1";
            this.simpleButton37.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // cFolguista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 323);
            this.Controls.Add(this.simpleButton32);
            this.Controls.Add(this.simpleButton33);
            this.Controls.Add(this.simpleButton34);
            this.Controls.Add(this.simpleButton35);
            this.Controls.Add(this.simpleButton36);
            this.Controls.Add(this.simpleButton37);
            this.Controls.Add(this.simpleButton25);
            this.Controls.Add(this.simpleButton26);
            this.Controls.Add(this.simpleButton27);
            this.Controls.Add(this.simpleButton28);
            this.Controls.Add(this.simpleButton29);
            this.Controls.Add(this.simpleButton30);
            this.Controls.Add(this.simpleButton31);
            this.Controls.Add(this.simpleButton24);
            this.Controls.Add(this.simpleButton20);
            this.Controls.Add(this.simpleButton21);
            this.Controls.Add(this.simpleButton22);
            this.Controls.Add(this.simpleButton23);
            this.Controls.Add(this.simpleButton15);
            this.Controls.Add(this.simpleButton16);
            this.Controls.Add(this.simpleButton17);
            this.Controls.Add(this.simpleButton18);
            this.Controls.Add(this.simpleButton19);
            this.Controls.Add(this.simpleButton13);
            this.Controls.Add(this.simpleButton14);
            this.Controls.Add(this.simpleButton7);
            this.Controls.Add(this.simpleButton8);
            this.Controls.Add(this.simpleButton9);
            this.Controls.Add(this.simpleButton10);
            this.Controls.Add(this.simpleButton11);
            this.Controls.Add(this.simpleButton12);
            this.Controls.Add(this.simpleButton6);
            this.Controls.Add(this.simpleButton5);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.H4);
            this.Controls.Add(this.H3);
            this.Controls.Add(this.H2);
            this.Controls.Add(this.H1);
            this.Controls.Add(this.BotaoOk);
            this.Controls.Add(this.chManual);
            this.Controls.Add(this.e2_2_18);
            this.Controls.Add(this.e2_1_18);
            this.Controls.Add(this.e2_2_6);
            this.Controls.Add(this.e2_1_6);
            this.Controls.Add(this.e5_5);
            this.Controls.Add(this.e5_4);
            this.Controls.Add(this.e5_3);
            this.Controls.Add(this.e5_2);
            this.Controls.Add(this.e5_1);
            this.Controls.Add(this.e8_8);
            this.Controls.Add(this.e8_7);
            this.Controls.Add(this.e8_6);
            this.Controls.Add(this.e8_5);
            this.Controls.Add(this.e8_4);
            this.Controls.Add(this.e8_3);
            this.Controls.Add(this.e8_2);
            this.Controls.Add(this.e8_1);
            this.Name = "cFolguista";
            this.Text = "Folguista";
            ((System.ComponentModel.ISupportInitialize)(this.chManual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton e8_1;
        private DevExpress.XtraEditors.SimpleButton e8_2;
        private DevExpress.XtraEditors.SimpleButton e8_4;
        private DevExpress.XtraEditors.SimpleButton e8_3;
        private DevExpress.XtraEditors.SimpleButton e8_6;
        private DevExpress.XtraEditors.SimpleButton e8_5;
        private DevExpress.XtraEditors.SimpleButton e8_8;
        private DevExpress.XtraEditors.SimpleButton e8_7;
        private DevExpress.XtraEditors.SimpleButton e5_1;
        private DevExpress.XtraEditors.SimpleButton e5_2;
        private DevExpress.XtraEditors.SimpleButton e5_3;
        private DevExpress.XtraEditors.SimpleButton e5_4;
        private DevExpress.XtraEditors.SimpleButton e5_5;
        private DevExpress.XtraEditors.SimpleButton e2_1_6;
        private DevExpress.XtraEditors.SimpleButton e2_2_6;
        private DevExpress.XtraEditors.SimpleButton e2_1_18;
        private DevExpress.XtraEditors.SimpleButton e2_2_18;
        private DevExpress.XtraEditors.CheckEdit chManual;
        private DevExpress.XtraEditors.SimpleButton BotaoOk;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.SimpleButton simpleButton17;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.SimpleButton simpleButton19;
        private DevExpress.XtraEditors.SimpleButton simpleButton20;
        private DevExpress.XtraEditors.SimpleButton simpleButton21;
        private DevExpress.XtraEditors.SimpleButton simpleButton22;
        private DevExpress.XtraEditors.SimpleButton simpleButton23;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TimeEdit H4;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TimeEdit H3;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TimeEdit H2;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TimeEdit H1;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.SimpleButton simpleButton25;
        private DevExpress.XtraEditors.SimpleButton simpleButton26;
        private DevExpress.XtraEditors.SimpleButton simpleButton27;
        private DevExpress.XtraEditors.SimpleButton simpleButton28;
        private DevExpress.XtraEditors.SimpleButton simpleButton29;
        private DevExpress.XtraEditors.SimpleButton simpleButton30;
        private DevExpress.XtraEditors.SimpleButton simpleButton31;
        private DevExpress.XtraEditors.SimpleButton simpleButton32;
        private DevExpress.XtraEditors.SimpleButton simpleButton33;
        private DevExpress.XtraEditors.SimpleButton simpleButton34;
        private DevExpress.XtraEditors.SimpleButton simpleButton35;
        private DevExpress.XtraEditors.SimpleButton simpleButton36;
        private DevExpress.XtraEditors.SimpleButton simpleButton37;
    }
}